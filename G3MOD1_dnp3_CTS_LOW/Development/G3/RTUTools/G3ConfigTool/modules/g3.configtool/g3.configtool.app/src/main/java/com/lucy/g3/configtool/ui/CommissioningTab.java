/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.ui;

import java.awt.Cursor;
import java.awt.FlowLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.ActionMap;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;

import com.l2fprod.common.swing.JButtonBar;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.common.event.Event;
import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.common.event.IEventHandler;
import com.lucy.g3.configtool.rtu.config.ConfigurationManager;
import com.lucy.g3.configtool.rtu.config.task.ConfigFileWriteTask;
import com.lucy.g3.configtool.rtu.connection.ConnectionManager;
import com.lucy.g3.configtool.rtu.restart.RTURestartTask;
import com.lucy.g3.configtool.subsys.realtime.factory.VirtualPointDataFactory;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.UpdatablePage;
import com.lucy.g3.gui.framework.workbench.WorkbenchWindowTab;
import com.lucy.g3.gui.framework.workbench.widgets.TransparentButtonBarUI;
import com.lucy.g3.rtu.commissioning.ui.CommissioningMimicPage;
import com.lucy.g3.rtu.commissioning.ui.CommissioningPage;
import com.lucy.g3.rtu.commissioning.ui.ICommissioningInvoker;
import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.LocalRemoteCode;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.PollingMode;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.RTUCommsService;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData;
import com.lucy.g3.rtu.comms.service.realtime.points.VirtualPointData;
import com.lucy.g3.rtu.comms.service.rtu.control.CommissioningAPI;
import com.lucy.g3.rtu.comms.service.rtu.info.RTUInfo;
import com.lucy.g3.rtu.config.shared.manager.ConfigManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SWITCH_OPERATION;

/**
 * This implementation of <code>TabContent</code> manages a button bar for
 * Commissioning buttons.
 */
public class CommissioningTab extends WorkbenchWindowTab  {
  public final static String ID = "CommissioningTab";
  
  private static final String ACTION_KEY_UPGRADE = "showUpgrade";
  private static final String ACTION_KEY_WIZARD = "showWizard";
  private static final String ACTION_KEY_MIMIC  = "showMimic";
  private static final String ACTION_KEY_CONFIG = "showConfig";

  private final ButtonGroup group;
  private final JButtonBar toolbar;

  private final Page wizardPage;
  private final CommissioningMimicPage mimicPage;
  private final Page firmwareUpgradePage;
  private final ConfigurationRoot configPage;
  
  private UpdatablePage updatingPage;
  private ConnectionState state;
  
  private final CommissioningInvoker invoker = new CommissioningInvoker();

    private final IContext context;


  public CommissioningTab(IContext context, Page firmwareUpgradePage) {
    super(ID);
    this.context = context;
    this.firmwareUpgradePage = firmwareUpgradePage;

    group = new ButtonGroup();
    wizardPage = new CommissioningPage(invoker); 
    mimicPage = new CommissioningMimicPage(invoker); 
    configPage = new ConfigurationRoot(context);

    /* Create toolbar */
    toolbar = new JButtonBar(SwingConstants.VERTICAL) {

      @Override
      public void updateUI() {
        this.setUI(TransparentButtonBarUI.createUI(this));
      }
    };
    toolbar.setOpaque(false);
    toolbar.setBorder(null);
    addToolbarButton(ACTION_KEY_CONFIG);
    addToolbarButton(ACTION_KEY_UPGRADE);
    addToolbarButton(ACTION_KEY_WIZARD);
    addToolbarButton(ACTION_KEY_MIMIC);
    getComponent().setLayout(new FlowLayout());
    getComponent().add(toolbar);

    /* Set the first button selected */
    AbstractButton firstBtn = group.getElements().nextElement();
    firstBtn.setSelected(true);
    firstBtn.getAction().actionPerformed(null);
    
    initEventHandling();
  }
  
  private void initEventHandling() {
    // Refresh mimic panel once RTU config changes
    ConfigurationManager conMgr = context.getComponent(ConfigurationManager.SUBSYSTEM_ID);
    conMgr.addPropertyChangeListener(
        ConfigurationManager.PROPERTY_RTU_CONFIG,
        new PropertyChangeListener() {
  
          @Override
          public void propertyChange(PropertyChangeEvent evt) {
            mimicPage.setConfigData((IConfig) evt.getNewValue());
          }
        });
 
    // Observe connection state
    EventBroker.getInstance().subscribe(EventTopics.EVT_TOPIC_CONN_STATE_CHG, 
        new IEventHandler() {
      @Override
      public void handleEvent(Event event) {
        ConnectionState newState = (ConnectionState) event.getProperty(EventTopics.EVT_PROPERTY_NEW_STATE);
        setState(newState);    
      }
    });
  }
  
  private void setState(ConnectionState state){
    this.state = state;
    triggerUpdating(isTabSelected()); 
  }
  
  @Action
  public void showUpgrade() {
    showContent(firmwareUpgradePage);
  }
  

  @Action
  public void showWizard() {
    showContent(wizardPage);
  }

  @Action
  public void showMimic() {
    showContent(mimicPage);
  }
  
  @Action
  public void showConfig() {
    showContent(configPage);
  }

  private void showContent(Page page) {
    if (updatingPage != null) {
      updatingPage.stopUpdating();
      updatingPage = null;
    }
    
    JFrame root = WindowUtils.getMainFrame();
    try {
      if (root != null) {
        root.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      }
      getPageViewer().show(page);
      
      if (page != null && page instanceof UpdatablePage) {
        this.updatingPage = (UpdatablePage) page;
      }
      
      triggerUpdating(isTabSelected());

    } finally {
      if (root != null) {
        root.setCursor(Cursor.getDefaultCursor());
      }
    }
  }

  private void addToolbarButton(String name) {
    ActionMap actions = Application.getInstance().getContext().getActionMap(this);
    JToggleButton btn = new JToggleButton(actions.get(name));
    toolbar.add(btn);
    group.add(btn);
  }


  private void triggerUpdating(boolean selected) {
    if (updatingPage != null) {
      if (selected && (ConnectionState.isConnected(state))) {
        updatingPage.startUpdating();
      } else {
        updatingPage.stopUpdating();
      }
    }
  }

  // ====== ICommissioningInvoker Implementation ======
  private class CommissioningInvoker implements ICommissioningInvoker {
  @Override
  public IConfig getConfigData() {
    return ConfigManager.getInstance().getLocalConfig();
  }

  @Override
  public Application getApp() {
    return Application.getInstance();
  }

  @Override
  public ConnectionState getConnectionState() {
    return getConnectionMgr().getConnectionState();
  }

  @Override
  public void loginWithLastAccount() {
    getConnectionMgr().setupConnection(null, false);
  }
  private ConnectionManager getConnectionMgr() {
    return context.getComponent(ConnectionManager.SUBSYSTEM_ID);
  }

  @Override
  public void showLoginDialog() {
    getConnectionMgr().connect();
  }

  @Override
  public void cmdGetPointStatus(List<IPointData> pointList, PollingMode mode) throws SerializationException, IOException {
    getComms().getRtuPoints().cmdGetPointStatus(pointList, mode);
  }
  
  private RTUCommsService getComms(){
    return getG3RTU().getComms();
  }

  @Override
  public void cmdOperateLogic(short group, short period) throws IOException, SerializationException {
    getComms().cmdOperateLogic(group, period);
  }

  @Override
  public void cmdOperateSwitch(short group, SWITCH_OPERATION operation, LocalRemoteCode localRemote) 
      throws IOException, SerializationException {
    getComms().cmdOperateSwitch(group, operation, localRemote);
  }

  @Override
  public void cmdRegisterModules() throws IOException, SerializationException {
    getComms().cmdRegisterModules();
  }


  @Override
  public void cmdActivateConfig() throws IOException, SerializationException {
    getComms().cmdActivateConfig();
  }
  
  @Override
  public String execTaskWriteConfigAndWait(IConfig configData) throws Exception {
    ConfigurationManager configMgr = context.getComponent(ConfigurationManager.SUBSYSTEM_ID);
    
    ConfigFileWriteTask task = configMgr .writeConfig(configData, false, true);
    return task.findNewIP();
  }

  @Override
  public void execTaskRestartRTUAndWait(String newIP) throws Exception {
    RTURestartTask task = new RTURestartTask(getApp(),
        CommissioningTab.this.context,
        getComms(),
        G3Protocol.RestartMode.RESTART,
        newIP);
    task.doInBackground();
  }

  @Override
  public TaskService getG3TaskService() {
    return getApp().getContext().getTaskService(IContext.COMMS_TASK_SERVICE_NAME);
  }

  @Override
  public VirtualPointData createPointData(VirtualPoint point) {
    List<VirtualPointData> pointData = VirtualPointDataFactory.createPointData(point);
    return pointData.isEmpty() ? null : pointData.get(0); //REVIEW
  }

  private G3RTU getG3RTU() {
    return G3RTUFactory.getDefault();
  }

  @Override
  public Task<?,?> createConfigImportTask() {
    ConfigurationManager configMgr = context.getComponent(ConfigurationManager.SUBSYSTEM_ID);
    return configMgr.createConfigImportTask();
  }

  @Override
  public CommissioningAPI getCommissioningCMD() {
    return getComms();
  }

  @Override
  public G3ConfigAPI getG3ConfigAPI() {
    return getComms().getRtuInfo().getConfigAPI();
  }

  @Override
  public RTUInfo getRTUInfo() {
    return getComms().getRtuInfo();
  }
  }

  @Override
  protected void tabSelectedActionPerform(boolean selected) {
    triggerUpdating(selected);    
  }

}
