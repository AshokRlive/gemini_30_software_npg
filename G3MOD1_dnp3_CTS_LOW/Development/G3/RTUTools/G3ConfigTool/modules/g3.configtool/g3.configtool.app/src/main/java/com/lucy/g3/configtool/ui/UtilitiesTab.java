/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.ui;

import java.awt.Cursor;
import java.awt.FlowLayout;

import javax.swing.AbstractButton;
import javax.swing.ActionMap;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

import org.jdesktop.application.Action;
import org.jdesktop.application.Application;

import com.l2fprod.common.swing.JButtonBar;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.rtu.test.RTUTester;
import com.lucy.g3.configtool.rtu.test.ui.TestPage;
import com.lucy.g3.configtool.rtu.upgrade.ui.FirmwareUpgradePage;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.workbench.WorkbenchWindowTab;
import com.lucy.g3.gui.framework.workbench.widgets.TransparentButtonBarUI;
import com.lucy.g3.rtu.manager.G3RTUFactory;

/**
 * This implementation of <code>TabContent</code> manages a button bar for
 * utilities buttons.
 */
public class UtilitiesTab extends WorkbenchWindowTab {
  public final static String ID = "UtilityTabContent";

  private static final String ACTION_KEY_UPGRADE = "showUpgrade";
  private static final String ACTION_KEY_TEST = "showTest";

  private final ButtonGroup group;
  private final JButtonBar toolbar;

  private final Page firwmarePage;
  private final Page rtuTestPage;


  public UtilitiesTab(IContext context) {
    super(ID);
    group = new ButtonGroup();
    
    firwmarePage = new FirmwareUpgradePage(context, G3RTUFactory.getDefault());
    rtuTestPage = new TestPage((RTUTester)context.getComponent(RTUTester.SUBSYSTEM_ID));

    /* Create toolbar */
    toolbar = new JButtonBar(SwingConstants.VERTICAL) {

      @Override
      public void updateUI() {
        this.setUI(TransparentButtonBarUI.createUI(this));
      }
    };
    toolbar.setOpaque(false);
    toolbar.setBorder(null);
    addToolbarButton(ACTION_KEY_TEST);
    addToolbarButton(ACTION_KEY_UPGRADE);
    getComponent().setLayout(new FlowLayout());
    getComponent().add(toolbar);

    /* Set the first button selected */
    AbstractButton firstBtn = group.getElements().nextElement();
    firstBtn.setSelected(true);
    firstBtn.getAction().actionPerformed(null);
  }

  @Action
  public void showUpgrade() {
    showContent(firwmarePage);
  }

  @Action
  public void showTest() {
    showContent(rtuTestPage);
  }

  private void showContent(Page page) {
    JFrame root = WindowUtils.getMainFrame();
    try {
      if (root != null) {
        root.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      }
      getPageViewer().show(page);

    } finally {
      if (root != null) {
        root.setCursor(Cursor.getDefaultCursor());
      }
    }
  }

  private void addToolbarButton(String name) {
    ActionMap actions = Application.getInstance().getContext().getActionMap(this);
    JToggleButton btn = new JToggleButton(actions.get(name));
    toolbar.add(btn);
    group.add(btn);

  }
  
  public Page getFirwmarePage() {
    return firwmarePage;
  }


  @Override
  protected void tabSelectedActionPerform(boolean selected) {
    
  }
}
