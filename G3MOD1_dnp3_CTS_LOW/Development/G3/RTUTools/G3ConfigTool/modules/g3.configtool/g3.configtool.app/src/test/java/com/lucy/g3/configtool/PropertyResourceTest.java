/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool;

import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.configtool.app.ConfigToolApp;
import com.lucy.g3.configtool.preferences.ui.PreferenceDialog;
import com.lucy.g3.configtool.rtu.config.ConfigurationManager;
import com.lucy.g3.configtool.rtu.config.task.ConfigFileExportTask;
import com.lucy.g3.configtool.rtu.config.task.ConfigFileImportTask;
import com.lucy.g3.configtool.rtu.config.task.ConfigFileOperationTask;
import com.lucy.g3.configtool.rtu.config.task.ConfigFileReadTask;
import com.lucy.g3.configtool.rtu.config.task.ConfigFileWriteTask;
import com.lucy.g3.configtool.rtu.connection.ConnectionManager;
import com.lucy.g3.configtool.rtu.controller.RTUController;
import com.lucy.g3.configtool.rtu.controller.task.RTUInfoReadTask;
import com.lucy.g3.configtool.rtu.controller.task.RegisterModulesTask;
import com.lucy.g3.configtool.rtu.diagnostic.ui.ChannelDiagnostic;
import com.lucy.g3.configtool.rtu.diagnostic.ui.VirtualPointDiagnostic;
import com.lucy.g3.configtool.rtu.eventlog.manager.EventLogManager;
import com.lucy.g3.configtool.rtu.restart.RTURestartTask;
import com.lucy.g3.configtool.rtu.sysalarm.SystemAlarmsManager;
import com.lucy.g3.configtool.rtu.syslog.manager.SystemLogManager;
import com.lucy.g3.configtool.rtu.syslog.ui.LogLevelDialog;
import com.lucy.g3.configtool.rtu.test.RTUTester;
import com.lucy.g3.configtool.rtu.test.ui.AutoTestWindow;
import com.lucy.g3.configtool.rtu.test.ui.FuncTestWindow;
import com.lucy.g3.configtool.rtu.upgrade.RTUUpgrader;
import com.lucy.g3.configtool.rtu.upgrade.SDPUpgrader;
import com.lucy.g3.configtool.rtu.upgrade.task.FactoryResetTask;
import com.lucy.g3.configtool.rtu.upgrade.task.FirmwareUpgradeTask;
import com.lucy.g3.configtool.rtu.upgrade.task.SlaveUpgradeTask;
import com.lucy.g3.configtool.rtu.upgrade.ui.FirmwareUpgradePage;
import com.lucy.g3.configtool.rtu.upgrade.ui.SlaveUpgradeWindow;
import com.lucy.g3.configtool.subsys.realtime.ui.logs.LogListPanel;
import com.lucy.g3.configtool.subsys.realtime.ui.modules.ModuleInfoPage;
import com.lucy.g3.configtool.subsys.realtime.ui.overview.RTMonitorRootPage;
import com.lucy.g3.configtool.subsys.realtime.ui.points.PointStatusPage;
import com.lucy.g3.configtool.subsystem.AbstractRealtimeSubsytem;
import com.lucy.g3.configtool.ui.ConfigurationTab;
import com.lucy.g3.configtool.ui.ConfigurationRoot;
import com.lucy.g3.configtool.ui.RealtimeMonitorTab;
import com.lucy.g3.configtool.ui.UtilitiesTab;
import com.lucy.g3.gui.common.dialogs.AboutBox;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.framework.page.AbstractListPage;
import com.lucy.g3.rtu.commissioning.ui.task.CommissioningTask;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.ui.ChannelManagerModel;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ui.CLogicManagerModel;
import com.lucy.g3.rtu.config.clogic.ui.CLogicPage;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.device.comms.ui.CommsDeviceManagerPage;
import com.lucy.g3.rtu.config.device.fields.ui.FieldDeviceManagerPage;
import com.lucy.g3.rtu.config.hmi.ui.HMIScreenPanel;
import com.lucy.g3.rtu.config.hmi.ui.editor.DataScreenDialog;
import com.lucy.g3.rtu.config.module.canmodule.ui.CANModuleManagerModel;
import com.lucy.g3.rtu.config.module.canmodule.ui.panels.HMISettingsPanel;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Constraints;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.SDNP3ChannelManagerPage;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.SDNP3ChannelPage;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.SDNP3IoMapPage;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.SDNP3IoMapPageModel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.SDNP3SessionManagerModel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.connmgr.ConnectionManagerPanel;
import com.lucy.g3.rtu.config.protocol.iec104.slave.ui.S104ChannelManagerPage;
import com.lucy.g3.rtu.config.protocol.iec104.slave.ui.S104SessionManagerModel;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
import com.lucy.g3.rtu.config.protocol.iec870.shared.ui.IEC870IoMapPage;
import com.lucy.g3.rtu.config.protocol.iec870.shared.ui.IEC870IoMapPageModel;
import com.lucy.g3.rtu.config.protocol.master.shared.ui.MasterProtocolManagerModel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBConstraints;
import com.lucy.g3.rtu.config.protocol.modbus.master.ui.MMBIoMapPageModel;
import com.lucy.g3.rtu.config.protocol.modbus.master.ui.MMBPage;
import com.lucy.g3.rtu.config.protocol.modbus.master.ui.MMBSessionManagerModel;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.ScadaProtocolManagerPage;
import com.lucy.g3.rtu.config.shared.base.dialogs.AbstractEditorDialog;
import com.lucy.g3.rtu.config.user.UserManagerModel;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.dialog.selector.VirtualPointSelector;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor.VirtualPointDialog;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.page.VirtualPointPage;
import com.lucy.g3.rtu.config.virtualpointsource.ui.VirtualPointSourceSelector;
import com.lucy.g3.rtu.filetransfer.FileReadingTask;
import com.lucy.g3.rtu.filetransfer.FileWritingTask;
import com.lucy.g3.test.support.utilities.TestUtil;

/**
 * The Class PropertyResourceTest.
 */
public class PropertyResourceTest {

  private Object[][] resources = {
      { AboutBox.class, null },
      { AutoTestWindow.class, null },
      { AbstractListPage.class, null },
      { VirtualPointDialog.class, "AnaloguePointDialog" },
      { VirtualPointDialog.class, "BinaryPointDialog" },
      { VirtualPointDialog.class, "CounterPointDialog" },
      { CANModuleManagerModel.class, null },
      { ICLogicType.class, null },
      { ChannelDiagnostic.class, null },
      { ChannelManagerModel.class, null },
      { VirtualPointSourceSelector.class, null },
      { ChannelType.class, null },
      { CommsDeviceManagerPage.class, "CommsDeviceManagerModel" },
      { ConfigurationTab.class, null },
      { ConfigFileOperationTask.class, null },
      { ConfigFileExportTask.class, null },
      { ConfigFileImportTask.class, null },
      { ConfigFileReadTask.class, null },
      { ConfigFileWriteTask.class, null },
      { ConfigurationRoot.class, null },
      { ConnectionManagerPanel.class, null },
      { ConnectionManager.class, null },
      { ConnectionManager.class, "ConnectionSetupTask" },
      { CLogicManagerModel.class, null },
      { CLogicPage.class, null },
      // { SDNP3PointDialogs.class, null},
      { SDNP3IoMapPageModel.class, null },
      { SDNP3IoMapPage.class, null },
      { DataScreenDialog.class, null },
      { VirtualPointDialog.class, "DoubleBinaryPointDialog" },
      { EventLogManager.class, null },
      { FactoryResetTask.class, null },
      { MMBIoMapPageModel.class, null },
      { FieldDeviceManagerPage.class, "FieldDeviceManagerModel" },
      { FieldDeviceManagerPage.class, null },
      { FileReadingTask.class, null },  
      { FileWritingTask.class, null },
      { FirmwareUpgradePage.class, null },
      { FirmwareUpgradeTask.class, null },
      { FuncTestWindow.class, null },
      { HMIScreenPanel.class, null },
      { HMISettingsPanel.class, null },
      { S104ChannelManagerPage.class, "S104ChannelManagerModel" },
      { IEC870Constraints.class, null },
      { IEC870IoMapPageModel.class, null },
      { S104SessionManagerModel.class, null },
      { IEC870IoMapPage.class, null },
      { LogLevelDialog.class, null },
      { LogListPanel.class, null },
      { MMBPage.class, "MMBPageModel" },
//      { MMBChannelPage.class, null },
      { MMBConstraints.class, null },
      { MMBSessionManagerModel.class, null },
      { ConfigToolApp.class, null },
      { MasterProtocolManagerModel.class, null },
      { ModuleInfoPage.class, null },
      { ModuleInfoPage.class, "ModuleInfoUpdateTask" },
      { ModuleResource.class, null },
      { AbstractEditorDialog.class, null },
      { PointStatusPage.class, "PointSelectTask" },
      { VirtualPointSelector.class, null },
      { PointStatusPage.class, null },
      { PointStatusPage.class, "PointStatusUpdateTask" },
      { PreferenceDialog.class, null },
      { RTMonitorRootPage.class, null },
      { RealtimeMonitorTab.class, null },
      { RTUController.class, null },
      { RTUInfoReadTask.class, null },
      { CommissioningTask.class, null },
      { AbstractRealtimeSubsytem.class, null },
      { RTURestartTask.class, null },
      { ConfigurationManager.class, null },
      { RTUTester.class, null },
      { RegisterModulesTask.class, null },
      { SDNP3ChannelManagerPage.class, "SDNP3ChannelManagerModel" },
      { SDNP3ChannelPage.class, null },
      { SDNP3Constraints.class, null },
      { SDNP3SessionManagerModel.class, null },
      { ScadaProtocolManagerPage.class, "ScadaProtoManagerModel" },
      { SlaveUpgradeTask.class, null },
      { SlaveUpgradeWindow.class, null },
      { AbstractDialog.class, null },
      { SystemAlarmsManager.class, null },
      { SystemLogManager.class, null },
      { SDPUpgrader.class, null },
      { RTUUpgrader.class, null },
      { UserManagerModel.class, null },
      { UtilitiesTab.class, null },
      { VirtualPointDiagnostic.class, null },
      { VirtualPointPage.class, "VirtualPointPageModel" },
      { VirtualPointPage.class, "VirtualPointTab" },
      { VirtualPointType.class, null },
  };


  /*
   * Ignored as it failed when running from Maven for some reason. Recommended
   * to run from Eclipse.
   */
  @Ignore
  @Test
  public void testAllPropertiesResourceFileExists() {
    TestUtil.testAllResourceFilesExist(resources);
  }
}
