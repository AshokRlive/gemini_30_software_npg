/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.EventObject;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.SessionStorage;
import org.jdesktop.application.SessionStorage.TableProperty;
import org.jdesktop.application.SessionStorage.TableState;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.JXLoginPane.Status;
import org.jdesktop.swingx.error.ErrorInfo;
import org.jdesktop.swingx.painter.MattePainter;

import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.common.event.Event;
import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.IEventHandler;
import com.lucy.g3.configtool.app.ExceptionHandler.IReporter;
import com.lucy.g3.configtool.preferences.PreferenceManager;
import com.lucy.g3.configtool.preferences.impl.AdvancedPreference;
import com.lucy.g3.configtool.preferences.impl.ConnectionPreference;
import com.lucy.g3.configtool.preferences.impl.GeneralPreference;
import com.lucy.g3.configtool.preferences.impl.RealtimePreference;
import com.lucy.g3.configtool.preferences.security.SecurityPreference;
import com.lucy.g3.configtool.rtu.config.ConfigurationManager;
import com.lucy.g3.configtool.rtu.connection.ConnectionManager;
import com.lucy.g3.configtool.rtu.connection.ui.LoginWindow;
import com.lucy.g3.configtool.subsys.integrated.SubsystemPlugin;
import com.lucy.g3.configtool.subsys.permissions.IPermissionCheckerCallback;
import com.lucy.g3.configtool.subsys.permissions.PermissionCheckChecks;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.UIThemeResources;
import com.lucy.g3.gui.common.widgets.utils.ResourceUtils;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.framework.workbench.internal.WorkbenchWindow;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.comms.shared.LoginResult;
import com.lucy.g3.rtu.config.generator.impl.ConfigGeneratorPlugin;
import com.lucy.g3.rtu.config.integrated.ConfigPlugin;
import com.lucy.g3.rtu.config.validation.impl.ConfigValidationPlugin;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;

/**
 * Implementation of ConfigTool main application which initialise all subsystems
 * and GUI.
 */
public class ConfigToolApp extends SingleFrameApplication {

  private Logger log = Logger.getLogger(ConfigToolApp.class);

  private WorkbenchWindow window;

  private final ExitListener exitListener = new ExitListener(this);
  private final ExceptionReporter exceptionReporter = new ExceptionReporter();
  private IContext context = new ConfigToolContext();
  private InactivityListener inactivityListener;
  
  @Override
  protected void initialize(String[] args) {
    getContext().addTaskService(new CommsTaskService());

    initSystemProperties();
    initPreferences();
    initExceptionHandler();
    initPermissionChecker();
    initPlugins();
    initEventHandling();
    initInactivityAction();
    configureGlobalUI();
    
    new ConfigToolActions(context);

    log.info("Application initialised");

    // Set initial state to disconnect
    ConnectionManager conMgr = context.getComponent(ConnectionManager.SUBSYSTEM_ID);
    conMgr.disconnect();

    // Patch for fixing bug that table width is not stored in session storage
    fixTableWidthSessionStore(getContext().getSessionStorage());
  }

  private void initPermissionChecker() {
    PermissionCheckerImpl per = new PermissionCheckerImpl(new IPermissionCheckerCallback() {
      @Override 
      public ConnectionState getConnectionState(){
        ConnectionManager cm =  ConfigToolApp.this.context.getComponent(ConnectionManager.SUBSYSTEM_ID);
        return cm.getConnectionState();
      }
      @Override
      public LoginResult getLoginResult() {
        ConnectionManager cm =context.getComponent(ConnectionManager.SUBSYSTEM_ID);
        return cm.getLoginInfo();
      }
    });

    /* Register Permission Checker */
    PermissionCheckChecks.getInstance().registerChecker(per);

    /* Register Permission Reporter */
    PermissionCheckChecks.getInstance().registerReporter(per);
  }

  /* Configure global UI settings */
  private void configureGlobalUI() {

    try {
      /* Set optionPane buttons right aligned */
      UIManager.getDefaults().put("OptionPane.buttonOrientation", SwingConstants.RIGHT);

      /* Set title border font to bold */
      UIManager.getDefaults().put("TitledBorder.font",
          UIManager.getDefaults().getFont("TitledBorder.font").deriveFont(Font.BOLD));

      /* Set tooltip font colour */
      UIManager.put("ToolTip.background", new ColorUIResource(250, 250, 250));

      /* Set the background of TaskPane container */
      UIManager.put("TaskPaneContainer.backgroundPainter", new MattePainter(UIThemeResources.COLOUR_LOGIC_BLOCK_BG_COLOR));

      /* Set tooltip box to rounded border */
      // Border border = new RoundedBorder();
      // UIManager.put("ToolTip.border", border);

      /* Set tooltip delay */
      // ToolTipManager.sharedInstance().setInitialDelay(500);

    } catch (Exception e) {
      this.log.error("Failt to set UI Defaults: " + e.getMessage());
    }
  }

  private void initEventHandling() {
    // Handle RTU config change
    ConfigurationManager configMgr =
        context.getComponent(ConfigurationManager.SUBSYSTEM_ID);
    configMgr.addPropertyChangeListener(ConfigurationManager.PROPERTY_RTU_CONFIG,
        new RTUConfigLoadingHandler());
    
    // Observe config file path change and update frame title
    EventBroker.getInstance().subscribe(ConfigurationManager.EVENT_TOPIC_CONFIG_PATH_CHANGED, new IEventHandler() {
      @Override
      public void handleEvent(Event event) {
        String filePath = (String) event.getEventData();
        updateFrameTitle(filePath);
      }
    });
  }

  private void initInactivityAction() {
    ConnectionPreference pref = getPrefs().getPreference(ConnectionPreference.ID);
    setInactivityPeriod(pref.getInactivityMins());
    pref.addPropertyChangeListener(ConnectionPreference.PROPERTY_INACTIVITY_MINS, 
        new PropertyChangeListener() {
      
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        setInactivityPeriod((int) evt.getNewValue());
      }
    });
  }
  
  private void setInactivityPeriod(int inactivityMins) {
    if(inactivityListener == null) {
      ConnectionManager connManager = context.getComponent(ConnectionManager.SUBSYSTEM_ID);
      inactivityListener = new InactivityListener(getMainFrame(), new InactivityAction(connManager));
    }
    
    inactivityListener.stop();
    
    if(inactivityMins > 0) {
      inactivityListener.setInterval(inactivityMins);
      inactivityListener.start(true);
      log.info("Enabled inactivity logout with period: " + inactivityMins + " mins");
    } else { 
      log.info("Disabled inactivity logout");
    }
  }

  private void initPlugins() {
    SubsystemPlugin.init(context);
    ConfigGeneratorPlugin.init();
    ConfigValidationPlugin.init();
    ConfigPlugin.init();
  }

  private void initExceptionHandler() {
    /* Register global exception handler */
    ExceptionHandler.registerExceptionHandler(exceptionReporter);
    GeneralPreference general = getPrefs().getPreference(GeneralPreference.ID);
    PropertyConnector.connect(general, GeneralPreference.PROPERTY_ERRORREPORT_ENABLED,
        exceptionReporter, IReporter.PROPERTY_ERROR_REPORT_ENABLED).updateProperty2();
  }

  private void initPreferences() {
    getContext().getSessionStorage().setSessionStorageRelativePath("gui-sessions");
    String appDir = getContext().getLocalStorage().getDirectory().getAbsolutePath();
    String[] prefsIDs = {
        GeneralPreference.ID,
        ConnectionPreference.ID,
        RealtimePreference.ID,
        SecurityPreference.ID,
        AdvancedPreference.ID,
    };

    Class<?>[] prefClasses = new Class<?>[] {
        GeneralPreference.class,
        ConnectionPreference.class,
        RealtimePreference.class,
        SecurityPreference.class,
        AdvancedPreference.class,
    };

    PreferenceManager pref = getPrefs();
    
    // Load preferences
    pref.init(new File(appDir + "/prefs/"), prefsIDs, prefClasses);
    
    // Apply loaded preferences to RTU
    new PreferencesProcessor().initialise(pref, getRTU());
  }

  private G3RTU getRTU() {
    return G3RTUFactory.getDefault();
  }

  @Override
  protected void startup() {
    // Create main frame
    JFrame mainFrame = getMainFrame();
    mainFrame.setPreferredSize(new Dimension(950, 700));
    mainFrame.setName("ConfigToolMainFrame");

    // Main frame initialisation
    log.info("Initialsing GUI");
    window = new WorkbenchWindow(new WorkbenchAdvisor(context));

    // Populate window components
    FrameView mainView = getMainView();
    mainView.setMenuBar(window.getMenuBar());
    mainView.setComponent(window.getContentComponent());
    mainView.setStatusBar(window.getStatusBar().getComponent());
    updateFrameTitle(null);
    
    // Register F1 help
    Helper.register(mainFrame.getRootPane());

    // Set frame icon
    ImageIcon icon = ResourceUtils.getAppIcon();
    if (icon != null) {
      mainFrame.setIconImage(icon.getImage());
    }

    // Handle the exit action
    addExitListener(exitListener);

    // Show login window
    startLogin();
  }

  private void startLogin() {
    log.info("Show Login Window");
    LoginWindow login = new LoginWindow();
    Status loginResult = login.showDialog(true);

    if (loginResult == Status.SUCCEEDED) {
      /* Login success */
      show(getMainView());
      
      // Setup connection
      ConnectionManager connMgr = context.getComponent(ConnectionManager.SUBSYSTEM_ID);
      connMgr.registerConnectionHandler(new ConnectionSetupHandler(window));
      connMgr.setupConnection(login.getLoginAccount(), true);

    } else if (loginResult == null) {
      /* Offline mode */
      show(getMainView());

    } else {
      /* Login cancelled, exit application */
      removeExitListener(exitListener);
      /*
       * Set main frame name to null to prevent saving its session cause it is
       * not showed yet and we don't want its session to be saved.
       */
      getMainFrame().setName(null);
      exit();
    }
  }

  private void updateFrameTitle(String filePath) {
    String title = getContext().getResourceMap().getString("Application.title");
    String version = ConfigToolVersion.getInstance().getManifest().getVersion();
    if(!Strings.isBlank(version)) {
      title = String.format("%s V%s",title,version);
    }
    
    if(!Strings.isBlank(filePath)) {
      title = String.format("%s - %s",title,filePath);
    }
    
    getMainFrame().setTitle(title);
  }
  
  @Override
  protected void shutdown() {
    log.info("Application is shutting down");

    // Shutdown tasks service
    getContext().getTaskService().shutdownNow();
    getContext().getTaskService(IContext.COMMS_TASK_SERVICE_NAME).shutdown();

    // Save user settings
    getPrefs().saveAll();

    if (window != null) {
      window.saveSession();
    }

    // Super shutdown must be called at the end to avoid shutdown too early.
    super.shutdown();
  }

  private PreferenceManager getPrefs() {
    return PreferenceManager.INSTANCE;
  }


  private static class ExitListener implements Application.ExitListener {

    private final SingleFrameApplication app;


    ExitListener(SingleFrameApplication app) {
      super();
      this.app = app;
    }

    @Override
    public boolean canExit(EventObject e) {
      String confirmExitText = app.getContext().getResourceMap().getString("confirmTextExit");
      JFrame parent = app.getMainFrame();

      /*
       * If the parent frame is iconified, dialog will be showed not in centre
       * screen, so in this case we don't use parent frame.
       */
      if (parent != null && parent.getState() == Frame.ICONIFIED) {
        parent = null;
      }

      int option = JOptionPane.showConfirmDialog(parent,
          confirmExitText, "Exit", JOptionPane.YES_NO_CANCEL_OPTION);
      return option == JOptionPane.YES_OPTION;
    }

    @Override
    public void willExit(EventObject e) {
    }
  }

  public static class ExceptionReporter implements IReporter {

    private boolean errorReportEnabled;


    @Override
    public boolean isErrorReportEnabled() {
      return errorReportEnabled;
    }

    @Override
    public void setErrorReportEnabled(boolean errorReportEnabled) {
      this.errorReportEnabled = errorReportEnabled;
    }

    /**
     * Checks if a throwable should be skipped and not handled by
     * ExceptionHandler.
     */
    private boolean shouldSkip(Throwable throwable) {
      boolean skip = false;

      // This throwable is caused by a bug from third party library that we
      // cannot
      // fix, so just skip to handle it.
      if (throwable != null && throwable.getClass() == java.lang.IllegalStateException.class
          && "cannot open system clipboard".equals(throwable.getMessage())) {
        skip = true;
      }

      return skip;
    }

    @Override
    public void report(Throwable throwable) {
      // Skip throwable that we don't want to handle.
      if (shouldSkip(throwable)) {
        return;
      }

      try {
        Logger.getLogger(ExceptionHandler.class).error("Unexpected exception caught", throwable);

        if (errorReportEnabled) {
          ErrorInfo error = new ErrorInfo("Error",
              "Unexpected exception caught: " + throwable.getClass().getName(),
              null, null, throwable, java.util.logging.Level.SEVERE, null);
          JXErrorPane.showDialog(WindowUtils.getMainFrame(), error);
        }

      } catch (Throwable t) {
        // Nothing to throw
      }
    }
  }


  /**
   * <p>
   * The TableProperty is extended to fix the bug below:
   * </p>
   * <p>
   * Table column width cannot be restored properly if this table is not
   * visualised because session storage only restore the "preferred width" of
   * table columns, but not the "width".
   * </p>
   * <b>Solution:</b> Replace original TableProperty with a overrode one which
   * update the column width with the values in sessionStorage.
   */
  private static void fixTableWidthSessionStore(SessionStorage sessionstore) {
    sessionstore.putProperty(JTable.class, new TableProperty() {

      @Override
      public void setSessionState(Component c, Object state) {
        super.setSessionState(c, state);
        JTable table = (JTable) c;
        int[] columnWidths = ((TableState) state).getColumnWidths();
        if (table.getColumnCount() == columnWidths.length) {
          for (int i = 0; i < columnWidths.length; i++) {
            if (columnWidths[i] != -1) {
              TableColumn tc = table.getColumnModel().getColumn(i);
              if (tc.getResizable()) {
                // It is key to set width with stored values,
                // while originally only the preferred width was set
                tc.setWidth(columnWidths[i]);
              }
            }
          }
        }
      }
    });
  }
  
  /**
   * Initialises system properties by loading customer properties files.
   */
  private static void initSystemProperties() {
    String propFile = System.getProperty("profilePropertiesFile");
    InputStream is;
    
    // Get properties file stream
    if(propFile != null) {
      // From a external profile file
      try {
        is = new FileInputStream(new File(propFile));
      } catch (FileNotFoundException e) {
        Logger.getLogger(ConfigToolApp.class).error("profilePropertiesFile read error",e);
        is = null;
      }
      
    } else {
      // From built-in profile file
      is = Main.class.getClassLoader().getResourceAsStream("profile.properties");
    }
    
    // Load properties from file
    if(is != null) {
      Properties props  = new Properties();
      try {
        props.load(is);
        
        Enumeration<Object> keys = props.keys();
        String key;
        String value;
        while(keys.hasMoreElements()) {
           key = (String) keys.nextElement();
           value = props.getProperty(key);
          System.setProperty(key, value);
        }
        
      } catch (IOException e) {
        e.printStackTrace();
      } finally {
        if(is != null) {
        try {
          is.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
        }
      }
    }
  }
}
