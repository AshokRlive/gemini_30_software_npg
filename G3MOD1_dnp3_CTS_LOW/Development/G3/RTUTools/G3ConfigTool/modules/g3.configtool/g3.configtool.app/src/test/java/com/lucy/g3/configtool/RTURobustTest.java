/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.rtu.comms.protocol.G3Protocol.LocalRemoteCode;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SWITCH_OPERATION;


/**
 *
 */
@Ignore
public class RTURobustTest {
  final static String HOST = "10.11.11.200";
  static G3RTU rtu;
  
  @BeforeClass
  static public void init() throws Exception{
    rtu = new G3RTU(HOST);
  }
  
  @Test
  public void testOperate() throws Exception {
    int err=0;
    int index = 0;
    SWITCH_OPERATION op = SWITCH_OPERATION.SWITCH_OPERATION_CLOSE;
    
    while(true) {
      try {
        if(op == SWITCH_OPERATION.SWITCH_OPERATION_CLOSE)
          op = SWITCH_OPERATION.SWITCH_OPERATION_OPEN;
        else
          op = SWITCH_OPERATION.SWITCH_OPERATION_CLOSE;
        
        rtu.getComms().cmdOperateSwitch((short)7, op, LocalRemoteCode.LOCAL);
        System.out.println("Success:"+index+++". error:"+err);
      } catch (IOException | SerializationException e) {
        System.err.println("Error:"+e.getMessage() +" count:"+err++);
      }
    }
  }
  
  /*
  @Test
  public void testDebugServer() {
    try {
      rtu.getComms().cmdOperateSwitch((short)7, SWITCH_OPERATION.SWITCH_OPERATION_CLOSE, LocalRemoteCode.LOCAL);
    } catch (IOException | SerializationException e1) {
      System.out.println("Failed to start automation:"+e1.getMessage());
    }
    
    int errcnt = 0;
    int successcnt = 0;
    IEC61131DebuggerComms comms = new IEC61131DebuggerComms();
    try {
      comms.setHost(HOST);
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    while(true) {
      try {
        comms.connect();
      comms.disconnect();
      Thread.sleep(500);
      System.out.println("Test Success:"+(successcnt++));
      } catch (Exception e) {
        System.out.println("Test Error:"+e.getMessage() +". count:"+(errcnt++));
      }
    }
  }
  */
}

