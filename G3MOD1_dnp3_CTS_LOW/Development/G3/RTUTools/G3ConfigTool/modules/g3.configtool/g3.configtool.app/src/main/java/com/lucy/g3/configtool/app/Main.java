/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import static com.lucy.g3.configtool.app.ConfigToolArgParser.OPTION_CERT;
import static com.lucy.g3.configtool.app.ConfigToolArgParser.OPTION_HELP;
import static com.lucy.g3.configtool.app.ConfigToolArgParser.OPTION_LOG_LEVEL;
import static com.lucy.g3.configtool.app.ConfigToolArgParser.OPTION_UPDATE;
import static com.lucy.g3.configtool.app.ConfigToolArgParser.OPTION_VERSION;

import org.apache.commons.cli.ParseException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jdesktop.application.Application;

import com.lucy.g3.rtu.cert.manager.RTUCertCommandLine;
import com.lucy.g3.rtu.updater.Updater;

public class Main {

  private Main() {
  }

  // Main
  public static void main(final String[] args) {
    // Parse arguments
    ConfigToolArgParser options = new ConfigToolArgParser();
    try {
      options.parse(args);
    } catch (ParseException e) {
      System.out.println(e.getMessage());
      exit(-1);
    }

    handleLogLevel(options);
    handleHelp(args, options);
    handleUpdate(args, options);
    handleCert(args, options);

    /* Launch GUI in EDT thread */
    Application.launch(ConfigToolApp.class, args);
  }

  /**
   * Handles certificates related arguments.
   */
  private static void handleCert(String[] args, ConfigToolArgParser options) {
    if (options.hasOption(OPTION_CERT)) {
      int ret = RTUCertCommandLine.run(args);
      exit(ret);
    }
  }

  /* Set log level from args. */
  private static void handleLogLevel(ConfigToolArgParser options) {
    Level level = Level.toLevel(options.getOptionValue(OPTION_LOG_LEVEL), null);
    if (level != null) {
      Logger.getRootLogger().setLevel(level);
    }
  }

  private static void handleUpdate(final String[] args, ConfigToolArgParser options) {
    /* Launching updater in the main thread */
    if (options.hasOption(OPTION_UPDATE)) {
      Updater app = new Updater();
      app.initialize(args);
      app.run();
      System.exit(app.getErrCode());
    }
  }

  private static void handleHelp(final String[] args, ConfigToolArgParser options) {
    if (options.hasOption(OPTION_HELP)) {
      /* Print help */
      String helpArg = options.getOptionValue(OPTION_HELP);

      if (ConfigToolArgParser.OPTION_UPDATE.equals(helpArg)) {
        Updater.printHelp();

      } else if (ConfigToolArgParser.OPTION_CERT.equals(helpArg)) {
        RTUCertCommandLine.printHelp();

      } else {
        options.printHelp();
      }

      exit(0);

    } else if (options.hasOption(OPTION_VERSION)) {
      /* Print version */
      System.out.println(ConfigToolVersion.getInstance().getManifest().getFormattedText());
      exit(0);
    }

  }

  private static void exit(int returnCode) {
    System.exit(returnCode);
  }
}
