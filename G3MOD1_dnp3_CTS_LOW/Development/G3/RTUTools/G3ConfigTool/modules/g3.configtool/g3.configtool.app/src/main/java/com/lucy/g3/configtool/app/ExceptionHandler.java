/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;


/**
 * Exception handler for showing uncaught exceptions.
 */
public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

  private IReporter reporter;
  
  public ExceptionHandler(IReporter reporter){
   this.reporter = reporter; 
  }

  @Override
  public void uncaughtException(Thread t, Throwable e) {
    if(reporter != null ){
      reporter.report(e);
    }
  }

  public static ExceptionHandler registerExceptionHandler(IReporter reporter) {
    if (handler == null) {
      handler = new ExceptionHandler(reporter);
      Thread.setDefaultUncaughtExceptionHandler(handler);
      System.setProperty("sun.awt.exception.handler",
          ExceptionHandler.class.getName());
    }
    return handler;
  }


  private static ExceptionHandler handler;
  
  
  public static interface IReporter {
    public static final String PROPERTY_ERROR_REPORT_ENABLED = "errorReportEnabled";
    void report(Throwable throwable);
    boolean isErrorReportEnabled();
    void setErrorReportEnabled(boolean errorReportEnabled); 
  }
  
}
