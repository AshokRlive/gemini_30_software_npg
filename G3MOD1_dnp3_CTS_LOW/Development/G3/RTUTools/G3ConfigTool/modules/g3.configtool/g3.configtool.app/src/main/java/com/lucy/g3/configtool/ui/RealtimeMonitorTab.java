/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import com.lucy.g3.common.context.IContext;
import com.lucy.g3.common.event.Event;
import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.common.event.IEventHandler;
import com.lucy.g3.configtool.rtu.connection.ConnectionManager;
import com.lucy.g3.configtool.subsys.realtime.ui.logs.LogListPanel;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.framework.navigation.IExplorerTreeNode;
import com.lucy.g3.gui.framework.page.IPageViewer;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.UpdatablePage;
import com.lucy.g3.gui.framework.workbench.WorkbenchWindowTab;
import com.lucy.g3.gui.framework.workbench.widgets.SplitViewerContainer;

/**
 * <p>
 * This implementation of <code>TabContent</code> contains in a tree, from which
 * we can select what real time information we want to monitor.
 * </p>
 * It supports showing {@linkplain UpdatablePage} and manages which
 * <code>UpdatablePage</code> should be triggered to do updating job.
 */
public class RealtimeMonitorTab extends WorkbenchWindowTab {
  public final static String ID = "RTMonitorTabContent";

  private final IPageViewer viewer;

  // ====== Tree ======
  private RealtimeMonitorTree explorerTree;

  // The view that can run updating task
  private UpdatablePage updatingPage;


  private ConnectionState state;
  
  private final ArrayList<IConnectionStateHandler> stateHandlers = new ArrayList<IConnectionStateHandler>();


  public RealtimeMonitorTab(IContext context) {
    super(ID);
    ConnectionManager conMgr = context.getComponent(ConnectionManager.SUBSYSTEM_ID);

    LogListPanel logPanel = new LogListPanel(context, conMgr.getConnectionState());

    viewer = new SplitViewerContainer(super.getPageViewer(), logPanel);

    /* Create explorer tree */
    explorerTree = new RealtimeMonitorTree(context,viewer);
    explorerTree.setContextMenu(createContextMenu(conMgr));
    explorerTree.addTreeSelectionListener(new TreeNodeSelectionHandler());

    JScrollPane pane = new JScrollPane();

    stateHandlers.add(explorerTree);
    pane.setViewportView(explorerTree);

    pane.setBorder(BorderFactory.createMatteBorder(8, 15, 8, 0, Color.WHITE));
    pane.setViewportBorder(BorderFactory.createEmptyBorder());
    
    getComponent().add(pane, BorderLayout.CENTER);
    
    initEventHandling();
  }

  private void initEventHandling() {
    // Observe connection state
    EventBroker.getInstance().subscribe(EventTopics.EVT_TOPIC_CONN_STATE_CHG, 
        new IEventHandler() {
      @Override
      public void handleEvent(Event event) {
        ConnectionState oldState = (ConnectionState) event.getProperty(EventTopics.EVT_PROPERTY_OLD_STATE);
        ConnectionState newState = (ConnectionState) event.getProperty(EventTopics.EVT_PROPERTY_NEW_STATE);
        setState(oldState, newState);    
      }
    });    
  }

  private JPopupMenu createContextMenu(ConnectionManager conMgr) {
    JPopupMenu popupMenu = new JPopupMenu("Command");
    popupMenu.add(conMgr.getAction(ConnectionManager.ACTION_KEY_CONNECT));
    popupMenu.add(conMgr.getAction(ConnectionManager.ACTION_KEY_DISCONNECT));
    popupMenu.addSeparator();

    return popupMenu;
  }

  private void setState(ConnectionState oldState, ConnectionState newState) {
    state = newState;

    for (IConnectionStateHandler handler : stateHandlers) {
      handler.handleStateChange(oldState, newState);
    }

    triggerUpdating(isTabSelected());
  }

  @Override
  public void tabSelectedActionPerform(boolean isSelected) {
    triggerUpdating(isSelected);
  }


  @Override
  public IPageViewer getPageViewer() {
    return viewer;
  }


  @Override
  public void saveSession() {
    // Must remove view from container before saving its state.
    viewer.clear();

    explorerTree.saveSession();
  }


  private class TreeNodeSelectionHandler implements TreeSelectionListener {

    @Override
    public void valueChanged(TreeSelectionEvent e) {
      Object selectNode = explorerTree.getLastSelectedPathComponent();

      UpdatablePage updatepage = null;
      if (selectNode != null && selectNode instanceof IExplorerTreeNode) {
        Page page = ((IExplorerTreeNode) selectNode).getPage();
        if (page instanceof UpdatablePage) {
          updatepage = (UpdatablePage) page;
        }
      }

      triggerUpdating(updatepage);
    }
  }


  /**
   * Schedule a job to update real-time page.
   */
  private void triggerUpdating(UpdatablePage page) {
    if (updatingPage != null) {
      /* Stop previous page updating*/
      updatingPage.stopUpdating();
      updatingPage = null;
    }
    updatingPage = page;

    triggerUpdating(isTabSelected());
  }

  /**
   * <p>
   * Start or stop updating the real-time view.
   * </p>
   * The condition of running updating task include:
   * <li>1.Application is not on disconnected state;
   * <li>2.This tab is selected;
   * <li>3.A updatable view is displayed on screen. So if any of these three
   * conditions has changed, this method must be called to handle changed
   * conditions and decide to start or stop updating view.
   */
  private void triggerUpdating(boolean tabSelected) {
    if (updatingPage != null) {
      if (tabSelected && (ConnectionState.isConnected(state))) {
        updatingPage.startUpdating();
      } else {
        updatingPage.stopUpdating();
      }
    }
  }

}
