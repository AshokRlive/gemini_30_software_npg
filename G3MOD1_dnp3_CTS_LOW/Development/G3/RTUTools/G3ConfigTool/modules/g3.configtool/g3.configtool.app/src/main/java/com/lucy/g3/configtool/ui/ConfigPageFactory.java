/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.ui;

import org.apache.log4j.Logger;

import com.lucy.g3.gui.framework.page.BlankPage;
import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;

/**
 * A factory for creating page objects for RTU configuration.
 */
public class ConfigPageFactory implements IPageFactory {

  private Logger log = Logger.getLogger(ConfigPageFactory.class);

  @Override
  public Page createPage(Object data) {
    if (data == null) {
      log.error("No page can be created for object: " + data
          + ".No valid config data! ");
      return null;
    }
    
    Page page;
    
    page = PageFactories.createPage(data);

    if(page == null) {
      log.error("No page can be created for object: " + data);
      page = new BlankPage(data.getClass().getSimpleName(), "Not Implemented");
    }

    return page;
  }


}
