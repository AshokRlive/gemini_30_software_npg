/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.configtool.app.ConfigToolArgParser;


public class ConfigToolArgParserTest {
  private ConfigToolArgParser fixture;
  
  @Before
  public void init(){
    fixture = new ConfigToolArgParser();
  }
  
  @Test
  public void testPrintHelp() {
    fixture.printHelp();
  }

  @Test
  public void testParseHelp() throws ParseException {
    String[][] valid = {
        {"-help", "update"},
        {"-help","?"},
        {"-help"},
        {"-h","update"},
        {"-update","-h"},
        {"-h","?"},
        {"-h"}
    };
    for (int i = 0; i < valid.length; i++) {
      fixture.parse(valid[i]);
      assertTrue("Failed to parse help args: "+Arrays.toString(valid[i]),
          fixture.hasOption(ConfigToolArgParser.OPTION_HELP));
    }
    
    
    String[][] invalid = {
        {"help", "update"},
        {"?", "help",},
        {"update", "-help"},
    };
    for (int i = 0; i < invalid.length; i++) {
      fixture.parse(invalid[i]);
      assertFalse("Failed to parse invalid help args: "+Arrays.toString(invalid[i]),
          fixture.hasOption(ConfigToolArgParser.OPTION_HELP));
    }
  }
  
  @Test
  public void testParseUpdate() throws ParseException {
    CommandLine cl = fixture.parse(new String[]{
        "-update","-sdp","sdppath","-sdpboot","sdpbootPath",
        "..."
        });
    
    assertTrue(cl.hasOption(ConfigToolArgParser.OPTION_UPDATE));
  }
  
  @Test
  public void testParseHelpUpdate() throws ParseException{
    fixture.parse(new String[]{
        "-help","update",
        });
    
    assertEquals(ConfigToolArgParser.OPTION_UPDATE, fixture.getOptionValue(ConfigToolArgParser.OPTION_HELP));
  }
  
  
}

