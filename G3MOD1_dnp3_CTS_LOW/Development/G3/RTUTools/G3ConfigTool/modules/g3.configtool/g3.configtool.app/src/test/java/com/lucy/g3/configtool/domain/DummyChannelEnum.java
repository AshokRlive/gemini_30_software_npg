/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.domain;

import java.util.HashMap;

import com.lucy.g3.xml.gen.api.IChannelEnum;

// Dummy ENUM for testing
/**
 * The Enum DummyChannelEnum.
 */
public enum DummyChannelEnum implements IChannelEnum {
  CHANNEL_CT(0x00, "A CT", ""),
  CHANNEL_BATT_VOLTAGE(0x01, "Battery Voltage", ""),
  CHANNEL_TEMP(0x02, "Temperature", ""),
  CHANNEL_A1(0x03, "AI 1", ""),
  CHANNEL_A2(0x04, "AI 2", ""),
  CHANNEL_A3(0x05, "AI 3", ""),
  CHANNEL_A4(0x06, "AI 4", ""),
  CHANNEL_A5(0x07, "AI 5", ""),
  CHANNEL_A6(0x08, "AI 6", ""),
  CHANNEL_A7(0x09, "AI 7", ""),
  CHANNEL_A8(0x0a, "AI 8", "");

  private static final HashMap<Integer, DummyChannelEnum> TYPES_BY_VALUE = new HashMap<Integer, DummyChannelEnum>();

  private final int value;
  private final String description;
  private final String group;


  DummyChannelEnum(int value, String description, String group) {
    this.value = value;
    this.description = description;
    this.group = group;
  }

  @Override
  public final int getID() {
    return value;
  }

  @Override
  public String getName() {
    return name();
  }

  @Override
  public final String getDescription() {
    if (this.description != null) {
      return this.description;
    } else {
      return super.toString();
    }
  }

  @Override
  public final String getGroup() {
    return group;
  }

  @Override
  public String toString() {
    if (this.description != null) {
      return description;
    } else {
      return super.toString();
    }
  }

  public String getUnitForScaleFactor(Double scaleFactor) {
    return null;
  }

  public static DummyChannelEnum forValue(int value) {
    return TYPES_BY_VALUE.get(value);
  }


  static {
    for (DummyChannelEnum type : DummyChannelEnum.values()) {
      TYPES_BY_VALUE.put(type.value, type);
    }
  }


  @Override
  public HashMap<Double, String> getAllScaleFactorMap() {
    return null;
  }

  @Override
  public int getDefaultEventRate() {
    return 0;
  }

  @Override
  public Double getDefaultScalingFactor() {
    return null;
  }

  @Override
  public int getValue() {
    return 0;
  }

  @Override
  public DefaultCreateOption getDefCreateOption() {
    return null;
  }

  @Override
  public String getDefConf(String parameterKey) {
    return null;
  }

}
