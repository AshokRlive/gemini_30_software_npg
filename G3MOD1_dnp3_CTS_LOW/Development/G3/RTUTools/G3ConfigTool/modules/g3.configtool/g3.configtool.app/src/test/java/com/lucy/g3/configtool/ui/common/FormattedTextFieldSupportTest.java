/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.ui.common;

import static org.junit.Assert.assertEquals;

import java.beans.PropertyVetoException;

import javax.swing.JFormattedTextField;
import javax.swing.JFrame;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.fest.swing.annotation.GUITest;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.testing.FestSwingTestCaseTemplate;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.BeanAdapter;
import com.jgoodies.binding.beans.Model;
import com.lucy.g3.rtu.config.shared.base.logger.LoggingUtil;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.rtu.config.general.GeneralConfig;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModulePSMSettings;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;

/**
 * The Class FTFCommitOnTypeTest.
 */
@GUITest
@Ignore
// Skipped GUI test cause it failes sometimes in linux
public class FormattedTextFieldSupportTest extends FestSwingTestCaseTemplate {

  private static Logger log = Logger.getLogger("FTFCommintOnTypeTest");

  static {
    log.setLevel(Level.DEBUG);
  }

  private FrameFixture window;


  @BeforeClass
  public static void setUpOnce() {
    // FailOnThreadViolationRepaintManager.install();
  }

  @Before
  public void setup() {
    setUpRobot();
  }

  private <T> FTFTestFrame<T> setupFrame(final T bean, final String property, final Object initialValue) {
    FTFTestFrame<T> frame = GuiActionRunner.execute(
        new GuiQuery<FTFTestFrame<T>>() {

          @Override
          protected FTFTestFrame<T> executeInEDT() {
            return new FTFTestFrame<T>(bean, property, initialValue);
          }
        });

    // IMPORTANT: note the call to 'robot()'
    window = new FrameFixture(robot(), frame);
    window.show(); // shows the frame to test
    return frame;
  }

  @After
  public void teardown() {
    window.cleanUp();
  }

  @Test
  public void testSetGeneralConfigProperty() {
    String property = GeneralConfig.PROPERTY_MODIFY_DATE;
    final FTFTestFrame<GeneralConfig> frame = setupFrame(new GeneralConfig(null), property, "1.2");

    window.textBox().requireText("1.2");
    window.textBox().deleteText();
    window.textBox().enterText("3.1");
    assertEquals("3.1", frame.beanModel.getValue(property));
  }

  @Test
  public void testSetDNP3SessionProperty() throws PropertyVetoException, InterruptedException {
    String property = SDNP3Session.PROPERTY_LINKSTATUS_PERIOD;
    SDNP3Session session = new SDNP3Channel(new SDNP3()).addSession("");
    final FTFTestFrame<SDNP3Session> frame = setupFrame(session, property, 123);

    window.textBox().requireText("123");
    window.textBox().deleteText();
    window.textBox().enterText("124");
    assertEquals(124L, frame.beanModel.getValue(property));
  }

  @Test
  public void testSetPSMProperty() throws PropertyVetoException {
    String property = ModulePSMSettings.PROPERTY_FAN_TEMP_THRESHOLD;
    final FTFTestFrame<ModulePSMSettings> frame = setupFrame(new ModulePSMSettings(), property, 123);

    window.textBox().requireText("123");
    window.textBox().deleteText();
    window.textBox().enterText("124");
    assertEquals(124, frame.beanModel.getValue(property));
  }

  @Test
  public void testSetTestBeanProperty() {
    String property = TestBean.PROPERTY_LENGHT;
    final FTFTestFrame<TestBean> frame = setupFrame(new TestBean(), property, "1.2");

    window.textBox().requireText("1.2");
    window.textBox().deleteText();
    window.textBox().enterText("3.1");
    assertEquals("3.1", frame.beanModel.getValue(property));
  }

  @Test
  public void testSetTestBeanProperty2() {
    String property = TestBean.PROPERTY_WIDTH;
    final FTFTestFrame<TestBean> frame = setupFrame(new TestBean(), property, 100);

    window.textBox().requireText("100");
    window.textBox().deleteText();
    window.textBox().enterText("321");
    assertEquals(321, frame.beanModel.getValue(property));
  }


  /**
   * The Class FTFTestFrame.
   *
   * @param <T>
   *          the generic type
   */
  public static final class FTFTestFrame<T> extends JFrame {

    private final T bean;
    private final String property;
    private final BeanAdapter<T> beanModel;


    FTFTestFrame(T bean, String property, Object initialValue) {
      super();
      this.bean = bean;
      beanModel = new BeanAdapter<T>(bean);
      this.property = property;
      LoggingUtil.logPropertyChanges(bean, log);
      buildContent();

      beanModel.setValue(property, initialValue);
    }

    private void buildContent() {
      PresentationModel<T> model = new PresentationModel<T>(bean);
      JFormattedTextField ftf = new JFormattedTextField();
      Bindings.bind(ftf, model.getModel(property));
      FormattedTextFieldSupport.installCommitOnType(ftf);
      setContentPane(ftf);
    }
  }

  /**
   * The Class TestBean.
   */
  public static class TestBean extends Model {

    public static final String PROPERTY_LENGHT = "length";
    public static final String PROPERTY_WIDTH = "width";

    private String length;
    private int width;


    public String getLength() {
      return length;
    }

    public void setLength(String length) {
      Object oldValue = getLength();
      this.length = length;
      firePropertyChange(PROPERTY_LENGHT, oldValue, length);
    }

    public int getWidth() {
      return width;
    }

    public void setWidth(int width) {
      Object oldValue = getWidth();
      this.width = width;
      firePropertyChange(PROPERTY_WIDTH, oldValue, width);
    }

    @Override
    public String toString() {
      return "TestBean";
    }
  }


  // Manual Test
  public static void main(String[] args) {
    final SDNP3 bean = new SDNP3();
    LoggingUtil.logPropertyChanges(bean, log);
    final String property = "rxFrameSize";
    FTFTestFrame<SDNP3> frame = new FTFTestFrame<SDNP3>(bean, property, 100);
    frame.pack();
    frame.setVisible(true);
  }
}
