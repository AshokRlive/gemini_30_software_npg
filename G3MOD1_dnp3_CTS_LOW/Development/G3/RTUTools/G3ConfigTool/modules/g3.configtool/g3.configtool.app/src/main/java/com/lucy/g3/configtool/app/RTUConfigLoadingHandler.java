/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.lucy.g3.configtool.subsys.realtime.factory.VirtualPointDataFactory;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.realtime.points.ProtocolPointData;
import com.lucy.g3.rtu.comms.service.realtime.points.RTUPoints;
import com.lucy.g3.rtu.comms.service.realtime.points.VirtualPointData;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointUtility;
import com.lucy.g3.rtu.manager.G3RTUFactory;

/**
 * Handling the event that RTU configuration is loaded, and update the real-time
 * data model, such as monitored real-time points.
 */
class RTUConfigLoadingHandler implements PropertyChangeListener {


  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    Object newvalue = evt.getNewValue();

    if (newvalue == null) {
      resetRTPointsList();
    } else if (newvalue instanceof IConfig) {
      updateRTPointList((IConfig) newvalue);
    }
  }

  /**
   * Update real-time point list.
   */
  private void updateRTPointList(IConfig rtuCfgData) {
    /* Update virtual point data list */
    Collection<VirtualPoint> vpointList = VirtualPointUtility.getAllPoints(rtuCfgData);
    Collection<VirtualPointData> vpointDataList = new ArrayList<VirtualPointData>();

    if (vpointList != null) {
      for (VirtualPoint vpoint : vpointList) {
        if (vpoint != null) {
          List<VirtualPointData> vpdataList = VirtualPointDataFactory.createPointData(vpoint);
          for (VirtualPointData vpdata : vpdataList) {
              vpointDataList.add(vpdata);
          }
        }
      }
    }
    
    // update selected points
    CommsUtil.getRTUPointsAPI(G3RTUFactory.getComms()).updatePointDataList(vpointDataList);

    /* Update protocol point data list */
    ScadaProtocolManager manager = rtuCfgData.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID);
    Collection<ScadaPoint> protoPointList =  manager.getAllInputPoints();
    Collection<ProtocolPointData> protoPointDataList = new ArrayList<ProtocolPointData>();

    ProtocolPointData ppdata;
    String protocolName;
    if (protoPointList != null) {
      for (ScadaPoint ppoint : protoPointList) {
        if (ppoint != null && ppoint.isEnabled()) {
          ScadaPointSource mappedRes = ppoint.getSource();
          if (mappedRes != null) {
            VirtualPointData vpdata = findPointData(vpointDataList, mappedRes.getGroup(), mappedRes.getId());
            protocolName = String.format("%s \"%s\"",
                ppoint.getProtocolName(),
                ppoint.getSessionName()
                );
            ppdata = new ProtocolPointData(protocolName, ppoint.getType().getDescription(),
                ppoint.getPointID(), ppoint.getDescription(), vpdata);
            protoPointDataList.add(ppdata);
          }
        }
      }
    }
    CommsUtil.getRTUPointsAPI(G3RTUFactory.getComms()).updateProtocolPointDataList(protoPointDataList);

  }

  /**
   * Reset the real-time points to initial unloaded state.
   */
  private void resetRTPointsList() {
    RTUPoints comms = CommsUtil.getRTUPointsAPI(G3RTUFactory.getComms());
    comms.updatePointDataList(null);
    comms.updateProtocolPointDataList(null);
  }

  private VirtualPointData findPointData(Collection<VirtualPointData> vpointsData, int group, int id) {
    for (VirtualPointData vp : vpointsData) {
      if (vp.getGroup() == group && vp.getId() == id) {
        return vp;
      }
    }
    return null;
  }
}
