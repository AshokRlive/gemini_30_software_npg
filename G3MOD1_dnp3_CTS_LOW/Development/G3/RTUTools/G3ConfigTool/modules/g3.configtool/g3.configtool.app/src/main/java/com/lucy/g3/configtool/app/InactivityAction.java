
package com.lucy.g3.configtool.app;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

import org.apache.log4j.Logger;

import com.lucy.g3.configtool.rtu.connection.ConnectionManager;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;

public class InactivityAction extends AbstractAction{
  private Logger log = Logger.getLogger(InactivityAction.class);
  private ConnectionManager connManager;
  
  public InactivityAction(ConnectionManager connManager){
    this.connManager = connManager;
  }
  
  @Override
  public void actionPerformed(ActionEvent e) {
    if(!connManager.isDisconnected()) {
        connManager.disconnect();
        String msg = "Disconnected from RTU due to inactivity.";
        log.warn(msg);
        JOptionPane.showMessageDialog(UIUtils.getMainFrame(), 
            msg,
            "Inactivity Logout", JOptionPane.INFORMATION_MESSAGE);
    }
  }
  
  
//  public static class TestPane extends JPanel {
//
//    private Timer timer;
//    private long startTime = -1;
//    private long duration = 5000;
//
//    private JLabel label;
//
//    public TestPane() {
//        setLayout(new GridBagLayout());
//        timer = new Timer(10, new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                if (startTime < 0) {
//                    startTime = System.currentTimeMillis();
//                }
//                long now = System.currentTimeMillis();
//                long clockTime = now - startTime;
//                if (clockTime >= duration) {
//                    clockTime = duration;
//                    timer.stop();
//                }
//                SimpleDateFormat df = new SimpleDateFormat("mm:ss:SSS");
//                label.setText(df.format(duration - clockTime));
//            }
//        });
//        timer.setInitialDelay(0);
//        addMouseListener(new MouseAdapter() {
//            @Override
//            public void mouseClicked(MouseEvent e) {
//                if (!timer.isRunning()) {
//                    startTime = -1;
//                    timer.start();
//                }
//            }
//        });
//        label = new JLabel("...");
//        add(label);
//    }
//
//    @Override
//    public Dimension getPreferredSize() {
//        return new Dimension(200, 200);
//    }

//}

}

