/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import static com.lucy.g3.gui.framework.workbench.internal.WorkbenchWindowActions.ACTION_LOG_DIR;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;

import com.jgoodies.binding.beans.PropertyConnector;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.rtu.config.ConfigurationManager;
import com.lucy.g3.configtool.rtu.connection.ConnectionManager;
import com.lucy.g3.configtool.rtu.controller.RTUController;
import com.lucy.g3.configtool.rtu.diagnostic.RTUDiagnositics;
import com.lucy.g3.configtool.rtu.restart.RTURestartController;
import com.lucy.g3.configtool.rtu.syslog.manager.SystemLogManager;
import com.lucy.g3.configtool.rtu.test.RTUTester;
import com.lucy.g3.configtool.rtu.upgrade.RTUUpgrader;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.gui.common.widgets.utils.ResourceUtils;
import com.lucy.g3.gui.framework.workbench.internal.WorkbenchMenuAdvisor;
import com.lucy.g3.gui.framework.workbench.internal.WorkbenchWindowActions;

/**
 * Menu bar for the main window.
 */
class MenuAdvisor extends WorkbenchMenuAdvisor {

  private final IContext context;


  public MenuAdvisor(IContext context) {
    super();
    this.context = context;
  }

  @Override
  public JMenu[] createMenus() {
    return new JMenu[] {
        createFileMenu(),
        createControlMenu(),
        createDiagnosticsMenu(),
        createAdvancedMenu(),
        createHelpMenu()
    };
  }

  private JMenu createFileMenu() {
    JMenu menu = new JMenu("File   ");
    menu.setMnemonic('F');

    ConfigurationManager configMgr = context.getComponent(ConfigurationManager.SUBSYSTEM_ID);

    menu.add(BusyCursorAction.wrap(configMgr.getAction(ConfigurationManager.ACTION_CREATE_FROM_SCRATCH)));
    menu.add(BusyCursorAction.wrap(configMgr.getAction(ConfigurationManager.ACTION_OPEN_CONFIG)));
    menu.add(BusyCursorAction.wrap(configMgr.getAction(ConfigurationManager.ACTION_CLOSE_CONFIG)));
    menu.addSeparator();
    menu.add(BusyCursorAction.wrap(configMgr.getAction(ConfigurationManager.ACTION_SAVE_CONFIG)));
    menu.add(BusyCursorAction.wrap(configMgr.getAction(ConfigurationManager.ACTION_SAVEAS_CONFIG)));
    menu.addSeparator();
    menu.add(BusyCursorAction.wrap(configMgr.getAction(ConfigurationManager.ACTION_VALIDATE_CONFIG)));
    menu.add(BusyCursorAction.wrap(configMgr.getAction(ConfigurationManager.ACTION_CLEAN_CONFIG)));
    menu.add(BusyCursorAction.wrap(configMgr.getAction(ConfigurationManager.ACTION_PRINT_COFIG)));
    menu.addSeparator();

    ConfigToolActions appActions = context.getComponent(ConfigToolActions.SUBSYSTEM_ID);
    menu.add(BusyCursorAction.wrap(appActions.getAction(ConfigToolActions.ACTION_PREFERENCE)));
    menu.addSeparator();
    menu.add(BusyCursorAction.wrap(getActions().getAction(WorkbenchWindowActions.ACTION_QUIT)));
    return menu;
  }

  private JMenu createHelpMenu() {
    JMenu menu = new JMenu("Help  ");
    menu.setMnemonic('H');

    menu.add(createHelpMenuItem());
    menu.addSeparator();

    // View Log
    menu.add(getActions().getAction(ACTION_LOG_DIR));
    menu.addSeparator();
    
    // About ConfigTool
    ConfigToolActions actions = context.getComponent(ConfigToolActions.SUBSYSTEM_ID);
    menu.add(actions.getAction(ConfigToolActions.ACTION_ABOUT));

    // About RTU
    RTUController controller = context.getComponent(RTUController.SUBSYSTEM_ID);
    menu.add(BusyCursorAction.wrap(controller.getAction(RTUController.ACTION_KEY_ABOUT_RTU))).setIcon(
        ResourceUtils.getImg("about.Action.icon", WorkbenchWindowActions.class));

    return menu;
  }

  private JMenu createDiagnosticsMenu() {
    JMenu menu = new JMenu("Diagnostics   ");
    menu.setMnemonic('D');

    RTUDiagnositics diagnos = context.getComponent(RTUDiagnositics.SUBSYSTEM_ID);
    SystemLogManager syslog = context.getComponent(SystemLogManager.SUBSYSTEM_ID);

    menu.add(BusyCursorAction.wrap(diagnos.getAction(RTUDiagnositics.ACTION_DIAGNOSTIC_CHANNEL)));
    menu.add(BusyCursorAction.wrap(diagnos.getAction(RTUDiagnositics.ACTION_DIAGNOSTIC_VIRTUALPOINT)));
    menu.addSeparator();
    menu.add(BusyCursorAction.wrap(syslog.getAction(SystemLogManager.ACTION_VIEW_SYSLOGS)));
    menu.add(BusyCursorAction.wrap(syslog.getAction(SystemLogManager.ACTION_VIEW_PROTOCOL_TRAFFIC)));
    menu.add(BusyCursorAction.wrap(syslog.getAction(SystemLogManager.ACTION_KEY_ARCHIVE)));

    return menu;
  }

  private JMenu createAdvancedMenu() {
    JMenu menu = new JMenu("Advanced   ");
    menu.setMnemonic('A');

    RTUTester tester = context.getComponent(RTUTester.SUBSYSTEM_ID);
    RTUController controller = context.getComponent(RTUController.SUBSYSTEM_ID);
    RTUUpgrader upgrade = context.getComponent(RTUUpgrader.SUBSYSTEM_ID);
    RTURestartController restartCtrl = context.getComponent(RTURestartController.SUBSYSTEM_ID);

    // Add test menu
    JMenu menuTest = new JMenu("RTU Test");
    menuTest.add(BusyCursorAction.wrap(tester.getAction(RTUTester.ACTION_KEY_FUNCTION_TEST)));
    menuTest.add(BusyCursorAction.wrap(tester.getAction(RTUTester.ACTION_KEY_AUTO_TEST)));
    menu.add(menuTest);
    menu.addSeparator();

    // Add upgrade menu
    JMenu upgModeMenu = new JMenu("Upgrade Mode");
    upgModeMenu.add(BusyCursorAction.wrap(upgrade.getAction(RTUUpgrader.ACTION_KEY_ENABLE_UPGMODE))).setText("Enable");
    upgModeMenu.add(BusyCursorAction.wrap(upgrade.getAction(RTUUpgrader.ACTION_KEY_DISABLE_UPGMODE))).setText(
        "Disable");
    menu.add(upgModeMenu);
    PropertyConnector.connect(upgModeMenu, "enabled", controller, RTUController.PROPERTY_CONNECTED).updateProperty1();
    
    // Add enable/disable debug mode menu
    JMenu debugMenu = new JMenu("Debug Mode");
    debugMenu.add(controller.getAction(RTUController.ACTION_KEY_ENABLE_DEBUG)).setText("Enable");
    debugMenu.add(controller.getAction(RTUController.ACTION_KEY_DISABLE_DEBUG)).setText("Disable");
    PropertyConnector.connect(debugMenu, "enabled", controller, RTUController.PROPERTY_NORMAL_MODE).updateProperty1();
    menu.add(debugMenu);

    /* Disabled temporarily until we need this feature */
    // menu.add(getAction(ACTION_START_CTERM));
//    menu.addSeparator();
//    IEC61131Subsystem iec61131 = context.getComponent(IEC61131Subsystem.SUBSYSTEM_ID);
//    menu.add(BusyCursorAction.wrap(iec61131.getAction(IEC61131Subsystem.ACTION_LAUNCH_IEC61131)));

    
    menu.addSeparator();
    // Certificate
    ConfigToolActions appActions = context.getComponent(ConfigToolActions.SUBSYSTEM_ID);
    menu.add(BusyCursorAction.wrap(appActions.getAction(ConfigToolActions.ACTION_SHOW_CERT_WIZARD)));
    menu.add(BusyCursorAction.wrap(appActions.getAction(ConfigToolActions.ACTION_VIEW_RTU_CERT)));
    menu.addSeparator();
    menu.add(BusyCursorAction.wrap(upgrade.getAction(RTUUpgrader.ACTION_KEY_ERASE_FIRMWARE)));
    menu.add(BusyCursorAction.wrap(upgrade.getAction(RTUUpgrader.ACTION_KEY_SLAVE_UPGRADE)));
    menu.add(BusyCursorAction.wrap(upgrade.getAction(RTUUpgrader.ACTION_KEY_FACTORY_RESET))).setIcon(null);
    menu.add(BusyCursorAction.wrap(restartCtrl.getAction(RTURestartController.ACTION_KEY_SHUTDOWN_RTU))).setText(
        "Shut down RTU");

    return menu;
  }

  private JMenu createControlMenu() {
    JMenu menu = new JMenu("Control   ");
    menu.setMnemonic('C');

    ConnectionManager conMgr = context.getComponent(ConnectionManager.SUBSYSTEM_ID);
    ConfigurationManager configMgr = context.getComponent(ConfigurationManager.SUBSYSTEM_ID);
    RTURestartController restarter = context.getComponent(RTURestartController.SUBSYSTEM_ID);

    // Connect
    menu.add(BusyCursorAction.wrap(conMgr.getAction(ConnectionManager.ACTION_KEY_CONNECT)));

    // Disconnect
    menu.add(BusyCursorAction.wrap(conMgr.getAction(ConnectionManager.ACTION_KEY_DISCONNECT)));

    menu.addSeparator();

    // Register Module
    RTUController controller = context.getComponent(RTUController.SUBSYSTEM_ID);
    menu.add(controller.getAction(RTUController.ACTION_KEY_REGISTER_MODULES)).setIcon(null);

    // Configuration
    JMenu configMenu = new JMenu("RTU Configuration");
    configMenu.add(BusyCursorAction.wrap(configMgr.getAction(ConfigurationManager.ACTION_READ_CONFIG)));
    configMenu.add(BusyCursorAction.wrap(configMgr.getAction(ConfigurationManager.ACTION_WRITE_CONFIG)));
    configMenu.add(BusyCursorAction.wrap(configMgr.getAction(ConfigurationManager.ACTION_ACTIVATE_CONFIG))).setText(
        "Activate");
    configMenu.add(BusyCursorAction.wrap(configMgr.getAction(ConfigurationManager.ACTION_RESTORE_CONFIG))).setText(
        "Restore");
    configMenu.add(BusyCursorAction.wrap(configMgr.getAction(ConfigurationManager.ACTION_ERASE_CONFIG))).setText(
        "Erase");
    menu.add(configMenu);

    menu.addSeparator();

    // Service Mode menu
    JCheckBoxMenuItem enableItem = new JCheckBoxMenuItem(controller.getAction(RTUController.ACTION_KEY_ENABLE_SERVICE));
    JCheckBoxMenuItem disableItem =
        new JCheckBoxMenuItem(controller.getAction(RTUController.ACTION_KEY_DISABLE_SERVICE));
    enableItem.setIcon(null);
    enableItem.setText("Start");
    disableItem.setIcon(null);
    disableItem.setText("Stop");

    JMenu serviceMenu = new JMenu("Service Mode");
    serviceMenu.add(enableItem);
    serviceMenu.add(disableItem);
    PropertyConnector.connect(serviceMenu, "enabled", controller, RTUController.PROPERTY_NORMAL_MODE).updateProperty1();
    menu.add(serviceMenu);

    // Set time
    menu.add(BusyCursorAction.wrap(controller.getAction(RTUController.ACTION_KEY_SET_TIME))).setIcon(null);
    menu.add(BusyCursorAction.wrap(controller.getAction(RTUController.ACTION_KEY_CONFIG_NTP)));
    menu.addSeparator();
    
    // Restart
    menu.add(BusyCursorAction.wrap(restarter.getAction(RTURestartController.ACTION_KEY_SHOW_RESTART_OPTION)));

    return menu;
  }

}
