/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import org.jdesktop.application.TaskEvent;
import org.jdesktop.application.TaskListener;

import com.lucy.g3.configtool.rtu.connection.ConnectionSetupResult;
import com.lucy.g3.configtool.ui.CommissioningTab;
import com.lucy.g3.gui.framework.workbench.IWorkbenchWindowTabContent;
import com.lucy.g3.gui.framework.workbench.internal.WorkbenchWindow;

/**
 * A task listener for handling connection result and navigate main GUI to
 * upgrade view if upgrade is needed.
 */
class ConnectionSetupHandler extends TaskListener.Adapter<ConnectionSetupResult, Void> {
  private final WorkbenchWindow window;
  
  public ConnectionSetupHandler(WorkbenchWindow window) {
    super();
    this.window = window;
  }

  @Override
  public void succeeded(TaskEvent<ConnectionSetupResult> event) {
    ConnectionSetupResult result = event.getValue();
    
    IWorkbenchWindowTabContent tabContent = window.getContent();
    
    if (result.isNeedUpgrade()) {
      tabContent.selectTab(tabContent.findTabIndex(CommissioningTab.ID)); 
    }
  }

  @Override
  public void failed(TaskEvent<Throwable> event) {
  }

  @Override
  public void cancelled(TaskEvent<Void> event) {
  }
}