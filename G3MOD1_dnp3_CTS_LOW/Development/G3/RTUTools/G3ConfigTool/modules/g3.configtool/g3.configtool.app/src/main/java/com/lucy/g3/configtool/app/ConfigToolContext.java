/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;


import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import com.lucy.g3.common.context.AbstractContext;
import com.lucy.g3.common.context.IComponent;
import com.lucy.g3.common.event.Event;
import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.common.event.IEventHandler;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.AbstractSubsystemContext;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.configtool.subsystem.ISubsystemContext;

public class ConfigToolContext extends AbstractSubsystemContext{

  private IEventHandler eventHandler;
  private ConnectionState state;

  public ConfigToolContext() {
    subscribeEvent();
  }

  private void subscribeEvent() {
    eventHandler =
        new IEventHandler() {

          @Override
          public void handleEvent(final Event event) {
            if (event.getTopic().equals(EventTopics.EVT_TOPIC_CONN_STATE_CHG)) {
              final ConnectionState oldState = (ConnectionState) event.getProperty(EventTopics.EVT_PROPERTY_OLD_STATE);
              final ConnectionState newState = (ConnectionState) event.getProperty(EventTopics.EVT_PROPERTY_NEW_STATE);
              ConfigToolContext.this.state = newState;
              
              if(SwingUtilities.isEventDispatchThread()) {
              handleStateChange(oldState, newState);
              
              } else {
                try {
                  SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                      handleStateChange(oldState, newState);
                    }
                  });
                }catch (Exception e) {
                  e.printStackTrace();
                }
              }
            }
          }
        };

    EventBroker.getInstance().subscribe(EventTopics.EVT_TOPIC_CONN_STATE_CHG, eventHandler);
  }

  private void handleStateChange(
      ConnectionState oldState,
      final ConnectionState newState) {
    IComponent[] comps = getAllComponents();
    for (IComponent comp : comps) {
      if (comp instanceof AbstractSubsystem) {
        AbstractSubsystem subsys = (AbstractSubsystem) comp;
        try {
          setSubystemState(subsys, oldState, newState);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }

  @Override
  public ConnectionState getState() {
    return state;
  }
}
