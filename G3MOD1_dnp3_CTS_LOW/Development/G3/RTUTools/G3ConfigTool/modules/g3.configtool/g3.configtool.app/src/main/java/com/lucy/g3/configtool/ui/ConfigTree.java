/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.ui;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang3.ArrayUtils;

import com.lucy.g3.gui.framework.navigation.ExplorerTree;
import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.IPageViewer;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.fields.manager.FieldDeviceManager;
import com.lucy.g3.rtu.config.general.GeneralConfig;
import com.lucy.g3.rtu.config.misc.MiscConfig;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.rtu.config.template.custom.integrated.CustomTemplates;
import com.lucy.g3.rtu.config.user.UserManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;

/**
 * Configuration tree. 
 */
public class ConfigTree extends ExplorerTree {

  private IConfig data;

  private final IPageFactory pagefactory;


  public ConfigTree(Page root, IPageViewer viewer, IPageFactory pagefactory) {
    super(viewer, root);

    this.pagefactory = pagefactory;
  }

  public void setConfigData(IConfig data2) {
    this.data = data2;

    // Prepare tree node data
    Object[] nodedata = null;
    if (data != null) {
      nodedata = new Object[] {
          data.getConfigModule(GeneralConfig.CONFIG_MODULE_ID),
          data.getConfigModule(CANModuleManager.CONFIG_MODULE_ID),
          data.getConfigModule(VirtualPointManager.CONFIG_MODULE_ID),
          data.getConfigModule(CLogicManager.CONFIG_MODULE_ID),
          data.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID),
          data.getConfigModule(FieldDeviceManager.CONFIG_MODULE_ID),
          data.getConfigModule(CommsDeviceManager.CONFIG_MODULE_ID),
          data.getConfigModule(PortsManager.CONFIG_MODULE_ID),
          data.getConfigModule(UserManager.CONFIG_MODULE_ID),
          data.getConfigModule(MiscConfig.CONFIG_MODULE_ID),
          data.getConfigModule(CustomTemplates.CONFIG_MODULE_ID),
      };
      
      Collection<IConfigModule> all = data.getAllConfigModules();
      all.removeAll(Arrays.asList(nodedata));
      ArrayUtils.addAll(nodedata, all.toArray());
    }

    reloadTreeInEDT(nodedata);
  }

  @Override
  protected IPageFactory getPageFacotry() {
    return pagefactory;
  }

}
