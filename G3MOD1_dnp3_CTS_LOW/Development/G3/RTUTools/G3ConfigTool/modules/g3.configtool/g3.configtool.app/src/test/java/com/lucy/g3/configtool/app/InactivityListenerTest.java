
package com.lucy.g3.configtool.app;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;


public class InactivityListenerTest {

  public static void main(String[] args) {
 
    JFrame frame = new JFrame();
    frame.setVisible(true);

    Action logout = new AbstractAction() {
      public void actionPerformed(ActionEvent e) {
        JFrame frame = (JFrame) e.getSource();
        frame.dispose();
      }
    };

    InactivityListener listener = new InactivityListener(frame, logout);
    listener.setIntervalInMillis(5000);
    listener.start(true);
  }
}
