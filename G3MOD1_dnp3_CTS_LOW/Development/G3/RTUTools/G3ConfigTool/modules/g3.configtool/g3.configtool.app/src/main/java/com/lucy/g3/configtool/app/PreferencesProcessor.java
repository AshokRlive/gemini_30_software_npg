/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.InputStream;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.lucy.g3.cert.manager.KeyStoreManager;
import com.lucy.g3.cert.rootCA.RootCA;
import com.lucy.g3.configtool.preferences.PreferenceManager;
import com.lucy.g3.configtool.preferences.impl.ConnectionPreference;
import com.lucy.g3.configtool.preferences.impl.GeneralPreference;
import com.lucy.g3.configtool.preferences.impl.RealtimePreference;
import com.lucy.g3.configtool.preferences.security.SecurityPreference;
import com.lucy.g3.configtool.preferences.security.SecurityPreferenceApplier;
import com.lucy.g3.rtu.config.protocol.iec870.shared.ui.points.IOAFormatter;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.system.config.SystemProperties;
import com.lucy.g3.xml.gen.common.IEC870Enum.LU_IOA_FORMAT;

/**
 * The Class for handling the change of user preference.
 */
class PreferencesProcessor {
  // private final static String PREFS_FILE_NAME = "options.serial";

  private Logger log = Logger.getLogger(PreferencesProcessor.class);

  private G3RTU rtu;

  /**
   * Initialises application and RTU with preferences.
   * @param pm
   * @param _rtu
   */
  public void initialise(PreferenceManager pm, G3RTU _rtu) {
    this.rtu = _rtu;

    // ====== General ======
    {
      GeneralPreference pref = pm.getPreference(GeneralPreference.ID);
      setLogLevel(pref.getLoglevel());
      setLocale(pref.getLocale());

      pref.addPropertyChangeListener(new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
          String property = evt.getPropertyName();

          if (GeneralPreference.PROPERTY_LOGLEVEL.equals(property))
            setLogLevel((org.apache.log4j.Level) evt.getNewValue());
          else if (GeneralPreference.PROPERTY_LOCALE.equals(property)) {
            setLocale((Locale) evt.getNewValue());
          }
        }
      });
    }

    // ====== Connection ======
    {
      ConnectionPreference pref = (ConnectionPreference) pm.getPreference(ConnectionPreference.ID);
      rtu.setHost(pref.getServer());
      rtu.setConnectTimeout(pref.getConnectTimeout());
      pref.addPropertyChangeListener(new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
          if (ConnectionPreference.PROPERTY_SERVER.equals(evt.getPropertyName())) {
            rtu.setHost((String) evt.getNewValue());
          } else if (ConnectionPreference.PROPERTY_CONNECT_TIMEOUT.equals(evt.getPropertyName())) {
            rtu.setConnectTimeout((int) evt.getNewValue());
          }
        }
      });
    }

    // ====== Realtime ======
    {
      RealtimePreference pref = pm.getPreference(RealtimePreference.ID);
      setIOA(pref.getIoaFormat());
      pref.addPropertyChangeListener(new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
          if (RealtimePreference.PROPERTY_IOA_FORMAT.equals(evt.getPropertyName())) {
            setIOA((LU_IOA_FORMAT) evt.getNewValue());
          }
        }
      });
    }

    // ====== Security ======
    {
      SecurityPreference pref = pm.getPreference(SecurityPreference.ID);
      SecurityPreferenceApplier.initialiseKeyStore(pref.getKeyStoreFilePath(), pref.getKeyStorePass());
      SecurityPreferenceApplier.apply(rtu.getComms().getLinkLayer().getSecurityManager(), pref);
    }
  }
  
  private void setIOA(LU_IOA_FORMAT ioa) {
    IOAFormatter.getInstance().setIoaFormat(ioa);
  }

  private void setLocale(Locale locale) {
    if (locale != null) {
      Locale.setDefault(locale);
      this.log.info("Current Language: " + locale.getDisplayLanguage());
    }
  }

  private void setLogLevel(org.apache.log4j.Level level) {
    if (level != null) {
      Logger.getRootLogger().setLevel(level);
      this.log.info("Current Log Level: " + level);
    }
  }

}
