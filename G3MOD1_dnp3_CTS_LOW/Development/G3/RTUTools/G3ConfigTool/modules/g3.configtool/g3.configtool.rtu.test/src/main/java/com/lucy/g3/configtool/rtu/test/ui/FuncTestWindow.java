/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.test.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.ButtonBarBuilder2;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.ext.swing.spinner.SpinnerWheelSupport;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.LocalRemoteCode;
import com.lucy.g3.rtu.comms.service.rtu.control.RTUTestAPI;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.batterytest.BatteryTest;
import com.lucy.g3.rtu.config.clogic.batterytest.BatteryTestSettings;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SWITCH_OPERATION;

/**
 * A Window for running RTU function test. E.g. Fan Test, Battery Charger Test,
 * Switch Operation Test, Off Local Remote Test, etc.
 */
public class FuncTestWindow extends AbstractRTUTestWindow implements ActionListener {

  private Logger log = Logger.getLogger(FuncTestWindow.class);

  private final TaskService taskService;

  private JRadioButton radioBtnLocal;
  private JRadioButton radioBtnRemote;
  private JToggleButton toggleBtnOff;
  private JToggleButton toggleBtnLocal;
  private JToggleButton toggleBtnRemote;

  private ICLogic[] controlLogics;

  private TestingOutput output = new TestingOutput();

  private final RTUTestAPI testcmd;

  private final OLRStateMonitor olrMonitor; // The task for monitoring RTU OLR
  // state

  private final HashMap<String, JSpinner> durationOptions = new HashMap<String, JSpinner>();
  private final HashMap<String, String> unitOptions = new HashMap<String, String>();


  /**
   * Constructs a RTU function test window.
   *
   * @param app
   *          current application.
   * @param testcmd
   *          commands for communicating with RTU.
   * @param controlLogics
   *          switch logic to be tested.
   * @param key
   *          the key value for storing this window.
   */
  public FuncTestWindow(Application app, RTUTestAPI testcmd,
      ICLogic[] controlLogics, String key) {
    super(app, key);
    this.testcmd = Preconditions.checkNotNull(testcmd, "testcmd is null");
    this.controlLogics = controlLogics;
    olrMonitor = new OLRStateMonitor(app);

    taskService = app.getContext().getTaskService(IContext.COMMS_TASK_SERVICE_NAME);

    setTitle("RTU Function Test");
    ImageIcon image = app.getContext().getResourceMap().getImageIcon("Application.icon");
    if (image != null) {
      setIconImage(image.getImage());
    }

    initComponents();
    pack();

    this.addWindowListener(new WindowAdapter() {

      @Override
      public void windowOpened(WindowEvent e) {
        // Schedule task for updating ORL state
        taskService.execute(olrMonitor);
      }

      @Override
      public void windowClosing(WindowEvent e) {
        exit();
      }
    });
  }

  @Action
  public void exit() {
    close();
  }

  private short getDuration(ICLogic logic) {
    short duration = 10;

    JSpinner durationField = durationOptions.get(logic.getName());
    if (durationField != null) {
      duration = (short) (Integer.valueOf(durationField.getValue().toString()).intValue() & 0xffff);
    }
    return duration;
  }

  private String getDurationUnit(ICLogic logic) {
    String durationUnit = unitOptions.get(logic.getName());
    if (durationUnit == null) {
      durationUnit = "";
    }
    return durationUnit;
  }

  private void initComponents() {
    ApplicationActionMap actions = getActionMap();

    toggleBtnOff = new JToggleButton("Off");
    toggleBtnOff.setActionCommand(OLR_STATE.OLR_STATE_OFF.name());
    toggleBtnOff.addActionListener(this);
    toggleBtnOff.setFocusable(false);
    toggleBtnLocal = new JToggleButton("Local");
    toggleBtnLocal.setActionCommand(OLR_STATE.OLR_STATE_LOCAL.name());
    toggleBtnLocal.addActionListener(this);
    toggleBtnLocal.setFocusable(false);
    toggleBtnRemote = new JToggleButton("Remote");
    toggleBtnRemote.setActionCommand(OLR_STATE.OLR_STATE_REMOTE.name());
    toggleBtnRemote.addActionListener(this);
    toggleBtnRemote.setFocusable(false);

    radioBtnLocal = new JRadioButton("Local");
    radioBtnRemote = new JRadioButton("Remote");
    radioBtnLocal.setSelected(true);

    ButtonGroup offLocalRemoteGroup = new ButtonGroup();
    offLocalRemoteGroup.add(toggleBtnOff);
    offLocalRemoteGroup.add(toggleBtnLocal);
    offLocalRemoteGroup.add(toggleBtnRemote);

    ButtonGroup offLocalGroup = new ButtonGroup();
    offLocalGroup.add(radioBtnLocal);
    offLocalGroup.add(radioBtnRemote);

    FormLayout layout = new FormLayout(
        "left:[10dlu,default], 3*($lcgap, [30dlu,pref]), $lcgap, [50dlu,pref],$lcgap,default:grow",
        "");
    DefaultFormBuilder builder = new DefaultFormBuilder(layout);

    // Add output pane
    builder.appendSeparator("Output Message");

    // Add output pane
    JScrollPane scroll = new JScrollPane(output.getComponent());
    scroll.setPreferredSize(new Dimension(100, 100));
    builder.append(scroll, 9);
    builder.nextLine();

    // Add Off Local Remote Test
    builder.appendSeparator("Off Local Remote Test");
    builder.append("", toggleBtnOff, toggleBtnLocal, toggleBtnRemote);
    builder.nextLine();

    // Display a message "No Control Logic"
    if (controlLogics == null || controlLogics.length == 0) {
      builder.append(new JLabel("No available control logic configured!"), 5);
      builder.nextLine();
    }

    else {
      appendAllOperableCLogicComps(builder);
    }
    // Build main panel
    JScrollPane mainPanel = new JScrollPane(builder.getPanel());
    mainPanel.setBorder(BorderFactory.createEmptyBorder());
    UIUtils.increaseScrollSpeed(mainPanel);

    // Build toolbar
    ButtonBarBuilder2 btnBarBuilder = new ButtonBarBuilder2();
    btnBarBuilder.addGlue();
    btnBarBuilder.addButton(new JButton(actions.get("exit")));
    btnBarBuilder.setDefaultButtonBarGapBorder();

    // Build content pane
    JPanel contentPane = new JPanel(new BorderLayout());
    contentPane.setBorder(Borders.DIALOG_BORDER);
    contentPane.add(mainPanel, BorderLayout.CENTER);
    contentPane.add(btnBarBuilder.getPanel(), BorderLayout.SOUTH);
    setContentPane(contentPane);

  }

  private void appendAllOperableCLogicComps(DefaultFormBuilder builder) {

    // Add Switch operation
    builder.appendSeparator("Switch Operation Test");

    // Add Off Local Option
    builder.append("Operate from:", radioBtnLocal, radioBtnRemote);
    builder.nextLine();

    // Append open/close logic
    ICLogicType type;
    for (int i = 0; i < controlLogics.length; i++) {
      type = controlLogics[i].getType();
      if (type.getOpMode() == OperationMode.OPEN_CLOSE) {
        builder.append(controlLogics[i].getName(),
            createOpenCloseButton(controlLogics[i], SWITCH_OPERATION.SWITCH_OPERATION_OPEN),
            createOpenCloseButton(controlLogics[i], SWITCH_OPERATION.SWITCH_OPERATION_CLOSE));
        builder.nextLine();
      }
    }

    builder.appendSeparator("Others");
    
    // Append start-stop logic
    for (int i = 0; i < controlLogics.length; i++) {
      type = controlLogics[i].getType();
      if (type.getOpMode() == OperationMode.START_STOP) {
        appendStartStopComps(builder, 
            controlLogics[i].getDescription(), 
            isDurationRequired(type),
            true,
            controlLogics[i]);
      }
    }
      
    // Append generic logic
    for (int i = 0; i < controlLogics.length; i++) {
      type = controlLogics[i].getType();
      if (type.getOpMode() == OperationMode.GENERIC_OPERATE) {
        appendStartStopComps(builder, 
            controlLogics[i].getDescription(), 
            isDurationRequired(type),
            false,
            controlLogics[i]);
      }
    }
  }

  private static boolean isDurationRequired(ICLogicType type) {
//    return type == ICLogicType.FAN
//            || type == ICLogicType.BAT_CHARGER;
    return type.requireDurationWhenTest();
  }

  private LocalRemoteCode getLocalRemoteOption() {
    LocalRemoteCode localRemote = null;
    if (radioBtnLocal.isSelected()) {
      localRemote = LocalRemoteCode.LOCAL;
    } else if (radioBtnRemote.isSelected()) {
      localRemote = LocalRemoteCode.REMOTE;
    } else {
      log.error("Local/Remote option is not selected");
    }
    return localRemote;
  }

  private JButton createOpenCloseButton(final ICLogic logic, final SWITCH_OPERATION opCode) {
    JButton btn = new JButton(opCode == SWITCH_OPERATION.SWITCH_OPERATION_OPEN ? "Open" : "Close");
    btn.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        taskService.execute(new SwitchLogicOperateTask(
            application,
            logic, opCode, getLocalRemoteOption()));
      }
    });
    return btn;
  }
  
  
  private void appendStartStopComps(DefaultFormBuilder builder, String description,
      boolean requireDuration, boolean requireStopBtn, ICLogic clogic) {
    // Append start button and label
    JButton start = new JButton("Start");
    start.addActionListener(new LogicOperateAction(clogic, requireDuration));
    builder.append(clogic.getName(), start);

    if (requireStopBtn) {
      // Append start button and label
      JButton stop = new JButton("Stop");
      stop.addActionListener(new LogicStopAction(clogic));
      builder.append(stop);
    }

    // Append duration fields and label
    if (requireDuration) {
      appendDuration(builder, clogic);
    }

    builder.nextLine();
  }

  private void appendDuration(DefaultFormBuilder builder, ICLogic clogic) {
    JLabel durationLabel = new JLabel(clogic.getType().getId() == (ICLogicType.BAT_CHARGER) ? "Maximum  Duration:" : "Duration:");
    durationLabel.setHorizontalAlignment(JLabel.RIGHT);
    JSpinner durationField = new JSpinner(new SpinnerNumberModel(10, 1, null, 10));
    SpinnerWheelSupport.installMouseWheelSupport(durationField);

    String unit = "seconds";
    if (clogic instanceof BatteryTest) {
      if (((BatteryTest) clogic).isConfiguredAsBatteryTest()) {
        unit = "minutes";
        durationField = new JSpinner(new SpinnerNumberModel(10,
            BatteryTestSettings.BATTERY_TEST_DURATION_MIN,
            BatteryTestSettings.BATTERY_TEST_DURATION_MAX,
            1));
      }
    }

    durationOptions.put(clogic.getName(), durationField);
    unitOptions.put(clogic.getName(), unit);

    SpinnerWheelSupport.installMouseWheelSupport(durationField);
    builder.append(durationLabel, durationField, new JLabel(unit));
  }

  private void updateOLRButtonState(OLR_STATE state) {
    if (state == null) {
      toggleBtnOff.setSelected(false);
      toggleBtnLocal.setSelected(false);
      toggleBtnRemote.setSelected(false);
    } else {
      switch (state) {
      case OLR_STATE_LOCAL:
        toggleBtnLocal.setSelected(true);
        break;
      case OLR_STATE_OFF:
        toggleBtnOff.setSelected(true);
        break;
      case OLR_STATE_REMOTE:
        toggleBtnRemote.setSelected(true);
        break;
      default:
        break;
      }
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    Object source = e.getSource();
    if (source == toggleBtnOff || source == toggleBtnLocal || source == toggleBtnRemote) {
      OLR_STATE state = OLR_STATE.valueOf(e.getActionCommand());
      olrMonitor.operateOLR(state);
    }
  }


  private class SwitchLogicOperateTask extends Task<Void, Void> {

    private ICLogic switchLogic;
    private SWITCH_OPERATION operation;
    private LocalRemoteCode localRemote;
    private String description;


    public SwitchLogicOperateTask(Application application, ICLogic switchLogic,
        SWITCH_OPERATION operation, LocalRemoteCode localRemote) {
      super(application);
      this.switchLogic = switchLogic;
      this.operation = operation;
      this.localRemote = localRemote;

      if (operation == SWITCH_OPERATION.SWITCH_OPERATION_OPEN) {
        description = "Open \"" + switchLogic.getName() + "\"";
      } else if (operation == SWITCH_OPERATION.SWITCH_OPERATION_CLOSE) {
        description = "Close \"" + switchLogic.getName() + "\"";
      }
      setTitle("Switch Operation");
      setDescription(description);
    }

    @Override
    protected Void doInBackground() throws Exception {
      testcmd.cmdOperateSwitch((short) switchLogic.getGroup(), operation, localRemote);
      return null;
    }

    @Override
    protected void failed(final Throwable cause) {
      final String msg = description + " failed";
      setMessage(msg);
      output.appendErr(msg + " cause: " + cause.getMessage());
    }

    @Override
    protected void succeeded(Void result) {
      final String msg = description;
      setMessage(msg);
      output.appendSuccess(msg);
    }
  }

  private class LogicStopAction implements ActionListener {

    private ICLogic logic;


    public LogicStopAction(ICLogic logic) {
      this.logic = logic;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      taskService.execute(new LogicStopTask(application, logic));
    }
  }

  private class LogicStopTask extends Task<Void, Void> {

    private ICLogic testLogic;


    public LogicStopTask(Application application, ICLogic testLogic) {
      super(application);
      this.testLogic = testLogic;

      setTitle("Test " + testLogic);
      setDescription("");
    }

    @Override
    protected Void doInBackground() throws Exception {
      testcmd.cmdOperateSwitch((short) testLogic.getGroup(), SWITCH_OPERATION.SWITCH_OPERATION_OPEN,
          getLocalRemoteOption());
      return null;
    }

    @Override
    protected void failed(final Throwable cause) {
      setMessage(cause.getMessage());
      output.appendErr(String.format("Failed to stop \"%s\" cause:%s ", testLogic, cause.getMessage()));
    }

    @Override
    protected void succeeded(Void result) {
      output.appendSuccess(String.format("\"%s\" stopped", testLogic));
    }
  }

  private class LogicOperateAction implements ActionListener {

    private ICLogic logic;
    private final boolean requireDuration;


    public LogicOperateAction(ICLogic logic, boolean requireDuration) {
      this.logic = logic;
      this.requireDuration = requireDuration;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      taskService.execute(new LogicOperateTask(application,
          logic, requireDuration));
    }
  }

  private class LogicOperateTask extends Task<Void, Void> {

    private ICLogic testLogic;
    private final boolean requireDuration;
    private final short duration;
    private final String durationUnit;


    public LogicOperateTask(Application application, ICLogic testLogic,
        boolean requireDuration) {
      super(application);
      this.testLogic = testLogic;
      duration = getDuration(testLogic);
      durationUnit = getDurationUnit(testLogic);

      this.requireDuration = requireDuration;
      setTitle("Test " + testLogic);
      setDescription("");
    }

    @Override
    protected Void doInBackground() throws Exception {
      testcmd.cmdOperateLogic((short) testLogic.getGroup(), duration);
      return null;
    }

    @Override
    protected void failed(final Throwable cause) {
      setMessage(cause.getMessage());
      output.appendErr(String.format("Failed to start \"%s\" cause:%s ", testLogic, cause.getMessage()));
    }

    @Override
    protected void succeeded(Void result) {
      if (requireDuration) {
        output.appendSuccess(String.format("\"%s\" started: %d %s", testLogic, duration, durationUnit));
      } else {
        output.appendSuccess(String.format("\"%s\" started", testLogic));
      }
    }
  }

  private class OLRStateMonitor extends Task<Void, Void> {

    private int period = 500; // (ms)Task loop period

    private OLR_STATE curState = null; // Current local state, synch with RTU
    private final LinkedList<OLR_STATE> operations = new LinkedList<OLR_STATE>();


    public OLRStateMonitor(Application application) {
      super(application);
      setTitle("Off Local State Monitor");
      setDescription("Synch OLR state with RTU");
    }

    public void operateOLR(OLR_STATE opState) {
      if (opState != null) {
        operations.add(opState);
        output.appendMsg("Trying to set OffLocalRemote to: " + opState);
      }
    }

    @Override
    protected Void doInBackground() throws Exception {

      while (true) {
        if (isCancelled())
        {
          break;// Exit
        }

        /* Operating OLR */
        while (!operations.isEmpty() && !isCancelled()) {
          OLR_STATE opState;
          try {
            opState = operations.remove();
            testcmd.cmdSetOLR(opState);
            output.appendSuccess("OffLocalRemote changed to: " + opState);
            curState = opState;
            Thread.sleep(2000); // Give RTU enough time to complete operation.
          } catch (Exception e) {
            output.appendErr("Failed to change OffLocalRemote cause: " + e.getMessage());
          }
        }

        /* Retrieving OLR state */
        if (!isCancelled()) {
          try {
            OLR_STATE newState = testcmd.cmdGetOLR();
            if (curState != newState) {
              curState = newState;
              output.appendMsg("Current OffLocalRemote state: " + newState);
              updateOLRButtonState(newState);
            }
          } catch (IllegalStateException e2) {
          } catch (Exception e) {
            Logger.getLogger(getClass()).error("Failed to get OffLocalRemote:" + e.getMessage());
          }
        }

        Thread.sleep(period);
      }
      return null;
    }
  }


  @Override
  public void notifyConnectionStateChanged(ConnectionState oldState, ConnectionState newState) {
    if (ConnectionState.isConnected(newState)) {
      output.appendMsg("Connected to RTU ");
    } else {
      output.appendErr("Disconnected from RTU ");
    }
  }

  @Override
  public void stopTesting() {
    olrMonitor.cancel(true);
  }

}
