/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.test;

import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.configtool.rtu.config.ConfigurationManager;
import com.lucy.g3.configtool.rtu.test.ui.AutoTestWindow;
import com.lucy.g3.configtool.rtu.test.ui.FuncTestWindow;
import com.lucy.g3.configtool.subsys.permissions.PermissionCheckChecks;
import com.lucy.g3.configtool.subsys.permissions.UserActivities;
import com.lucy.g3.configtool.subsystem.AbstractRealtimeWindow;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.manager.G3RTU;

/**
 * RTU Tester manages actions to perform RTU test. All actions can be accessed
 * using method {@linkplain #getAction(String)}.
 */
public class RTUTester extends AbstractSubsystem  {
  public final static String SUBSYSTEM_ID = "subystem.id.RTUTester";
  
  public static final String ACTION_KEY_FUNCTION_TEST = "funcTest";
  public static final String ACTION_KEY_AUTO_TEST = "autoTest";
  
  private static final String WINDOW_KEY_FUNC_TEST = "FunctionTest";
  private static final String WINDOW_KEY_AUTO_TEST = "AutoTest";
  
  private Logger log = Logger.getLogger(RTUTester.class);

  private final WindowListener testWindowListener = new TestWindowCloseListener();

  private final HashMap<String, AbstractRealtimeWindow> testWindows = new HashMap<String, AbstractRealtimeWindow>();

  private G3RTU g3RTU;


  public RTUTester(com.lucy.g3.common.context.IContext context, G3RTU g3RTU) {
    super(context, SUBSYSTEM_ID);
    this.g3RTU = Preconditions.checkNotNull(g3RTU, "g3RTU is null");
  }

  @Override
  protected void notifyStateChanged(ConnectionState oldState,
      ConnectionState newState) {

    if (newState == ConnectionState.DISCONNECTED) {
      // Close all test windows on disconnect state
      closeAllTestWindow();

    } else {
      // Notify all testWindow state changed
      Collection<AbstractRealtimeWindow> allwindows = testWindows.values();
      for (AbstractRealtimeWindow w : allwindows) {
        w.notifyConnectionStateChanged(oldState, newState);
      }
    }
  }

  private void closeAllTestWindow() {
    Collection<AbstractRealtimeWindow> allwindows = testWindows.values();
    for (AbstractRealtimeWindow w : allwindows) {
      w.close();
    }
  }

  @Action(enabledProperty = PROPERTY_NORMAL_MODE)
  public void autoTest() {
    final String key = WINDOW_KEY_AUTO_TEST;

    if (checkAllConditions() == true) {
      AbstractRealtimeWindow testWindow = testWindows.get(key);

      if (testWindow == null) {
        CLogicManager logicsManager = getRTUConfig().getConfigModule(CLogicManager.CONFIG_MODULE_ID);
        Collection<ICLogic> logics = logicsManager.getLogicByType(ICLogicType.SGL,
            ICLogicType.DSL, ICLogicType.DOL);
        testWindow = new AutoTestWindow(getApp(), g3RTU.getComms(), logics.toArray(new ICLogic[logics.size()]), key);
        testWindow.addWindowListener(testWindowListener);
        testWindows.put(key, testWindow);
      }

      showWindow(testWindow);
    }
  }

  @Action(enabledProperty = PROPERTY_NORMAL_MODE)
  public void funcTest() {
    final String key = WINDOW_KEY_FUNC_TEST;

    if (checkAllConditions() == true) {
      AbstractRealtimeWindow testWindow = testWindows.get(key);

      if (testWindow == null) {
        CLogicManager logicsManager = getRTUConfig().getConfigModule(CLogicManager.CONFIG_MODULE_ID);
        List<IOperableLogic> logics = logicsManager.getOperableCLogic();
        testWindow = new FuncTestWindow(getApp(), g3RTU.getComms(), logics.toArray(new ICLogic[logics.size()]), key);
        testWindow.addWindowListener(testWindowListener);
        testWindows.put(key, testWindow);
      }

      showWindow(testWindow);
    }
  }


  private boolean checkAllConditions() {
    if (checkIsConnected() == false) {
      return false;
    }

    if (checkConfigDataExist() == false) {
      return false;
    }

    if (PermissionCheckChecks.checkPermitted(UserActivities.TEST_RTU) == false) {
      return false;
    }

    return true;
  }

  /**
   * Check if configData is loaded. If not, show a dialog to help import
   * configuration
   */
  private boolean checkConfigDataExist() {
    if (getRTUConfig() == null) {
      JOptionPane
          .showMessageDialog(
              getMainFrame(),
              "No configuration found. Please read configuration from RTU first.",
              "Error", 
              JOptionPane.ERROR_MESSAGE);

      return false;
    }

    return true;
  }

  private IConfig getRTUConfig() {
    ConfigurationManager configMgr = getContext().getComponent(ConfigurationManager.SUBSYSTEM_ID);
    return configMgr == null ? null : configMgr.getRTUConfig();
  }


  private class TestWindowCloseListener extends WindowAdapter {

    @Override
    public void windowClosed(WindowEvent e) {
      Window window = (Window) e.getSource();

      /*
       * Removing closed window is necessary since we want test windows to be
       * re-created next time so it will be synched with the latest
       * configuration.
       */
      log.info("Window " + window.getName() + " is closed.");
      window.removeWindowListener(testWindowListener);
      AbstractRealtimeWindow testwindow = (AbstractRealtimeWindow) window;
      testWindows.remove(testwindow.getId());
    }

  }
}
