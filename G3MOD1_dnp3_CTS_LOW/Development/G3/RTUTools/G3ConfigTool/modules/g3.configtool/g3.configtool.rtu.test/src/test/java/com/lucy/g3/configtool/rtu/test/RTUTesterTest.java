/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.test;

import static org.junit.Assert.assertNotNull;

import org.jdesktop.application.Application;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.configtool.rtu.test.RTUTester;
import com.lucy.g3.rtu.manager.G3RTU;

/**
 * RTUTester Test.
 */
public class RTUTesterTest {

  private static final String[] ACTION_KEYS = {
      RTUTester.ACTION_KEY_FUNCTION_TEST,
      RTUTester.ACTION_KEY_AUTO_TEST,
  };

  private RTUTester rtutester;


  @Before
  public void setUp() throws Exception {
    rtutester = createTester();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testActionsNotNull() {
    for (int i = 0; i < ACTION_KEYS.length; i++) {
      assertNotNull(rtutester.getAction(ACTION_KEYS[i]));
    }
  }

  private static RTUTester createTester() {
    return new RTUTester(null,  new G3RTU(null));
  }
  
    
}
