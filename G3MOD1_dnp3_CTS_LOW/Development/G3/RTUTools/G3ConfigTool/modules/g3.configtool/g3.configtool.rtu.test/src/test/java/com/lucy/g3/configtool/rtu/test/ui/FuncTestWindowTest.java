/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.test.ui;

import java.io.IOException;

import org.jdesktop.application.Application;

import com.lucy.g3.configtool.rtu.test.ui.FuncTestWindow;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.LocalRemoteCode;
import com.lucy.g3.rtu.comms.protocol.exceptions.ProtocolException;
import com.lucy.g3.rtu.comms.service.rtu.control.RTUTestAPI;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.batterytest.BatteryTest;
import com.lucy.g3.rtu.config.clogic.fantest.FanTest;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SWITCH_OPERATION;

public class FuncTestWindowTest {

  // Manual Test
  public static void main(String[] args) {
    ICLogic[] logic = new ICLogic[3];
    logic[0] = new FanTest();
    logic[1] = new BatteryTest();
    logic[2] = new BatteryTest();
    new FuncTestWindow(Application.getInstance(), new TestCMDStub(), logic, "k").setVisible(true); 
  }


  private static class TestCMDStub implements RTUTestAPI {

    @Override
    public void cmdSetOLR(OLR_STATE state) throws IOException,
        ProtocolException {

    }

    @Override
    public void cmdOperateSwitch(short logicID, SWITCH_OPERATION openClose,
        LocalRemoteCode localRemote) throws IOException, ProtocolException {

    }

    @Override
    public OLR_STATE cmdGetOLR() throws IOException, ProtocolException {

      return null;
    }

    @Override
    public void cmdOperateLogic(short fanControlLogicID, short period)
        throws IOException, ProtocolException {

    }

  };

}
