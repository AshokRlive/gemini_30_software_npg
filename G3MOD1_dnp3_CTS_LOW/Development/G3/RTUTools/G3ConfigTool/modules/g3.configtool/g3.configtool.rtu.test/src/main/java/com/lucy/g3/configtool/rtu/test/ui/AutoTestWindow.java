/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.test.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.SpinnerNumberModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.ButtonStackBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.LocalRemoteCode;
import com.lucy.g3.rtu.comms.service.rtu.control.RTUTestAPI;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SWITCH_OPERATION;

/**
 * A Window for testing RTU switch operation automatically.
 */
public class AutoTestWindow extends AbstractRTUTestWindow {

  private final Logger log = Logger.getLogger("Switch-Operation-Auto-Test");

  private final ICLogic[] switchLogics;

  private Task<?, ?> autoTask;

  private final TestingOutput output = new TestingOutput(log);

  private final RTUTestAPI testcmd;


  /**
   * Constructs a RTU auto test window.
   *
   * @param app
   *          current application.
   * @param testcmd
   *          commands for communicating with RTU.
   * @param switchLogics
   *          switch logic to be tested.
   * @param key
   *          the key value for storing this window.
   */
  public AutoTestWindow(Application app, RTUTestAPI testcmd, ICLogic[] switchLogics, String key) {
    super(app, key);
    this.testcmd = Preconditions.checkNotNull(testcmd, "testcmd is null");
    this.switchLogics = switchLogics;

    initComponents();
    pack();
  }

  @Action
  public void start() {
    // Check control logic selection
    ICLogic clogic = (ICLogic) combCLogic.getSelectedItem();
    if (clogic == null) {
      JOptionPane.showMessageDialog(this, "No Switch Control Logic selected",
          "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    // Get parameters
    int times = (Integer) spinnerTimes.getValue();
    final long delayValue = (Integer) spinnerDelay.getValue();
    TimeUnit unit = (TimeUnit) combTimeUnit.getSelectedItem();
    String delayStr;
    long delay;
    if (unit == TimeUnit.SECONDS) {
      delayStr = delayValue + " seconds";
      delay = TimeUnit.SECONDS.toMillis(delayValue);
      
    } else if (unit == TimeUnit.MINUTES) {
      delayStr = delayValue + " minutes";
      delay = TimeUnit.MINUTES.toMillis(delayValue);
      
    } else {
      delayStr = delayValue + " ms";
      delay = delayValue;
    }

    // Get local/remote option
    LocalRemoteCode olr;
    if (radioBtnLocal.isSelected()) {
      olr = LocalRemoteCode.LOCAL;
    } else {
      olr = LocalRemoteCode.REMOTE;
    }

    int ret = JOptionPane.showConfirmDialog(this, 
        String.format("This will operate the \"%s\" %s times at a rate of 1 time per %s\nAre you sure?",
            clogic.getDescription(),
            isInfinitTimes(times) ? "Inifit" : times,
            delayStr), 
        "Confirm", JOptionPane.YES_NO_CANCEL_OPTION);
    
    if (ret == JOptionPane.YES_OPTION) {
      // Stop current running task
      stop();
      
      // Execute task
      autoTask = new SwitchOperationTask(application, clogic, olr, times, delay);
      updateEnablement(true);
      getTaskService().execute(autoTask);
    }
  }

  @Action
  public void exit() {
    if (autoTask != null && !autoTask.isDone()) {
      int rtn = JOptionPane.showConfirmDialog(AutoTestWindow.this,
          "Auto test is running. Do you want to exit?",
          "Warning", JOptionPane.WARNING_MESSAGE);
      if (rtn != JOptionPane.YES_OPTION) {
        return;
      }
    }

    close();
  }

  @Action
  public void stop() {
    stopTesting();
  }

  @Action
  public void log() {
    File log = new File("log/TestLog.html");
    try {
      Desktop.getDesktop().open(log);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  private void updateEnablement(boolean isTaskRunning) {
    ApplicationActionMap actions = getActionMap();
    actions.get("start").setEnabled(!isTaskRunning);
    combCLogic.setEnabled(!isTaskRunning);
    combTimeUnit.setEnabled(!isTaskRunning);
    spinnerDelay.setEnabled(!isTaskRunning);
    spinnerTimes.setEnabled(!isTaskRunning);
    radioBtnLocal.setEnabled(!isTaskRunning);
    radioBtnRemote2.setEnabled(!isTaskRunning);
  }

  private void createUIComponents() {
    if (switchLogics != null && switchLogics.length > 0) {
      combCLogic = new JComboBox<Object>(switchLogics);
    } else {
      combCLogic = new JComboBox<Object>();
    }

    combTimeUnit = new JComboBox<Object>(new Object[] { TimeUnit.SECONDS, TimeUnit.MINUTES });

    toolBar = new JToolBar();
    ApplicationActionMap actions = getActionMap();
    toolBar.add(new JButton(actions.get("start")));
    toolBar.add(new JButton(actions.get("stop")));
    toolBar.add(new JButton(actions.get("log")));
    toolBar.add(new JButton(actions.get("exit")));

    controlPanel = new JPanel();
    ButtonStackBuilder builder = new ButtonStackBuilder(controlPanel);
    builder.addGridded(new JButton(actions.get("start")));
    builder.addRelatedGap();
    builder.addGridded(new JButton(actions.get("stop")));
    builder.addUnrelatedGap();
    builder.addGridded(new JButton(actions.get("log")));
    builder.addGlue();
    builder.addGridded(new JButton(actions.get("exit")));
    builder.setDefaultDialogBorder();

    outputArea = output.getComponent();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    scrollPane2 = new JScrollPane();
    contentPane = new JPanel();
    xTitledSeparator7 = new JXTitledSeparator();
    scrollPane1 = new JScrollPane();
    xTitledSeparator1 = new JXTitledSeparator();
    JPanel panelOptions = new JPanel();
    label2 = new JLabel();
    label1 = new JLabel();
    radioBtnLocal = new JRadioButton();
    radioBtnRemote2 = new JRadioButton();
    lblSwitchLogic = new JLabel();
    label3 = new JLabel();
    spinnerTimes = new JSpinner();
    label5 = new JLabel();
    label4 = new JLabel();
    spinnerDelay = new JSpinner();
    progressBar1 = new JProgressBar();

    // ======== this ========
    setTitle("Switch Operation Auto Test");
    Container contentPane2 = getContentPane();
    contentPane2.setLayout(new BorderLayout());

    // ======== scrollPane2 ========
    {
      scrollPane2.setBorder(BorderFactory.createEmptyBorder());

      // ======== contentPane ========
      {
        contentPane.setBorder(Borders.DIALOG_BORDER);
        contentPane.setLayout(new FormLayout(
            "[10dlu,default]:grow",
            "default, $lgap, fill:[60dlu,default]:grow, 2*($lgap, default)"));

        // ---- xTitledSeparator7 ----
        xTitledSeparator7.setTitle("Output Message");
        contentPane.add(xTitledSeparator7, CC.xy(1, 1));

        // ======== scrollPane1 ========
        {
          scrollPane1.setPreferredSize(new Dimension(300, 100));

          // ---- outputArea ----
          outputArea.setEditable(false);
          outputArea.setFont(new Font("Arial", Font.PLAIN, 11));
          scrollPane1.setViewportView(outputArea);
        }
        contentPane.add(scrollPane1, CC.xy(1, 3));

        // ---- xTitledSeparator1 ----
        xTitledSeparator1.setTitle("Options");
        contentPane.add(xTitledSeparator1, CC.xy(1, 5));

        // ======== panelOptions ========
        {
          panelOptions.setLayout(new FormLayout(
              "[10dlu,default], $lcgap, right:[50dlu,default], $lcgap, default, 2*($lcgap, [50dlu,default]), $lcgap, [20dlu,pref]:grow",
              "2*(default, $lgap), fill:default, $lgap, default"));

          // ---- label2 ----
          label2.setText("Switch Control Logic: ");
          panelOptions.add(label2, CC.xy(3, 1));
          panelOptions.add(combCLogic, CC.xywh(5, 1, 5, 1));

          // ---- label1 ----
          label1.setText("Operate from:");
          panelOptions.add(label1, CC.xy(3, 3));

          // ---- radioBtnLocal ----
          radioBtnLocal.setText("Local");
          panelOptions.add(radioBtnLocal, CC.xy(5, 3));

          // ---- radioBtnRemote2 ----
          radioBtnRemote2.setText("Remote");
          radioBtnRemote2.setSelected(true);
          panelOptions.add(radioBtnRemote2, CC.xy(7, 3));
          panelOptions.add(lblSwitchLogic, CC.xy(1, 5));

          // ---- label3 ----
          label3.setText("Open & Close Cycles:");
          panelOptions.add(label3, CC.xy(3, 5));

          // ---- spinnerTimes ----
          spinnerTimes.setModel(new SpinnerNumberModel(3, 0, null, 1));
          panelOptions.add(spinnerTimes, CC.xywh(5, 5, 3, 1));

          // ---- label5 ----
          label5.setText("(Input 0 for infinite testing)");
          label5.setForeground(Color.gray);
          panelOptions.add(label5, CC.xywh(9, 5, 3, 1));

          // ---- label4 ----
          label4.setText("Delay between Operations:");
          panelOptions.add(label4, CC.xy(3, 7));

          // ---- spinnerDelay ----
          spinnerDelay.setModel(new SpinnerNumberModel(5, 0, null, 1));
          panelOptions.add(spinnerDelay, CC.xywh(5, 7, 3, 1));
          panelOptions.add(combTimeUnit, CC.xy(9, 7));
        }
        contentPane.add(panelOptions, CC.xy(1, 7));
      }
      scrollPane2.setViewportView(contentPane);
    }
    contentPane2.add(scrollPane2, BorderLayout.CENTER);
    contentPane2.add(progressBar1, BorderLayout.PAGE_END);
    contentPane2.add(controlPanel, BorderLayout.LINE_END);

    // ======== toolBar ========
    {
      toolBar.setMargin(new Insets(10, 10, 10, 10));
      toolBar.setFocusable(false);
      toolBar.setFloatable(false);
    }

    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(radioBtnLocal);
    buttonGroup1.add(radioBtnRemote2);

    setSize(660, 450);
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  private static boolean isInfinitTimes(int totalTimes) {
    return totalTimes <= 0;
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane scrollPane2;
  private JPanel contentPane;
  private JXTitledSeparator xTitledSeparator7;
  private JScrollPane scrollPane1;
  private JTextPane outputArea;
  private JXTitledSeparator xTitledSeparator1;
  private JLabel label2;
  private JComboBox<Object> combCLogic;
  private JLabel label1;
  private JRadioButton radioBtnLocal;
  private JRadioButton radioBtnRemote2;
  private JLabel lblSwitchLogic;
  private JLabel label3;
  private JSpinner spinnerTimes;
  private JLabel label5;
  private JLabel label4;
  private JSpinner spinnerDelay;
  private JComboBox<Object> combTimeUnit;
  private JProgressBar progressBar1;
  private JPanel controlPanel;
  private JToolBar toolBar;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  private class SwitchOperationTask extends Task<Void, Void> {

    private final LocalRemoteCode localRemote;
    private final int totalTimes;
    private final long delay;
    private final int logicGroup;
    private final ICLogic switchLogic;
    private int errorNum = 0;


    public SwitchOperationTask(Application application, ICLogic switchLogic, LocalRemoteCode localRemote,
        int times, long delay) {
      super(application);
      if (switchLogic == null) {
        throw new IllegalArgumentException("Control Logic is null");
      }
      logicGroup = switchLogic.getGroup();
      this.localRemote = localRemote;
      totalTimes = times;
      this.delay = delay;
      this.switchLogic = switchLogic;
      progressBar1.setMaximum(totalTimes * 2);

      setTitle("Auto Switch Operation ");
      setDescription("Switch operation periodically");
    }

    @Override
    protected Void doInBackground() throws Exception {
      output.appendMsg("=================================" +
          "================================="); // Separator

      int times = 1;
      String switchLogicName = switchLogic.getName();
      StringBuilder builder = new StringBuilder();
      builder.append("Start Testing Open/Close Logic: \"");
      builder.append(switchLogic.getName());
      builder.append("\"   Total times: ");
      if (isInfinitTimes(totalTimes)) {
        builder.append("infinit");
      } else {
        builder.append(totalTimes);
      }
      builder.append("   Operation Delay: ");
      builder.append(TimeUnit.MILLISECONDS.toSeconds(delay));
      builder.append(" seconds");

      output.appendMsg(builder.toString());

      progressBar1.setValue(0);
      while (times <= totalTimes || isInfinitTimes(totalTimes)) {
        // OPEN
        output.appendMsg("Trying to open: " + switchLogicName);
        try {
          testcmd.cmdOperateSwitch((short) logicGroup, SWITCH_OPERATION.SWITCH_OPERATION_OPEN, localRemote);
          output.appendSuccess(switchLogicName + " is opened");
        } catch (Exception e) {
          output.appendErr("Open switch failed: " + e.getMessage());
          errorNum++;
        }
        progressBar1.setValue(times);
        Thread.sleep(delay);

        // ClOSE
        output.appendMsg("Trying to close: " + switchLogicName);
        try {
          testcmd.cmdOperateSwitch((short) logicGroup, SWITCH_OPERATION.SWITCH_OPERATION_CLOSE, localRemote);
          output.appendSuccess(switchLogicName + " is closed");
        } catch (Exception e) {
          output.appendErr("Close switch failed: " + e.getMessage());
          errorNum++;
        }
        progressBar1.setValue(times + 1);
        Thread.sleep(delay);
        times++;
      }

      return null;
    }

    @Override
    protected void failed(final Throwable cause) {
      final String msg = cause.getMessage();
      output.appendErr(msg);
      JOptionPane.showMessageDialog(AutoTestWindow.this,
          msg + " cause:\n " + cause.getMessage(),
          "Operation Fail", JOptionPane.ERROR_MESSAGE);
    }

    @Override
    protected void cancelled() {
      output.appendMsg("[Test Canncelled] Error Num: " + errorNum);
    }

    @Override
    protected void finished() {
      updateEnablement(false);
    }

    @Override
    protected void succeeded(Void result) {
      output.appendMsg("[Test Finished] Error Num: " + errorNum);
    }
  }


  @Override
  public void stopTesting() {
    if (autoTask != null && !autoTask.isDone()) {
      autoTask.cancel(true);
      autoTask = null;
    }
  }

  @Override
  public void notifyConnectionStateChanged(ConnectionState oldState, ConnectionState newState) {
    // Nothing to do.
  }
}
