/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.test.ui;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;
import org.jdesktop.application.ApplicationActionMap;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.configtool.rtu.test.RTUTester;
import com.lucy.g3.gui.framework.page.AbstractPage;

/**
 * A view showing testing buttons and description.
 */
public class TestPage extends AbstractPage {

  private final RTUTester tester;

 
  public TestPage(RTUTester tester) {
    super(null);
    setNodeName("Test");
    this.tester = Preconditions.checkNotNull(tester, "tester is null");
  }

  @Override
  protected void init() throws Exception {
    initComponents();
  }

  private void createUIComponents() {
    ApplicationActionMap testActions = tester.getActionMap();

    Action action = testActions.get(RTUTester.ACTION_KEY_AUTO_TEST);
    btnAutoTest = new JButton(action);
    labelAutoTest = new JLabel((String) action.getValue(Action.SHORT_DESCRIPTION));

    action = testActions.get(RTUTester.ACTION_KEY_FUNCTION_TEST);
    btnFuncTest = new JButton(action);
    labelFuntest = new JLabel((String) action.getValue(Action.SHORT_DESCRIPTION));
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    panel1 = new JPanel();

    //======== this ========
    setBorder(Borders.DIALOG_BORDER);
    setLayout(new FormLayout(
        "[50dlu,default]:grow",
        "fill:default"));

    //======== panel1 ========
    {
      panel1.setBorder(new CompoundBorder(
          new TitledBorder("Actions"),
          Borders.DLU2_BORDER));
      panel1.setLayout(new FormLayout(
          "[50dlu,default], $ugap, default:grow",
          "fill:default, $ugap, fill:default"));

      //---- btnFuncTest ----
      btnFuncTest.setText("Manual Test");
      btnFuncTest.setIcon(null);
      btnFuncTest.setToolTipText(null);
      panel1.add(btnFuncTest, CC.xy(1, 1));
      panel1.add(labelFuntest, CC.xy(3, 1));

      //---- btnAutoTest ----
      btnAutoTest.setText("Auto Test");
      btnAutoTest.setIcon(null);
      btnAutoTest.setToolTipText(null);
      panel1.add(btnAutoTest, CC.xy(1, 3));
      panel1.add(labelAutoTest, CC.xy(3, 3));
    }
    add(panel1, CC.xy(1, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panel1;
  private JButton btnFuncTest;
  private JLabel labelFuntest;
  private JButton btnAutoTest;
  private JLabel labelAutoTest;
  // JFormDesigner - End of variables declaration //GEN-END:variables


  @Override
  public boolean hasError() {
    return false;
  }

}
