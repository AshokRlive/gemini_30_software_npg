/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.test.ui;

import org.apache.log4j.Logger;

import com.lucy.g3.gui.common.widgets.console.SimpleConsole;

class TestingOutput extends SimpleConsole {

  public TestingOutput() {
    super();
  }

  public TestingOutput(Logger log) {
    super(log);
  }
  
  
}
