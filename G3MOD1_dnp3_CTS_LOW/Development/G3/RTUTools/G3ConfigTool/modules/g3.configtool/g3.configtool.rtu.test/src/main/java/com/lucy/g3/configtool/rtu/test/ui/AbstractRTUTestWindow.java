/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.test.ui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import org.jdesktop.application.Application;
import org.jdesktop.application.TaskService;

import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.subsystem.AbstractRealtimeWindow;

/**
 * The basic implementation of Window for testing RTU.
 */
abstract class AbstractRTUTestWindow extends AbstractRealtimeWindow {

  public AbstractRTUTestWindow(Application app, String id) {
    super(app, id);
    
    addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosed(WindowEvent e) {
        stopTesting();
      }
      
    });
  }

  protected TaskService getTaskService() {
    return getApplication().getContext().getTaskService(IContext.COMMS_TASK_SERVICE_NAME);
  }

  public abstract void stopTesting();
}
