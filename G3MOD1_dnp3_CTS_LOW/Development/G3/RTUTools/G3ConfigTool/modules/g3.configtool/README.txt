Configuration Tool Project 

Naming Convention
--------------------------------------------------------------------------------
SDNP3		DNP3 Slave
S104		IEC104 Slave
MMB			Modbus Master
CLogic		Control Logic


Maven Build
--------------------------------------------------------------------------------
#Build from command line
>mvn package


#Build with additional options
>mvn clean package -Dmaven.test.skip=true -Dmaven.javadoc.skip=true -Dmaven.source.skip=true -DDIST_DIR=.\


#Build in Eclipse
a.Import G3ConfigTool project into Eclipse
b.RunAs -> Maven Build... -> set goals as follows:
	resources:resources compiler:compile 
	Apply and Run!
or simply
  RunAs -> maven install


#Launch ConfigTool from command line
>mvn exec:java 
or
>mvn exec:java -Dexec.mainClass="com.lucy.g3.configtool.app.Main"


Useful Maven commands
--------------------------------------------------------------------------------
mvn install
mvn compile
mvn test
mvn deploy
mvn generate-resources
mvn clean

#Copy all thirdparty libraries
>mvn dependency:copy-dependencies -DincludeScope="compile" -DexcludeGroupIds="com.lucy" -DoutputDirectory="C:\Work\ThirdParty\libs"

#Copy all thirdparty libraries javadoc 
>mvn dependency:copy-dependencies -DincludeScope="compile" -Dclassifier="javadoc" -DexcludeGroupIds="com.lucy" -DoutputDirectory="C:\Work\ThirdParty\libs-javadoc"

(Available classifier: sources,javadoc,artifact)

#Generate dependency tree
>mvn dependency:tree -Dincludes="com.lucy" -DoutputFile="C:\G3ConfigTool\target\dependency\${project.artifactId}.txt"

#Generate checkstyle report
>mvn checkstyle:checkstyle

#Specify maven folder "-f"
>mvn -f Library\pom.xml clean install

#Build with plugin configuration
$mvn package -DdoCheck=false -Dmaven.buildNumber.doCheck=false -Dmaven.buildNumber.doUpdate=false 

#Generate resources
$mvn generate-resources -DdoCheck=false -Dmaven.buildNumber.doCheck=false -Dmaven.buildNumber.doUpdate=false

# Skip test
-Dmaven.test.skip=true  #skip test compiling,install,etc
-DskipTests             #Only skip test running


Maven plugins
--------------------------------------------------------------------------------
# Set javacompiler version used by maven
	<plugin>
		<groupId>org.apache.maven.plugins</groupId>
		<artifactId>maven-compiler-plugin</artifactId>
		<configuration>
			<source>1.6</source>
			<target>1.6</target>
			<showDeprecation>true</showDeprecation>
			<showWarnings>true</showWarnings>
			<executable>${env.JAVA_HOME_6}/bin/javac</executable>
			<fork>true</fork>
		</configuration>
	</plugin>


Version Update
--------------------------------------------------------------------------------
mvn versions:set -DnewVersion=YOUR NEW VERSION

e.g
$:mvn versions:set -DnewVersion=0.9.6-SNAPSHOT

afterwards,
mvn versions:commit

or
mvn versions:revert





