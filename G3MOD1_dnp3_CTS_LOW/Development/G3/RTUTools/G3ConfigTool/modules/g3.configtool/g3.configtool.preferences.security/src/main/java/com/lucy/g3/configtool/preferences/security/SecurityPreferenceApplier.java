/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.security;

import java.io.File;
import java.io.InputStream;

import org.apache.log4j.Logger;

import com.lucy.g3.cert.manager.KeyStoreManager;
import com.lucy.g3.cert.rootCA.RootCA;
import com.lucy.g3.rtu.comms.datalink.ISecurityManager;

/**
 * Import certificate to security manag
 */
public class SecurityPreferenceApplier {
  private static Logger log = Logger.getLogger(SecurityPreferenceApplier.class);
  
  public static void apply(ISecurityManager security, SecurityPreference pref) {
    String ksPath = pref.getKeyStoreFilePath();
    char[] ksPass = pref.getKeyStorePassChars();
    
    importKeyStore(security, ksPath, ksPass);
    
    security.setIgnoreCertificateError(pref.exceptions().isException(security.getHostAddress()));
  }
  
  
  
  /**
   * Imports certificates store file to security manager.
   * @param security
   * @param ksPath
   * @param password
   */
  private static void importKeyStore(ISecurityManager security, String ksPath, char[] password) {
    if (security != null) {
      File ksFile = new File(ksPath);

      // Create empty store if not found.
      if (!ksFile.exists() || !ksFile.isFile()) {
        KeyStoreManager.createEmptyKeyStore(ksFile, password);
      }

      if (ksFile.exists() && ksFile.isFile()) {
        try {
            security.importKeyStore(ksFile, password);
            log.info("Imported Keystore: "+ksFile);
        } catch (Exception e) {
          log.error("Failed to import keystore:" + ksFile);
        }
      } else {
        log.warn("Keystore file not found:" + ksFile);
      }
    }
  }
  

  /**
   * Creates keystore file if not exists and import lucy CA root.
   * @param ksPath
   * @param ksPass
   * @return
   */
  public final static boolean initialiseKeyStore(String ksPath, String ksPass) {
    Logger log = Logger.getLogger(SecurityPreference.class);
    
      KeyStoreManager ksMgr = new KeyStoreManager();
      
      // Set default file path and pass for the store
      ksMgr.setKeyStoreFile(new File(ksPath));
      ksMgr.setPassword(ksPass.toCharArray());
      
      // Load existing key store
      if(new File(ksPath).exists()) {
        try {
          ksMgr.load(ksPath, ksPass.toCharArray());
        } catch (Exception e) {
          log.warn("Failed to load keystore! A new default store will be created.");
        }
      } else {
        log.warn("Keystore was not found. A default one will be created!");
      }
      
      
      // Import lucy root certificate to the store.
      try{
        if(!ksMgr.containsAlias(RootCA.ROOT_ALIAS)) {
          InputStream in = RootCA.getRootCAStream();
          ksMgr.importTrustedCert(RootCA.ROOT_ALIAS, in);
          ksMgr.store();
          in.close();
          log.info("Imported certificate: " + RootCA.ROOT_ALIAS);
        }
        return true;
      }catch(Exception e) {
        log.error("Failed to import lucy root certificate!",e);
        return false;
      }
    }
}

