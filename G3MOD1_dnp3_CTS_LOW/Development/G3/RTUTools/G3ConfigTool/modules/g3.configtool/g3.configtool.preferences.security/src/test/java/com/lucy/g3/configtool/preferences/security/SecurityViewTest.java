/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.security;

import java.io.File;

import com.lucy.g3.configtool.preferences.PreferenceManager;
import com.lucy.g3.configtool.preferences.security.SecurityPreference;
import com.lucy.g3.configtool.preferences.ui.PreferenceDialog;


public class SecurityViewTest {


  public static void main(String[] args) {
    PreferenceManager manager = new PreferenceManager();
    manager.init(new File("target/SecurityViewTest/"), 
        new String[]{SecurityPreference.ID}, new Class<?>[]{SecurityPreference.class});
    new PreferenceDialog(null, manager).setVisible(true);
  }
}

