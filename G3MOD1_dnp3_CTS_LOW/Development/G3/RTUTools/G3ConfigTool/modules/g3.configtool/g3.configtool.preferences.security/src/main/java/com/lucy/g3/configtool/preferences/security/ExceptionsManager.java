/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.security;

import java.io.Serializable;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.network.support.IPAddress;
import com.lucy.g3.security.support.SSLSupport;

 public class ExceptionsManager implements Serializable {
  private ArrayList<InetSocketAddress> exceptionHosts = new ArrayList<>();

  public void setExceptions(Collection<InetSocketAddress> exceptions) {
    for(InetSocketAddress address: exceptions) {
      validateAddress(address);
    }
    
    getList().clear();
    getList().addAll(exceptions);
  }

  public void addException(String hostIP) {
    if(IPAddress.validateIPAddress(hostIP) == false){
      throw new IllegalArgumentException("Invalid IP address: " + hostIP);
    }
    
    addException(InetSocketAddress.createUnresolved(hostIP, SSLSupport.DEFAULT_PORT));
  }

  /**
   * @param address only IP host name accepted
   */
  private void addException(InetSocketAddress address){
    validateAddress(address);
      
    
    if(!getList().contains(address))
      getList().add(address);
  }

  public static void validateAddress(InetSocketAddress address) {
    if( address == null)
      throw new IllegalArgumentException("Invalid address, host address must not be null");
    
    if(IPAddress.validateIPAddress(address.getHostName()) == false)
      throw new IllegalArgumentException("Invalid IP address: " + address.getHostName());
  }

  public void removeException(String host){
      getList().remove(host);
  }

  private ArrayList<InetSocketAddress> getList(){
    if(exceptionHosts == null)
      exceptionHosts = new ArrayList<>();
    return exceptionHosts;
  }

  public Collection<InetSocketAddress> getAllExceptionHosts() {
    return new ArrayList<>(getList());
  }

  /**
   * Checks if a host address is in exception list for verifying SSL certification.
   * @param hostAddress
   * @return
   */
  public boolean isException(String hostAddress) {
    if(hostAddress == null)
      return false;
      
    ArrayList<InetSocketAddress> list = getList();
    int port = -1;
    if(hostAddress.contains(":")) {
      port = Integer.valueOf(hostAddress.substring(hostAddress.indexOf(":")+1, hostAddress.length()));
      hostAddress = hostAddress.substring(0, hostAddress.indexOf(":"));
    }
    
    for (InetSocketAddress address: list) {
      if(hostAddress.trim().equals(address.getHostName())
          && (port < 0 || port == address.getPort()))
        return true;
    }
    
    return false;
  }

  public int size() {
    return getList().size();
  }

}

