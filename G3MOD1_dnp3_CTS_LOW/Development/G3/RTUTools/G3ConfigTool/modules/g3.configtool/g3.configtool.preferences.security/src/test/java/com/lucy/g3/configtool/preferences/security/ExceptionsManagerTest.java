/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.security;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.configtool.preferences.security.ExceptionsManager;


/**
 *
 */
public class ExceptionsManagerTest {
  private ExceptionsManager manager;
  
  @Before
  public void setUp() throws Exception {
    manager = new ExceptionsManager();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddValidException() {
    String [] validsamples = {
        "10.11.11.102",
        "0.0.0.0",
        "192.168.0.1",
    };
    
    for (int i = 0; i < validsamples.length; i++) {
      manager.addException(validsamples[i]);
    }
    
    for (int i = 0; i < validsamples.length; i++) {
      assertTrue(manager.isException(validsamples[i]));
    }
    
    assertEquals(validsamples.length, manager.size());
  }
  
  @Test
  public void testAddInvalidException() {
    String [] invalidsamples = {
        null,
        "",
        "-1.0.0.0",
        "192.168.0.256",
        "www",
        "ddd.com",
    };
    
    for (int i = 0; i < invalidsamples.length; i++) {
      try{
        manager.addException(invalidsamples[i]);
        fail("Exception expected for invalid host:"+invalidsamples[i]);
      }catch(Exception e){
        
      }
    }
    
    assertEquals(0, manager.size());
    
  }


}

