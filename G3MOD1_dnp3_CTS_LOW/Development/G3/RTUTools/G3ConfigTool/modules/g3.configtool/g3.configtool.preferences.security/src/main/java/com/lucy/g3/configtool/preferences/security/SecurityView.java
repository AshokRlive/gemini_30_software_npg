/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.security;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.net.InetSocketAddress;
import java.security.KeyStoreException;
import java.text.MessageFormat;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.util.WindowUtils;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.cert.manager.KeyStoreManager;
import com.lucy.g3.cert.manager.gui.DGetHostPort;
import com.lucy.g3.cert.manager.gui.DGetPassword;
import com.lucy.g3.cert.manager.gui.DViewCertificate;
import com.lucy.g3.cert.manager.gui.KeyStoreActions;
import com.lucy.g3.cert.manager.gui.KeyStoreActions.ActionCallback;
import com.lucy.g3.cert.manager.gui.KeyStoreTable;
import com.lucy.g3.configtool.preferences.impl.AbstractView;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.rtu.comms.datalink.ISecurityManager;
import com.lucy.g3.rtu.manager.G3RTUManager;

public class SecurityView extends AbstractView<SecurityPreference> {
  private Logger log = Logger.getLogger(SecurityView.class);

  private final KeyStoreManager ksManager;
  private final KeyStoreActions ksActions;
  
  private ArrayListModel<InetSocketAddress> exceptionListModel;
  
  
  public SecurityView(Object owner, SecurityPreference bean) {
    super(owner, bean);
    
    this.ksManager = new KeyStoreManager();
    this.ksActions = new KeyStoreActions(ksManager, new KeyStoreActionCallback());
    this.exceptionListModel = new ArrayListModel<>(bean.exceptions().getAllExceptionHosts());
    
    initComponents();
    initComponentsBinding();
    initActions();
    loadKeyStoreFile(bean.getKeyStoreFilePath(), bean.getKeyStorePassChars());
  }

  private void initActions() {
    Action action = null;
    
    action = ksActions.getAction(KeyStoreActions.ACTION_KEYSTOREREPORT);
    action.putValue(Action.NAME, "Report...");
    action.putValue(Action.SHORT_DESCRIPTION, "Show all certificates in a report.");
    btnKeystoreReport.setAction(action);
    
    action = ksActions.getAction(KeyStoreActions.ACTION_IMPORTTRUSTEDCERT);
    action.putValue(Action.NAME, "Import...");
    action.putValue(Action.SHORT_DESCRIPTION, "Import a trusted certificate to the current certificate store.");
    btnImportCert.setAction(action);
    
    action = ksActions.getAction(KeyStoreActions.ACTION_CERTDETAILS);
    action.putValue(Action.NAME, "View...");
    action.putValue(Action.SHORT_DESCRIPTION, "Show the details of the selected certificate.");
    btnViewCert.setAction(action);
    
    action = ksActions.getAction(KeyStoreActions.ACTION_DELETEENTRY);
    action.putValue(Action.NAME, "Delete");
    action.putValue(Action.SHORT_DESCRIPTION, "Delete the selected certificate.");
    btnRemoveCert.setAction(action);
    
    
    ActionListener acl;
    acl = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        addException();
      }
    };
    menuItemAddExcept.addActionListener(acl);
    btnAddException.addActionListener(acl);

    acl = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        removeException();
      }
    };
    menuItemRemoveExcept.addActionListener(acl);
    btnDeleteException.addActionListener(acl);

    acl = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        editException();
      }
    };
    menuItemEditExcept.addActionListener(acl);
    btnEditException.addActionListener(acl);
  }


  @Override
  public void commit() {
    super.commit();
    
    // Save KeyStore file
    try {
      if(ksManager.isChanged())
        ksManager.store();
      
    } catch (Exception e) {
      log.error("Failed to store certificates to the keystore file.", e);
    }
    
    // Save exception list
    getPreference().exceptions().setExceptions(exceptionListModel);
  }


  private void addException() {
    InetSocketAddress address = inputHost(null);
    if(address != null) {
      try{
        ExceptionsManager.validateAddress(address);
        exceptionListModel.add(address);
      } catch(Exception e) {
        MessageDialogs.error(this, "Failed to add exception", e);
      }
    }
  }

  private void removeException() {
    List<?> selections = exceptionList.getSelectedValuesList();
    if(MessageDialogs.confirmRemoveAll(this))
      exceptionListModel.removeAll(selections);
  }

  private void editException() {
    int index = exceptionList.getSelectedIndex();
    if(index >= 0) {
      InetSocketAddress host = exceptionListModel.get(index);
      host = inputHost(host);
      if(host != null)
        exceptionListModel.set(index, host);
    } else{
      MessageDialogs.warning(this, "No entry selected!");
    }
  }

  private InetSocketAddress inputHost(InetSocketAddress old) {
    DGetHostPort dlg = new DGetHostPort(WindowUtils.findWindow(this), "Add address to exception list", old);
    //dlg.setPortFieldEditable(false);
    dlg.setResolveName(false);
    dlg.setVisible(true);
    return dlg.getHostPort();
  }

  private void initComponentsBinding() {
    Bindings.bind(tfKeyStorePath, model.getBufferedModel(SecurityPreference.PROPERTY_KEYSTORE_FILEPATH));
    Bindings.bind(tfPass, model.getBufferedModel(SecurityPreference.PROPERTY_KEYSTORE_PASS));
    //Bindings.bind(checkBoxVerifyCert, model.getBufferedModel(SecurityPreference.PROPERTY_CERTIFICATE_VERIFY_EANBLED));
  }

  private boolean loadKeyStoreFile(String file, char[] pass) {
    try {
      ksManager.load(file, pass);
      return true;
    } catch (Exception e) {
        MessageDialogs.error(this, "Failed to load keystore", e);
        return false;
    }
  }
  
  private void refreshKeyStoreTable() {
    try {
      ((KeyStoreTable)keystoreTable).load(ksManager.getKeyStore());
    } catch (KeyStoreException e) {
      log.error("Failed to load keystore to table", e);
    }
  }
  
  @SuppressWarnings("unchecked")
  private void createUIComponents() {
    // Create keystore table
    keystoreTable = new KeyStoreTable();
    
    // Refresh table content when keystore changes
    ksManager.addPropertyChangeListener(KeyStoreManager.PROPERTY_IS_CHANGED, 
        new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        refreshKeyStoreTable();
      }
    });
    
    // Create keystore table popup menu
    keystoreTable.setComponentPopupMenu(createKeyStorePopupMenu());
    
    
    // Create exception list
    exceptionList = new JList<>(exceptionListModel);
  }


  private JPopupMenu createKeyStorePopupMenu() {
    JPopupMenu popup = new JPopupMenu();
    popup.add(ksActions.getAction(KeyStoreActions.ACTION_CERTDETAILS));
    popup.add(ksActions.getAction(KeyStoreActions.ACTION_KEYSTOREREPORT));
    popup.addSeparator();
    popup.add(ksActions.getAction(KeyStoreActions.ACTION_IMPORTTRUSTEDCERT));
    popup.add(ksActions.getAction(KeyStoreActions.ACTION_DELETEENTRY));
    popup.add(ksActions.getAction(KeyStoreActions.ACTION_RENAMECERT));
    popup.addSeparator();
    popup.add(new KeyStoreRefreshAction());
    return popup;
  }


  private void exceptionListMouseClicked(MouseEvent e) {
    if(e.getClickCount() > 1) {
      editException();
    }
  }

  private void keystoreTableMouseClicked(MouseEvent e) {
    if(e.getClickCount() > 1) {
      ksActions.certDetails();
    }
  }

  private void btnChangeKeyStoreActionPerformed(ActionEvent e) {
    File storeFile =
        DialogUtils.showFileChooseDialog(this, "Choose Certificate Store", G3Files.FC_FILTER_G3_CERT_STORE);
    if (storeFile != null) {
      char[] pass = DGetPassword.showDialog(WindowUtils.findWindow(this), 
          MessageFormat.format("Password required for ''{0}''", storeFile.getName()));
      
      String path = storeFile.getAbsolutePath();
      if(pass != null) {
        if(loadKeyStoreFile(path, pass)) {
          tfKeyStorePath.setText(path);
          tfPass.setText(String.valueOf(pass));
          MessageDialogs.finish(this, "The new keystore has been loaded"); 
         
        }
      }
    }
  
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    tabbedPane1 = new JTabbedPane();
    panelCert = new JPanel();
    panelCerts = new JPanel();
    label4 = new JLabel();
    scrollPane1 = new JScrollPane();
    panelControl = new JPanel();
    btnImportCert = new JButton();
    btnViewCert = new JButton();
    btnKeystoreReport = new JButton();
    btnRemoveCert = new JButton();
    panelStore = new JPanel();
    label1 = new JLabel();
    tfKeyStorePath = new JTextField();
    btnChangeKeyStore = new JButton();
    tfPass = new JTextField();
    label3 = new JLabel();
    panelExceptions = new JPanel();
    scrollPane2 = new JScrollPane();
    panelControl2 = new JPanel();
    btnAddException = new JButton();
    btnEditException = new JButton();
    btnDeleteException = new JButton();
    exceptionListPopup = new JPopupMenu();
    menuItemAddExcept = new JMenuItem();
    menuItemEditExcept = new JMenuItem();
    menuItemRemoveExcept = new JMenuItem();

    //======== this ========
    setLayout(new BorderLayout());

    //======== tabbedPane1 ========
    {

      //======== panelCert ========
      {
        panelCert.setLayout(new BorderLayout(0, 10));

        //======== panelCerts ========
        {
          panelCerts.setLayout(new BorderLayout());

          //---- label4 ----
          label4.setText("- Trusted Certificates -");
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          panelCerts.add(label4, BorderLayout.NORTH);

          //======== scrollPane1 ========
          {
            scrollPane1.setPreferredSize(new Dimension(10, 10));

            //---- keystoreTable ----
            keystoreTable.setPreferredSize(new Dimension(100, 32));
            keystoreTable.addMouseListener(new MouseAdapter() {

              @Override
              public void mouseClicked(MouseEvent e) {
                keystoreTableMouseClicked(e);
              }
            });
            scrollPane1.setViewportView(keystoreTable);
          }
          panelCerts.add(scrollPane1, BorderLayout.CENTER);
        }
        panelCert.add(panelCerts, BorderLayout.CENTER);

        //======== panelControl ========
        {
          panelControl.setBorder(Borders.createEmptyBorder("10dlu, 4dlu, 4dlu, 4dlu"));
          panelControl.setLayout(new FormLayout(
              "default",
              "default, $lgap, default, $rgap, default, $pgap, default"));

          //---- btnImportCert ----
          btnImportCert.setText("Import Certificate");
          panelControl.add(btnImportCert, CC.xy(1, 1));

          //---- btnViewCert ----
          btnViewCert.setText("View Certificate");
          panelControl.add(btnViewCert, CC.xy(1, 3));

          //---- btnKeystoreReport ----
          btnKeystoreReport.setText("Keystore Report");
          panelControl.add(btnKeystoreReport, CC.xy(1, 5));

          //---- btnRemoveCert ----
          btnRemoveCert.setText("Delete");
          panelControl.add(btnRemoveCert, CC.xy(1, 7));
        }
        panelCert.add(panelControl, BorderLayout.EAST);

        //======== panelStore ========
        {
          panelStore.setLayout(new FormLayout(
              "right:default, $lcgap, [default,10dlu]:grow, $lcgap, 10dlu",
              "fill:default, $lgap, default"));

          //---- label1 ----
          label1.setText("Trusted Certificate Store:");
          panelStore.add(label1, CC.xy(1, 1));

          //---- tfKeyStorePath ----
          tfKeyStorePath.setEditable(false);
          tfKeyStorePath.setToolTipText("The file for storing all trusted certificates used by G3 Configuration Tool");
          panelStore.add(tfKeyStorePath, CC.xy(3, 1));

          //---- btnChangeKeyStore ----
          btnChangeKeyStore.setText("...");
          btnChangeKeyStore.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              btnChangeKeyStoreActionPerformed(e);
            }
          });
          panelStore.add(btnChangeKeyStore, CC.xy(5, 1));

          //---- tfPass ----
          tfPass.setEditable(false);
          tfPass.setVisible(false);
          panelStore.add(tfPass, CC.xy(3, 3));

          //---- label3 ----
          label3.setText("Store Password:");
          label3.setVisible(false);
          panelStore.add(label3, CC.xy(1, 3));
        }
        panelCert.add(panelStore, BorderLayout.NORTH);
      }
      tabbedPane1.addTab("Certificates", panelCert);

      //======== panelExceptions ========
      {
        panelExceptions.setLayout(new BorderLayout());

        //======== scrollPane2 ========
        {

          //---- exceptionList ----
          exceptionList.setComponentPopupMenu(exceptionListPopup);
          exceptionList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
              exceptionListMouseClicked(e);
            }
          });
          scrollPane2.setViewportView(exceptionList);
        }
        panelExceptions.add(scrollPane2, BorderLayout.CENTER);

        //======== panelControl2 ========
        {
          panelControl2.setBorder(Borders.TABBED_DIALOG_BORDER);
          panelControl2.setLayout(new FormLayout(
              "default",
              "3*(default, $lgap), default"));

          //---- btnAddException ----
          btnAddException.setText("Add Exception");
          panelControl2.add(btnAddException, CC.xy(1, 1));

          //---- btnEditException ----
          btnEditException.setText("Edit");
          panelControl2.add(btnEditException, CC.xy(1, 3));

          //---- btnDeleteException ----
          btnDeleteException.setText("Delete");
          panelControl2.add(btnDeleteException, CC.xy(1, 5));
        }
        panelExceptions.add(panelControl2, BorderLayout.EAST);
      }
      tabbedPane1.addTab("Exceptions", panelExceptions);
    }
    add(tabbedPane1, BorderLayout.CENTER);

    //======== exceptionListPopup ========
    {

      //---- menuItemAddExcept ----
      menuItemAddExcept.setText("Add Exception");
      exceptionListPopup.add(menuItemAddExcept);

      //---- menuItemEditExcept ----
      menuItemEditExcept.setText("Edit");
      exceptionListPopup.add(menuItemEditExcept);

      //---- menuItemRemoveExcept ----
      menuItemRemoveExcept.setText("Delete");
      exceptionListPopup.add(menuItemRemoveExcept);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  
     BusyCursorAction.apply(btnChangeKeyStore, this);
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JTabbedPane tabbedPane1;
  private JPanel panelCert;
  private JPanel panelCerts;
  private JLabel label4;
  private JScrollPane scrollPane1;
  private JTable keystoreTable;
  private JPanel panelControl;
  private JButton btnImportCert;
  private JButton btnViewCert;
  private JButton btnKeystoreReport;
  private JButton btnRemoveCert;
  private JPanel panelStore;
  private JLabel label1;
  private JTextField tfKeyStorePath;
  private JButton btnChangeKeyStore;
  private JTextField tfPass;
  private JLabel label3;
  private JPanel panelExceptions;
  private JScrollPane scrollPane2;
  private JList<Object> exceptionList;
  private JPanel panelControl2;
  private JButton btnAddException;
  private JButton btnEditException;
  private JButton btnDeleteException;
  private JPopupMenu exceptionListPopup;
  private JMenuItem menuItemAddExcept;
  private JMenuItem menuItemEditExcept;
  private JMenuItem menuItemRemoveExcept;
  // JFormDesigner - End of variables declaration //GEN-END:variables


  @Override
  public int getIndex() {
    return 3;
  }
  
  private class KeyStoreRefreshAction extends AbstractAction {
    KeyStoreRefreshAction(){
      super("Refresh");
    }
    @Override
    public void actionPerformed(ActionEvent e) {
      refreshKeyStoreTable();
    }
    
  }
  
  private class KeyStoreActionCallback implements ActionCallback {
    @Override
    public String getSelectedType() {
      return ((KeyStoreTable)keystoreTable).getSelectedType();
    }

    @Override
    public String getSelectedAlias() {
      return ((KeyStoreTable)keystoreTable).getSelectedAlias();
    }

    @Override
    public Window getParent() {
      return SecurityView.this.getWindow();
    }
    
  }
}
