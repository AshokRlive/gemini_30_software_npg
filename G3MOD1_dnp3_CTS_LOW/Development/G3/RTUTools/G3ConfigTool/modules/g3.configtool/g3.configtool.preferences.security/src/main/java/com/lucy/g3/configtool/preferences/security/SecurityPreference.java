/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.security;

import java.io.File;
import org.jdesktop.application.Application;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.configtool.preferences.IPreferenceView;
import com.lucy.g3.configtool.preferences.impl.AbstractPreference;

/**
 * Security related preferences.
 */
public class SecurityPreference extends AbstractPreference {
  
  private static final long serialVersionUID = 599147996766062115L;
  private static final String DEFAULT_KEYSTORE_NAME = "g3truststore.jks";
  

  public static final String ID = "SecurityPreference";

  
  public static final String PROPERTY_KEYSTORE_FILEPATH = "keyStoreFilePath";
  public static final String PROPERTY_KEYSTORE_PASS = "keyStorePass";

  private String keyStoreFilePath;
  private String keyStorePass = "password";
  
  private ExceptionsManager exceptionsManager;
  
  
  public SecurityPreference() {
    super(ID);
    exceptionsManager = new ExceptionsManager();
  }
  
  public String getKeyStoreFilePath() {
    if(Strings.isBlank(keyStoreFilePath)) {
      keyStoreFilePath = 
          new File(Application.getInstance().getContext().getLocalStorage().getDirectory(), 
              DEFAULT_KEYSTORE_NAME).getAbsolutePath();
    }
    return keyStoreFilePath;
  }

  public void setKeyStoreFilePath(String keyStoreFilePath) {
    if(keyStoreFilePath == null)
      return;
    
    Object oldValue = this.keyStoreFilePath;
    this.keyStoreFilePath = keyStoreFilePath;
    firePropertyChange(PROPERTY_KEYSTORE_FILEPATH, oldValue, keyStoreFilePath);
  }

  
  public String getKeyStorePass() {
    return keyStorePass;
  }

  
  public void setKeyStorePass(String keyStorePass) {
    Object oldValue = this.keyStorePass;
    this.keyStorePass = keyStorePass;
    firePropertyChange(PROPERTY_KEYSTORE_PASS, oldValue, keyStorePass);
  }

  public char[] getKeyStorePassChars() {
    return keyStorePass == null ? null : keyStorePass.toCharArray();
  }
  
  
  @Override
  public IPreferenceView createView(Object owner) {
    return new SecurityView(owner, this);
  }
  
  /**
   * Gets the exception manager.
   * @return non-null exception manager.
   */
  public ExceptionsManager exceptions() {
    if(exceptionsManager == null)
      exceptionsManager = new ExceptionsManager();
    
    return exceptionsManager;
  }
}
