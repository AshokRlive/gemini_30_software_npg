/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.diagnostic.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.SortOrder;
import javax.swing.SpinnerNumberModel;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.EnabledHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.StringValue;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.configtool.subsys.realtime.factory.VirtualPointDataFactory;
import com.lucy.g3.configtool.subsys.realtime.ui.points.RTVirtualPointsTableModel;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.ext.swing.renderer.BooleanCellRenderer;
import com.lucy.g3.gui.common.widgets.ext.swing.spinner.SpinnerWheelSupport;
import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel.SelectionMode;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.PollingMode;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData;
import com.lucy.g3.rtu.comms.service.realtime.points.RTUPoints;
import com.lucy.g3.rtu.comms.service.realtime.points.VirtualPointData;
import com.lucy.g3.rtu.comms.shared.RTUTimeDecoder;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.dialog.selector.VirtualPointSelector;

/**
 * A window for diagnosing virtual point value.
 */
public class VirtualPointDiagnostic extends JDialog {

  public static final String ACTION_KEY_ADD = "addPoints";
  public static final String ACTION_KEY_DELETE = "deletePoints";
  public static final String ACTION_KEY_DELETE_ALL = "deleteAllPoints";
  public static final String ACTION_KEY_REFRESH_VALUE = "refreshValue";
  public static final String ACTION_KEY_CLEAR_VALUE = "clearValue";
  public static final String ACTION_KEY_CLOSE = "close";

  private Logger log = Logger.getLogger(VirtualPointDiagnostic.class);

  private final ArrayList<VirtualPoint> selectionPoints = new ArrayList<VirtualPoint>();

  private final ArrayListModel<IPointData> pollingPointListModel = new ArrayListModel<IPointData>();
  private final ArrayList<IPointData> pollingDPointList = new ArrayList<IPointData>();
  private final ArrayList<IPointData> pollingAPointList = new ArrayList<IPointData>();
  private final ArrayList<IPointData> pollingCPointList = new ArrayList<IPointData>();

  private final PollingTask pollingTask = new PollingTask();
  private final RefreshTask refreshTask = new RefreshTask();

  private final RTUPoints rtuPoints;


  public VirtualPointDiagnostic(Frame owner, RTUPoints rtuPoints, Collection<VirtualPoint> selectionPoints) {
    super(owner);
    this.rtuPoints = Preconditions.checkNotNull(rtuPoints, "rtuPoints is null");
    if (selectionPoints != null) {
      this.selectionPoints.addAll(selectionPoints);
    }

    initComponents();
    
    pointTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(ListSelectionEvent e) {
        setSelectedRow(pointTable.getSelectedRow());
      }
    });
    
    setSelectedRow(-1);
    btnRefresh.setEnabled(!toggleBtnPolling.isSelected());

    Helper.register(btnHelp, getClass());
    btnHelp.setIcon(Helper.getIcon());
  }

  @org.jdesktop.application.Action
  public void addPoints() {
    VirtualPointSelector selector = new VirtualPointSelector(this, null, selectionPoints);
    selector.setSelectionMode(SelectionMode.SELECTION_MODE_MULTI);
    selector.showDialog();

    Collection<VirtualPoint> selections = selector.getSelectedPoints();
    for (VirtualPoint p : selections) {
      List<VirtualPointData> pdataList = VirtualPointDataFactory.createPointData(p);

      for (VirtualPointData pdata : pdataList) {
        VirtualPointType pointType = (VirtualPointType)pdata.getType();
        if (pointType.isAnalogue()) {
          pollingAPointList.add(pdata);
        } else if (pointType.isDigital()) {
          pollingDPointList.add(pdata);
        } else if (pointType.isCounter()) {
          pollingCPointList.add(pdata);
        } else {
          log.error("Unsupported type:" + pointType);
        }
      }

      // Put new point data into table model
      pollingPointListModel.addAll(pdataList);
    }

  }

  /**
   * Deletes the selected points.
   */
  @org.jdesktop.application.Action
  public void deletePoints() {

    int[] selectedRows = pointTable.getSelectedRows();

    ArrayList<IPointData> removeList = new ArrayList<IPointData>();

    int selectedRowInModel;
    for (int j = 0; j < selectedRows.length; j++) {
      selectedRowInModel = pointTable.convertRowIndexToModel(selectedRows[j]);
      removeList.add(pollingPointListModel.get(selectedRowInModel));
    }

    pollingPointListModel.removeAll(removeList);
    pollingDPointList.removeAll(removeList);
    pollingAPointList.removeAll(removeList);
    pollingCPointList.removeAll(removeList);
    
    setSelectedRow(pointTable.getSelectedRow());
  }

  @org.jdesktop.application.Action
  public void deleteAllPoints() {
    if(pollingPointListModel.isEmpty())
      return; 
    
    if(MessageDialogs.confirmRemoveAll(this, "points")) {
      pollingPointListModel.clear();
      pollingDPointList.clear();
      pollingAPointList.clear();
      pollingCPointList.clear();
      setSelectedRow(-1);
    }
  }

  @org.jdesktop.application.Action
  public void refreshValue() {
    refreshTask.execute();
  }

  @org.jdesktop.application.Action
  public void clearValue() {
    for (IPointData pdata : pollingPointListModel) {
      pdata.clear();
    }

    pointTable.repaint();
  }

  @org.jdesktop.application.Action
  public void close() {
    dispose();
  }
  
  private void setSelectedRow(int row) {
    btnDel.setEnabled(row >= 0); // update enablement
  }

  private void createUIComponents() {
    JXTable table = new JXTable(new RTVirtualPointsTableModel(pollingPointListModel));
    table.setColumnControlVisible(true);
    table.setHorizontalScrollEnabled(true);
    table.setDefaultRenderer(Boolean.class, BooleanCellRenderer.create());
    table.setDefaultRenderer(Date.class, new RTUTimeRenderer());
    table.setHighlighters(new EnabledHighlighter(new RowEnablePredicator()));
    table.getColumn(RTVirtualPointsTableModel.COLUMN_ONLINE).setCellRenderer(BooleanCellRenderer.createOnline());
    table.setSortOrderCycle(SortOrder.values());

    pointTable = table;

    ApplicationActionMap actions = Application.getInstance().getContext().getActionMap(this);
    btnAdd = new JButton(actions.get(ACTION_KEY_ADD));
    btnClear = new JButton(actions.get(ACTION_KEY_CLEAR_VALUE));
    btnDel = new JButton(actions.get(ACTION_KEY_DELETE));
    btnDelAll = new JButton(actions.get(ACTION_KEY_DELETE_ALL));
    btnRefresh = new JButton(actions.get(ACTION_KEY_REFRESH_VALUE));
    btnClose = new JButton(actions.get(ACTION_KEY_CLOSE));
    btnHelp = new JButton();
  }
  
  /**
   * Predicator for highlighting invalid rows of channel data table.
   */
  private static class RowEnablePredicator implements HighlightPredicate {

    @Override
    public boolean isHighlighted(Component renderer,
        ComponentAdapter adapter) {
      return (Boolean) adapter.getValue(RTVirtualPointsTableModel.COLUMN_ONLINE) != Boolean.TRUE;
    }
  }

  private static class RTUTimeRenderer extends DefaultTableRenderer {
    public RTUTimeRenderer() {
      super(new StringValue() {
        @Override
        public String getString(Object value) {
          return value == null ? null : RTUTimeDecoder.formatPoint((Date) value);
        }
      });
    }
  }
  
  private void thisWindowClosed(WindowEvent e) {
    if (pollingTask != null) {
      pollingTask.stop();
    }

    log.info("Exit Virtual Point Diagnostic");
  }

  private void toggleBtnPollingStateChanged(ChangeEvent e) {
    if (toggleBtnPolling.isSelected()) {
      // Get polling period
      int period = 500;
      Object value = spinnerPeriod.getValue();
      if (value != null && value instanceof Integer) {
        period = (Integer) value;
      }

      pollingTask.start(period);

    } else {
      pollingTask.stop();
    }
    
    btnRefresh.setEnabled(!toggleBtnPolling.isSelected());
  }

  private void spinnerPeriodStateChanged(ChangeEvent e) {
    Object value = spinnerPeriod.getValue();
    if (value != null && value instanceof Integer && pollingTask != null) {
      pollingTask.setPeriod((Integer) value);
    }

  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    contentPanel = new JPanel();
    tablePanel = new JPanel();
    scrollPane1 = new JScrollPane();
    toolBar1 = new JToolBar();
    actionPanel = new JPanel();
    toggleBtnPolling = new JCheckBox();
    spinnerPeriod = new JSpinner();
    JLabel label1 = new JLabel();
    buttonBar = new JPanel();

    //======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Diagnostic - Observe Virtual Points Value");
    addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosed(WindowEvent e) {
        thisWindowClosed(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(Borders.DIALOG_BORDER);
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new BorderLayout());

        //======== tablePanel ========
        {
          tablePanel.setBorder(new TitledBorder("Virtual Point List"));
          tablePanel.setLayout(new BorderLayout());

          //======== scrollPane1 ========
          {
            scrollPane1.setBorder(null);
            scrollPane1.setViewportView(pointTable);
          }
          tablePanel.add(scrollPane1, BorderLayout.CENTER);

          //======== toolBar1 ========
          {
            toolBar1.setFloatable(false);
            toolBar1.add(btnAdd);
            toolBar1.add(btnDel);
            toolBar1.add(btnDelAll);
          }
          tablePanel.add(toolBar1, BorderLayout.NORTH);
        }
        contentPanel.add(tablePanel, BorderLayout.CENTER);

        //======== actionPanel ========
        {
          actionPanel.setBorder(new TitledBorder("Actions"));
          actionPanel.setLayout(new FormLayout(
              "default, $ugap, default, $lcgap, default:grow, $ugap, default, $rgap, default, $lcgap, default:grow",
              "fill:default"));
          ((FormLayout) actionPanel.getLayout()).setColumnGroups(new int[][] { { 1, 3 } });

          //---- btnRefresh ----
          btnRefresh.setText("Refresh");
          actionPanel.add(btnRefresh, CC.xy(1, 1));

          //---- btnClear ----
          btnClear.setPreferredSize(new Dimension(60, 21));
          btnClear.setText("Clear");
          actionPanel.add(btnClear, CC.xy(3, 1));

          //---- toggleBtnPolling ----
          toggleBtnPolling.setText(" Polling points every ");
          toggleBtnPolling.setSelected(true);
          toggleBtnPolling.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
              toggleBtnPollingStateChanged(e);
            }
          });
          actionPanel.add(toggleBtnPolling, CC.xy(7, 1));

          //---- spinnerPeriod ----
          spinnerPeriod.setRequestFocusEnabled(false);
          spinnerPeriod.setModel(new SpinnerNumberModel(500, 200, 100000, 50));
          spinnerPeriod.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
              spinnerPeriodStateChanged(e);
            }
          });
          actionPanel.add(spinnerPeriod, CC.xy(9, 1));

          //---- label1 ----
          label1.setText("ms");
          actionPanel.add(label1, CC.xy(11, 1));
        }
        contentPanel.add(actionPanel, BorderLayout.SOUTH);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);

      //======== buttonBar ========
      {
        buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
        buttonBar.setLayout(new FormLayout(
            "$lcgap, default, $glue, $button",
            "pref"));

        //---- btnHelp ----
        btnHelp.setText("Help");
        buttonBar.add(btnHelp, CC.xy(2, 1));

        //---- btnClose ----
        btnClose.setText("Close");
        buttonBar.add(btnClose, CC.xy(4, 1));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    // ====== Custom Code ======
    SpinnerWheelSupport.installMouseWheelSupport(spinnerPeriod);
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JPanel tablePanel;
  private JScrollPane scrollPane1;
  private JTable pointTable;
  private JToolBar toolBar1;
  private JButton btnAdd;
  private JButton btnDel;
  private JButton btnDelAll;
  private JPanel actionPanel;
  private JButton btnRefresh;
  private JButton btnClear;
  private JCheckBox toggleBtnPolling;
  private JSpinner spinnerPeriod;
  private JPanel buttonBar;
  private JButton btnHelp;
  private JButton btnClose;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  // Get point value from RTU and update table's content.
  private void refreshPointTable() {
    try {
      if (!pollingDPointList.isEmpty()) {
        rtuPoints.cmdGetPointStatus(new ArrayList<IPointData>(pollingDPointList), PollingMode.DIGITAL);
      }
      if (!pollingAPointList.isEmpty()) {
        rtuPoints.cmdGetPointStatus(new ArrayList<IPointData>(pollingAPointList), PollingMode.ANALOG);
      }
      if (!pollingCPointList.isEmpty()) {
        rtuPoints.cmdGetPointStatus(new ArrayList<IPointData>(pollingCPointList), PollingMode.COUNTER);
      }
      pointTable.repaint();
      log.debug("Points value refreshed");
    } catch (Exception e) {
      e.printStackTrace();
      log.error("Fail to get point status:" + e.getMessage());
    }
  }


  private class PollingTask implements Runnable {

    private volatile Thread pollingThread;

    private int period = 500;


    public void start(int period) {
      setPeriod(period);
      if (pollingThread == null || !pollingThread.isAlive()) {
        pollingThread = new Thread(this, "Polling Points Thread");
        pollingThread.setDaemon(true);
        pollingThread.start();
      }
    }

    public void stop() {
      pollingThread = null;
    }

    public void setPeriod(int period) {
      if (period > 0) {
        this.period = period;
      }
    }

    @Override
    public void run() {
      log.debug("Start polling points value");

      Thread thisThread = Thread.currentThread();

      while (pollingThread == thisThread) {
        refreshPointTable();

        // Delay
        try {
          Thread.sleep(period);
        } catch (InterruptedException e) {
          // Do nothing
        }
      }

      log.debug("Stop polling points value");
    }
  }

  private class RefreshTask implements Runnable {

    private Thread refreshThread;


    @Override
    public void run() {
      refreshPointTable();
    }

    public void execute() {
      if (refreshThread == null || refreshThread.isAlive() == false) {
        Thread refreshThread = new Thread(this, "Polling Points Thread");
        refreshThread.setDaemon(true);
        refreshThread.start();
      }
    }
  }
}
