/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.diagnostic.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListModel;
import javax.swing.SortOrder;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.EnabledHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.FormatStringValue;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.ext.swing.renderer.BooleanCellRenderer;
import com.lucy.g3.gui.common.widgets.ext.swing.spinner.SpinnerWheelSupport;
import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel.SelectionMode;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.comms.service.realtime.points.ChannelData;
import com.lucy.g3.rtu.comms.service.realtime.points.RTUPoints;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.rtu.config.virtualpointsource.ui.VirtualPointSourceSelector;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol;

/**
 * A dialog for diagnosing channels' raw value.
 */
public class ChannelDiagnostic extends JDialog {

  public static final String ACTION_KEY_ADD = "addChannels";
  public static final String ACTION_KEY_DELETE = "deleteChannels";
  public static final String ACTION_KEY_DELETE_ALL = "deleteAllChannels";
  public static final String ACTION_KEY_REFRESH_VALUE = "refreshValue";
  public static final String ACTION_KEY_CLEAR_VALUE = "clearValue";
  public static final String ACTION_KEY_CLOSE = "close";

  private Logger log = Logger.getLogger(ChannelDiagnostic.class);

  private final ArrayListModel<ChannelData> channelDataList = new ArrayListModel<ChannelData>();

  private final RTUPoints rtuPoints;

  private final PollingTask pollingTask = new PollingTask();
  private final RefreshTask refreshTask = new RefreshTask();

  private final Collection<IChannel> channelList;


  public ChannelDiagnostic(Frame owner, RTUPoints rtuPoints, Collection<IChannel> channelList) {
    super(owner);

    this.rtuPoints = Preconditions.checkNotNull(rtuPoints, "rtuPoints is null");
    this.channelList = channelList;
    init();
  }

  public ChannelDiagnostic(Dialog owner, RTUPoints rtuPoints, Collection<IChannel> channelList) {
    super(owner);

    this.channelList = Preconditions.checkNotNull(channelList, "channelList is null");
    this.rtuPoints = Preconditions.checkNotNull(rtuPoints, "rtuPoints is null");
    init();
  }

  private void init() {
    initComponents();
    initEventHandling();
    setSelectedRow(-1);

    Helper.register(btnHelp, getClass());
    btnHelp.setIcon(Helper.getIcon());

    polling(toggleBtnPolling.isSelected());
  }

  @org.jdesktop.application.Action
  public void addChannels() {

    VirtualPointSourceSelector selector = new VirtualPointSourceSelector(this, null,
        new ArrayList<IVirtualPointSource>(channelList), "Select channels for diagnostics");
    selector.setSelectionMode(SelectionMode.SELECTION_MODE_MULTI);
    selector.showDialog();
    Collection<IVirtualPointSource> chs = selector.getSelections();

    ArrayList<ChannelData> newChData = new ArrayList<ChannelData>();
    for (IVirtualPointSource ch : chs) {
      if (ch != null) {
        ChannelData chData = null;
        try {
          chData = createChannelData((IChannel) ch);
          if (!channelDataList.contains(chData)) {
            newChData.add(chData);
          } else {
            log.warn(String.format("Cannot add channel data (Type:ID  %s:5s)"
                + " cause it already exists",
                chData.channelType, chData.channelID));
          }
        } catch (IllegalArgumentException e) {
          log.warn(e.getMessage());
        }
      }
    }

    // Put new channel data into table model
    channelDataList.addAll(newChData);
  }

  @org.jdesktop.application.Action
  public void deleteChannels() {
    channelDataList.removeAll(getSelections());
    setSelectedRow(channelTable.getSelectedRow() );
  }
  
  private ArrayList<ChannelData> getSelections(){
    int[] selectedRows = channelTable.getSelectedRows();
    ArrayList<ChannelData> selections = new ArrayList<ChannelData>();
    int selectedRowInModel;
    for (int j = 0; j < selectedRows.length; j++) {
      selectedRowInModel = channelTable.convertRowIndexToModel(selectedRows[j]);
      selections.add(channelDataList.get(selectedRowInModel));
    }
    return selections;
  }
  
  @org.jdesktop.application.Action
  public void deleteAllChannels() {
    if(channelDataList.isEmpty())
      return; 
    
    if(MessageDialogs.confirmRemoveAll(this, "channels")) {
      channelDataList.clear();
      setSelectedRow(-1);
    }
  }

  @org.jdesktop.application.Action
  public void refreshValue() {
    refreshTask.execute();
  }

  @org.jdesktop.application.Action
  public void clearValue() {
    for (ChannelData chData : channelDataList) {
      chData.clearValue();
    }

    channelTable.repaint();
  }

  @org.jdesktop.application.Action
  public void close() {
    dispose();
  }

  private void createUIComponents() {
    @SuppressWarnings("unchecked")
    ListModel<ChannelData> lm = channelDataList;
    JXTable table = new JXTable(new ChannelDataTableModel(lm));
    table.setColumnControlVisible(true);

    table.setHighlighters(new EnabledHighlighter(new RowEnablePredicator()));
    
    table.setDefaultRenderer(Boolean.class, new BooleanCellRenderer());
    table.setDefaultRenderer(Integer.class, new DefaultTableRenderer(
            new FormatStringValue(NumberFormat.getIntegerInstance()), SwingConstants.CENTER));
    table.getColumn(ChannelDataTableModel.COLUMN_ONLINE).setCellRenderer(BooleanCellRenderer.createOnline());
    table.getColumn(ChannelDataTableModel.COLUMN_VALID).setCellRenderer(BooleanCellRenderer.createValid());
    table.setSortOrderCycle(SortOrder.values());

    // Hide columns that are not of interest
    int columnCount = table.getColumnCount();
    for (int i = columnCount - 1; i >= ChannelDataTableModel.COLUMN_VALID; i--) {
      table.getColumnExt(i).setVisible(false);
    }
    
    channelTable = table;

    ApplicationActionMap actions = Application.getInstance().getContext().getActionMap(this);
    btnAdd = new JButton(actions.get(ACTION_KEY_ADD));
    btnClear = new JButton(actions.get(ACTION_KEY_CLEAR_VALUE));
    btnDel = new JButton(actions.get(ACTION_KEY_DELETE));
    btnDelAll = new JButton(actions.get(ACTION_KEY_DELETE_ALL));
    btnRefresh = new JButton(actions.get(ACTION_KEY_REFRESH_VALUE));
    btnClose = new JButton(actions.get(ACTION_KEY_CLOSE));
    btnHelp = new JButton();
  }

  private void initEventHandling() {
    btnRefresh.setEnabled(!toggleBtnPolling.isSelected());
    toggleBtnPolling.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if (source == toggleBtnPolling) {
          polling(toggleBtnPolling.isSelected());
        }
        
        btnRefresh.setEnabled(!toggleBtnPolling.isSelected());
      }
    });


    channelTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(ListSelectionEvent e) {
        setSelectedRow(channelTable.getSelectedRow() );
      }
    });
  }

  private void setSelectedRow(int row) {
    btnDel.setEnabled(row >= 0); // update enablement
  }

  private void thisWindowClosed(WindowEvent e) {
    if (pollingTask != null) {
      pollingTask.stop();
    }
    log.info("Exit Channel Diagnostic");
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    buttonBar = new JPanel();
    contentPanel = new JPanel();
    tablePanel = new JPanel();
    toolBar1 = new JToolBar();
    scrollPane1 = new JScrollPane();
    actionPanel = new JPanel();
    toggleBtnPolling = new JCheckBox();
    spinnerPeriod = new JSpinner();
    JLabel label1 = new JLabel();

    //======== this ========
    setTitle("Diagnostic - Observe Channels Raw Value");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosed(WindowEvent e) {
        thisWindowClosed(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(Borders.DIALOG_BORDER);
      dialogPane.setLayout(new BorderLayout());

      //======== buttonBar ========
      {
        buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
        buttonBar.setLayout(new FormLayout(
            "$lcgap, default, $glue, $button",
            "pref"));

        //---- btnHelp ----
        btnHelp.setText("Help");
        btnHelp.setMaximumSize(new Dimension(75, 23));
        btnHelp.setMinimumSize(new Dimension(75, 23));
        btnHelp.setPreferredSize(new Dimension(75, 23));
        buttonBar.add(btnHelp, CC.xy(2, 1));

        //---- btnClose ----
        btnClose.setText("Close");
        buttonBar.add(btnClose, CC.xy(4, 1));
      }
      dialogPane.add(buttonBar, BorderLayout.PAGE_END);

      //======== contentPanel ========
      {
        contentPanel.setLayout(new BorderLayout());

        //======== tablePanel ========
        {
          tablePanel.setBorder(new TitledBorder("Channel List"));
          tablePanel.setLayout(new BorderLayout());

          //======== toolBar1 ========
          {
            toolBar1.setFloatable(false);
            toolBar1.setBorderPainted(false);
            toolBar1.add(btnAdd);
            toolBar1.add(btnDel);
            toolBar1.add(btnDelAll);
          }
          tablePanel.add(toolBar1, BorderLayout.NORTH);

          //======== scrollPane1 ========
          {
            scrollPane1.setBorder(null);

            //---- channelTable ----
            channelTable.setAutoCreateRowSorter(true);
            channelTable.setFillsViewportHeight(true);
            scrollPane1.setViewportView(channelTable);
          }
          tablePanel.add(scrollPane1, BorderLayout.CENTER);
        }
        contentPanel.add(tablePanel, BorderLayout.CENTER);

        //======== actionPanel ========
        {
          actionPanel.setBorder(new TitledBorder("Actions"));
          actionPanel.setLayout(new FormLayout(
              "default, $ugap, default, $lcgap, default:grow, $ugap, default, $rgap, default, $lcgap, default:grow",
              "fill:default"));
          ((FormLayout) actionPanel.getLayout()).setColumnGroups(new int[][] { { 1, 3 } });

          //---- btnRefresh ----
          btnRefresh.setText("Refresh");
          actionPanel.add(btnRefresh, CC.xy(1, 1));

          //---- btnClear ----
          btnClear.setPreferredSize(new Dimension(60, 21));
          btnClear.setText("Clear");
          actionPanel.add(btnClear, CC.xy(3, 1));

          //---- toggleBtnPolling ----
          toggleBtnPolling.setText("Polling channels every ");
          toggleBtnPolling.setSelected(true);
          actionPanel.add(toggleBtnPolling, CC.xy(7, 1));

          //---- spinnerPeriod ----
          spinnerPeriod.setRequestFocusEnabled(false);
          spinnerPeriod.setModel(new SpinnerNumberModel(500, 200, 100000, 50));
          actionPanel.add(spinnerPeriod, CC.xy(9, 1));

          //---- label1 ----
          label1.setText("ms");
          actionPanel.add(label1, CC.xy(11, 1));
        }
        contentPanel.add(actionPanel, BorderLayout.SOUTH);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    // ====== Custom Code ======
    SpinnerWheelSupport.installMouseWheelSupport(spinnerPeriod);
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel buttonBar;
  private JButton btnHelp;
  private JButton btnClose;
  private JPanel contentPanel;
  private JPanel tablePanel;
  private JToolBar toolBar1;
  private JButton btnAdd;
  private JButton btnDel;
  private JButton btnDelAll;
  private JScrollPane scrollPane1;
  private JTable channelTable;
  private JPanel actionPanel;
  private JButton btnRefresh;
  private JButton btnClear;
  private JCheckBox toggleBtnPolling;
  private JSpinner spinnerPeriod;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  // ================== Action Perform ===================

  private void polling(boolean enable) {
    if (enable) {
      // Get polling period
      int period = 500;
      Object value = spinnerPeriod.getValue();
      if (value != null && value instanceof Integer) {
        period = (Integer) value;
      }

      pollingTask.start(period);

    } else {
      pollingTask.stop();
    }
  }


  /*
   * Channel data table model.
   */
  private static class ChannelDataTableModel extends AbstractTableAdapter<ChannelData> {

    static final byte COLUMN_ONLINE = 5;
    static final byte COLUMN_VALID = 6;
    
    // Channel table column names
    private static String[] ColumnNames = { "Channel ID", "Type", "Value", "Unit", "Description", 
      "Online" ,
      "Valid",
      "Alarm"  ,
      "Restart",
      "Out Of Range",
      "Filtered"  ,
      "Simulated" ,  
    };


    ChannelDataTableModel(ListModel<ChannelData> lm) {
      super(lm, ColumnNames);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      ChannelData chData = getRow(rowIndex);
      if (chData == null) {
        return null;
      }

      switch (columnIndex) {
      case 0: // Column "ID"
        return chData.channelID;
      case 1: // Column "Type"
        return chData.getTypeString();
      case 2: // Column "Value"
        return chData.getValue();
      case 3: // Column "Unit"
        return chData.getUnit();
      case 4: // Column "Description"
        return chData.getDescription();
      case 5:
        return chData.isOnline();
      case COLUMN_VALID: // Column "Valid"
        return chData.isValid();
      case 7:
        return chData.isAlarm();
      case 8:
        return chData.isRestart();
      case 9:
        return chData.isOutOfRange();
      case 10:
        return chData.isFiltered();
      case 11:
        return chData.isSimulated();
      default:// Invalid Column
        break;
      }

      return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      if (columnIndex == 0) {
        return Integer.class;
      }
      
      if (columnIndex >= COLUMN_ONLINE)
        return Boolean.class;

      return super.getColumnClass(columnIndex);
    }
  }


  // Get channel value from RTU and update table content.
  private void updateAllChannels() {
    try {
      if (channelDataList.isEmpty() == false) {
        rtuPoints.cmdPollingChannelValue(channelDataList);

        channelTable.repaint();// Thread safe repaint

        log.debug("Channel value refreshed");
      }

    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }


  /**
   * Predicator for highlighting invalid rows of channel data table.
   */
  private static class RowEnablePredicator implements HighlightPredicate {

    @Override
    public boolean isHighlighted(Component renderer,
        ComponentAdapter adapter) {
      return (Boolean) adapter.getValue(ChannelDataTableModel.COLUMN_ONLINE) != Boolean.TRUE
          || (Boolean) adapter.getValue(ChannelDataTableModel.COLUMN_VALID) != Boolean.TRUE;
    }
  }

  private class RefreshTask implements Runnable {

    private Thread refreshThread;


    @Override
    public void run() {
      updateAllChannels();
    }

    public void execute() {
      if (refreshThread == null || refreshThread.isAlive() == false) {
        Thread refreshThread = new Thread(this, "Polling Channel Thread");
        refreshThread.setDaemon(true);
        refreshThread.start();
      } else {
        log.warn("Refreshing in progress!");
      }
    }
  }

  private class PollingTask implements Runnable {

    private volatile Thread pollingThread;

    private int period = 500;


    public void start(int period) {
      setPeriod(period);
      if (pollingThread == null || !pollingThread.isAlive()) {
        pollingThread = new Thread(this, "Polling Channel Thread");
        pollingThread.setDaemon(true);
        pollingThread.start();
      }
    }

    public void stop() {
      pollingThread = null;
    }

    public void setPeriod(int period) {
      if (period > 0) {
        this.period = period;
      }
    }

    @Override
    public void run() {
      log.debug("Start polling channel value");

      Thread thisThread = Thread.currentThread();

      while (pollingThread == thisThread) {
        updateAllChannels();

        // Delay
        try {
          Thread.sleep(period);
        } catch (InterruptedException e) {
        }
      }

      log.debug("Stop polling channel value");
    }
  }


  private ChannelData createChannelData(IChannel channel) {
    Preconditions.checkState(channel.getType() == ChannelType.ANALOG_INPUT
        || channel.getType() == ChannelType.DIGITAL_INPUT,
        "Invalid channel type: must be input channel");

    Module m = channel.getOwnerModule();

    byte moduleType = (byte) m.getType().getValue();
    byte moduleID = (byte) m.getId().getValue();
    byte channelType;
    if (channel.getType() == ChannelType.ANALOG_INPUT) {
      channelType = G3ConfigProtocol.ANALOGUE_INPUT_CHANNEL;
    } else if (channel.getType() == ChannelType.DIGITAL_INPUT) {
      channelType = G3ConfigProtocol.DIGITAL_INPUT_CHANNEL;
    } else {
      throw new IllegalArgumentException("Unsupport channel type:" + channel.getType());
    }

    byte channelID = (byte) channel.getId();
    String description = m.getShortName() + " - " + channel.getDescription();
    String unit = channel.predefined().getRawUnit();

    return new ChannelData(moduleType, moduleID, channelType, channelID, description, unit);
  }
  // public static void main(String[] args) {
  // Logger.getRootLogger().setLevel(Level.INFO);
  // new ChannelDiagnostic((Frame)null).setVisible(true);
  // }
}
