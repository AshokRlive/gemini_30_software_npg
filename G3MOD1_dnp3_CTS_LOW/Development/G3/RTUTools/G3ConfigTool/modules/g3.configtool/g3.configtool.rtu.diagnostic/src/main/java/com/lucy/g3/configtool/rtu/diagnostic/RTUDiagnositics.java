/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.diagnostic;

import java.util.Collection;

import org.jdesktop.application.Action;
import org.jdesktop.application.Application;

import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.rtu.diagnostic.ui.ChannelDiagnostic;
import com.lucy.g3.configtool.rtu.diagnostic.ui.VirtualPointDiagnostic;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.iomodule.manager.util.ChannelUtility;
import com.lucy.g3.rtu.config.shared.manager.ConfigManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointUtility;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;


/**
 *
 */
public class RTUDiagnositics extends AbstractSubsystem {
  public final static String SUBSYSTEM_ID = "subystem.id.RTUDiagnositics";
  
  public static final String ACTION_DIAGNOSTIC_CHANNEL = "channelDiagnostic";
  public static final String ACTION_DIAGNOSTIC_VIRTUALPOINT = "vpointDiagnostic";
  
  public RTUDiagnositics(IContext context) {
    super(context, SUBSYSTEM_ID);
  }
  
  @Action(enabledProperty = PROPERTY_NORMAL_MODE)
  public void channelDiagnostic() {
    Collection<IChannel> channelList = ChannelUtility.getAllChannels(
        getRTUConfig(),
        ChannelType.ANALOG_INPUT,
        ChannelType.DIGITAL_INPUT); 
    
    new ChannelDiagnostic(getMainFrame(), getRTU().getComms().getRtuPoints(), channelList)
        .setVisible(true);
  }

  @Action(enabledProperty = PROPERTY_NORMAL_MODE)
  public void vpointDiagnostic() {
    Collection<VirtualPoint> points = null;
  
    points = VirtualPointUtility.getAllPoints(getRTUConfig());
  
    new VirtualPointDiagnostic(getMainFrame(), getRTU().getComms().getRtuPoints(), points)
        .setVisible(true);
  }

  private G3RTU getRTU() {
    return G3RTUFactory.getDefault();
  }

  private IConfig getRTUConfig() {
    return ConfigManager.getInstance().getRemoteConfig();
  }

  @Override
  protected void notifyStateChanged(ConnectionState oldState, ConnectionState newState) {
    
  }

}

