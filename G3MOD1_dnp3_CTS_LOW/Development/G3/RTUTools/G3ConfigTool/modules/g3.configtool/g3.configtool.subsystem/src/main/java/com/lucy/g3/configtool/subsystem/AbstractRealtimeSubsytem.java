/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsystem;

import java.awt.Frame;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.JDialog;
import javax.swing.JFrame;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

import com.lucy.g3.common.context.IContext;

public abstract class AbstractRealtimeSubsytem extends AbstractSubsystem {
  
  private Logger log = Logger.getLogger(AbstractRealtimeSubsytem.class);

  private final WindowListener windowClosingListener = new RealtimeWindowCloseListener();
  private final HashMap<String, AbstractRealtimeWindow> realtimeWindows = new HashMap<String, AbstractRealtimeWindow>();

  public AbstractRealtimeSubsytem(IContext context, String SUBSYSTEM_ID) {
    super(context, SUBSYSTEM_ID);
  }

  protected void registerRealtimeWindow(String windowID, AbstractRealtimeWindow window) {
    if(realtimeWindows.containsKey(windowID))
      throw new IllegalArgumentException("The realtime window ID already exists: " + windowID);
    
    if(window == null)
      throw new NullPointerException("The window must not be null.");
    
    if(windowID == null)
      throw new NullPointerException("The windowID must not be null.");
    
    window.addWindowListener(windowClosingListener);
    realtimeWindows.put(windowID, window);
  }
  
  protected void degisterRealtimeWindow(String windowID) {
    AbstractRealtimeWindow window = realtimeWindows.get(windowID);
    if(window != null)
      window.removeWindowListener(windowClosingListener);
    
    realtimeWindows.remove(windowID);
  }
  
  @Override
  protected void notifyStateChanged(ConnectionState oldState,
      ConnectionState newState) {

    if (newState == ConnectionState.DISCONNECTED) {
       closeAllMonitorWindow();

    } else {
      // Notify all testWindow state changed
      Collection<AbstractRealtimeWindow> allwindows = realtimeWindows.values();
      for (AbstractRealtimeWindow w : allwindows) {
        w.notifyConnectionStateChanged(oldState, newState);
      }
    }
  }
  
  protected AbstractRealtimeWindow getWindow(String id) {
    return realtimeWindows.get(id);
  }

  private void closeAllMonitorWindow() { 
    Collection<AbstractRealtimeWindow> allwindows = realtimeWindows.values();
    for (AbstractRealtimeWindow w : allwindows) {
      w.close();
    }
  }
  
  protected abstract void realtimeWindowClosed(AbstractRealtimeWindow closedWindow);
  
  private class RealtimeWindowCloseListener extends WindowAdapter {

    @Override
    public void windowClosed(WindowEvent e) {
      Window window = (Window) e.getSource();
      log.info("Window " + window.getName() + " is closed.");

      realtimeWindowClosed((AbstractRealtimeWindow)window);
      
      // window.removeWindowListener(testWindowListener);
      // RTUTestWindow testwindow = (RTUTestWindow)window;
      // testWindows.remove(testwindow.key);
    }

  }
}
