
package com.lucy.g3.configtool.subsystem;

import com.lucy.g3.common.context.IComponent;

/**
 * ConfigTool sub module.
 */
public interface ISubsystem extends IComponent {
  @Override
  ISubsystemContext getContext();
}

