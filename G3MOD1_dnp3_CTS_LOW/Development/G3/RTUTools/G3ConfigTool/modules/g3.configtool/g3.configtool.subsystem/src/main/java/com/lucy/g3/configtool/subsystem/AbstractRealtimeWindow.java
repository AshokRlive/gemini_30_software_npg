/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsystem;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;


/**
 * This abstract class should be inherited by the window that communicates with
 * RTU.
 * <p>
 * The purpose is to ensure the communication will be stopped always when user
 * closes the window. It requires {@linkplain #stopCommunication()} to be
 * implemented, which will be called when this window is closing.
 * </p>
 */
public abstract class AbstractRealtimeWindow extends JFrame {

  protected final Application application;
  protected final String id;


  /**
   * Default constructor.
   *
   * @param app
   *          current application.
   * @param id unique of this window.
   */
  public AbstractRealtimeWindow(Application app, String id) {
    if(app == null)
      throw new NullPointerException("app must not be null");
    this.application = app;
    this.id = id;

    // Set Frame icon
    ImageIcon frameIcon = app.getContext().getResourceMap().getImageIcon("Application.icon");
    if (frameIcon != null) {
      setIconImage(frameIcon.getImage());
    }

    // Handle window closing
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
  }

  public String getId() {
    return id;
  }

  public void close() {
    dispose();
  }

  protected final ApplicationActionMap getActionMap() {
    return application.getContext().getActionMap(this);
  }

  protected final Application getApplication() {
    return application;
  }

  public abstract void notifyConnectionStateChanged(ConnectionState oldState,
      ConnectionState newState);
}