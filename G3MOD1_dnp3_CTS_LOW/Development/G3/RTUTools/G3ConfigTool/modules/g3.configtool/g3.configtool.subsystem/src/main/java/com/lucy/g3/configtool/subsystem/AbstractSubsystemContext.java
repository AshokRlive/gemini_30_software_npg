
package com.lucy.g3.configtool.subsystem;

import com.lucy.g3.common.context.AbstractContext;

public abstract class AbstractSubsystemContext extends AbstractContext
    implements ISubsystemContext {

  protected void setSubystemState(ISubsystem subsystem,
      ConnectionState oldState, ConnectionState newState) {
    ((AbstractSubsystem) subsystem).setConnectionState(oldState, newState);
  }
}
