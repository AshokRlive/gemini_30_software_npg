/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsystem;

import java.awt.Frame;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.TaskService;

import com.lucy.g3.common.context.AbstractComponent;
import com.lucy.g3.common.context.IContext;

/**
 * Configuration Tool sub system.
 */
public abstract class AbstractSubsystem extends AbstractComponent implements ISubsystem {
  
  public static final String PROPERTY_NORMAL_MODE = "normalMode";
  public static final String PROPERTY_UPGRADE_MODE = "upgradeMode";
  public static final String PROPERTY_CONNECTED = "connected";
  public static final String PROPERTY_CONNECTION_STATE = "connectionState";


  
  private Application app;
  private JFrame mainFrame;
  
  private ConnectionState state;
  
  // TODO CTContext
  public AbstractSubsystem(IContext context, String uniqueSubsystemID) {
    super(context, uniqueSubsystemID);
  }
  
  @Override
  public ISubsystemContext getContext(){
    return (ISubsystemContext) super.getContext();
  }
  
  /**
   * Get current connection state.
   */
  public ConnectionState getConnectionState() {
    return state;
  }

  void setConnectionState(ConnectionState oldState, ConnectionState newState) {
    this.state = newState;
    
    firePropertyChange(PROPERTY_CONNECTED,    ConnectionState.isConnected(oldState),   ConnectionState.isConnected(newState)  );  
    firePropertyChange(PROPERTY_NORMAL_MODE,  ConnectionState.isNormalMode(oldState),  ConnectionState.isNormalMode(newState) ); 
    firePropertyChange(PROPERTY_UPGRADE_MODE, ConnectionState.isUpgradeMode(oldState), ConnectionState.isUpgradeMode(newState));
    firePropertyChange(PROPERTY_CONNECTION_STATE, oldState, newState);
    
    notifyStateChanged(oldState, newState);
  }
  /**
   * Checks if it is connected to RTU.
   * <p>
   * Getter method for the property {@linkplain PROPERTY_CONNECTED}.
   * </p>
   */
  public final boolean isConnected() {
    return ConnectionState.isConnected(getConnectionState());
  }

  /**
   * Checks if it is disconnected state.
   */
  public final boolean isDisconnected() {
    return getConnectionState() == ConnectionState.DISCONNECTED;
  }

  /**
   * Checks if the connected RTU is running in normal mode.
   * <p>
   * Getter method for the property {@linkplain PROPERTY_NORMAL_MODE}.
   * </p>
   */
  public final boolean isNormalMode() {
    return ConnectionState.isNormalMode(getConnectionState());
  }

  /**
   * Checks if the connected RTU is running in upgrade mode.
   * <p>
   * Getter method for the property {@linkplain PROPERTY_UPGRADE_MODE}.
   * </p>
   */
  public final boolean isUpgradeMode() {
    return ConnectionState.isUpgradeMode(getConnectionState());
  }

  /**
   * This method will be called when connection state changes in order to inform
   * this subsystem that the state has changed and some action should be
   * performed if necessary.
   */
  protected abstract void notifyStateChanged(ConnectionState oldState, ConnectionState newState);
  
  public Application getApp() {
    if(app == null)
      app = Application.getInstance();
    return app;
  }

  public String getSubsystemID() { 
    return getUniqueIdentifier();
  }
  
  /**
   * Checks the current connection state and shows a connection setup dialog if
   * is now disconnected.
   */
  protected boolean checkIsConnected() {
    if (isDisconnected()) {
      showConnectDialog();
      return false;
    } else {
      return true;
    }
  }

  private void showConnectDialog() {
//    String[] options = { "Connect", "Cancel" };
//    int rtn = JOptionPane.showOptionDialog(getMainFrame(),
//        "<html><p><font size=3><b>No device is connected</b></font></p> <br>" 
//            + "<p>Would you like to connect to a device?</p><br></html>",
//        "Disconnected", JOptionPane.OK_CANCEL_OPTION,
//        JOptionPane.ERROR_MESSAGE,
//        null, options, options[1]);
//
//    if (rtn == JOptionPane.OK_OPTION) {
//      getStateHolder().connect();
//    }
    
    JOptionPane.showMessageDialog(getMainFrame(),
      "<html><p><font size=3><b>No RTU is connected</b></font></p> <br>" 
          + "<p>Please connect to a RTU first!</p><br></html>",
      "Disconnected", JOptionPane.ERROR_MESSAGE);
  }
  
  
  public TaskService getCommsTaskService() {
    return getApp().getContext().getTaskService(IContext.COMMS_TASK_SERVICE_NAME);
  }

  public TaskService getDefaultTaskService() {
    return getApp().getContext().getTaskService();
  }

  protected JFrame getMainFrame() {
    if(mainFrame == null) {
      Application application = getApp();
      if (application instanceof SingleFrameApplication) {
        mainFrame = ((SingleFrameApplication) application).getMainFrame();
      }
    }
    return mainFrame;
  }

  /**
   * Checks if the application is running in normal mode and shows error message
   * if it is not.
   */
  protected final boolean checkIsNormalMode() {
    if (isNormalMode() == false) {
      JOptionPane.showMessageDialog(getMainFrame(),
          "Please make sure Configuration Tool is connected to RTU in Nomral mode!",
          "Error",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }
  
  /**
   * Gets an action instance with the given action key.
   *
   * @param actionKey
   *          the key of the action, usually defined as "ACTION_KEY_***".
   * @return the action instance. <code>Null</code> if the action is not found
   *         in this object.
   */
  public final javax.swing.Action getAction(String actionKey) {
    return getApp().getContext().getActionMap(this).get(actionKey);
  }

  public final ApplicationActionMap getActionMap() {
    return getApp().getContext().getActionMap(this);
  }

  
  /**
   * Help method for showing a frame window.
   */
  protected void showWindow(JFrame window) {
    window.setExtendedState(Frame.NORMAL);
    //window.setLocationRelativeTo(getMainFrame());
    window.setVisible(true);
  }

  /**
   * Help method for showing a dialog window.
   */
  protected void showWindow(JDialog window) {
    Application app = Application.getInstance();
    if (app instanceof SingleFrameApplication) {
      ((SingleFrameApplication) app).show(window);
    } else {
      window.setVisible(true);
    }
  }
  
}
