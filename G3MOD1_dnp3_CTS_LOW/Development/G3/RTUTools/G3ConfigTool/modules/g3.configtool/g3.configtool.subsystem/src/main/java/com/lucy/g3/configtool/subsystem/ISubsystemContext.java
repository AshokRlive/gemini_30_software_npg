
package com.lucy.g3.configtool.subsystem;

import com.lucy.g3.common.context.IContext;

/**
 * ConfigTool context.
 */
public interface ISubsystemContext extends IContext {
  ConnectionState getState();
}

