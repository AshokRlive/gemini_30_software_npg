/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsystem;


/**
 * This enum list all connection states of ConfigTool.
 */
public enum ConnectionState {
  /** ConfigTool is well connected to MCM application. */
  CONNECTED_TO_G3_APP,
  /** ConfigTool is well connected to MCM updater. */
  CONNECTED_TO_G3_UPDATER,
  /** ConfigTool is well connected to MCM,but application type is not clear. */
  CONNECTED_TO_G3_UNKNOWN,
  /**
   * Communication was lost after connection has been setup. Under this state,
   * ConfigTool will always try to setup connection again. Once the
   * communication recovers, ConfigTool will go back to connected state.
   */
  CONNECTION_LOST,
  /** ConfigTool is set to disconnected, no communication activity. */
  DISCONNECTED;

  public static boolean isDisconnected(ConnectionState state) {
    return state == DISCONNECTED;
  }

  public static boolean isConnected(ConnectionState state) {
    return state == CONNECTED_TO_G3_APP || state == CONNECTED_TO_G3_UPDATER
        || state == CONNECTED_TO_G3_UNKNOWN;
  }

  public static boolean isNormalMode(ConnectionState state) {
    return state == CONNECTED_TO_G3_APP;
  }

  public boolean isNormalMode() {
    return isNormalMode(this);
  }

  public static boolean isUpgradeMode(ConnectionState state) {
    return state == CONNECTED_TO_G3_UPDATER;
  }

  public boolean isUpgradeMode() {
    return isUpgradeMode(this);
  }
  
  public static String convertToText(ConnectionState state) {
    String text;
    if (ConnectionState.isNormalMode(state)) {
      text = "Normal";
    } else if (ConnectionState.isUpgradeMode(state)) {
      text = "Upgrade Mode";
    } else if (state == ConnectionState.CONNECTED_TO_G3_UNKNOWN) {
      text = "Unknown Mode";
    } else {
      text = "";
    }
    return text;
  }
}
