/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.controller.task;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;

import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.RestartMode;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.rtu.control.CommissioningAPI;
import com.lucy.g3.rtu.manager.G3RTUFactory;

/**
 * A task sending commissioning command to RTU and shows success/fail dialog.
 */
public class RegisterModulesTask extends Task<Void, Void> {

  private JFrame parent;


  public RegisterModulesTask(Application app) {
    super(app);

    if (app != null && app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    }

    setUserCanCancel(false);
  }

  @Override
  protected void succeeded(Void result) {
    super.succeeded(result);
    message("successMessage");

    int rtn = JOptionPane.showOptionDialog(parent,
        "Restart the device to apply registration?\n",
        "Module Register",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE,
        null, new String[] { "Restart", "Restart later" }, "Restart");

    if (rtn == JOptionPane.YES_OPTION) {
      // Restart RTU 
      EventBroker.getInstance().send(EventTopics.EVT_TOPIC_RESTART_RTU, RestartMode.RESTART);
    }
  }

  @Override
  protected void failed(final Throwable cause) {
    super.failed(cause);
    JOptionPane.showMessageDialog(parent, "Module registration failed: " + cause.getMessage(),
        "Error", JOptionPane.ERROR_MESSAGE);
  }

  @Override
  protected Void doInBackground() throws Exception {
    CommissioningAPI api = CommsUtil.getCommissionAPI(G3RTUFactory.getComms());
    
    message("startMessage");
    setProgress(0);
    api.cmdRegisterModules();
    setProgress(100);

    return null;
  }
}
