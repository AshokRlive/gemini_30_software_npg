/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.controller.task;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jdesktop.application.Application;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.rtu.comms.service.realtime.status.IRTUStatusAPI;
import com.lucy.g3.rtu.comms.service.realtime.status.RTUStatus;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.OUTSERVICEREASON;

/**
 * Task for changing the state of RTU service.
 */
public class RTUSetDebugModeTask extends Task<Void, Void> {

  private final  RTUStatus rtuStatus;

  private JFrame parent = null;

  private boolean enabled;

  public RTUSetDebugModeTask(Application app, RTUStatus rtuStatus, boolean enabled) {

    super(app);
    this.rtuStatus = Preconditions.checkNotNull(rtuStatus, "rtuStatus is null");
    this.enabled = enabled;

    if (app != null && app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    }

    if (enabled) {
      setTitle("Enter RTU debug mode");
    } else {
      setTitle("Exit RTU debug mode");
    }
    
    setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, null, null));
  }

  @Override
  protected void succeeded(Void result) {
    if (enabled) {
      setMessage("RTU debug mode enabled");
    } else {
      setMessage("RTU debug mode disabled");
    }
  }

  @Override
  protected void failed(final Throwable cause) {
    MessageDialogs.error(parent, "Error occurred when settting debug mode!", cause);
  }

  @Override
  protected Void doInBackground() throws Exception {
    if(enabled)
    setMessage("Entering debug mode...");
    else
    setMessage("Exiting debug mode...");
    rtuStatus.cmdSetRTUDebugEnabled(enabled);
    rtuStatus.cmdGetRTUDebugState();
    return null;
  }

}
