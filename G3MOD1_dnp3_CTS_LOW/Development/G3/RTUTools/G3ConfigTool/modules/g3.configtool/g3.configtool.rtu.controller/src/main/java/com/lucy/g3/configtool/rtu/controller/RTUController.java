/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.controller;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;

import org.jdesktop.application.Action;
import org.jdesktop.application.Task;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.configtool.rtu.controller.datetime.ui.RTUDateTimeSettingDialog;
import com.lucy.g3.configtool.rtu.controller.ntp.ui.NTPSettingsDialog;
import com.lucy.g3.configtool.rtu.controller.task.RTUInfoReadTask;
import com.lucy.g3.configtool.rtu.controller.task.RTUSetDebugModeTask;
import com.lucy.g3.configtool.rtu.controller.task.RTUSetServiceTask;
import com.lucy.g3.configtool.rtu.controller.task.RegisterModulesTask;
import com.lucy.g3.configtool.rtu.controller.ui.RTUInfoDialogs;
import com.lucy.g3.configtool.subsys.permissions.PermissionCheckChecks;
import com.lucy.g3.configtool.subsys.permissions.UserActivities;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.comms.service.RTUCommsService;
import com.lucy.g3.rtu.comms.service.realtime.status.RTUStatus;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.OUTSERVICEREASON;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.SERVICEMODE;

/**
 * RTU controller manages actions that control RTU, e.g. as enable/disable RTU
 * service, restart RTU,etc. Each action instance can be accessed using method
 * {@linkplain #getAction(String)}.
 * <p>
 * All actions' "enabled" state are updated against the current application
 * state.
 * </p>
 */
public class RTUController extends AbstractSubsystem  {
  public final static String SUBSYSTEM_ID = "subystem.id.RTUController";
  
  // @formatter:off
  public static final String ACTION_KEY_REGISTER_MODULES     = "registerModules";
  public static final String ACTION_KEY_ENABLE_SERVICE       = "enableRtuService";
  public static final String ACTION_KEY_DISABLE_SERVICE      = "disableRtuService";
  public static final String ACTION_KEY_SET_TIME             = "setRtuTime";
  public static final String ACTION_KEY_CONFIG_NTP           = "configureNTP";
  public static final String ACTION_KEY_SHOW_NETINFO         = "showNetworkInfo";
  public static final String ACTION_KEY_ABOUT_RTU            = "aboutRTU";
  public static final String ACTION_KEY_REFRESH_INFO         = "refreshRtuInfo";
  public static final String ACTION_KEY_ENABLE_DEBUG         = "enableDebug";
  public static final String ACTION_KEY_DISABLE_DEBUG        = "disableDebug";
  // @formatter:on
  
  private static final String PROPERTY_OUT_OF_SERVICE = "outOfService";
  private static final String PROPERTY_IN_SERVICE = "inService";
  
  private static final String PROPERTY_DEBUG_ON  = "debugOn";
  private static final String PROPERTY_DEBUG_OFF = "debugOff";
  private static final String PROPERTY_DEBUG_ENABLED = "debugEnabled";

  private final RTUCommsService g3RTU;
  private SERVICEMODE serviceMode;

  private boolean rtuDebugEnabled;

  /**
   * Constructor.
   *
   * @param app
   *          current running application
   * @param getG3TaskService
   *          () task service for running controlling tasks.
   */
  public RTUController(IContext context, G3RTU g3RTU) {
    super(context, SUBSYSTEM_ID);
    Preconditions.checkNotNull(g3RTU, "g3RTU is null");
    this.g3RTU = g3RTU.getComms();

    final RTUStatus rtuStatus = this.g3RTU.getRtuStatus();
    setServiceMode(rtuStatus.getServiceMode());
    setDebugEnabled(rtuStatus.isAutoDebugging());
    rtuStatus.addPropertyChangeListener(RTUStatus.PROPERTY_SERVICE_MODE, new ServiceModePCL());
    rtuStatus.addPropertyChangeListener(RTUStatus.PROPERTY_AUTODEBUGGING, new IEC61499DebugEnableStatePCL());
  }



  // ================== Actions Methods ===================
  
  @Action(enabledProperty = PROPERTY_NORMAL_MODE)
  public void registerModules() {
    if (isNormalMode()) {
      Task<?, ?> task = new RegisterModulesTask(getApp());
      getCommsTaskService().execute(task);
    }
  }

  
  @Action(enabledProperty = PROPERTY_NORMAL_MODE)
  public void configureNTP(ActionEvent e) {
    new NTPSettingsDialog(getMainFrame()).setVisible(true);
  }

  
  @Action(enabledProperty = PROPERTY_NORMAL_MODE, selectedProperty = PROPERTY_OUT_OF_SERVICE)
  public void disableRtuService(ActionEvent e) {
    if (checkIsNormalMode() == false) {
      return;
    }

    JComboBox<OUTSERVICEREASON> reasonComp = new JComboBox<>(OUTSERVICEREASON.values());

    int option = JOptionPane.showConfirmDialog(
        getMainFrame(),
        new Object[] { "Reason: ", reasonComp },
        "Stop RTU Service",
        JOptionPane.OK_CANCEL_OPTION,
        JOptionPane.QUESTION_MESSAGE);

    if (option == JOptionPane.YES_OPTION) {
      OUTSERVICEREASON reason = (OUTSERVICEREASON) reasonComp.getSelectedItem();
      Task<?, ?> task = new RTUSetServiceTask(getApp(), g3RTU.getRtuStatus(), false, reason);
      getCommsTaskService().execute(task);
    }

    updateComponentState(e.getSource(), isOutOfService());
  }

  
  @Action(enabledProperty = PROPERTY_NORMAL_MODE, selectedProperty = PROPERTY_IN_SERVICE)
  public void enableRtuService(ActionEvent e) {
    if (checkIsNormalMode() == false) {
      return;
    }

    int ret = JOptionPane.showConfirmDialog(getMainFrame(), 
        "Do you want to start the service of RTU? ",
        "Start RTU Service", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
    if(ret != JOptionPane.YES_OPTION)
      return;
    
    
    // if(checkPermission(UserActivities.TEST_RTU) == false){
    // return ;
    // }

    Task<?, ?> task = new RTUSetServiceTask(getApp(), g3RTU.getRtuStatus(), true, null);
    getCommsTaskService().execute(task);

    updateComponentState(e.getSource(), isInService());
  }

  private void updateComponentState(Object source, boolean selected) {
    if (source instanceof JToggleButton) {
      ((JToggleButton) source).setSelected(selected);
    } else if (source instanceof JCheckBoxMenuItem) {
      ((JCheckBoxMenuItem) source).setSelected(selected);
    }
  }



  /**
   * Pop up a dialog which shows the versions of RTU software.
   */
  
  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void aboutRTU() {
    if (checkIsConnected()) {
      RTUInfoDialogs.showAboutRTU(getMainFrame(), g3RTU);
    }
  }

  
  @Action(enabledProperty = PROPERTY_NORMAL_MODE)
  public void showNetworkInfo() {
    if (checkIsNormalMode()) {
      RTUInfoDialogs.showNetworkInfo(getMainFrame(), g3RTU);
    }
  }

  /**
   * Executes a task to read all information from RTU.
   */
  
  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void refreshRtuInfo() {
    if (checkIsConnected() == false) {
      return;
    }

    getCommsTaskService().execute(new RTUInfoReadTask(getApp(), getConnectionState(), g3RTU));
  }

  
  @Action(enabledProperty = PROPERTY_NORMAL_MODE)
  public void setRtuTime() {
    if (checkIsNormalMode() == false) {
      return;
    }

    new RTUDateTimeSettingDialog(getApp(), getCommsTaskService(), g3RTU.getRtuStatus())
        .showDialog(getMainFrame());
  }
  
  @Override
  protected void notifyStateChanged(ConnectionState oldState, ConnectionState newState) {
    // Do nothing
  }

  /**
   * Property getter method for PROPERTY_OUT_OF_SERVICE.
   */
  public boolean isOutOfService() {
    return serviceMode == SERVICEMODE.SERVICEMODE_OUTSERVICE;
  }

  /**
   * Property getter method for PROPERTY_IN_SERVICE.
   */
  public boolean isInService() {
    return serviceMode == SERVICEMODE.SERVICEMODE_INSERVICE;
  }

  private void setServiceMode(SERVICEMODE mode) {
    Object oldValueInService = isInService();
    Object oldValueOutofService = isOutOfService();

    serviceMode = mode;
    firePropertyChange(PROPERTY_IN_SERVICE, oldValueInService, isInService());
    firePropertyChange(PROPERTY_OUT_OF_SERVICE, oldValueOutofService, isOutOfService());
  }


  private class ServiceModePCL implements PropertyChangeListener {
    
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      setServiceMode((SERVICEMODE) evt.getNewValue());
      
      // publish event
      HashMap<String, Object> properties = new HashMap<>();
      properties.put(EventTopics.EVT_PROPERTY_SERVICE_MODE,evt.getNewValue());
      properties.put(EventTopics.EVT_PROPERTY_OUTSERVICE_REASON,g3RTU.getRtuStatus().getOutServiceReason());
      EventBroker.getInstance().send(EventTopics.EVT_TOPIC_SERVICE_MODE_CHG, properties);
    }

  }
 
  

  @Action(enabledProperty = PROPERTY_DEBUG_OFF )
  public void enableDebug(ActionEvent e) {
    if(PermissionCheckChecks.checkPermitted(UserActivities.DEBUG_RTU)) {
      Task<?, ?> task = new RTUSetDebugModeTask(getApp(), g3RTU.getRtuStatus(), true);
      getCommsTaskService().execute(task);
      updateComponentState(e.getSource(), !isDebugEnabled());
    }
  }
  
  @Action(enabledProperty = PROPERTY_DEBUG_ON)
  public void disableDebug(ActionEvent e) {
    if(PermissionCheckChecks.checkPermitted(UserActivities.DEBUG_RTU)) {
      Task<?, ?> task = new RTUSetDebugModeTask(getApp(), g3RTU.getRtuStatus(), false);
      getCommsTaskService().execute(task);
      updateComponentState(e.getSource(), isDebugEnabled());
    }
  }
  
  public boolean isDebugOn(){
    return rtuDebugEnabled;
  }
  
  public boolean isDebugOff(){
    return !rtuDebugEnabled;
  }

  public boolean isDebugEnabled() {
    return rtuDebugEnabled;
  }
  
  private void setDebugEnabled(boolean enable) {
    Object oldValue = isDebugEnabled();
    rtuDebugEnabled = enable;
    firePropertyChange(PROPERTY_DEBUG_ENABLED, oldValue, rtuDebugEnabled);
    
    firePropertyChange(PROPERTY_DEBUG_ON, null, enable);
    firePropertyChange(PROPERTY_DEBUG_OFF , null, !enable);
  }

  private class IEC61499DebugEnableStatePCL implements PropertyChangeListener {
    
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      setDebugEnabled((boolean) evt.getNewValue());
      
      // publish event
      EventBroker.getInstance().send(EventTopics.EVT_TOPIC_DEBUG_MODE_CHG, evt.getNewValue());
    }
    
  }
}
