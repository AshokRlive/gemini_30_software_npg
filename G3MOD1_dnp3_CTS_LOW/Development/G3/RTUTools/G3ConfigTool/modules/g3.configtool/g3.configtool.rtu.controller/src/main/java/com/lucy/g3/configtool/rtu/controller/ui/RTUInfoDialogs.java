/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.controller.ui;

import static com.lucy.g3.gui.common.dialogs.InfoDialog.createProperties;

import java.awt.Frame;
import java.util.ArrayList;

import com.lucy.g3.common.utils.TimeUtils;
import com.lucy.g3.configtool.rtu.controller.RTUController;
import com.lucy.g3.gui.common.dialogs.InfoDialog;
import com.lucy.g3.gui.common.dialogs.InfoDialog.Properties;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.comms.service.RTUCommsService;
import com.lucy.g3.rtu.comms.service.realtime.status.RTUStatus;
import com.lucy.g3.rtu.comms.service.rtu.info.NetInfo;
import com.lucy.g3.rtu.comms.service.rtu.info.RTUInfo;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.SERVICEMODE;

/**
 * This class provides static API for showing RTU Information dialog.
 */
public class RTUInfoDialogs {

  public static void showAboutRTU(Frame parent, RTUCommsService rtu) {
    InfoDialog.show(parent,
        "About Gemini 3 RTU",
        Helper.getKey(RTUController.class),
        buildRTUInfoProperties(rtu));
  }

  public static void showNetworkInfo(Frame parent, RTUCommsService rtu) {
    InfoDialog.show(parent, "Network Details", buildNetInfoProperties(rtu));
  }

  private static Properties[] buildRTUInfoProperties(RTUCommsService rtu) {
    ArrayList<Properties> allProperties = new ArrayList<>();

    // RTU Version
    RTUInfo rtuInfo = rtu.getRtuInfo();

    allProperties.add(createProperties("Site Information", true,
        new Object[][] {
            { "Site Name", rtuInfo.getSiteName() },
            { "Software Package Version", rtuInfo.getSdpVersion() },
            { "Service Status", getServiceState(rtu.getRtuStatus()) },
            { "Date and Time ", rtu.getRtuStatus().getRTUTimeStr() },
        }));

    // Configuration
    allProperties.add(createProperties("Configuration",
        new Object[][] {
            { "Version", objToStr(rtuInfo.getConfigFileVersion()) },
            { "Modification date", rtuInfo.getConfigModifyDate() },
            { "Exist", objToStr(rtuInfo.getConfigExist()) },
            { "Valid", objToStr(rtuInfo.getConfigValid()) },
        }));

    // Operating System
    allProperties.add(createProperties("Operating System",
        new Object[][] {
            { "CPU serial No", rtuInfo.getCpuSerial() },
            { "Kernel", rtuInfo.getKernelVersion() },
            { "File System", rtuInfo.getRootfsVersion() },
            { "Uptime", TimeUtils.convertDurationFromSecsToDays(rtu.getRtuStatus().getSysUptimeSecs()) },
        }));

    // Application
    allProperties.add(createProperties("Application",
        new Object[][] {
            { "MCM", rtuInfo.getMcmVersion() },
            { "Uptime:", TimeUtils.convertDurationFromSecsToDays(rtu.getRtuStatus().getAppUptimeSecs()) },
        }));

    // API
    G3ConfigAPI configAPI = rtuInfo.getConfigAPI();
    if(configAPI != null) {
    allProperties.add(createProperties("API",
        new Object[][] {
            { "Config Protocol", configAPI.getG3ProtocolVersion() },
            { "Config Schema", configAPI.getG3SchemaVersion() },
            { "Module Protocol:", configAPI.getModuleProtocollVersion() },
        }));
    }

    return allProperties.toArray(new Properties[allProperties.size()]);
  }

  private static Properties[] buildNetInfoProperties(RTUCommsService rtu) {
    ArrayList<Properties> allProperties = new ArrayList<>();

    NetInfo[] nets = rtu.getRtuInfo().getNetInfoArray();

    for (int i = 0; i < nets.length; i++) {
      String title = nets[i].getNetID();

      // Customise title
      if (title != null && "eth0".equalsIgnoreCase(title)) {
        title = "[Control Port] " + title;
      } else if (title != null && "eth1".equalsIgnoreCase(title)) {
        title = "[Config Port] " + title;
      }

      allProperties.add(createProperties(title, true,
          new Object[][] {
              { "Physical Address", nets[i].getMac() },
              { "IP Address", nets[i].getIp() },
              { "Subnet Mask", nets[i].getMask() },
              { "Gateway", nets[i].getGateway() },
          }));
    }

    return allProperties.toArray(new Properties[allProperties.size()]);
  }

  private static String objToStr(Object obj) {
    return obj == null ? RTUInfo.TEXT_NA : obj.toString();
  }

  private static String getServiceState(RTUStatus status) {
    String serviceState;
    SERVICEMODE sm = status.getServiceMode();
    if (sm != null) {
      serviceState = sm.getDescription();
      if (sm == SERVICEMODE.SERVICEMODE_OUTSERVICE) {
        serviceState = serviceState + "(" + status.getOutServiceReason() + ")";
      }
    } else {
      serviceState = "N/A";
    }
    return serviceState;
  }

}
