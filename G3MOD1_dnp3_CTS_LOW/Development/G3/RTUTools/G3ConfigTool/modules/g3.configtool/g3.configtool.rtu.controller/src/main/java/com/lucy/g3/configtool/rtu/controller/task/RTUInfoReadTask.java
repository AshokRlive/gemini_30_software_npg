/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.controller.task;

import java.util.logging.Level;

import javax.swing.JFrame;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.comms.service.RTUCommsService;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.SERVICEMODE;

/**
 * Task for sending commands to RTU to read all RTU information, including
 * version, network information, module information, time and service state.
 * Showing an error dialog if there is any failure.
 */
public class RTUInfoReadTask extends Task<Void, Void> {

  private Logger log = Logger.getLogger(RTUInfoReadTask.class);
  private JFrame parent = null;
  private final ConnectionState state;

  private final RTUCommsService g3RTU;


  public RTUInfoReadTask(Application app, ConnectionState state, RTUCommsService g3RTU) {
    super(app);
    this.state = state;
    this.g3RTU = Preconditions.checkNotNull(g3RTU, "g3RTU is null");

    if (app != null && app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    }
  }

  @Override
  protected Void doInBackground() throws Exception {

    setMessage("Reading RTU device information...");
    StringBuilder sb = new StringBuilder();

    // Read basic RTUInfo
    try {
      g3RTU.getRtuInfo().cmdGetRTUInfo();
    } catch (Exception e) {
      log.error("Failed to get RTU info", e);
      sb.append("Fail to read RTU information. " + e.getMessage() + "<br>");
    }

    // Read RTU SDP version
    try {
      g3RTU.getRtuInfo().cmdGetSDPVersion();
    } catch (Exception e) {
      log.error("Fail to read SDP version",e);
    }

    // Read Module Info
    try {
      g3RTU.getRtuModules().cmdGetModueInfo();
    } catch (Exception e) {
      log.error("Fail to read module list information. ",e);
      sb.append("Fail to read module list information. " + e.getMessage() + "<br>");
    }

    // Read information that is available in normal mode.
    if (ConnectionState.isNormalMode(state)) {

      // Read ConfigInfo
      try {
        g3RTU.getRtuInfo().cmdGetConfigInfo();
        log.info("RTU Configuration Exist: " + g3RTU.getRtuInfo().getConfigExist());
        log.info("RTU Configuration Valid: " + g3RTU.getRtuInfo().getConfigValid());
        log.info("RTU Configuration CRC: " + g3RTU.getRtuInfo().getConfigCRC());
        log.info("RTU Configuration Modification Date: " + g3RTU.getRtuInfo().getConfigModifyDate());
        log.info("RTU Configuration File Version: " + g3RTU.getRtuInfo().getConfigFileVersion());
      } catch (Exception e) {
        log.error("Fail to read RTU Configuration information. ",e);
        sb.append("Fail to read RTU Configuration information. " + e.getMessage() + "<br>");
      }

      // Read NetInfo
      try {
        g3RTU.getRtuInfo().cmdGetNetInfo();
      } catch (Exception e) {
        log.error("Fail to read network information. ",e);
        sb.append("Fail to read network information. " + e.getMessage() + "<br>");
      }

      // Read DateTime
      try {
        g3RTU.getRtuStatus().cmdGetDateTime();
      } catch (Exception e) {
        log.error("Fail to read date and time information. ",e);
        sb.append("Fail to read date and time information. " + e.getMessage() + "<br>");
      }

      // Read ServiceMode
      try {
        SERVICEMODE mode = g3RTU.getRtuStatus().cmdGetServiceStatus();
        log.info(String.format("RTU Service Status: %s", mode));
      } catch (Exception e) {
        log.error("Fail to read service status. ",e);
        sb.append("Fail to read service status. " + e.getMessage() + "<br>");
      }
      
      // Read Debug Mode
      try {
        g3RTU.getRtuStatus().cmdGetRTUDebugState();
      } catch (Exception e) {
        log.error("Fail to read debug mode state. ",e);
      }
    }

    String error = sb.toString();
    if (!error.trim().isEmpty()) {
      throw new Exception(error);
    }
    return null;
  }

  @Override
  protected void failed(Throwable cause) {
    log.error("Fail to read RTU info", cause);

    ErrorInfo info = new ErrorInfo(
        "Error",
        "Fail to read RTU information",
        cause.getMessage(),
        "RTU Info Task",
        null, Level.SEVERE, null);
    JXErrorPane.showDialog(parent, info);
  }

  @Override
  protected void succeeded(Void result) {
    setMessage("RTU system information is read");
  }
}