/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.controller;

import static org.junit.Assert.*;

import org.jdesktop.application.Application;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.binding.beans.Model;
import com.lucy.g3.configtool.rtu.controller.RTUController;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.test.support.CommsTestSupport;

/**
 * The Class RTUControllerTest.
 */
public class RTUControllerTest extends Model {

  private RTUController fixture;


  @Before
  public void setUp() throws Exception {
    Assume.assumeNotNull(CommsTestSupport.HOST);
    G3RTU g3rtu = G3RTUFactory.createDefault(CommsTestSupport.HOST);
    fixture = new RTUController(null, g3rtu);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testShowVersion() {
    fixture.aboutRTU();
    fixture.showNetworkInfo();
  }

  @Test
  public void testGetAction() {
    String[] actionKeys = {
        RTUController.ACTION_KEY_REGISTER_MODULES,
        RTUController.ACTION_KEY_DISABLE_SERVICE,
        RTUController.ACTION_KEY_ENABLE_SERVICE,
        RTUController.ACTION_KEY_REFRESH_INFO,
        RTUController.ACTION_KEY_SET_TIME,
        RTUController.ACTION_KEY_SHOW_NETINFO,
    };

    for (String key : actionKeys) {
      assertNotNull("action for the key:\"" + key + "\" is null.", fixture.getAction(key));
    }
  }

}
