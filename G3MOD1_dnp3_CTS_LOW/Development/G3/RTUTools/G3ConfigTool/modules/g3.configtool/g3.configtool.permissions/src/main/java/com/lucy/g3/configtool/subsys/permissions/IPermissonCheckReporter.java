/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.permissions;

import com.lucy.g3.xml.gen.common.G3ConfigProtocol.USER_LEVEL;

public interface IPermissonCheckReporter {

  void showErrorMsg(String title, String error);

  void showActionNotPermittedMsg(String title, String error, UserActivities activity, USER_LEVEL userlevel);

}

