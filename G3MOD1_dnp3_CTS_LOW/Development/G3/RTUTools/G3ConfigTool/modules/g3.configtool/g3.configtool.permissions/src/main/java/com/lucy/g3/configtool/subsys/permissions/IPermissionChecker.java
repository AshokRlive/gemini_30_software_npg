/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.permissions;

/**
 * Component for checking whether a user activity is permitted or not.
 */
public interface IPermissionChecker {

  /**
   * Checks if a user's activity is permitted.
   */
  boolean checkPermitted(UserActivities activity);
}
