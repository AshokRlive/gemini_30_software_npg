/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.permissions;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.lucy.g3.xml.gen.common.G3ConfigProtocol.USER_LEVEL;

public class PermissionCheckChecks {
  private final ArrayList<IPermissionChecker> registry = new ArrayList<>();
  
  private IPermissonCheckReporter reporter = null;
  
  private PermissionCheckChecks(){}
  
  public void registerChecker(IPermissionChecker permission){
    if(permission != null)
      registry.add(permission);
  }
  
  public void deregisterChecker(IPermissionChecker permission){
    registry.remove(permission);
  }
  
  public ArrayList<IPermissionChecker> getRegisteredCheckers(){
    return new ArrayList<>(registry);
  }
  
  public void registerReporter(IPermissonCheckReporter reporter){
    if(reporter != null) {
      this.reporter = reporter;
    }
  }
  
  public IPermissonCheckReporter getReporter(){
    if(reporter == null) {
      reporter = new DefaultPermissionCheckReporter();
    }
    
    return reporter;
  }

  public static boolean checkPermitted(UserActivities activity) {
    for (IPermissionChecker p : INSTANCE.registry) {
      if(p.checkPermitted(activity) == false) {
        return false;
      }
    }
    
    return true;
  } 
  
  
  private static class DefaultPermissionCheckReporter implements IPermissonCheckReporter {
    private Logger log = Logger.getLogger(DefaultPermissionCheckReporter.class);

    @Override
    public void showErrorMsg(String title, String error) {
      log.error(error);
    }

    @Override
    public void showActionNotPermittedMsg(String title, String error, UserActivities activity, USER_LEVEL userlevel) {
      log.error(error);
    }
  }
  
  public static PermissionCheckChecks getInstance(){
    return INSTANCE;
  }
  
  private static final PermissionCheckChecks INSTANCE = new PermissionCheckChecks();
}

