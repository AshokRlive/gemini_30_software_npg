/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.permissions;

/**
 * This Enum defines User Activity from ConfigTools.
 */
public enum UserActivities {
  TEST_RTU("Test RTU"),
  DEBUG_RTU("Debug RTU"),
  WRITE_CONFIG("Write Configuration"),
  SHUTDOWN_RTU("Shut down RTU"),
  UPGRADE("Upgrade RTU");

  UserActivities(String description) {
    this.description = description;
  }
  
  public String getDescription(){
    return description;
  }

  private final String description;
}