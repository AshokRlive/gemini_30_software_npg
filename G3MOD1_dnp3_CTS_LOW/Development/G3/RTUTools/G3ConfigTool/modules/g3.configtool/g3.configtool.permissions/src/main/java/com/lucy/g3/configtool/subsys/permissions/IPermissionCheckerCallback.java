/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.permissions;

import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.comms.shared.LoginResult;

/**
 * The Interface IPermissionCheckerCallback.
 */
public interface IPermissionCheckerCallback {

  ConnectionState getConnectionState();

  LoginResult getLoginResult();
}