/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.permissions;

import org.apache.log4j.Logger;

import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.comms.shared.LoginResult;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.USER_LEVEL;

/**
 * Implementation of {@linkplain IPermissionChecker}.
 */
public abstract class AbstractPermissionChecker implements IPermissionChecker {

  // ================== User Permissions Matrix ===================
  private boolean[][] permissionMatrix = {
      // Admin, Operator, Engineer,
      { false, true, true },   // RTU_TEST
      { true, false, true },  // RTU_DEBUG
      { true, false, true },   // WRITE_CONFIG
      { true, false, true },   // SHUTDOWN_RTU
      { true, false, false },  // UPGRADE
  };

  private Logger log = Logger.getLogger(AbstractPermissionChecker.class);

  private IPermissionCheckerCallback callback;
  
  public AbstractPermissionChecker(IPermissionCheckerCallback callback){
    if(callback == null)
      throw new IllegalArgumentException("callback must not be null");
    
    this.callback = callback;
  }
  
  protected void setPermissionMatrix(boolean[][] matrix){
    this.permissionMatrix = matrix;
  }
  
  @Override
  public boolean checkPermitted(UserActivities activity) {
    if (activity == null) {
      return true;
    }

    LoginResult loginInfo = callback.getLoginResult();
    
    IPermissonCheckReporter reporter = PermissionCheckChecks.getInstance().getReporter();
    
    if (loginInfo == null
        || ConnectionState.isConnected(callback.getConnectionState()) == false) {
      String msg = "User permission denied! Please login to RTU.";
      reporter.showErrorMsg("Denied", msg);
      return false;
    }

    USER_LEVEL userlevel = loginInfo.level;
    if (userlevel == null) {
      log.error("User Level is not available!");
      return false;
    }

    boolean permitted = permissionMatrix[activity.ordinal()][userlevel.ordinal()];
    if (permitted == false) {
      String msg = String.format("<html><p>User permission denied!</p><br>"
          + "<p>It is not allowed to <i>%s </i> by <i>%s</i> users.</p></html>",
          activity.getDescription(),
          userlevel);
      String title = "Denied";
      reporter.showActionNotPermittedMsg(title, msg, activity,userlevel);
    }
    return permitted;
  }

}
