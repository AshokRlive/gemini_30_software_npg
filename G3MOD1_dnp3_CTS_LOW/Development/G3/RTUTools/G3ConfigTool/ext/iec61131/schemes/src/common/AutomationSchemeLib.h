/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AutomationSchemeLib.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef AUTOMATIONSCHEMELIB_H_
#define AUTOMATIONSCHEMELIB_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
void runGeneratedCode(void);

/*
 ******************************************************************************
 * Interfaces used by automation scheme
 ******************************************************************************
 */
void Operate(lu_uint32_t operate_id, lu_uint32_t value);
void SetOutput(lu_uint32_t output_id, lu_uint32_t value);
lu_uint32_t GetInput(lu_uint32_t input_id);
lu_int32_t GetConstant(lu_uint32_t input_id);
void LogInfo(lu_char_t* msg, ...);
void LogErr(lu_char_t* msg, ...);
void DelayMs(lu_uint32_t ms);
lu_int32_t processMessage(lu_char_t* received);
/**
 * Set the current line number.
 * args
 *  number - current line number, start from 1.
 */
void SetLineNum(lu_uint32_t number);
void SetCurrentRegister(lu_int32_t number);


#endif /* AUTOMATIONSCHEMELIB_H_ */

/*
 *********************** End of file ******************************************
 */
