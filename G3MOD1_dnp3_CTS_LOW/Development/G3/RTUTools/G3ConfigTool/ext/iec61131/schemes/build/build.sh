#!/bin/bash
# copy axf of MCM board to export path, for sending to the MCM itself
APP="Debug/*.axf"
DEST="/home/opt/exports/${USER}/gemini/application"

cd Debug
make $1
RESULT=$?
if [ $RESULT -eq 0 ]
then
    # successful make
    cd ..
    cp -v Debug/*.so $DEST
    echo copy done!
else
    exit $RESULT
fi
