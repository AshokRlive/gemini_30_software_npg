@echo off
REM This batch file is for generating schema components from XML sources in common directory using XSLT1.0
REM Make sure all schema components are regenerated after related XML sources are updated.

xsltproc ../../../xslt/xml2xsd.xsl ../../../xml/AutomationEnum.xml >AutomationEnum.xsd

echo The generation of schema components has been done!
pause