@echo off
REM This batch file is for generating schema components from XML sources in common directory using XSLT2.0
REM Make sure all schema components are regenerated after related XML sources are updated.



echo start transforming xml to xsd...
set transform="C:\Program Files\Saxonica\SaxonHE9.6N\bin\Transform"
%transform% -xsl:../../../xslt/xml2xsd.xsl -s:../../../xml/ModuleProtocolEnum.xml -o:ModuleProtocolEnum.xsd
%transform% -xsl:../../../xslt/xml2xsd.xsl -s:../../../xml/MCMConfigEnum.xml -o:MCMConfigEnum.xsd
%transform% -xsl:../../../xslt/xml2xsd.xsl -s:../../../xml/DNP3Enum.xml -o:DNP3Enum.xsd
%transform% -xsl:../../../xslt/xml2xsd.xsl -s:../../../xml/IEC870Enum.xml -o:IEC870Enum.xsd
%transform% -xsl:../../../xslt/xml2xsd.xsl -s:../../../xml/ProtocolStackCommonEnum.xml -o:ProtocolStackCommonEnum.xsd
%transform% -xsl:../../../xslt/xml2xsd.xsl -s:../../../xml/ModBusEnum.xml -o:ModBusEnum.xsd
echo The generation of schema components has been done!
pause