<?xml version="1.0" encoding="UTF-8"?>
<sourceFile classname="MainAppEnum" xsi:noNamespaceSchemaLocation="common.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <enum name="CHANNEL_TYPE" enableCString="true" comment="Channel types">
            <item name="CHANNEL_TYPE_DINPUT   "         value="0x00" description="Digital Input"/>  
            <item name="CHANNEL_TYPE_AINPUT   "         value="0x01" description="Analogue Input"/>
            <item name="CHANNEL_TYPE_DOUTPUT  "         value="0x02" description="Digital Output"/>
            <item name="CHANNEL_TYPE_AOUTPUT  "         value="0x03" description="Analogue Output"/>
            <item name="CHANNEL_TYPE_PSUPPLY  "         value="0x04" description="Power supply"/>
            <item name="CHANNEL_TYPE_BCHARGER "         value="0x05" description="Battery Charger"/>
            <item name="CHANNEL_TYPE_FPI      "         value="0x06" description="FPI"/>
            <item name="CHANNEL_TYPE_FAN      "         value="0x07" description="Fan"/>
            <item name="CHANNEL_TYPE_SW_OUT   "         value="0x08" description="Switch output"/>
            <item name="CHANNEL_TYPE_AUXSW_OUT"         value="0x09" description="Auxiliary switch output"/>
            <item name="CHANNEL_TYPE_HMI      "         value="0x0a" description="HMI"/>
            <item name="CHANNEL_TYPE_INTERNAL_DINPUT"   value="0x0b" description="Internal Digital Input"/>  
            <item name="CHANNEL_TYPE_INTERNAL_DDINPUT"  value="0x0c" description="Internal Double Digital Input"/>  
            <item name="CHANNEL_TYPE_INTERNAL_AINPUT"   value="0x0d" description="Internal Analogue Input"/>
            <item name="CHANNEL_TYPE_INTERNAL_DOUTPUT"  value="0x0e" description="Internal Digital Output"/>
            <item name="CHANNEL_TYPE_INTERNAL_DDOUTPUT" value="0x0f" description="Internal Double Digital Output"/>
    </enum>

    <!--Return codes for module operations    --> 
    <enum name="IOM_ERROR" enableCString="true" description="Module error codes in MCM" >
            <item name="IOM_ERROR_NONE         "  value=" 0"    description="No error"       />    
            <item name="IOM_ERROR_GENERIC      "  value=" 1"    description="general error"  />         
            <item name="IOM_ERROR_PARAM        "  value=" 2"    description="Invalid parameters"  />         
            <item name="IOM_ERROR_CONFIG       "  value=" 3"    description="Not configured"       />    
            <item name="IOM_ERROR_NOT_FOUND    "  value=" 4"    description="No item found"  />         
            <item name="IOM_ERROR_NOT_ENABLED  "  value=" 5"    description="Not enabled"        />         
            <item name="IOM_ERROR_NOT_RUNNING  "  value=" 6"    description="Operation is not running"  />         
            <item name="IOM_ERROR_NOT_SUPPORTED"  value=" 7"    description="Operation is not supported"  />         
            <item name="IOM_ERROR_ALREADY      "  value=" 8"    description="Operation already in progress"  />         
            <item name="IOM_ERROR_SEND         "  value=" 9"    description="Error sending" />         
            <item name="IOM_ERROR_READ         "  value="10"    description="Error reading"       />         
            <item name="IOM_ERROR_WRITE        "  value="11"    description="Error reading"      />         
            <item name="IOM_ERROR_CMD          "  value="12"    description="Command error"       />    
            <item name="IOM_ERROR_TIMEOUT      "  value="13"    description="Time out"  />         
            <item name="IOM_ERROR_INVALID_MSG  "  value="14"    description="Invalid message"       />    
            <item name="IOM_ERROR_PAYLOAD      "  value="15"    description="Invalid payload"  />         
            <item name="IOM_ERROR_REPLY        "  value="16"    description="Unexpected reply"  />         
            <item name="IOM_ERROR_SELECT       "  value="17"    description="Error in select operation"        />         
            <item name="IOM_ERROR_OPERATE_ERROR"  value="18"    description="Error in operate operation"  />         
            <item name="IOM_ERROR_CANCEL_ERROR "  value="19"    description="Error in cancel operation" />         
            <item name="IOM_ERROR_LOCAL_REMOTE "  value="20"    description="Local/Remote mismatch"       />         
            <item name="IOM_ERROR_OFFLINE      "  value="21"    description="The module is offline" />    
            <item name="IOM_ERROR_VMOTOR       "  value="22"    description="Motor supply failure"      />         
            <item name="IOM_ERROR_BATTERY      "  value="23"    description="Problem with RTU battery" />    
            <item name="IOM_ERROR_RELAY        "  value="24"    description="Relay malfunction" />    
    </enum>

    <!--The states of Slave Module State Machine managed in MCM. 
        NOTE: In this enum, the items and values must be consistent with the listed states in "SlaveModuleSM.sm". 
         Changing the states order, adding/removing states in "SlaveModuleSM.sm" requires this enum to be updated.
    --> 
    <enum name="FSM_STATE" enableCString="true" description="All states of Slave Module state machine managed in MCM" >
            <item name="FSM_STATE_CREATED      "  value="0x00"    description="Created"       />    
            <item name="FSM_STATE_WAITING_INFO "  value="0x01"    description="Waiting Info"  />         
            <item name="FSM_STATE_BOOTING      "  value="0x02"    description="Booting"       />    
            <item name="FSM_STATE_WAITING_INIT "  value="0x03"    description="Waiting Init"  />         
            <item name="FSM_STATE_INITIALISING "  value="0x04"    description="Initialising"  />         
            <item name="FSM_STATE_ACTIVE       "  value="0x05"    description="Active"        />         
            <item name="FSM_STATE_PROGING_BL   "  value="0x06"    description="Programming bootloader"  />         
            <item name="FSM_STATE_ILLEGAL      "  value="0x07"    description="Illegal State" />         
            <item name="FSM_STATE_OFFLINE      "  value="0x08"    description="Offline"       />         
            <item name="FSM_STATE_DISABLED     "  value="0x09"    description="Disabled"      />         
            <item name="FSM_STATE_ERROR        "  value="0x0A"    description="Module Error"  />
    </enum>	

    <!--Return codes for Gemini Database operations    --> 
    <enum name="GDB_ERROR" enableCString="true" description="Gemini Database error codes in MCM" >
            <item name="GDB_ERROR_NONE           "  value=" 0"    description="No error"       />    
            <item name="GDB_ERROR_PARAM          "  value=" 1"    description="Invalid parameters"  />         
            <item name="GDB_ERROR_ADDJOB         "  value=" 2"    description="Error adding a Job to the DB Scheduler"  />         
            <item name="GDB_ERROR_ADDOBSERVER    "  value=" 3"    description="Error attaching an observer"       />    
            <item name="GDB_ERROR_INITIALIZED    "  value=" 4"    description="Element already initialised"  />         
            <item name="GDB_ERROR_NOT_INITIALIZED"  value=" 5"    description="Element not initialised"        />         
            <item name="GDB_ERROR_NOPOINT        "  value=" 6"    description="Point not available/Invalid point"  />         
            <item name="GDB_ERROR_POINT_EXIST    "  value=" 7"    description="The point already exists"  />         
            <item name="GDB_ERROR_NOT_SUPPORTED  "  value=" 8"    description="Operation not supported"  />         
            <item name="GDB_ERROR_MEMORY         "  value=" 9"    description="Error accessing memory" />         
            <item name="GDB_ERROR_CMD            "  value="10"    description="Error in command"       />         
            <item name="GDB_ERROR_BUSY           "  value="11"    description="Element busy"      />         
            <item name="GDB_ERROR_DELAY          "  value="12"    description="Delay error"       />    
            <item name="GDB_ERROR_ALREADY_ACTIVE "  value="13"    description="Element is already active"  />         
            <item name="GDB_ERROR_AUTHORITY      "  value="14"    description="Not authorized"       />    
            <item name="GDB_ERROR_POSITION       "  value="15"    description="Position error"  />         
            <item name="GDB_ERROR_LOCAL_REMOTE   "  value="16"    description="Local/Remote mismatch"  />         
            <item name="GDB_ERROR_INHIBIT        "  value="17"    description="Operation Inhibited"        />         
            <item name="GDB_ERROR_CONFIG         "  value="18"    description="Invalid config"        />
            <item name="GDB_ERROR_NOT_ENABLED    "  value="19"    description="Element not enabled"        />
    </enum>
    
    <const name="SGL_MSUPPLY"  typeJava="int" typeC="lu_uint32_t"  description="Motor Supply constants" >
            <item name="SGL_MSUPPLY_SECURITY_THR_MS" value="10"         description="Motor Supply security threshold (seconds)"/>
            <item name="SGL_MSUPPLY_MAX_OVERALL_TIME_MS" value="255000" description="Maximum accepted Motor Supply overall time"/>
    </const>
    
    <enum name="SYNC_SOURCE" enableCString="true" description="Sources of time synchronisation" >
            <item name="SYNC_SOURCE_NONE      "  value="0x00"    description="None"         />    
            <item name="SYNC_SOURCE_SYSTEM    "  value="0x01"    description="Op. System"   />         
            <item name="SYNC_SOURCE_SCADA     "  value="0x02"    description="SCADA"        />    
            <item name="SYNC_SOURCE_CONFIGTOOL"  value="0x03"    description="Config Tool"  />         
            <item name="SYNC_SOURCE_NTP       "  value="0x04"    description="NTP"          />         
            <item name="SYNC_SOURCE_SNTP      "  value="0x05"    description="Secure NTP"   />         
            <item name="SYNC_SOURCE_GPS       "  value="0x06"    description="GPS"          />         
            <item name="SYNC_SOURCE_OTHER     "  value="0x07"    description="Other"        />         
    </enum> 
    
</sourceFile>
