/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief Cross-platform types
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _LU_TYPES_INCLUDED
#define _LU_TYPES_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define LU_TRUE            (1U)
#define LU_FALSE           (0U)

#define LU_UNUSED(expr)    (void)(expr)

#define LU_STRINGIFY(name) #name

/**
 * \brief Macro LU_CONCAT for token concatenation
 *
 * Concatenates 2 tokes to compose a new token, for example to create a variable
 * name at compile time.
 * Note that concatenation on the pre-processor the standard itself requires 2
 * levels of indirection.
 *
 * \return New token composed
 */
#define LU_CONCAT_PASTE(pre,post) pre ## post
#define LU_CONCAT(pre,post) LU_CONCAT_PASTE(pre,post)

/**
 * \brief Macro for getting the max/min of 2 elements
 *
 * \return Max/min value
 */
#define LU_MAX(a, b)       (((a) > (b))? (a) : (b))
#define LU_MIN(a, b)       (((a) < (b))? (a) : (b))

/**
 * \brief Macro for getting the number of elements in a given array
 */
#define LU_ARRAY_LENGTH(array)    (sizeof(array)/sizeof((array)[0]))

/**
 * \brief Macro for Enable/Disable warning messages from compiler
 */
#ifdef __GNUC__
#define PRAGMA(x) _Pragma(#x)
#if ((__GNUC__ * 100) + __GNUC_MINOR__) >= 404
#define LU_ENABLE_WARNING(_warning_) PRAGMA(GCC diagnostic warning #_warning_)
#define LU_DISABLE_WARNING(_warning_) PRAGMA(GCC diagnostic ignored #_warning_)
#else
#define LU_ENABLE_WARNING(_warning_)
#define LU_DISABLE_WARNING(_warning_)
#endif
#endif /* __GNUC__ */

#ifdef _MSC_VER
#define LU_ENABLE_WARNING(n)
#define LU_DISABLE_WARNING(n)
#endif /* _MSC_VER */

/**
 * \brief Macro LU_STATIC_ASSERT for static assertion 
 *
 * This is an assert() implementation intended to fire at compilation time.
 * Could be used anywhere, inside or outside function blocks.
 *
 * It works by defining a struct with an integer bitfield of valid/invalid length:
 *          typedef struct { int error_message : 0; } unique_name
 *      When the condition is false, the bit field size 0 is invalid and throws
 *      an error message about the field named as error_message.
 *      Zero-sized bitfield works better than negative size arrays for error
 *      messages in Visual C++. Typedef allows to be used nearly everywhere and
 *      is more portable than STAT_ASSERT()/_Static_assert()/static_assert()
 *      Explained in "cl" answer in https://stackoverflow.com/a/4815532
 *
 * \usage: LU_STATIC_ASSERT(<static_condition>, <error_message_as_variable_name>);
 *      Being:
 *          <static_condition>: condition for triggering -- being static means
 *              it should be valid at compile time in the scope of the call.
 *          <error_message_as_variable_name>: a variable name stating the error
 *              desired to be displayed at the compile output. It is not a string,
 *              is a variable name (so no spaces allowed) but does not need to be
 *              defined anywhere.
 *
 * \param condition Static condition that when false, stops compilation and throws an error
 * \param error_variable Error message in the form of a variable name (don't need be real)
 *
 * \return Compilation breaks at this point if condition not true
 *
 * Please note that when the condition fails, this macro may throw the error in this way:
 *  -Win32 (Visual C++):
 *      "error C2149: 'Static_assertion_failed_<error_variable>': named bit field cannot have zero width"
 *  -GNUC (GCC):
 *      "error: zero width for bit-field 'Static_assertion_failed_<error_variable>'"
 *
 * \example: LU_STATIC_ASSERT(sizeof(int)<2, A_weird_CPU);
 *      Errors generated:
 *          -Win32: "error C2149: 'Static_assertion_failed_A_weird_CPU': named bit field cannot have zero width"
 *          -GNUC: "error: zero width for bit-field 'Static_assertion_failed_A_weird_CPU'"
 */
#define LU_STATIC_ASSERT(cond,msg) \
    typedef struct { int LU_CONCAT(static_assertion_failed_,msg) : !!(cond); } \
                    LU_CONCAT(static_assertion_failed_,__COUNTER__)


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

#ifdef __GNUC__
typedef  unsigned char      lu_bool_t;

typedef char                lu_char_t;

typedef char                lu_int8_t;
typedef unsigned char       lu_uint8_t;

typedef short               lu_int16_t;
typedef unsigned short      lu_uint16_t;

typedef int                 lu_int32_t;
typedef unsigned int        lu_uint32_t;

typedef long long           lu_int64_t;
typedef unsigned long long  lu_uint64_t;

typedef float               lu_float32_t;

typedef double              lu_float64_t;

#endif

#ifdef _MSC_VER
typedef  unsigned char      lu_bool_t;

typedef char                lu_char_t;

typedef char                lu_int8_t;
typedef unsigned char       lu_uint8_t;

typedef short               lu_int16_t;
typedef unsigned short      lu_uint16_t;

typedef long                lu_int32_t;
typedef unsigned long       lu_uint32_t;

typedef __int64             lu_int64_t;
typedef unsigned __int64    lu_uint64_t;

typedef float               lu_float32_t;

typedef double              lu_float64_t;

#endif

#ifdef __18CXX
/* Microchip - MC18 compiler*/

typedef  unsigned char      lu_bool_t;

typedef char                lu_char_t;

typedef char                lu_int8_t;
typedef unsigned char       lu_uint8_t;

typedef int                 lu_int16_t;
typedef unsigned int        lu_uint16_t;

typedef long                lu_int32_t;
typedef unsigned long       lu_uint32_t;

#endif

#ifdef __XC__
/* Microchip - XC8 compiler*/

typedef  unsigned char      lu_bool_t;

typedef char                lu_char_t;

typedef char                lu_int8_t;
typedef unsigned char       lu_uint8_t;

typedef int                 lu_int16_t;
typedef unsigned int        lu_uint16_t;

typedef long                lu_int32_t;
typedef unsigned long       lu_uint32_t;

#endif


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */


#endif /* _LU_TYPES_INCLUDED */

/*
 *********************** End of file ******************************************
 */
