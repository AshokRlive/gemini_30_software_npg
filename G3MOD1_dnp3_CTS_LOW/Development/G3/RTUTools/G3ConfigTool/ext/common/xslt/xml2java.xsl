<!--Usage: xsltproc -stringparam package "com.lucy.g3.xml.gen.ModuleProtocol;" xml2java.xsl ModuleProtocolEnumFPM.xml>ModuleProtocolEnumFPM.java  -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:param  name = "package">com.lucy.configtool.domain.common;</xsl:param>
  <xsl:strip-space elements="*"/>
  <xsl:output method="text" encoding="utf-8" media-type="text/plain" indent="no"/>
  <xsl:template match="sourceFile">/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/
 
// Auto generated from XML file. - DO NOT MODIFY

package <xsl:value-of select="$package"/>
import java.util.HashMap;

<xsl:choose>
<xsl:when test="@interface ='true' " >
public interface <xsl:value-of select="@classname"/> {
</xsl:when>
<xsl:otherwise>
public class <xsl:value-of select="@classname"/> {
</xsl:otherwise>
</xsl:choose>

    <!-- Convert const -->
    <xsl:for-each select="const">
    <xsl:variable name="jtype" select="@typeJava"/>
     <xsl:if test ="position() = 1">
     </xsl:if>
    <xsl:choose>
        <xsl:when test="@description">
    /* <xsl:value-of select="@description"/> */</xsl:when>
    </xsl:choose>
       <xsl:for-each select="item">
    public final static <xsl:value-of select="$jtype"/><xsl:text> </xsl:text><xsl:value-of select="@name"/> = (<xsl:value-of select="$jtype"/>)<xsl:value-of select="@value"/>;<xsl:text>    </xsl:text>
        <xsl:choose><xsl:when test="@description">// <xsl:value-of select="@description"/> </xsl:when></xsl:choose>
       </xsl:for-each>
       <xsl:text>
       </xsl:text>
    </xsl:for-each>
    
    <!-- Convert Define -NOT Support yet
     <xsl:for-each select="Define">
    <xsl:variable name="jtype" select="@typeJava"/>
     <xsl:if test ="position() = 1">
     </xsl:if>
    <xsl:choose>
        <xsl:when test="@description">
    /* <xsl:value-of select="@description"/> */</xsl:when>
    </xsl:choose>
       <xsl:for-each select="item">
    public final static <xsl:text>int </xsl:text><xsl:value-of select="@name"/> = <xsl:value-of select="@value"/>;<xsl:text>    </xsl:text>
        <xsl:choose><xsl:when test="@description">// <xsl:value-of select="@description"/> </xsl:when></xsl:choose>
       </xsl:for-each>
       <xsl:text>
       </xsl:text>
    </xsl:for-each>
    -->
    <!-- Convert "channel" -->
 <xsl:for-each select="channel">
 
 <!-- Check if scaling factor is defined -->
 <xsl:variable name="supportScaling" select="./*/scalingFactorOption"></xsl:variable>
 
 <xsl:variable name="channelEnumName" select="@name"/>
   public enum <xsl:value-of select="@name"/> 
            implements com.lucy.g3.xml.gen.api.IChannelEnum{
      <xsl:for-each select="item">
        
      <xsl:value-of select="@name"/>
      <xsl:text>(</xsl:text>
      <xsl:value-of select="@value"/>
      <xsl:text>, "</xsl:text>
      <xsl:value-of select="@description"/>
      <xsl:text>", </xsl:text>
      <xsl:choose><xsl:when test="@group">"<xsl:value-of select="@group"/>"</xsl:when><xsl:otherwise>""</xsl:otherwise></xsl:choose>
      <xsl:text>, </xsl:text>
      <xsl:choose><xsl:when test="@defaultEventRate"><xsl:value-of select="@defaultEventRate"/></xsl:when><xsl:otherwise>0</xsl:otherwise></xsl:choose>
      <xsl:text>)</xsl:text>
      <xsl:choose><xsl:when test="position() != last()">,</xsl:when> <xsl:otherwise>;</xsl:otherwise></xsl:choose>
      <xsl:text>
      </xsl:text>
   </xsl:for-each>
      final private  int value;
      final private  String description;
      final private String group;
      final private int eventRate;//Default event rate 
      
      private       DefaultCreateOption defaultCreateOption = DefaultCreateOption.DoNotCreatePoint;
      final private HashMap&#60;String, String&#62; defconfig = new HashMap&#60;String, String&#62;();
     
     <xsl:if test="boolean($supportScaling)">
      private Double defaultScalingFactor;   
      final private HashMap&#60;Double, String> scalingFactors = new HashMap&#60;Double, String&#62;();
     </xsl:if>   
     
      final private static  HashMap&#60;Integer, <xsl:value-of select="@name"/>&#62; typesByValue 
                            = new HashMap&#60;Integer, <xsl:value-of select="@name"/>&#62;();  
                                                      
     <xsl:value-of select="@name"/>(int value,String description,String group,int eventRate) {
         this.value = value;
         this.description = description;
         this.group = group;
         this.eventRate = eventRate;
      }
    
     public int getID() {
         return value;
     }
     
    @Override
    public int getValue() {
        return value;
    }
     
     public String getName(){
         return name();     
     }
     
     public int getDefaultEventRate(){
         return eventRate;     
     }
     
     public final String getDescription(){
        if(this.description != null)
            return this.description;
        else
            return super.toString();
     }
      
     public final String getGroup() {
        return group;
     }
     
     
     @Override
     public String toString(){
        if(this.description != null)
            return description;
        else
            return super.toString();        
     }    

    @Override
    public DefaultCreateOption getDefCreateOption(){
        return defaultCreateOption;
    }
    
    @Override
    public String getDefConf(String parameterKey){
        return defconfig.get(parameterKey);
    }
    
    <xsl:choose><xsl:when test="boolean($supportScaling)">
     
    public HashMap&#60;Double, String&#62; getAllScaleFactorMap() {
        return new HashMap&#60;Double, String&#62;(scalingFactors);
    }
     
     public Double getDefaultScalingFactor(){
        return  defaultScalingFactor;
     }
     
     
     public String getUnitForScaleFactor(Double scaleFactor){
         return scalingFactors.get(scaleFactor);
     }
     </xsl:when><xsl:otherwise>
     public HashMap&#60;Double, String&#62; getAllScaleFactorMap() {return null; }
     
     public String getUnitForScaleFactor(Double scalingFactor){return null;}
     
     public Double getDefaultScalingFactor(){ return  null;}
     
     </xsl:otherwise></xsl:choose>
     
     public static <xsl:value-of select="@name"/> forValue(int value) {
         return typesByValue.get(value);
     }
    
    
    static {
	     for (<xsl:value-of select="@name"/> type : <xsl:value-of select="@name"/>.values()) {
	        typesByValue.put(type.value, type);   
	     }
     
     <!--Initialise pre-defined point configuration-->
     <xsl:for-each select="item">
     <xsl:variable name="name" select="@name"/>
     
     
     
     <!-- Initialise default create option -->
     <xsl:if test="defaultCreateOption">
     <xsl:value-of select="$channelEnumName"/>.<xsl:value-of select="$name"/>.defaultCreateOption = DefaultCreateOption.<xsl:value-of select="defaultCreateOption"/> 
     <xsl:text>;
     </xsl:text>
     </xsl:if>
     
     
     <!-- Initialise default config option -->
     <xsl:for-each select="defaultConfig">
     <xsl:value-of select="$name"/>.defconfig.put("<xsl:value-of select="@key"/>","<xsl:value-of select="@value"/>");
     </xsl:for-each>
     </xsl:for-each>
     
     
     <!--Initialise pre-defined scaling factors-->
     <xsl:if test="boolean($supportScaling)">
     <xsl:for-each select="item">
     <xsl:variable name="name" select="@name"/>
     <xsl:for-each select="scalingFactorOption">
     <!-- Set default scaling factor -->
     <xsl:if test="@isDefault">
     <xsl:value-of select="$channelEnumName"/>.<xsl:value-of select="$name"/>.defaultScalingFactor = <xsl:value-of select="@scalingFactor"/>D;
     </xsl:if>
     
     <!-- Add scaling factor options -->     
     <xsl:value-of select="$channelEnumName"/>.<xsl:value-of select="$name"/>.scalingFactors.put(<xsl:value-of select="@scalingFactor"/>D,"<xsl:value-of select="@unit"/>");
     </xsl:for-each>
     </xsl:for-each>
     </xsl:if>
    }
   }
</xsl:for-each>

    <!-- Convert enum -->
  <xsl:for-each select="enum">
   public enum <xsl:value-of select="@name"/> 
                    implements com.lucy.g3.xml.gen.api.IXmlEnum{
    <xsl:for-each select="item">
      <xsl:text>   </xsl:text>    
      <xsl:value-of select="@name"/>(<xsl:value-of select="@value"/>
      <xsl:text>, </xsl:text>
      <xsl:choose><xsl:when test="@description">"<xsl:value-of select="@description"/>"</xsl:when><xsl:otherwise>null</xsl:otherwise></xsl:choose>    
      <xsl:text>)</xsl:text>
      <xsl:choose><xsl:when test="position() != last()">,</xsl:when> <xsl:otherwise>;</xsl:otherwise></xsl:choose>
      <xsl:text>
      </xsl:text>
   </xsl:for-each>
     private static final HashMap&#60;Integer, <xsl:value-of select="@name"/>&#62; typesByValue = new HashMap&#60;Integer, <xsl:value-of select="@name"/>&#62;();

      static {
         for (<xsl:value-of select="@name"/> type : <xsl:value-of select="@name"/>.values()) {
            typesByValue.put(type.value, type);
         }
      }

      final private  int value;
      final private  String description;
                
      <xsl:value-of select="@name"/>(int value,String description) {
         this.value = value;
         this.description = description;
      }
    
      public final int getValue() {
         return this.value;
      }
      
      public final String getDescription(){
        if(this.description != null)
            return this.description;
        else
            return super.toString();
      }
      
      @Override
      public String toString(){
        if(this.description != null)
            return this.description;
        else
            return super.toString();        
     }

     public static <xsl:value-of select="@name"/> forValue(int value) {
         return typesByValue.get(value);
     }
   }
</xsl:for-each>

<!-- Convert logicPoint -->
<xsl:for-each select="logicPoint">
   public enum <xsl:value-of select="@name"/> 
                    implements com.lucy.g3.xml.gen.api.ILogicPointEnum{
    <xsl:for-each select="item">
      <xsl:text>   </xsl:text>    
      <xsl:value-of select="@name"/>(<xsl:value-of select="@value"/>
      <xsl:text>, </xsl:text>
      <xsl:choose><xsl:when test="@description">"<xsl:value-of select="@description"/>"</xsl:when><xsl:otherwise>null</xsl:otherwise></xsl:choose>    
      <xsl:text>)</xsl:text>
      <xsl:choose><xsl:when test="position() != last()">,</xsl:when> <xsl:otherwise>;</xsl:otherwise></xsl:choose>
      <xsl:text>
      </xsl:text>
   </xsl:for-each>

      static {
        <xsl:for-each select="item">
        <xsl:variable name="name" select="@name"/>

         <xsl:if test="@defaultScalingFactor">
         <xsl:value-of select="$name"/>.defaultScalingFactor = <xsl:value-of select="@defaultScalingFactor"/>;
         </xsl:if>
              
         <xsl:if test="@unit">
         <xsl:value-of select="$name"/>.unit ="<xsl:value-of select="@unit"/>";
         </xsl:if>
         
         <!-- Initialise default create option -->
	     <xsl:if test="defaultCreateOption">
	     <xsl:value-of select="$name"/>.defaultCreateOption = DefaultCreateOption.<xsl:value-of select="defaultCreateOption"/>;
	     </xsl:if>
	     <xsl:value-of select="$name"/>.type = LogicPointType.valueOf("<xsl:value-of select="@type"/>");
	     assert <xsl:value-of select="$name"/>.type!=null; 
	     
	     </xsl:for-each>
      }

      final private  int value;
      final private  String description;
      private  Double defaultScalingFactor;
      private  String unit;
      private LogicPointType type;  
      private DefaultCreateOption defaultCreateOption = DefaultCreateOption.DoNotCreatePoint;
      
                
      <xsl:value-of select="@name"/>(int value,String description) {
         this.value = value;
         this.description = description;
      }
    
      public final int getValue() {
         return this.value;
      }
      
      public final String getDescription(){
        if(this.description != null)
            return this.description;
        else
            return super.toString();
      }
      
      @Override
      public Double getDefaultScalingFactor(){
        return defaultScalingFactor;
      }
  
  
      @Override
      public String getUnit(){
        return unit;
      }
  
      @Override
      public LogicPointType getType(){
        return type;
      }
      
      @Override
      public DefaultCreateOption getDefCreateOption(){
        return defaultCreateOption;
      }
      
      @Override
      public String toString(){
        if(this.description != null)
            return this.description;
        else
            return super.toString();        
     }

   }
</xsl:for-each>


<!-- Convert logicInput -->
<xsl:for-each select="logicInput">
   public enum <xsl:value-of select="@name"/> 
                    implements com.lucy.g3.xml.gen.api.ILogicInputEnum{
    <xsl:for-each select="item">
      <xsl:text>   </xsl:text>    
      <xsl:value-of select="@name"/>(<xsl:value-of select="@value"/>
      <xsl:text>, </xsl:text>
      <xsl:choose><xsl:when test="@description">"<xsl:value-of select="@description"/>"</xsl:when><xsl:otherwise>null</xsl:otherwise></xsl:choose>    
      <xsl:text>)</xsl:text>
      <xsl:choose><xsl:when test="position() != last()">,</xsl:when> <xsl:otherwise>;</xsl:otherwise></xsl:choose>
      <xsl:text>
      </xsl:text>
   </xsl:for-each>

      static {
        <xsl:for-each select="item">
        <xsl:variable name="name" select="@name"/>
     
         <!-- Initialise default create option -->
         <xsl:value-of select="$name"/>.mandatory = <xsl:value-of select="@mandatory"/>;
         <xsl:value-of select="$name"/>.type = LogicInputType.valueOf("<xsl:value-of select="@type"/>");
         assert <xsl:value-of select="$name"/>.type!=null;
         </xsl:for-each>
      }

      final private  int value;
      final private  String description; 
      private boolean mandatory = true;
      private LogicInputType type;
      
                
      <xsl:value-of select="@name"/>(int value,String description) {
         this.value = value;
         this.description = description;
      }
    
      public final int getValue() {
         return this.value;
      }
      
      public final String getDescription(){
        if(this.description != null)
            return this.description;
        else
            return super.toString();
      }
      
      @Override
      public boolean isMandatory(){
        return mandatory;
      }
      
      @Override
      public LogicInputType getType(){
        return type;
      }
  
      @Override
      public String toString(){
        if(this.description != null)
            return this.description;
        else
            return super.toString();        
     }

   }
</xsl:for-each>

} 
</xsl:template>
</xsl:stylesheet> 
