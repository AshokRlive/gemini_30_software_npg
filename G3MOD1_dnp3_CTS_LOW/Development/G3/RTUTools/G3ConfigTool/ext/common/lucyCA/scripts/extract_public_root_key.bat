@echo off
set ks="..\root\private\root.jks"
set pass=password
set alias=lucyroot
set output="..\root\public\LucyRoot.cer"

echo Extracting public root certificate file from keystore...
keytool -export -keystore %ks% -storepass %pass% -alias %alias% -file %output%
if errorlevel 1 exit /B %errorlevel%

echo Extracted public root certificate file to:
echo %output%



