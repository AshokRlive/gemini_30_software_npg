REM  Generate a private root key and store it into a JKS file.
@echo off
REM set /P AREYOUSURE=Are you sure you want to re-generate lucy root CA(Y/N)?
REM IF /I "%AREYOUSURE%" NEQ "Y" GOTO END

set ks="..\root\private\root.jks"
set pass=password
set alias=lucyroot

echo Cleaning..
rmdir /Q /S ..\root
md ..\root\private
md ..\root\public

echo Generating private key into keystore(jks)...
keytool -genkey -alias %alias% -keystore %ks% -storepass %pass% -keypass %pass% -keysize 2048 -keyalg RSA -validity 18250 -noprompt -dname "CN=192.168.0.1, OU=Automation, O=Lucy Electric, L=Banbury, S=Oxford, C=GB"
if %ERRORLEVEL% GEQ 1 EXIT /B 1

echo Generated root keystore:%ks%
:END




