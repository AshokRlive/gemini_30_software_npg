:: A script for signing a csr with lucy CA.
@echo off
echo Extracting private root key files from keystore...
set target=..\root\private\
set ks="%target%\root.jks"
set ks_p12="%target%\p12RootKeyStore.pfx"
set rootkey="%target%\lucyroot.key"
set rootcert="%target%\lucyroot.crt"
set kspass=password

::Cleanup
DEL /S /Q %rootkey% %rootcert% %ks_p12%

echo Converting the JKS into to a P12
keytool -noprompt -importkeystore -srckeystore %ks% -destkeystore %ks_p12% -srcstorepass %kspass% -deststorepass %kspass% -srcstoretype jks -deststoretype pkcs12
if errorlevel 1 exit /b %errorlevel%

echo Extracting the Private Key as a PEM file 
openssl pkcs12 -in %ks_p12% -passin pass:%kspass% -nocerts -nodes -out %rootkey%
if errorlevel 1 exit /B %errorlevel%

echo Extracting the Certificate 
openssl pkcs12 -in %ks_p12% -passin pass:%kspass% -clcerts -nokeys -out %rootcert%
if errorlevel 1 exit /B %errorlevel%

del %ks_p12%
echo Extracted private root key files to:
echo %rootkey%
echo %rootcert%

