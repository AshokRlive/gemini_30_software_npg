==== Gemini3 Configuration Tool Project ====

Folder Structure Description:
build			- Contains scripts for building various artifacts.
modules 		- G3 application modules.
modules-common 	- G3 common modules shared with different applications.
modules-gen 	- G3 modules that contains generated code.
releng			- Configuration for release engineering.
schemas			- XML schemas.


=== How to build  === 
Build all modules:
$mvn package 

Build a specific module:
$mvn -pl com.lucy:{ARTIFACT.ID} -am package 
e.g. 
$mvn -pl com.lucy:g3.rtu.comms -am package 
$mvn -pl com.lucy:g3.configtool.app -am package 

Build all common modules:
$mvn install -f releng/g3.super.parent/pom.xml
$mvn package -f releng/g3.modules-common.parent/pom.xml

Build all gen modules:
$mvn install -f releng/g3.super.parent/pom.xml
$mvn package -f releng/g3.modules-gen.parent/pom.xml

Build with profile
$mvn package -P PROFILE-A,PROFILE-B 
e.g. 
$mvn package -P ukpn 

=== How to update version number ===
$mvn --batch-mode release:update-versions -DdevelopmentVersion=1.6.0-SNAPSHOT

=== How to release ===
$mvn release:clean release:prepare --batch-mode -DdevelopmentVersion=2.1.0-SNAPSHOT -DdryRun=true 
(dryRun: don't checkin or tag anything in the scm repository)


