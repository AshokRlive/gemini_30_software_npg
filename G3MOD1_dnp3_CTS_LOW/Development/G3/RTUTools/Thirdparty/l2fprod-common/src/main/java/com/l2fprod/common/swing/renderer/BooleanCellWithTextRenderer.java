package com.l2fprod.common.swing.renderer;

import java.awt.Component;

import javax.swing.JList;
import javax.swing.JTable;

public class BooleanCellWithTextRenderer extends BooleanCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
	    setSelected(Boolean.TRUE.equals(value));
	    String text = String.valueOf(Boolean.TRUE.equals(value));
	    setText(text);
		return super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
				row, column);
	}

	@Override
	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {
	    setSelected(Boolean.TRUE.equals(value));
	    String text = String.valueOf(Boolean.TRUE.equals(value));
	    setText(text);
		return super.getListCellRendererComponent(list, value, index, isSelected,
				cellHasFocus);
	}

	
}



/**
*********************** End of file *******************************************
*/
