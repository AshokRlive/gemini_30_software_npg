/**
 * @PROJECT.FULLNAME@ @VERSION@ License.
 *
 * Copyright @YEAR@ L2FProd.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.l2fprod.common.propertysheet;

import java.beans.PropertyDescriptor;
import java.beans.PropertyVetoException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import org.apache.log4j.Logger;

import com.l2fprod.common.beans.ExtendedPropertyDescriptor;

/**
 * Support sub properties.
 * PropertyDescriptorAdapter.<br>
 *
 */
class ExtendedPropertyDescriptorAdapter extends DefaultProperty {
	private Logger log = Logger
			.getLogger(ExtendedPropertyDescriptorAdapter.class.getSimpleName());

	private ExtendedPropertyDescriptor descriptor;

  public ExtendedPropertyDescriptorAdapter() {
    super();
  }
  
  public ExtendedPropertyDescriptorAdapter(ExtendedPropertyDescriptor descriptor) {
    this();
    setDescriptor(descriptor);
    for (ExtendedPropertyDescriptor sub: descriptor.getSubDescriptors()) {
    	ExtendedPropertyDescriptorAdapter subProperty = new ExtendedPropertyDescriptorAdapter(sub);
    	this.addSubProperty(subProperty);
	}
  }

  public void setDescriptor(ExtendedPropertyDescriptor descriptor) {
    this.descriptor = descriptor;
  }
  
  public PropertyDescriptor getDescriptor() {
    return descriptor;
  }
  
  public String getName() {
    return descriptor.getName();
  }
  
  public String getDisplayName() {
    return descriptor.getDisplayName();
  }
  
  public String getShortDescription() {
    return descriptor.getShortDescription();
  }

  public Class getType() {
    return descriptor.getPropertyType();
  }

  public Object clone() {
    ExtendedPropertyDescriptorAdapter clone = new ExtendedPropertyDescriptorAdapter(descriptor);
    clone.setValue(getValue());
    return clone;
  }
  
  public boolean isEditable() {
    return descriptor.getWriteMethod() != null;
  }

  public String getCategory() {
    if (descriptor instanceof ExtendedPropertyDescriptor) {
      return ((ExtendedPropertyDescriptor)descriptor).getCategory();
    } else {
      return null;
    }
  }
  
  public void readFromObject(Object object) {
    try {
      Method method = descriptor.getReadMethod();
      if (method != null) {
       //setValue(method.invoke(object, null));

      	Object value = method.invoke(object, null);
          initializeValue(value); // avoid updating parent or firing property change
          if (value != null) {
            for (Iterator iter = subProperties.iterator(); iter.hasNext();) {
              Property subProperty = (Property)iter.next();
              subProperty.readFromObject(value);
            }
          }
        
      }
    } catch (Exception e) {
    	e.printStackTrace();
      String message = "Got "+ e.getClass().getSimpleName()+" when reading property " + getName();
      if (object == null) {
        message += ", object was 'null'";
      } else {
        message += ", object was " + String.valueOf(object);
      }
      log.warn(message);
      throw new RuntimeException(message, e);
    }
  }
  
  public void writeToObject(Object object) {
    try {
      Method method = descriptor.getWriteMethod();
//      if (method != null) {
//        method.invoke(object, new Object[]{getValue()});
//      }
      Object targetData = object;
      if(getParentProperty()!= null){
    	  targetData = getParentProperty().getValue();
	  }
      if (method != null && targetData != null) {
    		  method.invoke(targetData, new Object[] { getValue()});
        }else{
      	  String cName = object.getClass().getSimpleName();
      	 log.warn("Cannot write Property:"+getName()+" to Object:"+ cName
      			 +" cause the write method is NOT found in the Class "+cName);
        }
    } catch (Exception e) {
      // let PropertyVetoException go to the upper level without logging
      if (e instanceof InvocationTargetException &&
        ((InvocationTargetException)e).getTargetException() instanceof PropertyVetoException) {
        throw new RuntimeException(((InvocationTargetException)e).getTargetException());
      }

      String message = "Got "+ e.getClass().getSimpleName()+" when writing property " + getName();
      if (object == null) {
        message += ", object was 'null'";
      } else {
        message += ", object was " + String.valueOf(object);
      }
      log.warn("Fail to write cause:"+message);

      throw new RuntimeException(message, e);
    }
  }
}
