/* 
 * $Id: SwingWorkerTest.java,v 1.4 2008-07-25 19:32:28 idk Exp $
 * 
 * Copyright � 2005 Sun Microsystems, Inc. All rights
 * reserved. Use is subject to license terms.
 */
package org.jdesktop.swingworker;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.SwingUtilities;

import junit.framework.TestCase;

import org.jdesktop.swingworker.SwingWorker.StateValue;

public class SwingWorkerTest extends TestCase {

    private final static int TIME_OUT = 30;
    private final static TimeUnit TIME_OUT_UNIT = TimeUnit.SECONDS;
    
    public static void main(String[] args) {
        junit.swingui.TestRunner.run(SwingWorkerTest.class);
    }
   
    
    // is to be run on a worker thread.
    public final void testdoInBackground() throws Exception {
        SwingWorker<Thread,?> test = new SwingWorker<Thread, Object>() {
            @Override
            protected Thread doInBackground() throws Exception {
                return Thread.currentThread();
            }
        };
        test.execute();
        Thread result = test.get(TIME_OUT, TIME_OUT_UNIT);
        assertNotNull(result);
        assertNotSame(Thread.currentThread(), result);
    }

    //{@code process} gets everything from {@code publish}
    //should be executed on the EDT
    public final void testPublishAndProcess() throws Exception {
        final Exchanger<List<Integer>> listExchanger = 
            new Exchanger<List<Integer>>();
        final Exchanger<Boolean> boolExchanger = 
            new Exchanger<Boolean>();
        SwingWorker<List<Integer>,Integer> test = 
            new SwingWorker<List<Integer>, Integer>() {
                List<Integer> receivedArgs = 
                    Collections.synchronizedList(new ArrayList<Integer>());
                Boolean isOnEDT = Boolean.TRUE;
                final int NUMBERS = 100;
                @Override
                protected List<Integer> doInBackground() throws Exception {
                    List<Integer> ret = 
                        Collections.synchronizedList(
                            new ArrayList<Integer>(NUMBERS));
                    for (int i = 0; i < NUMBERS; i++) {
                        publish(i);
                        ret.add(i);
                    }
                    return ret;
                }
                @Override
                protected void process(List<Integer> args) {
                    for(Integer i : args) {
                        receivedArgs.add(i);
                    }
                    isOnEDT = isOnEDT && SwingUtilities.isEventDispatchThread();
                    if (receivedArgs.size() == NUMBERS) {
                        try {
                            boolExchanger.exchange(isOnEDT);
                            listExchanger.exchange(receivedArgs);
                        } catch (InterruptedException ignore) {
                            ignore.printStackTrace();
                        }
                    }
                }
        };
        test.execute();
        assertTrue(boolExchanger.exchange(null, TIME_OUT, TIME_OUT_UNIT));
        assertEquals(test.get(TIME_OUT, TIME_OUT_UNIT), 
            listExchanger.exchange(null, TIME_OUT, TIME_OUT_UNIT));
    }

    // done is executed on the EDT
    // receives the return value from doInBackground using get()
    public final void testDone() throws Exception {
        final String testString  = "test"; 
        final Exchanger<Boolean> exchanger = new Exchanger<Boolean>();
        SwingWorker<?,?> test = new SwingWorker<String, Object>() {
            @Override
            protected String doInBackground() throws Exception {
                return testString;
            }
            @Override
            protected void done() {
                try {
                    exchanger.exchange(
                        testString == get()
                        && SwingUtilities.isEventDispatchThread());
                } catch (Exception ignore) {
                }
            }
        };
        test.execute();
        assertTrue(exchanger.exchange(null, TIME_OUT, TIME_OUT_UNIT));
    }

    //PropertyChangeListener should be notified on the EDT only
    public final void testPropertyChange() throws Exception {
        final Exchanger<Boolean> boolExchanger = 
            new Exchanger<Boolean>();
        final SwingWorker<?,?> test = 
            new SwingWorker<Object, Object>() {
                @Override
                protected Object doInBackground() throws Exception {
                    firePropertyChange("test", null, "test");
                    return null;
                }
            };
        test.addPropertyChangeListener(
            new PropertyChangeListener() {
                boolean isOnEDT = true;

                public  void propertyChange(PropertyChangeEvent evt) {
                    isOnEDT &= SwingUtilities.isEventDispatchThread();
                    if ("state".equals(evt.getPropertyName())
                        && StateValue.DONE == evt.getNewValue()) {
                        try {
                            boolExchanger.exchange(isOnEDT);
                        } catch (Exception ignore) {
                            ignore.printStackTrace();
                        }
                    }
                }
            });
        test.execute();
        assertTrue(boolExchanger.exchange(null, TIME_OUT, TIME_OUT_UNIT));
    }
    
    //the sequence should be
    //StateValue.STARTED, done, StateValue.DONE
    public final void testWorkFlow() throws Exception {
        final List<Object> goldenSequence = 
            Arrays.asList(new Object[]{StateValue.STARTED, "done", 
                                       StateValue.DONE});
        final List<Object> sequence = 
                    Collections.synchronizedList(new ArrayList<Object>());

        final Exchanger<List<Object>> listExchanger = new Exchanger<List<Object>>();
        
        final SwingWorker<?,?> test = 
            new SwingWorker<Object,Object>() {
                @Override
                protected Object doInBackground() throws Exception {
                    return null;
                }
                @Override
                protected void done() {
                    sequence.add("done");
                }
            };
        test.addPropertyChangeListener(
            new PropertyChangeListener() {
                public  void propertyChange(PropertyChangeEvent evt) {
                    if ("state".equals(evt.getPropertyName())) {
                        sequence.add(evt.getNewValue());
                        if (StateValue.DONE == evt.getNewValue()) {
                            try {
                                listExchanger.exchange(sequence);
                            } catch (Exception ignore) {
                                ignore.printStackTrace();
                            }
                        }
                    }
                }
            });
        test.execute();
        assertEquals(goldenSequence, 
                     listExchanger.exchange(null, TIME_OUT, TIME_OUT_UNIT));
    }
    
    /* 
     * regression test for 6493680
     * [SwingWorker notifications might be out of order.]
     */ 
    public final void test6493680() throws Exception {
        final AtomicInteger lastProgressValue = new AtomicInteger(-1);
        final Exchanger<Boolean> exchanger = new Exchanger<Boolean>();
        class Test {
            private final AtomicInteger lastProgressValue = 
                new AtomicInteger(-1);
            private final Exchanger<Boolean> exchanger = 
                new Exchanger<Boolean>();

            boolean test() throws Exception {
                TestSwingWorker swingWorker = new TestSwingWorker();
                swingWorker.addPropertyChangeListener(
                    new PropertyChangeListener() {
                        public void propertyChange(PropertyChangeEvent evt) {
                            if ("progress" == evt.getPropertyName()) {
                                lastProgressValue.set((Integer) evt.getNewValue());
                            }
                        }
                    });

                swingWorker.execute();
                return exchanger.exchange(true);
            }

            class TestSwingWorker extends SwingWorker<Void, Void> {
                @Override
                protected Void doInBackground() throws Exception {
                    for (int i = 0; i <= 100; i++) {
                        Thread.sleep(1);
                        setProgress(i);
                    }
                    return null;
                }
                @Override
                protected void done() {
                    boolean isPassed = (lastProgressValue.get() == 100);
                    try {
                        exchanger.exchange(isPassed);
                    } catch (Exception ingore) {
                    }
                }
            }
        }
        /*
         * because timing is involved in this bug we will run the test
         * NUMBER_OF_TRIES times.
         * the tes`t passes if it does not fail once.
         */
         final int NUMBER_OF_TRIES = 50;
         for (int i = 0; i < NUMBER_OF_TRIES; i++) {
             assertTrue((new Test()).test());
         }
    }
    /**
     * regression test for 6557137
     * [SwingWorker does not change the state to DONE on exception in done]
     * (issue 5)
     */
    public final void test6557137() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);

        SwingWorker<Void, Void> testWorker =
            new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() throws Exception {
                    return null;
                }
                @Override 
                protected void done() {
                    Thread.currentThread().setUncaughtExceptionHandler(
                        new Thread.UncaughtExceptionHandler() {
                            public void uncaughtException(Thread t, 
                                                          Throwable e) {
                                // do nothing
                            }
                        });
                    throw new RuntimeException("Test exception. Please ignore");
                }
            };
        testWorker.addPropertyChangeListener(
            new PropertyChangeListener() {
                public void propertyChange(PropertyChangeEvent evt) {
                    if ("state" == evt.getPropertyName()) {
                        if (SwingWorker.StateValue.DONE == evt.getNewValue()) {
                            latch.countDown();
                        }
                    }
                }
            });

        testWorker.execute();
        if (! latch.await(2, TimeUnit.SECONDS)) {
            throw new RuntimeException("failed");
        }
    }
}
