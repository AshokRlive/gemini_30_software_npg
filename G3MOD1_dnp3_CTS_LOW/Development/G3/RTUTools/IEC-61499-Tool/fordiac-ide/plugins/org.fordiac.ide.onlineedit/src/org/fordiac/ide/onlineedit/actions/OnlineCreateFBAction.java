/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.onlineedit.actions;

import java.util.ArrayList;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.deployment.DeploymentCoordinator;
import org.fordiac.ide.deployment.IDeploymentExecutor;
import org.fordiac.ide.deployment.util.IDeploymentListener;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.onlineedit.Activator;

public class OnlineCreateFBAction implements IObjectActionDelegate {

	private ArrayList<FBEditPart> fbs = new ArrayList<FBEditPart>();

	
	private IDeploymentListener listener = new IDeploymentListener() {
		
		@Override
		public void responseReceived(String response, String source) {
			Activator.statusLineErrorMessage(response);
		}
		
		@Override
		public void postCommandSent(String message) {			
		}
		
		@Override
		public void postCommandSent(String command, String destination) {			
		}
		
		@Override
		public void finished() {			
		}

		@Override
		public void postCommandSent(String info, String destination,
				String command) {			
		}
	};
	
	public OnlineCreateFBAction() {

	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		System.out.println("Action " + action + "IWorkbenchPart " + targetPart);
	}

	@Override
	public void run(IAction action) {
		for (FBEditPart fb : fbs) {
			FBView view = fb.getCastedModel();

			FBView mappedFB = view.getMappedFB();
			if (mappedFB != null) {
				FB functionBlock = mappedFB.getFb();
				ResourceView resView = (ResourceView) mappedFB.eContainer()
						.eContainer();
				if (resView != null) {
					Resource res = resView.getResourceElement();
					Device dev = res.getDevice();
					if (dev != null) {
						String mgrID = DeploymentCoordinator.getMGR_ID(dev);
						IDeploymentExecutor executor = DeploymentCoordinator
								.getInstance().getDeploymentExecutor(dev);
						if (executor != null) {

							if (!mgrID.equals("")) {
								try {
									executor.getDevMgmComHandler().connect(mgrID);

									executor.getDevMgmComHandler().addDeploymentListener(listener);
									executor.createFBInstance(functionBlock, res);
									executor.getDevMgmComHandler().disconnect();
									executor.getDevMgmComHandler().removeDeploymentListener(listener);
								} catch (Exception e) {
									Activator.getDefault().logError(e.getMessage(), e);
								}

							} else {
							}
						} else {
							// Executor for defined Profile not found or no
							// executor available
						}
					}
					// executor.connect(mgrID);
				}
			}

		}

	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		System.out.println("Action " + action + "ISelection" + selection);
		fbs.clear();
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structSel = (IStructuredSelection) selection;
			for (Object object : structSel.toList()) {
				if (object instanceof FBEditPart) {
					fbs.add((FBEditPart) object);
				}
			}
		}

	}

}
