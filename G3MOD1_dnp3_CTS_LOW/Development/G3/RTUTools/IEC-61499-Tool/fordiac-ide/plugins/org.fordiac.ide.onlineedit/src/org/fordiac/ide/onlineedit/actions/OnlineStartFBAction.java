/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.onlineedit.actions;

import java.util.ArrayList;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.deployment.DeploymentCoordinator;
import org.fordiac.ide.deployment.IDeploymentExecutor;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.ResourceView;

public class OnlineStartFBAction implements IObjectActionDelegate {

	private ArrayList<FBEditPart> fbs = new ArrayList<FBEditPart>();

	public OnlineStartFBAction() {

	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		System.out.println("Action " + action + "IWorkbenchPart " + targetPart);
	}

	@Override
	public void run(IAction action) {
		for (FBEditPart fb : fbs) {
			FBView view = fb.getCastedModel();

			FBView mappedFB = view.getMappedFB();
			if (mappedFB != null) {
				FB functionBlock = mappedFB.getFb();
				ResourceView resView = (ResourceView) mappedFB.eContainer()
						.eContainer();
				if (resView != null) {
					Resource res = resView.getResourceElement();
					Device dev = res.getDevice();
					if (dev != null) {
						String mgrID = DeploymentCoordinator.getMGR_ID(dev);
						IDeploymentExecutor executor = DeploymentCoordinator
								.getInstance().getDeploymentExecutor(dev);
						if (executor != null) {

							if (!mgrID.equals("")) {
								try {
									executor.getDevMgmComHandler().connect(mgrID);
									executor.startFB(res, functionBlock);
									executor.getDevMgmComHandler().disconnect();
								} catch (Exception e) {
									showErrorDialog(e, functionBlock);
								}

							} else {
								// TODO error handling
							}
						} else {
							// Executor for defined Profile not found or no
							// executor available
						}
					}
					// executor.connect(mgrID);
				}
			}

		}

	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		System.out.println("Action " + action + "ISelection" + selection);
		fbs.clear();
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structSel = (IStructuredSelection) selection;
			for (Object object : structSel.toList()) {
				if (object instanceof FBEditPart) {
					fbs.add((FBEditPart) object);
				}
			}
		}

	}
	
	private void showErrorDialog(final Exception e, final FB functionBlock) {
		Shell shell = Display.getDefault().getActiveShell();		
		MessageDialog.openError(shell, "Online Start Function Block Error", 
				"Function Block: " + functionBlock.getName() + "\n" +		
				"Problem: "+ e.getMessage());
	}


}
