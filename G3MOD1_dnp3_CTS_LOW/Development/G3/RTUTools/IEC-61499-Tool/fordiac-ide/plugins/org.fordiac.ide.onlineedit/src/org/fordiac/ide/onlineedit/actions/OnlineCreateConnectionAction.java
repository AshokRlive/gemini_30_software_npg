/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.onlineedit.actions;

import java.util.ArrayList;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.editparts.ConnectionEditPart;
import org.fordiac.ide.deployment.DeploymentCoordinator;
import org.fordiac.ide.deployment.IDeploymentExecutor;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.onlineedit.Activator;

public class OnlineCreateConnectionAction implements IObjectActionDelegate {

	private ArrayList<ConnectionEditPart> connections = new ArrayList<ConnectionEditPart>();

	public OnlineCreateConnectionAction() {

	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		System.out.println("Action " + action + "IWorkbenchPart " + targetPart);
	}

	@Override
	public void run(IAction action) {
		for (ConnectionEditPart connection : connections) {
			ConnectionView view = connection.getCastedModel();

			Resource res = (Resource) view.getConnectionElement().eContainer()
					.eContainer();

			Device dev = res.getDevice();
			if (dev != null) {
				String mgrID = DeploymentCoordinator.getMGR_ID(dev);
				IDeploymentExecutor executor = DeploymentCoordinator.getInstance().getDeploymentExecutor(dev);
				if (executor != null) {

					if (!mgrID.equals("")) {
						try {
							executor.getDevMgmComHandler().connect(mgrID);
							if (view.getConnectionElement() instanceof EventConnection) {
								executor.createEventConnection(res,
										(EventConnection) view
												.getConnectionElement());
							} else {
								executor.createDataConnection(res,
										(DataConnection) view
												.getConnectionElement());
							}
							executor.getDevMgmComHandler().disconnect();
						} catch (Exception e) {
							Activator.getDefault().logError(e.getMessage(), e);
						}

					} else {
						// TODO error handling
					}
				} else {
					// Executor for defined Profile not found or no executor available
				}
			}
			// executor.connect(mgrID);
		}

	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		System.out.println("Action " + action + "ISelection" + selection);
		connections.clear();
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structSel = (IStructuredSelection) selection;
			for (Object object : structSel.toList()) {
				if (object instanceof ConnectionEditPart) {
					connections.add((ConnectionEditPart) object);
				}
			}
		}

	}

}
