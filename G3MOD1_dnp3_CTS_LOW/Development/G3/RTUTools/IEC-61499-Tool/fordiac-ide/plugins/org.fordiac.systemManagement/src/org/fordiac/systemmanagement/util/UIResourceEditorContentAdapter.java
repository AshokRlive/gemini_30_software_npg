/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.util;

import java.util.Iterator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.ui.ResizeUIFBNetwork;
import org.fordiac.ide.model.ui.Size;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The Class UIResourceEditorContentAdapter.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class UIResourceEditorContentAdapter extends EContentAdapter {

	/** The ui resource editor. */
	private final UIResourceEditor uiResourceEditor;

	/**
	 * Instantiates a new uI resource editor content adapter.
	 * 
	 * @param uiResourceEditor
	 *          the ui resource editor
	 */
	public UIResourceEditorContentAdapter(final UIResourceEditor uiResourceEditor) {
		super();
		this.uiResourceEditor = uiResourceEditor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.ecore.util.EContentAdapter#notifyChanged(org.eclipse.emf
	 * .common.notify.Notification)
	 */
	@Override
	public void notifyChanged(final Notification notification) {

		Object feature = notification.getFeature();
		if (LibraryElementPackage.eINSTANCE.getAutomationSystem_Application()
				.equals(feature)) {
			int type = notification.getEventType();

			switch (type) {
			case Notification.ADD:
				if (notification.getNewValue() instanceof Application) {
					Application app = (Application) notification.getNewValue();
					if (uiResourceEditor != null) {
						ResizeUIFBNetwork resizeUIFBNetwork = UiFactory.eINSTANCE
								.createResizeUIFBNetwork();
						UIFBNetwork uiFBNetwork = (UIFBNetwork) app.getFBNetwork()
								.eContainer();
						resizeUIFBNetwork.setUiFBNetwork(uiFBNetwork);
						Size size = UiFactory.eINSTANCE.createSize();
						size.setHeight(-1);
						size.setWidth(-1);
						resizeUIFBNetwork.setSize(size);
						uiResourceEditor.getFbNetworks().add(resizeUIFBNetwork);
					}
				}
				break;
			case Notification.REMOVE:
				if (notification.getOldValue() instanceof Application) {
					Application app = (Application) notification.getOldValue();
					if (uiResourceEditor != null) {
						ResizeUIFBNetwork deleteable = null;
						for (Iterator<ResizeUIFBNetwork> iterator = uiResourceEditor
								.getFbNetworks().iterator(); iterator.hasNext();) {
							ResizeUIFBNetwork resizeUIFBNetwork = iterator.next();
							if (resizeUIFBNetwork.getUiFBNetwork().getFbNetwork().equals(
									app.getFBNetwork())) {
								deleteable = resizeUIFBNetwork;
								break;
							}
						}
						uiResourceEditor.getFbNetworks().remove(deleteable);

						if (notification.getNotifier() instanceof AutomationSystem) {
							SystemManager.getInstance().saveSystem(
									(AutomationSystem) notification.getNotifier(), true);
						}
					}
				}
				break;
			}
		}

		super.notifyChanged(notification);
	}

}
