/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.util;

import org.eclipse.core.resources.IProject;
import org.fordiac.ide.typelibrary.TypeLibrary;
import org.fordiac.ide.util.Utils;
import org.fordiac.systemmanagement.Activator;

/**
 * The Class SystemPaletteManagement.
 */
public class SystemPaletteManagement {

	/**
	 * Copy tool type lib to project.
	 * 
	 * @param project the project
	 */
	public static void copyToolTypeLibToProject(final IProject project) {		
		try {
			Utils.copyDirectory(TypeLibrary.getInstance().getToolLibFolder(), project);
		} catch (Exception e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
	}

}
