/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Diagnostic;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.swt.widgets.Display;
import org.fordiac.ide.model.MappingManager;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.NamedElementComparator;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.Annotation;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.ConfigurableObject;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.Segment;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.util.LibraryElementAdapterFactory;
import org.fordiac.ide.model.listeners.IApplicationListener;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.IConnectionView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.UiPackage;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.model.ui.util.UiAdapterFactory;
import org.fordiac.ide.typeexport.SystemExporter;
import org.fordiac.ide.typeimport.ImportUtils;
import org.fordiac.ide.typelibrary.DataTypeLibrary;
import org.fordiac.ide.typelibrary.TypeLibrary;
import org.fordiac.systemmanagement.extension.IExternalModelInitializer;
import org.fordiac.systemmanagement.extension.ITagProvider;
import org.fordiac.systemmanagement.util.UIResourceEditorContentAdapter;

/**
 * The Class SystemManager.
 * 
 * @author gebenh
 */
public class SystemManager implements IApplicationListener {
	public static final String DISTRIBUTED_PROJECT_NATURE_ID = "org.fordiac.systemManagement.DistributedNature"; //$NON-NLS-1$

	/** The Constant SYS_CONF_FILE_NAME. */
	private static final String SYS_CONF_FILE_NAME = "SysConf.xml"; //$NON-NLS-1$

	/** The Constant FILE_ENDING. */
	static final String FILE_ENDING = ".xml";//$NON-NLS-1$
	
	static final String SYSTEM_FILE_ENDING = ".sys"; //$NON-NLS-1$

	/** The Constant ENCODING_UTF_8. */
	private static final String ENCODING_UTF_8 = "UTF-8";//$NON-NLS-1$

	/** The instance. */
	private static SystemManager instance;

	/** The options. */
	private static Map<String, Object> options = new HashMap<String, Object>();

	static Map<String, Object> getOptions() {
		return options;
	}

	/** The resource set. */
	// ResourceSet resourceSet = new ResourceSetImpl();

	private final Hashtable<IProject, ResourceSet> resourceSets = new Hashtable<IProject, ResourceSet>();
	private final HashMap<AutomationSystem, ArrayList<ITagProvider>> tagProviders = new HashMap<AutomationSystem, ArrayList<ITagProvider>>();

	/**
	 * Gets the resource set.
	 * 
	 * @param project
	 *            the project
	 * 
	 * @return the resource set
	 */
	public ResourceSet getResourceSet(IProject project) {
		if (!resourceSets.containsKey(project)) {
			ResourceSet resSet = new ResourceSetImpl();
			resSet.getAdapterFactories()
					.add(new LibraryElementAdapterFactory());
			resSet.getAdapterFactories().add(new UiAdapterFactory());
			// resSet.getAdapterFactories().add(new VirtualDNSAdapterFactory());
			addExternalAdapterFactories(resSet);
			resourceSets.put(project, resSet);
		}
		return resourceSets.get(project);
	}

	// public ResourceSet getResourceSet() {
	// return resourceSet;
	// }

	/**
	 * Gets the single instance of SystemManager.
	 * 
	 * @return single instance of SystemManager
	 */
	public static SystemManager getInstance() {

		if (instance == null) {
			instance = new SystemManager();
			
		}
		return instance;
	}

	/**
	 * Force reload.
	 */
	public void forceReload() {
		loadSystems();
		notifyListeners();
	}

	/**
	 * Notify listeners.
	 */
	public void notifyListeners() {
		for (Iterator<DistributedSystemListener> iterator = listeners
				.iterator(); iterator.hasNext();) {
			DistributedSystemListener listener = iterator.next();
			listener.distributedSystemWorkspaceChanged();
		}
	}

	/** The listeners. */
	ArrayList<DistributedSystemListener> listeners = new ArrayList<DistributedSystemListener>();

	/**
	 * Adds the workspace listener.
	 * 
	 * @param listener
	 *            the listener
	 */
	public void addWorkspaceListener(final DistributedSystemListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	/**
	 * Instantiates a new system manager.
	 */
	private SystemManager() {
		initializeExternalModels();

		TypeLibrary.getInstance();   //this will correctly setup the tool library and initialize important stuff. should be done before loading any systems and adding the resource change listener
		loadSystems();
		
		options.put(XMLResource.OPTION_ENCODING, ENCODING_UTF_8);
		options.put(XMLResource.OPTION_DISABLE_NOTIFY, true);
		// TODO check whether the following options are faster
		options.put(XMLResource.OPTION_SAVE_ONLY_IF_CHANGED,
				XMLResource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);
		options.put(XMLResource.OPTION_PROCESS_DANGLING_HREF,
				XMLResource.OPTION_PROCESS_DANGLING_HREF_RECORD);
		// options.put(XMLResource.OPTION_ZIP, Boolean.TRUE);

		ResourcesPlugin.getWorkspace().addResourceChangeListener(
				new FordiacResourceChangeListener(this));
	}

	private void addExternalAdapterFactories(ResourceSet resSet) {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] elems = registry.getConfigurationElementsFor(
				Activator.PLUGIN_ID, "externalModelInitializer"); //$NON-NLS-1$
		for (int i = 0; i < elems.length; i++) {
			IConfigurationElement element = elems[i];
			try {
				Object object = element.createExecutableExtension("Interface"); //$NON-NLS-1$
				if (object instanceof IExternalModelInitializer) {
					IExternalModelInitializer externalModel = (IExternalModelInitializer) object;
					externalModel.addExternalAdapterFactories(resSet);
				}
			} catch (CoreException corex) {
				Activator.getDefault().logError("Error adding External Adapter Factories", corex);
			}
		}
	}

	private void initializeExternalModels() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] elems = registry.getConfigurationElementsFor(
				Activator.PLUGIN_ID, "externalModelInitializer"); //$NON-NLS-1$
		for (int i = 0; i < elems.length; i++) {
			IConfigurationElement element = elems[i];
			try {
				Object object = element.createExecutableExtension("Interface"); //$NON-NLS-1$
				if (object instanceof IExternalModelInitializer) {
					IExternalModelInitializer externalModel = (IExternalModelInitializer) object;
					externalModel.initializeModel();
				}
			} catch (CoreException corex) {
				Activator.getDefault().logError("Error loading External Model Initializer", corex);
			}
		}
	}

	/** The models. */
	List<String> models = new ArrayList<String>();

	/** The model systems. */
	Hashtable<String, AutomationSystem> systems = new Hashtable<String, AutomationSystem>();

	Hashtable<AutomationSystem, Runnable> runningJobs = new Hashtable<AutomationSystem, Runnable>();

	/**
	 * Add a system to be managed by the system manager. Additionally, it
	 * initializes the palette of the system.
	 * 
	 * @param system
	 *            the system to be added
	 */
	public void addSystem(final AutomationSystem system, IProject project) {
		if (project != null) {
			system.setProject(project);
			initializePalette(system);
		} else {
			Activator.getDefault().logError("Palette not initialzed correctly - a restart maybe solve the problem!");
		}
		systems.put(system.getId(), system);
		system.addApplicationListener(this);
		notifyListeners();
		Runnable job = createUniqueFBNamesValidity(system);
		runningJobs.put(system, job);
	}

	/**
	 * Remove a system from the set of systems managed by the system manager
	 * 
	 * @param system
	 *            to be added
	 */
	public void removeSystem(final AutomationSystem system) {
		removeSystemFromId(system.getId());
	}

	/**
	 * Remove a system with a given system id
	 * 
	 * @param systemId
	 */
	private void removeSystemFromId(final String systemId) {
		AutomationSystem system = systems.get(systemId);
		if (null != system) {
			systems.remove(systemId);
			system.removeApplicationListener(this);

			// TODO cleanup other hash tables the system may be in
			notifyListeners();
		}
	}

	/**
	 * Gets the systems.
	 * 
	 * @return the systems
	 */
	public List<AutomationSystem> getSystems() {
		ArrayList<AutomationSystem> temp = new ArrayList<AutomationSystem>();
		temp.addAll(Collections.list(systems.elements()));
		Collections.sort(temp, NamedElementComparator.INSTANCE);
		return temp;
	}

	/**
	 * Load systems.
	 */
	public void loadSystems() {
		IProject[] projects = ResourcesPlugin.getWorkspace().getRoot()
				.getProjects();
		List<String> oldModels = new ArrayList<String>(models);
		models.clear();
		for (int i = 0; i < projects.length; i++) {
			loadProject(projects[i]);
		}
		// remove all existing models to get the models that no longer exist
		oldModels.removeAll(models);
		for (Iterator<String> iterator = oldModels.iterator(); iterator
				.hasNext();) {
			String id = iterator.next();
			removeSystemFromId(id);
		}
		Enumeration<AutomationSystem> enumeration = notLoadableApps.keys();
		while (enumeration.hasMoreElements()) {
			AutomationSystem system = enumeration.nextElement();
			system.removeApplication(notLoadableApps.get(system));
		}

	}

	public AutomationSystem loadProject(IProject project) {
		long time1;
		long time2;
		AutomationSystem system = null;
		if (project.isOpen() && isDistributedSystem(project)) {
			time1 = System.currentTimeMillis();
			system = loadSystem(project);
			time2 = System.currentTimeMillis();
			System.out.println(time2 - time1 + " ms");
			if (system != null) {
				if (!systems.containsKey(system.getId())) {	
					addSystem(system, project);
					// load the applications
					loadDeviceTypesOfSysConf(system);
					loadApplications(system);
					verifyMappings(system);
				}
				models.add(system.getId());
				Runnable runnable = runningJobs.get(system);
				if (runnable != null) {
					Display.getDefault().asyncExec(runnable);
				}
			}
		}
		return system;
	}

	private void verifyMappings(AutomationSystem system) {
		for (Iterator<Application> iterator = system.getApplication()
				.iterator(); iterator.hasNext();) {
			Application application = iterator.next();
			FBNetwork fbNetwork = application.getFBNetwork();
			if (fbNetwork != null
					&& fbNetwork.eContainer() instanceof UIFBNetwork) {
				UIFBNetwork uifbnetwork = (UIFBNetwork) fbNetwork.eContainer();
				if (uifbnetwork != null) {
					for (View view : uifbnetwork.getChildren()) {
						if (view instanceof FBView) {
							FBView mappedFBView = ((FBView) view).getMappedFB();
							if (mappedFBView != null
									&& mappedFBView.eContainer() == null) {
								((FBView) view).setMappedFB(null);
								Activator.getDefault().logError("Something is wrong with mapping, try to resolve the issue.");
							}
						}
					}
				}
			}
		}
	}

	private void initializePalette(AutomationSystem system) {
		long time1;
		long time2;
		time1 = System.currentTimeMillis();
		DataTypeLibrary.getInstance();
		// load palette of the system and initialize the types		
		Palette palette = TypeLibrary.loadPalette(system.getProject());
		system.setPalette(palette);
		

		time2 = System.currentTimeMillis();
		System.out.println(time2 - time1 + " ms for typelib");
	}

	/**
	 * Gets the resource view for resource.
	 * 
	 * @param res
	 *            the res
	 * 
	 * @return the resource view for resource
	 */
	public ResourceView getResourceViewForResource(
			final org.fordiac.ide.model.libraryElement.Resource res) {
		return resourceViewMapping.get(res);
	}

	/**
	 * Adds the resource view for resource mapping.
	 * 
	 * @param res
	 *            the res
	 * @param resView
	 *            the res view
	 */
	public void addResourceViewForResourceMapping(
			final org.fordiac.ide.model.libraryElement.Resource res,
			final ResourceView resView) {
		resourceViewMapping.put(res, resView);
	}

	/**
	 * Removes the resource view for resource mapping.
	 * 
	 * @param res
	 *            the res
	 */
	public void removeResourceViewForResourceMapping(
			final org.fordiac.ide.model.libraryElement.Resource res) {
		resourceViewMapping.remove(res);
	}

	/**
	 * Gets the system for diagram.
	 * 
	 * @param diagram
	 *            the diagram
	 * 
	 * @return the system for diagram
	 */
	public AutomationSystem getSystemForDiagram(final Diagram diagram) {
		Diagram network = diagram;
		if (diagram instanceof UISubAppNetwork) {
			network = ((UISubAppNetwork)diagram).getRootApplication();
		}
		return diagramSystemMapping.get(network);
	}

	/**
	 * Adds the system diagram mapping.
	 * 
	 * @param diagram
	 *            the diagram
	 * @param system
	 *            the system
	 */
	public void addSystemDiagramMapping(final Diagram diagram,
			final AutomationSystem system) {
		diagramSystemMapping.put(diagram, system);
	}

	/**
	 * Removes the system diagram mapping.
	 * 
	 * @param diagram
	 *            the diagram
	 */
	public void removeSystemDiagramMapping(final Diagram diagram) {
		diagramSystemMapping.remove(diagram);
	}

	/** The resource view mapping. */
	private final Hashtable<org.fordiac.ide.model.libraryElement.Resource, ResourceView> resourceViewMapping = new Hashtable<org.fordiac.ide.model.libraryElement.Resource, ResourceView>();

	/** The diagram system mapping. */
	private final Hashtable<Diagram, AutomationSystem> diagramSystemMapping = new Hashtable<Diagram, AutomationSystem>();

	/**
	 * Load device types of sys conf.
	 * 
	 * @param system
	 *            the system
	 */
	private void loadDeviceTypesOfSysConf(final AutomationSystem system) {
		SystemConfigurationNetwork network = system.getSystemConfiguration()
				.getSystemConfigurationNetwork();
		
		network.getDevices();
		UISystemConfiguration uiSysConf = (UISystemConfiguration) network
				.eContainer();
		if (uiSysConf == null) {
			IProject project = system.getProject();
			uiSysConf = loadUISystemConfiguration(project);
			uiSysConf.setSystemConfigNetwork(network);
			Activator.getDefault().logWarning("Default loading failed - used alternative loading!");
		}
		diagramSystemMapping.put(uiSysConf, system);
		for (Iterator<Segment> iterator = network.getSegments().iterator(); iterator
				.hasNext();) {
			Segment segment = iterator.next();
			PaletteEntry entry = system.getPalette()
					.getTypeEntryForPath(segment.getTypePath(),
							"." + segment.getTypePath().split("\\.")[1]);
			segment.setPaletteEntry(entry);
		}

		for (Iterator<Device> iterator = network.getDevices().iterator(); iterator
				.hasNext();) {
			Device device = iterator.next();
			setVarDeclTypes(device.getVarDeclarations(), system);
			PaletteEntry entry = system.getPalette()
					.getTypeEntryForPath(device.getTypePath(),
							"." + device.getTypePath().split("\\.")[1]);
			device.setPaletteEntry(entry);
			DeviceView deviceView = findDeviceView(uiSysConf, device);

			for (Iterator<org.fordiac.ide.model.libraryElement.Resource> iterator2 = device
					.getResource().iterator(); iterator2.hasNext();) {
				org.fordiac.ide.model.libraryElement.Resource res = iterator2
						.next();
				setResouceType(res, system.getPalette());
				if (deviceView != null) {
					ResourceView resView = findResourceView(deviceView, res);
					if (resView != null) {

						UIResourceEditor uiResEd = resView
								.getUIResourceDiagram();// resolve
						// Proxy?
						EcoreUtil.resolveAll(uiResEd);

						// uiResEd.getNetwork();
						EcoreUtil.resolveAll(uiResEd.getResourceElement());
						ArrayList<FB> fbs = new ArrayList<FB>();
						for (Iterator<FB> iterator3 = uiResEd
								.getResourceElement().getFBs().iterator(); iterator3
								.hasNext();) {
							FB fb = (FB) iterator3.next();
							if (fb.getPaletteEntry() == null
									&& (fb.isResourceTypeFB() || fb
											.isResourceFB())) {
								fbs.add(fb);
							}
						}
						loadFBTypes(system, fbs, uiResEd.getResourceElement()
								.getFBNetwork());

						resourceViewMapping.put(res, resView);
						diagramSystemMapping.put(
								resView.getUIResourceDiagram(), system);
						system.eAdapters().add(
								new UIResourceEditorContentAdapter(resView
										.getUIResourceDiagram()));
					}
				}				
			}

		}
	}

	private void setResouceType(
			org.fordiac.ide.model.libraryElement.Resource res, final Palette palette) {
		res.setPaletteEntry(getPaletEntryFromTypeName(res.getType(), palette, TypeLibrary.RESOURCE_TYPE_FILE_ENDING_WITH_DOT));		
	}
	
	static private PaletteEntry getPaletEntryFromTypeName(final String typeName, final Palette palette, 
			final String typeFileEnding){
		PaletteEntry entry = null;
		
		if(typeName.contains("/") || typeName.contains("\\")){
			entry = palette.getTypeEntryForPath( typeName, typeFileEnding);				
		}
		else{
			List<PaletteEntry> entries = palette.getTypeEntries(TypeLibrary.getTypeNameFromFileName(typeName));
			if (entries.size() > 0) {
				entry = entries.get(0);
			} 
		}
		
		return entry;
	}

	/**
	 * Find resource view.
	 * 
	 * @param deviceView
	 *            the device view
	 * @param res
	 *            the res
	 * 
	 * @return the resource view
	 */
	private ResourceView findResourceView(final DeviceView deviceView,
			final org.fordiac.ide.model.libraryElement.Resource res) {
		for (Iterator<ResourceView> iterator = deviceView
				.getResourceContainerView().getResources().iterator(); iterator
				.hasNext();) {
			ResourceView resView = (ResourceView) iterator.next();
			if (resView.getResourceElement().equals(res)) {
				return resView;
			}
		}
		return null;
	}

	/**
	 * Find device view.
	 * 
	 * @param uiSysConf
	 *            the ui sys conf
	 * @param device
	 *            the device
	 * 
	 * @return the device view
	 */
	private DeviceView findDeviceView(final UISystemConfiguration uiSysConf,
			final Device device) {
		for (Iterator<View> iter = uiSysConf.getChildren().iterator(); iter
				.hasNext();) {
			View view = (View) iter.next();
			if (view instanceof DeviceView) {
				if (((DeviceView) view).getDeviceElement().equals(device)) {
					return (DeviceView) view;
				}
			}
		}
		return null;
	}

	/** The pack. */
	UiPackage pack = UiPackage.eINSTANCE;

	/**
	 * Load applications.
	 * 
	 * @param system
	 *            the system
	 */
	private void loadApplications(final AutomationSystem system) {
		for (Iterator<Application> iterator = system.getApplication()
				.iterator(); iterator.hasNext();) {
			Application application = iterator.next();
			try {
				ArrayList<FB> fbs = new ArrayList<FB>(application
						.getFBNetwork().getFBs());
				FBNetwork fbNetwork = application.getFBNetwork();

				if ((fbNetwork.getApplication() == null) || (!application.equals(fbNetwork.getApplication()))) {
					Activator.getDefault().logWarning("Loading of an System which maybe is corrupted!");
					fbNetwork.setApplication(application); // FIX @ 2009-06-03
					// by
					// gebenh
					// (required to load systems
					// which were imported before
					// the import bug fix)
				}
				for (Iterator<EventConnection> iterator2 = fbNetwork
						.getEventConnections().iterator(); iterator2.hasNext();) {
					EventConnection ec = (EventConnection) iterator2.next();
					EcoreUtil.resolveAll(ec);
				}
				EcoreUtil.resolveAll(fbNetwork.eContainer());
				Diagram diagram = (Diagram) fbNetwork.eContainer();
				diagramSystemMapping.put(diagram, system);
				
				for (IConnectionView c : diagram.getConnections()) {
					EcoreUtil.resolveAll(c);
				}
				EcoreUtil.resolveAll(fbNetwork);
				loadFBTypes(system, fbs, fbNetwork);

				ArrayList<SubApp> subApps = new ArrayList<SubApp>(application
						.getFBNetwork().getSubApps());
				loadFBTypesOfSubApps(system, subApps, fbNetwork, system.getPalette());
			} catch (Exception e) {
				Activator.getDefault().logError("Loading App failed: " + application.getName(), e);
			}
		}
	}

	/** The not loadable apps. */
	Hashtable<AutomationSystem, Application> notLoadableApps = new Hashtable<AutomationSystem, Application>();

	// private final ResourceSet virtualResSet;

	/**
	 * Load fb types of sub apps.
	 * 
	 * @param system
	 *            the system
	 * @param subApps
	 *            the sub apps
	 * @param fbNetwork
	 *            the fb network
	 */
	private void loadFBTypesOfSubApps(final AutomationSystem system,
			final ArrayList<SubApp> subApps, final SubAppNetwork fbNetwork, Palette palette) {

		for (SubApp subApp : subApps) {
			
			if(null != subApp.getType()){
				//if we have a typed subapplication set its correct palette entry
				subApp.setPaletteEntry(getPaletEntryFromTypeName(subApp.getType(), palette, TypeLibrary.SUBAPP_TYPE_FILE_ENDING_WITH_DOT));
			}
			
			// loadSubApp(system, subApp);
			ArrayList<FB> fbs = new ArrayList<FB>(subApp.getSubAppNetwork()
					.getFBs());
			loadFBTypes(system, fbs, subApp.getSubAppNetwork());

			ArrayList<SubApp> subApplications = new ArrayList<SubApp>(subApp
					.getSubAppNetwork().getSubApps());
			loadFBTypesOfSubApps(system, subApplications,
					subApp.getSubAppNetwork(), palette);
		}

	}

	/**
	 * Load fb types.
	 * 
	 * @param system
	 *            the system
	 * @param fbs
	 *            the fbs
	 * @param fbNetwork
	 *            the fb network
	 */
	private void loadFBTypes(final AutomationSystem system,
			final ArrayList<FB> fbs, final SubAppNetwork fbNetwork) {
		for (Iterator<FB> iterator1 = fbs.iterator(); iterator1.hasNext();) {
			FB fb = iterator1.next();
			setVarDeclTypes(fb, system);
			if (fb != null && fbNetwork != null) {
				if (fb.getId() == null) {
					fb.setId(EcoreUtil.generateUUID());
					Activator.getDefault().logWarning("Missing ID for " + fb.getName() + "\n Generated new ID.");
				}

				MappingManager.getInstance().addFbFBNetworkPair(fb.getId(),
						fbNetwork);

				if (fb.getFbtPath() != null) {
					PaletteEntry entry = system.getPalette().getTypeEntryForPath(
							fb.getFbtPath(), TypeLibrary.FB_TYPE_FILE_ENDING_WITH_DOT);
					if (entry != null) {
						fb.setPaletteEntry(entry);
					} else {
						IProject project = system.getProject();
						try {
							IMarker marker = project.createMarker(IMarker.PROBLEM);
							marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
							marker.setAttribute(IMarker.MESSAGE, "Type for FB Instance: " + fb.getName() + " not found (original FBType path was: " + fb.getFbtPath() + "). FBType maybe moved?");	
						} catch (CoreException e) {
							Activator.getDefault().logError(e.getMessage(), e);
						}
					}
				}
			} else {
				Activator.getDefault().logError("Faild to load FB of App");
			}
		}
	}

	/**
	 * Sets the var decl types.
	 * 
	 * @param fb
	 *            the new var decl types
	 */
	private void setVarDeclTypes(final FB fb, final AutomationSystem system) {
		setVarDeclTypes(fb.getInterface().getInputVars(), system);
		setVarDeclTypes(fb.getInterface().getOutputVars(), system);
	}

	/**
	 * Sets the var decl types.
	 * 
	 * @param list
	 *            of vardecls the new var decl types
	 */
	private void setVarDeclTypes(List<VarDeclaration> list, final AutomationSystem system) {
		for (Iterator<VarDeclaration> iterator = list.iterator(); iterator
				.hasNext();) {
			VarDeclaration varDecl = (VarDeclaration) iterator.next();
			if(varDecl instanceof AdapterDeclaration){
				List<PaletteEntry> adapterList = system.getPalette().getTypeEntries(varDecl.getTypeName());
				
				if(!adapterList.isEmpty()){
					varDecl.setType(((AdapterTypePaletteEntry)adapterList.get(0)).getAdapterType());
				}			
			}else{
				varDecl.setType(DataTypeLibrary.getInstance().getType(
					varDecl.getTypeName()));
			}
		}
	}

	/**
	 * Save diagram.
	 * 
	 * @param network
	 *            the network
	 * @param system
	 *            the system
	 * @param fileName
	 *            the file name
	 */
	public void saveDiagram(final Diagram network,
			final AutomationSystem system, final String fileName) {
		IProject project = system.getProject();

		if (project != null) {

			Runnable runnable = runningJobs.get(system);
			runnable.run();

			IPath path = project.getLocation().append(fileName);

			IPath path_old0 = project.getLocation().append(fileName + ".old0"); //$NON-NLS-1$
			IPath path_old1 = project.getLocation().append(fileName + ".old1");//$NON-NLS-1$
			IPath path_old2 = project.getLocation().append(fileName + ".old2");//$NON-NLS-1$
			IPath path_old3 = project.getLocation().append(fileName + ".old3");//$NON-NLS-1$
			IPath path_old4 = project.getLocation().append(fileName + ".old4");//$NON-NLS-1$
			File modelfile = new File(path.toOSString());

			File old0 = new File(path_old0.toOSString());
			File old1 = new File(path_old1.toOSString());
			File old2 = new File(path_old2.toOSString());
			File old3 = new File(path_old3.toOSString());
			new File(path_old4.toOSString());

			try {
				if (old3.exists()) {
					ImportUtils.copyFile(new File(path_old3.toOSString()), new File(
							path_old4.toOSString()));
				}
				if (old2.exists()) {
					ImportUtils.copyFile(new File(path_old2.toOSString()), new File(
							path_old3.toOSString()));
				}
				if (old1.exists()) {
					ImportUtils.copyFile(new File(path_old1.toOSString()), new File(
							path_old2.toOSString()));
				}
				if (old0.exists()) {
					ImportUtils.copyFile(new File(path_old0.toOSString()), new File(
							path_old1.toOSString()));
				}
				if (modelfile.exists()) {
					ImportUtils.copyFile(new File(path.toOSString()), new File(
							path_old0.toOSString()));
				}

			} catch (Exception e1) {
				Activator.getDefault().logError(e1.getMessage(), e1);
			}

			URI uri = URI.createFileURI(path.toOSString());

			// Resource resource = resourceSet.createResource(uri);
			Resource resource = null;
			try {
				ResourceSet resourceSet = getResourceSet(project);
				if (modelfile.exists()) {
					resource = resourceSet.getResource(uri, true);
				} else {
					resource = resourceSet.createResource(uri);
				}
				resource.getContents().clear();
				resource.getContents().add(network);
				resource.save(options);
				
				for (Diagnostic error : resource.getErrors()) {
					Activator.getDefault().logError(error.getMessage());
				}
				
			} catch (Exception e) {
				Activator.getDefault().logError(e.getMessage(), e);
			}
		}
	}

	/**
	 * Checks if is distributed system.
	 * 
	 * @param project
	 *            the project
	 * 
	 * @return true, if is distributed system
	 */
	private static boolean isDistributedSystem(final IProject project) {
		boolean retval = false;
		try {
			retval = project.getNature(DISTRIBUTED_PROJECT_NATURE_ID) != null;
		} catch (CoreException e) {
			Activator.getDefault().logWarning(e.getMessage(), e);
		}
		return retval;
	}

	/**
	 * Load system.
	 * 
	 * @param project
	 *            the project
	 * 
	 * @return the automation system
	 */
	private AutomationSystem loadSystem(final IProject project) {
		// ProjectModel projModel = uIFact.createProjectModel();
		IPath projectPath = project.getLocation();
		try {
			project.deleteMarkers(IMarker.PROBLEM, 
					      true, IResource.DEPTH_INFINITE);
		} catch (CoreException e1) {
			Activator.getDefault().logError(e1.getMessage(), e1);
		}
		IPath path = projectPath.append(project.getName() + FILE_ENDING);
		if (path.toFile().exists()) {
			URI uri = URI.createFileURI(path.toOSString());
			Resource resource = null;
			ResourceSet resourceSet = getResourceSet(project);
			try {
				resource = resourceSet.getResource(uri, true);
			} catch (WrappedException we) {
				resource = resourceSet.createResource(uri);
			}
			try {
				resource.load(options);
				if (resource.getContents().size() > 0) {
					EObject rootObject = resource.getContents().get(0);
					if (rootObject instanceof AutomationSystem) {
						EcoreUtil.resolveAll(resource);
						EcoreUtil.resolveAll(rootObject);
						loadTagProvider(project, (AutomationSystem) rootObject);
						return (AutomationSystem) rootObject;
					}
				}
			} catch (IOException e) {
				Activator.getDefault().logError(e.getMessage(), e);
			}
		}
		return null;
	}

	/**
	 * This method is used, to load external models within the 4DIAC resource
	 * set. </br> </br> <b>Note:</b> The model has to be registered by
	 * implementing the
	 * <em> org.fordiac.systemManagement.externalModelInitializer</em> extension
	 * point.
	 * 
	 * @param project
	 *            containing the file (model) to be loaded
	 * @param uri
	 *            of the file to be loaded
	 * @return
	 */
	public EList<EObject> loadExternalModel(final IProject project, URI uri) {
		ResourceSet resourceSet = getResourceSet(project);
		Resource resource = null;
		try {
			resource = resourceSet.getResource(uri, true);
			resource.load(options);
			return resource.getContents();

		} catch (WrappedException we) {
			// TODO WrappedException handling --> resource doesn't exist?
			System.err.println(we);
		} catch (Exception ex) {
			System.err.println(ex);
			// TODO exception handling
		}
		return null;
	}

	private void loadTagProvider(IProject project, AutomationSystem system) {

		if (!tagProviders.containsKey(system)) {
			tagProviders.put(system, new ArrayList<ITagProvider>());
		}
		ArrayList<ITagProvider> providers = tagProviders.get(system);

		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] elems = registry.getConfigurationElementsFor(
				Activator.PLUGIN_ID, "tagProvider"); //$NON-NLS-1$
		for (int i = 0; i < elems.length; i++) {
			IConfigurationElement element = elems[i];
			try {
				Object object = element.createExecutableExtension("Interface"); //$NON-NLS-1$
				if (object instanceof ITagProvider) {
					ITagProvider tagProvider = (ITagProvider) object;
					if (tagProvider.loadTagConfiguration(project.getLocation())) {
						providers.add(tagProvider);
					}
				}
			} catch (CoreException corex) {
				Activator.getDefault().logError("Error loading TagProviders!", corex);				
			}
		}

	}

	public void saveTagProvider(AutomationSystem system,
			ITagProvider tagProvider) {
		IProject project = system.getProject();
		IPath projectPath = project.getLocation();
		tagProvider.saveTagConfiguration(projectPath);
	}

	public String getReplacedString(AutomationSystem system,
			ConfigurableObject object, String value) {
		ArrayList<ITagProvider> tagProvider = tagProviders.get(system);
		String result = null;
		for (ITagProvider iTagProvider : tagProvider) {
			result = iTagProvider.getReplacedString(object, value);
			if (result != null) {
				break;
			}
		}
		return result;
	}

	private UISystemConfiguration loadUISystemConfiguration(
			final IProject project) {
		// ProjectModel projModel = uIFact.createProjectModel();
		IPath projectPath = project.getLocation();
		IPath path = projectPath.append("SysConf.xml");
		if (path.toFile().exists()) {
			URI uri = URI.createFileURI(path.toOSString());
			Resource resource = null;
			ResourceSet resourceSet = getResourceSet(project);
			try {
				resource = resourceSet.getResource(uri, true);
			} catch (WrappedException we) {
				resource = resourceSet.createResource(uri);
			}
			try {
				resource.load(options);
				EObject rootObject = resource.getContents().get(0);
				if (rootObject instanceof UISystemConfiguration) {

					return (UISystemConfiguration) rootObject;
				}

			} catch (IOException e) {
				Activator.getDefault().logError(e.getMessage(), e);
			}
		}
		return null;
	}

	private Runnable createUniqueFBNamesValidity(final AutomationSystem system) {

		return new Runnable() {
			public void run() {
				NameRepository.getInstance().clearUniqueFBInstanceNames(system);

				for (Application app : system.getApplication()) {
					checkAndCreateAnnotation(system, app.getFBNetwork().getFBs());
					checkSubApps(system, app.getFBNetwork().getSubApps());
				}
				checkDevices(system, system.getSystemConfiguration().getSystemConfigurationNetwork().getDevices());
			}


			private void checkSubApps(final AutomationSystem system,
					List<SubApp> subApps) {
				for (SubApp subApp :subApps) {
					checkAndCreateAnnotation(system, subApp.getSubAppNetwork().getFBs());
					checkSubApps(system, subApp.getSubAppNetwork().getSubApps());
				}
			}
			
			private void checkDevices(AutomationSystem system, EList<Device> devices) {
				for (Device device : devices) {
					for (org.fordiac.ide.model.libraryElement.Resource res : device.getResource()) {
						checkAndCreateAnnotation(system, res.getFBs());
					}					
				}				
			}
		};
	}
	private static void checkAndCreateAnnotation(AutomationSystem system, List<FB> fbs) {
		for (FB fb : fbs) {
			fb.getAnnotations().clear();
			if (!NameRepository.getInstance()
					.addSystemUniqueFBInstanceName(system,
							fb.getName(), fb.getId())) {
				Annotation anno = fb
						.createAnnotation("FB name not unique");
				anno.setServity(IMarker.SEVERITY_ERROR);
			}
		}
		
	}

	/**
	 * Save system.
	 * 
	 * @param system
	 *            the system
	 * @param all
	 *            the all
	 */
	public void saveSystem(
			final org.fordiac.ide.model.libraryElement.AutomationSystem system,
			final boolean all) {

		updateTypePaths(system);
		
		IProject project = system.getProject();
		if (project != null) {
			IFile systemFile = project.getFile(system.getName() + FILE_ENDING);
			URI uri = URI.createFileURI(systemFile.getLocation().toOSString());

			// Resource resource = resourceSet.createResource(uri);
			Resource resource = null;
			ResourceSet resourceSet = getResourceSet(project);
			try {
				if (systemFile.exists()) {
					resource = resourceSet.getResource(uri, true);
				} else {
					resource = resourceSet.createResource(uri);
				}
				if (all) {
					UISystemConfiguration uiSysConf = ((UISystemConfiguration) system
							.getSystemConfiguration()
							.getSystemConfigurationNetwork().eContainer());
					saveDiagram(uiSysConf, system, SYS_CONF_FILE_NAME);
					for (Iterator<Application> iterator = system
							.getApplication().iterator(); iterator.hasNext();) {
						Application app = iterator.next();
						saveDiagram(((UIFBNetwork) app.getFBNetwork()
								.eContainer()), system, app.getName()
								+ FILE_ENDING);
					}
				}
				if(null != resource){
					resource.getContents().clear();
					resource.getContents().add(system);
					resource.save(options);
				}
				//ensure that eclipse has the correct state and does not throw resource out of sync errors 
				project.refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
				
				//Save the project also as IEC 61499-2 XML file. This will allow to open 1.8.x projects in 
				//4diac-ide 1.9 and greater as well as with other IEC 61499 tools
				SystemExporter systemExporter = new SystemExporter();
				StringWriter stringWriter = new StringWriter();
				Result result = new StreamResult(stringWriter);
				systemExporter.generateSYSFileContent(system, result);
				
				IFile iec61499SystemFile = project.getFile(system.getName() + SYSTEM_FILE_ENDING);
				ByteArrayInputStream stream = new ByteArrayInputStream(stringWriter.toString().getBytes("UTF-8")); //$NON-NLS-1$
				if (iec61499SystemFile.exists()) {				
					iec61499SystemFile.setContents(stream, IFile.KEEP_HISTORY | IFile.FORCE, null);
				}else{
					iec61499SystemFile.create(stream, IFile.KEEP_HISTORY | IFile.FORCE, null);
				}
				
			} catch (Exception e) {
				Activator.getDefault().logError(e.getMessage(), e);
			}			
		}
	}

	private void updateTypePaths(AutomationSystem system) {
		
		for (Device device : system.getSystemConfiguration().getSystemConfigurationNetwork().getDevices()) {
			device.setTypePath(device.getPaletteEntry().getProjectRelativeTypePath());
			
			for (org.fordiac.ide.model.libraryElement.Resource resource : device.getResource()) {
				resource.setType(resource.getPaletteEntry().getProjectRelativeTypePath());
				for (FB fb : resource.getFBNetwork().getFBs()) {
					if(fb.isResourceFB()){
						fb.setFbtPath(fb.getPaletteEntry().getProjectRelativeTypePath());
					}
				}
			}
		}
		
		for (Segment segment : system.getSystemConfiguration().getSystemConfigurationNetwork().getSegments()) {
			segment.setTypePath(segment.getPaletteEntry().getProjectRelativeTypePath());
		}
		
		for (Application application : system.getApplication()) {
			for (FB fb : application.getFBNetwork().getFBs()) {
				System.out.println(fb.getName());
				if (fb.getPaletteEntry() != null) {
					fb.setFbtPath(fb.getPaletteEntry().getProjectRelativeTypePath());
				}
			}
		}
	}

	/**
	 * Gets the system for name.
	 * 
	 * @param string
	 *            the string
	 * 
	 * @return the system for name
	 */
	public AutomationSystem getSystemForName(final String string) {
		Enumeration<AutomationSystem> enumeration = systems.elements();
		while (enumeration.hasMoreElements()) {
			AutomationSystem sys = enumeration.nextElement();
			if (sys.getName().equals(string)) {
				return sys;
			}
		}
		return null;
	}
	
	/**
	 * Delete sub app. --> not used in this version
	 * 
	 * @param subApp
	 *            the sub app
	 */
	public void deleteSubApp(final SubAppView subApp) {
		AutomationSystem system = subApp.getUiSubAppNetwork().getRootApplication().getFbNetwork().getApplication().getAutomationSystem();
		IProject project = system.getProject();
		if (project != null) {
			IPath path = project.getLocation().append(
					subApp.getUiSubAppNetwork().getFileName());
			boolean success = (new File(path.toOSString())).delete();
			if (!success) {
				// Deletion failed - error log
			}
		}

	}

	/**
	 * Checks if is valid app name.
	 * 
	 * @param text
	 *            the text
	 * @param selectedSystem
	 *            the selected system
	 * 
	 * @return true, if is valid app name
	 */
	public static boolean isValidAppName(final String text,
			final AutomationSystem selectedSystem) {
		for (Iterator<Application> iterator = selectedSystem.getApplication()
				.iterator(); iterator.hasNext();) {
			Application app = (Application) iterator.next();
			if (text.equalsIgnoreCase(app.getName())) {
				return false;
			}
		}
		if (selectedSystem.getName().equals(text)) {
			return false;
		}
		return true;
	}

	/**
	 * Gets the project handle.
	 * 
	 * @param name
	 *            the name
	 * 
	 * @return the project handle
	 */
	private static IProject getProjectHandle(String name) {
		return ResourcesPlugin.getWorkspace().getRoot().getProject(name);
	}

	/**
	 * returns a unique/valid name for a system.
	 * 
	 * @param name
	 *            the name
	 * 
	 * @return a unique/valid system name
	 */
	public static String getValidSystemName(final String name) {

		if (getInstance().getSystemForName(name) == null
				&& !getProjectHandle(name).exists()) {
			return name;
		} else {
			int i = 1;
			String temp = name + "_" + i;
			while (getInstance().getSystemForName(temp) != null
					|| getProjectHandle(temp).exists()) {
				i++;
				temp = name + "_" + i;
			}
			return temp;
		}
	}

	/**
	 * Delets a diagram xml file. At the moment it renames it to "*_deleted"
	 * 
	 * @param diagram
	 * @param system
	 * @param fileName
	 */
	public void deleteDiagram(Diagram diagram, AutomationSystem system,
			String fileName) {
		IProject project = system.getProject();

		if (project != null) {
			IPath path = project.getLocation().append(fileName);
			File toDelete = new File(path.toOSString());
			File deletedFile = new File(path.toOSString() + "_deleted");
			toDelete.renameTo(deletedFile);// (fileName + "_deleted");
		}
	}

	@Override
	public void applicationRemoved(AutomationSystem system, Application app) {
		removeSystemDiagramMapping((Diagram) app.getFBNetwork().eContainer());

		deleteDiagram((Diagram) app.getFBNetwork().eContainer(), system,
				app.getName() + ".xml");
	}

	@Override
	public void newApplicationAdded(AutomationSystem system, Application app) {
		addSystemDiagramMapping((Diagram) app.getFBNetwork().eContainer(),
				system);
	}

	public ITagProvider getTagProvider(Class<?> class1,
			AutomationSystem system) {
		if (!tagProviders.containsKey(system)) {
			tagProviders.put(system, new ArrayList<ITagProvider> ());
		}
		ITagProvider provider = null;
		ArrayList<ITagProvider> tagProviderList = tagProviders.get(system);
		if (tagProviderList != null) {
			for (ITagProvider iTagProvider : tagProviderList) {
				if (iTagProvider.getClass().equals(class1)) {
					provider = iTagProvider;
					break;
				}
			}
		}
		if (provider == null) {
			try {
				Object obj = class1.newInstance();
				if (obj instanceof ITagProvider) {
					provider = (ITagProvider)obj;
					provider.initialzeNewTagProvider();
					saveTagProvider(system, provider);
					tagProviderList.add(provider);
				}
			} catch (InstantiationException e) {
				return null;
			} catch (IllegalAccessException e) {
				return null;
			}
		}
		return provider;
	}

	
	public Palette getPalette(IProject project) {
		AutomationSystem srcSystem = getSystemForName(project.getName());
		if(srcSystem != null){
			return srcSystem.getPalette();
		}
		return TypeLibrary.getInstance().getPalette();
	}


}
