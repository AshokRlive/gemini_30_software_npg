/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.fbtest;

import org.eclipse.swt.widgets.Composite;

/**
 * The Interface IAlgorithmEditorCreator.
 */
public interface IFBTestEditorCreator {

	/**
	 * Creates the algorithm editor.
	 * 
	 * @param parent
	 *            the parent
	 * 
	 * @return the i algorithm editor
	 */
	public IFBTestEditor createFBTestEditor(Composite parent);

}
