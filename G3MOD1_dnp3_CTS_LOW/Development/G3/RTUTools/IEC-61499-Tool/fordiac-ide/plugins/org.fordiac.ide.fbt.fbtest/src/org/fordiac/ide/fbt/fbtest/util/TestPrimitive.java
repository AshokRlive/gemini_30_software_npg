package org.fordiac.ide.fbt.fbtest.util;

import java.util.ArrayList;
import java.util.List;

import org.fordiac.ide.model.libraryElement.InputPrimitive;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;


public class TestPrimitive {
   private Event event;
   private List<DataVariable> data = new ArrayList<DataVariable>();
   private Object relatedModelElement=null;
	
   public TestPrimitive(Event paEvent) {
	   setEvent(paEvent);
   }

   
   public OutputPrimitive getRelatedOutputPrimitive() {
 	if (relatedModelElement instanceof OutputPrimitive) {
	   return (OutputPrimitive)relatedModelElement;
 	} else 
 		return null;
 }
   
   public InputPrimitive getRelatedInputPrimitive() {
	 	if (relatedModelElement instanceof InputPrimitive) {
	 	   return (InputPrimitive)relatedModelElement;
	  	} else 
	  		return null;
   }
   
   
 public void setRelatedModelElement(OutputPrimitive relatedModelElement) {
 	this.relatedModelElement = relatedModelElement;
 }   

 public void setRelatedModelElement(InputPrimitive relatedModelElement) {
	 this.relatedModelElement = relatedModelElement;
 }   
   
   
public Event getEvent() {
	return event;
}

public void setEvent(Event event) {
	this.event = event;
}

public List<DataVariable> getData() {
	return data;
}

public void setData(List<DataVariable> data) {
	this.data = data;
}
   
public boolean addData(DataVariable paData) {
	return this.data.add(paData);
}   

public String toString() {
	String retval="";
	if (null!=event) {
		retval=event.toString();
	}
	if (null!=data) {
		retval+=data.toString();
	}
	return retval;
}

}
