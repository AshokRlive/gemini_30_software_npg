package org.fordiac.ide.fbt.fbtest.util;

public class Event {
	private String eventName;
	private int eventID;
	private boolean input;
	
	public Event(String paEventName, int paEventID, boolean paIsInput) {
		eventName = paEventName;
		eventID = paEventID;
		input = paIsInput;
	}
	
	public boolean isInput() {
		return input;
	}
	public void setInput(boolean input) {
		this.input = input;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public int getEventID() {
		return eventID;
	}
	public void setEventID(int eventID) {
		this.eventID = eventID;
	}
	
	public String toString() {
		String retval;
		if (input) {
			retval="IN: ";
		} else {
			retval="OUT: ";
		}
		retval+=eventName+"("+eventID+")";
		return retval;
	}
	
}
