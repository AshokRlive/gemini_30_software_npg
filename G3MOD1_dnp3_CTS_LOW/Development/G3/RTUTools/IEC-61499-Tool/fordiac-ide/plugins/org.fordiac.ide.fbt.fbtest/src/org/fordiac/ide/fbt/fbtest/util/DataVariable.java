package org.fordiac.ide.fbt.fbtest.util;

public class DataVariable {
	private String dataName;
	private String dataType;
	private String dataValue;
	private int dataID;
	
	private boolean input;
	
	public DataVariable(String paDataName, int paDataID, String paDataType,boolean paIsInput, String paValue) {
		dataName = paDataName;
		setDataType(paDataType);
		dataID = paDataID;
		input = paIsInput;
		dataValue = paValue;
	}
	
	public String getDataName() {
		return dataName;
	}
	public void setDataName(String dataName) {
		this.dataName = dataName;
	}
	public int getDataID() {
		return dataID;
	}
	public void setDataID(int dataID) {
		this.dataID = dataID;
	}
	public boolean isInput() {
		return input;
	}
	public void setInput(boolean input) {
		this.input = input;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getDataValue() {
		return dataValue;
	}

	public void setDataValue(String dataValue) {
		this.dataValue = dataValue;
	}
	
	public String toString() {
		String retval;
		if (input) {
			retval="IN: ";
		} else {
			retval="OUT: ";
		}
		retval+=dataName+"("+dataID+") := "+dataValue;
		return retval;
	}
	
	
}
