/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.fbtest;

import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.ISourceViewerExtension2;
import org.eclipse.swt.widgets.Control;

/**
 * The Interface IAlgorithmEditor.
 */
public interface IFBTestEditor extends ISourceViewer,
		ISourceViewerExtension2 {

	/**
	 * Gets the control.
	 * 
	 * @return the control
	 */
	public Control getControl();
}
