/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.fbtest;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

/**
 * The Class TextEditorCreator.
 */
public class TextEditorCreator implements IFBTestEditorCreator {

	/**
	 * Instantiates a new text editor creator.
	 */
	public TextEditorCreator() {
	}

	@Override
	public IFBTestEditor createFBTestEditor(final Composite parent) {
		TextEditor editor = new TextEditor(parent, null, null, false,
				SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		return editor;
	}

}
