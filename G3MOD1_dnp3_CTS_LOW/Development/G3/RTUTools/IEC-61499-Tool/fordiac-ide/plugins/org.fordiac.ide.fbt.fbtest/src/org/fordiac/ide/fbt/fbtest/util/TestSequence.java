package org.fordiac.ide.fbt.fbtest.util;

import java.util.ArrayList;
import java.util.List;

import org.fordiac.ide.model.libraryElement.ServiceSequence;

public class TestSequence {
   private List<TestTransaction> testTransactions=new ArrayList<TestTransaction>();
   private boolean isSuccess=false;
   private String name="";
   private ServiceSequence relatedModelElement=null;
   
public ServiceSequence getRelatedModelElement() {
	return relatedModelElement;
}

public void setRelatedModelElement(ServiceSequence relatedModelElement) {
	this.relatedModelElement = relatedModelElement;
}

public List<TestTransaction> getTestTransactions() {
	return testTransactions;
}

public void setTestTransactions(List<TestTransaction> testTransactions) {
	this.testTransactions = testTransactions;
}
	
public void addTestTransaction(TestTransaction testTransaction) {
	this.testTransactions.add(testTransaction);
}

public String toString() {
	return testTransactions.toString();
}

public boolean isSuccess() {
	return isSuccess;
}

public void setSuccess(boolean isSuccess) {
	this.isSuccess = isSuccess;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

}
