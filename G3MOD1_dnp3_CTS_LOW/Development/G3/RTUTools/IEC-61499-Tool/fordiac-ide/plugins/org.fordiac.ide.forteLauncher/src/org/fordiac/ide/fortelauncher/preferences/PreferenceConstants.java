/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fortelauncher.preferences;

/**
 * Constant definitions for plug-in preferences.
 */
public class PreferenceConstants {

	/** The Constant P_PATH. */
	public static final String P_PATH = "pathPreference";
}
