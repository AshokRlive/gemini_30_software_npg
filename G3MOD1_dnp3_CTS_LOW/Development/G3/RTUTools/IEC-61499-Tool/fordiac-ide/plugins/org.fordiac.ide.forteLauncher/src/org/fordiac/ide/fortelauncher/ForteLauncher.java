/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fortelauncher;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.fordiac.ide.fortelauncher.preferences.PreferenceConstants;
import org.fordiac.ide.runtime.IRuntimeLauncher;
import org.fordiac.ide.runtime.LaunchParameter;
import org.fordiac.ide.runtime.LaunchRuntimeException;
import org.fordiac.ide.runtime.LaunchRuntimeUtils;

/**
 * The Class ForteLauncher.
 */
public class ForteLauncher implements IRuntimeLauncher {

	private final ArrayList<LaunchParameter> params = new ArrayList<LaunchParameter>();

	/**
	 * Instantiates a new forte launcher.
	 */
	public ForteLauncher() {
		// Define the initial parameters for the runtime
		setParam(Messages.ForteLauncher_LABEL_PortParam, "61499"); //$NON-NLS-1$

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.runtime.IRuntimeLauncher#getName()
	 */
	@Override
	public String getName() {
		return "Local FORTE"; //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.runtime.IRuntimeLauncher#launch()
	 */
	@Override
	public void launch() throws LaunchRuntimeException {
		try {
			int port = Integer.parseInt(params.get(0).getValue());
			if ((port < 1024) || (port > 65535)) {
				throw new NumberFormatException();
			}
			String runtime = Activator.getDefault().getPreferenceStore()
					.getString(PreferenceConstants.P_PATH);
			LaunchRuntimeUtils
					.startRuntime("FORTE", runtime, new File(runtime)
							.getParentFile().getAbsolutePath(), "-c "
							+  "localhost:"
							+ params.get(0).getValue());
		} catch (NumberFormatException num) {
			throw new LaunchRuntimeException(
					Messages.ForteLauncher_ERROR_WrongPort);
		} catch (Exception ex) {
			System.err.println(ex.toString());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.runtime.IRuntimeLauncher#getNumParameters()
	 */
	@Override
	public int getNumParameters() {
		return params.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.runtime.IRuntimeLauncher#getParams()
	 */
	@Override
	public List<LaunchParameter> getParams() {
		return params;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.runtime.IRuntimeLauncher#setParam(java.lang.String,
	 * java.lang.String)
	 */
	public LaunchParameter setParam(final String name, final String value) {
		boolean found = false;
		for (int i = 0; i < params.size(); i++) {
			if (params.get(i).getName().equals(name)) {
				params.get(i).setValue(value);
				found = true;
				if (found)
					return params.get(i);
			}
		}
		LaunchParameter param = new LaunchParameter();
		param.setName(name);
		param.setValue(value);
		params.add(param);
		return param;
	}
}
