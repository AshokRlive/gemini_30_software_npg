/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typemanagement.navigator;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.fordiac.ide.model.libraryElement.AutomationSystem;

public class TypeLibRootElement implements IAdaptable{

	AutomationSystem system;
	
	public TypeLibRootElement(AutomationSystem system) {
		this.system = system;
	}
	
	public AutomationSystem getSystem(){
		return system;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Object getAdapter(Class adapter) {
		if((adapter == IResource.class) || (adapter == IProject.class)){
			return getSystem().getProject();
		}
		return null;
	}

	
}
