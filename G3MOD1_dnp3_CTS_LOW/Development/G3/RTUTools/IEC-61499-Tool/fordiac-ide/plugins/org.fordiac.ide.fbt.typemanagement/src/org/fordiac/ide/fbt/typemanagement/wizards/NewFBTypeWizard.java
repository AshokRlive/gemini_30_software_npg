/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typemanagement.wizards;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.fordiac.ide.fbt.typemanagement.Activator;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.model.libraryElement.VersionInfo;
import org.fordiac.ide.typeexport.CommonElementExporter;
import org.fordiac.ide.typeimport.ImportUtils;
import org.fordiac.ide.typelibrary.TypeLibrary;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The Class NewFBTypeWizard.
 */
public class NewFBTypeWizard extends Wizard implements INewWizard {

	/** The page1. */
	
	private IStructuredSelection selection;
	
	NewFBTypeWizardPage page1;


	/** The format. */
	private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	private PaletteEntry entry;

	/**
	 * Instantiates a new new fb type wizard.
	 */
	public NewFBTypeWizard() {
		setWindowTitle("New Type");
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
	 * org.eclipse.jface.viewers.IStructuredSelection)
	 */
	@Override
	public void init(final IWorkbench workbench,
			final IStructuredSelection selection) {
		this.selection = selection;
        setWindowTitle("New Type"); 
    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		page1 = createNewFBTypeWizardPage();
		addPage(page1);
	}

	protected NewFBTypeWizardPage createNewFBTypeWizardPage() {
		return new NewFBTypeWizardPage(selection);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		String typeName = page1.getFileName();
		File template = page1.getTemplate();

		if (!checkTemplateAvailable(template.getAbsolutePath())) {
			return false;
		}
		
		IFile targetTypeFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(page1.getContainerFullPath() + 
				"/" + typeName));

		try {
			ImportUtils.copyFile(template, targetTypeFile);			
			return finishTypeCreation(targetTypeFile);

		} catch (Exception e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		return false;
	}

	private boolean checkTemplateAvailable(String templatePath) {
		if (!new File(templatePath).exists()) {
			templateNotAvailable(templatePath);
			return false;
		}
		return true;
	}

	private void templateNotAvailable(String templatePath) {
		MessageBox mbx = new MessageBox(Display.getDefault().getActiveShell());
		mbx.setMessage("Template not available! (" + templatePath + ")");
		mbx.open();
	}

	private boolean finishTypeCreation(IFile targetTypeFile) {
		Palette palette = SystemManager.getInstance().getPalette(targetTypeFile.getProject());
		entry = TypeLibrary.getPaletteEntry(palette, targetTypeFile);
		
		if(null == entry){
			//refresh the palette and retry to fetch the entry
			TypeLibrary.refreshPalette(palette);
			entry = TypeLibrary.getPaletteEntry(palette, targetTypeFile);
		}
		
		LibraryElement type = entry.getType();
		type.setId(EcoreUtil.generateUUID());		
		type.setName(TypeLibrary.getTypeNameFromFile(targetTypeFile));
		
		if (0 != type.getVersionInfo().size()) {
			VersionInfo versionInfo = type.getVersionInfo().get(0);
			versionInfo.setDate(format.format(new Date(System
					.currentTimeMillis())));
		}
		
		CommonElementExporter.saveType(entry);
		entry.setType(type);
		
		if(page1.getOpenType()){
			openTypeEditor(entry);
		}
		
		

		return true;
	}
	
	private void openTypeEditor(PaletteEntry entry) {		
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		
		IEditorDescriptor desc = PlatformUI.getWorkbench().
				        getEditorRegistry().getDefaultEditor(entry.getFile().getName());
		try {
			page.openEditor(new FileEditorInput(entry.getFile()), desc.getId());
		} catch (PartInitException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}		
		
	}

	public PaletteEntry getPaletteEntry() {
		return entry;
	}
}
