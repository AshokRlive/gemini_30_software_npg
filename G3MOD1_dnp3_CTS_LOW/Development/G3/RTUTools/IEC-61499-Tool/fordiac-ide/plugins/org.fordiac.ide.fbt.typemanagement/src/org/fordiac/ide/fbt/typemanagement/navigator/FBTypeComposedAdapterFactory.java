/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typemanagement.navigator;

import java.util.ArrayList;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.fordiac.ide.model.data.provider.DataItemProviderAdapterFactory;
import org.fordiac.ide.model.libraryElement.provider.LibraryElementItemProviderAdapterFactory;

public class FBTypeComposedAdapterFactory {
	
	private static ComposedAdapterFactory fbTypeCompAdapterFactory;
	
	public final static ComposedAdapterFactory getAdapterFactory()
	{
		if (fbTypeCompAdapterFactory == null){
			fbTypeCompAdapterFactory = new ComposedAdapterFactory(createFactoryList());
		}
		return fbTypeCompAdapterFactory;
	}
	
	public final static ArrayList<AdapterFactory> createFactoryList()
	{
		ArrayList<AdapterFactory> factories = new ArrayList<AdapterFactory>();
		factories.add(new LibraryElementItemProviderAdapterFactory());
		factories.add(new DataItemProviderAdapterFactory());
		return factories;
	}
}
