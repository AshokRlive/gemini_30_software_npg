/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typemanagement.util;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.dialogs.PatternFilter;
import org.fordiac.ide.model.libraryElement.provider.TransientBasicFBTypeListItemProvider;
import org.fordiac.ide.model.libraryElement.provider.TransientInterfaceListItemProvider;
import org.fordiac.ide.model.libraryElement.provider.TransientLibraryElementItemProvider;

public class TypeListPatternFilter extends PatternFilter {
	
	public TypeListPatternFilter() {
		super();
		setIncludeLeadingWildcard(true);
	}

	protected boolean isParentMatch(Viewer viewer, Object element){
		//prevent children filtering if is a file, avoids type loading on filtering
		if((element instanceof IFolder) ||
				(element instanceof IProject)){
			return super.isParentMatch(viewer, element);
		}
		
		return false;
	}
	
	protected boolean isLeafMatch(Viewer viewer, Object element){
		if((element instanceof EObject) ||
		   (element instanceof TransientInterfaceListItemProvider) ||
		   (element instanceof TransientBasicFBTypeListItemProvider) ||
		   (element instanceof TransientLibraryElementItemProvider)){
			//do not filter on type content
			return true;
		}
		//TODO add also matching for other elements e.g., description ev. in is parent Match einbauen
		return super.isLeafMatch(viewer, element);
	}

}
