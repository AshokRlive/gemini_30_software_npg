/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typemanagement;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * The Class FBTypeManagement.
 */
public class FBTypeManagement implements IPerspectiveFactory {

	/** The factory. */
	private IPageLayout factory;

	/*
	 * (non-Javadoc)
	 * 
	 * 
	 * @seeorg.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.
	 * IPageLayout)
	 */
	public void createInitialLayout(final IPageLayout factory) {
		this.factory = factory;
		factory.setEditorAreaVisible(true);
		addViews();
		addActionSets();
		addNewWizardShortcuts();
		addPerspectiveShortcuts();
		addViewShortcuts();
	}

	/**
	 * Adds the views.
	 */
	private void addViews() {
		IFolderLayout topLeft = factory.createFolder("topLeft", //$NON-NLS-1$
				IPageLayout.LEFT, 0.2f, factory.getEditorArea());
		topLeft.addView("org.fordiac.ide.fbt.typemanagement.navigator.view");//$NON-NLS-1$
		
		IFolderLayout topRight = factory.createFolder(
			    "topRight", IPageLayout.RIGHT, 0.8f, factory.getEditorArea()); //$NON-NLS-2$
		topRight.addView(IPageLayout.ID_OUTLINE);
		
		IFolderLayout bottom = factory.createFolder(
				"bottom", IPageLayout.BOTTOM, 0.78f, factory.getEditorArea()); //$NON-NLS-1$
		bottom.addView(IPageLayout.ID_PROP_SHEET);
	}

	/**
	 * Adds the action sets.
	 */
	private void addActionSets() {
		// currently not used
	}

	/**
	 * Adds the perspective shortcuts.
	 */
	private void addPerspectiveShortcuts() {
		factory.addPerspectiveShortcut("org.fordiac.ide.SystemPerspective");
		factory.addPerspectiveShortcut("org.fordiac.ide.deployment.ui.perspectives.DeploymentPerspective"); //$NON-NLS-1$
		factory.addPerspectiveShortcut("org.fordiac.ide.fbt.typemanagement.perspective1");
	}

	/**
	 * Adds the new wizard shortcuts.
	 */
	private void addNewWizardShortcuts() {
		factory.addNewWizardShortcut("org.fordiac.systemmanagement.ui.wizard.NewSystemWizard");
		factory.addNewWizardShortcut("org.fordiac.systemmanagement.ui.wizard.NewApplicationWizard");
		factory.addNewWizardShortcut("org.fordiac.ide.fbt.typemanagement.wizards.NewFBTypeWizard");
	}

	/**
	 * Adds the view shortcuts.
	 */
	private void addViewShortcuts() {
		factory.addShowViewShortcut(IPageLayout.ID_OUTLINE);
		factory.addShowViewShortcut(IPageLayout.ID_PROP_SHEET);
	}

}
