/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typemanagement.util;

import org.eclipse.core.resources.IFile;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.typelibrary.TypeLibrary;
import org.fordiac.systemmanagement.SystemManager;

public class FBTypeUtils {

	static public Palette getPalletteForFBTypeFile(IFile element) {
		Palette palette = null;
		if(null != element){
			if(element.getProject().getName().equals(TypeLibrary.TOOL_LIBRARY_PROJECT_NAME)){ 
			  palette = TypeLibrary.getInstance().getPalette(); 
			} 
			else{
				AutomationSystem system = SystemManager.getInstance().getSystemForName(element.getProject().getName());
				if(null != system){
					palette = system.getPalette();
				}
			}
		}
		return palette;
	}
	
	
}
