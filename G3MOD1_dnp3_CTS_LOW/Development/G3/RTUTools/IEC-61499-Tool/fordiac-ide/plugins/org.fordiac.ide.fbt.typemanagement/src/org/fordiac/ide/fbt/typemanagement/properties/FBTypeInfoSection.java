/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typemanagement.properties;

import org.eclipse.core.resources.IFile;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.typelibrary.TypeLibrary;
import org.fordiac.ide.util.properties.CompilableTypeInfoSection;
import org.fordiac.systemmanagement.SystemManager;

public class FBTypeInfoSection extends CompilableTypeInfoSection {

	@Override
	protected CommandStack getCommandStack(IWorkbenchPart part, Object input) {
		return null;
	}

	@Override
	protected LibraryElement getInputType(Object input) {
		if(input instanceof IFile){
			IFile file = ((IFile) input);
			return (TypeLibrary.getPaletteGroup( SystemManager.getInstance().getPalette(file.getProject()), file.getParent()))
					.getEntry(TypeLibrary.getTypeNameFromFileName(file.getName())).getType();
		}
		return null;
	}
}
