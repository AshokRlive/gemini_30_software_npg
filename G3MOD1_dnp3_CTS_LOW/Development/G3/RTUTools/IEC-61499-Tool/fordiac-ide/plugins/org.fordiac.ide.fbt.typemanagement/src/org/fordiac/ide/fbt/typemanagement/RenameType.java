/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typemanagement;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.mapping.IResourceChangeDescriptionFactory;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RenameParticipant;
import org.eclipse.ltk.core.refactoring.participants.ResourceChangeChecker;
import org.fordiac.ide.typelibrary.TypeLibrary;
import org.fordiac.ide.util.IdentifierVerifyListener;

public class RenameType extends RenameParticipant {
	
	@Override
	protected boolean initialize(Object element) {
		return (element instanceof IFile);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RefactoringStatus checkConditions(IProgressMonitor pm,
			CheckConditionsContext context) throws OperationCanceledException {
		
		ResourceChangeChecker resChecker = (ResourceChangeChecker) context.getChecker(ResourceChangeChecker.class);
		IResourceChangeDescriptionFactory deltaFactory = resChecker.getDeltaFactory();
		IResourceDelta[] affectedChildren = deltaFactory.getDelta().getAffectedChildren();

		return verifyAffectedChildren(affectedChildren);
	}
	
	protected RefactoringStatus verifyAffectedChildren(IResourceDelta[] affectedChildren) {
		for (IResourceDelta resourceDelta : affectedChildren) {
			if (resourceDelta.getMovedFromPath() != null) {
				String name = getTypeName(resourceDelta);
				if(null != name){					
					if (!IdentifierVerifyListener.isValidIdentifier(name)) {
						return getWrongIdentifierErrorStatus();
					}
				}
			}
			else if (resourceDelta.getMovedToPath() == null){
				return verifyAffectedChildren(resourceDelta.getAffectedChildren());
			}
		}
		
		return new RefactoringStatus();
	}

	protected String getTypeName(IResourceDelta resourceDelta) {
		if(resourceDelta.getResource() instanceof IFile){
			return TypeLibrary.getTypeNameFromFile((IFile)resourceDelta.getResource());
		}
		return null;
	}

	protected RefactoringStatus getWrongIdentifierErrorStatus() {
		return RefactoringStatus.createFatalErrorStatus("Type Name is not a valid Identifier");
	}


	@Override
	public Change createChange(IProgressMonitor pm) throws CoreException,
			OperationCanceledException {
		return null;
	}

}
