/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typemanagement;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.fbt.typemanagement.messages"; //$NON-NLS-1$
	
	/** The FB types view_19. */
	public static String FBTypesView_19;
	
	/** The FB types view_20. */
	public static String FBTypesView_20;
	
	/** The FB types view_ erro r_ empty group names not allowed. */
	public static String FBTypesView_ERROR_EmptyGroupNamesNotAllowed;
	
	/** The FB types view_ labe l_ aborted. */
	public static String FBTypesView_LABEL_Aborted;
	
	/** The FB types view_ labe l_ already exists. */
	public static String FBTypesView_LABEL_AlreadyExists;
	
	/** The FB types view_ labe l_ can not be deleted. */
	public static String FBTypesView_LABEL_CanNotBeDeleted;
	
	/** The FB types view_ labe l_ collapse all. */
	public static String FBTypesView_LABEL_CollapseAll;
	
	/** The FB types view_ labe l_ create group. */
	public static String FBTypesView_LABEL_CreateGroup;
	
	/** The FB types view_ labe l_ delete. */
	public static String FBTypesView_LABEL_Delete;
	
	/** The FB types view_ labe l_ delete entry. */
	public static String FBTypesView_LABEL_DeleteEntry;
	
	/** The FB types view_ labe l_ error. */
	public static String FBTypesView_LABEL_Error;
	
	/** The FB types view_ labe l_ expand all. */
	public static String FBTypesView_LABEL_ExpandAll;
	
	/** The FB types view_ labe l_ group can not be deleted. */
	public static String FBTypesView_LABEL_GroupCanNotBeDeleted;
	
	/** The FB types view_ labe l_ group name. */
	public static String FBTypesView_LABEL_GroupName;
	
	/** The FB types view_ labe l_ group name title. */
	public static String FBTypesView_LABEL_GroupNameTitle;
	
	/** The FB types view_ labe l_ open. */
	public static String FBTypesView_LABEL_Open;
	
	/** The FB types view_ labe l_ refresh. */
	public static String FBTypesView_LABEL_Refresh;
	
	/** The FB types view_ labe l_ too l_ library. */
	public static String FBTypesView_LABEL_TOOL_Library;
	
	/** The FB types view_ labe l_x entry can not be deleted_ list of items. */
	public static String FBTypesView_LABEL_xEntryCanNotBeDeleted_ListOfItems;
	
	/** The FB types view_ toolti p_ collapse all. */
	public static String FBTypesView_TOOLTIP_CollapseAll;
	
	/** The FB types view_ toolti p_ expand all. */
	public static String FBTypesView_TOOLTIP_ExpandAll;
	
	/** The FB types view_ toolti p_ refresh. */
	public static String FBTypesView_TOOLTIP_Refresh;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
