package org.fordiac.ide.fbt.typemanagement;

import org.eclipse.core.resources.IFile;
import org.eclipse.gef.dnd.TemplateTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.navigator.CommonDragAdapterAssistant;
import org.fordiac.ide.fbt.typemanagement.util.FBTypeUtils;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.typelibrary.TypeLibrary;

public class FBTypeDragAssistant extends CommonDragAdapterAssistant {

	public FBTypeDragAssistant() {
	}

	@Override
	public Transfer[] getSupportedTransferTypes() {
		Transfer[] types = new Transfer[] { TemplateTransfer.getInstance() };
		return types;
	}

	@Override
	public void dragStart(DragSourceEvent anEvent,
			IStructuredSelection aSelection) {
		// TemplateTransfer.getInstance().setTemplate(null);
		if (aSelection.getFirstElement() instanceof IFile) {
			IFile fbTypeFile = (IFile) aSelection.getFirstElement();
			Palette fbPalette = FBTypeUtils
					.getPalletteForFBTypeFile(fbTypeFile);
			if (fbPalette != null) {
				PaletteEntry entry = TypeLibrary.getPaletteEntry(
						fbPalette, fbTypeFile);
				if (entry != null)
					TemplateTransfer.getInstance().setTemplate(entry);

			} else {
				anEvent.doit = false;
			}
		}
		super.dragStart(anEvent, aSelection);
	}

	@Override
	public boolean setDragData(DragSourceEvent anEvent,
			IStructuredSelection aSelection) {
		if (aSelection.getFirstElement() instanceof IFile) {
			IFile fbTypeFile = (IFile) aSelection.getFirstElement();
			Palette fbPalette = FBTypeUtils
					.getPalletteForFBTypeFile(fbTypeFile);
			if (fbPalette != null) {
				PaletteEntry entry = TypeLibrary.getPaletteEntry(
						fbPalette, fbTypeFile);

				if (null != entry) {
					anEvent.data = entry;
					return true;
				}
			}
		}

		return false;
	}

}
