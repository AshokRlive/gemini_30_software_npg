package org.fordiac.ide.fbt.typemanagement.navigator;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.fordiac.ide.util.imageprovider.FordiacImage;

public class ToolLibraryLabelProvider implements ILabelProvider {

	@Override
	public void addListener(ILabelProviderListener listener) {
	}

	@Override
	public void dispose() {

	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {

	}

	@Override
	public Image getImage(Object element) {
		if (element instanceof IProject) {
			return FordiacImage.ICON_TypeNavigator.getImage();
		}
		else if (element instanceof IFolder && ((IFolder)element).isLinked()) {
			return FordiacImage.ICON_TypeNavigator.getImage();
		}
		else {
			return null;
		}
	}

	@Override
	public String getText(Object element) {
		return null;
	}

}
