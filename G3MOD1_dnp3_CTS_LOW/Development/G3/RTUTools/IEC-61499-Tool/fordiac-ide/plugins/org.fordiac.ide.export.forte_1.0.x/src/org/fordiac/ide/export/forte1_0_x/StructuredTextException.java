/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.export.forte1_0_x;

public class StructuredTextException extends Exception {

	public StructuredTextException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5310757976250294807L;


}
