/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editparts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;
import org.fordiac.ide.application.SpecificLayerEditPart;
import org.fordiac.ide.application.policies.UISubAppNetworkXYLayoutEditPolicy;
import org.fordiac.ide.gef.editparts.IDiagramEditPart;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.UISubAppNetwork;

/**
 * The Class UISubAppNetworkEditPart.
 */
public class UISubAppNetworkEditPart extends EditorWithInterfaceEditPart
		implements IDiagramEditPart {

	/** The content adapter. */
	private final EContentAdapter contentAdapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			int type = notification.getEventType();
			switch (type) {
			case Notification.ADD:
			case Notification.ADD_MANY:
			case Notification.REMOVE:
			case Notification.REMOVE_MANY:
				refreshChildren();
				break;
			case Notification.SET:
				refreshVisuals();
				break;
			}
		}

	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#refresh()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void refresh() {
		super.refresh();
		for (Iterator iterator = getChildren().iterator(); iterator.hasNext();) {
			EditPart ep = (EditPart) iterator.next();
			if (ep instanceof SubAppForFBNetworkEditPart) {
				ep.refresh();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (!isActive()) {
			super.activate();
			((Notifier) getModel()).eAdapters().add(contentAdapter);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((Notifier) getModel()).eAdapters().remove(contentAdapter);
		}
	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public UISubAppNetwork getCastedModel() {
		return (UISubAppNetwork) getModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@Override
	protected List<?> getModelChildren() {
		ArrayList<Object> children = new ArrayList<Object>();
		children.addAll(getCastedModel().getChildren());
		children.addAll(super.getModelChildren());
		children.addAll(getCastedModel().getInterfaceElements());
		return children;
	}

	/**
	 * Adds the childEditParts figure to the corresponding container.
	 * 
	 * @param childEditPart
	 *            the child edit part
	 * @param index
	 *            the index
	 */
	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		if (childEditPart instanceof InterfaceEditPart
				&& !((InterfaceEditPart) childEditPart).isInput()) {
			IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
			getLeftInterfaceContainer().add(child);
		} else if (childEditPart instanceof InterfaceEditPart
				&& ((InterfaceEditPart) childEditPart).isInput()) {
			IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
			getRightInterfaceContainer().add(child);
		} else if (childEditPart instanceof SpecificLayerEditPart) {
			String layer = ((SpecificLayerEditPart) childEditPart).getSpecificLayer();
			IFigure layerFig = getLayer(layer);
			if (layerFig != null) {
				IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
				// super.addChildVisual(childEditPart, index);
				layerFig.add(child);
			} else { // if layer does not exist use default layer
				super.addChildVisual(childEditPart, index);
			}
		} else {
			super.addChildVisual(childEditPart, index);
		}
	}

	/**
	 * Removes the childEditParts figures from the correct container.
	 * 
	 * @param childEditPart
	 *            the child edit part
	 */
	@Override
	protected void removeChildVisual(final EditPart childEditPart) {
		if (childEditPart instanceof InterfaceEditPart
				&& !((InterfaceEditPart) childEditPart).isInput()) {
			IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
			getLeftInterfaceContainer().remove(child);
		} else if (childEditPart instanceof InterfaceEditPart
				&& ((InterfaceEditPart) childEditPart).isInput()) {
			IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
			getRightInterfaceContainer().remove(child);
		} else if (childEditPart instanceof SpecificLayerEditPart) {
			String layer = ((SpecificLayerEditPart) childEditPart).getSpecificLayer();
			IFigure layerFig = getLayer(layer);
			if (layerFig != null) {
				IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
				layerFig.remove(child);
			} else { // if layer does not exist use default layer
				super.removeChildVisual(childEditPart);
			}
		} else {
			super.removeChildVisual(childEditPart);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.iec61499.ui.editparts.AbstractEMFEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE,
				new RootComponentEditPolicy());
		// handles constraint changes (e.g. moving and/or resizing) of model
		// elements and creation of new model elements
		installEditPolicy(EditPolicy.LAYOUT_ROLE,
				new UISubAppNetworkXYLayoutEditPolicy());

	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.IDiagramEditPart#getDiagram()
	 */
	@Override
	public Diagram getDiagram() {
		return getCastedModel();
	}

}
