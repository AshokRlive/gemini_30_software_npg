/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.policies;

import java.util.Iterator;

import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.SelectionEditPolicy;
import org.eclipse.gef.requests.AlignmentRequest;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;

/**
 * The Class FBSelectionPolicy.
 * Highlights the In/Outconnections of an FB. 
 * 
 * Returns AlignmentCommand for alignment of FBs.
 * 
 */
public class FBSelectionPolicy extends SelectionEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.SelectionEditPolicy#hideSelection()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected void hideSelection() {
		for (Iterator iterator = ((FBEditPart) getHost()).getChildren().iterator(); iterator
				.hasNext();) {

			Object object = iterator.next();
			if (object instanceof InterfaceEditPart) {
				((InterfaceEditPart) object).setInOutConnectionsWidth(0);

			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.SelectionEditPolicy#showSelection()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected void showSelection() {
		for (Iterator iterator = ((FBEditPart) getHost()).getChildren().iterator(); iterator
				.hasNext();) {
			Object object = iterator.next();
			if (object instanceof InterfaceEditPart) {
				((InterfaceEditPart) object).setInOutConnectionsWidth(2);

			}
		}
	}

	
	/**
	 * @see org.eclipse.gef.EditPolicy#getCommand(org.eclipse.gef.Request)
	 */
	public Command getCommand(Request request) {
		Object type = request.getType();
		if (REQ_ALIGN.equals(type))
			return getAlignCommand((AlignmentRequest) request);

		return null;
	}
	
	/**
	 * Returns the command contribution to an alignment request
	 * 
	 * @param request
	 *            the alignment request
	 * @return the contribution to the alignment
	 */
	protected Command getAlignCommand(AlignmentRequest request) {
		AlignmentRequest req = new AlignmentRequest(REQ_ALIGN_CHILDREN);
		req.setEditParts(getHost());
		req.setAlignment(request.getAlignment());
		req.setAlignmentRectangle(request.getAlignmentRectangle());
		return getHost().getParent().getCommand(req);
	}
	
}
