/*******************************************************************************
 * Copyright (c) 2014 - 2017 fortiss GmbH
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Alois Zoitl, Monika Wenger - initial implementation
 *******************************************************************************/
package org.fordiac.ide.application.properties;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.application.editparts.ConnectionEditPart;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.ui.ConnectionView;

public class ConnectionSection extends org.fordiac.ide.gef.properties.ConnectionSection {

	@Override
	protected CommandStack getCommandStack(IWorkbenchPart part, Object input) {
		if(part instanceof FBNetworkEditor){
			return ((FBNetworkEditor)part).getFBEditorCommandStack();
		}
		return null;
	}

	@Override
	protected Connection getInputType(Object input) {
		if(input instanceof ConnectionEditPart){
			return ((ConnectionView)((ConnectionEditPart) input).getModel()).getConnectionElement();
		}
		return null;
	}

}
