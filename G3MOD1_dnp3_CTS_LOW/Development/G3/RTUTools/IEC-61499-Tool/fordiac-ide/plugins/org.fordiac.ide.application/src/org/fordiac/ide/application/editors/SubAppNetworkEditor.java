/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.jface.action.IAction;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.application.utilities.ApplicationUIFBNetworkManager;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The Class SubAppNetworkEditor.
 */
public class SubAppNetworkEditor extends FBNetworkEditor {

	/** The adapter. */
	private EContentAdapter adapter= new EContentAdapter() {

		@Override
		public void notifyChanged(final Notification notification) {
			int type = notification.getEventType();
			int featureId = notification.getFeatureID(Application.class);

			switch (type) {
				case Notification.SET:
					if (featureId == LibraryElementPackage.SUB_APP__NAME) {
						setPartName(getSubApp().getName());
					}
			}
			firePropertyChange(PROP_DIRTY);
		}

	};

	@Override
	public void dispose() {
		// PaletteViewer viewer = getEditDomain().getPaletteViewer();
		//
		if (adapter != null && getDiagramModel() != null) {
			if (getSubApp().eAdapters().contains(adapter)) {
				getSubApp().eAdapters().remove(adapter);
				adapter = null;
			}
		}
		super.dispose();
		getEditDomain().setPaletteViewer(null);

	}

	@Override
	protected ContextMenuProvider getContextMenuProvider(
			final ScrollingGraphicalViewer viewer,
			final ZoomManager zoomManager) {
		ContextMenuProvider cmp = new UIFBNetworkContextMenuProvider(this,
				getActionRegistry(), zoomManager, getSystem().getPalette()) {
			@Override
			protected IAction getMapAction(IEditorPart activeEditor,
					ResourceView resView) {
				return null;
			}
		};
		return cmp;
	}
	
	//FIXME: this is a quick fix for getting the sub app editor back up. Needs an own DiagramManager, Maybe based on subapptype diagram manager
	class SubAppUIFBNEtworkManager extends ApplicationUIFBNetworkManager{
		
		public void setSubAppNetwork(UISubAppNetwork uiSubAppNetwork){
			setDiagram(uiSubAppNetwork);
			adaptFBNetwork(uiSubAppNetwork.getFunctionBlockNetwork());
		}
	};
	
	@Override
	public Diagram getDiagramModel() {
		return uiFBNetworkManager.getDiagram();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.application.editors.FBNetworkEditor#setModel(org.eclipse
	 * .ui.IEditorInput)
	 */
	@Override
	protected void setModel(final IEditorInput input) {
		if (input instanceof org.fordiac.ide.util.PersistableUntypedEditorInput) {
			org.fordiac.ide.util.PersistableUntypedEditorInput untypedInput = (org.fordiac.ide.util.PersistableUntypedEditorInput) input;
			Object content = untypedInput.getContent();
			if (content instanceof UISubAppNetwork) {
				final UISubAppNetwork uiSubAppNetwork = (UISubAppNetwork) content;		
				
				SubAppUIFBNEtworkManager uiSubappNetworkManager = new SubAppUIFBNEtworkManager();
				uiFBNetworkManager = uiSubappNetworkManager;
				uiSubappNetworkManager.setSubAppNetwork(uiSubAppNetwork);
				
				// register EContentAdapter to be informed on changes of the
				// application name
				getSubApp().eAdapters().add(adapter);
			}
			if (input.getName() != null) {
				setPartName(input.getName());
			}
		}
		super.setModel(input);
	}

	


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.application.editors.FBNetworkEditor#getSystem()
	 */
	@Override
	public AutomationSystem getSystem() {
		return ((UISubAppNetwork)getDiagramModel()).getRootApplication().getFbNetwork().getApplication().getAutomationSystem();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.application.editors.FBNetworkEditor#getFileName()
	 */
	@Override
	public String getFileName() {
		return ((UISubAppNetwork) getDiagramModel()).getFileName();
	}
	
	private SubApp getSubApp() {
		return ((UISubAppNetwork)getDiagramModel()).getSubAppView().getSubApp();
	}
	
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.application.editors.FBNetworkEditor#doSave(org.eclipse.
	 * core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(final IProgressMonitor monitor) {
		// TODO __gebenh error handling if save fails!
		SystemManager.getInstance().saveSystem(getSystem(), true);
		getCommandStack().markSaveLocation();
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

}
