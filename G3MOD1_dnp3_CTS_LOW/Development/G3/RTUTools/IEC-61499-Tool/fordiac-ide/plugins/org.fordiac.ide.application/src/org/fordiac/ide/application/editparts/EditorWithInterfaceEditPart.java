/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editparts;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.graphics.Point;
import org.fordiac.ide.gef.editparts.AbstractDiagramEditPart;
import org.fordiac.ide.gef.editparts.IDiagramEditPart;
import org.fordiac.ide.gef.router.RouterUtil;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.View;

/**
 * This class provides an EditPart for Graphical Editors which have an interface
 * where children can connect.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public abstract class EditorWithInterfaceEditPart extends
		AbstractDiagramEditPart implements IDiagramEditPart {

	/** The left interface container. */
	private Figure leftInterfaceContainer;
	private Figure leftEventContainer;
	private Figure leftVarContainer;

	/** The right interface container. */
	private Figure rightInterfaceContainer;
	private Figure rightEventContainer;
	private Figure rightVarContainer;

	/**
	 * Creates the <code>Figure</code> to be used as this part's <i>visuals</i>.
	 * 
	 * @return a figure
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		leftInterfaceContainer = new Figure();
		leftEventContainer = new Figure();
		leftVarContainer = new Figure();

		rightInterfaceContainer = new Figure();
		rightEventContainer = new Figure();
		rightVarContainer = new Figure();

		ToolbarLayout tbl;
		ToolbarLayout tbl1;
		ToolbarLayout tbl2;
		leftInterfaceContainer.setLayoutManager(tbl = new ToolbarLayout(false));
		leftEventContainer.setLayoutManager(tbl1 = new ToolbarLayout(false));
		leftVarContainer.setLayoutManager(tbl2 = new ToolbarLayout(false));
		tbl.setMinorAlignment(ToolbarLayout.ALIGN_BOTTOMRIGHT);
		tbl1.setMinorAlignment(ToolbarLayout.ALIGN_BOTTOMRIGHT);
		tbl2.setMinorAlignment(ToolbarLayout.ALIGN_BOTTOMRIGHT);
		leftInterfaceContainer.add(leftEventContainer);
		leftInterfaceContainer.add(leftVarContainer);

		rightInterfaceContainer.setLayoutManager((new ToolbarLayout(false)));
		rightEventContainer.setLayoutManager((new ToolbarLayout(false)));
		rightVarContainer.setLayoutManager((new ToolbarLayout(false)));
		rightInterfaceContainer.add(rightEventContainer);
		rightInterfaceContainer.add(rightVarContainer);
		FreeformLayer f = new FreeformLayer();

		f.setBorder(new MarginBorder(10));
		f.setLayoutManager(new FreeformLayout());
		f.setOpaque(false);
		f.add(leftInterfaceContainer);
		f.add(rightInterfaceContainer);

		// Create the static router for the connection layer
		ConnectionLayer connLayer = (ConnectionLayer) getLayer(LayerConstants.CONNECTION_LAYER);
		connLayer.setConnectionRouter(RouterUtil.getConnectionRouter(f));
		return f;

	}

	/**
	 * Gets the casted figure.
	 * 
	 * @return the figure casted to FreeformLayer
	 * 
	 * @see #createFigure()
	 */
	public FreeformLayer getCastedFigure() {
		return (FreeformLayer) getFigure();
	}

	/**
	 * Gets the left interface container.
	 * 
	 * @return contentpane for interface elements on the left side of the editor
	 *         (editpart).
	 * 
	 * @see #addChildVisual(EditPart, int)
	 * @see #removeChildVisual(EditPart)
	 */
	public Figure getLeftInterfaceContainer() {
		return leftInterfaceContainer;
	}

	public Figure getLeftEventInterfaceContainer() {
		return leftEventContainer;
	}

	public Figure getLeftVarInterfaceContainer() {
		return leftVarContainer;
	}

	/**
	 * Gets the right interface container.
	 * 
	 * @return contentpane for interface elements on the right side of the
	 *         editor (editpart).
	 * 
	 * @see #addChildVisual(EditPart, int)
	 * @see #removeChildVisual(EditPart)
	 */
	public Figure getRightInterfaceContainer() {
		return rightInterfaceContainer;
	}

	public Figure getRightEventInterfaceContainer() {
		return rightEventContainer;
	}

	public Figure getRightVarInterfaceContainer() {
		return rightVarContainer;
	}


	/**
	 * Returns a <code>List</code> containing the children model objects for the
	 * interface that editor (editpart) provides.
	 * <P>
	 * Callers must not modify the returned List. Must not return
	 * <code>null</code>.
	 * 
	 * @return the List of interface elements (ports)
	 */
	protected List<?> getInterfaceChildren() {
		return Collections.EMPTY_LIST;
	}

	/**
	 * Adds the child's Figure by default to the {@link #getContentPane()
	 * contentPane}. Override this method for inserting the interface elements
	 * to the defined interface containers ({@link #getLeftInterfaceContainer()}
	 * , {@link #getRightInterfaceContainer()}). Do not forget to override
	 * {@link #removeChildVisual(EditPart childEditPart)} also if you override
	 * this method.
	 * 
	 * @param childEditPart
	 *            the child edit part
	 * @param index
	 *            the index
	 * 
	 * @see #org.eclipse.gef.editparts.AbstractEditPart.addChildVisual(EditPart,
	 *      int)
	 * @see #removeChildVisual(EditPart)
	 */
	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		super.addChildVisual(childEditPart, index);
	}

	/** The control listener. */
	private ControlListener controlListener;

	@SuppressWarnings("rawtypes")
	protected void updateInterfacePosition() {
		if (getParent() != null && getParent().getViewer() != null
				&& getParent().getViewer().getControl() != null) {
			int xRight = Integer.MIN_VALUE;

			for (Iterator iterator = getModelChildren().iterator(); iterator
					.hasNext();) {
				Object obj = iterator.next();
				if (!(obj instanceof FBView || obj instanceof SubAppView)) {
					continue;
				}
				View view = (View) obj;
				int xNew = view.getPosition().getX() + 150;
				Object part = getViewer().getEditPartRegistry().get(view);

				if (part instanceof AbstractGraphicalEditPart) {
					xNew += ((AbstractGraphicalEditPart) part).getFigure()
							.getPreferredSize(-1, -1).width;
				}

				xRight = Math.max(xNew, xRight);

			}

			Point p = getParent().getViewer().getControl().getSize();

			xRight = Math.max(xRight, p.x
					- getRightInterfaceContainer().getSize().width - 20);

			Rectangle rect = new Rectangle(xRight, 0, -1, -1);
			getContentPane().setConstraint(getRightInterfaceContainer(), rect);

			rect = new Rectangle(0, 0, -1, -1);
			getContentPane().setConstraint(getLeftInterfaceContainer(), rect);
		}
	}

	/**
	 * positions the interface containers within the editor.
	 */
	@Override
	protected void refreshVisuals() {
		if (controlListener == null) {
			controlListener = new ControlListener() {

				public void controlResized(final ControlEvent e) {
					updateInterfacePosition();
				}

				public void controlMoved(final ControlEvent e) {
				}

			};
			getParent().getViewer().getControl()
					.addControlListener(controlListener);
		}
		updateInterfacePosition();
	}

	@Override
	public void deactivate() {
		super.deactivate();
		if (controlListener != null && getParent() != null
				&& getParent().getViewer() != null
				&& getParent().getViewer().getControl() != null) {
			getParent().getViewer().getControl()
					.removeControlListener(controlListener);
		}
	}

}
