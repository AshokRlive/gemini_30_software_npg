/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.application.commands;

import org.fordiac.ide.gef.commands.ChangeNameCommand;
import org.fordiac.ide.model.libraryElement.SubApp;

public class SubAppRenameCommand extends ChangeNameCommand {
	
	public SubAppRenameCommand(SubApp fb, String newName){
		super(fb, newName);
	}
}

