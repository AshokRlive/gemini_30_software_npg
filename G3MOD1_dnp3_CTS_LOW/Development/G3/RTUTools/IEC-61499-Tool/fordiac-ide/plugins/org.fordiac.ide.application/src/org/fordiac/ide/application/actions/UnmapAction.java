/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.actions;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.util.commands.UnmapCommand;
import org.fordiac.ide.util.commands.UnmapSubAppCommand;

/**
 * The Class UnmapAction.
 */
public class UnmapAction extends SelectionAction implements
		IObjectActionDelegate {

	/** The Constant ID. */
	public static final String ID = "Unmap"; //$NON-NLS-1$

	/**
	 * Instantiates a new unmap action.
	 * 
	 * @param part the part
	 */
	public UnmapAction(final IWorkbenchPart part) {
		super(part);
		setId(ID);
		setText(Messages.UnmapAction_Unmap_Label);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		CommandStack stack = getCommandStack();
		CompoundCommand cmd = new CompoundCommand();
		cmd.setLabel(Messages.UnmapAction_Unmap_Label);
		for (Iterator<FBView> iterator = selectedFBs.iterator(); iterator
				.hasNext();) {
			FBView fbView = iterator.next();
			UnmapCommand unmapCmd = new UnmapCommand(fbView);
			if (unmapCmd.canExecute()) {
				cmd.add(unmapCmd);
			} 
		}
		for (Iterator<MappedSubAppView> iterator = selectedSubApps.iterator(); iterator
				.hasNext();) {
			MappedSubAppView mappedSubAppView = iterator.next();
			UnmapSubAppCommand unmapSubAppCmd = new UnmapSubAppCommand(
					mappedSubAppView);
			if (unmapSubAppCmd.canExecute()) {
				cmd.add(unmapSubAppCmd);
			}
		}
		if (stack != null) {
			stack.execute(cmd);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action.IAction,
	 *      org.eclipse.ui.IWorkbenchPart)
	 */
	@Override
	public void setActivePart(final IAction action,
			final IWorkbenchPart targetPart) {
		// not used

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(final IAction action) {
		run();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(final IAction action,
			final ISelection selection) {
		// not used
	}

	/** The selected f bs. */
	protected final ArrayList<FBView> selectedFBs = new ArrayList<FBView>();

	/** The selected sub apps. */
	protected final ArrayList<MappedSubAppView> selectedSubApps = new ArrayList<MappedSubAppView>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
	 */
	@Override
	protected boolean calculateEnabled() {
		selectedFBs.clear();
		selectedSubApps.clear();

		for (Iterator<?> iterator = getSelectedObjects().iterator(); iterator
				.hasNext();) {
			Object selected = iterator.next();
			if (selected instanceof EditPart) {
				EditPart ep = (EditPart) selected;
				checkSelectedModelElement(ep.getModel());
			}
		}
		return ((selectedFBs.size() > 0)|| (selectedSubApps.size() > 0));
	}

	protected void checkSelectedModelElement(Object model) {
		//Attention this code here is copied and modified also in ResourceDeleteCommand. On changes here please update there 
		if (model instanceof FBView) {
			FBView view = (FBView)model;
			if (view.getMappedFB() != null
					&& !selectedFBs.contains(view.getMappedFB())) {
				selectedFBs.add(view.getMappedFB());
			}
			if (view.getApplicationFB() != null
					&& !selectedFBs.contains(view)) {
				selectedFBs.add(view);
			}
		}
		if (model instanceof SubAppView) {
			SubAppView view = (SubAppView) model;
			if (view.getMappedSubApp() != null
					&& !selectedSubApps
							.contains(view.getMappedSubApp())) {
				selectedSubApps.add(view.getMappedSubApp());
			}
		}
		if (model instanceof MappedSubAppView) {
			MappedSubAppView view = (MappedSubAppView) model;
			if (view != null && !selectedSubApps.contains(view)) {
				selectedSubApps.add(view);
			}
		}
	}
}
