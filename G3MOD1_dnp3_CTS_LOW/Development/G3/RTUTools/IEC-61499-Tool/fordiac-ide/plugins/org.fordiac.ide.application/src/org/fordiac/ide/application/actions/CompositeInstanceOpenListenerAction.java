/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.actions;

import org.eclipse.jface.action.Action;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.util.IOpenListener;
import org.fordiac.ide.util.OpenListenerManager;

/**
 * The Class SubAppOpenListenerAction.
 */
public class CompositeInstanceOpenListenerAction extends Action {
	private final IOpenListener openListener;

	/**
	 * Instantiates a new sub app open listener action.
	 * 
	 * @param openListener the open listener
	 */
	public CompositeInstanceOpenListenerAction(final IOpenListener openListener) {
		this.openListener = openListener;
		setText("Composite Network");
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		OpenListenerManager.getInstance().setDefaultOpenListener(FB.class,
				"org.fordiac.ide.application.actions.OpenCompositeInstanceViewerAction");
		openListener.run(null);
	}

}