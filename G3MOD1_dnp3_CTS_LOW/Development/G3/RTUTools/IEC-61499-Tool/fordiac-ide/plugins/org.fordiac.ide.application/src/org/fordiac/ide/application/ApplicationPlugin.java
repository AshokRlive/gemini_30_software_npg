/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application;

import java.util.Hashtable;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.ui.controls.Abstract4DIACUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * This class is the main class for the org.fordiac.ide.application plugin. It
 * coordinats changes in data in different views. (e.g. changes in Workspace)
 * 
 * @author Gerhard Ebenhofer (gerhad.ebenhofer@profactor.at)
 */
public class ApplicationPlugin extends Abstract4DIACUIPlugin {
	
	// The plug-in ID
	public static final String PLUGIN_ID = "org.fordiac.ide.application";

	// The shared instance.
	private static ApplicationPlugin plugin;

	/**
	 * The constructor.
	 */
	public ApplicationPlugin() {
		//empty constructur 
	}

	/**
	 * This method is called upon plug-in activation.
	 * 
	 * @param context the context
	 * 
	 * @throws Exception the exception
	 */
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/**
	 * This method is called when the plug-in is stopped.
	 * 
	 * @param context the context
	 * 
	 * @throws Exception the exception
	 */
	@Override
	public void stop(final BundleContext context) throws Exception {
		super.stop(context);
		plugin = null;
	}

	/**
	 * Returns the shared instance.
	 * 
	 * @return the default
	 */
	public static ApplicationPlugin getDefault() {
		return plugin;
	}

	/**
	 * Gets the current active editor.
	 * 
	 * @return the current active editor
	 */
	public IEditorPart getCurrentActiveEditor() {
		IWorkbenchWindow window = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();
		return window.getActivePage().getActiveEditor();
	}

	private final Hashtable<Diagram, CommandStack> commandStacks = new Hashtable<Diagram, CommandStack>();

	/**
	 * Gets the command stack.
	 * 
	 * @param diagram the diagram
	 * 
	 * @return the command stack
	 */
	public CommandStack getCommandStack(final Diagram diagram) {
		if (!commandStacks.containsKey(diagram)) {
			commandStacks.put(diagram, new CommandStack());
		}
		return commandStacks.get(diagram);
	}

}
