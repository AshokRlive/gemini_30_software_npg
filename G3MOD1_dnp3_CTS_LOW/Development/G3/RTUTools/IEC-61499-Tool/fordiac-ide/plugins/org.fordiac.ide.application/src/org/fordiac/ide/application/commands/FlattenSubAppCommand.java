/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.application.commands.ConnectionMoveCMD;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.IConnectionView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.ElementSelector;
import org.fordiac.ide.util.commands.ConnectionUtil;
import org.fordiac.ide.util.commands.UnmapSubAppCommand;

/**
 * The Class FlattenSubAppCommand.
 */
public class FlattenSubAppCommand extends Command {

	/** The parent network. */
	private Diagram parentNetwork;

	/** The editor. */
	private IEditorPart editor;

	/** The sub app. */
	private SubAppForFBNetworkEditPart subApp;

	/** The cmd. */
	private UnmapSubAppCommand cmd;

	/** The children. */
	private ArrayList<View> children;

	/** The original sources. */
	private Hashtable<ConnectionView, InterfaceElementView> originalSources;

	/** The original dests. */
	private Hashtable<ConnectionView, InterfaceElementView> originalDests;

	/** The original internal sources. */
	private Hashtable<ConnectionView, InternalSubAppInterfaceElementView> originalInternalSources;

	/** The original external sources. */
	private Hashtable<ConnectionView, SubAppInterfaceElementView> originalExternalSources;
	
	/** The original internal dests. */
	private Hashtable<ConnectionView, InternalSubAppInterfaceElementView> originalInternalDests;

	/** The original external dests. */
	private Hashtable<ConnectionView, SubAppInterfaceElementView> originalExternalDests;
	/**
	 * Gets the sub app.
	 * 
	 * @return the sub app
	 */
	public SubAppForFBNetworkEditPart getSubApp() {
		return subApp;
	}

	/**
	 * Sets the sub app.
	 * 
	 * @param subApp the new sub app
	 */
	public void setSubApp(final SubAppForFBNetworkEditPart subApp) {
		this.subApp = subApp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(ApplicationPlugin.getDefault()
				.getCurrentActiveEditor());

	}

	/**
	 * Instantiates a new flatten sub app command.
	 */
	public FlattenSubAppCommand() {
		super(Messages.FlattenSubAppCommand_LABEL_FlattenSubAppCommand);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return parentNetwork != null && subApp != null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = ApplicationPlugin.getDefault().getCurrentActiveEditor();

		input = CommandUtil.closeOpenedSubApp(subApp.getCastedModel()
				.getUiSubAppNetwork());

		if (subApp.getCastedModel().getMappedSubApp() != null) {
			cmd = new UnmapSubAppCommand(subApp.getCastedModel()
					.getMappedSubApp());
			if (cmd.canExecute()) {
				cmd.execute();
			}
		}

		moveChildsToParent();

		reconnectInputs();

		reconnectOutputs();

		disconnectInternalConnections();

		uiNetwork = subApp.getCastedModel().getUiSubAppNetwork();

		moveConnections();
		
		parentNetwork.getFunctionBlockNetwork().getSubApps().remove(
				subApp.getCastedModel().getSubApp());
		parentNetwork.getChildren().remove(subApp.getCastedModel());

		network = subApp.getCastedModel().getUiSubAppNetwork()
				.getSubAppNetwork();

		// subApp.getCastedModel().getSubApp().setSubAppNetwork(null);

		subApp.getCastedModel().getUiSubAppNetwork().setSubAppNetwork(null);
		// delteXMLFile();
		
		ElementSelector selector = new ElementSelector();
		selector.selectViewObjects(children);

	}

	/** The connection moved cm ds. */
	private final ArrayList<ConnectionMoveCMD> connectionMovedCMDs = new ArrayList<ConnectionMoveCMD>();

	/**
	 * Move connections.
	 */
	private void moveConnections() {
		List<EventConnection> eventCons = subApp.getCastedModel().getSubApp()
				.getSubAppNetwork().getEventConnections();
		List<DataConnection> dataCons = subApp.getCastedModel().getSubApp()
				.getSubAppNetwork().getDataConnections();

		for (Iterator<EventConnection> iterator = eventCons.iterator(); iterator
				.hasNext();) {
			EventConnection eventCon = iterator.next();
			ConnectionMoveCMD cmd = new ConnectionMoveCMD(eventCon,
					parentNetwork.getFunctionBlockNetwork());
			connectionMovedCMDs.add(cmd);
		}

		for (Iterator<DataConnection> iterator = dataCons.iterator(); iterator
				.hasNext();) {
			DataConnection dataCon = iterator.next();
			ConnectionMoveCMD cmd = new ConnectionMoveCMD(dataCon,
					parentNetwork.getFunctionBlockNetwork());
			connectionMovedCMDs.add(cmd);
		}
		executeConnectionMoveCMDs();

		connectionViews = new ArrayList<IConnectionView>();
		connectionViews.addAll(uiNetwork.getConnections());

		/*
		 * add all connections of the subAppNetwork which are not leading to interfaces of the subApp
		 * =>this means real internal connections
		 */
		for(IConnectionView currentIConnView : connectionViews){
			ConnectionView connectionView = (ConnectionView) currentIConnView;
			if(connectionView.getSource() != null && connectionView.getDestination() != null){
				parentNetwork.getConnections().add(currentIConnView);
			}
		}
	}

	/**
	 * Execute connection move cm ds.
	 */
	private void executeConnectionMoveCMDs() {
		for (Iterator<ConnectionMoveCMD> iterator = connectionMovedCMDs
				.iterator(); iterator.hasNext();) {
			ConnectionMoveCMD cmd = iterator.next();
			if (cmd.canExecute()) {
				cmd.execute();
			}
		}
	}

	/**
	 * Undo connectionMoveCMDs.
	 */
	private void undoConnectionMoveCMDs() {
		for (Iterator<ConnectionMoveCMD> iterator = connectionMovedCMDs
				.iterator(); iterator.hasNext();) {
			ConnectionMoveCMD cmd = iterator.next();
			if (cmd.canUndo()) {
				cmd.undo();
			}
		}
	}

	/**
	 * Redo connectionMoveCMDs.
	 */
	private void redoConnectionMoveCMDs() {
		for (Iterator<ConnectionMoveCMD> iterator = connectionMovedCMDs
				.iterator(); iterator.hasNext();) {
			ConnectionMoveCMD cmd = iterator.next();
			if (cmd.canUndo()) {
				cmd.redo();
			}
		}
	}

	// /**
	// * Delte xml file.
	// */
	// private void delteXMLFile() {
	// SystemManager.getInstance().deleteSubApp(subApp.getCastedModel());
	//
	// }

	/**
	 * Disconnect internal connections.
	 */
	private void disconnectInternalConnections() {

		for (Iterator<IConnectionView> iterator = subApp.getCastedModel().getUiSubAppNetwork()
				.getConnections().iterator(); iterator.hasNext();) {
			IConnectionView iConnectionView = iterator.next();
			ConnectionView connectionView = (ConnectionView) iConnectionView;
			if (connectionView.getSource() instanceof InternalSubAppInterfaceElementView
					|| connectionView.getDestination() instanceof InternalSubAppInterfaceElementView) {
				//add internal connection to delete list
				originalSources.put(connectionView, connectionView.getSource());
				originalDests.put(connectionView, connectionView
						.getDestination());
				connectionView.setSource(null);
				connectionView.setDestination(null);
			}
		}
	}

	/**
	 * Reconnect outputs.
	 */
	private void reconnectOutputs() {
		originalSources = new Hashtable<ConnectionView, InterfaceElementView>();
		originalInternalDests = new Hashtable<ConnectionView, InternalSubAppInterfaceElementView>();
		originalExternalDests = new Hashtable<ConnectionView, SubAppInterfaceElementView>();
		for (Iterator<SubAppInterfaceElementView> iterator = subApp
				.getCastedModel().getInterfaceElements().iterator(); iterator
				.hasNext();) {
			SubAppInterfaceElementView subAppInterfaceElementView = iterator
					.next();
			ArrayList<ConnectionView> outConnections = new ArrayList<ConnectionView>(
					subAppInterfaceElementView.getOutConnections());
			for (Iterator<ConnectionView> iterator2 = outConnections.iterator(); iterator2
					.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				originalSources.put(connectionView, connectionView.getSource());
				InterfaceElementView newSource = findInterfaceElement(connectionView
						.getSource().getIInterfaceElement());			
				connectionView.setSource(newSource);
				connectionView.getConnectionElement().setSource(
						newSource.getIInterfaceElement());
				if (newSource instanceof SubAppInterfaceElementView) {
					InternalSubAppInterfaceElementView internalInterfaceElementViewDest = ConnectionUtil
							.findInternalInterfaceElement(newSource
									.getIInterfaceElement(),
									((SubAppView) newSource.eContainer())
											.getUiSubAppNetwork());
					InternalSubAppInterfaceElementView old = connectionView.getInternalSourceInterfaceElementView();
					originalInternalDests.put(connectionView, old);
					for (Iterator<ConnectionView> iter = old.getInConnections()
							.iterator(); iter.hasNext();) {
						ConnectionView view = iter.next();
						if (view.getSource().equals(old)) {
							originalExternalDests.put(view, view.getExternalDestinationInterfaceElementView());
							view
									.setExternalDestinationInterfaceElementView((SubAppInterfaceElementView) newSource);
						}
					}
					connectionView
							.setInternalSourceInterfaceElementView(internalInterfaceElementViewDest);
				} else {
					InternalSubAppInterfaceElementView old = connectionView.getInternalSourceInterfaceElementView();
					if (old != null) {
						originalInternalDests.put(connectionView, old);
						connectionView.setInternalSourceInterfaceElementView(null);
					}
				}
			}
		}
	}

	/**
	 * Find interface element.
	 * 
	 * @param oldSource
	 *            the old source
	 * 
	 * @return the interface element view
	 */
	private InterfaceElementView findInterfaceElement(
			final IInterfaceElement oldSource) {
		for (Iterator<View> iterator = children.iterator(); iterator.hasNext();) {
			View child = iterator.next();
			if (child instanceof FBView) {
				FBView fbView = (FBView) child;
				for (Iterator<InterfaceElementView> iterator2 = fbView
						.getInterfaceElements().iterator(); iterator2.hasNext();) {
					InterfaceElementView elementView = iterator2.next();
					if (elementView.getIInterfaceElement().equals(oldSource)) {
						return elementView;
					}
				}
			}
			if (child instanceof SubAppView) {
				SubAppView subAppView = (SubAppView) child;
				for (Iterator<SubAppInterfaceElementView> iterator2 = subAppView
						.getInterfaceElements().iterator(); iterator2.hasNext();) {
					InterfaceElementView elementView = iterator2.next();
					if (elementView.getIInterfaceElement().equals(oldSource)) {
						return elementView;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Reconnect inputs.
	 */
	private void reconnectInputs() {
		originalDests = new Hashtable<ConnectionView, InterfaceElementView>();
		originalInternalSources = new Hashtable<ConnectionView, InternalSubAppInterfaceElementView>();
		originalExternalSources = new Hashtable<ConnectionView, SubAppInterfaceElementView>();
		for (Iterator<SubAppInterfaceElementView> iterator = subApp
				.getCastedModel().getInterfaceElements().iterator(); iterator
				.hasNext();) {
			SubAppInterfaceElementView subAppInterfaceElementView = iterator
					.next();
			ArrayList<ConnectionView> inConnections = new ArrayList<ConnectionView>(
					subAppInterfaceElementView.getInConnections());
			for (Iterator<ConnectionView> iterator2 = inConnections.iterator(); iterator2
					.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				originalDests.put(connectionView, connectionView
						.getDestination());
				InterfaceElementView newDest = findInterfaceElement(connectionView
						.getDestination().getIInterfaceElement());
				connectionView.setDestination(newDest);
				connectionView.getConnectionElement().setDestination(
						newDest.getIInterfaceElement());
				if (newDest instanceof SubAppInterfaceElementView) {
					InternalSubAppInterfaceElementView internalInterfaceElementViewDest = ConnectionUtil
							.findInternalInterfaceElement(newDest
									.getIInterfaceElement(),
									((SubAppView) newDest.eContainer())
											.getUiSubAppNetwork());
					InternalSubAppInterfaceElementView old = connectionView.getInternalDestinationInterfaceElementView();
					originalInternalSources.put(connectionView, old);
					for (Iterator<ConnectionView> iter = old
							.getOutConnections().iterator(); iter.hasNext();) {
						ConnectionView view = iter.next();
						if (view.getSource().equals(old)) {
							originalExternalSources.put(view, view
									.getExternalSourceInterfaceElementView());
							view
									.setExternalSourceInterfaceElementView((SubAppInterfaceElementView) newDest);
						}
					}
					connectionView.setInternalDestinationInterfaceElementView(internalInterfaceElementViewDest);
				} else {
					InternalSubAppInterfaceElementView old = connectionView.getInternalDestinationInterfaceElementView();
					if (old != null) {
						originalInternalSources.put(connectionView, old);
						connectionView.setInternalDestinationInterfaceElementView(null);
					}
				}
			}
		}
	}

	/**
	 * Move childs to parent.
	 */
	private void moveChildsToParent() {
		children = new ArrayList<View>(subApp.getCastedModel()
				.getUiSubAppNetwork().getChildren());
		parentNetwork.getChildren().addAll(children);
		for (Iterator<View> iterator = children.iterator(); iterator.hasNext();) {
			View view = iterator.next();
			if (view instanceof FBView) {
				FBView fbView = (FBView) view;
				parentNetwork.getFunctionBlockNetwork().getFBs().add(
						fbView.getFb());
			} else if (view instanceof SubAppView) {
				SubAppView subAppView = (SubAppView) view;
				parentNetwork.getFunctionBlockNetwork().getSubApps().add(
						subAppView.getSubApp());

			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {

		uiNetwork.setSubAppNetwork(network);
		uiNetwork.getConnections().addAll(connectionViews);

		subApp.getCastedModel().getUiSubAppNetwork().setSubAppNetwork(network);

		subApp.getCastedModel().getUiSubAppNetwork().getChildren().addAll(
				children);

		for (Iterator<View> iterator = children.iterator(); iterator.hasNext();) {
			View view = iterator.next();
			if (view instanceof FBView) {
				FBView fbView = (FBView) view;
				subApp.getCastedModel().getSubApp().getSubAppNetwork().getFBs()
						.add(fbView.getFb());
			} else if (view instanceof SubAppView) {
				SubAppView subAppView = (SubAppView) view;
				subApp.getCastedModel().getSubApp().getSubAppNetwork()
						.getSubApps().add(subAppView.getSubApp());

			}
		}
		restoreInputs();

		restoreOutputs();

		parentNetwork.getChildren().add(subApp.getCastedModel());
		parentNetwork.getFunctionBlockNetwork().getSubApps().add(
				subApp.getCastedModel().getSubApp());

		undoConnectionMoveCMDs();

		// SubAppView
		if (input != null) {
			CommandUtil.openSubAppEditor(input);
		}

		if (cmd != null && cmd.canUndo()) {
			cmd.undo();
		}

		ElementSelector selector = new ElementSelector();
		ArrayList<View> viewsToSelect = new ArrayList<View>();
		viewsToSelect.add(subApp.getCastedModel());
		selector.selectViewObjects(viewsToSelect);
	}

	/**
	 * Restore inputs.
	 */
	private void restoreInputs() {
		Enumeration<ConnectionView> enumeration = originalDests.keys();
		while (enumeration.hasMoreElements()) {
			ConnectionView connectionView = enumeration.nextElement();
			connectionView.setDestination(originalDests.get(connectionView));
		}
		Enumeration<ConnectionView> enumeration1 = originalInternalSources
				.keys();
		while (enumeration1.hasMoreElements()) {
			ConnectionView connectionView = enumeration1.nextElement();
			connectionView.setInternalDestinationInterfaceElementView(originalInternalSources
							.get(connectionView));
		}
		Enumeration<ConnectionView> enumeration2 = originalExternalSources
				.keys();
		while (enumeration2.hasMoreElements()) {
			ConnectionView connectionView = enumeration2.nextElement();
			connectionView.setExternalDestinationInterfaceElementView(originalExternalSources
							.get(connectionView));
		}
		
	}

	/**
	 * Restore outputs.
	 */
	private void restoreOutputs() {
		Enumeration<ConnectionView> enumeration = originalSources.keys();
		while (enumeration.hasMoreElements()) {
			ConnectionView connectionView = enumeration.nextElement();
			connectionView.setSource(originalSources.get(connectionView));
		}
		Enumeration<ConnectionView> enumeration1 = originalInternalDests.keys();
		while (enumeration1.hasMoreElements()) {
			ConnectionView connectionView = enumeration1.nextElement();
			connectionView.setInternalSourceInterfaceElementView(originalInternalDests
							.get(connectionView));
		}
		Enumeration<ConnectionView> enumeration2 = originalExternalDests.keys();
		while (enumeration2.hasMoreElements()) {
			ConnectionView connectionView = enumeration2.nextElement();
			connectionView.setExternalSourceInterfaceElementView(originalExternalDests
							.get(connectionView));
		}
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		input = CommandUtil.closeOpenedSubApp(subApp.getCastedModel()
				.getUiSubAppNetwork());
		moveChildsToParent();

		reconnectInputs();

		reconnectOutputs();
		disconnectInternalConnections();
		parentNetwork.getFunctionBlockNetwork().getSubApps().remove(
				subApp.getCastedModel().getSubApp());
		parentNetwork.getChildren().remove(subApp.getCastedModel());

		// delteXMLFile();
		redoConnectionMoveCMDs();
		if (cmd != null && cmd.canUndo()) {
			cmd.redo();
		}
		/*
		 * add all connections of the subAppNetwork which are not leading to interfaces of the subApp
		 * =>this means real internal connections
		 */
		for(IConnectionView currentIConnView : connectionViews){
			ConnectionView connectionView = (ConnectionView) currentIConnView;
			if(connectionView.getSource() != null && connectionView.getDestination() != null){
				parentNetwork.getConnections().add(currentIConnView);
			}
		}
		subApp.getCastedModel().getUiSubAppNetwork().setSubAppNetwork(null);
		
		ElementSelector selector = new ElementSelector();
		selector.selectViewObjects(children);
	}

	/** The input. */
	IEditorInput input = null;

	private SubAppNetwork network;

	private UISubAppNetwork uiNetwork;

	private ArrayList<IConnectionView> connectionViews;

	/**
	 * Sets the parent network.
	 * 
	 * @param parentNetwork the new parent network
	 */
	public void setParentNetwork(final Diagram parentNetwork) {
		this.parentNetwork = parentNetwork;
	}

}
