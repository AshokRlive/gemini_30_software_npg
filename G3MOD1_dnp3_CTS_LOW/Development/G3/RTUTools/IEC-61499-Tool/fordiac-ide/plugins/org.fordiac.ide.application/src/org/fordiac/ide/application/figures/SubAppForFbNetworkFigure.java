/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.figures;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.gef.draw2d.AdvancedRoundedRectangle;
import org.fordiac.ide.gef.draw2d.UnderlineAlphaLabel;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.ui.SubAppView;

/**
 * The Class SubAppForFbNetworkFigure.
 */
public class SubAppForFbNetworkFigure extends Shape {

	/** The Constant CORNER_DIM. */
	private static final int CORNER_DIM = 14;

	/** The model. */
	protected SubApp model;

	/** The view model. */
	protected SubAppView viewModel;

	/** The instance name label. */
	private Label instanceNameLabel;

	/** The main. */
	private final Figure main = new Figure();

	/** The top. */
	private RoundedRectangle top;

	/** The top inputs. */
	private final Figure topInputs = new Figure();

	/** The top outputs. */
	private final Figure topOutputs = new Figure();

	/** The middle. */
	private RoundedRectangle middle;

	/** The middle inputs. */
	private final Figure middleInputs = new Figure();

	/** The middle outputs. */
	private final Figure middleOutputs = new Figure();

	/** The bottom. */
	private RoundedRectangle bottom;

	/** The bottom inputs. */
	private final Figure bottomInputs = new Figure();

	/** The bottom input values figure. */
	private final Figure bottomInputValuesFigure = new Figure();

	/** The bottom outputs. */
	private final Figure bottomOutputs = new Figure();

	/** The event inputs. */
	private final Figure eventInputs = new Figure();

	/** The event outputs. */
	private final Figure eventOutputs = new Figure();

	/** The data inputs. */
	private final Figure dataInputs = new Figure();

	/** The data outputs. */
	private final Figure dataOutputs = new Figure();

	/** The middle border. */
	private TopBorder middleBorder;

	/** The bottom border. */
	private TopBorder bottomBorder;

	private static ZoomManager zoomManager; 
	
	UnderlineAlphaLabel typeLabel;

	private void configureRectangles() {
		top = new AdvancedRoundedRectangle(PositionConstants.NORTH | PositionConstants.EAST | PositionConstants.WEST, zoomManager, main, true, ColorConstants.black);
		middle = new AdvancedRoundedRectangle(PositionConstants.EAST | PositionConstants.WEST, zoomManager,main, true, ColorConstants.black);
		bottom = new AdvancedRoundedRectangle(PositionConstants.SOUTH | PositionConstants.EAST | PositionConstants.WEST, zoomManager,main,  true, ColorConstants.black);
	}
	
	/**
	 * Not intended to be used!
	 */
	public SubAppForFbNetworkFigure() {
		configureRectangles();
	}
	
	/**
	 * Instantiates a new sub app for fb network figure.
	 * 
	 * @param subAppView the sub app view
	 */
	public SubAppForFbNetworkFigure(final SubAppView subAppView) {
		this(subAppView.getSubApp(), zoomManager);
		configureRectangles();
		viewModel = subAppView;
		refreshToolTips();
	}

	/**
	 * Instantiates a new SubApplication Figure for a FBNetwork.
	 * 
	 * @param model the model
	 */

	public SubAppForFbNetworkFigure(final SubApp model, ZoomManager zoomManager) {
		setBackgroundColor(ColorConstants.white);
		configureRectangles();
		this.model = model;

		this.setFillXOR(true);

		GridData instanceNameLayout = new GridData(GridData.HORIZONTAL_ALIGN_FILL
				| GridData.GRAB_HORIZONTAL);

		instanceNameLabel = new Label();
		instanceNameLabel.setText(model != null && model.getName() != null ? model
				.getName() : "N/D");
		instanceNameLabel.setTextAlignment(PositionConstants.CENTER);

		GridLayout gridLayout = new GridLayout(1, true);
		gridLayout.verticalSpacing = 2;
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		setLayoutManager(gridLayout);

		GridLayout mainLayout = new GridLayout(3, false);
		mainLayout.marginHeight = 0;
		mainLayout.marginWidth = 0;
		mainLayout.horizontalSpacing = 0;
		mainLayout.verticalSpacing = -1;
		GridData mainLayoutData = new GridData(GridData.HORIZONTAL_ALIGN_FILL
				| GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL
				| GridData.GRAB_VERTICAL);

		main.setLayoutManager(mainLayout);

		add(main);
		setConstraint(main, mainLayoutData);
		main.add(new Label()); // to fill the layoutmanagers first row
		main.add(instanceNameLabel);
		main.setConstraint(instanceNameLabel, instanceNameLayout);
		main.add(new Label()); // to fill the layoutmanagers first row

		top.setCornerDimensions(new Dimension(CORNER_DIM, CORNER_DIM));
		GridLayout topLayout = new GridLayout(2, false);
		topLayout.marginHeight = 4;
		topLayout.marginWidth = 1;
		topLayout.horizontalSpacing = 2;
		GridData topLayoutData = new GridData(GridData.HORIZONTAL_ALIGN_FILL
				| GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL);

		top.setLayoutManager(topLayout);

		main.add(topInputs);
		topInputs.setBorder(new LineBorder(ColorConstants.cyan));
		main.add(top);
		main.add(topOutputs);
		main.setConstraint(top, topLayoutData);
		//		
		ToolbarLayout topInputsLayout = new ToolbarLayout(false);
		GridData topInputsLayoutData = new GridData(GridData.HORIZONTAL_ALIGN_FILL
				| GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL
				| GridData.GRAB_VERTICAL);
		topInputsLayout.setStretchMinorAxis(true);
		eventInputs.setLayoutManager(topInputsLayout);

		top.add(eventInputs);
		top.setConstraint(eventInputs, topInputsLayoutData);
		//			
		ToolbarLayout topOutputsLayout = new ToolbarLayout(false);
		GridData topOutputsLayoutData = new GridData(GridData.HORIZONTAL_ALIGN_FILL
				| GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL
				| GridData.GRAB_VERTICAL);
		topOutputsLayout.setStretchMinorAxis(true);
		eventOutputs.setLayoutManager(topOutputsLayout);
		eventOutputs.setMinimumSize(new Dimension(40, 18));
		top.add(eventOutputs);
		top.setConstraint(eventOutputs, topOutputsLayoutData);

		Figure middleContainer = new Figure();
		BorderLayout borderLayout;
		middleContainer.setLayoutManager(borderLayout = new BorderLayout());
		borderLayout.setHorizontalSpacing(10);
		middleContainer.setBorder(new MarginBorder(0, 7, 0, 7));

		main.add(middleInputs);
		middleInputs.setBorder(new LineBorder(ColorConstants.orange));
		main.add(middleContainer);
		main.add(middleOutputs);
		middleContainer.add(middle, BorderLayout.CENTER);

		GridLayout middleLayout = new GridLayout(1, true);
		GridData middleLayouData = new GridData(GridData.HORIZONTAL_ALIGN_FILL
				| GridData.GRAB_HORIZONTAL);
		main.setConstraint(middleContainer, middleLayouData);

		middle.setLayoutManager(middleLayout);
		middle.setBorder(middleBorder = new TopBorder(getBackgroundColor(), 0));
		middleLayout.marginHeight = 0;
		middleLayout.verticalSpacing = 1;
		
		setupSubbAppTypeNameLabel();
		
		middle.setCornerDimensions(new Dimension());

		bottom.setCornerDimensions(new Dimension(CORNER_DIM, CORNER_DIM));
		bottom.setBorder(bottomBorder = new TopBorder(getBackgroundColor(),
				CORNER_DIM - 4));
		GridLayout bottomLayout = new GridLayout(2, false);
		bottomLayout.marginHeight = 4;
		bottomLayout.marginWidth = 1;
		bottomLayout.horizontalSpacing = 0;
		bottom.setLayoutManager(bottomLayout);
		GridData bottomLayoutData = new GridData(GridData.HORIZONTAL_ALIGN_FILL
				| GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL
				| GridData.GRAB_VERTICAL);

		GridLayout bottomInputValuesLayout = new GridLayout();
		bottomInputValuesLayout.marginHeight = 4;
		bottomInputValuesLayout.marginWidth = 2;
		bottomInputValuesLayout.horizontalSpacing = 0;

		main.add(bottomInputs);
		bottomInputs.setLayoutManager(bottomInputValuesLayout);

		ToolbarLayout bottomInputValuesFigureLayout = new ToolbarLayout(false);
		bottomInputValuesFigureLayout.setStretchMinorAxis(true);
		bottomInputValuesFigure.setLayoutManager(bottomInputValuesFigureLayout);
		bottomInputs.add(bottomInputValuesFigure);

		main.add(bottom);
		main.add(bottomOutputs);
		main.setConstraint(bottom, bottomLayoutData);
		//		
		ToolbarLayout bottomInputsLayout = new ToolbarLayout(false);
		bottomInputsLayout.setStretchMinorAxis(true);
		dataInputs.setLayoutManager(bottomInputsLayout);
		GridData bottomInputsLayoutData = new GridData(
				GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
						| GridData.VERTICAL_ALIGN_FILL | GridData.GRAB_VERTICAL);

		bottom.add(dataInputs);
		bottom.setConstraint(dataInputs, bottomInputsLayoutData);
		//			
		ToolbarLayout bottomOutputsLayout = new ToolbarLayout(false);
		bottomOutputsLayout.setStretchMinorAxis(true);
		GridData bottomOutputsLayoutData = new GridData(
				GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
						| GridData.VERTICAL_ALIGN_FILL | GridData.GRAB_VERTICAL);
		dataOutputs.setLayoutManager(bottomOutputsLayout);

		bottom.add(dataOutputs);
		bottom.setConstraint(dataOutputs, bottomOutputsLayoutData);

		refreshToolTips();

	}

	
	private void setupSubbAppTypeNameLabel(){
		String typeName = Messages.SubAppForFbNetworkFigure_LABEL_NotDefined;
		Boolean hasType = false;
		
		if((null != model) && (null != model.getPaletteEntry())) {
			typeName = model.getPaletteEntry().getLabel();
			hasType = true;
		}
		
		typeLabel = new UnderlineAlphaLabel(typeName);
		middle.add(typeLabel);
		
		typeLabel.setTextAlignment(PositionConstants.CENTER);
		middle.setConstraint(typeLabel, new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL));
		
		if(hasType){
			//if we have a type we want to show hyperlink inderline behavior when ctrl is clicked
			setupMouseListener();
		}
	}
	
	/** Draw an hyperlink like undeline when the mouse is over the label and control is pressed
	 * TODO duplicated code from FBFigure
	 */
	private void setupMouseListener() {
		
		middle.addMouseMotionListener(new MouseMotionListener(){

			@Override
			public void mouseDragged(MouseEvent me) {
			}

			@Override
			public void mouseEntered(MouseEvent me) {
				if( 0 != (me.getState() & SWT.CONTROL)){
					typeLabel.setDrawUnderline(true);
				}
			}

			@Override
			public void mouseExited(MouseEvent me) {
				typeLabel.setDrawUnderline(false);
			}

			@Override
			public void mouseHover(MouseEvent me) {
				//currently mouseHover should be the same as mouse moved
				mouseMoved(me);
			}

			@Override
			public void mouseMoved(MouseEvent me) {
				if( 0 != (me.getState() & SWT.CONTROL)){
					if(!typeLabel.isDrawUnderline()){
						typeLabel.setDrawUnderline(true);
					}
				}
				else{
					if(typeLabel.isDrawUnderline()){
						typeLabel.setDrawUnderline(false);
					}
				}
			}
			
		});
		
	}
	

	/**
	 * Gets the instance name label.
	 * 
	 * @return the instance name label
	 */
	public Label getInstanceNameLabel() {
		return instanceNameLabel;
	}

	/**
	 * Refresh tool tips.
	 */
	public void refreshToolTips() {
		setToolTip(new SubAppTooltipFigure(model));
	}

	/**
	 * Gets the event inputs.
	 * 
	 * @return the event inputs
	 */
	public Figure getEventInputs() {
		return eventInputs;
	}

	/**
	 * Gets the event outputs.
	 * 
	 * @return the event outputs
	 */
	public Figure getEventOutputs() {
		return eventOutputs;
	}

	/**
	 * Gets the data inputs.
	 * 
	 * @return the data inputs
	 */
	public Figure getDataInputs() {
		return dataInputs;
	}

	/**
	 * Gets the data outputs.
	 * 
	 * @return the data outputs
	 */
	public Figure getDataOutputs() {
		return dataOutputs;
	}

	/**
	 * The Class TopBorder.
	 */
	public class TopBorder extends LineBorder {

		/** The corner dimensions. */
		int cornerDimensions = 0;

		/**
		 * Instantiates a new top border.
		 * 
		 * @param color the color
		 * @param cornerDimensions the corner dimensions
		 */
		public TopBorder(final Color color, final int cornerDimensions) {
			super(color);

			this.cornerDimensions = cornerDimensions;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.draw2d.LineBorder#getInsets(org.eclipse.draw2d.IFigure)
		 */
		@Override
		public Insets getInsets(final IFigure figure) {
			return new Insets(0, 0, 0, 0);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.draw2d.LineBorder#paint(org.eclipse.draw2d.IFigure,
		 * org.eclipse.draw2d.Graphics, org.eclipse.draw2d.geometry.Insets)
		 */
		@Override
		public void paint(final IFigure figure, final Graphics graphics,
				final Insets insets) {
			tempRect.setBounds(getPaintRectangle(figure, insets));
			if (Math.abs(getWidth() % 2) == 1) {
				tempRect.width--;
				tempRect.height--;
			}
			tempRect.shrink(getWidth() / 2, getWidth() / 2);
			graphics.setLineWidth(getWidth());
			if (getColor() != null) {
				graphics.setForegroundColor(getColor());
			}
			graphics.drawLine(tempRect.x + cornerDimensions, tempRect.y, tempRect.x
					+ tempRect.width - cornerDimensions, tempRect.y);

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Shape#fillShape(org.eclipse.draw2d.Graphics)
	 */
	@Override
	protected void fillShape(final Graphics graphics) {
		// not used
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Shape#outlineShape(org.eclipse.draw2d.Graphics)
	 */
	@Override
	protected void outlineShape(final Graphics graphics) {
		// not used
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.draw2d.Figure#setBackgroundColor(org.eclipse.swt.graphics.Color
	 * )
	 */
	@Override
	public void setBackgroundColor(final Color bg) {
		super.setBackgroundColor(bg);
		if (middleBorder != null) {
			middleBorder.setColor(bg);
		}
		if (bottomBorder != null) {
			bottomBorder.setColor(bg);
		}

	}

	/**
	 * Gets the input values content pane.
	 * 
	 * @return the input values content pane
	 */
	public Figure getInputValuesContentPane() {
		return bottomInputValuesFigure;
	}

	public IFigure getMiddle() {
		return middle;
	}

}
