/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.ArrayList;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.Utils;
import org.fordiac.ide.util.commands.DeleteFBCommand;
import org.fordiac.ide.util.commands.UnmapSubAppCommand;

public class DeleteSubAppCommand extends Command {

	/** The editor. */
	private IEditorPart editor;

	/** The view. */
	private SubAppView view;

	/** The cmd. */
	private UnmapSubAppCommand unmapCmd;
	
	/** The input. */
	IEditorInput input = null;
	
	/** The delete FB and sub app commands. */
	private final ArrayList<DeleteFBCommand> deleteFBCommands = new ArrayList<DeleteFBCommand>();
	private final ArrayList<FlattenSubAppCommand> flattenSubAppCommands = new ArrayList<FlattenSubAppCommand>();

	/**
	 * Instantiates a new delete subapp command.
	 * 
	 * @param view
	 *            the view
	 */
	public DeleteSubAppCommand(final SubAppView view) {
		super("Delete SubApp");
		this.view = view;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor != null && editor.equals(Utils.getCurrentActiveEditor());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		
		input = CommandUtil.closeOpenedSubApp(view.getUiSubAppNetwork());
		
		// Unmap if subapp is mapped
		if (view.getMappedSubApp() != null) {
			unmapCmd = new UnmapSubAppCommand(view.getMappedSubApp());
			if (unmapCmd.canExecute()) {
				unmapCmd.execute();
			}
		}
		
		// Find FBs and SAs within the current subapp
		traverseSubApps(view);
			
		// Flatten all subapps
		for (FlattenSubAppCommand flatCmd : flattenSubAppCommands) {
			flatCmd.execute();
		}
		// Delete remaining FBs
		for (DeleteFBCommand deleteFBCmd : deleteFBCommands) {
			deleteFBCmd.execute();	
		}
	}
	
	private void traverseSubApps(View view) {
		if (view instanceof FBView) {
			DeleteFBCommand deleteFBCmd = new DeleteFBCommand((FBView) view);
			deleteFBCommands.add(deleteFBCmd);
		} else if (view instanceof SubAppView) {
			for (View childView : ((SubAppView)view).getUiSubAppNetwork().getChildren()) {
				traverseSubApps(childView);
			}
			FlattenSubAppCommand fSA = new FlattenSubAppCommand();
			SubAppForFBNetworkEditPart saNP = new SubAppForFBNetworkEditPart();
			saNP.setModel(view);
			fSA.setSubApp(saNP);
			fSA.setParentNetwork((Diagram) view.eContainer());
			flattenSubAppCommands.add(fSA);
		}
	}

	/**
	 * Redo.
	 * 
	 * @see DeleteSubAppCommand#execute()
	 */
	@Override
	public void redo() {
		input = CommandUtil.closeOpenedSubApp(view.getUiSubAppNetwork());
		
		if (unmapCmd != null) {
			unmapCmd.redo();
		}

		for (FlattenSubAppCommand flattenCmd : flattenSubAppCommands) {
			flattenCmd.redo();
		}		
		for (DeleteFBCommand deleteFBCmd : deleteFBCommands) {
			deleteFBCmd.redo();
		}	
	}

	/**
	 * Restores the removed Connections and adds the UIFB to the UIApplication.
	 */
	@Override
	public void undo() {

		for (int i = deleteFBCommands.size()-1; i>=0; i--) {
			deleteFBCommands.get(i).undo();
		}		
		for (int i = flattenSubAppCommands.size()-1; i>=0; i--) {
			flattenSubAppCommands.get(i).undo();
		}		
		
		if (unmapCmd != null && unmapCmd.canUndo()) {
			unmapCmd.undo();
		}
		
		if (input != null) {
			CommandUtil.openSubAppEditor(input);
		}
	}

}
