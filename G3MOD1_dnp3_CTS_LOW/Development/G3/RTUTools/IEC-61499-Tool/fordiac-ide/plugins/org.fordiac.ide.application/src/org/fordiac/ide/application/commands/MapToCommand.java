/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.util.Utils;
import org.fordiac.ide.util.commands.ConnectionUtil;
import org.fordiac.ide.util.commands.DeleteConnectionCommand;
import org.fordiac.ide.util.commands.UnmapCommand;

/**
 * The Class MapToCommand.
 */
public class MapToCommand extends Command {


	/** The fb view. */
	protected final FBView fbView;

	/** The mapped fb view. */
	private FBView mappedFBView;

	public FBView getMappedFBView() {
		return mappedFBView;
	}


	/** The ui resource editor. */
	protected final UIResourceEditor uiResourceEditor;

	/** The position. */
	private Point position;

	/**
	 * Instantiates a new map to command.
	 * 
	 * @param fb
	 *            the fb
	 * @param uiResourceEditor
	 *            the ui resource editor
	 */
	public MapToCommand(final FBView fb, final UIResourceEditor uiResourceEditor) {
		this.fbView = fb;
		this.uiResourceEditor = uiResourceEditor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {

		if (fbView != null && fbView.getMappedFB() != null) {
			if (fbView.getMappedFB().getFb().getResource().equals(
					uiResourceEditor.getResourceElement().getFBNetwork())) {
				Utils
						.getCurrentActiveEditor()
						.getEditorSite()
						.getActionBars()
						.getStatusLineManager()
						.setErrorMessage(
								Messages.MapToCommand_STATUSMessage_AlreadyMapped);

				return false; // already mapped to this resource -> nothing to
				// do ->
				// mapping not possible!
			}
		}

		if (fbView == null) {
			return false;
		}

		boolean supports = deviceSupportsType();

		if (supports) {
			Utils.getCurrentActiveEditor().getEditorSite().getActionBars()
					.getStatusLineManager().setErrorMessage(null);
		} else {
			Utils.getCurrentActiveEditor().getEditorSite().getActionBars()
					.getStatusLineManager().setErrorMessage(
							"Type not supported");
		}
		return supports;
	}

	private boolean deviceSupportsType() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		// return editor.equals(Utils.getCurrentActiveEditor());
		return false;
	}

	/** The undo cmd. */
	private UnmapCommand undoCmd;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		undoCmd = new UnmapCommand(mappedFBView);
		if (undoCmd.canExecute()) {
			undoCmd.execute();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		if (undoCmd != null) {
			undoCmd.undo();
		}
	}

	/** The orig interface elements. */
	private final Hashtable<String, InterfaceElementView> origInterfaceElements = new Hashtable<String, InterfaceElementView>();

	/** The copy interface elements. */
	private final Hashtable<String, InterfaceElementView> copyInterfaceElements = new Hashtable<String, InterfaceElementView>();

	/** The unmap cmd. */
	private UnmapCommand unmapCmd;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		boolean deletedConnections = false;
		if (fbView.getMappedFB() != null) {
			if (!fbView.getMappedFB().getFb().getResource().equals(
					uiResourceEditor.getResourceElement().getFBNetwork())) {
				// unmap before mapping to this resource

				unmapCmd = new UnmapCommand(fbView.getMappedFB());
				if (unmapCmd.canExecute()) {
					unmapCmd.execute();
				}
			}
		}

		mappedFBView = (FBView) EcoreUtil.copy(fbView);
		if (position != null) {
			mappedFBView.getPosition().setX(position.x);
			mappedFBView.getPosition().setY(position.y);
		}

		uiResourceEditor.getChildren().add(mappedFBView);
		fbView.setMappedFB(mappedFBView);

		for (Iterator<InterfaceElementView> iterator = fbView
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			InterfaceElementView interfaceElementview = iterator.next();
			origInterfaceElements.put(interfaceElementview.getLabel(),
					interfaceElementview);
		}
		for (Iterator<InterfaceElementView> iterator = mappedFBView
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			InterfaceElementView interfaceElementview = iterator.next();
			copyInterfaceElements.put(interfaceElementview.getLabel(),
					interfaceElementview);
		}

		Enumeration<String> enumer = origInterfaceElements.keys();
		while (enumer.hasMoreElements()) {
			String key = enumer.nextElement();
			InterfaceElementView copyElem = copyInterfaceElements.get(key);

			origInterfaceElements.get(key).setMappedInterfaceElement(copyElem);

			copyElem.setFbNetwork(uiResourceEditor.getResourceElement()
					.getFBNetwork());
		}

		uiResourceEditor.getResourceElement().getFBNetwork().getMappedFBs()
				.add(mappedFBView.getFb());

		for (Iterator<InterfaceElementView> iterator = fbView
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			InterfaceElementView interfaceElement = iterator.next();

			for (Iterator<ConnectionView> iterator2 = interfaceElement
					.getInConnections().iterator(); iterator2.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				if (connectionView.getSource().eContainer() instanceof FBView) {
					FBView sourceFBView = ((FBView) connectionView.getSource()
							.eContainer()).getMappedFB();
					if (sourceFBView != null
							&& sourceFBView.getFb().getResource().equals(
									uiResourceEditor.getResourceElement()
											.getFBNetwork())) {
						ConnectionView newConnection = UiFactory.eINSTANCE
								.createConnectionView();
						newConnection.setConnectionElement(connectionView
								.getConnectionElement());
						newConnection.setDestination(connectionView
								.getDestination().getMappedInterfaceElement());
						newConnection.setSource(connectionView.getSource()
								.getMappedInterfaceElement());
						uiResourceEditor.getConnections().add(newConnection);
						ConnectionUtil.addConnectionToResource(newConnection
								.getConnectionElement(), uiResourceEditor
								.getResourceElement());
						connectionView.getConnectionElement()
								.setBrokenConnection(false);
						System.out.println("notBroken: " + connectionView);

						for (Iterator<ConnectionView> iterator3 = connectionView.getSource()
								.getMappedInterfaceElement()
								.getOutConnections().iterator(); iterator3
								.hasNext();) {
							ConnectionView temp = (ConnectionView) iterator3
									.next();
							System.out.println("Is Resource Connection "
									+ temp.getConnectionElement()
											.isResourceConnection());
							if (temp.getConnectionElement()
									.isResourceConnection()) {
								DeleteConnectionCommand deleteCMD = new DeleteConnectionCommand(
										temp);
								deleteCMD.execute();
								deletedConnections = true;
							}
						}

					} else {
						System.out.println("isBroken: " + connectionView);
						connectionView.getConnectionElement()
								.setBrokenConnection(true);
						// nothing to do
					}
				} else if (connectionView.getSource().eContainer() instanceof SubAppView) {
					MappedSubAppView sourceSubAppView = ((SubAppView) connectionView
							.getSource().eContainer()).getMappedSubApp();
					if (sourceSubAppView != null
							&& sourceSubAppView.getSubApp().getResource()
									.equals(
											uiResourceEditor
													.getResourceElement()
													.getFBNetwork())) {
						ConnectionView newConnection = UiFactory.eINSTANCE
								.createConnectionView();
						newConnection.setConnectionElement(connectionView
								.getConnectionElement());
						newConnection.setDestination(connectionView
								.getDestination().getMappedInterfaceElement());
						newConnection.setSource(connectionView.getSource()
								.getMappedInterfaceElement());
						uiResourceEditor.getConnections().add(newConnection);
						ConnectionUtil.addConnectionToResource(newConnection
								.getConnectionElement(), uiResourceEditor
								.getResourceElement());
					}

				}
			}
			for (Iterator<ConnectionView> iterator2 = interfaceElement
					.getOutConnections().iterator(); iterator2.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				if (connectionView.getDestination().eContainer() instanceof FBView
						&& !connectionView.getSource().eContainer().equals(
								connectionView.getDestination().eContainer())) { // to
					// avoid
					// self
					// connections
					// shown
					// twice
					// when
					// mapped
					FBView destFBView = ((FBView) connectionView
							.getDestination().eContainer()).getMappedFB();
					if (destFBView != null
							&& destFBView.getFb().getResource().equals(
									uiResourceEditor.getResourceElement()
											.getFBNetwork())) {
						ConnectionView newConnection = UiFactory.eINSTANCE
								.createConnectionView();
						newConnection.setConnectionElement(connectionView
								.getConnectionElement());

						newConnection.setDestination(connectionView
								.getDestination().getMappedInterfaceElement());

						newConnection.setSource(connectionView.getSource()
								.getMappedInterfaceElement());
						uiResourceEditor.getConnections().add(newConnection);
						ConnectionUtil.addConnectionToResource(newConnection
								.getConnectionElement(), uiResourceEditor
								.getResourceElement());
						connectionView.getConnectionElement()
								.setBrokenConnection(false);
					} else {
						connectionView.getConnectionElement()
								.setBrokenConnection(true);
						// nothing to do
					}
				} else if (connectionView.getDestination().eContainer() instanceof SubAppView) {
					MappedSubAppView sourceSubAppView = ((SubAppView) connectionView
							.getDestination().eContainer()).getMappedSubApp();
					if (sourceSubAppView != null
							&& sourceSubAppView.getSubApp().getResource()
									.equals(
											uiResourceEditor
													.getResourceElement()
													.getFBNetwork())) {
						ConnectionView newConnection = UiFactory.eINSTANCE
								.createConnectionView();
						newConnection.setConnectionElement(connectionView
								.getConnectionElement());
						newConnection.setDestination(connectionView
								.getDestination().getMappedInterfaceElement());
						newConnection.setSource(connectionView.getSource()
								.getMappedInterfaceElement());
						uiResourceEditor.getConnections().add(newConnection);
						ConnectionUtil.addConnectionToResource(newConnection
								.getConnectionElement(), uiResourceEditor
								.getResourceElement());
					}
				}
			}
		}
		if (deletedConnections) {
			MessageBox informUser = new MessageBox(Display.getDefault()
					.getActiveShell());
			informUser.setText("Warning");
			informUser
					.setMessage("Remapping required deletion of Connections added within the Resource - please check your network");
			informUser.open();
			// TODO check whether markers could be used!
		}
	}

	/**
	 * Gets the position.
	 * 
	 * @return the position
	 */
	public Point getPosition() {
		return position;
	}

	/**
	 * Sets the position.
	 * 
	 * @param position
	 *            the new position
	 */
	public void setPosition(final Point position) {
		this.position = position;
	}
}
