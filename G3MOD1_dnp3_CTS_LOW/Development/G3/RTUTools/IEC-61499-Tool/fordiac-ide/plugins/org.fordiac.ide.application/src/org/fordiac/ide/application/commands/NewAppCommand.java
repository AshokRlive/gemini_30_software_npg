/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The Class NewAppCommand.
 */
public class NewAppCommand extends AbstractOperation {

	private org.fordiac.ide.model.libraryElement.AutomationSystem system;
	private String appName;

	private String comment;
	private Application application;
	private UIFBNetwork uifbNetwork;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return true;

	}

	public NewAppCommand(
			org.fordiac.ide.model.libraryElement.AutomationSystem system,
			String appName, String comment) {
		super(Messages.NewApplicationCommand_LABEL_NewApplication);
		this.system = system;
		this.appName = appName;
		this.comment = comment;
	}

	public Application getApplication() {
		return application;
	}

	/**
	 * checks whether all required information for creating a new App are set.
	 * 
	 * @return true, if can execute
	 */
	@Override
	public boolean canExecute() {
		return system != null && appName != null && comment != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info) {
		if (system != null) {
			application = LibraryElementFactory.eINSTANCE.createApplication();
			application.setName(appName);
			application.setComment(comment);
			
			FBNetwork network = LibraryElementFactory.eINSTANCE
					.createFBNetwork();
			application.setFBNetwork(network);

			SystemManager.getInstance().saveSystem(system, false);

			uifbNetwork = UiFactory.eINSTANCE.createUIFBNetwork();
			uifbNetwork.setFbNetwork(network);

			system.addApplication(application);
			
			SystemManager.getInstance().saveDiagram(uifbNetwork, system,
					application.getName() + ".xml"); //$NON-NLS-1$

			SystemManager.getInstance().saveSystem(system, true); // to save the
			// uiresource editor
			// of the resources!
			SystemManager.getInstance().saveDiagram(uifbNetwork, system,
					application.getName() + ".xml"); //$NON-NLS-1$
			return Status.OK_STATUS;
		}
		return new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, "", null);

	}

	/**
	 * Redo.
	 * 
	 * @see NewAppCommand#execute()
	 */
	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info) {
		if (system != null) {
			SystemManager.getInstance().saveSystem(system, false);
			system.addApplication(application);
			SystemManager.getInstance().saveDiagram(uifbNetwork, system,
					application.getName() + ".xml"); //$NON-NLS-1$
			SystemManager.getInstance().saveSystem(system, true); // to save the	
			return Status.OK_STATUS;
		}
		return new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, "", null);
	}

	/**
	 * undo of FBCreateCommand.
	 */
	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info) {
		if (system != null) {
			system.removeApplication(application);
			SystemManager.getInstance().deleteDiagram(uifbNetwork, system,
					application.getName() + ".xml"); //$NON-NLS-1$
			SystemManager.getInstance().saveSystem(system, false);			
			return Status.OK_STATUS;
		}
		return new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, "", null);

	}
}