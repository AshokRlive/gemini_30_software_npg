/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;
import org.fordiac.ide.application.editparts.UISubAppNetworkEditPart;
import org.fordiac.ide.gef.editparts.IDiagramEditPart;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.Palette.FBTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.ElementSelector;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The Class PasteCommand.
 */
public class PasteCommand extends Command {

	@SuppressWarnings("rawtypes")
	private final List templates;
	private final IDiagramEditPart destination;
	private final CompositeFBType compositeFBType;
	
	private final HashMap<String, FBView> copiedFBs = new HashMap<String, FBView>();
	
	private final List<FBView> fbsToCopy = new ArrayList<FBView>();
	
	private class ConnectionCopy {
		public InterfaceElementView source;
		public InterfaceElementView destination;
		public ConnectionView connView;
	};
	private final HashMap<String, ConnectionCopy> connectionsToCopy = new HashMap<String, ConnectionCopy>();
	
	private class SubAppCopy {
		public SubAppView origSubAppView;
		public String origName;
		public final ArrayList<FBView> newFBs = new ArrayList<FBView>();
		public final ArrayList<SubAppView> newSubApps = new ArrayList<SubAppView>();
	}
	private final List<SubAppCopy> subAppsToCopy = new ArrayList<SubAppCopy>();	
	private final HashMap<String, SubAppCopy> fbsWithSubApp = new HashMap<String, SubAppCopy>();
	
	private final ArrayList<FBCreateCommand> fbCreateCmds = new ArrayList<FBCreateCommand>();
	private final ArrayList<CreateSubAppCommand> subAppCreateCmds = new ArrayList<CreateSubAppCommand>();
	private final ArrayList<EventConnectionCreateCommand> eventConnCreateCmds = new ArrayList<EventConnectionCreateCommand>();
	private final ArrayList<DataConnectionCreateCommand> dataConnCreateCmds = new ArrayList<DataConnectionCreateCommand>();
	
	private final ArrayList<View> viewsToSelect = new ArrayList<View>();
	

	/**
	 * Instantiates a new paste command.
	 * 
	 * @param templates
	 *            the templates
	 * @param diagramEditPart
	 *            the diagram edit part
	 */
	@SuppressWarnings("rawtypes")
	public PasteCommand(List templates, IDiagramEditPart diagramEditPart) {
		this.templates = templates;
		this.destination = diagramEditPart;
		this.compositeFBType = null;
	}
	
	@SuppressWarnings("rawtypes")
	public PasteCommand(List templates, IDiagramEditPart diagramEditPart, CompositeFBType compositeFBType) {
		this.templates = templates;
		this.destination = diagramEditPart;
		this.compositeFBType = compositeFBType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void execute() {
		if (destination != null) {
			
			for (Iterator iterator = templates.iterator(); iterator.hasNext();) {
				Object object = iterator.next();
				if (object instanceof SubAppView) {
					traverseSubApps((SubAppView)object);
				}
				else if (object instanceof FBView) {	
					fbsToCopy.add((FBView)object);
				}
				else if(object instanceof ConnectionView){
					addConnectionToCopy((ConnectionView) object);
				}
			}
			
			copyFBs();
			copyConnections();
			
			// Create subapps of copied FBs
			copySubApps();
			
			ElementSelector selector = new ElementSelector();
			selector.selectViewObjects(viewsToSelect);
		}
	}
	
	private void addConnectionToCopy(ConnectionView connView) {
		String destName = ((FB)connView.getConnectionElement().getIInterfaceElementDestination().eContainer().eContainer()).getName();
		destName = destName + "." + connView.getConnectionElement().getIInterfaceElementDestination().getName();
		String srcName = ((FB)connView.getConnectionElement().getIInterfaceElementSource().eContainer().eContainer()).getName();
		srcName = srcName + "." + connView.getConnectionElement().getIInterfaceElementSource().getName();
		String connName = srcName + "-" + destName;
		if (!connectionsToCopy.containsKey(connName)) {
			ConnectionCopy connCopy = new ConnectionCopy();
			connCopy.connView = connView;
			connCopy.destination = connView.getDestination();
			connCopy.source = connView.getSource();
			connectionsToCopy.put(connName, connCopy);
		}
	}

	private void traverseSubApps(SubAppView saView) {
		SubAppCopy saCopy = new SubAppCopy();
		saCopy.origName = saView.getSubApp().getName();
		saCopy.origSubAppView = saView;
		for (View childView : saView.getUiSubAppNetwork().getChildren()) {
			if (childView instanceof FBView) {
				fbsWithSubApp.put(((FBView) childView).getFb().getName(), saCopy);
				
				fbsToCopy.add((FBView) childView);
				for (InterfaceElementView interfaceView : ((FBView)childView).getInterfaceElements()) {
					for (ConnectionView connView : interfaceView.getInConnections()) {
						addConnectionToCopy((ConnectionView) connView);							
					}
					for (ConnectionView connView : interfaceView.getOutConnections()) {
						addConnectionToCopy((ConnectionView) connView);							
					}					
				}
			} else if (childView instanceof SubAppView) {
				fbsWithSubApp.put(((SubAppView) childView).getSubApp().getName(), saCopy);
				
				traverseSubApps((SubAppView) childView);
			}
		}
		
		subAppsToCopy.add(saCopy);
	}
	
	private void copyFBs() {
		for (FBView fbView : fbsToCopy) {
			copyFB(fbView);
		}
	}

	private void copyConnections() {
		Collection<ConnectionCopy> conns = connectionsToCopy.values();
		for (ConnectionCopy connectionCopy : conns) {
			FB  dst = (FB)connectionCopy.connView.getConnectionElement().getIInterfaceElementDestination().eContainer().eContainer();
			FB  src = (FB)connectionCopy.connView.getConnectionElement().getIInterfaceElementSource().eContainer().eContainer();
			
			boolean doCopy = false;
			if (connectionCopy.source instanceof SubAppInterfaceElementView || 
					connectionCopy.destination instanceof SubAppInterfaceElementView) {
				// if the connection is part of a subapp only copy it if both FBs are copied
				if (null != copiedFBs.get(src.getName()) && (null != copiedFBs.get(dst.getName()))) {
					doCopy = true;
				}
			} else if (null != copiedFBs.get(src.getName()) || (null != copiedFBs.get(dst.getName()))){
				//only copy a connection if one of both ends is connected to a copied FB
				doCopy = true;
			}
			
			if (doCopy) {
				if(connectionCopy.connView.getConnectionElement() instanceof EventConnection){
					copyEventConnection(connectionCopy);
				}
				else if(connectionCopy.connView.getConnectionElement() instanceof DataConnection){
					if(null != copiedFBs.get(dst.getName())){
						//in order to avoid data fan-in data connections are only copied if also destination is part of the copy
						copyDataConnection(connectionCopy);
					}
				}
			}
		}		
	}
	

	private void copyEventConnection(ConnectionCopy connectionCopy) {
		InterfaceElementView sourceView = connectionCopy.source; 
		InterfaceElementView destinationView = connectionCopy.destination;
		Connection connectionElement = connectionCopy.connView.getConnectionElement();
		
		sourceView = checkForCopiedInterfaceElementView(copiedFBs.get(((FB)sourceView.getIInterfaceElement().eContainer().eContainer()).getName()), sourceView);
		destinationView = checkForCopiedInterfaceElementView(copiedFBs.get(((FB)destinationView.getIInterfaceElement().eContainer().eContainer()).getName()), destinationView);
		
		EventConnectionCreateCommand cmd = new EventConnectionCreateCommand();
		cmd.setSourceView(sourceView);
		cmd.setDestinationView(destinationView);
		cmd.setParent(destination.getDiagram());
		cmd.setArrangementConstraints(connectionElement.getDx1(), connectionElement.getDx2(), connectionElement.getDy());
		cmd.execute();
		
		eventConnCreateCmds.add(cmd);
	}
	
	private void copyDataConnection(ConnectionCopy connectionCopy) {
		InterfaceElementView sourceView = connectionCopy.source; 
		InterfaceElementView destinationView = connectionCopy.destination;
		Connection connectionElement = connectionCopy.connView.getConnectionElement();
		
		sourceView = checkForCopiedInterfaceElementView(copiedFBs.get(((FB)sourceView.getIInterfaceElement().eContainer().eContainer()).getName()), sourceView);
		destinationView = checkForCopiedInterfaceElementView(copiedFBs.get(((FB)destinationView.getIInterfaceElement().eContainer().eContainer()).getName()), destinationView);

		DataConnectionCreateCommand cmd = new DataConnectionCreateCommand();
		cmd.setParent(destination.getDiagram());
		cmd.setSourceView(sourceView);
		cmd.setDestinationView(destinationView);
		cmd.setArrangementConstraints(connectionElement.getDx1(), connectionElement.getDx2(), connectionElement.getDy());
		cmd.execute();
		
		dataConnCreateCmds.add(cmd);
	}

	private InterfaceElementView checkForCopiedInterfaceElementView(
			FBView fBView, InterfaceElementView origView) {
		InterfaceElementView retval = origView;
		
		if(null != fBView){
			//source is copied
			for (InterfaceElementView interfaceElement : fBView.getInterfaceElements()) {
				if(interfaceElement.getIInterfaceElement().getName().equals(origView.getIInterfaceElement().getName())){
					retval = interfaceElement;
					break;
				}
			}
		}
		return retval;
	}

	private void copyFB(final FBView fbView) {
		Rectangle bounds = new Rectangle(fbView.getPosition().getX() + 20, fbView.getPosition().getY() + 20, 10, 10);
		
		FBCreateCommand cmd = null;
		if (destination instanceof UISubAppNetworkEditPart) {
			cmd = new FBCreateInSubAppCommand((FBTypePaletteEntry)fbView.getFb().getPaletteEntry(), destination.getDiagram(), bounds);
		} else {
			cmd = new FBCreateCommand((FBTypePaletteEntry)fbView.getFb().getPaletteEntry(), destination.getDiagram(), bounds){

				@Override
				protected void createValues() {
					super.createValues();
					pasteParams(fbView.getFb(), getFB());
				}
				
				private void pasteParams(FB src, FB dst) {
					InterfaceList interfaceList = src.getInterface();
					if (interfaceList != null) {
						for (VarDeclaration varDecl : interfaceList .getInputVars()) {
							if (varDecl.getInputConnections().size() == 0) {
								Value value = varDecl.getValue();
								if (value != null && value.getValue() != null) {
									Value newValue = LibraryElementFactory.eINSTANCE.createValue();
									newValue.setValue(value.getValue());
									dst.getInterfaceElement(varDecl.getName()).setValue(newValue);
								}
							}
						}
					}
				}
				
			};
		}
		if(null != compositeFBType){
		     cmd.setParentComposite(compositeFBType);
	    }
		cmd.execute();
		
		String name = fbView.getFb().getName();		
		
		if (compositeFBType != null) {
			name = NameRepository.getInstance().getCompositeUniqueFBInstanceName(compositeFBType, name);
		} else {
			name = NameRepository.getInstance().getSystemUniqueFBInstanceName(SystemManager.getInstance().getSystemForDiagram(destination.getDiagram()), name, cmd.getFB().getId());
		}
		cmd.getFB().setName(name);
		
		FBView copiedFBView = getCopiedFBView(destination.getDiagram(), cmd.getFB());
		if(null != copiedFBView){
			copiedFBs.put(fbView.getFb().getName(), copiedFBView);
		}
		
		// Add to SubApp
//		if (fbsWithSubApp.containsKey(fbView.getFb().getName())) {
//			SubAppCopy saCopy = fbsWithSubApp.get(fbView.getFb().getName());
//			saCopy.newFBs.add(cmd.getFbView());
//		} else {
//			// FB not part of subapp, add to selection
//			viewsToSelect.add(cmd.getFbView());
//		}
		
		// Add to cmd list
		fbCreateCmds.add(cmd);
	}
	
	//currently the Connection create commands need the FBViews
	private FBView getCopiedFBView(Diagram diagram, FB fb) {
		for (View view : diagram.getChildren()) {
			if(view instanceof FBView){
				FBView fbView = (FBView)view;
				if(fbView.getFb().equals(fb)){
					return fbView;
				}
			}
		}
		return null;
	}

	private void copySubApps() {
		for (SubAppCopy saCopy : subAppsToCopy) {
			ArrayList<FBView> fbs = saCopy.newFBs;
			ArrayList<SubAppView> subApps = saCopy.newSubApps;
			
			CreateSubAppCommand cmd = new CreateSubAppCommand();
			cmd.setFbs(fbs);
			cmd.setSubApps(subApps);
			cmd.setParentNetwork(destination.getDiagram());
			
			Position newPos = (Position) EcoreUtil.copy(saCopy.origSubAppView.getPosition());
			newPos.setX(newPos.getX() + 20);
			newPos.setY(newPos.getY() + 20);
			cmd.setPosition(newPos);
			
			cmd.execute();
			
			if (fbsWithSubApp.containsKey(saCopy.origName)) {
				cmd.getSubAppView().getSubApp().setName(saCopy.origName);
				fbsWithSubApp.get(saCopy.origName).newSubApps.add(cmd.getSubAppView());
			} else {
				cmd.getSubAppView().getSubApp().setName(getNetworkUniqueSubAppName(saCopy.origName, destination.getDiagram()));
				// SubApp not part of subapp, add to selection
				viewsToSelect.add(cmd.getSubAppView());
			}
			
			subAppCreateCmds.add(cmd);
		}
	}
	
	private String getNetworkUniqueSubAppName(String desiredName, Diagram parentDiagram){
		
		String temp = desiredName;
		if(parentDiagram != null){
			if (parentDiagram instanceof UIFBNetwork) {
				FBNetwork parentNet = ((UIFBNetwork) parentDiagram).getFbNetwork();
				temp = NameRepository.getInstance().getNetworkUniqueSubApplicationName(parentNet, desiredName);
				
			} else if (parentDiagram instanceof UISubAppNetwork) {
				SubAppNetwork parentNet = ((UISubAppNetwork) parentDiagram).getSubAppNetwork();
				temp = NameRepository.getInstance().getNetworkUniqueSubApplicationName(parentNet, desiredName);
			}
		}
		
		return temp;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		// Redo FB copy
		for (FBCreateCommand cmd : fbCreateCmds) {
			cmd.redo();
		}
		
		// Redo connections
		for (EventConnectionCreateCommand cmd : eventConnCreateCmds) {
			cmd.redo();
		}
		for (DataConnectionCreateCommand cmd : dataConnCreateCmds) {
			cmd.redo();
		}
		
		// Redo subApps
		for (CreateSubAppCommand cmd : subAppCreateCmds) {
			cmd.redo();
		}
		
		ElementSelector selector = new ElementSelector();
		selector.selectViewObjects(viewsToSelect);
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		// Undo subApps
		for (int i=subAppCreateCmds.size()-1; i>=0; i--) {
			subAppCreateCmds.get(i).undo();
		}
		
		// Undo connections
		for (int i=eventConnCreateCmds.size()-1; i>=0; i--) {
			eventConnCreateCmds.get(i).undo();
		}
		for (int i=dataConnCreateCmds.size()-1; i>=0; i--) {
			dataConnCreateCmds.get(i).undo();
		}
		
		// Undo FBs
		for (int i=fbCreateCmds.size()-1; i>=0; i--) {
			fbCreateCmds.get(i).undo();
		}
		
		ElementSelector selector = new ElementSelector();
		selector.selectViewObjects(templates);
	}
}
