/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.geometry.Rectangle;
import org.fordiac.ide.model.Palette.FBTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.UISubAppNetwork;

/**
 * The Class FBCreateInSubAppCommand.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class FBCreateInSubAppCommand extends FBCreateCommand {

	/**
	 * FBCreatecommand for creating FBs in subApps.
	 * 
	 * @param type the type
	 * @param parent the parent
	 * @param bounds the bounds
	 */
	public FBCreateInSubAppCommand(final FBTypePaletteEntry type,
			final Diagram parent, final Rectangle bounds) {
		super(type, parent, bounds);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.application.commands.FBCreateCommand#execute()
	 */
	@Override
	public void execute() {
		super.execute();
		if (parentDiagram instanceof UISubAppNetwork) {
			addInterface(((UISubAppNetwork) parentDiagram), fB);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.application.commands.FBCreateCommand#undo()
	 */
	@Override
	public void undo() {
		super.undo();
		undoAddInterface();
	}

	/**
	 * Undo add interface.
	 */
	private void undoAddInterface() {
		for (Iterator<AddNewInterfaceElementCommand> iterator = cmds.iterator(); iterator
				.hasNext();) {
			AddNewInterfaceElementCommand cmd = iterator.next();
			cmd.undo();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.application.commands.FBCreateCommand#redo()
	 */
	@Override
	public void redo() {
		super.redo();
		redoAddInterface();
	}

	/**
	 * Redo add interface.
	 */
	private void redoAddInterface() {
		for (Iterator<AddNewInterfaceElementCommand> iterator = cmds.iterator(); iterator
				.hasNext();) {
			AddNewInterfaceElementCommand cmd = iterator.next();
			cmd.redo();
		}

	}

	/** The cmds. */
	List<AddNewInterfaceElementCommand> cmds = new ArrayList<AddNewInterfaceElementCommand>();

	/**
	 * Adds the interface.
	 * 
	 * @param uiSubAppNetwork
	 *            the ui sub app network
	 * @param fB
	 *            the functionBlock
	 */
	private void addInterface(final UISubAppNetwork uiSubAppNetwork, final FB fB) {
		InterfaceList interfaceList = fB.getInterface();

		for (Iterator<Event> iterator = interfaceList.getEventInputs()
				.iterator(); iterator.hasNext();) {
			Event event = iterator.next();
			AddNewInterfaceElementCommand cmd = new AddNewInterfaceElementCommand(
					uiSubAppNetwork.getSubAppView(), event);
			cmds.add(cmd);
			cmd.execute();
		}
		for (Iterator<Event> iterator = interfaceList.getEventOutputs()
				.iterator(); iterator.hasNext();) {
			Event event = iterator.next();
			AddNewInterfaceElementCommand cmd = new AddNewInterfaceElementCommand(
					uiSubAppNetwork.getSubAppView(), event);
			cmds.add(cmd);
			cmd.execute();
		}
		for (Iterator<VarDeclaration> iterator = interfaceList.getInputVars()
				.iterator(); iterator.hasNext();) {
			VarDeclaration varDeclaration = iterator.next();
			AddNewInterfaceElementCommand cmd = new AddNewInterfaceElementCommand(
					uiSubAppNetwork.getSubAppView(), varDeclaration);
			cmds.add(cmd);
			cmd.execute();
		}
		for (Iterator<VarDeclaration> iterator = interfaceList.getOutputVars()
				.iterator(); iterator.hasNext();) {
			VarDeclaration varDeclaration = iterator.next();
			AddNewInterfaceElementCommand cmd = new AddNewInterfaceElementCommand(
					uiSubAppNetwork.getSubAppView(), varDeclaration);
			cmds.add(cmd);
			cmd.execute();
		}
		if (uiSubAppNetwork.getSubAppView().eContainer() instanceof UISubAppNetwork) {
			addInterface((UISubAppNetwork) uiSubAppNetwork.getSubAppView()
					.eContainer(), fB);
		}
	}

}