/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.preferences.PreferenceConstants;
import org.fordiac.ide.util.Activator;

/**
 * The Class AddNewInterfaceElementCommand.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class AddNewInterfaceElementCommand extends Command {

	/** The sub app view. */
	private final SubAppView subAppView;

	/** The interface element. */
	private final IInterfaceElement interfaceElement;

	/** The sub app interface element view. */
	private SubAppInterfaceElementView subAppInterfaceElementView;

	/** The copy. */
	private MappedSubAppInterfaceElementView copy;

	/**
	 * Instantiates a new adds the new interface element command.
	 * 
	 * @param subAppView the sub app view
	 * @param interfaceElement the interface element
	 */
	public AddNewInterfaceElementCommand(final SubAppView subAppView,
			final IInterfaceElement interfaceElement) {
		this.subAppView = subAppView;
		this.interfaceElement = interfaceElement;

	}

	/**
	 * Returns the newly created SubAppInterfaceElementView
	 * 
	 * @return the subAppInterfaceElementView
	 */
	public SubAppInterfaceElementView getSubAppInterfaceElementView() {
		return subAppInterfaceElementView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		boolean visibility = Activator
				.getDefault()
				.getPreferenceStore()
				.getBoolean(
						PreferenceConstants.P_VISIBILITY_OF_SUB_APP_INTERFACE_ELEMENTS);
		subAppInterfaceElementView = UiFactory.eINSTANCE
				.createSubAppInterfaceElementView();
		subAppInterfaceElementView.setIInterfaceElement(interfaceElement);
		subAppInterfaceElementView.setVisible(visibility);
		subAppInterfaceElementView.setFbNetwork(((Diagram) subAppView.eContainer()).getNetwork());

		if (subAppView.getMappedSubApp() != null) {

			copy = UiFactory.eINSTANCE.createMappedSubAppInterfaceElementView();
			copy.setApplicationInterfaceElement(subAppInterfaceElementView);
			copy.setIInterfaceElement(subAppInterfaceElementView
					.getIInterfaceElement());

		}
		
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		subAppView.getInterfaceElements().add(subAppInterfaceElementView);

		if (subAppView.getMappedSubApp() != null) {


			subAppInterfaceElementView.setMappedInterfaceElement(copy);
			
			UIResourceEditor uiResourceEditor = (UIResourceEditor) subAppView
					.getMappedSubApp().eContainer();
			copy.setFbNetwork(uiResourceEditor.getResourceElement()
					.getFBNetwork());
			
			subAppView.getMappedSubApp().getInterfaceElements().add(copy);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {

		if (subAppView.getMappedSubApp() != null && copy != null) {
			subAppInterfaceElementView.setMappedInterfaceElement(null);
			copy.setFbNetwork(null);
			subAppView.getMappedSubApp().getInterfaceElements().remove(copy);

		}

		subAppView.getInterfaceElements().remove(subAppInterfaceElementView);
	}

}
