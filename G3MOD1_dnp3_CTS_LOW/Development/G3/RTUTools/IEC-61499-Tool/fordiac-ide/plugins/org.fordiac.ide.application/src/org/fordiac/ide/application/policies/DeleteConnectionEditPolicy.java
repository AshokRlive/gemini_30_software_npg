/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.GroupRequest;
import org.fordiac.ide.application.editparts.ConnectionEditPart;
import org.fordiac.ide.util.commands.DeleteConnectionCommand;

/**
 * This EditPolicy returns a command for deleting a Connection.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class DeleteConnectionEditPolicy extends
		org.eclipse.gef.editpolicies.ConnectionEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.ConnectionEditPolicy#getDeleteCommand(org.eclipse.gef.requests.GroupRequest)
	 */
	@Override
	protected Command getDeleteCommand(final GroupRequest request) {
		if (getHost() instanceof ConnectionEditPart) {
			DeleteConnectionCommand c = new DeleteConnectionCommand(
					((ConnectionEditPart) getHost()).getCastedModel());
			return c;
		}
		return null;
	}
}
