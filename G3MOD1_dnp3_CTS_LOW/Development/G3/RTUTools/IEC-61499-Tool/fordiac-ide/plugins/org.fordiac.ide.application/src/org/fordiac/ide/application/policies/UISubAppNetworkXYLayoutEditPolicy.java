/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.policies;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.CreateRequest;
import org.fordiac.ide.application.commands.FBCreateInSubAppCommand;
import org.fordiac.ide.model.ui.Diagram;

/**
 * The Class UISubAppNetworkXYLayoutEditPolicy.
 */
public class UISubAppNetworkXYLayoutEditPolicy extends
		FBNetworkXYLayoutEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see LayoutEditPolicy#getCreateCommand(CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(final CreateRequest request) {
		if (request == null) {
			return null;
		}
		Object childClass = request.getNewObjectType();
		Rectangle constraint = (Rectangle) getConstraintFor(request);
		if (childClass instanceof org.fordiac.ide.model.Palette.FBTypePaletteEntry) {
			org.fordiac.ide.model.Palette.FBTypePaletteEntry type = (org.fordiac.ide.model.Palette.FBTypePaletteEntry) request
					.getNewObjectType();
			if (getHost().getModel() instanceof Diagram) {
				return new FBCreateInSubAppCommand(type, (Diagram) getHost()
						.getModel(), new Rectangle(constraint.getLocation().x,
						constraint.getLocation().y, -1, -1));
			}
		}
		return null;
	}

}
