/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.util.commands.ConnectionUtil;

/**
 * The Class AbstractDataConnectionCreateCommand.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public abstract class AbstractDataConnectionCreateCommand extends AbstractConnectionCreateCommand {

	/** The data connection. */
	protected DataConnection dataConnection;

	private boolean checkWith = true;

	public boolean isCheckWith() {
		return checkWith;
	}

	public void setCheckWith(boolean checkWith) {
		this.checkWith = checkWith;
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		setEditor(ApplicationPlugin.getDefault().getCurrentActiveEditor());
		connectionView = UiFactory.eINSTANCE.createConnectionView();

		checkSourceAndTarget();

		if (sourceView instanceof InternalSubAppInterfaceElementView) {
			findOuterSource();
		}
		if (destinationView instanceof InternalSubAppInterfaceElementView) {
			findOuterDest();
		}
		
		dataConnection = LibraryElementFactory.eINSTANCE.createDataConnection();
		dataConnection.setSource((VarDeclaration) sourceView
				.getIInterfaceElement());
		dataConnection.setDestination((VarDeclaration) destinationView
				.getIInterfaceElement());
		dataConnection.setDx1(getDx1());
		dataConnection.setDx2(getDx2());
		dataConnection.setDy(getDy());

		connectionView.setConnectionElement(dataConnection);

		connectionView.setSource(sourceView);
		connectionView.setDestination(destinationView);

		if (sourceView instanceof SubAppInterfaceElementView) {
			// create internal connection
			SubAppInterfaceElementView subAppInterfaceElementViewSrc = (SubAppInterfaceElementView) sourceView;
			
			if(subAppInterfaceElementViewSrc instanceof MappedSubAppInterfaceElementView){
				subAppInterfaceElementViewSrc = (SubAppInterfaceElementView)((MappedSubAppInterfaceElementView) subAppInterfaceElementViewSrc).getApplicationInterfaceElement();
			}
			
			if(null == ((SubAppView) subAppInterfaceElementViewSrc.eContainer()).getSubApp().getPaletteEntry()){
				//only go into the sub-app if don't have a type
				UISubAppNetwork destNetwork = ((SubAppView) subAppInterfaceElementViewSrc
						.eContainer()).getUiSubAppNetwork();
				
				internalConnections.addAll(ConnectionUtil
						.createInternalSourceConnection(
								subAppInterfaceElementViewSrc, destNetwork,
								connectionView));
			}
		}
		if (destinationView instanceof SubAppInterfaceElementView) {
			// create internal connection
			SubAppInterfaceElementView subAppInterfaceElementViewDest = (SubAppInterfaceElementView) destinationView;
			
			if(subAppInterfaceElementViewDest instanceof MappedSubAppInterfaceElementView){
				subAppInterfaceElementViewDest = (SubAppInterfaceElementView)((MappedSubAppInterfaceElementView) subAppInterfaceElementViewDest).getApplicationInterfaceElement();
			}
			if(null == ((SubAppView) subAppInterfaceElementViewDest.eContainer()).getSubApp().getPaletteEntry()){
					//only go into the sub-app if don't have a type
				UISubAppNetwork destNetwork = ((SubAppView) subAppInterfaceElementViewDest
						.eContainer()).getUiSubAppNetwork();
				
				internalConnections.addAll(ConnectionUtil
						.createInternalDestConnection(
								subAppInterfaceElementViewDest, destNetwork,
								connectionView));
			}
		}
		parent.getFunctionBlockNetwork().getDataConnections()
				.add(dataConnection);
		parent.getConnections().add(connectionView);

		doAdvancedCreation();

	}
	
	
	private void checkSourceAndTarget() {
		VarDeclaration dataInput = (VarDeclaration) sourceView
				.getIInterfaceElement();

		InterfaceList ifList = (InterfaceList) dataInput.eContainer();
		boolean needsChange = false;

		if (ifList.eContainer() instanceof CompositeFBType) {
			needsChange = (!(ifList.getInputVars().contains(dataInput)));
		} else {
			needsChange = (!(ifList.getOutputVars().contains(dataInput)));
		}

		if (needsChange) {
			// our src is an input we have to swap source and target
			InterfaceElementView buf = destinationView;
			destinationView = sourceView;
			sourceView = buf;
		}
	}

	/**
	 * Do advanced creation.
	 */
	protected abstract void doAdvancedCreation();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		if (sourceView == null && destinationView == null) {
			return false;
		}
		if (sourceView != null
				&& !(sourceView.getIInterfaceElement() instanceof VarDeclaration)) {
			return false;
		}
		if (destinationView != null
				&& !(destinationView.getIInterfaceElement() instanceof VarDeclaration)) {
			return false;
		}
		if ((sourceView == null) || (destinationView == null)) {
			return true; // link creation is in progress; source is not
			// defined yet
		}

		boolean retVal = LinkConstraints.canCreateDataConnection(
				(VarDeclaration) sourceView.getIInterfaceElement(),
				(VarDeclaration) destinationView.getIInterfaceElement(),
				internalConnection, checkWith);
		return retVal;
	}

	/**
	 * Undo advanced creation.
	 */
	protected abstract void undoAdvancedCreation();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		undoAdvancedCreation();
		ConnectionUtil.undoInternalConnection(internalConnections);

		dataConnection.setSource(null);
		dataConnection.setDestination(null);
		parent.getFunctionBlockNetwork().getDataConnections()
				.remove(dataConnection);

		connectionView.setSource(null);
		connectionView.setDestination(null);
		parent.getConnections().remove(connectionView);

	}

	/**
	 * Redo advanced creation.
	 */
	protected abstract void redoAdvancedCreation();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {

		ConnectionUtil.redoInternalConnection(internalConnections);

		dataConnection.setSource((VarDeclaration) sourceView
				.getIInterfaceElement());
		dataConnection.setDestination((VarDeclaration) destinationView
				.getIInterfaceElement());
		parent.getFunctionBlockNetwork().getDataConnections()
				.add(dataConnection);

		connectionView.setSource(sourceView);
		connectionView.setDestination(destinationView);
		parent.getConnections().add(connectionView);

		redoAdvancedCreation();

	}
	
}
