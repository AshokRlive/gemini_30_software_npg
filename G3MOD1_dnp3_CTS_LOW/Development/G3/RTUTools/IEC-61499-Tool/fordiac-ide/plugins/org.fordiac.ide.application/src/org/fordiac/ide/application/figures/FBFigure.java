/*******************************************************************************
 * Copyright (c) 2010 - 2017 Profactor GmbH, ACIN, fortiss GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Alois Zoitl, Gerd Kainz, Gerhard Ebenhofer, Monika Wenger
 *    - initial implementation
 *******************************************************************************/
package org.fordiac.ide.application.figures;

import org.eclipse.core.resources.IMarker;
import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.draw2d.AdvancedRoundedRectangle;
import org.fordiac.ide.gef.draw2d.ITransparencyFigure;
import org.fordiac.ide.gef.draw2d.SetableAlphaLabel;
import org.fordiac.ide.gef.draw2d.UnderlineAlphaLabel;
import org.fordiac.ide.gef.preferences.DiagramPreferences;
import org.fordiac.ide.model.libraryElement.AdapterFBType;
import org.fordiac.ide.model.libraryElement.Annotation;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.VersionInfo;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.preferences.PreferenceConstants;
import org.fordiac.ide.preferences.PreferenceGetter;
import org.fordiac.ide.util.imageprovider.FordiacImage;

/**
 * The visualization of an FB. It Provides several containers for its interface.
 */
public class FBFigure extends Shape implements ITransparencyFigure {
	protected static final Dimension MIN_DIMENSION = new Dimension(50, 50);
	protected FBView model = null;
	protected SetableAlphaLabel instanceNameLabel = null;
	protected final Figure main = new Figure();
	protected RoundedRectangle top;
	protected final Figure topInputs = new Figure();
	protected final Figure topOutputs = new Figure();
	protected AdvancedRoundedRectangle middle;
	protected final Figure middleInputs = new Figure();
	protected final Figure middleOutputs = new Figure();
	protected RoundedRectangle bottom;
	protected final Figure bottomInputs = new Figure();
	protected final Figure bottomOutputs = new Figure();
	protected final Figure eventInputs = new Figure();
	protected final Figure eventOutputs = new Figure();
	protected final Figure dataInputs = new Figure();
	protected final Figure sockets = new Figure();
	protected final Figure dataOutputs = new Figure();
	protected final Figure plugs = new Figure();
	protected TopBorder middleBorder;
	protected TopBorder bottomBorder;
	private Figure middleContainer;
	protected UnderlineAlphaLabel typeLabel;
	protected UnderlineAlphaLabel versionLabel;
	private ZoomManager zoomManager;
	
	public AdvancedRoundedRectangle getMiddle() {
		return middle;
	}
	
	public FBFigure(final FBView model, ZoomManager zoomManager) {
	this.model = model;
	this.zoomManager = zoomManager;
	configureRectangles(model.getFb().getFBType());
	this.setFillXOR(false);
	this.setOpaque(false);
	GridData instanceNameLayout = new GridData();
	instanceNameLayout.grabExcessHorizontalSpace = true;
	instanceNameLayout.horizontalAlignment = SWT.CENTER;
	
	instanceNameLabel = new SetableAlphaLabel();
	instanceNameLabel.setText(model.getFb().getName());
	instanceNameLabel.setTextAlignment(PositionConstants.CENTER);
	instanceNameLabel.setLabelAlignment(PositionConstants.CENTER);
	//instanceNameLabel.setBackgroundColor(org.eclipse.draw2d.ColorConstants.white);
	
	GridLayout gridLayout = new GridLayout(1, true);
	gridLayout.verticalSpacing = 2;
	gridLayout.marginHeight = 0;
	gridLayout.marginWidth = 0;
	setLayoutManager(gridLayout);
	
	GridLayout mainLayout = new GridLayout(3, false);
	mainLayout.marginHeight = 0;
	mainLayout.marginWidth = 0;
	mainLayout.horizontalSpacing = 0;
	mainLayout.verticalSpacing = -1;
	GridData mainLayoutData = new GridData(GridData.HORIZONTAL_ALIGN_FILL
			| GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL
			| GridData.GRAB_VERTICAL);
	
	main.setLayoutManager(mainLayout);
	
	add(main);
	setConstraint(main, mainLayoutData);
	main.add(new SetableAlphaLabel()); // to fill the layoutmanagers first
	// row
	main.add(instanceNameLabel);
	main.setConstraint(instanceNameLabel, instanceNameLayout);
	main.add(new SetableAlphaLabel()); // to fill the layoutmanagers first
	// row
	IPreferenceStore pf = Activator.getDefault().getPreferenceStore();
	int cornerDim = pf.getInt(DiagramPreferences.CORNER_DIM);
	top.setCornerDimensions(new Dimension(cornerDim, cornerDim));
	GridLayout topLayout = new GridLayout(2, false);
	topLayout.marginHeight = 4;
	topLayout.marginWidth = 1;
	topLayout.horizontalSpacing = 2;
	GridData topLayoutData = new GridData(GridData.HORIZONTAL_ALIGN_FILL
			| GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL);
	
	top.setLayoutManager(topLayout);
	main.add(topInputs);
	main.add(top);
	main.add(topOutputs);
	main.setConstraint(top, topLayoutData);
	//
	ToolbarLayout topInputsLayout = new ToolbarLayout(false);
	GridData topInputsLayoutData = new GridData(
			GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
			| GridData.VERTICAL_ALIGN_FILL | GridData.GRAB_VERTICAL);
	eventInputs.setLayoutManager(topInputsLayout);
	//
	top.add(eventInputs);
	top.setConstraint(eventInputs, topInputsLayoutData);
	
	//
	ToolbarLayout topOutputsLayout = new ToolbarLayout(false);
	GridData topOutputsLayoutData = new GridData(
			GridData.HORIZONTAL_ALIGN_END | GridData.GRAB_HORIZONTAL
			| GridData.VERTICAL_ALIGN_FILL | GridData.GRAB_VERTICAL);
	topOutputsLayout.setMinorAlignment(ToolbarLayout.ALIGN_BOTTOMRIGHT);
	eventOutputs.setLayoutManager(topOutputsLayout);
	top.add(eventOutputs);
	top.setConstraint(eventOutputs, topOutputsLayoutData);
	
	middleContainer = new Figure();
	BorderLayout borderLayout;
	middleContainer.setLayoutManager(borderLayout = new BorderLayout());
	borderLayout.setHorizontalSpacing(10);
	middleContainer.setBorder(new MarginBorder(0, 7, 0, 7));
	
	main.add(middleInputs);
	main.add(middleContainer);
	main.add(middleOutputs);
	middleContainer.add(middle, BorderLayout.CENTER);
	middle.setCornerDimensions(new Dimension());
	
	GridLayout middleLayout = new GridLayout(1, true);
	GridData middleLayouData = new GridData(GridData.HORIZONTAL_ALIGN_FILL
			| GridData.GRAB_HORIZONTAL);
	main.setConstraint(middleContainer, middleLayouData);
	
	middle.setLayoutManager(middleLayout);
	middleLayout.marginHeight = 0;
	middleLayout.verticalSpacing = 1;
	
	setupTypeNameAndVersion(model);
	//
	bottom.setCornerDimensions(new Dimension(cornerDim, cornerDim));
//		bottom.setBorder(bottomBorder = new TopBorder(getBackgroundColor(),
//				14 - 4));
	GridLayout bottomLayout = new GridLayout(2, false);
	bottomLayout.marginHeight = 4;
	bottomLayout.marginWidth = 1;
	bottomLayout.horizontalSpacing = 0;
	bottom.setLayoutManager(bottomLayout);
	GridData bottomLayoutData = new GridData(GridData.HORIZONTAL_ALIGN_FILL
			| GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL
			| GridData.GRAB_VERTICAL);
	bottomLayoutData.verticalAlignment = SWT.TOP;
	
	GridLayout bottomInputValuesLayout = new GridLayout();
	bottomInputValuesLayout.marginHeight = 0;
	bottomInputValuesLayout.marginWidth = 0;
	bottomInputValuesLayout.horizontalSpacing = 0;
	
	GridData bottomILayoutData = new GridData(
			GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
			| GridData.VERTICAL_ALIGN_FILL | GridData.GRAB_VERTICAL);
	bottomILayoutData.verticalAlignment = SWT.TOP;
	main.add(bottomInputs);
	main.setConstraint(bottomInputs, bottomILayoutData);
	bottomInputs.setLayoutManager(bottomInputValuesLayout);
	
	main.add(bottom);
	main.add(bottomOutputs);
	main.setConstraint(bottom, bottomLayoutData);
	
	Figure bottomInputArea = new Figure();
	bottomInputArea.setLayoutManager(new ToolbarLayout(false));
	
	GridData bottomInputsLayoutData = new GridData(
			GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
			| GridData.VERTICAL_ALIGN_FILL | GridData.GRAB_VERTICAL);
	bottomInputsLayoutData.verticalAlignment = SWT.TOP;
	bottom.add(bottomInputArea);
	bottom.setConstraint(bottomInputArea, bottomInputsLayoutData);
	
	dataInputs.setLayoutManager(new ToolbarLayout(false));
	bottomInputArea.add(dataInputs);
	
	sockets.setLayoutManager(new ToolbarLayout(false));
	bottomInputArea.add(sockets);
	
	Figure bottomOutputArea = new Figure();
	bottomOutputArea.setLayoutManager(new ToolbarLayout(false));
	((ToolbarLayout)bottomOutputArea.getLayoutManager()).setMinorAlignment(ToolbarLayout.ALIGN_BOTTOMRIGHT);
	
	// bottomOutputsLayout.setStretchMinorAxis(true);		
	GridData bottomOutputsLayoutData = new GridData(
			GridData.HORIZONTAL_ALIGN_END | GridData.GRAB_HORIZONTAL
			| GridData.VERTICAL_ALIGN_FILL);
	bottom.add(bottomOutputArea);
	bottom.setConstraint(bottomOutputArea, bottomOutputsLayoutData);
	
	
	dataOutputs.setLayoutManager(new ToolbarLayout(false));
	((ToolbarLayout)dataOutputs.getLayoutManager()).setMinorAlignment(ToolbarLayout.ALIGN_BOTTOMRIGHT);
	bottomOutputArea.add(dataOutputs);
	
	plugs.setLayoutManager(new ToolbarLayout(false));
	((ToolbarLayout)plugs.getLayoutManager()).setMinorAlignment(ToolbarLayout.ALIGN_BOTTOMRIGHT);
	bottomOutputArea.add(plugs);
	
	refreshToolTips();
	updateResourceTypeFigure();		
}

	private Color getColor(FBType type){
		if(type instanceof AdapterFBType){
			return PreferenceGetter.getColor(PreferenceConstants.P_ADAPTER_CONNECTOR_COLOR);
		}
		return ColorConstants.gray;
	}
	
	public ZoomManager getZoomManager() {
		return zoomManager;
	}
	
	private void configureRectangles(FBType type) {	
		top = new AdvancedRoundedRectangle(
				PositionConstants.NORTH | PositionConstants.EAST
				| PositionConstants.WEST, zoomManager, main, true, getColor(type));
		
		middle = new AdvancedRoundedRectangle(
				PositionConstants.EAST | PositionConstants.WEST, zoomManager,main, true, getColor(type));
		
		bottom = new AdvancedRoundedRectangle(
				PositionConstants.SOUTH | PositionConstants.EAST
				| PositionConstants.WEST, zoomManager,main,  true, getColor(type));
	}

	private void setupTypeNameAndVersion(final FBView model) {		
		FBType type = model.getFb().getFBType();
		VersionInfo versionInfo = null;
		String typeName = null;

		if (type != null) {
			typeName = type.getName();
			if (type.getVersionInfo().size() > 0) {
				versionInfo = type.getVersionInfo().get(0);
			}
		}
		else{
			typeName = Messages.FBFigure_TYPE_NOT_SET;
		}
			
		middle.add(typeLabel = new UnderlineAlphaLabel(
				typeName != null ? typeName
						: Messages.FBFigure_NOT_DEFINED_Text));
		typeLabel.setTextAlignment(PositionConstants.CENTER);
		middle.setConstraint(typeLabel, new GridData(
				GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL));
		typeLabel.setOpaque(false);
		
		middle.add(versionLabel = new UnderlineAlphaLabel(
				versionInfo != null ? versionInfo.getVersion()
						: Messages.FBFigure_NOT_DEFINED_Text));
		versionLabel.setTextAlignment(PositionConstants.CENTER);
		versionLabel.setOpaque(false);

		middle.setConstraint(versionLabel, new GridData(
				GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL));
		setupMouseListener();
	}

	private void setupMouseListener() {
		middle.addMouseMotionListener(new MouseMotionListener(){

			@Override
			public void mouseDragged(MouseEvent me) {
			}

			@Override
			public void mouseEntered(MouseEvent me) {
				if( 0 != (me.getState() & SWT.CONTROL)){
					typeLabel.setDrawUnderline(true);
					versionLabel.setDrawUnderline(true);
				}
			}

			@Override
			public void mouseExited(MouseEvent me) {
				typeLabel.setDrawUnderline(false);
				versionLabel.setDrawUnderline(false);
			}

			@Override
			public void mouseHover(MouseEvent me) {
				//currently mouseHover should be the same as mouse moved
				mouseMoved(me);
			}

			@Override
			public void mouseMoved(MouseEvent me) {
				if( 0 != (me.getState() & SWT.CONTROL)){
					if(!typeLabel.isDrawUnderline()){
						typeLabel.setDrawUnderline(true);
						versionLabel.setDrawUnderline(true);
					}
				}
				else{
					if(typeLabel.isDrawUnderline()){
						typeLabel.setDrawUnderline(false);
						versionLabel.setDrawUnderline(false);
					}
				}
			}
			
		});
		
	}

	private void updateResourceTypeFigure() {
		if (model.getFb().isResourceTypeFB()) {
			getInstanceNameLabel().setIcon(FordiacImage.ICON_LockedState.getImage());
		}
	}

	/**
	 * Sets the mapped.
	 * 
	 * @param mapped
	 *            the new mapped
	 */
	public void setMapped(final boolean mapped) {
		if (model.getFb().isResourceTypeFB()) {
			updateResourceTypeFigure();
		} else {
			getInstanceNameLabel().setIcon(null);
		}
		updateOverlay();
	}

	private void updateOverlay() {
		Image image = getInstanceNameLabel().getIcon();
		for (Annotation anno : model.getFb().getAnnotations()) {
			if (anno.getServity() == IMarker.SEVERITY_ERROR) {
				image = FordiacImage.getErrorOverlayImage(image);
				break;
			}
		}
		if(null == model.getFb().getPaletteEntry()){
			image = FordiacImage.getErrorOverlayImage(image);
		}
		getInstanceNameLabel().setIcon(image);
	}

	public void refreshIcon() {
		setMapped(model.getMappedFB() != null
				|| model.getApplicationFB() != null);
	}

	@Override
	public String toString() {
		return model.getFb().getPaletteEntry().getLabel() + ":" //$NON-NLS-1$
				+ model.getFb().getName();
	}

	public SetableAlphaLabel getInstanceNameLabel() {
		return instanceNameLabel;
	}

	public void refreshToolTips() {
		setToolTip(new FBTooltipFigure(model));
		setMapped(model.getMappedFB() != null
				|| model.getApplicationFB() != null);
	}

	public Figure getEventInputs() {
		return eventInputs;
	}

	public Figure getEventOutputs() {
		return eventOutputs;
	}

	public Figure getDataInputs() {
		return dataInputs;
	}
	
	public Figure getSockets() {
		return sockets;
	}

	public Figure getDataOutputs() {
		return dataOutputs;
	}
	
	public Figure getPlugs() {
		return plugs;
	}

	public class TopBorder extends LineBorder {
		/** The corner dimensions. */
		int cornerDimensions = 0;
		int alpha = 255;

		/**
		 * Instantiates a new top border.
		 * 
		 * @param color
		 *            the color
		 * @param cornerDimensions
		 *            the corner dimensions
		 */
		public TopBorder(final Color color, final int cornerDimensions) {
			super(color);
			this.cornerDimensions = cornerDimensions;
		}

		@Override
		public Insets getInsets(final IFigure figure) {
			return new Insets(0, 0, 0, 0);
		}

		@Override
		public void paint(final IFigure figure, final Graphics graphics,
				final Insets insets) {
			graphics.setAlpha(alpha);

			tempRect.setBounds(getPaintRectangle(figure, insets));
			if (Math.abs(getWidth() % 2) == 1) {
				tempRect.width--;
				tempRect.height--;
			}
			tempRect.shrink(getWidth() / 2, getWidth() / 2);
			graphics.setLineWidth(getWidth());
			if (getColor() != null) {
				graphics.setForegroundColor(getColor());
			}
			graphics.drawLine(tempRect.x + cornerDimensions, tempRect.y,
					tempRect.x + tempRect.width - cornerDimensions, tempRect.y);

		}

		public void setAlpha(int alpha) {
			if (this.alpha != alpha) {
				this.alpha = alpha;

			}
		}

		public int getAlpha() {
			return alpha;
		}

		@Override
		public boolean isOpaque() {
			return alpha > 254;
		}

	}

	@Override
	protected void fillShape(final Graphics graphics) {
		// not used
	}

	@Override
	protected void outlineShape(final Graphics graphics) {
		// not used
	}

	@Override
	public void setBackgroundColor(final Color bg) {
		super.setBackgroundColor(bg);
		if (middleBorder != null) {
			middleBorder.setColor(bg);
		}
		if (bottomBorder != null) {
			bottomBorder.setColor(bg);
		}
		repaint();
	}

	@Override
	public void setAlpha(int value) {
		super.setAlpha(value);

		bottom.setAlpha(value);
		top.setAlpha(value);
		middle.setAlpha(value);

		if (instanceNameLabel != null) {
			instanceNameLabel.setAlpha(value);
		}
		if (versionLabel != null) {
			versionLabel.setAlpha(value);
		}
		if (typeLabel != null) {
			typeLabel.setAlpha(value);
		}
		if (middleBorder != null) {
			middleBorder.setAlpha(value);
		}
		if (bottomBorder != null) {
			bottomBorder.setAlpha(value);
		}
		// middleContainer.setOpaque(false);

	}

	@Override
	public void setTransparency(int value) {
		setAlpha(value);
	}

	@Override
	public int getTransparency() {
		return getAlpha();
	}

	public Image getIcon() {
		return getInstanceNameLabel().getIcon();
	}
	
	public void setIcon(Image image) {
		getInstanceNameLabel().setIcon(image);
	}
}
