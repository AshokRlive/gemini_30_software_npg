/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseListener;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.tools.DirectEditManager;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.application.actions.OpenCompositeInstanceViewerAction;
import org.fordiac.ide.application.figures.FBFigure;
import org.fordiac.ide.application.policies.DeleteFBEditPolicy;
import org.fordiac.ide.application.policies.FBRenameEditPolicy;
import org.fordiac.ide.application.policies.FBSelectionPolicy;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.gef.editparts.InputValuesEditPart;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.gef.editparts.LabelDirectEditManager;
import org.fordiac.ide.gef.editparts.NameCellEditorLocator;
import org.fordiac.ide.gef.properties.PropertyUtil;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.Color;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UiPackage;
import org.fordiac.ide.preferences.PreferenceConstants;
import org.fordiac.ide.util.IdentifierVerifyListener;

/**
 * This class implements an EditPart for a FunctionBlock.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class FBEditPart extends AbstractViewEditPart implements
		InputValuesEditPart {

	private EContentAdapter adapter;

	private DeviceView referencedDevice;

	/** necessary that the gradient pattern can be scaled accordingly */
	private ZoomManager zoomManager;

	public FBEditPart(ZoomManager zoomManager) {
		super();
		this.zoomManager = zoomManager;
	}

	private EContentAdapter colorChangeListener = new EContentAdapter() {
		public void notifyChanged(Notification notification) {
			if (notification.getFeature() == UiPackage.eINSTANCE
					.getView_BackgroundColor()) {
				backgroundColorChanged(getFigure());
			}
		};
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getContentAdapter()
	 */
	@Override
	public EContentAdapter getContentAdapter() {
		if (adapter == null) {
			adapter = new EContentAdapter() {

				@Override
				public void notifyChanged(final Notification notification) {
					super.notifyChanged(notification);
					refreshToolTip();
					backgroundColorChanged(getFigure());
					if (notification.getFeature() == UiPackage.eINSTANCE
							.getFBView_MappedFB()) {
						if (notification.getNewValue() instanceof FBView) {
							updateDeviceViewListener();
						}
					}
				}

			};
		}
		return adapter;
	}

	/** The i named element content adapter. */
	private final EContentAdapter annotationContentAdapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			if (notification.getFeature() == LibraryElementPackage.eINSTANCE
					.getI4DIACElement_Annotations()) {
				refreshName();
				
			}
		}

	};

	private void updateDeviceViewListener() {
		DeviceView devView = findDeviceView();
		if (devView != null) {
			if (devView.equals(referencedDevice)) {
				// nothing to do
			} else {
				if (referencedDevice != null) {
					referencedDevice.eAdapters().remove(colorChangeListener);
				}
				referencedDevice = devView;
				referencedDevice.eAdapters().add(colorChangeListener);
			}
		}
	}

	@Override
	public void activate() {
		super.activate();
		updateDeviceViewListener();
		if (getCastedModel() != null && getCastedModel().getFb() != null) {
			getCastedModel().getFb().eAdapters().add(annotationContentAdapter);
		}
	}

	@Override
	public void deactivate() {
		super.deactivate();
		if (referencedDevice != null) {
			referencedDevice.eAdapters().remove(colorChangeListener);
		}
	}

	private void refreshToolTip() {
		getCastedFigure().refreshToolTips();
	}

	@Override
	protected void refreshComment() {
		refreshToolTip();
	}

	@Override
	protected void refreshName() {
		super.refreshName();
		getCastedFigure().refreshIcon();
	}

	/**
	 * Creates the figure (for the specified model) to be used as this parts
	 * visuals.
	 * 
	 * @return IFigure The figure for the model
	 */
	@Override
	protected IFigure createFigureForModel() {
		// extend this if FunctionBlock gets extended!
		FBView fb = getCastedModel();
		FBFigure f = null;
		if (fb.getFb() != null) {
			f = new FBFigure(getCastedModel(), getZoomManager());
		}

		if (f != null) {
			f.getMiddle().addMouseListener(new MouseListener(){

				@Override
				public void mousePressed(MouseEvent me) {
					if( 0 != (me.getState() & SWT.CONTROL) && isOnlyFBOrNothingSelected()){
							openFBTypeInEditor();
					}					
				}

				@Override
				public void mouseReleased(MouseEvent me) {					
				}

				@Override
				public void mouseDoubleClicked(MouseEvent me) {					
				}
				
			});			
			return f;
		} else {
			throw new IllegalArgumentException(
					Messages.FBEditPart_ERROR_UnsupportedFBType);
		}
	}
	
	public boolean isOnlyFBOrNothingSelected() {
		@SuppressWarnings("unchecked")
		List<EditPart> selection = getViewer().getSelectedEditParts();
		if(selection.size() > 1){
			return false;
		} else if(selection.size() == 1){
			return selection.get(0) == this;
		}
		return true;
	}
	
	private void openFBTypeInEditor(){
		//open the default editor for the adapter file
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		
		PaletteEntry entry = getCastedModel().getFb().getPaletteEntry();
		if(null != entry)	{			
			IEditorDescriptor desc = PlatformUI.getWorkbench().
			        getEditorRegistry().getDefaultEditor(entry.getFile().getName());
			try {
				page.openEditor(new FileEditorInput(entry.getFile()), desc.getId());
			} catch (PartInitException e) {
				ApplicationPlugin.getDefault().logError(e.getMessage(), e);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		// allow delete of a FB
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new DeleteFBEditPolicy());

		// Highlight In and Outconnections of the selected fb, allow alignment
		// of FBs
		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE,
				new FBSelectionPolicy());

		// A FB needs a special rename policy as it needs to update the unique
		// name data base
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new FBRenameEditPolicy());

	}
	
	/**
	 * Returns the "View" of this editpart.
	 * 
	 * @return the FBView
	 */
	public FBView getCastedModel() {
		return (FBView) getModel();
	}

	/**
	 * Returns the label wich contains the instance name of a FB.
	 * 
	 * @return the label
	 */
	public Label getInstanceNameLabel() {
		if (getCastedFigure() != null) {
			return getCastedFigure().getInstanceNameLabel();
		}
		return null;
	}

	private FBFigure getCastedFigure() {
		if (getFigure() instanceof FBFigure) {
			return (FBFigure) getFigure();
		}
		return null;
	}

	/** The listener. */
	IPropertyChangeListener listener;

	/**
	 * Returns an <code>IPropertyChangeListener</code> with implemented
	 * <code>propertyChange()</code>. e.g. a color change event repaints the
	 * FunctionBlock.
	 * 
	 * @return the preference change listener
	 * 
	 * @see org.fordiac.iec61499.ui.editparts.AbstractViewEditPart#getPreferenceChangeListener()
	 */
	@Override
	public org.eclipse.jface.util.IPropertyChangeListener getPreferenceChangeListener() {
		if (listener == null) {
			listener = new org.eclipse.jface.util.IPropertyChangeListener() {
				public void propertyChange(final PropertyChangeEvent event) {
					if (event.getProperty().equals(
							PreferenceConstants.P_EVENT_CONNECTOR_COLOR)
							|| event.getProperty().equals(
									PreferenceConstants.P_DATA_CONNECTOR_COLOR)
							|| event.getProperty().equals(
									PreferenceConstants.P_ADAPTER_CONNECTOR_COLOR)){
						getFigure().repaint();
					}
				}
			};
		}
		return listener;
	}

	@Override
	protected void backgroundColorChanged(IFigure figure) {
		Color color = null;
		if (getCastedModel() != null) {
			DeviceView dev = findDeviceView();
			if (dev != null) {
				color = dev.getBackgroundColor();
			}
		}
		setColor(figure, color);
	}

	private DeviceView findDeviceView() {
		FBView mappedFBView = null;
		if (getCastedModel() != null && getCastedModel().getMappedFB() != null) {
			mappedFBView = getCastedModel().getMappedFB();
		} else if (getCastedModel() != null
				&& getCastedModel().getApplicationFB() != null) {
			mappedFBView = getCastedModel();
		}

		if (mappedFBView != null) {
			EObject uiResourceEditorObject = mappedFBView.eContainer();
			if (uiResourceEditorObject instanceof UIResourceEditor) {
				EObject uiResourceViewObject = uiResourceEditorObject
						.eContainer();
				if (uiResourceViewObject instanceof ResourceView) {
					EObject resContViewObject = uiResourceViewObject
							.eContainer();
					if (resContViewObject instanceof ResourceContainerView) {
						EObject devViewObject = resContViewObject.eContainer();
						if (devViewObject instanceof DeviceView) {
							return (DeviceView) devViewObject;
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * Returns the Properties of an FB.
	 * 
	 * @return the IPropertyDescripter[]
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#getPropertyDescriptors()
	 */
	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		List<VarDeclaration> constants = getCastedModel().getFb()
				.getInterface().getInputVars();
		IPropertyDescriptor[] descriptors = new IPropertyDescriptor[constants
				.size() * 2 + 2];
		int i = PropertyUtil.addInstanceNameDescriptor(
				getCastedModel().getFb(), descriptors, 0);
		i = PropertyUtil.addCommentDescriptor(getCastedModel().getFb(),
				descriptors, i);
		PropertyUtil.addVarDeclDescriptors(constants, descriptors, i);
		return descriptors;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#getEditableValue()
	 */
	public Object getEditableValue() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#getPropertyValue(java
	 * .lang .Object)
	 */
	public Object getPropertyValue(final Object id) {
		return PropertyUtil.getPropertyValue(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#isPropertySet(java.lang
	 * .Object)
	 */
	public boolean isPropertySet(final Object id) {
		return true;
	}

	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof InterfaceEditPart) {
			InterfaceEditPart interfaceEditPart = (InterfaceEditPart) childEditPart;
			if (interfaceEditPart.isInput()) {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventInputs().add(child);
				} else{
					if(interfaceEditPart.isAdapter()){
						getCastedFigure().getSockets().add(child);
						interfaceEditPart.setSystemPalette(getCastedModel().getFb().getPaletteEntry().getGroup().getPallete());
					}else if (interfaceEditPart.isVariable()) {
						getCastedFigure().getDataInputs().add(child);
					}
				}
			} else {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventOutputs().add(child);
				} else { 
					if(interfaceEditPart.isAdapter()){
						getCastedFigure().getPlugs().add(child);
						interfaceEditPart.setSystemPalette(getCastedModel().getFb().getPaletteEntry().getGroup().getPallete());
					}else if (interfaceEditPart.isVariable()) {
						getCastedFigure().getDataOutputs().add(child);
					}
				}

			}
		} else {
			super.addChildVisual(childEditPart, index);
		}
	}

	@Override
	protected void removeChildVisual(final EditPart childEditPart) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof InterfaceEditPart) {
			InterfaceEditPart interfaceEditPart = (InterfaceEditPart) childEditPart;
			if (interfaceEditPart.isInput()) {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventInputs().remove(child);
				} else { 
					if(interfaceEditPart.isAdapter()){
						getCastedFigure().getSockets().remove(child);
					}else if (interfaceEditPart.isVariable()) {
						getCastedFigure().getDataInputs().remove(child);
					}	
				}
			} else {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventOutputs().remove(child);
				} else { 
					if(interfaceEditPart.isAdapter()){
						getCastedFigure().getPlugs().remove(child);
					}else if (interfaceEditPart.isVariable()) {
						getCastedFigure().getDataOutputs().remove(child);
					}
				}

			}
		} else {
			super.removeChildVisual(childEditPart);
		}
	}

	@Override
	protected List<Object> getModelChildren() {
		ArrayList<Object> elements = new ArrayList<Object>();
		elements.addAll(getCastedModel().getInterfaceElements());
		return elements;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#resetPropertyValue(java
	 * .lang.Object)
	 */
	public void resetPropertyValue(final Object id) {
		// TODO - implement
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#setPropertyValue(java
	 * .lang .Object, java.lang.Object)
	 */
	public void setPropertyValue(final Object id, final Object value) {
		PropertyUtil.setPropertyValue(id, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getINamedElement()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel().getFb();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getNameLabel()
	 */
	@Override
	public Label getNameLabel() {
		return getInstanceNameLabel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getInterfaceElements()
	 */
	@Override
	public List<? extends InterfaceElementView> getInterfaceElements() {
		return getCastedModel().getInterfaceElements();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.InputValuesEditPart#getInputValuesContentPane
	 * ()
	 */
	@Override
	public Figure getInputValuesContentPane() {
		return null;
		// return getCastedFigure().getInputValuesContentPane();
	}

	/**
	 * Gets the manager.
	 * 
	 * @return the manager
	 */
	@Override
	public DirectEditManager getManager() {
		if (manager == null) {
			Label l = getNameLabel();
			manager = new LabelDirectEditManager(this, TextCellEditor.class,
					new NameCellEditorLocator(l), l,
					new IdentifierVerifyListener());
		}

		return manager;
	}

	@Override
	public void setTransparency(int value) {
		for (Object ep : getChildren()) {
			if (ep instanceof AbstractViewEditPart) {
				((AbstractViewEditPart) ep).setTransparency(value);
			}
		}
		super.setTransparency(value);
	}

	@Override
	public void performRequest(Request request) {
		if (request.getType().equals(RequestConstants.REQ_OPEN)
				&& getCastedModel() != null
				&& getCastedModel().getFb() != null
				&& getCastedModel().getFb().getFBType() instanceof CompositeFBType) {
			new OpenCompositeInstanceViewerAction(this, getCastedModel().getFb())
					.run();
		} else {
			super.performRequest(request);
		}
	}

	public ZoomManager getZoomManager() {
		return zoomManager;
	}

}
