/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.text.FlowPage;
import org.eclipse.draw2d.text.ParagraphTextLayout;
import org.eclipse.draw2d.text.TextFlow;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.INamedElement;

/**
 * The Class ConnectionTooltipFigure.
 */
public class ConnectionTooltipFigure extends Figure {

	/**
	 * Instantiates a new fB tooltip figure.
	 * 
	 * @param connection the fb view
	 */
	public ConnectionTooltipFigure(final Connection connection) {
		setLayoutManager(new GridLayout());

		String label = new String();
		
		if( null != connection.getIInterfaceElementSource()){
			if((null != connection.getIInterfaceElementSource().eContainer()) &&
					   (null != connection.getIInterfaceElementSource().eContainer().eContainer()) &&
					   (null != ((INamedElement)connection.getIInterfaceElementSource().eContainer().eContainer()).getName())){
				label = ((INamedElement)connection.getIInterfaceElementSource().eContainer().eContainer()).getName() +
					"." +				
					connection.getIInterfaceElementSource().getName() + " -> ";
			}
		}
		
		if(connection!=null && null != connection.getIInterfaceElementDestination()){
			if((null != connection.getIInterfaceElementDestination().eContainer()) &&
			   (null != connection.getIInterfaceElementDestination().eContainer().eContainer()) &&
			   (null != ((INamedElement)connection.getIInterfaceElementDestination().eContainer().eContainer()).getName())){
				label += ((INamedElement)connection.getIInterfaceElementDestination().eContainer().eContainer()).getName() +
						"." +
						connection.getIInterfaceElementDestination().getName();
			}
		}
		
		Label connNameLabel = new Label(label);
		add(connNameLabel);

		TextFlow content = new TextFlow(
				connection != null && connection.getComment() != null ? connection.getComment() : "");
		content.setLayoutManager(new ParagraphTextLayout(content,
				ParagraphTextLayout.WORD_WRAP_HARD));

		FlowPage fp = new FlowPage();
		fp.add(content);
		if (connection != null && connection.getComment() != null
				&& connection.getComment().length() > 0) {
			add(fp);
			setConstraint(fp, new GridData(PositionConstants.CENTER,
					PositionConstants.MIDDLE, false, true));
		}

		setConstraint(connNameLabel, new GridData(PositionConstants.CENTER,
				PositionConstants.MIDDLE, true, true));

	}
}
