/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editparts;

import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.fordiac.ide.application.policies.EventNodeEditPolicy;
import org.fordiac.ide.application.policies.VariableNodeEditPolicy;
import org.fordiac.ide.gef.editparts.PropertiesInterfaceEditPart;

/**
 * The Class InterfaceEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class InterfaceEditPart extends PropertiesInterfaceEditPart {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getNodeEditPolicy()
	 */
	@Override
	protected GraphicalNodeEditPolicy getNodeEditPolicy() {
		if (isEvent()) {
			return new EventNodeEditPolicy();
		}
		if (isVariable()) {
			return new VariableNodeEditPolicy();
		}
		return null;
	}

}
