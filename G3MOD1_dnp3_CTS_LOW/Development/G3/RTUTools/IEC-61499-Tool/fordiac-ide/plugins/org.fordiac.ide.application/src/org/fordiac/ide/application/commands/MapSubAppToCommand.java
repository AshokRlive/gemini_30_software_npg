/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.util.Utils;
import org.fordiac.ide.util.commands.ConnectionUtil;
import org.fordiac.ide.util.commands.UnmapSubAppCommand;

/**
 * The Class MapSubAppToCommand.
 */
public class MapSubAppToCommand extends Command {

	/** The editor. */
	private IEditorPart editor;

	/** The sub app view. */
	private final SubAppView subAppView;

	/** The ui resource editor. */
	private final UIResourceEditor uiResourceEditor;

	/** The position. */
	private Point position;

	/**
	 * Instantiates a new map sub app to command.
	 * 
	 * @param subApp the sub app
	 * @param uiResourceEditor the ui resource editor
	 */
	public MapSubAppToCommand(final SubAppView subApp,
			final UIResourceEditor uiResourceEditor) {
		this.subAppView = subApp;
		this.uiResourceEditor = uiResourceEditor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {

		if (subAppView != null && subAppView.getMappedSubApp() != null) {
			if (subAppView.getMappedSubApp().getSubApp().getResource().equals(
					uiResourceEditor.getResourceElement().getFBNetwork())) {
				ApplicationPlugin.statusLineErrorMessage(Messages.MapSubAppToCommand_STAUSMessage_AlreadyMapped);

				return false; // already mapped to this resource -> nothing to
				// do ->
				// mapping not possible!
			}
		}
		if (subAppView != null) {
			ApplicationPlugin.statusLineErrorMessage(null);
		}
		return subAppView != null && subAppView.getSubApp().getType() == null;  //only allow the mapping of untyped subapps
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		editor.equals(Utils.getCurrentActiveEditor()); // required for enabling
		// undo
		return false;
	}

	/** The orig interface elements. */
	private final Hashtable<String, SubAppInterfaceElementView> origInterfaceElements = new Hashtable<String, SubAppInterfaceElementView>();

	/** The copy interface elements. */
	private final Hashtable<String, SubAppInterfaceElementView> copyInterfaceElements = new Hashtable<String, SubAppInterfaceElementView>();

	/** The unmap cmd. */
	private UnmapSubAppCommand unmapCmd;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();

		if (subAppView.getMappedSubApp() != null) {
			if (!subAppView.getMappedSubApp().getSubApp().getResource().equals(
					uiResourceEditor.getResourceElement().getFBNetwork())) {
				// unmap before mapping to this resource

				unmapCmd = new UnmapSubAppCommand(subAppView.getMappedSubApp());
				if (unmapCmd.canExecute()) {
					unmapCmd.execute();
				}
			}
		}

		MappedSubAppView mappedSubAppView = UiFactory.eINSTANCE
				.createMappedSubAppView();
		mappedSubAppView.setApplicationSubApp(subAppView);
		mappedSubAppView.setSubApp(subAppView.getSubApp());

		for (Iterator<SubAppInterfaceElementView> iterator = subAppView
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			SubAppInterfaceElementView subAppInterfaceElement = iterator.next();
			MappedSubAppInterfaceElementView copy = UiFactory.eINSTANCE
					.createMappedSubAppInterfaceElementView();
			copy.setApplicationInterfaceElement(subAppInterfaceElement);
			copy.setIInterfaceElement(subAppInterfaceElement.getIInterfaceElement());
			mappedSubAppView.getInterfaceElements().add(copy);

		}
		Position pos = (Position) EcoreUtil.copy(subAppView.getPosition());
		mappedSubAppView.setPosition(pos);
		if (position != null) {
			mappedSubAppView.getPosition().setX(position.x);
			mappedSubAppView.getPosition().setY(position.y);
		}
		uiResourceEditor.getChildren().add(mappedSubAppView);
		subAppView.setMappedSubApp(mappedSubAppView);

		for (Iterator<SubAppInterfaceElementView> iterator = subAppView
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			SubAppInterfaceElementView interfaceElementview = iterator.next();

			origInterfaceElements.put(interfaceElementview.getLabel(),
					interfaceElementview);
		}
		for (Iterator<MappedSubAppInterfaceElementView> iterator = mappedSubAppView
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			SubAppInterfaceElementView interfaceElementview = iterator.next();

			copyInterfaceElements.put(interfaceElementview.getLabel(),
					interfaceElementview);
		}

		Enumeration<String> enumer = origInterfaceElements.keys();
		while (enumer.hasMoreElements()) {
			String key = enumer.nextElement();
			InterfaceElementView copyElem = copyInterfaceElements.get(key);

			origInterfaceElements.get(key).setMappedInterfaceElement(copyElem);

			copyElem.setFbNetwork(uiResourceEditor.getResourceElement()
					.getFBNetwork());
		}

		// TODO !!_ check whether copy contains subApp!!!
		uiResourceEditor.getResourceElement().getFBNetwork().getMappedSubApps()
				.add(mappedSubAppView.getSubApp());

		for (Iterator<SubAppInterfaceElementView> iterator = subAppView
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			SubAppInterfaceElementView interfaceElement = iterator.next();

			for (Iterator<ConnectionView> iterator2 = interfaceElement
					.getInConnections().iterator(); iterator2.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				if (connectionView.getSource().eContainer() instanceof FBView) {
					FBView sourceFBView = ((FBView) connectionView.getSource()
							.eContainer()).getMappedFB();
					if (sourceFBView != null
							&& sourceFBView.getFb().getResource().equals(
									uiResourceEditor.getResourceElement().getFBNetwork())) {
						ConnectionView newConnection = UiFactory.eINSTANCE
								.createConnectionView();
						newConnection.setConnectionElement(connectionView
								.getConnectionElement());
						newConnection.setDestination(connectionView.getDestination()
								.getMappedInterfaceElement());
						newConnection.setSource(connectionView.getSource()
								.getMappedInterfaceElement());
						uiResourceEditor.getConnections().add(newConnection);
						ConnectionUtil.addConnectionToResource(newConnection
								.getConnectionElement(), uiResourceEditor.getResourceElement());
					}
				} else if (connectionView.getSource().eContainer() instanceof SubAppView) {
					MappedSubAppView sourceSubAppView = ((SubAppView) connectionView
							.getSource().eContainer()).getMappedSubApp();
					if (sourceSubAppView != null
							&& sourceSubAppView.getSubApp().getResource().equals(
									uiResourceEditor.getResourceElement().getFBNetwork())) {
						ConnectionView newConnection = UiFactory.eINSTANCE
								.createConnectionView();
						newConnection.setConnectionElement(connectionView
								.getConnectionElement());
						newConnection.setDestination(connectionView.getDestination()
								.getMappedInterfaceElement());
						newConnection.setSource(connectionView.getSource()
								.getMappedInterfaceElement());
						uiResourceEditor.getConnections().add(newConnection);
						ConnectionUtil.addConnectionToResource(newConnection
								.getConnectionElement(), uiResourceEditor.getResourceElement());
					}

				}
			}
			for (Iterator<ConnectionView> iterator2 = interfaceElement
					.getOutConnections().iterator(); iterator2.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				if (connectionView.getDestination().eContainer() instanceof FBView) {
					FBView destFBView = ((FBView) connectionView.getDestination()
							.eContainer()).getMappedFB();
					if (destFBView != null
							&& destFBView.getFb().getResource().equals(
									uiResourceEditor.getResourceElement().getFBNetwork())) {
						ConnectionView newConnection = UiFactory.eINSTANCE
								.createConnectionView();
						newConnection.setConnectionElement(connectionView
								.getConnectionElement());

						newConnection.setDestination(connectionView.getDestination()
								.getMappedInterfaceElement());

						newConnection.setSource(connectionView.getSource()
								.getMappedInterfaceElement());
						uiResourceEditor.getConnections().add(newConnection);
						ConnectionUtil.addConnectionToResource(newConnection
								.getConnectionElement(), uiResourceEditor.getResourceElement());
					}
				} else if (connectionView.getDestination().eContainer() instanceof SubAppView) {
					MappedSubAppView sourceSubAppView = ((SubAppView) connectionView
							.getDestination().eContainer()).getMappedSubApp();
					if (sourceSubAppView != null
							&& sourceSubAppView.getSubApp().getResource().equals(
									uiResourceEditor.getResourceElement().getFBNetwork())) {
						ConnectionView newConnection = UiFactory.eINSTANCE
								.createConnectionView();
						newConnection.setConnectionElement(connectionView
								.getConnectionElement());
						newConnection.setDestination(connectionView.getDestination()
								.getMappedInterfaceElement());
						newConnection.setSource(connectionView.getSource()
								.getMappedInterfaceElement());
						uiResourceEditor.getConnections().add(newConnection);
						ConnectionUtil.addConnectionToResource(newConnection
								.getConnectionElement(), uiResourceEditor.getResourceElement());
					}
				}
			}
		}
	}

	/**
	 * Gets the position.
	 * 
	 * @return the position
	 */
	public Point getPosition() {
		return position;
	}

	/**
	 * Sets the position.
	 * 
	 * @param position the new position
	 */
	public void setPosition(final Point position) {
		this.position = position;
	}
}
