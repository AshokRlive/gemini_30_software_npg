/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ComponentEditPolicy;
import org.eclipse.gef.requests.GroupRequest;
import org.fordiac.ide.application.commands.DeleteSubInstanceAppCommand;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;

public class DeleteSubAppTypeInstanceEditPolicy extends ComponentEditPolicy {
	
	@Override
	protected Command createDeleteCommand(final GroupRequest request) {
		if (getHost() instanceof SubAppForFBNetworkEditPart) {
			DeleteSubInstanceAppCommand c = new DeleteSubInstanceAppCommand(((SubAppForFBNetworkEditPart) getHost()).getCastedModel());
			return c;
		}
		return null;
	}
}
