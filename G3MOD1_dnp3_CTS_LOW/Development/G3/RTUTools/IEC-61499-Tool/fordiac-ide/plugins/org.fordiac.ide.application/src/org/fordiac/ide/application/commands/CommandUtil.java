/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.application.editors.SubAppNetworkEditor;
import org.fordiac.ide.gef.DiagramEditorWithFlyoutPalette;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.util.editors.EditorFilter;
import org.fordiac.ide.util.editors.EditorUtils;

/**
 * The Class CommandUtil.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class CommandUtil {

	/**
	 * Close opened subApp.
	 * 
	 * @param diagram the diagram
	 * 
	 * @return the i editor input
	 */
	public static IEditorInput closeOpenedSubApp(final Diagram diagram) {
		EditorFilter filter = ((IEditorPart editor) -> {
									return (editor instanceof DiagramEditorWithFlyoutPalette) && 
											(diagram.equals(((DiagramEditorWithFlyoutPalette)editor).getDiagramModel()));
								});
		
		IEditorPart editor = EditorUtils.findEditor(filter);

		if(null != editor){
			IEditorInput input = editor.getEditorInput();			
			EditorUtils.CloseEditor.run(editor);
			return input;
		}		
		return null;
	}

	/**
	 * Open subApp editor.
	 * 
	 * @param input the input
	 */
	public static void openSubAppEditor(final IEditorInput input) {
		IWorkbenchPage activePage = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		IEditorPart part = activePage.getActiveEditor();
		try {
			activePage.openEditor(input, SubAppNetworkEditor.class.getName());
		} catch (PartInitException e) {
			// TODO log error
			ApplicationPlugin.getDefault().logError(
					Messages.CommandUtil_ERROR_ReopenSubApp, e);
		}
		activePage.activate(part);
	}

}
