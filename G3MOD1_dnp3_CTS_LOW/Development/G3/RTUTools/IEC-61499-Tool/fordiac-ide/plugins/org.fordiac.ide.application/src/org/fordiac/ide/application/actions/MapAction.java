/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.actions;

import java.util.Iterator;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.commands.MapSubAppToCommand;
import org.fordiac.ide.application.commands.MapToCommand;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIResourceEditor;

/**
 * The Class MapAction. Creates a compound command that contains several
 * "MapCommands".
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class MapAction extends SelectionAction {

	/** The resource. */
	private final UIResourceEditor resource;

	/** The map command. */
	private final CompoundCommand mapCommand = new CompoundCommand();

	/**
	 * The Constructor.
	 * 
	 * @param part the part
	 * @param resource the resource
	 */
	public MapAction(final IWorkbenchPart part, final UIResourceEditor resource) {
		super(part);
		this.resource = resource;
		setText(resource.getResourceElement().getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		update();
		for (Iterator<?> iterator = getSelectedObjects().iterator(); iterator
				.hasNext();) {
			EditPart ep = (EditPart) iterator.next();
			if (ep instanceof FBEditPart) {
				MapToCommand cmd = new MapToCommand((FBView) ep.getModel(),
						resource);
				if (cmd.canExecute()) {
					mapCommand.add(cmd);
				}
			}
			if (ep instanceof SubAppForFBNetworkEditPart) {
				MapSubAppToCommand cmd = new MapSubAppToCommand((SubAppView) ep
						.getModel(), resource);
				if (cmd.canExecute()) {
					mapCommand.add(cmd);
				}
			}
		}
		executeMapToCmds();
	}

	/**
	 * Executes the mapCommand
	 */
	private void executeMapToCmds() {
		if (getCommandStack() != null) {
			getCommandStack().execute(mapCommand);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
	 */
	@Override
	protected boolean calculateEnabled() {
		return true;
	}

}
