/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.text.MessageFormat;

import org.eclipse.emf.ecore.EObject;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.model.data.BaseType1;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.util.Utils;

/**
 * The Class LinkConstraints.
 */
public class LinkConstraints {

	/**
	 * Can create event connection.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * 
	 * @return true, if an eventconnection can be created
	 */
	public static boolean canCreateEventConnection(final Event source,
			final Event target) {
		return canExistEventConnection(source, target, false);
	}

	/**
	 * Can create event connection.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @param internalConnection
	 *            the internal connection
	 * 
	 * @return true, if an eventconnection can be created
	 */
	public static boolean canCreateEventConnection(final Event source,
			final Event target, final boolean internalConnection) {
		return canExistEventConnection(source, target, internalConnection);
	}

	/**
	 * Can create data connection.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * 
	 * @return true, if a dataconnection can be created (the source and target
	 *         type are compatible
	 */
	public static boolean canCreateDataConnection(final VarDeclaration source,
			final VarDeclaration target) {
		ApplicationPlugin.statusLineErrorMessage(null);
		return canExistDataConnection(source, target, false, true);
	}

	/**
	 * Can create data connection.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @param internalConnection
	 *            the internal connection
	 * 
	 * @return true, if a dataconnection can be created (the source and target
	 *         type are compatible
	 */
	public static boolean canCreateDataConnection(final VarDeclaration source,
			final VarDeclaration target, final boolean internalConnection, boolean checkWith) {
		ApplicationPlugin.statusLineErrorMessage(null);
		return canExistDataConnection(source, target, internalConnection, checkWith);
	}
	
	/**
	 * Can create data connection.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @param internalConnection
	 *            the internal connection
	 * 
	 * @return true, if a dataconnection can be created (the source and target
	 *         type are compatible
	 */
	public static boolean canCreateDataConnection(final VarDeclaration source,
			final VarDeclaration target, final boolean internalConnection) {

		return canCreateDataConnection(source, target, internalConnection, true);
	}
	
	/**
	 * Can reconnect data connection.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * 
	 * @return true, if a dataconnection can be reconnected (the source and target
	 *         type are compatible
	 */
	public static boolean canReconnectDataConnection(final VarDeclaration source,
			final VarDeclaration target) {
		ApplicationPlugin.statusLineErrorMessage(null);
		return canExistDataConnection(source, target, false, true, true);
	}

	/**
	 * Can reconnect data connection source.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * 
	 * @return true, if successful
	 */
	public static boolean canReconnectDataConnectionSource(
			final VarDeclaration source, final VarDeclaration target) {
		if (source != null && target == null) {// connection just started
			ApplicationPlugin.statusLineErrorMessage(null);

			return true;
		}
		if (source != null && target != null) {
			boolean canExist = true;
			canExist = sourceAndDestCheck(source, target);
			if (!canExist) {
				ApplicationPlugin.statusLineErrorMessage(Messages.LinkConstraints_STATUSMessage_IN_IN_OUT_OUT_notAllowed);
				return false;
			}

			// canExist = canExist && hasAlreadyInputConnectionsCheck(target);
			// if (!canExist) {
			// ApplicationPlugin.statusLineErrorMessage(target.getName()
			// + " has already an inputconnection!");
			// return false;
			// }

			canExist = canExist && typeCheck(source, target);
			if (!canExist) {
				ApplicationPlugin.statusLineErrorMessage(MessageFormat.format(
						Messages.LinkConstraints_STATUSMessage_NotCompatible,
						new Object[] { source.getType().getName(),
								target.getType().getName() }));
				return false;
			}

			ApplicationPlugin.statusLineErrorMessage(null);

			return canExist;
		}

		return false;
	}
	
	/**
	 * Can exist data connection.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @param internalConnection
	 *            the internal connection
	 * 
	 * @return true, if successful
	 */
	public static boolean canExistDataConnection(final VarDeclaration source,
			final VarDeclaration target, final boolean internalConnection, boolean checkWith) {
		return canExistDataConnection(source, target, internalConnection, checkWith, false);
	}
	

	/**
	 * Can exist data connection.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @param internalConnection
	 *            the internal connection
	 * @param reConnect
	 * 			  is it a reconnection or not
	 * 
	 * @return true, if successful
	 */
	public static boolean canExistDataConnection(final VarDeclaration source,
			final VarDeclaration target, final boolean internalConnection,
			boolean checkWith, boolean reConnect) {
		if (source != null && target == null) {// connection just started
			if (!isWithConstraintOK(source, checkWith)) {
				return false;
			}
			ApplicationPlugin.statusLineErrorMessage(null);

			return true;
		}
		
		if (source != null && target != null && source != target) {
			if (((source instanceof AdapterDeclaration) && (!(target instanceof AdapterDeclaration))) ||
			     (!(source instanceof AdapterDeclaration) && (target instanceof AdapterDeclaration))) {
				//don't check if we try to create a connection from data to adapter or vice versa
				return false;
			}
			
			
			boolean canExist = true;
			if (!internalConnection) {
				canExist = sourceAndDestCheck(source, target);
			}
			if (!canExist) {
				ApplicationPlugin.statusLineErrorMessage(Messages.LinkConstraints_STATUSMessage_IN_IN_OUT_OUT_notAllowed);
				return false;
			}

			canExist = canExist && hasAlreadyInputConnectionsCheck(target, source);
			if (!canExist) {
				ApplicationPlugin.statusLineErrorMessage(MessageFormat
										.format(Messages.LinkConstraints_STATUSMessage_hasAlreadyInputConnection,
												new Object[] { target.getName() }));
				return false;
			}
			if (source instanceof AdapterDeclaration) {

				canExist = canExist && (reConnect || !hasAlreadyOutputConnectionsCheck(source));
				if (!canExist) {
					ApplicationPlugin.statusLineErrorMessage(MessageFormat.format(
							Messages.LinkConstraints_STATUSMessage_hasAlreadyOutputConnection,
							new Object[] { source.getName() }));
					return false;
				}
			}

			canExist = canExist && typeCheck(source, target);
			if (!canExist) {
				ApplicationPlugin.statusLineErrorMessage(MessageFormat.format(
						Messages.LinkConstraints_STATUSMessage_NotCompatible,
						new Object[] { (null != source.getType()) ? source.getType().getName() : "N/A",
								(null != target.getType()) ? target.getType().getName() : "N/A" }));
				return false;
			}

			ApplicationPlugin.statusLineErrorMessage(null);
			
			// FIX Input event can be connected to its own output event - ID: 3029433
			if (source.isIsInput() && source.eContainer().equals(target.eContainer())) // connection from input to output within one fb 
				return false;
			
			return isWithConstraintOK(source, checkWith) && isWithConstraintOK(target, checkWith)&& canExist;

		}
		return false;
	}

	/**
	 * Elements which are not linked by a with construct are not allowed to be connected
	 * @param varDecl
	 * @return
	 */
	public static boolean isWithConstraintOK(final VarDeclaration varDecl, boolean checkWhith) {
		if (checkWhith && (!(varDecl instanceof AdapterDeclaration)) && (varDecl.getWiths().size() == 0)) { // elements which are not connect by withs are not allowed to be connected
			EObject obj = varDecl.eContainer();
			if(null != obj){
				obj = obj.eContainer();
				if((obj instanceof CompositeFBType) || (obj instanceof SubApp)){
					//data connections from and to interface data ports from composits should also be allowed from unwithed composite inputs (e.g., parameters for the FB)
					ApplicationPlugin.statusLineErrorMessage(null);
					return true;
				}
			}
			
			ApplicationPlugin.statusLineErrorMessage(MessageFormat.format(
					"{0} is not connected to an Event by a With-Construct" ,  new Object[] { varDecl.getName()}));
			return false;
		}
		ApplicationPlugin.statusLineErrorMessage(null);
		return true;
	}

	private static boolean hasAlreadyOutputConnectionsCheck(
			VarDeclaration source) {
		return source.getOutputConnections().size() > 0;
	}

	/** The compatibility. */
	private static boolean[][] compatibility = {
			{ true, false, false, false, false, false, false, false, false,
					true, false, false, false, false, false, false, false,
					false, false, false, true, false, false, true },
			{ false, true, false, false, false, false, false, false, false,
					false, false, false, false, false, false, false, false,
					false, false, false, false, false, false, true },
			{ false, false, true, true, true, false, false, true, false, true,
					true, true, true, false, false, true, false, true, true,
					false, true, true, true, true },
			{ false, false, true, true, true, false, false, true, false, true,
					true, true, true, false, false, true, false, true, true,
					false, true, true, true, true },
			{ false, false, true, true, true, false, false, true, false, true,
					true, true, true, false, false, true, false, true, true,
					false, true, true, true, true },
			{ false, false, false, false, false, true, false, false, false,
					true, false, false, false, false, false, false, false,
					false, false, false, true, false, false, true },
			{ false, false, false, false, false, false, true, false, false,
					true, false, false, false, false, false, false, false,
					false, false, false, true, false, false, true },
			{ false, false, true, true, true, false, false, true, false, true,
					true, true, true, false, false, true, false, true, true,
					false, true, true, true, true },
			{ false, false, false, false, false, false, false, false, true,
					false, false, false, false, false, false, false, false,
					false, false, false, false, false, false, true },
			{ false, false, false, false, false, false, false, false, false,
					true, false, false, false, false, false, false, false,
					false, false, false, true, false, false, true },
			{ false, false, true, true, true, false, false, true, false, true,
					true, true, true, false, false, true, false, true, true,
					false, true, true, true, true },
			{ false, false, true, true, true, false, false, true, false, true,
					true, true, true, false, false, true, false, true, true,
					false, true, true, true, true },
			{ false, false, true, true, true, false, false, true, false, true,
					true, true, true, false, false, true, false, true, true,
					false, true, true, true, true },
			{ false, false, false, false, false, false, false, false, false,
					true, false, false, false, true, false, false, false,
					false, false, false, true, false, false, true },
			{ false, false, false, false, false, false, false, false, false,
					true, false, false, false, false, true, false, false,
					false, false, false, true, false, false, true },
			{ false, false, true, true, true, false, false, true, false, true,
					true, true, true, false, false, true, false, true, true,
					false, true, true, true, true },
			{ false, false, false, false, false, false, false, false, false,
					true, false, false, false, false, false, false, true,
					false, false, false, true, false, false, true },
			{ false, false, true, true, true, false, false, true, false, true,
					true, true, true, false, false, true, false, true, true,
					false, true, true, true, true },
			{ false, false, true, true, true, false, false, true, false, true,
					true, true, true, false, false, true, false, true, true,
					false, true, true, true, true },
			{ false, false, false, false, false, false, false, false, false,
					true, false, false, false, false, false, false, false,
					false, false, true, true, false, false, true },
			{ false, false, false, false, false, false, false, false, false,
					true, false, false, false, false, false, false, false,
					false, false, false, true, false, false, true },
			{ false, false, true, true, true, false, false, true, false, true,
					true, true, true, false, false, true, false, true, true,
					false, true, true, true, true },
			{ false, false, true, true, true, false, false, true, false, true,
					true, true, true, false, false, true, false, true, true,
					false, true, true, true, true },
			{ true, true, true, true, true, true, true, true, true, true, true,
					true, true, true, true, true, true, true, true, true, true,
					true, true, false } };

	/**
	 * Type check.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * 
	 * @return true, if successful
	 */
	private static boolean typeCheck(final VarDeclaration source,
			final VarDeclaration target) {

		boolean automaticCasts = ApplicationPlugin
				.getDefault()
				.getPreferenceStore()
				.getBoolean(
						org.fordiac.ide.application.preferences.PreferenceConstants.P_BOOLEAN);

		if (automaticCasts
				&& !(source instanceof AdapterDeclaration || target instanceof AdapterDeclaration)) {
			BaseType1 sourceType = BaseType1.getByName(source.getTypeName()
					.toUpperCase());
			BaseType1 destType = BaseType1.getByName(target.getTypeName()
					.toUpperCase());

			if (sourceType != null && destType != null) {
				return compatibility[sourceType.getValue()][destType.getValue()];
			} else {
				return false;
			}
		} else {
			return Utils.defaultTypeCompatibilityCheck(source, target);
		}

	}

	/**
	 * Checks for already input connections check.
	 * 
	 * @param target
	 *            the target
	 * 
	 * @return true, if successful
	 */
	private static boolean hasAlreadyInputConnectionsCheck(
			final VarDeclaration target, final VarDeclaration src) {
		boolean hasOnlyBrokenCons = true;
		
		if(!target.getInputConnections().isEmpty()){
			FBNetwork fbNetworkTarget = getContainingFBNetwork(target);
			FBNetwork fbNetworkSrc = getContainingFBNetwork(src);
			if((null != fbNetworkTarget) && (fbNetworkTarget != fbNetworkSrc)){
				//Broken connection check must only be performed of src and target are in different networks allowing to connect broken 
				//connections in the same container would lead to broken fb networks, see [issues:#82] for details
				for (DataConnection connection : target.getInputConnections()) {
					if (!connection.isBrokenConnection()){
						hasOnlyBrokenCons = false;
						break;
					}
				}
			}
			else{
				hasOnlyBrokenCons = false;
			}
		}
		return hasOnlyBrokenCons;
		// return target.getInputConnections().isEmpty();
	}

	private static FBNetwork getContainingFBNetwork(VarDeclaration target) {
		FBNetwork retVal = null;
		EObject obj = target.eContainer();
		if(null != obj){
			obj = obj.eContainer();
			if(null != obj){
				obj = obj.eContainer();
				if(obj instanceof FBNetwork){
					retVal = (FBNetwork)obj; 				
				}
			}
		}
		return retVal;
	}

	/**
	 * Source and dest check.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * 
	 * @return true, if successful
	 */
	private static boolean sourceAndDestCheck(final VarDeclaration source,
			final VarDeclaration target) {
		boolean canExist = (source.isIsInput() && !target.isIsInput())
				|| (!source.isIsInput() && target.isIsInput());
		
		if(!canExist){
			if(source.isIsInput() && target.isIsInput()){
				canExist = (source.eContainer().eContainer() instanceof CompositeFBType);
			}
			else if((!source.isIsInput()) && !target.isIsInput()){
				canExist = (target.eContainer().eContainer() instanceof CompositeFBType);
			}
		}
		return canExist;
	}

	/**
	 * Can exist event connection.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @param internalConnection
	 *            the internal connection
	 * 
	 * @return true, if successful
	 */
	public static boolean canExistEventConnection(final Event source,
			final Event target, final boolean internalConnection) {
		if (source != null && target == null) {
			return true;
		}
		if (internalConnection) {
			if (source != null && target != null && !source.equals(target)) {
				return true;
			}
		} else {
			if (source != null && target != null) {
				boolean canExist = true;
				canExist = canExist
						&& ((source.isIsInput() && !target.isIsInput()) || (!source
								.isIsInput() && target.isIsInput()));
				// FIX Input event can be connected to its own output event - ID: 3029433
				if (source.isIsInput() && source.eContainer().equals(target.eContainer())) // connection from input to output within one fb 
					return false;
				
				if(!canExist){
					if(source.isIsInput() && target.isIsInput()){
						canExist = (source.eContainer().eContainer() instanceof CompositeFBType);
					}
					else if((!source.isIsInput()) && !target.isIsInput()){
						canExist = (target.eContainer().eContainer() instanceof CompositeFBType);
					}
				}
				return canExist;
			}
		}
		return false;
	}
}

