/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.application.commands.ConnectionMoveCMD;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.commands.DeleteConnectionCommand;
import org.fordiac.ide.util.commands.UnmapCommand;
import org.fordiac.ide.util.commands.UnmapSubAppCommand;

/**
 * Adds selected FBs and subapps to an already existing subapp
 * 
 * @author Filip Andr�n (fandr)
 */
public class AddFBToSubAppCommand extends Command {

	/** The parent network. */
	private Diagram parentNetwork;

	/** The unmap cmds. */
	private final List<Command> unmapCmds = new ArrayList<Command>();

	/** The fbs. */
	private List<FBView> fbs;

	/** The sub apps. */
	private List<SubAppView> subApps;
	
	/** All views (fbs and subapps) */
	private final List<View> allViews = new ArrayList<View>();

	/** The sub app view. */
	private SubAppView subAppView;

	/** The ui sub app network. */
	private UISubAppNetwork uiSubAppNetwork;

	/** The sub app network. */
	private SubAppNetwork subAppNetwork;

	/** The connection moved cm ds. */
	private final ArrayList<ConnectionMoveCMD> connectionMovedCMDs = new ArrayList<ConnectionMoveCMD>();
	
	/** Lists to handle connections and interfaces */
	private final List<DataConnectionCreateCommand> dataConnsInSubAppCmds = new ArrayList<DataConnectionCreateCommand>();
	private final List<EventConnectionCreateCommand> eventConnsInSubAppCmds = new ArrayList<EventConnectionCreateCommand>();
	private final List<DeleteConnectionCommand> deleteConnsCmds = new ArrayList<DeleteConnectionCommand>();
	private final List<SubAppInterfaceElementView> usedSAInterfaces = new ArrayList<SubAppInterfaceElementView>();

	/** The new subapp interface elements */
	private List<AddNewInterfaceElementCommand> addSubAppInterfaceCmds = new ArrayList<AddNewInterfaceElementCommand>();
	
	
	public AddFBToSubAppCommand(SubAppView subAppView, List<FBView> fbs, List<SubAppView> subApps) {
		this.subAppView = subAppView;
		this.fbs = fbs;
		this.subApps = subApps;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		
		allViews.addAll(fbs);
		allViews.addAll(subApps);

		// unmap all fbs before creating the subApp
		for (FBView fbView : fbs) {
			if (fbView != null) {
				if (fbView.getMappedFB() != null) {
					UnmapCommand cmd = new UnmapCommand(fbView.getMappedFB());
					unmapCmds.add(cmd);
				}
			}
		}
		for (SubAppView subAppView : subApps) {
			if (subAppView != null) {
				if (subAppView.getMappedSubApp() != null) {
					UnmapSubAppCommand cmd = new UnmapSubAppCommand(subAppView
							.getMappedSubApp());
					unmapCmds.add(cmd);
				}
			}
		}

		executeUnmapCmds();

		uiSubAppNetwork = subAppView.getUiSubAppNetwork();
		SubApp subApp = subAppView.getSubApp();
		subAppNetwork = subApp.getSubAppNetwork();
		parentNetwork = (Diagram)subAppView.eContainer();
		
		createInterface(subAppView, allViews);

		// find connections to and from the subapp
		findConnsToAndFromSubApp(subAppView, allViews);
		
		for (DeleteConnectionCommand cmd : deleteConnsCmds) {
			if (cmd.canExecute()) {
				cmd.execute();
			}
		}

		// move fbs and fbview to their new containers
		moveFBsToNewContainer(uiSubAppNetwork, subAppNetwork);

		// move subApps and subAppVies to their new containers
		moveSubAppsToNewContainer(uiSubAppNetwork, subAppNetwork);

		// recreate connection between FBs in the subapp
		for (EventConnectionCreateCommand cmd : eventConnsInSubAppCmds) {
			if (cmd.canExecute()) {
				cmd.execute();
			}
		}
		for (DataConnectionCreateCommand cmd : dataConnsInSubAppCmds) {
			if (cmd.canExecute()) {
				cmd.execute();
			}
		}

		// move connections between "component fbs" to subApp
		moveConnections();
		
	}
	
	private List<? extends InterfaceElementView> getInterfaceElements(View fb) {
		List<? extends InterfaceElementView> ievs = new ArrayList<InterfaceElementView>();
		if (fb instanceof FBView) {
			ievs = ((FBView) fb).getInterfaceElements();
		} else if (fb instanceof SubAppView) {
			ievs = ((SubAppView) fb).getInterfaceElements();
		}
		return ievs;
	}
	
	private List<ConnectionView> getInConnections(View fb) {
		List<ConnectionView> retList = new ArrayList<ConnectionView>();
		for (InterfaceElementView iev : getInterfaceElements(fb)) {
			retList.addAll(iev.getInConnections());
		}		
		return retList;
	}
	
	private List<ConnectionView> getOutConnections(View fb) {
		List<ConnectionView> retList = new ArrayList<ConnectionView>();
		for (InterfaceElementView iev : getInterfaceElements(fb)) {
			retList.addAll(iev.getOutConnections());
		}		
		return retList;
	}
	
	private void findConnsToAndFromSubApp(SubAppView subAppView, List<? extends View> fbs) {
		for (View fb : fbs) {
			for (ConnectionView connView : getInConnections(fb)) {
				InterfaceElementView src = null;
				InterfaceElementView dest = null;
				Diagram parent = null;
				if (connView.getSource().eContainer().equals(subAppView)) {		
					// Connection is to subAppView -> delete and reconnect
					for (ConnectionView internalConn : connView.getInternalSourceInterfaceElementView().getInConnections()) {
						if (internalConn.getSource().getIInterfaceElement().equals(connView.getSource().getIInterfaceElement())) {
							src = internalConn.getSource();
							dest = connView.getDestination();
							parent = uiSubAppNetwork;
						}
					}
					usedSAInterfaces.add((SubAppInterfaceElementView) connView.getSource());
				} else if (!allViews.contains(connView.getSource().eContainer())) { 
					// Conn is to an fb outside subAppView -> delete and reconnect it
					src = connView.getSource();
					for (InterfaceElementView iev : getInterfaceElements(subAppView)) {
						if (iev.getIInterfaceElement().equals(connView.getDestination().getIInterfaceElement())) {
							dest = iev;
						}
					}
					parent = parentNetwork;
				}
				if (dest != null && src != null) {
					if (connView.getSource().getIInterfaceElement() instanceof Event) {
						EventConnectionCreateCommand cmd = new EventConnectionCreateCommand();
						cmd.setSourceView(src);
						cmd.setDestinationView(dest);
						cmd.setParent(parent);
						eventConnsInSubAppCmds.add(cmd);
					} else if (connView.getSource().getIInterfaceElement() instanceof VarDeclaration) {
						DataConnectionCreateCommand cmd = new DataConnectionCreateCommand();
						cmd.setSourceView(src);
						cmd.setDestinationView(dest);
						cmd.setParent(parent);
						dataConnsInSubAppCmds.add(cmd);							
					}
					deleteConnsCmds.add(new DeleteConnectionCommand(connView));
				}
			}
			for (ConnectionView connView : getOutConnections(fb)) {
				InterfaceElementView src = null;
				InterfaceElementView dest = null;
				Diagram parent = null;
				// find conns to subapp
				if (connView.getDestination().eContainer().equals(subAppView)) {		
					for (ConnectionView internalConn : connView.getInternalDestinationInterfaceElementView().getOutConnections()) {
						if (internalConn.getDestination().getIInterfaceElement().equals(connView.getDestination().getIInterfaceElement())) {
							src = connView.getSource();
							dest = internalConn.getDestination();
							parent = uiSubAppNetwork;
						}
					}
					usedSAInterfaces.add((SubAppInterfaceElementView) connView.getDestination());
				} else if (!allViews.contains(connView.getDestination().eContainer())) { 
					// Conn is to an fb outside subAppView -> reconnect it
					for (InterfaceElementView iev : getInterfaceElements(subAppView)) {
						if (iev.getIInterfaceElement().equals(connView.getSource().getIInterfaceElement())) {
							src = iev;
						}
					}
					dest = connView.getDestination();
					parent = parentNetwork;
				}
				if (dest != null && src != null) {
					if (connView.getSource().getIInterfaceElement() instanceof Event) {
						EventConnectionCreateCommand cmd = new EventConnectionCreateCommand();
						cmd.setSourceView(src);
						cmd.setDestinationView(dest);
						cmd.setParent(parent);
						eventConnsInSubAppCmds.add(cmd);
					} else if (connView.getSource().getIInterfaceElement() instanceof VarDeclaration) {
						DataConnectionCreateCommand cmd = new DataConnectionCreateCommand();
						cmd.setSourceView(src);
						cmd.setDestinationView(dest);
						cmd.setParent(parent);
						dataConnsInSubAppCmds.add(cmd);		
					}
					deleteConnsCmds.add(new DeleteConnectionCommand(connView));
				}
			}
		}
	}

	/**
	 * Move connections.
	 */
	private void moveConnections() {
		for (FB fb : subAppNetwork.getFBs()) {
			InterfaceList iList = fb.getInterface();
			if (iList != null) {
				for (Event event : iList.getEventOutputs()) {
					for (EventConnection eventCon : event.getOutputConnections()) {
						ConnectionMoveCMD cmd = new ConnectionMoveCMD(eventCon, subAppNetwork);
						connectionMovedCMDs.add(cmd);
					}
				}
				for (VarDeclaration var : iList.getOutputVars()) {
					for (DataConnection dataCon : var.getOutputConnections()) {
						ConnectionMoveCMD cmd = new ConnectionMoveCMD(dataCon, subAppNetwork);
						connectionMovedCMDs.add(cmd);
					}
				}
			}
		}
		executeConnectionMoveCMDs();
	}

	/**
	 * Execute connection move cm ds.
	 */
	private void executeConnectionMoveCMDs() {
		for (ConnectionMoveCMD cmd : connectionMovedCMDs) {
			if (cmd.canExecute()) {
				cmd.execute();
			}
		}
	}

	/**
	 * Undo connection move cm ds.
	 */
	private void undoConnectionMoveCMDs() {
		for (ConnectionMoveCMD cmd : connectionMovedCMDs) {
			if (cmd.canUndo()) {
				cmd.undo();
			}
		}
	}

	/**
	 * Redo connection move cm ds.
	 */
	private void redoConnectionMoveCMDs() {
		for (ConnectionMoveCMD cmd : connectionMovedCMDs) {
			if (cmd.canUndo()) {
				cmd.redo();
			}
		}
	}

	/**
	 * Execute unmap cmds.
	 */
	private void executeUnmapCmds() {
		for (Command cmd : unmapCmds) {
			if (cmd.canExecute()) {
				cmd.execute();
			}
		}
	}

	/**
	 * Move sub apps to new container.
	 * 
	 * @param uiSubAppNetwork
	 *          the ui sub app network
	 * @param subAppNetwork
	 *          the sub app network
	 */
	private void moveSubAppsToNewContainer(final UISubAppNetwork uiSubAppNetwork,
			final SubAppNetwork subAppNetwork) {
		for (SubAppView subAppView : subApps) {
			uiSubAppNetwork.getChildren().add(subAppView);
			subAppNetwork.getSubApps().add(subAppView.getSubApp());
		}
	}

	/**
	 * Move f bs to new container.
	 * 
	 * @param uiSubAppNetwork
	 *          the ui sub app network
	 * @param subAppNetwork
	 *          the sub app network
	 */
	private void moveFBsToNewContainer(final UISubAppNetwork uiSubAppNetwork,
			final SubAppNetwork subAppNetwork) {
		for (FBView fbView : fbs) {
			uiSubAppNetwork.getChildren().add(fbView);
			subAppNetwork.getFBs().add(fbView.getFb());
		}
	}

	/**
	 * Creates the interface.
	 * 
	 * @param subAppView
	 *          the sub app view
	 * @param list
	 *          the list
	 */
	private void createInterface(final SubAppView subAppView,
			final List<? extends View> list) {
		for (View view : list) {
			for (InterfaceElementView interfaceElementview : getInterfaceElements(view)) {

				AddNewInterfaceElementCommand cmd = new AddNewInterfaceElementCommand(subAppView, interfaceElementview.getIInterfaceElement());
				addSubAppInterfaceCmds.add(cmd);
				cmd.execute();
				
				// Set interface visibility
				SubAppInterfaceElementView subAppInterfaceElementView = cmd.getSubAppInterfaceElementView();
				if (subAppInterfaceElementView.getIInterfaceElement() instanceof Event || 
						!subAppInterfaceElementView.getIInterfaceElement().isIsInput()) {
					subAppInterfaceElementView.setVisible(true);
				} else if (subAppInterfaceElementView.getIInterfaceElement() instanceof VarDeclaration) {
					if (interfaceElementview.getInConnections().size() > 0) {
						if (anySourceIsOutsideSubApp(interfaceElementview.getInConnections())) {
							subAppInterfaceElementView.setVisible(true);
						} else {
							subAppInterfaceElementView.setVisible(false);
						}
					} else {
						subAppInterfaceElementView.setVisible(true);
					}
				}
			}
		}
	}

	/**
	 * Any source is outside subAppView.
	 * 
	 * @param inconnections
	 *          the inconnections
	 * 
	 * @return true, if successful
	 */
	private boolean anySourceIsOutsideSubApp(
			final List<ConnectionView> inconnections) {
		for (ConnectionView connectionView : inconnections) {
			if (!allViews.contains(connectionView.getSource().eContainer()) &&
					!uiSubAppNetwork.getChildren().contains(connectionView.getSource().eContainer())) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		// undo recreation of connections inside subapp
		for (EventConnectionCreateCommand cmd : eventConnsInSubAppCmds) {
			cmd.undo();
		}
		for (DataConnectionCreateCommand cmd : dataConnsInSubAppCmds) {
			cmd.undo();
		}
		
		// move fbs and fbview to their original container
		for (FBView fbView : fbs) {
			parentNetwork.getChildren().add(fbView);
			parentNetwork.getNetwork().getFBs().add(
					fbView.getFb());
		}

		// move subApps and subAppViews to their original container
		for (SubAppView subAppView : subApps) {
			parentNetwork.getChildren().add(subAppView);
			parentNetwork.getNetwork().getSubApps().add(subAppView.getSubApp());
		}

		undoConnectionMoveCMDs();
		
		for (AddNewInterfaceElementCommand cmd : addSubAppInterfaceCmds) {
			cmd.undo();
		}
		
		// show all previously used subapp interface views
		for (SubAppInterfaceElementView saInterface : usedSAInterfaces) {
			saInterface.setVisible(true);
		}
		
		// undo delete connection commands
		for (DeleteConnectionCommand cmd : deleteConnsCmds) {
			cmd.undo();
		}

		undoExecuteUnmap();
	}

	/**
	 * Undo execute unmap.
	 */
	private void undoExecuteUnmap() {
		for (Command cmd : unmapCmds) {
			if (cmd.canUndo()) {
				cmd.undo();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		
		redoExecuteUnmap();

		// delete connections to subapp
		for (DeleteConnectionCommand cmd : deleteConnsCmds) {
			cmd.redo();
		}
		
		// move fbs and fbview to their new containers
		moveFBsToNewContainer(uiSubAppNetwork, subAppNetwork);

		// move subApps and subAppVies to their new containers
		moveSubAppsToNewContainer(uiSubAppNetwork, subAppNetwork);

		for (AddNewInterfaceElementCommand cmd : addSubAppInterfaceCmds) {
			cmd.redo();
		}

		redoConnectionMoveCMDs();
		
		// recreate connection between FBs in the subapp
		for (EventConnectionCreateCommand cmd : eventConnsInSubAppCmds) {
			cmd.redo();
		}
		for (DataConnectionCreateCommand cmd : dataConnsInSubAppCmds) {
			cmd.redo();
		}

	}

	/**
	 * Redo execute unmap.
	 */
	private void redoExecuteUnmap() {
		for (Command cmd : unmapCmds) {
			if (cmd.canExecute()) {
				cmd.redo();
			}
		}
	}

}
