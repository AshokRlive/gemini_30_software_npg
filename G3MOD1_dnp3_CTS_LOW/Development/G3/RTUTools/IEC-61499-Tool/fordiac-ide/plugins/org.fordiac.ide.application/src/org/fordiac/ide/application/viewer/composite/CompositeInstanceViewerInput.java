package org.fordiac.ide.application.viewer.composite;

import org.eclipse.ui.IPersistableElement;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.util.UntypedEditorInput;

public class CompositeInstanceViewerInput extends UntypedEditorInput {

	private FBEditPart fbEditPart;

	public FBEditPart getFbEditPart() {
		return fbEditPart;
	}

	public void setFbEditPart(FBEditPart fbEditPart) {
		this.fbEditPart = fbEditPart;
	}

	public CompositeInstanceViewerInput(FBEditPart fbEditPart, Object content,
			String name, String toolTip) {
		super(content, name, toolTip);
		this.fbEditPart = fbEditPart;
	}

	
	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public IPersistableElement getPersistable() {
		return null;
	}

}
