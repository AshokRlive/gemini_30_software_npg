package org.fordiac.ide.application.properties;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.gef.properties.AbstractInterfaceSection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.ui.FBView;

public class InterfaceSection extends AbstractInterfaceSection {

	@Override
	protected CommandStack getCommandStack(IWorkbenchPart part, Object input) {
		if(part instanceof FBNetworkEditor){
			return ((FBNetworkEditor)part).getFBEditorCommandStack();
		}
		return null;
	}

	@Override
	protected FB getInputType(Object input) {
		if(input instanceof FBEditPart){
			return ((FBView)((FBEditPart) input).getModel()).getFb();
		}
		return null;
	}

}
