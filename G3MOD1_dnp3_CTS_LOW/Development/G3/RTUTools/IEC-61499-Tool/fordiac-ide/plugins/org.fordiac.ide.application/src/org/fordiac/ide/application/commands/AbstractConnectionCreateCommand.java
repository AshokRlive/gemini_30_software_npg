/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.commands.InternalConnection;

public abstract class AbstractConnectionCreateCommand extends Command {

	private int connDx1;
	
	private int connDx2;
	
	private int connDy;
	
	/** The editor. */
	private IEditorPart editor;

	protected IEditorPart getEditor() {
		return editor;
	}

	/** The parent. */
	protected Diagram parent;

	/** The connection view. */
	protected ConnectionView connectionView;
	
	/** The source view. */
	protected InterfaceElementView sourceView;

	/** The destination view. */
	protected InterfaceElementView destinationView;
	
	protected boolean internalConnection = false;
	
	/** The internal connections. */
	protected ArrayList<InternalConnection> internalConnections = new ArrayList<InternalConnection>();

	
	public AbstractConnectionCreateCommand(){
		super();
		//initialize values
		this.connDx1 = 0;
		this.connDx2 = 0;
		this.connDy = 0;
	}
	
	public void setArrangementConstraints(int dx1, int dx2, int dy){
		
		this.connDx1 = dx1;
		this.connDx2 = dx2;
		this.connDy = dy;
	}
	
	public int getDx1() {
		return connDx1;
	}
	
	public int getDx2() {
		return connDx2;
	}
	
	public int getDy() {
		return connDy;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return getEditor().equals(ApplicationPlugin.getDefault()
				.getCurrentActiveEditor());
	}
	
	/**
	 * Sets the source.
	 * 
	 * @param source the new source
	 */
	public void setSource(final InterfaceEditPart source) {
		this.sourceView = source.getCastedModel();
	}
	
	public void setSourceView(final InterfaceElementView sourceView) {
		this.sourceView = sourceView;
	}

	public InterfaceElementView getSourceView() {
		return sourceView;
	}
	
	/**
	 * Sets the target.
	 * 
	 * @param target the new target
	 */
	public void setTarget(final InterfaceEditPart target) {
		this.destinationView = target.getCastedModel();
	}
	
	public void setDestinationView(final InterfaceElementView destinationView) {
		this.destinationView = destinationView;
	}

	protected void setEditor(IEditorPart editor) {
		this.editor = editor;
	}
	
	/**
	 * Find outer source.
	 */
	protected void findOuterSource() {
		boolean sourceFound = false;
		boolean destinationFound = false;
		Diagram diagram = null;
		InternalSubAppInterfaceElementView internalInterface = (InternalSubAppInterfaceElementView) sourceView;
		SubAppView  saView = (SubAppView) ((UISubAppNetwork)parent).getSubAppView();
		
		while (!sourceFound || !destinationFound) {
			diagram = (Diagram) saView.eContainer();
			if (diagram != null) {
				for (Iterator<View> iterator = diagram.getChildren().iterator(); iterator
						.hasNext();) {
					View view = iterator.next();
					if (view instanceof FBView) {
						for (Iterator<InterfaceElementView> iterator2 = ((FBView) view)
								.getInterfaceElements().iterator(); iterator2
								.hasNext();) {
							InterfaceElementView interfaceElementView = iterator2
									.next();
							if (interfaceElementView.getIInterfaceElement().equals(
									internalInterface.getIInterfaceElement())) {
								sourceView = interfaceElementView;
								sourceFound = true;
								break;
							}
						}
					} else if (view instanceof SubAppView) {
						SubAppView subAppView = (SubAppView) view;
						for (Iterator<SubAppInterfaceElementView> iterator2 = subAppView
								.getInterfaceElements().iterator(); iterator2
								.hasNext();) {
							SubAppInterfaceElementView subAppInterfaceElementView = iterator2
									.next();
							if (subAppInterfaceElementView.getIInterfaceElement()
									.equals(sourceView.getIInterfaceElement())) {
								sourceView = subAppInterfaceElementView;
								sourceFound = true;
								break;
							} else if (subAppInterfaceElementView.getIInterfaceElement()
									.equals(destinationView.getIInterfaceElement())) {
								// need to show the element!
								subAppInterfaceElementView.setVisible(true); 
								destinationView = subAppInterfaceElementView;
								destinationFound = true;
								break;
							}
	
						}
					}
				}
				if (!sourceFound) {
					if (diagram instanceof UISubAppNetwork) {
						saView = ((UISubAppNetwork)diagram).getSubAppView();
					}
				}
			}
		}
	}

	/**
	 * Find outer dest.
	 */
	protected void findOuterDest() {	
		boolean sourceFound = false;
		boolean destinationFound = false;
		Diagram diagram = null;
		InternalSubAppInterfaceElementView internalInterface = (InternalSubAppInterfaceElementView) destinationView;
		SubAppView  saView = (SubAppView) ((UISubAppNetwork)parent).getSubAppView();
		
		while (!sourceFound || !destinationFound) {
			diagram = (Diagram) saView.eContainer();
			if (diagram != null) {
				for (Iterator<View> iterator = diagram.getChildren().iterator(); iterator
						.hasNext();) {
					View view = iterator.next();
					if (view instanceof FBView) {
						for (Iterator<InterfaceElementView> iterator2 = ((FBView) view)
								.getInterfaceElements().iterator(); iterator2
								.hasNext();) {
							InterfaceElementView interfaceElementView = iterator2
									.next();
							if (interfaceElementView.getIInterfaceElement()
									.equals(internalInterface
											.getIInterfaceElement())) {
								destinationView = interfaceElementView;
								destinationFound = true;
								break;
							}
						}
					} else if (view instanceof SubAppView) {
						SubAppView subAppView = (SubAppView) view;
						for (Iterator<SubAppInterfaceElementView> iterator2 = subAppView
								.getInterfaceElements().iterator(); iterator2
								.hasNext();) {
							SubAppInterfaceElementView subAppInterfaceElementView = iterator2
									.next();
							if (subAppInterfaceElementView
									.getIInterfaceElement().equals(
											destinationView
													.getIInterfaceElement())) {
								destinationView = subAppInterfaceElementView;
								destinationFound = true;
								break;
							} else if (subAppInterfaceElementView
									.getIInterfaceElement().equals(
											sourceView.getIInterfaceElement())) {
								// need to show the element!
								subAppInterfaceElementView.setVisible(true); 
								sourceView = subAppInterfaceElementView;
								sourceFound = true;
								break;
							}

						}
					}
				}
				if (!destinationFound) {
					if (diagram instanceof UISubAppNetwork) {
						saView = ((UISubAppNetwork)diagram).getSubAppView();
					}
				}
			}
		}
	}
	
	/**
	 * Gets the parent.
	 * 
	 * @return the parent
	 */
	public Diagram getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 * 
	 * @param parent
	 *            the new parent
	 */
	public void setParent(final Diagram parent) {
		this.parent = parent;
	}

	/**
	 * Checks if is internal connection.
	 * 
	 * @return true, if is internal connection
	 */
	public boolean isInternalConnection() {
		return internalConnection;
	}

	/**
	 * Sets the internal connection.
	 * 
	 * @param internalConnection
	 *            the new internal connection
	 */
	public void setInternalConnection(final boolean internalConnection) {
		this.internalConnection = internalConnection;
	}

}
