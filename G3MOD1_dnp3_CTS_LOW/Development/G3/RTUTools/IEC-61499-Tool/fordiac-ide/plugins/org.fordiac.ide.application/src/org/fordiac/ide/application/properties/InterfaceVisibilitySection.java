/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.properties;

import java.util.Iterator;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * The Class InterfaceVisibilitySection.
 */
public class InterfaceVisibilitySection extends AbstractPropertySection {

	/** The sub app edit part. */
	private SubAppForFBNetworkEditPart subAppEditPart;

	/** The inputs table. */
	private Table inputsTable;

	/** The outputs table. */
	private Table outputsTable;
	
	private TableEditor inputsEditor;
	private TableEditor outputsEditor;

	/** The econtent adapter. */
	private final EContentAdapter econtentAdapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			Object feature = notification.getFeature();
			if (UiPackage.eINSTANCE.getInterfaceElementView_InConnections()
					.equals(feature)
					|| UiPackage.eINSTANCE
							.getInterfaceElementView_OutConnections().equals(
									feature)) {
				fillTable();
			}
			if (UiPackage.eINSTANCE.getSubAppView_InterfaceElements().equals(
					feature)
					|| UiPackage.eINSTANCE.getInterfaceElementView_Label()
							.equals(feature)) {
				fillTable();
			}
		}

	};

	/**
	 * Instantiates a new interface visibility section.
	 */
	public InterfaceVisibilitySection() {
		// empty constructor
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#createControls(org.eclipse.swt.widgets.Composite,
	 *      org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage)
	 */
	@Override
	public void createControls(final Composite parent,
			final TabbedPropertySheetPage aTabbedPropertySheetPage) {
		super.createControls(parent, aTabbedPropertySheetPage);

		Composite composite = getWidgetFactory().createComposite(parent);
		composite.setLayout(new GridLayout(2, true));

		GridData inputsTableData = new GridData(SWT.FILL, SWT.FILL, true, true);
		inputsTableData.heightHint = 100;

		Section inputs = getWidgetFactory().createSection(composite, SWT.NONE);
		inputs.setText(Messages.InterfaceVisibilitySection_LABEL_SubAppInputsHeadline);

		Section outputs = getWidgetFactory().createSection(composite, SWT.NONE);
		outputs.setText(Messages.InterfaceVisibilitySection_LABEL_SubAppOutputsHeadline);

		inputsTable = getWidgetFactory().createTable(composite,
				SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		inputsTable.setLayoutData(inputsTableData);
		
		inputsEditor = new TableEditor(inputsTable);
		//The editor must have the same size as the cell and must
		//not be any smaller than 50 pixels.
		inputsEditor.horizontalAlignment = SWT.LEFT;
		inputsEditor.grabHorizontal = true;
		inputsEditor.minimumWidth = 50;

		GridData outputsTableData = new GridData(SWT.FILL, SWT.FILL, true, true);
		outputsTableData.heightHint = 100;

		outputsTable = getWidgetFactory().createTable(composite,
				SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		outputsTable.setLayoutData(outputsTableData);
		
		outputsEditor = new TableEditor(inputsTable);
		//The editor must have the same size as the cell and must
		//not be any smaller than 50 pixels.
		outputsEditor.horizontalAlignment = SWT.LEFT;
		outputsEditor.grabHorizontal = true;
		outputsEditor.minimumWidth = 50;

		// Create check all/none buttons
		RowLayout rowLayout = new RowLayout();
		rowLayout.marginBottom = 0;
		rowLayout.marginTop = 0;
		rowLayout.center = true;
		Composite compositeInputsButtons = getWidgetFactory().createComposite(composite);
		compositeInputsButtons.setLayout(rowLayout);
		Composite compositeOutputButtons = getWidgetFactory().createComposite(composite);
		compositeOutputButtons.setLayout(rowLayout);
		
		getWidgetFactory().createLabel(compositeInputsButtons, "Check: ");
		Button btnAllInputs = getWidgetFactory().createButton(compositeInputsButtons, "All", SWT.NONE);
		Button btnNoneInputs = getWidgetFactory().createButton(compositeInputsButtons, "None", SWT.NONE);
		btnAllInputs.setLayoutData(new RowData(btnNoneInputs.computeSize(SWT.DEFAULT, SWT.DEFAULT).x,20));
		btnNoneInputs.setLayoutData(new RowData(btnNoneInputs.computeSize(SWT.DEFAULT, SWT.DEFAULT).x,20));
		
		btnAllInputs.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				TableItem[] tableItems = inputsTable.getItems();
				for (TableItem item : tableItems) {
					if (!item.getGrayed()) {
						if (!item.getChecked()) {
							SubAppInterfaceElementView interfaceElementView = (SubAppInterfaceElementView) item
									.getData();
							item.setChecked(true);
							interfaceElementView.setVisible(true);
						}
					}
				}
			}
		});
		btnNoneInputs.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				TableItem[] tableItems = inputsTable.getItems();
				for (TableItem item : tableItems) {
					if (!item.getGrayed()) {
						if (item.getChecked()) {
							SubAppInterfaceElementView interfaceElementView = (SubAppInterfaceElementView) item
									.getData();
							item.setChecked(false);
							interfaceElementView.setVisible(false);
						}
					}
				}
			}
		});

		getWidgetFactory().createLabel(compositeOutputButtons, "Check: ");
		Button btnAllOutputs = getWidgetFactory().createButton(compositeOutputButtons, "All", SWT.NONE);
		Button btnNoneOutputs = getWidgetFactory().createButton(compositeOutputButtons, "None", SWT.NONE);
		btnAllOutputs.setLayoutData(new RowData(btnNoneOutputs.computeSize(SWT.DEFAULT, SWT.DEFAULT).x,20));
		btnNoneOutputs.setLayoutData(new RowData(btnNoneOutputs.computeSize(SWT.DEFAULT, SWT.DEFAULT).x,20));

		btnAllOutputs.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				TableItem[] tableItems = outputsTable.getItems();
				for (TableItem item : tableItems) {
					if (!item.getGrayed()) {
						if (!item.getChecked()) {
							SubAppInterfaceElementView interfaceElementView = (SubAppInterfaceElementView) item
									.getData();
							item.setChecked(true);
							interfaceElementView.setVisible(true);
						}
					}
				}
			}
		});
		btnNoneOutputs.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				TableItem[] tableItems = outputsTable.getItems();
				for (TableItem item : tableItems) {
					if (!item.getGrayed()) {
						if (item.getChecked()) {
							SubAppInterfaceElementView interfaceElementView = (SubAppInterfaceElementView) item
									.getData();
							item.setChecked(false);
							interfaceElementView.setVisible(false);
						}
					}
				}
			}
		});
	}

	/**
	 * Fill table.
	 */
	private void fillTable() {
		try {
			if (!outputsTable.isDisposed()) {
				outputsTable.removeAll();
			}

			if (!inputsTable.isDisposed()) {
				inputsTable.removeAll();
			}
			for (Iterator<SubAppInterfaceElementView> iterator = subAppEditPart
					.getCastedModel().getInterfaceElements().iterator(); iterator
					.hasNext();) {
				SubAppInterfaceElementView interfaceElementView = iterator
						.next();

				TableItem item = null;
				if (interfaceElementView.getIInterfaceElement().isIsInput()) {
					if (!inputsTable.isDisposed()) {
						item = new TableItem(inputsTable, SWT.NONE);
					}
				} else if (!outputsTable.isDisposed()) {
					item = new TableItem(outputsTable, SWT.NONE);
				}
				if (item != null) {
					item.setData(interfaceElementView);
					item.setText(interfaceElementView.getLabel());
					if (interfaceElementView.isVisible()) {
						item.setChecked(true);
					}
					if (interfaceElementView.getInConnections().size() > 0
							|| interfaceElementView.getOutConnections().size() > 0) {
						item.setGrayed(true);
					}
				}
			}
			if (!inputsTable.isDisposed()) {				
				inputsTable.addListener(SWT.Selection, new Listener() {

					public void handleEvent(final Event event) {
						TableItem item = (TableItem) event.item;
						if (item == null) return;
						
						SubAppInterfaceElementView interfaceElementView = (SubAppInterfaceElementView) item
								.getData();
						if (item.getGrayed()) {
							if (!item.getChecked()) {
								item.setChecked(true);
								item.setGrayed(true);
								interfaceElementView.setVisible(true);
							}
						} else {
							interfaceElementView.setVisible(item.getChecked());
						}
						
						// Clean up any previous editor control
						Control oldEditor = inputsEditor.getEditor();
						if (oldEditor != null) oldEditor.dispose();
				
						// The control that will be the editor must be a child of the Table
						Text newEditor = new Text(inputsTable, SWT.NONE);
						newEditor.setText(item.getText());
						newEditor.addModifyListener(new ModifyListener() {
							public void modifyText(ModifyEvent me) {
								Text text = (Text)inputsEditor.getEditor();
								SubAppInterfaceElementView interfaceElementView = (SubAppInterfaceElementView) inputsEditor.getItem()
										.getData();
								if (text.getText().trim().isEmpty()) {
									interfaceElementView.setLabelSubstitute("");
									inputsEditor.getItem().setText(interfaceElementView.getLabel());
									return;
								}
								
								inputsEditor.getItem().setText(text.getText());								
								interfaceElementView.setLabelSubstitute(text.getText());
							}
						});
						newEditor.selectAll();
						newEditor.setFocus();
						inputsEditor.setEditor(newEditor, item, 0);
					}
				});
			}
			if (!outputsTable.isDisposed()) {
				outputsTable.addListener(SWT.Selection, new Listener() {
					public void handleEvent(final Event event) {
						TableItem item = (TableItem) event.item;
						SubAppInterfaceElementView interfaceElementView = (SubAppInterfaceElementView) item
								.getData();
						if (item.getGrayed()) {
							if (!item.getChecked()) {
								item.setChecked(true);
								item.setGrayed(true);
								interfaceElementView.setVisible(true);
							}
						} else {
							interfaceElementView.setVisible(item.getChecked());
						}
						
						// Clean up any previous editor control
						Control oldEditor = outputsEditor.getEditor();
						if (oldEditor != null) oldEditor.dispose();
				
						// The control that will be the editor must be a child of the Table
						Text newEditor = new Text(outputsTable, SWT.NONE);
						newEditor.setText(item.getText());
						newEditor.addModifyListener(new ModifyListener() {
							public void modifyText(ModifyEvent me) {
								Text text = (Text)outputsEditor.getEditor();
								SubAppInterfaceElementView interfaceElementView = (SubAppInterfaceElementView) outputsEditor.getItem()
										.getData();
								if (text.getText().trim().isEmpty()) {
									interfaceElementView.setLabelSubstitute("");
									outputsEditor.getItem().setText(interfaceElementView.getLabel());
									return;
								}
								
								outputsEditor.getItem().setText(text.getText());								
								interfaceElementView.setLabelSubstitute(text.getText());
							}
						});
						newEditor.selectAll();
						newEditor.setFocus();
						outputsEditor.setEditor(newEditor, item, 0);
					}
				});
			}
		} catch (SWTException e) {
			// TODO __gebenh error handling, (if this is an error)
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#setInput(org.eclipse.ui.IWorkbenchPart,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void setInput(final IWorkbenchPart part, final ISelection selection) {
		super.setInput(part, selection);
		Assert.isTrue(selection instanceof IStructuredSelection);
		Object input = ((IStructuredSelection) selection).getFirstElement();
		Assert.isTrue(input instanceof SubAppForFBNetworkEditPart);

		if (this.subAppEditPart != null
				&& this.subAppEditPart.getCastedModel().eAdapters().contains(
						econtentAdapter)) {
			this.subAppEditPart.getCastedModel().eAdapters().remove(
					econtentAdapter);
		}
		this.subAppEditPart = (SubAppForFBNetworkEditPart) input;
		subAppEditPart.getCastedModel().eAdapters().add(econtentAdapter);
		fillTable();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#dispose()
	 */
	@Override
	public void dispose() {
		if (this.subAppEditPart != null
				&& this.subAppEditPart.getCastedModel().eAdapters().contains(
						econtentAdapter)) {
			this.subAppEditPart.getCastedModel().eAdapters().remove(
					econtentAdapter);
		}

		super.dispose();
	}
}
