/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.actions;

import java.util.ArrayList;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.application.commands.CreateSubAppCommand;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UiFactory;

/**
 * The Class NewSubApplicationAction.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class NewSubApplicationAction extends SelectionAction {

	/** The Constant ID. */
	public static final String ID = "NewSubApplicationAction"; //$NON-NLS-1$

	/** The fbs. */
	private ArrayList<FBView> fbs;

	/** The sub apps. */
	private ArrayList<SubAppView> subApps;

	private Point pt;

	/**
	 * Instantiates a new new sub application action.
	 * 
	 * @param part the part
	 */
	public NewSubApplicationAction(IWorkbenchPart part) {
		super(part);
		setId(ID);
		setText(Messages.NewSubApplicationAction_NewSubapplicationText);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
	 */
	protected boolean calculateEnabled() {
		return (null != pt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
		fbs = new ArrayList<FBView>();
		subApps = new ArrayList<SubAppView>();
		
		for (Object obj : getSelectedObjects()) {
			if (obj instanceof FBEditPart)
				fbs.add(((FBEditPart) obj).getCastedModel());
			if (obj instanceof SubAppForFBNetworkEditPart)
				subApps.add(((SubAppForFBNetworkEditPart) obj).getCastedModel());
		}

		CreateSubAppCommand cmd = new CreateSubAppCommand();

		cmd.setFbs(fbs);
		cmd.setSubApps(subApps);

		if (fbs.size() > 0) {
			FBView fbView = fbs.get(0);
			cmd.setPosition((Position) EcoreUtil.copy(fbView.getPosition()));
			//cmd.setParentNetwork((Diagram) fbView.eContainer());

		} else if (subApps.size() > 0) {
			SubAppView saView = subApps.get(0);
			cmd.setPosition((Position) EcoreUtil.copy(saView.getPosition()));
			//cmd.setParentNetwork((Diagram) saView.eContainer());
		}else{
			Position pos = UiFactory.eINSTANCE.createPosition();
			pos.setX(pt.x);
			pos.setY(pt.y);			
			cmd.setPosition(pos);
		}
		cmd.setParentNetwork(((FBNetworkEditor)getWorkbenchPart()).getDiagramModel());
		getCommandStack().execute(cmd);
	}
	
	public void updateCreatePosition(Point pt) {
		this.pt = pt;		
	}

}
