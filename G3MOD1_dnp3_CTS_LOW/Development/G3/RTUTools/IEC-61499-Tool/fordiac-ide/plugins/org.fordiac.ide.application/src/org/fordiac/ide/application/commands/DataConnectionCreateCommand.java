/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.util.commands.ConnectionUtil;

/**
 * The Class DataConnectionCreateCommand.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class DataConnectionCreateCommand extends
		AbstractDataConnectionCreateCommand {

	/** The resource connection. */
	protected ConnectionView resourceConnection;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.application.commands.AbstractDataConnectionCreateCommand
	 * #doAdvancedCreation()
	 */
	@Override
	protected void doAdvancedCreation() {
		InterfaceElementView mappedSource = sourceView.getMappedInterfaceElement();
		InterfaceElementView mappedDest = destinationView
				.getMappedInterfaceElement();

		resourceConnection = ConnectionUtil.createConnectionInResource(
				mappedSource, mappedDest, connectionView);

	}

	@Override
	protected void redoAdvancedCreation() {
		if (resourceConnection != null) {
			Connection connectionElement = resourceConnection.getConnectionElement();
			if (resourceParent != null && resourceParent instanceof UIResourceEditor) {
				resourceParent.getConnections().add(resourceConnection);
				UIResourceEditor uiResourceEditor = (UIResourceEditor) resourceParent;
				Resource resourceElement = uiResourceEditor.getResourceElement();
				if (resourceElement != null) {
					if (connectionElement instanceof EventConnection) {
						resourceElement.getFBNetwork().getEventConnections().add(
								(EventConnection) connectionElement);
					}
					if (connectionElement instanceof DataConnection) {
						resourceElement.getFBNetwork().getDataConnections().add(
								(DataConnection) connectionElement);
					}
				}
			}
			resourceConnection.setSource(source);
			resourceConnection.setDestination(dest);
		}
	}

	private Diagram resourceParent;
	private InterfaceElementView source;
	private InterfaceElementView dest;

	@Override
	protected void undoAdvancedCreation() {
		if (resourceConnection != null) {
			resourceParent = (Diagram) resourceConnection.eContainer();
			source = resourceConnection.getSource();
			dest = resourceConnection.getDestination();

			resourceParent.getConnections().remove(resourceConnection);

			Connection connectionElement = resourceConnection.getConnectionElement();

			if (resourceParent != null && resourceParent instanceof UIResourceEditor) {
				UIResourceEditor uiResourceEditor = (UIResourceEditor) resourceParent;
				Resource resourceElement = uiResourceEditor.getResourceElement();
				if (resourceElement != null) {
					if (connectionElement instanceof EventConnection) {
						resourceElement.getFBNetwork().getEventConnections().remove(
								connectionElement);
					}
					if (connectionElement instanceof DataConnection) {
						resourceElement.getFBNetwork().getDataConnections().remove(
								connectionElement);
					}
				}
			}
			resourceConnection.setSource(null);
			resourceConnection.setDestination(null);
		}
	}

}
