/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editors;

import org.eclipse.ui.IMemento;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.util.PersistableUntypedEditorInput;

public class ApplicationEditorInput extends PersistableUntypedEditorInput {

	public ApplicationEditorInput(Application app) {
		super(app.getFBNetwork().eContainer(), app.getName(), app.getComment());
	}
	
	@Override
	public void saveState(IMemento memento) {
		ApplicationEditorInputFactory.saveState(memento, this);
		
	}

	@Override
	public String getFactoryId() {
		return ApplicationEditorInputFactory.getFactoryId();
	}
	
	public Application getApplication(){
		UIFBNetwork uiFBNetwork = (UIFBNetwork)getContent();
		return uiFBNetwork.getFbNetwork().getApplication();
	}

}
