/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.util.commands.ConnectionUtil;

/**
 * The Class AbstractEventConnectionCreateCommand.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public abstract class AbstractEventConnectionCreateCommand extends AbstractConnectionCreateCommand {

	/** The event connection. */
	protected EventConnection eventConnection;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		setEditor(ApplicationPlugin.getDefault().getCurrentActiveEditor());
		connectionView = UiFactory.eINSTANCE.createConnectionView();
		
		checkSourceAndTarget();
		
		if (sourceView instanceof InternalSubAppInterfaceElementView) {
			findOuterSource();
		}
		if (destinationView instanceof InternalSubAppInterfaceElementView) {
			findOuterDest();
		}
		eventConnection = LibraryElementFactory.eINSTANCE
				.createEventConnection();
		eventConnection.setSource((Event) sourceView.getIInterfaceElement());
		eventConnection.setDestination((Event) destinationView.getIInterfaceElement());
		eventConnection.setDx1(getDx1());
		eventConnection.setDx2(getDx2());
		eventConnection.setDy(getDy());
				
		connectionView.setConnectionElement(eventConnection);

		connectionView.setSource(sourceView);
		connectionView.setDestination(destinationView);

		if (sourceView instanceof SubAppInterfaceElementView) {
			// create internal connection
			SubAppInterfaceElementView subAppInterfaceElementViewDest = (SubAppInterfaceElementView) sourceView;
			if(subAppInterfaceElementViewDest instanceof MappedSubAppInterfaceElementView){
				subAppInterfaceElementViewDest = (SubAppInterfaceElementView)((MappedSubAppInterfaceElementView) subAppInterfaceElementViewDest).getApplicationInterfaceElement();
			}
			if(null == ((SubAppView) subAppInterfaceElementViewDest.eContainer()).getSubApp().getPaletteEntry()){
					//only go into the sub-app if don't have a type
				UISubAppNetwork destNetwork = ((SubAppView) subAppInterfaceElementViewDest
						.eContainer()).getUiSubAppNetwork();
				internalConnections.addAll(ConnectionUtil
						.createInternalSourceConnection(
								subAppInterfaceElementViewDest, destNetwork,
								connectionView));
			}
		}
		if (destinationView instanceof SubAppInterfaceElementView) {
			// create internal connection
			SubAppInterfaceElementView subAppInterfaceElementViewDest = (SubAppInterfaceElementView) destinationView;

			if(subAppInterfaceElementViewDest instanceof MappedSubAppInterfaceElementView){
				subAppInterfaceElementViewDest = (SubAppInterfaceElementView)((MappedSubAppInterfaceElementView) subAppInterfaceElementViewDest).getApplicationInterfaceElement();
			}

			if(null == ((SubAppView) subAppInterfaceElementViewDest.eContainer()).getSubApp().getPaletteEntry()){
				//only go into the sub-app if don't have a type
				
				UISubAppNetwork destNetwork = ((SubAppView) subAppInterfaceElementViewDest.eContainer()).getUiSubAppNetwork();
				
				internalConnections.addAll(ConnectionUtil
						.createInternalDestConnection(
								subAppInterfaceElementViewDest, destNetwork,
								connectionView));
			}
		}

		parent.getFunctionBlockNetwork().getEventConnections().add(
				eventConnection);
		parent.getConnections().add(connectionView);

		doAdvancedCreation();
	}

	private void checkSourceAndTarget() {
		Event srcEvent = (Event) sourceView.getIInterfaceElement();
		
		InterfaceList ifList = (InterfaceList)srcEvent.eContainer();
		boolean needsChange = false;
		
		if(ifList.eContainer() instanceof CompositeFBType){
			needsChange = (!(ifList.getEventInputs().contains(srcEvent)));			
		}
		else{
			needsChange = (!(ifList.getEventOutputs().contains(srcEvent))); 
		}
		
		if(needsChange){
			// our src is an input we have to swap source and target
			InterfaceElementView buf = destinationView;
			destinationView = sourceView;
			sourceView = buf;
		}
	}

	/**
	 * Do advanced creation.
	 */
	protected abstract void doAdvancedCreation();

	

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		if (sourceView == null && destinationView == null) {
			return false;
		}
		if (sourceView != null && !(sourceView.getIInterfaceElement() instanceof Event)) {
			return false;
		}
		if (destinationView != null && !(destinationView.getIInterfaceElement() instanceof Event)) {
			return false;
		}
		if (sourceView == null) {
			return true; // link creation is in progress; source is not
			// defined yet
		}
		
		if(duplicateConnection()){			
			return false;
		}

		boolean retVal = LinkConstraints.canCreateEventConnection(
				(Event) sourceView.getIInterfaceElement(), (Event)destinationView.getIInterfaceElement(), internalConnection);
		return retVal;
	}

	private boolean duplicateConnection() {
		Event dest = (Event)destinationView.getIInterfaceElement();
		if(null != dest){
			for (EventConnection con : ((Event)sourceView.getIInterfaceElement()).getInputConnections()) {
				//as we are maybe creating a reverse connection we need to check both 
				if(dest.equals(con.getSource()) || dest.equals(con.getDestination())){
					return true;
				}
			}
			for (EventConnection con : ((Event)sourceView.getIInterfaceElement()).getOutputConnections()) {
				//as we are maybe creating a reverse connection we need to check both
				if(dest.equals(con.getSource()) || dest.equals(con.getDestination())){
					return true;
				}
			}
		}
		
		return false;
	}

	/**
	 * Undo advanced creation.
	 */
	protected abstract void undoAdvancedCreation();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {

		undoAdvancedCreation();

		ConnectionUtil.undoInternalConnection(internalConnections);

		eventConnection.setSource(null);
		eventConnection.setDestination(null);
		parent.getFunctionBlockNetwork().getEventConnections().remove(
				eventConnection);

		connectionView.setSource(null);
		connectionView.setDestination(null);
		parent.getConnections().remove(connectionView);

	}

	/**
	 * Redo advanced creation.
	 */
	protected abstract void redoAdvancedCreation();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {

		ConnectionUtil.redoInternalConnection(internalConnections);

		eventConnection.setSource((Event) sourceView.getIInterfaceElement());
		eventConnection.setDestination((Event) destinationView.getIInterfaceElement());
		parent.getFunctionBlockNetwork().getEventConnections().add(
				eventConnection);

		connectionView.setSource(sourceView);
		connectionView.setDestination(destinationView);
		parent.getConnections().add(connectionView);

		redoAdvancedCreation();

	}

}
