/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.ReconnectRequest;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.util.commands.DeleteConnectionCommand;

/**
 * A command for reconnecting data connections.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ReconnectDataConnectionCommand extends Command {

	/** The parent. */
	private Diagram parent;

	/** The request. */
	protected final ReconnectRequest request;

	/** The editor. */
	private IEditorPart editor;

	/** The cmd. */
	protected DeleteConnectionCommand cmd;

	/** The dccc. */
	protected AbstractDataConnectionCreateCommand dccc;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(ApplicationPlugin.getDefault()
				.getCurrentActiveEditor());

	}

	/**
	 * A command for reconnecting data connection.
	 * 
	 * @param request the request
	 */
	public ReconnectDataConnectionCommand(final ReconnectRequest request) {
		super(Messages.ReconnectDataConnectionCommand_LABEL_Reconnect);
		this.request = request;
	}

	/**
	 * Can execute.
	 * 
	 * @return <code>true</code> if the new connection is possible.
	 */
	@Override
	public boolean canExecute() {
		if (request.getType().equals(RequestConstants.REQ_RECONNECT_TARGET)) {
			EditPart source = request.getConnectionEditPart().getSource();
			if (source instanceof InterfaceEditPart) {
				InterfaceEditPart interfaceEditPart = (InterfaceEditPart) source;
				if (interfaceEditPart.getCastedModel() != null
						&& interfaceEditPart.getCastedModel()
								.getIInterfaceElement() instanceof VarDeclaration) {
					VarDeclaration sourceVarDeclaration = (VarDeclaration) interfaceEditPart
							.getCastedModel().getIInterfaceElement();

					if (request.getTarget() instanceof InterfaceEditPart) {
						InterfaceEditPart newTargetEditPart = (InterfaceEditPart) request
								.getTarget();
						if (newTargetEditPart.getCastedModel() != null
								&& newTargetEditPart.getCastedModel()
										.getIInterfaceElement() instanceof VarDeclaration) {
							VarDeclaration newTarget = (VarDeclaration) newTargetEditPart
									.getCastedModel().getIInterfaceElement();
							return LinkConstraints.canReconnectDataConnection(
									sourceVarDeclaration, newTarget);
						}
					}

				}
			}
			return false;
		}
		if (request.getType().equals(RequestConstants.REQ_RECONNECT_SOURCE)) {
			EditPart target = request.getConnectionEditPart().getTarget();
			if (target instanceof InterfaceEditPart) {
				InterfaceEditPart interfaceEditPart = (InterfaceEditPart) target;
				if (interfaceEditPart.getCastedModel() != null
						&& interfaceEditPart.getCastedModel()
								.getIInterfaceElement() instanceof VarDeclaration) {
					VarDeclaration targetVarDeclaration = (VarDeclaration) interfaceEditPart
							.getCastedModel().getIInterfaceElement();

					if (request.getTarget() instanceof InterfaceEditPart) { // target
						// in
						// this
						// case
						// is
						// the
						// source!
						InterfaceEditPart newTargetEditPart = (InterfaceEditPart) request
								.getTarget();
						if (newTargetEditPart.getCastedModel() != null
								&& newTargetEditPart.getCastedModel()
										.getIInterfaceElement() instanceof VarDeclaration) {
							VarDeclaration newSourceVarDeclaration = (VarDeclaration) newTargetEditPart
									.getCastedModel().getIInterfaceElement();
							return LinkConstraints
									.canReconnectDataConnectionSource(
											newSourceVarDeclaration,
											targetVarDeclaration);
						}
					}

				}
			}
			return false;
		}
		return false;
	}

	/**
	 * sets the new Source our DestinationPoint.
	 */
	@Override
	public void execute() {
		editor = ApplicationPlugin.getDefault().getCurrentActiveEditor();
		if (request.getType().equals(RequestConstants.REQ_RECONNECT_TARGET)) {
			doReconnectTarget();
		}
		if (request.getType().equals(RequestConstants.REQ_RECONNECT_SOURCE)) {
			doReconnectSource();
		}

	}

	/**
	 * Do reconnect source.
	 */
	protected void doReconnectSource() {
		cmd = new DeleteConnectionCommand((ConnectionView) request
				.getConnectionEditPart().getModel());
		dccc = createCreateDataConCommand();
		dccc.setSource((InterfaceEditPart) request.getTarget());
		dccc.setTarget((InterfaceEditPart) request.getConnectionEditPart()
				.getTarget());
		dccc.setParent(parent);
		cmd.execute();
		dccc.execute();

	}

	/**
	 * Do reconnect target.
	 */
	protected void doReconnectTarget() {
		cmd = new DeleteConnectionCommand((ConnectionView) request
				.getConnectionEditPart().getModel());
		dccc = createCreateDataConCommand();
		dccc.setSource((InterfaceEditPart) request.getConnectionEditPart()
				.getSource());
		dccc.setTarget((InterfaceEditPart) request.getTarget());
		dccc.setParent(parent);

		cmd.execute();
		dccc.execute();

	}

	protected AbstractDataConnectionCreateCommand createCreateDataConCommand() {
		return new DataConnectionCreateCommand();
	}

	/**
	 * Redo.
	 * 
	 * @see ReconnectDataConnectionCommand#execute()
	 */
	@Override
	public void redo() {
		cmd.redo();
		dccc.redo();

	}

	/**
	 * undo this command.
	 */
	@Override
	public void undo() {
		dccc.undo();
		cmd.undo();

	}

	/**
	 * Gets the parent.
	 * 
	 * @return the parent
	 */
	public Diagram getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 * 
	 * @param parent the new parent
	 */
	public void setParent(final Diagram parent) {
		this.parent = parent;
	}

}
