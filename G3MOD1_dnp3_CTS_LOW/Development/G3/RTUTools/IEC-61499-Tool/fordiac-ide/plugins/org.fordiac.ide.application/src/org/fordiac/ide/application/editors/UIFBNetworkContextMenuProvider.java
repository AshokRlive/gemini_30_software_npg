/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editors;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.application.actions.FBInsertAction;
import org.fordiac.ide.application.actions.FlattenSubApplicationAction;
import org.fordiac.ide.application.actions.MapAction;
import org.fordiac.ide.application.actions.NewSubApplicationAction;
import org.fordiac.ide.application.actions.SaveAsSubApplicationTypeAction;
import org.fordiac.ide.application.actions.UnmapAction;
import org.fordiac.ide.application.actions.UnmapAllAction;
import org.fordiac.ide.application.actions.UpdateFBTypeAction;
import org.fordiac.ide.application.commands.FBCreateCommand;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;
import org.fordiac.ide.application.utilities.FBTypeTemplateCreationFactory;
import org.fordiac.ide.gef.DiagramEditorWithFlyoutPalette;
import org.fordiac.ide.gef.ZoomUndoRedoContextMenuProvider;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.model.Palette.FBTypePaletteEntry;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.Palette.PaletteGroup;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.util.AdvancedPanningSelectionTool;
import org.fordiac.systemmanagement.SystemManager;

/**
 * This class builds the context menu for the FBNetwork Editor.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class UIFBNetworkContextMenuProvider extends
ZoomUndoRedoContextMenuProvider {

	Palette palette;
	DiagramEditorWithFlyoutPalette editor;
	Point pt;

	/**
	 * Instantiates a new uIFB network context menu provider.
	 * 
	 * @param viewer
	 *            the viewer
	 * @param registry
	 *            the registry
	 * @param zoomManager
	 *            the zoom manager
	 */
	public UIFBNetworkContextMenuProvider(final DiagramEditorWithFlyoutPalette editor,
			final ActionRegistry registry, final ZoomManager zoomManager, Palette palette) {
		super(editor.getViewer(), zoomManager, registry);
		this.palette = palette;
		this.editor = editor;

		getViewer().getControl().addMenuDetectListener(new MenuDetectListener() {
			public void menuDetected(MenuDetectEvent e) {
				pt = getViewer().getControl().toControl(e.x, e.y);			  
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.ContextMenuProvider#buildContextMenu(org.eclipse.jface.
	 * action.IMenuManager)
	 */
	@Override
	public void buildContextMenu(final IMenuManager menu) {

		if (getViewer().getEditDomain().getActiveTool() instanceof AdvancedPanningSelectionTool) {
			AdvancedPanningSelectionTool st = (AdvancedPanningSelectionTool) getViewer()
					.getEditDomain().getActiveTool();
			if (st.isMoved()) { // pan executed
				return;
			} else {
				EditPart currentPart = getViewer().findObjectAt(
						st.getLocation());
				EditPart selected = st.getTargetEditPart();
				if (selected != null
						&& selected.getSelected() == EditPart.SELECTED_NONE
						&& selected instanceof AbstractViewEditPart) {
					getViewer().select(currentPart);
				}

				if (selected == null) {
					getViewer().select(currentPart);
				}

			}
		}

		super.buildContextMenu(menu);
		IAction action;

		action = registry.getAction(ActionFactory.SELECT_ALL.getId());
		menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);

		action = registry.getAction(ActionFactory.DELETE.getId());
		menu.appendToGroup(GEFActionConstants.GROUP_COPY, action);

		action = registry.getAction(ActionFactory.COPY.getId());
		menu.appendToGroup(GEFActionConstants.GROUP_COPY, action);

		action = registry.getAction(ActionFactory.PASTE.getId());
		menu.appendToGroup(GEFActionConstants.GROUP_COPY, action);

		action = registry.getAction(GEFActionConstants.DIRECT_EDIT);
		if (action != null && action.isEnabled()) {
			menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
		}
		action = registry.getAction(NewSubApplicationAction.ID);
		if (action instanceof NewSubApplicationAction) {
			NewSubApplicationAction newSubAppAction = (NewSubApplicationAction) action;
			newSubAppAction.updateCreatePosition(pt);
			if (action.isEnabled()) {
				menu.appendToGroup(GEFActionConstants.GROUP_REST, action);
			}
		}
		action = registry.getAction(FlattenSubApplicationAction.ID);
		if ((action != null) && (action.isEnabled())) {
			menu.appendToGroup(GEFActionConstants.GROUP_REST, action);
		}

		action = registry.getAction(SaveAsSubApplicationTypeAction.ID);
		if ((action != null) && (action.isEnabled())) {
			menu.appendToGroup(GEFActionConstants.GROUP_REST, action);
		}

		
		menu.appendToGroup(GEFActionConstants.GROUP_REST, new Separator());
		menu.appendToGroup(GEFActionConstants.GROUP_REST, addHWMappingMenu());
		
		action = registry.getAction(UnmapAction.ID);
		if (action != null) {
			menu.appendToGroup(GEFActionConstants.GROUP_REST, action);
		}
		
		action = registry.getAction(UnmapAllAction.ID);
		if (action != null) {
			menu.appendToGroup(GEFActionConstants.GROUP_REST, action);
		}

		menu.appendToGroup(GEFActionConstants.GROUP_REST, new Separator()); 
				
		action = registry.getAction(UpdateFBTypeAction.ID);
		if (action != null && action.isEnabled()) {
			menu.appendToGroup(GEFActionConstants.GROUP_ADD, action);
		}

		menu.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));

		createFBMenus(menu);		

	}


	@SuppressWarnings("rawtypes")
	private void createFBMenus(final IMenuManager menu){	
		useChangeFBType = false;
		String text = "Insert FB";
		List eps = editor.getViewer().getSelectedEditParts();			
		for (Object ep : eps) {
			if (ep instanceof FBEditPart) {
				text = "Change FB Type";
				useChangeFBType = true;
				break;
			}
		}
		MenuManager submenu = new MenuManager(text);
		menu.appendToGroup(GEFActionConstants.GROUP_ADD, submenu);
		fillMenuForPalletteGroup(submenu, palette.getRootGroup().getSubGroups());
	}

	private boolean useChangeFBType = false;

	private void fillMenuForPalletteGroup(MenuManager insertTypeEntry, EList<PaletteGroup> subGroups) {

		MenuManager submenu;
		Action action;

		//TODO sort groups alphabetically

		for (PaletteGroup group : subGroups) {
			submenu = new MenuManager(group.getLabel());
			fillMenuForPalletteGroup(submenu, group.getSubGroups());

			for (org.fordiac.ide.model.Palette.PaletteEntry entry : group.getEntries()) {

				if(entry instanceof FBTypePaletteEntry){
					if (useChangeFBType) {
						action = (Action) registry.getAction(entry.getFile()
								.getFullPath().toString().concat("_")
								.concat(UpdateFBTypeAction.ID));
					} else {
						action = (Action) registry.getAction(entry.getFile().getFullPath().toString());
					}
					if(null == action){
						if (useChangeFBType) {
							action = createChangeFBTypeAction(entry);
						} else {
							action = createFBInsertAction(entry);
							((FBInsertAction) action).updateCreatePosition(pt);
						}
					}
					submenu.add(action);
				}
			}
			if(!submenu.isEmpty()){
				insertTypeEntry.add(submenu);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private UpdateFBTypeAction createChangeFBTypeAction(PaletteEntry entry) {
		UpdateFBTypeAction action = new UpdateFBTypeAction(editor, entry);
		registry.registerAction(action);
		editor.getSelActions().add(action.getId());
		action.update();
		return action;
	}

	private FBInsertAction createFBInsertAction(PaletteEntry entry) {
		FBInsertAction action = null;
		CreateRequest request = new CreateRequest();
		request.setFactory(new FBTypeTemplateCreationFactory(entry));

		org.eclipse.swt.graphics.Point location = Display.getCurrent().getCursorLocation();
		location = getViewer().getControl().toControl(location.x, location.y);
		request.setLocation(new org.eclipse.draw2d.geometry.Point(location));

		Command cmd = getViewer().getContents().getCommand(request);

		if(cmd instanceof FBCreateCommand){
			action = new FBInsertAction(editor, (FBCreateCommand)cmd);
			registry.registerAction(action);
		}

		return action;
	}

	/**
	 * Adds the hw mapping menu.
	 * 
	 * @return the IMenuManager
	 */
	@SuppressWarnings("unchecked")
	private IMenuManager addHWMappingMenu() {
		MenuManager submenu = new MenuManager(
				Messages.UIFBNetworkContextMenuProvider_LABEL_HardwareMapping);
		GEFActionConstants.addStandardActionGroups(submenu);
		FBNetworkEditor editor;
		// SubApplicationEditor subAppEditor;
		List<Device> devices = Collections.emptyList();
		List<Resource> resources = Collections.emptyList();
		AutomationSystem system;
		// Hardware hw = null;
		IAction action; 

		IEditorPart activeEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		ISelection selection = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getSelection();		

		if(isFBorSubAppSelected(selection) && activeEditor instanceof FBNetworkEditor){		
			editor = (FBNetworkEditor) activeEditor;
			system = editor.getSystem();
			devices = system.getSystemConfiguration().getSystemConfigurationNetwork().getDevices();

			for (Device device : devices){
				MenuManager devmenu = new MenuManager(
						device.getName() == null ? "Device" : device.getName()); //$NON-NLS-1$
				GEFActionConstants.addStandardActionGroups(devmenu);

				resources = device.getResource();
				if (resources.size() > 0) {
					for (Iterator<Resource> miter = resources.iterator(); miter
							.hasNext();) {
						Resource res = miter.next();
						if (!res.isDeviceTypeResource()) {
							ResourceView resView = SystemManager.getInstance()
									.getResourceViewForResource(res);
							action = getMapAction(activeEditor, resView);
							if (action != null) {
								editor.getSelActions().add(action.getId());
								if (action.isEnabled()) {
									if (checkIsCurrentlyMappedTo(resView
											.getUIResourceDiagram())) {
										action.setChecked(true);
									}
									devmenu.appendToGroup(
											GEFActionConstants.GROUP_REST,
											action);

								}
							}
						}
					}
				}

				submenu.appendToGroup(GEFActionConstants.GROUP_REST, devmenu);

			}
		}
		return submenu;
	}

	private boolean isFBorSubAppSelected(ISelection selection) {
		if(selection instanceof StructuredSelection){
			for(Object element : ((IStructuredSelection)selection).toArray()){
				if (element instanceof FBEditPart || element instanceof SubAppForFBNetworkEditPart){
					return true;
				}				
			}
		}
		return false;
	}

	protected IAction getMapAction(IEditorPart activeEditor, ResourceView resView) {
		if (resView != null) {
			IAction action;
			action = new MapAction(activeEditor, resView.getUIResourceDiagram());
			return action;
		} 
		return null;  
	}

	/**
	 * Check is currently mapped to.
	 * 
	 * @param res
	 *            the res
	 * 
	 * @return true, if successful
	 */
	private boolean checkIsCurrentlyMappedTo(final UIResourceEditor res) {
		// multiple fbs/subapps selected -> selection is not uniquely mapped to
		// a
		// single resource
		if (getViewer().getSelectedEditParts().size() != 1) {
			return false;
		} else {
			if (getViewer().getSelectedEditParts().get(0) instanceof FBEditPart) {
				FBEditPart fbep = (FBEditPart) getViewer()
						.getSelectedEditParts().get(0);
				if (fbep.getCastedModel().getMappedFB() != null) {
					if (fbep.getCastedModel().getMappedFB().eContainer() != null) {
						return fbep.getCastedModel().getMappedFB().eContainer()
								.equals(res);
					} else {
						return false;
					}
				} else {
					return false;
				}
			}
		}
		return false;
	}

}
