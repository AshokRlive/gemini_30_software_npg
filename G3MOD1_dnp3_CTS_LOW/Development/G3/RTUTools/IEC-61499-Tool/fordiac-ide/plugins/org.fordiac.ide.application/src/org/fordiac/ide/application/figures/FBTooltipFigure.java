/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.text.FlowPage;
import org.eclipse.draw2d.text.ParagraphTextLayout;
import org.eclipse.draw2d.text.TextFlow;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.UIResourceEditor;

/**
 * The Class FBTooltipFigure.
 */
public class FBTooltipFigure extends Figure {

	/**
	 * Instantiates a new fB tooltip figure.
	 * 
	 * @param fbView the fb view
	 */
	public FBTooltipFigure(final FBView fbView) {
		setLayoutManager(new GridLayout());

		Label instanceNameLabel = new Label(fbView.getFb().getName());
		add(instanceNameLabel);

		TextFlow content = new TextFlow(
				fbView.getFb().getComment() != null ? fbView.getFb().getComment() : "");
		content.setLayoutManager(new ParagraphTextLayout(content,
				ParagraphTextLayout.WORD_WRAP_HARD));

		FlowPage fp = new FlowPage();
		fp.add(content);
		if (fbView.getFb().getComment() != null
				&& fbView.getFb().getComment().length() > 0) {
			add(fp);
			setConstraint(fp, new GridData(PositionConstants.CENTER,
					PositionConstants.MIDDLE, false, true));
		}

		setConstraint(instanceNameLabel, new GridData(PositionConstants.CENTER,
				PositionConstants.MIDDLE, true, true));

		SubAppNetwork fbNetwork = (SubAppNetwork) fbView.getFb().eContainer();
		if (fbNetwork instanceof FBNetwork) {

			Application app = ((FBNetwork) fbNetwork).getApplication();
			if (app != null
					&& app.eContainer() instanceof org.fordiac.ide.model.libraryElement.AutomationSystem) {

				Label system = new Label(Messages.FBTooltipFigure_LABEL_System
						+ ((AutomationSystem) app.eContainer()).getName());
				add(system);

				setConstraint(system, new GridData(PositionConstants.CENTER,
						PositionConstants.MIDDLE, true, true));
			}
			if (app != null) {
				Label application = new Label(
						Messages.FBTooltipFigure_LABEL_Application + app.getName());
				add(application);

				setConstraint(application, new GridData(PositionConstants.CENTER,
						PositionConstants.MIDDLE, true, true));

			}

		}
		FBView mappedFB = null;
		if (fbView.getApplicationFB() != null) {
			mappedFB = fbView;
		}
		if (fbView.getMappedFB() != null) {
			mappedFB = fbView.getMappedFB();

		}
		if (mappedFB != null) {
			UIResourceEditor uiResEditor = (UIResourceEditor) mappedFB.eContainer();
			if (uiResEditor != null) {
				Resource res = uiResEditor.getResourceElement();
				if (res != null && res.getName() != null) {
					Device device = res.getDevice();
					String deviceName = "";
					if (device != null) {
						deviceName = device.getName();
					}
					Label resourceLabel = new Label(
							Messages.FBTooltipFigure_LABEL_MappedTo + deviceName + "."
									+ res.getName());
					add(resourceLabel);
					setConstraint(resourceLabel, new GridData(PositionConstants.CENTER,
							PositionConstants.MIDDLE, true, true));
				}

			}
		}
	}
}
