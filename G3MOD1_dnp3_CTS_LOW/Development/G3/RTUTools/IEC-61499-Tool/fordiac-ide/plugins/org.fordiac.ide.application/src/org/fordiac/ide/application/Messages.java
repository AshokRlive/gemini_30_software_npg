/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.application.messages"; //$NON-NLS-1$

	public static String FBFigure_TYPE_NOT_SET;
	
	/** The Command util_ erro r_ reopen sub app. */
	public static String CommandUtil_ERROR_ReopenSubApp;
	
	/** The Create sub app command_ label create sub app command. */
	public static String CreateSubAppCommand_LABELCreateSubAppCommand;
	
	/** The Delete connection command_ labe l_ delete connection. */
	public static String DeleteConnectionCommand_LABEL_DeleteConnection;
	
	/** The Delete fb command_ labl e_ delete fb command. */
	public static String DeleteFBCommand_LABLE_DeleteFBCommand;
	
	/** The Element edit part factory_ labe l_ runtime exception_ cant create model for element. */
	public static String ElementEditPartFactory_LABEL_RUNTIMEException_CantCreateModelForElement;
	
	/** The FB create command_ labl e_ create function block. */
	public static String FBCreateCommand_LABLE_CreateFunctionBlock;
	
	public static String NewApplicationCommand_LABEL_NewApplication;
	
	/** The FB edit part_ erro r_ unsupported fb type. */
	public static String FBEditPart_ERROR_UnsupportedFBType;
		
	/** The FB figure_ no t_ define d_ text. */
	public static String FBFigure_NOT_DEFINED_Text;
				
	/** The FB network palette factory_ labe l_ tools group. */
	public static String FBNetworkPaletteFactory_LABEL_ToolsGroup;
	
	/** The FB tooltip figure_ labe l_ application. */
	public static String FBTooltipFigure_LABEL_Application;
	
	/** The FB tooltip figure_ labe l_ mapped to. */
	public static String FBTooltipFigure_LABEL_MappedTo;
	
	/** The FB tooltip figure_ labe l_ system. */
	public static String FBTooltipFigure_LABEL_System;
	
	/** The Flatten sub app command_ labe l_ flatten sub app command. */
	public static String FlattenSubAppCommand_LABEL_FlattenSubAppCommand;
	
	/** The Flatten sub application action_ flatten subapplication text. */
	public static String FlattenSubApplicationAction_FlattenSubapplicationText;
	
	/** The Interface visibility section_ labe l_ sub app inputs headline. */
	public static String InterfaceVisibilitySection_LABEL_SubAppInputsHeadline;
	
	/** The Interface visibility section_ labe l_ sub app outputs headline. */
	public static String InterfaceVisibilitySection_LABEL_SubAppOutputsHeadline;
	
	/** The Link constraints_ status message_has already input connection. */
	public static String LinkConstraints_STATUSMessage_hasAlreadyInputConnection;

	/** The Link constraint outputconnections to check that adapters has only one outputconn */
	public static String LinkConstraints_STATUSMessage_hasAlreadyOutputConnection;
	
	/** The Link constraints_ status message_ i n_ i n_ ou t_ ou t_not allowed. */
	public static String LinkConstraints_STATUSMessage_IN_IN_OUT_OUT_notAllowed;
	
	/** The Link constraints_ status message_ not compatible. */
	public static String LinkConstraints_STATUSMessage_NotCompatible;
	
	/** The Map sub app to command_ staus message_ already mapped. */
	public static String MapSubAppToCommand_STAUSMessage_AlreadyMapped;
	
	/** The Map to command_ status message_ already mapped. */
	public static String MapToCommand_STATUSMessage_AlreadyMapped;
	
	/** The New sub application action_ new subapplication text. */
	public static String NewSubApplicationAction_NewSubapplicationText;
	
	/** The Open application editor action_ erro r_ open application editor. */
	public static String OpenApplicationEditorAction_ERROR_OpenApplicationEditor;
	
	/** The Open sub application editor action_ erro r_ open subapplication editor. */
	public static String OpenSubApplicationEditorAction_ERROR_OpenSubapplicationEditor;
		
	/** The Reconnect data connection command_ labe l_ reconnect. */
	public static String ReconnectDataConnectionCommand_LABEL_Reconnect;
	
	/** The Reconnect event connection command_ labe l_ reconnect data connection. */
	public static String ReconnectEventConnectionCommand_LABEL_ReconnectDataConnection;
	
	/** The Sub app for fb network figure_ labe l_ not defined. */
	public static String SubAppForFbNetworkFigure_LABEL_NotDefined;
	
	/** The Sub app tooltip figure_ labe l_ application. */
	public static String SubAppTooltipFigure_LABEL_Application;
	
	/** The Sub app tooltip figure_ labe l_ mapped to. */
	public static String SubAppTooltipFigure_LABEL_MappedTo;
	
	/** The Sub app tooltip figure_ labe l_ system. */
	public static String SubAppTooltipFigure_LABEL_System;
	
	/** The UIFB network context menu provider_ labe l_ hardware mapping. */
	public static String UIFBNetworkContextMenuProvider_LABEL_HardwareMapping;
	
	/** The Unmap action_ erro r_ unmap. */
	public static String UnmapAction_ERROR_Unmap;
	
	/** The Unmap action_ unmap_ label. */
	public static String UnmapAction_Unmap_Label;

	public static String SaveAsSubApplicationTypeAction_SaveAsSubApplicationTypeText;

	public static String SaveAsSubApplicationTypeAction_UntypedSubappError;

	public static String SaveAsSubApplicationTypeAction_UntypedSubappErrorDescription;
	
	public static String SaveAsSubApplicationTypeAction_WizardTitle;

	public static String SaveAsSubApplicationTypeAction_WizardPageName;

	public static String SaveAsSubApplicationTypeAction_WizardPageTitel;

	public static String SaveAsSubApplicationTypeAction_WizardPageDescription;

	public static String SaveAsSubApplicationTypeAction_WizardPageOpenType;

	public static String SaveAsSubApplicationTypeAction_WizardPageNameLabel;

	public static String SaveAsSubApplicationTypeAction_WizardOverrideTitle;

	public static String SaveAsSubApplicationTypeAction_WizardOverrideMessage;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
		// empty private constructor
	}
}
