/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.actions;

import org.eclipse.ui.IWorkbenchPart;

public class UnmapAllAction extends UnmapAction {
	
	/** The Constant ID. */
	public static final String ID = "UnmapAll"; //$NON-NLS-1$

	/**
	 * Instantiates a new unmap all action.
	 * 
	 * @param part the part
	 */
	public UnmapAllAction(final IWorkbenchPart part) {
		super(part);
		setId(ID);
		setText("Unmap All");
	}
	

	@Override
	protected boolean calculateEnabled() {
		return !super.calculateEnabled();
	}
}
