/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.actions;

import org.eclipse.jface.action.Action;
import org.fordiac.ide.model.libraryElement.impl.ApplicationImpl;
import org.fordiac.ide.util.IOpenListener;
import org.fordiac.ide.util.OpenListenerManager;

/**
 * The Class OpenListenerAction.
 */
public class OpenListenerAction extends Action {
	private final IOpenListener openListener;

	/**
	 * Instantiates a new open listener action.
	 * 
	 * @param openListener the open listener
	 */
	public OpenListenerAction(final IOpenListener openListener) {
		this.openListener = openListener;
		setText("FBNetwork");
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		OpenListenerManager
				.getInstance()
				.setDefaultOpenListener(ApplicationImpl.class,
						"org.fordiac.ide.application.actions.OpenApplicationEditorAction");
		openListener.run(null);
	}

}