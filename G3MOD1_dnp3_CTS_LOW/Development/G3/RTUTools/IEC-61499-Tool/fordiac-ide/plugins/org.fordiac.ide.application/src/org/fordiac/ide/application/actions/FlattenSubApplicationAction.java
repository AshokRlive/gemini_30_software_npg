/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.actions;

import java.util.List;

import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.application.commands.FlattenSubAppCommand;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;
import org.fordiac.ide.model.ui.Diagram;

/**
 * Action to create and execute (if possible) an FlattenSubAppCommand.
 */
public class FlattenSubApplicationAction extends SelectionAction {

	/** The Constant ID. */
	public static final String ID = "FlattenSubApplicationAction"; //$NON-NLS-1$

	/** The sub app. */
	private SubAppForFBNetworkEditPart subApp;

	/**
	 * Instantiates a new flatten sub application action.
	 * 
	 * @param part the part
	 */
	public FlattenSubApplicationAction(IWorkbenchPart part) {
		super(part);
		setId(ID);
		setText(Messages.FlattenSubApplicationAction_FlattenSubapplicationText);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
	 */
	protected boolean calculateEnabled() {
		// TODO check whether all conditions are required
		if (getSelectedObjects().isEmpty())
			return false;
		List<?> parts = getSelectedObjects();
		if (parts.size() == 1
				&& parts.get(0) instanceof SubAppForFBNetworkEditPart){
			if(null == ((SubAppForFBNetworkEditPart)parts.get(0)).getCastedModel().getSubApp().getPaletteEntry()){
				//flatten is currently only possible for untyped sub apps
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	public void run() {
		List<?> parts = getSelectedObjects();
		if (parts.size() == 1
				&& parts.get(0) instanceof SubAppForFBNetworkEditPart) {
			subApp = (SubAppForFBNetworkEditPart) parts.get(0);
			FlattenSubAppCommand cmd = new FlattenSubAppCommand();
			cmd.setSubApp(subApp);
			cmd.setParentNetwork((Diagram) subApp.getCastedModel().eContainer());
			getCommandStack().execute(cmd);
		}
	}
}
