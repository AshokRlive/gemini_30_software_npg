/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editors;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.ui.palette.PaletteViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PatternFilter;
import org.eclipse.ui.navigator.CommonViewer;
import org.eclipse.ui.navigator.CommonViewerSorter;
import org.eclipse.ui.navigator.INavigatorContentService;
import org.eclipse.ui.navigator.INavigatorFilterService;
import org.eclipse.ui.navigator.NavigatorContentServiceFactory;
import org.fordiac.ide.fbt.typemanagement.util.TypeListPatternFilter;
import org.fordiac.ide.typelibrary.TypeLibrary;
import org.fordiac.ide.typelibrary.TypeLibraryTags;

public class FBPaletteViewer extends PaletteViewer {
	final static String FBPaletteNavigatorId = "org.fordiac.ide.fbpaletteviewer"; //$NON-NLS-N$

	private CommonViewer commonViewer;
	private PatternFilter patternFilter = null;
	
	public void createTypeLibTreeControl(Composite parent,
			IProject project) {

		Composite container = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout(1, false);
		layout.marginLeft = 0;
		layout.marginRight = 0;
		layout.marginBottom = 0;
		layout.marginTop = 0;
		container.setLayout(layout);

		final Text text = new Text(container, SWT.SEARCH | SWT.ICON_CANCEL);

		text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		text.addSelectionListener(new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent e) {
				if (e.detail == SWT.CANCEL) {
					setSearchFilter("");
				} else {
					setSearchFilter(text.getText());
				}
			}

		});
		text.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				setSearchFilter(text.getText());
			}
		});

		createCommonViewer(container, project);

		setControl(container);
	}

	private void createCommonViewer(Composite container, IProject project) {
		commonViewer = new CommonViewer(FBPaletteNavigatorId,
				container, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);

		INavigatorContentService contentService = NavigatorContentServiceFactory.INSTANCE
				.createContentService(FBPaletteNavigatorId, commonViewer);

		contentService.createCommonContentProvider();
		contentService.createCommonLabelProvider();
		
		INavigatorFilterService filterService = commonViewer
				.getNavigatorContentService().getFilterService();
		ViewerFilter[] visibleFilters = filterService.getVisibleFilters(true);
		for (int i = 0; i < visibleFilters.length; i++) {
			commonViewer.addFilter(visibleFilters[i]);
		}

		commonViewer.setSorter(new CommonViewerSorter());
		commonViewer.addFilter(new TypeListPatternFilter());

		
		if(project.getName().equals(TypeLibraryTags.TOOL_LIBRARY_PROJECT_NAME)){
			commonViewer.setInput(TypeLibrary.getInstance().getToolLibFolder());
		}else{
			commonViewer.setInput(project);			
		}

		GridData fillBoth = new GridData();
		fillBoth.horizontalAlignment = GridData.FILL;
		fillBoth.grabExcessHorizontalSpace = true;
		fillBoth.verticalAlignment = GridData.FILL;
		fillBoth.grabExcessVerticalSpace = true;
		commonViewer.getControl().setLayoutData(fillBoth);
	}

	private void setSearchFilter(String string) {
		if (patternFilter == null)	{
			patternFilter = new TypeListPatternFilter();
			commonViewer.addFilter(patternFilter);
		}		
		patternFilter.setPattern(string);
		commonViewer.refresh(false);
	}

	@Override
	protected void hookControl() {
		// do nothing here! Especially do not call super.hookControl!
	}
}
