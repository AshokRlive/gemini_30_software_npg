/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIResourceEditor;

/**
 * The Class SubAppTooltipFigure.
 */
public class SubAppTooltipFigure extends Figure {

	/**
	 * Instantiates a tooltip Figure for a SubApp.
	 * 
	 * @param subApp the sub app
	 */
	public SubAppTooltipFigure(final SubApp subApp) {
		setLayoutManager(new GridLayout());

		Label instanceNameLabel = new Label(subApp.getName());
		add(instanceNameLabel);
		setConstraint(instanceNameLabel, new GridData(PositionConstants.CENTER,
				PositionConstants.MIDDLE, true, true));

		SubAppNetwork fbNetwork = (SubAppNetwork) subApp.eContainer();
		if (fbNetwork instanceof FBNetwork) {

			Application app = ((FBNetwork) fbNetwork).getApplication();
			if (app != null
					&& app.eContainer() instanceof org.fordiac.ide.model.libraryElement.AutomationSystem) {

				Label system = new Label(
						Messages.SubAppTooltipFigure_LABEL_System
								+ ((AutomationSystem) app.eContainer())
										.getName());
				add(system);

				setConstraint(system, new GridData(PositionConstants.CENTER,
						PositionConstants.MIDDLE, true, true));
			}
			if (app != null) {
				Label application = new Label(
						Messages.SubAppTooltipFigure_LABEL_Application
								+ app.getName());
				add(application);

				setConstraint(application, new GridData(
						PositionConstants.CENTER, PositionConstants.MIDDLE,
						true, true));

			}

		}

	}

	/**
	 * Instantiates a tooltip Figure for a SubApp.
	 * 
	 * @param subAppView the subAppView
	 */
	public SubAppTooltipFigure(final SubAppView subAppView) {
		this(subAppView.getSubApp());
		MappedSubAppView mappedsubAppView = null;

		if (subAppView.getMappedSubApp() != null) {
			mappedsubAppView = subAppView.getMappedSubApp();

		}
		if (mappedsubAppView != null) {
			UIResourceEditor uiResEditor = (UIResourceEditor) mappedsubAppView
					.eContainer();
			if (uiResEditor != null) {
				Resource res = uiResEditor.getResourceElement();
				if (res != null && res.getName() != null) {
					Label resourceLabel = new Label(
							Messages.SubAppTooltipFigure_LABEL_MappedTo
									+ res.getName());
					add(resourceLabel);
					setConstraint(resourceLabel, new GridData(
							PositionConstants.CENTER, PositionConstants.MIDDLE,
							true, true));
				}

			}
		}
	}

}
