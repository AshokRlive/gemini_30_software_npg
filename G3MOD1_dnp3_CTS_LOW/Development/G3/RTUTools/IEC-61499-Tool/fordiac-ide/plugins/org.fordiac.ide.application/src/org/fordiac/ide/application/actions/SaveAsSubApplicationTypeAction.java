/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.actions;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.application.wizards.SaveAsSubappWizard;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.ui.SubAppView;

public class SaveAsSubApplicationTypeAction extends SelectionAction {
	

	/** The Constant ID. */
	public static final String ID = "SaveAsSubApplicationTypeAction"; //$NON-NLS-1$
	

	public SaveAsSubApplicationTypeAction(IWorkbenchPart part) {
		super(part);
		setId(ID);
		setText(Messages.SaveAsSubApplicationTypeAction_SaveAsSubApplicationTypeText);
	}

	@Override
	protected boolean calculateEnabled() {
		
		if(!getSelectedObjects().isEmpty()){
			for (Object selected : getSelectedObjects()) {
				if(selected instanceof EditPart){
					if( ((EditPart)selected).getModel() instanceof SubAppView){
						if(null != ((SubAppView)((EditPart)selected).getModel()).getSubApp().getPaletteEntry()){
							//a typed subapplication has been selected
							return false;
						}
					}else{
						// a non subapplication has been selected
						return false;
					}
				}else{
					return false;
				}
			}
			//we only have subapps selected
			return true;
		}		
		return false;
	}

	@Override
	public void run() {
		for (Object selected : getSelectedObjects()) {
			EditPart ep = (EditPart)selected;
			SubAppView subAppView = (SubAppView)ep.getModel();
			
			if(!checkContainedSubApps(subAppView)){
				showInformationDialog(subAppView);
			}else{
				invokeSaveWizard(subAppView);
			}			
		}
	}

	private void showInformationDialog(SubAppView subAppView) {
		//TODO consider to show the name of the subapplication in the error dialog
		MessageDialog.openError(getWorkbenchPart().getSite().getShell(), Messages.SaveAsSubApplicationTypeAction_UntypedSubappError,
				Messages.SaveAsSubApplicationTypeAction_UntypedSubappErrorDescription);		
	}
	
	private void invokeSaveWizard(SubAppView subAppView) {
		SaveAsSubappWizard wizard = new SaveAsSubappWizard(subAppView);
		
		WizardDialog dialog = new WizardDialog(getWorkbenchPart().getSite().getShell(), wizard);
		dialog.create();
		dialog.open();
	}

	private Boolean checkContainedSubApps(SubAppView subAppView) {
		for (SubApp subApp : subAppView.getSubApp().getSubAppNetwork().getSubApps()) {
			if(null == subApp.getPaletteEntry()){
				//we have an untyped subapplication 
				return false;
			}
		}
		return true;
	}

}
