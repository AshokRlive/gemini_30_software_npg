/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.ui.actions.DeleteAction;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.editparts.ConnectionEditPart;

/** This special delete object will sort the commands that way that first the connections are added and then the other objects. 
 * 
 */
public class DeleteFBNetworkAction extends DeleteAction {

	
	public DeleteFBNetworkAction(IWorkbenchPart part) {
		super(part);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Command createDeleteCommand(List objects) {
		if (objects.isEmpty())
			return null;
		if (!(objects.get(0) instanceof EditPart))
			return null;

		ArrayList<EditPart> list = new ArrayList<EditPart>();

		//Resort list such that the connects are before any other edit parts
		for (Object object : objects) {
			if(object instanceof ConnectionEditPart){
				list.add((EditPart)object);
			}
		}
		
		for (Object object : objects) {
			if(!(object instanceof ConnectionEditPart)){
				list.add((EditPart)object);				
			}
		}	

		return super.createDeleteCommand(list);
	}

	
}
