/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;

/**
 * The Class ConnectionMoveCMD.
 */
public class ConnectionMoveCMD extends Command {

	/** The connection. */
	private final Connection connection;

	/** The network. */
	private final SubAppNetwork network;

	/** The parent. */
	private SubAppNetwork parent;

	/**
	 * Instantiates a new connection move cmd.
	 * 
	 * @param connection the connection
	 * @param dest the dest
	 */
	public ConnectionMoveCMD(final Connection connection,
			final SubAppNetwork dest) {
		this.connection = connection;
		this.network = dest;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		if (connection.eContainer() instanceof SubAppNetwork) {
			parent = (SubAppNetwork) connection.eContainer();
		}
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		if (parent != null) {
			if (connection instanceof DataConnection) {
				DataConnection dataCon = (DataConnection) connection;
				if (!parent.getDataConnections().contains(dataCon)) {
					parent.getDataConnections().add(dataCon);
				}
			}
			if (connection instanceof EventConnection) {
				EventConnection eventCon = (EventConnection) connection;
				if (!parent.getEventConnections().contains(eventCon)) {
					parent.getEventConnections().add(eventCon);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		if (network != null) {
			if (connection instanceof DataConnection) {
				DataConnection dataCon = (DataConnection) connection;
				if (!network.getDataConnections().contains(dataCon)) {
					network.getDataConnections().add(dataCon);
				}
			}
			if (connection instanceof EventConnection) {
				EventConnection eventCon = (EventConnection) connection;
				if (!network.getEventConnections().contains(eventCon)) {
					network.getEventConnections().add(eventCon);
				}
			}
		}

	}

}