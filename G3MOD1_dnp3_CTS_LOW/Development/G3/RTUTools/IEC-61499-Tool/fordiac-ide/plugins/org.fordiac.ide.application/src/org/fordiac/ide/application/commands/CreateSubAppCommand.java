/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.ArrayList;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.UiFactory;

/**
 * Creates a SubApplication from the selected function blocks and sub
 * applications.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at) FIXME __gebenh use
 * visibility preference for interfaceelements?????
 */
public class CreateSubAppCommand extends Command {

	/** The pos. */
	private Position pos;

	/** The parent network. */
	private Diagram parentNetwork;

	/** The editor. */
	private IEditorPart editor;

	/** The fbs. */
	private ArrayList<FBView> fbs;

	/** The sub apps. */
	private ArrayList<SubAppView> subApps;

	/** The sub app view. */
	private SubAppView subAppView;

	/** The ui sub app network. */
	private UISubAppNetwork uiSubAppNetwork;

	/** The sub app network. */
	private SubAppNetwork subAppNetwork;
	
	/** Command to add fbs and subapps */
	private AddFBToSubAppCommand addCmd;

	/** The input for reopening subApp. */
	private IEditorInput input = null;

	/**
	 * Gets the fbs.
	 * 
	 * @return the fbs
	 */
	public ArrayList<FBView> getFbs() {
		return fbs;
	}

	/**
	 * Sets the fbs.
	 * 
	 * @param fbs the new fbs
	 */	
	public void setFbs(final ArrayList<FBView> fbs) {
		this.fbs = fbs;
	}

	/**
	 * Gets the sub apps.
	 * 
	 * @return the sub apps
	 */
	public ArrayList<SubAppView> getSubApps() {
		return subApps;
	}

	/**
	 * Sets the sub apps.
	 * 
	 * @param subApps the new sub apps
	 */
	public void setSubApps(final ArrayList<SubAppView> subApps) {
		this.subApps = subApps;
	}
	
	/**
	 * Gets the SubApp view.
	 * 
	 * @return the created SubAppView
	 */
	public SubAppView getSubAppView() {
		return subAppView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(ApplicationPlugin.getDefault()
				.getCurrentActiveEditor());

	}

	/**
	 * Instantiates a new creates the sub app command.
	 */
	public CreateSubAppCommand() {
		super(Messages.CreateSubAppCommand_LABELCreateSubAppCommand);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = ApplicationPlugin.getDefault().getCurrentActiveEditor();

		// create new subapp object
		subAppView = UiFactory.eINSTANCE.createSubAppView();
		uiSubAppNetwork = UiFactory.eINSTANCE.createUISubAppNetwork();
		subAppView.setUiSubAppNetwork(uiSubAppNetwork);

		SubApp subApp = LibraryElementFactory.eINSTANCE.createSubApp();
		subAppNetwork = LibraryElementFactory.eINSTANCE.createSubAppNetwork();

		subApp.setSubAppNetwork(subAppNetwork);
		subAppView.setSubApp(subApp);

		uiSubAppNetwork.setSubAppNetwork(subAppNetwork);
		subApp.setName(getNetworkUniqueSubAppName("SubApp")); //$NON-NLS-1$
		subAppNetwork.setName(subApp.getName());
		subAppView.setPosition(pos);
		subApp.setX(Integer.toString(pos.getX()));
		subApp.setY(Integer.toString(pos.getY()));

		// set root application (fbnetwork)
		if (parentNetwork instanceof UIFBNetwork) {
			uiSubAppNetwork.setRootApplication((UIFBNetwork) parentNetwork);
		} else if (parentNetwork instanceof UISubAppNetwork) {
			uiSubAppNetwork.setRootApplication(((UISubAppNetwork) parentNetwork)
					.getRootApplication());
		}

		// Add subapp to parent
		parentNetwork.getChildren().add(subAppView);
		if (parentNetwork instanceof UIFBNetwork) {
			((UIFBNetwork) parentNetwork).getFbNetwork().getSubApps().add(
					subAppView.getSubApp());
		} else if (parentNetwork instanceof UISubAppNetwork) {
			((UISubAppNetwork) parentNetwork).getSubAppNetwork().getSubApps().add(
					subAppView.getSubApp());
		}

		String fileName = subApp.getId() + ".xml"; //$NON-NLS-1$
		uiSubAppNetwork.setFileName(fileName);
		// SystemManager.getInstance().saveDiagram(uiSubAppNetwork, system,
		// fileName);
		
		// Create a new command and add fbs and subapps		
		addCmd = new AddFBToSubAppCommand(subAppView, fbs, subApps);
		addCmd.execute();
	}
	
	private String getNetworkUniqueSubAppName(String desiredName){
		
		String temp = desiredName;
		if(parentNetwork != null){
			if (parentNetwork instanceof UIFBNetwork) {
				FBNetwork parentNet = ((UIFBNetwork) parentNetwork).getFbNetwork();
				temp = NameRepository.getInstance().getNetworkUniqueSubApplicationName(parentNet, desiredName);
				
			} else if (parentNetwork instanceof UISubAppNetwork) {
				SubAppNetwork parentNet = ((UISubAppNetwork) parentNetwork).getSubAppNetwork();
				temp = NameRepository.getInstance().getNetworkUniqueSubApplicationName(parentNet, desiredName);
			}
		}
		
		return temp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		// undo add of fbs and subapps
		addCmd.undo();

		// undo creation of subapp
		parentNetwork.getChildren().remove(subAppView);
		parentNetwork.getNetwork().getSubApps().remove(subAppView.getSubApp());

		closeOpenedSubApp();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		// redo creation of subapp
		parentNetwork.getChildren().add(subAppView);
		parentNetwork.getNetwork().getSubApps().add(subAppView.getSubApp());
		
		// redo add of fbs and subapps
		addCmd.redo();

		openClosedEditor();
	}

	/**
	 * Close opened sub app.
	 */
	private void closeOpenedSubApp() {
		input = CommandUtil.closeOpenedSubApp(uiSubAppNetwork);
	}

	/**
	 * Open closed editor.
	 */
	private void openClosedEditor() {
		if (input != null) {
			CommandUtil.openSubAppEditor(input);
		}
	}

	/**
	 * Gets the position.
	 * 
	 * @return the position
	 */
	public Position getPosition() {
		return pos;
	}

	/**
	 * Sets the position.
	 * 
	 * @param pos the new position
	 */
	public void setPosition(final Position pos) {
		this.pos = pos;
	}

	/**
	 * Sets the parent network.
	 * 
	 * @param parentNetwork the new parent network
	 */
	public void setParentNetwork(final Diagram parentNetwork) {
		this.parentNetwork = parentNetwork;

	}

}
