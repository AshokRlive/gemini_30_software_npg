/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;
import org.fordiac.ide.application.utilities.ApplicationUIFBNetworkManager;
import org.fordiac.ide.model.Palette.FBTypePaletteEntry;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.AdapterFB;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.Position;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.util.commands.DeleteConnectionCommand;
import org.fordiac.ide.util.commands.UnmapCommand;

/**
 * UpdateFBTypeCommand triggers an update of the type for an FB instance
 *  
 * @author Filip Andr�n (Filip.Andren@ait.ac.at)
 */
public class UpdateFBTypeCommand extends Command {
	
	/** The FB view */
	private FBView fbView;
	
	/** The copied FB view */
	private FBView copiedFBView;
	
	private Diagram diagram;
	
	/** The FB type if not null this entry should be used */
	private FBTypePaletteEntry entry;
	
		
	private List<DeleteConnectionCommand> deleteConnCmds = new ArrayList<DeleteConnectionCommand>();
	private List<AbstractConnectionCreateCommand> connCreateCmds = new ArrayList<AbstractConnectionCreateCommand>();
	
	private MapToCommand mapCmd = null;
	private UnmapCommand unmapCmd = null;
	
	
	public UpdateFBTypeCommand(FBView fbView) {
		this.fbView = fbView;
		diagram = ((Diagram) fbView.eContainer());
	}
	
	public UpdateFBTypeCommand(FBView fbView, PaletteEntry entry) {
		this.fbView = fbView;
		diagram = ((Diagram) fbView.eContainer());
		if(entry instanceof FBTypePaletteEntry){
			this.entry = (FBTypePaletteEntry)entry;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		UIResourceEditor resourceEditor = null;
		if (fbView.getMappedFB() != null) {
			resourceEditor = (UIResourceEditor)fbView.getMappedFB().eContainer();
			unmapCmd = new UnmapCommand(fbView.getMappedFB());
			unmapCmd.execute();
		}
		
		// Create new FB		
		copyFBView(fbView);
		
		// Find connections which should be reconnected
		List<ConnectionView> reConnections = new ArrayList<ConnectionView>();
		for (InterfaceElementView elView : fbView.getInterfaceElements()) {
			reConnections.addAll(elView.getInConnections());
			reConnections.addAll(elView.getOutConnections());		
		}
		for (ConnectionView connView : reConnections) {
			doReconnect(connView, findUpdatedInterfaceElementView(copiedFBView, connView.getSource()),
					findUpdatedInterfaceElementView(copiedFBView, connView.getDestination()));
		}
		
		// Change name
		diagram.getChildren().remove(copiedFBView);
		String name = fbView.getFb().getName();
		copiedFBView.getFb().setName(name);	
		diagram.getChildren().add(copiedFBView);

				
		// Map FB
		if (resourceEditor != null) {
			mapCmd = new MapToCommand(copiedFBView, resourceEditor);
			if (mapCmd.canExecute()) {
				mapCmd.execute();
			}
		}
	}
	

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		if (unmapCmd != null) {
			unmapCmd.redo();
		}

		for (DeleteConnectionCommand cmd : deleteConnCmds) {
			cmd.redo();
		}

		replaceFBViews(fbView, copiedFBView);
		
		for (AbstractConnectionCreateCommand cmd : connCreateCmds) {
			cmd.redo();
		}

		if (mapCmd != null) {
			mapCmd.redo();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		if (mapCmd != null) {
			mapCmd.undo();
		}
		
		for (AbstractConnectionCreateCommand cmd : connCreateCmds) {
			cmd.undo();
		}
		
		replaceFBViews(copiedFBView, fbView);

		for (DeleteConnectionCommand cmd : deleteConnCmds) {
			cmd.undo();
		}
		
		if (unmapCmd != null) {
			unmapCmd.undo();
		}
		
	}

	private InterfaceElementView findUpdatedInterfaceElementView(FBView copiedFBView, InterfaceElementView origView) {
		if (origView == null) {
			// this happens when a connection has already been reconnected,
			// i.e., a connection from an output to an input of the same FB
			return null;
		}
		InterfaceElementView retval = origView;
		if (((INamedElement)origView.getIInterfaceElement().eContainer().eContainer()).getName().equals(fbView.getFb().getName())) {
			// origView is an interface of the original FB => find same interface on copied FB
			for (InterfaceElementView interfaceElement : copiedFBView.getInterfaceElements()) {
				if(interfaceElement.getIInterfaceElement().getName().equals(origView.getIInterfaceElement().getName())){
					// interface exist on new type
					retval = interfaceElement;
					break;
				}
			}
		}
		if (retval.eContainer().equals(fbView)) {
			return null;
		}		
		return retval;
	}
	
	private void doReconnect(ConnectionView oldConn, InterfaceElementView source, InterfaceElementView dest) {
		//the connection may be allready in our list if source and dest are on our FB
		if(!isInDeleteConnList(oldConn)){		
			//we have to delete the old connection in all cases
			Diagram diagram = (Diagram)oldConn.eContainer();
			DeleteConnectionCommand cmd = new DeleteConnectionCommand(oldConn);
			cmd.execute();
			deleteConnCmds.add(cmd);
	
			if (source != null && dest != null) {			
				// if source or dest is null it means that an interface element is not available any more
				AbstractConnectionCreateCommand dccc = null;
				
				if(oldConn.getConnectionElement() instanceof EventConnection){
					dccc = new EventConnectionCreateCommand();
				}
				else if(oldConn.getConnectionElement() instanceof DataConnection){
					dccc = new DataConnectionCreateCommand();
				}
				if(null != dccc){
					dccc.setSourceView(source);
					dccc.setDestinationView(dest);
					dccc.setParent(diagram);
					if (dccc.canExecute()) {
						dccc.execute();
						connCreateCmds.add(dccc);
					}
				}
			}
		}
	}

	private boolean isInDeleteConnList(ConnectionView conn) {
		for (DeleteConnectionCommand cmd : deleteConnCmds) {
			if(cmd.getConnectionView().equals(conn)){
				return true;
			}
		}
		return false;
	}

	private void copyFBView(FBView fbView) {		
		FB copiedFB = copyFB(fbView.getFb());
		
		//Currently the FB model will not have an up to date position when the FB has been moved therefore we  have to set it here
		copiedFB.getPosition().setX(fbView.getPosition().getX());
		copiedFB.getPosition().setY(fbView.getPosition().getY());
		
		copiedFBView = ApplicationUIFBNetworkManager.createFBView(copiedFB, 
				diagram.getFunctionBlockNetwork());
		
		replaceFBViews(fbView, copiedFBView);
	}

	private void replaceFBViews(FBView oldFB, FBView newFB) {
		diagram.getFunctionBlockNetwork().getFBs().remove(oldFB.getFb());
		diagram.getChildren().add(newFB);
		diagram.getFunctionBlockNetwork().getFBs().add(newFB.getFb());
	}

	private FB copyFB(FB srcFB) {
		FB copiedFB = createCopiedFBEntry(srcFB);
		
		copiedFB.setFbtPath(copiedFB.getPaletteEntry().getProjectRelativeTypePath());
		copiedFB.setId(srcFB.getId());
		copiedFB.setInterface((InterfaceList) EcoreUtil.copy(copiedFB.getFBType().getInterfaceList()));
		copiedFB.setIdentifier(srcFB.isIdentifier());
		copiedFB.setResourceFB(srcFB.isResourceFB());
		copiedFB.setParentCompositeFBType(srcFB.getParentCompositeFBType());
		copiedFB.setName(srcFB.getName());

		// Fix for similar issue as reported in [issue#933] CompositeEditor - setting constant values not possible
		// initialize the value element of all vardeclarations which have no parameters 
		for (VarDeclaration var : copiedFB.getInterface().getInputVars()) {
			if (var.getValue() == null) {
					var.setValue(LibraryElementFactory.eINSTANCE.createValue());
			}
		}
		
		Position pos = EcoreUtil.copy(srcFB.getPosition());
		copiedFB.setPosition(pos);	
		createValues(copiedFB);
		
		pasteParams(srcFB, copiedFB);
		
		return copiedFB;
	}

	protected FB createCopiedFBEntry(FB srcFB) {
		FB fb = null;
		
		if(srcFB instanceof AdapterFB){
			AdapterFB aFB = LibraryElementFactory.eINSTANCE.createAdapterFB();
			aFB.setPlug(((AdapterFB)srcFB).isPlug());
			fb = aFB;				
		}
		else{
			fb = LibraryElementFactory.eINSTANCE.createFB();
		}
		
		//Entry handling is here to allow the reuse of this class also for adapter updates
		if(null == entry){
			fb.setPaletteEntry(srcFB.getPaletteEntry());
		}else{
			fb.setPaletteEntry(entry);
		}		
		return fb; 
	}
	
	private void pasteParams(FB src, FB dst) {
		InterfaceList interfaceList = src.getInterface();
		if (interfaceList != null) {
			for (VarDeclaration varDecl : interfaceList .getInputVars()) {
				if (dst.getInterfaceElement(varDecl.getName()) != null) {
					// interface exist on new type
					if (varDecl.getInputConnections().size() == 0) {
						Value value = varDecl.getValue();
						if (value != null && value.getValue() != null) {
							Value newValue = LibraryElementFactory.eINSTANCE.createValue();
							newValue.setValue(value.getValue());
							dst.getInterfaceElement(varDecl.getName()).setValue(newValue);
						}
					}
				}
			}
		}

	}
	
	protected void createValues(FB copiedFB) {
		for (IInterfaceElement element : copiedFB.getInterface().getInputVars()) {
			Value value = LibraryElementFactory.eINSTANCE.createValue();
			element.setValue(value);

		}
	}
}
