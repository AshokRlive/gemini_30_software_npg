/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editparts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.ui.parts.GraphicalEditor;
import org.fordiac.ide.gef.editparts.Abstract4diacEditPartFactory;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UISubAppNetwork;

/**
 * A factory for creating new EditParts.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ElementEditPartFactory extends Abstract4diacEditPartFactory {

	protected final ZoomManager zoomManager;

	public ElementEditPartFactory(GraphicalEditor editor, ZoomManager zoomManager) {
		super(editor);
		this.zoomManager = zoomManager;
	}
	
	/**
	 * Maps an object to an EditPart.
	 * 
	 * @throws RuntimeException
	 *           if no match was found (programming error)
	 */
	protected EditPart getPartForElement(final EditPart context,
			final Object modelElement) {
		EditPart part = null;

		if (modelElement instanceof UIFBNetwork) {
			part = new FBNetworkEditPart();
		}
		else if (modelElement instanceof FBView) {
			part = new FBEditPart(zoomManager);
		}
		else if (modelElement instanceof ConnectionView) {
			part = new ConnectionEditPart();
		}
		else if (modelElement instanceof SubAppView) {
			part = new SubAppForFBNetworkEditPart();
		}
		else if (modelElement instanceof SubAppInterfaceElementView) {
			if(null != ((SubAppInterfaceElementView)modelElement).eContainer() && (((SubAppInterfaceElementView)modelElement).eContainer() instanceof SubAppView) && 
					null != ((SubAppView)((SubAppInterfaceElementView)modelElement).eContainer()).getSubApp()){
				if(null != ((SubAppView)((SubAppInterfaceElementView)modelElement).eContainer()).getSubApp().getPaletteEntry()){
					return new InterfaceEditPart(){
						//TODO with new sub app design this may not be necessary in the future
						@Override
						protected String getLabelText(){
							if (getCastedModel().getIInterfaceElement() != null) {
								return getCastedModel().getIInterfaceElement().getName();
							}
							return "";
						}
					};
				}
			}
			return new InterfaceEditPart();
		}
		else if (modelElement instanceof UISubAppNetwork) {
			part = new UISubAppNetworkEditPart();
		} 
		else  if (modelElement instanceof InterfaceElementView) {   
			part = new InterfaceEditPart();
		}
		else {
			throw createEditpartCreationException(modelElement); //$NON-NLS-1$
		}
		return part;
	}

	
}
