package org.fordiac.ide.application;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.application.editparts.ConnectionEditPart;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.FBView;

/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

public class FocusOnPredecessor implements IObjectActionDelegate {

	private FBEditPart selectedFB = null;
	IWorkbenchPart workbench = null;
	
	public FocusOnPredecessor() {
	}

	@Override
	public void run(IAction action) {
		if (selectedFB != null) {
			FBView fbView = selectedFB.getCastedModel();
			FB fb = fbView.getFb();
			ArrayList<FB> fbsToHighlight = new ArrayList<FB>();
			ArrayList<Connection> connectionsToHighlight = new ArrayList<Connection>();
			fbsToHighlight.add(fb);

			addPredecessorFBs(fb, fbsToHighlight, connectionsToHighlight);
			
			if (workbench instanceof FBNetworkEditor) {
				GraphicalViewer viewer = ((FBNetworkEditor) workbench).getViewer();
				Map<?, ?> map = viewer.getEditPartRegistry();
				for (Object obj : map.keySet()) {
					Object editPartAsObject = map.get(obj);
					if (obj instanceof ConnectionView) {
						if (connectionsToHighlight.contains(((ConnectionView) obj).getConnectionElement())) {
							if (editPartAsObject instanceof ConnectionEditPart) {
								// if previously the transparency was set to a value lower than 255
								((ConnectionEditPart) editPartAsObject).setTransparency(255);
							}
						} else {
							if (editPartAsObject instanceof ConnectionEditPart) {
								((ConnectionEditPart) editPartAsObject).setTransparency(50);
							}
						}
					}
					if (obj instanceof FBView) {
						if (editPartAsObject != null && fbsToHighlight.contains(((FBView) obj).getFb())) {
							if (editPartAsObject instanceof AbstractViewEditPart) {
								// if previously the transparency was set to a value lower than 255
								((AbstractViewEditPart) editPartAsObject).setTransparency(255);
							}
						} else {
							if (editPartAsObject instanceof AbstractViewEditPart) {
								((AbstractViewEditPart) editPartAsObject).setTransparency(50);
							}
						}
					}
				}
			}
			
		}
	}

	private void addPredecessorFBs(FB fb, ArrayList<FB> fbsToHighlight, ArrayList<Connection> connectionsToHighlight) {
		List<VarDeclaration> inputs = fb.getInterface().getInputVars();
		for (VarDeclaration varDeclaration : inputs) {
			for (Connection con : varDeclaration.getInputConnections()) {
				IInterfaceElement source = con.getIInterfaceElementSource();
				if (source != null) {
					EObject sourceContainer = source.eContainer();
					if (sourceContainer instanceof InterfaceList) {
						EObject sourceFBEObject = sourceContainer.eContainer();
						if (sourceFBEObject instanceof FB) {
							connectionsToHighlight.add(con);
							if (!fbsToHighlight.contains(sourceFBEObject)) {
								fbsToHighlight.add((FB)sourceFBEObject);
								addPredecessorFBs((FB)sourceFBEObject, fbsToHighlight, connectionsToHighlight);
							}
						}
					}
				}
			}
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			if (((IStructuredSelection) selection).getFirstElement() instanceof FBEditPart) {
				selectedFB  = (FBEditPart)((IStructuredSelection) selection).getFirstElement();
			}
		}

	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		this.workbench = targetPart;
	}

}
