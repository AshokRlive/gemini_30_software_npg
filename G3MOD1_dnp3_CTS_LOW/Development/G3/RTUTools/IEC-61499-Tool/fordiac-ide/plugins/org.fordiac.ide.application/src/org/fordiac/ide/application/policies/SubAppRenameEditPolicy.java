/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.DirectEditRequest;
import org.fordiac.ide.application.commands.SubAppRenameCommand;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;
import org.fordiac.ide.gef.policies.AbstractViewRenameEditPolicy;

public class SubAppRenameEditPolicy extends AbstractViewRenameEditPolicy {

	@Override
	protected Command getDirectEditCommand(DirectEditRequest request) {
		if (getHost() instanceof SubAppForFBNetworkEditPart) {
			SubAppForFBNetworkEditPart subAppEditPart = (SubAppForFBNetworkEditPart) getHost();
            return new SubAppRenameCommand(subAppEditPart.getCastedModel().getSubApp(), (String) request.getCellEditor().getValue());
		}
		return null;
	}

}
