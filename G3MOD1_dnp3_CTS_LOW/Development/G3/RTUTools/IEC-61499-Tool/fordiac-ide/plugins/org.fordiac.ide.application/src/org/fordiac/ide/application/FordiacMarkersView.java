/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.util.OpenStrategy;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.OpenAndLinkWithEditorHelper;
import org.eclipse.ui.views.markers.MarkerSupportView;
import org.fordiac.ide.model.libraryElement.FB;

public class FordiacMarkersView extends MarkerSupportView {

	public FordiacMarkersView() {
		super("org.fordiac.ide.application.fordiacmarkerContentGenerator");
	}

	@SuppressWarnings("restriction")
	@Override
	public void createPartControl(Composite parent) {
		// TODO find a better way, as super.createPartControl normally is
		// restricted for access;
		super.createPartControl(parent);
		Object provider = getSite().getSelectionProvider();
		if (provider instanceof StructuredViewer) {

			new OpenAndLinkWithEditorHelper(
					(StructuredViewer)provider) {
				/*
				 * (non-Javadoc)
				 * 
				 * @see
				 * org.eclipse.ui.OpenAndLinkWithEditorHelper#activate(org.eclipse
				 * .jface.viewers.ISelection )
				 */
				protected void activate(ISelection selection) {
					final int currentMode = OpenStrategy.getOpenMethod();
					try {
						OpenStrategy.setOpenMethod(OpenStrategy.DOUBLE_CLICK);
						openMySelectedMarkers();
					} finally {
						OpenStrategy.setOpenMethod(currentMode);
					}
				}

				/*
				 * (non-Javadoc)
				 * 
				 * @see
				 * org.eclipse.ui.OpenAndLinkWithEditorHelper#linkToEditor(org
				 * .eclipse .jface.viewers .ISelection)
				 */
				protected void linkToEditor(ISelection selection) {
					// Not supported by this part
				}

				/*
				 * (non-Javadoc)
				 * 
				 * @see
				 * org.eclipse.ui.OpenAndLinkWithEditorHelper#open(org.eclipse
				 * .jface .viewers.ISelection, boolean)
				 */
				protected void open(ISelection selection, boolean activate) {
					openMySelectedMarkers();
				}
			};
		}
	}

	
	/**
	 * Open the selected markers
	 */
	void openMySelectedMarkers() {
		@SuppressWarnings("restriction")
		IMarker[] markers = getSelectedMarkers();
		for (int i = 0; i < markers.length; i++) {
			IMarker marker = markers[i];
			Object markerObject;
			try {
				markerObject = marker.getAttribute("org.fordiac.ide.application.marker.fb");
				if (markerObject instanceof FB) {
					System.out.println("should open markerObject");
				}
			} catch (CoreException e) {
				ApplicationPlugin.getDefault().logError(e.getMessage(), e);
			}
		}
	}

}
