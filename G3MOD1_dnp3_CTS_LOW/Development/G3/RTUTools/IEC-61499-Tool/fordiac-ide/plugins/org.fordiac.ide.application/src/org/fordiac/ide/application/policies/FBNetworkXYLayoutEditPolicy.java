/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.policies;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.fordiac.ide.application.commands.CreateSubAppInstanceCommand;
import org.fordiac.ide.application.commands.FBCreateCommand;
import org.fordiac.ide.application.commands.ListFBCreateCommand;
import org.fordiac.ide.gef.commands.ViewSetPositionCommand;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.gef.policies.ModifiedNonResizeableEditPolicy;
import org.fordiac.ide.model.Palette.SubApplicationTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.dnd.TransferDataSelectionOfFb;
import org.fordiac.systemmanagement.SystemManager;

/**
 * An EditPolicy which returns commands e.g. CreateCommand or
 * ChangeConstraintCommand.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class FBNetworkXYLayoutEditPolicy extends XYLayoutEditPolicy {

	@Override
	protected EditPolicy createChildEditPolicy(EditPart child) {
		return new ModifiedNonResizeableEditPolicy();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#createAddCommand
	 * (org.eclipse.gef.EditPart, java.lang.Object)
	 */
	@Override
	protected Command createAddCommand(final EditPart child,
			final Object constraint) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.LayoutEditPolicy#getDeleteDependantCommand
	 * (org.eclipse.gef.Request)
	 */
	@Override
	protected Command getDeleteDependantCommand(final Request request) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ConstrainedLayoutEditPolicy#createChangeConstraintCommand(ChangeBoundsRequest
	 * , EditPart, Object)
	 */
	@Override
	protected Command createChangeConstraintCommand(
			final ChangeBoundsRequest request, final EditPart child,
			final Object constraint) {
		// return a command that can move a "ViewEditPart"
		if (child instanceof AbstractViewEditPart
				&& constraint instanceof Rectangle) {
			AbstractViewEditPart temp = (AbstractViewEditPart) child;
			if(temp.getModel() instanceof View){
				return new ViewSetPositionCommand((View) temp.getModel(), request,
						(Rectangle) constraint);
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ConstrainedLayoutEditPolicy#createChangeConstraintCommand(EditPart,
	 * Object)
	 */
	@Override
	protected Command createChangeConstraintCommand(final EditPart child,
			final Object constraint) {
		// not used
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see LayoutEditPolicy#getCreateCommand(CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(final CreateRequest request) {
		if (request == null) {
			return null;
		}
		Object childClass = request.getNewObjectType();
		Rectangle constraint = (Rectangle) getConstraintFor(request);
		if (childClass instanceof org.fordiac.ide.model.Palette.FBTypePaletteEntry) {
			org.fordiac.ide.model.Palette.FBTypePaletteEntry type = (org.fordiac.ide.model.Palette.FBTypePaletteEntry) childClass;

			if (getHost().getModel() instanceof Diagram) {				
				return new FBCreateCommand(type, (Diagram) getHost().getModel(),
						new Rectangle(constraint.getLocation().x,
								constraint.getLocation().y, -1, -1));
			}
		}
		if (childClass instanceof org.fordiac.ide.model.Palette.FBTypePaletteEntry[]) {
			org.fordiac.ide.model.Palette.FBTypePaletteEntry[] type = (org.fordiac.ide.model.Palette.FBTypePaletteEntry[]) childClass;

			if (getHost().getModel() instanceof Diagram) {
				return new ListFBCreateCommand(type, (Diagram) getHost().getModel(),
						new Rectangle(constraint.getLocation().x,
								constraint.getLocation().y, -1, -1));
			}
		}
		
		if (childClass instanceof SubApplicationTypePaletteEntry) {
			SubApplicationTypePaletteEntry type = (SubApplicationTypePaletteEntry) request
					.getNewObjectType();

			if (getHost().getModel() instanceof UIFBNetwork) {
				UIFBNetwork uiFBNetwork = (UIFBNetwork) getHost().getModel();
				CreateSubAppInstanceCommand cmd = new CreateSubAppInstanceCommand(type, 
						uiFBNetwork.getFbNetwork(), new Rectangle(
								constraint.getLocation().x, constraint.getLocation().y,
								-1, -1));
				return cmd;
			}
		}
		
		if (childClass instanceof TransferDataSelectionOfFb[]) {
			TransferDataSelectionOfFb[] type = (TransferDataSelectionOfFb[]) childClass;
			
			if (getHost().getModel() instanceof Diagram) {
				Diagram diagram = (Diagram)getHost().getModel();
				AutomationSystem system = SystemManager.getInstance().getSystemForDiagram(diagram);
				
				return new ListFBCreateCommand(type, (Diagram) getHost().getModel(),
						new Rectangle(constraint.getLocation().x,
								constraint.getLocation().y, -1, -1), system);
			}
		}
		
		
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#getAddCommand(
	 * org.eclipse.gef.Request)
	 */
	@Override
	protected Command getAddCommand(final Request generic) {
		return null;
	}

}
