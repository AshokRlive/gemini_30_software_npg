/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.policies;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.jface.preference.IPreferenceStore;
import org.fordiac.ide.application.commands.AddFBToSubAppCommand;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.policies.EmptyXYLayoutEditPolicy;
import org.fordiac.ide.gef.policies.ModifiedMoveHandle;
import org.fordiac.ide.gef.preferences.DiagramPreferences;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.SubAppView;

/**
 * This policy creates an AddFBToSubAppCommand when user moves selected FBs
 * over a subapp. When this is possible the subapp is marked as selected.
 * 
 * @author Filip Andr�n (fandr)
 */
public class FBAddToSubAppLayoutEditPolicy extends EmptyXYLayoutEditPolicy {

	Figure moveHandle;
	
	/* (non-Javadoc)
	 * @see org.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#getAddCommand(org.eclipse.gef.Request)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected Command getAddCommand(Request generic) {
		ChangeBoundsRequest request = (ChangeBoundsRequest) generic;
		List editParts = request.getEditParts();
		
		ArrayList<FBView> fbs = new ArrayList<FBView>();
		ArrayList<SubAppView> subApps = new ArrayList<SubAppView>(); 
		
		GraphicalEditPart child;

		for (int i = 0; i < editParts.size(); i++) {
			child = (GraphicalEditPart) editParts.get(i);
			if (child instanceof FBEditPart) {
				fbs.add(((FBEditPart) child).getCastedModel());
			} else if (child instanceof SubAppForFBNetworkEditPart) {
				subApps.add(((SubAppForFBNetworkEditPart) child).getCastedModel());
			}
		}
		if (fbs.isEmpty() && subApps.isEmpty()) {
			return super.getAddCommand(generic);
		}
		
		AddFBToSubAppCommand command = new AddFBToSubAppCommand(((SubAppForFBNetworkEditPart)getTargetEditPart(request)).getCastedModel(), fbs, subApps);
		return command;
	}



	/* (non-Javadoc)
	 * @see org.eclipse.gef.editpolicies.LayoutEditPolicy#showLayoutTargetFeedback(org.eclipse.gef.Request)
	 */
	@Override
	protected void showLayoutTargetFeedback(Request request) {
		if (REQ_ADD.equals(request.getType())) {
			if (moveHandle == null) {
				IPreferenceStore pf = Activator.getDefault().getPreferenceStore();
				int cornerDim = pf.getInt(DiagramPreferences.CORNER_DIM);
				if (cornerDim > 1) {
					cornerDim = cornerDim / 2;
				}
				moveHandle = new ModifiedMoveHandle((GraphicalEditPart) getTargetEditPart(request), new Insets(1), cornerDim);							
				addFeedback(moveHandle);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editpolicies.LayoutEditPolicy#eraseLayoutTargetFeedback(org.eclipse.gef.Request)
	 */
	@Override
	protected void eraseLayoutTargetFeedback(Request request) {
		if (moveHandle != null) {
			removeFeedback(moveHandle);
			moveHandle = null;
		}
	}
	
	
}
