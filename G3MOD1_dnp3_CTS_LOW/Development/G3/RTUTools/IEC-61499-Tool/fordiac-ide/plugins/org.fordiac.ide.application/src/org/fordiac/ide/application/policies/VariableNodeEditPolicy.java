/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.policies;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.ReconnectRequest;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.application.commands.AbstractDataConnectionCreateCommand;
import org.fordiac.ide.application.commands.DataConnectionCreateCommand;
import org.fordiac.ide.application.commands.LinkConstraints;
import org.fordiac.ide.application.commands.ReconnectDataConnectionCommand;
import org.fordiac.ide.gef.editparts.IDiagramEditPart;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

/**
 * An EditPolicy which allows drawing Connections between VariableInterfaces.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class VariableNodeEditPolicy extends
		org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getConnectionCompleteCommand(org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCompleteCommand(
			final CreateConnectionRequest request) {
		if (request.getStartCommand() instanceof AbstractDataConnectionCreateCommand) {
			AbstractDataConnectionCreateCommand command = (AbstractDataConnectionCreateCommand) request
					.getStartCommand();
			command.setTarget((InterfaceEditPart) getHost());
			return command;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getConnectionCreateCommand(org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCreateCommand(
			final CreateConnectionRequest request) {
		
		InterfaceEditPart host = (InterfaceEditPart) getHost();
		
		if( (host.isAdapter()) && (0 != (host.getMouseState() & SWT.CONTROL))){
			openAdapterType(host);
			return null;
		}
					
		AbstractDataConnectionCreateCommand cmd = new DataConnectionCreateCommand();
		cmd.setSource((InterfaceEditPart) getHost());
		if (cmd.getSourceView().getIInterfaceElement() instanceof VarDeclaration) {
			if (!LinkConstraints.isWithConstraintOK(((VarDeclaration)cmd.getSourceView().getIInterfaceElement()), true)) {
				return null; // Elements which are not connected by a with construct are not allowed to be connected
			}
		}
		
		EditPart parent = getHost().getParent();
		while (parent != null && !(parent instanceof IDiagramEditPart)) {
			parent = parent.getParent();
		}
		if (parent instanceof IDiagramEditPart) { // also means that parent !=
			// null
			cmd.setParent(((IDiagramEditPart) parent).getDiagram());
		}
		// if (parent instanceof UISubAppNetworkEditPart) {
		// cmd.setParent(((UISubAppNetworkEditPart) parent).getCastedModel());
		// }

		request.setStartCommand(cmd);
		return new DataConnectionCreateCommand();

	}

	private void openAdapterType(InterfaceEditPart host) {
		Palette systemPalette = host.getSystemPalette(); 
		if(null != systemPalette){
			
			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			
			PaletteEntry entry = systemPalette.getTypeEntries(((AdapterDeclaration)host.getCastedModel().getIInterfaceElement()).getTypeName()).get(0);
			if((null != entry) && (entry instanceof AdapterTypePaletteEntry))	{			
				IEditorDescriptor desc = PlatformUI.getWorkbench().
				        getEditorRegistry().getDefaultEditor(entry.getFile().getName());
				try {
					page.openEditor(new FileEditorInput(entry.getFile()), desc.getId());
				} catch (PartInitException e) {
					ApplicationPlugin.getDefault().logError(e.getMessage(), e);
				}
			}
		
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getReconnectTargetCommand(org.eclipse.gef.requests.ReconnectRequest)
	 */
	@Override
	protected Command getReconnectTargetCommand(final ReconnectRequest request) {
		ReconnectDataConnectionCommand cmd = new ReconnectDataConnectionCommand(
				request);
		EditPart parent = getHost().getParent();
		while (parent != null && !(parent instanceof IDiagramEditPart)) {
			parent = parent.getParent();
		}
		if (parent instanceof IDiagramEditPart) { // also means that parent !=
			// null
			cmd.setParent(((IDiagramEditPart) parent).getDiagram());
		}
		return cmd;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getReconnectSourceCommand(org.eclipse.gef.requests.ReconnectRequest)
	 */
	@Override
	protected Command getReconnectSourceCommand(final ReconnectRequest request) {
		ReconnectDataConnectionCommand cmd = new ReconnectDataConnectionCommand(
				request);
		EditPart parent = getHost().getParent();
		while (parent != null && !(parent instanceof IDiagramEditPart)) {
			parent = parent.getParent();
		}
		if (parent instanceof IDiagramEditPart) { // also means that parent !=
			// null
			cmd.setParent(((IDiagramEditPart) parent).getDiagram());
		}
		return cmd;

	}

}
