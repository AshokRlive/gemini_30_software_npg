/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editparts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseListener;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.application.actions.OpenSubApplicationEditorAction;
import org.fordiac.ide.application.figures.SubAppForFbNetworkFigure;
import org.fordiac.ide.application.policies.DeleteSubAppEditPolicy;
import org.fordiac.ide.application.policies.DeleteSubAppTypeInstanceEditPolicy;
import org.fordiac.ide.application.policies.FBAddToSubAppLayoutEditPolicy;
import org.fordiac.ide.application.policies.SubAppRenameEditPolicy;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.gef.editparts.ValueEditPart;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * The Class SubAppForFBNetworkEditPart.
 */
public class SubAppForFBNetworkEditPart extends AbstractViewEditPart {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#createFigureForModel()
	 */
	@Override
	protected IFigure createFigureForModel() {
		SubAppForFbNetworkFigure figure =  new SubAppForFbNetworkFigure(getCastedModel());
		
		if((null != getCastedModel().getSubApp()) && (null != getCastedModel().getSubApp().getPaletteEntry())) {
			figure.getMiddle().addMouseListener(new MouseListener(){
	
				@Override
				public void mousePressed(MouseEvent me) {
					if( 0 != (me.getState() & SWT.CONTROL)){
						openInSubappTypeEditor();
					}					
				}
	
				@Override
				public void mouseReleased(MouseEvent me) {					
				}
	
				@Override
				public void mouseDoubleClicked(MouseEvent me) {					
				}
				
			});
		}
		return figure;
	}
	
	private void openInSubappTypeEditor(){
		//open the default editor for the adapter file
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		
		PaletteEntry entry = getCastedModel().getSubApp().getPaletteEntry();
		if(null != entry)	{			
			IEditorDescriptor desc = PlatformUI.getWorkbench().
			        getEditorRegistry().getDefaultEditor(entry.getFile().getName());
			try {
				page.openEditor(new FileEditorInput(entry.getFile()), desc.getId());
			} catch (PartInitException e) {
				ApplicationPlugin.getDefault().logError(e.getMessage(), e);
			}
		}
	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public SubAppView getCastedModel() {
		return (SubAppView) getModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@Override
	protected List<?> getModelChildren() {
		ArrayList<SubAppInterfaceElementView> elements = new ArrayList<SubAppInterfaceElementView>();
		for (Iterator<SubAppInterfaceElementView> iterator = getCastedModel()
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			SubAppInterfaceElementView interfaceElementView = iterator.next();
			if (interfaceElementView.isVisible()) {
				elements.add(interfaceElementView);
			}
		}
		return elements;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractGraphicalEditPart#addChildVisual(org.
	 * eclipse.gef.EditPart, int)
	 */
	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof InterfaceEditPart) {
			InterfaceEditPart interfaceEditPart = (InterfaceEditPart) childEditPart;
			if (interfaceEditPart.isInput()) {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventInputs().add(child);
				} else if (interfaceEditPart.isVariable()) {
					getCastedFigure().getDataInputs().add(child);
				}
			} else {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventOutputs().add(child);
				} else if (interfaceEditPart.isVariable()) {
					getCastedFigure().getDataOutputs().add(child);
				}

			}
		} else if (childEditPart instanceof ValueEditPart) {
			ValueEditPart valueEditPart = (ValueEditPart) childEditPart;
			if (((VarDeclaration) valueEditPart.getCastedModel().eContainer())
					.getInputConnections().size() > 0) {
				valueEditPart.setVisible(false);
			}
			if (valueEditPart.getCastedModel().getValue() == null) {
				valueEditPart.setVisible(false);
			}
			getCastedFigure().getInputValuesContentPane().add(child);
		} else {
			super.addChildVisual(childEditPart, index);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractGraphicalEditPart#removeChildVisual(org
	 * .eclipse.gef.EditPart)
	 */
	@Override
	protected void removeChildVisual(final EditPart childEditPart) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof InterfaceEditPart) {
			InterfaceEditPart interfaceEditPart = (InterfaceEditPart) childEditPart;
			if (interfaceEditPart.isInput()) {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventInputs().remove(child);
				} else if (interfaceEditPart.isVariable()) {
					getCastedFigure().getDataInputs().remove(child);
				}
			} else {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventOutputs().remove(child);
				} else if (interfaceEditPart.isVariable()) {
					getCastedFigure().getDataOutputs().remove(child);
				}

			}
		} else if (childEditPart instanceof ValueEditPart) {
			getCastedFigure().getInputValuesContentPane().remove(child);
		} else {
			super.removeChildVisual(childEditPart);
		}
	}

	/** The adapter. */
	private EContentAdapter adapter = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getContentAdapter()
	 */
	@Override
	public EContentAdapter getContentAdapter() {
		if (adapter == null) {
			adapter = new EContentAdapter() {

				@Override
				public void notifyChanged(final Notification notification) {
					super.notifyChanged(notification);
					Object feature = notification.getFeature();
					if (UiPackage.eINSTANCE.getSubAppView_InterfaceElements().equals(
							feature)) {
						refresh();
					} else if (UiPackage.eINSTANCE
							.getSubAppInterfaceElementView_Visible().equals(feature)) {
						refresh();
					} else if (LibraryElementPackage.eINSTANCE.getINamedElement_Name()
							.equals(feature)) {
						refresh();
					}
					refreshToolTip();
				}
			};
		}
		return adapter;
	}

	private void refreshToolTip() {
		getCastedFigure().refreshToolTips();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#refresh()
	 */
	@Override
	public void refresh() {
		super.refresh();
		refreshInterfaceLabels();
	}

	/**
	 * Creates the EditPolicies used for this EditPart.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		
		
		// allow delete of a sub app
		if(null != getCastedModel().getSubApp().getPaletteEntry()){
			//sub app types for now a special delete policy 
			installEditPolicy(EditPolicy.COMPONENT_ROLE, new DeleteSubAppTypeInstanceEditPolicy());
		}else{
			installEditPolicy(EditPolicy.COMPONENT_ROLE, new DeleteSubAppEditPolicy());
		}
		
		// A FB needs a special rename policy as it needs to update the unique name data base
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new SubAppRenameEditPolicy());
		
		// Add policy to handle drag&drop of fbs
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new FBAddToSubAppLayoutEditPolicy());
	}

	/**
	 * Refresh interface labels.
	 */
	private void refreshInterfaceLabels() {
		for (Iterator<?> iterator = getChildren().iterator(); iterator.hasNext();) {
			EditPart ep = (EditPart) iterator.next();
			if (ep instanceof InterfaceEditPart) {
				((InterfaceEditPart) ep).refreshName();
			}
		}

	}

	/** The listener. */
	private IPropertyChangeListener listener;

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.fordiac.iec61499.ui.editparts.AbstractEMFEditPart#
	 * getPreferenceChangeListener()
	 */
	@Override
	public IPropertyChangeListener getPreferenceChangeListener() {
		if (listener == null) {
			listener = new IPropertyChangeListener() {
				public void propertyChange(final PropertyChangeEvent event) {
					// nothing to do, required for later versions
				}
			};
		}
		return listener;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#getEditableValue()
	 */
	public Object getEditableValue() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#getPropertyValue(java.lang
	 * .Object)
	 */
	public Object getPropertyValue(final Object id) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#isPropertySet(java.lang
	 * .Object)
	 */
	public boolean isPropertySet(final Object id) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#resetPropertyValue(java
	 * .lang.Object)
	 */
	public void resetPropertyValue(final Object id) {
		// not used

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#setPropertyValue(java.lang
	 * .Object, java.lang.Object)
	 */
	public void setPropertyValue(final Object id, final Object value) {
		// not used

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#performRequest(org.eclipse
	 * .gef.Request)
	 */
	@Override
	public void performRequest(final Request request) {
		if (request.getType().equals(RequestConstants.REQ_OPEN)) {
			
			if(null != getCastedModel().getSubApp().getPaletteEntry()){
				// we have a type open the sub-app type editor
				openInSubappTypeEditor();
			} else {
				new OpenSubApplicationEditorAction(getCastedModel().getUiSubAppNetwork()).run();
			}					
		} else {
			super.performRequest(request);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getINamedElement()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel().getSubApp();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getNameLabel()
	 */
	@Override
	public Label getNameLabel() {
		if (getCastedFigure() != null) {
			return getCastedFigure().getInstanceNameLabel();
		}
		return null;
	}

	/**
	 * Gets the casted figure.
	 * 
	 * @return the casted figure
	 */
	private SubAppForFbNetworkFigure getCastedFigure() {
		if (getFigure() instanceof SubAppForFbNetworkFigure) {
			return (SubAppForFbNetworkFigure) getFigure();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getInterfaceElements()
	 */
	@Override
	public List<? extends InterfaceElementView> getInterfaceElements() {
		return getCastedModel().getInterfaceElements();
	}

}
