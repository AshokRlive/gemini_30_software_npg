/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.commands;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.Palette.FBTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.Position;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The Class FBCreateCommand.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class FBCreateCommand extends Command {

	/** The type. */
	// protected final FBTypePaletteEntry type;

	/** The parent. */
	protected final Diagram parentDiagram;  //only needed for applications and subApps TODO try to get rid of it
	
	private final SubAppNetwork subAppNetwork;

	/** The create position. */
	protected Rectangle bounds;
	
	private CompositeFBType parentComposite;
	
	public CompositeFBType getParentComposite() {
		return parentComposite;
	}

	public void setParentComposite(CompositeFBType parentComposite) {
		this.parentComposite = parentComposite;
	}

	/** The f b. */
	protected FB fB;

	/**
	 * Gets the fb.
	 * 
	 * @return the newly create FB
	 */
	public FB getFB() {
		return fB;
	}

	/** The editor. */
	protected IEditorPart editor;

	protected FBTypePaletteEntry paletteEntry;	

	public FBTypePaletteEntry getPaletteEntry() {
		return paletteEntry;
	}

	private boolean createResourceFB = false; // needs to be set to true, if the
												// fb is created in a resource!

	public boolean isCreateResourceFB() {
		return createResourceFB;
	}

	public void setCreateResourceFB(boolean createResourceFB) {
		this.createResourceFB = createResourceFB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(ApplicationPlugin.getDefault()
				.getCurrentActiveEditor());

	}

	/**
	 * Instantiates a new fB create command.
	 * 
	 * @param type
	 *            the type
	 * @param parent
	 *            the parent
	 * @param bounds
	 *            the bounds
	 */
	public FBCreateCommand(final FBTypePaletteEntry paletteEntry, final Diagram parent,
			final Rectangle bounds) {
		this.paletteEntry = paletteEntry;
		this.parentDiagram = parent;
		this.subAppNetwork = parent.getFunctionBlockNetwork();
		this.bounds = bounds;
		setLabel(Messages.FBCreateCommand_LABLE_CreateFunctionBlock);
	}
	
	public FBCreateCommand(final FBTypePaletteEntry paletteEntry, final SubAppNetwork subAppNetwork,
			final Rectangle bounds) {
		this.paletteEntry = paletteEntry;
		this.parentDiagram = null;
		this.subAppNetwork = subAppNetwork;
		this.bounds = bounds;
		setLabel(Messages.FBCreateCommand_LABLE_CreateFunctionBlock);
	}

	/**
	 * checks whether all required information for creating a new UIFB are set.
	 * 
	 * @return true, if can execute
	 */
	@Override
	public boolean canExecute() {
		return paletteEntry != null && bounds != null && (subAppNetwork != null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = ApplicationPlugin.getDefault().getCurrentActiveEditor();
		setLabel(getLabel()
				+ "(" + (editor != null ? editor.getTitle() : "") + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if(null != subAppNetwork){	
			createFB();			
			fB.setFbtPath(fB.getPaletteEntry().getProjectRelativeTypePath());
			fB.setId(EcoreUtil.generateUUID());
			fB.setInterface((InterfaceList) EcoreUtil.copy(fB.getFBType().getInterfaceList()));
			fB.setIdentifier(true);
			fB.setResourceFB(isCreateResourceFB());
			fB.setParentCompositeFBType(parentComposite);
			
			Position pos = LibraryElementFactory.eINSTANCE.createPosition();
			pos.setX(bounds.x);
			pos.setY(bounds.y);
			fB.setPosition(pos);
			
			createValues();
			
			String name = "";
			if (parentComposite != null) {
				name = NameRepository.getInstance().getCompositeUniqueFBInstanceName(parentComposite, fB.getFBType().getName());
			} else {
				if (parentDiagram != null) {
					name = NameRepository.getInstance().getSystemUniqueFBInstanceName(
							SystemManager.getInstance().getSystemForDiagram(parentDiagram), 
							fB.getPaletteEntry().getLabel(), fB.getId());
				}
			}
			
			//this has to be done before the name is set in order to avoid problems in the name setting
			subAppNetwork.getFBs().add(fB);
			fB.setName(name);
		}
	}

	/** allows that it can be overwritten by the adapterFBCreateCommand*/
	protected void createFB() {
		fB = LibraryElementFactory.eINSTANCE.createFB();
		fB.setPaletteEntry(paletteEntry);
	}

	/**
	 * Creates the values.
	 */
	protected void createValues() {
		ArrayList<IInterfaceElement> iInterfaceElements = new ArrayList<IInterfaceElement>();

		// iInterfaceElements.addAll(fB.getInterface().getEventInputs());
		iInterfaceElements.addAll(fB.getInterface().getInputVars());

		for (Iterator<IInterfaceElement> iterator = iInterfaceElements
				.iterator(); iterator.hasNext();) {
			IInterfaceElement element = iterator.next();
			Value value = LibraryElementFactory.eINSTANCE.createValue();
			element.setValue(value);

		}
	}


	/**
	 * Redo.
	 * 
	 * @see FBCreateCommand#execute()
	 */
	@Override
	public void redo() {
		if (subAppNetwork != null) {
			subAppNetwork.getFBs().add(fB);			
			
			if (parentComposite == null) {
				//only if the Fb is not within a composite we will need to reinsert the name
				NameRepository.getInstance().addSystemUniqueFBInstanceName(SystemManager.getInstance().getSystemForDiagram(parentDiagram), fB.getName(), fB.getId());
			}
		}

	}

	/**
	 * undo of FBCreateCommand.
	 */
	@Override
	public void undo() {
		if (subAppNetwork != null) {
			if (parentComposite == null) {
				//only if the FB is in an application we will need to remove its name from the application hash set
				NameRepository.getInstance().removeInstanceName(fB);
			}
			subAppNetwork.getFBs().remove(fB);
		}
	}

	public void updateCreatePosition(Point pt) {
		bounds = new Rectangle(pt.x, pt.y, -1, -1);
		
	}
}