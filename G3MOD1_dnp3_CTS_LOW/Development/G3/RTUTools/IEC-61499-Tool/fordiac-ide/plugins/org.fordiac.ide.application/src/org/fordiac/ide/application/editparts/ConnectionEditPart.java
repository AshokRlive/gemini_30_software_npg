/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.editparts;

import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.Shape;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.ui.views.properties.ComboBoxPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;
import org.fordiac.ide.application.figures.ConnectionTooltipFigure;
import org.fordiac.ide.application.policies.DeleteConnectionEditPolicy;
import org.fordiac.ide.application.policies.DisableConnectionHandleRoleEditPolicy;
import org.fordiac.ide.application.policies.FeedbackConnectionEndpointEditPolicy;
import org.fordiac.ide.gef.figures.HideableConnection;
import org.fordiac.ide.gef.properties.PropertyUtil;
import org.fordiac.ide.gef.router.BendpointPolicyRouter;
import org.fordiac.ide.gef.router.MoveableRouter;
import org.fordiac.ide.gef.router.RouterUtil;
import org.fordiac.ide.model.libraryElement.AdapterConnection;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.impl.FBViewImpl;
import org.fordiac.ide.preferences.PreferenceConstants;
import org.fordiac.ide.preferences.PreferenceGetter;
import org.fordiac.ide.util.Activator;

/**
 * The Class ConnectionEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ConnectionEditPart extends AbstractConnectionEditPart implements
		IPropertySource {

	private static final String VISIBLE = "VISIBLE";
	private static final String HIDDEN = "HIDDEN";
	private static final String HIDEN_CON = "HIDEN_CON";

	public ConnectionEditPart() {
		super();
	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public ConnectionView getCastedModel() {
		return (ConnectionView) getModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#setSelected(int)
	 */
	@Override
	public void setSelected(int value) {
		super.setSelected(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// Selection handle edit policy.
		// Makes the connection show a feedback, when selected by the user.
		installEditPolicy(EditPolicy.CONNECTION_ENDPOINTS_ROLE,
				new FeedbackConnectionEndpointEditPolicy());
		installEditPolicy(EditPolicy.CONNECTION_ENDPOINTS_ROLE,
				new DisableConnectionHandleRoleEditPolicy());

		// Allows the removal of the connection model element
		installEditPolicy(EditPolicy.CONNECTION_ROLE,
				new DeleteConnectionEditPolicy());

		if (getConnectionFigure().getConnectionRouter() instanceof BendpointPolicyRouter) {
			installEditPolicy(EditPolicy.CONNECTION_BENDPOINTS_ROLE,
					((BendpointPolicyRouter) getConnectionFigure()
							.getConnectionRouter())
							.getBendpointPolicy(getCastedModel()));
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractConnectionEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		PolylineConnection connection = null;

		connection = RouterUtil.getConnectionRouterFactory(null)
				.createConnectionFigure();
		
		if (getCastedModel().getConnectionElement() == null) {
			// Broken connection, mark it
			connection.setAlpha(200);
			return connection;
		}
		
		String status = getCastedModel().getConnectionElement().getParameter(HIDEN_CON);
		if (connection instanceof HideableConnection) {
			((HideableConnection) connection).setHidden((status != null && status.equalsIgnoreCase(HIDDEN) ? true: false ));
			if (getCastedModel() != null && getCastedModel().getSource() != null && getCastedModel().getSource().eContainer() != null && getCastedModel().getSource().eContainer() instanceof FBViewImpl) {
				((HideableConnection) connection).setLabel(((FBViewImpl)getCastedModel().getSource().eContainer()).getFb().getName() + "." + getCastedModel().getSource().getLabel());
			}
		}
		
		
		PolygonDecoration arrow = new PolygonDecoration();
		arrow.setTemplate(PolygonDecoration.TRIANGLE_TIP);
		arrow.setScale(7, 4);
		connection.setTargetDecoration(arrow);
		
		PolygonDecoration arrow1 = new PolygonDecoration();
		arrow1.setTemplate(PolygonDecoration.INVERTED_TRIANGLE_TIP);
		arrow1.setScale(7, 4);
		arrow1.setLocation(new org.eclipse.draw2d.geometry.Point(10, 10));
		connection.setSourceDecoration(arrow1);

		if (getCastedModel().getConnectionElement() instanceof EventConnection) {
			connection.setForegroundColor(PreferenceGetter
					.getColor(PreferenceConstants.P_EVENT_CONNECTOR_COLOR));
		}

		if (getCastedModel().getConnectionElement() instanceof AdapterConnection) {
			connection.setForegroundColor(PreferenceGetter
					.getColor(PreferenceConstants.P_ADAPTER_CONNECTOR_COLOR));
			connection.setTargetDecoration(null);
			connection.setSourceDecoration(null);

		}
		
		if (getCastedModel().getConnectionElement() instanceof DataConnection) {
			//Intermediate fix until we have real adapter connections
			if(((DataConnection)getCastedModel().getConnectionElement()).getSource() instanceof AdapterDeclaration){
				connection.setForegroundColor(PreferenceGetter
						.getColor(PreferenceConstants.P_ADAPTER_CONNECTOR_COLOR));
				connection.setTargetDecoration(null);
				connection.setSourceDecoration(null);
			}else{
				connection.setForegroundColor(PreferenceGetter
					.getColor(PreferenceConstants.P_DATA_CONNECTOR_COLOR));
			}

		}

		connection.setToolTip(new ConnectionTooltipFigure(getCastedModel()
				.getConnectionElement()));

		return connection;
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();

		if (getConnectionFigure() instanceof PolylineConnection) {
			ConnectionRouter router = getConnectionFigure()
					.getConnectionRouter();
			if (getCastedModel().getConnectionElement() != null) {
				if (router instanceof MoveableRouter) {
					((MoveableRouter) router).setDeltaX1(getConnectionFigure(),
							getCastedModel().getConnectionElement().getDx1());
					((MoveableRouter) router).setDeltaX2(getConnectionFigure(),
							getCastedModel().getConnectionElement().getDx2());
					((MoveableRouter) router).setDeltaY(getConnectionFigure(),
							getCastedModel().getConnectionElement().getDy());
				}

				if (getCastedModel().getConnectionElement()
						.isBrokenConnection()) {
					((PolylineConnection) getConnectionFigure())
							.setLineStyle(SWT.LINE_DASH);

				} else {
					((PolylineConnection) getConnectionFigure())
							.setLineStyle(SWT.LINE_SOLID);
				}
			}
		}
	}

	/** The property change listener. */
	private final IPropertyChangeListener propertyChangeListener = new IPropertyChangeListener() {
		public void propertyChange(PropertyChangeEvent event) {
			if (event.getProperty().equals(
					PreferenceConstants.P_EVENT_CONNECTOR_COLOR)
					&& getCastedModel().getConnectionElement() instanceof EventConnection) {
				getFigure()
						.setForegroundColor(
								PreferenceGetter
										.getColor(PreferenceConstants.P_EVENT_CONNECTOR_COLOR));
			}
			if (event.getProperty().equals(
					PreferenceConstants.P_ADAPTER_CONNECTOR_COLOR)
					&& getCastedModel().getConnectionElement() instanceof AdapterConnection) {
				getFigure()
						.setForegroundColor(
								PreferenceGetter
										.getColor(PreferenceConstants.P_ADAPTER_CONNECTOR_COLOR));
			}
			if (event.getProperty().equals(
					PreferenceConstants.P_DATA_CONNECTOR_COLOR)
					&& getCastedModel().getConnectionElement() instanceof DataConnection) {
				getFigure()
						.setForegroundColor(
								PreferenceGetter
										.getColor(PreferenceConstants.P_DATA_CONNECTOR_COLOR));
			}
			if (event.getProperty().equals(PreferenceConstants.P_HIDE_DATA_CON)) {
				if (getCastedModel().getConnectionElement() instanceof DataConnection) {
					getFigure().setVisible(!((Boolean) event.getNewValue()));
				}
			}
			if (event.getProperty()
					.equals(PreferenceConstants.P_HIDE_EVENT_CON)) {
				if (getCastedModel().getConnectionElement() instanceof EventConnection) {
					getFigure().setVisible(!((Boolean) event.getNewValue()));
				}
			}
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (!isActive()) {
			super.activate();
			Activator.getDefault().getPreferenceStore()
					.addPropertyChangeListener(propertyChangeListener);
			if (getCastedModel().getConnectionElement() != null) {
				getCastedModel().getConnectionElement().eAdapters()
						.add(getContentAdapter());
			}
		}
	}

	private EContentAdapter contentAdapter;

	private Adapter getContentAdapter() {
		if (contentAdapter == null) {
			contentAdapter = new EContentAdapter() {
				@Override
				public void notifyChanged(Notification notification) {
					Object feature = notification.getFeature();
					refreshVisuals();

					if (LibraryElementPackage.eINSTANCE
							.getINamedElement_Comment().equals(feature)) {
						refreshComment();
					}
				}

			};
		}
		return contentAdapter;
	}

	private void refreshComment() {
		getFigure().setToolTip(
				new ConnectionTooltipFigure(getCastedModel()
						.getConnectionElement()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			Activator.getDefault().getPreferenceStore()
					.removePropertyChangeListener(propertyChangeListener);
			if (getCastedModel().getConnectionElement() != null) {
				getCastedModel().getConnectionElement().eAdapters()
						.remove(getContentAdapter());
			}
		}
	}

	public void setTransparency(int value) {
		if (getFigure() instanceof PolylineConnection) {
			PolylineConnection connection = ((PolylineConnection) getFigure());
			connection.setAlpha(value);
			for (Object fig : connection.getChildren()) {
				if (fig instanceof Shape) {
					((Shape) fig).setAlpha(value);
				}
			}
		}
	}

	public IPropertyDescriptor[] getPropertyDescriptors() {
		IPropertyDescriptor[] descriptors = new IPropertyDescriptor[2];
		PropertyUtil.addCommentDescriptor(getCastedModel()
				.getConnectionElement(), descriptors, 0);

		descriptors[1] = new ComboBoxPropertyDescriptor(HIDEN_CON, "Hide Line",
				new String[] { HIDDEN, VISIBLE });
		return descriptors;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#getEditableValue()
	 */
	public Object getEditableValue() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#getPropertyValue(java
	 * .lang .Object)
	 */
	public Object getPropertyValue(final Object id) {
		if (id == HIDEN_CON) {
			String x = getCastedModel().getConnectionElement().getParameter(
					HIDEN_CON);
			if (x != null && x.equalsIgnoreCase(HIDDEN)) {
				return 0;
			} else {
				return 1;
			}
		} else {
			return PropertyUtil.getPropertyValue(id);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#isPropertySet(java.lang
	 * .Object)
	 */
	public boolean isPropertySet(final Object id) {
		return true;
	}

	@Override
	public void resetPropertyValue(Object id) {
		if (id == HIDEN_CON) {
			getCastedModel().getConnectionElement().setParameter(HIDEN_CON,
					VISIBLE);
		}
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if (id == HIDEN_CON) {
			if (Integer.parseInt(value.toString()) == 0) {
				getCastedModel().getConnectionElement().setParameter(HIDEN_CON,
						HIDDEN);
			} else {
				getCastedModel().getConnectionElement().setParameter(HIDEN_CON,
						VISIBLE);
			}
			updateHiddenStatus();
			getFigure().repaint();
		} else {
			PropertyUtil.setPropertyValue(id, value);
		}

	}

	private void updateHiddenStatus() {
		String status = getCastedModel().getConnectionElement().getParameter(HIDEN_CON);
		if (getFigure() instanceof HideableConnection) {
			((HideableConnection) getFigure()).setHidden((status != null && status.equalsIgnoreCase(HIDDEN) ? true: false ));
		}

	}

}
