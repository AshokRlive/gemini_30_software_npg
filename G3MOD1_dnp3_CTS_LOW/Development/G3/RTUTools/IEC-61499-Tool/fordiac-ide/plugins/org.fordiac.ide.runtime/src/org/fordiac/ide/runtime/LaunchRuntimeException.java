/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.runtime;

/**
 * The Class LaunchRuntimeException.
 */
public class LaunchRuntimeException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8564311995940813195L;

	/**
	 * Instantiates a new launch runtime exception.
	 * 
	 * @param msg the msg
	 */
	public LaunchRuntimeException(String msg) {
		super(msg);
	}
}
