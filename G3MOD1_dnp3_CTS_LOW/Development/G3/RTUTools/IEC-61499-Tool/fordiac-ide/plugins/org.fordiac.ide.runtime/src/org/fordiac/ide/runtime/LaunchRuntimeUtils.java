/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.runtime;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;

/**
 * The Class LaunchRuntimeUtils.
 */
public class LaunchRuntimeUtils {

	/**
	 * Starts a new launch runtime configuration.
	 * 
	 * @param configname
	 *            the configuration name
	 * @param runtime
	 *            the path to the runtime
	 * @param location
	 *            the location of the runtime
	 * @param arguments
	 *            the runtime arguments
	 */
	public static ILaunch startRuntime(String configname, String runtime,
			String location, String arguments) {
		/** Launch configuration. */
		ILaunchConfiguration config;
		/** The launch configuration's working copy. */
		ILaunchConfigurationWorkingCopy wc;
		ILaunch launcht = null;

		// Get the default launch manager
		DebugPlugin debug = DebugPlugin.getDefault();
		ILaunchManager lm = debug.getLaunchManager();
		// Set launch configuration type to 'Program'
		ILaunchConfigurationType configType = lm
				.getLaunchConfigurationType("org.eclipse.ui.externaltools.ProgramLaunchConfigurationType");
		try {
			wc = configType.newInstance(null, configname);
			// Set necessary attributes for the launch configuration
			wc.setAttribute(
					"org.eclipse.debug.core.appendEnvironmentVariables", true);
			wc.setAttribute("org.eclipse.ui.externaltools.ATTR_LOCATION",
					runtime);
			wc.setAttribute("org.eclipse.ui.externaltools.ATTR_TOOL_ARGUMENTS",
					arguments);
			wc.setAttribute(
					"org.eclipse.ui.externaltools.ATTR_WORKING_DIRECTORY",
					location);

			// wc.setAttribute(IDebugUIConstants.ATTR_CAPTURE_IN_CONSOLE,
			// false);
			// wc.setAttribute(DebugPlugin.ATTR_CAPTURE_OUTPUT, false);

			config = wc.doSave();
			launcht = config.launch(ILaunchManager.RUN_MODE, null);

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				Activator.getDefault().logError(e.getMessage(), e);
			}
			// config.launch(ILaunchManager.RUN_MODE, null);
		} catch (CoreException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		return launcht;
	}
}
