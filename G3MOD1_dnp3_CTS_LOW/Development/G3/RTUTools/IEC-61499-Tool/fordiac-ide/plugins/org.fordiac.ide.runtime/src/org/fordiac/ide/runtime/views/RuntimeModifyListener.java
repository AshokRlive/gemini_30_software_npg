/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.runtime.views;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.fordiac.ide.runtime.IRuntimeLauncher;


/**
 * The listener interface for receiving runtimeModify events.
 * The class that is interested in processing a runtimeModify
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addRuntimeModifyListener<code> method. When
 * the runtimeModify event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see RuntimeModifyEvent
 */
public class RuntimeModifyListener implements ModifyListener {

	protected IRuntimeLauncher launcher;
	protected String name;
	
	/**
	 * Instantiates a new runtime modify listener.
	 * 
	 * @param iLauncher the i launcher
	 * @param name the name
	 */
	RuntimeModifyListener (IRuntimeLauncher iLauncher, String name) {
		launcher = iLauncher;
		this.name = name;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.swt.events.ModifyListener#modifyText(org.eclipse.swt.events.ModifyEvent)
	 */
	@Override
	public void modifyText(ModifyEvent e) {		
	}

}
