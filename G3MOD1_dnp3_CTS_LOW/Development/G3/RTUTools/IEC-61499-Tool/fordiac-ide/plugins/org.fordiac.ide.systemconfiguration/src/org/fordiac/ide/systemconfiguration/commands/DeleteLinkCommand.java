/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.Link;
import org.fordiac.ide.model.libraryElement.Segment;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.LinkView;
import org.fordiac.ide.model.ui.SegmentView;

/**
 * The Class DeleteLinkCommand.
 */
public class DeleteLinkCommand extends Command {

	private LinkView linkView;
	private Link link;

	private Diagram parent;

	private SystemConfigurationNetwork sysConNetwork;

	private SegmentView sourceSegmentView;
	private DeviceView destDeviceView;
	private Segment sourceSegment;
	private Device destDevice;

	/**
	 * Gets the link view.
	 * 
	 * @return the link view
	 */
	public LinkView getLinkView() {
		return linkView;
	}

	/**
	 * Sets the link view.
	 * 
	 * @param linkView the new link view
	 */
	public void setLinkView(final LinkView linkView) {
		this.linkView = linkView;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return super.canExecute();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		sourceSegmentView = linkView.getSource();
		destDeviceView = linkView.getDestination();

		sourceSegment = linkView.getLink().getSegment();
		destDevice = linkView.getLink().getDevice();

		link = linkView.getLink();

		linkView.setSource(null);
		linkView.setDestination(null);

		linkView.getLink().setDevice(null);
		linkView.getLink().setSegment(null);

		parent = (Diagram) linkView.eContainer();
		parent.getConnections().remove(linkView);

		sysConNetwork = (SystemConfigurationNetwork) linkView.getLink()
				.eContainer();
		sysConNetwork.getLinks().remove(linkView.getLink());

	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {

		linkView.setSource(sourceSegmentView);
		linkView.setDestination(destDeviceView);

		linkView.getLink().setDevice(destDevice);
		linkView.getLink().setSegment(sourceSegment);

		parent.getConnections().add(linkView);

		sysConNetwork.getLinks().add(link);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		linkView.setSource(null);
		linkView.setDestination(null);

		linkView.getLink().setDevice(null);
		linkView.getLink().setSegment(null);

		parent.getConnections().remove(linkView);

		sysConNetwork.getLinks().remove(linkView.getLink());
	}
}
