/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.editparts;

import java.util.Collections;
import java.util.List;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.AlignmentRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Pattern;
import org.eclipse.swt.widgets.Display;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.gef.figures.InteractionStyleFigure;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.SegmentView;
import org.fordiac.ide.model.ui.Size;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.model.ui.UiPackage;
import org.fordiac.ide.systemconfiguration.policies.DeleteSegmentEditPolicy;
import org.fordiac.ide.systemconfiguration.policies.SegmentNodeEditPolicy;
import org.fordiac.ide.util.ColorHelper;

/**
 * The Class SegmentEditPart.
 */
public class SegmentEditPart extends AbstractViewEditPart implements
		NodeEditPart {
	
	public SegmentEditPart(ZoomManager zoomManager) {
		super();
		setConnectable(true);
		this.zoomManager = zoomManager;
	}

	@Override
	protected IFigure createFigureForModel() {
		return new SegmentFigure();
	}

	private final EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {

			Object feature = notification.getFeature();
			if (UiPackage.eINSTANCE.getSize_Width().equals(feature)
					|| UiPackage.eINSTANCE.getSize_Height().equals(feature)) {
				refreshVisuals();
			}

			super.notifyChanged(notification);
			refreshSourceConnections();
		}

	};

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getContentAdapter()
	 */
	@Override
	public EContentAdapter getContentAdapter() {
		return adapter;
	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public SegmentView getCastedModel() {
		return (SegmentView) getModel();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getINamedElement()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel().getSegment();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getInterfaceElements()
	 */
	@Override
	public List<? extends InterfaceElementView> getInterfaceElements() {
		return null;
	}

	private SegmentFigure getCastedFigure() {
		return (SegmentFigure) getFigure();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getNameLabel()
	 */
	@Override
	public Label getNameLabel() {
		return getCastedFigure().getName();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getPreferenceChangeListener()
	 */
	@Override
	public IPropertyChangeListener getPreferenceChangeListener() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#getEditableValue()
	 */
	@Override
	public Object getEditableValue() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#getPropertyValue(java.lang.Object)
	 */
	@Override
	public Object getPropertyValue(final Object id) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#isPropertySet(java.lang.Object)
	 */
	@Override
	public boolean isPropertySet(final Object id) {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#resetPropertyValue(java.lang.Object)
	 */
	@Override
	public void resetPropertyValue(final Object id) {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertySource#setPropertyValue(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void setPropertyValue(final Object id, final Object value) {
	}

	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		installEditPolicy(EditPolicy.LAYOUT_ROLE,
				new XYLayoutEditPolicy() {
					public Command getCommand(Request request) {
						Object type = request.getType();

						if (REQ_ALIGN.equals(type))
							return getAlignCommand((AlignmentRequest) request);

						return null;
					}

					protected Command getAlignCommand(AlignmentRequest request) {
						AlignmentRequest req = new AlignmentRequest(
								REQ_ALIGN_CHILDREN);
						req.setEditParts(getHost());
						req.setAlignment(request.getAlignment());
						req.setAlignmentRectangle(request
								.getAlignmentRectangle());
						return getHost().getParent().getCommand(req);
					}

					@Override
					protected Command getCreateCommand(CreateRequest request) {
						return null;
					}
				});
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE,
				new SegmentNodeEditPolicy());
		installEditPolicy(EditPolicy.COMPONENT_ROLE,
				new DeleteSegmentEditPolicy());
//		installEditPolicy(EditPolicyRoles.CONNECTION_HANDLES_ROLE,
//				new TopBottomConnectionHandleEditPolicy());
		
	}

	@Override
	protected void refreshPosition() {
		Rectangle bounds = null;

		Size size = getCastedModel().getSize();
		if (size == null) {
			size = UiFactory.eINSTANCE.createSize();
			size.setWidth(300);
			size.setHeight(40);
		}

		if (getView().getPosition() != null) {
			// if (getView() != null && getView().getPosition() != null) {
			bounds = new Rectangle(getView().getPosition().getX(), getView()
					.getPosition().getY(), size.getWidth(), size.getHeight());
			// } else {
			// bounds = new Rectangle()
			// }
			((GraphicalEditPart) getParent()).setLayoutConstraint(this,
					getFigure(), bounds);
		}
	}

	/**necessary that the gradient pattern can be scaled accordingly */ 
	ZoomManager zoomManager;

	/**
	 * The Class DeviceFigure.
	 */
	public class SegmentFigure extends Shape implements InteractionStyleFigure {

		/** The instance name label. */
		private final Label instanceNameLabel;
		/** The main. */
		private final Figure main = new Figure();

		private final RoundedRectangle rect = new RoundedRectangle() {
			@Override
			protected void outlineShape(Graphics graphics) {

			}
			
			

			@Override
			protected void fillShape(Graphics graphics) {
				Display display = Display.getCurrent();	
				Rectangle boundingRect = getBounds().getCopy();

				boundingRect.scale(zoomManager.getZoom());
				Point topLeft = boundingRect.getTopLeft();
				Point bottomRight = boundingRect.getBottomRight();
				
				Color first = ColorHelper.lighter(getBackgroundColor());
				
				Pattern pattern = new Pattern(display, topLeft.x, topLeft.y + boundingRect.height/2, topLeft.x, bottomRight.y, 
						getBackgroundColor(), first);	
				graphics.setBackgroundPattern(pattern);					
				graphics.fillRoundRectangle(getBounds(), getCornerDimensions().width, getCornerDimensions().height);
				graphics.setBackgroundPattern(null);	
				pattern.dispose();
				
				
				Color darker = ColorHelper.darker(getBackgroundColor());
				
				pattern = new Pattern(display, topLeft.x, topLeft.y + boundingRect.height/2, topLeft.x, bottomRight.y, 
						darker, getBackgroundColor());	
				graphics.setBackgroundPattern(pattern);					
				graphics.fillOval(getBounds().right() - (getBounds().height() * 2 / 3), 
						getBounds().getTop().y, (getBounds().height() * 2 / 3), getBounds().height());
				graphics.setBackgroundPattern(null);	
				pattern.dispose();
				
				pattern = new Pattern(display, topLeft.x, topLeft.y + getBounds().height/2, topLeft.x, bottomRight.y, 
						first, getBackgroundColor());	
				graphics.setBackgroundPattern(pattern);	
				
				Rectangle clipRect = getBounds().getCopy();
				clipRect.setHeight(clipRect.height/2);	
				clipRect.setY(clipRect.y + clipRect.height);			
				graphics.clipRect(clipRect);
				graphics.fillRoundRectangle(getBounds(), getCornerDimensions().width, getCornerDimensions().height);
				graphics.setBackgroundPattern(null);	
				pattern.dispose();				
				first.dispose();
				graphics.clipRect(getBounds().getCopy());
				
				pattern = new Pattern(display, topLeft.x, topLeft.y + boundingRect.height/2, topLeft.x, bottomRight.y, 
						getBackgroundColor(), darker);	
				graphics.setBackgroundPattern(pattern);					
				graphics.fillOval(getBounds().right() - (getBounds().height() * 2 / 3), 
						getBounds().getTop().y, (getBounds().height() * 2 / 3), getBounds().height());
				graphics.setBackgroundPattern(null);	
				pattern.dispose();
				
				darker.dispose();
			}

			@Override
			public void setBounds(Rectangle rect) {
				super.setBounds(rect);
				setCornerDimensions(new Dimension(rect.height * 2 /3, rect.height));
			}
			
		};


		/**
		 * Instantiates a new device figure.
		 */
		public SegmentFigure() {

			this.setFillXOR(true);
			setFill(true);

			GridData rectLayoutData = new GridData(
					GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL);
			GridData instanceNameLayout = new GridData(
					GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL);

			instanceNameLabel = new Label();
			instanceNameLabel.setText(getINamedElement().getName());
			instanceNameLabel.setTextAlignment(PositionConstants.RIGHT);
			instanceNameLabel.setLabelAlignment(PositionConstants.RIGHT);

			GridLayout gridLayout = new GridLayout(1, true);
			gridLayout.verticalSpacing = 2;
			gridLayout.marginHeight = 0;
			gridLayout.marginWidth = 0;
			setLayoutManager(gridLayout);

			GridLayout mainLayout = new GridLayout(3, false);
			mainLayout.marginHeight = 0;
			mainLayout.marginWidth = 0;
			mainLayout.horizontalSpacing = 0;
			mainLayout.verticalSpacing = -1;
			GridData mainLayoutData = new GridData(
					GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
							| GridData.VERTICAL_ALIGN_FILL
							| GridData.GRAB_VERTICAL);
			
			main.setLayoutManager(mainLayout);
			add(main);
			setConstraint(main, mainLayoutData);

			main.add(rect);
			main.setConstraint(rect, rectLayoutData);
			rect.add(instanceNameLabel);
			instanceNameLabel.setBorder(new MarginBorder(4, 0, 4, 0));
			
			GridLayout rectLayout = new GridLayout(2, true);
			rectLayout.marginHeight = 2;
			rectLayout.marginWidth = 0;
			rect.setLayoutManager(rectLayout);
			rect.setConstraint(instanceNameLabel, instanceNameLayout);
			rect.add(new Label(": " + getCastedModel().getSegment().getSegmentType().getName()));
			
		}

		/**
		 * Gets the name.
		 * 
		 * @return the name
		 */
		public Label getName() {
			return instanceNameLabel;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.draw2d.Shape#fillShape(org.eclipse.draw2d.Graphics)
		 */
		@Override
		protected void fillShape(final Graphics graphics) {

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.draw2d.Shape#outlineShape(org.eclipse.draw2d.Graphics)
		 */
		@Override
		protected void outlineShape(final Graphics graphics) {
		}

		@Override
		public int getIntersectionStyle(Point location) {
			if (instanceNameLabel.intersects(new Rectangle(location, new Dimension(1,1)))) {
				return InteractionStyleFigure.REGION_DRAG; // move/drag
			} else {
				return InteractionStyleFigure.REGION_CONNECTION; // connection 
			}
		}

	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(
			final ConnectionEditPart connection) {
		return new ChopboxAnchor(getFigure());
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.Request)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(final Request request) {
		return new ChopboxAnchor(getFigure());
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(
			final ConnectionEditPart connection) {
		return new ChopboxAnchor(getFigure());
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.Request)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(final Request request) {
		return new ChopboxAnchor(getFigure());
	}

	@Override
	protected List<?> getModelSourceConnections() {
		return getCastedModel().getOutConnections();
	}

	@Override
	protected List<?> getModelTargetConnections() {
		return Collections.emptyList();
	}

}
