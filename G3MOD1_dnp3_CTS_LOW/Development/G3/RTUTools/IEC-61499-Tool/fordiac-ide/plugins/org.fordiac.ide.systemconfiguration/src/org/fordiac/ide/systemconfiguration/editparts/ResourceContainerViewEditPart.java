/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.editparts;

import java.util.Collections;
import java.util.List;

import org.eclipse.draw2d.ActionEvent;
import org.eclipse.draw2d.ActionListener;
import org.eclipse.draw2d.Clickable;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.swt.graphics.Image;
import org.fordiac.ide.gef.draw2d.FordiacFigureUtilities;
import org.fordiac.ide.gef.policies.ShowContentEditPolicy;
import org.fordiac.ide.gef.requests.ShowContentRequest;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.UiPackage;
import org.fordiac.ide.systemconfiguration.policies.ResourceViewLayoutEditPolicy;
import org.fordiac.ide.util.imageprovider.FordiacImage;

/**
 * The Class ResourceContainerViewEditPart.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ResourceContainerViewEditPart extends AbstractGraphicalEditPart {

	/** The content adapter. */
	EContentAdapter contentAdapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			Object feature = notification.getFeature();
			if (UiPackage.eINSTANCE.getResourceContainerView_ShowContent()
					.equals(feature)
					|| UiPackage.eINSTANCE.getResourceContainerView_Resources()
							.equals(feature)) {
				refreshChildren();
				updateIcon();
			}
		}
	};

	/**
	 * Update icon.
	 */
	private void updateIcon() {
		getCastedFigure().setOpen(getCastedModel().isShowContent());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (!isActive()) {
			super.activate();
			((Notifier) getModel()).eAdapters().add(contentAdapter);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((Notifier) getModel()).eAdapters().remove(contentAdapter);
		}
	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public ResourceContainerView getCastedModel() {
		return (ResourceContainerView) getModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected List getModelChildren() {
		if (getCastedModel().isShowContent()) {
			return getCastedModel().getResources();
		} else {
			return Collections.EMPTY_LIST;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		return new CollapsibleFigure();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.LAYOUT_ROLE,
				new ResourceViewLayoutEditPolicy());
		installEditPolicy("ShowContentEditPolicy", new ShowContentEditPolicy()); //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#understandsRequest(org.eclipse.gef.Request)
	 */
	@Override
	public boolean understandsRequest(final Request req) {
		return super.understandsRequest(req);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#performRequest(org.eclipse.gef.Request)
	 */
	@Override
	public void performRequest(final Request req) {
		Command cmd = getCommand(req);
		if (cmd != null && cmd.canExecute()) {
			getViewer().getEditDomain().getCommandStack().execute(cmd);
		}
		super.performRequest(req);
	}

	/**
	 * Gets the casted figure.
	 * 
	 * @return the casted figure
	 */
	public CollapsibleFigure getCastedFigure() {
		return (CollapsibleFigure) getFigure();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#addChildVisual(org.eclipse.gef.EditPart,
	 *      int)
	 */
	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof ResourceEditPart) {
			getCastedFigure().getContent().add(child, index);
		} else {
			super.addChildVisual(childEditPart, index);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#removeChildVisual(org.eclipse.gef.EditPart)
	 */
	@Override
	protected void removeChildVisual(final EditPart childEditPart) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof ResourceEditPart) {
			getCastedFigure().getContent().remove(child);
		} else {
			super.removeChildVisual(childEditPart);
		}
	}

	/**
	 * The Class CollapsibleFigure.
	 */
	public class CollapsibleFigure extends Shape {

		/** The open. */
		Image open = FordiacImage.ICON_Open.getImage();

		/** The close. */
		Image close = FordiacImage.ICON_Close.getImage();

		/** The collapse figure. */
		private Label collapseFigure;

		/** The content. */
		private Figure content = new Figure();

		/** The main. */
		private final Figure main = new Figure();

		/**
		 * Instantiates a new collapsible figure.
		 */
		public CollapsibleFigure() {

			FlowLayout layout = new FlowLayout();
			layout.setMajorSpacing(0);
			layout.setMinorSpacing(0);
			layout.setHorizontal(false);
			layout.setStretchMinorAxis(true);
			setLayoutManager(layout);

			ToolbarLayout mainLayout = new ToolbarLayout();
			layout.setMajorSpacing(0);
			layout.setMinorSpacing(0);
			layout.setHorizontal(false);
			layout.setStretchMinorAxis(true);
			add(main);
			main.setLayoutManager(mainLayout);
			main.setBorder(new LineBorder() {
				
				@Override
				public void paint(final IFigure figure,
						final Graphics graphics, final Insets insets) {
					tempRect.setBounds(getPaintRectangle(figure, insets));
					if (1 == (getWidth() & 1)) {
						tempRect.height--;
					}
					tempRect.shrink(getWidth() / 2, getWidth() / 2);
					graphics.setLineWidth(getWidth());
					
					graphics.setBackgroundColor(getBackgroundColor());
					FordiacFigureUtilities.paintEtchedBorder(graphics, 
							new Rectangle(tempRect.x - 3, tempRect.y, tempRect.width + 5, 3));
				}
				
			});

			createCollapseFigure(main);
			

			ToolbarLayout contentLayout = new ToolbarLayout(false);
			content.setLayoutManager(contentLayout);

//			GridData contentPaneLyoutData = new GridData(
//					GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
//							| GridData.VERTICAL_ALIGN_FILL);
			main.add(content);
			//main.setConstraint(content, contentPaneLyoutData);
		}

		private void createCollapseFigure(Figure container) {
			Figure collapseFigureContainer = new Figure();
			
			main.add(collapseFigureContainer);
			collapseFigureContainer.setLayoutManager(new FlowLayout(true));
			
			
			collapseFigure = new Label(getCastedModel().isShowContent() ? open
					: close);
			Clickable cl = new Clickable(collapseFigure);
			cl.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(final ActionEvent event) {
					ShowContentRequest request = new ShowContentRequest(
							!getCastedModel().isShowContent());
					performRequest(request);

				}
			});
			cl.setBorder(new MarginBorder(3,0,0,0));
			collapseFigureContainer.add(cl);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.draw2d.Shape#fillShape(org.eclipse.draw2d.Graphics)
		 */
		@Override
		protected void fillShape(final Graphics graphics) {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.draw2d.Shape#outlineShape(org.eclipse.draw2d.Graphics)
		 */
		@Override
		protected void outlineShape(final Graphics graphics) {
		}

		/**
		 * Gets the content.
		 * 
		 * @return the content
		 */
		public Figure getContent() {
			return content;
		}

		/**
		 * Sets the content.
		 * 
		 * @param content the new content
		 */
		public void setContent(final Figure content) {
			this.content = content;
		}

		/**
		 * Sets the open.
		 * 
		 * @param isOpen the new open
		 */
		public void setOpen(final boolean isOpen) {
			collapseFigure.setIcon(isOpen ? open : close);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		GridData contentPaneLyoutData = new GridData(
				GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL);

		((GraphicalEditPart) getParent()).setLayoutConstraint(this,
				getFigure(), contentPaneLyoutData);

		super.refreshVisuals();
	}

}
