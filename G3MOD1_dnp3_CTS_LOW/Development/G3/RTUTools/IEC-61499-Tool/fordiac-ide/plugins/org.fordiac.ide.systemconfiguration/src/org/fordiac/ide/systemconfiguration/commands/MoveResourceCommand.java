/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;

/**
 * The Class MoverResourceCommand.
 */
public class MoveResourceCommand extends Command {
	
	/** The container. */
	private final ResourceContainerView container;	
	
	/** The res view. */
	private ResourceView child;
	
	/** The old index. */
	private final int oldIndex;

	/** The new index. */
	private int newIndex;

	/**
	 * Instantiates a new move input variable command.
	 * 
	 * @param child the child
	 * @param parent the parent
	 * @param oldIndex the old index
	 * @param newIndex the new index
	 */
	public MoveResourceCommand(final ResourceView child, final ResourceContainerView container, final int oldIndex, final int newIndex) {
		super();
		this.child = child;
		this.container = container;
		this.oldIndex = oldIndex;
		this.newIndex = newIndex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		if (newIndex > oldIndex) {
			newIndex--;
		}
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		((DeviceView) container.eContainer()).getDeviceElement().getResource().move(oldIndex, child.getResourceElement());
		container.getResources().move(oldIndex, child);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		((DeviceView) container.eContainer()).getDeviceElement().getResource().move(newIndex, child.getResourceElement());
		container.getResources().move(newIndex, child);
	}

}
