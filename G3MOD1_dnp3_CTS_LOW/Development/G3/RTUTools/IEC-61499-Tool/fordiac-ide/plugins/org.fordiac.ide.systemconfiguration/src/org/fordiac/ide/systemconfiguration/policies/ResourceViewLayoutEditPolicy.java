/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.policies;

import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.FlowLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.jface.preference.IPreferenceStore;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.policies.ModifiedNonResizeableEditPolicy;
import org.fordiac.ide.gef.preferences.DiagramPreferences;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.systemconfiguration.commands.MoveResourceCommand;
import org.fordiac.ide.systemconfiguration.commands.ResourceCreateCommand;
import org.fordiac.ide.systemconfiguration.editparts.ResourceEditPart;

/**
 * The Class ResourceViewLayoutEditPolicy.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ResourceViewLayoutEditPolicy extends FlowLayoutEditPolicy {
	@Override
	protected EditPolicy createChildEditPolicy(EditPart child) {
		IPreferenceStore pf = Activator.getDefault().getPreferenceStore();
		int cornerDim = pf.getInt(DiagramPreferences.CORNER_DIM);
		if (cornerDim > 1) {
			cornerDim = cornerDim / 2;
		}
		return new ModifiedNonResizeableEditPolicy(cornerDim, new Insets(1));
	}

	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.LayoutEditPolicy#getCreateCommand(org.eclipse
	 * .gef.requests.CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(final CreateRequest request) {
		if (request == null) {
			return null;
		}
		Object childClass = request.getNewObjectType();
		if (childClass instanceof org.fordiac.ide.model.Palette.ResourceTypeEntry) {
			org.fordiac.ide.model.Palette.ResourceTypeEntry type = (org.fordiac.ide.model.Palette.ResourceTypeEntry) request
					.getNewObjectType();
			if (getHost().getModel() instanceof ResourceContainerView) {				
				ResourceContainerView resourceContainerView = (ResourceContainerView)getHost().getModel();					
				int index = -1;
				EditPart ref = getInsertionReference(request);		
				if (ref != null) {
					index = ((DeviceView) resourceContainerView.eContainer()).getDeviceElement().getResource().indexOf(((ResourceEditPart)ref).getCastedModel().getResourceElement());
				}
				
				return new ResourceCreateCommand(type, resourceContainerView, index);
			}
		}
		return null;

	}

	@Override
	protected Command createAddCommand(EditPart child, EditPart after) {
		return null;
	}

	@Override
	protected Command createMoveChildCommand(EditPart child, EditPart after) {
		if (child instanceof ResourceEditPart) {
			ResourceEditPart childEP = (ResourceEditPart) child;			
			
			int oldIndex = getHost().getChildren().indexOf(child);
			int newIndex = -1;
			if (after == null) {
				newIndex = getHost().getChildren().size();
			} else {
				newIndex = getHost().getChildren().indexOf(after);
			}
			return new MoveResourceCommand( (ResourceView) childEP.getModel(), (ResourceContainerView)getHost().getModel(), oldIndex, newIndex);
			

		}
		return null;
	}

}
