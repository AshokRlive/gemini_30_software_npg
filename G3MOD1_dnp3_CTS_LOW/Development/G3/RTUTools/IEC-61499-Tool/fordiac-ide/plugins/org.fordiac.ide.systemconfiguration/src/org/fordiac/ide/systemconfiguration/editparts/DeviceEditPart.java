/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.editparts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Pattern;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.draw2d.FordiacFigureUtilities;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.gef.editparts.ValueEditPart;
import org.fordiac.ide.gef.figures.InteractionStyleFigure;
import org.fordiac.ide.gef.preferences.DiagramPreferences;
import org.fordiac.ide.gef.properties.PropertyUtil;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.VersionInfo;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.systemconfiguration.Messages;
import org.fordiac.ide.systemconfiguration.policies.DeleteDeviceEditPolicy;
import org.fordiac.ide.systemconfiguration.policies.DeviceViewLayoutEditPolicy;
import org.fordiac.ide.systemconfiguration.policies.SegmentNodeEditPolicy;
import org.fordiac.ide.util.ColorHelper;

/**
 * The Class DeviceEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class DeviceEditPart extends AbstractViewEditPart implements
		NodeEditPart {

	public DeviceEditPart(ZoomManager zoomManager) {
		super();
		setConnectable(true);
		this.zoomManager = zoomManager;
	}

	/** The content adapter. */
	EContentAdapter contentAdapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			refreshChildren();
			refreshTargetConnections();
		}

	};

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public DeviceView getCastedModel() {
		return (DeviceView) getModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getContentAdapter()
	 */
	@Override
	public EContentAdapter getContentAdapter() {
		return contentAdapter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getINamedElement()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel().getDeviceElement();
	}

	/**
	 * Gets the casted figure.
	 * 
	 * @return the casted figure
	 */
	private DeviceFigure getCastedFigure() {
		return (DeviceFigure) getFigure();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractGraphicalEditPart#addChildVisual(org.
	 * eclipse.gef.EditPart, int)
	 */
	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof DeviceInterfaceEditPart) {
			getCastedFigure().getDataInputs().add(child);
		} else if (childEditPart instanceof ValueEditPart) {
			ValueEditPart valueEditPart = (ValueEditPart) childEditPart;
			if (((VarDeclaration) valueEditPart.getCastedModel().eContainer())
					.getInputConnections().size() > 0) {
				valueEditPart.setVisible(false);
			}
			if (valueEditPart.getCastedModel().getValue() == null) {
				valueEditPart.setVisible(false);
			}
			getCastedFigure().getInputValuesContentPane().add(child);
		} else if (childEditPart instanceof ResourceContainerViewEditPart) {
			getCastedFigure().getContentPane().add(child);
		} else {
			super.addChildVisual(childEditPart, index);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractGraphicalEditPart#removeChildVisual(org
	 * .eclipse.gef.EditPart)
	 */
	@Override
	protected void removeChildVisual(final EditPart childEditPart) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof DeviceInterfaceEditPart) {
			getCastedFigure().getDataInputs().remove(child);
		} else if (childEditPart instanceof ValueEditPart) {
			getCastedFigure().getInputValuesContentPane().remove(child);
		} else if (childEditPart instanceof ResourceContainerViewEditPart) {
			getCastedFigure().getContentPane().remove(child);
		} else {
			super.removeChildVisual(childEditPart);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected List getModelChildren() {
		ArrayList elements = new ArrayList();
		elements.addAll(getCastedModel().getInterfaceElements());
		for (Iterator iterator = getCastedModel().getInterfaceElements()
				.iterator(); iterator.hasNext();) {
			InterfaceElementView interfaceElementView = (InterfaceElementView) iterator
					.next();
			IInterfaceElement interfaceElement = interfaceElementView
					.getIInterfaceElement();
			if (interfaceElement.getValue() != null) {
				elements.add(interfaceElement.getValue());
			}
		}
		if (getCastedModel().getResourceContainerView() != null) {
			elements.add(getCastedModel().getResourceContainerView());
		}
		return elements;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getInterfaceElements()
	 */
	@Override
	public List<? extends InterfaceElementView> getInterfaceElements() {
		return getCastedModel().getInterfaceElements();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		installEditPolicy(EditPolicy.LAYOUT_ROLE,
				new DeviceViewLayoutEditPolicy());
		installEditPolicy(EditPolicy.COMPONENT_ROLE,
				new DeleteDeviceEditPolicy());

		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE,
				new SegmentNodeEditPolicy());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getNameLabel()
	 */
	@Override
	public Label getNameLabel() {
		return ((DeviceFigure) getFigure()).getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#
	 * getPreferenceChangeListener ()
	 */
	@Override
	public IPropertyChangeListener getPreferenceChangeListener() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#getEditableValue()
	 */
	@Override
	public Object getEditableValue() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#getPropertyValue(java
	 * .lang .Object)
	 */
	@Override
	public Object getPropertyValue(final Object id) {
		return PropertyUtil.getPropertyValue(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#isPropertySet(java.lang
	 * .Object)
	 */
	@Override
	public boolean isPropertySet(final Object id) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getPropertyDescriptors
	 * ()
	 */
	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		List<VarDeclaration> constants = getCastedModel().getDeviceElement()
				.getVarDeclarations();
		IPropertyDescriptor[] descriptors = new IPropertyDescriptor[constants
				.size() * 2 + 1 + 1];
		int i = PropertyUtil.addInstanceNameDescriptor(getCastedModel()
				.getDeviceElement(), descriptors, 0);
		i = PropertyUtil.addVarDeclDescriptors(constants, descriptors, i);
		PropertyUtil.addProfileDescriptor(getCastedModel().getDeviceElement(),
				descriptors, i);
		return descriptors;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#resetPropertyValue(java
	 * .lang.Object)
	 */
	@Override
	public void resetPropertyValue(final Object id) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#setPropertyValue(java
	 * .lang .Object, java.lang.Object)
	 */
	@Override
	public void setPropertyValue(final Object id, final Object value) {
		PropertyUtil.setPropertyValue(id, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#createFigureForModel()
	 */
	@Override
	protected IFigure createFigureForModel() {
		return new DeviceFigure();
	}

	
	/** necessary that the gradient pattern can be scaled accordingly */
	ZoomManager zoomManager;


	private final class DeviceConnectionAnchor extends ChopboxAnchor {
		DeviceConnectionAnchor(IFigure owner) {
			super(owner);
		}

		@Override
		protected Rectangle getBox() {
			// calculate a bounding box which consist of device main part and
			// name label
			Rectangle main = getCastedFigure().getConnectionReferenceFigure()
					.getBounds().getCopy();
			Rectangle top = getCastedFigure().getName().getBounds();

			main.setHeight(main.height + top.height());
			main.y = top.y();

			return main;
		}
	}

	/**
	 * The Class DeviceFigure.
	 */
	public class DeviceFigure extends Shape implements InteractionStyleFigure {
		
		public int getIntersectionStyle(Point location) {
			if (instanceNameLabel.intersects(new Rectangle(location, new Dimension(1,1)))) {
				return InteractionStyleFigure.REGION_DRAG; // move/drag
			} else {
				return InteractionStyleFigure.REGION_CONNECTION; // connection 
			}
		}

		/** The instance name label. */
		protected final Label instanceNameLabel;

		/** The main. */
		Figure main = new Figure();

		/** The bottom. */
		RoundedRectangle bottom = new RoundedRectangle() {

			protected void fillShape(Graphics graphics) {
				Display display = Display.getCurrent();
				Rectangle boundingRect = getBounds().getCopy();

				boundingRect.scale(zoomManager.getZoom());
				Point topLeft = boundingRect.getTopLeft();
				Point bottomRight = boundingRect.getBottomRight();

				Color first = ColorHelper.lighter(getBackgroundColor());

				Pattern pattern = new Pattern(display, topLeft.x, topLeft.y,
						bottomRight.x, bottomRight.y, first,
						getBackgroundColor());
				graphics.setBackgroundPattern(pattern);
				// graphics.fillRectangle(boundingRect);
				graphics.fillRoundRectangle(getBounds(),
						getCornerDimensions().width,
						getCornerDimensions().height);
				graphics.setBackgroundPattern(null);
				pattern.dispose();
				first.dispose();
			}

		};

		/** The bottom inputs. */
		Figure bottomInputs = new Figure();

		/** The data inputs. */
		Figure dataInputs = new Figure();

		/** The input values figure. */
		Figure inputValuesFigure = new Figure();

		/** The top outputs. */
		Figure topOutputs = new Figure();

		/** The device info. */
		protected Figure deviceInfo = new Figure();

		/** The content pane. */
		Figure contentPane;

		/**
		 * Instantiates a new device figure.
		 */
		public DeviceFigure() {

			setBackgroundColor(ColorConstants.white);

			this.setFillXOR(true);

			GridData instanceNameLayout = new GridData();
			instanceNameLayout.grabExcessHorizontalSpace = true;
			instanceNameLayout.horizontalAlignment = SWT.CENTER;

			instanceNameLabel = new Label();
			instanceNameLabel.setText(getINamedElement().getName());
			instanceNameLabel.setTextAlignment(PositionConstants.CENTER);
			// instanceNameLabel.setBorder(new
			// LineBorder(ColorConstants.green));

			// BorderLayout borderLayout = new BorderLayout();
			GridLayout gridLayout = new GridLayout(1, true);
			gridLayout.verticalSpacing = 2;
			gridLayout.marginHeight = 0;
			gridLayout.marginWidth = 0;
			setLayoutManager(gridLayout);

			// add(instanceNameLabel);
			// setConstraint(instanceNameLabel, instanceNameLayout);

			GridLayout mainLayout = new GridLayout(3, false);
			mainLayout.marginHeight = 0;
			mainLayout.marginWidth = 0;
			mainLayout.horizontalSpacing = 0;
			mainLayout.verticalSpacing = -1;
			GridData mainLayoutData = new GridData(
					GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
							| GridData.VERTICAL_ALIGN_FILL
							| GridData.GRAB_VERTICAL);

			// mainLayoutData.widthHint = 120;
			// mainLayoutData.heightHint = 80;
			main.setLayoutManager(mainLayout);
			// main.setBorder(new LineBorder(ColorConstants.red));
			add(main);
			setConstraint(main, mainLayoutData);
			main.add(new Label()); // to fill the layoutmanagers first row
			main.add(instanceNameLabel);
			main.setConstraint(instanceNameLabel, instanceNameLayout);
			main.add(new Label()); // to fill the layoutmanagers first row

			IPreferenceStore pf = Activator.getDefault().getPreferenceStore();
			int cornerDim = pf.getInt(DiagramPreferences.CORNER_DIM);

			bottom.setCornerDimensions(new Dimension(cornerDim, cornerDim));

			GridLayout bottomLayout = new GridLayout(2, false);
			bottomLayout.marginHeight = 4;
			bottomLayout.marginWidth = 1;
			bottomLayout.horizontalSpacing = 2;
			bottom.setLayoutManager(bottomLayout);
			GridData bottomLayoutData = new GridData(
					GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
							| GridData.VERTICAL_ALIGN_FILL
							| GridData.GRAB_VERTICAL);

			bottom.setOutline(false);

			// bottomLayoutData.widthHint = 100;
			// bottomLayoutData.heightHint = 60;
			// bottom.setMinimumSize(new Dimension(100, 40));
			GridLayout bottomInputValuesLayout = new GridLayout();
			bottomInputValuesLayout.marginHeight = 4;
			bottomInputValuesLayout.marginWidth = 2;
			bottomInputValuesLayout.horizontalSpacing = 2;
			GridData bottomILayoutData = new GridData(
					GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
							| GridData.VERTICAL_ALIGN_FILL
							| GridData.GRAB_VERTICAL);
			bottomILayoutData.verticalAlignment = SWT.TOP;

			main.add(bottomInputs);
			main.setConstraint(bottomInputs, bottomILayoutData);
			bottomInputs.setLayoutManager(bottomInputValuesLayout);
			// bottomInputs.setBorder(new LineBorder(ColorConstants.blue));

			ToolbarLayout bottomInputValuesFigureLayout = new ToolbarLayout(
					false);
			bottomInputValuesFigureLayout.setStretchMinorAxis(true);
			GridData bottomInputsFigureLayoutData = new GridData(
					GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
							| GridData.VERTICAL_ALIGN_FILL);
			bottomInputsFigureLayoutData.verticalAlignment = SWT.TOP;
			inputValuesFigure.setLayoutManager(bottomInputValuesFigureLayout);
			inputValuesFigure.setOpaque(false);
			// inputValuesFigure.setBorder(new LineBorder(ColorConstants.red));
			bottomInputs.add(inputValuesFigure);
			bottomInputs.setConstraint(inputValuesFigure,
					bottomInputsFigureLayoutData);
			// inputValuesFigure.setBorder(new LineBorder(ColorConstants.red));

			// bottomInputValuesFigure.add(new Label("test"));

			main.add(bottom);

			main.setConstraint(bottom, bottomLayoutData);

			GridLayout deviceInfoLayout = new GridLayout();
			deviceInfoLayout.marginHeight = 0;
			deviceInfoLayout.horizontalSpacing = 2;
			deviceInfoLayout.verticalSpacing = 2;

			GridData deviceInfoLayoutData = new GridData(
					GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
							| GridData.VERTICAL_ALIGN_FILL);
			deviceInfoLayoutData.verticalAlignment = SWT.TOP;
			deviceInfoLayoutData.horizontalSpan = 2;
			// deviceInfoLayoutData.horizontalAlignment = SWT.CENTER;

			deviceInfo.setLayoutManager(deviceInfoLayout);
			Label l;
			GridData grabL1 = new GridData(GridData.HORIZONTAL_ALIGN_FILL
					| GridData.GRAB_HORIZONTAL);
			deviceInfo.add(l = new Label(getCastedModel().getDeviceElement()
					.getDeviceType().getName()));
			// l.setBorder(new MarginBorder(0, 2, 0, 2));
			l.setTextAlignment(PositionConstants.CENTER);
			deviceInfo.setConstraint(l, grabL1);

			VersionInfo versionInfo = null;
			if (getCastedModel().getDeviceElement().getDeviceType()
					.getVersionInfo().size() > 0) {
				versionInfo = getCastedModel().getDeviceElement()
						.getDeviceType().getVersionInfo().get(0);
			}

			deviceInfo.add(l = new Label(versionInfo != null ? "V" //$NON-NLS-1$
					+ versionInfo.getVersion()
					: Messages.DeviceEditPart_LABEL_NotDefined));
			GridData grabL2 = new GridData(GridData.HORIZONTAL_ALIGN_FILL
					| GridData.GRAB_HORIZONTAL);

			l.setTextAlignment(PositionConstants.CENTER);
			l.setBorder(new MarginBorder(0, 0, 5, 0));
			deviceInfo.setConstraint(l, grabL2);
			// fill inputValuesFigure due to version info elements
			inputValuesFigure.add(l = new Label("")); //$NON-NLS-1$
			l.setBorder(new MarginBorder(3, 0, 8, 0));
			inputValuesFigure.add(new Label("")); //$NON-NLS-1$

			bottom.add(deviceInfo);
			bottom.setConstraint(deviceInfo, deviceInfoLayoutData);

			deviceInfo.setBorder(new LineBorder() {

				@Override
				public void paint(final IFigure figure,
						final Graphics graphics, final Insets insets) {
					tempRect.setBounds(getPaintRectangle(figure, insets));
					if (1 == (getWidth() & 1)) {
						tempRect.height--;
					}
					tempRect.shrink(getWidth() / 2, getWidth() / 2);
					graphics.setLineWidth(getWidth());

					graphics.setBackgroundColor(bottom.getBackgroundColor());
					FordiacFigureUtilities.paintEtchedBorder(graphics,
							new Rectangle(tempRect.x - 3, tempRect.y
									+ tempRect.height - 3, tempRect.width + 5,
									3));
				}

			});

			//
			ToolbarLayout bottomInputsLayout = new ToolbarLayout(false);
			bottomInputsLayout.setStretchMinorAxis(true);
			dataInputs.setLayoutManager(bottomInputsLayout);
			GridData bottomInputsLayoutData = new GridData(
					GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
							| GridData.VERTICAL_ALIGN_FILL);
			dataInputs.setOpaque(false);

			bottom.add(dataInputs);
			bottom.setConstraint(dataInputs, bottomInputsLayoutData);
			createContentPane(bottom);
		}

		private void createContentPane(RoundedRectangle container) {

			GridData contentPaneLyoutData = new GridData(
					GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL
							| GridData.VERTICAL_ALIGN_FILL);
			contentPaneLyoutData.verticalAlignment = SWT.TOP;
			contentPaneLyoutData.horizontalSpan = 2;
			contentPaneLyoutData.grabExcessHorizontalSpace = true;

			contentPane = new Figure();
			contentPane.setLayoutManager(new ToolbarLayout());

			container.add(contentPane);
			container.setConstraint(contentPane, contentPaneLyoutData);
		}

		/**
		 * Gets the name.
		 * 
		 * @return the name
		 */
		public Label getName() {
			return instanceNameLabel;
		}

		public Figure getConnectionReferenceFigure() {
			return bottom;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.draw2d.Shape#fillShape(org.eclipse.draw2d.Graphics)
		 */
		@Override
		protected void fillShape(final Graphics graphics) {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.draw2d.Shape#outlineShape(org.eclipse.draw2d.Graphics)
		 */
		@Override
		protected void outlineShape(final Graphics graphics) {
		}

		/**
		 * Gets the input values content pane.
		 * 
		 * @return the input values content pane
		 */
		public Figure getInputValuesContentPane() {
			return inputValuesFigure;
		}

		/**
		 * Gets the data inputs.
		 * 
		 * @return the data inputs
		 */
		public Figure getDataInputs() {
			return dataInputs;
		}

		/**
		 * Gets the content pane.
		 * 
		 * @return the content pane
		 */
		public Figure getContentPane() {
			return contentPane;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.
	 * ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(
			final ConnectionEditPart connection) {
		return new DeviceConnectionAnchor(getCastedFigure()
				.getConnectionReferenceFigure());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.
	 * Request)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(final Request request) {
		return new DeviceConnectionAnchor(getCastedFigure()
				.getConnectionReferenceFigure());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.
	 * ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(
			final ConnectionEditPart connection) {
		return new DeviceConnectionAnchor(getCastedFigure()
				.getConnectionReferenceFigure());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.
	 * Request)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(final Request request) {
		return new DeviceConnectionAnchor(getCastedFigure()
				.getConnectionReferenceFigure());
	}

	@Override
	protected List<?> getModelSourceConnections() {
		return super.getModelSourceConnections();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected List<?> getModelTargetConnections() {
		ArrayList<Object> connections = new ArrayList<Object>();
		connections.addAll(getCastedModel().getInConnections());
		connections.addAll(super.getModelTargetConnections());
		return connections;
	}

	
	

}
