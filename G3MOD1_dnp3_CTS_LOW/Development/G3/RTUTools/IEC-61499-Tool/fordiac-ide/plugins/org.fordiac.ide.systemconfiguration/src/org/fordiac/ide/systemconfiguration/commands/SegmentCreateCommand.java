/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.model.Palette.SegmentTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.Segment;
import org.fordiac.ide.model.ui.Color;
import org.fordiac.ide.model.ui.SegmentView;
import org.fordiac.ide.model.ui.Size;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.util.ColorHelper;
import org.fordiac.ide.util.Utils;

/**
 * The Class SegmentCreateCommand.
 */
public class SegmentCreateCommand extends Command {

	/** The editor. */
	private IEditorPart editor;

	/** The type. */
	private final SegmentTypePaletteEntry type;

	/** The parent. */
	protected final UISystemConfiguration parent;

	/** The bounds. */
	private final Rectangle bounds;

	private Segment segment;

	/**
	 * Instantiates a new segment create command.
	 * 
	 * @param type the type
	 * @param parent the parent
	 * @param bounds the bounds
	 */
	public SegmentCreateCommand(final SegmentTypePaletteEntry type,
			final UISystemConfiguration parent, final Rectangle bounds) {
		super();
		this.type = type;
		this.parent = parent;
		this.bounds = bounds;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();

		setLabel(getLabel()
				+ "(" + (editor != null ? editor.getTitle() : "") + ")"); //$NON-NLS-1$ //$NON-NLS-2$
		if (parent != null) {

			segment = LibraryElementFactory.eINSTANCE.createSegment();
			segment.setPaletteEntry(type);
			segment.setTypePath(type.getProjectRelativeTypePath());

			segment.getVarDeclarations()
					.addAll(
							EcoreUtil.copyAll(type.getSegmentType()
									.getVarDeclaration()));

			segment.setName(type.getSegmentType().getName());

			parent.getSystemConfigNetwork().getSegments().add(segment);

			createSegmentView(segment);

			parent.getChildren().add(segmentView);
		}

	}

	private SegmentView segmentView;

	private void createSegmentView(final Segment segment) {
		segmentView = UiFactory.eINSTANCE.createSegmentView();
		org.fordiac.ide.model.ui.Position pos = UiFactory.eINSTANCE
				.createPosition();
		pos.setX(bounds.x);
		pos.setY(bounds.y);

		Size size = UiFactory.eINSTANCE.createSize();
		size.setHeight(-1);
		size.setWidth((-1 != bounds.width) ? bounds.width : 300);

		segmentView.setPosition(pos);
		segmentView.setSize(size);
		segmentView.setSegment(segment);
		
		Color color = UiFactory.eINSTANCE.createColor();
		RGB randomColor = ColorHelper.createRandomColor();
		color.setBlue(randomColor.blue);
		color.setRed(randomColor.red);
		color.setGreen(randomColor.green);
		segmentView.setBackgroundColor(color);

	}
}
