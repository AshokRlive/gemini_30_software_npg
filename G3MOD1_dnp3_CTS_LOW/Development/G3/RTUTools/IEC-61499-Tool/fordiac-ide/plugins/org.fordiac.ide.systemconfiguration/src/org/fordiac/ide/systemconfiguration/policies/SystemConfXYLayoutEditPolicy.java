/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.policies;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ResizableEditPolicy;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.handles.ResizeHandle;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.fordiac.ide.gef.commands.ViewSetPositionCommand;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.gef.policies.ModifiedMoveHandle;
import org.fordiac.ide.gef.policies.ModifiedNonResizeableEditPolicy;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.systemconfiguration.commands.DeviceCreateCommand;
import org.fordiac.ide.systemconfiguration.commands.SegmentCreateCommand;
import org.fordiac.ide.systemconfiguration.commands.SegmentSetConstraintCommand;
import org.fordiac.ide.systemconfiguration.editparts.SegmentEditPart;

/**
 * The Class SystemConfXYLayoutEditPolicy.
 */
public class SystemConfXYLayoutEditPolicy extends XYLayoutEditPolicy {
	@Override
	protected EditPolicy createChildEditPolicy(EditPart child) {
		if (child instanceof SegmentEditPart) {
			return new ResizableEditPolicy() {
				@SuppressWarnings({ "unchecked", "rawtypes" })
				@Override
				protected List createSelectionHandles() {
					List list = new ArrayList();
					list.add(new ResizeHandle((GraphicalEditPart) getHost(),
							PositionConstants.EAST));
					list.add(new ResizeHandle((GraphicalEditPart) getHost(),
							PositionConstants.WEST));
					list.add(new ModifiedMoveHandle((GraphicalEditPart) getHost(),
							new Insets(0, 2, 0, 2), 20));
					return list;
				}
			};
		} else {
			return new ModifiedNonResizeableEditPolicy();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#createAddCommand
	 * (org.eclipse.gef.EditPart, java.lang.Object)
	 */
	@Override
	protected Command createAddCommand(final EditPart child,
			final Object constraint) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.LayoutEditPolicy#getDeleteDependantCommand
	 * (org.eclipse.gef.Request)
	 */
	@Override
	protected Command getDeleteDependantCommand(final Request request) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ConstrainedLayoutEditPolicy#createChangeConstraintCommand(ChangeBoundsRequest
	 * , EditPart, Object)
	 */
	@Override
	protected Command createChangeConstraintCommand(
			final ChangeBoundsRequest request, final EditPart child,
			final Object constraint) {
		if (child instanceof SegmentEditPart && constraint instanceof Rectangle) {
			SegmentEditPart seg = (SegmentEditPart) child;
			Rectangle rec = new Rectangle(((Rectangle) constraint).x,
					((Rectangle) constraint).y, ((Rectangle) constraint).width,
					((Rectangle) constraint).height);
			SegmentSetConstraintCommand cmd = new SegmentSetConstraintCommand();
			cmd.setNewBounds(rec);
			cmd.setSegmentView(seg.getCastedModel());
			cmd.setRequest(request);
			return cmd;
		}
		// return a command that can move a "View"
		if (child instanceof AbstractViewEditPart
				&& constraint instanceof Rectangle) {
			AbstractViewEditPart temp = (AbstractViewEditPart) child;
			return new ViewSetPositionCommand((View) temp.getModel(), request,
					(Rectangle) constraint);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ConstrainedLayoutEditPolicy#createChangeConstraintCommand(EditPart,
	 * Object)
	 */
	@Override
	protected Command createChangeConstraintCommand(final EditPart child,
			final Object constraint) {
		// not used in this example
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see LayoutEditPolicy#getCreateCommand(CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(final CreateRequest request) {
		if (request == null) {
			return null;
		}
		Object childClass = request.getNewObjectType();
		Rectangle constraint = (Rectangle) getConstraintFor(request);
		if (childClass instanceof org.fordiac.ide.model.Palette.DeviceTypePaletteEntry) {
			org.fordiac.ide.model.Palette.DeviceTypePaletteEntry type = (org.fordiac.ide.model.Palette.DeviceTypePaletteEntry) request
					.getNewObjectType();
			if (getHost().getModel() instanceof UISystemConfiguration) {
				return new DeviceCreateCommand(type, (UISystemConfiguration) getHost()
						.getModel(), new Rectangle(constraint.getLocation().x, constraint
						.getLocation().y, -1, -1));
			}
		}
		if (childClass instanceof org.fordiac.ide.model.Palette.SegmentTypePaletteEntry) {
			org.fordiac.ide.model.Palette.SegmentTypePaletteEntry type = (org.fordiac.ide.model.Palette.SegmentTypePaletteEntry) request
					.getNewObjectType();
			if (getHost().getModel() instanceof UISystemConfiguration) {
				return new SegmentCreateCommand(type, (UISystemConfiguration) getHost()
						.getModel(), constraint); 
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#getAddCommand(
	 * org.eclipse.gef.Request)
	 */
	@Override
	protected Command getAddCommand(final Request generic) {
		return null;
	}

}
