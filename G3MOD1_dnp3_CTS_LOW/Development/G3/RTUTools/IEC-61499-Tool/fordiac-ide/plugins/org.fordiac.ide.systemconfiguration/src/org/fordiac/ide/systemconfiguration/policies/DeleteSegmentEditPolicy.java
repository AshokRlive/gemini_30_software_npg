/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.GroupRequest;
import org.fordiac.ide.model.ui.SegmentView;
import org.fordiac.ide.systemconfiguration.commands.SegmentDeleteCommand;
import org.fordiac.ide.systemconfiguration.editparts.SegmentEditPart;

/**
 * An EditPolicy which returns a command for deleting.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class DeleteSegmentEditPolicy extends
		org.eclipse.gef.editpolicies.ComponentEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.ComponentEditPolicy#createDeleteCommand(org.eclipse.gef.requests.GroupRequest)
	 */
	@Override
	protected Command createDeleteCommand(final GroupRequest request) {
		if (getHost() instanceof SegmentEditPart) {
			SegmentView segmentView = ((SegmentEditPart) getHost())
					.getCastedModel();
			SegmentDeleteCommand c = new SegmentDeleteCommand();
			c.setSegmentView(segmentView);

			return c;
		}
		return null;
	}

}
