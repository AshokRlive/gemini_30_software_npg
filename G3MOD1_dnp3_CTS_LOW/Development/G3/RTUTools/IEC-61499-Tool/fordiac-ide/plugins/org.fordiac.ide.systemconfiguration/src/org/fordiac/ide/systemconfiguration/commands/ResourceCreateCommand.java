/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.commands;

import java.util.Hashtable;
import java.util.Iterator;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.application.utilities.ApplicationUIFBNetworkManager;
import org.fordiac.ide.model.Palette.ResourceTypeEntry;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.ResourceFBNetwork;
import org.fordiac.ide.model.libraryElement.ResourceType;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.ResizeUIFBNetwork;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.Size;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.systemconfiguration.editor.SystemConfigurationEditor;
import org.fordiac.ide.util.Utils;
import org.fordiac.systemmanagement.SystemManager;
import org.fordiac.systemmanagement.util.UIResourceEditorContentAdapter;

// TODO: Auto-generated Javadoc
/**
 * The Class ResourceCreateCommand.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ResourceCreateCommand extends Command {

	/** The type. */
	private final ResourceTypeEntry entry;

	/** The container. */
	private final ResourceContainerView container;

	/** The resource. */
	private Resource resource;

	/** The res view. */
	private ResourceView resView;

	/** The ui resource editor. */
	private UIResourceEditor uiResourceEditor;

	/** The editor. */
	private IEditorPart editor;

	/** The resource editor content adapter. */
	private UIResourceEditorContentAdapter resourceEditorContentAdapter;

	/** The system. */
	private AutomationSystem system;

	private int index = -1;

	/**
	 * The Constructor.
	 * 
	 * @param type the type
	 * @param container the container
	 * @param system the system
	 */
	public ResourceCreateCommand(final ResourceTypeEntry entry,
		final ResourceContainerView container,
			AutomationSystem system) {
		this.entry = entry;
		this.container = container;
		this.system = system;
	}

	/**
	 * Instantiates a new resource create command.
	 * 
	 * @param type the type
	 * @param container the container
	 */
	public ResourceCreateCommand(final ResourceTypeEntry entry,
		 final ResourceContainerView container, int index) {
		this.entry = entry;
		this.container = container;
		this.index  = index;
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();

		// ResourceType type = typeEntry.getResourceType();

		resource = LibraryElementFactory.eINSTANCE.createResource();
		resource.setType(entry.getProjectRelativeTypePath());
		resource.setPaletteEntry(entry);
		resource.getVarDeclarations().addAll(
				EcoreUtil.copyAll(entry.getResourceType().getVarDeclaration()));

		for (Iterator<VarDeclaration> iterator = resource.getVarDeclarations()
				.iterator(); iterator.hasNext();) {
			IInterfaceElement element = iterator.next();
			Value value = LibraryElementFactory.eINSTANCE.createValue();
			element.setValue(value);
		}

		resView = UiFactory.eINSTANCE.createResourceView();
		resView.setResourceElement(resource);
		resView.setDeviceTypeResource(false);

		SystemManager.getInstance().addResourceViewForResourceMapping(resource,
				resView);

		uiResourceEditor = UiFactory.eINSTANCE.createUIResourceEditor();
		uiResourceEditor.setResourceElement(resource);
		Size size = UiFactory.eINSTANCE.createSize();
		size.setHeight(250);
		size.setWidth(-1);
		uiResourceEditor.setMappingEditorSize(size);
		if (system == null && editor != null) {
			system = ((SystemConfigurationEditor) editor).getSystem();
		}
		if (system != null) {
			system.eAdapters().add(
					resourceEditorContentAdapter = new UIResourceEditorContentAdapter(
							uiResourceEditor));
			SystemManager.getInstance().addSystemDiagramMapping(uiResourceEditor,
					system);
		}
		if (entry.getResourceType().getFBNetwork() != null) {
			// ResourceFBNetwork resourceFBNetwork = (ResourceFBNetwork)
			// EcoreUtil
			// .copy(type.getFBNetwork());
			ResourceFBNetwork resourceFBNetwork = LibraryElementFactory.eINSTANCE
					.createResourceFBNetwork();
			resource.setFBNetwork(resourceFBNetwork);

			createResourceTypeNetwork(resource, uiResourceEditor, entry.getResourceType(),
					resourceFBNetwork);
		} else {
			ResourceFBNetwork resourceFBNetwork = LibraryElementFactory.eINSTANCE
					.createResourceFBNetwork();

			resource.setFBNetwork(resourceFBNetwork);
		}

		boolean first = true;
		if(null != system) {
			for (Application app : system.getApplication()) {
				FBNetwork fbNetwork = app.getFBNetwork();
				UIFBNetwork uiFBNetwork = (UIFBNetwork) fbNetwork.eContainer();
				ResizeUIFBNetwork resizeUIFBNetwork = UiFactory.eINSTANCE
						.createResizeUIFBNetwork();
				resizeUIFBNetwork.setUiFBNetwork(uiFBNetwork);
	
				if (first) {
					uiResourceEditor.setActiveNetwork(resizeUIFBNetwork);
					first = false;
				}
				uiResourceEditor.getFbNetworks().add(resizeUIFBNetwork);
	
			}
		}
		resView.setUIResourceDiagram(uiResourceEditor);
		
		if (index < 0 || index > container.getResources().size()) {
			index = container.getResources().size();
		}

		((DeviceView) container.eContainer()).getDeviceElement().getResource().add(index, resource);
		resource.setName(entry.getLabel());   //resource name needs to be added after it is inserted in the device so that name checking works
		resource.setIdentifier(true);
		container.getResources().add(index, resView);
		
		SystemManager.getInstance().notifyListeners();
	}

	/**
	 * Creates the resource type network.
	 * 
	 * @param resource the resource
	 * @param uiResourceEditor the ui resource editor
	 * @param type the type
	 * @param resourceFBNetwork the resource fb network
	 */
	public static void createResourceTypeNetwork(final Resource resource,
			final UIResourceEditor uiResourceEditor, final ResourceType type,
			final ResourceFBNetwork resourceFBNetwork) {
		Hashtable<String, Event> events = new Hashtable<String, Event>();
		Hashtable<String, VarDeclaration> varDecls = new Hashtable<String, VarDeclaration>();

		for (Iterator<FB> iterator = type.getFBNetwork().getFBs().iterator(); iterator
				.hasNext();) {
			FB fb = iterator.next();
			FB copy = LibraryElementFactory.eINSTANCE.createResourceTypeFB();
			
			
			copy.setResourceTypeFB(true);
			resource.getFBNetwork().getFBs().add(copy);
			copy.setId(EcoreUtil.generateUUID());
			copy.setIdentifier(true);
			copy.setFbtPath(fb.getFbtPath());
			copy.setPaletteEntry(fb.getPaletteEntry());
			copy.setName(fb.getName());    //name should be last so that checks are working correctly
			
			InterfaceList interfaceList = LibraryElementFactory.eINSTANCE
					.createInterfaceList();

			for (Iterator<VarDeclaration> iterator2 = fb.getInterface()
					.getOutputVars().iterator(); iterator2.hasNext();) {
				VarDeclaration varDecl = iterator2.next();
				VarDeclaration varDeclCopy = LibraryElementFactory.eINSTANCE
						.createVarDeclaration();
				varDeclCopy.setType(varDecl.getType());
				varDeclCopy.setName(varDecl.getName());
				varDeclCopy.setComment(varDecl.getComment());
				varDeclCopy.setIsInput(varDecl.isIsInput());
				if (varDecl.getValue() != null) {
					varDeclCopy.setValue((Value) EcoreUtil.copy(varDecl.getValue()));
				}
				varDecls.put(fb.getName() + "." + varDeclCopy.getName(), //$NON-NLS-1$
						varDeclCopy);
				interfaceList.getOutputVars().add(varDeclCopy);
			}
			for (Iterator<VarDeclaration> iterator2 = fb.getInterface()
					.getInputVars().iterator(); iterator2.hasNext();) {
				VarDeclaration varDecl = iterator2.next();
				VarDeclaration varDeclCopy = LibraryElementFactory.eINSTANCE
						.createVarDeclaration();
				varDeclCopy.setType(varDecl.getType());
				varDeclCopy.setName(varDecl.getName());
				varDeclCopy.setComment(varDecl.getComment());
				varDeclCopy.setIsInput(varDecl.isIsInput());
				if (varDecl.getValue() != null) {
					varDeclCopy.setValue((Value) EcoreUtil.copy(varDecl.getValue()));
				}
				varDecls.put(fb.getName() + "." + varDeclCopy.getName(), //$NON-NLS-1$
						varDeclCopy);
				interfaceList.getInputVars().add(varDeclCopy);
			}
			for (Iterator<Event> iterator2 = fb.getInterface().getEventInputs()
					.iterator(); iterator2.hasNext();) {
				Event event = iterator2.next();
				Event eventCopy = LibraryElementFactory.eINSTANCE.createEvent();
				// eventCopy.setType(event.getType());
				eventCopy.setName(event.getName());
				eventCopy.setComment(event.getComment());
				eventCopy.setIsInput(event.isIsInput());
				if (event.getValue() != null) {
					eventCopy.setValue((Value) EcoreUtil.copy(event.getValue()));
				}
				events.put(fb.getName() + "." + event.getName(), eventCopy); //$NON-NLS-1$
				interfaceList.getEventInputs().add(eventCopy);
			}
			for (Iterator<Event> iterator2 = fb.getInterface().getEventOutputs()
					.iterator(); iterator2.hasNext();) {
				Event event = iterator2.next();
				Event eventCopy = LibraryElementFactory.eINSTANCE.createEvent();
				// eventCopy.setType(event.getType());
				eventCopy.setName(event.getName());
				eventCopy.setComment(event.getComment());
				eventCopy.setIsInput(event.isIsInput());
				if (event.getValue() != null) {
					eventCopy.setValue((Value) EcoreUtil.copy(event.getValue()));
				}
				events.put(fb.getName() + "." + event.getName(), eventCopy); //$NON-NLS-1$
				interfaceList.getEventOutputs().add(eventCopy);
			}
			copy.setInterface(interfaceList);
			copy.setPosition(EcoreUtil.copy(fb.getPosition()));
			
			FBView view = ApplicationUIFBNetworkManager.createFBView(copy, resourceFBNetwork);
			uiResourceEditor.getChildren().add(view);
		}

		for (Iterator<EventConnection> iterator2 = type.getFBNetwork()
				.getEventConnections().iterator(); iterator2.hasNext();) {
			EventConnection eventCon = iterator2.next();
			if (eventCon.getSource() != null && eventCon.getDestination() != null) {
				FB sourceFB = (FB) eventCon.getSource().eContainer().eContainer();
				FB destFB = (FB) eventCon.getDestination().eContainer().eContainer();

				Event source = events.get(sourceFB.getName() + "." //$NON-NLS-1$
						+ eventCon.getSource().getName());
				Event dest = events.get(destFB.getName() + "." //$NON-NLS-1$
						+ eventCon.getDestination().getName());

				EventConnection copyEventCon = LibraryElementFactory.eINSTANCE
						.createEventConnection();
				copyEventCon.setSource(source);
				copyEventCon.setDestination(dest);
				copyEventCon.setResTypeConnection(true);
				resourceFBNetwork.getEventConnections().add(copyEventCon); // TODO
				// check
				// this

				ConnectionView conView = UiFactory.eINSTANCE.createConnectionView();
				conView.setConnectionElement(copyEventCon);
				conView.setSource(getInterfaceElement(uiResourceEditor, sourceFB,
						source));
				conView.setDestination(getInterfaceElement(uiResourceEditor, destFB,
						dest));
				uiResourceEditor.getConnections().add(conView);
			} else {
				// TODO error log -> no valid event connection!
			}
		}
		for (Iterator<DataConnection> iterator2 = type.getFBNetwork()
				.getDataConnections().iterator(); iterator2.hasNext();) {
			DataConnection dataCon = iterator2.next();
			if (dataCon.getSource() != null && dataCon.getDestination() != null) {
				FB sourceFB = (FB) dataCon.getSource().eContainer().eContainer();
				FB destFB = (FB) dataCon.getDestination().eContainer().eContainer();

				VarDeclaration source = varDecls.get(sourceFB.getName() + "." //$NON-NLS-1$
						+ dataCon.getSource().getName());
				VarDeclaration dest = varDecls.get(destFB.getName() + "." //$NON-NLS-1$
						+ dataCon.getDestination().getName());

				DataConnection copyDataCon = LibraryElementFactory.eINSTANCE
						.createDataConnection();
				copyDataCon.setSource(source);
				copyDataCon.setDestination(dest);
				copyDataCon.setResTypeConnection(true);
				resourceFBNetwork.getDataConnections().add(copyDataCon);

				ConnectionView conView = UiFactory.eINSTANCE.createConnectionView();
				conView.setConnectionElement(copyDataCon);
				conView.setSource(getInterfaceElement(uiResourceEditor, sourceFB,
						source));
				conView.setDestination(getInterfaceElement(uiResourceEditor, destFB,
						dest));
				uiResourceEditor.getConnections().add(conView);
			} else {
				// TODO error log -> no valid data connection
			}
		}
	}

	/**
	 * Gets the interface element.
	 * 
	 * @param uiResourceEditor the ui resource editor
	 * @param fb the fb
	 * @param element the element
	 * 
	 * @return the interface element
	 */
	private static InterfaceElementView getInterfaceElement(
			final UIResourceEditor uiResourceEditor, final FB fb,
			final IInterfaceElement element) {
		for (Iterator<View> iterator = uiResourceEditor.getChildren().iterator(); iterator
				.hasNext();) {
			View view = iterator.next();
			if (view instanceof FBView) {
				FBView fbView = (FBView) view;
				if (fbView.getFb().getName().equals(fb.getName())) {
					for (Iterator<InterfaceElementView> iterator3 = fbView
							.getInterfaceElements().iterator(); iterator3.hasNext();) {
						InterfaceElementView interfaceElementView = iterator3.next();
						if (interfaceElementView.getIInterfaceElement().equals(element)) {
							return interfaceElementView;
						}
					}
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		((DeviceView) container.eContainer()).getDeviceElement().getResource()
				.remove(resource);
		container.getResources().remove(resView);
		resView.setUIResourceDiagram(null);
		org.fordiac.ide.model.libraryElement.AutomationSystem system = ((SystemConfigurationEditor) editor)
				.getSystem();
		system.eAdapters().remove(resourceEditorContentAdapter);
		SystemManager.getInstance().removeResourceViewForResourceMapping(resource);

		SystemManager.getInstance().removeSystemDiagramMapping(uiResourceEditor);
		SystemManager.getInstance().notifyListeners();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		((DeviceView) container.eContainer()).getDeviceElement().getResource().add(index, resource);
		container.getResources().add(index, resView);
		resView.setUIResourceDiagram(uiResourceEditor);
		org.fordiac.ide.model.libraryElement.AutomationSystem system = ((SystemConfigurationEditor) editor)
				.getSystem();
		system.eAdapters().add(resourceEditorContentAdapter);

		SystemManager.getInstance().addResourceViewForResourceMapping(resource,
				resView);

		SystemManager.getInstance().addSystemDiagramMapping(uiResourceEditor,
				system);
		SystemManager.getInstance().notifyListeners();
	}

	/**
	 * Gets the resource.
	 * 
	 * @return the resource
	 */
	public Resource getResource() {
		return resource;
	}

	/**
	 * Gets the resource view.
	 * 
	 * @return the resource view
	 */
	public ResourceView getResView() {
		return resView;
	}
	
	
	/**
	 * Gets the uI resource editor.
	 * 
	 * @return the uI resource editor
	 */
	public UIResourceEditor getUIResourceEditor() {
		return uiResourceEditor;
	}

}
