/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.commands;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.LinkView;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.util.Utils;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The Class DeviceDeleteCommand.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class DeviceDeleteCommand extends Command {

	/** The editor. */
	private IEditorPart editor;

	/** The device. */
	private Device device;

	/** The dev view. */
	private final DeviceView devView;

	/** The parent. */
	private Diagram parent;

	/** The device parent. */
	private SystemConfigurationNetwork deviceParent;

	/** The resource container. */
	private ResourceContainerView resourceContainer;

	/** The delete links. */
	private CompoundCommand deleteLinks;

	/**
	 * Instantiates a new device delete command.
	 * 
	 * @param devView the dev view
	 */
	public DeviceDeleteCommand(final DeviceView devView) {
		this.devView = devView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		device = devView.getDeviceElement();
		parent = (Diagram) devView.eContainer();

		deviceParent = (SystemConfigurationNetwork) device.eContainer();

		removeResources();

		removeLinks();

		devView.setResourceContainerView(null);

		deviceParent.getDevices().remove(device);
		parent.getChildren().remove(devView);
		
		SystemManager.getInstance().notifyListeners();
	}

	private void removeLinks() {
		deleteLinks = new CompoundCommand();
		
		for (LinkView linkView : devView.getInConnections()) {
			DeleteLinkCommand cmd = new DeleteLinkCommand();
			cmd.setLinkView(linkView);
			deleteLinks.add(cmd);
		}
		deleteLinks.execute();
	}

	/** The resource delete cm ds. */
	ArrayList<ResourceDeleteCommand> resourceDeleteCMDs = new ArrayList<ResourceDeleteCommand>();

	/**
	 * Removes the resources.
	 */
	private void removeResources() {
		resourceContainer = devView.getResourceContainerView();

		for (Iterator<ResourceView> iterator = resourceContainer.getResources()
				.iterator(); iterator.hasNext();) {
			ResourceView resourceView = iterator.next();
			ResourceDeleteCommand cmd = new ResourceDeleteCommand(resourceView);
			resourceDeleteCMDs.add(cmd);
		}

		for (Iterator<ResourceDeleteCommand> iterator = resourceDeleteCMDs
				.iterator(); iterator.hasNext();) {
			ResourceDeleteCommand cmd = iterator.next();
			cmd.execute();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		devView.setResourceContainerView(resourceContainer);
		deviceParent.getDevices().add(device);
		parent.getChildren().add(devView);
		restoreResources();
		deleteLinks.undo();
		SystemManager.getInstance().notifyListeners();
	}

	/**
	 * Restore resources.
	 */
	private void restoreResources() {
		for (Iterator<ResourceDeleteCommand> iterator = resourceDeleteCMDs
				.iterator(); iterator.hasNext();) {
			ResourceDeleteCommand cmd = iterator.next();
			cmd.undo();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		redoRemoveResources();

		devView.setResourceContainerView(null);

		deviceParent.getDevices().remove(device);
		parent.getChildren().remove(devView);
		deleteLinks.redo();
		SystemManager.getInstance().notifyListeners();
	}

	/**
	 * Redo remove resources.
	 */
	private void redoRemoveResources() {
		for (Iterator<ResourceDeleteCommand> iterator = resourceDeleteCMDs
				.iterator(); iterator.hasNext();) {
			ResourceDeleteCommand cmd = iterator.next();
			cmd.redo();
		}
	}

}
