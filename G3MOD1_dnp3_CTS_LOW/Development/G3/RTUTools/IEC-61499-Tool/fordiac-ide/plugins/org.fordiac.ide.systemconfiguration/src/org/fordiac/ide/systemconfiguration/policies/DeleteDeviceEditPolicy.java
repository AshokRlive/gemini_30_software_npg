/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.GroupRequest;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.systemconfiguration.commands.DeviceDeleteCommand;
import org.fordiac.ide.systemconfiguration.editparts.DeviceEditPart;

/**
 * An EditPolicy which returns a command for deleting.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class DeleteDeviceEditPolicy extends
		org.eclipse.gef.editpolicies.ComponentEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.ComponentEditPolicy#createDeleteCommand(org.eclipse.gef.requests.GroupRequest)
	 */
	@Override
	protected Command createDeleteCommand(final GroupRequest request) {
		if (getHost() instanceof DeviceEditPart) {
			DeviceView deviceView = ((DeviceEditPart) getHost())
					.getCastedModel();
			DeviceDeleteCommand c = new DeviceDeleteCommand(deviceView);
			return c;
		}
		return null;
	}

}
