/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.eclipse.swt.graphics.RGB;
import org.fordiac.ide.application.policies.FeedbackConnectionEndpointEditPolicy;
import org.fordiac.ide.gef.router.BendpointPolicyRouter;
import org.fordiac.ide.gef.router.RouterUtil;
import org.fordiac.ide.model.ui.Color;
import org.fordiac.ide.model.ui.LinkView;
import org.fordiac.ide.systemconfiguration.policies.DeleteLinkEditPolicy;
import org.fordiac.ide.systemconfiguration.routers.LinkConnectionRouter;
import org.fordiac.ide.util.ColorManager;

/**
 * The Class ConnectionEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class LinkEditPart extends AbstractConnectionEditPart {

	
	/** The adapter. */
	private final EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			updateLinkColor((PolylineConnection) getFigure());
		}


	};
	
	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public LinkView getCastedModel() {
		return (LinkView) getModel();
	}

	protected void updateLinkColor(PolylineConnection connection) {
		if(null != getCastedModel().getSource()){
			Color segmentColor = getCastedModel().getSource().getBackgroundColor();
			if(null != segmentColor){				
				org.eclipse.swt.graphics.Color newColor = 
						ColorManager.getColor(new RGB(segmentColor.getRed(),
								segmentColor.getGreen(), segmentColor.getBlue()));
		
				connection.setForegroundColor(newColor);
			}
		}
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// Selection handle edit policy.
		// Makes the connection show a feedback, when selected by the user.
		installEditPolicy(EditPolicy.CONNECTION_ENDPOINTS_ROLE,
				new FeedbackConnectionEndpointEditPolicy(2, 4));

		// Allows the removal of the link model element
		installEditPolicy(EditPolicy.CONNECTION_ROLE,
				new DeleteLinkEditPolicy());
		
		if (getConnectionFigure().getConnectionRouter() instanceof BendpointPolicyRouter) {
			installEditPolicy(EditPolicy.CONNECTION_BENDPOINTS_ROLE,
					((BendpointPolicyRouter) getConnectionFigure()
							.getConnectionRouter())
							.getBendpointPolicy(getCastedModel()));
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractConnectionEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		PolylineConnection connection;
		
		connection = RouterUtil.getConnectionRouterFactory(null).createConnectionFigure();		
		connection.setConnectionRouter(new LinkConnectionRouter(this));
		connection.setLineWidth(3);
		updateLinkColor(connection);
		
		return connection;
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (!isActive()) {
			super.activate();	
			getCastedModel().eAdapters().add(adapter);
			getCastedModel().getSource().eAdapters().add(adapter);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			getCastedModel().eAdapters().remove(adapter);
			if(null != getCastedModel().getSource()){
				getCastedModel().getSource().eAdapters().remove(adapter);
			}
		}
	}

}
