/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.editparts;

import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.gef.policies.AbstractViewRenameEditPolicy;
import org.fordiac.ide.gef.properties.PropertyUtil;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.UiPackage;
import org.fordiac.ide.resourceediting.editors.ResourceDiagramEditor;
import org.fordiac.ide.resourceediting.editors.ResourceEditorInput;
import org.fordiac.ide.systemconfiguration.Activator;
import org.fordiac.ide.systemconfiguration.policies.DeleteResourceEditPolicy;
import org.fordiac.ide.util.imageprovider.FordiacImage;

/**
 * The Class ResourceEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ResourceEditPart extends AbstractViewEditPart {

	/** The figure. */
	private ResourceFigure figure;

	/** The content adapter. */
	private final EContentAdapter contentAdapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			Object feature = notification.getFeature();
			if (UiPackage.eINSTANCE.getInterfaceElementView_InConnections().equals(
					feature)
					|| UiPackage.eINSTANCE.getInterfaceElementView_OutConnections()
							.equals(feature)) {
				refresh();
			}
			super.notifyChanged(notification);
		}

	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#refreshName()
	 */
	@Override
	public void refreshName() {
		getNameLabel().setText(getINamedElement().getName());
	}

	/**
	 * The Class ResourceFigure.
	 */
	public class ResourceFigure extends Figure {

		/** The instance name. */
		private final Label instanceName;

		/** The type info. */
		private final Label typeInfo;

		/**
		 * Instantiates a new resource figure.
		 */
		public ResourceFigure() {
			GridLayout mainLayout = new GridLayout(2, false);
			mainLayout.marginHeight = 2;
			setLayoutManager(mainLayout);
			if (getINamedElement() == null) {
				instanceName = new Label("N/D");

			} else {
				instanceName = new Label(getINamedElement().getName());
			}
			if (getCastedModel().isDeviceTypeResource()) {
				instanceName.setIcon(FordiacImage.ICON_FirmwareResource.getImage());
			}
			add(instanceName);
			String type = "N/D";
			if (getCastedModel().getResourceElement() != null) {
				type = getCastedModel().getResourceElement().getResourceTypeName();
			}

			typeInfo = new Label("(" //$NON-NLS-1$
					+ type + ")"); //$NON-NLS-1$
			add(typeInfo);
			setOpaque(false);
		}

		/**
		 * Gets the instance name.
		 * 
		 * @return the instance name
		 */
		public Label getInstanceName() {
			return instanceName;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#createFigureForModel()
	 */
	@Override
	protected IFigure createFigureForModel() {
		if (figure == null) {
			figure = new ResourceFigure();
		}
		return figure;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		// nothing to do
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new DeleteResourceEditPolicy());
		
		// EditPolicy which allows the direct edit of the Instance Name
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new AbstractViewRenameEditPolicy());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractEditPart#understandsRequest(org.eclipse
	 * .gef.Request)
	 */
	@Override
	public boolean understandsRequest(final Request req) {
		if (getCastedModel().isDeviceTypeResource()) {
			return false;
		}
		return super.understandsRequest(req);
	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public ResourceView getCastedModel() {
		return (ResourceView) getModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractGraphicalEditPart#addChildVisual(org.
	 * eclipse.gef.EditPart, int)
	 */
	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		// super.addChildVisual(childEditPart, index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getContentAdapter()
	 */
	@Override
	public EContentAdapter getContentAdapter() {
		return contentAdapter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getINamedElement()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel().getResourceElement();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getNameLabel()
	 */
	@Override
	public Label getNameLabel() {
		return ((ResourceFigure) getFigure()).getInstanceName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getPreferenceChangeListener
	 * ()
	 */
	@Override
	public IPropertyChangeListener getPreferenceChangeListener() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#getEditableValue()
	 */
	@Override
	public Object getEditableValue() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#getPropertyValue(java.lang
	 * .Object)
	 */
	@Override
	public Object getPropertyValue(final Object id) {
		return PropertyUtil.getPropertyValue(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getPropertyDescriptors()
	 */
	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		List<VarDeclaration> constants = getCastedModel().getResourceElement()
				.getVarDeclarations();
		IPropertyDescriptor[] descriptors = new IPropertyDescriptor[constants
				.size() * 2 + 1];
		int i = PropertyUtil.addInstanceNameDescriptor(getCastedModel()
				.getResourceElement(), descriptors, 0);
		i = PropertyUtil.addVarDeclDescriptors(constants, descriptors, i);
		return descriptors;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#isPropertySet(java.lang
	 * .Object)
	 */
	@Override
	public boolean isPropertySet(final Object id) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#resetPropertyValue(java
	 * .lang.Object)
	 */
	@Override
	public void resetPropertyValue(final Object id) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#setPropertyValue(java.lang
	 * .Object, java.lang.Object)
	 */
	@Override
	public void setPropertyValue(final Object id, final Object value) {
		PropertyUtil.setPropertyValue(id, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#backgroundColorChanged()
	 */
	@Override
	protected void backgroundColorChanged(IFigure figure) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getInterfaceElements()
	 */
	@Override
	public List<? extends InterfaceElementView> getInterfaceElements() {
		return null;
	}
	
	@Override
	public void performRequest(final Request request) {
		if (request.getType() == RequestConstants.REQ_OPEN) {		
			ResourceEditorInput input = new ResourceEditorInput(getCastedModel().getResourceElement());
			
			IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			try {
				activePage.openEditor(input,ResourceDiagramEditor.class.getName());
			} catch (PartInitException e) {
				Activator.getDefault().logError(e.getMessage(), e);
			}
		}else{
			super.performRequest(request);
		}		
	}

}
