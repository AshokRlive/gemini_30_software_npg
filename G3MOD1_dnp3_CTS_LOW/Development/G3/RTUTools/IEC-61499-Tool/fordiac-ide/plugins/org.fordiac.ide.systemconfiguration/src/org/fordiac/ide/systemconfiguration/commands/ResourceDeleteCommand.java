/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.commands;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.gef.DiagramEditorWithFlyoutPalette;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.Utils;
import org.fordiac.ide.util.commands.UnmapCommand;
import org.fordiac.ide.util.commands.UnmapSubAppCommand;
import org.fordiac.ide.util.editors.EditorUtils;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The Class ResourceDeleteCommand.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ResourceDeleteCommand extends Command {

	/** The editor. */
	private IEditorPart editor;

	/** The view parent. */
	private ResourceContainerView viewParent;

	/** The device. */
	private Device device;

	/** The res view. */
	private final ResourceView resView;

	/** The res. */
	private Resource res;

	/** The unmap cmds. */
	private final ArrayList<UnmapCommand> unmapCmds = new ArrayList<UnmapCommand>();

	/** The sub unmap cmds. */
	private final ArrayList<UnmapSubAppCommand> subUnmapCmds = new ArrayList<UnmapSubAppCommand>();

	/**
	 * Instantiates a new resource delete command.
	 * 
	 * @param resView the res view
	 */
	public ResourceDeleteCommand(final ResourceView resView) {
		this.resView = resView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return !resView.isDeviceTypeResource();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		res = resView.getResourceElement();
		if (res != null) {
			closeResourceEditor();   // we need to do this before any model changes so that the editor closes cleanly
			device = (Device) res.eContainer();
			if (device != null && device.getResource() != null) {
				device.getResource().remove(res);
			}
			
			for (View view : resView.getUIResourceDiagram().getChildren()) {				
				//TODO this is partly duplicated code from the UnmapAction
				if (view instanceof FBView) {
					FBView fbView = (FBView) view;
					if (fbView.getMappedFB() != null) {
						UnmapCommand cmd = new UnmapCommand(fbView.getMappedFB());
						unmapCmds.add(cmd);
					}else if (fbView.getApplicationFB() != null) {
						UnmapCommand cmd = new UnmapCommand(fbView);
						unmapCmds.add(cmd);
					}
				}else if (view instanceof SubAppView) {
					SubAppView subAppView = (SubAppView) view;
					if (null != subAppView.getMappedSubApp()) {
						UnmapSubAppCommand cmd = new UnmapSubAppCommand(subAppView.getMappedSubApp());
						subUnmapCmds.add(cmd);
					}
				}else if (view instanceof MappedSubAppView) {
					UnmapSubAppCommand cmd = new UnmapSubAppCommand((MappedSubAppView) view);
					subUnmapCmds.add(cmd);
				}
			}

			executeUnmapCommands();
		}
		viewParent = ((ResourceContainerView) resView.eContainer());
		viewParent.getResources().remove(resView);

		SystemManager.getInstance().notifyListeners();
	}

	/** Close the editor if the an editor for the deleted resource is opend
	 * 
	 */
	private void closeResourceEditor() {
		EditorUtils.closeEditorsFiltered((IEditorPart editor) -> {
			return (editor instanceof DiagramEditorWithFlyoutPalette) && (
					resView.getUIResourceDiagram().equals(((DiagramEditorWithFlyoutPalette)editor).getDiagramModel()));
		});
	}

	/**
	 * Execute unmap commands.
	 */
	private void executeUnmapCommands() {
		for (Iterator<UnmapCommand> iterator = unmapCmds.iterator(); iterator
				.hasNext();) {
			UnmapCommand cmd = iterator.next();
			if (cmd.canExecute()) {
				cmd.execute();
			}
		}
		for (Iterator<UnmapSubAppCommand> iterator = subUnmapCmds.iterator(); iterator
				.hasNext();) {
			UnmapSubAppCommand cmd = iterator.next();
			if (cmd.canExecute()) {
				cmd.execute();
			}
		}
	}

	/**
	 * Undo unmap commands.
	 */
	private void undoUnmapCommands() {
		for (Iterator<UnmapCommand> iterator = unmapCmds.iterator(); iterator
				.hasNext();) {
			UnmapCommand cmd = iterator.next();
			if (cmd.canUndo()) {
				cmd.undo();
			}
		}
		for (Iterator<UnmapSubAppCommand> iterator = subUnmapCmds.iterator(); iterator
				.hasNext();) {
			UnmapSubAppCommand cmd = iterator.next();
			if (cmd.canUndo()) {
				cmd.undo();
			}
		}
	}

	/**
	 * Redo unmap commands.
	 */
	private void redoUnmapCommands() {
		for (Iterator<UnmapCommand> iterator = unmapCmds.iterator(); iterator
				.hasNext();) {
			UnmapCommand cmd = iterator.next();
			if (cmd.canUndo()) {
				cmd.redo();
			}
		}
		for (Iterator<UnmapSubAppCommand> iterator = subUnmapCmds.iterator(); iterator
				.hasNext();) {
			UnmapSubAppCommand cmd = iterator.next();
			if (cmd.canUndo()) {
				cmd.redo();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		device.getResource().add(res);
		viewParent.getResources().add(resView);
		undoUnmapCommands();
		SystemManager.getInstance().notifyListeners();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		device.getResource().remove(res);
		viewParent.getResources().remove(resView);
		redoUnmapCommands();
		SystemManager.getInstance().notifyListeners();
	}

}
