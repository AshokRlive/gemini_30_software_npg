/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.editor;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.palette.FlyoutPaletteComposite.FlyoutPreferences;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.jface.util.TransferDropTargetListener;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.gef.DiagramEditorWithFlyoutPalette;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.SystemConfiguration;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.systemconfiguration.editparts.SystemConfEditPartFactory;
import org.fordiac.systemmanagement.ISystemEditor;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The main editor for editing system configurations.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class SystemConfigurationEditor extends DiagramEditorWithFlyoutPalette implements ISystemEditor{

	/** The sys conf. */
	SystemConfiguration sysConf;

	/**
	 * Instantiates a new system configuration editor.
	 */
	public SystemConfigurationEditor() {
		// setEditDomain(new DefaultEditDomain(this));
	}

	@Override
	protected EditPartFactory getEditPartFactory() {
		return new SystemConfEditPartFactory(this, getZoomManger());
	}

	@Override
	protected ContextMenuProvider getContextMenuProvider(
			final ScrollingGraphicalViewer viewer,
			final ZoomManager zoomManager) {
		return new SystemConfigurationContextMenueProvider(viewer, zoomManager,
				getActionRegistry());
	}

	@Override
	protected TransferDropTargetListener createTransferDropTargetListener() {
		return new SysConfTemplateTransferDropTargetListener(getViewer(), getSystem());
	}

	@Override
	protected void setModel(final IEditorInput input) {
		if (input instanceof org.fordiac.ide.util.PersistableUntypedEditorInput) {
			org.fordiac.ide.util.PersistableUntypedEditorInput untypedInput = (org.fordiac.ide.util.PersistableUntypedEditorInput) input;
			Object content = untypedInput.getContent();
			if (content instanceof SystemConfiguration) {
				sysConf = (SystemConfiguration) content;
				setDiagramModel((UISystemConfiguration) sysConf
						.getSystemConfigurationNetwork().eContainer());

				if (input.getName() != null) {
					setPartName(input.getName());
				}
			}
			super.setModel(untypedInput);
		}
	}

	@Override
	public AutomationSystem getSystem() {
		return (AutomationSystem) sysConf.eContainer();
	}

	@Override
	public String getFileName() {
		return "SysConf.xml"; //$NON-NLS-1$
	}

	@Override
	public void doSave(final IProgressMonitor monitor) {
		// TODO __gebenh error handling if save fails!
		SystemManager.getInstance().saveSystem(getSystem(), true);

		getCommandStack().markSaveLocation();
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	@Override
	protected FlyoutPreferences getPalettePreferences() {
		return SystemConfPaletteFactory.createPalettePreferences();
	}

	@Override
	protected PaletteRoot getPaletteRoot() {

		if (getDiagramModel() != null && getSystem() != null) {
			return SystemConfPaletteFactory.createPalette(getSystem());
		}
		return new PaletteRoot();
	}

	public void selectDevice(Device refDev) {
		DeviceView devView = getDeviceView(refDev);
		
		if(null != devView){
			EditPart editPart = (EditPart)getViewer().getEditPartRegistry().get(devView);
			getViewer().select(editPart);
			getViewer().reveal(editPart);
		}		
	}

	private DeviceView getDeviceView(Device refDev) {		
		for (View view : getDiagramModel().getChildren()) {
			if (view instanceof DeviceView){
				DeviceView devView = (DeviceView)view;
				if(devView.getDeviceElement().equals(refDev)){
					return devView;
				}
			}
		}
		return null;
	}
	
}
