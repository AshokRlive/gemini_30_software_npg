/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.model.libraryElement.I4DIACElement;
import org.fordiac.ide.model.libraryElement.SystemConfiguration;
import org.fordiac.ide.model.libraryElement.impl.SystemConfigurationImpl;
import org.fordiac.ide.systemconfiguration.Activator;
import org.fordiac.ide.systemconfiguration.editor.SystemConfigurationEditor;
import org.fordiac.ide.systemconfiguration.editor.SystemConfigurationEditorInput;
import org.fordiac.ide.util.OpenListener;

/**
 * The Class OpenSystemConfEditorAction.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class OpenSystemConfEditorAction extends OpenListener {

	/** The sys conf. */
	private SystemConfiguration sysConf;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action
	 * .IAction, org.eclipse.ui.IWorkbenchPart)
	 */
	@Override
	public void setActivePart(final IAction action,
			final IWorkbenchPart targetPart) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(final IAction action) {
		
		SystemConfigurationEditorInput input = new SystemConfigurationEditorInput(sysConf); 

		IWorkbenchPage activePage = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		try {
			editor = activePage.openEditor(input, SystemConfigurationEditor.class.getName());
		} catch (PartInitException e) {
			editor = null;
			Activator.getDefault().logError(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action
	 * .IAction, org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(final IAction action, final ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSel = (IStructuredSelection) selection;
			if (structuredSel.getFirstElement() instanceof SystemConfiguration) {
				sysConf = (SystemConfiguration) structuredSel.getFirstElement();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.systemmanagement.OpenListener#supportsObject(java.lang.Class)
	 */
	@Override
	public boolean supportsObject(final Class<? extends I4DIACElement> clazz) {
		return clazz != null && clazz.equals(SystemConfigurationImpl.class);
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.util.OpenListener#getOpenListenerAction()
	 */
	@Override
	public Action getOpenListenerAction() {
		return new OpenListenerAction(this);
	}

}
