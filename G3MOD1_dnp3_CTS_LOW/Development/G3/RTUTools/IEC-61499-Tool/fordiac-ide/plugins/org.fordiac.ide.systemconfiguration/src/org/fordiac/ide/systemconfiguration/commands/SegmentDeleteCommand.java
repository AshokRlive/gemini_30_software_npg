/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.model.libraryElement.Segment;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.LinkView;
import org.fordiac.ide.model.ui.SegmentView;
import org.fordiac.ide.util.Utils;

/**
 * The Class SegmentDeleteCommand.
 */
public class SegmentDeleteCommand extends Command {
	/** The editor. */
	private IEditorPart editor;
	/** The delete links. */
	private CompoundCommand deleteLinks;

	/** The segment. */
	private SegmentView segmentView;

	/** The segment. */
	private Segment segment;

	/** The parent. */
	private Diagram parent;

	/** The segment parent. */
	private SystemConfigurationNetwork segmentParent;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		removeLinks();
		segment = segmentView.getSegment();

		parent = (Diagram) segmentView.eContainer();

		segmentParent = (SystemConfigurationNetwork) segment.eContainer();

		removeLinks();

		segmentParent.getSegments().remove(segment);
		parent.getChildren().remove(segmentView);

	}

	private void removeLinks() {
		deleteLinks = new CompoundCommand();
		for (LinkView linkView : segmentView.getOutConnections()) {
			DeleteLinkCommand cmd = new DeleteLinkCommand();
			cmd.setLinkView(linkView);
			deleteLinks.add(cmd);
		}
		deleteLinks.execute();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		segmentParent.getSegments().add(segment);
		parent.getChildren().add(segmentView);
		deleteLinks.undo();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		segmentParent.getSegments().remove(segment);
		parent.getChildren().remove(segmentView);
		deleteLinks.redo();
	}

	/**
	 * Gets the segment view.
	 * 
	 * @return the segment view
	 */
	public SegmentView getSegmentView() {
		return segmentView;
	}

	/**
	 * Sets the segment view.
	 * 
	 * @param segmentView the new segment view
	 */
	public void setSegmentView(final SegmentView segmentView) {
		this.segmentView = segmentView;
	}
}
