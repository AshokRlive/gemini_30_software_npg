/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.editparts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.ui.parts.GraphicalEditor;
import org.fordiac.ide.gef.editparts.Abstract4diacEditPartFactory;
import org.fordiac.ide.gef.editparts.ValueEditPart;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.LinkView;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.SegmentView;
import org.fordiac.ide.model.ui.UISystemConfiguration;

/**
 * A factory for creating new EditParts.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class SystemConfEditPartFactory  extends Abstract4diacEditPartFactory {

	/**Editparts that have gradient fill require a zoom manger to correctly scale the pattern when zoomed*/
	protected ZoomManager zoomManager;
	
	
	public SystemConfEditPartFactory(GraphicalEditor editor, ZoomManager zoomManager) {
		super(editor);
		this.zoomManager = zoomManager;
	}
	
	@Override
	protected EditPart getPartForElement(final EditPart context,
			final Object modelElement) {
		if (modelElement instanceof UISystemConfiguration) {
			return new SystemNetworkEditPart();
		}
		if (modelElement instanceof LinkView) {
			return new LinkEditPart();
		}
		if (modelElement instanceof DeviceView) {
			return new DeviceEditPart(zoomManager);
		}
		if (modelElement instanceof InterfaceElementView) {
			return new DeviceInterfaceEditPart();
		}
		if (modelElement instanceof Value) {
			ValueEditPart part = new ValueEditPart();
			part.setContext(context);
			return part;
		}
		if (modelElement instanceof SegmentView) {
			return new SegmentEditPart(zoomManager);
		}
		if (modelElement instanceof ResourceContainerView) {
			return new ResourceContainerViewEditPart();
		}
		if (modelElement instanceof ResourceView) {
			return new ResourceEditPart();
		}		
		throw createEditpartCreationException(modelElement);

	}

}
