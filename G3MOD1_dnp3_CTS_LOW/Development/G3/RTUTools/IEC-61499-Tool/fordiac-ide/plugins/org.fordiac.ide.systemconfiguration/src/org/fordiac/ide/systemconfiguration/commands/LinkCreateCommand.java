/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.Link;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.LinkView;
import org.fordiac.ide.model.ui.SegmentView;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.UiFactory;

/**
 * The Class LinkCreateCommand.
 */
public class LinkCreateCommand extends Command {

	private SegmentView source;
	private DeviceView destination;

	private boolean segmentDeviceLink;

	private UISystemConfiguration uiSystemConfiguration;

	/**
	 * Gets the ui system configuration.
	 * 
	 * @return the ui system configuration
	 */
	public UISystemConfiguration getUiSystemConfiguration() {
		return uiSystemConfiguration;
	}

	/**
	 * Sets the ui system configuration.
	 * 
	 * @param uiSystemConfiguration the new ui system configuration
	 */
	public void setUiSystemConfiguration(
			final UISystemConfiguration uiSystemConfiguration) {
		this.uiSystemConfiguration = uiSystemConfiguration;
	}

	/**
	 * Gets the source.
	 * 
	 * @return the source
	 */
	public SegmentView getSource() {
		return source;
	}

	/**
	 * Sets the source.
	 * 
	 * @param source the new source
	 */
	public void setSource(final SegmentView source) {
		this.source = source;
	}

	/**
	 * Gets the destination.
	 * 
	 * @return the destination
	 */
	public DeviceView getDestination() {
		return destination;
	}

	/**
	 * Sets the destination.
	 * 
	 * @param destination the new destination
	 */
	public void setDestination(final DeviceView destination) {
		this.destination = destination;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return source != null && destination != null && canCreateLink();
	}

	private boolean canCreateLink() {

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		Link link = LibraryElementFactory.eINSTANCE.createLink();
		LinkView linkView = UiFactory.eINSTANCE.createLinkView();
		linkView.setLink(link);
		uiSystemConfiguration.getSystemConfigNetwork().getLinks().add(link);
		uiSystemConfiguration.getConnections().add(linkView);
		source.getOutConnections().add(linkView);
		destination.getInConnections().add(linkView);
		source.getSegment().getOutConnections().add(link);
		destination.getDeviceElement().getInConnections().add(link);

	}

	/**
	 * Checks if is segment device link.
	 * 
	 * @return true, if is segment device link
	 */
	public boolean isSegmentDeviceLink() {
		return segmentDeviceLink;
	}

	/**
	 * Sets the segment device link.
	 * 
	 * @param segmentDeviceLink the new segment device link
	 */
	public void setSegmentDeviceLink(final boolean segmentDeviceLink) {
		this.segmentDeviceLink = segmentDeviceLink;
	}
}
