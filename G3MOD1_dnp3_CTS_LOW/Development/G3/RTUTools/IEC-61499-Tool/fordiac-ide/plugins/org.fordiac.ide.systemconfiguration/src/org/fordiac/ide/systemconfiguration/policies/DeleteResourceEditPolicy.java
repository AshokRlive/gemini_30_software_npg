/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.GroupRequest;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.systemconfiguration.commands.ResourceDeleteCommand;
import org.fordiac.ide.systemconfiguration.editparts.ResourceEditPart;

/**
 * An EditPolicy which returns a command for deleting.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class DeleteResourceEditPolicy extends
		org.eclipse.gef.editpolicies.ComponentEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.ComponentEditPolicy#createDeleteCommand(org.eclipse.gef.requests.GroupRequest)
	 */
	@Override
	protected Command createDeleteCommand(final GroupRequest request) {
		if (getHost() instanceof ResourceEditPart) {
			ResourceView resView = ((ResourceEditPart) getHost())
					.getCastedModel();
			ResourceDeleteCommand c = new ResourceDeleteCommand(resView);
			return c;
		}
		return null;
	}

}
