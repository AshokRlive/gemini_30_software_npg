/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.policies;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.ReconnectRequest;
import org.fordiac.ide.systemconfiguration.commands.LinkCreateCommand;
import org.fordiac.ide.systemconfiguration.editparts.DeviceEditPart;
import org.fordiac.ide.systemconfiguration.editparts.SegmentEditPart;
import org.fordiac.ide.systemconfiguration.editparts.SystemNetworkEditPart;

/**
 * The Class SegmentNodeEditPolicy.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class SegmentNodeEditPolicy extends
		org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#
	 * getConnectionCompleteCommand
	 * (org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCompleteCommand(
			final CreateConnectionRequest request) {
		if (request.getStartCommand() instanceof LinkCreateCommand) {
			LinkCreateCommand command = (LinkCreateCommand) request.getStartCommand();
			if (command.isSegmentDeviceLink()) {
				if (getHost() instanceof DeviceEditPart) {
					command.setDestination(((DeviceEditPart) getHost()).getCastedModel());
				} else if (getHost() instanceof SegmentEditPart) {
					command.setDestination(null);
				}
			} else {
				if (getHost() instanceof SegmentEditPart) {
					command.setSource(((SegmentEditPart) getHost()).getCastedModel());
				} else if (getHost() instanceof DeviceEditPart) {
					command.setSource(null);
				}
				// else if (getHost() instanceof DeviceEditPart) {
				// command.setSource(null);
				// }
			}
			return command;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getConnectionCreateCommand
	 * (org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCreateCommand(
			final CreateConnectionRequest request) {

		LinkCreateCommand cmd = new LinkCreateCommand();
		if (getHost() instanceof SegmentEditPart) {
			cmd.setSource(((SegmentEditPart) getHost()).getCastedModel());
			cmd.setSegmentDeviceLink(true);
		} else if (getHost() instanceof DeviceEditPart) {
			cmd.setDestination(((DeviceEditPart) getHost()).getCastedModel());
		}
		EditPart parent = getHost().getParent();
		if (parent instanceof SystemNetworkEditPart) {
			SystemNetworkEditPart systemNetworkEditPart = (SystemNetworkEditPart) parent;
			cmd.setUiSystemConfiguration(systemNetworkEditPart.getCastedModel());
		}
		request.setStartCommand(cmd);
		return cmd;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getReconnectTargetCommand
	 * (org.eclipse.gef.requests.ReconnectRequest)
	 */
	@Override
	protected Command getReconnectTargetCommand(final ReconnectRequest request) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getReconnectSourceCommand
	 * (org.eclipse.gef.requests.ReconnectRequest)
	 */
	@Override
	protected Command getReconnectSourceCommand(final ReconnectRequest request) {

		return null;
	}

}
