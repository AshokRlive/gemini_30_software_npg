/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.editparts;

import org.eclipse.gef.DragTracker;
import org.eclipse.gef.Request;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gef.tools.SelectEditPartTracker;
import org.fordiac.ide.gef.editparts.PropertiesInterfaceEditPart;

/**
 * The Class DeviceInterfaceEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class DeviceInterfaceEditPart extends PropertiesInterfaceEditPart {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getNodeEditPolicy()
	 */
	@Override
	protected GraphicalNodeEditPolicy getNodeEditPolicy() {
		return null;
	}
	
	@Override
	public DragTracker getDragTracker(Request request) {
		return new SelectEditPartTracker(this);
	}
	
}
