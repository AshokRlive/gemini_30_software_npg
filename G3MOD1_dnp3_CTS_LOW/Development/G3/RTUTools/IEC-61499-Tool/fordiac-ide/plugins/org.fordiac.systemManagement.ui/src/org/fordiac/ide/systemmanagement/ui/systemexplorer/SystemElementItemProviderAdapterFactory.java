/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemmanagement.ui.systemexplorer;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.ecore.EObject;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.provider.DeviceItemProvider;
import org.fordiac.ide.model.libraryElement.provider.FBItemProvider;
import org.fordiac.ide.model.libraryElement.provider.LibraryElementItemProviderAdapterFactory;

class SystemElementItemProviderAdapterFactory extends LibraryElementItemProviderAdapterFactory {

	@Override
	public Adapter createApplicationAdapter() {
		if (applicationItemProvider == null) {
			applicationItemProvider = new ApplicationItemProviderForSystem(this);
		}
		return applicationItemProvider;
	}

	@Override
	public Adapter createSystemConfigurationAdapter() {
		if (systemConfigurationItemProvider == null) {
			systemConfigurationItemProvider = new SystemConfigurationItemProviderForSystem(this);
		}
		return systemConfigurationItemProvider;
	}

	@Override
	public Adapter createSubAppAdapter() {
		if (subAppItemProvider == null) {
			subAppItemProvider = new SubAppItemProviderForSystem(this);
		}
		return subAppItemProvider;
	}

	@Override
	public Adapter createDeviceAdapter() {
		if (deviceItemProvider == null) {
			deviceItemProvider = new DeviceItemProvider(this){
				/**We are not showing the model parent of devices (i.e., systemconfiguration network) 
				 * in the tree. Therefore we need to provide the correct tree parent here to ensure 
				 * correct CNF behavior.
				 */
				@Override
				public Object getParent(Object object) {
					if(null != ((Device)object).getSystemConfigurationNetwork()){
						return ((Device)object).getSystemConfigurationNetwork().getSystemConfiguration();
					}
					return null;
				}
			};
		}

		return deviceItemProvider;
	}

	@Override
	public Adapter createFBAdapter() {
		if (fbItemProvider == null) {
			fbItemProvider = new FBItemProvider(this){
				
				/** we are not showing the real parent of FBs (i.e., FBNetwork or SubAppNetwork)
				 *  in the tree. In order to ensure correct CNF behavior we need to provide a special getparent
				 */
				@Override
				public Object getParent(Object object) {
					
					EObject cont = ((FB)object).eContainer();
					if(cont instanceof FBNetwork){
						return ((FBNetwork)cont).getApplication();
					}else if (cont instanceof SubAppNetwork){
						return ((SubAppNetwork)cont).getParentSubApp();
					}
					return super.getParent(object);
				}
			};
		}
		return fbItemProvider;
	}
	
	
	
}