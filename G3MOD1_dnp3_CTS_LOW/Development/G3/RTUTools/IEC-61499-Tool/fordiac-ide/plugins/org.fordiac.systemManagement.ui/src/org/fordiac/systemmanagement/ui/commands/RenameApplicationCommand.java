/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.ui.commands;

import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The Class NewAppCommand.
 */
public class RenameApplicationCommand extends AbstractOperation {

	
	private String newApplicationName;
	private String oldApplicationName;	
	private Application application;
	
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return true;

	}

	public RenameApplicationCommand(Application application, String newApplicationName) {
		super("Rename Application");
		this.application = application;
		this.newApplicationName = newApplicationName;
	}

	/**
	 * checks whether all required information for creating a new App are set.
	 * 
	 * @return true, if can execute
	 */
	@Override
	public boolean canExecute() {
		return null != application && newApplicationName != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public IStatus execute(IProgressMonitor monitor, IAdaptable info) {
		oldApplicationName = application.getName();
		return redo(monitor, info);
	}

	/**
	 * Redo.
	 * 
	 * @see RenameApplicationCommand#execute()
	 */
	@Override
	public IStatus redo(IProgressMonitor monitor, IAdaptable info) {
		setApplicationName(newApplicationName);
		return Status.OK_STATUS;
	}

	/**
	 * undo of FBCreateCommand.
	 */
	@Override
	public IStatus undo(IProgressMonitor monitor, IAdaptable info) {
		setApplicationName(oldApplicationName);
		return Status.OK_STATUS;
	}
	
	private void setApplicationName(String applicationName){
		if(null != application){
			AutomationSystem system = application.getAutomationSystem();
			
			SystemManager.getInstance().saveSystem(system, false);
			application.setName(applicationName);
			
			EObject obj = application.getFBNetwork().eContainer();
			
			SystemManager.getInstance().saveDiagram((Diagram)obj, system,
					application.getName() + ".xml"); //$NON-NLS-1$
			
			SystemManager.getInstance().saveSystem(system, true); // to save the	
		}
	}
}