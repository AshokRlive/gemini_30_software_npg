/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.ui.wizard;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.SystemConfiguration;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;
import org.fordiac.ide.model.libraryElement.VersionInfo;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.systemmanagement.SystemManager;
import org.fordiac.systemmanagement.ui.Messages;
import org.fordiac.systemmanagement.util.SystemPaletteManagement;

/**
 * The Class NewSystemWizard.
 */
public class NewSystemWizard extends Wizard implements INewWizard {

	/** The page. */
	NewSystemPage page;

	private String projectName;

	private boolean defaultPalette = true;

	private boolean useWizard = true;

	/**
	 * Instantiates a new new system wizard.
	 */
	public NewSystemWizard() {
		setWindowTitle(Messages.NewSystemWizard_WizardName);
	}

	/**
	 * Instantiates a new new system wizard.
	 */
	public NewSystemWizard(String projectName, boolean defaultPalette,
			boolean open) {
		setWindowTitle(Messages.NewSystemWizard_WizardName);
		this.projectName = projectName;
		this.defaultPalette = defaultPalette;
		this.useWizard = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		page = new NewSystemPage(
				Messages.NewSystemWizard_WizardName);
		page.setTitle(Messages.NewSystemWizard_WizardName);
		page.setDescription(Messages.NewSystemWizard_WizardDesc);

		addPage(page);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.IWizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		try {
			WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
				@Override
				protected void execute(IProgressMonitor monitor) {
					createProject(monitor != null ? monitor
							: new NullProgressMonitor());
				}
			};
			getContainer().run(false, true, op);
		} catch (InvocationTargetException x) {
			return false;
		} catch (InterruptedException x) {
			return false;
		}
		// everything worked fine
		return true;
	}

	protected String[] getNatureIDs() {
		return new String[] { SystemManager.DISTRIBUTED_PROJECT_NATURE_ID, "org.fordiac.systemmanagement.nature.IEC61499" }; //$NON-NLS-1$ 
	}
	
	/**
	 * Creates a new project in the workspace.
	 * 
	 * @param monitor
	 *            the monitor
	 */
	public AutomationSystem createProject(final IProgressMonitor monitor) {
		try {
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

			String projectName = "";
			boolean defaultPalette = true;
			IPath location = null;

			if (useWizard) {
				projectName = page.getProjectName();
				defaultPalette = page.importDefaultPalette();
				location = page.getLocationPath();
			} else {
				projectName = this.projectName;
				defaultPalette = this.defaultPalette;
				location = Platform.getLocation();
				projectName = SystemManager.getValidSystemName(projectName);
			}

			IProject project = root.getProject(projectName);
			IProjectDescription description = ResourcesPlugin.getWorkspace()
					.newProjectDescription(project.getName());
			if (!Platform.getLocation().equals(location)) {
				description.setLocation(location);
			}

			description
					.setNatureIds(getNatureIDs()); 

			project.create(description, monitor);
			project.open(monitor);

			// create the system
			org.fordiac.ide.model.libraryElement.AutomationSystem system = LibraryElementFactory.eINSTANCE
					.createAutomationSystem();

			system.setName(projectName);

			VersionInfo verInfo = LibraryElementFactory.eINSTANCE
					.createVersionInfo();
			verInfo.setAuthor("Author");
			verInfo.setOrganization("4DIAC-Consortium");
			verInfo.setVersion("1.0");
			system.getVersionInfo().add(verInfo);

			// configure palette

			if (defaultPalette) {
				SystemPaletteManagement.copyToolTypeLibToProject(project);
			}
			// create PhysicalConfiguration
			SystemConfiguration sysConf = LibraryElementFactory.eINSTANCE
					.createSystemConfiguration();
			system.setSystemConfiguration(sysConf);

			SystemConfigurationNetwork sysConfNetwork = LibraryElementFactory.eINSTANCE
					.createSystemConfigurationNetwork();
			sysConf.setSystemConfigurationNetwork(sysConfNetwork);

			UISystemConfiguration uiSysConf = UiFactory.eINSTANCE
					.createUISystemConfiguration();
			uiSysConf.setSystemConfigNetwork(sysConfNetwork);

			SystemManager.getInstance().addSystemDiagramMapping(uiSysConf,
					system);

			// add system to system management after palette is configured,
			// otherwise the palette is not correctly initialzed
			SystemManager.getInstance().addSystem(system, project);

			SystemManager.getInstance().saveDiagram(uiSysConf, system,
					"SysConf.xml"); //$NON-NLS-1$

			SystemManager.getInstance().saveSystem(system, true);
			return system;

		} catch (CoreException x) {
			// TODO __gebenh log error
		} finally {
			monitor.done();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
	 * org.eclipse.jface.viewers.IStructuredSelection)
	 */
	@Override
	public void init(final IWorkbench workbench,
			final IStructuredSelection selection) {
	}

}
