/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.ui.linkhelpers;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.fordiac.ide.application.editors.ApplicationEditorInput;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.application.editors.SubApplicationEditorInput;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.UISubAppNetwork;


/** Application and Subapplication linking need to be performed in one class as it has the same trigger classes
 * and FB selection would not work otherwise.
 * 
 */
public class ApplicationSubAppEditorLinkHelper extends AbstractEditorLinkHelper {

	@Override
	public IStructuredSelection findSelection(IEditorInput anInput) {		
		if(anInput instanceof ApplicationEditorInput){
			ApplicationEditorInput appInput = (ApplicationEditorInput)anInput;
			return new StructuredSelection(appInput.getApplication());
		}else if (anInput instanceof SubApplicationEditorInput){
			SubApplicationEditorInput subAppInput = (SubApplicationEditorInput)anInput;
			return new StructuredSelection(subAppInput.getSubApp());
		}
		return StructuredSelection.EMPTY;
	}

	@Override
	public void activateEditor(IWorkbenchPage aPage, IStructuredSelection aSelection) {
		if (aSelection == null || aSelection.isEmpty()){
			return;
		}
		
		if (aSelection.getFirstElement() instanceof Application) {
			performEditorSelect(aPage, new ApplicationEditorInput((Application)aSelection.getFirstElement()), null);
		} else if (aSelection.getFirstElement() instanceof SubApp){
			performEditorSelect(aPage, generateSubAppEditorInput((SubApp)aSelection.getFirstElement()), null);
		} else if(aSelection.getFirstElement() instanceof FB){
			FB refFB = (FB)aSelection.getFirstElement();
			EObject fbCont = refFB.eContainer();
			if(fbCont instanceof FBNetwork){
				Application  app = ((FBNetwork)fbCont).getApplication();
				performEditorSelect(aPage, new ApplicationEditorInput(app), refFB);
			} else if(fbCont instanceof SubAppNetwork){
				SubApp subApp = ((SubAppNetwork)fbCont).getParentSubApp(); 
				performEditorSelect(aPage, generateSubAppEditorInput(subApp), refFB);
			}
		}		
	}

	private void performEditorSelect(IWorkbenchPage aPage, IEditorInput editorInput, FB refFB) {
		IEditorPart editor = activateEditor(aPage, editorInput);
		if ((null != editor) && (editor instanceof FBNetworkEditor) && (null != refFB)){
			((FBNetworkEditor)editor).selectFB(refFB);
		}
	}
	
	private static SubApplicationEditorInput generateSubAppEditorInput(SubApp subApp){
		return new SubApplicationEditorInput((UISubAppNetwork)subApp.getSubAppNetwork().eContainer());
	}

}
