/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.ui.linkhelpers;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.SystemConfiguration;
import org.fordiac.ide.systemconfiguration.editor.SystemConfigurationEditor;
import org.fordiac.ide.systemconfiguration.editor.SystemConfigurationEditorInput;

public class SystemConfigurationEditorLinkHelper extends AbstractEditorLinkHelper {

	@Override
	public IStructuredSelection findSelection(IEditorInput anInput) {		
		if(anInput instanceof SystemConfigurationEditorInput){
			SystemConfigurationEditorInput sysConfInput = (SystemConfigurationEditorInput)anInput;
			return new StructuredSelection(sysConfInput.getSystemConfiguration());
		}
		return StructuredSelection.EMPTY;
	}

	@Override
	public void activateEditor(IWorkbenchPage aPage, IStructuredSelection aSelection) {
		if (aSelection == null || aSelection.isEmpty()){
			return;
		}
		
		SystemConfiguration sysConf = null;
		Device refDev = null;
		if (aSelection.getFirstElement() instanceof SystemConfiguration) {
			sysConf = (SystemConfiguration)aSelection.getFirstElement();
		}else if(aSelection.getFirstElement() instanceof Device){
			refDev = (Device)aSelection.getFirstElement();
			sysConf = refDev.getSystemConfigurationNetwork().getSystemConfiguration();
		}		
		
		if (null != sysConf) {
			IEditorInput sysConfInput = new SystemConfigurationEditorInput(sysConf);
			IEditorPart editor = activateEditor(aPage, sysConfInput);
			if ((null != editor) && (editor instanceof SystemConfigurationEditor) && (null != refDev)){
				((SystemConfigurationEditor)editor).selectDevice(refDev);
			}
		}
		
	}

}
