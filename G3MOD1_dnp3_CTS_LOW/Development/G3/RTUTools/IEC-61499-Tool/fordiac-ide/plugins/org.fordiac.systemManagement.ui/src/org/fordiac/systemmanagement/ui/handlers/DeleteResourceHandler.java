/*******************************************************************************
 * Copyright (c) 2016 fortiss GmbH
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Alois Zoitl - Initial contribution and api documentation 
 *******************************************************************************/

package org.fordiac.systemmanagement.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.systemconfiguration.commands.ResourceDeleteCommand;
import org.fordiac.systemmanagement.SystemManager;

public class DeleteResourceHandler extends AbstractHandler  {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if(selection instanceof TreeSelection) {
			if(((TreeSelection) selection).getFirstElement() instanceof Resource) {
				Resource resouce = (Resource) ((TreeSelection) selection).getFirstElement();
				runDeleteAction(resouce);
			}
		}
		return null;
	}

	private static void runDeleteAction(Resource resource) {
		ResourceView resView = SystemManager.getInstance().getResourceViewForResource(resource);
		if(null != resView){
			ResourceDeleteCommand cmd = new ResourceDeleteCommand(resView);
			AutomationSystem system = resource.getAutomationSystem();			
			org.eclipse.gef.commands.CommandStack commandStack = org.fordiac.ide.gef.Activator.getDefault().getCommandStack(system);
			commandStack.execute(cmd);
		}
	}

}
