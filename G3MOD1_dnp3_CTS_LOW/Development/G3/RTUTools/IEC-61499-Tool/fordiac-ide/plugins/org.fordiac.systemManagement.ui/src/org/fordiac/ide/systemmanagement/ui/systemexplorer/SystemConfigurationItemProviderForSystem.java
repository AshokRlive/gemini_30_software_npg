/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.systemmanagement.ui.systemexplorer;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.fordiac.ide.model.libraryElement.SystemConfiguration;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;
import org.fordiac.ide.model.libraryElement.provider.SystemConfigurationItemProvider;
import org.fordiac.ide.model.libraryElement.provider.SystemConfigurationNetworkItemProvider;

/** a dedicated item provider that will ensure that in the system tree the system configuration will have the content of 
 * the system configuration without the intermediate system configuration netowrk node shown.
 * 
 * @author alil
 */
public class SystemConfigurationItemProviderForSystem extends
		SystemConfigurationItemProvider {

	SystemConfigurationNetworkItemProvider networkProvider = null;
	
	public SystemConfigurationItemProviderForSystem(
			AdapterFactory adapterFactory) {
		super(adapterFactory);
		networkProvider = new SystemConfigurationNetworkItemProvider(adapterFactory){
			@Override
			public void fireNotifyChanged(Notification notification) {
				SystemConfigurationNetwork network = (SystemConfigurationNetwork)notification.getNotifier();
				Notification wrappedNotification = ViewerNotification.wrapNotification(notification, network.getSystemConfiguration()); 
				super.fireNotifyChanged(wrappedNotification);
			}
		};
	}
	
	
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		return networkProvider.getChildrenFeatures(getSystemConfNetwork(object));
	}

	@Override
	public Collection<?> getChildren(Object object) {
		return networkProvider.getChildren(getSystemConfNetwork(object));
	}

	@Override
	public boolean hasChildren(Object object) {
		return networkProvider.hasChildren(getSystemConfNetwork(object));
	}

	private SystemConfigurationNetwork getSystemConfNetwork(Object object) {
		SystemConfigurationNetwork sysConfNetwork = ((SystemConfiguration)object).getSystemConfigurationNetwork();
		if(!sysConfNetwork.eAdapters().contains(networkProvider)){
			//register to the systemconfiguration network changes so that the viewer is updated
			sysConfNetwork.eAdapters().add(networkProvider);
		}		
		return sysConfNetwork;
	}
}
