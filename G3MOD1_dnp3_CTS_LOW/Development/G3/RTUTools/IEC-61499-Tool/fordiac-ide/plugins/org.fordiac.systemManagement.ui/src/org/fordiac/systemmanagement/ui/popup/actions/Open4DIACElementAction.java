/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.ui.popup.actions;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.actions.BaseSelectionListenerAction;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.I4DIACElement;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.SystemConfiguration;
import org.fordiac.ide.util.IOpenListener;
import org.fordiac.ide.util.OpenListenerManager;
import org.fordiac.systemmanagement.ui.Activator;
import org.fordiac.systemmanagement.ui.Messages;

public class Open4DIACElementAction extends BaseSelectionListenerAction {
	
	public static final String ID = Activator.PLUGIN_ID + ".OpenAction";//$NON-NLS-1$

	public Open4DIACElementAction(IWorkbenchPart part) {
		super(Messages.OpenEditorAction_text);
		setId(ID);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected boolean updateSelection(IStructuredSelection selection) {
		boolean retval = true;
		Iterator element = getStructuredSelection().iterator();
		while(element.hasNext() && (true == retval)) {
			Object obj = element.next();
			if((obj instanceof Device) || (obj instanceof SystemConfiguration) || (obj instanceof Application) ||
					(obj instanceof SubApp) || obj instanceof Resource){
				continue;
			}else if(obj instanceof FB){
				//if we have an Fb check if it is in a subapp or application
				retval = isFBInAppOrSubApp((FB)obj); 
						
			}else{
				retval = false;
			}
		}
		return retval;
	}
	

	@SuppressWarnings("rawtypes")
	@Override
	public void run() {
		Iterator element = getStructuredSelection().iterator();
		while(element.hasNext()) {
			Object obj = element.next();
			Object refObject = null;
			
			if(obj instanceof FB){
				//if an FB is selected we need to open the according root node and use FB for selecting
				refObject = obj;
				obj = getFBRootNode((FB)obj);
			}else if (obj instanceof Device){
				refObject = obj;
				obj = ((Device)obj).getSystemConfigurationNetwork().getSystemConfiguration();				
			}

			IOpenListener openListener = OpenListenerManager.getInstance()
					.getDefaultOpenListener(((I4DIACElement) obj).getClass(), obj);
			if (openListener != null) {
				openListener.run(null);
				IEditorPart editor = openListener.getOpenedEditor();
				if((null != editor) && (null != refObject)){
					//TODO select object
				}
			}
		}
	}

	private static boolean isFBInAppOrSubApp(FB fb) {
		EObject rootNode = getFBRootNode(fb);
		return ((rootNode instanceof Application) || (rootNode instanceof SubApp));
	}

	private static EObject getFBRootNode(FB fb) {
		EObject fbCont = fb.eContainer();
		EObject rootNode = null;
		if(fbCont instanceof FBNetwork){
			rootNode = ((FBNetwork)fbCont).getApplication();
		}else if (fbCont instanceof SubAppNetwork){
			rootNode = ((SubAppNetwork)fbCont).getParentSubApp();
		}
		return rootNode;
	}


	
	
}
