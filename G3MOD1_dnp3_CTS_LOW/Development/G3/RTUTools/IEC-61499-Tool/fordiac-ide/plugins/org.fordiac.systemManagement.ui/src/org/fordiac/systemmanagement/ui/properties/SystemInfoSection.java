/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.ui.properties;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.navigator.CommonNavigator;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.util.properties.TypeInfoSection;

public class SystemInfoSection extends TypeInfoSection {

	@Override
	protected CommandStack getCommandStack(IWorkbenchPart part, Object input) {
		if((part instanceof CommonNavigator) && (input instanceof AutomationSystem)){
			return  org.fordiac.ide.gef.Activator.getDefault().getCommandStack((AutomationSystem)input);
		}
		return null;
	}

	@Override
	protected LibraryElement getInputType(Object input) {
		if(input instanceof AutomationSystem){
			return (AutomationSystem) input;	
		}
		return null;
	}

}
