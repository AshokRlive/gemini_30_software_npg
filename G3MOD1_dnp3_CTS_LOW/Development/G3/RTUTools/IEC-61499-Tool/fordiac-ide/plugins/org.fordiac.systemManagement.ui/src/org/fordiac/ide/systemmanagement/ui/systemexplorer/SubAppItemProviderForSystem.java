/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemmanagement.ui.systemexplorer;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.provider.SubAppItemProvider;
import org.fordiac.ide.model.libraryElement.provider.SubAppNetworkItemProvider;

/** a dedicated item provider that will ensure that in the system tree the subapplication will have the 
 * content of the subapp without the intermediate subappnetwork node shown.
 * 
 * @author alil
 *
 */
public class SubAppItemProviderForSystem extends SubAppItemProvider {
	SubAppNetworkItemProvider subAppNetworkItemProvider = null;

	public SubAppItemProviderForSystem(AdapterFactory adapterFactory) {
		super(adapterFactory);
		subAppNetworkItemProvider = new SubAppNetworkItemProvider(adapterFactory){

			@Override
			public void fireNotifyChanged(Notification notification) {
				SubAppNetwork network = (SubAppNetwork)notification.getNotifier();
				Notification wrappedNotification = ViewerNotification.wrapNotification(notification, network.getParentSubApp()); 
				super.fireNotifyChanged(wrappedNotification);
			}
			
		};
	}

	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		return subAppNetworkItemProvider.getChildrenFeatures(getSubAppNetwork(object));
	}

	@Override
	public Collection<?> getChildren(Object object) {		
		return subAppNetworkItemProvider.getChildren(getSubAppNetwork(object));
	}

	@Override
	public boolean hasChildren(Object object) {
		return subAppNetworkItemProvider.hasChildren(getSubAppNetwork(object));
	}

	@Override
	public Object getParent(Object object) {
		EObject cont = ((SubApp)object).eContainer();
		if(cont instanceof FBNetwork){
			return ((FBNetwork)cont).getApplication();
		}else if (cont instanceof SubAppNetwork){
			return ((SubAppNetwork)cont).getParentSubApp();
		}
		return super.getParent(object);
	}

	private SubAppNetwork getSubAppNetwork(Object object) {
		SubAppNetwork subAppNetwork = ((SubApp)object).getSubAppNetwork();
		if(!subAppNetwork.eAdapters().contains(subAppNetworkItemProvider)){
			//register to the subappnetwork changes so that the viewer is updated
			subAppNetwork.eAdapters().add(subAppNetworkItemProvider);
		}
		return subAppNetwork;
	}
}
