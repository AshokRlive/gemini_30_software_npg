/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.ui.commands;

import java.util.ArrayList;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.commands.UnmapCommand;
import org.fordiac.ide.util.commands.UnmapSubAppCommand;
import org.fordiac.ide.util.editors.EditorUtils;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The Class NewAppCommand.
 */
public class DeleteApplicationCommand extends Command {

	
	private Application application;
	private AutomationSystem system;
	
	private ArrayList<Command> commands = new ArrayList<>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return true;

	}

	public DeleteApplicationCommand(Application application) {
		super("Delete Application");
		this.application = application;
		if(null != application){
			this.system = application.getAutomationSystem();
		}
	}

	@Override
	public boolean canExecute() {
		return ((null != application) && (null != system));
	}

	@Override
	public void execute() {
		
		Diagram diagram = (Diagram) application.getFBNetwork().eContainer();
		if (diagram != null) {
			for (View view : diagram.getChildren()) {
				if (view instanceof FBView) {
					if(null != ((FBView) view).getMappedFB()){
						commands.add(new UnmapCommand(((FBView)view).getMappedFB()));
					}
				}
				if (view instanceof SubAppView) {
					if(null != ((SubAppView) view).getMappedSubApp()){
						commands.add(new UnmapSubAppCommand(((SubAppView) view).getMappedSubApp()));
					}
				}
			}
		}

		for (Command command : commands) {
			command.execute();
		}
		
		closeApplicationEditor();

		system.removeApplication(application);		
		SystemManager.getInstance().saveSystem(system, false);
	}

	private void closeApplicationEditor() {
		EditorUtils.closeEditorsFiltered((IEditorPart editor) -> {
			boolean retVal = false;
			if(editor instanceof FBNetworkEditor){
				FBNetworkEditor fbEditor = (FBNetworkEditor)editor;
				if(fbEditor.getDiagramModel() instanceof UIFBNetwork){
					UIFBNetwork uiFBNetwork = (UIFBNetwork)fbEditor.getDiagramModel();
					retVal = application.getFBNetwork().equals(uiFBNetwork.getFbNetwork());
				}
			}
			return retVal;
		});
	}

	@Override
	public void redo() {
		for (Command command : commands) {
			command.redo();
		}
		system.removeApplication(application);
		SystemManager.getInstance().saveSystem(system, false);
	}

	@Override
	public void undo() {
		system.addApplication(application);
		for (Command command : commands) {
			command.undo();
		}
		SystemManager.getInstance().saveSystem(system, false);
	}
}