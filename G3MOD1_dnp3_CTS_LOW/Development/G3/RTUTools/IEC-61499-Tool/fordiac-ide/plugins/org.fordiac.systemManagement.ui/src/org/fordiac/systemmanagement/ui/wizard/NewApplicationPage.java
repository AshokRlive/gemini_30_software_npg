/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.ui.wizard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.fordiac.systemmanagement.SystemManager;
import org.fordiac.systemmanagement.ui.Messages;

public class NewApplicationPage extends NewElementPage {
	
	private Button openApplicationCheckbox;
	
	

	protected NewApplicationPage(String pageName) {
		super(pageName);
	}

	@Override
	public void createControl(final Composite parent) {
		super.createControl(parent);
		
		Composite container = (Composite) getControl();
		openApplicationCheckbox = new Button(container, SWT.CHECK);
		openApplicationCheckbox.setText(Messages.NewApplicationPage_OpenApplicationForEditing);
		openApplicationCheckbox.setSelection(true);
		
	}
	
	@Override
	public String validateElementName(String text) {
		if (!SystemManager.isValidAppName(text, getSelectedSystem())) {
			return Messages.NewApplicationPage_ErrorMessageInvalidAppName;
		}
		return null;
	}

	public Boolean getOpenApplication() {
		return openApplicationCheckbox.getSelection();
	}
}
