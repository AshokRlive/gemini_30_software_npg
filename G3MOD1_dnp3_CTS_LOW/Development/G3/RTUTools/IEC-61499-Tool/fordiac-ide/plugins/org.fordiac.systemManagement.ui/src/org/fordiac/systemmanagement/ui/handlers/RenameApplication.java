/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.systemmanagement.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.IOperationHistory;
import org.eclipse.core.commands.operations.IUndoContext;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.undo.WorkspaceUndoUtil;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.util.IdentifierVerifyListener;
import org.fordiac.systemmanagement.SystemManager;
import org.fordiac.systemmanagement.ui.Activator;
import org.fordiac.systemmanagement.ui.Messages;
import org.fordiac.systemmanagement.ui.commands.RenameApplicationCommand;

public class RenameApplication extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);

		if (selection instanceof TreeSelection) {
			if (((TreeSelection) selection).getFirstElement() instanceof Application) {
				Application application = (Application) ((TreeSelection) selection)
						.getFirstElement();
				
				String newName = showRenameDialog(application);
				if(null != newName){
					performApplicationRename(application, newName);					
				}
			}
		}
		return null;
	}

	private static String showRenameDialog(final Application application) {
		
		InputDialog dialog = new InputDialog(Display.getDefault()
				.getActiveShell(), "Rename Application", "Enter new application name", application.getName(), 
				new IInputValidator(){

					@Override
					public String isValid(String newText) {
						if(application.getName().equals(newText)){
							return new String("Application name not different!");
						}
						if (!SystemManager.isValidAppName(newText, application.getAutomationSystem())) {
							return Messages.NewApplicationPage_ErrorMessageInvalidAppName;
						}
						return null;
					}
			
		}){
			protected Control createDialogArea(Composite parent){
				Control retval = super.createDialogArea(parent);
				getText().addVerifyListener(new IdentifierVerifyListener());
				return retval;
			}
		};
		
		int ret = dialog.open();
		if (ret == InputDialog.OK) {
			return dialog.getValue();
		}
		
		return null;
	}

	private static void performApplicationRename(Application application, String newName) {
		
		RenameApplicationCommand cmd = new RenameApplicationCommand(application, newName);
		
		IWorkbench workbench = PlatformUI.getWorkbench();
		IOperationHistory operationHistory = workbench.getOperationSupport().getOperationHistory();
		IUndoContext undoContext = workbench.getOperationSupport().getUndoContext();
		cmd.addContext(undoContext);
		
		//new UndoRedoActionGroup(, undoContext, true);

		try {
			operationHistory.execute(cmd, null, WorkspaceUndoUtil.getUIInfoAdapter(Display.getDefault()
					.getActiveShell()));
		} catch (ExecutionException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
	}
}
