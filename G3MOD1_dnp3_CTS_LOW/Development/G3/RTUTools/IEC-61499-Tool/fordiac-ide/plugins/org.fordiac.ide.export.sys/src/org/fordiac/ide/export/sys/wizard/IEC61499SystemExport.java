/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.export.sys.wizard;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;
import org.fordiac.ide.export.sys.Activator;
import org.fordiac.ide.export.sys.ISystemExport;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.ui.ExportStatusMessageDialog;

/**
 * The Class IEC61499SystemExport.
 */
public class IEC61499SystemExport extends Wizard implements IExportWizard {
	private static final String FORDIAC_SYSTEM_EXPORT_SECTION = "4DIAC_SYSTEM_EXPORT_SECTION"; //$NON-NLS-1$

	/**
	 * Instantiates a new iE c61499 system export.
	 */
	public IEC61499SystemExport() {

		IDialogSettings settings = Activator.getDefault().getDialogSettings();

		IDialogSettings dialogSettings = settings.getSection(FORDIAC_SYSTEM_EXPORT_SECTION);
		if (dialogSettings == null) {
			dialogSettings = settings
					.addNewSection(FORDIAC_SYSTEM_EXPORT_SECTION);
		}
		setDialogSettings(settings);
	}

	/** The page. */
	ExportSystemWizardPage page;
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		super.addPages();
		page = new ExportSystemWizardPage("System Export");
		page.setDescription("Exporting a system configuration from 4DIAC");
		page.setTitle("System Export");

		addPage(page);
	}

	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		
		final ISystemExport filter = page.getSelectedExportFilter();
		
		 	IRunnableWithProgress op = new IRunnableWithProgress() {
	    	   
				@Override
				public void run(IProgressMonitor monitor) throws InvocationTargetException,
						InterruptedException {
					
					ArrayList<AutomationSystem> selectedSystems = page.getSelectedSystems();
					String outputDirectory = page.getDirectory();	
					
					monitor.beginTask("Exporting selected selected systems using exporter: " + filter.getExportFilterName(), 
							selectedSystems.size());
					
					for (AutomationSystem automationSystem : selectedSystems) {
						filter.export(automationSystem, outputDirectory, page.overwriteWithoutWarning());
						monitor.worked(1);
					}
		
					monitor.done();
		
				}
		 	};
			
		try {	    
			new ProgressMonitorDialog(getShell()).run(false, false, op);
	    } catch (Exception e) {
	    	MessageBox msg = new MessageBox(Display.getDefault().getActiveShell());
			msg.setMessage("Export error:\n" + e.getMessage());
			msg.open();
			
			Activator.getDefault().logError(msg.getMessage(), e);
	    }

		if((!filter.getErrors().isEmpty()) || (!filter.getWarnings().isEmpty())){
			new ExportStatusMessageDialog(getShell(), filter.getWarnings(), filter.getErrors()).open();
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
	 */
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
	}

}
