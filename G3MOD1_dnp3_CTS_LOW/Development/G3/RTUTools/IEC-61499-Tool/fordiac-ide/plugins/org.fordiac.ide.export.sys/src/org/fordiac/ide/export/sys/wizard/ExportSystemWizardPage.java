/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.export.sys.wizard;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.fordiac.ide.export.sys.Activator;
import org.fordiac.ide.export.sys.ISystemExport;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.systemmanagement.ui.systemexplorer.SystemLabelProvider;
import org.fordiac.ide.ui.controls.DirectoryChooserControl;
import org.fordiac.ide.ui.controls.IDirectoryChanged;
import org.fordiac.systemmanagement.contentprovider.SystemContentProvider;

/**
 * The Class NewApplicationPage.
 * 
 * @author gebenh
 */
public class ExportSystemWizardPage extends WizardPage {
	

	/**
	 * Instantiates a new new application page.
	 * 
	 * @param pageName
	 *          the page name
	 */
	protected ExportSystemWizardPage(final String pageName) {
		super(pageName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	public void createControl(final Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		composite.setFont(parent.getFont());

		initializeDialogUnits(parent);

		composite.setLayout(new GridLayout());
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));

		createProjectContainer(composite);
		createDestinationGroup(composite);

		Group group = new Group(composite, SWT.SHADOW_ETCHED_IN);
		group.setText("Options");
		group.setLayout(new GridLayout());
		GridData stretch = new GridData();
		// stretch.horizontalSpan = 2;
		stretch.grabExcessHorizontalSpace = true;
		stretch.horizontalAlignment = SWT.FILL;
		group.setLayoutData(stretch);

		addAvailableExportFilter(group);

		GridData stretchOverwrite = new GridData();
		// stretch.horizontalSpan = 2;
		stretchOverwrite.grabExcessHorizontalSpace = true;
		stretchOverwrite.horizontalAlignment = SWT.FILL;
		overwrite = new Button(group, SWT.CHECK);
		overwrite.setText("Overwrite without warning");
		overwrite.setLayoutData(stretchOverwrite);

		loadDir();

		setPageComplete(validatePage());
		// Show description on opening
		setErrorMessage(null);
		setMessage(null);
		setControl(composite);
	}

	/**
	 * Overwrite without warning.
	 * 
	 * @return true, if successful
	 */
	public boolean overwriteWithoutWarning() {
		return overwrite.getSelection();
	}

	/**
	 * Creates the file name group.
	 * 
	 * @param composite
	 *          the composite
	 */
	private void createDestinationGroup(final Composite composite) {

		GridData stretch = new GridData();
		// stretch.horizontalSpan = 2;
		stretch.grabExcessHorizontalSpace = true;
		stretch.horizontalAlignment = SWT.FILL;

		dcc = new DirectoryChooserControl(composite, SWT.NONE, "Destination: ");
		dcc.addDirectoryChangedListener(new IDirectoryChanged() {

			@Override
			public void directoryChanged(String newDirectory) {
				saveDir(newDirectory);
				setPageComplete(validatePage());
			}
		});

		dcc.setLayoutData(stretch);
	}

	/** The tree viewer. */
	private CheckboxTreeViewer treeViewer;
	private DirectoryChooserControl dcc;

	/**
	 * Creates the project container.
	 * 
	 * @param parent
	 *          the parent
	 */
	private void createProjectContainer(final Composite parent) {

		Label l = new Label(parent, SWT.NONE);
		l.setText("System");
		l.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		treeViewer = new CheckboxTreeViewer(parent); // , SWT.MULTI |
		// SWT.H_SCROLL |
		// SWT.V_SCROLL);
		treeViewer.setContentProvider(new SystemContentProvider());
		treeViewer.setLabelProvider(new SystemLabelProvider()); 

		treeViewer.setInput(new Object());
		treeViewer.getTree().setLayoutData(new GridData(GridData.FILL_BOTH));

		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(final SelectionChangedEvent event) {
				setPageComplete(validatePage());
			}
		});

	}

	private final ArrayList<ISystemExport> exportFilters = new ArrayList<ISystemExport>();
	private Combo filters;
	private Button overwrite;

	private void addAvailableExportFilter(final Group group) {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] elems = registry.getConfigurationElementsFor(
				Activator.PLUGIN_ID, "SystemExport"); //$NON-NLS-1$
		for (int i = 0; i < elems.length; i++) {
			IConfigurationElement element = elems[i];
			try {
				Object object = element.createExecutableExtension("class"); //$NON-NLS-1$
				if (object instanceof ISystemExport) {
					ISystemExport exportFilter = (ISystemExport) object;
					exportFilters.add(exportFilter);
				}
			} catch (CoreException corex) {
				Activator.getDefault().logError("Error loading Export Filter", corex);
			}
		}
		GridData stretch = new GridData();
		stretch.grabExcessHorizontalSpace = true;
		stretch.horizontalAlignment = SWT.FILL;

		filters = new Combo(group, SWT.NONE);
		filters.setLayoutData(stretch);
		for (Iterator<ISystemExport> iterator = exportFilters.iterator(); iterator.hasNext();) {
			ISystemExport exportFilter = (ISystemExport) iterator.next();
			filters.add(exportFilter.getExportFilterName() != null ? exportFilter
					.getExportFilterName() : exportFilter.toString());
		}

		filters.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				saveFilter(exportFilters.get(filters.getSelectionIndex())
						.getExportFilterName());
				setPageComplete(validatePage());
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				// not required
			}

		});
	}

	/**
	 * Gets the selected export filter.
	 * 
	 * @return the selected export filter
	 */
	public ISystemExport getSelectedExportFilter() {
		return exportFilters.get(filters.getSelectionIndex());
	}

	/**
	 * Returns whether this page's controls currently all contain valid values.
	 * 
	 * @return <code>true</code> if all controls are valid, and <code>false</code>
	 *         if at least one is invalid
	 */
	protected boolean validatePage() {
		if (dcc.getDirectory() == null || dcc.getDirectory().equals("")) {
			setErrorMessage("Destination not selected!");
			return false;
		}
		if (getSelectedSystems().size() == 0) {
			setErrorMessage("No system selected!");
			return false;
		}

		if (filters.getSelectionIndex() == -1) {
			setErrorMessage("No ExportFilter selected!");
			return false;
		}

		setErrorMessage(null);
		return true;

	}

	
	/**
	 * Gets the selected system.
	 * 
	 * @return the selected system
	 */
	public ArrayList<AutomationSystem> getSelectedSystems() {
		ArrayList<AutomationSystem> systems = new ArrayList<AutomationSystem>();
		Object[] selection = treeViewer.getCheckedElements();

		for (int i = 0; i < selection.length; i++) {
			Object object = selection[i];
			if (object instanceof AutomationSystem) {
				systems.add((AutomationSystem) object);
			}
		}
		return systems;
	}

	/**
	 * Saves current directory for next session.
	 * 
	 * @param currentDir
	 *          the current dir
	 */
	private void saveDir(final String currentDir) {
		getDialogSettings().put("currentDir", currentDir);
	}

	/**
	 * Saves current export filter for next session.
	 * 
	 * @param filter
	 *          the export filter
	 */
	private void saveFilter(final String filter) {
		getDialogSettings().put("currentExportFilterSelectionName", filter);
	}

	/**
	 * Sets the directory.
	 * 
	 * @param dir the new directory
	 */
	public void setDirectory(final String dir) {
		dcc.setDirectory(dir);
	}

	/**
	 * Gets the directory.
	 * 
	 * @return the directory
	 */
	public String getDirectory() {
		return dcc.getDirectory();
	}

	/**
	 * Loads cached directory, if available.
	 */
	private void loadDir() {
		if (getDialogSettings() != null) {
			String cachedDir = getDialogSettings().get("currentDir");
			if (cachedDir != null) {
				setDirectory(cachedDir);
			}

			String currentFilterSelectionName = getDialogSettings().get(
					"currentExportFilterSelectionName");
			if (currentFilterSelectionName != null) {
				for (Iterator<ISystemExport> iterator = exportFilters.iterator(); iterator.hasNext();) {
					ISystemExport filter = (ISystemExport) iterator.next();
					if (filter.getExportFilterName().equals(currentFilterSelectionName)) {
						filters.select(exportFilters.indexOf(filter));
						break;
					}
				}
			}
		}
	}
}
