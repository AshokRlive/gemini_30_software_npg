package org.fordiac.ide.export.sys;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.MessageFormat;
import java.util.ArrayList;

import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.typeexport.SystemExporter;

public class IEC61499_2CompliantSystemExport implements ISystemExport {
	private SystemExporter exporter = new SystemExporter();
	
	@Override public void export(AutomationSystem system, String destinationPath, boolean overwriteWithoutWarning) {
		if (system == null) {
			getErrors().add("System must not be null!");
			return;
		}
		if (destinationPath == null || destinationPath.equals("")) {
			getErrors().add("Destination must not be empty!");
			return;
		}
		
		File outFile = new File(destinationPath + File.separatorChar + system.getName() + ".sys");
		
		if(checkTargetFile(outFile, overwriteWithoutWarning)){
			try {
				Result result = new StreamResult(new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8"));
				exporter.generateSYSFileContent(system, result);			
				getInfos().add(system.getName() + " exported.");
			} catch (Exception e) {
				getErrors().add(e.toString());
				e.printStackTrace(System.out);
			}
		}
	}
	
	private boolean checkTargetFile(File outFile, boolean overwriteWithoutWarning) {
		boolean retVal = true;
		
		if (outFile.exists() && !overwriteWithoutWarning) {
			MessageBox msgBox = new MessageBox(Display.getDefault().getActiveShell(), SWT.YES
					| SWT.NO | SWT.ICON_QUESTION);
			String msg = MessageFormat.format("File Exists, overwrite?",
					new Object[] { outFile.getAbsolutePath() });
			msgBox.setMessage(msg);
			retVal = (SWT.YES == msgBox.open());
			if (!retVal) {
				getWarnings().add(" - Export aborted by user because file exists!");
			}
		}		
		return retVal;
	}
	
	@Override
	public ArrayList<String> getErrors() {
		return exporter.getErrors();
	}

	@Override
	public ArrayList<String> getInfos() {
		return exporter.getInfos();
	}

	@Override
	public ArrayList<String> getWarnings() {
		return exporter.getWarnings();
	}	
	
	@Override
	public String getExportFilterDescription() {
		return "Compliant export regarding the LibraryElement and DataType DTDs given in IEC Standard 61499-2 published in January 2005.";
	}

	@Override
	public String getExportFilterName() {
		return "IEC 61499 System - IEC 61499-2 compliant";
	}
}
