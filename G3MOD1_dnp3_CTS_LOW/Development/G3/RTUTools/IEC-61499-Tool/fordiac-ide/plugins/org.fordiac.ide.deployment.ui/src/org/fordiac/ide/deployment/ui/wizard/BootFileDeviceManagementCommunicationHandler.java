/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment.ui.wizard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.fordiac.ide.deployment.AbstractDeviceManagementCommunicationHandler;
import org.fordiac.ide.deployment.DeploymentCoordinator;
import org.fordiac.ide.deployment.exceptions.DisconnectException;
import org.fordiac.ide.deployment.exceptions.InvalidMgmtID;
import org.fordiac.ide.deployment.ui.Activator;

public class BootFileDeviceManagementCommunicationHandler extends AbstractDeviceManagementCommunicationHandler {

	static public void createBootFile(List<Object> workList, String fileName, Shell shell){
		if(null != fileName){
			DeploymentCoordinator deployment = DeploymentCoordinator.getInstance();
			BootFileDeviceManagementCommunicationHandler bootFileHandler = new BootFileDeviceManagementCommunicationHandler();
			deployment.performDeployment(workList.toArray(), bootFileHandler);
			
			bootFileHandler.writeToBootFile(fileName, shell);
		}
	}
	
	/* only the static function of this class should be able to create an instance */
	private BootFileDeviceManagementCommunicationHandler() {
		super();
	}

	StringBuffer stringBuffer = new StringBuffer();
	
	/** The original MgrID. */
	public String origMgrID;

	@Override
	public void connect(String address) throws InvalidMgmtID,
			UnknownHostException, IOException {
		origMgrID = address;
	}

	@Override
	public void disconnect() throws DisconnectException {
		//For bootfile writing we need nothing to do here
		
	}

	@Override
	public void sendREQ(String destination, String request)
			throws IOException {
		
		stringBuffer.append(destination + ";" + request + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		
		String info = origMgrID;
		if(!destination.equals("")){ //$NON-NLS-1$
			info += ": " + destination; //$NON-NLS-1$
		}

		postCommandSent(info, destination, request);
	}
	
	/*take the current state of the string buffer and write it to the given file */
	public void writeToBootFile(String fileName, Shell shell){
		File bootFile = new File(fileName);

		int res = SWT.YES;
		if (bootFile.exists()) {
			MessageBox msgBox = new MessageBox(
					shell, SWT.YES
							| SWT.NO
							| SWT.ICON_QUESTION);
			String msg = MessageFormat.format("File Exists, overwrite ("+ fileName + ")?",
							new Object[] { bootFile.getAbsolutePath() });
			msgBox.setMessage(msg);

			res = msgBox.open();
		} else {
			try {
				bootFile.createNewFile();
			} catch (IOException e) {
				Activator.getDefault().logError(e.getMessage(), e);
			}
		}
		if (res == SWT.YES) {
			try {
				PrintWriter boot = new PrintWriter(new FileOutputStream(bootFile));
				boot.write(stringBuffer.toString());
				boot.flush();
				boot.close();
			} catch (FileNotFoundException e) {
				Activator.getDefault().logError(e.getMessage(), e);
			}
		}
	}
	
	public void clearStringBuffer(){
		stringBuffer = new StringBuffer();
	}
	
}