/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment.ui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.deployment.DeploymentCoordinator;
import org.fordiac.ide.deployment.IDeploymentExecutor;
import org.fordiac.ide.deployment.ui.Activator;
import org.fordiac.ide.model.libraryElement.Device;

/**
 * The Class SendClearAction.
 */
public class SendClearAction implements IObjectActionDelegate {

	private Device device;

	/**
	 * Instantiates a new send clear action.
	 */
	public SendClearAction() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action.IAction, org.eclipse.ui.IWorkbenchPart)
	 */
	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(IAction action) {
		if (device != null) {
			IDeploymentExecutor executor = DeploymentCoordinator.getInstance().getDeploymentExecutor(device);

			if(null != executor){
				String mgrid = DeploymentCoordinator.getMGR_ID(device);
				DeploymentCoordinator.getInstance().enableOutput(executor.getDevMgmComHandler());
				try {
					executor.getDevMgmComHandler().connect(mgrid);
					executor.clearDevice(device);
					executor.getDevMgmComHandler().disconnect();
				} catch (Exception e) {
					Activator.getDefault().logError(e.getMessage(), e);
				}
	
				DeploymentCoordinator.getInstance().flush();
				DeploymentCoordinator.getInstance().disableOutput(executor.getDevMgmComHandler());
			}else{
				DeploymentCoordinator.printUnsupportedDeviceProfileMessageBox(device, null);
			}			
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			Object element = ((IStructuredSelection) selection).getFirstElement();
			if (element instanceof Device) {
				device = (Device) element;
			} else {
				device = null;
			}

		}

	}

}
