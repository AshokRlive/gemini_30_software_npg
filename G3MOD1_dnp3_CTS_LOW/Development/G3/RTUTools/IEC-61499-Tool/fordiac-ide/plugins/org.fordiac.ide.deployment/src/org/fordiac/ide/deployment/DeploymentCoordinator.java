/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment;

import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.fordiac.ide.deployment.exceptions.CreateConnectionException;
import org.fordiac.ide.deployment.exceptions.CreateFBInstanceException;
import org.fordiac.ide.deployment.exceptions.WriteFBParameterException;
import org.fordiac.ide.deployment.util.IDeploymentListener;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.ConfigurableObject;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.ResourceFBNetwork;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The Class DeploymentCoordinator.
 */
public class DeploymentCoordinator {

	private final Hashtable<Device, ArrayList<VarDeclaration>> deployedDeviceProperties = new Hashtable<Device, ArrayList<VarDeclaration>>();

	/**
	 * Adds the device property.
	 * 
	 * @param dev
	 *            the dev
	 * @param property
	 *            the property
	 */
	public void addDeviceProperty(Device dev, VarDeclaration property) {
		if (deployedDeviceProperties.containsKey(dev)) {
			ArrayList<VarDeclaration> temp = deployedDeviceProperties.get(dev);
			if (!temp.contains(property)) {
				temp.add(property);
			}
		} else {
			ArrayList<VarDeclaration> temp = new ArrayList<VarDeclaration>();
			temp.add(property);
			deployedDeviceProperties.put(dev, temp);
		}
	}

	/**
	 * Sets the device properties.
	 * 
	 * @param dev
	 *            the dev
	 * @param properties
	 *            the properties
	 */
	public void setDeviceProperties(Device dev,
			ArrayList<VarDeclaration> properties) {
		deployedDeviceProperties.put(dev, properties);
	}

	/**
	 * Removes the device property.
	 * 
	 * @param dev
	 *            the dev
	 * @param property
	 *            the property
	 */
	public void removeDeviceProperty(Device dev, VarDeclaration property) {
		if (deployedDeviceProperties.containsKey(dev)) {
			ArrayList<VarDeclaration> temp = deployedDeviceProperties.get(dev);
			if (temp.contains(property)) {
				temp.remove(property);
			}
		}
	}

	/**
	 * Gets the selected device properties.
	 * 
	 * @param dev
	 *            the dev
	 * 
	 * @return the selected device properties
	 */
	public ArrayList<VarDeclaration> getSelectedDeviceProperties(Device dev) {
		return deployedDeviceProperties.get(dev);
	}

	/** The instance. */
	private static DeploymentCoordinator instance;

	/**
	 * Instantiates a new deployment coordinator.
	 */
	private DeploymentCoordinator() {
		// empty private constructor
	}

	/**
	 * Gets the single instance of DeploymentCoordinator.
	 * 
	 * @return single instance of DeploymentCoordinator
	 */
	public static DeploymentCoordinator getInstance() {
		if (instance == null) {
			instance = new DeploymentCoordinator();
		}
		return instance;
	}

	/** The deployment executors. */
	private ArrayList<IDeploymentExecutor> deploymentExecutors = null;

	/** The device management communication handlers. */
	private ArrayList<AbstractDeviceManagementCommunicationHandler> deviceMangementCommunicationHandlers = null;

	/** The listener. */
	private final IDeploymentListener listener = new IDeploymentListener() {

		@Override
		public void responseReceived(String response, String source) {
			responseReceivedX(response, source);
		}

		@Override
		public void postCommandSent(String command, String destination) {
			postCommandSentX(command, destination);
		}

		@Override
		public void postCommandSent(String message) {
			postCommandSentX(message);
		}

		@Override
		public void finished() {
			finishedX();
		}

		@Override
		public void postCommandSent(String info, String destination,
				String command) {
			postCommandSentX(info, destination, command);
		}

	};

	/**
	 * Gets the mG r_ id.
	 * 
	 * @param resource
	 *            the resource
	 * 
	 * @return the mG r_ id
	 */
	private String getMGR_ID(final Resource resource) {
		return getMGR_ID(resource.getDevice());
	}

	/**
	 * Gets the mG r_ id.
	 * 
	 * @param dev
	 *            the dev
	 * 
	 * @return the mG r_ id
	 */
	public static String getMGR_ID(final Device dev) {
		for(VarDeclaration varDecl : dev.getVarDeclarations()) {
			if (varDecl.getName().equalsIgnoreCase("MGR_ID")) {
				String val = getVariableValue(varDecl, dev.getAutomationSystem(), dev);
				if(null != val){				
					return val;
				}
			}
		}
		return "";
	}

	/**
	 * The Class DownloadRunnable.
	 */
	class DownloadRunnable implements IRunnableWithProgress {

		/** The work. */
		private final int work;

		/** The selection. */
		private final Object[] selection;

		/** the Communication handler */
		AbstractDeviceManagementCommunicationHandler overrideDevMgmCommHandler;

		/**
		 * DownloadRunnable constructor.
		 * 
		 * @param work
		 *            the amount of download operations
		 * @param selection
		 *            the selection
		 * @param overrideDevMgmCommHandler
		 *            if not null this device management communication should be
		 *            used instead the one derived from the device profile.
		 */
		public DownloadRunnable(
				final int work,
				final Object[] selection,
				AbstractDeviceManagementCommunicationHandler overrideDevMgmCommHandler) {
			this.work = work;
			this.selection = selection;
			this.overrideDevMgmCommHandler = overrideDevMgmCommHandler;
		}

		/**
		 * Runs the check.
		 * 
		 * @param monitor
		 *            the progress monitor
		 * 
		 * @throws InvocationTargetException
		 *             the invocation target exception
		 * @throws InterruptedException
		 *             the interrupted exception
		 */
		public void run(final IProgressMonitor monitor)
				throws InvocationTargetException, InterruptedException {
			monitor.beginTask(
					Messages.DeploymentCoordinator_LABEL_PerformingDownload,
					work);

			ArrayList<Device> devicesWhichNeedsToBeConfigured = new ArrayList<Device>();
			for (int i = 0; i < selection.length && !monitor.isCanceled(); i++) {
				Object object = selection[i];
				if (object instanceof Resource) {
					final Resource res = (Resource) object;

					IDeploymentExecutor executor = getDeploymentExecutor(
							res.getDevice(), overrideDevMgmCommHandler);

					if (executor != null) {
						executor.getDevMgmComHandler().addDeploymentListener(
								listener);
						try {
							String mgrid = getMGR_ID(res);
							if (!res.isDeviceTypeResource()) {
								executor.getDevMgmComHandler().connect(mgrid);
								executor.createResource(res);
								monitor.worked(1);
								for (VarDeclaration varDecl : res.getVarDeclarations()) {
									String val = getVariableValue(varDecl, res.getAutomationSystem(), res);
									if(null != val){
										executor.writeResourceParameter(res, varDecl.getName(), val);
										monitor.worked(1);
									}
								}
								FBNetwork fbNetwork = res.getFBNetwork();
								if (fbNetwork != null) {
									// EList<FB> fbs = res.getFBNetwork()
									// .getMappedFBs();
									ArrayList<FB> temp = new ArrayList<FB>();
									temp.addAll(res.getFBNetwork()
											.getMappedFBs());

									for (FB fb : res.getFBNetwork().getFBs()) {
										if (fb.isResourceFB()) {
											temp.add(fb);
										}
									}
									EList<SubApp> subApps = res.getFBNetwork()
											.getMappedSubApps();
									createFBInstance(res, temp, subApps,
											executor, monitor);
									createEventConnections(res, executor,
											fbNetwork, monitor);
									createDataConnections(res, executor,
											fbNetwork, monitor);
								}
								if (!devicesWhichNeedsToBeConfigured
										.contains(res.getDevice())) {
									executor.startResource(res);
								} else {
									// resource is started when device is
									// started
								}
								executor.getDevMgmComHandler().disconnect();
							}
						} catch (final Exception e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									final Shell shell = Display.getDefault().getActiveShell();
									
									MessageDialog.openError(shell, "Major Download Error", 
											"Resource: " + res.getDevice().getName() + "." + res.getName() + "\n" +
											"MGR_ID: " + getMGR_ID(res) + "\n" +		
											"Problem: "+ e.getMessage());
								}
							});
						}
						executor.getDevMgmComHandler()
								.removeDeploymentListener(listener);

					} else {
						printUnsupportedDeviceProfileMessageBox(
								res.getDevice(), res);
					}

				} else if (object instanceof Device) {
					Device device = (Device) object;
					ArrayList<VarDeclaration> parameters = getSelectedDeviceProperties(device);
					if (parameters != null && parameters.size() > 0) {
						devicesWhichNeedsToBeConfigured.add(device);
					}
				}
				if (monitor.isCanceled()) {
					throw new InterruptedException(
							Messages.DeploymentCoordinator_LABEL_DownloadAborted);
				}
			}
			for (Iterator<Device> iterator = devicesWhichNeedsToBeConfigured
					.iterator(); iterator.hasNext();) {
				Device device = iterator.next();
				configureDevice(monitor, device);
				if (monitor.isCanceled()) {
					throw new InterruptedException(
							Messages.DeploymentCoordinator_LABEL_DownloadAborted);
				}
			}
			// Thread.sleep(500);
			finishedX();
			monitor.done();
		}

		private void configureDevice(final IProgressMonitor monitor,
				Device device) {
			IDeploymentExecutor executor = getDeploymentExecutor(device,
					overrideDevMgmCommHandler);
			ArrayList<VarDeclaration> parameters = getSelectedDeviceProperties(device);

			if (executor != null && parameters != null && parameters.size() > 0) {
				try {
					executor.getDevMgmComHandler().addDeploymentListener(
							listener);
					String mgrid = getMGR_ID(device);
					executor.getDevMgmComHandler().connect(mgrid);
					for (Iterator<VarDeclaration> iterator = parameters
							.iterator(); iterator.hasNext();) {
						VarDeclaration varDeclaration = iterator.next();
						if (varDeclaration.getValue() != null
								&& varDeclaration.getValue().getValue() != null
								&& !varDeclaration.getValue().getValue()
										.equals("")) {
							executor.writeDeviceParameter(device,
									varDeclaration.getName(), varDeclaration
											.getValue().getValue());
						}

					}
					executor.startDevice(device);
					executor.getDevMgmComHandler().disconnect();
					executor.getDevMgmComHandler().removeDeploymentListener(
							listener);
					monitor.worked(1);
				} catch  (Exception e) {
					Activator.getDefault().logError(e.getMessage(), e);
				}
			}
		}

		/**
		 * Creates the event connections.
		 * 
		 * @param res
		 *            the res
		 * @param executor
		 *            the executor
		 * @param fbNetwork
		 *            the fb network
		 * @param monitor
		 *            the monitor
		 * 
		 * @throws CreateConnectionException
		 *             the create connection exception
		 */
		private void createEventConnections(final Resource res,
				final IDeploymentExecutor executor,
				final SubAppNetwork fbNetwork, final IProgressMonitor monitor)
				throws CreateConnectionException {
			for (Iterator<EventConnection> iterator = fbNetwork
					.getEventConnections().iterator(); iterator.hasNext()
					&& !monitor.isCanceled();) {
				EventConnection eventCon = iterator.next();
				if (!eventCon.isResTypeConnection()) {
					executor.createEventConnection(res, eventCon);
					monitor.worked(1);
				}
			}
			if (fbNetwork instanceof ResourceFBNetwork) {

				for (Iterator<SubApp> iterator = ((ResourceFBNetwork) fbNetwork)
						.getMappedSubApps().iterator(); iterator.hasNext()
						&& !monitor.isCanceled();) {
					SubApp subApp = iterator.next();
					createEventConnections(res, executor,
							subApp.getSubAppNetwork(), monitor);
				}

			} else {
				for (Iterator<SubApp> iterator = (fbNetwork).getSubApps()
						.iterator(); iterator.hasNext()
						&& !monitor.isCanceled();) {
					SubApp subApp = iterator.next();
					createEventConnections(res, executor,
							subApp.getSubAppNetwork(), monitor);
				}
			}
		}

		/**
		 * Creates the data connections.
		 * 
		 * @param res
		 *            the res
		 * @param executor
		 *            the executor
		 * @param fbNetwork
		 *            the fb network
		 * @param monitor
		 *            the monitor
		 * 
		 * @throws CreateConnectionException
		 *             the create connection exception
		 */
		private void createDataConnections(final Resource res,
				final IDeploymentExecutor executor,
				final SubAppNetwork fbNetwork, final IProgressMonitor monitor)
				throws CreateConnectionException {
			for (Iterator<DataConnection> iterator = fbNetwork
					.getDataConnections().iterator(); iterator.hasNext()
					&& !monitor.isCanceled();) {
				DataConnection dataCon = iterator.next();
				if (!dataCon.isResTypeConnection()) {
					executor.createDataConnection(res, dataCon);
					monitor.worked(1);
				}
			}
			if (fbNetwork instanceof ResourceFBNetwork) {

				for (Iterator<SubApp> iterator = ((ResourceFBNetwork) fbNetwork)
						.getMappedSubApps().iterator(); iterator.hasNext()
						&& !monitor.isCanceled();) {
					SubApp subApp = iterator.next();
					createDataConnections(res, executor,
							subApp.getSubAppNetwork(), monitor);
				}

			} else {
				for (Iterator<SubApp> iterator = fbNetwork.getSubApps()
						.iterator(); iterator.hasNext()
						&& !monitor.isCanceled();) {
					SubApp subApp = iterator.next();
					createDataConnections(res, executor,
							subApp.getSubAppNetwork(), monitor);
				}
			}
		}

		/**
		 * Creates the fb instance.
		 * 
		 * @param res
		 *            the res
		 * @param fbs
		 *            the fbs
		 * @param subApps
		 *            the sub apps
		 * @param executor
		 *            the executor
		 * @param monitor
		 *            the monitor
		 * 
		 * @throws CreateFBInstanceException
		 *             the create fb instance exception
		 * @throws WriteFBParameterException
		 *             the write fb parameter exception
		 */
		private void createFBInstance(final Resource res, final List<FB> fbs,
				final List<SubApp> subApps, final IDeploymentExecutor executor,
				final IProgressMonitor monitor)
				throws CreateFBInstanceException, WriteFBParameterException {
			for (Iterator<FB> iterator = fbs.iterator(); iterator.hasNext()
					&& !monitor.isCanceled();) {
				final FB fb = iterator.next();
				executor.createFBInstance(fb, res);
				monitor.worked(1);
				InterfaceList interfaceList = fb.getInterface();
				if (interfaceList != null) {
					for (Iterator<VarDeclaration> iterator2 = interfaceList
							.getInputVars().iterator(); iterator2.hasNext();) {
						VarDeclaration varDecl = iterator2.next();
						if (varDecl.getInputConnections().size() == 0) {
							String val = getVariableValue(varDecl, res.getAutomationSystem(), fb);
							if(null != val){
								executor.writeFBParameter(res, val, fb, varDecl);
								monitor.worked(1);								
							}							
						}
					}
				}
			}
			for (Iterator<SubApp> iterator = subApps.iterator(); iterator
					.hasNext() && !monitor.isCanceled();) {
				SubApp subApp = iterator.next();
				List<FB> subAppFBs = subApp.getSubAppNetwork().getFBs();
				List<SubApp> subAppSubApps = subApp.getSubAppNetwork()
						.getSubApps();
				createFBInstance(res, subAppFBs, subAppSubApps, executor,
						monitor);
			}
		}

	}

	private static String getVariableValue(VarDeclaration varDecl, AutomationSystem system, ConfigurableObject object) {
		Value value = varDecl.getValue();
		if (null != value && null != value.getValue() && !"".equals(value.getValue())){
			String val = value.getValue();
			if (val.contains("%")) {
				String replaced = SystemManager.getInstance().getReplacedString(system, object, val);
				if (replaced != null) {
					val = replaced;
				}
			}
			return val;
		}		
		return null;	
	}

	/**
	 * Count work creating f bs.
	 * 
	 * @param res
	 *            the res
	 * @param fbs
	 *            the fbs
	 * @param subApps
	 *            the sub apps
	 * 
	 * @return the int
	 */
	private int countWorkCreatingFBs(final Resource res, final List<FB> fbs,
			final List<SubApp> subApps) {
		int work = 0;
		for (Iterator<FB> iterator = fbs.iterator(); iterator.hasNext();) {
			FB fb = iterator.next();
			work++;
			InterfaceList interfaceList = fb.getInterface();
			if (interfaceList != null) {
				for (Iterator<VarDeclaration> iterator2 = interfaceList
						.getInputVars().iterator(); iterator2.hasNext();) {
					VarDeclaration varDecl = iterator2.next();
					if (varDecl.getInputConnections().size() == 0) {
						Value value = varDecl.getValue();
						if (value != null && value.getValue() != null) {
							work++;
						}
					}
				}
			}
		}
		for (Iterator<SubApp> iterator = subApps.iterator(); iterator.hasNext();) {
			SubApp subApp = iterator.next();
			List<FB> subAppFBs = subApp.getSubAppNetwork().getFBs();
			List<SubApp> subAppSubApps = subApp.getSubAppNetwork().getSubApps();
			work += countWorkCreatingFBs(res, subAppFBs, subAppSubApps);
		}
		return work;
	}

	public static void printUnsupportedDeviceProfileMessageBox(
			final Device device, final Resource res) {
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				MessageBox messageBox = new MessageBox(Display.getDefault()
						.getActiveShell(), SWT.ICON_ERROR | SWT.OK);
				String resName = "";
				if (null != res) {
					resName = res.getName();
				}

				messageBox.setMessage(MessageFormat
						.format(Messages.DeploymentCoordinator_MESSAGE_DefinedProfileNotSupported,
								new Object[] { device.getProfile(),
										device.getName(), resName }));
				messageBox.open();

			}

		});
	}

	/**
	 * Count create event connections.
	 * 
	 * @param res
	 *            the res
	 * @param fbNetwork
	 *            the fb network
	 * 
	 * @return the int
	 */
	private int countCreateEventConnections(final Resource res,
			final SubAppNetwork fbNetwork) {
		int work = 0;
		for (Iterator<EventConnection> iterator = fbNetwork
				.getEventConnections().iterator(); iterator.hasNext();) {
			EventConnection eventCon = iterator.next();
			if (!eventCon.isResTypeConnection()) {
				work++;
			}
		}
		if (fbNetwork instanceof ResourceFBNetwork) {

			for (Iterator<SubApp> iterator = ((ResourceFBNetwork) fbNetwork)
					.getMappedSubApps().iterator(); iterator.hasNext();) {
				SubApp subApp = iterator.next();
				work += countCreateEventConnections(res,
						subApp.getSubAppNetwork());
			}

		} else {
			for (Iterator<SubApp> iterator = (fbNetwork).getSubApps()
					.iterator(); iterator.hasNext();) {
				SubApp subApp = iterator.next();
				work += countCreateEventConnections(res,
						subApp.getSubAppNetwork());
			}
		}
		return work;
	}

	/**
	 * Count create data connections.
	 * 
	 * @param res
	 *            the res
	 * @param fbNetwork
	 *            the fb network
	 * 
	 * @return the int
	 */
	private int countCreateDataConnections(final Resource res,
			final SubAppNetwork fbNetwork) {
		int work = 0;
		for (Iterator<DataConnection> iterator = fbNetwork.getDataConnections()
				.iterator(); iterator.hasNext();) {
			DataConnection dataCon = iterator.next();
			if (!dataCon.isResTypeConnection()) {
				work++;
			}
		}
		if (fbNetwork instanceof ResourceFBNetwork) {

			for (Iterator<SubApp> iterator = ((ResourceFBNetwork) fbNetwork)
					.getMappedSubApps().iterator(); iterator.hasNext();) {
				SubApp subApp = iterator.next();
				work += countCreateDataConnections(res,
						subApp.getSubAppNetwork());
			}

		} else {
			for (Iterator<SubApp> iterator = (fbNetwork).getSubApps()
					.iterator(); iterator.hasNext();) {
				SubApp subApp = iterator.next();
				work += countCreateDataConnections(res,
						subApp.getSubAppNetwork());
			}
		}
		return work;
	}

	/**
	 * Perform deployment.
	 * 
	 * @param selection
	 *            the selection
	 * @param overrideDevMgmCommHandler
	 *            if not null this device management communication should be
	 *            used instead the one derived from the device profile.
	 */
	public void performDeployment(
			final Object[] selection,
			AbstractDeviceManagementCommunicationHandler overrideDevMgmCommHandler) {
		int work = 0;
		// for (Iterator iterator = resources.iterator(); iterator.hasNext();) {
		for (int i = 0; i < selection.length; i++) {
			Object object = selection[i];
			if (object instanceof Resource) {
				Resource res = (Resource) object;
				work++; // for creating the resource
				work += countResourceParams(res);
				work += countWorkCreatingFBs(res, res.getFBNetwork()
						.getMappedFBs(), res.getFBNetwork().getMappedSubApps());
				work += countCreateEventConnections(res, res.getFBNetwork());
				work += countCreateDataConnections(res, res.getFBNetwork());
				// TODO count AdapterConnections
			}
		}
		DownloadRunnable download = new DownloadRunnable(work, selection,
				overrideDevMgmCommHandler);
		Shell shell = Display.getDefault().getActiveShell();
		try {
			new ProgressMonitorDialog(Display.getDefault().getActiveShell())
					.run(true, true, download);
		} catch (InvocationTargetException ex) {
			MessageDialog.openError(shell, "Error", ex.getMessage());
		} catch (InterruptedException ex) {
			MessageDialog.openInformation(shell,
					Messages.DeploymentCoordinator_LABEL_DownloadAborted,
					ex.getMessage());
		}
	}

	public void performDeployment(final Object[] selection) {
		performDeployment(selection, null);
	}

	/**
	 * Count resource params.
	 * 
	 * @param res
	 *            the res
	 * 
	 * @return the int
	 */
	private int countResourceParams(final Resource res) {
		int work = 0;
		for (Iterator<VarDeclaration> iterator = res.getVarDeclarations()
				.iterator(); iterator.hasNext();) {
			VarDeclaration varDecl = iterator.next();
			if (varDecl.getValue() != null
					&& varDecl.getValue().getValue() != null
					&& varDecl.getValue().getValue().equals("")) {
				work++;
			}
		}
		return work;
	}

	/**
	 * Gets the deployment executor.
	 * 
	 * @param device
	 *            the device for which a deployment executor should be get
	 * @param overrideComHandler
	 *            if not null this com handler will be given to the executor
	 * 
	 * @return the deployment executor
	 */
	public IDeploymentExecutor getDeploymentExecutor(
			final Device device,
			final AbstractDeviceManagementCommunicationHandler overrideComHandler) {
		if (null == deploymentExecutors) {
			deploymentExecutors = loadDeploymentExecutors();
		}

		for (IDeploymentExecutor idepExec : deploymentExecutors) {
			if (idepExec.supports(device.getProfile())) {
				idepExec.setDeviceManagementCommunicationHandler((null != overrideComHandler) ? overrideComHandler
						: getDevMgmCommunicationHandler(device));
				return (null != idepExec.getDevMgmComHandler()) ? idepExec
						: null;
			}
		}

		return null;
	}

	public IDeploymentExecutor getDeploymentExecutor(final Device device) {
		return getDeploymentExecutor(device, null);
	}

	public static ArrayList<IDeploymentExecutor> loadDeploymentExecutors() {
		ArrayList<IDeploymentExecutor> deploymentExecutors = new ArrayList<IDeploymentExecutor>();
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] elems = registry.getConfigurationElementsFor(
				Activator.PLUGIN_ID, "downloadexecutor"); //$NON-NLS-1$
		for (int i = 0; i < elems.length; i++) {
			IConfigurationElement element = elems[i];
			try {
				Object object = element.createExecutableExtension("class"); //$NON-NLS-1$
				if (object instanceof IDeploymentExecutor) {
					deploymentExecutors.add((IDeploymentExecutor) object);
				}
			} catch (CoreException corex) {
				Activator.getDefault().logError(Messages.DeploymentCoordinator_ERROR_Message, corex);
			}
		}
		return deploymentExecutors;
	}

	private AbstractDeviceManagementCommunicationHandler getDevMgmCommunicationHandler(
			Device device) {
		if (null == deviceMangementCommunicationHandlers) {
			loadDeviceManagementCommunicationHandlers();
		}

		// TODO currently we only have one communication handler. Extend this
		// here
		if (deviceMangementCommunicationHandlers.size() > 0) {
			return deviceMangementCommunicationHandlers.get(0);
		}
		return null;
	}

	private void loadDeviceManagementCommunicationHandlers() {
		deviceMangementCommunicationHandlers = new ArrayList<AbstractDeviceManagementCommunicationHandler>();
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] elems = registry.getConfigurationElementsFor(
				Activator.PLUGIN_ID, "devicemanagementcommunicationhandler"); //$NON-NLS-1$

		for (int i = 0; i < elems.length; i++) {
			IConfigurationElement element = elems[i];
			try {
				Object object = element.createExecutableExtension("class"); //$NON-NLS-1$
				if (object instanceof AbstractDeviceManagementCommunicationHandler) {
					deviceMangementCommunicationHandlers
							.add((AbstractDeviceManagementCommunicationHandler) object);
				}
			} catch (CoreException corex) {
				Activator.getDefault().logError(Messages.DeploymentCoordinator_ERROR_Message, corex);
			}
		}
	}

	/** The listeners. */
	private final ArrayList<IDeploymentListener> listeners = new ArrayList<IDeploymentListener>();

	/**
	 * Response received x.
	 * 
	 * @param response
	 *            the response
	 * @param source
	 *            the source
	 */
	private void responseReceivedX(final String response, final String source) {
		for (Iterator<IDeploymentListener> iterator = listeners.iterator(); iterator
				.hasNext();) {
			IDeploymentListener listener = iterator.next();
			listener.responseReceived(response, source);
		}
	}

	/**
	 * Post command sent x.
	 * 
	 * @param command
	 *            the command
	 * @param destination
	 *            the destination
	 */
	private void postCommandSentX(final String command, final String destination) {
		for (Iterator<IDeploymentListener> iterator = listeners.iterator(); iterator
				.hasNext();) {
			IDeploymentListener listener = iterator.next();
			listener.postCommandSent(command, destination);
		}
	}

	private void postCommandSentX(final String info, final String destination,
			final String command) {
		for (Iterator<IDeploymentListener> iterator = listeners.iterator(); iterator
				.hasNext();) {
			IDeploymentListener listener = iterator.next();
			listener.postCommandSent(info, destination, command);
		}
	}

	/**
	 * Post command sent x.
	 * 
	 * @param message
	 *            the message unformatted
	 */
	private void postCommandSentX(final String message) {
		for (Iterator<IDeploymentListener> iterator = listeners.iterator(); iterator
				.hasNext();) {
			IDeploymentListener listener = iterator.next();
			listener.postCommandSent(message);
		}
	}

	/**
	 * Finished x.
	 */
	private void finishedX() {
		for (Iterator<IDeploymentListener> iterator = listeners.iterator(); iterator
				.hasNext();) {
			IDeploymentListener listener = iterator.next();
			listener.finished();
		}
	}

	/**
	 * Adds the deployment listener.
	 * 
	 * @param listener
	 *            the listener
	 */
	public void addDeploymentListener(final IDeploymentListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	/**
	 * Removes the deployment listener.
	 * 
	 * @param listener
	 *            the listener
	 */
	public void removeDeploymentListener(final IDeploymentListener listener) {
		if (listeners.contains(listener)) {
			listeners.remove(listener);
		}
	}

	/**
	 * Enable output.
	 * 
	 * @param executor
	 *            the executor
	 */
	public void enableOutput(
			AbstractDeviceManagementCommunicationHandler handler) {
		handler.addDeploymentListener(listener);
	}

	/**
	 * Flush.
	 */
	public void flush() {
		finishedX();
	}

	/**
	 * Disable output.
	 * 
	 * @param executor
	 *            the executor
	 */
	public void disableOutput(
			AbstractDeviceManagementCommunicationHandler handler) {
		handler.removeDeploymentListener(listener);
	}

}
