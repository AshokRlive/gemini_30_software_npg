/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment.exceptions;

/**
 * The Class WriteFBParameterException.
 */
public class WriteFBParameterException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new write fb parameter exception.
	 * 
	 * @param message the message
	 */
	public WriteFBParameterException(final String message) {
		super(message);
	}

}
