package org.fordiac.monitoring;

import java.util.ArrayList;

import org.eclipse.emf.ecore.EObject;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.fbt.typeeditor.network.viewer.CompositeNetworkViewerEditPart;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.ResourceFBNetwork;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.model.gen.monitoring.MonitoringFactory;
import org.fordiac.model.gen.monitoring.PortElement;

public class MonitoringManagerUtils {
	
	private MonitoringManagerUtils() {
		throw new AssertionError();  //class should not be instantiated
	}

	public static boolean canBeMonitored(org.fordiac.ide.gef.editparts.InterfaceEditPart editPart) {
		PortElement port = MonitoringManagerUtils.createPortElement(editPart);  //FIXME think how we can get away without creating a port element 
		return ((port != null) && (port.getPortString() != null));
	}
	
	public static boolean canBeMonitored(FBEditPart obj) {
		// As a first solution try to find the first interface editpart and see if we can monitoring
		for (Object child : obj.getChildren()) {
			if(child instanceof InterfaceEditPart){
				return canBeMonitored((InterfaceEditPart)child);
			}
		}
		return false;
	}

	public static PortElement createPortElement(org.fordiac.ide.gef.editparts.InterfaceEditPart editPart) {
		if (editPart.getParent() instanceof FBEditPart
				&& editPart.getParent().getParent() instanceof CompositeNetworkViewerEditPart) {
			return createCompositeInternalPortString(editPart);
		}

		Object obj = editPart.getCastedModel().getIInterfaceElement().eContainer().eContainer();
		if(obj instanceof FB){
			FB fb = (FB)obj; 
			return createPortElement(fb, editPart);					
		}
		
		return null;

	}

	private static PortElement createPortElement(FB fb,
			org.fordiac.ide.gef.editparts.InterfaceEditPart ep) {
		ResourceFBNetwork resFBNetwork = findResourceFBNetwork(fb);
		if (resFBNetwork == null) {
			return null; // can not be monitored because the fb is not mapped
		}

		Resource res = (Resource) resFBNetwork.eContainer();
		if (res == null) {
			return null;
		}
		Device dev = (Device) res.eContainer();
		if (dev == null) {
			return null;
		}
		
		AutomationSystem system = dev.getAutomationSystem();

		PortElement p;
		if (ep.getCastedModel().getIInterfaceElement() instanceof AdapterDeclaration)
		{
			p = MonitoringFactory.eINSTANCE.createAdapterPortElement(); 
		}
		else
		{
			p = MonitoringFactory.eINSTANCE.createPortElement();
		}
		p.setSystem(system);
		p.setDevice(dev);
		p.setResource(res);
		p.setFb(fb);
		p.setInterfaceElement(ep.getCastedModel().getIInterfaceElement());
		return p;
	}

	private static PortElement createCompositeInternalPortString(
			org.fordiac.ide.gef.editparts.InterfaceEditPart editPart) {
		
		FBEditPart fbep = (FBEditPart)editPart.getParent();
		CompositeNetworkViewerEditPart cnep = (CompositeNetworkViewerEditPart) editPart
				.getParent().getParent();

		ArrayList<CompositeNetworkViewerEditPart> parents = new ArrayList<>();

		CompositeNetworkViewerEditPart root = cnep; 
		parents.add(0, root);
		while (root.getparentInstanceViewerEditPart() != null) {
			parents.add(0, root.getparentInstanceViewerEditPart());
			root = root.getparentInstanceViewerEditPart();
		}

		FB fb = root.getFbInstance();
		PortElement pe = createPortElement(fb, editPart);
		if (pe != null) {
			pe.setFb(fbep.getCastedModel().getFb());

			for (CompositeNetworkViewerEditPart compositeNetworkEditPart : parents) {
				pe.getHierarchy().add(
						compositeNetworkEditPart.getFbInstance().getName());
			}
			return pe;
		}
		return null;
	}

	private static ResourceFBNetwork findResourceFBNetwork(FB fb) {
		EObject container = fb.eContainer();
		
		if(container instanceof ResourceFBNetwork){
			//we have a resource FB
			return (ResourceFBNetwork)container;
		}
		
		while (!(container instanceof FBNetwork)) {
			if (container instanceof SubAppNetwork) {
				ResourceFBNetwork resourceNetwork = ((SubAppNetwork) container)
						.getParentSubApp().getResource();
				if (resourceNetwork != null) {
					return resourceNetwork;
				}
				container = ((SubAppNetwork) container).getParentSubApp()
						.eContainer();
			}
		}
		return fb.getResource();
	}

	
}
