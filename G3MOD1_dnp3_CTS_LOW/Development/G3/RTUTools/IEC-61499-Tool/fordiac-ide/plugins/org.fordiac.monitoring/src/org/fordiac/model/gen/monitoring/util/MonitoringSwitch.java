/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.model.gen.monitoring.util;


import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.fordiac.ide.gef.IEditPartCreator;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.View;
import org.fordiac.model.gen.monitoring.*;
import org.fordiac.model.gen.monitoring.AdapterMonitoringInterfaceElementView;
import org.fordiac.model.gen.monitoring.AdapterPortElement;
import org.fordiac.model.gen.monitoring.Breakpoints;
import org.fordiac.model.gen.monitoring.MonitoringAdapterElement;
import org.fordiac.model.gen.monitoring.MonitoringBaseElement;
import org.fordiac.model.gen.monitoring.MonitoringElement;
import org.fordiac.model.gen.monitoring.MonitoringPackage;
import org.fordiac.model.gen.monitoring.PortElement;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.fordiac.model.gen.monitoring.MonitoringPackage
 * @generated
 */
public class MonitoringSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MonitoringPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitoringSwitch() {
		if (modelPackage == null) {
			modelPackage = MonitoringPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case MonitoringPackage.MONITORING_BASE_ELEMENT: {
				MonitoringBaseElement monitoringBaseElement = (MonitoringBaseElement)theEObject;
				T result = caseMonitoringBaseElement(monitoringBaseElement);
				if (result == null) result = caseIEditPartCreator(monitoringBaseElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MonitoringPackage.MONITORING_ELEMENT: {
				MonitoringElement monitoringElement = (MonitoringElement)theEObject;
				T result = caseMonitoringElement(monitoringElement);
				if (result == null) result = caseMonitoringBaseElement(monitoringElement);
				if (result == null) result = caseIEditPartCreator(monitoringElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MonitoringPackage.MONITORING_ADAPTER_ELEMENT: {
				MonitoringAdapterElement monitoringAdapterElement = (MonitoringAdapterElement)theEObject;
				T result = caseMonitoringAdapterElement(monitoringAdapterElement);
				if (result == null) result = caseMonitoringBaseElement(monitoringAdapterElement);
				if (result == null) result = caseIEditPartCreator(monitoringAdapterElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MonitoringPackage.BREAKPOINTS: {
				Breakpoints breakpoints = (Breakpoints)theEObject;
				T result = caseBreakpoints(breakpoints);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MonitoringPackage.IEDIT_PART_CREATOR: {
				IEditPartCreator iEditPartCreator = (IEditPartCreator)theEObject;
				T result = caseIEditPartCreator(iEditPartCreator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MonitoringPackage.PORT_ELEMENT: {
				PortElement portElement = (PortElement)theEObject;
				T result = casePortElement(portElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MonitoringPackage.ADAPTER_PORT_ELEMENT: {
				AdapterPortElement adapterPortElement = (AdapterPortElement)theEObject;
				T result = caseAdapterPortElement(adapterPortElement);
				if (result == null) result = casePortElement(adapterPortElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MonitoringPackage.ADAPTER_MONITORING_INTERFACE_ELEMENT_VIEW: {
				AdapterMonitoringInterfaceElementView adapterMonitoringInterfaceElementView = (AdapterMonitoringInterfaceElementView)theEObject;
				T result = caseAdapterMonitoringInterfaceElementView(adapterMonitoringInterfaceElementView);
				if (result == null) result = caseInterfaceElementView(adapterMonitoringInterfaceElementView);
				if (result == null) result = caseIEditPartCreator(adapterMonitoringInterfaceElementView);
				if (result == null) result = caseView(adapterMonitoringInterfaceElementView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMonitoringBaseElement(MonitoringBaseElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMonitoringElement(MonitoringElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Adapter Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Adapter Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMonitoringAdapterElement(MonitoringAdapterElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Breakpoints</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Breakpoints</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBreakpoints(Breakpoints object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IEdit Part Creator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IEdit Part Creator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIEditPartCreator(IEditPartCreator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortElement(PortElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Adapter Port Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Adapter Port Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAdapterPortElement(AdapterPortElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Adapter Monitoring Interface Element View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Adapter Monitoring Interface Element View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAdapterMonitoringInterfaceElementView(AdapterMonitoringInterfaceElementView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseView(View object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interface Element View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interface Element View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterfaceElementView(InterfaceElementView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //MonitoringSwitch
