/**
 */
package org.fordiac.model.gen.monitoring.impl;

import java.util.Hashtable;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.gef.EditPart;
import org.fordiac.ide.model.ui.impl.InterfaceElementViewImpl;
import org.fordiac.model.gen.monitoring.AdapterMonitoringInterfaceElementView;
import org.fordiac.model.gen.monitoring.MonitoringPackage;
import org.fordiac.monitoring.editparts.MonitoringAdapterInterfaceEditPart;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Adapter Monitoring Interface Element View</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AdapterMonitoringInterfaceElementViewImpl extends InterfaceElementViewImpl implements AdapterMonitoringInterfaceElementView {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AdapterMonitoringInterfaceElementViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MonitoringPackage.Literals.ADAPTER_MONITORING_INTERFACE_ELEMENT_VIEW;
	}


	//TODO find a better way how to add edit part creation to the model
	protected Hashtable<String, MonitoringAdapterInterfaceEditPart> diagramEPMapping = new Hashtable<String, MonitoringAdapterInterfaceEditPart>();
	
	@Override
	public EditPart createEditPart(String diagram) {
		if (!diagramEPMapping.containsKey(diagram)) {
			MonitoringAdapterInterfaceEditPart part = new MonitoringAdapterInterfaceEditPart();
			diagramEPMapping.put(diagram, part);
		}
		return diagramEPMapping.get(diagram);
	}

} //AdapterMonitoringInterfaceElementViewImpl
