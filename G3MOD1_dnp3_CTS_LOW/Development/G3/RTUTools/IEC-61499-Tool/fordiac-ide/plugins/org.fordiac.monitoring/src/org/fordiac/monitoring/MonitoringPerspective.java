/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class MonitoringPerspective implements IPerspectiveFactory {

	private IPageLayout factory;

	@Override
	public void createInitialLayout(final IPageLayout layout) {
		this.factory = layout;

		layout.setEditorAreaVisible(true);
		layout.setFixed(false);

		//
		// layout.createFolder("breakpoints", relationship, ratio, refId)

		IFolderLayout left = layout.createFolder(
				"left", IPageLayout.LEFT, 0.20f, layout.getEditorArea()); //$NON-NLS-1$
		left.addView("org.fordiac.ide.systemmanagement.ui.systemexplorer");

		IFolderLayout bottomLeft = layout.createFolder(
				"bottomLeft", IPageLayout.BOTTOM, 0.7f, "left"); //$NON-NLS-1$	//$NON-NLS-2$
		bottomLeft.addView(IPageLayout.ID_OUTLINE);

		IFolderLayout top = layout.createFolder("top", IPageLayout.TOP, 0.25f,
				layout.getEditorArea());

		top.addView("org.fordiac.monitoring.views.BreakpointsView");

		IFolderLayout topRight = layout.createFolder("topRight", IPageLayout.RIGHT,
				0.5f, "top");

		topRight.addView("org.fordiac.monitoring.views.WatchesView");

		addPerspectiveShortcuts();
	}

	private void addPerspectiveShortcuts() {
		factory.addShowViewShortcut(IPageLayout.ID_OUTLINE);
		factory.addShowViewShortcut(IPageLayout.ID_PROP_SHEET);

		factory
				.addNewWizardShortcut("org.fordiac.systemmanagement.ui.wizard.NewSystemWizard");
		factory
				.addNewWizardShortcut("org.fordiac.systemmanagement.ui.wizard.NewApplicationWizard");
		factory
				.addNewWizardShortcut("org.fordiac.ide.fbt.typeeditor.wizard.NewFBTypeWizard");
		factory
				.addPerspectiveShortcut("org.fordiac.ide.deployment.ui.perspectives.DeploymentPerspective"); //$NON-NLS-1$
		factory
				.addPerspectiveShortcut("org.fordiac.ide.fbt.typemanagement.perspective1");
	}

}
