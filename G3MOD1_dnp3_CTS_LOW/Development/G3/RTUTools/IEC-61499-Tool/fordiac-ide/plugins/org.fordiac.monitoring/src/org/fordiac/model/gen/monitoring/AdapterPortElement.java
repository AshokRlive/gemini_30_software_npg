/**
 */
package org.fordiac.model.gen.monitoring;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Adapter Port Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.model.gen.monitoring.AdapterPortElement#getPorts <em>Ports</em>}</li>
 * </ul>
 *
 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getAdapterPortElement()
 * @model
 * @generated
 */
public interface AdapterPortElement extends PortElement {
	/**
	 * Returns the value of the '<em><b>Ports</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.model.gen.monitoring.PortElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ports</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ports</em>' containment reference list.
	 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getAdapterPortElement_Ports()
	 * @model containment="true"
	 * @generated
	 */
	EList<PortElement> getPorts();

} // AdapterPortElement
