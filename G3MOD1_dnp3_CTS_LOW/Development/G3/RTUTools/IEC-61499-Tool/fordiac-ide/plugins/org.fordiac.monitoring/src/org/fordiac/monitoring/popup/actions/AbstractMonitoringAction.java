/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.popup.actions;

import java.util.Iterator;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.RootEditPart;
import org.eclipse.jface.action.IAction;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.fbt.typeeditor.network.viewer.CompositeInstanceViewer;
import org.fordiac.ide.gef.DiagramEditor;
import org.fordiac.ide.gef.DiagramEditorWithFlyoutPalette;
import org.fordiac.ide.resourceediting.editors.ResourceDiagramEditor;

public abstract class AbstractMonitoringAction implements IObjectActionDelegate {

	//todo find better solution for this
	private DiagramEditorWithFlyoutPalette editorWithPalette = null;
	private DiagramEditor editor = null;

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		if ((targetPart instanceof FBNetworkEditor)
				|| (targetPart instanceof ResourceDiagramEditor)){
			DiagramEditorWithFlyoutPalette editorW = (DiagramEditorWithFlyoutPalette)targetPart;
			editorWithPalette = editorW;
		}else if(targetPart instanceof CompositeInstanceViewer) {
			DiagramEditor editor = (DiagramEditor) targetPart;
			this.editor = editor;						
		}

	}

	protected void refreshEditor() {
		RootEditPart rootEditPart = null;
		if (null != editor) {
			rootEditPart = editor.getViewer().getRootEditPart(); 
		} else if(null != editorWithPalette){
			rootEditPart = editorWithPalette.getViewer().getRootEditPart();
		}
		if(null != rootEditPart){ 
			refresh(rootEditPart);
		}
	}

	@SuppressWarnings("rawtypes")
	private static void refresh(RootEditPart rootEditPart) {
		rootEditPart.refresh();
		for (Iterator iterator = rootEditPart.getChildren().iterator(); iterator.hasNext();) {
			EditPart part = (EditPart) iterator.next();
			part.refresh();
		}
	}
}
