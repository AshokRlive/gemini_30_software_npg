/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.fordiac.ide.application.figures.FBFigure;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.model.gen.monitoring.MonitoringAdapterElement;

public class MonitoringAdapterEditPart extends MonitoringEditPart {
	
	
	@Override
	protected IFigure createFigureForModel() {
		return new FBFigure(getFBView(), null);
	}

	private FBView getFBView(){
		return ((MonitoringAdapterElement)getCastedModel()).getMonitoredAdapterFBView();
	}

	@Override
	public void updateBreakpoint() {
	}

	private FBFigure getCastedFigure() {
		if (getFigure() instanceof FBFigure) {
			return (FBFigure) getFigure();
		}
		return null;
	}
	
	@Override
	protected void createEditPolicies() {
		//currently for adapters we don't need any edit policies
	}
	
	@Override
	public boolean understandsRequest(Request request) {
		if (request.getType() == RequestConstants.REQ_DIRECT_EDIT
				|| request.getType() == RequestConstants.REQ_OPEN) {
			//no direct edit for the monitored adapter fb
			return false;
		} 
		return super.understandsRequest(request);
	}

	@Override
	public void performRequest(Request request) {
		if (request.getType() == RequestConstants.REQ_DIRECT_EDIT
				|| request.getType() == RequestConstants.REQ_OPEN) {
			//no direct edit for the monitored adapter fb
		}else {
			super.performRequest(request);
		}
	}

	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof InterfaceEditPart) {
			InterfaceEditPart interfaceEditPart = (InterfaceEditPart) childEditPart;
			if (interfaceEditPart.isInput()) {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventInputs().add(child);
				} else{
					if(interfaceEditPart.isAdapter()){
						getCastedFigure().getSockets().add(child);
						interfaceEditPart.setSystemPalette(getCastedModel().getPort().getFb().getPaletteEntry().getGroup().getPallete());
					}else if (interfaceEditPart.isVariable()) {
						getCastedFigure().getDataInputs().add(child);
					}
				}
			} else {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventOutputs().add(child);
				} else { 
					if(interfaceEditPart.isAdapter()){
						getCastedFigure().getPlugs().add(child);
						interfaceEditPart.setSystemPalette(getCastedModel().getPort().getFb().getPaletteEntry().getGroup().getPallete());
					}else if (interfaceEditPart.isVariable()) {
						getCastedFigure().getDataOutputs().add(child);
					}
				}

			}
		} else {
			super.addChildVisual(childEditPart, index);
		}
	}

	@Override
	protected void removeChildVisual(final EditPart childEditPart) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof InterfaceEditPart) {
			InterfaceEditPart interfaceEditPart = (InterfaceEditPart) childEditPart;
			if (interfaceEditPart.isInput()) {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventInputs().remove(child);
				} else { 
					if(interfaceEditPart.isAdapter()){
						getCastedFigure().getSockets().remove(child);
					}else if (interfaceEditPart.isVariable()) {
						getCastedFigure().getDataInputs().remove(child);
					}	
				}
			} else {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventOutputs().remove(child);
				} else { 
					if(interfaceEditPart.isAdapter()){
						getCastedFigure().getPlugs().remove(child);
					}else if (interfaceEditPart.isVariable()) {
						getCastedFigure().getDataOutputs().remove(child);
					}
				}

			}
		} else {
			super.removeChildVisual(childEditPart);
		}
	}

	@Override
	protected List<Object> getModelChildren() {
		ArrayList<Object> elements = new ArrayList<Object>();
		elements.addAll(getFBView().getInterfaceElements());
		return elements;
	}
}
