/**
 */
package org.fordiac.model.gen.monitoring;

import org.eclipse.emf.common.util.EList;
import org.fordiac.ide.model.ui.FBView;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Adapter Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.model.gen.monitoring.MonitoringAdapterElement#getElements <em>Elements</em>}</li>
 *   <li>{@link org.fordiac.model.gen.monitoring.MonitoringAdapterElement#getMonitoredAdapterFBView <em>Monitored Adapter FB View</em>}</li>
 * </ul>
 *
 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getMonitoringAdapterElement()
 * @model
 * @generated
 */
public interface MonitoringAdapterElement extends MonitoringBaseElement {

	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.model.gen.monitoring.MonitoringElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getMonitoringAdapterElement_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<MonitoringElement> getElements();

	/**
	 * Returns the value of the '<em><b>Monitored Adapter FB View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Monitored Adapter FB View</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Monitored Adapter FB View</em>' reference.
	 * @see #setMonitoredAdapterFBView(FBView)
	 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getMonitoringAdapterElement_MonitoredAdapterFBView()
	 * @model
	 * @generated
	 */
	FBView getMonitoredAdapterFBView();

	/**
	 * Sets the value of the '{@link org.fordiac.model.gen.monitoring.MonitoringAdapterElement#getMonitoredAdapterFBView <em>Monitored Adapter FB View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Monitored Adapter FB View</em>' reference.
	 * @see #getMonitoredAdapterFBView()
	 * @generated
	 */
	void setMonitoredAdapterFBView(FBView value);
} // MonitoringAdapterElement
