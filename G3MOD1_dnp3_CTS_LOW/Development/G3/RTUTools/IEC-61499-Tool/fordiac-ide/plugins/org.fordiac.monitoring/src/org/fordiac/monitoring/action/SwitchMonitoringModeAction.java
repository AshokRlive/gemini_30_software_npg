/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.action;

import java.util.Iterator;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowPulldownDelegate2;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.monitoring.MonitoredSystems;
import org.fordiac.monitoring.MonitoringManager;
import org.fordiac.systemmanagement.SystemManager;


public class SwitchMonitoringModeAction implements
		IWorkbenchWindowPulldownDelegate2 {

	boolean selectAll = true;

	@Override
	public Menu getMenu(Menu parent) {
		return null;
	}

	@Override
	public Menu getMenu(Control parent) {
		Menu menu = new Menu(parent);
		MonitoredSystems.createMenuEntriesForSystems(menu);
		return menu;
	}	
	
	@Override
	public void dispose() {

	}

	@Override
	public void init(IWorkbenchWindow window) {
	}

	@Override
	public void run(IAction action) {
		if (selectAll) {
			for (Iterator<AutomationSystem> iterator = SystemManager.getInstance().getSystems()
					.iterator(); iterator.hasNext();) {
				AutomationSystem system = iterator.next();
				MonitoringManager.getInstance().enableSystem(system.getName());
			}
			selectAll = false;
		} else {
			for (Iterator<AutomationSystem> iterator = SystemManager.getInstance().getSystems()
					.iterator(); iterator.hasNext();) {
				AutomationSystem system = iterator.next();
				MonitoringManager.getInstance().disableSystem(system.getName());
			}
			selectAll = true;
		}
		MonitoredSystems.refreshSystemTree();
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
	}
	
}
