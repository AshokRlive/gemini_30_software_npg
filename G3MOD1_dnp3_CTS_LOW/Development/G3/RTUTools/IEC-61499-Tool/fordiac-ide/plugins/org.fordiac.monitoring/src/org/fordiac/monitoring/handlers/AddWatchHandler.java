/*******************************************************************************
 * Copyright (c) 2015, 2016 fortiss GmbH
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Gerd Kainz, Alois Zoitl - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.fordiac.monitoring.handlers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISources;
import org.eclipse.ui.handlers.HandlerUtil;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.AdapterFB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.model.gen.monitoring.AdapterPortElement;
import org.fordiac.model.gen.monitoring.MonitoringAdapterElement;
import org.fordiac.model.gen.monitoring.MonitoringBaseElement;
import org.fordiac.model.gen.monitoring.MonitoringElement;
import org.fordiac.model.gen.monitoring.MonitoringFactory;
import org.fordiac.model.gen.monitoring.PortElement;
import org.fordiac.monitoring.MonitoringManager;
import org.fordiac.monitoring.MonitoringManagerUtils;
import org.fordiac.monitoring.editparts.MonitoringAdapterInterfaceEditPart;

public class AddWatchHandler extends AbstractMonitoringHandler {
	
	@SuppressWarnings("rawtypes")
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		super.execute(event);
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		MonitoringManager manager = MonitoringManager.getInstance();
		
		if (selection instanceof StructuredSelection) {
			StructuredSelection sel = (StructuredSelection) selection;
			for (Iterator iterator = sel.iterator(); iterator.hasNext();) {
				Object obj = iterator.next();
				if (obj instanceof InterfaceEditPart) {
					InterfaceEditPart editPart = (InterfaceEditPart) obj;				
					if (!manager.containsPort(editPart.getCastedModel().getIInterfaceElement())) {
						PortElement port = MonitoringManagerUtils.createPortElement(editPart);
						createMonitoringElement(manager, port);
					}
				}
			}			
			refreshEditor();
		}
		
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setEnabled(Object evaluationContext){
		boolean needToAdd = false;
		Object selection = HandlerUtil.getVariable(evaluationContext, ISources.ACTIVE_CURRENT_SELECTION_NAME);
		
		if (selection instanceof StructuredSelection) {
			StructuredSelection sel = (StructuredSelection) selection;
			MonitoringManager manager = MonitoringManager.getInstance();

			for (Iterator iterator = sel.iterator(); iterator.hasNext();) {
				Object obj = iterator.next();
				if ((obj instanceof InterfaceEditPart) && 
						!(obj instanceof MonitoringAdapterInterfaceEditPart)){
					InterfaceEditPart editPart = (InterfaceEditPart) obj;
					if(MonitoringManagerUtils.canBeMonitored(editPart)
								&& !manager.containsPort(editPart.getCastedModel().getIInterfaceElement())) {
						needToAdd = true;
						break; // can return from loop because one is enough to enable the action
					}
				}
			}
		}
		setBaseEnabled(needToAdd);
	}
	
	protected MonitoringBaseElement createMonitoringElement(MonitoringManager manager, PortElement port) {
		MonitoringBaseElement element;
		if (port instanceof AdapterPortElement) {
			element = MonitoringFactory.eINSTANCE.createMonitoringAdapterElement();
		}
		else {
			element = MonitoringFactory.eINSTANCE.createMonitoringElement();
		}
		element.setPort(port);
		manager.addMonitoringElement(element);

		if (port instanceof AdapterPortElement) {
			MonitoringAdapterElement adpaterElement = (MonitoringAdapterElement)element;
			createMonitoringElementsForAdapterInterface(manager, adpaterElement);
		}

		return element;
	}

	private void createMonitoringElementsForAdapterInterface(MonitoringManager manager, MonitoringAdapterElement adpaterElement) {
		createMonitoredAdpaterFBView(adpaterElement);
		refreshEditor();
		
		PortElement port = adpaterElement.getPort();

		List<MonitoringElement> childElements = adpaterElement.getElements();
		InterfaceList interfaceList =  adpaterElement.getMonitoredAdapterFBView().getFb().getInterface();
		List<PortElement> ports = ((AdapterPortElement)port).getPorts();

		MonitoringFactory monitoringFactory = MonitoringFactory.eINSTANCE;
		ArrayList<IInterfaceElement> ios = new ArrayList<>();
		ios.addAll(interfaceList.getEventInputs());
		ios.addAll(interfaceList.getEventOutputs());
		ios.addAll(interfaceList.getInputVars());
		ios.addAll(interfaceList.getOutputVars());
		for (IInterfaceElement io : ios) {
			PortElement newPort = monitoringFactory.createPortElement();
			newPort.setDevice(port.getDevice());
			newPort.setFb(port.getFb());
			newPort.setInterfaceElement(io);
			newPort.setResource(port.getResource());
			newPort.setSystem(port.getSystem());
			ports.add(newPort);
			childElements.add((MonitoringElement)createMonitoringElement(manager, newPort));
		}
	}

	private static void createMonitoredAdpaterFBView(MonitoringAdapterElement adpaterElement) {
		AdapterFB fb = LibraryElementFactory.eINSTANCE.createAdapterFB();
		
		IInterfaceElement interfaceElement = adpaterElement.getPort().getInterfaceElement();
		
		Palette palette = ((UIFBNetwork)interfaceElement.eResource().getContents().get(0)).getFbNetwork().getApplication().getAutomationSystem().getPalette();
		List<PaletteEntry> types = palette.getTypeEntries(((AdapterDeclaration)interfaceElement).getType().getName());
		
		fb.setPaletteEntry(types.get(0));	
		fb.setPlug(!interfaceElement.isIsInput());
		fb.setFbtPath(fb.getPaletteEntry().getProjectRelativeTypePath());
		fb.setId(EcoreUtil.generateUUID());
		fb.setInterface(EcoreUtil.copy(fb.getFBType().getInterfaceList()));
		fb.setIdentifier(true);
		fb.setResourceFB(false);
		
		ArrayList<IInterfaceElement> iInterfaceElements = new ArrayList<>();
		iInterfaceElements.addAll(fb.getInterface().getInputVars());

		for (IInterfaceElement element : iInterfaceElements) {
			Value value = LibraryElementFactory.eINSTANCE.createValue();
			element.setValue(value);
		}
	
		//fb.setName(((AdapterDeclaration)getCastedModel().getInterfaceElement()).getName());
		FBNetwork parent = (FBNetwork)adpaterElement.getPort().getFb().eContainer();
		FBView view = UiFactory.eINSTANCE.createFBView();
		view.setFb(fb);

		InterfaceList interfaceList = fb.getInterface();
		if (interfaceList != null) {
			ArrayList<IInterfaceElement> list = new ArrayList<>();
			list.addAll(interfaceList.getInputVars());
			list.addAll(interfaceList.getOutputVars());
			list.addAll(interfaceList.getEventInputs());
			list.addAll(interfaceList.getEventOutputs());
			for (IInterfaceElement element : list) {
				view.getInterfaceElements().add(createInterfaceElementView(element, parent));
			}
		}
		adpaterElement.setMonitoredAdapterFBView(view);
	}
	
	private static InterfaceElementView createInterfaceElementView(IInterfaceElement element, SubAppNetwork parent) {
		InterfaceElementView view =  MonitoringFactory.eINSTANCE.createAdapterMonitoringInterfaceElementView();
		view.setIInterfaceElement(element);
		view.setFbNetwork(parent);
		return view;
	}
}
