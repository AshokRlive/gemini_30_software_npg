/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.handlers;

import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.RootEditPart;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.fbt.typeeditor.network.viewer.CompositeInstanceViewer;
import org.fordiac.ide.gef.DiagramEditor;
import org.fordiac.ide.gef.DiagramEditorWithFlyoutPalette;
import org.fordiac.ide.resourceediting.editors.ResourceDiagramEditor;

public abstract class AbstractMonitoringHandler extends AbstractHandler {

	RootEditPart rootEditPart = null;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		setEditor(HandlerUtil.getActiveEditor(event));
		return null;
	}
	
	protected void setEditor(IEditorPart activeEditor) {
		if ((activeEditor instanceof FBNetworkEditor)
				|| (activeEditor instanceof ResourceDiagramEditor)){
			DiagramEditorWithFlyoutPalette editorW = (DiagramEditorWithFlyoutPalette)activeEditor;
			rootEditPart = editorW.getViewer().getRootEditPart();
		}else if(activeEditor instanceof CompositeInstanceViewer) {
			DiagramEditor editor = (DiagramEditor) activeEditor;
			rootEditPart = editor.getViewer().getRootEditPart(); 			
		}else{
			rootEditPart = null;
		}		
	}

	protected void refreshEditor() {
		if(null != rootEditPart){ 
			refresh(rootEditPart);
		}
	}

	@SuppressWarnings("rawtypes")
	private static void refresh(RootEditPart rootEditPart) {
		rootEditPart.refresh();
		for (Iterator iterator = rootEditPart.getChildren().iterator(); iterator.hasNext();) {
			EditPart part = (EditPart) iterator.next();
			part.refresh();
		}
	}
}
