/**
 */
package org.fordiac.model.gen.monitoring;

import org.fordiac.ide.gef.IEditPartCreator;
import org.fordiac.ide.model.ui.InterfaceElementView;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Adapter Monitoring Interface Element View</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getAdapterMonitoringInterfaceElementView()
 * @model superTypes="org.fordiac.ide.model.ui.InterfaceElementView org.fordiac.model.gen.monitoring.IEditPartCreator"
 * @generated
 */
public interface AdapterMonitoringInterfaceElementView extends InterfaceElementView, IEditPartCreator {

} // AdapterMonitoringInterfaceElementView
