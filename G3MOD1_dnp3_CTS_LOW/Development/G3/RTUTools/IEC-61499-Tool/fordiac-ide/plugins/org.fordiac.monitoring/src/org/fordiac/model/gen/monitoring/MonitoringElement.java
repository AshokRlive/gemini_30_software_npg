/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.model.gen.monitoring;


/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Element</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.model.gen.monitoring.MonitoringElement#isForce <em>Force</em>}</li>
 *   <li>{@link org.fordiac.model.gen.monitoring.MonitoringElement#getForceValue <em>Force Value</em>}</li>
 *   <li>{@link org.fordiac.model.gen.monitoring.MonitoringElement#isBreakpoint <em>Breakpoint</em>}</li>
 *   <li>{@link org.fordiac.model.gen.monitoring.MonitoringElement#isBreakpointActive <em>Breakpoint Active</em>}</li>
 *   <li>{@link org.fordiac.model.gen.monitoring.MonitoringElement#getBreakpointCondition <em>Breakpoint Condition</em>}</li>
 *   <li>{@link org.fordiac.model.gen.monitoring.MonitoringElement#getCurrentValue <em>Current Value</em>}</li>
 *   <li>{@link org.fordiac.model.gen.monitoring.MonitoringElement#getSec <em>Sec</em>}</li>
 *   <li>{@link org.fordiac.model.gen.monitoring.MonitoringElement#getUsec <em>Usec</em>}</li>
 * </ul>
 *
 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getMonitoringElement()
 * @model
 * @generated
 */
public interface MonitoringElement extends MonitoringBaseElement{
	/**
	 * Returns the value of the '<em><b>Force</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Force</em>' attribute isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Force</em>' attribute.
	 * @see #setForce(boolean)
	 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getMonitoringElement_Force()
	 * @model default="false" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isForce();

	/**
	 * Sets the value of the '{@link org.fordiac.model.gen.monitoring.MonitoringElement#isForce <em>Force</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Force</em>' attribute.
	 * @see #isForce()
	 * @generated
	 */
	void setForce(boolean value);

	/**
	 * Returns the value of the '<em><b>Force Value</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Force Value</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Force Value</em>' attribute.
	 * @see #setForceValue(String)
	 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getMonitoringElement_ForceValue()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getForceValue();

	/**
	 * Sets the value of the '
	 * {@link org.fordiac.model.gen.monitoring.MonitoringElement#getForceValue
	 * <em>Force Value</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *          the new value of the '<em>Force Value</em>' attribute.
	 * @see #getForceValue()
	 * @generated
	 */
	void setForceValue(String value);

	/**
	 * Returns the value of the '<em><b>Breakpoint</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Breakpoint</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Breakpoint</em>' attribute.
	 * @see #setBreakpoint(boolean)
	 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getMonitoringElement_Breakpoint()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isBreakpoint();

	/**
	 * Sets the value of the '
	 * {@link org.fordiac.model.gen.monitoring.MonitoringElement#isBreakpoint
	 * <em>Breakpoint</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *          the new value of the '<em>Breakpoint</em>' attribute.
	 * @see #isBreakpoint()
	 * @generated
	 */
	void setBreakpoint(boolean value);

	/**
	 * Returns the value of the '<em><b>Breakpoint Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Breakpoint Active</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Breakpoint Active</em>' attribute.
	 * @see #setBreakpointActive(boolean)
	 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getMonitoringElement_BreakpointActive()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isBreakpointActive();

	/**
	 * Sets the value of the '{@link org.fordiac.model.gen.monitoring.MonitoringElement#isBreakpointActive <em>Breakpoint Active</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Breakpoint Active</em>' attribute.
	 * @see #isBreakpointActive()
	 * @generated
	 */
	void setBreakpointActive(boolean value);

	/**
	 * Returns the value of the '<em><b>Breakpoint Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Breakpoint Condition</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Breakpoint Condition</em>' attribute.
	 * @see #setBreakpointCondition(String)
	 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getMonitoringElement_BreakpointCondition()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getBreakpointCondition();

	/**
	 * Sets the value of the '{@link org.fordiac.model.gen.monitoring.MonitoringElement#getBreakpointCondition <em>Breakpoint Condition</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Breakpoint Condition</em>' attribute.
	 * @see #getBreakpointCondition()
	 * @generated
	 */
	void setBreakpointCondition(String value);

	/**
	 * Returns the value of the '<em><b>Current Value</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Value</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Current Value</em>' attribute.
	 * @see #setCurrentValue(String)
	 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getMonitoringElement_CurrentValue()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getCurrentValue();

	/**
	 * Sets the value of the '{@link org.fordiac.model.gen.monitoring.MonitoringElement#getCurrentValue <em>Current Value</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Current Value</em>' attribute.
	 * @see #getCurrentValue()
	 * @generated
	 */
	void setCurrentValue(String value);

	/**
	 * Returns the value of the '<em><b>Sec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sec</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sec</em>' attribute.
	 * @see #setSec(long)
	 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getMonitoringElement_Sec()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Long"
	 * @generated
	 */
	long getSec();

	/**
	 * Sets the value of the '{@link org.fordiac.model.gen.monitoring.MonitoringElement#getSec <em>Sec</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sec</em>' attribute.
	 * @see #getSec()
	 * @generated
	 */
	void setSec(long value);

	/**
	 * Returns the value of the '<em><b>Usec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Usec</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Usec</em>' attribute.
	 * @see #setUsec(long)
	 * @see org.fordiac.model.gen.monitoring.MonitoringPackage#getMonitoringElement_Usec()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Long"
	 * @generated
	 */
	long getUsec();

	/**
	 * Sets the value of the '{@link org.fordiac.model.gen.monitoring.MonitoringElement#getUsec <em>Usec</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Usec</em>' attribute.
	 * @see #getUsec()
	 * @generated
	 */
	void setUsec(long value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model valueDataType="org.eclipse.emf.ecore.xml.type.String" refreshDataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tsetCurrentValue(value);\r\n\t\tif (refresh) {\r\n\t\t\tDisplay.getDefault().asyncExec(new Runnable() {\r\n\t\t\t\t@Override\r\n\t\t\t\tpublic void run() {\r\n\t\t\t\t\tif (part != null) {\r\n\t\t\t\t\t\tpart.setValue(value);\r\n\t\t\t\t\t}\r\n\t\t\t\t}\r\n\t\t\t});\r\n\t\t}'"
	 * @generated
	 */
	void updateValue(String value, boolean refresh);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model valueDataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	void forceValue(String value);

	int getCurrentPos();
	
	String getHistoryValue(int pos, boolean time);
	public long getHistorySec(int pos) ;
	public long getHistoryUSec(int pos) ;
	
} // MonitoringElement
