/*******************************************************************************
 * Copyright (c) 2015 fortiss Gmbh
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Alois Zoitl - initial contribution and API
 *******************************************************************************/
package org.fordiac.monitoring.editparts;

import org.eclipse.gef.DragTracker;
import org.eclipse.gef.Request;
import org.fordiac.ide.application.editparts.InterfaceEditPart;

public class MonitoringAdapterInterfaceEditPart extends InterfaceEditPart {
	
	@Override
	public DragTracker getDragTracker(Request request) {
		return new org.eclipse.gef.tools.DragEditPartsTracker(this);
	}

}
