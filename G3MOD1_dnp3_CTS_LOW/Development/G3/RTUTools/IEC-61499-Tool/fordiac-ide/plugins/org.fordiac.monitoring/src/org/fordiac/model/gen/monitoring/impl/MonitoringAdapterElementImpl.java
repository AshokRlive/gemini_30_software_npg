/**
 */
package org.fordiac.model.gen.monitoring.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.gef.EditPart;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.model.gen.monitoring.MonitoringAdapterElement;
import org.fordiac.model.gen.monitoring.MonitoringElement;
import org.fordiac.model.gen.monitoring.MonitoringPackage;
import org.fordiac.monitoring.editparts.MonitoringAdapterEditPart;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Adapter Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.model.gen.monitoring.impl.MonitoringAdapterElementImpl#getElements <em>Elements</em>}</li>
 *   <li>{@link org.fordiac.model.gen.monitoring.impl.MonitoringAdapterElementImpl#getMonitoredAdapterFBView <em>Monitored Adapter FB View</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MonitoringAdapterElementImpl extends MonitoringBaseElementImpl implements MonitoringAdapterElement {
	/**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
	protected EList<MonitoringElement> elements;

	/**
	 * The cached value of the '{@link #getMonitoredAdapterFBView() <em>Monitored Adapter FB View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonitoredAdapterFBView()
	 * @generated
	 * @ordered
	 */
	protected FBView monitoredAdapterFBView;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonitoringAdapterElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MonitoringPackage.Literals.MONITORING_ADAPTER_ELEMENT;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MonitoringElement> getElements() {
		if (elements == null) {
			elements = new EObjectContainmentEList<MonitoringElement>(MonitoringElement.class, this, MonitoringPackage.MONITORING_ADAPTER_ELEMENT__ELEMENTS);
		}
		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FBView getMonitoredAdapterFBView() {
		if (monitoredAdapterFBView != null && monitoredAdapterFBView.eIsProxy()) {
			InternalEObject oldMonitoredAdapterFBView = (InternalEObject)monitoredAdapterFBView;
			monitoredAdapterFBView = (FBView)eResolveProxy(oldMonitoredAdapterFBView);
			if (monitoredAdapterFBView != oldMonitoredAdapterFBView) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MonitoringPackage.MONITORING_ADAPTER_ELEMENT__MONITORED_ADAPTER_FB_VIEW, oldMonitoredAdapterFBView, monitoredAdapterFBView));
			}
		}
		return monitoredAdapterFBView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FBView basicGetMonitoredAdapterFBView() {
		return monitoredAdapterFBView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMonitoredAdapterFBView(FBView newMonitoredAdapterFBView) {
		FBView oldMonitoredAdapterFBView = monitoredAdapterFBView;
		monitoredAdapterFBView = newMonitoredAdapterFBView;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MonitoringPackage.MONITORING_ADAPTER_ELEMENT__MONITORED_ADAPTER_FB_VIEW, oldMonitoredAdapterFBView, monitoredAdapterFBView));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MonitoringPackage.MONITORING_ADAPTER_ELEMENT__ELEMENTS:
				return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MonitoringPackage.MONITORING_ADAPTER_ELEMENT__ELEMENTS:
				return getElements();
			case MonitoringPackage.MONITORING_ADAPTER_ELEMENT__MONITORED_ADAPTER_FB_VIEW:
				if (resolve) return getMonitoredAdapterFBView();
				return basicGetMonitoredAdapterFBView();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MonitoringPackage.MONITORING_ADAPTER_ELEMENT__ELEMENTS:
				getElements().clear();
				getElements().addAll((Collection<? extends MonitoringElement>)newValue);
				return;
			case MonitoringPackage.MONITORING_ADAPTER_ELEMENT__MONITORED_ADAPTER_FB_VIEW:
				setMonitoredAdapterFBView((FBView)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MonitoringPackage.MONITORING_ADAPTER_ELEMENT__ELEMENTS:
				getElements().clear();
				return;
			case MonitoringPackage.MONITORING_ADAPTER_ELEMENT__MONITORED_ADAPTER_FB_VIEW:
				setMonitoredAdapterFBView((FBView)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MonitoringPackage.MONITORING_ADAPTER_ELEMENT__ELEMENTS:
				return elements != null && !elements.isEmpty();
			case MonitoringPackage.MONITORING_ADAPTER_ELEMENT__MONITORED_ADAPTER_FB_VIEW:
				return monitoredAdapterFBView != null;
		}
		return super.eIsSet(featureID);
	}

	@Override
	public EditPart createEditPart(String diagram) {
		if (!diagramEPMapping.containsKey(diagram)) {
			MonitoringAdapterEditPart part;
			part = new MonitoringAdapterEditPart();
			diagramEPMapping.put(diagram, part);
		}
		return diagramEPMapping.get(diagram);
	}

} //MonitoringAdapterElementImpl
