/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String P_WATCH_COLOR = "watchColor";

	public static final String P_FORCE_COLOR = "forceColor";

	public static final String P_CHOICE = "choicePreference";

	public static final String P_STRING = "stringPreference";

	public static final String P_POLLING_INTERVAL = "pollingInterval";

	public static final String P_RESPONSE_TIMEOUT = "responseTimeout";

}
