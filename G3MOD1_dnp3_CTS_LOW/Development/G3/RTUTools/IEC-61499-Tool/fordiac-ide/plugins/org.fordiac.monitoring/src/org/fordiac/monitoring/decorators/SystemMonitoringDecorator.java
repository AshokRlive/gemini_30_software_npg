package org.fordiac.monitoring.decorators;

import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILabelDecorator;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.util.imageprovider.FordiacImage;
import org.fordiac.monitoring.MonitoringManager;

public class SystemMonitoringDecorator implements ILabelDecorator {

	@Override
	public void addListener(ILabelProviderListener listener) {
	}

	@Override
	public void dispose() {
		if(null != overlayImage){
			overlayImage.dispose();
		}
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {

	}

	@Override
	public Image decorateImage(Image image, Object element) {
		Image retval;

		if ((element instanceof AutomationSystem)
				&& (MonitoringManager.getInstance().monitoringForSystemEnabled((AutomationSystem) element))) {
			retval = getOverlayImage(image);
		} else {
			retval = image;
		}

		return retval;
	}
	
	private Image overlayImage = null;

	private Image getOverlayImage(Image image) {
		if(null == overlayImage){
			DecorationOverlayIcon DOC = new DecorationOverlayIcon(image, 
					FordiacImage.ICON_MonitoringDecorator.getImageDescriptor(), IDecoration.TOP_LEFT);
			overlayImage = DOC.createImage();
		}
		
		return overlayImage;
	}

	@Override
	public String decorateText(String text, Object element) {
		return null;
	}
}
