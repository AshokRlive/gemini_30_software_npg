/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.model.gen.monitoring.util;


import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.fordiac.ide.gef.IEditPartCreator;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.View;
import org.fordiac.model.gen.monitoring.*;
import org.fordiac.model.gen.monitoring.AdapterMonitoringInterfaceElementView;
import org.fordiac.model.gen.monitoring.AdapterPortElement;
import org.fordiac.model.gen.monitoring.Breakpoints;
import org.fordiac.model.gen.monitoring.MonitoringAdapterElement;
import org.fordiac.model.gen.monitoring.MonitoringBaseElement;
import org.fordiac.model.gen.monitoring.MonitoringElement;
import org.fordiac.model.gen.monitoring.MonitoringPackage;
import org.fordiac.model.gen.monitoring.PortElement;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.fordiac.model.gen.monitoring.MonitoringPackage
 * @generated
 */
public class MonitoringAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MonitoringPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitoringAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = MonitoringPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonitoringSwitch<Adapter> modelSwitch =
		new MonitoringSwitch<Adapter>() {
			@Override
			public Adapter caseMonitoringBaseElement(MonitoringBaseElement object) {
				return createMonitoringBaseElementAdapter();
			}
			@Override
			public Adapter caseMonitoringElement(MonitoringElement object) {
				return createMonitoringElementAdapter();
			}
			@Override
			public Adapter caseMonitoringAdapterElement(MonitoringAdapterElement object) {
				return createMonitoringAdapterElementAdapter();
			}
			@Override
			public Adapter caseBreakpoints(Breakpoints object) {
				return createBreakpointsAdapter();
			}
			@Override
			public Adapter caseIEditPartCreator(IEditPartCreator object) {
				return createIEditPartCreatorAdapter();
			}
			@Override
			public Adapter casePortElement(PortElement object) {
				return createPortElementAdapter();
			}
			@Override
			public Adapter caseAdapterPortElement(AdapterPortElement object) {
				return createAdapterPortElementAdapter();
			}
			@Override
			public Adapter caseAdapterMonitoringInterfaceElementView(AdapterMonitoringInterfaceElementView object) {
				return createAdapterMonitoringInterfaceElementViewAdapter();
			}
			@Override
			public Adapter caseView(View object) {
				return createViewAdapter();
			}
			@Override
			public Adapter caseInterfaceElementView(InterfaceElementView object) {
				return createInterfaceElementViewAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.model.gen.monitoring.MonitoringBaseElement <em>Base Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.model.gen.monitoring.MonitoringBaseElement
	 * @generated
	 */
	public Adapter createMonitoringBaseElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.model.gen.monitoring.MonitoringElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.model.gen.monitoring.MonitoringElement
	 * @generated
	 */
	public Adapter createMonitoringElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.model.gen.monitoring.MonitoringAdapterElement <em>Adapter Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.model.gen.monitoring.MonitoringAdapterElement
	 * @generated
	 */
	public Adapter createMonitoringAdapterElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.model.gen.monitoring.Breakpoints <em>Breakpoints</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.model.gen.monitoring.Breakpoints
	 * @generated
	 */
	public Adapter createBreakpointsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.gef.IEditPartCreator <em>IEdit Part Creator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.gef.IEditPartCreator
	 * @generated
	 */
	public Adapter createIEditPartCreatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.model.gen.monitoring.PortElement <em>Port Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.model.gen.monitoring.PortElement
	 * @generated
	 */
	public Adapter createPortElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.model.gen.monitoring.AdapterPortElement <em>Adapter Port Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.model.gen.monitoring.AdapterPortElement
	 * @generated
	 */
	public Adapter createAdapterPortElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.model.gen.monitoring.AdapterMonitoringInterfaceElementView <em>Adapter Monitoring Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.model.gen.monitoring.AdapterMonitoringInterfaceElementView
	 * @generated
	 */
	public Adapter createAdapterMonitoringInterfaceElementViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.View <em>View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.View
	 * @generated
	 */
	public Adapter createViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.InterfaceElementView <em>Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.InterfaceElementView
	 * @generated
	 */
	public Adapter createInterfaceElementViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //MonitoringAdapterFactory
