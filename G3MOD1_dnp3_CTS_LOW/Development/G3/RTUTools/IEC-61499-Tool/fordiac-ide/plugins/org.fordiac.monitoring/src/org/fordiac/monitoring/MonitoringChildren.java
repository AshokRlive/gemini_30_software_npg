/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring;

import java.util.ArrayList;

import org.fordiac.ide.gef.IEditPartCreator;
import org.fordiac.ide.gef.editparts.IChildrenProvider;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.model.gen.monitoring.MonitoringBaseElement;
import org.fordiac.model.gen.monitoring.PortElement;


public class MonitoringChildren implements IMonitoringListener,
		IChildrenProvider {

	public MonitoringChildren() {
		MonitoringManager.getInstance().registerMonitoringListener(this);
	}

	@Override
	public ArrayList<IEditPartCreator> getChildren(Diagram diagram) {
		ArrayList<IEditPartCreator> arrayList = new ArrayList<IEditPartCreator>();
		
		//TODO - model refactoring fetch only the list of monitored elements of the same system
		for (MonitoringBaseElement element : MonitoringManager.getInstance().getElementsToMonitor()) {			
			if(null != element){
				if(null != diagram.getFunctionBlockNetwork() && 
						diagram.getFunctionBlockNetwork().getFBs().contains(element.getPort().getFb())){
					arrayList.add(element);
				}
				else if(null != element.getPort().getFb().getResource() && (!element.getPort().getFb().isResourceFB())){
					//check if we are in the resource diagram editor for a mapped FB	
					 if(((Resource)element.getPort().getFb().getResource().eContainer()).getFBNetwork().equals(diagram.getFunctionBlockNetwork())){
						arrayList.add(element);
					 }
				}
			}
			
		}
		return arrayList;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}


	@Override
	public void notifyAddPort(PortElement port) {
		// nothing to do;
	}

	@Override
	public void notifyRemovePort(PortElement port) {
	}

	@Override
	public void notifyTriggerEvent(PortElement port) {
	}

}
