/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.AncestorListener;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.widgets.Display;
import org.fordiac.ide.application.SpecificLayerEditPart;
import org.fordiac.ide.gef.draw2d.SetableAlphaLabel;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.gef.editparts.ZoomScalableFreeformRootEditPart;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.AdapterType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.impl.EventImpl;
import org.fordiac.ide.model.libraryElement.impl.VarDeclarationImpl;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.preferences.PreferenceGetter;
import org.fordiac.ide.util.imageprovider.FordiacImage;
import org.fordiac.model.gen.monitoring.MonitoringBaseElement;
import org.fordiac.model.gen.monitoring.MonitoringElement;
import org.fordiac.monitoring.Activator;
import org.fordiac.monitoring.MonitoringManager;


public class MonitoringEditPart extends AbstractViewEditPart implements
		SpecificLayerEditPart {

	InterfaceEditPart parentPart;

	/**
	 * FIXME implement deactivate
	 */
	@Override
	public void activate() {
		super.activate();
		for (Object object : getViewer().getEditPartRegistry().keySet()) {
			if (object instanceof InterfaceElementView) {
				IInterfaceElement interfaceElement = ((InterfaceElementView) object).getIInterfaceElement();
				if (interfaceElement.equals(getCastedModel().getPort().getInterfaceElement())) {
					EditPart part = (EditPart) getViewer().getEditPartRegistry().get(object);
					if (part instanceof InterfaceEditPart) {
						parentPart = (InterfaceEditPart) part;
						IFigure f = parentPart.getFigure();
						f.addAncestorListener(new AncestorListener() {

							@Override
							public void ancestorRemoved(IFigure ancestor) {
							}

							@Override
							public void ancestorMoved(IFigure ancestor) {
								// calculatePos();
								refreshVisuals();

							}

							@Override
							public void ancestorAdded(IFigure ancestor) {
							}
						});
					}	
				}
				else if (interfaceElement instanceof AdapterDeclaration) {
					IInterfaceElement subInterfaceElement = null;
					InterfaceList interfaceList = ((AdapterType)((AdapterDeclaration)interfaceElement).getType()).getInterfaceList();
					ArrayList<IInterfaceElement> list = new ArrayList<IInterfaceElement>();
					list.addAll(interfaceList.getEventInputs());
					list.addAll(interfaceList.getEventOutputs());
					list.addAll(interfaceList.getInputVars());
					list.addAll(interfaceList.getOutputVars());
					for (IInterfaceElement element : list) {
						if (element.equals(getCastedModel().getPort().getInterfaceElement()) && interfaceElement.eContainer().eContainer() == getCastedModel().getPort().getFb()) {
							subInterfaceElement = element;
							break;
						}
					}
					
					if (subInterfaceElement != null) {
						Object subObject = null;
						for (Object obj : getViewer().getEditPartRegistry().values()) {
							if (obj instanceof MonitoringAdapterEditPart) {
								MonitoringAdapterEditPart part = (MonitoringAdapterEditPart)obj;
								if (part.getCastedModel().getPort().getInterfaceElement() == interfaceElement) {
									for (Object subView : part.getModelChildren()) {
										if (((InterfaceElementView)subView).getIInterfaceElement().getName().equals(subInterfaceElement.getName())) {
											subObject = subView;
											break;
										}
									}
									if (subObject != null) {
										break;
									}
								}
							}
						}

						if (subObject != null) {
							EditPart part = (EditPart) getViewer().getEditPartRegistry().get(subObject);
							if (part instanceof InterfaceEditPart) {
								parentPart = (InterfaceEditPart) part;
								IFigure f = parentPart.getFigure();
								f.addAncestorListener(new AncestorListener() {
		
									@Override
									public void ancestorRemoved(IFigure ancestor) {
									}
		
									@Override
									public void ancestorMoved(IFigure ancestor) {
										// calculatePos();
										refreshVisuals();
		
									}
		
									@Override
									public void ancestorAdded(IFigure ancestor) {
									}
								});
							}
						}
					}
				}
			}
		}
		org.fordiac.monitoring.Activator.getDefault().getPreferenceStore().addPropertyChangeListener(getPreferenceChangeListener());
		refreshVisuals();
	}

	@Override
	public void setSelected(int value) {
		// avoid that element can be selected
		// super.setSelected(value);
	}

	public boolean isInput() {
		return getCastedModel().getPort().getInterfaceElement().isIsInput();
	}

	public boolean isEvent() {
		return getCastedModel().getPort().getInterfaceElement() instanceof EventImpl;
	}

	public boolean isVariable() {
		return getCastedModel().getPort().getInterfaceElement() instanceof VarDeclarationImpl;
	}

	private void updateForceStatus() {
		setBackgroundColor(getFigure());
	}

	int oldx;
	int oldy;

	private Point calculatePos() {
		if (parentPart != null) {
			Rectangle bounds = parentPart.getFigure().getBounds();
			int x = 0;
			if (isInput()) {
				int width = 40;
				width = getFigure().getBounds().width;
				width = Math.max(40, width);
				x = bounds.x - 2 - width;
			}
			else {
				x = bounds.x + bounds.width + 2;
			}
			int y = bounds.y;
			return new Point(x, y);
		}
		else {
			return new Point(0, 0);
		}
	}

	@Override
	protected void refreshPosition() {
		if (getParent() != null) {
			Rectangle bounds = null;
			Point p = calculatePos();
			int width = getFigure().getPreferredSize().width;
			width = Math.max(40, width);
			bounds = new Rectangle(p.x, p.y, width, -1);
			((GraphicalEditPart) getParent()).setLayoutConstraint(this,
					getFigure(), bounds);

		}
	}

	MonitoringBaseElement getCastedModel() {
		return (MonitoringBaseElement) getModel();
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
				new DirectEditPolicy(){

					@Override
					protected Command getDirectEditCommand(DirectEditRequest request) {
						String value = (String) request.getCellEditor().getValue();
						MonitoringEditPart editPart = (MonitoringEditPart)getHost();
						MonitoringManager.getInstance().writeValue((MonitoringElement)editPart.getCastedModel(), value);
						return null;
					}

					@Override
					protected void showCurrentEditValue(DirectEditRequest request) {
						String value = (String) request.getCellEditor().getValue();
						MonitoringEditPart editPart = (MonitoringEditPart)getHost();
						if (null != editPart) {
							editPart.getNameLabel().setText(value);
						}
						
					}
									
		});
	}

	protected void setBackgroundColor(IFigure l) {
		if (getCastedModel() instanceof MonitoringElement && ((MonitoringElement)getCastedModel()).isForce()) {
			l.setBackgroundColor(PreferenceGetter
					.getColor(
							Activator.getDefault().getPreferenceStore(),
							org.fordiac.monitoring.preferences.PreferenceConstants.P_FORCE_COLOR));
		} else {
			
			l.setBackgroundColor(PreferenceGetter
					.getColor(
							Activator.getDefault().getPreferenceStore(),
							org.fordiac.monitoring.preferences.PreferenceConstants.P_WATCH_COLOR));
		}
	}

	@Override
	protected IFigure createFigureForModel() {
		SetableAlphaLabel l = new SetableAlphaLabel();
		setBackgroundColor(l);
		l.setOpaque(true);
		if (isInput()) {
			l.setLabelAlignment(PositionConstants.RIGHT);
			l.setTextAlignment(PositionConstants.RIGHT);
		} else {
			l.setTextAlignment(PositionConstants.LEFT);
			l.setLabelAlignment(PositionConstants.LEFT);
		}
		l.setBorder(new MarginBorder(0, 5, 0, 5));
		l.setText("N/A");
		l.setMinimumSize(new Dimension(50, 1));
		l.setAlpha(190);
		return l;
	}

	private EContentAdapter adapter;

	@Override
	public EContentAdapter getContentAdapter() {
		if (adapter == null) {
			adapter = new EContentAdapter() {

				@Override
				public void notifyChanged(final Notification notification) {
					super.notifyChanged(notification);
					Display.getDefault().asyncExec(new Runnable() {

						@Override
						public void run() {
							refreshVisuals();

						}
					});
				}

			};
		}
		return adapter;
	}

	@Override
	public boolean understandsRequest(Request request) {
		if (request.getType() == RequestConstants.REQ_MOVE) {
			return false;
		}
		if (request.getType() == RequestConstants.REQ_DIRECT_EDIT
				|| request.getType() == RequestConstants.REQ_OPEN) {
			return isVariable(); //Currently only allow direct when we are a variable
		} 
		return super.understandsRequest(request);
	}

	@Override
	public INamedElement getINamedElement() {
		return getCastedModel().getPort().getInterfaceElement();
	}

	@Override
	public List<? extends InterfaceElementView> getInterfaceElements() {
		return null;
	}

	@Override
	public Label getNameLabel() {
		return (Label)getFigure();
	}

	private IPropertyChangeListener listener;

	@Override
	public IPropertyChangeListener getPreferenceChangeListener() {
		if (listener == null) {
			listener = new IPropertyChangeListener() {
				public void propertyChange(final PropertyChangeEvent event) {
					if (event
							.getProperty()
							.equals(org.fordiac.monitoring.preferences.PreferenceConstants.P_WATCH_COLOR)
							|| event.getProperty()
									.equals(org.fordiac.monitoring.preferences.PreferenceConstants.P_FORCE_COLOR)) {
						updateForceStatus();
					}
				}
			};
		}
		return listener;

	}

	@Override
	public Object getEditableValue() {
		return null;
	}

	@Override
	public Object getPropertyValue(Object id) {
		return null;
	}

	@Override
	public boolean isPropertySet(Object id) {
		return false;
	}

	@Override
	public void resetPropertyValue(Object id) {
	}

	@Override
	public void setPropertyValue(Object id, Object value) {

	}

	public String getSpecificLayer() {
		return ZoomScalableFreeformRootEditPart.TOPLAYER;
	}

	public void setValue(String string) {
		if (isActive()) {
			if (getFigure() != null) {
				if (((MonitoringElement)getCastedModel()).isForce()
						&& ((MonitoringElement)getCastedModel()).getForceValue() != null) {
					((Label) getFigure()).setText(((MonitoringElement)getCastedModel())
							.getForceValue() + " (" + string + ")");
				} else {
					((Label) getFigure()).setText(string);
				}
				refreshVisuals();
			}
		}
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		updateLayout();
		updateBreakpoint();
	}

	public void updateBreakpoint() {
		if (((MonitoringElement)getCastedModel()).isBreakpoint()) {
			if ((((Label) getFigure()).getIcon() == null)) {
				((Label) getFigure()).setIcon(FordiacImage.ICON_BreakPoint.getImage());
			}
		} else {
			((Label) getFigure()).setIcon(null);
		}

		if (((MonitoringElement)getCastedModel()).isBreakpointActive()) {
			//getFigure().setForegroundColor(org.eclipse.draw2d.ColorConstants.red);
			getFigure().setBackgroundColor(org.eclipse.draw2d.ColorConstants.red);
		} else {
			setBackgroundColor(getFigure());
		}
	}
	
	@Override
	protected void backgroundColorChanged(IFigure figure) {
		setBackgroundColor(figure);
	}

	private void updateLayout() {
		updateForceStatus();
		getFigure().setEnabled(!getCastedModel().isOffline());

	}
}
