/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.monitoring.popup.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.model.gen.monitoring.PortElement;
import org.fordiac.monitoring.MonitoringManagerUtils;


//this class is still here to support BreakpointsViewConitueBreakpoint. Shall be removed when the breakpoint view is reworked. 
public abstract class ContinueBreakpoint extends AbstractMonitoringAction {

	public ContinueBreakpoint() {
		// empty constructor
	}

	protected Shell shell;
	protected StructuredSelection selection;

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		super.setActivePart(action, targetPart);
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	@Override
	public void run(IAction action) {			
		if (null != selection) {
			if (selection.getFirstElement() instanceof InterfaceEditPart) {
				InterfaceEditPart editPart = (InterfaceEditPart) selection.getFirstElement();
				
				PortElement port = MonitoringManagerUtils.createPortElement(editPart);
				if(null != port){
					//MonitoringManager.getInstance().toggleBreakpoint(port, BreakPoint.clear);
				}
			}
		}
	}

}
