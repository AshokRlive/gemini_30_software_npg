/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.preferences;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.fbt.typeeditor.ecc.preferences.messages"; //$NON-NLS-1$

	/** The Fordiac ECC Editor preference page_ label_ preference page description. */
	public static String FordiacECCPreferencePage_LABEL_PreferencePageDescription;	
	
	/** The Fordiac ECC Editor preference page_ label_ default ECC State color. */             
	public static String FordiacECCPreferencePage_LABEL_ECCStateColor;          
	                                                                                
	/** The Fordiac ECC Editor preference page_ label_ default ECC State border color. */      
	public static String FordiacECCPreferencePage_LABEL_ECCStateBorderColor;    
	                                                                                
	/** The Fordiac ECC Editor preference page_ label_ default ECC Algorithm color. */         
	public static String FordiacECCPreferencePage_LABEL_ECCAlgorithmColor;      
	                                                                                
	/** The Fordiac ECC Editor preference page_ label_ default ECC Algorithm border color. */  
	public static String FordiacECCPreferencePage_LABEL_ECCAlgorithmBorderColor;
	                                                                                
	/** The Fordiac ECC Editor preference page_ label_ default ECC Event color. */             
	public static String FordiacECCPreferencePage_LABEL_ECCEventColor;          
	                                                                                
	/** The Fordiac ECC Editor preference page_ label_ default ECC Event border color. */      
	public static String FordiacECCPreferencePage_LABEL_ECCEventBorderColor;    
	                                                                                
	/** The Fordiac ECC Editor preference page_ label_ default ECC Transition color. */        
	public static String FordiacECCPreferencePage_LABEL_ECCTransitionColor;     
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
		// private empty constructor
	}
}
