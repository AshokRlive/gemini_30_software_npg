package org.fordiac.ide.fbt.typeeditor.ecc.properties;

import org.eclipse.jface.viewers.IFilter;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECActionAlgorithm;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECActionAlgorithmEditPart;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECActionOutputEvent;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECActionOutputEventEditPart;
import org.fordiac.ide.model.libraryElement.ECAction;

public class ECActionFilter implements IFilter {

	@Override
	public boolean select(Object toTest) {
		if(toTest instanceof ECActionAlgorithmEditPart && ((ECActionAlgorithmEditPart) toTest).getCastedModel() instanceof ECActionAlgorithm){
			return true;
		}
		if(toTest instanceof ECActionOutputEventEditPart && ((ECActionOutputEventEditPart) toTest).getCastedModel() instanceof ECActionOutputEvent){
			return true;
		}
		if(toTest instanceof ECActionAlgorithm || toTest instanceof ECActionOutputEvent){
			return true;
		}
		if(toTest instanceof ECAction){
			return true;
		}
		return false;
	}

}
