/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.fbt.typeeditor.ecc.Messages;
import org.fordiac.ide.model.libraryElement.ECState;
import org.fordiac.ide.util.Utils;

/**
 * A command for moving an ECState object.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ECStateSetPositionCommand extends Command {

	/** The Constant MOVE_LABEL. */
	private static final String MOVE_LABEL = Messages.ECStateSetPositionCommand_LABEL_Move;

	/** The new bounds. */
	private final Rectangle newBounds;

	/** The old bounds. */
	private Rectangle oldBounds;

	/** The request. */
	private final ChangeBoundsRequest request;

	/** The view. */
	private final ECState ecState;

	/** The editor. */
	private IEditorPart editor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());

	}

	/**
	 * Instantiates a new view set position command.
	 * 
	 * @param ecState the ecState
	 * @param req the req
	 * @param newBounds the new bounds
	 */
	public ECStateSetPositionCommand(final ECState ecState,
			final ChangeBoundsRequest req, final Rectangle newBounds) {
		this.ecState = ecState;
		this.request = req;
		this.newBounds = newBounds.getCopy();
		setLabel(MOVE_LABEL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		Object type = request.getType();
		// make sure the Request is of a type we support: (Move or
		// Move_Children)
		// e.g. a FB moves within an application
		return RequestConstants.REQ_MOVE.equals(type)
				|| RequestConstants.REQ_MOVE_CHILDREN.equals(type)
				|| RequestConstants.REQ_ALIGN_CHILDREN.equals(type);
	}

	/**
	 * Sets the new Position of the affected UIFB.
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		setLabel(getLabel() + "(" + editor.getTitle() + ")"); //$NON-NLS-1$ //$NON-NLS-2$
		oldBounds = new Rectangle(ecState.getPosition().getX(), ecState
				.getPosition().getY(), -1, -1);
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		ecState.getPosition().setX(newBounds.x);
		ecState.getPosition().setY(newBounds.y);
	}

	/**
	 * Restores the old position.
	 */
	@Override
	public void undo() {
		ecState.getPosition().setX(oldBounds.x);
		ecState.getPosition().setY(oldBounds.y);
	}

}
