/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.libraryElement.Algorithm;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.ECAction;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.STAlgorithm;

public class CreateAlgorithmCommand extends Command {
	private final BasicFBType fbType;
	private STAlgorithm newAlgorithm;
	private Algorithm oldAlgorithm;
	private ECAction action;
	
	public CreateAlgorithmCommand(final BasicFBType fbType){
		this.fbType = fbType;
	}
	
	public CreateAlgorithmCommand(final BasicFBType fbType, ECAction action){
		this(fbType);
		this.action = action;
	}
	
	@Override
	public void execute() {
		if(null != action){
			oldAlgorithm = action.getAlgorithm();
		}
		newAlgorithm = LibraryElementFactory.eINSTANCE.createSTAlgorithm();
		newAlgorithm.setName(NameRepository.getUniqueAlgorithmName(newAlgorithm, fbType, "ALG"));
		newAlgorithm.setComment("new algorithm");
		newAlgorithm.setText("");  //especially the xtext editor requires at least an empty algorithm text
		redo();
	}

	@Override
	public void undo() {
		if(null != action){
			action.setAlgorithm(oldAlgorithm);
		}
		fbType.getAlgorithm().remove(newAlgorithm);
	}

	@Override
	public void redo() {
		if(null != action){
			action.setAlgorithm(newAlgorithm);
		}
		fbType.getAlgorithm().add(newAlgorithm);
	}
}
