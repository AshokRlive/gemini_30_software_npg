/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc;

import org.eclipse.swt.widgets.Composite;
import org.fordiac.ide.model.libraryElement.BasicFBType;

/**
 * The Interface IAlgorithmEditorCreator.
 */
public interface IAlgorithmEditorCreator {

	/**
	 * Creates the algorithm editor.
	 * 
	 * @param parent
	 *            the parent
	 * 
	 * @return the i algorithm editor
	 */
	public IAlgorithmEditor createAlgorithmEditor(Composite parent, BasicFBType fbType);

}
