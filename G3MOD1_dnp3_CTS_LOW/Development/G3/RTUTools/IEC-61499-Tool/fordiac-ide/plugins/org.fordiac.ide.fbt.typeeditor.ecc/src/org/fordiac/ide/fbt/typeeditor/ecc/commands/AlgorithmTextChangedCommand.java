/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.TextAlgorithm;

public class AlgorithmTextChangedCommand extends Command {

	private TextAlgorithm algorithm;
	
	private String newAlgorithmText;
	
	private String oldAlgorithmText;
	
	public AlgorithmTextChangedCommand(TextAlgorithm algorithm, String algorithmText) {
		this.algorithm = algorithm;
		this.newAlgorithmText = algorithmText;
	}
	
	@Override
	public void execute() {		
		oldAlgorithmText = algorithm.getText();		
		redo();
	}
	
	@Override
	public void undo() {
		algorithm.setText(oldAlgorithmText);
	}

	@Override
	public void redo() {
		algorithm.setText(newAlgorithmText);
	}

}
