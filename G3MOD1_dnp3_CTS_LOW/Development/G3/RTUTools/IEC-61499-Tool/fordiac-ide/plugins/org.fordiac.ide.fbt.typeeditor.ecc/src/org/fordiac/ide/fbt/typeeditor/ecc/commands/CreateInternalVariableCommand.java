/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.data.DataFactory;
import org.fordiac.ide.model.data.DataType;
import org.fordiac.ide.model.data.VarInitialization;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.typelibrary.DataTypeLibrary;

/**
 * The Class CreateInternalVariableCommand.
 */
public class CreateInternalVariableCommand extends Command {

	/** The data type. */
	private final DataType dataType;

	/** The fb type. */
	private final BasicFBType fbType;

	private VarDeclaration varDecl;
	
	/**
	 * Instantiates a new creates the input variable command.
	 * 
	 * @param dataType
	 *            the data type
	 * @param fbType
	 *            the fb type
	 */
	public CreateInternalVariableCommand(final BasicFBType fbType) {
		this.dataType = DataTypeLibrary.getInstance().getType("BOOL");
		this.fbType = fbType;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		varDecl = LibraryElementFactory.eINSTANCE.createVarDeclaration();
		varDecl.setType(dataType);
		String name = NameRepository.getUniqueInterfaceElementName(varDecl, fbType, "INTERNALVAR");
		varDecl.setName(name);
		varDecl.setTypeName(dataType.getName());
		varDecl.setComment("Internal Variable");
		varDecl.setArraySize(0);
		VarInitialization varInitialization = DataFactory.eINSTANCE.createVarInitialization();
		varInitialization.setInitialValue("");
		varDecl.setVarInitialization(varInitialization);
		Value value = LibraryElementFactory.eINSTANCE.createValue();
		value.setValue("");
		varDecl.setValue(value);
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		fbType.getInternalVars().remove(varDecl);
	}

	public VarDeclaration getVarDecl() {
		return varDecl;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		fbType.getInternalVars().add(varDecl);
	}
}
