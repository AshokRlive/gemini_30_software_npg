/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.ECAction;
import org.fordiac.ide.model.libraryElement.ECState;

/**
 * The Class CreateECActionCommand.
 */
public class CreateECActionCommand extends Command {

	/** The action. */
	private final ECAction action;

	/** The parent. */
	private final ECState parent;

	/**
	 * Instantiates a new creates the ec action command.
	 * 
	 * @param action the action
	 * @param parent the parent
	 */
	public CreateECActionCommand(final ECAction action, final ECState parent) {
		super();
		this.action = action;
		this.parent = parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		parent.getECAction().add(action);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		parent.getECAction().remove(action);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		parent.getECAction().add(action);
	}

}
