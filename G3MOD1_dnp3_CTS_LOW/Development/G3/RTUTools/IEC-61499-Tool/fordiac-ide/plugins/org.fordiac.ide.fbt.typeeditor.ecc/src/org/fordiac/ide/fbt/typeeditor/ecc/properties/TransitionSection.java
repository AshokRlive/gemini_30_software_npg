package org.fordiac.ide.fbt.typeeditor.ecc.properties;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.xtext.Constants;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditor;
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditorFactory;
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditorModelAccess;
import org.eclipse.xtext.ui.editor.embedded.IEditedResourceProvider;
import org.fordiac.ide.fbt.typeeditor.ecc.commands.ChangeConditionEventCommand;
import org.fordiac.ide.fbt.typeeditor.ecc.commands.ChangeConditionExpressionCommand;
import org.fordiac.ide.fbt.typeeditor.ecc.commands.ChangeECTransitionCommentCommand;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECTransitionEditPart;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.AdapterType;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.ECTransition;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FBType;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;

@SuppressWarnings("restriction")
public class TransitionSection extends AbstractECSection {
	
	private Text commentText;
	private Combo eventCombo;
	
	Composite conditionEditingContainer;
	
	//the closing braket lable need for puting the xtext editor before it.
	CLabel closingBraket;
	
	private EmbeddedEditor editor;
	
	Composite composite;
	
	@Inject
	protected EmbeddedEditorFactory editorFactory;
	
	@Inject
	private Provider<XtextResourceSet> resourceSetProvider;

	@Inject
	@Named(Constants.FILE_EXTENSIONS)
	public String fileExtension;

	
	EmbeddedEditorModelAccess embeddedEditorModelAccess;
	
	private final IDocumentListener listener = new IDocumentListener() {
		@Override
		public void documentChanged(final DocumentEvent event) {
			executeCommand(new ChangeConditionExpressionCommand(getType(), embeddedEditorModelAccess.getEditablePart()));	
		}

		@Override
		public void documentAboutToBeChanged(final DocumentEvent event) {
		}
	};
	
	@Override
	protected ECTransition getType() {
		return (ECTransition) type;
	}
	
	protected BasicFBType getBasicFBType() {
		return (null != getType().eContainer()) ? (BasicFBType) getType().eContainer().eContainer() : null;
	}
	
	@Override
	protected Object getInputType(Object input) {
		if(input instanceof ECTransitionEditPart){
			return ((ECTransitionEditPart) input).getCastedModel();	
		}
		if(input instanceof ECTransition){
			return (ECTransition) input;	
		}
		return null;
	}

	@Override
	public void createControls(final Composite parent, final TabbedPropertySheetPage tabbedPropertySheetPage) {
		createSuperControls = false;
		super.createControls(parent, tabbedPropertySheetPage);		
		composite = getWidgetFactory().createComposite(parent);
		composite.setLayout(new GridLayout(2, false));
		composite.setLayoutData(new GridData(SWT.FILL, 0, true, false));
		getWidgetFactory().createCLabel(composite, "Condition:"); 
		
		createConditionEditingPlaceHolder(composite);
		
		getWidgetFactory().createCLabel(composite, "Comment:"); 
		commentText = createGroupText(composite, true);	
		commentText.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				removeContentAdapter();
				executeCommand(new ChangeECTransitionCommentCommand(getType(), commentText.getText()));
				addContentAdapter();
			}
		});
	}
	
	/** creates a composite where the transition condition editing widgets can be placed later on.
	 * 
	 * The reason for having this is that the condition expression editor needs the FB Type for 
	 * context resolution (e.g., allow resolving of data inputs outputs and internal variables).
	 * The type is only available later therefore these widgets will be created when the input is set.
	 * 
	 * @param parent
	 */
	private void createConditionEditingPlaceHolder(Composite parent){
		conditionEditingContainer = getWidgetFactory().createComposite(parent);
		conditionEditingContainer.setLayout(new GridLayout(4,false));
		GridData compositeLayoutData = new GridData(SWT.FILL, 0, true, false);
		compositeLayoutData.verticalIndent = 0;
		conditionEditingContainer.setLayoutData(compositeLayoutData);
		
		eventCombo = new Combo(conditionEditingContainer, SWT.SINGLE | SWT.READ_ONLY | SWT.BORDER);
		eventCombo.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				removeContentAdapter();
				executeCommand(new ChangeConditionEventCommand(getType(), eventCombo.getText()));
				checkEnablement();
				addContentAdapter();
			}
			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
			}
		});
		
		getWidgetFactory().createCLabel(conditionEditingContainer, "[");
		
		closingBraket = getWidgetFactory().createCLabel(conditionEditingContainer, "]");
	}
	
	@Override
	public void setInput(final IWorkbenchPart part, final ISelection selection) {
		Assert.isTrue(selection instanceof IStructuredSelection);
		Object input = ((IStructuredSelection) selection).getFirstElement();
		commandStack = getCommandStack(part, input);
		if(null == commandStack){ //disable all fields
			commentText.setEnabled(false);
			eventCombo.removeAll();
			eventCombo.setEnabled(false);
		}
		setType(input);
		if(null == editor){
			createTransitionEditor(conditionEditingContainer);
		}
	}	
		
	private void createTransitionEditor(Composite parent) {
		FBType fbType = getBasicFBType();	

		IEditedResourceProvider resourceProvider = new IEditedResourceProvider() {
			
			@Override
			public XtextResource createResource() {
				XtextResourceSet resourceSet = resourceSetProvider.get();
				EcoreUtil.Copier copier = new EcoreUtil.Copier();
				Resource fbResource = resourceSet.createResource(computeUnusedUri(resourceSet, "fbt"));
				fbResource.getContents().add(copier.copy(EcoreUtil.getRootContainer(fbType)));
				for(AdapterDeclaration adapter : fbType.getInterfaceList().getSockets()) {
					createAdapterResource(resourceSet, copier, adapter);
				}
				for(AdapterDeclaration adapter : fbType.getInterfaceList().getPlugs()) {
					createAdapterResource(resourceSet, copier, adapter);
				}
				copier.copyReferences();
				Resource resource = resourceSet.createResource(computeUnusedUri(resourceSet, fileExtension));
				return (XtextResource) resource;
			}

			private void createAdapterResource(XtextResourceSet resourceSet, EcoreUtil.Copier copier,
					AdapterDeclaration adapter) {
				if(adapter.getType() instanceof AdapterType){
					Resource adapterResource = resourceSet.createResource(computeUnusedUri(resourceSet, "fbt"));
					copier.copy(adapter.getType());
					adapterResource.getContents().add(copier.copy(EcoreUtil.getRootContainer(((AdapterType)adapter.getType()).getAdapterFBType())));
				}
			}

			protected URI computeUnusedUri(ResourceSet resourceSet, String fileExtension) {
				String name = "__synthetic";
				for (int i = 0; i < Integer.MAX_VALUE; i++) {
					URI syntheticUri = URI.createURI(name + i + "." + fileExtension);
					if (resourceSet.getResource(syntheticUri, false) == null)
						return syntheticUri;
				}
				throw new IllegalStateException();
			}
		};

		editor = editorFactory.newEditor(resourceProvider).withParent(parent);
		StyledText conditionText = (StyledText)editor.getViewer().getControl(); 
		conditionText.setLayoutData(new GridData(SWT.FILL, 0, true, false));
		conditionText.setBottomMargin(5);
		conditionText.setTopMargin(5);
		conditionText.setLeftMargin(5);
		conditionText.setRightMargin(5);
		conditionText.moveAbove(closingBraket);

		embeddedEditorModelAccess = editor.createPartialEditor();
		editor.getDocument().addDocumentListener(listener);
		composite.layout();
	}

	@Override
	public void refresh() {
		CommandStack commandStackBuffer = commandStack;
		commandStack = null;		
		if(null != type && null != getBasicFBType()) {
			setEventConditionDropdown();	
			commentText.setText(getType().getComment() != null ? getType().getComment() : "");
			updateConditionExpressionText(getType().getConditionExpression());
			if(getType().getConditionExpression() != null && getType().getConditionExpression().equals("1")){
				eventCombo.select(1);
			}else{
				eventCombo.select(getType().getConditionEvent() != null ? 
					eventCombo.indexOf(getType().getConditionEvent().getName()) : 0);
			}
			checkEnablement();
		} 
		commandStack = commandStackBuffer;
	}

	private void updateConditionExpressionText(String conditionExpression) {
		if(null != embeddedEditorModelAccess){
			embeddedEditorModelAccess.updateModel((null != conditionExpression) ? conditionExpression : "");
		}
	}

	private void checkEnablement() {
		CommandStack commandStackBuffer = commandStack;
		commandStack = null;
		if(getType().getConditionExpression() != null && getType().getConditionExpression().equals("1")){
			updateConditionExpressionText("");
			editor.getViewer().getControl().setEnabled(false);
		}else{
			editor.getViewer().getControl().setEnabled(true);
		}
		commandStack = commandStackBuffer;
	}
	
	public void setEventConditionDropdown(){
		eventCombo.removeAll();
		eventCombo.add("");
		eventCombo.add("1");
		for(Event event : getBasicFBType().getInterfaceList().getEventInputs()){
			eventCombo.add(event.getName());
		}
		for(AdapterDeclaration adapter : getBasicFBType().getInterfaceList().getPlugs()){
			if(adapter.getType() instanceof AdapterType){
				for(Event event : ((AdapterType) adapter.getType()).getInterfaceList().getEventInputs()){
					eventCombo.add(adapter.getName() + "." + event.getName());
				}
			}
		}
		for(AdapterDeclaration adapter : getBasicFBType().getInterfaceList().getSockets()){
			if(adapter.getType() instanceof AdapterType){
				for(Event event : ((AdapterType) adapter.getType()).getInterfaceList().getEventOutputs()){
					eventCombo.add(adapter.getName() + "." + event.getName());
				}
			}
		}
	}
}
