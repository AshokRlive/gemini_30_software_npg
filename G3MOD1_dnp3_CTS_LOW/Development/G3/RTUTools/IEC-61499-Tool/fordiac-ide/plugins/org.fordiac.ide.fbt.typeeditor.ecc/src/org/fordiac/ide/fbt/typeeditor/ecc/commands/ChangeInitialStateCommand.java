/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.ECC;
import org.fordiac.ide.model.libraryElement.ECState;

/**
 * The Class ChangeInitialStateCommand.
 */
public class ChangeInitialStateCommand extends Command {

	/** The ecc. */
	private final ECC ecc;

	/** The state. */
	private final ECState state;

	/** The old state. */
	private ECState oldState;

	/**
	 * Instantiates a new change initial state command.
	 * 
	 * @param ecc the ecc
	 * @param state the state
	 */
	public ChangeInitialStateCommand(final ECState state) {
		super();
		this.ecc = (ECC)state.eContainer();
		this.state = state;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldState = ecc.getStart();
		ecc.setStart(state);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		ecc.setStart(oldState);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		ecc.setStart(state);
	}

}
