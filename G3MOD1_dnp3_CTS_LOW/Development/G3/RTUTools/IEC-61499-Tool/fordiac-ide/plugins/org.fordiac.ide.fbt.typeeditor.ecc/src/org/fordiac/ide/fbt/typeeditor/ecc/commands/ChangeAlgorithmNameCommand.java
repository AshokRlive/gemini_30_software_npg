/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.fbt.typeeditor.ecc.Activator;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.libraryElement.Algorithm;
import org.fordiac.ide.model.libraryElement.BasicFBType;

public class ChangeAlgorithmNameCommand extends Command {
	
	/** The interface element. */
	private Algorithm algorithm;

	private final BasicFBType fbType;
	
	/** The name. */
	private String name;

	/** The old name. */
	private String oldName;

	public ChangeAlgorithmNameCommand(BasicFBType fbType, Algorithm algorithm, String name) {
		super();
		this.algorithm = algorithm;
		this.fbType = fbType;
		this.name = name;
	}
	
	@Override
	public boolean canExecute() {
		return performNameCheck(name);
	}

	
	@Override
	public void execute() {
		oldName = algorithm.getName();
		if(performNameCheck(name)){
			redo();
		}
	}

	@Override
	public void undo() {
		algorithm.setName(oldName);
	}

	@Override
	public void redo() {
		algorithm.setName(name);
	}

	private boolean performNameCheck(String nameToCheck) {		
		if(nameToCheck.equals(NameRepository.getUniqueAlgorithmName(algorithm, fbType, nameToCheck))){
			Activator.statusLineErrorMessage(null);	
			return true;
		}
		else{
			Activator.statusLineErrorMessage("Element with Name: " + nameToCheck + " already exists!");
			return false;
		}
	}


}
