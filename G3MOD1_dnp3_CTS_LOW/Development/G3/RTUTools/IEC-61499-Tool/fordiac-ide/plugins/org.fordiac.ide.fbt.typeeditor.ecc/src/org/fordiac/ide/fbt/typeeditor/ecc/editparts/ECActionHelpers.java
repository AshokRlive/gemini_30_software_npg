/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.editparts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ui.views.properties.ComboBoxPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.fordiac.ide.fbt.typeeditor.ecc.commands.ChangeAlgorithmCommand;
import org.fordiac.ide.fbt.typeeditor.ecc.commands.ChangeOutputCommand;
import org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart;
import org.fordiac.ide.model.NamedElementComparator;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.AdapterEvent;
import org.fordiac.ide.model.libraryElement.AdapterType;
import org.fordiac.ide.model.libraryElement.Algorithm;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.ECAction;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;


/** Helper functions need by the action edit parts.
 * 
 *
 */
public class ECActionHelpers {
	
	/** The Constant VALUE_PARAMETER. */
	private static final String EVENT_NAME_PARAMETER = "Output Event";

	/** The Constant COMMENT_PARAMETER. */
	private static final String ALGORITHM_PARAMETER = "Algorithm";
	
		
	public static List<Event> getOutputEvents(BasicFBType type) {
		ArrayList<Event> events = new ArrayList<Event>();
		if(null != type){
			events.addAll(type.getInterfaceList().getEventOutputs());
	
			for (AdapterDeclaration socket : type.getInterfaceList().getSockets()) {
				if(socket.getType() instanceof AdapterType){
					events.addAll(createAdapterEventList(((AdapterType) socket
						.getType()).getInterfaceList().getEventInputs(), socket));
				}
			}
	
			for (AdapterDeclaration plug : type.getInterfaceList().getPlugs()) {
				if(plug.getType() instanceof AdapterType){
					events.addAll(createAdapterEventList(((AdapterType) plug.getType())
						.getInterfaceList().getEventOutputs(), plug));
				}
			}

			Collections.sort(events, NamedElementComparator.INSTANCE);
		}

		return events;
	}

	public static List<String> getOutputEventNames(BasicFBType type) {
		ArrayList<String> eventNames = new ArrayList<String>();
		for (Event event : getOutputEvents(type)) {
			eventNames.add(event.getName());
		}
		return eventNames;
	}

	// TODO move to a utility class as same function is used in
	// ECTransitionEditPart
	public static List<Event> createAdapterEventList(EList<Event> events,
			AdapterDeclaration adapter) {
		ArrayList<Event> adapterEvents = new ArrayList<Event>();

		for (Event event : events) {
			AdapterEvent ae = LibraryElementFactory.eINSTANCE
					.createAdapterEvent();
			ae.setName(event.getName());
			ae.setComment(event.getComment());
			ae.setAdapterDeclaration(adapter);
			adapterEvents.add(ae);
		}
		return adapterEvents;
	}

	public static List<Algorithm> getAlgorithms(BasicFBType type) {
		ArrayList<Algorithm> algorithms = new ArrayList<Algorithm>();
		algorithms.addAll(type.getAlgorithm());

		Collections.sort(algorithms, NamedElementComparator.INSTANCE);
		return algorithms;
	}

	static public BasicFBType getFBType(ECAction action) {
		if (action.eContainer() != null
				&& action.eContainer().eContainer() != null
				&& action.eContainer().eContainer().eContainer() != null)
			return (BasicFBType) action.eContainer().eContainer().eContainer();
		return null;
	}
	
	static public IPropertyDescriptor[] getPropertyDescriptors(ECAction action) {
		BasicFBType fbType = getFBType(action);
		if (fbType != null) {
			IPropertyDescriptor[] descriptors = new IPropertyDescriptor[2];

			List<String> outputEventNames = getOutputEventNames(fbType);
			String[] values = new String[outputEventNames.size() + 1];
			int i = 0;
			for (String inEvent : outputEventNames) {
				values[i] = inEvent;
				i++;
			}
			values[outputEventNames.size()] = " "; //$NON-NLS-N$

			descriptors[0] = new ComboBoxPropertyDescriptor(new String(
					EVENT_NAME_PARAMETER), EVENT_NAME_PARAMETER, values);

			List<Algorithm> algs = getAlgorithms(getFBType(action));
			values = new String[algs.size() + 1];
			i = 0;
			for (Algorithm alg : algs) {
				values[i] = alg.getName();
				i++;
			}
			values[algs.size()] = " "; //$NON-NLS-N$
			descriptors[1] = new ComboBoxPropertyDescriptor(new String(
					ALGORITHM_PARAMETER), ALGORITHM_PARAMETER, values);
			return descriptors;
		}
		return null;
	}

	static public Object getPropertyValue(Object id, ECAction action) {
		if (id instanceof String) {
			String stringId = (String) id;
			if (stringId.equals(EVENT_NAME_PARAMETER)) {
				List<String> outputEvents = getOutputEventNames(getFBType(action));
				if (null != action.getOutput()) {
					Event ev = action.getOutput();
					int val = outputEvents.indexOf(ev.getName());
					return new Integer(val);
				} else
					return new Integer(outputEvents.size());
			} else if (stringId.equals(ALGORITHM_PARAMETER)) {
				List<Algorithm> algs = getAlgorithms(getFBType(action));
				if (null != action.getAlgorithm()) {
					return new Integer(algs.indexOf(action.getAlgorithm()));
				} else
					return new Integer(algs.size());
			}

		}
		return null;
	}

	static public boolean isPropertySet(Object id, ECAction action) {
		return false;
	}

	static public void resetPropertyValue(Object id, ECAction action) {
	}

	static public void setPropertyValue(Object id, Object value, ECAction action) {
		if (id instanceof String) {
			String stringId = (String) id;
			if (stringId.equals(EVENT_NAME_PARAMETER)) {
				setOutputEvent(((Integer) value).intValue(), action);
			} else if (stringId.equals(ALGORITHM_PARAMETER)) {
				setAlgorithm(((Integer) value).intValue(), action);
			}
		}
	}
	
	private static void setOutputEvent(int selected, ECAction action) {
		// TODO this is a workaround for adapter events as these are newly
		// created every time getOutPutEvents is called
		// a fix could be to have the list of output as well as input events in
		// the model
		if (!setOutputEventRunning) {
			setOutputEventRunning = true;
			List<Event> outputEvents = getOutputEvents(getFBType(action));
			Event eo = null;
			if (selected < outputEvents.size()) {
				eo = outputEvents.get(selected);
			}
			AbstractDirectEditableEditPart.executeCommand(new ChangeOutputCommand(action, eo));
			setOutputEventRunning = false;
		}
	}

	private static void setAlgorithm(int selected, ECAction action) {
		List<Algorithm> algs = getAlgorithms(getFBType(action));
		Algorithm algorithm = null;
		if (selected < algs.size()) {
			algorithm = algs.get(selected);
		}
		AbstractDirectEditableEditPart.executeCommand(new ChangeAlgorithmCommand(action, algorithm));
	}

	// TODO already duplicated on several places put it into a util class
	 
	
	static boolean setOutputEventRunning = false;
}
