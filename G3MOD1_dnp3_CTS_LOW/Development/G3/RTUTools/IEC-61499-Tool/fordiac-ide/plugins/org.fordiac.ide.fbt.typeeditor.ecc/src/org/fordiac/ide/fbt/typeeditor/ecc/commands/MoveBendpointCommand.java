/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.commands;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.ECTransition;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.Position;

/**
 * The Class MoveBendpointCommand.
 */
public class MoveBendpointCommand extends Command {

	/** The e c transition. */
	private final ECTransition eCTransition;

	/** The point. */
	private final Point point;

	/** The old pos. */
	private Position oldPos;

	/** The new pos. */
	private Position newPos;

	/**
	 * Instantiates a new move bendpoint command.
	 * 
	 * @param transition the transition
	 * @param point the point
	 */
	public MoveBendpointCommand(final ECTransition transition, final Point point) {
		super();
		this.eCTransition = transition;
		this.point = point;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldPos = eCTransition.getPosition();
		newPos = LibraryElementFactory.eINSTANCE.createPosition();
		newPos.setX(point.x);
		newPos.setY(point.y);
		eCTransition.setPosition(newPos);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		eCTransition.setPosition(oldPos);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		eCTransition.setPosition(newPos);
	}

}
