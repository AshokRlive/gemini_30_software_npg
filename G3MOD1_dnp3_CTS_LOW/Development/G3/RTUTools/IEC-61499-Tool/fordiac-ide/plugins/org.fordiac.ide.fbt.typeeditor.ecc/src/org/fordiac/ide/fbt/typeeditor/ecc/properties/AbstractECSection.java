package org.fordiac.ide.fbt.typeeditor.ecc.properties;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.fbt.typeeditor.ecc.IAlgorithmEditorCreator;
import org.fordiac.ide.gef.properties.AbstractSection;

public abstract class AbstractECSection extends AbstractSection {
	
	@Override
	protected CommandStack getCommandStack(IWorkbenchPart part, Object input) {
		if(part instanceof IAdaptable){
            Object  obj = ((IAdaptable)part).getAdapter(CommandStack.class);
            if(null != obj){
              return (CommandStack)obj;
            }
        }
		return null;
	}

	static List<String> getLanguages() {
		ArrayList<String> languages = new ArrayList<String>();
		IExtensionRegistry registry = Platform.getExtensionRegistry();	
		IExtensionPoint point = registry.getExtensionPoint("org.fordiac.ide.fbt.typeeditor.ecc.algorithmEditor");
		IExtension[] extensions = point.getExtensions();
		for (int i = 0; i < extensions.length; i++) {
			IExtension extension = extensions[i];
			IConfigurationElement[] elements = extension.getConfigurationElements();
			for (int j = 0; j < elements.length; j++) {
				IConfigurationElement element = elements[j];
				try {
					Object obj = element.createExecutableExtension("class");
					if (obj instanceof IAlgorithmEditorCreator) {
						String lang = element.getAttribute("language");
						languages.add(lang);
					}
				} catch (Exception e) {
				}
			}
		}
		return languages;
	}
}
