/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.fbt.typeeditor.ecc.messages"; //$NON-NLS-1$
	public static String ECActionDialog_LABEL_Algorithm;
	public static String ECActionDialog_LABEL_Event;
	public static String ECActionDialog_LABEL_Title;
	public static String ECActionDialog_STATUS_Error;
	public static String ECActionDialog_STATUS_Error_Message_AlgAndEventEmpty;
	public static String ECActionDialog_STATUS_OK;
	public static String ECCEditor_LABEL_ECCEditorTabName;
	public static String ECCPaletteFactory_LABEL_Action;
	public static String ECCPaletteFactory_LABEL_STAlgorithm;
	public static String ECCPaletteFactory_LABEL_ECCGroup;
	public static String ECCPaletteFactory_LABEL_State;
	public static String ECCPaletteFactory_LABEL_ToolsGroup;
	public static String ECCPaletteFactory_TOOLTIP_Action;
	public static String ECCPaletteFactory_TOOLTIP_State;
	public static String ECCPaletteFactory_TOOLTIP_STAlgorithm;
	public static String ECStateDialog_LABEL_ECInitialState;
	public static String ECStateDialog_LABEL_ECStateComment;
	public static String ECStateDialog_LABEL_ECStateName;
	public static String ECStateDialog_LABEL_STATUS_EmptyNameError;
	public static String ECStateDialog_LABEL_STATUS_Error;
	public static String ECStateDialog_LABEL_STATUS_OK;
	public static String ECStateDialog_LABEL_Title;
	public static String ECStateSetPositionCommand_LABEL_Move;
	public static String ECTransitionConditionDialog_LABEL_Condition;
	public static String ECTransitionConditionDialog_LABEL_Event;
	public static String ECTransitionConditionDialog_LABEL_Title;
	public static String StateCreationFactory_LABEL_NewECState;	
	
	public static String ECAlgorithmGroup_Title;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}
	private Messages() {
	}
}
