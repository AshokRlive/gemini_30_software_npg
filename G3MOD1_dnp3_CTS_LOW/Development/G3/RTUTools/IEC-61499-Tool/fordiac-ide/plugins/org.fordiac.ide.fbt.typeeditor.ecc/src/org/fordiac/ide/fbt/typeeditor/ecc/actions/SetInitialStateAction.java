/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.actions;

import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.fbt.typeeditor.ecc.commands.ChangeInitialStateCommand;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECStateEditPart;
import org.fordiac.ide.model.libraryElement.ECState;

public class SetInitialStateAction extends SelectionAction {

	/**
	 * Set Initial State Action action id. Value: <code>"org.fordiac.ide.fbt.typeeditor.ecc.actions.SetInitialStateAction"</code>
	 */
	public static final String SET_INITIAL_STATE_ACTION = "org.fordiac.ide.fbt.typeeditor.ecc.actions.SetInitialStateAction";//$NON-NLS-1$

	public SetInitialStateAction(IWorkbenchPart part) {
		super(part);
		setId(SET_INITIAL_STATE_ACTION);
		setText("Initial State");
	}

	@Override
	protected boolean calculateEnabled() {
		if(1 == getSelectedObjects().size()){
			if(getSelectedObjects().get(0) instanceof ECStateEditPart){
				ECState state = ((ECStateEditPart)getSelectedObjects().get(0)).getCastedModel();
				if(null != state){
					//we can only set this state as initial state when we are not the initial sate and have no actions
					return (!state.isStartState()) && state.getECAction().isEmpty();
				}
			}
		}
		return false;
	}
	
	@Override
	public void run() {	
		ECState state = ((ECStateEditPart)getSelectedObjects().get(0)).getCastedModel();		
		execute(new ChangeInitialStateCommand(state));
		refresh();
	}

}
