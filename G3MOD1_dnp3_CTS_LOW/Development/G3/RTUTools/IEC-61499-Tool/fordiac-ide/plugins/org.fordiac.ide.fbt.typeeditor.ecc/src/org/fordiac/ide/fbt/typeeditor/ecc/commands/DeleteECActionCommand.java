/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.ECAction;
import org.fordiac.ide.model.libraryElement.ECState;

/**
 * The Class DeleteECActionCommand.
 */
public class DeleteECActionCommand extends Command {

	/** The ec action. */
	private final ECAction ecAction;

	/** The parent. */
	private ECState parent;

	/**
	 * Instantiates a new delete ec action command.
	 * 
	 * @param ecAction the ec action
	 */
	public DeleteECActionCommand(final ECAction ecAction) {
		this.ecAction = ecAction;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		parent = (ECState) ecAction.eContainer();
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		if(null != parent){
			parent.getECAction().add(ecAction);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		if(null != parent){
			parent.getECAction().remove(ecAction);
		}
	}
}
