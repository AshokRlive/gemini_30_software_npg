package org.fordiac.ide.fbt.typeeditor.ecc.properties;

import org.eclipse.jface.viewers.IFilter;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECTransitionEditPart;
import org.fordiac.ide.model.libraryElement.ECTransition;

public class ECTransitionFilter implements IFilter {

	@Override
	public boolean select(Object toTest) {
		if(toTest instanceof ECTransitionEditPart && ((ECTransitionEditPart) toTest).getCastedModel() instanceof ECTransition){
			return true;
		}
		if(toTest instanceof ECTransition){
			return true;
		}
		return false;
	}

}
