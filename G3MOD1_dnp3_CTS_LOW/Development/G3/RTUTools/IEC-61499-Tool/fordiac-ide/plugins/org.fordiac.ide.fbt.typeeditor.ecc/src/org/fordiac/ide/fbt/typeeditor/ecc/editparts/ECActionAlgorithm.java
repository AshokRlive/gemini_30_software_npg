/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.editparts;

import org.fordiac.ide.model.libraryElement.ECAction;

public class ECActionAlgorithm {

	ECAction action;
	
	public ECAction getAction(){
		return action;
	}
	
	public ECActionAlgorithm(ECAction action) {
		this.action = action;
	}
	
	String getLabel(){
		return action.getAlgorithm().getName();
	}
	
	
}
