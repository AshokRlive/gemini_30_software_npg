package org.fordiac.ide.fbt.typeeditor.ecc.properties;

import org.eclipse.jface.viewers.IFilter;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECStateEditPart;
import org.fordiac.ide.model.libraryElement.ECState;

public class ECStateFilter implements IFilter {

	@Override
	public boolean select(Object toTest) {
		if(toTest instanceof ECStateEditPart && ((ECStateEditPart) toTest).getCastedModel() instanceof ECState){
			return true;
		}
		if(toTest instanceof ECState){
			return true;
		}
		return false;
	}

}
