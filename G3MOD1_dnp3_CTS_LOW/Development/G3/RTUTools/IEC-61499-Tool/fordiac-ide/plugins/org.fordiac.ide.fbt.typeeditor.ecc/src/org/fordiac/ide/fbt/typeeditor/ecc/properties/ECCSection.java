package org.fordiac.ide.fbt.typeeditor.ecc.properties;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECActionAlgorithmEditPart;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECActionOutputEventEditPart;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECCRootEditPart;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECStateEditPart;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECTransitionEditPart;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.ECAction;
import org.fordiac.ide.model.libraryElement.ECC;

public abstract class ECCSection extends AbstractECSection {
	
	@Override
	protected BasicFBType getType() {
		if(type instanceof ECC){
			return (BasicFBType) ((ECC)type).eContainer();
		}
		return null;
	}

	@Override
	protected Object getInputType(Object input) {
		if(input instanceof ECCRootEditPart){
			return ((ECCRootEditPart) input).getCastedECCModel();	
		}
		if(input instanceof ECC){
			return (ECC) input;
		}
		if(input instanceof ECActionAlgorithmEditPart){
			return (ECC) ((ECActionAlgorithmEditPart) input).getAction().eContainer().eContainer();	
		}
		if(input instanceof ECActionOutputEventEditPart){
			ECAction action = ((ECActionOutputEventEditPart) input).getAction();
			if(null != action && null != action.eContainer() && null != action.eContainer()){
				return (ECC) action.eContainer().eContainer();
			}
		}
		if(input instanceof ECTransitionEditPart){
			return (ECC) ((ECTransitionEditPart) input).getCastedModel().eContainer();	
		}
		if(input instanceof ECStateEditPart){
			return (ECC) ((ECStateEditPart) input).getCastedModel().eContainer();	
		}
		return null;
	}
	@Override
	public void createControls(final Composite parent, final TabbedPropertySheetPage tabbedPropertySheetPage) {
		createSuperControls = false;
		super.createControls(parent, tabbedPropertySheetPage);	
	}
}
