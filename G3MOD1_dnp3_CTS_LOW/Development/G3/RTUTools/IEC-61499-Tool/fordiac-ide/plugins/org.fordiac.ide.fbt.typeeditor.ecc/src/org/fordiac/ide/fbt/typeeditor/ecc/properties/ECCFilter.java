package org.fordiac.ide.fbt.typeeditor.ecc.properties;

import org.eclipse.jface.viewers.IFilter;
import org.fordiac.ide.fbt.typeeditor.ecc.editparts.ECCRootEditPart;
import org.fordiac.ide.model.libraryElement.ECC;

public class ECCFilter implements IFilter {

	@Override
	public boolean select(Object toTest) {
		if(toTest instanceof ECCRootEditPart && ((ECCRootEditPart) toTest).getCastedECCModel() instanceof ECC){
			return true;
		}
		if(toTest instanceof ECC){
			return true;
		}
		return false;
	}

}
