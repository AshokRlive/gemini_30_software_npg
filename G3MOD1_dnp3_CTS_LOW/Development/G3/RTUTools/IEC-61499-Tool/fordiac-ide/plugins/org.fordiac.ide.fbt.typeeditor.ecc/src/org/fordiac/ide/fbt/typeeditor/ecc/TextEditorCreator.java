/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.fordiac.ide.model.libraryElement.BasicFBType;

/**
 * The Class TextEditorCreator.
 */
public class TextEditorCreator implements IAlgorithmEditorCreator {

	/**
	 * Instantiates a new text editor creator.
	 */
	public TextEditorCreator() {
	}

	@Override
	public IAlgorithmEditor createAlgorithmEditor(final Composite parent, BasicFBType fbType) {
		TextEditor editor = new TextEditor(parent, null, null, false,
				SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		return editor;
	}

}
