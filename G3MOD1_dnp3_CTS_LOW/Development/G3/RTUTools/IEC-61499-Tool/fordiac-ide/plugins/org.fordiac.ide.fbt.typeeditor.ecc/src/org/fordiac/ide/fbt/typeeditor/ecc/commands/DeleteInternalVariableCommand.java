/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.ecc.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

/**
 * The Class CreateInternalVariableCommand.
 */
public class DeleteInternalVariableCommand extends Command {

	
	/** The fb type. */
	private final BasicFBType fbType;

	private VarDeclaration varToDelete;
	
	/** The old index. */
	private int oldIndex;
	
	/**
	 * Instantiates a new creates the input variable command.
	 * 
	 * @param dataType
	 *            the data type
	 * @param fbType
	 *            the fb type
	 */
	public DeleteInternalVariableCommand(final BasicFBType fbType, final VarDeclaration var) {
		this.fbType = fbType;
		this.varToDelete = var;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {		
		oldIndex = fbType.getInternalVars().indexOf(varToDelete);
		
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		fbType.getInternalVars().add(oldIndex, varToDelete);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		fbType.getInternalVars().remove(varToDelete);
	}
}
