/**
 * *******************************************************************************
 *  * Copyright (c) 2007 - 2011 4DIAC - consortium.
 *  * All rights reserved. This program and the accompanying materials
 *  * are made available under the terms of the Eclipse Public License v1.0
 *  * which accompanies this distribution, and is available at
 *  * http://www.eclipse.org/legal/epl-v10.html
 *  *
 *  *******************************************************************************
 */
package org.fordiac.ide.model.ui.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.fordiac.ide.model.Palette.provider.fordiacEditPlugin;

import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * This is the item provider adapter for a {@link org.fordiac.ide.model.ui.Diagram} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DiagramItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiagramItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(UiPackage.Literals.DIAGRAM__CHILDREN);
			childrenFeatures.add(UiPackage.Literals.DIAGRAM__CONNECTIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_Diagram_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Diagram.class)) {
			case UiPackage.DIAGRAM__CHILDREN:
			case UiPackage.DIAGRAM__CONNECTIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createFBView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createInterfaceElementView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createSubAppView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createSubAppInterfaceElementView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createInternalSubAppInterfaceElementView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createDeviceView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createResourceView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createResourceContainerView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createMappedSubAppView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createMappedSubAppInterfaceElementView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createSegmentView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createCompositeInternalInterfaceElementView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CHILDREN,
				 UiFactory.eINSTANCE.createMonitoringView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CONNECTIONS,
				 UiFactory.eINSTANCE.createConnectionView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.DIAGRAM__CONNECTIONS,
				 UiFactory.eINSTANCE.createLinkView()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return fordiacEditPlugin.INSTANCE;
	}

}
