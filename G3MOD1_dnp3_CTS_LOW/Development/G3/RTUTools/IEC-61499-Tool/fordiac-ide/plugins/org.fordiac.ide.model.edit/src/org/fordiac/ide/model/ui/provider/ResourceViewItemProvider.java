/**
 * *******************************************************************************
 *  * Copyright (c) 2007 - 2011 4DIAC - consortium.
 *  * All rights reserved. This program and the accompanying materials
 *  * are made available under the terms of the Eclipse Public License v1.0
 *  * which accompanies this distribution, and is available at
 *  * http://www.eclipse.org/legal/epl-v10.html
 *  *
 *  *******************************************************************************
 */
package org.fordiac.ide.model.ui.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * This is the item provider adapter for a {@link org.fordiac.ide.model.ui.ResourceView} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ResourceViewItemProvider
	extends ViewItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceViewItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addResourceElementPropertyDescriptor(object);
			addDeviceTypeResourcePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Resource Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addResourceElementPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ResourceView_resourceElement_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ResourceView_resourceElement_feature", "_UI_ResourceView_type"),
				 UiPackage.Literals.RESOURCE_VIEW__RESOURCE_ELEMENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Device Type Resource feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDeviceTypeResourcePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ResourceView_deviceTypeResource_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ResourceView_deviceTypeResource_feature", "_UI_ResourceView_type"),
				 UiPackage.Literals.RESOURCE_VIEW__DEVICE_TYPE_RESOURCE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(UiPackage.Literals.RESOURCE_VIEW__INTERFACE_ELEMENTS);
			childrenFeatures.add(UiPackage.Literals.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ResourceView.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ResourceView"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		ResourceView resourceView = (ResourceView)object;
		return getString("_UI_ResourceView_type") + " " + resourceView.isDeviceTypeResource();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ResourceView.class)) {
			case UiPackage.RESOURCE_VIEW__DEVICE_TYPE_RESOURCE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case UiPackage.RESOURCE_VIEW__INTERFACE_ELEMENTS:
			case UiPackage.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.RESOURCE_VIEW__INTERFACE_ELEMENTS,
				 UiFactory.eINSTANCE.createInterfaceElementView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.RESOURCE_VIEW__INTERFACE_ELEMENTS,
				 UiFactory.eINSTANCE.createSubAppInterfaceElementView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.RESOURCE_VIEW__INTERFACE_ELEMENTS,
				 UiFactory.eINSTANCE.createInternalSubAppInterfaceElementView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.RESOURCE_VIEW__INTERFACE_ELEMENTS,
				 UiFactory.eINSTANCE.createMappedSubAppInterfaceElementView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.RESOURCE_VIEW__INTERFACE_ELEMENTS,
				 UiFactory.eINSTANCE.createCompositeInternalInterfaceElementView()));

		newChildDescriptors.add
			(createChildParameter
				(UiPackage.Literals.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM,
				 UiFactory.eINSTANCE.createUIResourceEditor()));
	}

}
