/**
 * *******************************************************************************
 *  * Copyright (c) 2007 - 2011 4DIAC - consortium.
 *  * All rights reserved. This program and the accompanying materials
 *  * are made available under the terms of the Eclipse Public License v1.0
 *  * which accompanies this distribution, and is available at
 *  * http://www.eclipse.org/legal/epl-v10.html
 *  *
 *  *******************************************************************************
 */
package org.fordiac.ide.model.libraryElement.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.fordiac.ide.model.Palette.provider.fordiacEditPlugin;

import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.SubAppInterfaceList;

/**
 * This is the item provider adapter for a {@link org.fordiac.ide.model.libraryElement.SubAppInterfaceList} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SubAppInterfaceListItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppInterfaceListItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__EVENT_INPUTS);
			childrenFeatures.add(LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__EVENT_OUTPUTS);
			childrenFeatures.add(LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__INPUT_VARS);
			childrenFeatures.add(LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__OUTPUT_VARS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns SubAppInterfaceList.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SubAppInterfaceList"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_SubAppInterfaceList_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SubAppInterfaceList.class)) {
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_INPUTS:
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_OUTPUTS:
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__INPUT_VARS:
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__OUTPUT_VARS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__EVENT_INPUTS,
				 LibraryElementFactory.eINSTANCE.createEvent()));

		newChildDescriptors.add
			(createChildParameter
				(LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__EVENT_INPUTS,
				 LibraryElementFactory.eINSTANCE.createAdapterEvent()));

		newChildDescriptors.add
			(createChildParameter
				(LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__EVENT_OUTPUTS,
				 LibraryElementFactory.eINSTANCE.createEvent()));

		newChildDescriptors.add
			(createChildParameter
				(LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__EVENT_OUTPUTS,
				 LibraryElementFactory.eINSTANCE.createAdapterEvent()));

		newChildDescriptors.add
			(createChildParameter
				(LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__INPUT_VARS,
				 LibraryElementFactory.eINSTANCE.createVarDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__INPUT_VARS,
				 LibraryElementFactory.eINSTANCE.createAdapterDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__OUTPUT_VARS,
				 LibraryElementFactory.eINSTANCE.createVarDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__OUTPUT_VARS,
				 LibraryElementFactory.eINSTANCE.createAdapterDeclaration()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__EVENT_INPUTS ||
			childFeature == LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__EVENT_OUTPUTS ||
			childFeature == LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__INPUT_VARS ||
			childFeature == LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST__OUTPUT_VARS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return fordiacEditPlugin.INSTANCE;
	}

}
