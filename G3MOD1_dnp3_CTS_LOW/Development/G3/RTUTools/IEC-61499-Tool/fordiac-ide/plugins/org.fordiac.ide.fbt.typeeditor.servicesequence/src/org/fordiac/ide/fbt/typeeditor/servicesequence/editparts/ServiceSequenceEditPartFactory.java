/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.editparts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.ui.parts.GraphicalEditor;
import org.fordiac.ide.gef.editparts.Abstract4diacEditPartFactory;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.InputPrimitive;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;
import org.fordiac.ide.model.libraryElement.ServiceSequence;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

/**
 * A factory for creating FBInterfaceEditPart objects.
 */
public class ServiceSequenceEditPartFactory extends Abstract4diacEditPartFactory {

	

	public ServiceSequenceEditPartFactory(GraphicalEditor editor) {
		super(editor);
	}

	@Override
	protected EditPart getPartForElement(EditPart context, final Object modelElement) {
		if (modelElement instanceof FBType && context == null) {
			return new SequenceRootEditPart();
		}
		if (modelElement instanceof ServiceSequence) {
			return new ServiceSequenceEditPart();
		}
		if (modelElement instanceof ServiceTransaction) {
			return new TransactionEditPart();
		}
		if (modelElement instanceof InputPrimitive) {
			return new InputPrimitiveEditPart();
		}
		if (modelElement instanceof OutputPrimitive) {
			return new OutputPrimitiveEditPart();
		}
		if (modelElement instanceof PrimitiveConnection) {
			return new PrimitiveConnectionEditPart();
		}
		if (modelElement instanceof ConnectingConnection) {
			return new ConnectingConnectionEditPart();
		}
		throw createEditpartCreationException(modelElement);
	}

}
