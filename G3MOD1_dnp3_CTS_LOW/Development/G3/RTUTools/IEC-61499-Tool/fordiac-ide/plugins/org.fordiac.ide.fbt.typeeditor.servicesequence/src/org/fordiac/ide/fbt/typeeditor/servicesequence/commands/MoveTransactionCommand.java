/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.ServiceSequence;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

public class MoveTransactionCommand extends Command {

	private final ServiceSequence parent;
	private final ServiceTransaction moveElement;
	private final int oldIndex;
	private int newIndex;

	public MoveTransactionCommand(ServiceTransaction model, int oldIndex, int newIndex) {
		this.moveElement = model;
		this.oldIndex = oldIndex;
		this.newIndex = newIndex;
		this.parent = (ServiceSequence) moveElement.eContainer();
	}

	@Override
	public boolean canExecute() {
		return ((parent != null) && (moveElement != null));
	}
	
	@Override
	public void execute() {
		parent.getServiceTransaction().move(newIndex, moveElement);
	}
	
	@Override
	public void undo() {
		parent.getServiceTransaction().move(oldIndex, moveElement);
	}
	
	@Override
	public void redo() {
		parent.getServiceTransaction().move(newIndex, moveElement);
	}
}
