/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.ChangeServiceInterfaceNameCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.SequenceRootEditPart;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.SequenceRootEditPart.ServiceFigure;

public class ChangeInterfaceNameEditPolicy extends DirectEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.DirectEditPolicy#getDirectEditCommand(org
	 * .eclipse.gef.requests.DirectEditRequest)
	 */
	@Override
	protected Command getDirectEditCommand(final DirectEditRequest request) {
		if (getHost() instanceof SequenceRootEditPart) {
			SequenceRootEditPart viewEditPart = (SequenceRootEditPart) getHost();
			return new ChangeServiceInterfaceNameCommand((String) request.getCellEditor().getValue(), viewEditPart.getFBType(), viewEditPart.isLeft(request));
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.DirectEditPolicy#showCurrentEditValue(org
	 * .eclipse.gef.requests.DirectEditRequest)
	 */
	@Override
	protected void showCurrentEditValue(final DirectEditRequest request) {
		String value = (String) request.getCellEditor().getValue();
		if (getHost() instanceof SequenceRootEditPart) {
			SequenceRootEditPart viewEditPart = (SequenceRootEditPart) getHost();
			if(viewEditPart.isLeft(request)){
				((ServiceFigure)viewEditPart.getFigure()).getLeftLabel().setText(value);
			}else{
				((ServiceFigure)viewEditPart.getFigure()).getRightLabel().setText(value);
			}
		}
	}
}
