/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

public class MoveOutputPrimitiveCommand extends Command {

	private final ServiceTransaction parent;
	private OutputPrimitive moveElement;
	private final OutputPrimitive refElement;
	private int oldIndex;
	
	public MoveOutputPrimitiveCommand(ServiceTransaction transaction, OutputPrimitive element, OutputPrimitive refElement) {
		this.parent = transaction;
		this.moveElement = element;
		this.refElement = refElement;
		this.oldIndex = parent.getOutputPrimitive().indexOf(moveElement);
	}
	
	@Override
	public boolean canExecute() {
		return ((parent != null) && (moveElement != null));
	}
	
	@Override
	public void execute() {
		move();
	}
	
	@Override
	public void undo() {
		if(null == refElement){
			parent.getOutputPrimitive().move(parent.getOutputPrimitive().size() + 1, moveElement);
		}
		else{
			parent.getOutputPrimitive().move(oldIndex, moveElement);
		}
	}
	
	@Override
	public void redo() {
		move();
	}
	
	private void move(){
		if(null == refElement){
			parent.getOutputPrimitive().move(parent.getOutputPrimitive().size() - 1, moveElement);
		}
		else{
			int index = parent.getOutputPrimitive().indexOf(refElement);
			parent.getOutputPrimitive().move(index, moveElement);
		}
	}
}
