/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.InputPrimitive;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

/**
 * DeleteInputPrimitiveCommand removes an InputPrimitive from its parent. If the
 * primitive is the last one of the parent, then also the parent
 * (ServiceTransaction) is removed.
 * 
 * @author gebenh
 */
public class DeleteInputPrimitiveCommand extends Command {

	private final InputPrimitive primitive;
	private ServiceTransaction parent;
	private DeleteTransactionCommand deleteTransactionCmd = null;

	/**
	 * Constructor.
	 * 
	 * @param primitive the primitive
	 */
	public DeleteInputPrimitiveCommand(InputPrimitive primitive) {
		this.primitive = primitive;
		this.parent = (ServiceTransaction) primitive.eContainer();
	}

	@Override
	public boolean canExecute() {
		if (null == primitive) {
			return false;
		}
		if (primitive.eContainer() == null || !(primitive.eContainer() instanceof ServiceTransaction)) {
			return false;
		}
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		if (null != parent) {
			parent.setInputPrimitive(null);
			if (parent.getOutputPrimitive().size() == 0) {
				deleteTransactionCmd = new DeleteTransactionCommand(parent);
				deleteTransactionCmd.execute();
			}
		}

	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		if (deleteTransactionCmd != null) {
			deleteTransactionCmd.undo();
		}
		parent.setInputPrimitive(primitive);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		parent.setInputPrimitive(null);
		if (deleteTransactionCmd != null) {
			deleteTransactionCmd.redo();
		}
	}

}
