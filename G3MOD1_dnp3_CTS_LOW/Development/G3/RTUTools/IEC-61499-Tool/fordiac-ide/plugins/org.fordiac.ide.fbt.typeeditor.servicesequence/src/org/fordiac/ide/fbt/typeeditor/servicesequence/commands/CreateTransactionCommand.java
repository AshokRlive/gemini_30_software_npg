/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.InputPrimitive;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.Service;
import org.fordiac.ide.model.libraryElement.ServiceSequence;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

/**
 * Command that creates a new transaction.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class CreateTransactionCommand extends Command {

	private ServiceTransaction transaction;	
	private ServiceSequence parent;
	
	/**
	 * The Constructor.
	 * 
	 * @param view the view
	 */
	public CreateTransactionCommand(ServiceSequence sequence) {
		this.parent = sequence;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return transaction != null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		transaction = LibraryElementFactory.eINSTANCE.createServiceTransaction();
		InputPrimitive primitive = LibraryElementFactory.eINSTANCE.createInputPrimitive();
		primitive.setEvent("INIT");
		primitive.setInterface(((Service)parent.eContainer()).getLeftInterface());
		transaction.setInputPrimitive(primitive);
		parent.getServiceTransaction().add(transaction);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		parent.getServiceTransaction().remove(transaction);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		parent.getServiceTransaction().add(transaction);
	}
}
