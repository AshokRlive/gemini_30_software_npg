package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.Primitive;

public class ChangePrimitiveParameterCommand extends Command {
	private Primitive primitive;
	private String parameters;
	private String oldParameters;

	public ChangePrimitiveParameterCommand(Primitive primitive, String parameters) {
		super();
		this.primitive = primitive;
		this.parameters = parameters;
	}

	@Override
	public void execute() {
		oldParameters = primitive.getParameters();
		primitive.setParameters(parameters);
	}

	@Override
	public void undo() {
		primitive.setParameters(oldParameters);
	}
	@Override
	public void redo() {
		primitive.setParameters(parameters);
	}
}
