package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.Service;

public class ChangeServiceInterfaceCommentCommand extends Command {
	private Service service;
	private boolean isLeftInterface;
	private String comment;
	private String oldComment;
	
	public ChangeServiceInterfaceCommentCommand(String comment, Service service, boolean isLeftInterface){
		this.service = service;
		this.isLeftInterface = isLeftInterface;
		this.comment = comment;
	}
	
	public void execute(){
		if(isLeftInterface){
			oldComment = service.getLeftInterface().getComment();
		}else{
			oldComment = service.getRightInterface().getComment();
		}
		redo();
	}
	
	public void undo() {
		if(isLeftInterface){
			service.getLeftInterface().setComment(oldComment);
		}else{
			service.getRightInterface().setComment(oldComment);
		}
	}

	public void redo() {
		if(isLeftInterface){
			service.getLeftInterface().setComment(comment);
		}else{
			service.getRightInterface().setComment(comment);
		}
	}
}
