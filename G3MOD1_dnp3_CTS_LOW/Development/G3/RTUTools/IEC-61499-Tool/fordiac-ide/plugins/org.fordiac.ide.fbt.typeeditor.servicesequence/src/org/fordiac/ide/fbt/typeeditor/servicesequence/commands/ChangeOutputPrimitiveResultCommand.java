package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;

public class ChangeOutputPrimitiveResultCommand extends Command {
	private OutputPrimitive primitive;
	private int results;
	private int oldResults;

	public ChangeOutputPrimitiveResultCommand(OutputPrimitive primitive, String results) {
		super();
		this.primitive = primitive;
		this.results = (!results.equals("")) ? Integer.valueOf(results) : 0; //$NON-NLS-1$
	}

	@Override
	public void execute() {
		oldResults = primitive.getTestResult();
		primitive.setTestResult(results);
	}

	@Override
	public void undo() {
		primitive.setTestResult(oldResults);
	}
	@Override
	public void redo() {
		primitive.setTestResult(results);
	}
}
