/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ComponentEditPolicy;
import org.eclipse.gef.requests.GroupRequest;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.DeleteInputPrimitiveCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.DeleteOutputPrimitiveCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.InputPrimitiveEditPart;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.OutputPrimitiveEditPart;

/**
 * Policy for deleting ServiceSequence.
 * 
 * @author gebenh
 */
public class DeletePrimitiveEditPolicy extends ComponentEditPolicy {

	@Override
	protected Command getDeleteCommand(GroupRequest request) {
		if (getHost() instanceof InputPrimitiveEditPart) {
			DeleteInputPrimitiveCommand c = new DeleteInputPrimitiveCommand(
					((InputPrimitiveEditPart) getHost()).getCastedModel());
			return c;
		}
		if (getHost() instanceof OutputPrimitiveEditPart) {
			DeleteOutputPrimitiveCommand c = new DeleteOutputPrimitiveCommand(
					((OutputPrimitiveEditPart) getHost()).getCastedModel());
			return c;
		}
		return null;
	}

}
