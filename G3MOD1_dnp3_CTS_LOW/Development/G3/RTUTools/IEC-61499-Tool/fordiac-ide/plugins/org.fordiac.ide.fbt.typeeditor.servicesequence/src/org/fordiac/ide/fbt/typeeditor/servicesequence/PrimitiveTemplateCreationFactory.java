/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence;

import org.fordiac.ide.gef.utilities.TemplateCreationFactory;

/**
 * A factory for creating PrimitiveTemplateCreation objects.
 */
public class PrimitiveTemplateCreationFactory extends TemplateCreationFactory {

	private boolean leftInterface;
	
	/**
	 * Instantiates a new primitive template creation factory.
	 * 
	 * @param typeTemplate the type template
	 * @param leftInterface the left interface
	 */
	public PrimitiveTemplateCreationFactory(Object typeTemplate, boolean leftInterface) {
		super(typeTemplate);
		this.leftInterface = leftInterface;
	}

	/**
	 * Checks if is left interface.
	 * 
	 * @return true, if is left interface
	 */
	public boolean isLeftInterface() {
		return leftInterface;
	}

	/**
	 * Sets the left interface.
	 * 
	 * @param leftInterface the new left interface
	 */
	public void setLeftInterface(boolean leftInterface) {
		this.leftInterface = leftInterface;
	}

	
}
