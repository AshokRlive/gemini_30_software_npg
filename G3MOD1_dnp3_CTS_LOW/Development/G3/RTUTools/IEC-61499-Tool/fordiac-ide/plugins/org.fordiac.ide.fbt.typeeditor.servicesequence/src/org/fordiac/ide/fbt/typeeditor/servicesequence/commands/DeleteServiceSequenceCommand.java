/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.ServiceSequence;

/**
 * The Class DeleteServiceSquenceCommand.
 */
public class DeleteServiceSequenceCommand extends Command {

	private FBType fbType;
	private ServiceSequence sequence;
	
	/**
	 * Instantiates a new delete service squence command.
	 * 
	 * @param fbType the fb type
	 * @param sequence the sequence
	 */
	public DeleteServiceSequenceCommand(FBType fbType, ServiceSequence sequence) {
		this.fbType = fbType;
		this.sequence = sequence;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return fbType != null && sequence != null;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		fbType.getService().getServiceSequence().remove(sequence);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		fbType.getService().getServiceSequence().add(sequence);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		fbType.getService().getServiceSequence().remove(sequence);
	}
}
