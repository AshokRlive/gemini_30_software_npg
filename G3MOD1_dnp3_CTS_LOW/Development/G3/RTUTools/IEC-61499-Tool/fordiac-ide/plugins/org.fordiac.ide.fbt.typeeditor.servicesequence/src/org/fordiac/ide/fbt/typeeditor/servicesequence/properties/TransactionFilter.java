package org.fordiac.ide.fbt.typeeditor.servicesequence.properties;

import org.eclipse.jface.viewers.IFilter;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.TransactionEditPart;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

public class TransactionFilter implements IFilter {

	@Override
	public boolean select(Object toTest) {
		if(toTest instanceof TransactionEditPart){
			return true;
		}
		if(toTest instanceof ServiceTransaction){
			return true;
		}
		return false;
	}

}
