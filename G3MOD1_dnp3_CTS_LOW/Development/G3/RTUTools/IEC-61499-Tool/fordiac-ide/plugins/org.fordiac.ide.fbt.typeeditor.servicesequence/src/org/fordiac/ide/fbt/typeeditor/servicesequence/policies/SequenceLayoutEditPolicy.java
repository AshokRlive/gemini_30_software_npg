/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.policies;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gef.requests.DropRequest;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.CreateTransactionCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.MoveTransactionCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.TransactionEditPart;
import org.fordiac.ide.gef.policies.EmptyXYLayoutEditPolicy;
import org.fordiac.ide.model.libraryElement.Primitive;
import org.fordiac.ide.model.libraryElement.ServiceSequence;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

public class SequenceLayoutEditPolicy extends EmptyXYLayoutEditPolicy {

	@Override
	protected Command createChangeConstraintCommand(ChangeBoundsRequest request, EditPart child, Object constraint){
		EditPart after;
		if(child instanceof TransactionEditPart){
			ServiceSequence target = (ServiceSequence) getHost().getModel();
			after = getInsertionReference(((DropRequest) request).getLocation());
			int newindex = -1;
			if(after != null){		
				if(after.getModel() instanceof ServiceTransaction){
					ServiceTransaction refElement = ((TransactionEditPart)after).getCastedModel();
					newindex = target.getServiceTransaction().indexOf(refElement);
				}else{
					if(after.getModel() instanceof Primitive){
						ServiceTransaction refElement = ((TransactionEditPart)after.getParent()).getCastedModel();
						newindex = target.getServiceTransaction().indexOf(refElement);
					}					
				}
				if(newindex > -1){
					return new MoveTransactionCommand((ServiceTransaction) child.getModel(), target.getServiceTransaction().indexOf(child.getModel()), newindex);					
				}
			}		
		}
		return null;
	}
	
	@Override
	protected Command getCreateCommand(final CreateRequest request) {
		Object model = getHost().getModel();
		if(model instanceof ServiceSequence){
			return new CreateTransactionCommand((ServiceSequence) model);
		}
		return null;
	}

}
