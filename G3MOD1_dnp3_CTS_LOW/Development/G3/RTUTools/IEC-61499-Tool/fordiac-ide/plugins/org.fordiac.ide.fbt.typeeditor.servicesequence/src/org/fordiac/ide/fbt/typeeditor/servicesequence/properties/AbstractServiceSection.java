package org.fordiac.ide.fbt.typeeditor.servicesequence.properties;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.contentoutline.ContentOutline;
import org.fordiac.ide.fbt.typeeditor.editors.FBTypeContentOutline;
import org.fordiac.ide.fbt.typeeditor.editors.FBTypeEditor;
import org.fordiac.ide.gef.properties.AbstractSection;
import org.fordiac.ide.model.libraryElement.Primitive;
import org.fordiac.ide.model.libraryElement.Service;

public abstract class AbstractServiceSection extends AbstractSection {
	protected CommandStack getCommandStack(IWorkbenchPart part) {
		if(part instanceof FBTypeEditor){
			return ((FBTypeEditor)part).getCommandStack();
		}
		if(part instanceof ContentOutline){
			return ((FBTypeContentOutline) ((ContentOutline)part).getCurrentPage()).getCommandStack();
		}
		return null;
	}
	
	protected boolean isLeftInterface(Primitive primitive){
		if(null != primitive){
			Service service = (Service) primitive.eContainer().eContainer().eContainer();
			if(primitive.getInterface().equals(service.getLeftInterface())){
				return true;
			}
		}
		return false;
	}
}
