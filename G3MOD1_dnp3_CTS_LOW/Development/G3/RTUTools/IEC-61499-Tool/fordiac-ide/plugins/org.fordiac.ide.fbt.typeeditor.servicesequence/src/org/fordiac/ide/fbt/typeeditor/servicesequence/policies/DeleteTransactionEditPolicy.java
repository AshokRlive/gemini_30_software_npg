/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ComponentEditPolicy;
import org.eclipse.gef.requests.GroupRequest;
import org.fordiac.ide.fbt.typeeditor.servicesequence.commands.DeleteTransactionCommand;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.TransactionEditPart;

/**
 * DeleteTransactionEditPolicy. The policy creates a command to delete
 * Transactions.
 * 
 * @author gebenh
 */
public class DeleteTransactionEditPolicy extends ComponentEditPolicy {

	@Override
	protected Command getDeleteCommand(GroupRequest request) {
		if (getHost() instanceof TransactionEditPart) {
			return new DeleteTransactionCommand(((TransactionEditPart) getHost()).getCastedModel());
		}
		return super.getDeleteCommand(request);
	}

}
