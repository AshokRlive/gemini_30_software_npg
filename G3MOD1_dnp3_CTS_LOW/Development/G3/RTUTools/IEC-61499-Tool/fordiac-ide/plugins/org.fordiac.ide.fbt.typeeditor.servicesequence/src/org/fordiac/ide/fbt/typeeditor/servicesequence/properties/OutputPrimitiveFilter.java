package org.fordiac.ide.fbt.typeeditor.servicesequence.properties;

import org.eclipse.jface.viewers.IFilter;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.OutputPrimitiveEditPart;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;

public class OutputPrimitiveFilter implements IFilter {

	@Override
	public boolean select(Object toTest) {
		if(toTest instanceof OutputPrimitiveEditPart && ((OutputPrimitiveEditPart) toTest).getCastedModel() instanceof OutputPrimitive){
			return true;
		}
		if(toTest instanceof OutputPrimitive){
			return true;
		}
		return false;
	}

}
