package org.fordiac.ide.fbt.typeeditor.servicesequence.properties;

import org.eclipse.jface.viewers.IFilter;
import org.fordiac.ide.fbt.typeeditor.servicesequence.editparts.InputPrimitiveEditPart;
import org.fordiac.ide.model.libraryElement.InputPrimitive;

public class InputPrimitiveFilter implements IFilter {

	@Override
	public boolean select(Object toTest) {
		if(toTest instanceof InputPrimitiveEditPart && ((InputPrimitiveEditPart) toTest).getCastedModel() instanceof InputPrimitive){
			return true;
		}
		if(toTest instanceof InputPrimitive){
			return true;
		}
		return false;
	}

}
