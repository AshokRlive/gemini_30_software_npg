/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;

public class MoveOutputPrimitiveToOtherTransactionCommand extends Command {

	private ServiceTransaction srcTransaction;
	private ServiceTransaction dstTransaction;
	private OutputPrimitive element;
	private OutputPrimitive refElement;
	
	public MoveOutputPrimitiveToOtherTransactionCommand(ServiceTransaction srcTransaction,
			ServiceTransaction dstTransaction, OutputPrimitive element, OutputPrimitive refElement) {
		this.srcTransaction = srcTransaction;
		this.dstTransaction = dstTransaction;
		this.element = element;
		this.refElement = refElement;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return ((srcTransaction != null) && (dstTransaction != null) && (element != null));
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		srcTransaction.getOutputPrimitive().remove(element);
		if(null == refElement){
			dstTransaction.getOutputPrimitive().add(element);
		}
		else{
			int index = dstTransaction.getOutputPrimitive().indexOf(refElement);
			dstTransaction.getOutputPrimitive().add(index, element);
		}
		super.execute();
	}
	
}
