package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.ServiceSequence;

public class ChangeSequenceCommentCommand extends Command {
	private ServiceSequence sequence;
	private String comment;
	private String oldComment;
	
	public ChangeSequenceCommentCommand(String comment, ServiceSequence sequence){
		this.sequence = sequence;
		this.comment = comment;
	}
	
	public void execute(){
		oldComment = sequence.getComment();
		redo();
	}
	
	public void undo() {
		sequence.setComment(oldComment);
	}

	public void redo() {
		sequence.setComment(comment);
	}
}
