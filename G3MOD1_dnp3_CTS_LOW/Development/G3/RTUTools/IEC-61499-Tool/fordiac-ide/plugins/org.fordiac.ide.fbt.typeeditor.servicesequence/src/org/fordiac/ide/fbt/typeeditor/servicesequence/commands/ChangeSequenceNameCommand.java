package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.ServiceSequence;

public class ChangeSequenceNameCommand extends Command {
	private ServiceSequence sequence;
	private String name;
	private String oldName;
	
	public ChangeSequenceNameCommand(String name, ServiceSequence sequence){
		this.sequence = sequence;
		this.name = name;
	}
	
	public void execute(){
		oldName = sequence.getName();
		redo();
	}
	
	public void undo() {
		sequence.setName(oldName);
	}

	public void redo() {
		sequence.setName(name);
	}
}
