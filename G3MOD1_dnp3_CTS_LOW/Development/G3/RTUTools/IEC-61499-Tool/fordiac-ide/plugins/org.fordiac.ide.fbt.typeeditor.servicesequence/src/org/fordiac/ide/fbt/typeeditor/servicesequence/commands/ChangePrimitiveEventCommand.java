package org.fordiac.ide.fbt.typeeditor.servicesequence.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.Primitive;

public class ChangePrimitiveEventCommand extends Command {
	private Primitive primitive;
	private String eventName;
	private String oldEventName;

	public ChangePrimitiveEventCommand(Primitive primitive, String eventName) {
		super();
		this.primitive = primitive;
		this.eventName = eventName;
	}

	@Override
	public void execute() {
		oldEventName = primitive.getEvent();
		primitive.setEvent(eventName);
	}

	@Override
	public void undo() {
		primitive.setEvent(oldEventName);
	}
	@Override
	public void redo() {
		primitive.setEvent(eventName);
	}
}
