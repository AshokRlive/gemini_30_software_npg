/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.sysimport.iec61499_2systemimport;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;
import org.fordiac.ide.ui.wizardpages.ResultWizardPage;

/**
 * The Class SystemImport.
 * 
 * @author gebenh
 */
public class SystemImport extends Wizard implements IImportWizard {

	/**
	 * Instantiates a new system import.
	 */
	public SystemImport() {
	}

	private IEC61499_2ImportWizardPage page2;
	private ResultWizardPage resultPage;

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		super.addPages();
		resultPage = new ResultWizardPage("Result page");
		page2 = new IEC61499_2ImportWizardPage("Import System", resultPage);

		addPage(page2);
		addPage(resultPage);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
	 */
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
	}

}
