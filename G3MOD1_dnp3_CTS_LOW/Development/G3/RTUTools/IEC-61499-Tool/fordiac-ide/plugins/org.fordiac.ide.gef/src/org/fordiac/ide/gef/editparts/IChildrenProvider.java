/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.editparts;

import java.util.ArrayList;

import org.fordiac.ide.gef.IEditPartCreator;
import org.fordiac.ide.model.ui.Diagram;

/**
 * The Interface IChildrenProvider.
 * 
 * @author gebenh
 */
public interface IChildrenProvider {

	/**
	 * Returns a list of children,.
	 * 
	 * @param diagram the diagram
	 * 
	 * @return a list of children
	 */
	ArrayList<IEditPartCreator> getChildren(Diagram diagram);

	/**
	 * Checks if is enabled.
	 * 
	 * @return true, if is enabled
	 */
	boolean isEnabled();

}
