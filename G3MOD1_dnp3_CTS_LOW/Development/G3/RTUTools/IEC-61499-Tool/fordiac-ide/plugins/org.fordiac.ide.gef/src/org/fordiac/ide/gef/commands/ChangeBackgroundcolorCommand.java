/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.gef.Messages;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.Utils;

/**
 * A command to rename any INamedElement.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ChangeBackgroundcolorCommand extends Command {

	/** The Constant CHANGE. */
	private static final String CHANGE = Messages.ChangeBackgroundcolorCommand_LABEL_ChangeColor;

	/** The fordiac color. */
	private org.fordiac.ide.model.ui.Color fordiacColor;

	/** The old color. */
	private org.fordiac.ide.model.ui.Color oldColor;

	/** The rgb. */
	private final RGB rgb;

	/** The view. */
	private final View view;

	/** The editor. */
	private IEditorPart editor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return view != null;
	}

	/**
	 * Instantiates a new change backgroundcolor command.
	 * 
	 * @param view the view
	 * @param rgb the rgb
	 */
	public ChangeBackgroundcolorCommand(final View view, final RGB rgb) {
		super(CHANGE);
		this.rgb = rgb;
		this.view = view;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();

		fordiacColor = UiFactory.eINSTANCE.createColor();
		fordiacColor.setBlue(rgb.blue);
		fordiacColor.setRed(rgb.red);
		fordiacColor.setGreen(rgb.green);
		oldColor = view.getBackgroundColor();
		view.setBackgroundColor(fordiacColor);

	}

	/**
	 * Restores the old InstanceName.
	 */
	@Override
	public void undo() {
		view.setBackgroundColor(oldColor);
	}

	/**
	 * Redo.
	 * 
	 * @see ChangeBackgroundcolorCommand#execute()
	 */
	@Override
	public void redo() {
		view.setBackgroundColor(fordiacColor);

	}

	/**
	 * Gets the fordiac color.
	 * 
	 * @return the fordiac color
	 */
	public org.fordiac.ide.model.ui.Color getFordiacColor() {
		return fordiacColor;
	}

	/**
	 * Sets the fordiac color.
	 * 
	 * @param fordiacColor the new fordiac color
	 */
	public void setFordiacColor(
			final org.fordiac.ide.model.ui.Color fordiacColor) {
		this.fordiacColor = fordiacColor;
	}

}
