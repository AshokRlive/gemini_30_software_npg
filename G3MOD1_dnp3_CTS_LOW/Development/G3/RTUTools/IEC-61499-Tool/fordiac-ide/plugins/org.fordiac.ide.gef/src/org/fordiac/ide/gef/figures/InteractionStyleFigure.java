/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.figures;

import org.eclipse.draw2d.geometry.Point;

public interface InteractionStyleFigure {

	public static int REGION_CONNECTION = 0; 
	public static int REGION_DRAG = 1; 
	
	
	
	public int getIntersectionStyle(Point location);
	
}
