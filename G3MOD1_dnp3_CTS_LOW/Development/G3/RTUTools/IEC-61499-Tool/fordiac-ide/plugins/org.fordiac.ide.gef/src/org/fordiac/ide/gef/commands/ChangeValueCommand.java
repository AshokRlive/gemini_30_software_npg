/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.Messages;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.typelibrary.DataTypeLibrary;
import org.fordiac.ide.util.Utils;

public class ChangeValueCommand extends Command {

	private static final String CHANGE = Messages.ChangeValueCommand_LABEL_ChangeValue;
	private VarDeclaration var;
	private String newValue, oldValue;
	private IEditorPart editor;

	public ChangeValueCommand(VarDeclaration var, String value){
		this.var = var;
		newValue = value;
	}
	
	@Override
	public boolean canExecute(){
		if (var != null && var.getType() != null && var.getType().equals(DataTypeLibrary.getInstance().getType("ANY")) && null != newValue) { //$NON-NLS-1$
			if ((!newValue.equals("")) && (!newValue.contains("#"))){ //$NON-NLS-1$ //$NON-NLS-2$
				Activator.statusLineErrorMessage("Constant Values are not allowed on ANY Input!");
				return false;
			}
		}
		return super.canExecute();
	}

	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());

	}

	public ChangeValueCommand() {
		super(CHANGE);
	}

	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		if(var.getValue() == null){
			var.setValue(LibraryElementFactory.eINSTANCE.createValue());
			oldValue = ""; //$NON-NLS-1$
		}else{
			oldValue = var.getValue().getValue() != null ? var.getValue().getValue() : ""; //$NON-NLS-1$
		}		
		if (newValue == null || newValue.equals("")) { //$NON-NLS-1$
			var.getValue().setValue(null);
		} else {
			var.getValue().setValue(newValue);
		}
	}

	@Override
	public void undo() {
		var.getValue().setValue(oldValue);
	}

	@Override
	public void redo() {
		var.getValue().setValue(newValue);
	}
}
