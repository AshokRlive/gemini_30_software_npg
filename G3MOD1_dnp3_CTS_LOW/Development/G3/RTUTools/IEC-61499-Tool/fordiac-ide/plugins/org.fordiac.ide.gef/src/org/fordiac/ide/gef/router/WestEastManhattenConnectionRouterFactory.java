/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.router;

import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.IFigure;
import org.fordiac.ide.gef.WestEastManhattanConnectionRouter;

/**
 * A factory for creating WestEastManhattenConnectionRouter objects.
 */
public class WestEastManhattenConnectionRouterFactory extends
		AbstractConnectionRouterFactory {

	/**
	 * Instantiates a new west east manhatten connection router factory.
	 */
	public WestEastManhattenConnectionRouterFactory() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.router.IConnectionRouterFactory#getConnectionRouter
	 * (org.eclipse.draw2d.IFigure)
	 */
	@Override
	public ConnectionRouter getConnectionRouter(IFigure container) {
		return new WestEastManhattanConnectionRouter();
	}

}
