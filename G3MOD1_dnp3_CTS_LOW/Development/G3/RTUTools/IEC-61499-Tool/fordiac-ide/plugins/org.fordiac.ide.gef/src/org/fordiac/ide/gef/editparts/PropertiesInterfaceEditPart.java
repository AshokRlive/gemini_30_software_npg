/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.DragTracker;
import org.eclipse.gef.Request;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.PropertyDescriptor;
import org.fordiac.ide.gef.properties.PropertyUtil;
import org.fordiac.ide.model.libraryElement.VarDeclaration;


/** Base class for all interface edit parts which should show properties in one of the properties views 
 * 
 *
 */
abstract public class PropertiesInterfaceEditPart extends InterfaceEditPart {

	@Override
	public void setPropertyValue(final Object id, final Object value) {
		PropertyUtil.setPropertyValue(id, value);
	}

	@Override
	public Object getPropertyValue(final Object id) {
		return PropertyUtil.getPropertyValue(id);
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		if (getCastedModel().getIInterfaceElement() instanceof VarDeclaration) {
			List<VarDeclaration> constants = new ArrayList<VarDeclaration>();
			constants.add((VarDeclaration) getCastedModel().getIInterfaceElement());
			IPropertyDescriptor[] descriptors = new IPropertyDescriptor[constants
					.size() * 2];
			PropertyUtil.addVarDeclDescriptors(constants, descriptors, 0);
			
			
			return descriptors;
		}
		return new PropertyDescriptor[0];
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.EditPart#getDragTracker(Request)
	 */
	@Override
	public DragTracker getDragTracker(Request request){
		return new ConnCreateDirectEditDragTrackerProxy(this);
	}
}
