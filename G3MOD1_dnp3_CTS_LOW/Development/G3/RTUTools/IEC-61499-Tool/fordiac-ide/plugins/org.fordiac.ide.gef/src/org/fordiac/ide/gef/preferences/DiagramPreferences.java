/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.preferences;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Hashtable;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.gef.rulers.RulerProvider;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.router.IConnectionRouterFactory;

/**
 * The Class DiagramPreferences.
 */
public class DiagramPreferences extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	/** The Constant CONNECTION_ROUTER. */
	public static final String CONNECTION_ROUTER = "ConnectionRouter"; //$NON-NLS-1$
	
	/** The Constant SELECTION_COLOR. */
	public static final String SELECTION_COLOR = "SelectionColor"; //$NON-NLS-1$
	
	/** The Constant CORNER_DIM. */
	public static final String CORNER_DIM = "CornerDim"; //$NON-NLS-1$
	
	/** The units the ruler should show */
	public static final String RULER_UNITS  = "RulerUnits";  //$NON-NLS-1$
	
	public static final String GRID_SPACING = "GridSpacing"; //$NON-NLS-1$
	
	public static final String SNAP_TO_GRID = "SnapToGrid"; //$NON-NLS-1$
	
	public static final String SHOW_GRID    = "ShowGrid";    //$NON-NLS-1$
	
	public static final String SHOW_RULERS  = "ShowRulers";  //$NON-NLS-1$

	// START code from
	// org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage
	// 20150617 A Zoitl: replaced gmf ComboFieldEditor with jface ComboFieldEditor
	/**
	 * 
	 * the DoubleFieldEditor class is copied from
	 * org.eclipse.gmf.runtime.diagram.ui
	 * .preferences.RulerGridPreferencePage.DoubleFieldEditor
	 * 
	 */
	private class DoubleFieldEditor extends StringFieldEditor {

		private final double minValidValue = 00.009;
		private final double maxValidValue = 99.999;

		public DoubleFieldEditor(final String pref, final String label,
				final Composite parent) {
			super(pref, label, parent);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.preference.StringFieldEditor#doCheckState()
		 */
		@Override
		protected boolean doCheckState() {
			Text text = getTextControl();

			if (text == null) {
				return false;
			}

			try {
				NumberFormat numberFormatter = NumberFormat.getInstance();
				ParsePosition parsePosition = new ParsePosition(0);
				Number parsedNumber = numberFormatter.parse(text.getText(),
						parsePosition);

				if (parsedNumber == null) {
					showErrorMessage();
					return false;
				}

				Double pageHeight = forceDouble(parsedNumber);
				double number = pageHeight.doubleValue();
				number = convertToBase(number);
				if (number >= minValidValue && number <= maxValidValue
						&& parsePosition.getIndex() == text.getText().length()) {
					clearErrorMessage();
					return true;
				} else {
					showErrorMessage();
					return false;
				}
			} catch (NumberFormatException e1) {
				showErrorMessage();
			}

			return false;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.preference.StringFieldEditor#doLoadDefault()
		 */
		@Override
		protected void doLoadDefault() {
			Text text = getTextControl();
			if (text != null) {
				double value = getPreferenceStore().getDefaultDouble(
						getPreferenceName());
				NumberFormat numberFormatter = NumberFormat.getNumberInstance();
				text.setText(numberFormatter.format(value));
			}
			valueChanged();
		}

		/*
		 * (non-Javadoc) Method declared on FieldEditor.
		 */
		@Override
		protected void doLoad() {
			Text text = getTextControl();
			if (text != null) {
				double value = getPreferenceStore().getDouble(getPreferenceName());
				NumberFormat numberFormatter = NumberFormat.getNumberInstance();
				text.setText(numberFormatter.format(value));
			}
		}

		@Override
		protected void doStore() {
			NumberFormat numberFormatter = NumberFormat.getInstance();
			Double gridWidth;
			try {
				gridWidth = forceDouble(numberFormatter.parse(getTextControl()
						.getText()));
				getPreferenceStore().setValue(getPreferenceName(),
						gridWidth.doubleValue());
			} catch (ParseException e) {
				showErrorMessage();
			}

		}

	}

	private DoubleFieldEditor gridSpacing = null;

	/**
	 * The NumberFormatter.parse() could return a Long or Double We are storing
	 * all values related to the page setup as doubles so we call this function
	 * when ever we are getting values from the dialog.
	 * 
	 * @param number
	 * @return
	 */
	private static Double forceDouble(final Number number) {
		if (!(number instanceof Double)) {
			return new Double(number.doubleValue());
		}
		return (Double) number;
	}

	private int oldUnits = -1;

	// Conversion from inch to centimeter
	private static final double INCH2CM = 2.54;

	/*
	 * 20150617, AZoitl: moved units to a function parameter, changed to use the constants from the Rulerprovider
	 */
	private void updateUnits(int units) {
		switch (units) {
		case RulerProvider.UNIT_INCHES:
			gridSpacing.setLabelText("Grid space in Inches");
			break;

		case RulerProvider.UNIT_CENTIMETERS:
			gridSpacing.setLabelText("Grid space in centimeters");
			break;

		case RulerProvider.UNIT_PIXELS:
			gridSpacing.setLabelText("Grid space in pixel");
			break;
		}

		gridSpacing.setStringValue(convertUnits(oldUnits, units));
		oldUnits = units;

		dblGroup.layout();

	}

	private String convertUnits(final int fromUnits, final int toUnits) {
		String valueStr = gridSpacing.getStringValue();
		if (fromUnits == toUnits) {
			return valueStr;
		}

		// Double value = Double.valueOf( valueStr );
		NumberFormat numberFormatter = NumberFormat.getInstance();
		Double value = new Double(0.125);
		try {
			value = forceDouble(numberFormatter.parse(valueStr));
		} catch (ParseException e) {
			// Use the default
		}
		double pixelValue = 0;

		switch (fromUnits) {
		case RulerProvider.UNIT_INCHES:
			pixelValue = value.doubleValue() * Display.getDefault().getDPI().x;
			break;
		case RulerProvider.UNIT_CENTIMETERS:
			pixelValue = value.doubleValue() * Display.getDefault().getDPI().x
					/ INCH2CM;
			break;
		case RulerProvider.UNIT_PIXELS:
			pixelValue = value.intValue();
		}

		double returnValue = 0;

		switch (toUnits) {
		case RulerProvider.UNIT_INCHES:
			returnValue = pixelValue / Display.getDefault().getDPI().x;
			break;
		case RulerProvider.UNIT_CENTIMETERS:
			returnValue = pixelValue * INCH2CM / Display.getDefault().getDPI().x;
			break;
		case RulerProvider.UNIT_PIXELS:
			returnValue = pixelValue;
		}

		return numberFormatter.format(returnValue);
	}

	/**
	 * converts the current units used to a base unit value to be used (e.g. in
	 * validation)
	 * 
	 * @param number
	 *          Units to be converted to the base unit
	 * 
	 * @return the double
	 */
	private double convertToBase(final double number) {

		double returnValue = 0;
		switch (oldUnits) {
		case RulerProvider.UNIT_INCHES:
			returnValue = number;
			break;
		case RulerProvider.UNIT_CENTIMETERS:
			returnValue = number / INCH2CM;
			break;
		case RulerProvider.UNIT_PIXELS:
			returnValue = number / Display.getDefault().getDPI().x;
		}
		return returnValue;
	}

	private void addGridSpacing(final Composite parent) {

		dblGroup = new Composite(parent, SWT.NONE);

		GridLayout gridLayout = new GridLayout(2, false);

		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalSpan = 2;

		gridSpacing = new DoubleFieldEditor(GRID_SPACING,
				"Grid Spacing", dblGroup);
		gridSpacing.setTextLimit(10);
		addField(gridSpacing);

		int units = getPreferenceStore().getInt(DiagramPreferences.RULER_UNITS);
		oldUnits = units;
		updateUnits(units);

		dblGroup.setLayoutData(gridData);
		dblGroup.setLayout(gridLayout);
	}

	private void addRulerFields(final Composite parent) {

		// Create a Group to hold the ruler fields
		Group group = new Group(parent, SWT.NONE);
		group.setText("Ruler");

		GridLayout gridLayout = new GridLayout(2, false);
		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalSpan = 2;

		// Add the fields to the group
		showRulers = new BooleanFieldEditor(SHOW_RULERS,
				"Show Ruler", group);
		addField(showRulers);

		String unitsArray[][] = new String[3][2];
		unitsArray[0][0] = "Inches";
		unitsArray[0][1] = Integer.toString(RulerProvider.UNIT_INCHES);
		
		unitsArray[1][0] = "cm";
		unitsArray[1][1] = Integer.toString(RulerProvider.UNIT_CENTIMETERS);
		
		unitsArray[2][0] = "pixel";
		unitsArray[2][1] = Integer.toString(RulerProvider.UNIT_PIXELS);

		
		rulerUnits = new ComboFieldEditor(DiagramPreferences.RULER_UNITS, "Unit", unitsArray, group); 
		addField(rulerUnits);

		group.setLayoutData(gridData);
		group.setLayout(gridLayout);
	}

	// END code from
	// org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		super.propertyChange(event);
		if(event.getSource() == rulerUnits){
			int units = Integer.parseInt((String)event.getNewValue());
			updateUnits(units);			
		}
	}

	/**
	 * Instantiates a new diagram preferences.
	 */
	public DiagramPreferences() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("General Diagram Preferences");
	}

	// Ruler Field Editors
	private BooleanFieldEditor showRulers = null;
	private ComboFieldEditor rulerUnits;

	// Grid Field Editors
	private BooleanFieldEditor showGrid = null;
	private BooleanFieldEditor snapToGrid = null;

	private Composite dblGroup = null;

	/* (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	public void createFieldEditors() {

		addRulerFields(getFieldEditorParent());

		// Create a Group to hold the grid fields
		Group group = new Group(getFieldEditorParent(), SWT.NONE);
		group.setText("Grid");

		GridLayout gridLayout = new GridLayout(2, false);

		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalSpan = 2;

		showGrid = new BooleanFieldEditor(SHOW_GRID,
				"Show Grid", group);
		addField(showGrid);

		snapToGrid = new BooleanFieldEditor(SNAP_TO_GRID, "Snap to Grid", group);
		addField(snapToGrid);

		addGridSpacing(group);

		group.setLayoutData(gridData);
		group.setLayout(gridLayout);

		// Create a Group to hold the connection router fields
		Group router = new Group(getFieldEditorParent(), SWT.NONE);
		router.setText("Connection Router");
		GridLayout routerLayout = new GridLayout(2, false);

		Hashtable<String, IConnectionRouterFactory> connectionRouter = new Hashtable<String, IConnectionRouterFactory>();

		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] elems = registry.getConfigurationElementsFor(
				Activator.PLUGIN_ID, "ConnectionRouterProvider"); //$NON-NLS-1$
		for (int i = 0; i < elems.length; i++) {
			IConfigurationElement element = elems[i];
			try {
				Object object = element.createExecutableExtension("class"); //$NON-NLS-1$
				String name = element.getAttribute("name");
				if (object instanceof IConnectionRouterFactory) {
					IConnectionRouterFactory routerFactory = (IConnectionRouterFactory) object;
					connectionRouter.put(name, routerFactory);
				}
			} catch (CoreException corex) {
				Activator.getDefault().logError("Error loading ConnectionRouter", corex);
			}
		}
		
		Set<String> keySet = connectionRouter.keySet();
		String nameArray[][] = new String[keySet.size()][2];
		int i = 0;
		for (String key : keySet) {
			nameArray[i][0] = key;
			nameArray[i][1] = key;
			i++;
		}

		ComboFieldEditor routerEditor = new ComboFieldEditor(CONNECTION_ROUTER,
				"Default Router", nameArray, router);
		addField(routerEditor);

		GridData routerData = new GridData(GridData.FILL_HORIZONTAL);
		routerData.grabExcessHorizontalSpace = true;
		routerData.horizontalSpan = 2;

		router.setLayoutData(routerData);
		router.setLayout(routerLayout);

		// Create a Group to hold the connection router fields
		Group color = new Group(getFieldEditorParent(), SWT.NONE);
		color.setText("Colors");
		GridLayout colorLayout = new GridLayout(2, false);

		ColorFieldEditor selectionBorderColor = new ColorFieldEditor(
				SELECTION_COLOR, "Selection Color", color);

		addField(selectionBorderColor);
		color.setLayout(colorLayout);
		color.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		// Create a Group to specify the arc of the figure edges
		Group arc = new Group(getFieldEditorParent(), SWT.NONE);
		arc.setText("FB");
		GridLayout arcLayout = new GridLayout(2, false);

		IntegerFieldEditor integerFieldEditor = new IntegerFieldEditor(CORNER_DIM,
				"Corner Dimension", arc);
		integerFieldEditor.setValidRange(0, 15);

		addField(integerFieldEditor);
		arc.setLayout(arcLayout);
		arc.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(final IWorkbench workbench) {
	}

}