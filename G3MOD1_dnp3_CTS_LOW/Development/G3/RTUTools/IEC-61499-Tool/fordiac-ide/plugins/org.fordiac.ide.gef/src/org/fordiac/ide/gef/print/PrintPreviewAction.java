/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.print;

import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.ui.parts.GraphicalEditor;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.fordiac.ide.gef.Messages;

/**
 * This class runs the IEC61499PrintDialog.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class PrintPreviewAction extends Action {

	private GraphicalEditor editor;

	private GraphicalViewer viewer;

	/**
	 * Constructor for the IEC61499PrintAction for printing the content of an
	 * ApplicationEditor
	 * 
	 * Adapt isEnabled and run if this Action should work with other editors than
	 * ApplicationEditor or PhysicalViewEditor.
	 */
	public PrintPreviewAction() {
		super();
		setId(ActionFactory.PRINT.getId());
		setText(Messages.PrintPreviewAction_LABEL_Print);
	}

	/**
	 * Instantiates a new prints the preview action.
	 * 
	 * @param viewer the viewer
	 */
	public PrintPreviewAction(GraphicalViewer viewer) {
		super();
		this.viewer = viewer;
		setId(ActionFactory.PRINT.getId());
		setText(Messages.PrintPreviewAction_LABEL_Print);
	}

	/**
	 * returns true if editor is either an applicationEditor or a
	 * PhysicalViewEditor.
	 * 
	 * @return true, if checks if is enabled
	 */
	@Override
	public boolean isEnabled() {
		
		if (viewer != null)  {
			return true;
		}
		
		IWorkbenchWindow window = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();
		if (editor != null
				|| window.getActivePage().getActiveEditor() instanceof GraphicalEditor) {
			return true;
		}
		return false;
	}

	/**
	 * opens the IEC61499PrintDialog.
	 */
	@Override
	public void run() {
		if (viewer == null) {
			IWorkbenchWindow window = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow();
			if (window.getActivePage().getActiveEditor() instanceof GraphicalEditor) {
				editor = (GraphicalEditor) window.getActivePage().getActiveEditor();
			}
			// open a Print Preview
			if (editor != null) {
				viewer = (GraphicalViewer) editor.getAdapter(GraphicalViewer.class);
			}
		}

		if (viewer != null && viewer instanceof GraphicalViewer) {
			Shell shell = new Shell(SWT.RESIZE | SWT.CLOSE | SWT.BORDER);
			// PrintPreview preview = new PrintPreview(Display.getDefault()
			// .getActiveShell(), viewer,
			// Messages.PrintPreviewAction_LABEL_PrintPreview);
			PrintPreview preview = new PrintPreview(shell, viewer,
					Messages.PrintPreviewAction_LABEL_PrintPreview);
			preview.setBlockOnOpen(true);
			preview.open();

		}

	}
}
