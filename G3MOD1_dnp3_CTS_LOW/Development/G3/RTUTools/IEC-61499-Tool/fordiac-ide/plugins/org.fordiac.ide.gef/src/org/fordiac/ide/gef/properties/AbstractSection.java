/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.properties;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.provider.EcoreItemProviderAdapterFactory;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.fordiac.ide.model.Palette.provider.PaletteItemProviderAdapterFactory;
import org.fordiac.ide.model.data.provider.DataItemProviderAdapterFactory;
import org.fordiac.ide.model.libraryElement.provider.LibraryElementItemProviderAdapterFactory;
import org.fordiac.ide.model.ui.provider.UiItemProviderAdapterFactory;

abstract public class AbstractSection extends AbstractPropertySection {
	
	protected Object type;
	protected CommandStack commandStack;
	protected Composite rightComposite;
	protected Composite leftComposite;
	protected boolean createSuperControls = true;
	protected ComposedAdapterFactory adapterFactory;
	
	//block updates triggered by any command
	protected boolean blockRefresh = false;
	
	abstract protected EObject getType();
	abstract protected CommandStack getCommandStack(IWorkbenchPart part, Object input);
	abstract protected Object getInputType(Object input);
	
	protected void setType(Object input){
		type = getInputType(input);
		addContentAdapter();
	}
	
	private final EContentAdapter contentAdapter = new EContentAdapter() {
		@Override
		public void notifyChanged(Notification notification) {
			if(null != getType() && getType().eAdapters().contains(contentAdapter) && !blockRefresh){				
				refresh();
			}
		}
	};
	
	@Override
	public void dispose() {
		removeContentAdapter();
		super.dispose();
	}
	
	protected void removeContentAdapter(){
		if (getType() != null && getType().eAdapters().contains(contentAdapter)) {
			getType().eAdapters().remove(contentAdapter);
		}
	}
	
	protected void addContentAdapter(){
		if(null != getType()  && ! getType().eAdapters().contains(contentAdapter)){
			getType().eAdapters().add(contentAdapter);
		}
	}
	
	public void createControls(final Composite parent, final TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);	
		if(createSuperControls){
			createControls(parent);
		}else{
			parent.setLayout(new GridLayout(1, true));
			parent.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		}
	}
	
	public void createControls(final Composite parent){
		SashForm view = new SashForm(parent, SWT.HORIZONTAL | SWT.SMOOTH);
		view.setLayout(new FillLayout());
		leftComposite = getWidgetFactory().createComposite(view);
		leftComposite.setLayout(new GridLayout());
		rightComposite = getWidgetFactory().createComposite(view);
		rightComposite.setLayout(new GridLayout());	
	}
	
	protected void executeCommand(Command cmd){
		if (type != null && commandStack != null) {
			blockRefresh = true;
			commandStack.execute(cmd);
			blockRefresh = false;
		}
	}
	
	protected Text createGroupText(Composite group, Boolean editable) {			
		Text text = getWidgetFactory().createText(group, "", SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, 0, true, false));
		text.setEditable(editable);	
		text.setEnabled(editable);
		return text;
	}
	
	protected ComposedAdapterFactory getAdapterFactory() {
		if(null == adapterFactory){
			adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
			adapterFactory.addAdapterFactory(new ResourceItemProviderAdapterFactory());
			adapterFactory.addAdapterFactory(new PaletteItemProviderAdapterFactory());
			adapterFactory.addAdapterFactory(new LibraryElementItemProviderAdapterFactory());
			adapterFactory.addAdapterFactory(new UiItemProviderAdapterFactory());
			adapterFactory.addAdapterFactory(new DataItemProviderAdapterFactory());
			adapterFactory.addAdapterFactory(new EcoreItemProviderAdapterFactory());
			adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());
		}		
		return adapterFactory;	
	}
}
