/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.editparts;

import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.SelectionEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gef.tools.DirectEditManager;
import org.fordiac.ide.gef.FixedAnchor;
import org.fordiac.ide.gef.commands.ChangeValueCommand;
import org.fordiac.ide.gef.draw2d.ConnectorBorder;
import org.fordiac.ide.gef.draw2d.SetableAlphaLabel;
import org.fordiac.ide.gef.figures.ToolTipFigure;
import org.fordiac.ide.gef.policies.ValueEditPartChangeEditPolicy;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.ResourceTypeEntry;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.impl.EventImpl;
import org.fordiac.ide.model.libraryElement.impl.VarDeclarationImpl;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UiPackage;
import org.fordiac.ide.preferences.PreferenceConstants;
import org.fordiac.ide.util.Activator;

/**
 * The Class InterfaceEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public abstract class InterfaceEditPart extends AbstractViewEditPart implements
		NodeEditPart, IDeactivatableConnectionHandleRoleEditPart {

	private InterfaceFigure figure;
	private ValueEditPart referencedPart;
	private EContentAdapter contentAdapter;	
	private Palette systemPalette;
	protected int mouseState;
	int topMargin = 0;
	int bottomMargin = 0;
	
	
	public InterfaceEditPart(){
		setConnectable(true);
	}

	public void setTopMargin(int topMargin) {
		this.topMargin = topMargin;
	}

	public void setBottomMargin(int bottomMargin) {
		this.bottomMargin = bottomMargin;
	}
	
	public int getMouseState() {
		return mouseState;
	}

	public void setSystemPalette(Palette systemPalette) {
		this.systemPalette = systemPalette;
	}

	public Palette getSystemPalette() {
		return systemPalette;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		super.deactivate();
		referencedPart = null;
	}

	@Override
	protected void refreshTargetConnections() {
		super.refreshTargetConnections();
		if (getReferencedValueEditPart() != null) {
			getReferencedValueEditPart().refreshValue();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#refreshName()
	 */
	@Override
	public void refreshName() {
		if (getIInterfaceElement() instanceof AdapterDeclaration) {
			getNameLabel().setText(getLabelText());
			
		} else
			getNameLabel().setText(getLabelText());
	}
	
	protected String getLabelText(){
		return getCastedModel().getLabel();
	}
	
	/**
	 * The Class InterfaceFigure.
	 */
	public class InterfaceFigure extends SetableAlphaLabel {

		/**
		 * Instantiates a new interface figure.
		 */
		public InterfaceFigure() {
			super();
			setOpaque(false);
			setText(getLabelText());
			setBorder(new ConnectorBorder(getCastedModel().getIInterfaceElement()));	
			if (isInput()) {
				setLabelAlignment(PositionConstants.LEFT);
				setTextAlignment(PositionConstants.LEFT);
			} else {
				setLabelAlignment(PositionConstants.RIGHT);
				setTextAlignment(PositionConstants.RIGHT);
			}
			setToolTip(new ToolTipFigure(getIInterfaceElement()));
			
			if(isAdapter()){
				//this mouse listener acquires the current mouse state including the modifier keys so that we can use it in 
				//case we are clicking on an adapter with the ctrl key pressed.
				addMouseMotionListener(new MouseMotionListener(){
	
					@Override
					public void mouseDragged(MouseEvent me) {
						mouseState = me.getState();
					}
	
					@Override
					public void mouseEntered(MouseEvent me) {
						mouseState = me.getState();
						
					}
	
					@Override
					public void mouseExited(MouseEvent me) {
						mouseState = me.getState();
					}
	
					@Override
					public void mouseHover(MouseEvent me) {
						mouseState = me.getState();
					}
	
					@Override
					public void mouseMoved(MouseEvent me) {
						mouseState = me.getState();
					}
					
				});
			}
		}
	}

	/**
	 * The Class ConnectorBorder.
	 */

	@Override
	protected IFigure createFigureForModel() {
		if (figure == null) {
			figure = new InterfaceFigure();
		}
		return figure;
	}

	@Override
	protected void refreshVisuals() {
		// nothing to do

	}

	protected abstract GraphicalNodeEditPolicy getNodeEditPolicy();

	/**
	 * Sets the in out connections width.
	 * 
	 * @param with
	 *            the new in out connections width
	 */
	public void setInOutConnectionsWidth(int with) {
		boolean hideData = Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_HIDE_DATA_CON);
		boolean hideEvent = Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_HIDE_EVENT_CON);
		for (@SuppressWarnings("rawtypes")
		Iterator iterator = getSourceConnections().iterator(); iterator
				.hasNext();) {
			ConnectionEditPart cep = (ConnectionEditPart) iterator.next();

			if (cep.getFigure() instanceof PolylineConnection) {
				((PolylineConnection) cep.getFigure()).setLineWidth(with);
				if (cep.getModel() instanceof ConnectionView
						&& ((ConnectionView) cep.getModel())
								.getConnectionElement() instanceof DataConnection) {
					if (with > 1 && hideData) {
						((PolylineConnection) cep.getFigure()).setVisible(true);
					} else if (with < 2) {
						((PolylineConnection) cep.getFigure())
								.setVisible(!hideData);
					}
				}
				if (cep.getModel() instanceof ConnectionView
						&& ((ConnectionView) cep.getModel())
								.getConnectionElement() instanceof EventConnection) {
					if (with > 1 && hideEvent) {
						((PolylineConnection) cep.getFigure()).setVisible(true);
					} else if (with < 2) {
						((PolylineConnection) cep.getFigure())
								.setVisible(!hideEvent);
					}
				}
			}
		}
		for (@SuppressWarnings("rawtypes")
		Iterator iterator = getTargetConnections().iterator(); iterator
				.hasNext();) {
			ConnectionEditPart cep = (ConnectionEditPart) iterator.next();
			if (cep.getFigure() instanceof PolylineConnection) {
				((PolylineConnection) cep.getFigure()).setLineWidth(with);
				if (cep.getModel() instanceof ConnectionView
						&& ((ConnectionView) cep.getModel())
								.getConnectionElement() instanceof DataConnection) {
					if (with > 1 && hideData) {
						((PolylineConnection) cep.getFigure()).setVisible(true);
					} else if (with < 2) {
						((PolylineConnection) cep.getFigure())
								.setVisible(!hideData);
					}
				}
				if (cep.getModel() instanceof ConnectionView
						&& ((ConnectionView) cep.getModel())
								.getConnectionElement() instanceof EventConnection) {
					if (with > 1 && hideEvent) {
						((PolylineConnection) cep.getFigure()).setVisible(true);
					} else if (with < 2) {
						((PolylineConnection) cep.getFigure())
								.setVisible(!hideEvent);
					}
				}
			}
		}
	}

	@Override
	protected void createEditPolicies() {

		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, getNodeEditPolicy());
		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE,
				new SelectionEditPolicy() {

					@Override
					protected void showSelection() {

						ConnectorBorder cb = new ConnectorBorder(getCastedModel().getIInterfaceElement(), false, topMargin, bottomMargin);
						getFigure().setBorder(cb);
						setInOutConnectionsWidth(2);
					}

					@Override
					protected void hideSelection() {
						ConnectorBorder cb = new ConnectorBorder(getCastedModel().getIInterfaceElement(), false, topMargin, bottomMargin);
						getFigure().setBorder(cb);
						setInOutConnectionsWidth(0);
					}
				});

		if (isVariable()) {
			// layoutrole that allows to drop "strings" to an Input Variable
			// which is than used as Parameter
			installEditPolicy(EditPolicy.LAYOUT_ROLE, new LayoutEditPolicy() {

				@Override
				protected Command getMoveChildrenCommand(Request request) {
					return null;
				}

				public Command getCommand(Request request) {
					if (REQ_CREATE.equals(request.getType()))
						return getCreateCommand((CreateRequest) request);

					return null;
				}

				@Override
				protected Command getCreateCommand(CreateRequest request) {
					if ((getHost() instanceof InterfaceEditPart) && (!(request.getNewObjectType() instanceof ResourceTypeEntry))){
						//TODO think of a better check that allows only appropriate request object types
						InterfaceEditPart host = (InterfaceEditPart) getHost();
						if ((host.getIInterfaceElement() instanceof VarDeclaration) && (!(host.getIInterfaceElement() instanceof AdapterDeclaration))) {
							VarDeclaration v = (VarDeclaration) host.getIInterfaceElement();
							return new ChangeValueCommand(v, request != null && request.getNewObject() != null ? request.getNewObject().toString():"");
						}
					}
					return null;
				}

				@Override
				protected EditPolicy createChildEditPolicy(EditPart child) {
					return null;
				}
			});
			
			installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
					new ValueEditPartChangeEditPolicy(){
						@Override
						protected ValueEditPart getValueEditPart() {						
							return getReferencedValueEditPart();
						}					
			});
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.IDeactivatableConnectionHandleRoleEditPart
	 * #setConnectionHandleRoleEnabled(boolean)
	 */
	public void setConnectionHandleRoleEnabled(boolean enabled) {
		//nothing to do
	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public InterfaceElementView getCastedModel() {
		return (InterfaceElementView) getModel();
	}

	/**
	 * Checks if is input.
	 * 
	 * @return true, if is input
	 */
	public boolean isInput() {
		return getCastedModel().getIInterfaceElement().isIsInput();
	}

	/**
	 * Checks if is event.
	 * 
	 * @return true, if is event
	 */
	public boolean isEvent() {
		return getCastedModel().getIInterfaceElement() instanceof EventImpl;
	}
	
	public boolean isAdapter() {
		return getCastedModel().getIInterfaceElement() instanceof AdapterDeclaration;
	}

	/**
	 * Checks if is variable.
	 * 
	 * @return true, if is variable
	 */
	public boolean isVariable() {
		return getCastedModel().getIInterfaceElement() instanceof VarDeclarationImpl;
	}

	private IInterfaceElement getIInterfaceElement() {
		return getCastedModel().getIInterfaceElement();
	}

	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		// super.addChildVisual(childEditPart, index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getContentAdapter()
	 */
	@Override
	public EContentAdapter getContentAdapter() {
		if (contentAdapter == null) {
			contentAdapter = new EContentAdapter() {

				@Override
				public void notifyChanged(final Notification notification) {
					Object feature = notification.getFeature();
					if (UiPackage.eINSTANCE
							.getInterfaceElementView_InConnections().equals(
									feature)
							|| UiPackage.eINSTANCE
									.getInterfaceElementView_OutConnections()
									.equals(feature)) {
						refresh();
					}
					super.notifyChanged(notification);
				}

			};
		}
		return contentAdapter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getINamedElement()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel().getIInterfaceElement();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getNameLabel()
	 */
	@Override
	public Label getNameLabel() {
		return (Label) getFigure();
	}

	//
	// @Override
	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.fordiac.ide.gef.editparts.AbstractViewEditPart#
	 * getPreferenceChangeListener ()
	 */
	@Override
	public org.eclipse.jface.util.IPropertyChangeListener getPreferenceChangeListener() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#getEditableValue()
	 */
	@Override
	public Object getEditableValue() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#getPropertyValue(java
	 * .lang .Object)
	 */
	@Override
	public Object getPropertyValue(final Object id) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#isPropertySet(java.lang
	 * .Object)
	 */
	@Override
	public boolean isPropertySet(final Object id) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#resetPropertyValue(java
	 * .lang.Object)
	 */
	@Override
	public void resetPropertyValue(final Object id) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#setPropertyValue(java
	 * .lang .Object, java.lang.Object)
	 */
	@Override
	public void setPropertyValue(final Object id, final Object value) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.
	 * ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(
			final ConnectionEditPart connection) {
		return new FixedAnchor(getFigure(), isInput(), this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.
	 * Request)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(final Request request) {
		return new FixedAnchor(getFigure(), isInput(), this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.
	 * ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(
			final ConnectionEditPart connection) {
		return new FixedAnchor(getFigure(), isInput());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.
	 * Request)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(final Request request) {
		return new FixedAnchor(getFigure(), isInput());
	}

	@Override
	protected List<?> getModelSourceConnections() {
		return getCastedModel().getOutConnections();
	}

	@Override
	protected List<?> getModelTargetConnections() {
		return getCastedModel().getInConnections();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#setSelected(int)
	 */
	@Override
	public void setSelected(int value) {
		if (value == 0) { // clear possible statusmessage
			Activator.statusLineErrorMessage(null);
		}
		super.setSelected(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#performRequest(org
	 * .eclipse .gef.Request)
	 */
	@Override
	public void performRequest(final Request request) {
		// REQ_DIRECT_EDIT -> first select 0.4 sec pause -> click -> edit
		// REQ_OPEN -> doubleclick
		if (request.getType() == RequestConstants.REQ_MOVE) {
			// TODO: move parent editpart??

		}
		if (request.getType() == RequestConstants.REQ_DIRECT_EDIT
				|| request.getType() == RequestConstants.REQ_OPEN) {
			ValueEditPart part = getReferencedValueEditPart();
			if ((part != null) && (isVariable()) && (!(getIInterfaceElement() instanceof AdapterDeclaration))) {
				part.performDirectEdit();
			}
		} else {
			super.performRequest(request);
		}
	}

	/**
	 * Gets the referenced value edit part.
	 * 
	 * @return the referenced value edit part
	 */
	public ValueEditPart getReferencedValueEditPart() {
		if (referencedPart == null) {
			Object temp; 
			if ((temp = getViewer().getEditPartRegistry().get(getCastedModel().getIInterfaceElement().getValue())) instanceof ValueEditPart) {
				referencedPart = (ValueEditPart)temp;
			}
		}
		return referencedPart;
	}

	@Override
	protected void backgroundColorChanged(IFigure figure) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getInterfaceElements()
	 */
	@Override
	public List<? extends InterfaceElementView> getInterfaceElements() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getManager()
	 */
	@Override
	public DirectEditManager getManager() {
		return super.getManager();
	}
}
