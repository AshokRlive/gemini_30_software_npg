/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.editparts;

/**
 * Interface for an editpart that makes possible that the connectionhandle role
 * can be disabled.
 * 
 * @author gebenh
 */
public interface IDeactivatableConnectionHandleRoleEditPart {

	/**
	 * Specifies whether the connectionhandlerole should be enabled or disabled.
	 * 
	 * @param enabled the enabled
	 */
	public void setConnectionHandleRoleEnabled(boolean enabled);

}
