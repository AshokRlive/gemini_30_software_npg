/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.gef.print;

import java.text.MessageFormat;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PrintFigureOperation;
import org.eclipse.draw2d.PrinterGraphics;
import org.eclipse.draw2d.SWTGraphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editparts.LayerManager;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.Printer;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.gef.Messages;

/**
 * A print preview Dialog where the user can specify some print options. Setting
 * / changing the properties of the printer lead to recalculating the "print"
 * area. Therefore, the preview should represent the output of the printer.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class PrintPreview extends Dialog {

	private Button tile, fitPage, printBorder; // fitWidth, fitHeight,

	private Button pageDown;

	private int selection;

	private Combo combo;

	private PrintMargin margin;

	private Printer printer;

	private Canvas canvas;

	private int page = 0;

	private Label pageCounterLabel;

	/** The display. */
	Display display = PlatformUI.getWorkbench().getDisplay();

	private final GraphicalViewer viewer;

	private final String printName;

	/**
	 * Instantiates a new prints the preview.
	 * 
	 * @param shell the shell
	 * @param viewer the viewer
	 * @param printName the print name
	 */
	public PrintPreview(final Shell shell, final GraphicalViewer viewer,
			final String printName) {
		super(shell);
		this.viewer = viewer;
		this.printName = printName;
	}

	@Override
	protected void configureShell(final Shell newShell) {
		newShell.setText(Messages.PrintPreview_LABEL_PrintPreview);
		super.configureShell(newShell);
	}

	@Override
	protected int getShellStyle() {
		return SWT.RESIZE | SWT.CLOSE | SWT.MAX | SWT.APPLICATION_MODAL;
	}

	/**
	 * insert the required buttons to the given composite.
	 * 
	 * @param composite
	 *          The composite where the buttons (GUI elements) are drawn
	 */
	private void createButtons(final Composite composite) {

		final Button buttonSelectPrinter = new Button(composite, SWT.PUSH);
		buttonSelectPrinter.setText(Messages.PrintPreview_LABEL_PrinterSettings);
		buttonSelectPrinter.addListener(SWT.Selection, new Listener() {
			public void handleEvent(final Event event) {
				PrintDialog dialog = new PrintDialog(getShell());
				// Prompts the printer dialog to let the user select a printer.
				PrinterData printerData = dialog.open();

				if (printerData == null) {
					return;
				}
				// Loads the printer.
				final Printer printer = new Printer(printerData);
				double value = Double.parseDouble(combo.getItem(combo
						.getSelectionIndex()));
				// calculate from cm to inches
				setPrinter(printer, value / 2.54);
			}
		});

		final Button buttonPrint = new Button(composite, SWT.PUSH);
		buttonPrint.setText(Messages.PrintPreview_LABEL_Print);
		buttonPrint.addListener(SWT.Selection, new Listener() {
			public void handleEvent(final Event event) {
				if (printer == null) {
					print();
				} else {
					// print the document
					print(printer, margin);
				}
			}
		});

		pageDown = new Button(composite, SWT.PUSH);
		pageDown.setText(Messages.PrintPreview_LABEL_NextPage);
		pageDown.addListener(SWT.Selection, new Listener() {
			public void handleEvent(final Event event) {
				page++;
				canvas.redraw();
			}
		});

		final Button closeButton = new Button(composite, SWT.PUSH);
		closeButton.setText(Messages.PrintPreview_LABEL_Close);
		closeButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(final Event event) {
				exit();
			}
		});

	}

	/**
	 * Adds some GUI elements for defining some print options to the specified
	 * composite
	 * 
	 * @param composite
	 *          The container of the elements
	 */
	private void createOptionsGUI(final Composite composite) {
		SelectionListener listener = new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {
				selection = getOptionsSelection();
				canvas.redraw();
			}

			public void widgetSelected(SelectionEvent e) {
				selection = getOptionsSelection();
				canvas.redraw();
			}

		};

		tile = new Button(composite, SWT.RADIO);
		tile.setText(Messages.PrintPreview_LABEL_Tile);
		tile.setSelection(true);
		tile.addSelectionListener(listener);

		fitPage = new Button(composite, SWT.RADIO);
		fitPage.setText(Messages.PrintPreview_LABEL_FitPage);
		fitPage.addSelectionListener(listener);

		// fitWidth = new Button(composite, SWT.RADIO);
		// fitWidth.setText("Fit Width");
		// fitWidth.addSelectionListener(listener);
		//
		// fitHeight = new Button(composite, SWT.RADIO);
		// fitHeight.setText("Fit Height");
		// fitHeight.addSelectionListener(listener);

		selection = getOptionsSelection();

		printBorder = new Button(composite, SWT.CHECK);
		printBorder.setText(Messages.PrintPreview_LABEL_PrintBorder);
		printBorder.setSelection(true);
		printBorder.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(final SelectionEvent e) {
				canvas.redraw();
			}

			public void widgetSelected(final SelectionEvent e) {
				canvas.redraw();
			}

		});

		new Label(composite, SWT.NULL).setText(Messages.PrintPreview_LABEL_Margin);
		combo = new Combo(composite, SWT.READ_ONLY);
		combo.add("0.5"); //$NON-NLS-1$
		combo.add("1.0"); //$NON-NLS-1$
		combo.add("1.5"); //$NON-NLS-1$
		combo.add("2.0"); //$NON-NLS-1$
		combo.add("2.5"); //$NON-NLS-1$
		combo.add("3.0"); //$NON-NLS-1$
		combo.select(1);
		combo.addListener(SWT.Selection, new Listener() {
			public void handleEvent(final Event event) {
				double value = Double.parseDouble(combo.getItem(combo
						.getSelectionIndex()));
				// calculate from cm to inches
				setPrinter(printer, value / 2.54);
			}
		});

	}

	/**
	 * Checks which print option (Tile, Fit Page, ...) is selected.
	 * 
	 * @return the PrintFigureOperation
	 */
	private int getOptionsSelection() {
		int returnCode = -1;
		if (tile.getSelection()) {
			returnCode = PrintFigureOperation.TILE;
			pageDown.setEnabled(true);
		} else if (fitPage.getSelection()) {
			returnCode = PrintFigureOperation.FIT_PAGE;
			pageDown.setEnabled(false);
		}
		// else if (fitHeight.getSelection())
		// returnCode = PrintFigureOperation.FIT_HEIGHT;
		// else if (fitWidth.getSelection())
		// returnCode = PrintFigureOperation.FIT_WIDTH;
		return returnCode;

	}

	private void exit() {
		this.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#createContents(org.eclipse.swt.widgets.
	 * Composite)
	 */
	@Override
	protected Control createContents(final Composite parent) {
		parent.setSize(800, 600);

		final Composite composite = (Composite) super.createDialogArea(parent);
		Canvas buttonsCanvas = new Canvas(composite, SWT.NONE);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 10;
		buttonsCanvas.setLayout(gridLayout);
		createButtons(buttonsCanvas);
		createOptionsGUI(buttonsCanvas);
		pageCounterLabel = new Label(buttonsCanvas, SWT.NONE);
		// pageCounterLabel.setSize(100, 10);
		pageCounterLabel.setText("this text can never be seen ..."); //$NON-NLS-1$
		/* the preview */
		canvas = new Canvas(composite, SWT.BORDER);
		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.horizontalSpan = 4;
		canvas.setLayoutData(gridData);

		canvas.addPaintListener(new PaintListener() {
			public void paintControl(final PaintEvent e) {
				int canvasBorder = 20;

				if (printer == null || printer.isDisposed()) {
					return;
				}
				Rectangle rectangle = printer.getBounds();
				Point canvasSize = canvas.getSize();

				double viewScaleFactor = (canvasSize.x - canvasBorder * 2) * 1.0
						/ rectangle.width;
				viewScaleFactor = Math.min(viewScaleFactor,
						(canvasSize.y - canvasBorder * 2) * 1.0 / rectangle.height);

				int offsetX = (canvasSize.x - (int) (viewScaleFactor * rectangle.width)) / 2;
				int offsetY = (canvasSize.y - (int) (viewScaleFactor * rectangle.height)) / 2;

				e.gc.setBackground(composite.getDisplay().getSystemColor(
						SWT.COLOR_WHITE));
				// draws the page layout
				e.gc.fillRectangle(offsetX, offsetY,
						(int) (viewScaleFactor * rectangle.width),
						(int) (viewScaleFactor * rectangle.height));

				// draws the margin.
				e.gc.setLineStyle(SWT.LINE_DASH);
				e.gc.setForeground(composite.getDisplay().getSystemColor(
						SWT.COLOR_BLACK));

				int marginOffsetX = offsetX + (int) (viewScaleFactor * margin.left);
				int marginOffsetY = offsetY + (int) (viewScaleFactor * margin.top);

				Dimension rectSize = new Dimension(
						(int) (viewScaleFactor * (margin.right - margin.left)),
						(int) (viewScaleFactor * (margin.bottom - margin.top)));
				if (printBorder.getSelection()) {
					e.gc.drawRectangle(marginOffsetX, marginOffsetY, rectSize.width,
							rectSize.height);
				}

				LayerManager lm = (LayerManager) viewer.getEditPartRegistry().get(
						LayerManager.ID);
				IFigure figure = lm.getLayer(LayerConstants.PRINTABLE_LAYERS);
				double dpiScale = printer.getDPI().x * 1.0
						/ Display.getCurrent().getDPI().x * 1.0;
				double previewScaleFactor = viewScaleFactor * dpiScale;
				org.eclipse.draw2d.geometry.Rectangle bounds = figure.getBounds();
				double xScale = (double) rectSize.width / bounds.width;
				double yScale = (double) rectSize.height / bounds.height;
				switch (selection) {
				case PrintFigureOperation.TILE:
					int pages = calculateNrOfPages(figure, rectSize, previewScaleFactor);
					int currentPage = (page % pages) + 1;
					pageCounterLabel.setText(MessageFormat.format(
							Messages.PrintPreview_LABLE_PageOfPages, new Object[] {
									currentPage, pages }));
					org.eclipse.draw2d.geometry.Rectangle clipRect = new org.eclipse.draw2d.geometry.Rectangle();
					clipRect.setSize((int) (rectSize.width / previewScaleFactor),
							(int) (rectSize.height / previewScaleFactor));

					org.eclipse.draw2d.geometry.Point p = getClipRectLocationForPage(
							figure, clipRect.getSize(), previewScaleFactor, currentPage);
					if (p == null) {
						return;
					}
					if (figure != null) {
						Graphics g = new SWTGraphics(e.gc);
						g.scale(previewScaleFactor);
						g.translate(-p.x + (int) (marginOffsetX / previewScaleFactor), -p.y
								+ (int) (marginOffsetY / previewScaleFactor));
						clipRect.setLocation(p.x, p.y);
						g.clipRect(clipRect);
						g.setLineStyle(Graphics.LINE_SOLID);
						figure.paint(g);
						g.dispose();

					}
					break;
				case PrintFigureOperation.FIT_PAGE:
					pageCounterLabel.setText("Page " + 1 + " of " + 1); //$NON-NLS-1$ //$NON-NLS-2$
					if (figure != null) {
						Graphics g = new SWTGraphics(e.gc);
						g.setLineStyle(Graphics.LINE_SOLID);
						g.translate(marginOffsetX, marginOffsetY);
						g.scale(Math.min(xScale, yScale));
						figure.paint(g);
					}

					break;
				case PrintFigureOperation.FIT_WIDTH:
					if (figure != null) {
						Graphics g = new SWTGraphics(e.gc);
						g.translate(marginOffsetX, marginOffsetY);
						g.scale(xScale);
						figure.paint(g);
					}
					break;
				case PrintFigureOperation.FIT_HEIGHT:
					if (figure != null) {
						Graphics g = new SWTGraphics(e.gc);
						g.translate(marginOffsetX, marginOffsetY);
						g.scale(yScale);
						figure.paint(g);
					}
					break;

				}

			}

			private int calculateNrOfPages(final IFigure figure,
					final Dimension pageSize, final double previewScaleFactor) {
				int pages = 0;

				pages = (int) Math.round((figure.getSize().width * previewScaleFactor)
						/ pageSize.width + 0.5);
				pages *= (int) Math
						.round((figure.getSize().height * previewScaleFactor)
								/ pageSize.height + 0.5);
				return pages;

			}

			private org.eclipse.draw2d.geometry.Point getClipRectLocationForPage(
					final IFigure figure, final Dimension pageSize,
					final double previewScaleFactor, final int page) {

				org.eclipse.draw2d.geometry.Rectangle bounds = figure.getBounds();
				int x = bounds.x, y = bounds.y;

				int pages = 0;
				while (y < bounds.y + bounds.height) {
					while (x < bounds.x + bounds.width) {
						pages++;
						if (pages == page) {

							return new org.eclipse.draw2d.geometry.Point(x, y);
						}
						x += pageSize.width;
					}
					x = bounds.x;
					y += pageSize.height;
				}
				return null;
			}
		});
		double value = Double.parseDouble(combo.getItem(combo.getSelectionIndex()));
		setPrinter(null, value / 2.54); // calculate from cm to inches

		return composite;

	}

	/**
	 * Lets the user to select a printer and prints the document.
	 */
	void print() {
		PrintDialog dialog = new PrintDialog(getShell());
		// Prompts the printer dialog to let the user select a printer.
		PrinterData printerData = dialog.open();

		if (printerData == null) {
			return;
		}
		// Loads the printer.
		Printer printer = new Printer(printerData);
		print(printer, null);
	}

	/**
	 * Prints the IEC 61499 Application current displayed to the specified
	 * printer.
	 * 
	 * @param printer the printer
	 * @param printMargin the print margin
	 */
	void print(final Printer printer, final PrintMargin printMargin) {
		final PrintMargin margin = (printMargin == null ? PrintMargin
				.getPrintMargin(printer, 1.0) : printMargin);
		if (!printer.startJob(printName)) {
			System.err.println(Messages.PrintPreview_ERROR_StartingPrintJob);
			printer.dispose();
			return;
		}

		GC gc = new GC(printer);

		SWTGraphics g = new SWTGraphics(gc);
		PrinterGraphics printerGraphics = new PrinterGraphics(g, printer);
		Graphics graphics = printerGraphics;

		LayerManager lm = (LayerManager) viewer.getEditPartRegistry().get(
				LayerManager.ID);
		IFigure figure = lm.getLayer(LayerConstants.PRINTABLE_LAYERS);

		graphics.setForegroundColor(figure.getForegroundColor());
		graphics.setBackgroundColor(figure.getBackgroundColor());
		graphics.setFont(figure.getFont());
		double dpiScale = printer.getDPI().x * 1.0
				/ Display.getCurrent().getDPI().x * 1.0;
		org.eclipse.draw2d.geometry.Rectangle bounds = figure.getBounds();
		int x = bounds.x, y = bounds.y;
		switch (selection) {
		case PrintFigureOperation.TILE:
			org.eclipse.draw2d.geometry.Rectangle clipRect = new org.eclipse.draw2d.geometry.Rectangle();
			graphics.scale(dpiScale);
			clipRect.setSize((int) ((margin.right - margin.left) / dpiScale),
					(int) ((margin.bottom - margin.top) / dpiScale));
			while (y < bounds.y + bounds.height) {
				while (x < bounds.x + bounds.width) {
					if (!printer.startPage()) {
						System.err.println(Messages.PrintPreview_ERROR_StartingNewPage);
						return;
					}
					graphics.pushState();
					graphics.translate(-x + (int) (margin.left / dpiScale), -y
							+ (int) (margin.top / dpiScale));
					// graphics.getClip(clipRect);
					clipRect.setLocation(x, y);
					graphics.clipRect(clipRect);
					graphics.setLineStyle(Graphics.LINE_DASH);
					graphics.setForegroundColor(ColorConstants.black);
					if (printBorder.getSelection()) {
						graphics.drawRectangle(x, y,
								(int) ((margin.right - margin.left) / dpiScale),
								(int) ((margin.bottom - margin.top) / dpiScale));
					}
					graphics.setLineStyle(Graphics.LINE_SOLID);
					figure.paint(graphics);

					printer.endPage();
					graphics.popState();
					x += clipRect.width;
				}
				x = bounds.x;
				y += clipRect.height;
			}
			// }
			break;
		case PrintFigureOperation.FIT_PAGE:
			graphics.scale(dpiScale);
			if (!printer.startPage()) {
				System.err.println(Messages.PrintPreview_ERROR_StartingNewPage);
				return;
			}
			graphics.pushState();
			graphics.translate(-x + (int) (margin.left / dpiScale), -y
					+ (int) (margin.top / dpiScale));

			double xScale = ((margin.right - margin.left) / dpiScale) / bounds.width;
			double yScale = ((margin.bottom - margin.top) / dpiScale) / bounds.height;
			graphics.setLineStyle(Graphics.LINE_DASH);
			graphics.setForegroundColor(ColorConstants.black);
			if (printBorder.getSelection()) {
				graphics.drawRectangle(x, y,
						(int) ((margin.right - margin.left) / dpiScale),
						(int) ((margin.bottom - margin.top) / dpiScale));
			}
			graphics.setLineStyle(Graphics.LINE_SOLID);

			graphics.scale(Math.min(xScale, yScale));
			graphics.setLineStyle(Graphics.LINE_SOLID);
			figure.paint(graphics);
			printer.endPage();
			graphics.popState();

		}
		printer.endJob();

		printer.dispose();
		gc.dispose();

		this.close();
	}

	/**
	 * Sets target printer.
	 * 
	 * @param printer the printer
	 * @param marginSize the margin size
	 */
	void setPrinter(Printer printer, final double marginSize) {
		if (printer == null) {
			printer = new Printer(Printer.getDefaultPrinterData());
		}
		this.printer = printer;
		margin = PrintMargin.getPrintMargin(printer, marginSize);
		canvas.redraw();
	}

}

/**
 * Contains margin information (in pixels) for a print job.
 * 
 */
class PrintMargin {
	// Margin to the left side, in pixels
	public int left;

	// Margins to the right side, in pixels
	public int right;

	// Margins to the top side, in pixels
	public int top;

	// Margins to the bottom side, in pixels
	public int bottom;

	private PrintMargin(final int left, final int right, final int top,
			final int bottom) {
		this.left = left;
		this.right = right;
		this.top = top;
		this.bottom = bottom;
	}

	/**
	 * Returns a PrintMargin object containing the true border margins for the
	 * specified printer with the given margin in inches. Note: all four sides
	 * share the same margin width.
	 * 
	 * @param printer
	 * @param margin
	 * @return
	 */
	static PrintMargin getPrintMargin(final Printer printer, final double margin) {
		return getPrintMargin(printer, margin, margin, margin, margin);
	}

	/**
	 * Returns a PrintMargin object containing the true border margins for the
	 * specified printer with the given margin width (in inches) for each side.
	 */
	static PrintMargin getPrintMargin(final Printer printer,
			final double marginLeft, final double marginRight,
			final double marginTop, final double marginBottom) {
		Rectangle clientArea = printer.getClientArea();
		Rectangle trim = printer.computeTrim(0, 0, 0, 0);
		// Rectangle trim = printer.computeTrim(clientArea.x, clientArea.y,
		// clientArea.width, clientArea.height);

		Point dpi = printer.getDPI();

		int leftMargin = (int) (marginLeft * dpi.x) - trim.x;
		int rightMargin = clientArea.width + trim.width
				- (int) (marginRight * dpi.x) - trim.x;
		int topMargin = (int) (marginTop * dpi.y) - trim.y;
		int bottomMargin = clientArea.height + trim.height
				- (int) (marginBottom * dpi.y) - trim.y;

		return new PrintMargin(leftMargin, rightMargin, topMargin, bottomMargin);
	}

	@Override
	public String toString() {
		return "Margin { " + left + ", " + right + "; " + top + ", " + bottom //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				+ " }"; //$NON-NLS-1$
	}
}
