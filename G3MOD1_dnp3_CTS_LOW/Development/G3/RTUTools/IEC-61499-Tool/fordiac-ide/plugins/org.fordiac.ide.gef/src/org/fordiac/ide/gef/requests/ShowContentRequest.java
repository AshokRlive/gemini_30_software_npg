/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.requests;

import org.eclipse.gef.Request;

/**
 * The Class ShowContentRequest.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ShowContentRequest extends Request {

	/** The show content. */
	private boolean showContent;

	/**
	 * Instantiates a new show content request.
	 * 
	 * @param showContent the show content
	 */
	public ShowContentRequest(final boolean showContent) {
		this.showContent = showContent;
	}

	/**
	 * Checks if is show content.
	 * 
	 * @return true, if is show content
	 */
	public boolean isShowContent() {
		return showContent;
	}

	/**
	 * Sets the show content.
	 * 
	 * @param showContent the new show content
	 */
	public void setShowContent(final boolean showContent) {
		this.showContent = showContent;
	}

}
