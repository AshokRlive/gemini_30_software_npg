/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.policies;

import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.fordiac.ide.gef.commands.ShowContentCommand;
import org.fordiac.ide.gef.requests.ShowContentRequest;
import org.fordiac.ide.model.ui.ResourceContainerView;

/**
 * The Class ShowContentEditPolicy.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ShowContentEditPolicy extends AbstractEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#getCommand(org.eclipse.gef.Request)
	 */
	@Override
	public Command getCommand(final Request request) {
		if (request instanceof ShowContentRequest) {
			ShowContentCommand cmd = new ShowContentCommand(
					(ShowContentRequest) request);
			cmd.setContainer((ResourceContainerView) getHost().getModel());
			return cmd;
		}
		return super.getCommand(request);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#understandsRequest(org.eclipse.gef.Request)
	 */
	@Override
	public boolean understandsRequest(final Request req) {
		if (req instanceof ShowContentRequest) {
			return true;
		}
		return super.understandsRequest(req);
	}

}
