/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.ruler;

import org.eclipse.gef.rulers.RulerProvider;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.preferences.DiagramPreferences;

class FordiacRulerProvider extends RulerProvider {
	@Override
	public int getUnit() {
		return Activator.getDefault().getPreferenceStore().getInt(DiagramPreferences.RULER_UNITS);
	}

	@Override
	public Object getRuler() {
		return this;
	}
}