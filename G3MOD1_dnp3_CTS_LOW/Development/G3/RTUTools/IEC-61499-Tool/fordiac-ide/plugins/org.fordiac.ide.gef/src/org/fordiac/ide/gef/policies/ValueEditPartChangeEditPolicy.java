/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.policies;

import org.eclipse.draw2d.Label;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.fordiac.ide.gef.commands.ChangeValueCommand;
import org.fordiac.ide.gef.editparts.ValueEditPart;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

/**
 * DirectEditPolicy for renameing View elements that contain an INamedElement.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ValueEditPartChangeEditPolicy extends DirectEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.DirectEditPolicy#getDirectEditCommand(org.eclipse.gef.requests.DirectEditRequest)
	 */
	@Override
	protected Command getDirectEditCommand(final DirectEditRequest request) {
		ValueEditPart valueEditPart = getValueEditPart();
		if (null != valueEditPart) {
			return new ChangeValueCommand((VarDeclaration) valueEditPart.getCastedModel().eContainer(), (String) request.getCellEditor().getValue());
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.DirectEditPolicy#showCurrentEditValue(org.eclipse.gef.requests.DirectEditRequest)
	 */
	@Override
	protected void showCurrentEditValue(final DirectEditRequest request) {
		String value = (String) request.getCellEditor().getValue();
		ValueEditPart valueEditPart = getValueEditPart();
		if (null != valueEditPart) {
			((Label) valueEditPart.getFigure()).setText(value);
		}
	}
	
	/**Retrieve the correct valueedit part for this policy
	 * Referencing classes like the interfaceedit part may override it 
	 */
	protected ValueEditPart getValueEditPart(){
		ValueEditPart retval = null;
		if (getHost() instanceof ValueEditPart) {
			retval = (ValueEditPart) getHost();
		}
		return retval;
	}
}
