/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.actions;

import org.eclipse.gef.GraphicalViewer;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.gef.Messages;
import org.fordiac.ide.gef.utilities.ImageSaveUtil;
import org.fordiac.ide.util.imageprovider.FordiacImage;

/**
 * Action to save a graphical viewer as image.
 * 
 * @author gebenh
 */
public class SaveImageAction extends Action {
	
	/** the action id. */
	public static final String ID = "org.fordiac.ide.gef.actions.SaveImageAction"; //$NON-NLS-1$

	/**
	 * Action to save a graphical viewer as image.
	 * 
	 * @param viewer the viewer
	 */
	public SaveImageAction(GraphicalViewer viewer) {
		super(LABEL);
		setId(ID);
		this.viewer = viewer;

		setImageDescriptor(FordiacImage.ICON_SaveImage.getImageDescriptor());
		setDisabledImageDescriptor(FordiacImage.ICON_SaveImageDisabled.getImageDescriptor());

		setToolTipText("Save diagram to image file");

	}

	/** The label for the action. */
	public static final String LABEL = Messages.SaveImageAction_LABEL_SaveImage;

	private final GraphicalViewer viewer;

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		IEditorPart editorPart = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		ImageSaveUtil.save(editorPart, viewer);
	}

}
