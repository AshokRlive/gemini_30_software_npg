/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.gef.messages"; //$NON-NLS-1$
	
	/** The Abstract view edit part_ erro r_create figure. */
	public static String AbstractViewEditPart_ERROR_createFigure;
	
	/** The Element edit part factory_ erro r_ cant create part for model element. */
	public static String Abstract4diacEditPartFactory_ERROR_CantCreatePartForModelElement;
	
	public static String Abstract4diacEditPartFactory_LABEL_RUNTIMEException_CantCreateModelForElement;
	
	/** The Appearance property section_ labe l_ background color. */
	public static String AppearancePropertySection_LABEL_BackgroundColor;
	
	/** The Appearance property section_ labe l_ choose color. */
	public static String AppearancePropertySection_LABEL_ChooseColor;
	
	/** The Appearance property section_ labl e_ color. */
	public static String AppearancePropertySection_LABLE_Color;
	
	/** The Change backgroundcolor command_ labe l_ change color. */
	public static String ChangeBackgroundcolorCommand_LABEL_ChangeColor;
	
	/** The Change comment command_ labe l_ change comment. */
	public static String ChangeCommentCommand_LABEL_ChangeComment;
	
	/** The Change value command_ labe l_ change value. */
	public static String ChangeValueCommand_LABEL_ChangeValue;
	
	/** The Print preview_ erro r_ starting new page. */
	public static String PrintPreview_ERROR_StartingNewPage;
	
	/** The Print preview_ erro r_ starting print job. */
	public static String PrintPreview_ERROR_StartingPrintJob;
	
	/** The Print preview_ labe l_ close. */
	public static String PrintPreview_LABEL_Close;
	
	/** The Print preview_ labe l_ fit page. */
	public static String PrintPreview_LABEL_FitPage;
	
	/** The Print preview_ labe l_ margin. */
	public static String PrintPreview_LABEL_Margin;
	
	/** The Print preview_ labe l_ next page. */
	public static String PrintPreview_LABEL_NextPage;
	
	/** The Print preview_ labe l_ print. */
	public static String PrintPreview_LABEL_Print;
	
	/** The Print preview_ labe l_ print border. */
	public static String PrintPreview_LABEL_PrintBorder;
	
	/** The Print preview_ labe l_ printer settings. */
	public static String PrintPreview_LABEL_PrinterSettings;
	
	/** The Print preview_ labe l_ print preview. */
	public static String PrintPreview_LABEL_PrintPreview;
	
	/** The Print preview_ labe l_ tile. */
	public static String PrintPreview_LABEL_Tile;
	
	/** The Print preview_ labl e_ page of pages. */
	public static String PrintPreview_LABLE_PageOfPages;
	
	/** The Print preview action_ labe l_ print. */
	public static String PrintPreviewAction_LABEL_Print;
	
	/** The Print preview action_ labe l_ print preview. */
	public static String PrintPreviewAction_LABEL_PrintPreview;
	
	/** The Save image action_ labe l_ save image. */
	public static String SaveImageAction_LABEL_SaveImage;
	
	/** The View rename command_ labe l_ rename view. */
	public static String ViewRenameCommand_LABEL_RenameView;
	
	/** The View set position command_ labe l_ move. */
	public static String ViewSetPositionCommand_LABEL_Move;
	
	/** The Property util_ labe l_ description_ complianceprofile. */
	public static String PropertyUtil_LABEL_Description_Complianceprofile;
	
	/** The Property util_ labe l_ instancename. */
	public static String PropertyUtil_LABEL_Instancename;
	
	/** The Property util_ labe l_ instancecomment. */
	public static String PropertyUtil_LABEL_Instancecomment;
	
	/** The Set profile command_ labe l_ set profile. */
	public static String SetProfileCommand_LABEL_SetProfile;

	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
		// private empty constructor
	}
}
