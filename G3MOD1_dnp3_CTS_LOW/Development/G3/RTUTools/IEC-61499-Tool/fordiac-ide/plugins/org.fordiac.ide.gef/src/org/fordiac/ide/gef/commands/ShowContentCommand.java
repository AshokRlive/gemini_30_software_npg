/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.gef.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.gef.requests.ShowContentRequest;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.util.Utils;

/**
 * The Class ShowContentCommand.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ShowContentCommand extends Command {

	/** The request. */
	private final ShowContentRequest request;

	/** The container. */
	private ResourceContainerView container;

	/** The old state. */
	private boolean oldState;

	/** The editor. */
	private IEditorPart editor;

	/**
	 * Instantiates a new show content command.
	 * 
	 * @param request the request
	 */
	public ShowContentCommand(final ShowContentRequest request) {
		this.request = request;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		oldState = container.isShowContent();
		container.setShowContent(request.isShowContent());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		container.setShowContent(oldState);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		container.setShowContent(request.isShowContent());
	}

	/**
	 * Gets the container.
	 * 
	 * @return the container
	 */
	public ResourceContainerView getContainer() {
		return container;
	}

	/**
	 * Sets the container.
	 * 
	 * @param container the new container
	 */
	public void setContainer(final ResourceContainerView container) {
		this.container = container;
	}

}
