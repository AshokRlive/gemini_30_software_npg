/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.router;

import org.eclipse.gef.EditPolicy;

public interface BendpointPolicyRouter {
	/**
	 * 
	 * @return a bendpoint editpolicy suitable for the router returned in
	 *         getConnectionRouter
	 */
	public EditPolicy getBendpointPolicy(final Object modelObject);
}
