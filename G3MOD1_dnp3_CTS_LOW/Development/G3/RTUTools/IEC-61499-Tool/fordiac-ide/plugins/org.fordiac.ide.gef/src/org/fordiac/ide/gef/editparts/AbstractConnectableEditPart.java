/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.editparts;

import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.fordiac.ide.gef.figures.InteractionStyleFigure;
import org.fordiac.ide.gef.policies.ConnectionMovementHighlightEditPolicy;

public abstract class AbstractConnectableEditPart extends AbstractGraphicalEditPart {

	boolean connectable = false;

	public boolean isConnectable() {
		return connectable;
	}

	public void setConnectable(boolean connectable) {
		this.connectable = connectable;
	}

	protected ConnectionMovementHighlightEditPolicy cmPolicy;

	@Override
	protected void createEditPolicies() {
		if (isConnectable()) {
			cmPolicy = new ConnectionMovementHighlightEditPolicy();
			installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, cmPolicy);
		}
	}

	@Override
	public DragTracker getDragTracker(Request request) {
		if (useConnectionTool()) {
			return new ConnCreateDirectEditDragTrackerProxy(this); 
		} else {
			return super.getDragTracker(request);
		}

	}

	protected boolean useConnectionTool() {
		if (!isConnectable() || cmPolicy == null) {
			return false;
		}
		return cmPolicy.getCurrentInteractionStyle() == InteractionStyleFigure.REGION_CONNECTION;
	}

}
