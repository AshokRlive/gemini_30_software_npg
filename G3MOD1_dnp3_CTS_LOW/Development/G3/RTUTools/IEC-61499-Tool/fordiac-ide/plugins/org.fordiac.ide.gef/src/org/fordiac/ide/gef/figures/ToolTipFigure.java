/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.figures;

import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.ToolbarLayout;
import org.fordiac.ide.gef.draw2d.AdvancedLineBorder;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.With;

/**
 * The Class ToolTipFigure.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ToolTipFigure extends Figure {

	class CompartmentFigure extends Figure {

		public CompartmentFigure() {
			ToolbarLayout layout = new ToolbarLayout();
			layout.setMinorAlignment(ToolbarLayout.ALIGN_TOPLEFT);
			layout.setStretchMinorAxis(false);
			layout.setSpacing(2);
			setLayoutManager(layout);
			setBorder(new AdvancedLineBorder(PositionConstants.NORTH));
		}

	}

	/**
	 * Instantiates a new tool tip figure.
	 * 
	 * @param element
	 *            the element
	 */
	public ToolTipFigure(final INamedElement element) {

		ToolbarLayout mainLayout = new ToolbarLayout(false);
		setLayoutManager(mainLayout);
		mainLayout.setStretchMinorAxis(true);

		String nameLine = new String(element.getName());
		String comment = "";
		String withText = "With: ";

		if (element instanceof VarDeclaration) {
			if (((VarDeclaration) element).getType() != null) {
				nameLine += " - "
						+ ((VarDeclaration) element).getType().getName();
			}
			comment += element.getComment();
		}

		if (element instanceof Event) {
			comment += element.getComment();
			List<With> withs = ((Event) element).getWith();
			boolean first = true;
			withText += "[";
			for (With with : withs) {
				if (first) {
					first = false;
				} else {
					withText += ", ";
				}
				if (with != null && with.getVariables() != null)
					withText += with.getVariables().getName();
			}
			withText += "]";

		}

		add(new Label(nameLine));

		CompartmentFigure lines = new CompartmentFigure();
		add(lines);

		if (comment != null) {
			if (!comment.isEmpty()) {
				lines.add(new Label(comment));
			}
		}
		if (withText != null && element instanceof Event) {
			lines.add(new Label(withText));
		}
	}

}
