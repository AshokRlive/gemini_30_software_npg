/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.gef.commands;

import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.libraryElement.FB;

public class FBRenameCommand extends ChangeNameCommand {
	public FBRenameCommand(FB fb, String newName){
		super(fb, newName);
	}

	@Override
	public void execute() {
		// remove the old name from the repository
		NameRepository.getInstance().removeInstanceName((FB)element);
		super.execute();
		// add the new one
		NameRepository.getInstance().addSystemUniqueFBInstanceName((FB)element);
	}

	@Override
	public void undo() {
		// remove the old name from the repository
		NameRepository.getInstance().removeInstanceName((FB)element);
		super.undo();
		// add the new one
		NameRepository.getInstance().addSystemUniqueFBInstanceName((FB)element);
	}

	@Override
	public void redo() {
		// remove the old name from the repository
		NameRepository.getInstance().removeInstanceName((FB)element);
		super.redo();
		// add the new one
		NameRepository.getInstance().addSystemUniqueFBInstanceName((FB)element);
	}
}

