package org.fordiac.ide.gef.dnd;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.Request;
import org.eclipse.gef.dnd.AbstractTransferDropTargetListener;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.swt.dnd.DND;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;

public class ParameterDropTargetListener extends
		AbstractTransferDropTargetListener {

	private ParameterValueFactory factory = new ParameterValueFactory();

	public ParameterDropTargetListener(EditPartViewer viewer) {
		super(viewer, ParameterValueTemplateTransfer.getInstance());
	}

	// public ParameterDropTargetListener(EditPartViewer viewer) {
	// super(viewer, FileTransfer.getInstance());
	// }

	protected Request createTargetRequest() {
		CreateRequest request = new CreateRequest();
		request.setFactory(factory);
		if (getCurrentEvent().data != null) {
			factory.setText(getCurrentEvent().data.toString());
		}
		return request;
	}

	protected void updateTargetRequest() {
		((CreateRequest) getTargetRequest()).setLocation(getDropLocation());
	}

	protected void handleDragOver() {
		EditPart ep = getTargetEditPart();
		if (ep instanceof InterfaceEditPart) {
			if (((InterfaceEditPart) ep).isInput()
					&& !((InterfaceEditPart) ep).isEvent())
				getCurrentEvent().detail = DND.DROP_COPY;
		} else {
			getCurrentEvent().detail = DND.DROP_NONE;
		}
		super.handleDragOver();
	}

}