/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * The Class VerticalLineFigure.
 */
public class VerticalLineFigure extends Figure {

	/**
	 * Instantiates a new horizontal line figure.
	 * 
	 * @param height the height
	 */
	public VerticalLineFigure(final int height) {
		setMaximumSize(new Dimension(1, height));
		setMinimumSize(new Dimension(1, height));
		setPreferredSize(1, height);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
	 */
	@Override
	public void paintFigure(final Graphics g) {
		Rectangle r = getBounds().getCopy();
		// r.y = r.y + r.height / 2;
		g.setLineWidth(2);
		g.drawLine(r.getTopLeft(), r.getBottomLeft());
	}
}
