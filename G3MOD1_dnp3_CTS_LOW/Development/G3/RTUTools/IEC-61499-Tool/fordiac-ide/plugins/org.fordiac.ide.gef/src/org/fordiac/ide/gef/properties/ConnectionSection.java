/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.properties;

import org.eclipse.core.runtime.Assert;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.util.commands.ChangeCommentCommand;

public abstract class ConnectionSection extends AbstractSection {
	private Text commentText;
	private Text sourceText;
	private Text targetText;
	
	protected Connection getType(){
		return (Connection) type;
	}
	
	@Override
	public void createControls(final Composite parent, final TabbedPropertySheetPage tabbedPropertySheetPage) {
		createSuperControls = false;
		super.createControls(parent, tabbedPropertySheetPage);		
		Composite composite = getWidgetFactory().createComposite(parent);
		composite.setLayout(new GridLayout(2, false));
		composite.setLayoutData(new GridData(SWT.FILL, 0, true, false));
		getWidgetFactory().createCLabel(composite, "Source:"); 
		sourceText = createGroupText(composite, false);
		getWidgetFactory().createCLabel(composite, "Traget:"); 
		targetText = createGroupText(composite, false);	
		getWidgetFactory().createCLabel(composite, "Comment:"); 
		commentText = createGroupText(composite, true);
		commentText.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				removeContentAdapter();
				executeCommand(new ChangeCommentCommand(getType(), commentText.getText()));
				addContentAdapter();
			}
		});
	}
	
	@Override
	public void setInput(final IWorkbenchPart part, final ISelection selection) {
		Assert.isTrue(selection instanceof IStructuredSelection);
		Object input = ((IStructuredSelection) selection).getFirstElement();
		commandStack = getCommandStack(part, input);
		if(null == commandStack){ //disable all fields
			commentText.setEnabled(false);
			sourceText.setEnabled(false);
			targetText.setEnabled(false);
		}
		setType(input);
	}	

	@Override
	public void refresh() {
		CommandStack commandStackBuffer = commandStack;
		commandStack = null;		
		if(null != type) {
			commentText.setText(getType().getComment() != null ? getType().getComment() : "");
			if(null != getType().getIInterfaceElementSource()){
				sourceText.setText(getFBNameFromIInterfaceElement(getType().getIInterfaceElementSource()) + "." + getType().getIInterfaceElementSource().getName());
			}
			if(null != getType().getIInterfaceElementDestination()){
				targetText.setText(getFBNameFromIInterfaceElement(getType().getIInterfaceElementDestination()) + "." + getType().getIInterfaceElementDestination().getName());
			}
		} 
		commandStack = commandStackBuffer;
	}
	
	private String getFBNameFromIInterfaceElement(IInterfaceElement element){
		return element.eContainer().eContainer() instanceof FB ? ((FB)element.eContainer().eContainer()).getName() : "";
	}
}
