package org.fordiac.ide.gef;

import java.util.ArrayList;
import java.util.Iterator;

import org.fordiac.ide.gef.editparts.IChildrenProvider;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.View;

public class ConstantValuesProvider implements IChildrenProvider {

	public ConstantValuesProvider() {
	}

	@Override
	public ArrayList<IEditPartCreator> getChildren(Diagram diagram) {
		ArrayList<IEditPartCreator> valueElenents = new ArrayList<IEditPartCreator>();
		for (View view : diagram.getChildren()) {
			if (view instanceof FBView) {
				for (Iterator<InterfaceElementView> iterator = ((FBView)view).getInterfaceElements()
						.iterator(); iterator.hasNext();) {
					InterfaceElementView interfaceElementView = iterator
							.next();
					IInterfaceElement interfaceElement = interfaceElementView
							.getIInterfaceElement();
					if (null != interfaceElement && interfaceElement.isIsInput() && interfaceElement.getValue() != null) {
						valueElenents.add(interfaceElement.getValue());
					}
				}

			}
		}
		return valueElenents;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
