/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.router;

import org.eclipse.draw2d.PolylineConnection;

abstract public class AbstractConnectionRouterFactory implements IConnectionRouterFactory {

	@Override
	public PolylineConnection createConnectionFigure() {
		return new PolylineConnection();
	}

}
