package org.fordiac.ide.gef.editparts;

import java.util.HashMap;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.fordiac.ide.gef.IEditPartCreatorFactory;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.libraryElement.impl.ValueImpl;

public class ValueEditPartCreationFactory implements IEditPartCreatorFactory {

	public ValueEditPartCreationFactory() {
	}


	HashMap<String, EditPart> editParts = new HashMap<String, EditPart>();
	
	@Override
	public boolean supportsElement(Class<?> type) {
		return Value.class.equals(type) || ValueImpl.class.equals(type);
	}

	@Override
	public EditPart getEditpart(String diagram, EObject element) {
		if (!editParts.containsKey(diagram)) {
			ValueEditPart vep = new ValueEditPart();
			vep.setModel(element);
			editParts.put(diagram, vep);
		}
		return editParts.get(diagram);
	}

}
