/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.properties;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.runtime.Assert;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.fordiac.ide.gef.Messages;
import org.fordiac.ide.gef.commands.ChangeBackgroundcolorCommand;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.Utils;

/**
 * The Class AppearancePropertySection.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class AppearancePropertySection extends AbstractPropertySection {

	/** The abstract view edit part. */
	private AbstractViewEditPart abstractViewEditPart;

	/** The view. */
	private View view;

	private final ArrayList<View> selectedViews = new ArrayList<View>();

	/** The color. */
	private Color color;

	/** The color label. */
	protected Label colorLabel;

	protected Button chooseColorBtn;

	protected Group colorsGroup;

	/**
	 * Instantiates a new appearance property section.
	 */
	public AppearancePropertySection() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#createControls
	 * (org.eclipse.swt.widgets.Composite,
	 * org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage)
	 */
	@Override
	public void createControls(final Composite parent,
			final TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);
		Composite composite = getWidgetFactory().createFlatFormComposite(parent);
		FormLayout layout = (FormLayout) composite.getLayout();
		layout.spacing = 3;
		initializeControls(composite);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#setInput
	 * (org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void setInput(final IWorkbenchPart part, final ISelection selection) {
		super.setInput(part, selection);
		Assert.isTrue(selection instanceof IStructuredSelection);
		Object input = ((IStructuredSelection) selection).getFirstElement();
		Assert.isTrue(input instanceof AbstractViewEditPart);
		this.abstractViewEditPart = (AbstractViewEditPart) input;
		if (abstractViewEditPart.getModel() instanceof View) {
			view = (View) abstractViewEditPart.getModel();
		}
		selectedViews.clear();
		for (Iterator iterator = ((IStructuredSelection) selection).iterator(); iterator
				.hasNext();) {
			Object object = iterator.next();
			if (object instanceof AbstractViewEditPart
					&& ((AbstractViewEditPart) object).getModel() instanceof View) {
				selectedViews.add((View) ((AbstractViewEditPart) object).getModel());
			}
		}
		if (selectedViews.size() <= 1) {
			selectedViews.clear();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#refresh()
	 */
	@Override
	public void refresh() {
		if (view != null && view.getBackgroundColor() != null) {
			org.fordiac.ide.model.ui.Color color = view.getBackgroundColor();
			this.color.dispose();
			this.color = new Color(null, new RGB(color.getRed(), color.getGreen(),
					color.getBlue()));
			colorLabel.setBackground(this.color);
		} else {
			this.color.dispose();
			this.color = new Color(null, new RGB(255, 255, 255));
			colorLabel.setBackground(this.color);

		}

		super.refresh();
	}

	/**
	 * Initialize controls.
	 * 
	 * @param parent
	 *          the parent
	 */
	protected void initializeControls(final Composite parent) {
		createColorsGroup(parent);
	}

	/**
	 * Create fonts and colors group.
	 * 
	 * @param parent
	 *          - parent composite
	 */
	protected void createColorsGroup(final Composite parent) {
		colorsGroup = getWidgetFactory().createGroup(parent,
				Messages.AppearancePropertySection_LABLE_Color);
		
		
		GridLayout layout = new GridLayout(1, false);
		colorsGroup.setLayout(layout);

		
		
		// Start with Celtics green
		color = new Color(null, new RGB(255, 255, 255));
		// Use a label full of spaces to show the color
		colorLabel = getWidgetFactory().createLabel(colorsGroup, ""); //$NON-NLS-1$
		colorLabel.setText("          "); //$NON-NLS-1$
		colorLabel.setBackground(color);
		GridData gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		colorLabel.setLayoutData(gd);

		chooseColorBtn = new Button(colorsGroup, SWT.PUSH);
		chooseColorBtn.setText(Messages.AppearancePropertySection_LABEL_BackgroundColor);
		chooseColorBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent event) {
				// Create the color-change dialog
				
				ColorDialog dlg = new ColorDialog(colorsGroup.getShell());

				// Set the selected color in the dialog from
				// user's selected color
				dlg.setRGB(colorLabel.getBackground().getRGB());

				// Change the title bar text
				dlg.setText(Messages.AppearancePropertySection_LABEL_ChooseColor);

				// Open the dialog and retrieve the selected color
				RGB rgb = dlg.open();
				if (rgb != null) {
					// Dispose the old color, create the
					// new one, and set into the label
					Command cmd;
					if (selectedViews.size() > 0) {
						cmd = new CompoundCommand("Change Background Color");
						for (Iterator<View> iterator = selectedViews.iterator(); iterator
								.hasNext();) {
							View selView = (View) iterator.next();
							ChangeBackgroundcolorCommand tmp = new ChangeBackgroundcolorCommand(
									selView, rgb);
							((CompoundCommand) cmd).add(tmp);
						}
					} else {
						cmd = new ChangeBackgroundcolorCommand(view, rgb);
					}

					if (cmd.canExecute()) {
						Object viewer = Utils.getCurrentActiveEditor().getAdapter(
								GraphicalViewer.class);
						if (viewer instanceof GraphicalViewer) {
							((GraphicalViewer) viewer).getEditDomain().getCommandStack()
									.execute(cmd);
						} else {
							cmd.execute();
						}
						color.dispose();
						color = new Color(null, rgb);
						colorLabel.setBackground(color);

					}
				}
			}
		});

	}

}
