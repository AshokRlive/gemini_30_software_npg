/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.utilities;

import org.eclipse.core.runtime.Assert;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.SWTGraphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editparts.LayerManager;
import org.eclipse.gef.editparts.SimpleRootEditPart;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IEditorPart;

/**
 * Save format should be any of ... SWT.IMAGE_BMP, SWT.IMAGE_JPEG,
 * SWT.IMAGE_GIF, SWT.IMAGE_TIFF, SWT.IMAGE_PNG
 */
public class ImageSaveUtil {

	/**
	 * Save.
	 * 
	 * @param editorPart the editor part
	 * @param viewer the viewer
	 * @param saveFilePath the save file path
	 * @param format the format
	 * 
	 * @return true, if successful
	 */
	public static boolean save(IEditorPart editorPart, GraphicalViewer viewer,
			String saveFilePath, int format) {
		Assert.isNotNull(editorPart,
				"null editorPart passed to ImageSaveUtil::save");
		Assert.isNotNull(viewer, "null viewer passed to ImageSaveUtil::save");
		Assert.isNotNull(saveFilePath,
				"null saveFilePath passed to ImageSaveUtil::save");

		if (format != SWT.IMAGE_BMP && format != SWT.IMAGE_JPEG
				&& format != SWT.IMAGE_PNG) {
			throw new IllegalArgumentException("Save format not supported");
		}

		try {
			saveEditorContentsAsImage(editorPart, viewer, saveFilePath, format);
		} catch (Exception ex) {
			MessageDialog.openError(editorPart.getEditorSite().getShell(),
					"Save Error", "Could not save editor contents");
			return false;
		}

		return true;
	}

	/**
	 * Save.
	 * 
	 * @param editorPart the editor part
	 * @param viewer the viewer
	 * 
	 * @return true, if successful
	 */
	public static boolean save(IEditorPart editorPart, GraphicalViewer viewer) {
		// Assert.isNotNull(editorPart,
		// "null editorPart passed to ImageSaveUtil::save");
		Assert.isNotNull(viewer, "null viewer passed to ImageSaveUtil::save");

		String saveFilePath = getSaveFilePath(editorPart, viewer, -1);
		if (saveFilePath == null) {
			return false;
		}

		int format = SWT.IMAGE_JPEG;
		if (saveFilePath.endsWith(".jpg")) {
			format = SWT.IMAGE_JPEG;
		} else if (saveFilePath.endsWith(".bmp")) {
			format = SWT.IMAGE_BMP;
		} else if (saveFilePath.endsWith(".png")) {
			format = SWT.IMAGE_PNG;
		}

		return save(editorPart, viewer, saveFilePath, format);
	}

	private static String getSaveFilePath(IEditorPart editorPart,
			GraphicalViewer viewer, int format) {
		FileDialog fileDialog = new FileDialog(editorPart.getEditorSite()
				.getShell(), SWT.SAVE);

		String[] filterExtensions = new String[] { "*.jpg", "*.bmp", "*.png" };
		if (format == SWT.IMAGE_BMP) {
			filterExtensions = new String[] { "*.bmp" };
		} else if (format == SWT.IMAGE_JPEG) {
			filterExtensions = new String[] { "*.jpg" };
		} else if (format == SWT.IMAGE_PNG) {
			filterExtensions = new String[] { "*.png" };
		}
		fileDialog.setFilterExtensions(filterExtensions);

		return fileDialog.open();
	}

	private static void saveEditorContentsAsImage(IEditorPart editorPart,
			GraphicalViewer viewer, String saveFilePath, int format) {
		/*
		 * 1. First get the figure whose visuals we want to save as image. So we
		 * would like to save the rooteditpart which actually hosts all the
		 * printable layers.
		 * 
		 * NOTE: ScalableRootEditPart manages layers and is registered
		 * graphicalviewer's editpartregistry with the key LayerManager.ID ...
		 * well that is because ScalableRootEditPart manages all layers that are
		 * hosted on a FigureCanvas. Many layers exist for doing different
		 * things
		 */
		SimpleRootEditPart rootEditPart = (SimpleRootEditPart) viewer
				.getEditPartRegistry().get(LayerManager.ID);
		IFigure rootFigure = ((LayerManager) rootEditPart)
				.getLayer(LayerConstants.PRINTABLE_LAYERS);
		
		
		// calcualate the minimum bounds
        Rectangle minimumBounds = calculateMinimumBounds(rootFigure);
        Image img;
        if (minimumBounds == null)
        {
            img = new Image(Display.getDefault(), 10, 10);
        }
        else
        {
            img = new Image(Display.getDefault(), minimumBounds.width, minimumBounds.height);
        }
        GC imageGC = new GC(img);

        SWTGraphics imgGraphics = new SWTGraphics(imageGC);
        if (minimumBounds != null)
        {
            // Reset origin to make it the top/left most part of the diagram
        	imgGraphics.translate(minimumBounds.x * -1, minimumBounds.y * -1);
            //Utils.paintDiagram(swtGraphics, figure);
        }
		
		

		/*
		 * 2. Now we want to get the GC associated with the control on which all
		 * figures are painted by SWTGraphics. For that first get the SWT
		 * Control associated with the viewer on which the rooteditpart is set
		 * as contents
		 */
		Control figureCanvas = viewer.getControl();
		GC figureCanvasGC = new GC(figureCanvas);

		/*
		 * 3. Create a new Graphics for an Image onto which we want to paint
		 * rootFigure
		 */
		imageGC.setBackground(figureCanvasGC.getBackground());
		imageGC.setForeground(figureCanvasGC.getForeground());
		imageGC.setFont(figureCanvasGC.getFont());
		imageGC.setLineStyle(figureCanvasGC.getLineStyle());
		imageGC.setLineWidth(figureCanvasGC.getLineWidth());

		// TODO check whether this method is required
		// imageGC.setXORMode(figureCanvasGC.getXORMode());

		//Graphics imgGraphics = new SWTGraphics(imageGC);

		/*
		 * 4. Draw rootFigure onto image. After that image will be ready for
		 * save
		 */
		rootFigure.paint(imgGraphics);

		/* 5. Save image */
		ImageData[] imgData = new ImageData[1];
		imgData[0] = img.getImageData();

		ImageLoader imgLoader = new ImageLoader();
		imgLoader.data = imgData;
		imgLoader.save(saveFilePath, format);

		/* release OS resources */
		figureCanvasGC.dispose();
		imageGC.dispose();
		img.dispose();
	}
	
	public static Rectangle calculateMinimumBounds(IFigure rootFigure)
    {
        Rectangle minimumBounds = null;
        for (Object layer : rootFigure.getChildren())
        {
            Rectangle bounds;
            if (layer instanceof FreeformLayer)
            {
                bounds = calculateMinimumBounds((IFigure) layer);
            }
            else
            {
                bounds = ((IFigure) layer).getBounds();
            }
            if (minimumBounds == null)
            {
                minimumBounds = new Rectangle(bounds);
            }
            else
            {
                minimumBounds.union(bounds);
            }
        }
        if (minimumBounds != null)
        {
            minimumBounds.expand(10, 10);
        }

        return minimumBounds;
    }
}
