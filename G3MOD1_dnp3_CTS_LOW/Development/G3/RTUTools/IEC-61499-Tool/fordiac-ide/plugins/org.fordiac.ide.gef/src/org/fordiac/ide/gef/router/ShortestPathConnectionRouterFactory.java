/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.router;

import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ShortestPathConnectionRouter;

/**
 * A factory for creating ShortestPathConnectionRouter objects.
 */
public class ShortestPathConnectionRouterFactory extends AbstractConnectionRouterFactory {

	/**
	 * Instantiates a new ShortestPath connection router factory.
	 */
	public ShortestPathConnectionRouterFactory() {
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.router.IConnectionRouterFactory#getConnectionRouter(org.eclipse.draw2d.IFigure)
	 */
	@Override
	public ConnectionRouter getConnectionRouter(IFigure container) {
		return new ShortestPathConnectionRouter(container);
	}

}