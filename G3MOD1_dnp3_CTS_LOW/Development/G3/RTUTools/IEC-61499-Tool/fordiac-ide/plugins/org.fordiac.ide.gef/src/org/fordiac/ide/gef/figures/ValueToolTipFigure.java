/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.figures;

import org.eclipse.draw2d.Label;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.Value;

/**
 * The Class ToolTipFigure.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ValueToolTipFigure extends ToolTipFigure {
	/**
	 * Instantiates a new tool tip figure.
	 * 
	 * @param element
	 *            the element
	 */
	public ValueToolTipFigure(final INamedElement element, Value value) {
		super(element);
		Label l = new Label();
		l.setText(value.getValue());
		add(l);
	}
	

}
