/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.editparts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.tools.DirectEditManager;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.Messages;
import org.fordiac.ide.gef.draw2d.ITransparencyFigure;
import org.fordiac.ide.gef.policies.AbstractViewRenameEditPolicy;
import org.fordiac.ide.gef.policies.EmptyXYLayoutEditPolicy;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.ui.Color;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UiPackage;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.ColorManager;

/**
 * This abstract class for the EditParts.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public abstract class AbstractViewEditPart extends AbstractConnectableEditPart
		implements IPropertySource {

	/** The Constant ERROR_IN_CREATE_FIGURE. */
	private static final String ERROR_IN_CREATE_FIGURE = Messages.AbstractViewEditPart_ERROR_createFigure;

	/** The manager. */
	protected DirectEditManager manager;

	/** The ui information content adapter. */
	private final EContentAdapter uiInformationContentAdapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			Object feature = notification.getFeature();
			if (UiPackage.eINSTANCE.getPosition_X().equals(feature)
					|| UiPackage.eINSTANCE.getPosition_Y().equals(feature)
					|| UiPackage.eINSTANCE.getPosition().equals(feature)) {
				refreshPosition();
			}

			if (UiPackage.eINSTANCE.getView_BackgroundColor().equals(feature)
					&& getView().equals(notification.getNotifier())) {
				backgroundColorChanged(getFigure());
			}
			super.notifyChanged(notification);
		}
	};

	/** The i named element content adapter. */
	private final EContentAdapter iNamedElementContentAdapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			if(notification.getNotifier().equals(getINamedElement())){
				Object feature = notification.getFeature();
				if (LibraryElementPackage.eINSTANCE.getINamedElement_Name().equals(
						feature)) {
					refreshName();
				}
				if (LibraryElementPackage.eINSTANCE.getINamedElement_Comment()
						.equals(feature)) {
					refreshComment();
				}
	
				super.notifyChanged(notification);
			}
		}
	};

	/**
	 * Background color changed.
	 */
	protected void backgroundColorChanged(IFigure figure) {
		Color fordiacColor = getView().getBackgroundColor();
		setColor(figure, fordiacColor);
	}

	protected void setColor(IFigure figure, Color fordiacColor) {
		org.eclipse.swt.graphics.Color newColor;
		if (fordiacColor != null) {
			newColor = ColorManager.getColor(new RGB(fordiacColor.getRed(),
					fordiacColor.getGreen(), fordiacColor.getBlue()));
		}
 		else {
			newColor = null;
		}
		figure.setBackgroundColor(newColor);
	}

	/**
	 * Gets the interface elements.
	 * 
	 * @return the interface elements
	 */
	public abstract List<? extends InterfaceElementView> getInterfaceElements();

	/**
	 * Gets the view.
	 * 
	 * @return the view
	 */
	protected View getView() {
		return (View) getModel();
	}

	/**
	 * Gets the i named element.
	 * 
	 * @return the i named element
	 */
	public abstract INamedElement getINamedElement();

	/**
	 * Gets the name label.
	 * 
	 * @return the name label
	 */
	public abstract Label getNameLabel();

	/**
	 * Refresh name.
	 */
	protected void refreshName() {
		getNameLabel().setText(getINamedElement().getName());
	}

	/**
	 * Refresh comment.
	 */
	protected void refreshComment() {
		// TODO __gebenh implement refreshComment if necessary
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		refreshPosition();
	}

	/**
	 * Refresh position.
	 */
	protected void refreshPosition() {
		Rectangle bounds = null;
		if (getView().getPosition() != null) {
			// if (getView() != null && getView().getPosition() != null) {
			bounds = new Rectangle(getView().getPosition().getX(), getView()
					.getPosition().getY(), -1, -1);
			// } else {
			// bounds = new Rectangle()
			// }
			if (getParent() != null) {
				((GraphicalEditPart) getParent()).setLayoutConstraint(this,
						getFigure(), bounds);
			}
		}
	}

	/**
	 * Needs to return an EContentAdapter which is registered on the model
	 * objects and gets informed if something change.
	 * 
	 * @return EContentAdapter the EContentAdapter of the derived class (must
	 *         not be null).
	 */
	public abstract EContentAdapter getContentAdapter();

	/**
	 * If an View needs to be informed for changes in the PreferencePage (e.g.
	 * change of a Color) the derived class have to return an
	 * IPropertyChangeListener with implemented <code>propertyChange()</code>
	 * method if notification on changes are required otherwise it can return
	 * <code>null</code>.
	 * 
	 * @return IPropertyChangeListener the IPropertyChangeListener of the
	 *         derived class or <code>null</code> if derived class should not be
	 *         added to the listeners.
	 */
	public abstract IPropertyChangeListener getPreferenceChangeListener();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (!isActive()) {
			super.activate();
			((Notifier) getModel()).eAdapters()
					.add(uiInformationContentAdapter);
			if (getINamedElement() != null) {
				getINamedElement().eAdapters().add(iNamedElementContentAdapter);
			}
			((Notifier) getModel()).eAdapters().add(getContentAdapter());
			if (getPreferenceChangeListener() != null) {
				Activator
						.getDefault()
						.getPreferenceStore()
						.addPropertyChangeListener(
								getPreferenceChangeListener());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((Notifier) getModel()).eAdapters().remove(
					uiInformationContentAdapter);
			if (getINamedElement() != null) {
				getINamedElement().eAdapters().remove(
						iNamedElementContentAdapter);
			}
			((Notifier) getModel()).eAdapters().remove(getContentAdapter());
			if (getPreferenceChangeListener() != null) {
				Activator
						.getDefault()
						.getPreferenceStore()
						.removePropertyChangeListener(
								getPreferenceChangeListener());
			}
		}
		if (connectionProviders != null) {
			connectionProviders.clear();
			connectionProviders = null;
		}
	}

	/**
	 * Children should override this. Default implementation returns an empty
	 * array.
	 * 
	 * @return the property descriptors
	 */
	public IPropertyDescriptor[] getPropertyDescriptors() {
		return new IPropertyDescriptor[0];
	}

	/**
	 * derived classes have to override this method.
	 * 
	 * @return the figure
	 */
	protected abstract IFigure createFigureForModel();

	/**
	 * Creates the <code>Figure</code> to be used as this part's <i>visuals</i>.
	 * 
	 * @return a figure
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 * @see #createFigureForModel()
	 */
	@Override
	protected IFigure createFigure() {
		IFigure f = null;
		try {
			f = createFigureForModel();
			if (f != null) {
				backgroundColorChanged(f);
			}
		} catch (IllegalArgumentException e) {
			Activator.getDefault().logError(ERROR_IN_CREATE_FIGURE, e);
		}
		return f;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new EmptyXYLayoutEditPolicy());

		// EditPolicy which allows the direct edit of the Instance Name
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
				new AbstractViewRenameEditPolicy());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.EditPart#performRequest(org.eclipse.gef.Request)
	 */
	@Override
	public void performRequest(final Request request) {
		// REQ_DIRECT_EDIT -> first select 0.4 sec pause -> click -> edit
		// REQ_OPEN -> doubleclick
		if (request.getType() == RequestConstants.REQ_DIRECT_EDIT
				|| request.getType() == RequestConstants.REQ_OPEN) {
			performDirectEdit();
		} else {
			super.performRequest(request);
		}
	}

	/**
	 * Gets the manager.
	 * 
	 * @return the manager
	 */
	public DirectEditManager getManager() {
		if (manager == null) {
			Label l = getNameLabel();
			manager = new LabelDirectEditManager(this, TextCellEditor.class,
					new NameCellEditorLocator(l), l);
		}

		return manager;
	}

	/**
	 * performs the directEdit.
	 */
	public void performDirectEdit() {
		getManager().show();
	}

	/**
	 * Performs direct edit setting the initial text to be the initialCharacter.
	 * 
	 * @param initialCharacter
	 *            the initial character
	 */
	public void performDirectEdit(final char initialCharacter) {
		if (getManager() instanceof LabelDirectEditManager) {
			((LabelDirectEditManager) getManager()).show(initialCharacter);
		} else {
			getManager().show();
		}
	}

	public void setTransparency(int value) {
		if (getFigure() instanceof ITransparencyFigure) {
			((ITransparencyFigure) getFigure()).setTransparency(value);
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List getModelSourceConnections() {
		if (connectionProviders == null) {
			getExtensions();
		}
		ArrayList<Object> children = new ArrayList<Object>();
		for (Iterator<IConnectionProvider> iterator = connectionProviders.iterator(); iterator
				.hasNext();) {
			IConnectionProvider provider = (IConnectionProvider) iterator.next();
			if (provider.isEnabled()) {
				children.addAll(provider.getSourceConnections(this));
			}
		}
		return children;
	}
	
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List getModelTargetConnections() {
		if (connectionProviders == null) {
			getExtensions();
		}
		ArrayList<Object> children = new ArrayList<Object>();
		for (Iterator<IConnectionProvider> iterator = connectionProviders.iterator(); iterator
				.hasNext();) {
			IConnectionProvider provider = (IConnectionProvider) iterator.next();
			if (provider.isEnabled()) {
				children.addAll(provider.getTargetConnections(this));
			}
		}
		return children;
	}
	
	/** The child providers. */
	ArrayList<IConnectionProvider> connectionProviders = null;
	
	private void getExtensions() {
		connectionProviders = new ArrayList<IConnectionProvider>();
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] elems = registry.getConfigurationElementsFor(
				"org.fordiac.ide.gef", "ConnectionProvider"); //$NON-NLS-1$
		for (int i = 0; i < elems.length; i++) {
			IConfigurationElement element = elems[i];
			try {
				Object object = element.createExecutableExtension("class"); //$NON-NLS-1$
				if (object instanceof IConnectionProvider) {
					IConnectionProvider connectionProvider = (IConnectionProvider) object;
					connectionProviders.add(connectionProvider);

				}
			} catch (CoreException corex) {
				Activator.getDefault().logError("Error loading ConnectionProvider Extensions in org.fordiac.ide.gef", corex);
			}
		}
	}

	
	
}
