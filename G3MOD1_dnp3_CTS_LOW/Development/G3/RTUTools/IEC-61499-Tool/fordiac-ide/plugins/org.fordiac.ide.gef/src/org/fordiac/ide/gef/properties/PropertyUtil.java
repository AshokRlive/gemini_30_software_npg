/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.properties;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.viewers.ICellEditorValidator;
import org.eclipse.ui.views.properties.ComboBoxPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.PropertyDescriptor;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;
import org.fordiac.ide.deployment.DeploymentCoordinator;
import org.fordiac.ide.deployment.IDeploymentExecutor;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.Messages;
import org.fordiac.ide.gef.commands.ChangeNameCommand;
import org.fordiac.ide.gef.commands.ChangeValueCommand;
import org.fordiac.ide.gef.commands.SetProfileCommand;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.util.IdentifierVerifyListener;
import org.fordiac.ide.util.Utils;
import org.fordiac.ide.util.commands.ChangeCommentCommand;

/**
 * The Class PropertyUtil.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class PropertyUtil {

	/** The Constant COMMENT_PARAMETER. */
	private static final String COMMENT_PARAMETER = "Comment"; //$NON-NLS-1$

	/** The Constant VALUE_PARAMETER. */
	private static final String VALUE_PARAMETER = "Value"; //$NON-NLS-1$

	/** The Constant PROFILE_PARAMETER. */
	private static final String PROFILE_PARAMETER = "Profile"; //$NON-NLS-1$

	/**
	 * The Class VarDeclarationMapping.
	 */
	private static class VarDeclarationMapping {

		/** The var decl. */
		public VarDeclaration varDecl = null;

		/** The property. */
		public String property = null;
	}

	/**
	 * The Class DeviceMapping.
	 */
	private static class DeviceMapping {

		/** The device. */
		public Device device = null;

		/** The property. */
		public String property = null;
	}

	private static class CommentWrapper {
		public INamedElement element;
	}

	/**
	 * Gets the var declaration comment descriptor.
	 * 
	 * @param var the var
	 * 
	 * @return the var declaration comment descriptor
	 */
	public static PropertyDescriptor getVarDeclarationCommentDescriptor(
			final VarDeclaration var) {

		VarDeclarationMapping mapping = new VarDeclarationMapping();
		mapping.varDecl = var;
		mapping.property = COMMENT_PARAMETER;

		PropertyDescriptor propDesc = new PropertyDescriptor(mapping,
				mapping.property);
		propDesc.setCategory(var.getName());
		propDesc.setDescription(var.getComment() != null ? var.getComment() : ""); //$NON-NLS-1$
		return propDesc;
	}

	/**
	 * Gets the editable var declaration descriptor.
	 * 
	 * @param var the var
	 * 
	 * @return the editable var declaration descriptor
	 */
	public static PropertyDescriptor getEditableVarDeclarationDescriptor(
			final VarDeclaration var) {

		VarDeclarationMapping mapping = new VarDeclarationMapping();
		mapping.varDecl = var;
		mapping.property = VALUE_PARAMETER;

		PropertyDescriptor propDesc = new TextPropertyDescriptor(mapping,
				mapping.property);
		propDesc.setCategory(var.getName());
		propDesc.setDescription(var.getComment() != null ? var.getComment() : ""); //$NON-NLS-1$
		return propDesc;
	}

	/**
	 * Gets the locked var declaration descriptor.
	 * 
	 * @param var the var
	 * 
	 * @return the locked var declaration descriptor
	 */
	public static PropertyDescriptor getLockedVarDeclarationDescriptor(
			final VarDeclaration var) {
		VarDeclarationMapping mapping = new VarDeclarationMapping();
		mapping.varDecl = var;
		mapping.property = VALUE_PARAMETER;

		PropertyDescriptor propDesc = new PropertyDescriptor(mapping,
				mapping.property);
		propDesc.setCategory(var.getName());
		propDesc.setDescription(var.getComment() != null ? var.getComment() : ""); //$NON-NLS-1$
		return propDesc;
	}

	/**
	 * Adds the var decl descriptors.
	 * 
	 * @param constants the constants
	 * @param descriptors the descriptors
	 * @param i the i
	 * 
	 * @return the int
	 */
	public static int addVarDeclDescriptors(final List<VarDeclaration> constants,
			final IPropertyDescriptor[] descriptors, int i) {
		for (Iterator<VarDeclaration> iter = constants.iterator(); iter.hasNext();) {
			VarDeclaration var = iter.next();
			// if there is a connection to the variable, changing the constant
			// value is not possible - use PropertyDescriptor instead of
			// TextPropertyDescripter
			if (var.getName() != null) {
				if (var.isIsInput() && var.getInputConnections().size() == 0) {
					descriptors[i] = PropertyUtil.getVarDeclarationCommentDescriptor(var);
					i++;
					descriptors[i] = PropertyUtil
							.getEditableVarDeclarationDescriptor(var);
					i++;
				} else {
					descriptors[i] = PropertyUtil.getVarDeclarationCommentDescriptor(var);
					i++;
					descriptors[i] = PropertyUtil.getLockedVarDeclarationDescriptor(var);
					i++;
				}
			}
		}
		return i;
	}

	/**
	 * Adds the instance name descriptor.
	 * 
	 * @param namedElement the named element
	 * @param descriptors the descriptors
	 * @param i the i
	 * 
	 * @return the int
	 */
	public static int addInstanceNameDescriptor(final INamedElement namedElement,
			final IPropertyDescriptor[] descriptors, int i) {
		descriptors[i] = new TextPropertyDescriptor(namedElement,
				Messages.PropertyUtil_LABEL_Instancename);
		((TextPropertyDescriptor) descriptors[i])
				.setValidator(new ICellEditorValidator() {

					@Override
					public String isValid(Object value) {
						if (!IdentifierVerifyListener.isValidIdentifier(value.toString())) {
							return value + " is not a valid Identifier!";
						}
						return null;
					}
				});

		return ++i;
	}

	/**
	 * Adds the instance comment descriptor.
	 * 
	 * @param namedElement the named element
	 * @param descriptors the descriptors
	 * @param i the i
	 * 
	 * @return the int
	 */
	public static int addCommentDescriptor(final INamedElement namedElement,
			final IPropertyDescriptor[] descriptors, int i) {
		CommentWrapper wrapper = new CommentWrapper();
		wrapper.element = namedElement;
		descriptors[i] = new TextPropertyDescriptor(wrapper,
				Messages.PropertyUtil_LABEL_Instancecomment);
		return ++i;
	}

	/**
	 * Sets the property value.
	 * 
	 * @param id the id
	 * @param value the value
	 */
	public static void setPropertyValue(final Object id, final Object value) {
		if (Utils.getCurrentActiveEditor() != null) {
			CommandStack stack = (CommandStack) Utils.getCurrentActiveEditor()
					.getAdapter(CommandStack.class);

			if (id instanceof VarDeclarationMapping) {
				VarDeclarationMapping mapping = (VarDeclarationMapping) id;
				VarDeclaration v = mapping.varDecl;
				ChangeValueCommand cmd = new ChangeValueCommand(v, !value.toString().equals("") ? value.toString() : null);

				if (stack != null) {
					stack.execute(cmd);
				}

				// // TODO: check whether it is useful to reset it completly ....
				// v.getValue().setValue(value.toString() != "" ? value.toString() :
				// null);
			} else if (id instanceof DeviceMapping) {
				DeviceMapping dm = (DeviceMapping) id;
				if (dm.property == PROFILE_PARAMETER) {
					SetProfileCommand cmd = new SetProfileCommand(dm.device, 
							getAvailableProfileNames()[((Integer) value).intValue()]);
					if (stack != null) {
						stack.execute(cmd);
					}
				}
			} else if (id instanceof CommentWrapper) {
				CommentWrapper wrapper = (CommentWrapper) id;
				ChangeCommentCommand cmd = new ChangeCommentCommand(wrapper.element, value.toString());
				if (stack != null) {
					stack.execute(cmd);
				}
			} else if (id instanceof INamedElement) {
				if (((INamedElement) id).isIdentifier()) {
					if (!IdentifierVerifyListener.isValidIdentifier(value.toString())) {
						Activator.statusLineErrorMessage(value.toString() + " is not a valid Identifier!");
						return;
					}
				}
				ChangeNameCommand cmd = new ChangeNameCommand((INamedElement) id, value.toString());
				if (stack != null) {
					stack.execute(cmd);
				}
			}
		}
	}

	/**
	 * Gets the property value.
	 * 
	 * @param id the id
	 * 
	 * @return the property value
	 */
	public static Object getPropertyValue(final Object id) {

		if (id instanceof VarDeclarationMapping) {
			VarDeclarationMapping mapping = (VarDeclarationMapping) id;
			VarDeclaration v = mapping.varDecl;
			if (mapping.property == COMMENT_PARAMETER) {
				return v.getComment() != null ? v.getComment() : ""; //$NON-NLS-1$
			} else if (mapping.property == VALUE_PARAMETER) {
				if (v.getInputConnections().size() == 0
						&& v.getOutputConnections().size() == 0) {
					return v.getValue() != null && v.getValue().getValue() != null ? v
							.getValue().getValue() : ""; //$NON-NLS-1$
				}
			}
			return null;
		} else if (id instanceof DeviceMapping) {
			DeviceMapping dm = (DeviceMapping) id;
			if (dm.property == PROFILE_PARAMETER) {
				String profile = dm.device.getProfile();
				
				if(null != profile){
					String[] profileList = getAvailableProfileNames();
					for(int i = 0; i < profileList.length; i++){
						if(profile.equalsIgnoreCase(profileList[i])){
							return new Integer(i);
						}
					}
				}				
				return new Integer(-1);
			}

		} else if (id instanceof INamedElement) {
			INamedElement namedElement = (INamedElement) id;
			return namedElement.getName();
		} else if (id instanceof CommentWrapper) {
			INamedElement namedElement = ((CommentWrapper) id).element;
			return namedElement.getComment() != null ? namedElement.getComment() : "";
		} else if (id instanceof String) {
			return id.toString();
		}
		return null;
	}

	/**
	 * Adds the profile descriptor.
	 * 
	 * @param deviceElement the device element
	 * @param descriptors the descriptors
	 * @param i the i
	 * 
	 * @return the int
	 */
	public static int addProfileDescriptor(final Device deviceElement,
			final IPropertyDescriptor[] descriptors, int i) {

		DeviceMapping dm = new DeviceMapping();
		dm.device = deviceElement;
		dm.property = PROFILE_PARAMETER;

		PropertyDescriptor propDesc = new ComboBoxPropertyDescriptor(dm, dm.property, getAvailableProfileNames());
		propDesc.setDescription(Messages.PropertyUtil_LABEL_Description_Complianceprofile);
		
		descriptors[i] = propDesc;
		i++;
		return i;
	}
	
	
	private static String[] profileNames = null;

	private static String[] getAvailableProfileNames() {
		if (null == profileNames) {
			ArrayList<IDeploymentExecutor> deploymentExecutors = DeploymentCoordinator.loadDeploymentExecutors();

			String newProfileNames[] = new String[deploymentExecutors.size()];
			int i = 0;
			
			for (IDeploymentExecutor idepExec : deploymentExecutors) {
				newProfileNames[i] = idepExec.getProfileName();
				i++;
			}
			profileNames = newProfileNames;
		}
		
		return profileNames;
	}

}
