package org.fordiac.ide.gef.dnd;

import org.eclipse.gef.requests.CreationFactory;

public class ParameterValueFactory implements CreationFactory {

   private String text = "";

   public Object getNewObject() {
	   return text;
   }

   public Object getObjectType() {
      return String.class;
   }

   public void setText(String s) {
      text = s;
   }
}