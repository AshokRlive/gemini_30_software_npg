/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.gef.Messages;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.util.Utils;

/**
 * A command to rename any INamedElement.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class SetProfileCommand extends Command {

	/** The device. */
	private final Device device;

	/** The profile. */
	private final String profile;

	/** The old profile. */
	private String oldProfile;

	/** The editor. */
	private IEditorPart editor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());
	}

	/**
	 * Instantiates a new sets the profile command.
	 * 
	 * @param device the device
	 * @param profile the profile
	 */
	public SetProfileCommand(final Device device, final String profile) {
		super(Messages.SetProfileCommand_LABEL_SetProfile);
		this.device = device;
		this.profile = profile;
	}

	/**
	 * Renames the INamedElement.
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		oldProfile = device.getProfile() != null ? device.getProfile() : ""; //$NON-NLS-1$
		device.setProfile(profile);
	}

	/**
	 * Restores the old InstanceName.
	 */
	@Override
	public void undo() {
		device.setName(oldProfile);
	}

	/**
	 * Redo.
	 * 
	 * @see SetProfileCommand#execute()
	 */
	@Override
	public void redo() {
		device.setName(profile);
	}

}
