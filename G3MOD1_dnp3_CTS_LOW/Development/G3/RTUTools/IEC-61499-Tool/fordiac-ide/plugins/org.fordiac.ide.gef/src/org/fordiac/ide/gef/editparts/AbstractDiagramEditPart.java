/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.editparts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gef.CompoundSnapToHelper;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.SnapToGrid;
import org.eclipse.gef.SnapToHelper;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.editpolicies.SnapFeedbackPolicy;
import org.eclipse.gef.rulers.RulerProvider;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.widgets.Display;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.preferences.DiagramPreferences;
import org.fordiac.ide.gef.router.RouterUtil;
import org.fordiac.ide.model.ui.Diagram;

/**
 * The Class AbstractDiagramEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public abstract class AbstractDiagramEditPart extends AbstractGraphicalEditPart {

	/** The child providers. */
	ArrayList<IChildrenProvider> childProviders = null;

	/**
	 * Creates the <code>Figure</code> to be used as this part's <i>visuals</i>.
	 * 
	 * @return a figure
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		Figure f = new FreeformLayer();
		f.setBorder(new MarginBorder(10));
		f.setLayoutManager(new FreeformLayout());
		f.setOpaque(false);
		
		// Create the static router for the connection layer
		ConnectionLayer connLayer = (ConnectionLayer) getLayer(LayerConstants.CONNECTION_LAYER);
		connLayer.setConnectionRouter(RouterUtil.getConnectionRouter(f));

		return f;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#refresh()
	 */
	@Override
	public void refresh() {
		super.refresh();
		updateGrid();
		showGrid();
		updateRuler();
	}

	private void updateRuler() {
		getViewer().setProperty(
				RulerProvider.PROPERTY_RULER_VISIBILITY,
				Activator.getDefault().getPreferenceStore()
						.getBoolean(DiagramPreferences.SHOW_RULERS));
	}

	private void updateUnit() {
		getViewer().setProperty(RulerProvider.PROPERTY_RULER_VISIBILITY,
				Boolean.FALSE);
		updateRuler();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (!isActive()) {
			super.activate();
			if (getPreferenceChangeListener() != null) {
				Activator
						.getDefault()
						.getPreferenceStore()
						.addPropertyChangeListener(
								getPreferenceChangeListener());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			if (getPreferenceChangeListener() != null) {
				Activator
						.getDefault()
						.getPreferenceStore()
						.removePropertyChangeListener(
								getPreferenceChangeListener());
			}
		}
		if (childProviders != null) {
			childProviders.clear();
			childProviders = null;
		}
	}

	protected void showGrid() {
		getViewer().setProperty(
				SnapToGrid.PROPERTY_GRID_VISIBLE,
				new Boolean(Activator.getDefault().getPreferenceStore()
						.getBoolean(DiagramPreferences.SHOW_GRID)));

	}

	private IPropertyChangeListener listener;

	/**
	 * Gets the preference change listener.
	 * 
	 * @return the preference change listener
	 */
	public IPropertyChangeListener getPreferenceChangeListener() {
		if (listener == null) {
			listener = new IPropertyChangeListener() {
				public void propertyChange(final PropertyChangeEvent event) {
					if (event.getProperty().equals(
							DiagramPreferences.SNAP_TO_GRID)) {
						// refresh();

					}
					if (event.getProperty().equals(
							DiagramPreferences.SHOW_GRID)) {
						showGrid();
					}
					if (event.getProperty().equals(
							DiagramPreferences.GRID_SPACING)) {
						updateGrid();
					}
					if (event.getProperty().equals(
							DiagramPreferences.SHOW_RULERS)) {
						updateRuler();
					}
					if (event.getProperty().equals(
							DiagramPreferences.RULER_UNITS)) {
						updateUnit();
					}
					if (event.getProperty().equals(
							DiagramPreferences.CONNECTION_ROUTER)) {
						updateRouter();
					}
				}
			};
		}
		return listener;
	}

	protected void updateRouter() {
		ConnectionLayer connLayer = (ConnectionLayer) getLayer(LayerConstants.CONNECTION_LAYER);
		connLayer.setConnectionRouter(RouterUtil
				.getConnectionRouter(getFigure()));
	}

	protected void updateGrid() {
		int unit = Activator.getDefault().getPreferenceStore()
				.getInt(DiagramPreferences.RULER_UNITS);
		double gridSpacing = Activator.getDefault().getPreferenceStore()
				.getDouble(DiagramPreferences.GRID_SPACING);
		// Get the Displays DPIs
		double dotsPerInch = Display.getDefault().getDPI().x;
		int spacingInPixels = 0;

		// Evaluate the Grid Spacing based on the ruler units
		switch (unit) {
		case RulerProvider.UNIT_INCHES:
			spacingInPixels = (int) Math.round(dotsPerInch * gridSpacing);
			break;

		case RulerProvider.UNIT_CENTIMETERS:
			spacingInPixels = (int) Math
					.round(dotsPerInch * gridSpacing / 2.54);
			break;

		default:
			spacingInPixels = (int) gridSpacing;
		}

		// int spacing = getMapMode().DPtoLP(spacingInPixels);

		int spacing = spacingInPixels;

		getViewer().setProperty(SnapToGrid.PROPERTY_GRID_SPACING,
				new Dimension(spacing, spacing));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractGraphicalEditPart#getAdapter(java.lang
	 * .Class)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(final Class key) {
		if (key == SnapToHelper.class) {
			List<SnapToGrid> snapStrategies = new ArrayList<SnapToGrid>();
			Boolean val = Activator.getDefault().getPreferenceStore()
					.getBoolean(DiagramPreferences.SNAP_TO_GRID);
			if (val != null && val.booleanValue()) {
				snapStrategies.add(new SnapToGrid(this));
			}

			if (snapStrategies.size() == 0) {
				return null;
			}
			if (snapStrategies.size() == 1) {
				return snapStrategies.get(0);
			}

			SnapToHelper ss[] = new SnapToHelper[snapStrategies.size()];
			for (int i = 0; i < snapStrategies.size(); i++) {
				ss[i] = snapStrategies.get(i);
			}
			return new CompoundSnapToHelper(ss);
		}
		return super.getAdapter(key);
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy("Snap Feedback", new SnapFeedbackPolicy()); //$NON-NLS-1$
	}

	/**
	 * returns the model object as <code>FBNetwork</code>.
	 * 
	 * @return FBNetwork to be visualized
	 */
	public Diagram getCastedModel() {
		if (getModel() instanceof Diagram) {
			return (Diagram) getModel();
		}
		return null;
	}

	@Override
	protected List<?> getModelChildren() {
		if (childProviders == null) {
			getExtensions();
		}
		ArrayList<Object> children = new ArrayList<Object>();
		for (Iterator<IChildrenProvider> iterator = childProviders.iterator(); iterator
				.hasNext();) {
			IChildrenProvider provider = (IChildrenProvider) iterator.next();
			if (provider.isEnabled()) {
				children.addAll(provider.getChildren(getCastedModel()));
			}
		}
		return children;
	}

	private void getExtensions() {
		childProviders = new ArrayList<IChildrenProvider>();
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IConfigurationElement[] elems = registry.getConfigurationElementsFor(
				"org.fordiac.ide.gef", "ChildrenProvider"); //$NON-NLS-1$
		for (int i = 0; i < elems.length; i++) {
			IConfigurationElement element = elems[i];
			try {
				Object object = element.createExecutableExtension("class"); //$NON-NLS-1$
				if (object instanceof IChildrenProvider) {
					IChildrenProvider childrenProvider = (IChildrenProvider) object;

					childProviders.add(childrenProvider);

				}
			} catch (CoreException corex) {
				Activator.getDefault().logError("Error loading ChildrenProvider Extensions in org.fordiac.ide.gef", corex); //$NON-NLS-1$
			}
		}
	}

}
