/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.commands;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.libraryElement.Algorithm;
import org.fordiac.ide.model.libraryElement.ECState;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.ui.controls.Abstract4DIACUIPlugin;

/**
 * The Class ChangeNameCommand.
 */
public class ChangeNameCommand extends Command {
	protected INamedElement element;
	private final EObject type;
	protected String name;
	private String oldName;

	public ChangeNameCommand(final INamedElement element, final String name) {
		super();
		this.element = element;
		if(element instanceof FB){
			this.type = element;
		}else{
			if(element.eContainer() instanceof FBType || element instanceof ECState || element instanceof Algorithm){
				this.type = element.eContainer();
			}else{
				if((null != element.eContainer()) && (element.eContainer().eContainer() instanceof FBType)){
					this.type = (FBType)element.eContainer().eContainer();	
				}else{
					this.type = element;
				}
			}
		}
		this.name = name;
	}
	
	@Override
	public boolean canExecute() {
		return performNameCheck(name);
	}

	@Override
	public void execute() {
		oldName = element.getName();
		if(performNameCheck(name)){
			redo();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		element.setName(oldName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		element.setName(name);
	}

	public boolean performNameCheck(String nameToCheck) {
		
		if(nameToCheck.equals(NameRepository.getUniqueElementName(element, type, nameToCheck))){
			Abstract4DIACUIPlugin.statusLineErrorMessage(null);	
			return true;
		}
		else{
			Abstract4DIACUIPlugin.statusLineErrorMessage("Element with Name: " + nameToCheck + " already exists!");
			return false;
		}
	}
}
