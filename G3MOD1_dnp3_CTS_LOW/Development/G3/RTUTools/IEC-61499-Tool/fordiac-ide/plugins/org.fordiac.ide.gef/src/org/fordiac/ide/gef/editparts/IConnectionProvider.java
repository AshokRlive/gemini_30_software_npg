/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.editparts;

import java.util.ArrayList;

import org.fordiac.ide.gef.IConnectionEditPartCreator;

/**
 * The Interface IChildrenProvider.
 * 
 * @author gebenh
 */
public interface IConnectionProvider {

	ArrayList<IConnectionEditPartCreator> getSourceConnections(AbstractViewEditPart context);
	ArrayList<IConnectionEditPartCreator> getTargetConnections(AbstractViewEditPart context);

	/**
	 * Checks if is enabled.
	 * 
	 * @return true, if is enabled
	 */
	boolean isEnabled();

}
