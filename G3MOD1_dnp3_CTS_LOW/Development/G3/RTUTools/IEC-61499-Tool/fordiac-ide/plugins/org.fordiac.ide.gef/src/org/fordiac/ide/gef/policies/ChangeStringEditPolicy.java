/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.fordiac.ide.gef.commands.ChangeNameCommand;
import org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart;
import org.fordiac.ide.model.libraryElement.INamedElement;

/**
 * DirectEditPolicy for renaming/change specified string value.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ChangeStringEditPolicy extends DirectEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.DirectEditPolicy#getDirectEditCommand(org
	 * .eclipse.gef.requests.DirectEditRequest)
	 */
	@Override
	protected Command getDirectEditCommand(final DirectEditRequest request) {
		if (getHost() instanceof IChangeStringEditPart) {
			IChangeStringEditPart viewEditPart = (IChangeStringEditPart) getHost();
			return  new ChangeNameCommand((INamedElement) viewEditPart.getElement(), (String) request.getCellEditor().getValue());
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.DirectEditPolicy#showCurrentEditValue(org
	 * .eclipse.gef.requests.DirectEditRequest)
	 */
	@Override
	protected void showCurrentEditValue(final DirectEditRequest request) {
		String value = (String) request.getCellEditor().getValue();
		if (getHost() instanceof IChangeStringEditPart) {
			AbstractDirectEditableEditPart viewEditPart = (AbstractDirectEditableEditPart) getHost();
			viewEditPart.getNameLabel().setText(value);
		}
	}
}
