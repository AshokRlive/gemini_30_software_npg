/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.gef.Messages;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.Utils;

/**
 * A command for moving a View object.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ViewSetPositionCommand extends Command {

	/** The Constant MOVE_LABEL. */
	private static final String MOVE_LABEL = Messages.ViewSetPositionCommand_LABEL_Move;

	/** The new bounds. */
	private final Rectangle newBounds;

	/** The old bounds. */
	private Rectangle oldBounds;

	/** The request. */
	private final ChangeBoundsRequest request;

	/** The view. */
	private final View view;

	/** The editor. */
	private IEditorPart editor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());

	}

	/**
	 * Instantiates a new view set position command.
	 * 
	 * @param view the view
	 * @param req the req
	 * @param newBounds the new bounds
	 */
	public ViewSetPositionCommand(final View view,
			final ChangeBoundsRequest req, final Rectangle newBounds) {
		this.view = view;
		this.request = req;
		this.newBounds = newBounds.getCopy();
		setLabel(MOVE_LABEL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		Object type = request.getType();
		// make sure the Request is of a type we support: (Move or
		// Move_Children)
		// e.g. a FB moves within an application
		return RequestConstants.REQ_MOVE.equals(type)
				|| RequestConstants.REQ_MOVE_CHILDREN.equals(type)
				|| RequestConstants.REQ_ALIGN_CHILDREN.equals(type);
	}

	/**
	 * Sets the new Position of the affected UIFB.
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		setLabel(getLabel() + "(" + editor.getTitle() + ")"); //$NON-NLS-1$ //$NON-NLS-2$
		if (view.getPosition() != null) {
			oldBounds = new Rectangle(view.getPosition().getX(), view
					.getPosition().getY(), -1, -1);
		} else {
			oldBounds = new Rectangle(0, 0, -1, -1);
		}
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		Position pos = view.getPosition();
		if(null != pos){
			pos.setX(newBounds.x);
			pos.setY(newBounds.y);
			view.setPosition(pos); //allows that view models can update their model
		}
	}

	/**
	 * Restores the old position.
	 */
	@Override
	public void undo() {
		Position pos = view.getPosition();
		if(null != pos){
			pos.setX(oldBounds.x);
			pos.setY(oldBounds.y);
			view.setPosition(pos);  //allows that view models can update their model
		}
	}

}
