/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editparts;

import org.eclipse.gef.editparts.ZoomManager;
import org.fordiac.ide.application.editparts.FBEditPart;

/**
 * The Class ResFBEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ResFBEditPart extends FBEditPart {
	
	public ResFBEditPart(ZoomManager zoomManager) {
		super(zoomManager);
	}
	// empty class for distinguishing in the resource editor
}
