/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.policies;

import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.fordiac.ide.application.commands.FBCreateCommand;
import org.fordiac.ide.application.commands.MapSubAppToCommand;
import org.fordiac.ide.application.commands.MapToCommand;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;
import org.fordiac.ide.application.policies.FBNetworkXYLayoutEditPolicy;
import org.fordiac.ide.gef.commands.ViewSetPositionCommand;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.resourceediting.editparts.FBNetworkContainerEditPart;

/**
 * The Class MappingEditPolicy.
 */
public class MappingEditPolicy extends FBNetworkXYLayoutEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.LayoutEditPolicy#getCreateCommand(org.eclipse
	 * .gef.requests.CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(final CreateRequest request) {
		FBCreateCommand cmd = (FBCreateCommand) super.getCreateCommand(request);

		if (cmd != null)
			cmd.setCreateResourceFB(true);

		return cmd;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#getAddCommand(
	 * org.eclipse.gef.Request)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected Command getAddCommand(final Request request) {
		ChangeBoundsRequest cbr = (ChangeBoundsRequest) request;
		CompoundCommand compound = new CompoundCommand();
		List editParts = cbr.getEditParts();
		if (getHost() instanceof FBNetworkContainerEditPart) {
			FBNetworkContainerEditPart host = (FBNetworkContainerEditPart) getHost();
			UIResourceEditor uiResourceEditor = host.getCastedFBNetworkContainerModel()
					.getResourceEditor(); // .getResourceElement();
			for (Iterator iterator = editParts.iterator(); iterator.hasNext();) {
				EditPart editPart = (EditPart) iterator.next();
				if (editPart instanceof FBEditPart) {
					FBEditPart fbEditPart = (FBEditPart) editPart;
					MapToCommand cmd = new MapToCommand(fbEditPart
							.getCastedModel(), uiResourceEditor);

					Rectangle r;
					r = fbEditPart.getFigure().getBounds().getCopy();
					// convert r to absolute from childpart figure
					fbEditPart.getFigure().translateToAbsolute(r);
					r = cbr.getTransformedRectangle(r);
					// convert this figure to relative
					getLayoutContainer().translateToRelative(r);
					getLayoutContainer().translateFromParent(r);
					r.translate(getLayoutOrigin().getNegated());

					// fbEditPart = getConstraintFor(r);
					cmd.setPosition(new Point(r.x, r.y));

					compound.add(cmd);
				} else if (editPart instanceof SubAppForFBNetworkEditPart) {
					SubAppForFBNetworkEditPart subAppEditPart = (SubAppForFBNetworkEditPart) editPart;
					MapSubAppToCommand mapToCmd = new MapSubAppToCommand(
							subAppEditPart.getCastedModel(), uiResourceEditor);

					Rectangle r;
					r = subAppEditPart.getFigure().getBounds().getCopy();
					// convert r to absolute from childpart figure
					subAppEditPart.getFigure().translateToAbsolute(r);
					r = cbr.getTransformedRectangle(r);
					// convert this figure to relative
					getLayoutContainer().translateToRelative(r);
					getLayoutContainer().translateFromParent(r);
					r.translate(getLayoutOrigin().getNegated());

					// fbEditPart = getConstraintFor(r);
					mapToCmd.setPosition(new Point(r.x, r.y));

					compound.add(mapToCmd);

				}
			}
			return compound.unwrap();
		}
		return super.getAddCommand(request);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#
	 * createChangeConstraintCommand
	 * (org.eclipse.gef.requests.ChangeBoundsRequest, org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	@Override
	protected Command createChangeConstraintCommand(
			final ChangeBoundsRequest request, final EditPart child,
			final Object constraint) {
		if (getHost() instanceof FBNetworkContainerEditPart) {
			if (child instanceof AbstractViewEditPart
					&& constraint instanceof Rectangle) {
				AbstractViewEditPart temp = (AbstractViewEditPart) child;
				if(temp.getModel() instanceof View){
					return new ViewSetPositionCommand((View) temp.getModel(),
							request, (Rectangle) constraint);
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#
	 * createChangeConstraintCommand(org.eclipse.gef.EditPart, java.lang.Object)
	 */
	@Override
	protected Command createChangeConstraintCommand(final EditPart child,
			final Object constraint) {
		return null;
	}

}
