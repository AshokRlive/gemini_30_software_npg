/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.dnd;

import org.eclipse.gef.dnd.SimpleObjectTransfer;

public class FBMappingTransfer extends SimpleObjectTransfer {

	/**
	 * Singleton instance
	 */
	private static final FBMappingTransfer INSTANCE = new FBMappingTransfer();

	/**
	 * Create a unique ID to make sure that different Eclipse applications use
	 * different "types" of <code>IEC61499FBMappingTransfer</code>.
	 */
	private static final String TYPE_NAME = "FBMapping-transfer-format:"
			+ System.currentTimeMillis() + ":" + INSTANCE.hashCode();

	/**
	 * The registered identifier.
	 */
	private static final int TYPEID = registerType(TYPE_NAME);

	/**
	 * Singleton constructor.
	 */
	private FBMappingTransfer() {
		super();
	}

	/**
	 * return the instance.
	 */
	public static FBMappingTransfer getInstance() {
		return INSTANCE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.dnd.Transfer#getTypeIds()
	 */
	@Override
	protected int[] getTypeIds() {
		return new int[] { TYPEID };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.dnd.Transfer#getTypeNames()
	 */
	@Override
	protected String[] getTypeNames() {
		return new String[] { TYPE_NAME };
	}

	public void setElement(Object element) {
		setObject(element);
	}

	public Object getElement() {
		return getObject();
	}

}