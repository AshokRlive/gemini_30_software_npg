/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editors;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.dnd.TemplateTransferDropTargetListener;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.palette.PaletteViewerProvider;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.util.TransferDropTargetListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabFolder2Listener;
import org.eclipse.swt.custom.CTabFolderEvent;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.ActionFactory;
import org.fordiac.ide.application.actions.DeleteFBNetworkAction;
import org.fordiac.ide.application.actions.FBNetworkSelectAllAction;
import org.fordiac.ide.application.actions.UnmapAction;
import org.fordiac.ide.application.actions.UnmapAllAction;
import org.fordiac.ide.application.editors.FBTypePaletteViewerProvider;
import org.fordiac.ide.application.utilities.ApplicationEditorTemplateTransferDropTargetListener;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.DiagramEditorWithFlyoutPalette;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.listeners.IApplicationListener;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.ResizeUIFBNetwork;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.resourceediting.dnd.EditorDropTargetListener;
import org.fordiac.ide.resourceediting.editparts.FBNetworkContainerEditPart;
import org.fordiac.ide.resourceediting.editparts.ResourceDiagramEditPartFactory;
import org.fordiac.systemmanagement.ISystemEditor;
import org.fordiac.systemmanagement.SystemManager;

/**
 * The main editor for ResourceDiagramEditors (mapping and resource editing).
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ResourceDiagramEditor extends DiagramEditorWithFlyoutPalette  implements ISystemEditor{
	private SashForm s;
	private CTabFolder tabFolder;
	private Resource resource;
	private ResourceView resourceView;
	private IApplicationListener adapter;
	private EContentAdapter resourceAdapter = new EContentAdapter(){

		@Override
		public void notifyChanged(Notification notification) {
			Object feature = notification.getFeature();
			if ((LibraryElementPackage.eINSTANCE.getINamedElement_Name().equals(feature)) && 
					(resource.equals(notification.getNotifier()))){				
				setPartName(ResourceEditorInput.getResourceEditorName(resource));
			}
			super.notifyChanged(notification);
		}
		
	};
	private final ArrayList<UIFBNetwork> uifbNetworks = new ArrayList<UIFBNetwork>();
	private ResourceDiagramManager resourceDiagramManager = new ResourceDiagramManager();
		
	public CommandStack getCommandStack(){
		return Activator.getDefault().getCommandStack(getSystem());
	}
	
	@Override
	public Diagram getDiagramModel() {
		return resourceDiagramManager.getDiagram();
	}

	/**
	 * Instantiates a new resource diagram editor.
	 */
	public ResourceDiagramEditor() {
		// empty constructor
	}

	@Override
	protected void initializeGraphicalViewer() {
		super.initializeGraphicalViewer();
		GraphicalViewer viewer = getGraphicalViewer();
		
		// listen for dropped parts
		TransferDropTargetListener listener = createTransferDropTargetListener();
		if (listener != null) {
			viewer.addDropTargetListener(createTransferDropTargetListener());
		}
		
		getGraphicalViewer()
		.addDropTargetListener(
				(TransferDropTargetListener) new TemplateTransferDropTargetListener(
						getGraphicalViewer()));
		
		getGraphicalViewer().addDropTargetListener(
				new EditorDropTargetListener(getGraphicalViewer()));

	}

	private void registerOnChanges() {
		getSystem().addApplicationListener(
				adapter = new IApplicationListener() {

					@Override
					public void newApplicationAdded(AutomationSystem system,
							Application app) {
						createTab(tabFolder, app.getFBNetwork().getApplication().getName(),
								(UIFBNetwork) app.getFBNetwork().eContainer());
					}

					@Override
					public void applicationRemoved(AutomationSystem system,
							Application app) {
						CTabItem item = tabItems.remove(app.getFBNetwork()
								.eContainer());
						if (item != null) {
							item.dispose();
						}
					}
				});
		
		
	}

	@Override
	public void createPartControl(Composite parent) {

		registerOnChanges();
		parent.setLayout(new FormLayout());
		

		FormData data = new FormData();
		data.top = new FormAttachment(0, 0);
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(100, 0);
		data.bottom = new FormAttachment(100, 0);
		s = new SashForm(parent, SWT.VERTICAL);
		s.setLayoutData(data);
		
		tabFolder = new CTabFolder(s,  SWT.TOP | SWT.BORDER);
		tabFolder.setMinimizeVisible(true);
		tabFolder.addCTabFolder2Listener(new CTabFolder2Listener() {

			private int[] minimized = new int[] { 4, 96 };
			private int[] currentSize = new int[] { 30, 70 };

			@Override
			public void showList(CTabFolderEvent event) {
			}

			@Override
			public void restore(CTabFolderEvent event) {
				tabFolder.setMinimized(false);
				if (tabFolder.getHorizontalBar() != null) {
					tabFolder.getHorizontalBar().setVisible(true);
				}
				if (tabFolder.getVerticalBar() != null) {
					tabFolder.getVerticalBar().setVisible(true);
				}
				s.setWeights(currentSize);
			}

			@Override
			public void minimize(CTabFolderEvent event) {
				currentSize = s.getWeights();
				s.setWeights(minimized);
				if (tabFolder.getHorizontalBar() != null) {
					tabFolder.getHorizontalBar().setVisible(false);
				}
				if (tabFolder.getVerticalBar() != null) {
					tabFolder.getVerticalBar().setVisible(false);
				}
				tabFolder.setMinimized(true);
			}

			@Override
			public void maximize(CTabFolderEvent event) {
			}

			@Override
			public void close(CTabFolderEvent event) {
			}
		});

		if (uifbNetworks != null) {
			for (Iterator<UIFBNetwork> iter = uifbNetworks.iterator(); iter
					.hasNext();) {
				UIFBNetwork app = iter.next();
				// FIX: There was a bug deleting applications which lead
				// to problems opening resource
				AutomationSystem system = app.getFbNetwork().getApplication().getAutomationSystem();
				// if system is null, it is possible that the
				// application was deleted with a buggy version of the
				// 4diac ide -> therefore do not add the app to the
				// resource editor
				if (system != null) {
					createTab(tabFolder, app.getFbNetwork().getApplication().getName(), app);
				}
			}
		}
		tabFolder.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateSelection();
			}
		});
		tabFolder.setSelection(0);
		updateSelection();
		super.createPartControl(s);

		// specify the relative weight of the two containing editors
		s.setWeights(new int[] { 30, 70 });
		
	}

	private final Hashtable<UIFBNetwork, CTabItem> tabItems = new Hashtable<UIFBNetwork, CTabItem>();

	private void createTab(CTabFolder tabFolder, 
			String title, UIFBNetwork app) {
		CTabItem item = new CTabItem(tabFolder, SWT.NULL);
		item.setText(title != null ? title : "");
		tabItems.put(app, item);
		UnmappedFBsEditor appEditor = getUnmappedFBsEditor();
		try {
			appEditor
					.init(getEditorSite(),
							new org.fordiac.ide.application.editors.ApplicationEditorInput(app.getFbNetwork().getApplication()));
		} catch (PartInitException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		item.setData(appEditor);
		appEditor.createPartControl(tabFolder);
	}

	protected UnmappedFBsEditor getUnmappedFBsEditor() {
		return new UnmappedFBsEditor();
	}

	private void updateSelection() {
		CTabItem item = tabFolder.getSelection();
		if (item != null && item.getData() != null) {
			UnmappedFBsEditor editor = (UnmappedFBsEditor) item.getData();
			item.setControl(editor.getRootParent());
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setFocus() {
		super.setFocus();
		for (Iterator iterator = getViewer().getRootEditPart().getChildren()
				.iterator(); iterator.hasNext();) {
			EditPart part = (EditPart) iterator.next();
			if (part instanceof FBNetworkContainerEditPart) {
				part.refresh();
			}
		}
	}

	@Override
	protected EditPartFactory getEditPartFactory() {
		return new ResourceDiagramEditPartFactory(this, getGraphicalViewer(), getZoomManger());
	}

	@Override
	protected ContextMenuProvider getContextMenuProvider(
			final ScrollingGraphicalViewer viewer,
			final ZoomManager zoomManager) {
		return new ResourceDiagramEditorContextMenuProvider(viewer, zoomManager, getActionRegistry());
	}

	@Override
	protected TransferDropTargetListener createTransferDropTargetListener() {		
		Palette palette = resource.getPaletteEntry().getGroup().getPallete();
		return new ApplicationEditorTemplateTransferDropTargetListener(
				getGraphicalViewer(), SystemManager.getInstance().getSystemForName(palette.getAutomationSystem().getName()));
	}

	@Override
	protected void setModel(final IEditorInput input) {
		if (input instanceof org.fordiac.ide.util.PersistableUntypedEditorInput) {
			org.fordiac.ide.util.PersistableUntypedEditorInput untypedInput = (org.fordiac.ide.util.PersistableUntypedEditorInput) input;
			Object content = untypedInput.getContent();
			if (content instanceof Resource) {
				resource = (Resource) content;
				resource.eAdapters().add(resourceAdapter);
				resourceView = SystemManager.getInstance()
						.getResourceViewForResource(resource);

				resourceDiagramManager.setUIResourceEditor(resourceView.getUIResourceDiagram());
				
				for (ResizeUIFBNetwork uiFBNetwork : resourceView
						.getUIResourceDiagram().getFbNetworks()) {
					if (uiFBNetwork.getUiFBNetwork().eIsProxy()) {
						// System.out.println("Application is Proxy");
						// application not loaded -> maybe problems loading it?
						// there was a bug which did not deleted the apps from
						// file system
					} else {
						uifbNetworks.add(uiFBNetwork.getUiFBNetwork());
					}
				}

				if (input.getName() != null) {
					setPartName(input.getName());
				}
			}
			setEditDomain(new DefaultEditDomain(this));
			// use one "System - Wide" command stack to avoid incositensies due
			// to
			// undo redo
			getEditDomain().setCommandStack(
					Activator.getDefault().getCommandStack(getSystem()));
		}
	}
	
	@Override
	public AutomationSystem getSystem() {
		return resource.getAutomationSystem();
	}

	@Override
	public String getFileName() {
		return "SysConf.xml";
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void createActions() {
		ActionRegistry registry = getActionRegistry();
		IAction action;

		action = new UnmapAction(this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
		
		action = new UnmapAllAction((IWorkbenchPart) this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		super.createActions();
		
		//TODO duplicated code from FBNetworkEditor
		//remove the select all action added in the graphical editor and replace it with our
		action = registry.getAction(ActionFactory.SELECT_ALL.getId());
		registry.removeAction(action);		
		getSelectionActions().remove(action.getId());
		action = new FBNetworkSelectAllAction((IWorkbenchPart) this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
		
		//we need a special delete action that will order connections before everything else		
		action = registry.getAction(ActionFactory.DELETE.getId());
		registry.removeAction(action);
		getSelectionActions().remove(action);		
		action = new DeleteFBNetworkAction((IWorkbenchPart) this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
	}

	@Override
	public void doSave(final IProgressMonitor monitor) {
		// // TODO __gebenh error handling if save fails!
		SystemManager.getInstance().saveSystem(getSystem(), true);
		getCommandStack().markSaveLocation();
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	@Override
	public void dispose() {

		if (adapter != null && getSystem() != null) {
			getSystem().removeApplicationListener(adapter);
		}
		
		if(null != resource){
			resource.eAdapters().remove(resourceAdapter);
		}

		super.dispose();
	}

	@Override
	protected PaletteRoot getPaletteRoot() {
		return null;  //we are filling a the palete directly in the viewer so we don't need it here
	}

	
	@Override
	protected PaletteViewerProvider createPaletteViewerProvider() {
		return new FBTypePaletteViewerProvider(getSystem().getProject(), getEditDomain());
	}
}
