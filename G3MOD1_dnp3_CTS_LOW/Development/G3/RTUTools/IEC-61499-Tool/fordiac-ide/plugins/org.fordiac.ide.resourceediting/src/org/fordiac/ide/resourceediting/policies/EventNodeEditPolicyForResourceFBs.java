/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.policies;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.ReconnectRequest;
import org.fordiac.ide.application.commands.AbstractEventConnectionCreateCommand;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.resourceediting.commands.EventConnectionCreateCommandForResourceFBs;
import org.fordiac.ide.resourceediting.commands.ReconnectEventConnectionCommandForResourceFBs;
import org.fordiac.ide.resourceediting.editparts.FBNetworkContainerEditPart;

/**
 * An EditPolicy which allows drawing Connections between FBs and/or SubApps.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class EventNodeEditPolicyForResourceFBs extends
		org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#
	 * getConnectionCompleteCommand
	 * (org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCompleteCommand(
			final CreateConnectionRequest request) {
		if (request.getStartCommand() instanceof AbstractEventConnectionCreateCommand) {
			AbstractEventConnectionCreateCommand command = (AbstractEventConnectionCreateCommand) request
					.getStartCommand();
			command.setTarget((InterfaceEditPart) getHost());
			return command;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getConnectionCreateCommand
	 * (org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCreateCommand(
			final CreateConnectionRequest request) {
		AbstractEventConnectionCreateCommand cmd = new EventConnectionCreateCommandForResourceFBs();
		cmd.setSource((InterfaceEditPart) getHost());
		EditPart parent = getHost().getParent();
		while (parent != null && !(parent instanceof FBNetworkContainerEditPart)) {
			parent = parent.getParent();
		}
		if (parent instanceof FBNetworkContainerEditPart) { 
			// also means that parent != null
			cmd.setParent(((FBNetworkContainerEditPart) parent).getCastedFBNetworkContainerModel()
					.getResourceEditor());
		}
		request.setStartCommand(cmd);
		return new EventConnectionCreateCommandForResourceFBs();
	}

	@Override
	protected Command getReconnectTargetCommand(final ReconnectRequest request) {
		return createReconnectCommand(request);
	}

	@Override
	protected Command getReconnectSourceCommand(final ReconnectRequest request) {
		return createReconnectCommand(request);
	}
	
	private Command createReconnectCommand(ReconnectRequest request) {
		ReconnectEventConnectionCommandForResourceFBs cmd = new ReconnectEventConnectionCommandForResourceFBs(request);
		cmd.setParent(getParentDiagram());
		return cmd;
	}
	
	private Diagram getParentDiagram() {
		EditPart parent = getHost().getParent();
		while (parent != null && !(parent instanceof FBNetworkContainerEditPart)) {
			parent = parent.getParent();
		}
		if (parent instanceof FBNetworkContainerEditPart) { 
			// also means that parent != null
			return ((FBNetworkContainerEditPart) parent).getCastedModel();
		}
		return null;
	}

}
