/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editors;

import org.eclipse.swt.widgets.Control;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.resourceediting.dnd.EditorDragSourceListener;

public class UnmappedFBsEditor extends FBNetworkEditor {

	@Override
	protected void initializeGraphicalViewer() {
		super.initializeGraphicalViewer();

		EditorDragSourceListener edsl = new EditorDragSourceListener(
				getGraphicalViewer());
		getGraphicalViewer().addDragSourceListener(edsl);
	}
	
	public Control getRootParent(){
		return getViewer().getControl().getParent().getParent();
	}
	
}
