/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editparts;

import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.fordiac.ide.gef.editparts.PropertiesInterfaceEditPart;

/**
 * The Class NotConnectableInterfaceEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class NotConnectableInterfaceEditPart extends PropertiesInterfaceEditPart {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getNodeEditPolicy()
	 */
	@Override
	protected GraphicalNodeEditPolicy getNodeEditPolicy() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// no editpolicies required is it is not "connectable"
	}
}
