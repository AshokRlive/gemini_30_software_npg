/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editparts;

import java.util.List;

import org.eclipse.draw2d.AncestorListener;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.fordiac.ide.gef.editparts.PropertiesInterfaceEditPart;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.resourceediting.policies.EventNodeEditPolicyForResourceFBs;
import org.fordiac.ide.resourceediting.policies.VariableNodeEditPolicyForResourceFBs;

/**
 * The Class InterfaceEditPartForResourceFBs.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class InterfaceEditPartForResourceFBs extends PropertiesInterfaceEditPart {

	@Override
	public void setSelected(int value) {
		super.setSelected(value);
	}
	
	@Override
	protected IFigure createFigureForModel() {
		IFigure fig = super.createFigureForModel();
		fig.addAncestorListener(new AncestorListener() {

			@Override
			public void ancestorRemoved(IFigure ancestor) {

			}

			@Override
			public void ancestorMoved(IFigure ancestor) {
				updateReferenced();
			}

			@Override
			public void ancestorAdded(IFigure ancestor) {
			}
		});

		return fig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getNodeEditPolicy()
	 */
	@Override
	protected GraphicalNodeEditPolicy getNodeEditPolicy() {
		if (isEvent()) {
			return new EventNodeEditPolicyForResourceFBs();
		}
		if (isVariable()) {
			return new VariableNodeEditPolicyForResourceFBs();
		}
		return null;
	}

	@Override
	protected List<?> getModelSourceConnections() {
		return super.getModelSourceConnections();
	}

	@Override
	protected List<?> getModelTargetConnections() {
		return super.getModelTargetConnections();
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getSourceConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(
			ConnectionEditPart connection) {
		return super.getSourceConnectionAnchor(connection);
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getSourceConnectionAnchor(org.eclipse.gef.Request)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(Request request) {
		return super.getSourceConnectionAnchor(request);
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getTargetConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(
			ConnectionEditPart connection) {
		return super.getTargetConnectionAnchor(connection);
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getTargetConnectionAnchor(org.eclipse.gef.Request)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(Request request) {
		return super.getTargetConnectionAnchor(request);
	}

	private void updateReferenced() {
		EditPart parent = getParent();
		while (parent != null && !(parent instanceof FBNetworkContainerEditPart)) {
			parent = parent.getParent();
		}
		if (parent != null) {
			FBNetworkContainerEditPart fbcep = (FBNetworkContainerEditPart) parent;
			InterfaceElementView referencedView = fbcep
					.getAssignedInterfaceElementView(getCastedModel()
							.getIInterfaceElement());

			if (referencedView != null) {
				Object o = getViewer().getEditPartRegistry().get(referencedView);
				String label = referencedView.getLabel();

				int x = 0;
				Rectangle bounds = getFigure().getBounds();
				if (o instanceof VirtualInOutputEditPart) {
					label = ((Label) ((VirtualInOutputEditPart) o).getFigure()).getText();
					if (!((VirtualInOutputEditPart) o).isInput()) {
						x = bounds.x - 20
								- FigureUtilities.getTextWidth(label, getFigure().getFont());
					} else {
						x = bounds.x + bounds.width + 1;

					}
				}
				Position pos = UiFactory.eINSTANCE.createPosition();
				pos.setX(x);
				pos.setY(bounds.y);
				referencedView.setPosition(pos);
			}
		}
	}

}
