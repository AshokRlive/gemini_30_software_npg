/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editparts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;
import org.eclipse.swt.graphics.Point;
import org.fordiac.ide.application.editparts.FBNetworkEditPart;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.ResourceTypeFB;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.model.ui.UiPackage;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.resourceediting.policies.MappingEditPolicy;

/**
 * The Class FBNetworkContainerEditPart.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class FBNetworkContainerEditPart extends FBNetworkEditPart {


	/** The content adapter. */
	EContentAdapter contentAdapter;
	

	FBNetworkContainer container;

	private final Map<IInterfaceElement, InterfaceElementView> interfaceElementViewMapping = new HashMap<IInterfaceElement, InterfaceElementView>();
	
	private final Map<InterfaceElementView, InterfaceElementView> elementViewMapping = new HashMap<InterfaceElementView, InterfaceElementView>();

	
	@Override
	protected EContentAdapter getContentAdapter(){
		if(null == contentAdapter){
			contentAdapter = new EContentAdapter() {
					@Override
					public void notifyChanged(Notification notification) {
						super.notifyChanged(notification);
						Object feature = notification.getFeature();
						if (UiPackage.eINSTANCE.getUIResourceEditor_ShowResizeUIFBNetworks()
								.equals(feature)
								|| UiPackage.eINSTANCE.getDiagram_Children().equals(feature)
								|| UiPackage.eINSTANCE.getUIResourceEditor_MappingEditorSize()
										.equals(feature)
								|| UiPackage.eINSTANCE.getSize_Height().equals(feature)
								|| UiPackage.eINSTANCE.getInterfaceElementView_InConnections()
										.equals(feature)
								|| UiPackage.eINSTANCE.getInterfaceElementView_OutConnections()
										.equals(feature)) {
							refreshChildren();
							refreshVisuals();
						}
					}
				};
		}
		return contentAdapter;
	}
	
	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public FBNetworkContainer getCastedFBNetworkContainerModel() {
		if ((getModel() instanceof UIResourceEditor) && (null == container)) {
			container = new FBNetworkContainer((UIResourceEditor) getModel());
		}
		return container;
	}


	/**
	 * Gets the main interface element view.
	 * 
	 * @param element
	 *          the element
	 * 
	 * @return the main interface element view
	 */
	public InterfaceElementView getMainInterfaceElementView(
			InterfaceElementView element) {
		return elementViewMapping.get(element);
	}

	/**
	 * Gets the assigned interface element view.
	 * 
	 * @param element
	 *          the element
	 * 
	 * @return the assigned interface element view
	 */
	public InterfaceElementView getAssignedInterfaceElementView(
			IInterfaceElement element) {
		return interfaceElementViewMapping.get(element);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected List getModelChildren() {
		interfaceElementViewMapping.clear();
		elementViewMapping.clear();
		ArrayList<Object> children = new ArrayList<Object>(super.getModelChildren()); 

		ArrayList<Object> interfaceElements = new ArrayList<Object>();
		UIResourceEditor uiResEditor = getCastedFBNetworkContainerModel().getResourceEditor();

		for (Object object : children) {
			if(object instanceof View){
				View view = (View)object;
				if (view instanceof MappedSubAppView) {
					MappedSubAppView subAppView = (MappedSubAppView) view;
					for (Iterator iterator2 = subAppView.getInterfaceElements().iterator(); iterator2
							.hasNext();) {
						MappedSubAppInterfaceElementView interfaceElementview = (MappedSubAppInterfaceElementView) iterator2
								.next();
						for (Iterator iterator3 = interfaceElementview.getInConnections()
								.iterator(); iterator3.hasNext();) {
							ConnectionView connView = (ConnectionView) iterator3.next();
							if (connView.getConnectionElement() instanceof EventConnection) {
								EventConnection eventCon = (EventConnection) connView
										.getConnectionElement();
								if (eventCon.getSource() != null) {
									Event source = eventCon.getSource();
									InterfaceList parentList = (InterfaceList) source.eContainer();
									if (parentList != null) {
										FB sourceFB = (FB) parentList.eContainer();
	
										if (!(sourceFB instanceof ResourceTypeFB || sourceFB.isResourceFB())
												&& !isMappedToEqualResource(uiResEditor, sourceFB)) {
											// createVirtualIO
											createVirtualIO(interfaceElements, (MappedSubAppView) view,
													eventCon.getDestination(), source);
										}
									}
								}
	
							}
						}
					}
				}
	
				if (view instanceof FBView) {
					FBView fbView = (FBView) view;
					FB fb = fbView.getFb();
					for (Iterator iterator2 = fb.getInterface().getEventInputs().iterator(); iterator2
							.hasNext();) {
						Event event = (Event) iterator2.next();
						for (Iterator iterator3 = event.getInputConnections().iterator(); iterator3
								.hasNext();) {
							EventConnection eventCon = (EventConnection) iterator3.next();
							if (eventCon.getSource() != null) {
								Event source = eventCon.getSource();
								InterfaceList parentList = (InterfaceList) source.eContainer();
								if (parentList != null) {
									FB sourceFB = (FB) parentList.eContainer();
	
									if (!(sourceFB instanceof ResourceTypeFB || sourceFB.isResourceFB())
											&& !isMappedToEqualResource(uiResEditor, sourceFB)) {
										createVirtualIO(interfaceElements, fbView, event, source);
									}
								}
							}
						}
					}
					for (Iterator iterator2 = fb.getInterface().getEventOutputs()
							.iterator(); iterator2.hasNext();) {
						Event event = (Event) iterator2.next();
						for (Iterator iterator3 = event.getOutputConnections().iterator(); iterator3
								.hasNext();) {
							EventConnection eventCon = (EventConnection) iterator3.next();
							if (eventCon.getDestination() != null) {
								Event dest = eventCon.getDestination();
								InterfaceList parentList = (InterfaceList) dest.eContainer();
								if (parentList != null) {
									FB destFB = (FB) parentList.eContainer();
									if (!(destFB instanceof ResourceTypeFB || destFB.isResourceFB())
											&& !isMappedToEqualResource(uiResEditor, destFB)) {
										createVirtualIO(interfaceElements, fbView, event, dest);
									}
								}
							}
	
						}
					}
					for (Iterator iterator2 = fb.getInterface().getInputVars().iterator(); iterator2
							.hasNext();) {
						VarDeclaration var = (VarDeclaration) iterator2.next();
						for (Iterator iterator3 = var.getInputConnections().iterator(); iterator3
								.hasNext();) {
							DataConnection dataCon = (DataConnection) iterator3.next();
							if (dataCon.getSource() != null) {
								VarDeclaration source = dataCon.getSource();
								InterfaceList parentList = (InterfaceList) source.eContainer();
								if (parentList != null) {
									FB sourceFB = (FB) parentList.eContainer();
									if (!(sourceFB instanceof ResourceTypeFB  || sourceFB.isResourceFB())
											&& !isMappedToEqualResource(uiResEditor, sourceFB)) {
										createVirtualIO(interfaceElements, fbView, var, source);
	
									}
								}
							}
	
						}
					}
					for (Iterator iterator2 = fb.getInterface().getOutputVars().iterator(); iterator2
							.hasNext();) {
						VarDeclaration var = (VarDeclaration) iterator2.next();
						for (Iterator iterator3 = var.getOutputConnections().iterator(); iterator3
								.hasNext();) {
							DataConnection dataCon = (DataConnection) iterator3.next();
							if (dataCon.getDestination() != null) {
								VarDeclaration dest = dataCon.getDestination();
								InterfaceList parentList = (InterfaceList) dest.eContainer();
								if (parentList != null) {
									FB destFB = (FB) parentList.eContainer();
									if (!(destFB instanceof ResourceTypeFB || destFB.isResourceFB())
											&& !isMappedToEqualResource(uiResEditor, destFB)) {
										createVirtualIO(interfaceElements, fbView, var, dest);
	
									}
								}
							}
	
						}
					}
				}
			}
		}
		
		interfaceElements.addAll(children);
		return interfaceElements;
	}

	private void createVirtualIO(List<Object> interfaceElements,
			FBView fbView, IInterfaceElement element, IInterfaceElement source) {
		InterfaceElementView iev = UiFactory.eINSTANCE.createInterfaceElementView();
		iev.setIInterfaceElement(source);
		interfaceElements.add(iev);
		interfaceElementViewMapping.put(element, iev);
		elementViewMapping.put(iev, getInterfaceElementViewForIInterfaceElement(
				fbView, element));
	}

	private void createVirtualIO(List<Object> interfaceElements,
			MappedSubAppView fbView, IInterfaceElement element,
			IInterfaceElement source) {
		InterfaceElementView iev = UiFactory.eINSTANCE.createInterfaceElementView();
		iev.setIInterfaceElement(source);
		interfaceElements.add(iev);
		interfaceElementViewMapping.put(element, iev);
		elementViewMapping.put(iev, getInterfaceElementViewForIInterfaceElement(
				fbView, element));
	}

	private boolean isMappedToEqualResource(UIResourceEditor uiResEditor, FB fb) {
		if (fb.eContainer() instanceof SubAppNetwork
				&& !(fb.eContainer() instanceof FBNetwork)) {
			SubAppNetwork subAppNetwork = (SubAppNetwork) fb.eContainer();
			EObject parent = subAppNetwork.eContainer();
			while (parent != null && !(parent instanceof SubAppView)) {
				parent = parent.eContainer();
			}
			if (parent == null) {
				return false;
			} else if (parent instanceof SubAppView) {
				SubAppView view = (SubAppView) parent;
				if (view.getMappedSubApp() != null) {
					MappedSubAppView mappedView = view.getMappedSubApp();
					if (mappedView.getSubApp() != null) {
						SubApp subApp = mappedView.getSubApp();
						if (subApp.getResource() != null) {
							return subApp.getResource().eContainer().equals(
									uiResEditor.getResourceElement());
						}
					}
				}
			}
			return false;
		}
		if (fb.getResource() == null || fb.getResource().eContainer() == null) {
			return false;
		}
		if (uiResEditor == null) {
			return false;
		}
		if (uiResEditor.getResourceElement() == null) {
			return false;
		}
		return uiResEditor.getResourceElement().equals(
				fb.getResource().eContainer());
	}

	private InterfaceElementView getInterfaceElementViewForIInterfaceElement(
			FBView view, IInterfaceElement element) {
		for (Iterator<InterfaceElementView> iterator = view.getInterfaceElements().iterator(); iterator
				.hasNext();) {
			InterfaceElementView ieView = (InterfaceElementView) iterator.next();
			if (ieView.getIInterfaceElement().equals(element)) {
				return ieView;
			}
		}
		return null;
	}

	private InterfaceElementView getInterfaceElementViewForIInterfaceElement(
			MappedSubAppView view, IInterfaceElement element) {
		for (Iterator<MappedSubAppInterfaceElementView> iterator = view.getInterfaceElements().iterator(); iterator
				.hasNext();) {
			InterfaceElementView ieView = (InterfaceElementView) iterator.next();
			if (ieView.getIInterfaceElement().equals(element)) {
				return ieView;
			}
		}
		return null;
	}


	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();

		installEditPolicy(EditPolicy.COMPONENT_ROLE, new RootComponentEditPolicy());		
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new MappingEditPolicy());
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractEditPart#performRequest(org.eclipse.gef
	 * .Request)
	 */
	@Override
	public void performRequest(final Request req) {
		Command cmd = getCommand(req);
		if (cmd != null && cmd.canExecute()) {
			getViewer().getEditDomain().getCommandStack().execute(cmd);
		}
		super.performRequest(req);
	}


	@Override
	protected void refreshVisuals() {
		Point p = getParent().getViewer().getControl().getSize();
		Rectangle rect = new Rectangle(0, 0, p.x, p.y);
		((GraphicalEditPart) getParent()).setLayoutConstraint(this, getFigure(), rect);
		super.refreshVisuals();
	}

}
