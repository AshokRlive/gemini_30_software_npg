/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.dnd;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.dnd.AbstractTransferDropTargetListener;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.swt.dnd.Transfer;
import org.fordiac.ide.application.commands.MapToCommand;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.resourceediting.editparts.FBNetworkContainerEditPart;

public class EditorDropTargetListener extends
		AbstractTransferDropTargetListener {

	private EditPart oldEditPart;

	public EditorDropTargetListener(EditPartViewer viewer, Transfer xfer) {
		super(viewer, xfer);
	}

	public EditorDropTargetListener(EditPartViewer viewer) {
		super(viewer, FBMappingTransfer.getInstance());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.dnd.AbstractTransferDropTargetListener#updateTargetRequest
	 * ()
	 */
	@Override
	protected void updateTargetRequest() {
		EditPart part = getViewer().findObjectAt(getDropLocation());
		CreateRequest request = new CreateRequest();
		if (oldEditPart == null) {
			oldEditPart = part;
		}
		if (!part.equals(oldEditPart)) {
			oldEditPart.eraseTargetFeedback(request);
			oldEditPart = part;
		}

	}

	@Override
	protected Request createTargetRequest() {
		ChangeBoundsRequest request = new ChangeBoundsRequest();
		request.setType(RequestConstants.REQ_ADD);
		FBMappingTransfer tf = (FBMappingTransfer) getTransfer();
		FBEditPart fbEditPart = (FBEditPart) tf.getElement();
		request.setEditParts(fbEditPart);
		return request;
	}

	@Override
	protected void handleDrop() {
		// MapToCommand cmd = new MapToCommand(fb, uiResourceEditor)

		EditPart part = getViewer().findObjectAt(getDropLocation());
		UIResourceEditor uiResourceEditor = null;
		if (part.getChildren().size() == 1) {
			Object temp = part.getChildren().get(0);
			if (temp instanceof FBNetworkContainerEditPart) {
				FBNetworkContainerEditPart fbnetworkCEP = (FBNetworkContainerEditPart) temp;
				uiResourceEditor = fbnetworkCEP.getCastedFBNetworkContainerModel()
						.getResourceEditor();
			}
		}
		if (getTargetRequest() instanceof ChangeBoundsRequest) {
			ChangeBoundsRequest req = (ChangeBoundsRequest) getTargetRequest();
			if (req.getEditParts().size() == 1) {
				Object obj = req.getEditParts().get(0);

				if (obj instanceof FBEditPart && uiResourceEditor != null) {
					FBEditPart fbpart = (FBEditPart) obj;
					MapToCommand cmd = new MapToCommand(
							fbpart.getCastedModel(), uiResourceEditor);
					cmd.setPosition(new Point(getDropLocation().x,
							getDropLocation().y));
					if (cmd.canExecute()) {
						if (getViewer() != null
								&& getViewer().getEditDomain() != null
								&& getViewer().getEditDomain()
										.getCommandStack() != null) {

							getViewer().getEditDomain().getCommandStack()
									.execute(cmd);
						}
						else {
							cmd.execute();
						}
					}
				}
			}
		}
		// if (part instanceof ResourceEditPart) {
		// super.handleDrop();
		//
		// }
	}
}