/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editparts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.fordiac.ide.application.figures.SubAppForFbNetworkFigure;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.gef.editparts.ValueEditPart;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * The Class MappedSubAppForFBNetworkEditPart.
 */
public class MappedSubAppForFBNetworkEditPart extends AbstractViewEditPart {

	// protected DirectEditManager manager;

	/** The show all outputs. */
	private boolean showAllOutputs = false;

	/** The show event inputs. */
	private boolean showEventInputs = false;

	/** The show var inputs. */
	private boolean showVarInputs = false;

	private static ZoomManager zoomManager; 
	/**
	 * Checks if is show all outputs.
	 * 
	 * @return true, if is show all outputs
	 */
	public boolean isShowAllOutputs() {
		return showAllOutputs;
	}

	/**
	 * Sets the show all outputs.
	 * 
	 * @param showAllOutputs the new show all outputs
	 */
	public void setShowAllOutputs(final boolean showAllOutputs) {
		this.showAllOutputs = showAllOutputs;
	}

	/**
	 * Checks if is show event inputs.
	 * 
	 * @return true, if is show event inputs
	 */
	public boolean isShowEventInputs() {
		return showEventInputs;
	}

	/**
	 * Sets the show event inputs.
	 * 
	 * @param showEventInputs the new show event inputs
	 */
	public void setShowEventInputs(final boolean showEventInputs) {
		this.showEventInputs = showEventInputs;
	}

	/**
	 * Checks if is show var inputs.
	 * 
	 * @return true, if is show var inputs
	 */
	public boolean isShowVarInputs() {
		return showVarInputs;
	}

	/**
	 * Sets the show var inputs.
	 * 
	 * @param showVarInputs the new show var inputs
	 */
	public void setShowVarInputs(final boolean showVarInputs) {
		this.showVarInputs = showVarInputs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		// UISubApp uiSubApp = getCastedModel();

		IFigure f = super.createFigure();

		return f;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#createFigureForModel()
	 */
	@Override
	protected IFigure createFigureForModel() {
		SubAppForFbNetworkFigure figure = new SubAppForFbNetworkFigure(
				getCastedModel().getSubApp(), zoomManager);
		return figure;
	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public MappedSubAppView getCastedModel() {
		return (MappedSubAppView) getModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected List getModelChildren() {
		ArrayList<SubAppInterfaceElementView> elements = new ArrayList<SubAppInterfaceElementView>();
		for (Iterator<MappedSubAppInterfaceElementView> iterator = getCastedModel()
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			SubAppInterfaceElementView interfaceElementView = iterator.next();
			if (interfaceElementView.getApplicationInterfaceElement() instanceof SubAppInterfaceElementView) {
				SubAppInterfaceElementView appSubAppInterfaceElementView = (SubAppInterfaceElementView) interfaceElementView
						.getApplicationInterfaceElement();
				if (appSubAppInterfaceElementView.isVisible()) {
					interfaceElementView
							.setVisible(appSubAppInterfaceElementView
									.isVisible());
					elements.add(interfaceElementView);
				}
			}
		}
		return elements;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#addChildVisual(org.eclipse.gef.EditPart,
	 *      int)
	 */
	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof InterfaceEditPart) {
			InterfaceEditPart interfaceEditPart = (InterfaceEditPart) childEditPart;
			if (interfaceEditPart.isInput()) {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventInputs().add(child);
				} else if (interfaceEditPart.isVariable()) {
					getCastedFigure().getDataInputs().add(child);
				}
			} else {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventOutputs().add(child);
				} else if (interfaceEditPart.isVariable()) {
					getCastedFigure().getDataOutputs().add(child);
				}

			}
		} else if (childEditPart instanceof ValueEditPart) {
			ValueEditPart valueEditPart = (ValueEditPart) childEditPart;
			if (((VarDeclaration) valueEditPart.getCastedModel().eContainer())
					.getInputConnections().size() > 0) {
				valueEditPart.setVisible(false);
			}
			if (valueEditPart.getCastedModel().getValue() == null) {
				valueEditPart.setVisible(false);
			}
			getCastedFigure().getInputValuesContentPane().add(child);
		} else {
			super.addChildVisual(childEditPart, index);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#removeChildVisual(org.eclipse.gef.EditPart)
	 */
	@Override
	protected void removeChildVisual(final EditPart childEditPart) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof InterfaceEditPart) {
			InterfaceEditPart interfaceEditPart = (InterfaceEditPart) childEditPart;
			if (interfaceEditPart.isInput()) {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventInputs().remove(child);
				} else if (interfaceEditPart.isVariable()) {
					getCastedFigure().getDataInputs().remove(child);
				}
			} else {
				if (interfaceEditPart.isEvent()) {
					getCastedFigure().getEventOutputs().remove(child);
				} else if (interfaceEditPart.isVariable()) {
					getCastedFigure().getDataOutputs().remove(child);
				}

			}
		} else if (childEditPart instanceof ValueEditPart) {
			getCastedFigure().getInputValuesContentPane().remove(child);
		} else {
			super.removeChildVisual(childEditPart);
		}
	}

	/** The adapter. */
	private EContentAdapter adapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getContentAdapter()
	 */
	@Override
	public EContentAdapter getContentAdapter() {
		if (adapter == null) {
			adapter = new EContentAdapter() {

				@Override
				public void notifyChanged(final Notification notification) {
					Object feature = notification.getFeature();
					if (UiPackage.eINSTANCE
							.getMappedSubAppView_InterfaceElements().equals(
									feature)) {
						refresh();
					} else if (UiPackage.eINSTANCE
							.getSubAppInterfaceElementView_Visible().equals(
									feature)) {
						refresh();
					} else if (LibraryElementPackage.eINSTANCE
							.getINamedElement_Name().equals(feature)) {
						refresh();
					}
				}
			};
		}
		return adapter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#refresh()
	 */
	@Override
	public void refresh() {
		super.refresh();
		refreshInterfaceLabels();
	}

	/**
	 * Refresh interface labels.
	 */
	@SuppressWarnings("rawtypes")
	private void refreshInterfaceLabels() {
		for (Iterator iterator = getChildren().iterator(); iterator.hasNext();) {
			EditPart ep = (EditPart) iterator.next();
			if (ep instanceof InterfaceEditPart) {
				((InterfaceEditPart) ep).refreshName();
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.iec61499.ui.editparts.AbstractEMFEditPart#getPreferenceChangeListener()
	 */
	@Override
	public IPropertyChangeListener getPreferenceChangeListener() {
		return new IPropertyChangeListener() {
			public void propertyChange(final PropertyChangeEvent event) {
				// not used in this version
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#getEditableValue()
	 */
	public Object getEditableValue() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#getPropertyValue(java.lang.Object)
	 */
	public Object getPropertyValue(final Object id) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#isPropertySet(java.lang.Object)
	 */
	public boolean isPropertySet(final Object id) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#resetPropertyValue(java.lang.Object)
	 */
	public void resetPropertyValue(final Object id) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#setPropertyValue(java.lang.Object,
	 *      java.lang.Object)
	 */
	public void setPropertyValue(final Object id, final Object value) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#performRequest(org.eclipse.gef.Request)
	 */
	@Override
	public void performRequest(final Request request) {
		// if (request.getType().equals(RequestConstants.REQ_OPEN)) {
		// new
		// OpenSubApplicationEditorAction(getCastedModel().getUiSubAppNetwork()).run();
		// }
		super.performRequest(request);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getINamedElement()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel().getSubApp();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getNameLabel()
	 */
	@Override
	public Label getNameLabel() {
		if (getCastedFigure() != null) {
			return getCastedFigure().getInstanceNameLabel();
		}
		return null;
	}

	/**
	 * Gets the casted figure.
	 * 
	 * @return the casted figure
	 */
	private SubAppForFbNetworkFigure getCastedFigure() {
		if (getFigure() instanceof SubAppForFbNetworkFigure) {
			return (SubAppForFbNetworkFigure) getFigure();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getInterfaceElements()
	 */
	@Override
	public List<? extends InterfaceElementView> getInterfaceElements() {
		return getCastedModel().getInterfaceElements();
	}

}
