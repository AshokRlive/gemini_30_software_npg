/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editparts;

import org.fordiac.ide.model.ui.UIResourceEditor;

/**
 * The Class FBNetworkContainer.
 */
public class FBNetworkContainer {

	/** The resource editor. */
	UIResourceEditor resourceEditor;

	/**
	 * Instantiates a new fB network container.
	 * 
	 * @param resourceEditor
	 *          the resource editor
	 */
	public FBNetworkContainer(final UIResourceEditor resourceEditor) {
		this.resourceEditor = resourceEditor;
	}

	/**
	 * Gets the resource editor.
	 * 
	 * @return the resource editor
	 */
	public UIResourceEditor getResourceEditor() {
		return resourceEditor;
	}

	/**
	 * Sets the resource editor.
	 * 
	 * @param resourceEditor
	 *          the new resource editor
	 */
	public void setResourceEditor(final UIResourceEditor resourceEditor) {
		this.resourceEditor = resourceEditor;
	}
}