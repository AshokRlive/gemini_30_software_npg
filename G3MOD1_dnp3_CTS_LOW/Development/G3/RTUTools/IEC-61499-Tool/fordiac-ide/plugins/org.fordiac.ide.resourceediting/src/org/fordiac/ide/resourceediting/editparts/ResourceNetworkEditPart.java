/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.gef.editparts.AbstractDiagramEditPart;
import org.fordiac.ide.gef.editparts.IDiagramEditPart;
import org.fordiac.ide.gef.router.RouterUtil;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.UIResourceEditor;

/**
 * Edit Part for the visualization of FBNetworks.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ResourceNetworkEditPart extends AbstractDiagramEditPart implements
		IDiagramEditPart {

	/** The adapter. */
	private EContentAdapter adapter;

	/**
	 * Creates the <code>Figure</code> to be used as this part's <i>visuals</i>.
	 * 
	 * @return a figure
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {

		Figure f = new FreeformLayer();
		// f.setBorder(new MarginBorder(10));
		f.setLayoutManager(new FreeformLayout());
		f.setOpaque(false);
		// Create the static router for the connection layer
		ConnectionLayer connLayer = (ConnectionLayer) getLayer(LayerConstants.CONNECTION_LAYER);
		connLayer.setConnectionRouter(RouterUtil.getConnectionRouter(f));
		return f;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (!isActive()) {
			super.activate();
			((Notifier) getModel()).eAdapters().add(getContentAdapter());
			if (getPreferenceChangeListener() != null) {
				ApplicationPlugin.getDefault().getPreferenceStore()
						.addPropertyChangeListener(getPreferenceChangeListener());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((Notifier) getModel()).eAdapters().remove(getContentAdapter());
			if (getPreferenceChangeListener() != null) {
				ApplicationPlugin.getDefault().getPreferenceStore()
						.removePropertyChangeListener(getPreferenceChangeListener());
			}
		}
	}

	/**
	 * Gets the content adapter.
	 * 
	 * @return the content adapter
	 */
	public EContentAdapter getContentAdapter() {
		if (adapter == null) {
			adapter = new EContentAdapter() {
				@Override
				public void notifyChanged(final Notification notification) {
					int type = notification.getEventType();
					switch (type) {
					case Notification.ADD:
					case Notification.ADD_MANY:
					case Notification.REMOVE:
					case Notification.REMOVE_MANY:
						refreshChildren();
						break;
					case Notification.SET:
						break;
					}
				}
			};
		}
		return adapter;
	}

	/**
	 * Creates the EditPolicies used for this EditPart.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new RootComponentEditPolicy());

	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public UIResourceEditor getCastedModel() {
		return (UIResourceEditor) getModel();
	}

	/**
	 * The Class ApplicationsContainer.
	 */
	public class ApplicationsContainer {

		/** The resource editor. */
		UIResourceEditor resourceEditor;

		/**
		 * Instantiates a new applications container.
		 * 
		 * @param resourceEditor
		 *          the resource editor
		 */
		public ApplicationsContainer(final UIResourceEditor resourceEditor) {
			this.resourceEditor = resourceEditor;
		}

		/**
		 * Gets the resource editor.
		 * 
		 * @return the resource editor
		 */
		public UIResourceEditor getResourceEditor() {
			return resourceEditor;
		}

		/**
		 * Sets the resource editor.
		 * 
		 * @param resourceEditor
		 *          the new resource editor
		 */
		public void setResourceEditor(final UIResourceEditor resourceEditor) {
			this.resourceEditor = resourceEditor;
		}
	}

	/** The root. */
	ApplicationsContainer root;

	/** The fb network container. */
	FBNetworkContainer fbNetworkContainer;

	/**
	 * Returns the children of the FBNetwork.
	 * 
	 * @return the list of children
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected List getModelChildren() {
		if (root == null) {
			root = new ApplicationsContainer(getCastedModel());
		}
		if (fbNetworkContainer == null) {
			fbNetworkContainer = new FBNetworkContainer(getCastedModel());
		}
		ArrayList<Object> children = new ArrayList<Object>();
		children.add(root);
		children.add(fbNetworkContainer);

		return children;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.fordiac.iec61499.ui.editparts.AbstractEMFEditPart#
	 * getPreferenceChangeListener()
	 */
	/**
	 * Gets the preference change listener.
	 * 
	 * @return the preference change listener
	 */
	@Override
	public IPropertyChangeListener getPreferenceChangeListener() {
		return null;
	}

	/**
	 * Gets the property descriptors.
	 * 
	 * @return the property descriptors
	 */
	public IPropertyDescriptor[] getPropertyDescriptors() {
		// unused
		return new IPropertyDescriptor[0];
	}

	/**
	 * Gets the editable value.
	 * 
	 * @return the editable value
	 */
	public Object getEditableValue() {
		// unused
		return null;
	}

	/**
	 * Gets the property value.
	 * 
	 * @param id
	 *          the id
	 * 
	 * @return the property value
	 */
	public Object getPropertyValue(final Object id) {
		// unused
		return null;
	}

	/**
	 * Checks if is property set.
	 * 
	 * @param id
	 *          the id
	 * 
	 * @return true, if is property set
	 */
	public boolean isPropertySet(final Object id) {
		// unused
		return false;
	}

	/**
	 * Reset property value.
	 * 
	 * @param id
	 *          the id
	 */
	public void resetPropertyValue(final Object id) {
		// unused
	}

	/**
	 * Sets the property value.
	 * 
	 * @param id
	 *          the id
	 * @param value
	 *          the value
	 */
	public void setPropertyValue(final Object id, final Object value) {
		// unused
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.IDiagramEditPart#getDiagram()
	 */
	@Override
	public Diagram getDiagram() {
		return getCastedModel();
	}

}
