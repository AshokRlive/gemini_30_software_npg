/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.model.ui.Size;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.util.Utils;

/**
 * The Class ResizeCommand.
 */
public class ResizeCommand extends Command {

	/** The resource editor. */
	private final UIResourceEditor resourceEditor;

	/** The resize height. */
	private int resizeHeight = 0;

	/** The editor. */
	private IEditorPart editor;

	/**
	 * Instantiates a new resize command.
	 * 
	 * @param resourceEditor the resource editor
	 */
	public ResizeCommand(final UIResourceEditor resourceEditor) {
		this.resourceEditor = resourceEditor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		Size size = resourceEditor.getMappingEditorSize();
		if (size == null) {
			size = UiFactory.eINSTANCE.createSize();
			size.setHeight(200);
			size.setWidth(-1);
		}
		size.setHeight(size.getHeight() + resizeHeight);
	}

	/**
	 * Sets the resize height.
	 * 
	 * @param height the new resize height
	 */
	public void setResizeHeight(final int height) {
		resizeHeight = height;
	}
}
