/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.dnd;

import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.dnd.AbstractTransferDragSourceListener;
import org.eclipse.jface.util.TransferDragSourceListener;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;
import org.fordiac.ide.application.editparts.FBEditPart;

public class EditorDragSourceListener implements TransferDragSourceListener {

	private EditPartViewer viewer;
	private Transfer transfer;

	public EditorDragSourceListener(EditPartViewer viewer) {
		setViewer(viewer);
		setTransfer(FBMappingTransfer.getInstance());
	}

	/**
	 * @see AbstractTransferDragSourceListener#dragFinished(DragSourceEvent)
	 */
	public void dragFinished(DragSourceEvent event) {
		FBMappingTransfer.getInstance().setElement(null);
		getViewer().setSelection(StructuredSelection.EMPTY);
	}

	/**
	 * Get the <i>template</i> from the selected and sets it as the event data to
	 * be dropped.
	 * 
	 * @param event
	 *          the DragSourceEvent
	 */
	public void dragSetData(DragSourceEvent event) {
		event.data = getTemplate();
	}

	/**
	 * Cancels the drag if the selected item does not represent a UIFB
	 * (FBEditPart).
	 * 
	 * @see org.eclipse.swt.dnd.DragSourceListener#dragStart(DragSourceEvent)
	 */
	public void dragStart(DragSourceEvent event) {
		Object template = getTemplate();
		if (template == null || !(template instanceof FBEditPart)) {
			event.doit = false;
		}
		FBMappingTransfer.getInstance().setElement(template);
	}

	/**
	 * A helper method that returns <code>null</code> or the <i>template</i>
	 * Object from the currently selected EditPart.
	 * 
	 * @return the template
	 */
	@SuppressWarnings("rawtypes")
	protected Object getTemplate() {
		List selection = getViewer().getSelectedEditParts();
		if (selection.size() == 1) {
			EditPart editPart = (EditPart) getViewer().getSelectedEditParts().get(0);
			if (editPart instanceof FBEditPart) {
				return editPart;
			}
		}
		return null;
	}

	/**
	 * @see TransferDragSourceListener#getTransfer()
	 */
	public Transfer getTransfer() {
		return transfer;
	}

	/**
	 * Returns the <code>EditPartViewer</code>.
	 * 
	 * @return the EditPartViewer
	 */
	protected EditPartViewer getViewer() {
		return viewer;
	}

	/**
	 * Sets the <code>Transfer</code> for this listener.
	 * 
	 * @param xfer
	 *          the Transfer
	 */
	protected void setTransfer(Transfer xfer) {
		transfer = xfer;
	}

	/**
	 * Sets the EditPartViewer for this listener.
	 * 
	 * @param viewer
	 *          the EditPartViewer
	 */
	protected void setViewer(EditPartViewer viewer) {
		this.viewer = viewer;
	}
}