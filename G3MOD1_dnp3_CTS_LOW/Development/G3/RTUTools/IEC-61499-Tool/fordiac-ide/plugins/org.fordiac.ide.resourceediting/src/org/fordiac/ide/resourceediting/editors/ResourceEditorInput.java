/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editors;

import org.eclipse.ui.IMemento;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.util.PersistableUntypedEditorInput;

public class ResourceEditorInput extends PersistableUntypedEditorInput {

	public ResourceEditorInput(Resource res) {
		super(res, getResourceEditorName(res), getResourceEditorName(res));
	}
	
	@Override
	public void saveState(IMemento memento) {
		ResourceEditorInputFactory.saveState(memento, this);
		
	}

	@Override
	public String getFactoryId() {
		return ResourceEditorInputFactory.getFactoryId();
	}
	
	@Override
	public Resource getContent(){
		return (Resource)super.getContent();
	}
	
	public static String getResourceEditorName(Resource res){
		return res.getDevice().getName() + "." + res.getName();
	}

}
