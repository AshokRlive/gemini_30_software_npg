/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.commands;

import org.fordiac.ide.application.commands.AbstractEventConnectionCreateCommand;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.util.commands.ConnectionUtil;

/**
 * The Class EventConnectionCreateCommandForResourceFBs.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class EventConnectionCreateCommandForResourceFBs extends
		AbstractEventConnectionCreateCommand {

	/** The resource connection. */
	protected ConnectionView appConnection;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.application.commands.AbstractEventConnectionCreateCommand#doAdvancedCreation()
	 */
	@Override
	protected void doAdvancedCreation() {
		InterfaceElementView appSource = sourceView
				.getApplicationInterfaceElement();
		InterfaceElementView appDest = destinationView
				.getApplicationInterfaceElement();
		//
		appConnection = ConnectionUtil.createConnectionInApp(appSource,
				appDest, connectionView);
		
		if (appConnection == null) {
			connectionView.getConnectionElement().setResourceConnection(true);
		}
	}

	@Override
	protected void redoAdvancedCreation() {

		Connection connectionElement = appConnection.getConnectionElement();
		if (appParent != null && appParent instanceof UIFBNetwork) {
			appParent.getConnections().add(appConnection);
			UIFBNetwork uiFBNetwork = (UIFBNetwork) appParent;
			SubAppNetwork fbNetwork = uiFBNetwork.getFunctionBlockNetwork();

			if (fbNetwork != null) {
				if (connectionElement instanceof EventConnection) {
					fbNetwork.getEventConnections().add(
							(EventConnection) connectionElement);
				}
				if (connectionElement instanceof DataConnection) {
					fbNetwork.getDataConnections().add(
							(DataConnection) connectionElement);
				}
			}
		}
		appConnection.setSource(source);
		appConnection.setDestination(dest);

	}

	private Diagram appParent;
	private InterfaceElementView source;
	private InterfaceElementView dest;

	@Override
	protected void undoAdvancedCreation() {
		if (appConnection != null) {
			appParent = (Diagram) appConnection.eContainer();
			source = appConnection.getSource();
			dest = appConnection.getDestination();

			appParent.getConnections().remove(appConnection);

			Connection connectionElement = appConnection.getConnectionElement();

			if (appParent != null && appParent instanceof UIFBNetwork) {
				UIFBNetwork uiFBNetwork = (UIFBNetwork) appParent;
				SubAppNetwork fbNetwork = uiFBNetwork.getFunctionBlockNetwork();
				if (fbNetwork != null) {
					if (connectionElement instanceof EventConnection) {
						fbNetwork.getEventConnections().remove(
								connectionElement);
					}
					if (connectionElement instanceof DataConnection) {
						fbNetwork.getDataConnections()
								.remove(connectionElement);
					}
				}
			}

			appConnection.setSource(null);
			appConnection.setDestination(null);
		}
	}

}
