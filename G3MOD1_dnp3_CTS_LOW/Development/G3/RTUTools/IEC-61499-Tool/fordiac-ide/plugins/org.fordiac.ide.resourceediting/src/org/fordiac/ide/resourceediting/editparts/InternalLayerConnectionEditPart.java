/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editparts;

import org.eclipse.draw2d.IFigure;
import org.fordiac.ide.application.editparts.ConnectionEditPart;

/**
 * The Class InternalLayerConnectionEditPart.
 */
public class InternalLayerConnectionEditPart extends ConnectionEditPart {

	/** The connection layer. */
	private final IFigure connectionLayer;

	/**
	 * Instantiates a new internal layer connection edit part.
	 * 
	 * @param layer the layer
	 */
	public InternalLayerConnectionEditPart(final IFigure layer) {
		this.connectionLayer = layer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#getLayer(java.lang.Object)
	 */
	@Override
	protected IFigure getLayer(final Object layer) {
		return connectionLayer;
	}
}
