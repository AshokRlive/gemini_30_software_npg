/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editparts;

import java.util.List;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.fordiac.ide.gef.FixedAnchor;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.impl.EventImpl;
import org.fordiac.ide.model.libraryElement.impl.VarDeclarationImpl;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.util.imageprovider.FordiacImage;

/**
 * This class implements an EditPart for an "VirtualInOutput". It is required if
 * a connection is "brocken" when mapped. (fbs distributed to different
 * resources)
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class VirtualInOutputEditPart extends AbstractViewEditPart implements
		NodeEditPart {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#activate()
	 */
	@Override
	public void activate() {
		super.activate();
		updatePos();
	}

	private EContentAdapter adapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getContentAdapter()
	 */
	@Override
	public EContentAdapter getContentAdapter() {
		if (adapter == null) {
			adapter = new EContentAdapter() {

				@Override
				public void notifyChanged(final Notification notification) {
					super.notifyChanged(notification);
					refreshSourceConnections();
					refreshTargetConnections();
					refreshVisuals();
					refreshTooltip();
				}

			};
		}
		return adapter;
	}

	private void refreshTooltip() {
		getFigure().setToolTip(new VirtualIOTooltipFigure());

	}

	@Override
	protected void refreshVisuals() {

		super.refreshVisuals();
	}

	/** The oldx. */
	int oldx = 0;

	/** The oldy. */
	int oldy = 0;

	private void updatePos() {
		if (getParent() instanceof FBNetworkContainerEditPart) {
			FBNetworkContainerEditPart fbnce = (FBNetworkContainerEditPart) getParent();
			InterfaceElementView view = fbnce
					.getMainInterfaceElementView(getCastedModel());
			Object o = getViewer().getEditPartRegistry().get(view);
			if (o instanceof InterfaceEditPartForResourceFBs) {
				InterfaceEditPartForResourceFBs iep = (InterfaceEditPartForResourceFBs) o;

				String label = ((Label) getFigure()).getText();

				Rectangle bounds = iep.getFigure().getBounds();
				Position pos = UiFactory.eINSTANCE.createPosition();
				int x = 0;
				if (!isInput()) {
					x = bounds.x
							- 20
							- FigureUtilities.getTextWidth(label, getFigure()
									.getFont());
				} else {
					x = bounds.x + bounds.width + 1;

				}
				int y = bounds.y;
				if (x != oldx && y != oldy) {
					if (getCastedModel().getPosition() != null) {
						pos = getCastedModel().getPosition();
					}
					pos.setX(x);
					pos.setY(bounds.y);
					getCastedModel().setPosition(pos);
					oldx = x;
					oldy = y;
				}
			}
		}
	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public InterfaceElementView getCastedModel() {
		return (InterfaceElementView) getModel();
	}

	/**
	 * Checks if is input.
	 * 
	 * @return true, if is input
	 */
	public boolean isInput() {
		return getCastedModel().getIInterfaceElement().isIsInput();
	}

	/**
	 * Checks if is event.
	 * 
	 * @return true, if is event
	 */
	public boolean isEvent() {
		return getCastedModel().getIInterfaceElement() instanceof EventImpl;
	}

	/**
	 * Checks if is variable.
	 * 
	 * @return true, if is variable
	 */
	public boolean isVariable() {
		return getCastedModel().getIInterfaceElement() instanceof VarDeclarationImpl;
	}

//	private IInterfaceElement getIInterfaceElement() {
//		return getCastedModel().getIInterfaceElement();
//	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		// overwrite the rename edit policy - no rename possible
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractEditPart#understandsRequest(org.eclipse
	 * .gef.Request)
	 */
	@Override
	public boolean understandsRequest(Request request) {
		if (request.getType() == RequestConstants.REQ_MOVE) {
			return false;
		}
		if (request.getType() == RequestConstants.REQ_DIRECT_EDIT
				|| request.getType() == RequestConstants.REQ_OPEN) {
			return false;
		}
		return super.understandsRequest(request);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#performRequest(org
	 * .eclipse.gef.Request)
	 */
	@Override
	public void performRequest(Request request) {
		if (request.getType() == RequestConstants.REQ_DIRECT_EDIT
				|| request.getType() == RequestConstants.REQ_OPEN) {
			return;
		}
		super.performRequest(request);
	}

	/**
	 * The Class VirtualInputOutputFigure.
	 */
	public class VirtualInputOutputFigure extends Label {

		/**
		 * Instantiates a new virtual input output figure.
		 */
		public VirtualInputOutputFigure() {
			super();
			// FB fb = (FB) getCastedModel().getIInterfaceElement().eContainer()
			// .eContainer();
			// setText(fb.getName() + "." + getCastedModel().getLabel());
			// setBorder(new MarginBorder(0, 5, 0, 5));
			// setBorder(new ConnectorBorder());
			setOpaque(false);
			if (isInput()) {
				setIcon(FordiacImage.ICON_LinkOutput.getImage());
				setLabelAlignment(PositionConstants.LEFT);
				setTextAlignment(PositionConstants.LEFT);
			} else {
				setIcon(FordiacImage.ICON_LinkInput.getImage());
				setLabelAlignment(PositionConstants.RIGHT);
				setTextAlignment(PositionConstants.RIGHT);
			}
			setToolTip(new VirtualIOTooltipFigure());
			setSize(-1, -1);
		}

	}

	private class VirtualIOTooltipFigure extends Figure {
		public VirtualIOTooltipFigure() {

			setLayoutManager(new BorderLayout());
			IFigure leftCol = new Figure();
			IFigure rightCol = new Figure();
			leftCol.setLayoutManager(new ToolbarLayout());
			rightCol.setLayoutManager(new ToolbarLayout());
			add(leftCol, BorderLayout.LEFT);
			add(rightCol, BorderLayout.CENTER);
			add(new Label(getCastedModel().getLabel()), BorderLayout.TOP);

			FB fb = (FB) getCastedModel().getIInterfaceElement().eContainer()
					.eContainer();
			if (fb == null) {
				return;
			}
			if (fb.getResource() == null) {
				return;
			}

			Resource res = (Resource) fb.getResource().eContainer();
			if (res == null) {
				return;
			}
			Device dev = res.getDevice();
			if (dev == null) {
				return;
			}

			add(new Label(dev.getName() + "." + res.getName() + "."
					+ fb.getName() + "." + getCastedModel().getLabel()),
					BorderLayout.TOP);

		}
	}

	@Override
	protected IFigure createFigureForModel() {
		IFigure f = new VirtualInputOutputFigure();
		return f; // new VirtualInputOutputFigure();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getINamedElement()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel().getIInterfaceElement();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef
	 * .ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(
			final ConnectionEditPart connection) {
		return new FixedAnchor(getFigure(), isInput());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef
	 * .Request)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(final Request request) {
		return new FixedAnchor(getFigure(), isInput());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef
	 * .ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(
			final ConnectionEditPart connection) {
		return new FixedAnchor(getFigure(), isInput());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef
	 * .Request)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(final Request request) {
		return new FixedAnchor(getFigure(), isInput());
	}

	@Override
	protected List<?> getModelSourceConnections() {
		return getCastedModel().getOutConnections();
	}

	@Override
	protected List<?> getModelTargetConnections() {
		return getCastedModel().getInConnections();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getInterfaceElements()
	 */
	@Override
	public List<? extends InterfaceElementView> getInterfaceElements() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getNameLabel()
	 */
	@Override
	public Label getNameLabel() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#
	 * getPreferenceChangeListener()
	 */
	@Override
	public IPropertyChangeListener getPreferenceChangeListener() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#getEditableValue()
	 */
	@Override
	public Object getEditableValue() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#getPropertyValue(java
	 * .lang.Object)
	 */
	@Override
	public Object getPropertyValue(Object id) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#isPropertySet(java.lang
	 * .Object)
	 */
	@Override
	public boolean isPropertySet(Object id) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#resetPropertyValue(java
	 * .lang.Object)
	 */
	@Override
	public void resetPropertyValue(Object id) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#setPropertyValue(java
	 * .lang.Object, java.lang.Object)
	 */
	@Override
	public void setPropertyValue(Object id, Object value) {
	}

}
