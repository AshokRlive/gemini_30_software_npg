/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editors;

import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.ui.actions.ActionFactory;
import org.fordiac.ide.application.actions.UnmapAction;
import org.fordiac.ide.application.actions.UnmapAllAction;
import org.fordiac.ide.gef.ZoomUndoRedoContextMenuProvider;

/**
 * The Class ResourceDiagramEditorContextMenuProvider.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ResourceDiagramEditorContextMenuProvider extends
		ZoomUndoRedoContextMenuProvider {

	/**
	 * Instantiates a new resource diagram editor context menu provider.
	 * 
	 * @param viewer the viewer
	 * @param zoomManager the zoom manager
	 * @param registry the registry
	 */
	public ResourceDiagramEditorContextMenuProvider(
			final EditPartViewer viewer, final ZoomManager zoomManager,
			final ActionRegistry registry) {
		super(viewer, zoomManager, registry);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.ZoomUndoRedoContextMenuProvider#buildContextMenu(org.eclipse.jface.action.IMenuManager)
	 */
	@Override
	public void buildContextMenu(final IMenuManager menu) {
		super.buildContextMenu(menu);

		IAction action;
		action = registry.getAction(UnmapAction.ID);
		if (action != null) {
			menu.appendToGroup(GEFActionConstants.GROUP_REST, action);
		}
		
		action = registry.getAction(UnmapAllAction.ID);
		if (action != null) {
			menu.appendToGroup(GEFActionConstants.GROUP_REST, action);
		}
		
		action = registry.getAction(ActionFactory.DELETE.getId());
		menu.appendToGroup(GEFActionConstants.GROUP_COPY, action);
		
		action = registry.getAction(GEFActionConstants.DIRECT_EDIT);
		if (action != null && action.isEnabled()) {
			menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
		}
	}
}
