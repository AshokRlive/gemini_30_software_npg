/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editors;

import org.fordiac.ide.gef.DiagramManager;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.UIResourceEditor;

public class ResourceDiagramManager extends DiagramManager {

	
	public void setUIResourceEditor(UIResourceEditor uiResourceEditor) {
		setDiagram(uiResourceEditor);
		adaptFBNetwork(uiResourceEditor.getResourceElement().getFBNetwork());
	}

	@Override
	protected Diagram createDiagram() {
		//Resources do not need to create a diagram they get it set from the editor
		return null;
	}
	
}
