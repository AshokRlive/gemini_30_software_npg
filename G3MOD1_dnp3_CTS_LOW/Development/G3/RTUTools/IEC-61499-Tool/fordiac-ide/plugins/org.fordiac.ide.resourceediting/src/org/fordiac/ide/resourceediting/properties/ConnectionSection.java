package org.fordiac.ide.resourceediting.properties;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.editparts.ConnectionEditPart;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.resourceediting.editors.ResourceDiagramEditor;

public class ConnectionSection extends org.fordiac.ide.gef.properties.ConnectionSection {

	@Override
	protected CommandStack getCommandStack(IWorkbenchPart part, Object input) {
		if(part instanceof ResourceDiagramEditor){
			return ((ResourceDiagramEditor)part).getCommandStack();
		}
		return null;
	}

	@Override
	protected Connection getInputType(Object input) {
		if(input instanceof ConnectionEditPart){
			return ((ConnectionView)((ConnectionEditPart) input).getModel()).getConnectionElement();
		}
		return null;
	}

}
