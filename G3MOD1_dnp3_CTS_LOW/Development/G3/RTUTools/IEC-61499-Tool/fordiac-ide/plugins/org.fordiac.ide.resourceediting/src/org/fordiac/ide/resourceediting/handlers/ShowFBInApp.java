/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.resourceediting.handlers;

import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.resourceediting.editparts.ResFBEditPart;
import org.fordiac.ide.util.OpenListenerManager;

/**
 * Handler to open the corresponding FB in its application if selected in a
 * Resource.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ShowFBInApp extends AbstractHandler {
	
	/**
	 * The constructor.
	 */
	public ShowFBInApp() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			Object first = ((IStructuredSelection) selection).getFirstElement();
			if (first instanceof ResFBEditPart) {
				FBView fbView = ((ResFBEditPart) first).getCastedModel();
				FBView appFBView = fbView.getApplicationFB();
				Application app = getApplication(fbView);
				if (app != null && appFBView != null) {
					IEditorPart editor = OpenListenerManager.openEditor(app);
					if (editor instanceof FBNetworkEditor) {
						GraphicalViewer viewer = ((FBNetworkEditor) editor)
								.getViewer();
						if (viewer != null) {
							Map<?, ?> map = viewer.getEditPartRegistry();
							Object fbToSelect = map.get(appFBView);

							if (fbToSelect instanceof EditPart) {
								viewer.setSelection(new StructuredSelection(
										fbToSelect));
								viewer.reveal((EditPart) fbToSelect);
							}
						}
					}

				}
			}
		}
		return null;
	}

	/**
	 * Finds the corresponding application of a <em>mapped</em> FB. 
	 * @param fbView
	 * @return the application of the fbView if available
	 */
	private Application getApplication(FBView fbView) {
		if (fbView != null) {
			FBView appFBView = fbView.getApplicationFB();
			if (appFBView != null) {
				FB fb = appFBView.getFb();
				EObject fbNetworkObject = fb.eContainer();
				if (fbNetworkObject instanceof FBNetwork) {
					return ((FBNetwork) fbNetworkObject).getApplication();
				}
			}
		}
		return null;
	}

	@Override
	public void setEnabled(Object evaluationContext) {
		IEvaluationContext ctx = (IEvaluationContext) evaluationContext;
		Object obj = ctx.getDefaultVariable();

		if (obj instanceof List) {
			List<?> list = (List<?>) obj;
			if (list.size() > 0) {
				obj = list.get(0);
			}
		}
		if(obj instanceof ResFBEditPart){
			FBView fbView = ((ResFBEditPart) obj).getCastedModel();
			FBView appFBView = fbView.getApplicationFB();
			Application app = getApplication(fbView);
			setBaseEnabled((app != null && appFBView != null));
		}
		else{
			setBaseEnabled(false);
		}
	}
}
