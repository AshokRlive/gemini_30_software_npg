/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.editparts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.fordiac.ide.application.editparts.ConnectionEditPart;
import org.fordiac.ide.application.editparts.InterfaceEditPart;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;
import org.fordiac.ide.gef.editparts.Abstract4diacEditPartFactory;
import org.fordiac.ide.gef.editparts.ValueEditPart;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.resourceediting.editors.ResourceDiagramEditor;
import org.fordiac.ide.resourceediting.policies.EventNodeEditPolicyForResourceFBs;
import org.fordiac.ide.resourceediting.policies.VariableNodeEditPolicyForResourceFBs;

/**
 * A factory for creating new EditParts.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class ResourceDiagramEditPartFactory extends Abstract4diacEditPartFactory {

	protected ZoomManager zoomManager;	
	
	/**
	 * Instantiates a new resource diagram edit part factory.
	 * 
	 * @param viewer
	 *          the viewer
	 */
	public ResourceDiagramEditPartFactory(final ResourceDiagramEditor editor, final GraphicalViewer viewer, ZoomManager zoomManager) {
		super(editor);
		this.zoomManager = zoomManager;
	}

	@Override
	protected EditPart getPartForElement(final EditPart context,
			final Object modelElement) {
		
		if (modelElement instanceof UIResourceEditor) {
			return new FBNetworkContainerEditPart();
		}
		if (modelElement instanceof FBNetworkContainer) {
			FBNetworkContainerEditPart fbnc = new FBNetworkContainerEditPart();
			return fbnc;
		}
		if (modelElement instanceof FBView) {
			return new ResFBEditPart(zoomManager);
		}
		if (modelElement instanceof MappedSubAppInterfaceElementView) {
			//TODO with new sub-application design move this into an own class
			return new InterfaceEditPart(){
				/*
				 * (non-Javadoc)
				 * 
				 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getNodeEditPolicy()
				 */
				@Override
				protected GraphicalNodeEditPolicy getNodeEditPolicy() {
					if (isEvent()) {
						return new EventNodeEditPolicyForResourceFBs();
					}
					if (isVariable()) {
						return new VariableNodeEditPolicyForResourceFBs();
					}
					return null;
				}
			};
		}

		if (modelElement instanceof InterfaceElementView) {
			EditPart parent = context.getParent();
			if (parent instanceof FBNetworkContainerEditPart) {
				return new InterfaceEditPartForResourceFBs();
			}
			if (context instanceof FBNetworkContainerEditPart) {
				return new VirtualInOutputEditPart();
			}
		}
		if (modelElement instanceof SubAppInterfaceElementView) {
			return new InterfaceEditPart();
		}
		if (modelElement instanceof ConnectionView) {
			return new ConnectionEditPart(); // return default connection
			// edit part
			// with the wrong connection style
		}
		if (modelElement instanceof Value) {
			ValueEditPart part = new ValueEditPart();
			part.setContext(context);
			return part;
		}
		if (modelElement instanceof SubAppView) {
			return new SubAppForFBNetworkEditPart();
		}
		if (modelElement instanceof MappedSubAppView) {
			return new MappedSubAppForFBNetworkEditPart();
		}		

		throw createEditpartCreationException(modelElement);
	}

}
