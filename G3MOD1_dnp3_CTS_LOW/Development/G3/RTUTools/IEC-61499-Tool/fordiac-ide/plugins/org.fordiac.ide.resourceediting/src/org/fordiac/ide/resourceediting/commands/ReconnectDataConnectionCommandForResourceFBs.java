/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.resourceediting.commands;

import org.eclipse.gef.requests.ReconnectRequest;
import org.fordiac.ide.application.commands.AbstractDataConnectionCreateCommand;
import org.fordiac.ide.application.commands.ReconnectDataConnectionCommand;

/**
 * The Class ReconnectDataConnectionCommandForResourceFBs.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class ReconnectDataConnectionCommandForResourceFBs extends
		ReconnectDataConnectionCommand {

	/**
	 * Instantiates a new reconnect data connection command for resource f bs.
	 * 
	 * @param request the request
	 */
	public ReconnectDataConnectionCommandForResourceFBs(
			final ReconnectRequest request) {
		super(request);
	}

	/**
	 * Do reconnect target.
	 */
	@Override
	protected AbstractDataConnectionCreateCommand createCreateDataConCommand() {
		return new DataConnectionCreateCommandForResourceFBs();
	}
	
}
