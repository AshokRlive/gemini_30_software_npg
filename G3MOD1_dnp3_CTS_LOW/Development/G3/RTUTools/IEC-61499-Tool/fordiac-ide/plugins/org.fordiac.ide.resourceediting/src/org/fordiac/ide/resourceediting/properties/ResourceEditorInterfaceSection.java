package org.fordiac.ide.resourceediting.properties;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.properties.InterfaceSection;
import org.fordiac.ide.resourceediting.editors.ResourceDiagramEditor;

public class ResourceEditorInterfaceSection extends InterfaceSection {

	@Override
	protected CommandStack getCommandStack(IWorkbenchPart part, Object input) {
		if(part instanceof ResourceDiagramEditor){
			return ((ResourceDiagramEditor)part).getCommandStack();
		}
		return null;
	}

}
