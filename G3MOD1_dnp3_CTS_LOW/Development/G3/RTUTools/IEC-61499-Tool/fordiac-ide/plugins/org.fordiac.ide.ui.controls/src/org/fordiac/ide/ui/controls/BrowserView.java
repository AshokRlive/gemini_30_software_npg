/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.ui.controls;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

/**
 * View which contains a browser for displaying all kinds of information which
 * can be displayed in a browser.
 * 
 * @author gebenh
 */
public class BrowserView extends ViewPart {

	private Browser browser;

	/**
	 * Sets the title text.
	 * 
	 * @param title the new title text
	 */
	public void setTitleText(String title) {
		setPartName(title);
	}

	/**
	 * Constructor.
	 */
	public BrowserView() {
		// nothing to do;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		browser = new Browser(parent, SWT.NONE);
		browser.setLayoutData(new GridData(GridData.FILL_BOTH));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		browser.setFocus();
	}

	/**
	 * Specifies the location/url to be displayed in the browser.
	 * 
	 * @param url the url
	 */
	public void setInput(String url) {
		browser.setUrl((url != null) ? url : "about:blank"); //$NON-NLS-1$
	}

}
