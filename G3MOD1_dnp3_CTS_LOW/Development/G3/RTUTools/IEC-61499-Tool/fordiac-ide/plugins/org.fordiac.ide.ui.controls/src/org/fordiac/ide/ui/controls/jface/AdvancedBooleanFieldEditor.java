/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.ui.controls.jface;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

/**
 * 
 * Adds the getCheckbox functionality to the BooleanFieldEditor. Required, e.g.
 * if someone wants to register on checkbox changes to enable disable User
 * controls on a preference change. (and the notify mechanism notifies to late -
 * after the apply)
 * 
 */
public class AdvancedBooleanFieldEditor extends BooleanFieldEditor {

	/**
	 * Creates a boolean field editor in the default style.
	 * 
	 * @param name
	 *            the name of the preference this field editor works on
	 * @param label
	 *            the label text of the field editor
	 * @param parent
	 *            the parent of the field editor's control
	 */

	public AdvancedBooleanFieldEditor(String name, String label,
			Composite parent) {
		super(name, label, parent);
	}

	/**
	 * returns the checkbox (button) of the current field editor
	 * 
	 * @param parent
	 *            the parent of the field editor's control
	 * @return the checkbox as button
	 */
	public Button getCheckbox(Composite parent) {
		return getChangeControl(parent);
	}

}
