/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.ui.wizardpages;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

/**
 * Wizard page that can display warnings, infos and errors.
 * 
 * @author gebenh
 */
public class ResultWizardPage extends WizardPage {

	/**
	 * Instantiates a new result wizard page.
	 * 
	 * @param pageName the page name
	 */
	public ResultWizardPage(String pageName) {
		super(pageName);
		warnings = new ArrayList<String>();
		errors = new ArrayList<String>();
		infos = new ArrayList<String>();
	}

	/**
	 * Sets the error messages to be displayed.
	 * 
	 * @param errors the errors
	 */
	public void setErrors(final ArrayList<String> errors) {
		this.errors.clear();
		this.errors.addAll(errors);
	}

	/**
	 * Sets the warning messages to be displayed.
	 * 
	 * @param warnings the warnings
	 */
	public void setWarnings(final ArrayList<String> warnings) {
		this.warnings.clear();
		this.warnings.addAll(warnings);
	}

	/**
	 * Sets the info messages to be displayed.
	 * 
	 * @param infos the infos
	 */
	public void setInfos(final ArrayList<String> infos) {
		this.infos.clear();
		this.infos.addAll(infos);
	}

	private StyledText text;

	private final ArrayList<String> warnings;
	private final ArrayList<String> errors;
	private final ArrayList<String> infos;

	private String newLine = "";

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createControl(Composite parent) {

		Composite main = new Composite(parent, SWT.NONE);
		main.setLayout(new GridLayout());
		GridData fillBoth = new GridData();
		fillBoth.grabExcessHorizontalSpace = true;
		fillBoth.grabExcessVerticalSpace = true;
		fillBoth.horizontalAlignment = GridData.FILL;
		fillBoth.verticalAlignment = GridData.FILL;

		main.setLayoutData(fillBoth);

		GridData fillText = new GridData();
		fillText.grabExcessHorizontalSpace = true;
		fillText.grabExcessVerticalSpace = true;
		fillText.horizontalAlignment = GridData.FILL;
		fillText.verticalAlignment = GridData.FILL;

		text = new StyledText(main, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL
				| SWT.H_SCROLL);
		text.setLayoutData(fillText);

		setPageComplete(false);
		setErrorMessage(null);
		setMessage(null);
		setControl(main);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.DialogPage#setVisible(boolean)
	 */
	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		setPageComplete(visible);
	}

	/**
	 * writes all the messages (infos, warnings and errors) in the textfield.
	 */
	public void printMessages() {

		text.setText("");

		String info = "Infos \n";

		text.append(info);
		int count = info.length();
		count += addLines(infos);

		StyleRange style1 = new StyleRange();
		style1.start = 0;
		style1.length = info.length();
		style1.fontStyle = SWT.BOLD;
		text.setStyleRange(style1);
		text.append(newLine);
		count += newLine.length();
		String warning = "Warnings \n";
		text.append(warning);
		style1 = new StyleRange();
		style1.start = count;
		style1.length = warning.length();
		style1.fontStyle = SWT.BOLD;
		text.setStyleRange(style1);

		count += warning.length();
		count += addLines(warnings);
		text.append(newLine);
		count += newLine.length();
		String error = "Errors \n";
		text.append(error);
		style1 = new StyleRange();
		style1.start = count;
		style1.length = error.length();
		style1.fontStyle = SWT.BOLD;
		text.setStyleRange(style1);

		addLines(errors);
	}

	private int addLines(ArrayList<String> messages) {
		int count = 0;
		for (Iterator<String> iterator = messages.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			count += string.length();
			text.append(string);
			newLine = "\n";
			text.append(newLine);
			count += newLine.length();
		}
		return count;
	}

}
