package org.fordiac.ide.model.structuredtext.converter;

import java.text.SimpleDateFormat;
import org.fordiac.ide.model.structuredtext.converter.DateValueConverter;

@SuppressWarnings("all")
public class Date_LiteralValueConverter extends DateValueConverter {
  public Date_LiteralValueConverter() {
    super(new SimpleDateFormat("yyyy-MM-dd"));
  }
}
