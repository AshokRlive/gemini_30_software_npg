package org.fordiac.ide.model.structuredtext.converter

import java.text.DateFormat
import java.util.Date
import org.eclipse.xtext.conversion.ValueConverterException
import org.eclipse.xtext.conversion.impl.AbstractNullSafeConverter
import org.eclipse.xtext.nodemodel.INode
import java.text.ParseException

class DateValueConverter extends AbstractNullSafeConverter<Date> {

	final extension DateFormat format;
	
	new(DateFormat format) {
		this.format = format
	}

	override protected internalToString(Date value) {
		value.format
	}

	override protected internalToValue(String string, INode node) throws ValueConverterException {
		if (string.nullOrEmpty) {
			throw new ValueConverterException("Couldn't convert empty string to a date value.", node, null)
		}
		try {
			string.parse
		} catch (ParseException e) {
			throw new ValueConverterException("Couldn't convert '" + string + "' to a date value.", node, e);
		}
	}

}
