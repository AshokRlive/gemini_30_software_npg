/**
 */
package org.fordiac.ide.model.structuredtext.structuredText.impl;

import org.eclipse.emf.ecore.EClass;

import org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage;
import org.fordiac.ide.model.structuredtext.structuredText.SuperStatement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Super Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SuperStatementImpl extends StatementImpl implements SuperStatement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SuperStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return StructuredTextPackage.Literals.SUPER_STATEMENT;
  }

} //SuperStatementImpl
