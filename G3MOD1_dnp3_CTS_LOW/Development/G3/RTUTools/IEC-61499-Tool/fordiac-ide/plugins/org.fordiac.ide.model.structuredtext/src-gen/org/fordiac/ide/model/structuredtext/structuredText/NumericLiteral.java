/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Numeric Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getNumericLiteral()
 * @model
 * @generated
 */
public interface NumericLiteral extends Constant
{
} // NumericLiteral
