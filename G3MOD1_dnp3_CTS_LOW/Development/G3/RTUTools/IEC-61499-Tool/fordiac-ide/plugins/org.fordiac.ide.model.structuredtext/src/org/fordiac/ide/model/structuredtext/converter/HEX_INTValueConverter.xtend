package org.fordiac.ide.model.structuredtext.converter

class HEX_INTValueConverter extends LongValueConverter {

	override getRadix() {
		16
	}

}
