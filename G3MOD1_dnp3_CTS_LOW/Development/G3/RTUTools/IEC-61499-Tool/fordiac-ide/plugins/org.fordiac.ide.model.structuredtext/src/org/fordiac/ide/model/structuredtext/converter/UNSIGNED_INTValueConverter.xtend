package org.fordiac.ide.model.structuredtext.converter

class UNSIGNED_INTValueConverter extends LongValueConverter {

	override getRadix() {
		10
	}

}
