package org.fordiac.ide.model.structuredtext.resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.linking.lazy.LazyLinkingResource;
import org.eclipse.xtext.parser.IParseResult;

public class StructuredTextResource extends LazyLinkingResource {

	public static final String OPTION_PARSER_RULE = "PARSER_RULE";

	@Override
	protected void doLoad(InputStream inputStream, Map<?, ?> options)
			throws IOException {
		setEncodingFromOptions(options);
		ParserRule rule = options != null ? (ParserRule)options.get(OPTION_PARSER_RULE) : null;
		IParseResult result;
		if (rule != null) {
			result = getParser().parse(rule, createReader(inputStream));
		} else {
			result = getParser().parse(createReader(inputStream));
		}
		updateInternalState(this.getParseResult(), result);
		if (options != null && Boolean.TRUE.equals(options.get(OPTION_RESOLVE_ALL))) {
			EcoreUtil.resolveAll(this);
		}
	}

}
