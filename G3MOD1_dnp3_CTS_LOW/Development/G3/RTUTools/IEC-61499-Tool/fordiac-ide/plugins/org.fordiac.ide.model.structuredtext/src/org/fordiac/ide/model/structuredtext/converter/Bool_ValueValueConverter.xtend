package org.fordiac.ide.model.structuredtext.converter

import org.eclipse.xtext.conversion.impl.AbstractNullSafeConverter
import org.eclipse.xtext.nodemodel.INode
import org.eclipse.xtext.conversion.ValueConverterException

import static extension java.lang.Boolean.valueOf

class Bool_ValueValueConverter extends AbstractNullSafeConverter<Boolean> {

	override protected internalToString(Boolean value) {
		value.toString.toUpperCase
	}

	override protected internalToValue(String string, INode node) throws ValueConverterException {
		if (string.nullOrEmpty) {
			throw new ValueConverterException("Couldn't convert empty string to a bool value.", node, null)
		}
		string.valueOf
	}

}
