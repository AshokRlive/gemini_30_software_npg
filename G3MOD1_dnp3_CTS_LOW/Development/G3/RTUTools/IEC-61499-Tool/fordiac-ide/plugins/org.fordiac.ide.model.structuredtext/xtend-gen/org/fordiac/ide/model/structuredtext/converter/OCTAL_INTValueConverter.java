package org.fordiac.ide.model.structuredtext.converter;

import org.fordiac.ide.model.structuredtext.converter.LongValueConverter;

@SuppressWarnings("all")
public class OCTAL_INTValueConverter extends LongValueConverter {
  @Override
  public int getRadix() {
    return 8;
  }
}
