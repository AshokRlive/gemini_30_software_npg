package org.fordiac.ide.model.structuredtext.converter

class BINARY_INTValueConverter extends LongValueConverter {

	override getRadix() {
		2
	}

}
