package org.fordiac.ide.model.structuredtext.converter;

import com.google.common.base.Objects;
import java.io.Reader;
import java.io.StringReader;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractNullSafeConverter;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function0;
import org.eclipse.xtext.xbase.lib.IntegerRange;

@SuppressWarnings("all")
public abstract class StringValueConverter extends AbstractNullSafeConverter<String> {
  @Override
  protected String internalToString(final String value) {
    try {
      String _xblockexpression = null;
      {
        final StringReader reader = new StringReader(value);
        final StringBuffer result = new StringBuffer();
        char _quote = this.getQuote();
        result.append(_quote);
        for (int c = new Function0<Integer>() {
          public Integer apply() {
            try {
              return reader.read();
            } catch (Throwable _e) {
              throw Exceptions.sneakyThrow(_e);
            }
          }
        }.apply().intValue(); (c != (-1)); c = reader.read()) {
          boolean _matched = false;
          if (Objects.equal(c, " ")) {
            _matched=true;
          }
          if (!_matched) {
            if (Objects.equal(c, "!")) {
              _matched=true;
            }
          }
          if (!_matched) {
            if (Objects.equal(c, "#")) {
              _matched=true;
            }
          }
          if (!_matched) {
            if (Objects.equal(c, "%")) {
              _matched=true;
            }
          }
          if (!_matched) {
            if (Objects.equal(c, "&")) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo = this.operator_upTo("(", "/");
            if (Objects.equal(c, _upTo)) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo_1 = this.operator_upTo("0", "9");
            if (Objects.equal(c, _upTo_1)) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo_2 = this.operator_upTo(":", "@");
            if (Objects.equal(c, _upTo_2)) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo_3 = this.operator_upTo("A", "Z");
            if (Objects.equal(c, _upTo_3)) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo_4 = this.operator_upTo("[", "`");
            if (Objects.equal(c, _upTo_4)) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo_5 = this.operator_upTo("a", "z");
            if (Objects.equal(c, _upTo_5)) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo_6 = this.operator_upTo("{", "~");
            if (Objects.equal(c, _upTo_6)) {
              _matched=true;
            }
          }
          if (_matched) {
            result.append(c);
          }
          if (!_matched) {
            if (Objects.equal(c, "\'")) {
              _matched=true;
            }
            if (!_matched) {
              if (Objects.equal(c, "\"")) {
                _matched=true;
              }
            }
            if (_matched) {
              char _quote_1 = this.getQuote();
              boolean _equals = (c == _quote_1);
              if (_equals) {
                result.append("$");
              }
              result.append(c);
            }
          }
          if (!_matched) {
            if (Objects.equal(c, "$")) {
              _matched=true;
              result.append("$$");
            }
          }
          if (!_matched) {
            if (Objects.equal(c, "\n")) {
              _matched=true;
              result.append("$N");
            }
          }
          if (!_matched) {
            if (Objects.equal(c, "\f")) {
              _matched=true;
              result.append("$P");
            }
          }
          if (!_matched) {
            if (Objects.equal(c, "\r")) {
              _matched=true;
              result.append("$R");
            }
          }
          if (!_matched) {
            if (Objects.equal(c, "\t")) {
              _matched=true;
              result.append("$T");
            }
          }
          if (!_matched) {
            if (((c >= 0) && (c < Character.MAX_VALUE))) {
              _matched=true;
              CharSequence _hexLiteral = this.toHexLiteral(((char) c));
              result.append(_hexLiteral);
            }
          }
          if (!_matched) {
            throw new ValueConverterException("Couldn\'t convert value due to invalid character", null, null);
          }
        }
        char _quote_1 = this.getQuote();
        result.append(_quote_1);
        _xblockexpression = result.toString();
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Override
  protected String internalToValue(final String string, final INode node) {
    try {
      String _xblockexpression = null;
      {
        if ((string.isEmpty() || (string.length() < 2))) {
          throw new ValueConverterException("Unclosed string literal", null, null);
        }
        if (((string.charAt(0) != this.getQuote()) || (string.charAt((string.length() - 1)) != this.getQuote()))) {
          throw new ValueConverterException("Invalid quotes for string literal", null, null);
        }
        int _length = string.length();
        int _minus = (_length - 1);
        final String value = string.substring(1, _minus);
        final StringReader reader = new StringReader(value);
        final StringBuffer result = new StringBuffer();
        for (int c = new Function0<Integer>() {
          public Integer apply() {
            try {
              return reader.read();
            } catch (Throwable _e) {
              throw Exceptions.sneakyThrow(_e);
            }
          }
        }.apply().intValue(); (c != (-1)); c = reader.read()) {
          Object _switchResult = null;
          boolean _matched = false;
          if (Objects.equal(c, " ")) {
            _matched=true;
          }
          if (!_matched) {
            if (Objects.equal(c, "!")) {
              _matched=true;
            }
          }
          if (!_matched) {
            if (Objects.equal(c, "#")) {
              _matched=true;
            }
          }
          if (!_matched) {
            if (Objects.equal(c, "%")) {
              _matched=true;
            }
          }
          if (!_matched) {
            if (Objects.equal(c, "&")) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo = this.operator_upTo("(", "/");
            if (Objects.equal(c, _upTo)) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo_1 = this.operator_upTo("0", "9");
            if (Objects.equal(c, _upTo_1)) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo_2 = this.operator_upTo(":", "@");
            if (Objects.equal(c, _upTo_2)) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo_3 = this.operator_upTo("A", "Z");
            if (Objects.equal(c, _upTo_3)) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo_4 = this.operator_upTo("[", "`");
            if (Objects.equal(c, _upTo_4)) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo_5 = this.operator_upTo("a", "z");
            if (Objects.equal(c, _upTo_5)) {
              _matched=true;
            }
          }
          if (!_matched) {
            IntegerRange _upTo_6 = this.operator_upTo("{", "~");
            if (Objects.equal(c, _upTo_6)) {
              _matched=true;
            }
          }
          if (_matched) {
            _switchResult = Integer.valueOf(c);
          }
          if (!_matched) {
            if (Objects.equal(c, "\'")) {
              _matched=true;
            }
            if (!_matched) {
              if (Objects.equal(c, "\"")) {
                _matched=true;
              }
            }
            if (_matched) {
              int _xblockexpression_1 = (int) 0;
              {
                char _quote = this.getQuote();
                boolean _equals = (c == _quote);
                if (_equals) {
                  throw new ValueConverterException(
                    "Couldn\'t convert value due to illegal quote character inside string", null, null);
                }
                _xblockexpression_1 = c;
              }
              _switchResult = Integer.valueOf(_xblockexpression_1);
            }
          }
          if (!_matched) {
            if (Objects.equal(c, "$")) {
              _matched=true;
              Object _switchResult_1 = null;
              int _read = reader.read();
              boolean _matched_1 = false;
              if (Objects.equal(_read, "$")) {
                _matched_1=true;
                _switchResult_1 = "$";
              }
              if (!_matched_1) {
                if (Objects.equal(_read, "L")) {
                  _matched_1=true;
                }
                if (!_matched_1) {
                  if (Objects.equal(_read, "l")) {
                    _matched_1=true;
                  }
                }
                if (_matched_1) {
                  _switchResult_1 = "\n";
                }
              }
              if (!_matched_1) {
                if (Objects.equal(_read, "N")) {
                  _matched_1=true;
                }
                if (!_matched_1) {
                  if (Objects.equal(_read, "n")) {
                    _matched_1=true;
                  }
                }
                if (_matched_1) {
                  _switchResult_1 = "\n";
                }
              }
              if (!_matched_1) {
                if (Objects.equal(_read, "P")) {
                  _matched_1=true;
                }
                if (!_matched_1) {
                  if (Objects.equal(_read, "p")) {
                    _matched_1=true;
                  }
                }
                if (_matched_1) {
                  _switchResult_1 = "\f";
                }
              }
              if (!_matched_1) {
                if (Objects.equal(_read, "R")) {
                  _matched_1=true;
                }
                if (!_matched_1) {
                  if (Objects.equal(_read, "r")) {
                    _matched_1=true;
                  }
                }
                if (_matched_1) {
                  _switchResult_1 = "\r";
                }
              }
              if (!_matched_1) {
                if (Objects.equal(_read, "T")) {
                  _matched_1=true;
                }
                if (!_matched_1) {
                  if (Objects.equal(_read, "t")) {
                    _matched_1=true;
                  }
                }
                if (_matched_1) {
                  _switchResult_1 = "\t";
                }
              }
              if (!_matched_1) {
                char _quote = this.getQuote();
                if (Objects.equal(_read, _quote)) {
                  _matched_1=true;
                  _switchResult_1 = Character.valueOf(this.getQuote());
                }
              }
              if (!_matched_1) {
                _switchResult_1 = Character.valueOf(this.parseHexLiteral(reader));
              }
              _switchResult = _switchResult_1;
            }
          }
          if (!_matched) {
            throw new ValueConverterException("Couldn\'t convert value due to invalid character", null, null);
          }
          result.append(_switchResult);
        }
        _xblockexpression = result.toString();
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public IntegerRange operator_upTo(final String string, final String string2) {
    IntegerRange _xblockexpression = null;
    {
      if (((string.length() != 1) || (string2.length() != 1))) {
        throw new UnsupportedOperationException("Strings must have length 1");
      }
      char _charAt = string.charAt(0);
      char _charAt_1 = string2.charAt(0);
      _xblockexpression = new IntegerRange(_charAt, _charAt_1);
    }
    return _xblockexpression;
  }
  
  public abstract char getQuote();
  
  public abstract char parseHexLiteral(final Reader reader);
  
  public abstract CharSequence toHexLiteral(final char c);
}
