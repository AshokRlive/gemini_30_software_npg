package org.fordiac.ide.model.structuredtext.converter

import java.io.Reader
import java.io.StringReader
import org.eclipse.xtext.conversion.ValueConverterException
import org.eclipse.xtext.conversion.impl.AbstractNullSafeConverter
import org.eclipse.xtext.nodemodel.INode

abstract class StringValueConverter extends AbstractNullSafeConverter<String> {

	override protected internalToString(String value) {
		val reader = new StringReader(value)
		val result = new StringBuffer()
		result.append(quote)
		for (var c = reader.read; c != -1; c = reader.read) {
			switch (c) {
				case ' ',
				case '!',
				case '#',
				case '%',
				case '&',
				case '(' .. '/',
				case '0' .. '9',
				case ':' .. '@',
				case 'A' .. 'Z',
				case '[' .. '`',
				case 'a' .. 'z',
				case '{' .. '~':
					result.append(c)
				case '\'',
				case '"': {
					if (c == quote) {
						result.append("$")
					}
					result.append(c)
				}
				case '$':
					result.append("$$")
				case '\n':
					result.append("$N")
				case '\f':
					result.append("$P")
				case '\r':
					result.append("$R")
				case '\t':
					result.append("$T")
				case c >= 0 && c < Character.MAX_VALUE:
					result.append(toHexLiteral(c as char))
				default:
					throw new ValueConverterException("Couldn't convert value due to invalid character", null, null)
			}
		}
		result.append(quote)
		result.toString
	}

	override protected internalToValue(String string, INode node) {
		if (string.empty || string.length < 2) {
			throw new ValueConverterException("Unclosed string literal", null, null)
		}
		if (string.charAt(0) != quote || string.charAt(string.length - 1) != quote) {
			throw new ValueConverterException("Invalid quotes for string literal", null, null)
		}
		val value = string.substring(1, string.length - 1)
		val reader = new StringReader(value)
		val result = new StringBuffer()
		for (var c = reader.read; c != -1; c = reader.read) {
			result.append(
				switch (c) {
					case ' ',
					case '!',
					case '#',
					case '%',
					case '&',
					case '(' .. '/',
					case '0' .. '9',
					case ':' .. '@',
					case 'A' .. 'Z',
					case '[' .. '`',
					case 'a' .. 'z',
					case '{' .. '~':
						c
					case '\'',
					case '"': {
						if (c == quote) {
							throw new ValueConverterException(
								"Couldn't convert value due to illegal quote character inside string", null, null)
						}
						c
					}
					case '$': {
						switch (reader.read) {
							case '$':
								'$'
							case 'L',
							case 'l':
								'\n'
							case 'N',
							case 'n':
								'\n'
							case 'P',
							case 'p':
								'\f'
							case 'R',
							case 'r':
								'\r'
							case 'T',
							case 't':
								'\t'
							case quote:
								quote
							default:
								parseHexLiteral(reader)
						}
					}
					default:
						throw new ValueConverterException("Couldn't convert value due to invalid character", null, null)
				}
			)
		}
		result.toString
	}

	def operator_upTo(String string, String string2) {
		if (string.length != 1 || string2.length != 1) {
			throw new UnsupportedOperationException("Strings must have length 1")
		}
		string.charAt(0) .. string2.charAt(0)
	}

	def abstract char getQuote();

	def abstract char parseHexLiteral(Reader reader);

	def abstract CharSequence toHexLiteral(char c);

}
