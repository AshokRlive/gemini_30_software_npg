/*
 * generated by Xtext
 */
package org.fordiac.ide.model.structuredtext.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class StructuredTextAntlrTokenFileProvider implements IAntlrTokenFileProvider {
	
	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
    	return classLoader.getResourceAsStream("org/fordiac/ide/model/structuredtext/parser/antlr/internal/InternalStructuredTextParser.tokens");
	}
}
