package org.fordiac.ide.model.structuredtext.converter

class OCTAL_INTValueConverter extends LongValueConverter {

	override getRadix() {
		8
	}

}
