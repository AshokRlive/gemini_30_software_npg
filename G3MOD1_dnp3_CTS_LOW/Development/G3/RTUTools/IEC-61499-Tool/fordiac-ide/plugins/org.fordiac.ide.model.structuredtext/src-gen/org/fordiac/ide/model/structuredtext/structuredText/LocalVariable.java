/**
 */
package org.fordiac.ide.model.structuredtext.structuredText;

import org.fordiac.ide.model.libraryElement.VarDeclaration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Local Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.LocalVariable#isConstant <em>Constant</em>}</li>
 *   <li>{@link org.fordiac.ide.model.structuredtext.structuredText.LocalVariable#getInitialValue <em>Initial Value</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getLocalVariable()
 * @model
 * @generated
 */
public interface LocalVariable extends VarDeclaration
{
  /**
   * Returns the value of the '<em><b>Constant</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Constant</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Constant</em>' attribute.
   * @see #setConstant(boolean)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getLocalVariable_Constant()
   * @model
   * @generated
   */
  boolean isConstant();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.LocalVariable#isConstant <em>Constant</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Constant</em>' attribute.
   * @see #isConstant()
   * @generated
   */
  void setConstant(boolean value);

  /**
   * Returns the value of the '<em><b>Initial Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Initial Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Initial Value</em>' containment reference.
   * @see #setInitialValue(Constant)
   * @see org.fordiac.ide.model.structuredtext.structuredText.StructuredTextPackage#getLocalVariable_InitialValue()
   * @model containment="true"
   * @generated
   */
  Constant getInitialValue();

  /**
   * Sets the value of the '{@link org.fordiac.ide.model.structuredtext.structuredText.LocalVariable#getInitialValue <em>Initial Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Initial Value</em>' containment reference.
   * @see #getInitialValue()
   * @generated
   */
  void setInitialValue(Constant value);

} // LocalVariable
