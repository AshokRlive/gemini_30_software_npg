package org.fordiac.ide.model.structuredtext.converter;

import java.text.SimpleDateFormat;
import org.fordiac.ide.model.structuredtext.converter.DateValueConverter;

@SuppressWarnings("all")
public class DaytimeValueConverter extends DateValueConverter {
  public DaytimeValueConverter() {
    super(new SimpleDateFormat("HH:mm:ss"));
  }
}
