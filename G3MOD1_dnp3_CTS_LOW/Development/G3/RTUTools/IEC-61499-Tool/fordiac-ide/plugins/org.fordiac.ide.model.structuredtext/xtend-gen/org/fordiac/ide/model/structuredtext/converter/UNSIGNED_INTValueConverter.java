package org.fordiac.ide.model.structuredtext.converter;

import org.fordiac.ide.model.structuredtext.converter.LongValueConverter;

@SuppressWarnings("all")
public class UNSIGNED_INTValueConverter extends LongValueConverter {
  @Override
  public int getRadix() {
    return 10;
  }
}
