package org.fordiac.ide.model.structuredtext.converter;

import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractNullSafeConverter;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class Bool_ValueValueConverter extends AbstractNullSafeConverter<Boolean> {
  @Override
  protected String internalToString(final Boolean value) {
    String _string = value.toString();
    return _string.toUpperCase();
  }
  
  @Override
  protected Boolean internalToValue(final String string, final INode node) throws ValueConverterException {
    Boolean _xblockexpression = null;
    {
      boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(string);
      if (_isNullOrEmpty) {
        throw new ValueConverterException("Couldn\'t convert empty string to a bool value.", node, null);
      }
      _xblockexpression = Boolean.valueOf(string);
    }
    return _xblockexpression;
  }
}
