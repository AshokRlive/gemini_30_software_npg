package org.fordiac.ide.model.structuredtext.converter

import java.io.Reader
import org.eclipse.xtext.conversion.ValueConverterException

import static extension java.lang.Integer.parseUnsignedInt
import static extension java.lang.Integer.toUnsignedString

class D_BYTE_CHAR_STRValueConverter extends StringValueConverter {

	override getQuote() {
		'"'
	}

	override parseHexLiteral(Reader reader) {
		val cbuf = newCharArrayOfSize(4)
		if (reader.read(cbuf) != 4) {
			throw new ValueConverterException("Couldn't convert value due to invalid escape sequence", null, null)
		}
		try {
			String.valueOf(cbuf).parseUnsignedInt(16) as char
		} catch (NumberFormatException e) {
			throw new ValueConverterException("Couldn't convert value due to invalid escape sequence", null, null)
		}
	}

	override toHexLiteral(char c) {
		c.toUnsignedString(16)
	}

}
