package org.fordiac.ide.model.structuredtext.converter

import java.text.SimpleDateFormat

class Date_LiteralValueConverter extends DateValueConverter {

	new() {
		super(new SimpleDateFormat("yyyy-MM-dd"))
	}

}
