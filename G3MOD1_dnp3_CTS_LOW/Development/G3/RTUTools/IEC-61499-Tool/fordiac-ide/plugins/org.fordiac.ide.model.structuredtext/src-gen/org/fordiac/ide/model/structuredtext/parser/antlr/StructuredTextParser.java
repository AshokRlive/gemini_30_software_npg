/*
 * generated by Xtext
 */
package org.fordiac.ide.model.structuredtext.parser.antlr;

import com.google.inject.Inject;

import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.fordiac.ide.model.structuredtext.services.StructuredTextGrammarAccess;

public class StructuredTextParser extends org.eclipse.xtext.parser.antlr.AbstractAntlrParser {
	
	@Inject
	private StructuredTextGrammarAccess grammarAccess;
	
	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	
	@Override
	protected org.fordiac.ide.model.structuredtext.parser.antlr.internal.InternalStructuredTextParser createParser(XtextTokenStream stream) {
		return new org.fordiac.ide.model.structuredtext.parser.antlr.internal.InternalStructuredTextParser(stream, getGrammarAccess());
	}
	
	@Override 
	protected String getDefaultRuleName() {
		return "StructuredTextAlgorithm";
	}
	
	public StructuredTextGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}
	
	public void setGrammarAccess(StructuredTextGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
	
}
