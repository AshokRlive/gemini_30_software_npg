package org.fordiac.ide.model.structuredtext.converter

import java.text.SimpleDateFormat

class DaytimeValueConverter extends DateValueConverter {

	new() {
		super(new SimpleDateFormat("HH:mm:ss"))
	}

}
