/*******************************************************************************
 * Copyright (c) 2014 - 2015 Luka Lednicki
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.eclipse.fordiac.ide.comgeneration.implementation;

import org.eclipse.emf.ecore.EObject;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.PaletteGroup;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.View;

public class Utils {
	
	public static Diagram getDiagramForResource(Resource resource) {
		if (resource.eContainer() instanceof Device) {
			Device device = (Device) resource.eContainer();
			
			if (device.eContainer() instanceof SystemConfigurationNetwork) {
				SystemConfigurationNetwork systemConfigurationNetwork = (SystemConfigurationNetwork) device.eContainer();
				if (systemConfigurationNetwork.eContainer() instanceof UISystemConfiguration) {
					UISystemConfiguration uiSystemConfiguration = (UISystemConfiguration) systemConfigurationNetwork.eContainer();
					
					for (View view : uiSystemConfiguration.getChildren()) {
						if (view instanceof DeviceView) {
							DeviceView deviceView = (DeviceView) view;
							if (deviceView.getDeviceElement() == device) {
								for (ResourceView resourceView : deviceView.getResourceContainerView().getResources()) {
									if (resourceView.getResourceElement() == resource) {
										return resourceView.getUIResourceDiagram();
									}
								}
							}
						}
					}
					
				}
			}
			
		}
		return null;
	}
	
	/*public static InterfaceElementView getInterfaceElementView(IInterfaceElement event) {
		return getInterfaceElementView(event, null);
	}*/
	
	public static InterfaceElementView getInterfaceElementView(IInterfaceElement interfaceElement, Resource inResource) {
		
		
		FB originalFB = null;
		
		EObject object = interfaceElement.eContainer();
		while (object != null) {
			if (object instanceof FB) {
				originalFB = (FB) object;
				break;
			} else {
				object = object.eContainer();
			}
		}
		
		
		object = (inResource != null) ? inResource : interfaceElement.eContainer();
		
		Resource originalResource = null;
		Device originalDevice = null;
	
		
		
		
		while(object != null) {
			
			if (object instanceof FB) {
				originalFB = (FB) object;
				
			} else if (object instanceof Resource) {
				originalResource = (Resource) object;
				
			} else if (object instanceof Device) {
				originalDevice = (Device) object;
			}
			
			if (object instanceof UIFBNetwork) {	
				UIFBNetwork uifbNetwork = (UIFBNetwork) object;
				
				for (View view : uifbNetwork.getChildren()) {
					if (view instanceof FBView) {
						FBView fbView = (FBView) view;
						if (fbView.getFb() == originalFB) {
							for (InterfaceElementView interfaceElementView : fbView.getInterfaceElements()) {
								if (interfaceElementView.getIInterfaceElement() == interfaceElement) {
									return interfaceElementView;
								}
							}
						}
					}
				}
				
			} else if (object instanceof UISystemConfiguration) {
				UISystemConfiguration uiSystemConfiguration = (UISystemConfiguration) object;
				for (View view : uiSystemConfiguration.getChildren()) {
					if (view instanceof DeviceView) {
						DeviceView deviceView = (DeviceView) view;
						if (deviceView.getDeviceElement() == originalDevice) {
							for (ResourceView resourceView : deviceView.getResourceContainerView().getResources()) {
								if (resourceView.getResourceElement() == originalResource) {
									for (View resourceChildView : resourceView.getUIResourceDiagram().getChildren()) {
										if (resourceChildView instanceof FBView) {
											FBView fbView = (FBView) resourceChildView;
											if (fbView.getFb() == originalFB) {
												for (InterfaceElementView interfaceElementView : fbView.getInterfaceElements()) {
													if (interfaceElementView.getIInterfaceElement() == interfaceElement) {
														return interfaceElementView;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				
			} else {
				object = object.eContainer();
			}
		}
		
		return null;
	}
	
	public static FBView getFBView(FB fb) {
		EObject object = fb.eContainer();
		
		Resource originalResource = null;
		Device originalDevice = null;
		
		while(object != null) {
			if (object instanceof Resource) {
				originalResource = (Resource) object;
				
			} else if (object instanceof Device) {
				originalDevice = (Device) object;
			}
			
			if (object instanceof UIFBNetwork) {	
				UIFBNetwork uifbNetwork = (UIFBNetwork) object;
				
				for (View view : uifbNetwork.getChildren()) {
					if (view instanceof FBView) {
						FBView fbView = (FBView) view;
						if (fbView.getFb() == fb) {
							return fbView;
						}
					}
				}
				
			} else if (object instanceof UISystemConfiguration) {
				UISystemConfiguration uiSystemConfiguration = (UISystemConfiguration) object;
				for (View view : uiSystemConfiguration.getChildren()) {
					if (view instanceof DeviceView) {
						DeviceView deviceView = (DeviceView) view;
						if (deviceView.getDeviceElement() == originalDevice) {
							for (ResourceView resourceView : deviceView.getResourceContainerView().getResources()) {
								if (resourceView.getResourceElement() == originalResource) {
									for (View resourceChildView : resourceView.getUIResourceDiagram().getChildren()) {
										if (resourceChildView instanceof FBView) {
											FBView fbView = (FBView) resourceChildView;
											if (fbView.getFb() == fb) {
												return fbView;
											}
										}
									}
								}
							}
						}
					}
				}
				
			} else {
				object = object.eContainer();
			}
		}
		
		return null;
	}
	
	
	public static Palette getPalette(Application application) {
		Object o = application.getFBNetwork().getFBs().get(0).getPaletteEntry().eContainer();
		while (o != null && !(o instanceof Palette)) {
			if (o instanceof PaletteGroup) {
				o = ((PaletteGroup) o).eContainer();
			} else {
				o = null;
			}
		}

		if (o instanceof Palette) {
			return (Palette) o;
		} else {
			return null;
		}
		
	}

}
