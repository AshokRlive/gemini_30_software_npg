/*******************************************************************************
 * Copyright (c) 2014 - 2015 Luka Lednicki
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.eclipse.fordiac.ide.comgeneration.implementation.mediagenerators;

import org.fordiac.ide.model.Palette.Palette;

public abstract class AbstractMediaSpecificGenerator implements MediaSpecificGenerator {
	protected Palette palette;
	
	public AbstractMediaSpecificGenerator(Palette palette) {
		super();
		this.palette = palette;
	}
	

}
