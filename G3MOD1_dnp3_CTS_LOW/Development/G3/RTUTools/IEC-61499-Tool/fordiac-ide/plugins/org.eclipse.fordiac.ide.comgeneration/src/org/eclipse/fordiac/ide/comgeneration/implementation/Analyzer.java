/*******************************************************************************
 * Copyright (c) 2014 - 2015 Luka Lednicki
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.eclipse.fordiac.ide.comgeneration.implementation;

import java.util.ArrayList;

import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.Link;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.With;


public class Analyzer {
	
	private CommunicationModel communicationModel;
	
	public CommunicationModel analyze(Application application) {
		
		communicationModel = new CommunicationModel();
		
		
		for (EventConnection connection : application.getFBNetwork().getEventConnections()) {
			collectChannels(connection);
		}
		
		for (DataConnection connection : application.getFBNetwork().getDataConnections()) {
			collectChannels(connection);
		}
		
		collectMediaInformation();
		
		return communicationModel;
	}
	
	
	public void collectChannels(Connection connection) {
		InterfaceList sourceInterface = (InterfaceList) connection.getIInterfaceElementSource().eContainer();
		FB sourceFB = (FB) sourceInterface.eContainer();
		Resource sourceResource = (Resource) ((sourceFB.getResource() != null) ? sourceFB.getResource().eContainer() : null);
		ArrayList<Event> sourceEvents = new ArrayList<Event>();
		
		
		if (connection instanceof EventConnection) {
			EventConnection eventConnection = (EventConnection) connection;
			sourceEvents.add(eventConnection.getSource());
		} else if (connection instanceof DataConnection) {
			DataConnection dataConnection = (DataConnection) connection;
			for (With with : dataConnection.getSource().getWiths()) {
				if (with.eContainer() instanceof Event) {
					sourceEvents.add((Event) with.eContainer());
					
				}
			}
		}
		
		InterfaceList destinationInterface = (InterfaceList) connection.getIInterfaceElementDestination().eContainer();
		FB destinationFB = (FB) destinationInterface.eContainer();
		Resource destinationResource = (Resource) ((destinationFB.getResource() != null) ? destinationFB.getResource().eContainer() : null);
		
		
		
		boolean shouldCreate = sourceResource != null && destinationResource != null && sourceResource != destinationResource;
		
		if (shouldCreate) {
			
			boolean local = sourceResource.getDevice() == destinationResource.getDevice();
			
			for (Event sourceEvent : sourceEvents) {
				
				CommunicationChannel channel = communicationModel.getChannels().get(sourceEvent);
				
				if (channel == null) {
					channel = new CommunicationChannel();
					//channel.setSourceInterface(sourceInterface);
					channel.setSourceResource(sourceResource);
					channel.setSourceEvent(sourceEvent);
					channel.setNumberOfDataPorts(sourceEvent.getWith().size());
					channel.setLocal(local);
					
					communicationModel.getChannels().put(sourceEvent, channel);
					
				}
				if (!local) {
					channel.setLocal(false);
				}
				
				CommunicationChannelDestination destination = channel.getDestination(destinationResource);
				destination.getConnection().add(connection);
				
				int portIndex = -2;			
				if (connection instanceof EventConnection) {
					portIndex = -1;
					
					
				} else if (connection instanceof DataConnection) {
					DataConnection dataConnection = (DataConnection) connection;
					portIndex = 0;
					for (With with : sourceEvent.getWith()) {
						if (with.getVariables() == dataConnection.getSource()) {
							break;
						}
						portIndex++;
					}
				}
				
				ArrayList<IInterfaceElement> destinationPortList = destination.getDestinationPorts().get(portIndex);
				
				if (destinationPortList == null) {
					destinationPortList = new ArrayList<IInterfaceElement>();
					destination.getDestinationPorts().put(portIndex, destinationPortList);
				}
				destinationPortList.add(connection.getIInterfaceElementDestination());
				

				
				
			}
		}
	}
	
	private void collectMediaInformation() {
		for (CommunicationChannel channel : communicationModel.getChannels().values()) {
			collectMediaInformation(channel);
		}
	}
	
	private void collectMediaInformation(CommunicationChannel channel) {
		for (CommunicationChannelDestination destination : channel.getDestinations()) {
			collectMediaInformation(destination);
		}
	}
	
	private void collectMediaInformation(CommunicationChannelDestination destination) {
		Device sourceDevice = (Device) destination.getCommunicationChannel().getSourceResource().eContainer();
		
		Device destinationDevice = (Device) destination.getDestinationResource().eContainer();
		
		for (Link sourceLink : sourceDevice.getInConnections()) {
			for (Link destinationLink : destinationDevice.getInConnections()) {
				if (sourceLink.getSegment() == destinationLink.getSegment()) {
					destination.getAvailableMedia().add(new CommunicationMediaInfo(sourceLink, destinationLink, sourceLink.getSegment()));
				}
			}
		}
		
	}

}
