/*******************************************************************************
 * Copyright (c) 2014 - 2015 Luka Lednicki
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.eclipse.fordiac.ide.comgeneration.implementation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.fordiac.ide.comgeneration.implementation.mediagenerators.MediaSpecificGenerator;
import org.eclipse.fordiac.ide.comgeneration.implementation.mediagenerators.MediaSpecificGeneratorFactory;
import org.fordiac.ide.application.commands.DataConnectionCreateCommand;
import org.fordiac.ide.application.commands.EventConnectionCreateCommand;
import org.fordiac.ide.application.commands.FBCreateCommand;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.Palette.FBTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.ResourceFBNetwork;
import org.fordiac.ide.model.libraryElement.Segment;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.With;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.util.commands.DeleteFBCommand;
import org.fordiac.systemmanagement.SystemManager;

public class CommFBGenerator {
	private static final String GENERATED_ANNOTATION = "generatedComm";
	private static final String GENERATED_SOURCE_FB_ANNOTATION = "generatedSrcFB";
	private static final String GENERATED_DESTINATION_FB_ANNOTATION = "generatedDestFB";
	private static final String GENERATED_FOR_CONNECTION_ANNOTATION = "generatedForConnection";
	
	private HashSet<Diagram> modifiedDiagrams;
	
	private HashSet<GeneratedFBInfo> generatedFBs;
	private TransferedData transferedData;
	private MediaSpecificGeneratorFactory specificGeneratorFactory;
	
	private int generatedFBIndex;

	
	public CommFBGenerator(MediaSpecificGeneratorFactory specificGeneratorFactory) {
		super();
	
		this.specificGeneratorFactory = specificGeneratorFactory;
		transferedData = TransferedData.ALL;
	}
	
	

	public TransferedData getTransferedData() {
		return transferedData;
	}



	public void setTransferedData(TransferedData transferedData) {
		this.transferedData = transferedData;
	}



	public void generate(CommunicationModel communicationModel) {
		modifiedDiagrams = new HashSet<Diagram>();
		generatedFBs = new HashSet<GeneratedFBInfo>();
		generatedFBIndex = 0;
		
		for (CommunicationChannel channel : communicationModel.getChannels().values()) {		
			generate(channel);
		}
		
		HashSet<AutomationSystem> savedSystems = new HashSet<AutomationSystem>();
		for (Diagram diagram : modifiedDiagrams) {
			AutomationSystem automationSystem = SystemManager.getInstance().getSystemForDiagram(diagram);
			if (!savedSystems.contains(automationSystem)) {
				SystemManager.getInstance().saveSystem(automationSystem, true);
				savedSystems.add(automationSystem);
			}
		}
	}
		
	private void generate(CommunicationChannel channel) {
		for (CommunicationChannelDestination destination : channel.getDestinations()) {
			generateFBs(destination);
		}
	}
		
	
	private void generateFBs(CommunicationChannelDestination destination) {
		if (destination.getSelectedMedia() == null || destination.getSelectedProtocolId() == null) {
			System.err.println("No media or protocol selected for " + destination);
			return;
		}
		
		int numberDataPorts = 0;
		Set<Integer> withPorts = null;
		switch (transferedData) {
		case ALL:
			numberDataPorts = destination.getCommunicationChannel().getNumberOfDataPorts();
			HashSet<Integer> allWithPorts = new HashSet<Integer>();
			for (int i = 0; i < destination.getCommunicationChannel().getSourceEvent().getWith().size(); i++) {
				allWithPorts.add(i);
			}
			withPorts = allWithPorts;
			break;
		case EXACT:
			numberDataPorts = destination.getDestinationPorts().keySet().size();
			// if it includes the event, reduce number of dataports
			if (destination.getDestinationPorts().keySet().contains(-1)) {
				numberDataPorts--;
			}
			withPorts = destination.getDestinationPorts().keySet();
			break;
		}
		
		MediaSpecificGenerator specificGenerator = specificGeneratorFactory.getForProtocolId(destination.getSelectedProtocolId());
		
		if (specificGenerator == null) {
			System.err.println("No generator for protocol " + destination.getSelectedProtocolId() + "!");
		}
		
		GeneratedFBInfo sourceGeneratedFBInfo = generateFB(ChannelEnd.SOURCE, numberDataPorts, withPorts, destination, specificGenerator);
		GeneratedFBInfo destinationGeneratedFBInfo = generateFB(ChannelEnd.DESTINATION, numberDataPorts, withPorts, destination, specificGenerator);

		specificGenerator.configureFBs(sourceGeneratedFBInfo.getFb(), destinationGeneratedFBInfo.getFb(), destination.getSelectedMedia());
		
	}
	
	private GeneratedFBInfo generateFB(ChannelEnd end, int numberOfDataPorts, Set<Integer> withPorts, CommunicationChannelDestination destination, MediaSpecificGenerator specificGenerator) {
		GeneratedFBInfo generatedFBInfo = null;
		if (end == ChannelEnd.SOURCE && !destination.isSeparated() && !specificGenerator.isSeparatedSource()) {
			generatedFBInfo = findExistingGeneratedFBInfo(end, destination.getCommunicationChannel(), withPorts, destination.getSelectedMedia().getSegment());
		}
		 
		
		if (generatedFBInfo != null) {
			generatedFBInfo.getDestinations().add(destination);
		} else {
			FBTypePaletteEntry paletteEntry = specificGenerator.getPaletteType(end, numberOfDataPorts, destination.getCommunicationChannel().isLocal());
			
			Resource resource = (end == ChannelEnd.SOURCE) ? destination.getCommunicationChannel().getSourceResource() : destination.getDestinationResource();
					
			Diagram destinationDiagram = Utils.getDiagramForResource(resource);
			modifiedDiagrams.add(destinationDiagram);
			
						
			Rectangle bounds = new Rectangle(10, 10, -1, -1);
			
			FBCreateCommand command = new FBCreateCommand(paletteEntry, destinationDiagram , bounds);
			command.setCreateResourceFB(true);
			command.execute();
			FB generatedFB = command.getFB();
			
			//FBView generateFbView = ApplicationUIFBNetworkManager.createFBView(generatedFB, /*destinationDiagram.getNetwork()*/ null);
			//destinationDiagram.getChildren().add(generateFbView);
			
			
			
			//generatedFB.setName("GENERATED_" + generatedFBIndex);
			String name = NameRepository.getInstance().getResourceUniqueFBInstanceName/*getUniqueResourceInstanceName*/(resource /*.getDevice()*/, "GENERATED");
			generatedFB.setName(name);
			generatedFBIndex++;
			StringBuilder newFBComment = new StringBuilder();
			newFBComment.append(GENERATED_ANNOTATION);
			newFBComment.append("; ");
			for (Connection connection : destination.getConnection()) {
				if (connection.getId() == null) {
					connection.setId(EcoreUtil.generateUUID());
					// TODO is this ok, should I also set identifier to true?
				}
				newFBComment.append(GENERATED_FOR_CONNECTION_ANNOTATION);
				newFBComment.append("=");
				newFBComment.append(connection.getId());
				newFBComment.append("; ");
				
				StringBuilder newConnectionComment = new StringBuilder();
				if (connection.getComment() != null) {
					newConnectionComment.append(connection.getComment());
				}
				newConnectionComment.append((end == ChannelEnd.SOURCE) ? GENERATED_SOURCE_FB_ANNOTATION : GENERATED_DESTINATION_FB_ANNOTATION);
				newConnectionComment.append("=");
				newConnectionComment.append(generatedFB.getId());
				newConnectionComment.append("; ");
				connection.setComment(newConnectionComment.toString());
			}
			generatedFB.setComment(newFBComment.toString());
			
			// TODO annotation
			
			generatedFBInfo = new GeneratedFBInfo(end, destination.getSelectedMedia().getSegment());
			generatedFBInfo.getDestinations().add(destination);
			generatedFBInfo.setFb(generatedFB);
			generatedFBInfo.setWithPorts(withPorts);
		
			generatedFBs.add(generatedFBInfo);
			
			if (end == ChannelEnd.SOURCE) {
				generateSourceConnections(generatedFBInfo, specificGenerator);
			} else {
				generateDestinationConnections(generatedFBInfo, specificGenerator);
			}
			generateInitConnection(generatedFBInfo);
		}
		return generatedFBInfo;
	}
	
	private void generateSourceConnections(GeneratedFBInfo generatedFBInfo, MediaSpecificGenerator specificGenerator) {
		
		// get channel for any destination, they must all be same...
		CommunicationChannel channel = generatedFBInfo.getDestinations().iterator().next().getCommunicationChannel();
		
		EventConnectionCreateCommand eventCreateCommand = new EventConnectionCreateCommand();
	
		eventCreateCommand.setSourceView(Utils.getInterfaceElementView(channel.getSourceEvent(), channel.getSourceResource()));
		
				
		Event targetEvent = generatedFBInfo.getFb().getInterface().getEventInputs().get(1);
		eventCreateCommand.setDestinationView(Utils.getInterfaceElementView(targetEvent, channel.getSourceResource()));
		
		eventCreateCommand.setParent(Utils.getDiagramForResource(channel.getSourceResource()));
		eventCreateCommand.execute();
		
		// TODO annotation
		int targetDataIndex = 0;
		for (int i = 0; i < channel.getSourceEvent().getWith().size(); i++) {
			if (generatedFBInfo.getWithPorts() != null && !generatedFBInfo.getWithPorts().contains(i)) {
				continue;
			}
			
			With with = channel.getSourceEvent().getWith().get(i);
			
			DataConnectionCreateCommand dataCreateCommand = new DataConnectionCreateCommand();
			
			dataCreateCommand.setSourceView(Utils.getInterfaceElementView(with.getVariables(), channel.getSourceResource()));
			
//			VarDeclaration targetData = generatedFBInfo.getFb().getInterface().getInputVars().get(targetDataIndex);
			VarDeclaration targetData = specificGenerator.getTargetInputData(targetDataIndex, generatedFBInfo.getFb());
			dataCreateCommand.setDestinationView(Utils.getInterfaceElementView(targetData, channel.getSourceResource()));
			
			dataCreateCommand.setParent(Utils.getDiagramForResource(channel.getSourceResource()));
			dataCreateCommand.execute();
			// TODO annotation
			targetDataIndex++;
		}
	}
	
	private void generateDestinationConnections(GeneratedFBInfo generatedFBInfo, MediaSpecificGenerator specificGenerator) {
		
		// destinations must have only one fb generated for them...
		CommunicationChannelDestination destination = generatedFBInfo.getDestinations().iterator().next();
		
		for (Integer withIndex : destination.getDestinationPorts().keySet()) {
			if (withIndex == -1) {
				for (IInterfaceElement interfaceElement : destination.getDestinationPorts().get(withIndex)) {
					EventConnectionCreateCommand eventCreateCommand = new EventConnectionCreateCommand();
					Event sourceEvent = generatedFBInfo.getFb().getInterface().getEventOutputs().get(1);
					eventCreateCommand.setSourceView(Utils.getInterfaceElementView(sourceEvent, destination.getDestinationResource()));
					
					eventCreateCommand.setDestinationView(Utils.getInterfaceElementView(interfaceElement, destination.getDestinationResource()));
					
					eventCreateCommand.setParent(Utils.getDiagramForResource(destination.getDestinationResource()));
					eventCreateCommand.execute();
					// TODO annotation
				}
			} else if (withIndex >= 0) {
				// calculated the index of port on the generated FB adding 1 for each with port used for generation which has smaller index than current with port
				int generatedPortIndex = 0;
				for (Integer generatedWithPort : generatedFBInfo.getWithPorts()) {
					if (generatedWithPort >= 0 && generatedWithPort < withIndex) {
						generatedPortIndex++;
					}
				}
				for (IInterfaceElement interfaceElement : destination.getDestinationPorts().get(withIndex)) {
					DataConnectionCreateCommand dataCreateCommand = new DataConnectionCreateCommand();
					//VarDeclaration sourcePort = generatedFBInfo.getFb().getInterface().getOutputVars().get(generatedPortIndex);
					VarDeclaration targetData = specificGenerator.getTargetOutputData(generatedPortIndex, generatedFBInfo.getFb());
					dataCreateCommand.setSourceView(Utils.getInterfaceElementView(targetData, destination.getDestinationResource()));
					
					dataCreateCommand.setDestinationView(Utils.getInterfaceElementView(interfaceElement, destination.getDestinationResource()));
					
					dataCreateCommand.setParent(Utils.getDiagramForResource(destination.getDestinationResource()));
					dataCreateCommand.execute();
					// TODO annotation
				}
			}
		}
	}
	
	private void generateInitConnection(GeneratedFBInfo generatedFBInfo) {
		ArrayList<Event> sourceEvents = new ArrayList<Event>();
		Resource resource = generatedFBInfo.getResource();
		Event targetEvent = generatedFBInfo.getFb().getInterface().getEventInputs().get(0);
		for (GeneratedFBInfo existingInfo : generatedFBs) {
			if (resource.equals(existingInfo.getResource()) && !generatedFBInfo.getFb().equals(existingInfo.getFb())) {
				sourceEvents.add(existingInfo.getFb().getInterface().getEventOutputs().get(0));
				break;
			}
		}
		if (sourceEvents.size() == 0) {
			FB startFB = resource.getFBNetwork().getFB("START");
			if (startFB == null) {
				System.err.println("No start FB in resource " + resource.getName() + "!");
				return;
			}
			sourceEvents.add(startFB.getInterface().getEventOutputs().get(0));
			sourceEvents.add(startFB.getInterface().getEventOutputs().get(1));
		}
		
		for (Event sourceEvent : sourceEvents) {
			EventConnectionCreateCommand eventCreateCommand = new EventConnectionCreateCommand();
			eventCreateCommand.setSourceView(Utils.getInterfaceElementView(sourceEvent, resource));
			
			eventCreateCommand.setDestinationView(Utils.getInterfaceElementView(targetEvent, resource));
			
			eventCreateCommand.setParent(Utils.getDiagramForResource(resource));
			eventCreateCommand.execute();
			// TODO annotation
		}
	}

	
		
	private GeneratedFBInfo findExistingGeneratedFBInfo(ChannelEnd end, CommunicationChannel channel, Set<Integer> withPorts, Segment segment) {
		for (GeneratedFBInfo generatedFBInfo : generatedFBs) {
			if (generatedFBInfo.getEnd() == end) {
				if (generatedFBInfo.getChannel().equals(channel))
				if ((generatedFBInfo.getWithPorts() == null && withPorts == null)
						|| generatedFBInfo.getWithPorts().equals(withPorts)) {
					if (generatedFBInfo.getSegment().equals(segment)) {
						return generatedFBInfo;
					}
				}
			}
		}
		return null;
	}
	
	public static enum TransferedData {
		ALL,
		EXACT
	}
	
	private class GeneratedFBInfo {
		private FB fb;
		private HashSet<CommunicationChannelDestination> destinations;
		private Set<Integer> withPorts;
		private Segment segment;
		private ChannelEnd end;
		
		public GeneratedFBInfo(ChannelEnd end, Segment segment) {
			super();
			this.end = end;
			destinations = new HashSet<CommunicationChannelDestination>();
			this.segment = segment;
		}

		public FB getFb() {
			return fb;
		}

		public void setFb(FB fb) {
			this.fb = fb;
		}

		public Set<Integer> getWithPorts() {
			return withPorts;
		}

		public void setWithPorts(Set<Integer> withPorts) {
			this.withPorts = withPorts;
		}

		public HashSet<CommunicationChannelDestination> getDestinations() {
			return destinations;
		}

		public ChannelEnd getEnd() {
			return end;
		}

		public void setEnd(ChannelEnd end) {
			this.end = end;
		}

		public Segment getSegment() {
			return segment;
		}

		public void setSegment(Segment segment) {
			this.segment = segment;
		}
		
		public Resource getResource() {
			return (end == ChannelEnd.SOURCE) ? 
					destinations.iterator().next().getCommunicationChannel().getSourceResource() 
					: destinations.iterator().next().getDestinationResource();
		}
		
		public CommunicationChannel getChannel() {
			return destinations.iterator().next().getCommunicationChannel();
		}
		
	}
	
	public void removeGeneratedElements(Application application) {
		HashSet<ResourceFBNetwork> usedResources = new HashSet<ResourceFBNetwork>();
		for (FB fb : application.getFBNetwork().getFBs()) {
			if (fb.getResource() != null) {
				usedResources.add(fb.getResource());
			}
		}
		
		for (ResourceFBNetwork resourceFBNetwork : usedResources) {
			removeGeneratedElements(resourceFBNetwork);
		}
	}
	
	public void removeGeneratedElements(ResourceFBNetwork network) {
		HashSet<FB> fbsToRemove = new HashSet<FB>();
		for (FB fb : network.getFBs()) {
			if (fb.getComment() != null && fb.getComment().contains(GENERATED_ANNOTATION)) {
				fbsToRemove.add(fb);
			}
		}
		
		for (FB fb : fbsToRemove) {
			FBView fbView = Utils.getFBView(fb);
			DeleteFBCommand deleteFBCommand = new DeleteFBCommand(fbView);
			deleteFBCommand.execute();
		}
	}
}
