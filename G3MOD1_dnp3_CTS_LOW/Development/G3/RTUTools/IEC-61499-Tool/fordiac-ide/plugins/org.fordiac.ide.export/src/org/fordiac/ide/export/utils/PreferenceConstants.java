/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.export.utils;

/**
 * Constant definitions.
 */
public class PreferenceConstants {

	/** The Constant for the Compare Editor Preference. */
	public static final String P_COMPARE_EDITOR = "compareeditor";

}
