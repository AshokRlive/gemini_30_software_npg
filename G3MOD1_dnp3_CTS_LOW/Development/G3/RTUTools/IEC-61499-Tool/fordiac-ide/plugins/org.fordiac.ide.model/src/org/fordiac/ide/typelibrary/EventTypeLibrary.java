/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.typelibrary;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.fordiac.ide.model.data.DataFactory;
import org.fordiac.ide.model.data.DataType;
import org.fordiac.ide.model.data.ElementaryType;
import org.fordiac.ide.model.data.EventType;

/**
 * The Class DataTypeLibrary.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class EventTypeLibrary {

	/** The type map. */
	protected Map<String, DataType> typeMap;

	/**
	 * Instantiates a new data type library.
	 */
	private EventTypeLibrary() {
		typeMap = new HashMap<String, DataType>();
		initElementaryTypes();
	}

	/** The instance. */
	private static EventTypeLibrary instance;

	/**
	 * Gets the single instance of DataTypeLibrary.
	 * 
	 * @return single instance of DataTypeLibrary
	 */
	public static EventTypeLibrary getInstance() {
		if (instance == null) {
			instance = new EventTypeLibrary();

		}
		return instance;
	}

	/**
	 * Inits the elementary types.
	 */
	private void initElementaryTypes() {
		if (typeMap == null) {
			typeMap = new HashMap<String, DataType>();
		}

		EventType type = DataFactory.eINSTANCE.createEventType();
		type.setName("Event");
		typeMap.put("Event", type);

		// // List<> BaseType1.VALUES;
		// List<BaseType1> elementaryTypes = BaseType1.VALUES;
		//
		// for (Iterator<BaseType1> iterator = elementaryTypes.iterator();
		// iterator
		// .hasNext();) {
		// BaseType1 baseType = iterator.next();
		// type = DataFactory.eINSTANCE.createElementaryType();
		// type.setName(baseType.getLiteral());
		// typeMap.put(baseType.getLiteral(), type);
		// }
	}

	/**
	 * Gets the event types.
	 * 
	 * @return the event types
	 */
	public Collection<DataType> getEventTypes() {
		return typeMap.values();
	}

	/**
	 * FIXME only return type if it really exists! --> after parsing/importing
	 * of types is implemented --> planned for V0.3
	 * 
	 * @param name the name
	 * 
	 * @return the type
	 */
	public DataType getType(final String name) {
		if (name == null) {
			return typeMap.get("Event"); //$NON-NLS-1$
		}
		Object value = typeMap.get(name.toUpperCase());
		if (value != null) {
			return (DataType) value;
		} else {
			ElementaryType type = DataFactory.eINSTANCE.createElementaryType();
			type.setName(name);
			typeMap.put(name, type);
			return type;
		}

	}

}
