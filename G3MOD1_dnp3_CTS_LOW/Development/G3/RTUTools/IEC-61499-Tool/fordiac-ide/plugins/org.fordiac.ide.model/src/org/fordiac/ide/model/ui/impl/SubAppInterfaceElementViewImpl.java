/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Sub App Interface Element View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.SubAppInterfaceElementViewImpl#isVisible <em>Visible</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.SubAppInterfaceElementViewImpl#getLabelSubstitute <em>Label Substitute</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubAppInterfaceElementViewImpl extends InterfaceElementViewImpl
		implements SubAppInterfaceElementView {
	/**
	 * The default value of the '{@link #isVisible() <em>Visible</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isVisible()
	 * @generated
	 * @ordered
	 */
	protected static final boolean VISIBLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isVisible() <em>Visible</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isVisible()
	 * @generated
	 * @ordered
	 */
	protected boolean visible = VISIBLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabelSubstitute() <em>Label Substitute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelSubstitute()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_SUBSTITUTE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabelSubstitute() <em>Label Substitute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelSubstitute()
	 * @generated
	 * @ordered
	 */
	protected String labelSubstitute = LABEL_SUBSTITUTE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected SubAppInterfaceElementViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.SUB_APP_INTERFACE_ELEMENT_VIEW;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setVisible(final boolean newVisible) {
		if (visible != newVisible) {
			boolean oldVisible = visible;
			visible = newVisible;
			if (eNotificationRequired()) {
				eNotify(new ENotificationImpl(this, Notification.SET,
						UiPackage.SUB_APP_INTERFACE_ELEMENT_VIEW__VISIBLE,
						oldVisible, visible));
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabelSubstitute() {
		return labelSubstitute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabelSubstitute(String newLabelSubstitute) {
		String oldLabelSubstitute = labelSubstitute;
		labelSubstitute = newLabelSubstitute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL_SUBSTITUTE, oldLabelSubstitute, labelSubstitute));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.SUB_APP_INTERFACE_ELEMENT_VIEW__VISIBLE:
				return isVisible();
			case UiPackage.SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL_SUBSTITUTE:
				return getLabelSubstitute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.SUB_APP_INTERFACE_ELEMENT_VIEW__VISIBLE:
				setVisible((Boolean)newValue);
				return;
			case UiPackage.SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL_SUBSTITUTE:
				setLabelSubstitute((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.SUB_APP_INTERFACE_ELEMENT_VIEW__VISIBLE:
				setVisible(VISIBLE_EDEFAULT);
				return;
			case UiPackage.SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL_SUBSTITUTE:
				setLabelSubstitute(LABEL_SUBSTITUTE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.SUB_APP_INTERFACE_ELEMENT_VIEW__VISIBLE:
				return visible != VISIBLE_EDEFAULT;
			case UiPackage.SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL_SUBSTITUTE:
				return LABEL_SUBSTITUTE_EDEFAULT == null ? labelSubstitute != null : !LABEL_SUBSTITUTE_EDEFAULT.equals(labelSubstitute);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (visible: ");
		result.append(visible);
		result.append(", labelSubstitute: ");
		result.append(labelSubstitute);
		result.append(')');
		return result.toString();
	}

	@Override
	public String getLabel() {
		if (labelSubstitute != null && !labelSubstitute.isEmpty()) {
			return labelSubstitute;
		}
		
		IInterfaceElement element = getIInterfaceElement();
		StringBuffer label = new StringBuffer();
		if (element != null) {
			InterfaceList interfaceList = (InterfaceList) element.eContainer();
			FB fb = (FB) interfaceList.eContainer();
			EObject container = fb.eContainer();
			while (container instanceof SubAppNetwork) {
				if (((SubAppNetwork) container).getParentSubApp() != null
						&& !((SubAppNetwork) container).getParentSubApp()
								.equals(
										((SubAppView) this.eContainer())
												.getSubApp())) {
					label.insert(0, ".");
					label.insert(0, ((SubAppNetwork) container)
							.getParentSubApp().getName());
				}
				container = container.eContainer();
			}
			label.append(fb.getName());
			label.append(".");
		}
		label.append(super.getLabel());
		return label.toString();
	}

	// @Override
	// public void setLabel(String newLabel) {
	// String oldLabel = label;
	// label = newLabel;
	// if (eNotificationRequired())
	// eNotify(new ENotificationImpl(this, Notification.SET,
	// UiPackage.INTERFACE_ELEMENT_VIEW__LABEL, oldLabel, label));
	//
	// }

} // SubAppInterfaceElementViewImpl
