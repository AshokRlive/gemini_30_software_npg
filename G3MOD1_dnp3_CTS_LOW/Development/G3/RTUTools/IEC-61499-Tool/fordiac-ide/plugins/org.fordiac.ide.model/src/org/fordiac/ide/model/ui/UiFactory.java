/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.fordiac.ide.model.ui.UiPackage
 * @generated
 */
public interface UiFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UiFactory eINSTANCE = org.fordiac.ide.model.ui.impl.UiFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>View</em>'.
	 * @generated
	 */
	View createView();

	/**
	 * Returns a new object of class '<em>UIFB Network</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UIFB Network</em>'.
	 * @generated
	 */
	UIFBNetwork createUIFBNetwork();

	/**
	 * Returns a new object of class '<em>FB View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FB View</em>'.
	 * @generated
	 */
	FBView createFBView();

	/**
	 * Returns a new object of class '<em>Position</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Position</em>'.
	 * @generated
	 */
	Position createPosition();

	/**
	 * Returns a new object of class '<em>Interface Element View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interface Element View</em>'.
	 * @generated
	 */
	InterfaceElementView createInterfaceElementView();

	/**
	 * Returns a new object of class '<em>Connection View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection View</em>'.
	 * @generated
	 */
	ConnectionView createConnectionView();

	/**
	 * Returns a new object of class '<em>Sub App View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sub App View</em>'.
	 * @generated
	 */
	SubAppView createSubAppView();

	/**
	 * Returns a new object of class '<em>Sub App Interface Element View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sub App Interface Element View</em>'.
	 * @generated
	 */
	SubAppInterfaceElementView createSubAppInterfaceElementView();

	/**
	 * Returns a new object of class '<em>UI Sub App Network</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UI Sub App Network</em>'.
	 * @generated
	 */
	UISubAppNetwork createUISubAppNetwork();

	/**
	 * Returns a new object of class '<em>Internal Sub App Interface Element View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Internal Sub App Interface Element View</em>'.
	 * @generated
	 */
	InternalSubAppInterfaceElementView createInternalSubAppInterfaceElementView();

	/**
	 * Returns a new object of class '<em>Color</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Color</em>'.
	 * @generated
	 */
	Color createColor();

	/**
	 * Returns a new object of class '<em>UI System Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UI System Configuration</em>'.
	 * @generated
	 */
	UISystemConfiguration createUISystemConfiguration();

	/**
	 * Returns a new object of class '<em>Device View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Device View</em>'.
	 * @generated
	 */
	DeviceView createDeviceView();

	/**
	 * Returns a new object of class '<em>Resource View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Resource View</em>'.
	 * @generated
	 */
	ResourceView createResourceView();

	/**
	 * Returns a new object of class '<em>Resource Container View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Resource Container View</em>'.
	 * @generated
	 */
	ResourceContainerView createResourceContainerView();

	/**
	 * Returns a new object of class '<em>UI Resource Editor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UI Resource Editor</em>'.
	 * @generated
	 */
	UIResourceEditor createUIResourceEditor();

	/**
	 * Returns a new object of class '<em>Resize UIFB Network</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Resize UIFB Network</em>'.
	 * @generated
	 */
	ResizeUIFBNetwork createResizeUIFBNetwork();

	/**
	 * Returns a new object of class '<em>Size</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Size</em>'.
	 * @generated
	 */
	Size createSize();

	/**
	 * Returns a new object of class '<em>Mapped Sub App View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mapped Sub App View</em>'.
	 * @generated
	 */
	MappedSubAppView createMappedSubAppView();

	/**
	 * Returns a new object of class '<em>Mapped Sub App Interface Element View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mapped Sub App Interface Element View</em>'.
	 * @generated
	 */
	MappedSubAppInterfaceElementView createMappedSubAppInterfaceElementView();

	/**
	 * Returns a new object of class '<em>Segment View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Segment View</em>'.
	 * @generated
	 */
	SegmentView createSegmentView();

	/**
	 * Returns a new object of class '<em>Link View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Link View</em>'.
	 * @generated
	 */
	LinkView createLinkView();

	/**
	 * Returns a new object of class '<em>Composite Internal Interface Element View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composite Internal Interface Element View</em>'.
	 * @generated
	 */
	CompositeInternalInterfaceElementView createCompositeInternalInterfaceElementView();

	/**
	 * Returns a new object of class '<em>Monitoring View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Monitoring View</em>'.
	 * @generated
	 */
	MonitoringView createMonitoringView();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	UiPackage getUiPackage();

} //UiFactory
