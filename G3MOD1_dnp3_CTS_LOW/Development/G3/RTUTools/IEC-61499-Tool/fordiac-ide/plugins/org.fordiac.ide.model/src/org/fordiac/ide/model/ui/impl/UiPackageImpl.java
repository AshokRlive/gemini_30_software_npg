/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;
import org.fordiac.ide.model.Palette.PalettePackage;
import org.fordiac.ide.model.Palette.impl.PalettePackageImpl;
import org.fordiac.ide.model.data.DataPackage;
import org.fordiac.ide.model.data.impl.DataPackageImpl;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.impl.LibraryElementPackageImpl;
import org.fordiac.ide.model.ui.Color;
import org.fordiac.ide.model.ui.CompositeInternalInterfaceElementView;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.IConnectionView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.LinkView;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.MonitoringView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.ResizeUIFBNetwork;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.SegmentView;
import org.fordiac.ide.model.ui.Size;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.model.ui.UiPackage;
import org.fordiac.ide.model.ui.View;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class UiPackageImpl extends EPackageImpl implements UiPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass viewEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diagramEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uifbNetworkEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fbViewEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass positionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interfaceElementViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subAppViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subAppInterfaceElementViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uiSubAppNetworkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass internalSubAppInterfaceElementViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass colorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uiSystemConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deviceViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceContainerViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uiResourceEditorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resizeUIFBNetworkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sizeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mappedSubAppViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mappedSubAppInterfaceElementViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass segmentViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass linkViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iConnectionViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeInternalInterfaceElementViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass monitoringViewEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.fordiac.ide.model.ui.UiPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private UiPackageImpl() {
		super(eNS_URI, UiFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link UiPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static UiPackage init() {
		if (isInited) return (UiPackage)EPackage.Registry.INSTANCE.getEPackage(UiPackage.eNS_URI);

		// Obtain or create and register package
		UiPackageImpl theUiPackage = (UiPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof UiPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new UiPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		PalettePackageImpl thePalettePackage = (PalettePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PalettePackage.eNS_URI) instanceof PalettePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PalettePackage.eNS_URI) : PalettePackage.eINSTANCE);
		LibraryElementPackageImpl theLibraryElementPackage = (LibraryElementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(LibraryElementPackage.eNS_URI) instanceof LibraryElementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(LibraryElementPackage.eNS_URI) : LibraryElementPackage.eINSTANCE);
		DataPackageImpl theDataPackage = (DataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) instanceof DataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) : DataPackage.eINSTANCE);

		// Create package meta-data objects
		theUiPackage.createPackageContents();
		thePalettePackage.createPackageContents();
		theLibraryElementPackage.createPackageContents();
		theDataPackage.createPackageContents();

		// Initialize created meta-data
		theUiPackage.initializePackageContents();
		thePalettePackage.initializePackageContents();
		theLibraryElementPackage.initializePackageContents();
		theDataPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUiPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(UiPackage.eNS_URI, theUiPackage);
		return theUiPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getView() {
		return viewEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getView_Position() {
		return (EReference)viewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getView_BackgroundColor() {
		return (EReference)viewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDiagram() {
		return diagramEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDiagram_Children() {
		return (EReference)diagramEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDiagram_Connections() {
		return (EReference)diagramEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUIFBNetwork() {
		return uifbNetworkEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUIFBNetwork_FbNetwork() {
		return (EReference)uifbNetworkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFBView() {
		return fbViewEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFBView_Fb() {
		return (EReference)fbViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFBView_InterfaceElements() {
		return (EReference)fbViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFBView_ResourceTypeFB() {
		return (EAttribute)fbViewEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFBView_MappedFB() {
		return (EReference)fbViewEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFBView_ApplicationFB() {
		return (EReference)fbViewEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPosition() {
		return positionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPosition_X() {
		return (EAttribute)positionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPosition_Y() {
		return (EAttribute)positionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterfaceElementView() {
		return interfaceElementViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterfaceElementView_IInterfaceElement() {
		return (EReference)interfaceElementViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterfaceElementView_InConnections() {
		return (EReference)interfaceElementViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterfaceElementView_OutConnections() {
		return (EReference)interfaceElementViewEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInterfaceElementView_Label() {
		return (EAttribute)interfaceElementViewEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterfaceElementView_MappedInterfaceElement() {
		return (EReference)interfaceElementViewEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterfaceElementView_FbNetwork() {
		return (EReference)interfaceElementViewEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterfaceElementView_ApplicationInterfaceElement() {
		return (EReference)interfaceElementViewEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionView() {
		return connectionViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionView_ConnectionElement() {
		return (EReference)connectionViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionView_Source() {
		return (EReference)connectionViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionView_Destination() {
		return (EReference)connectionViewEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionView_ExternalDestinationInterfaceElementView() {
		return (EReference)connectionViewEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionView_InternalDestinationInterfaceElementView() {
		return (EReference)connectionViewEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionView_ExternalSourceInterfaceElementView() {
		return (EReference)connectionViewEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionView_InternalSourceInterfaceElementView() {
		return (EReference)connectionViewEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubAppView() {
		return subAppViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubAppView_SubApp() {
		return (EReference)subAppViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubAppView_InterfaceElements() {
		return (EReference)subAppViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubAppView_UiSubAppNetwork() {
		return (EReference)subAppViewEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubAppView_MappedSubApp() {
		return (EReference)subAppViewEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubAppInterfaceElementView() {
		return subAppInterfaceElementViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubAppInterfaceElementView_Visible() {
		return (EAttribute)subAppInterfaceElementViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubAppInterfaceElementView_LabelSubstitute() {
		return (EAttribute)subAppInterfaceElementViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUISubAppNetwork() {
		return uiSubAppNetworkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUISubAppNetwork_SubAppNetwork() {
		return (EReference)uiSubAppNetworkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUISubAppNetwork_FileName() {
		return (EAttribute)uiSubAppNetworkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUISubAppNetwork_InterfaceElements() {
		return (EReference)uiSubAppNetworkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUISubAppNetwork_SubAppView() {
		return (EReference)uiSubAppNetworkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUISubAppNetwork_RootApplication() {
		return (EReference)uiSubAppNetworkEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInternalSubAppInterfaceElementView() {
		return internalSubAppInterfaceElementViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getColor() {
		return colorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getColor_Red() {
		return (EAttribute)colorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getColor_Green() {
		return (EAttribute)colorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getColor_Blue() {
		return (EAttribute)colorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUISystemConfiguration() {
		return uiSystemConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUISystemConfiguration_SystemConfigNetwork() {
		return (EReference)uiSystemConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeviceView() {
		return deviceViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeviceView_DeviceElement() {
		return (EReference)deviceViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeviceView_ShowResorceList() {
		return (EAttribute)deviceViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeviceView_InterfaceElements() {
		return (EReference)deviceViewEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeviceView_ResourceContainerView() {
		return (EReference)deviceViewEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeviceView_InConnections() {
		return (EReference)deviceViewEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResourceView() {
		return resourceViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceView_ResourceElement() {
		return (EReference)resourceViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceView_InterfaceElements() {
		return (EReference)resourceViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResourceView_DeviceTypeResource() {
		return (EAttribute)resourceViewEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceView_UIResourceDiagram() {
		return (EReference)resourceViewEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResourceContainerView() {
		return resourceContainerViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResourceContainerView_ShowContent() {
		return (EAttribute)resourceContainerViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResourceContainerView_Resources() {
		return (EReference)resourceContainerViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUIResourceEditor() {
		return uiResourceEditorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUIResourceEditor_ResourceElement() {
		return (EReference)uiResourceEditorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUIResourceEditor_FbNetworks() {
		return (EReference)uiResourceEditorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUIResourceEditor_ShowResizeUIFBNetworks() {
		return (EAttribute)uiResourceEditorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUIResourceEditor_ActiveNetwork() {
		return (EReference)uiResourceEditorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUIResourceEditor_MappingEditorSize() {
		return (EReference)uiResourceEditorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUIResourceEditor_SystemConfiguration() {
		return (EReference)uiResourceEditorEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResizeUIFBNetwork() {
		return resizeUIFBNetworkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResizeUIFBNetwork_Size() {
		return (EReference)resizeUIFBNetworkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResizeUIFBNetwork_UiFBNetwork() {
		return (EReference)resizeUIFBNetworkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSize() {
		return sizeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSize_Width() {
		return (EAttribute)sizeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSize_Height() {
		return (EAttribute)sizeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMappedSubAppView() {
		return mappedSubAppViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMappedSubAppView_InterfaceElements() {
		return (EReference)mappedSubAppViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMappedSubAppView_ApplicationSubApp() {
		return (EReference)mappedSubAppViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMappedSubAppView_SubApp() {
		return (EReference)mappedSubAppViewEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMappedSubAppInterfaceElementView() {
		return mappedSubAppInterfaceElementViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSegmentView() {
		return segmentViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentView_Segment() {
		return (EReference)segmentViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentView_OutConnections() {
		return (EReference)segmentViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentView_Size() {
		return (EReference)segmentViewEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLinkView() {
		return linkViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLinkView_Source() {
		return (EReference)linkViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLinkView_Link() {
		return (EReference)linkViewEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLinkView_Destination() {
		return (EReference)linkViewEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIConnectionView() {
		return iConnectionViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeInternalInterfaceElementView() {
		return compositeInternalInterfaceElementViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMonitoringView() {
		return monitoringViewEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public UiFactory getUiFactory() {
		return (UiFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		viewEClass = createEClass(VIEW);
		createEReference(viewEClass, VIEW__POSITION);
		createEReference(viewEClass, VIEW__BACKGROUND_COLOR);

		diagramEClass = createEClass(DIAGRAM);
		createEReference(diagramEClass, DIAGRAM__CHILDREN);
		createEReference(diagramEClass, DIAGRAM__CONNECTIONS);

		uifbNetworkEClass = createEClass(UIFB_NETWORK);
		createEReference(uifbNetworkEClass, UIFB_NETWORK__FB_NETWORK);

		fbViewEClass = createEClass(FB_VIEW);
		createEReference(fbViewEClass, FB_VIEW__FB);
		createEReference(fbViewEClass, FB_VIEW__INTERFACE_ELEMENTS);
		createEAttribute(fbViewEClass, FB_VIEW__RESOURCE_TYPE_FB);
		createEReference(fbViewEClass, FB_VIEW__MAPPED_FB);
		createEReference(fbViewEClass, FB_VIEW__APPLICATION_FB);

		positionEClass = createEClass(POSITION);
		createEAttribute(positionEClass, POSITION__X);
		createEAttribute(positionEClass, POSITION__Y);

		interfaceElementViewEClass = createEClass(INTERFACE_ELEMENT_VIEW);
		createEReference(interfaceElementViewEClass, INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT);
		createEReference(interfaceElementViewEClass, INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS);
		createEReference(interfaceElementViewEClass, INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS);
		createEAttribute(interfaceElementViewEClass, INTERFACE_ELEMENT_VIEW__LABEL);
		createEReference(interfaceElementViewEClass, INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT);
		createEReference(interfaceElementViewEClass, INTERFACE_ELEMENT_VIEW__FB_NETWORK);
		createEReference(interfaceElementViewEClass, INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT);

		connectionViewEClass = createEClass(CONNECTION_VIEW);
		createEReference(connectionViewEClass, CONNECTION_VIEW__CONNECTION_ELEMENT);
		createEReference(connectionViewEClass, CONNECTION_VIEW__SOURCE);
		createEReference(connectionViewEClass, CONNECTION_VIEW__DESTINATION);
		createEReference(connectionViewEClass, CONNECTION_VIEW__EXTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW);
		createEReference(connectionViewEClass, CONNECTION_VIEW__INTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW);
		createEReference(connectionViewEClass, CONNECTION_VIEW__EXTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW);
		createEReference(connectionViewEClass, CONNECTION_VIEW__INTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW);

		subAppViewEClass = createEClass(SUB_APP_VIEW);
		createEReference(subAppViewEClass, SUB_APP_VIEW__SUB_APP);
		createEReference(subAppViewEClass, SUB_APP_VIEW__INTERFACE_ELEMENTS);
		createEReference(subAppViewEClass, SUB_APP_VIEW__UI_SUB_APP_NETWORK);
		createEReference(subAppViewEClass, SUB_APP_VIEW__MAPPED_SUB_APP);

		subAppInterfaceElementViewEClass = createEClass(SUB_APP_INTERFACE_ELEMENT_VIEW);
		createEAttribute(subAppInterfaceElementViewEClass, SUB_APP_INTERFACE_ELEMENT_VIEW__VISIBLE);
		createEAttribute(subAppInterfaceElementViewEClass, SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL_SUBSTITUTE);

		uiSubAppNetworkEClass = createEClass(UI_SUB_APP_NETWORK);
		createEReference(uiSubAppNetworkEClass, UI_SUB_APP_NETWORK__SUB_APP_NETWORK);
		createEAttribute(uiSubAppNetworkEClass, UI_SUB_APP_NETWORK__FILE_NAME);
		createEReference(uiSubAppNetworkEClass, UI_SUB_APP_NETWORK__INTERFACE_ELEMENTS);
		createEReference(uiSubAppNetworkEClass, UI_SUB_APP_NETWORK__SUB_APP_VIEW);
		createEReference(uiSubAppNetworkEClass, UI_SUB_APP_NETWORK__ROOT_APPLICATION);

		internalSubAppInterfaceElementViewEClass = createEClass(INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW);

		colorEClass = createEClass(COLOR);
		createEAttribute(colorEClass, COLOR__RED);
		createEAttribute(colorEClass, COLOR__GREEN);
		createEAttribute(colorEClass, COLOR__BLUE);

		uiSystemConfigurationEClass = createEClass(UI_SYSTEM_CONFIGURATION);
		createEReference(uiSystemConfigurationEClass, UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK);

		deviceViewEClass = createEClass(DEVICE_VIEW);
		createEReference(deviceViewEClass, DEVICE_VIEW__DEVICE_ELEMENT);
		createEAttribute(deviceViewEClass, DEVICE_VIEW__SHOW_RESORCE_LIST);
		createEReference(deviceViewEClass, DEVICE_VIEW__INTERFACE_ELEMENTS);
		createEReference(deviceViewEClass, DEVICE_VIEW__RESOURCE_CONTAINER_VIEW);
		createEReference(deviceViewEClass, DEVICE_VIEW__IN_CONNECTIONS);

		resourceViewEClass = createEClass(RESOURCE_VIEW);
		createEReference(resourceViewEClass, RESOURCE_VIEW__RESOURCE_ELEMENT);
		createEReference(resourceViewEClass, RESOURCE_VIEW__INTERFACE_ELEMENTS);
		createEAttribute(resourceViewEClass, RESOURCE_VIEW__DEVICE_TYPE_RESOURCE);
		createEReference(resourceViewEClass, RESOURCE_VIEW__UI_RESOURCE_DIAGRAM);

		resourceContainerViewEClass = createEClass(RESOURCE_CONTAINER_VIEW);
		createEAttribute(resourceContainerViewEClass, RESOURCE_CONTAINER_VIEW__SHOW_CONTENT);
		createEReference(resourceContainerViewEClass, RESOURCE_CONTAINER_VIEW__RESOURCES);

		uiResourceEditorEClass = createEClass(UI_RESOURCE_EDITOR);
		createEReference(uiResourceEditorEClass, UI_RESOURCE_EDITOR__RESOURCE_ELEMENT);
		createEReference(uiResourceEditorEClass, UI_RESOURCE_EDITOR__FB_NETWORKS);
		createEAttribute(uiResourceEditorEClass, UI_RESOURCE_EDITOR__SHOW_RESIZE_UIFB_NETWORKS);
		createEReference(uiResourceEditorEClass, UI_RESOURCE_EDITOR__ACTIVE_NETWORK);
		createEReference(uiResourceEditorEClass, UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE);
		createEReference(uiResourceEditorEClass, UI_RESOURCE_EDITOR__SYSTEM_CONFIGURATION);

		resizeUIFBNetworkEClass = createEClass(RESIZE_UIFB_NETWORK);
		createEReference(resizeUIFBNetworkEClass, RESIZE_UIFB_NETWORK__SIZE);
		createEReference(resizeUIFBNetworkEClass, RESIZE_UIFB_NETWORK__UI_FB_NETWORK);

		sizeEClass = createEClass(SIZE);
		createEAttribute(sizeEClass, SIZE__WIDTH);
		createEAttribute(sizeEClass, SIZE__HEIGHT);

		mappedSubAppViewEClass = createEClass(MAPPED_SUB_APP_VIEW);
		createEReference(mappedSubAppViewEClass, MAPPED_SUB_APP_VIEW__INTERFACE_ELEMENTS);
		createEReference(mappedSubAppViewEClass, MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP);
		createEReference(mappedSubAppViewEClass, MAPPED_SUB_APP_VIEW__SUB_APP);

		mappedSubAppInterfaceElementViewEClass = createEClass(MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW);

		segmentViewEClass = createEClass(SEGMENT_VIEW);
		createEReference(segmentViewEClass, SEGMENT_VIEW__SEGMENT);
		createEReference(segmentViewEClass, SEGMENT_VIEW__OUT_CONNECTIONS);
		createEReference(segmentViewEClass, SEGMENT_VIEW__SIZE);

		linkViewEClass = createEClass(LINK_VIEW);
		createEReference(linkViewEClass, LINK_VIEW__SOURCE);
		createEReference(linkViewEClass, LINK_VIEW__LINK);
		createEReference(linkViewEClass, LINK_VIEW__DESTINATION);

		iConnectionViewEClass = createEClass(ICONNECTION_VIEW);

		compositeInternalInterfaceElementViewEClass = createEClass(COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW);

		monitoringViewEClass = createEClass(MONITORING_VIEW);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		LibraryElementPackage theLibraryElementPackage = (LibraryElementPackage)EPackage.Registry.INSTANCE.getEPackage(LibraryElementPackage.eNS_URI);
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		uifbNetworkEClass.getESuperTypes().add(this.getDiagram());
		fbViewEClass.getESuperTypes().add(this.getView());
		interfaceElementViewEClass.getESuperTypes().add(this.getView());
		connectionViewEClass.getESuperTypes().add(this.getIConnectionView());
		subAppViewEClass.getESuperTypes().add(this.getView());
		subAppInterfaceElementViewEClass.getESuperTypes().add(this.getInterfaceElementView());
		uiSubAppNetworkEClass.getESuperTypes().add(this.getDiagram());
		internalSubAppInterfaceElementViewEClass.getESuperTypes().add(this.getSubAppInterfaceElementView());
		uiSystemConfigurationEClass.getESuperTypes().add(this.getDiagram());
		deviceViewEClass.getESuperTypes().add(this.getView());
		resourceViewEClass.getESuperTypes().add(this.getView());
		resourceContainerViewEClass.getESuperTypes().add(this.getView());
		uiResourceEditorEClass.getESuperTypes().add(this.getDiagram());
		mappedSubAppViewEClass.getESuperTypes().add(this.getView());
		mappedSubAppInterfaceElementViewEClass.getESuperTypes().add(this.getSubAppInterfaceElementView());
		segmentViewEClass.getESuperTypes().add(this.getView());
		linkViewEClass.getESuperTypes().add(this.getIConnectionView());
		compositeInternalInterfaceElementViewEClass.getESuperTypes().add(this.getInterfaceElementView());
		monitoringViewEClass.getESuperTypes().add(this.getView());

		// Initialize classes and features; add operations and parameters
		initEClass(viewEClass, View.class, "View", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getView_Position(), this.getPosition(), null, "position", null, 0, 1, View.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getView_BackgroundColor(), this.getColor(), null, "backgroundColor", null, 0, 1, View.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(diagramEClass, Diagram.class, "Diagram", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDiagram_Children(), this.getView(), null, "children", null, 0, -1, Diagram.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDiagram_Connections(), this.getIConnectionView(), null, "connections", null, 0, -1, Diagram.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uifbNetworkEClass, UIFBNetwork.class, "UIFBNetwork", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUIFBNetwork_FbNetwork(), theLibraryElementPackage.getFBNetwork(), null, "fbNetwork", null, 1, 1, UIFBNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fbViewEClass, FBView.class, "FBView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFBView_Fb(), theLibraryElementPackage.getFB(), null, "fb", null, 1, 1, FBView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFBView_InterfaceElements(), this.getInterfaceElementView(), null, "interfaceElements", null, 0, -1, FBView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFBView_ResourceTypeFB(), theXMLTypePackage.getBoolean(), "resourceTypeFB", null, 0, 1, FBView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFBView_MappedFB(), this.getFBView(), this.getFBView_ApplicationFB(), "mappedFB", null, 0, 1, FBView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFBView_ApplicationFB(), this.getFBView(), this.getFBView_MappedFB(), "applicationFB", null, 0, 1, FBView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(positionEClass, Position.class, "Position", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPosition_X(), theXMLTypePackage.getInt(), "x", null, 0, 1, Position.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPosition_Y(), theXMLTypePackage.getInt(), "y", null, 0, 1, Position.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interfaceElementViewEClass, InterfaceElementView.class, "InterfaceElementView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInterfaceElementView_IInterfaceElement(), theLibraryElementPackage.getIInterfaceElement(), null, "iInterfaceElement", null, 1, 1, InterfaceElementView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInterfaceElementView_InConnections(), this.getConnectionView(), this.getConnectionView_Destination(), "inConnections", null, 0, -1, InterfaceElementView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInterfaceElementView_OutConnections(), this.getConnectionView(), this.getConnectionView_Source(), "outConnections", null, 0, -1, InterfaceElementView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInterfaceElementView_Label(), ecorePackage.getEString(), "label", null, 0, 1, InterfaceElementView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInterfaceElementView_MappedInterfaceElement(), this.getInterfaceElementView(), this.getInterfaceElementView_ApplicationInterfaceElement(), "mappedInterfaceElement", null, 0, 1, InterfaceElementView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInterfaceElementView_FbNetwork(), theLibraryElementPackage.getSubAppNetwork(), null, "fbNetwork", null, 0, 1, InterfaceElementView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInterfaceElementView_ApplicationInterfaceElement(), this.getInterfaceElementView(), this.getInterfaceElementView_MappedInterfaceElement(), "applicationInterfaceElement", null, 0, 1, InterfaceElementView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(connectionViewEClass, ConnectionView.class, "ConnectionView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnectionView_ConnectionElement(), theLibraryElementPackage.getConnection(), null, "connectionElement", null, 0, 1, ConnectionView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionView_Source(), this.getInterfaceElementView(), this.getInterfaceElementView_OutConnections(), "source", null, 0, 1, ConnectionView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionView_Destination(), this.getInterfaceElementView(), this.getInterfaceElementView_InConnections(), "destination", null, 0, 1, ConnectionView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionView_ExternalDestinationInterfaceElementView(), this.getSubAppInterfaceElementView(), null, "externalDestinationInterfaceElementView", null, 0, 1, ConnectionView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionView_InternalDestinationInterfaceElementView(), this.getInternalSubAppInterfaceElementView(), null, "internalDestinationInterfaceElementView", null, 0, 1, ConnectionView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionView_ExternalSourceInterfaceElementView(), this.getSubAppInterfaceElementView(), null, "externalSourceInterfaceElementView", null, 0, 1, ConnectionView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionView_InternalSourceInterfaceElementView(), this.getInternalSubAppInterfaceElementView(), null, "internalSourceInterfaceElementView", null, 0, 1, ConnectionView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(subAppViewEClass, SubAppView.class, "SubAppView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubAppView_SubApp(), theLibraryElementPackage.getSubApp(), null, "subApp", null, 1, 1, SubAppView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubAppView_InterfaceElements(), this.getSubAppInterfaceElementView(), null, "interfaceElements", null, 0, -1, SubAppView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubAppView_UiSubAppNetwork(), this.getUISubAppNetwork(), this.getUISubAppNetwork_SubAppView(), "uiSubAppNetwork", null, 0, 1, SubAppView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubAppView_MappedSubApp(), this.getMappedSubAppView(), this.getMappedSubAppView_ApplicationSubApp(), "mappedSubApp", null, 0, 1, SubAppView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(subAppInterfaceElementViewEClass, SubAppInterfaceElementView.class, "SubAppInterfaceElementView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSubAppInterfaceElementView_Visible(), theXMLTypePackage.getBoolean(), "visible", null, 0, 1, SubAppInterfaceElementView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSubAppInterfaceElementView_LabelSubstitute(), ecorePackage.getEString(), "labelSubstitute", null, 0, 1, SubAppInterfaceElementView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uiSubAppNetworkEClass, UISubAppNetwork.class, "UISubAppNetwork", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUISubAppNetwork_SubAppNetwork(), theLibraryElementPackage.getSubAppNetwork(), null, "subAppNetwork", null, 1, 1, UISubAppNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUISubAppNetwork_FileName(), ecorePackage.getEString(), "fileName", null, 0, 1, UISubAppNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUISubAppNetwork_InterfaceElements(), this.getInternalSubAppInterfaceElementView(), null, "interfaceElements", null, 0, -1, UISubAppNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUISubAppNetwork_SubAppView(), this.getSubAppView(), this.getSubAppView_UiSubAppNetwork(), "subAppView", null, 0, 1, UISubAppNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUISubAppNetwork_RootApplication(), this.getUIFBNetwork(), null, "rootApplication", null, 0, 1, UISubAppNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(internalSubAppInterfaceElementViewEClass, InternalSubAppInterfaceElementView.class, "InternalSubAppInterfaceElementView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(colorEClass, Color.class, "Color", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getColor_Red(), theXMLTypePackage.getInt(), "red", null, 0, 1, Color.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getColor_Green(), theXMLTypePackage.getInt(), "green", null, 0, 1, Color.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getColor_Blue(), theXMLTypePackage.getInt(), "blue", null, 0, 1, Color.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uiSystemConfigurationEClass, UISystemConfiguration.class, "UISystemConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUISystemConfiguration_SystemConfigNetwork(), theLibraryElementPackage.getSystemConfigurationNetwork(), null, "systemConfigNetwork", null, 1, 1, UISystemConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deviceViewEClass, DeviceView.class, "DeviceView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDeviceView_DeviceElement(), theLibraryElementPackage.getDevice(), null, "deviceElement", null, 0, 1, DeviceView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeviceView_ShowResorceList(), theXMLTypePackage.getBoolean(), "showResorceList", null, 0, 1, DeviceView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeviceView_InterfaceElements(), this.getInterfaceElementView(), null, "interfaceElements", null, 0, -1, DeviceView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeviceView_ResourceContainerView(), this.getResourceContainerView(), null, "resourceContainerView", null, 0, 1, DeviceView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeviceView_InConnections(), this.getLinkView(), this.getLinkView_Destination(), "inConnections", null, 0, -1, DeviceView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resourceViewEClass, ResourceView.class, "ResourceView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResourceView_ResourceElement(), theLibraryElementPackage.getResource(), null, "resourceElement", null, 0, 1, ResourceView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResourceView_InterfaceElements(), this.getInterfaceElementView(), null, "interfaceElements", null, 0, -1, ResourceView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResourceView_DeviceTypeResource(), theXMLTypePackage.getBoolean(), "deviceTypeResource", null, 0, 1, ResourceView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResourceView_UIResourceDiagram(), this.getUIResourceEditor(), null, "uIResourceDiagram", null, 0, 1, ResourceView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resourceContainerViewEClass, ResourceContainerView.class, "ResourceContainerView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getResourceContainerView_ShowContent(), theXMLTypePackage.getBoolean(), "showContent", null, 0, 1, ResourceContainerView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResourceContainerView_Resources(), this.getResourceView(), null, "resources", null, 0, -1, ResourceContainerView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uiResourceEditorEClass, UIResourceEditor.class, "UIResourceEditor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUIResourceEditor_ResourceElement(), theLibraryElementPackage.getResource(), null, "resourceElement", null, 0, 1, UIResourceEditor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUIResourceEditor_FbNetworks(), this.getResizeUIFBNetwork(), null, "fbNetworks", null, 0, -1, UIResourceEditor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUIResourceEditor_ShowResizeUIFBNetworks(), theXMLTypePackage.getBoolean(), "showResizeUIFBNetworks", null, 0, 1, UIResourceEditor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUIResourceEditor_ActiveNetwork(), this.getResizeUIFBNetwork(), null, "activeNetwork", null, 0, 1, UIResourceEditor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUIResourceEditor_MappingEditorSize(), this.getSize(), null, "mappingEditorSize", null, 0, 1, UIResourceEditor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUIResourceEditor_SystemConfiguration(), this.getUISystemConfiguration(), null, "systemConfiguration", null, 0, 1, UIResourceEditor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resizeUIFBNetworkEClass, ResizeUIFBNetwork.class, "ResizeUIFBNetwork", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResizeUIFBNetwork_Size(), this.getSize(), null, "size", null, 0, 1, ResizeUIFBNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResizeUIFBNetwork_UiFBNetwork(), this.getUIFBNetwork(), null, "uiFBNetwork", null, 0, 1, ResizeUIFBNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sizeEClass, Size.class, "Size", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSize_Width(), ecorePackage.getEInt(), "width", null, 0, 1, Size.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSize_Height(), ecorePackage.getEInt(), "height", null, 0, 1, Size.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mappedSubAppViewEClass, MappedSubAppView.class, "MappedSubAppView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMappedSubAppView_InterfaceElements(), this.getMappedSubAppInterfaceElementView(), null, "interfaceElements", null, 0, -1, MappedSubAppView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMappedSubAppView_ApplicationSubApp(), this.getSubAppView(), this.getSubAppView_MappedSubApp(), "applicationSubApp", null, 0, 1, MappedSubAppView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMappedSubAppView_SubApp(), theLibraryElementPackage.getSubApp(), null, "subApp", null, 0, 1, MappedSubAppView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mappedSubAppInterfaceElementViewEClass, MappedSubAppInterfaceElementView.class, "MappedSubAppInterfaceElementView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(segmentViewEClass, SegmentView.class, "SegmentView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSegmentView_Segment(), theLibraryElementPackage.getSegment(), null, "segment", null, 0, 1, SegmentView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentView_OutConnections(), this.getLinkView(), this.getLinkView_Source(), "outConnections", null, 0, -1, SegmentView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentView_Size(), this.getSize(), null, "size", null, 0, 1, SegmentView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(linkViewEClass, LinkView.class, "LinkView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLinkView_Source(), this.getSegmentView(), this.getSegmentView_OutConnections(), "source", null, 0, 1, LinkView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLinkView_Link(), theLibraryElementPackage.getLink(), null, "link", null, 0, 1, LinkView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLinkView_Destination(), this.getDeviceView(), this.getDeviceView_InConnections(), "destination", null, 0, 1, LinkView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iConnectionViewEClass, IConnectionView.class, "IConnectionView", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(compositeInternalInterfaceElementViewEClass, CompositeInternalInterfaceElementView.class, "CompositeInternalInterfaceElementView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(monitoringViewEClass, MonitoringView.class, "MonitoringView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} // UiPackageImpl
