/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.eclipse.emf.common.util.EList;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UI Sub App Network</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.UISubAppNetwork#getSubAppNetwork <em>Sub App Network</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.UISubAppNetwork#getFileName <em>File Name</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.UISubAppNetwork#getInterfaceElements <em>Interface Elements</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.UISubAppNetwork#getSubAppView <em>Sub App View</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.UISubAppNetwork#getRootApplication <em>Root Application</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getUISubAppNetwork()
 * @model
 * @generated
 */
public interface UISubAppNetwork extends Diagram {
	/**
	 * Returns the value of the '<em><b>Sub App Network</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub App Network</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub App Network</em>' containment reference.
	 * @see #setSubAppNetwork(SubAppNetwork)
	 * @see org.fordiac.ide.model.ui.UiPackage#getUISubAppNetwork_SubAppNetwork()
	 * @model containment="true" resolveProxies="true" required="true"
	 * @generated
	 */
	SubAppNetwork getSubAppNetwork();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.UISubAppNetwork#getSubAppNetwork <em>Sub App Network</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub App Network</em>' containment reference.
	 * @see #getSubAppNetwork()
	 * @generated
	 */
	void setSubAppNetwork(SubAppNetwork value);

	/**
	 * Returns the value of the '<em><b>File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>File Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>File Name</em>' attribute.
	 * @see #setFileName(String)
	 * @see org.fordiac.ide.model.ui.UiPackage#getUISubAppNetwork_FileName()
	 * @model
	 * @generated
	 */
	String getFileName();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.UISubAppNetwork#getFileName <em>File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>File Name</em>' attribute.
	 * @see #getFileName()
	 * @generated
	 */
	void setFileName(String value);

	/**
	 * Returns the value of the '<em><b>Interface Elements</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface Elements</em>' containment reference list.
	 * @see org.fordiac.ide.model.ui.UiPackage#getUISubAppNetwork_InterfaceElements()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<InternalSubAppInterfaceElementView> getInterfaceElements();

	/**
	 * Returns the value of the '<em><b>Sub App View</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.SubAppView#getUiSubAppNetwork <em>Ui Sub App Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub App View</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub App View</em>' container reference.
	 * @see #setSubAppView(SubAppView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getUISubAppNetwork_SubAppView()
	 * @see org.fordiac.ide.model.ui.SubAppView#getUiSubAppNetwork
	 * @model opposite="uiSubAppNetwork" transient="false"
	 * @generated
	 */
	SubAppView getSubAppView();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.UISubAppNetwork#getSubAppView <em>Sub App View</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub App View</em>' container reference.
	 * @see #getSubAppView()
	 * @generated
	 */
	void setSubAppView(SubAppView value);

	/**
	 * Returns the value of the '<em><b>Root Application</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Application</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Application</em>' reference.
	 * @see #setRootApplication(UIFBNetwork)
	 * @see org.fordiac.ide.model.ui.UiPackage#getUISubAppNetwork_RootApplication()
	 * @model
	 * @generated
	 */
	UIFBNetwork getRootApplication();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.UISubAppNetwork#getRootApplication <em>Root Application</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Application</em>' reference.
	 * @see #getRootApplication()
	 * @generated
	 */
	void setRootApplication(UIFBNetwork value);

} // UISubAppNetwork
