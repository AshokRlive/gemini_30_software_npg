/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.eclipse.emf.common.util.EList;
import org.fordiac.ide.model.libraryElement.FB;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FB View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.FBView#getFb <em>Fb</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.FBView#getInterfaceElements <em>Interface Elements</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.FBView#isResourceTypeFB <em>Resource Type FB</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.FBView#getMappedFB <em>Mapped FB</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.FBView#getApplicationFB <em>Application FB</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getFBView()
 * @model
 * @generated
 */
public interface FBView extends View {
	/**
	 * Returns the value of the '<em><b>Fb</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fb</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fb</em>' reference.
	 * @see #setFb(FB)
	 * @see org.fordiac.ide.model.ui.UiPackage#getFBView_Fb()
	 * @model required="true"
	 * @generated
	 */
	FB getFb();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.FBView#getFb <em>Fb</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fb</em>' reference.
	 * @see #getFb()
	 * @generated
	 */
	void setFb(FB value);

	/**
	 * Returns the value of the '<em><b>Interface Elements</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.ui.InterfaceElementView}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface Elements</em>' containment reference list.
	 * @see org.fordiac.ide.model.ui.UiPackage#getFBView_InterfaceElements()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<InterfaceElementView> getInterfaceElements();

	/**
	 * Returns the value of the '<em><b>Resource Type FB</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Type FB</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Type FB</em>' attribute.
	 * @see #setResourceTypeFB(boolean)
	 * @see org.fordiac.ide.model.ui.UiPackage#getFBView_ResourceTypeFB()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isResourceTypeFB();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.FBView#isResourceTypeFB <em>Resource Type FB</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Type FB</em>' attribute.
	 * @see #isResourceTypeFB()
	 * @generated
	 */
	void setResourceTypeFB(boolean value);

	/**
	 * Returns the value of the '<em><b>Mapped FB</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.FBView#getApplicationFB <em>Application FB</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapped FB</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mapped FB</em>' reference.
	 * @see #setMappedFB(FBView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getFBView_MappedFB()
	 * @see org.fordiac.ide.model.ui.FBView#getApplicationFB
	 * @model opposite="applicationFB"
	 * @generated
	 */
	FBView getMappedFB();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.FBView#getMappedFB <em>Mapped FB</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mapped FB</em>' reference.
	 * @see #getMappedFB()
	 * @generated
	 */
	void setMappedFB(FBView value);

	/**
	 * Returns the value of the '<em><b>Application FB</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.FBView#getMappedFB <em>Mapped FB</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Application FB</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Application FB</em>' reference.
	 * @see #setApplicationFB(FBView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getFBView_ApplicationFB()
	 * @see org.fordiac.ide.model.ui.FBView#getMappedFB
	 * @model opposite="mappedFB"
	 * @generated
	 */
	FBView getApplicationFB();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.FBView#getApplicationFB <em>Application FB</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Application FB</em>' reference.
	 * @see #getApplicationFB()
	 * @generated
	 */
	void setApplicationFB(FBView value);

} // FBView
