/**
 * *******************************************************************************
 *  * Copyright (c) 2007 - 2012 4DIAC - consortium.
 *  * All rights reserved. This program and the accompanying materials
 *  * are made available under the terms of the Eclipse Public License v1.0
 *  * which accompanies this distribution, and is available at
 *  * http://www.eclipse.org/legal/epl-v10.html
 *  *
 *  *******************************************************************************
 */
package org.fordiac.ide.model.data.impl;

import org.eclipse.emf.ecore.EClass;
import org.fordiac.ide.model.data.DataPackage;
import org.fordiac.ide.model.data.ValueType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Value Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ValueTypeImpl extends DataTypeImpl implements ValueType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ValueTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.VALUE_TYPE;
	}

} //ValueTypeImpl
