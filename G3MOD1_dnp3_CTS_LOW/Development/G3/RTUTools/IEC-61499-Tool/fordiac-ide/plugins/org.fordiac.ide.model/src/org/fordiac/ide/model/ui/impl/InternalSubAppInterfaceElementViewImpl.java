/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Internal Sub App Interface Element View</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InternalSubAppInterfaceElementViewImpl extends
		SubAppInterfaceElementViewImpl implements
		InternalSubAppInterfaceElementView {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected InternalSubAppInterfaceElementViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW;
	}

	@Override
	public String getLabel() {
		if (labelSubstitute != null && !labelSubstitute.isEmpty()) {
			return labelSubstitute;
		}
		
		InterfaceList interfaceList = (InterfaceList) getIInterfaceElement()
				.eContainer();
		INamedElement rootElement = (INamedElement)interfaceList.eContainer();
		StringBuffer label = new StringBuffer();
		EObject container = rootElement.eContainer();
		while (container instanceof SubAppNetwork) {
			if (((SubAppNetwork) container).getParentSubApp() != null) {
				label.insert(0, ".");
				label.insert(0, ((SubAppNetwork) container).getParentSubApp()
						.getName());
			}
			container = container.eContainer();
		}
		label.append(rootElement.getName());
		label.append(".");
		label.append(getIInterfaceElement().getName());
		return label.toString();
	}

} // InternalSubAppInterfaceElementViewImpl
