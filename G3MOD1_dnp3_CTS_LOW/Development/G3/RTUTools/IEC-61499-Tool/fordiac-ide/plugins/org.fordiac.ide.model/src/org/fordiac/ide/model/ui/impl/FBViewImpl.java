/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FB View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.FBViewImpl#getFb <em>Fb</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.FBViewImpl#getInterfaceElements <em>Interface Elements</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.FBViewImpl#isResourceTypeFB <em>Resource Type FB</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.FBViewImpl#getMappedFB <em>Mapped FB</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.FBViewImpl#getApplicationFB <em>Application FB</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FBViewImpl extends ViewImpl implements FBView {
	/**
	 * The cached value of the '{@link #getFb() <em>Fb</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFb()
	 * @generated
	 * @ordered
	 */
	protected FB fb;

	/**
	 * The cached value of the '{@link #getInterfaceElements() <em>Interface Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaceElements()
	 * @generated
	 * @ordered
	 */
	protected EList<InterfaceElementView> interfaceElements;

	/**
	 * The default value of the '{@link #isResourceTypeFB() <em>Resource Type FB</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResourceTypeFB()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RESOURCE_TYPE_FB_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isResourceTypeFB() <em>Resource Type FB</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResourceTypeFB()
	 * @generated
	 * @ordered
	 */
	protected boolean resourceTypeFB = RESOURCE_TYPE_FB_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMappedFB() <em>Mapped FB</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMappedFB()
	 * @generated
	 * @ordered
	 */
	protected FBView mappedFB;

	/**
	 * The cached value of the '{@link #getApplicationFB() <em>Application FB</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplicationFB()
	 * @generated
	 * @ordered
	 */
	protected FBView applicationFB;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FBViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.FB_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FB getFb() {
		if (fb != null && fb.eIsProxy()) {
			InternalEObject oldFb = (InternalEObject)fb;
			fb = (FB)eResolveProxy(oldFb);
			if (fb != oldFb) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.FB_VIEW__FB, oldFb, fb));
			}
		}
		return fb;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FB basicGetFb() {
		return fb;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFb(FB newFb) {
		FB oldFb = fb;
		fb = newFb;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.FB_VIEW__FB, oldFb, fb));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterfaceElementView> getInterfaceElements() {
		if (interfaceElements == null) {
			interfaceElements = new EObjectContainmentEList.Resolving<InterfaceElementView>(InterfaceElementView.class, this, UiPackage.FB_VIEW__INTERFACE_ELEMENTS);
		}
		return interfaceElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isResourceTypeFB() {
		return resourceTypeFB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourceTypeFB(boolean newResourceTypeFB) {
		boolean oldResourceTypeFB = resourceTypeFB;
		resourceTypeFB = newResourceTypeFB;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.FB_VIEW__RESOURCE_TYPE_FB, oldResourceTypeFB, resourceTypeFB));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FBView getMappedFB() {
		if (mappedFB != null && mappedFB.eIsProxy()) {
			InternalEObject oldMappedFB = (InternalEObject)mappedFB;
			mappedFB = (FBView)eResolveProxy(oldMappedFB);
			if (mappedFB != oldMappedFB) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.FB_VIEW__MAPPED_FB, oldMappedFB, mappedFB));
			}
		}
		return mappedFB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FBView basicGetMappedFB() {
		return mappedFB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMappedFB(FBView newMappedFB, NotificationChain msgs) {
		FBView oldMappedFB = mappedFB;
		mappedFB = newMappedFB;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.FB_VIEW__MAPPED_FB, oldMappedFB, newMappedFB);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMappedFB(FBView newMappedFB) {
		if (newMappedFB != mappedFB) {
			NotificationChain msgs = null;
			if (mappedFB != null)
				msgs = ((InternalEObject)mappedFB).eInverseRemove(this, UiPackage.FB_VIEW__APPLICATION_FB, FBView.class, msgs);
			if (newMappedFB != null)
				msgs = ((InternalEObject)newMappedFB).eInverseAdd(this, UiPackage.FB_VIEW__APPLICATION_FB, FBView.class, msgs);
			msgs = basicSetMappedFB(newMappedFB, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.FB_VIEW__MAPPED_FB, newMappedFB, newMappedFB));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FBView getApplicationFB() {
		if (applicationFB != null && applicationFB.eIsProxy()) {
			InternalEObject oldApplicationFB = (InternalEObject)applicationFB;
			applicationFB = (FBView)eResolveProxy(oldApplicationFB);
			if (applicationFB != oldApplicationFB) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.FB_VIEW__APPLICATION_FB, oldApplicationFB, applicationFB));
			}
		}
		return applicationFB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FBView basicGetApplicationFB() {
		return applicationFB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetApplicationFB(FBView newApplicationFB, NotificationChain msgs) {
		FBView oldApplicationFB = applicationFB;
		applicationFB = newApplicationFB;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.FB_VIEW__APPLICATION_FB, oldApplicationFB, newApplicationFB);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicationFB(FBView newApplicationFB) {
		if (newApplicationFB != applicationFB) {
			NotificationChain msgs = null;
			if (applicationFB != null)
				msgs = ((InternalEObject)applicationFB).eInverseRemove(this, UiPackage.FB_VIEW__MAPPED_FB, FBView.class, msgs);
			if (newApplicationFB != null)
				msgs = ((InternalEObject)newApplicationFB).eInverseAdd(this, UiPackage.FB_VIEW__MAPPED_FB, FBView.class, msgs);
			msgs = basicSetApplicationFB(newApplicationFB, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.FB_VIEW__APPLICATION_FB, newApplicationFB, newApplicationFB));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.FB_VIEW__MAPPED_FB:
				if (mappedFB != null)
					msgs = ((InternalEObject)mappedFB).eInverseRemove(this, UiPackage.FB_VIEW__APPLICATION_FB, FBView.class, msgs);
				return basicSetMappedFB((FBView)otherEnd, msgs);
			case UiPackage.FB_VIEW__APPLICATION_FB:
				if (applicationFB != null)
					msgs = ((InternalEObject)applicationFB).eInverseRemove(this, UiPackage.FB_VIEW__MAPPED_FB, FBView.class, msgs);
				return basicSetApplicationFB((FBView)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.FB_VIEW__INTERFACE_ELEMENTS:
				return ((InternalEList<?>)getInterfaceElements()).basicRemove(otherEnd, msgs);
			case UiPackage.FB_VIEW__MAPPED_FB:
				return basicSetMappedFB(null, msgs);
			case UiPackage.FB_VIEW__APPLICATION_FB:
				return basicSetApplicationFB(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.FB_VIEW__FB:
				if (resolve) return getFb();
				return basicGetFb();
			case UiPackage.FB_VIEW__INTERFACE_ELEMENTS:
				return getInterfaceElements();
			case UiPackage.FB_VIEW__RESOURCE_TYPE_FB:
				return isResourceTypeFB();
			case UiPackage.FB_VIEW__MAPPED_FB:
				if (resolve) return getMappedFB();
				return basicGetMappedFB();
			case UiPackage.FB_VIEW__APPLICATION_FB:
				if (resolve) return getApplicationFB();
				return basicGetApplicationFB();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.FB_VIEW__FB:
				setFb((FB)newValue);
				return;
			case UiPackage.FB_VIEW__INTERFACE_ELEMENTS:
				getInterfaceElements().clear();
				getInterfaceElements().addAll((Collection<? extends InterfaceElementView>)newValue);
				return;
			case UiPackage.FB_VIEW__RESOURCE_TYPE_FB:
				setResourceTypeFB((Boolean)newValue);
				return;
			case UiPackage.FB_VIEW__MAPPED_FB:
				setMappedFB((FBView)newValue);
				return;
			case UiPackage.FB_VIEW__APPLICATION_FB:
				setApplicationFB((FBView)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.FB_VIEW__FB:
				setFb((FB)null);
				return;
			case UiPackage.FB_VIEW__INTERFACE_ELEMENTS:
				getInterfaceElements().clear();
				return;
			case UiPackage.FB_VIEW__RESOURCE_TYPE_FB:
				setResourceTypeFB(RESOURCE_TYPE_FB_EDEFAULT);
				return;
			case UiPackage.FB_VIEW__MAPPED_FB:
				setMappedFB((FBView)null);
				return;
			case UiPackage.FB_VIEW__APPLICATION_FB:
				setApplicationFB((FBView)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.FB_VIEW__FB:
				return fb != null;
			case UiPackage.FB_VIEW__INTERFACE_ELEMENTS:
				return interfaceElements != null && !interfaceElements.isEmpty();
			case UiPackage.FB_VIEW__RESOURCE_TYPE_FB:
				return resourceTypeFB != RESOURCE_TYPE_FB_EDEFAULT;
			case UiPackage.FB_VIEW__MAPPED_FB:
				return mappedFB != null;
			case UiPackage.FB_VIEW__APPLICATION_FB:
				return applicationFB != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (resourceTypeFB: ");
		result.append(resourceTypeFB);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	@Override
	public void setPosition(Position newPosition) {
		super.setPosition(newPosition);
		if(null != fb){
			org.fordiac.ide.model.libraryElement.Position pos = fb.getPosition();
			pos.setX(position.getX());
			pos.setY(position.getY());
			fb.setPosition(pos);
		}
	}

} //FBViewImpl
