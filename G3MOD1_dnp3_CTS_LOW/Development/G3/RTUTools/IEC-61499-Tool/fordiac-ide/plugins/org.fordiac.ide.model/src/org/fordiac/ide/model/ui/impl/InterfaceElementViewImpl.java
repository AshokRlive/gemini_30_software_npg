/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Interface Element View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.InterfaceElementViewImpl#getIInterfaceElement <em>IInterface Element</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.InterfaceElementViewImpl#getInConnections <em>In Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.InterfaceElementViewImpl#getOutConnections <em>Out Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.InterfaceElementViewImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.InterfaceElementViewImpl#getMappedInterfaceElement <em>Mapped Interface Element</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.InterfaceElementViewImpl#getFbNetwork <em>Fb Network</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.InterfaceElementViewImpl#getApplicationInterfaceElement <em>Application Interface Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InterfaceElementViewImpl extends ViewImpl implements
		InterfaceElementView {
	/**
	 * The cached value of the '{@link #getIInterfaceElement() <em>IInterface Element</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getIInterfaceElement()
	 * @generated
	 * @ordered
	 */
	protected IInterfaceElement iInterfaceElement;

	/**
	 * The cached value of the '{@link #getInConnections() <em>In Connections</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getInConnections()
	 * @generated
	 * @ordered
	 */
	protected EList<ConnectionView> inConnections;
	/**
	 * The cached value of the '{@link #getOutConnections() <em>Out Connections</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOutConnections()
	 * @generated
	 * @ordered
	 */
	protected EList<ConnectionView> outConnections;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMappedInterfaceElement() <em>Mapped Interface Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMappedInterfaceElement()
	 * @generated
	 * @ordered
	 */
	protected InterfaceElementView mappedInterfaceElement;

	/**
	 * The cached value of the '{@link #getFbNetwork() <em>Fb Network</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFbNetwork()
	 * @generated
	 * @ordered
	 */
	protected SubAppNetwork fbNetwork;

	/**
	 * The cached value of the '{@link #getApplicationInterfaceElement() <em>Application Interface Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplicationInterfaceElement()
	 * @generated
	 * @ordered
	 */
	protected InterfaceElementView applicationInterfaceElement;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected InterfaceElementViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.INTERFACE_ELEMENT_VIEW;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public IInterfaceElement getIInterfaceElement() {
		if (iInterfaceElement != null && iInterfaceElement.eIsProxy()) {
			InternalEObject oldIInterfaceElement = (InternalEObject)iInterfaceElement;
			iInterfaceElement = (IInterfaceElement)eResolveProxy(oldIInterfaceElement);
			if (iInterfaceElement != oldIInterfaceElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT, oldIInterfaceElement, iInterfaceElement));
			}
		}
		return iInterfaceElement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public IInterfaceElement basicGetIInterfaceElement() {
		return iInterfaceElement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setIInterfaceElement(IInterfaceElement newIInterfaceElement) {
		IInterfaceElement oldIInterfaceElement = iInterfaceElement;
		iInterfaceElement = newIInterfaceElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT, oldIInterfaceElement, iInterfaceElement));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConnectionView> getInConnections() {
		if (inConnections == null) {
			inConnections = new EObjectWithInverseResolvingEList<ConnectionView>(ConnectionView.class, this, UiPackage.INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS, UiPackage.CONNECTION_VIEW__DESTINATION);
		}
		return inConnections;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConnectionView> getOutConnections() {
		if (outConnections == null) {
			outConnections = new EObjectWithInverseResolvingEList<ConnectionView>(ConnectionView.class, this, UiPackage.INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS, UiPackage.CONNECTION_VIEW__SOURCE);
		}
		return outConnections;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public String getLabel() {
		if (getIInterfaceElement() != null) {
			return getIInterfaceElement().getName();
		}
		return label;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setLabel(String newLabel) {
		String oldLabel = getIInterfaceElement().getName();
		getIInterfaceElement().setName(newLabel);
		// label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					UiPackage.INTERFACE_ELEMENT_VIEW__LABEL, oldLabel, newLabel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceElementView getMappedInterfaceElement() {
		if (mappedInterfaceElement != null && mappedInterfaceElement.eIsProxy()) {
			InternalEObject oldMappedInterfaceElement = (InternalEObject)mappedInterfaceElement;
			mappedInterfaceElement = (InterfaceElementView)eResolveProxy(oldMappedInterfaceElement);
			if (mappedInterfaceElement != oldMappedInterfaceElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT, oldMappedInterfaceElement, mappedInterfaceElement));
			}
		}
		return mappedInterfaceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceElementView basicGetMappedInterfaceElement() {
		return mappedInterfaceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMappedInterfaceElement(InterfaceElementView newMappedInterfaceElement, NotificationChain msgs) {
		InterfaceElementView oldMappedInterfaceElement = mappedInterfaceElement;
		mappedInterfaceElement = newMappedInterfaceElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT, oldMappedInterfaceElement, newMappedInterfaceElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMappedInterfaceElement(InterfaceElementView newMappedInterfaceElement) {
		if (newMappedInterfaceElement != mappedInterfaceElement) {
			NotificationChain msgs = null;
			if (mappedInterfaceElement != null)
				msgs = ((InternalEObject)mappedInterfaceElement).eInverseRemove(this, UiPackage.INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT, InterfaceElementView.class, msgs);
			if (newMappedInterfaceElement != null)
				msgs = ((InternalEObject)newMappedInterfaceElement).eInverseAdd(this, UiPackage.INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT, InterfaceElementView.class, msgs);
			msgs = basicSetMappedInterfaceElement(newMappedInterfaceElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT, newMappedInterfaceElement, newMappedInterfaceElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppNetwork getFbNetwork() {
		if (fbNetwork != null && fbNetwork.eIsProxy()) {
			InternalEObject oldFbNetwork = (InternalEObject)fbNetwork;
			fbNetwork = (SubAppNetwork)eResolveProxy(oldFbNetwork);
			if (fbNetwork != oldFbNetwork) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.INTERFACE_ELEMENT_VIEW__FB_NETWORK, oldFbNetwork, fbNetwork));
			}
		}
		return fbNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppNetwork basicGetFbNetwork() {
		return fbNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFbNetwork(SubAppNetwork newFbNetwork) {
		SubAppNetwork oldFbNetwork = fbNetwork;
		fbNetwork = newFbNetwork;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.INTERFACE_ELEMENT_VIEW__FB_NETWORK, oldFbNetwork, fbNetwork));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceElementView getApplicationInterfaceElement() {
		if (applicationInterfaceElement != null && applicationInterfaceElement.eIsProxy()) {
			InternalEObject oldApplicationInterfaceElement = (InternalEObject)applicationInterfaceElement;
			applicationInterfaceElement = (InterfaceElementView)eResolveProxy(oldApplicationInterfaceElement);
			if (applicationInterfaceElement != oldApplicationInterfaceElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT, oldApplicationInterfaceElement, applicationInterfaceElement));
			}
		}
		return applicationInterfaceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceElementView basicGetApplicationInterfaceElement() {
		return applicationInterfaceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetApplicationInterfaceElement(InterfaceElementView newApplicationInterfaceElement, NotificationChain msgs) {
		InterfaceElementView oldApplicationInterfaceElement = applicationInterfaceElement;
		applicationInterfaceElement = newApplicationInterfaceElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT, oldApplicationInterfaceElement, newApplicationInterfaceElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicationInterfaceElement(InterfaceElementView newApplicationInterfaceElement) {
		if (newApplicationInterfaceElement != applicationInterfaceElement) {
			NotificationChain msgs = null;
			if (applicationInterfaceElement != null)
				msgs = ((InternalEObject)applicationInterfaceElement).eInverseRemove(this, UiPackage.INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT, InterfaceElementView.class, msgs);
			if (newApplicationInterfaceElement != null)
				msgs = ((InternalEObject)newApplicationInterfaceElement).eInverseAdd(this, UiPackage.INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT, InterfaceElementView.class, msgs);
			msgs = basicSetApplicationInterfaceElement(newApplicationInterfaceElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT, newApplicationInterfaceElement, newApplicationInterfaceElement));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInConnections()).basicAdd(otherEnd, msgs);
			case UiPackage.INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutConnections()).basicAdd(otherEnd, msgs);
			case UiPackage.INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT:
				if (mappedInterfaceElement != null)
					msgs = ((InternalEObject)mappedInterfaceElement).eInverseRemove(this, UiPackage.INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT, InterfaceElementView.class, msgs);
				return basicSetMappedInterfaceElement((InterfaceElementView)otherEnd, msgs);
			case UiPackage.INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT:
				if (applicationInterfaceElement != null)
					msgs = ((InternalEObject)applicationInterfaceElement).eInverseRemove(this, UiPackage.INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT, InterfaceElementView.class, msgs);
				return basicSetApplicationInterfaceElement((InterfaceElementView)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS:
				return ((InternalEList<?>)getInConnections()).basicRemove(otherEnd, msgs);
			case UiPackage.INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS:
				return ((InternalEList<?>)getOutConnections()).basicRemove(otherEnd, msgs);
			case UiPackage.INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT:
				return basicSetMappedInterfaceElement(null, msgs);
			case UiPackage.INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT:
				return basicSetApplicationInterfaceElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT:
				if (resolve) return getIInterfaceElement();
				return basicGetIInterfaceElement();
			case UiPackage.INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS:
				return getInConnections();
			case UiPackage.INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS:
				return getOutConnections();
			case UiPackage.INTERFACE_ELEMENT_VIEW__LABEL:
				return getLabel();
			case UiPackage.INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT:
				if (resolve) return getMappedInterfaceElement();
				return basicGetMappedInterfaceElement();
			case UiPackage.INTERFACE_ELEMENT_VIEW__FB_NETWORK:
				if (resolve) return getFbNetwork();
				return basicGetFbNetwork();
			case UiPackage.INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT:
				if (resolve) return getApplicationInterfaceElement();
				return basicGetApplicationInterfaceElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT:
				setIInterfaceElement((IInterfaceElement)newValue);
				return;
			case UiPackage.INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS:
				getInConnections().clear();
				getInConnections().addAll((Collection<? extends ConnectionView>)newValue);
				return;
			case UiPackage.INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS:
				getOutConnections().clear();
				getOutConnections().addAll((Collection<? extends ConnectionView>)newValue);
				return;
			case UiPackage.INTERFACE_ELEMENT_VIEW__LABEL:
				setLabel((String)newValue);
				return;
			case UiPackage.INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT:
				setMappedInterfaceElement((InterfaceElementView)newValue);
				return;
			case UiPackage.INTERFACE_ELEMENT_VIEW__FB_NETWORK:
				setFbNetwork((SubAppNetwork)newValue);
				return;
			case UiPackage.INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT:
				setApplicationInterfaceElement((InterfaceElementView)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT:
				setIInterfaceElement((IInterfaceElement)null);
				return;
			case UiPackage.INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS:
				getInConnections().clear();
				return;
			case UiPackage.INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS:
				getOutConnections().clear();
				return;
			case UiPackage.INTERFACE_ELEMENT_VIEW__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case UiPackage.INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT:
				setMappedInterfaceElement((InterfaceElementView)null);
				return;
			case UiPackage.INTERFACE_ELEMENT_VIEW__FB_NETWORK:
				setFbNetwork((SubAppNetwork)null);
				return;
			case UiPackage.INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT:
				setApplicationInterfaceElement((InterfaceElementView)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT:
				return iInterfaceElement != null;
			case UiPackage.INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS:
				return inConnections != null && !inConnections.isEmpty();
			case UiPackage.INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS:
				return outConnections != null && !outConnections.isEmpty();
			case UiPackage.INTERFACE_ELEMENT_VIEW__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case UiPackage.INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT:
				return mappedInterfaceElement != null;
			case UiPackage.INTERFACE_ELEMENT_VIEW__FB_NETWORK:
				return fbNetwork != null;
			case UiPackage.INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT:
				return applicationInterfaceElement != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (label: ");
		result.append(label);
		result.append(')');
		return result.toString();
	}

} // InterfaceElementViewImpl
