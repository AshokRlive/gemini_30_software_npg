/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Segment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Segment#getDx1 <em>Dx1</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Segment#getX <em>X</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Segment#getY <em>Y</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Segment#getVarDeclarations <em>Var Declarations</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Segment#getOutConnections <em>Out Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Segment#getTypePath <em>Type Path</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSegment()
 * @model
 * @generated
 */
public interface Segment extends TypedConfigureableObject {
	/**
	 * Returns the value of the '<em><b>Dx1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dx1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dx1</em>' attribute.
	 * @see #setDx1(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSegment_Dx1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='dx1'"
	 * @generated
	 */
	String getDx1();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Segment#getDx1 <em>Dx1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dx1</em>' attribute.
	 * @see #getDx1()
	 * @generated
	 */
	void setDx1(String value);

	/**
	 * Returns the value of the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>X</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X</em>' attribute.
	 * @see #setX(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSegment_X()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='x'"
	 * @generated
	 */
	String getX();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Segment#getX <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X</em>' attribute.
	 * @see #getX()
	 * @generated
	 */
	void setX(String value);

	/**
	 * Returns the value of the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Y</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y</em>' attribute.
	 * @see #setY(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSegment_Y()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='y'"
	 * @generated
	 */
	String getY();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Segment#getY <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y</em>' attribute.
	 * @see #getY()
	 * @generated
	 */
	void setY(String value);

	/**
	 * Returns the value of the '<em><b>Var Declarations</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.VarDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Declarations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Declarations</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSegment_VarDeclarations()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<VarDeclaration> getVarDeclarations();

	/**
	 * Returns the value of the '<em><b>Out Connections</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.Link}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.Link#getSegment <em>Segment</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Connections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Connections</em>' reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSegment_OutConnections()
	 * @see org.fordiac.ide.model.libraryElement.Link#getSegment
	 * @model opposite="segment"
	 * @generated
	 */
	EList<Link> getOutConnections();

	/**
	 * Returns the value of the '<em><b>Type Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Path</em>' attribute.
	 * @see #setTypePath(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSegment_TypePath()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 * @generated
	 */
	String getTypePath();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Segment#getTypePath <em>Type Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Path</em>' attribute.
	 * @see #getTypePath()
	 * @generated
	 */
	void setTypePath(String value);
	
	SegmentType getSegmentType();

} // Segment
