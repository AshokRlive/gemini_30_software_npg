/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Event#getWith <em>With</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Event#getInputConnections <em>Input Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Event#getOutputConnections <em>Output Connections</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getEvent()
 * @model
 * @generated
 */
public interface Event extends IInterfaceElement {
	/**
	 * Returns the value of the '<em><b>With</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.With}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>With</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>With</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getEvent_With()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='With' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<With> getWith();

	/**
	 * Returns the value of the '<em><b>Input Connections</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.EventConnection}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.EventConnection#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Connections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Connections</em>' reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getEvent_InputConnections()
	 * @see org.fordiac.ide.model.libraryElement.EventConnection#getDestination
	 * @model opposite="destination"
	 * @generated
	 */
	EList<EventConnection> getInputConnections();

	/**
	 * Returns the value of the '<em><b>Output Connections</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.EventConnection}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.EventConnection#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Connections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Connections</em>' reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getEvent_OutputConnections()
	 * @see org.fordiac.ide.model.libraryElement.EventConnection#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<EventConnection> getOutputConnections();
	
	/**
	 * returns true if the event is an event input 
	 * @return
	 */


} // Event
