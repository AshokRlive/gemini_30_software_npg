/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.fordiac.ide.model.Palette.PaletteEntry;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub App</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubApp#getType <em>Type</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubApp#getX <em>X</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubApp#getY <em>Y</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubApp#getInterface <em>Interface</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubApp#getSubAppNetwork <em>Sub App Network</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubApp#getResource <em>Resource</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubApp#getPaletteEntry <em>Palette Entry</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubApp()
 * @model
 * @generated
 */
public interface SubApp extends IDObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubApp_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='Type'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.SubApp#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>X</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X</em>' attribute.
	 * @see #setX(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubApp_X()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='x'"
	 * @generated
	 */
	String getX();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.SubApp#getX <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X</em>' attribute.
	 * @see #getX()
	 * @generated
	 */
	void setX(String value);

	/**
	 * Returns the value of the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Y</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y</em>' attribute.
	 * @see #setY(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubApp_Y()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='y'"
	 * @generated
	 */
	String getY();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.SubApp#getY <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y</em>' attribute.
	 * @see #getY()
	 * @generated
	 */
	void setY(String value);

	/**
	 * Returns the value of the '<em><b>Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface</em>' containment reference.
	 * @see #setInterface(SubAppInterfaceList)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubApp_Interface()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	SubAppInterfaceList getInterface();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.SubApp#getInterface <em>Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface</em>' containment reference.
	 * @see #getInterface()
	 * @generated
	 */
	void setInterface(SubAppInterfaceList value);

	/**
	 * Returns the value of the '<em><b>Sub App Network</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.SubAppNetwork#getParentSubApp <em>Parent Sub App</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub App Network</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub App Network</em>' reference.
	 * @see #setSubAppNetwork(SubAppNetwork)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubApp_SubAppNetwork()
	 * @see org.fordiac.ide.model.libraryElement.SubAppNetwork#getParentSubApp
	 * @model opposite="parentSubApp" required="true"
	 * @generated
	 */
	SubAppNetwork getSubAppNetwork();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.SubApp#getSubAppNetwork <em>Sub App Network</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub App Network</em>' reference.
	 * @see #getSubAppNetwork()
	 * @generated
	 */
	void setSubAppNetwork(SubAppNetwork value);

	/**
	 * Returns the value of the '<em><b>Resource</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.ResourceFBNetwork#getMappedSubApps <em>Mapped Sub Apps</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' reference.
	 * @see #setResource(ResourceFBNetwork)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubApp_Resource()
	 * @see org.fordiac.ide.model.libraryElement.ResourceFBNetwork#getMappedSubApps
	 * @model opposite="mappedSubApps"
	 * @generated
	 */
	ResourceFBNetwork getResource();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.SubApp#getResource <em>Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource</em>' reference.
	 * @see #getResource()
	 * @generated
	 */
	void setResource(ResourceFBNetwork value);

	/**
	 * Returns the value of the '<em><b>Palette Entry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Palette Entry</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Palette Entry</em>' reference.
	 * @see #setPaletteEntry(PaletteEntry)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubApp_PaletteEntry()
	 * @model required="true" transient="true"
	 * @generated
	 */
	PaletteEntry getPaletteEntry();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.SubApp#getPaletteEntry <em>Palette Entry</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Palette Entry</em>' reference.
	 * @see #getPaletteEntry()
	 * @generated
	 */
	void setPaletteEntry(PaletteEntry value);

} // SubApp
