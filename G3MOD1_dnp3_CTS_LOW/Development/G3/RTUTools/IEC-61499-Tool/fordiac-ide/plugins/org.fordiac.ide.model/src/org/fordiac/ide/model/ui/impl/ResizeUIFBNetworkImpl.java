/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.fordiac.ide.model.ui.ResizeUIFBNetwork;
import org.fordiac.ide.model.ui.Size;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resize UIFB Network</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ResizeUIFBNetworkImpl#getSize <em>Size</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ResizeUIFBNetworkImpl#getUiFBNetwork <em>Ui FB Network</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResizeUIFBNetworkImpl extends EObjectImpl implements ResizeUIFBNetwork {
	/**
	 * The cached value of the '{@link #getSize() <em>Size</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected Size size;

	/**
	 * The cached value of the '{@link #getUiFBNetwork() <em>Ui FB Network</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUiFBNetwork()
	 * @generated
	 * @ordered
	 */
	protected UIFBNetwork uiFBNetwork;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResizeUIFBNetworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.RESIZE_UIFB_NETWORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Size getSize() {
		if (size != null && size.eIsProxy()) {
			InternalEObject oldSize = (InternalEObject)size;
			size = (Size)eResolveProxy(oldSize);
			if (size != oldSize) {
				InternalEObject newSize = (InternalEObject)size;
				NotificationChain msgs = oldSize.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.RESIZE_UIFB_NETWORK__SIZE, null, null);
				if (newSize.eInternalContainer() == null) {
					msgs = newSize.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.RESIZE_UIFB_NETWORK__SIZE, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.RESIZE_UIFB_NETWORK__SIZE, oldSize, size));
			}
		}
		return size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Size basicGetSize() {
		return size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSize(Size newSize, NotificationChain msgs) {
		Size oldSize = size;
		size = newSize;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.RESIZE_UIFB_NETWORK__SIZE, oldSize, newSize);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSize(Size newSize) {
		if (newSize != size) {
			NotificationChain msgs = null;
			if (size != null)
				msgs = ((InternalEObject)size).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.RESIZE_UIFB_NETWORK__SIZE, null, msgs);
			if (newSize != null)
				msgs = ((InternalEObject)newSize).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.RESIZE_UIFB_NETWORK__SIZE, null, msgs);
			msgs = basicSetSize(newSize, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.RESIZE_UIFB_NETWORK__SIZE, newSize, newSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UIFBNetwork getUiFBNetwork() {
		if (uiFBNetwork != null && uiFBNetwork.eIsProxy()) {
			InternalEObject oldUiFBNetwork = (InternalEObject)uiFBNetwork;
			uiFBNetwork = (UIFBNetwork)eResolveProxy(oldUiFBNetwork);
			if (uiFBNetwork != oldUiFBNetwork) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.RESIZE_UIFB_NETWORK__UI_FB_NETWORK, oldUiFBNetwork, uiFBNetwork));
			}
		}
		return uiFBNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UIFBNetwork basicGetUiFBNetwork() {
		return uiFBNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUiFBNetwork(UIFBNetwork newUiFBNetwork) {
		UIFBNetwork oldUiFBNetwork = uiFBNetwork;
		uiFBNetwork = newUiFBNetwork;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.RESIZE_UIFB_NETWORK__UI_FB_NETWORK, oldUiFBNetwork, uiFBNetwork));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.RESIZE_UIFB_NETWORK__SIZE:
				return basicSetSize(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.RESIZE_UIFB_NETWORK__SIZE:
				if (resolve) return getSize();
				return basicGetSize();
			case UiPackage.RESIZE_UIFB_NETWORK__UI_FB_NETWORK:
				if (resolve) return getUiFBNetwork();
				return basicGetUiFBNetwork();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.RESIZE_UIFB_NETWORK__SIZE:
				setSize((Size)newValue);
				return;
			case UiPackage.RESIZE_UIFB_NETWORK__UI_FB_NETWORK:
				setUiFBNetwork((UIFBNetwork)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.RESIZE_UIFB_NETWORK__SIZE:
				setSize((Size)null);
				return;
			case UiPackage.RESIZE_UIFB_NETWORK__UI_FB_NETWORK:
				setUiFBNetwork((UIFBNetwork)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.RESIZE_UIFB_NETWORK__SIZE:
				return size != null;
			case UiPackage.RESIZE_UIFB_NETWORK__UI_FB_NETWORK:
				return uiFBNetwork != null;
		}
		return super.eIsSet(featureID);
	}

} //ResizeUIFBNetworkImpl
