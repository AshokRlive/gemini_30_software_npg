/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub App Interface List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubAppInterfaceList#getEventInputs <em>Event Inputs</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubAppInterfaceList#getEventOutputs <em>Event Outputs</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubAppInterfaceList#getInputVars <em>Input Vars</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubAppInterfaceList#getOutputVars <em>Output Vars</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubAppInterfaceList()
 * @model
 * @generated
 */
public interface SubAppInterfaceList extends EObject {
	/**
	 * Returns the value of the '<em><b>Event Inputs</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Inputs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Inputs</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubAppInterfaceList_EventInputs()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SubAppEventInputs' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Event> getEventInputs();

	/**
	 * Returns the value of the '<em><b>Event Outputs</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Outputs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Outputs</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubAppInterfaceList_EventOutputs()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SubAppEventOutputs' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Event> getEventOutputs();

	/**
	 * Returns the value of the '<em><b>Input Vars</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.VarDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Vars</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Vars</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubAppInterfaceList_InputVars()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='InputVars' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<VarDeclaration> getInputVars();

	/**
	 * Returns the value of the '<em><b>Output Vars</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.VarDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Vars</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Vars</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubAppInterfaceList_OutputVars()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OutputVars' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<VarDeclaration> getOutputVars();
	
	/**
	 * Returns the variable with the name
	 * @param varName
	 * @return the variable with the name or null
	 */
	VarDeclaration getVariable(String name);
	
	/**
	 * Returns the event with the name
	 * @param eventName
	 * @return the event with the name or null
	 */
	Event getEvent(String name);

} // SubAppInterfaceList
