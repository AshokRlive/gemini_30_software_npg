/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.fordiac.ide.model.libraryElement.Resource;

import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ResourceViewImpl#getResourceElement <em>Resource Element</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ResourceViewImpl#getInterfaceElements <em>Interface Elements</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ResourceViewImpl#isDeviceTypeResource <em>Device Type Resource</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ResourceViewImpl#getUIResourceDiagram <em>UI Resource Diagram</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResourceViewImpl extends ViewImpl implements ResourceView {
	/**
	 * The cached value of the '{@link #getResourceElement() <em>Resource Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceElement()
	 * @generated
	 * @ordered
	 */
	protected Resource resourceElement;

	/**
	 * The cached value of the '{@link #getInterfaceElements() <em>Interface Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaceElements()
	 * @generated
	 * @ordered
	 */
	protected EList<InterfaceElementView> interfaceElements;

	/**
	 * The default value of the '{@link #isDeviceTypeResource() <em>Device Type Resource</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeviceTypeResource()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DEVICE_TYPE_RESOURCE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDeviceTypeResource() <em>Device Type Resource</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeviceTypeResource()
	 * @generated
	 * @ordered
	 */
	protected boolean deviceTypeResource = DEVICE_TYPE_RESOURCE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getUIResourceDiagram() <em>UI Resource Diagram</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUIResourceDiagram()
	 * @generated
	 * @ordered
	 */
	protected UIResourceEditor uIResourceDiagram;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.RESOURCE_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource getResourceElement() {
		if (resourceElement != null && resourceElement.eIsProxy()) {
			InternalEObject oldResourceElement = (InternalEObject)resourceElement;
			resourceElement = (Resource)eResolveProxy(oldResourceElement);
			if (resourceElement != oldResourceElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.RESOURCE_VIEW__RESOURCE_ELEMENT, oldResourceElement, resourceElement));
			}
		}
		return resourceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource basicGetResourceElement() {
		return resourceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourceElement(Resource newResourceElement) {
		Resource oldResourceElement = resourceElement;
		resourceElement = newResourceElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.RESOURCE_VIEW__RESOURCE_ELEMENT, oldResourceElement, resourceElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterfaceElementView> getInterfaceElements() {
		if (interfaceElements == null) {
			interfaceElements = new EObjectContainmentEList.Resolving<InterfaceElementView>(InterfaceElementView.class, this, UiPackage.RESOURCE_VIEW__INTERFACE_ELEMENTS);
		}
		return interfaceElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDeviceTypeResource() {
		return deviceTypeResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeviceTypeResource(boolean newDeviceTypeResource) {
		boolean oldDeviceTypeResource = deviceTypeResource;
		deviceTypeResource = newDeviceTypeResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.RESOURCE_VIEW__DEVICE_TYPE_RESOURCE, oldDeviceTypeResource, deviceTypeResource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UIResourceEditor getUIResourceDiagram() {
		if (uIResourceDiagram != null && uIResourceDiagram.eIsProxy()) {
			InternalEObject oldUIResourceDiagram = (InternalEObject)uIResourceDiagram;
			uIResourceDiagram = (UIResourceEditor)eResolveProxy(oldUIResourceDiagram);
			if (uIResourceDiagram != oldUIResourceDiagram) {
				InternalEObject newUIResourceDiagram = (InternalEObject)uIResourceDiagram;
				NotificationChain msgs = oldUIResourceDiagram.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM, null, null);
				if (newUIResourceDiagram.eInternalContainer() == null) {
					msgs = newUIResourceDiagram.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM, oldUIResourceDiagram, uIResourceDiagram));
			}
		}
		return uIResourceDiagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UIResourceEditor basicGetUIResourceDiagram() {
		return uIResourceDiagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUIResourceDiagram(UIResourceEditor newUIResourceDiagram, NotificationChain msgs) {
		UIResourceEditor oldUIResourceDiagram = uIResourceDiagram;
		uIResourceDiagram = newUIResourceDiagram;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM, oldUIResourceDiagram, newUIResourceDiagram);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUIResourceDiagram(UIResourceEditor newUIResourceDiagram) {
		if (newUIResourceDiagram != uIResourceDiagram) {
			NotificationChain msgs = null;
			if (uIResourceDiagram != null)
				msgs = ((InternalEObject)uIResourceDiagram).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM, null, msgs);
			if (newUIResourceDiagram != null)
				msgs = ((InternalEObject)newUIResourceDiagram).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM, null, msgs);
			msgs = basicSetUIResourceDiagram(newUIResourceDiagram, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM, newUIResourceDiagram, newUIResourceDiagram));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.RESOURCE_VIEW__INTERFACE_ELEMENTS:
				return ((InternalEList<?>)getInterfaceElements()).basicRemove(otherEnd, msgs);
			case UiPackage.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM:
				return basicSetUIResourceDiagram(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.RESOURCE_VIEW__RESOURCE_ELEMENT:
				if (resolve) return getResourceElement();
				return basicGetResourceElement();
			case UiPackage.RESOURCE_VIEW__INTERFACE_ELEMENTS:
				return getInterfaceElements();
			case UiPackage.RESOURCE_VIEW__DEVICE_TYPE_RESOURCE:
				return isDeviceTypeResource();
			case UiPackage.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM:
				if (resolve) return getUIResourceDiagram();
				return basicGetUIResourceDiagram();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.RESOURCE_VIEW__RESOURCE_ELEMENT:
				setResourceElement((Resource)newValue);
				return;
			case UiPackage.RESOURCE_VIEW__INTERFACE_ELEMENTS:
				getInterfaceElements().clear();
				getInterfaceElements().addAll((Collection<? extends InterfaceElementView>)newValue);
				return;
			case UiPackage.RESOURCE_VIEW__DEVICE_TYPE_RESOURCE:
				setDeviceTypeResource((Boolean)newValue);
				return;
			case UiPackage.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM:
				setUIResourceDiagram((UIResourceEditor)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.RESOURCE_VIEW__RESOURCE_ELEMENT:
				setResourceElement((Resource)null);
				return;
			case UiPackage.RESOURCE_VIEW__INTERFACE_ELEMENTS:
				getInterfaceElements().clear();
				return;
			case UiPackage.RESOURCE_VIEW__DEVICE_TYPE_RESOURCE:
				setDeviceTypeResource(DEVICE_TYPE_RESOURCE_EDEFAULT);
				return;
			case UiPackage.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM:
				setUIResourceDiagram((UIResourceEditor)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.RESOURCE_VIEW__RESOURCE_ELEMENT:
				return resourceElement != null;
			case UiPackage.RESOURCE_VIEW__INTERFACE_ELEMENTS:
				return interfaceElements != null && !interfaceElements.isEmpty();
			case UiPackage.RESOURCE_VIEW__DEVICE_TYPE_RESOURCE:
				return deviceTypeResource != DEVICE_TYPE_RESOURCE_EDEFAULT;
			case UiPackage.RESOURCE_VIEW__UI_RESOURCE_DIAGRAM:
				return uIResourceDiagram != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (deviceTypeResource: ");
		result.append(deviceTypeResource);
		result.append(')');
		return result.toString();
	}

} //ResourceViewImpl
