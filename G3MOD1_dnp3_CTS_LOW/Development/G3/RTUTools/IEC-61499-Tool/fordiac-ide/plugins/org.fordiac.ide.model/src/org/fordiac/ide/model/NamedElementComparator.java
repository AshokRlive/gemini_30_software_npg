/*******************************************************************************
 * Copyright (c) 2016 fortiss GmbH
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Alois Zoitl - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.fordiac.ide.model;

import java.text.Collator;
import java.util.Comparator;

import org.fordiac.ide.model.libraryElement.INamedElement;

public final class NamedElementComparator implements Comparator<INamedElement> {
	Collator col = Collator.getInstance();
	
	public static final NamedElementComparator INSTANCE = new NamedElementComparator();

	private NamedElementComparator() {
	}

	@Override
	public int compare(INamedElement o1, INamedElement o2) {
		return col.compare(o1.getName(), o2.getName());
	}
}