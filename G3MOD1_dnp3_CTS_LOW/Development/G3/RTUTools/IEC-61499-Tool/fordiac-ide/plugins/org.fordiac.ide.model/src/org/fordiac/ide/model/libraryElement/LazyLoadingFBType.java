/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lazy Loading FB Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.LazyLoadingFBType#getFbTypeName <em>Fb Type Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getLazyLoadingFBType()
 * @model
 * @generated
 */
public interface LazyLoadingFBType extends FBType {
	/**
	 * Returns the value of the '<em><b>Fb Type Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fb Type Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fb Type Name</em>' attribute.
	 * @see #setFbTypeName(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getLazyLoadingFBType_FbTypeName()
	 * @model
	 * @generated
	 */
	String getFbTypeName();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.LazyLoadingFBType#getFbTypeName <em>Fb Type Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fb Type Name</em>' attribute.
	 * @see #getFbTypeName()
	 * @generated
	 */
	void setFbTypeName(String value);

} // LazyLoadingFBType
