package org.fordiac.ide.typelibrary;

import org.eclipse.core.resources.IFile;
import org.fordiac.ide.gef.IPaletteEntryCreator;
import org.fordiac.ide.model.Palette.FBTypePaletteEntry;
import org.fordiac.ide.model.Palette.PaletteFactory;
import org.fordiac.ide.model.Palette.impl.PaletteEntryImpl;

public class CreateFBTypePaletteEntry implements IPaletteEntryCreator, TypeLibraryTags {
	
	@Override
	public boolean canHandle(IFile file) {
		 if (FB_TYPE_FILE_ENDING.equalsIgnoreCase(file.getFileExtension())){
			 return true;
		 } else
			 return false;
	}

	@Override
	public PaletteEntryImpl createPaletteEntry() {
		FBTypePaletteEntry entry = PaletteFactory.eINSTANCE.createFBTypePaletteEntry();
		
		return (PaletteEntryImpl) entry;
	}

}
