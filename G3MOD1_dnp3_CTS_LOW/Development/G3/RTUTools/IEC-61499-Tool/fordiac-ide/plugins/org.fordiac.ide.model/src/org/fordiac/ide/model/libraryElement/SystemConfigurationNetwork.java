/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System Configuration Network</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork#getDevices <em>Devices</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork#getSegments <em>Segments</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork#getLinks <em>Links</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork#getSystemConfiguration <em>System Configuration</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSystemConfigurationNetwork()
 * @model
 * @generated
 */
public interface SystemConfigurationNetwork extends I4DIACElement {
	/**
	 * Returns the value of the '<em><b>Devices</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.Device}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.Device#getSystemConfigurationNetwork <em>System Configuration Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Devices</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Devices</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSystemConfigurationNetwork_Devices()
	 * @see org.fordiac.ide.model.libraryElement.Device#getSystemConfigurationNetwork
	 * @model opposite="systemConfigurationNetwork" containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Device' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Device> getDevices();

	/**
	 * Returns the value of the '<em><b>Segments</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.Segment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segments</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSystemConfigurationNetwork_Segments()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Segment' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Segment> getSegments();

	/**
	 * Returns the value of the '<em><b>Links</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.Link}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Links</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Links</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSystemConfigurationNetwork_Links()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Link' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Link> getLinks();

	/**
	 * Returns the value of the '<em><b>System Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Configuration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Configuration</em>' reference.
	 * @see #setSystemConfiguration(SystemConfiguration)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSystemConfigurationNetwork_SystemConfiguration()
	 * @model resolveProxies="false" transient="true"
	 * @generated
	 */
	SystemConfiguration getSystemConfiguration();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork#getSystemConfiguration <em>System Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Configuration</em>' reference.
	 * @see #getSystemConfiguration()
	 * @generated
	 */
	void setSystemConfiguration(SystemConfiguration value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getSystemConfiguration().getAutomationSystem();'"
	 * @generated
	 */
	AutomationSystem getAutomationSystem();

} // SystemConfigurationNetwork
