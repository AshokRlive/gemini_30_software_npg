/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import org.eclipse.emf.ecore.EClass;

import org.fordiac.ide.model.ui.MonitoringView;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Monitoring View</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MonitoringViewImpl extends ViewImpl implements MonitoringView {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonitoringViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.MONITORING_VIEW;
	}

} //MonitoringViewImpl
