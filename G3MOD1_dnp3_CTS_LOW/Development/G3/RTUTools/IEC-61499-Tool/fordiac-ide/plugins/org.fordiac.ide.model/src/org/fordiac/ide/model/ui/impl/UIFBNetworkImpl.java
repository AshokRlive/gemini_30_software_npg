/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UIFB Network</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.UIFBNetworkImpl#getFbNetwork <em>Fb Network</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UIFBNetworkImpl extends DiagramImpl implements UIFBNetwork {
	/**
	 * The cached value of the '{@link #getFbNetwork() <em>Fb Network</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFbNetwork()
	 * @generated
	 * @ordered
	 */
	protected FBNetwork fbNetwork;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UIFBNetworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.UIFB_NETWORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FBNetwork getFbNetwork() {
		if (fbNetwork != null && fbNetwork.eIsProxy()) {
			InternalEObject oldFbNetwork = (InternalEObject)fbNetwork;
			fbNetwork = (FBNetwork)eResolveProxy(oldFbNetwork);
			if (fbNetwork != oldFbNetwork) {
				InternalEObject newFbNetwork = (InternalEObject)fbNetwork;
				NotificationChain msgs = oldFbNetwork.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.UIFB_NETWORK__FB_NETWORK, null, null);
				if (newFbNetwork.eInternalContainer() == null) {
					msgs = newFbNetwork.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.UIFB_NETWORK__FB_NETWORK, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.UIFB_NETWORK__FB_NETWORK, oldFbNetwork, fbNetwork));
			}
		}
		return fbNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FBNetwork basicGetFbNetwork() {
		return fbNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFbNetwork(FBNetwork newFbNetwork, NotificationChain msgs) {
		FBNetwork oldFbNetwork = fbNetwork;
		fbNetwork = newFbNetwork;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.UIFB_NETWORK__FB_NETWORK, oldFbNetwork, newFbNetwork);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFbNetwork(FBNetwork newFbNetwork) {
		if (newFbNetwork != fbNetwork) {
			NotificationChain msgs = null;
			if (fbNetwork != null)
				msgs = ((InternalEObject)fbNetwork).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.UIFB_NETWORK__FB_NETWORK, null, msgs);
			if (newFbNetwork != null)
				msgs = ((InternalEObject)newFbNetwork).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.UIFB_NETWORK__FB_NETWORK, null, msgs);
			msgs = basicSetFbNetwork(newFbNetwork, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.UIFB_NETWORK__FB_NETWORK, newFbNetwork, newFbNetwork));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.UIFB_NETWORK__FB_NETWORK:
				return basicSetFbNetwork(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.UIFB_NETWORK__FB_NETWORK:
				if (resolve) return getFbNetwork();
				return basicGetFbNetwork();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.UIFB_NETWORK__FB_NETWORK:
				setFbNetwork((FBNetwork)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.UIFB_NETWORK__FB_NETWORK:
				setFbNetwork((FBNetwork)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.UIFB_NETWORK__FB_NETWORK:
				return fbNetwork != null;
		}
		return super.eIsSet(featureID);
	}

	public SubAppNetwork getNetwork() {
		return getFbNetwork();
	}

	public SubAppNetwork getFunctionBlockNetwork() {
		return getFbNetwork();
	}

} //UIFBNetworkImpl
