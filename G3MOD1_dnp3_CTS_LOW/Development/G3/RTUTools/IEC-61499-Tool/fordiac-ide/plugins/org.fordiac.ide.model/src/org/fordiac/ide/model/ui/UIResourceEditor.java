/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.eclipse.emf.common.util.EList;
import org.fordiac.ide.model.libraryElement.Resource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UI Resource Editor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.UIResourceEditor#getResourceElement <em>Resource Element</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.UIResourceEditor#getFbNetworks <em>Fb Networks</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.UIResourceEditor#isShowResizeUIFBNetworks <em>Show Resize UIFB Networks</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.UIResourceEditor#getActiveNetwork <em>Active Network</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.UIResourceEditor#getMappingEditorSize <em>Mapping Editor Size</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.UIResourceEditor#getSystemConfiguration <em>System Configuration</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getUIResourceEditor()
 * @model
 * @generated
 */
public interface UIResourceEditor extends Diagram {
	/**
	 * Returns the value of the '<em><b>Resource Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Element</em>' reference.
	 * @see #setResourceElement(Resource)
	 * @see org.fordiac.ide.model.ui.UiPackage#getUIResourceEditor_ResourceElement()
	 * @model
	 * @generated
	 */
	Resource getResourceElement();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.UIResourceEditor#getResourceElement <em>Resource Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Element</em>' reference.
	 * @see #getResourceElement()
	 * @generated
	 */
	void setResourceElement(Resource value);

	/**
	 * Returns the value of the '<em><b>Fb Networks</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.ui.ResizeUIFBNetwork}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fb Networks</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fb Networks</em>' containment reference list.
	 * @see org.fordiac.ide.model.ui.UiPackage#getUIResourceEditor_FbNetworks()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<ResizeUIFBNetwork> getFbNetworks();

	/**
	 * Returns the value of the '<em><b>Show Resize UIFB Networks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Resize UIFB Networks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Resize UIFB Networks</em>' attribute.
	 * @see #setShowResizeUIFBNetworks(boolean)
	 * @see org.fordiac.ide.model.ui.UiPackage#getUIResourceEditor_ShowResizeUIFBNetworks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isShowResizeUIFBNetworks();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.UIResourceEditor#isShowResizeUIFBNetworks <em>Show Resize UIFB Networks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Resize UIFB Networks</em>' attribute.
	 * @see #isShowResizeUIFBNetworks()
	 * @generated
	 */
	void setShowResizeUIFBNetworks(boolean value);

	/**
	 * Returns the value of the '<em><b>Active Network</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Network</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Network</em>' reference.
	 * @see #setActiveNetwork(ResizeUIFBNetwork)
	 * @see org.fordiac.ide.model.ui.UiPackage#getUIResourceEditor_ActiveNetwork()
	 * @model
	 * @generated
	 */
	ResizeUIFBNetwork getActiveNetwork();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.UIResourceEditor#getActiveNetwork <em>Active Network</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active Network</em>' reference.
	 * @see #getActiveNetwork()
	 * @generated
	 */
	void setActiveNetwork(ResizeUIFBNetwork value);

	/**
	 * Returns the value of the '<em><b>Mapping Editor Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapping Editor Size</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mapping Editor Size</em>' containment reference.
	 * @see #setMappingEditorSize(Size)
	 * @see org.fordiac.ide.model.ui.UiPackage#getUIResourceEditor_MappingEditorSize()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Size getMappingEditorSize();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.UIResourceEditor#getMappingEditorSize <em>Mapping Editor Size</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mapping Editor Size</em>' containment reference.
	 * @see #getMappingEditorSize()
	 * @generated
	 */
	void setMappingEditorSize(Size value);

	/**
	 * Returns the value of the '<em><b>System Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Configuration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Configuration</em>' reference.
	 * @see #setSystemConfiguration(UISystemConfiguration)
	 * @see org.fordiac.ide.model.ui.UiPackage#getUIResourceEditor_SystemConfiguration()
	 * @model
	 * @generated
	 */
	UISystemConfiguration getSystemConfiguration();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.UIResourceEditor#getSystemConfiguration <em>System Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Configuration</em>' reference.
	 * @see #getSystemConfiguration()
	 * @generated
	 */
	void setSystemConfiguration(UISystemConfiguration value);

} // UIResourceEditor
