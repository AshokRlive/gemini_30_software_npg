/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.fordiac.ide.model.ui.UiFactory
 * @model kind="package"
 * @generated
 */
public interface UiPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "org.fordiac.ide.model.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.fordiac.ide.model.ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UiPackage eINSTANCE = org.fordiac.ide.model.ui.impl.UiPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.ViewImpl <em>View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.ViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getView()
	 * @generated
	 */
	int VIEW = 0;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW__POSITION = 0;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW__BACKGROUND_COLOR = 1;

	/**
	 * The number of structural features of the '<em>View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.DiagramImpl <em>Diagram</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.DiagramImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getDiagram()
	 * @generated
	 */
	int DIAGRAM = 1;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__CHILDREN = 0;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__CONNECTIONS = 1;

	/**
	 * The number of structural features of the '<em>Diagram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.UIFBNetworkImpl <em>UIFB Network</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.UIFBNetworkImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getUIFBNetwork()
	 * @generated
	 */
	int UIFB_NETWORK = 2;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UIFB_NETWORK__CHILDREN = DIAGRAM__CHILDREN;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UIFB_NETWORK__CONNECTIONS = DIAGRAM__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Fb Network</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UIFB_NETWORK__FB_NETWORK = DIAGRAM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>UIFB Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UIFB_NETWORK_FEATURE_COUNT = DIAGRAM_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.FBViewImpl <em>FB View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.FBViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getFBView()
	 * @generated
	 */
	int FB_VIEW = 3;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FB_VIEW__POSITION = VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FB_VIEW__BACKGROUND_COLOR = VIEW__BACKGROUND_COLOR;

	/**
	 * The feature id for the '<em><b>Fb</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FB_VIEW__FB = VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Interface Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FB_VIEW__INTERFACE_ELEMENTS = VIEW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Resource Type FB</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FB_VIEW__RESOURCE_TYPE_FB = VIEW_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Mapped FB</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FB_VIEW__MAPPED_FB = VIEW_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Application FB</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FB_VIEW__APPLICATION_FB = VIEW_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>FB View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FB_VIEW_FEATURE_COUNT = VIEW_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.PositionImpl <em>Position</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.PositionImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getPosition()
	 * @generated
	 */
	int POSITION = 4;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION__Y = 1;

	/**
	 * The number of structural features of the '<em>Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_FEATURE_COUNT = 2;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.InterfaceElementViewImpl <em>Interface Element View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.InterfaceElementViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getInterfaceElementView()
	 * @generated
	 */
	int INTERFACE_ELEMENT_VIEW = 5;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_ELEMENT_VIEW__POSITION = VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_ELEMENT_VIEW__BACKGROUND_COLOR = VIEW__BACKGROUND_COLOR;

	/**
	 * The feature id for the '<em><b>IInterface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT = VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>In Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS = VIEW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Out Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS = VIEW_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_ELEMENT_VIEW__LABEL = VIEW_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Mapped Interface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT = VIEW_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Fb Network</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_ELEMENT_VIEW__FB_NETWORK = VIEW_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Application Interface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT = VIEW_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Interface Element View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_ELEMENT_VIEW_FEATURE_COUNT = VIEW_FEATURE_COUNT + 7;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.IConnectionView <em>IConnection View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.IConnectionView
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getIConnectionView()
	 * @generated
	 */
	int ICONNECTION_VIEW = 23;

	/**
	 * The number of structural features of the '<em>IConnection View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTION_VIEW_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.ConnectionViewImpl <em>Connection View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.ConnectionViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getConnectionView()
	 * @generated
	 */
	int CONNECTION_VIEW = 6;

	/**
	 * The feature id for the '<em><b>Connection Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_VIEW__CONNECTION_ELEMENT = ICONNECTION_VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_VIEW__SOURCE = ICONNECTION_VIEW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_VIEW__DESTINATION = ICONNECTION_VIEW_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>External Destination Interface Element View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_VIEW__EXTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW = ICONNECTION_VIEW_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Internal Destination Interface Element View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_VIEW__INTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW = ICONNECTION_VIEW_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>External Source Interface Element View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_VIEW__EXTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW = ICONNECTION_VIEW_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Internal Source Interface Element View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_VIEW__INTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW = ICONNECTION_VIEW_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Connection View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_VIEW_FEATURE_COUNT = ICONNECTION_VIEW_FEATURE_COUNT + 7;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.SubAppViewImpl <em>Sub App View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.SubAppViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getSubAppView()
	 * @generated
	 */
	int SUB_APP_VIEW = 7;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_VIEW__POSITION = VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_VIEW__BACKGROUND_COLOR = VIEW__BACKGROUND_COLOR;

	/**
	 * The feature id for the '<em><b>Sub App</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_VIEW__SUB_APP = VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Interface Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_VIEW__INTERFACE_ELEMENTS = VIEW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ui Sub App Network</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_VIEW__UI_SUB_APP_NETWORK = VIEW_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Mapped Sub App</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_VIEW__MAPPED_SUB_APP = VIEW_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Sub App View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_VIEW_FEATURE_COUNT = VIEW_FEATURE_COUNT + 4;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.SubAppInterfaceElementViewImpl <em>Sub App Interface Element View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.SubAppInterfaceElementViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getSubAppInterfaceElementView()
	 * @generated
	 */
	int SUB_APP_INTERFACE_ELEMENT_VIEW = 8;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_INTERFACE_ELEMENT_VIEW__POSITION = INTERFACE_ELEMENT_VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_INTERFACE_ELEMENT_VIEW__BACKGROUND_COLOR = INTERFACE_ELEMENT_VIEW__BACKGROUND_COLOR;

	/**
	 * The feature id for the '<em><b>IInterface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT = INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT;

	/**
	 * The feature id for the '<em><b>In Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS = INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Out Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS = INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL = INTERFACE_ELEMENT_VIEW__LABEL;

	/**
	 * The feature id for the '<em><b>Mapped Interface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT = INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Fb Network</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_INTERFACE_ELEMENT_VIEW__FB_NETWORK = INTERFACE_ELEMENT_VIEW__FB_NETWORK;

	/**
	 * The feature id for the '<em><b>Application Interface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT = INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_INTERFACE_ELEMENT_VIEW__VISIBLE = INTERFACE_ELEMENT_VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Label Substitute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL_SUBSTITUTE = INTERFACE_ELEMENT_VIEW_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sub App Interface Element View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_APP_INTERFACE_ELEMENT_VIEW_FEATURE_COUNT = INTERFACE_ELEMENT_VIEW_FEATURE_COUNT + 2;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.UISubAppNetworkImpl <em>UI Sub App Network</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.UISubAppNetworkImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getUISubAppNetwork()
	 * @generated
	 */
	int UI_SUB_APP_NETWORK = 9;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_SUB_APP_NETWORK__CHILDREN = DIAGRAM__CHILDREN;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_SUB_APP_NETWORK__CONNECTIONS = DIAGRAM__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Sub App Network</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_SUB_APP_NETWORK__SUB_APP_NETWORK = DIAGRAM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_SUB_APP_NETWORK__FILE_NAME = DIAGRAM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Interface Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_SUB_APP_NETWORK__INTERFACE_ELEMENTS = DIAGRAM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sub App View</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_SUB_APP_NETWORK__SUB_APP_VIEW = DIAGRAM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Root Application</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_SUB_APP_NETWORK__ROOT_APPLICATION = DIAGRAM_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>UI Sub App Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_SUB_APP_NETWORK_FEATURE_COUNT = DIAGRAM_FEATURE_COUNT + 5;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.InternalSubAppInterfaceElementViewImpl <em>Internal Sub App Interface Element View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.InternalSubAppInterfaceElementViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getInternalSubAppInterfaceElementView()
	 * @generated
	 */
	int INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW = 10;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW__POSITION = SUB_APP_INTERFACE_ELEMENT_VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW__BACKGROUND_COLOR = SUB_APP_INTERFACE_ELEMENT_VIEW__BACKGROUND_COLOR;

	/**
	 * The feature id for the '<em><b>IInterface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT = SUB_APP_INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT;

	/**
	 * The feature id for the '<em><b>In Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS = SUB_APP_INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Out Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS = SUB_APP_INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL = SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL;

	/**
	 * The feature id for the '<em><b>Mapped Interface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT = SUB_APP_INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Fb Network</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW__FB_NETWORK = SUB_APP_INTERFACE_ELEMENT_VIEW__FB_NETWORK;

	/**
	 * The feature id for the '<em><b>Application Interface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT = SUB_APP_INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW__VISIBLE = SUB_APP_INTERFACE_ELEMENT_VIEW__VISIBLE;

	/**
	 * The feature id for the '<em><b>Label Substitute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL_SUBSTITUTE = SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL_SUBSTITUTE;

	/**
	 * The number of structural features of the '<em>Internal Sub App Interface Element View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW_FEATURE_COUNT = SUB_APP_INTERFACE_ELEMENT_VIEW_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.ColorImpl <em>Color</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.ColorImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getColor()
	 * @generated
	 */
	int COLOR = 11;

	/**
	 * The feature id for the '<em><b>Red</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLOR__RED = 0;

	/**
	 * The feature id for the '<em><b>Green</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLOR__GREEN = 1;

	/**
	 * The feature id for the '<em><b>Blue</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLOR__BLUE = 2;

	/**
	 * The number of structural features of the '<em>Color</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLOR_FEATURE_COUNT = 3;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.UISystemConfigurationImpl <em>UI System Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.UISystemConfigurationImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getUISystemConfiguration()
	 * @generated
	 */
	int UI_SYSTEM_CONFIGURATION = 12;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_SYSTEM_CONFIGURATION__CHILDREN = DIAGRAM__CHILDREN;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_SYSTEM_CONFIGURATION__CONNECTIONS = DIAGRAM__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>System Config Network</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK = DIAGRAM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>UI System Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_SYSTEM_CONFIGURATION_FEATURE_COUNT = DIAGRAM_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.DeviceViewImpl <em>Device View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.DeviceViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getDeviceView()
	 * @generated
	 */
	int DEVICE_VIEW = 13;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_VIEW__POSITION = VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_VIEW__BACKGROUND_COLOR = VIEW__BACKGROUND_COLOR;

	/**
	 * The feature id for the '<em><b>Device Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_VIEW__DEVICE_ELEMENT = VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Show Resorce List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_VIEW__SHOW_RESORCE_LIST = VIEW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Interface Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_VIEW__INTERFACE_ELEMENTS = VIEW_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Resource Container View</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_VIEW__RESOURCE_CONTAINER_VIEW = VIEW_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>In Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_VIEW__IN_CONNECTIONS = VIEW_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Device View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_VIEW_FEATURE_COUNT = VIEW_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.ResourceViewImpl <em>Resource View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.ResourceViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getResourceView()
	 * @generated
	 */
	int RESOURCE_VIEW = 14;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_VIEW__POSITION = VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_VIEW__BACKGROUND_COLOR = VIEW__BACKGROUND_COLOR;

	/**
	 * The feature id for the '<em><b>Resource Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_VIEW__RESOURCE_ELEMENT = VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Interface Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_VIEW__INTERFACE_ELEMENTS = VIEW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Device Type Resource</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_VIEW__DEVICE_TYPE_RESOURCE = VIEW_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>UI Resource Diagram</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_VIEW__UI_RESOURCE_DIAGRAM = VIEW_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Resource View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_VIEW_FEATURE_COUNT = VIEW_FEATURE_COUNT + 4;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.ResourceContainerViewImpl <em>Resource Container View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.ResourceContainerViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getResourceContainerView()
	 * @generated
	 */
	int RESOURCE_CONTAINER_VIEW = 15;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_CONTAINER_VIEW__POSITION = VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_CONTAINER_VIEW__BACKGROUND_COLOR = VIEW__BACKGROUND_COLOR;

	/**
	 * The feature id for the '<em><b>Show Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_CONTAINER_VIEW__SHOW_CONTENT = VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_CONTAINER_VIEW__RESOURCES = VIEW_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Resource Container View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_CONTAINER_VIEW_FEATURE_COUNT = VIEW_FEATURE_COUNT + 2;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.UIResourceEditorImpl <em>UI Resource Editor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.UIResourceEditorImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getUIResourceEditor()
	 * @generated
	 */
	int UI_RESOURCE_EDITOR = 16;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_RESOURCE_EDITOR__CHILDREN = DIAGRAM__CHILDREN;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_RESOURCE_EDITOR__CONNECTIONS = DIAGRAM__CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Resource Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_RESOURCE_EDITOR__RESOURCE_ELEMENT = DIAGRAM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Fb Networks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_RESOURCE_EDITOR__FB_NETWORKS = DIAGRAM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Show Resize UIFB Networks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_RESOURCE_EDITOR__SHOW_RESIZE_UIFB_NETWORKS = DIAGRAM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Active Network</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_RESOURCE_EDITOR__ACTIVE_NETWORK = DIAGRAM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Mapping Editor Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE = DIAGRAM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>System Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_RESOURCE_EDITOR__SYSTEM_CONFIGURATION = DIAGRAM_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>UI Resource Editor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UI_RESOURCE_EDITOR_FEATURE_COUNT = DIAGRAM_FEATURE_COUNT + 6;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.ResizeUIFBNetworkImpl <em>Resize UIFB Network</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.ResizeUIFBNetworkImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getResizeUIFBNetwork()
	 * @generated
	 */
	int RESIZE_UIFB_NETWORK = 17;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESIZE_UIFB_NETWORK__SIZE = 0;

	/**
	 * The feature id for the '<em><b>Ui FB Network</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESIZE_UIFB_NETWORK__UI_FB_NETWORK = 1;

	/**
	 * The number of structural features of the '<em>Resize UIFB Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESIZE_UIFB_NETWORK_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.SizeImpl <em>Size</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.SizeImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getSize()
	 * @generated
	 */
	int SIZE = 18;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE__WIDTH = 0;

	/**
	 * The feature id for the '<em><b>Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE__HEIGHT = 1;

	/**
	 * The number of structural features of the '<em>Size</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_FEATURE_COUNT = 2;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.MappedSubAppViewImpl <em>Mapped Sub App View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.MappedSubAppViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getMappedSubAppView()
	 * @generated
	 */
	int MAPPED_SUB_APP_VIEW = 19;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_VIEW__POSITION = VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_VIEW__BACKGROUND_COLOR = VIEW__BACKGROUND_COLOR;

	/**
	 * The feature id for the '<em><b>Interface Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_VIEW__INTERFACE_ELEMENTS = VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Application Sub App</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP = VIEW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sub App</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_VIEW__SUB_APP = VIEW_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Mapped Sub App View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_VIEW_FEATURE_COUNT = VIEW_FEATURE_COUNT + 3;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.MappedSubAppInterfaceElementViewImpl <em>Mapped Sub App Interface Element View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.MappedSubAppInterfaceElementViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getMappedSubAppInterfaceElementView()
	 * @generated
	 */
	int MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW = 20;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW__POSITION = SUB_APP_INTERFACE_ELEMENT_VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW__BACKGROUND_COLOR = SUB_APP_INTERFACE_ELEMENT_VIEW__BACKGROUND_COLOR;

	/**
	 * The feature id for the '<em><b>IInterface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT = SUB_APP_INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT;

	/**
	 * The feature id for the '<em><b>In Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS = SUB_APP_INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Out Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS = SUB_APP_INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL = SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL;

	/**
	 * The feature id for the '<em><b>Mapped Interface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT = SUB_APP_INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Fb Network</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW__FB_NETWORK = SUB_APP_INTERFACE_ELEMENT_VIEW__FB_NETWORK;

	/**
	 * The feature id for the '<em><b>Application Interface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT = SUB_APP_INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW__VISIBLE = SUB_APP_INTERFACE_ELEMENT_VIEW__VISIBLE;

	/**
	 * The feature id for the '<em><b>Label Substitute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL_SUBSTITUTE = SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL_SUBSTITUTE;

	/**
	 * The number of structural features of the '<em>Mapped Sub App Interface Element View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW_FEATURE_COUNT = SUB_APP_INTERFACE_ELEMENT_VIEW_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.SegmentViewImpl <em>Segment View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.SegmentViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getSegmentView()
	 * @generated
	 */
	int SEGMENT_VIEW = 21;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_VIEW__POSITION = VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_VIEW__BACKGROUND_COLOR = VIEW__BACKGROUND_COLOR;

	/**
	 * The feature id for the '<em><b>Segment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_VIEW__SEGMENT = VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Out Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_VIEW__OUT_CONNECTIONS = VIEW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_VIEW__SIZE = VIEW_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Segment View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_VIEW_FEATURE_COUNT = VIEW_FEATURE_COUNT + 3;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.LinkViewImpl <em>Link View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.LinkViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getLinkView()
	 * @generated
	 */
	int LINK_VIEW = 22;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_VIEW__SOURCE = ICONNECTION_VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Link</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_VIEW__LINK = ICONNECTION_VIEW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_VIEW__DESTINATION = ICONNECTION_VIEW_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Link View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_VIEW_FEATURE_COUNT = ICONNECTION_VIEW_FEATURE_COUNT + 3;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.CompositeInternalInterfaceElementViewImpl <em>Composite Internal Interface Element View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.CompositeInternalInterfaceElementViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getCompositeInternalInterfaceElementView()
	 * @generated
	 */
	int COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW = 24;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW__POSITION = INTERFACE_ELEMENT_VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW__BACKGROUND_COLOR = INTERFACE_ELEMENT_VIEW__BACKGROUND_COLOR;

	/**
	 * The feature id for the '<em><b>IInterface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT = INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT;

	/**
	 * The feature id for the '<em><b>In Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS = INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Out Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS = INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW__LABEL = INTERFACE_ELEMENT_VIEW__LABEL;

	/**
	 * The feature id for the '<em><b>Mapped Interface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT = INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Fb Network</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW__FB_NETWORK = INTERFACE_ELEMENT_VIEW__FB_NETWORK;

	/**
	 * The feature id for the '<em><b>Application Interface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT = INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT;

	/**
	 * The number of structural features of the '<em>Composite Internal Interface Element View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW_FEATURE_COUNT = INTERFACE_ELEMENT_VIEW_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.fordiac.ide.model.ui.impl.MonitoringViewImpl <em>Monitoring View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.fordiac.ide.model.ui.impl.MonitoringViewImpl
	 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getMonitoringView()
	 * @generated
	 */
	int MONITORING_VIEW = 25;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_VIEW__POSITION = VIEW__POSITION;

	/**
	 * The feature id for the '<em><b>Background Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_VIEW__BACKGROUND_COLOR = VIEW__BACKGROUND_COLOR;

	/**
	 * The number of structural features of the '<em>Monitoring View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_VIEW_FEATURE_COUNT = VIEW_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.View <em>View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>View</em>'.
	 * @see org.fordiac.ide.model.ui.View
	 * @generated
	 */
	EClass getView();

	/**
	 * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.ui.View#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Position</em>'.
	 * @see org.fordiac.ide.model.ui.View#getPosition()
	 * @see #getView()
	 * @generated
	 */
	EReference getView_Position();

	/**
	 * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.ui.View#getBackgroundColor <em>Background Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Background Color</em>'.
	 * @see org.fordiac.ide.model.ui.View#getBackgroundColor()
	 * @see #getView()
	 * @generated
	 */
	EReference getView_BackgroundColor();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.Diagram <em>Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Diagram</em>'.
	 * @see org.fordiac.ide.model.ui.Diagram
	 * @generated
	 */
	EClass getDiagram();

	/**
	 * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.ui.Diagram#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see org.fordiac.ide.model.ui.Diagram#getChildren()
	 * @see #getDiagram()
	 * @generated
	 */
	EReference getDiagram_Children();

	/**
	 * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.ui.Diagram#getConnections <em>Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Connections</em>'.
	 * @see org.fordiac.ide.model.ui.Diagram#getConnections()
	 * @see #getDiagram()
	 * @generated
	 */
	EReference getDiagram_Connections();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.UIFBNetwork <em>UIFB Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UIFB Network</em>'.
	 * @see org.fordiac.ide.model.ui.UIFBNetwork
	 * @generated
	 */
	EClass getUIFBNetwork();

	/**
	 * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.ui.UIFBNetwork#getFbNetwork <em>Fb Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fb Network</em>'.
	 * @see org.fordiac.ide.model.ui.UIFBNetwork#getFbNetwork()
	 * @see #getUIFBNetwork()
	 * @generated
	 */
	EReference getUIFBNetwork_FbNetwork();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.FBView <em>FB View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FB View</em>'.
	 * @see org.fordiac.ide.model.ui.FBView
	 * @generated
	 */
	EClass getFBView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.FBView#getFb <em>Fb</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Fb</em>'.
	 * @see org.fordiac.ide.model.ui.FBView#getFb()
	 * @see #getFBView()
	 * @generated
	 */
	EReference getFBView_Fb();

	/**
	 * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.ui.FBView#getInterfaceElements <em>Interface Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interface Elements</em>'.
	 * @see org.fordiac.ide.model.ui.FBView#getInterfaceElements()
	 * @see #getFBView()
	 * @generated
	 */
	EReference getFBView_InterfaceElements();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.FBView#isResourceTypeFB <em>Resource Type FB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Resource Type FB</em>'.
	 * @see org.fordiac.ide.model.ui.FBView#isResourceTypeFB()
	 * @see #getFBView()
	 * @generated
	 */
	EAttribute getFBView_ResourceTypeFB();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.FBView#getMappedFB <em>Mapped FB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Mapped FB</em>'.
	 * @see org.fordiac.ide.model.ui.FBView#getMappedFB()
	 * @see #getFBView()
	 * @generated
	 */
	EReference getFBView_MappedFB();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.FBView#getApplicationFB <em>Application FB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Application FB</em>'.
	 * @see org.fordiac.ide.model.ui.FBView#getApplicationFB()
	 * @see #getFBView()
	 * @generated
	 */
	EReference getFBView_ApplicationFB();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.Position <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Position</em>'.
	 * @see org.fordiac.ide.model.ui.Position
	 * @generated
	 */
	EClass getPosition();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.Position#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see org.fordiac.ide.model.ui.Position#getX()
	 * @see #getPosition()
	 * @generated
	 */
	EAttribute getPosition_X();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.Position#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see org.fordiac.ide.model.ui.Position#getY()
	 * @see #getPosition()
	 * @generated
	 */
	EAttribute getPosition_Y();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.InterfaceElementView <em>Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface Element View</em>'.
	 * @see org.fordiac.ide.model.ui.InterfaceElementView
	 * @generated
	 */
	EClass getInterfaceElementView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.InterfaceElementView#getIInterfaceElement <em>IInterface Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>IInterface Element</em>'.
	 * @see org.fordiac.ide.model.ui.InterfaceElementView#getIInterfaceElement()
	 * @see #getInterfaceElementView()
	 * @generated
	 */
	EReference getInterfaceElementView_IInterfaceElement();

	/**
	 * Returns the meta object for the reference list '{@link org.fordiac.ide.model.ui.InterfaceElementView#getInConnections <em>In Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>In Connections</em>'.
	 * @see org.fordiac.ide.model.ui.InterfaceElementView#getInConnections()
	 * @see #getInterfaceElementView()
	 * @generated
	 */
	EReference getInterfaceElementView_InConnections();

	/**
	 * Returns the meta object for the reference list '{@link org.fordiac.ide.model.ui.InterfaceElementView#getOutConnections <em>Out Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Out Connections</em>'.
	 * @see org.fordiac.ide.model.ui.InterfaceElementView#getOutConnections()
	 * @see #getInterfaceElementView()
	 * @generated
	 */
	EReference getInterfaceElementView_OutConnections();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.InterfaceElementView#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.fordiac.ide.model.ui.InterfaceElementView#getLabel()
	 * @see #getInterfaceElementView()
	 * @generated
	 */
	EAttribute getInterfaceElementView_Label();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.InterfaceElementView#getMappedInterfaceElement <em>Mapped Interface Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Mapped Interface Element</em>'.
	 * @see org.fordiac.ide.model.ui.InterfaceElementView#getMappedInterfaceElement()
	 * @see #getInterfaceElementView()
	 * @generated
	 */
	EReference getInterfaceElementView_MappedInterfaceElement();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.InterfaceElementView#getFbNetwork <em>Fb Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Fb Network</em>'.
	 * @see org.fordiac.ide.model.ui.InterfaceElementView#getFbNetwork()
	 * @see #getInterfaceElementView()
	 * @generated
	 */
	EReference getInterfaceElementView_FbNetwork();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.InterfaceElementView#getApplicationInterfaceElement <em>Application Interface Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Application Interface Element</em>'.
	 * @see org.fordiac.ide.model.ui.InterfaceElementView#getApplicationInterfaceElement()
	 * @see #getInterfaceElementView()
	 * @generated
	 */
	EReference getInterfaceElementView_ApplicationInterfaceElement();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.ConnectionView <em>Connection View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection View</em>'.
	 * @see org.fordiac.ide.model.ui.ConnectionView
	 * @generated
	 */
	EClass getConnectionView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.ConnectionView#getConnectionElement <em>Connection Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Connection Element</em>'.
	 * @see org.fordiac.ide.model.ui.ConnectionView#getConnectionElement()
	 * @see #getConnectionView()
	 * @generated
	 */
	EReference getConnectionView_ConnectionElement();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.ConnectionView#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.fordiac.ide.model.ui.ConnectionView#getSource()
	 * @see #getConnectionView()
	 * @generated
	 */
	EReference getConnectionView_Source();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.ConnectionView#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Destination</em>'.
	 * @see org.fordiac.ide.model.ui.ConnectionView#getDestination()
	 * @see #getConnectionView()
	 * @generated
	 */
	EReference getConnectionView_Destination();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.ConnectionView#getExternalDestinationInterfaceElementView <em>External Destination Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>External Destination Interface Element View</em>'.
	 * @see org.fordiac.ide.model.ui.ConnectionView#getExternalDestinationInterfaceElementView()
	 * @see #getConnectionView()
	 * @generated
	 */
	EReference getConnectionView_ExternalDestinationInterfaceElementView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.ConnectionView#getInternalDestinationInterfaceElementView <em>Internal Destination Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Internal Destination Interface Element View</em>'.
	 * @see org.fordiac.ide.model.ui.ConnectionView#getInternalDestinationInterfaceElementView()
	 * @see #getConnectionView()
	 * @generated
	 */
	EReference getConnectionView_InternalDestinationInterfaceElementView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.ConnectionView#getExternalSourceInterfaceElementView <em>External Source Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>External Source Interface Element View</em>'.
	 * @see org.fordiac.ide.model.ui.ConnectionView#getExternalSourceInterfaceElementView()
	 * @see #getConnectionView()
	 * @generated
	 */
	EReference getConnectionView_ExternalSourceInterfaceElementView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.ConnectionView#getInternalSourceInterfaceElementView <em>Internal Source Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Internal Source Interface Element View</em>'.
	 * @see org.fordiac.ide.model.ui.ConnectionView#getInternalSourceInterfaceElementView()
	 * @see #getConnectionView()
	 * @generated
	 */
	EReference getConnectionView_InternalSourceInterfaceElementView();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.SubAppView <em>Sub App View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub App View</em>'.
	 * @see org.fordiac.ide.model.ui.SubAppView
	 * @generated
	 */
	EClass getSubAppView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.SubAppView#getSubApp <em>Sub App</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sub App</em>'.
	 * @see org.fordiac.ide.model.ui.SubAppView#getSubApp()
	 * @see #getSubAppView()
	 * @generated
	 */
	EReference getSubAppView_SubApp();

	/**
	 * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.ui.SubAppView#getInterfaceElements <em>Interface Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interface Elements</em>'.
	 * @see org.fordiac.ide.model.ui.SubAppView#getInterfaceElements()
	 * @see #getSubAppView()
	 * @generated
	 */
	EReference getSubAppView_InterfaceElements();

	/**
	 * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.ui.SubAppView#getUiSubAppNetwork <em>Ui Sub App Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Ui Sub App Network</em>'.
	 * @see org.fordiac.ide.model.ui.SubAppView#getUiSubAppNetwork()
	 * @see #getSubAppView()
	 * @generated
	 */
	EReference getSubAppView_UiSubAppNetwork();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.SubAppView#getMappedSubApp <em>Mapped Sub App</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Mapped Sub App</em>'.
	 * @see org.fordiac.ide.model.ui.SubAppView#getMappedSubApp()
	 * @see #getSubAppView()
	 * @generated
	 */
	EReference getSubAppView_MappedSubApp();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.SubAppInterfaceElementView <em>Sub App Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub App Interface Element View</em>'.
	 * @see org.fordiac.ide.model.ui.SubAppInterfaceElementView
	 * @generated
	 */
	EClass getSubAppInterfaceElementView();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.SubAppInterfaceElementView#isVisible <em>Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visible</em>'.
	 * @see org.fordiac.ide.model.ui.SubAppInterfaceElementView#isVisible()
	 * @see #getSubAppInterfaceElementView()
	 * @generated
	 */
	EAttribute getSubAppInterfaceElementView_Visible();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.SubAppInterfaceElementView#getLabelSubstitute <em>Label Substitute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label Substitute</em>'.
	 * @see org.fordiac.ide.model.ui.SubAppInterfaceElementView#getLabelSubstitute()
	 * @see #getSubAppInterfaceElementView()
	 * @generated
	 */
	EAttribute getSubAppInterfaceElementView_LabelSubstitute();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.UISubAppNetwork <em>UI Sub App Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UI Sub App Network</em>'.
	 * @see org.fordiac.ide.model.ui.UISubAppNetwork
	 * @generated
	 */
	EClass getUISubAppNetwork();

	/**
	 * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.ui.UISubAppNetwork#getSubAppNetwork <em>Sub App Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sub App Network</em>'.
	 * @see org.fordiac.ide.model.ui.UISubAppNetwork#getSubAppNetwork()
	 * @see #getUISubAppNetwork()
	 * @generated
	 */
	EReference getUISubAppNetwork_SubAppNetwork();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.UISubAppNetwork#getFileName <em>File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>File Name</em>'.
	 * @see org.fordiac.ide.model.ui.UISubAppNetwork#getFileName()
	 * @see #getUISubAppNetwork()
	 * @generated
	 */
	EAttribute getUISubAppNetwork_FileName();

	/**
	 * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.ui.UISubAppNetwork#getInterfaceElements <em>Interface Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interface Elements</em>'.
	 * @see org.fordiac.ide.model.ui.UISubAppNetwork#getInterfaceElements()
	 * @see #getUISubAppNetwork()
	 * @generated
	 */
	EReference getUISubAppNetwork_InterfaceElements();

	/**
	 * Returns the meta object for the container reference '{@link org.fordiac.ide.model.ui.UISubAppNetwork#getSubAppView <em>Sub App View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Sub App View</em>'.
	 * @see org.fordiac.ide.model.ui.UISubAppNetwork#getSubAppView()
	 * @see #getUISubAppNetwork()
	 * @generated
	 */
	EReference getUISubAppNetwork_SubAppView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.UISubAppNetwork#getRootApplication <em>Root Application</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Root Application</em>'.
	 * @see org.fordiac.ide.model.ui.UISubAppNetwork#getRootApplication()
	 * @see #getUISubAppNetwork()
	 * @generated
	 */
	EReference getUISubAppNetwork_RootApplication();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView <em>Internal Sub App Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Internal Sub App Interface Element View</em>'.
	 * @see org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView
	 * @generated
	 */
	EClass getInternalSubAppInterfaceElementView();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.Color <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Color</em>'.
	 * @see org.fordiac.ide.model.ui.Color
	 * @generated
	 */
	EClass getColor();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.Color#getRed <em>Red</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Red</em>'.
	 * @see org.fordiac.ide.model.ui.Color#getRed()
	 * @see #getColor()
	 * @generated
	 */
	EAttribute getColor_Red();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.Color#getGreen <em>Green</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Green</em>'.
	 * @see org.fordiac.ide.model.ui.Color#getGreen()
	 * @see #getColor()
	 * @generated
	 */
	EAttribute getColor_Green();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.Color#getBlue <em>Blue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Blue</em>'.
	 * @see org.fordiac.ide.model.ui.Color#getBlue()
	 * @see #getColor()
	 * @generated
	 */
	EAttribute getColor_Blue();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.UISystemConfiguration <em>UI System Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UI System Configuration</em>'.
	 * @see org.fordiac.ide.model.ui.UISystemConfiguration
	 * @generated
	 */
	EClass getUISystemConfiguration();

	/**
	 * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.ui.UISystemConfiguration#getSystemConfigNetwork <em>System Config Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>System Config Network</em>'.
	 * @see org.fordiac.ide.model.ui.UISystemConfiguration#getSystemConfigNetwork()
	 * @see #getUISystemConfiguration()
	 * @generated
	 */
	EReference getUISystemConfiguration_SystemConfigNetwork();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.DeviceView <em>Device View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Device View</em>'.
	 * @see org.fordiac.ide.model.ui.DeviceView
	 * @generated
	 */
	EClass getDeviceView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.DeviceView#getDeviceElement <em>Device Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device Element</em>'.
	 * @see org.fordiac.ide.model.ui.DeviceView#getDeviceElement()
	 * @see #getDeviceView()
	 * @generated
	 */
	EReference getDeviceView_DeviceElement();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.DeviceView#isShowResorceList <em>Show Resorce List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Resorce List</em>'.
	 * @see org.fordiac.ide.model.ui.DeviceView#isShowResorceList()
	 * @see #getDeviceView()
	 * @generated
	 */
	EAttribute getDeviceView_ShowResorceList();

	/**
	 * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.ui.DeviceView#getInterfaceElements <em>Interface Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interface Elements</em>'.
	 * @see org.fordiac.ide.model.ui.DeviceView#getInterfaceElements()
	 * @see #getDeviceView()
	 * @generated
	 */
	EReference getDeviceView_InterfaceElements();

	/**
	 * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.ui.DeviceView#getResourceContainerView <em>Resource Container View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Resource Container View</em>'.
	 * @see org.fordiac.ide.model.ui.DeviceView#getResourceContainerView()
	 * @see #getDeviceView()
	 * @generated
	 */
	EReference getDeviceView_ResourceContainerView();

	/**
	 * Returns the meta object for the reference list '{@link org.fordiac.ide.model.ui.DeviceView#getInConnections <em>In Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>In Connections</em>'.
	 * @see org.fordiac.ide.model.ui.DeviceView#getInConnections()
	 * @see #getDeviceView()
	 * @generated
	 */
	EReference getDeviceView_InConnections();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.ResourceView <em>Resource View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource View</em>'.
	 * @see org.fordiac.ide.model.ui.ResourceView
	 * @generated
	 */
	EClass getResourceView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.ResourceView#getResourceElement <em>Resource Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource Element</em>'.
	 * @see org.fordiac.ide.model.ui.ResourceView#getResourceElement()
	 * @see #getResourceView()
	 * @generated
	 */
	EReference getResourceView_ResourceElement();

	/**
	 * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.ui.ResourceView#getInterfaceElements <em>Interface Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interface Elements</em>'.
	 * @see org.fordiac.ide.model.ui.ResourceView#getInterfaceElements()
	 * @see #getResourceView()
	 * @generated
	 */
	EReference getResourceView_InterfaceElements();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.ResourceView#isDeviceTypeResource <em>Device Type Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Device Type Resource</em>'.
	 * @see org.fordiac.ide.model.ui.ResourceView#isDeviceTypeResource()
	 * @see #getResourceView()
	 * @generated
	 */
	EAttribute getResourceView_DeviceTypeResource();

	/**
	 * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.ui.ResourceView#getUIResourceDiagram <em>UI Resource Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>UI Resource Diagram</em>'.
	 * @see org.fordiac.ide.model.ui.ResourceView#getUIResourceDiagram()
	 * @see #getResourceView()
	 * @generated
	 */
	EReference getResourceView_UIResourceDiagram();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.ResourceContainerView <em>Resource Container View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Container View</em>'.
	 * @see org.fordiac.ide.model.ui.ResourceContainerView
	 * @generated
	 */
	EClass getResourceContainerView();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.ResourceContainerView#isShowContent <em>Show Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Content</em>'.
	 * @see org.fordiac.ide.model.ui.ResourceContainerView#isShowContent()
	 * @see #getResourceContainerView()
	 * @generated
	 */
	EAttribute getResourceContainerView_ShowContent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.ui.ResourceContainerView#getResources <em>Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Resources</em>'.
	 * @see org.fordiac.ide.model.ui.ResourceContainerView#getResources()
	 * @see #getResourceContainerView()
	 * @generated
	 */
	EReference getResourceContainerView_Resources();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.UIResourceEditor <em>UI Resource Editor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UI Resource Editor</em>'.
	 * @see org.fordiac.ide.model.ui.UIResourceEditor
	 * @generated
	 */
	EClass getUIResourceEditor();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.UIResourceEditor#getResourceElement <em>Resource Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource Element</em>'.
	 * @see org.fordiac.ide.model.ui.UIResourceEditor#getResourceElement()
	 * @see #getUIResourceEditor()
	 * @generated
	 */
	EReference getUIResourceEditor_ResourceElement();

	/**
	 * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.ui.UIResourceEditor#getFbNetworks <em>Fb Networks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fb Networks</em>'.
	 * @see org.fordiac.ide.model.ui.UIResourceEditor#getFbNetworks()
	 * @see #getUIResourceEditor()
	 * @generated
	 */
	EReference getUIResourceEditor_FbNetworks();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.UIResourceEditor#isShowResizeUIFBNetworks <em>Show Resize UIFB Networks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Resize UIFB Networks</em>'.
	 * @see org.fordiac.ide.model.ui.UIResourceEditor#isShowResizeUIFBNetworks()
	 * @see #getUIResourceEditor()
	 * @generated
	 */
	EAttribute getUIResourceEditor_ShowResizeUIFBNetworks();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.UIResourceEditor#getActiveNetwork <em>Active Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Active Network</em>'.
	 * @see org.fordiac.ide.model.ui.UIResourceEditor#getActiveNetwork()
	 * @see #getUIResourceEditor()
	 * @generated
	 */
	EReference getUIResourceEditor_ActiveNetwork();

	/**
	 * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.ui.UIResourceEditor#getMappingEditorSize <em>Mapping Editor Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Mapping Editor Size</em>'.
	 * @see org.fordiac.ide.model.ui.UIResourceEditor#getMappingEditorSize()
	 * @see #getUIResourceEditor()
	 * @generated
	 */
	EReference getUIResourceEditor_MappingEditorSize();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.UIResourceEditor#getSystemConfiguration <em>System Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>System Configuration</em>'.
	 * @see org.fordiac.ide.model.ui.UIResourceEditor#getSystemConfiguration()
	 * @see #getUIResourceEditor()
	 * @generated
	 */
	EReference getUIResourceEditor_SystemConfiguration();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.ResizeUIFBNetwork <em>Resize UIFB Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resize UIFB Network</em>'.
	 * @see org.fordiac.ide.model.ui.ResizeUIFBNetwork
	 * @generated
	 */
	EClass getResizeUIFBNetwork();

	/**
	 * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.ui.ResizeUIFBNetwork#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Size</em>'.
	 * @see org.fordiac.ide.model.ui.ResizeUIFBNetwork#getSize()
	 * @see #getResizeUIFBNetwork()
	 * @generated
	 */
	EReference getResizeUIFBNetwork_Size();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.ResizeUIFBNetwork#getUiFBNetwork <em>Ui FB Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ui FB Network</em>'.
	 * @see org.fordiac.ide.model.ui.ResizeUIFBNetwork#getUiFBNetwork()
	 * @see #getResizeUIFBNetwork()
	 * @generated
	 */
	EReference getResizeUIFBNetwork_UiFBNetwork();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.Size <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Size</em>'.
	 * @see org.fordiac.ide.model.ui.Size
	 * @generated
	 */
	EClass getSize();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.Size#getWidth <em>Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Width</em>'.
	 * @see org.fordiac.ide.model.ui.Size#getWidth()
	 * @see #getSize()
	 * @generated
	 */
	EAttribute getSize_Width();

	/**
	 * Returns the meta object for the attribute '{@link org.fordiac.ide.model.ui.Size#getHeight <em>Height</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Height</em>'.
	 * @see org.fordiac.ide.model.ui.Size#getHeight()
	 * @see #getSize()
	 * @generated
	 */
	EAttribute getSize_Height();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.MappedSubAppView <em>Mapped Sub App View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mapped Sub App View</em>'.
	 * @see org.fordiac.ide.model.ui.MappedSubAppView
	 * @generated
	 */
	EClass getMappedSubAppView();

	/**
	 * Returns the meta object for the containment reference list '{@link org.fordiac.ide.model.ui.MappedSubAppView#getInterfaceElements <em>Interface Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interface Elements</em>'.
	 * @see org.fordiac.ide.model.ui.MappedSubAppView#getInterfaceElements()
	 * @see #getMappedSubAppView()
	 * @generated
	 */
	EReference getMappedSubAppView_InterfaceElements();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.MappedSubAppView#getApplicationSubApp <em>Application Sub App</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Application Sub App</em>'.
	 * @see org.fordiac.ide.model.ui.MappedSubAppView#getApplicationSubApp()
	 * @see #getMappedSubAppView()
	 * @generated
	 */
	EReference getMappedSubAppView_ApplicationSubApp();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.MappedSubAppView#getSubApp <em>Sub App</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sub App</em>'.
	 * @see org.fordiac.ide.model.ui.MappedSubAppView#getSubApp()
	 * @see #getMappedSubAppView()
	 * @generated
	 */
	EReference getMappedSubAppView_SubApp();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView <em>Mapped Sub App Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mapped Sub App Interface Element View</em>'.
	 * @see org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView
	 * @generated
	 */
	EClass getMappedSubAppInterfaceElementView();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.SegmentView <em>Segment View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Segment View</em>'.
	 * @see org.fordiac.ide.model.ui.SegmentView
	 * @generated
	 */
	EClass getSegmentView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.SegmentView#getSegment <em>Segment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Segment</em>'.
	 * @see org.fordiac.ide.model.ui.SegmentView#getSegment()
	 * @see #getSegmentView()
	 * @generated
	 */
	EReference getSegmentView_Segment();

	/**
	 * Returns the meta object for the reference list '{@link org.fordiac.ide.model.ui.SegmentView#getOutConnections <em>Out Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Out Connections</em>'.
	 * @see org.fordiac.ide.model.ui.SegmentView#getOutConnections()
	 * @see #getSegmentView()
	 * @generated
	 */
	EReference getSegmentView_OutConnections();

	/**
	 * Returns the meta object for the containment reference '{@link org.fordiac.ide.model.ui.SegmentView#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Size</em>'.
	 * @see org.fordiac.ide.model.ui.SegmentView#getSize()
	 * @see #getSegmentView()
	 * @generated
	 */
	EReference getSegmentView_Size();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.LinkView <em>Link View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Link View</em>'.
	 * @see org.fordiac.ide.model.ui.LinkView
	 * @generated
	 */
	EClass getLinkView();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.LinkView#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.fordiac.ide.model.ui.LinkView#getSource()
	 * @see #getLinkView()
	 * @generated
	 */
	EReference getLinkView_Source();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.LinkView#getLink <em>Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Link</em>'.
	 * @see org.fordiac.ide.model.ui.LinkView#getLink()
	 * @see #getLinkView()
	 * @generated
	 */
	EReference getLinkView_Link();

	/**
	 * Returns the meta object for the reference '{@link org.fordiac.ide.model.ui.LinkView#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Destination</em>'.
	 * @see org.fordiac.ide.model.ui.LinkView#getDestination()
	 * @see #getLinkView()
	 * @generated
	 */
	EReference getLinkView_Destination();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.IConnectionView <em>IConnection View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IConnection View</em>'.
	 * @see org.fordiac.ide.model.ui.IConnectionView
	 * @generated
	 */
	EClass getIConnectionView();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.CompositeInternalInterfaceElementView <em>Composite Internal Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Internal Interface Element View</em>'.
	 * @see org.fordiac.ide.model.ui.CompositeInternalInterfaceElementView
	 * @generated
	 */
	EClass getCompositeInternalInterfaceElementView();

	/**
	 * Returns the meta object for class '{@link org.fordiac.ide.model.ui.MonitoringView <em>Monitoring View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitoring View</em>'.
	 * @see org.fordiac.ide.model.ui.MonitoringView
	 * @generated
	 */
	EClass getMonitoringView();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UiFactory getUiFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.ViewImpl <em>View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.ViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getView()
		 * @generated
		 */
		EClass VIEW = eINSTANCE.getView();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIEW__POSITION = eINSTANCE.getView_Position();

		/**
		 * The meta object literal for the '<em><b>Background Color</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIEW__BACKGROUND_COLOR = eINSTANCE.getView_BackgroundColor();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.DiagramImpl <em>Diagram</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.DiagramImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getDiagram()
		 * @generated
		 */
		EClass DIAGRAM = eINSTANCE.getDiagram();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIAGRAM__CHILDREN = eINSTANCE.getDiagram_Children();

		/**
		 * The meta object literal for the '<em><b>Connections</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIAGRAM__CONNECTIONS = eINSTANCE.getDiagram_Connections();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.UIFBNetworkImpl <em>UIFB Network</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.UIFBNetworkImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getUIFBNetwork()
		 * @generated
		 */
		EClass UIFB_NETWORK = eINSTANCE.getUIFBNetwork();

		/**
		 * The meta object literal for the '<em><b>Fb Network</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UIFB_NETWORK__FB_NETWORK = eINSTANCE.getUIFBNetwork_FbNetwork();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.FBViewImpl <em>FB View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.FBViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getFBView()
		 * @generated
		 */
		EClass FB_VIEW = eINSTANCE.getFBView();

		/**
		 * The meta object literal for the '<em><b>Fb</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FB_VIEW__FB = eINSTANCE.getFBView_Fb();

		/**
		 * The meta object literal for the '<em><b>Interface Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FB_VIEW__INTERFACE_ELEMENTS = eINSTANCE.getFBView_InterfaceElements();

		/**
		 * The meta object literal for the '<em><b>Resource Type FB</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FB_VIEW__RESOURCE_TYPE_FB = eINSTANCE.getFBView_ResourceTypeFB();

		/**
		 * The meta object literal for the '<em><b>Mapped FB</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FB_VIEW__MAPPED_FB = eINSTANCE.getFBView_MappedFB();

		/**
		 * The meta object literal for the '<em><b>Application FB</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FB_VIEW__APPLICATION_FB = eINSTANCE.getFBView_ApplicationFB();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.PositionImpl <em>Position</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.PositionImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getPosition()
		 * @generated
		 */
		EClass POSITION = eINSTANCE.getPosition();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POSITION__X = eINSTANCE.getPosition_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POSITION__Y = eINSTANCE.getPosition_Y();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.InterfaceElementViewImpl <em>Interface Element View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.InterfaceElementViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getInterfaceElementView()
		 * @generated
		 */
		EClass INTERFACE_ELEMENT_VIEW = eINSTANCE.getInterfaceElementView();

		/**
		 * The meta object literal for the '<em><b>IInterface Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_ELEMENT_VIEW__IINTERFACE_ELEMENT = eINSTANCE.getInterfaceElementView_IInterfaceElement();

		/**
		 * The meta object literal for the '<em><b>In Connections</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS = eINSTANCE.getInterfaceElementView_InConnections();

		/**
		 * The meta object literal for the '<em><b>Out Connections</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS = eINSTANCE.getInterfaceElementView_OutConnections();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE_ELEMENT_VIEW__LABEL = eINSTANCE.getInterfaceElementView_Label();

		/**
		 * The meta object literal for the '<em><b>Mapped Interface Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_ELEMENT_VIEW__MAPPED_INTERFACE_ELEMENT = eINSTANCE.getInterfaceElementView_MappedInterfaceElement();

		/**
		 * The meta object literal for the '<em><b>Fb Network</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_ELEMENT_VIEW__FB_NETWORK = eINSTANCE.getInterfaceElementView_FbNetwork();

		/**
		 * The meta object literal for the '<em><b>Application Interface Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_ELEMENT_VIEW__APPLICATION_INTERFACE_ELEMENT = eINSTANCE.getInterfaceElementView_ApplicationInterfaceElement();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.ConnectionViewImpl <em>Connection View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.ConnectionViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getConnectionView()
		 * @generated
		 */
		EClass CONNECTION_VIEW = eINSTANCE.getConnectionView();

		/**
		 * The meta object literal for the '<em><b>Connection Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_VIEW__CONNECTION_ELEMENT = eINSTANCE.getConnectionView_ConnectionElement();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_VIEW__SOURCE = eINSTANCE.getConnectionView_Source();

		/**
		 * The meta object literal for the '<em><b>Destination</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_VIEW__DESTINATION = eINSTANCE.getConnectionView_Destination();

		/**
		 * The meta object literal for the '<em><b>External Destination Interface Element View</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_VIEW__EXTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW = eINSTANCE.getConnectionView_ExternalDestinationInterfaceElementView();

		/**
		 * The meta object literal for the '<em><b>Internal Destination Interface Element View</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_VIEW__INTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW = eINSTANCE.getConnectionView_InternalDestinationInterfaceElementView();

		/**
		 * The meta object literal for the '<em><b>External Source Interface Element View</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_VIEW__EXTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW = eINSTANCE.getConnectionView_ExternalSourceInterfaceElementView();

		/**
		 * The meta object literal for the '<em><b>Internal Source Interface Element View</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_VIEW__INTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW = eINSTANCE.getConnectionView_InternalSourceInterfaceElementView();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.SubAppViewImpl <em>Sub App View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.SubAppViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getSubAppView()
		 * @generated
		 */
		EClass SUB_APP_VIEW = eINSTANCE.getSubAppView();

		/**
		 * The meta object literal for the '<em><b>Sub App</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_APP_VIEW__SUB_APP = eINSTANCE.getSubAppView_SubApp();

		/**
		 * The meta object literal for the '<em><b>Interface Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_APP_VIEW__INTERFACE_ELEMENTS = eINSTANCE.getSubAppView_InterfaceElements();

		/**
		 * The meta object literal for the '<em><b>Ui Sub App Network</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_APP_VIEW__UI_SUB_APP_NETWORK = eINSTANCE.getSubAppView_UiSubAppNetwork();

		/**
		 * The meta object literal for the '<em><b>Mapped Sub App</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_APP_VIEW__MAPPED_SUB_APP = eINSTANCE.getSubAppView_MappedSubApp();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.SubAppInterfaceElementViewImpl <em>Sub App Interface Element View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.SubAppInterfaceElementViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getSubAppInterfaceElementView()
		 * @generated
		 */
		EClass SUB_APP_INTERFACE_ELEMENT_VIEW = eINSTANCE.getSubAppInterfaceElementView();

		/**
		 * The meta object literal for the '<em><b>Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUB_APP_INTERFACE_ELEMENT_VIEW__VISIBLE = eINSTANCE.getSubAppInterfaceElementView_Visible();

		/**
		 * The meta object literal for the '<em><b>Label Substitute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUB_APP_INTERFACE_ELEMENT_VIEW__LABEL_SUBSTITUTE = eINSTANCE.getSubAppInterfaceElementView_LabelSubstitute();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.UISubAppNetworkImpl <em>UI Sub App Network</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.UISubAppNetworkImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getUISubAppNetwork()
		 * @generated
		 */
		EClass UI_SUB_APP_NETWORK = eINSTANCE.getUISubAppNetwork();

		/**
		 * The meta object literal for the '<em><b>Sub App Network</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UI_SUB_APP_NETWORK__SUB_APP_NETWORK = eINSTANCE.getUISubAppNetwork_SubAppNetwork();

		/**
		 * The meta object literal for the '<em><b>File Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UI_SUB_APP_NETWORK__FILE_NAME = eINSTANCE.getUISubAppNetwork_FileName();

		/**
		 * The meta object literal for the '<em><b>Interface Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UI_SUB_APP_NETWORK__INTERFACE_ELEMENTS = eINSTANCE.getUISubAppNetwork_InterfaceElements();

		/**
		 * The meta object literal for the '<em><b>Sub App View</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UI_SUB_APP_NETWORK__SUB_APP_VIEW = eINSTANCE.getUISubAppNetwork_SubAppView();

		/**
		 * The meta object literal for the '<em><b>Root Application</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UI_SUB_APP_NETWORK__ROOT_APPLICATION = eINSTANCE.getUISubAppNetwork_RootApplication();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.InternalSubAppInterfaceElementViewImpl <em>Internal Sub App Interface Element View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.InternalSubAppInterfaceElementViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getInternalSubAppInterfaceElementView()
		 * @generated
		 */
		EClass INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW = eINSTANCE.getInternalSubAppInterfaceElementView();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.ColorImpl <em>Color</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.ColorImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getColor()
		 * @generated
		 */
		EClass COLOR = eINSTANCE.getColor();

		/**
		 * The meta object literal for the '<em><b>Red</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COLOR__RED = eINSTANCE.getColor_Red();

		/**
		 * The meta object literal for the '<em><b>Green</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COLOR__GREEN = eINSTANCE.getColor_Green();

		/**
		 * The meta object literal for the '<em><b>Blue</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COLOR__BLUE = eINSTANCE.getColor_Blue();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.UISystemConfigurationImpl <em>UI System Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.UISystemConfigurationImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getUISystemConfiguration()
		 * @generated
		 */
		EClass UI_SYSTEM_CONFIGURATION = eINSTANCE.getUISystemConfiguration();

		/**
		 * The meta object literal for the '<em><b>System Config Network</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK = eINSTANCE.getUISystemConfiguration_SystemConfigNetwork();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.DeviceViewImpl <em>Device View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.DeviceViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getDeviceView()
		 * @generated
		 */
		EClass DEVICE_VIEW = eINSTANCE.getDeviceView();

		/**
		 * The meta object literal for the '<em><b>Device Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEVICE_VIEW__DEVICE_ELEMENT = eINSTANCE.getDeviceView_DeviceElement();

		/**
		 * The meta object literal for the '<em><b>Show Resorce List</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEVICE_VIEW__SHOW_RESORCE_LIST = eINSTANCE.getDeviceView_ShowResorceList();

		/**
		 * The meta object literal for the '<em><b>Interface Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEVICE_VIEW__INTERFACE_ELEMENTS = eINSTANCE.getDeviceView_InterfaceElements();

		/**
		 * The meta object literal for the '<em><b>Resource Container View</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEVICE_VIEW__RESOURCE_CONTAINER_VIEW = eINSTANCE.getDeviceView_ResourceContainerView();

		/**
		 * The meta object literal for the '<em><b>In Connections</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEVICE_VIEW__IN_CONNECTIONS = eINSTANCE.getDeviceView_InConnections();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.ResourceViewImpl <em>Resource View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.ResourceViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getResourceView()
		 * @generated
		 */
		EClass RESOURCE_VIEW = eINSTANCE.getResourceView();

		/**
		 * The meta object literal for the '<em><b>Resource Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_VIEW__RESOURCE_ELEMENT = eINSTANCE.getResourceView_ResourceElement();

		/**
		 * The meta object literal for the '<em><b>Interface Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_VIEW__INTERFACE_ELEMENTS = eINSTANCE.getResourceView_InterfaceElements();

		/**
		 * The meta object literal for the '<em><b>Device Type Resource</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE_VIEW__DEVICE_TYPE_RESOURCE = eINSTANCE.getResourceView_DeviceTypeResource();

		/**
		 * The meta object literal for the '<em><b>UI Resource Diagram</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_VIEW__UI_RESOURCE_DIAGRAM = eINSTANCE.getResourceView_UIResourceDiagram();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.ResourceContainerViewImpl <em>Resource Container View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.ResourceContainerViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getResourceContainerView()
		 * @generated
		 */
		EClass RESOURCE_CONTAINER_VIEW = eINSTANCE.getResourceContainerView();

		/**
		 * The meta object literal for the '<em><b>Show Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE_CONTAINER_VIEW__SHOW_CONTENT = eINSTANCE.getResourceContainerView_ShowContent();

		/**
		 * The meta object literal for the '<em><b>Resources</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_CONTAINER_VIEW__RESOURCES = eINSTANCE.getResourceContainerView_Resources();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.UIResourceEditorImpl <em>UI Resource Editor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.UIResourceEditorImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getUIResourceEditor()
		 * @generated
		 */
		EClass UI_RESOURCE_EDITOR = eINSTANCE.getUIResourceEditor();

		/**
		 * The meta object literal for the '<em><b>Resource Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UI_RESOURCE_EDITOR__RESOURCE_ELEMENT = eINSTANCE.getUIResourceEditor_ResourceElement();

		/**
		 * The meta object literal for the '<em><b>Fb Networks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UI_RESOURCE_EDITOR__FB_NETWORKS = eINSTANCE.getUIResourceEditor_FbNetworks();

		/**
		 * The meta object literal for the '<em><b>Show Resize UIFB Networks</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UI_RESOURCE_EDITOR__SHOW_RESIZE_UIFB_NETWORKS = eINSTANCE.getUIResourceEditor_ShowResizeUIFBNetworks();

		/**
		 * The meta object literal for the '<em><b>Active Network</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UI_RESOURCE_EDITOR__ACTIVE_NETWORK = eINSTANCE.getUIResourceEditor_ActiveNetwork();

		/**
		 * The meta object literal for the '<em><b>Mapping Editor Size</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE = eINSTANCE.getUIResourceEditor_MappingEditorSize();

		/**
		 * The meta object literal for the '<em><b>System Configuration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UI_RESOURCE_EDITOR__SYSTEM_CONFIGURATION = eINSTANCE.getUIResourceEditor_SystemConfiguration();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.ResizeUIFBNetworkImpl <em>Resize UIFB Network</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.ResizeUIFBNetworkImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getResizeUIFBNetwork()
		 * @generated
		 */
		EClass RESIZE_UIFB_NETWORK = eINSTANCE.getResizeUIFBNetwork();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESIZE_UIFB_NETWORK__SIZE = eINSTANCE.getResizeUIFBNetwork_Size();

		/**
		 * The meta object literal for the '<em><b>Ui FB Network</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESIZE_UIFB_NETWORK__UI_FB_NETWORK = eINSTANCE.getResizeUIFBNetwork_UiFBNetwork();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.SizeImpl <em>Size</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.SizeImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getSize()
		 * @generated
		 */
		EClass SIZE = eINSTANCE.getSize();

		/**
		 * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIZE__WIDTH = eINSTANCE.getSize_Width();

		/**
		 * The meta object literal for the '<em><b>Height</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIZE__HEIGHT = eINSTANCE.getSize_Height();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.MappedSubAppViewImpl <em>Mapped Sub App View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.MappedSubAppViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getMappedSubAppView()
		 * @generated
		 */
		EClass MAPPED_SUB_APP_VIEW = eINSTANCE.getMappedSubAppView();

		/**
		 * The meta object literal for the '<em><b>Interface Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPPED_SUB_APP_VIEW__INTERFACE_ELEMENTS = eINSTANCE.getMappedSubAppView_InterfaceElements();

		/**
		 * The meta object literal for the '<em><b>Application Sub App</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP = eINSTANCE.getMappedSubAppView_ApplicationSubApp();

		/**
		 * The meta object literal for the '<em><b>Sub App</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPPED_SUB_APP_VIEW__SUB_APP = eINSTANCE.getMappedSubAppView_SubApp();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.MappedSubAppInterfaceElementViewImpl <em>Mapped Sub App Interface Element View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.MappedSubAppInterfaceElementViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getMappedSubAppInterfaceElementView()
		 * @generated
		 */
		EClass MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW = eINSTANCE.getMappedSubAppInterfaceElementView();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.SegmentViewImpl <em>Segment View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.SegmentViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getSegmentView()
		 * @generated
		 */
		EClass SEGMENT_VIEW = eINSTANCE.getSegmentView();

		/**
		 * The meta object literal for the '<em><b>Segment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_VIEW__SEGMENT = eINSTANCE.getSegmentView_Segment();

		/**
		 * The meta object literal for the '<em><b>Out Connections</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_VIEW__OUT_CONNECTIONS = eINSTANCE.getSegmentView_OutConnections();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_VIEW__SIZE = eINSTANCE.getSegmentView_Size();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.LinkViewImpl <em>Link View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.LinkViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getLinkView()
		 * @generated
		 */
		EClass LINK_VIEW = eINSTANCE.getLinkView();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK_VIEW__SOURCE = eINSTANCE.getLinkView_Source();

		/**
		 * The meta object literal for the '<em><b>Link</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK_VIEW__LINK = eINSTANCE.getLinkView_Link();

		/**
		 * The meta object literal for the '<em><b>Destination</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK_VIEW__DESTINATION = eINSTANCE.getLinkView_Destination();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.IConnectionView <em>IConnection View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.IConnectionView
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getIConnectionView()
		 * @generated
		 */
		EClass ICONNECTION_VIEW = eINSTANCE.getIConnectionView();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.CompositeInternalInterfaceElementViewImpl <em>Composite Internal Interface Element View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.CompositeInternalInterfaceElementViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getCompositeInternalInterfaceElementView()
		 * @generated
		 */
		EClass COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW = eINSTANCE.getCompositeInternalInterfaceElementView();

		/**
		 * The meta object literal for the '{@link org.fordiac.ide.model.ui.impl.MonitoringViewImpl <em>Monitoring View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.fordiac.ide.model.ui.impl.MonitoringViewImpl
		 * @see org.fordiac.ide.model.ui.impl.UiPackageImpl#getMonitoringView()
		 * @generated
		 */
		EClass MONITORING_VIEW = eINSTANCE.getMonitoringView();

	}

} //UiPackage
