/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Sub App View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.SubAppViewImpl#getSubApp <em>Sub App</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.SubAppViewImpl#getInterfaceElements <em>Interface Elements</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.SubAppViewImpl#getUiSubAppNetwork <em>Ui Sub App Network</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.SubAppViewImpl#getMappedSubApp <em>Mapped Sub App</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubAppViewImpl extends ViewImpl implements SubAppView {
	/**
	 * The cached value of the '{@link #getSubApp() <em>Sub App</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSubApp()
	 * @generated
	 * @ordered
	 */
	protected SubApp subApp;

	/**
	 * The cached value of the '{@link #getInterfaceElements() <em>Interface Elements</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getInterfaceElements()
	 * @generated
	 * @ordered
	 */
	protected EList<SubAppInterfaceElementView> interfaceElements;

	/**
	 * The cached value of the '{@link #getUiSubAppNetwork() <em>Ui Sub App Network</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getUiSubAppNetwork()
	 * @generated
	 * @ordered
	 */
	protected UISubAppNetwork uiSubAppNetwork;

	/**
	 * The cached value of the '{@link #getMappedSubApp() <em>Mapped Sub App</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMappedSubApp()
	 * @generated
	 * @ordered
	 */
	protected MappedSubAppView mappedSubApp;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected SubAppViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.SUB_APP_VIEW;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SubApp getSubApp() {
		if (subApp != null && subApp.eIsProxy()) {
			InternalEObject oldSubApp = (InternalEObject)subApp;
			subApp = (SubApp)eResolveProxy(oldSubApp);
			if (subApp != oldSubApp) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.SUB_APP_VIEW__SUB_APP, oldSubApp, subApp));
			}
		}
		return subApp;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SubApp basicGetSubApp() {
		return subApp;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubApp(SubApp newSubApp) {
		SubApp oldSubApp = subApp;
		subApp = newSubApp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.SUB_APP_VIEW__SUB_APP, oldSubApp, subApp));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubAppInterfaceElementView> getInterfaceElements() {
		if (interfaceElements == null) {
			interfaceElements = new EObjectContainmentEList.Resolving<SubAppInterfaceElementView>(SubAppInterfaceElementView.class, this, UiPackage.SUB_APP_VIEW__INTERFACE_ELEMENTS);
		}
		return interfaceElements;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public UISubAppNetwork getUiSubAppNetwork() {
		// if (uiSubAppNetwork == null || uiSubAppNetwork != ) {

		return uiSubAppNetwork = (UISubAppNetwork) getSubApp()
				.getSubAppNetwork().eContainer();

		// }
		// return uiSubAppNetwork;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public UISubAppNetwork basicGetUiSubAppNetwork() {
		return uiSubAppNetwork;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUiSubAppNetwork(
			UISubAppNetwork newUiSubAppNetwork, NotificationChain msgs) {
		UISubAppNetwork oldUiSubAppNetwork = uiSubAppNetwork;
		uiSubAppNetwork = newUiSubAppNetwork;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.SUB_APP_VIEW__UI_SUB_APP_NETWORK, oldUiSubAppNetwork, newUiSubAppNetwork);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setUiSubAppNetwork(UISubAppNetwork newUiSubAppNetwork) {
		if (newUiSubAppNetwork != uiSubAppNetwork) {
			NotificationChain msgs = null;
			if (uiSubAppNetwork != null)
				msgs = ((InternalEObject)uiSubAppNetwork).eInverseRemove(this, UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW, UISubAppNetwork.class, msgs);
			if (newUiSubAppNetwork != null)
				msgs = ((InternalEObject)newUiSubAppNetwork).eInverseAdd(this, UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW, UISubAppNetwork.class, msgs);
			msgs = basicSetUiSubAppNetwork(newUiSubAppNetwork, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.SUB_APP_VIEW__UI_SUB_APP_NETWORK, newUiSubAppNetwork, newUiSubAppNetwork));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MappedSubAppView getMappedSubApp() {
		if (mappedSubApp != null && mappedSubApp.eIsProxy()) {
			InternalEObject oldMappedSubApp = (InternalEObject)mappedSubApp;
			mappedSubApp = (MappedSubAppView)eResolveProxy(oldMappedSubApp);
			if (mappedSubApp != oldMappedSubApp) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.SUB_APP_VIEW__MAPPED_SUB_APP, oldMappedSubApp, mappedSubApp));
			}
		}
		return mappedSubApp;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MappedSubAppView basicGetMappedSubApp() {
		return mappedSubApp;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMappedSubApp(
			MappedSubAppView newMappedSubApp, NotificationChain msgs) {
		MappedSubAppView oldMappedSubApp = mappedSubApp;
		mappedSubApp = newMappedSubApp;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.SUB_APP_VIEW__MAPPED_SUB_APP, oldMappedSubApp, newMappedSubApp);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMappedSubApp(MappedSubAppView newMappedSubApp) {
		if (newMappedSubApp != mappedSubApp) {
			NotificationChain msgs = null;
			if (mappedSubApp != null)
				msgs = ((InternalEObject)mappedSubApp).eInverseRemove(this, UiPackage.MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP, MappedSubAppView.class, msgs);
			if (newMappedSubApp != null)
				msgs = ((InternalEObject)newMappedSubApp).eInverseAdd(this, UiPackage.MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP, MappedSubAppView.class, msgs);
			msgs = basicSetMappedSubApp(newMappedSubApp, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.SUB_APP_VIEW__MAPPED_SUB_APP, newMappedSubApp, newMappedSubApp));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.SUB_APP_VIEW__UI_SUB_APP_NETWORK:
				if (uiSubAppNetwork != null)
					msgs = ((InternalEObject)uiSubAppNetwork).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.SUB_APP_VIEW__UI_SUB_APP_NETWORK, null, msgs);
				return basicSetUiSubAppNetwork((UISubAppNetwork)otherEnd, msgs);
			case UiPackage.SUB_APP_VIEW__MAPPED_SUB_APP:
				if (mappedSubApp != null)
					msgs = ((InternalEObject)mappedSubApp).eInverseRemove(this, UiPackage.MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP, MappedSubAppView.class, msgs);
				return basicSetMappedSubApp((MappedSubAppView)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.SUB_APP_VIEW__INTERFACE_ELEMENTS:
				return ((InternalEList<?>)getInterfaceElements()).basicRemove(otherEnd, msgs);
			case UiPackage.SUB_APP_VIEW__UI_SUB_APP_NETWORK:
				return basicSetUiSubAppNetwork(null, msgs);
			case UiPackage.SUB_APP_VIEW__MAPPED_SUB_APP:
				return basicSetMappedSubApp(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.SUB_APP_VIEW__SUB_APP:
				if (resolve) return getSubApp();
				return basicGetSubApp();
			case UiPackage.SUB_APP_VIEW__INTERFACE_ELEMENTS:
				return getInterfaceElements();
			case UiPackage.SUB_APP_VIEW__UI_SUB_APP_NETWORK:
				if (resolve) return getUiSubAppNetwork();
				return basicGetUiSubAppNetwork();
			case UiPackage.SUB_APP_VIEW__MAPPED_SUB_APP:
				if (resolve) return getMappedSubApp();
				return basicGetMappedSubApp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.SUB_APP_VIEW__SUB_APP:
				setSubApp((SubApp)newValue);
				return;
			case UiPackage.SUB_APP_VIEW__INTERFACE_ELEMENTS:
				getInterfaceElements().clear();
				getInterfaceElements().addAll((Collection<? extends SubAppInterfaceElementView>)newValue);
				return;
			case UiPackage.SUB_APP_VIEW__UI_SUB_APP_NETWORK:
				setUiSubAppNetwork((UISubAppNetwork)newValue);
				return;
			case UiPackage.SUB_APP_VIEW__MAPPED_SUB_APP:
				setMappedSubApp((MappedSubAppView)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.SUB_APP_VIEW__SUB_APP:
				setSubApp((SubApp)null);
				return;
			case UiPackage.SUB_APP_VIEW__INTERFACE_ELEMENTS:
				getInterfaceElements().clear();
				return;
			case UiPackage.SUB_APP_VIEW__UI_SUB_APP_NETWORK:
				setUiSubAppNetwork((UISubAppNetwork)null);
				return;
			case UiPackage.SUB_APP_VIEW__MAPPED_SUB_APP:
				setMappedSubApp((MappedSubAppView)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.SUB_APP_VIEW__SUB_APP:
				return subApp != null;
			case UiPackage.SUB_APP_VIEW__INTERFACE_ELEMENTS:
				return interfaceElements != null && !interfaceElements.isEmpty();
			case UiPackage.SUB_APP_VIEW__UI_SUB_APP_NETWORK:
				return uiSubAppNetwork != null;
			case UiPackage.SUB_APP_VIEW__MAPPED_SUB_APP:
				return mappedSubApp != null;
		}
		return super.eIsSet(featureID);
	}

} // SubAppViewImpl
