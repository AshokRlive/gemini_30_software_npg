package org.fordiac.ide.typelibrary;

import org.eclipse.core.resources.IFile;
import org.fordiac.ide.gef.IPaletteEntryCreator;
import org.fordiac.ide.model.Palette.PaletteFactory;
import org.fordiac.ide.model.Palette.SegmentTypePaletteEntry;
import org.fordiac.ide.model.Palette.impl.PaletteEntryImpl;

public class CreateSegmentTypePaletteEntry implements IPaletteEntryCreator, TypeLibraryTags {

	@Override
	public boolean canHandle(IFile file) {
		if (SEGMENT_TYPE_FILE_ENDING.equalsIgnoreCase(file.getFileExtension())){
			return true;
		} else
			return false;
	}

	@Override
	public PaletteEntryImpl createPaletteEntry() {
		SegmentTypePaletteEntry entry = PaletteFactory.eINSTANCE.createSegmentTypePaletteEntry();

		return (PaletteEntryImpl) entry;
	}

}
