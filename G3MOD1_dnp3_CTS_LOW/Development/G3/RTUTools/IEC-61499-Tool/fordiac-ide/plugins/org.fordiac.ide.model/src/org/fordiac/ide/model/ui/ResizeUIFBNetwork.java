/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.eclipse.emf.ecore.EObject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resize UIFB Network</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.ResizeUIFBNetwork#getSize <em>Size</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.ResizeUIFBNetwork#getUiFBNetwork <em>Ui FB Network</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getResizeUIFBNetwork()
 * @model
 * @generated
 */
public interface ResizeUIFBNetwork extends EObject {
	/**
	 * Returns the value of the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size</em>' containment reference.
	 * @see #setSize(Size)
	 * @see org.fordiac.ide.model.ui.UiPackage#getResizeUIFBNetwork_Size()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Size getSize();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.ResizeUIFBNetwork#getSize <em>Size</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size</em>' containment reference.
	 * @see #getSize()
	 * @generated
	 */
	void setSize(Size value);

	/**
	 * Returns the value of the '<em><b>Ui FB Network</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ui FB Network</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ui FB Network</em>' reference.
	 * @see #setUiFBNetwork(UIFBNetwork)
	 * @see org.fordiac.ide.model.ui.UiPackage#getResizeUIFBNetwork_UiFBNetwork()
	 * @model
	 * @generated
	 */
	UIFBNetwork getUiFBNetwork();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.ResizeUIFBNetwork#getUiFBNetwork <em>Ui FB Network</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ui FB Network</em>' reference.
	 * @see #getUiFBNetwork()
	 * @generated
	 */
	void setUiFBNetwork(UIFBNetwork value);

} // ResizeUIFBNetwork
