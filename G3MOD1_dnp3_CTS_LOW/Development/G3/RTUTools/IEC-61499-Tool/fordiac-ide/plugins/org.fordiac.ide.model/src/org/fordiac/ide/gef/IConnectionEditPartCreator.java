/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef;

import org.eclipse.gef.ConnectionEditPart;


/**
 * Objects implementing this element can create an EditPart (for graphical
 * Visualisation in GEF editors).
 * 
 * @author gebenh
 */
public interface IConnectionEditPartCreator {

	/**
	 * Creates the edit part.
	 * 
	 * @return the created EditPart
	 */
	public ConnectionEditPart createEditPart(String diagram);

	/**
	 * Creates the edit part.
	 * 
	 * @return the created EditPart
	 */
	public ConnectionEditPart createEditPart();
	
}
