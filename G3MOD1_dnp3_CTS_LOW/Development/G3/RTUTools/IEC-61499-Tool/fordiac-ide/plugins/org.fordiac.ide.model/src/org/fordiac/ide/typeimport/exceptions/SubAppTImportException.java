/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.typeimport.exceptions;

/**
 * The Class SubAppTImportException.
 */
public class SubAppTImportException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 904026625114781862L;

	/**
	 * Instantiates a new sub app t import exception.
	 * 
	 * @param arg0 the arg0
	 */
	public SubAppTImportException(final String arg0) {
		super(arg0);
	}

}
