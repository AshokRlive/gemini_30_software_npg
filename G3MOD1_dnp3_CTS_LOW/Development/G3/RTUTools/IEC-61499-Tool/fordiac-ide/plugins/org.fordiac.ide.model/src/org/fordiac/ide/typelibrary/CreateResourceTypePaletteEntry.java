package org.fordiac.ide.typelibrary;

import org.eclipse.core.resources.IFile;
import org.fordiac.ide.gef.IPaletteEntryCreator;
import org.fordiac.ide.model.Palette.PaletteFactory;
import org.fordiac.ide.model.Palette.ResourceTypeEntry;
import org.fordiac.ide.model.Palette.impl.PaletteEntryImpl;

public class CreateResourceTypePaletteEntry implements IPaletteEntryCreator, TypeLibraryTags {

	@Override
	public boolean canHandle(IFile file) {
		 if (RESOURCE_TYPE_FILE_ENDING.equalsIgnoreCase(file.getFileExtension())){
			 return true;
		 } else
			 return false;
	}

	@Override
	public PaletteEntryImpl createPaletteEntry() {
		
		ResourceTypeEntry entry = PaletteFactory.eINSTANCE.createResourceTypeEntry();
		
		return (PaletteEntryImpl) entry;
	}

}
