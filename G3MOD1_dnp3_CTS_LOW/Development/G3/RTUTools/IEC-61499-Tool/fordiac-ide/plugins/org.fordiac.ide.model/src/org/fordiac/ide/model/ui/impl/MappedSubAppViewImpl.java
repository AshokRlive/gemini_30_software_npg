/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mapped Sub App View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.MappedSubAppViewImpl#getInterfaceElements <em>Interface Elements</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.MappedSubAppViewImpl#getApplicationSubApp <em>Application Sub App</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.MappedSubAppViewImpl#getSubApp <em>Sub App</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MappedSubAppViewImpl extends ViewImpl implements MappedSubAppView {
	/**
	 * The cached value of the '{@link #getInterfaceElements() <em>Interface Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaceElements()
	 * @generated
	 * @ordered
	 */
	protected EList<MappedSubAppInterfaceElementView> interfaceElements;

	/**
	 * The cached value of the '{@link #getApplicationSubApp() <em>Application Sub App</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplicationSubApp()
	 * @generated
	 * @ordered
	 */
	protected SubAppView applicationSubApp;

	/**
	 * The cached value of the '{@link #getSubApp() <em>Sub App</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubApp()
	 * @generated
	 * @ordered
	 */
	protected SubApp subApp;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MappedSubAppViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.MAPPED_SUB_APP_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MappedSubAppInterfaceElementView> getInterfaceElements() {
		if (interfaceElements == null) {
			interfaceElements = new EObjectContainmentEList.Resolving<MappedSubAppInterfaceElementView>(MappedSubAppInterfaceElementView.class, this, UiPackage.MAPPED_SUB_APP_VIEW__INTERFACE_ELEMENTS);
		}
		return interfaceElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppView getApplicationSubApp() {
		if (applicationSubApp != null && applicationSubApp.eIsProxy()) {
			InternalEObject oldApplicationSubApp = (InternalEObject)applicationSubApp;
			applicationSubApp = (SubAppView)eResolveProxy(oldApplicationSubApp);
			if (applicationSubApp != oldApplicationSubApp) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP, oldApplicationSubApp, applicationSubApp));
			}
		}
		return applicationSubApp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppView basicGetApplicationSubApp() {
		return applicationSubApp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetApplicationSubApp(SubAppView newApplicationSubApp, NotificationChain msgs) {
		SubAppView oldApplicationSubApp = applicationSubApp;
		applicationSubApp = newApplicationSubApp;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP, oldApplicationSubApp, newApplicationSubApp);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicationSubApp(SubAppView newApplicationSubApp) {
		if (newApplicationSubApp != applicationSubApp) {
			NotificationChain msgs = null;
			if (applicationSubApp != null)
				msgs = ((InternalEObject)applicationSubApp).eInverseRemove(this, UiPackage.SUB_APP_VIEW__MAPPED_SUB_APP, SubAppView.class, msgs);
			if (newApplicationSubApp != null)
				msgs = ((InternalEObject)newApplicationSubApp).eInverseAdd(this, UiPackage.SUB_APP_VIEW__MAPPED_SUB_APP, SubAppView.class, msgs);
			msgs = basicSetApplicationSubApp(newApplicationSubApp, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP, newApplicationSubApp, newApplicationSubApp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubApp getSubApp() {
		if (subApp != null && subApp.eIsProxy()) {
			InternalEObject oldSubApp = (InternalEObject)subApp;
			subApp = (SubApp)eResolveProxy(oldSubApp);
			if (subApp != oldSubApp) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.MAPPED_SUB_APP_VIEW__SUB_APP, oldSubApp, subApp));
			}
		}
		return subApp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubApp basicGetSubApp() {
		return subApp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubApp(SubApp newSubApp) {
		SubApp oldSubApp = subApp;
		subApp = newSubApp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.MAPPED_SUB_APP_VIEW__SUB_APP, oldSubApp, subApp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP:
				if (applicationSubApp != null)
					msgs = ((InternalEObject)applicationSubApp).eInverseRemove(this, UiPackage.SUB_APP_VIEW__MAPPED_SUB_APP, SubAppView.class, msgs);
				return basicSetApplicationSubApp((SubAppView)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.MAPPED_SUB_APP_VIEW__INTERFACE_ELEMENTS:
				return ((InternalEList<?>)getInterfaceElements()).basicRemove(otherEnd, msgs);
			case UiPackage.MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP:
				return basicSetApplicationSubApp(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.MAPPED_SUB_APP_VIEW__INTERFACE_ELEMENTS:
				return getInterfaceElements();
			case UiPackage.MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP:
				if (resolve) return getApplicationSubApp();
				return basicGetApplicationSubApp();
			case UiPackage.MAPPED_SUB_APP_VIEW__SUB_APP:
				if (resolve) return getSubApp();
				return basicGetSubApp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.MAPPED_SUB_APP_VIEW__INTERFACE_ELEMENTS:
				getInterfaceElements().clear();
				getInterfaceElements().addAll((Collection<? extends MappedSubAppInterfaceElementView>)newValue);
				return;
			case UiPackage.MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP:
				setApplicationSubApp((SubAppView)newValue);
				return;
			case UiPackage.MAPPED_SUB_APP_VIEW__SUB_APP:
				setSubApp((SubApp)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.MAPPED_SUB_APP_VIEW__INTERFACE_ELEMENTS:
				getInterfaceElements().clear();
				return;
			case UiPackage.MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP:
				setApplicationSubApp((SubAppView)null);
				return;
			case UiPackage.MAPPED_SUB_APP_VIEW__SUB_APP:
				setSubApp((SubApp)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.MAPPED_SUB_APP_VIEW__INTERFACE_ELEMENTS:
				return interfaceElements != null && !interfaceElements.isEmpty();
			case UiPackage.MAPPED_SUB_APP_VIEW__APPLICATION_SUB_APP:
				return applicationSubApp != null;
			case UiPackage.MAPPED_SUB_APP_VIEW__SUB_APP:
				return subApp != null;
		}
		return super.eIsSet(featureID);
	}

} //MappedSubAppViewImpl
