/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.eclipse.emf.common.util.EList;
import org.fordiac.ide.model.libraryElement.Device;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Device View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.DeviceView#getDeviceElement <em>Device Element</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.DeviceView#isShowResorceList <em>Show Resorce List</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.DeviceView#getInterfaceElements <em>Interface Elements</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.DeviceView#getResourceContainerView <em>Resource Container View</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.DeviceView#getInConnections <em>In Connections</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getDeviceView()
 * @model
 * @generated
 */
public interface DeviceView extends View {
	/**
	 * Returns the value of the '<em><b>Device Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Device Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device Element</em>' reference.
	 * @see #setDeviceElement(Device)
	 * @see org.fordiac.ide.model.ui.UiPackage#getDeviceView_DeviceElement()
	 * @model
	 * @generated
	 */
	Device getDeviceElement();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.DeviceView#getDeviceElement <em>Device Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Device Element</em>' reference.
	 * @see #getDeviceElement()
	 * @generated
	 */
	void setDeviceElement(Device value);

	/**
	 * Returns the value of the '<em><b>Show Resorce List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Resorce List</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Resorce List</em>' attribute.
	 * @see #setShowResorceList(boolean)
	 * @see org.fordiac.ide.model.ui.UiPackage#getDeviceView_ShowResorceList()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isShowResorceList();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.DeviceView#isShowResorceList <em>Show Resorce List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Resorce List</em>' attribute.
	 * @see #isShowResorceList()
	 * @generated
	 */
	void setShowResorceList(boolean value);

	/**
	 * Returns the value of the '<em><b>Interface Elements</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.ui.InterfaceElementView}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface Elements</em>' containment reference list.
	 * @see org.fordiac.ide.model.ui.UiPackage#getDeviceView_InterfaceElements()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<InterfaceElementView> getInterfaceElements();

	/**
	 * Returns the value of the '<em><b>Resource Container View</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Container View</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Container View</em>' containment reference.
	 * @see #setResourceContainerView(ResourceContainerView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getDeviceView_ResourceContainerView()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	ResourceContainerView getResourceContainerView();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.DeviceView#getResourceContainerView <em>Resource Container View</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Container View</em>' containment reference.
	 * @see #getResourceContainerView()
	 * @generated
	 */
	void setResourceContainerView(ResourceContainerView value);

	/**
	 * Returns the value of the '<em><b>In Connections</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.ui.LinkView}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.LinkView#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Connections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Connections</em>' reference list.
	 * @see org.fordiac.ide.model.ui.UiPackage#getDeviceView_InConnections()
	 * @see org.fordiac.ide.model.ui.LinkView#getDestination
	 * @model opposite="destination"
	 * @generated
	 */
	EList<LinkView> getInConnections();

} // DeviceView
