/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.eclipse.emf.common.util.EList;
import org.fordiac.ide.model.libraryElement.Resource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.ResourceView#getResourceElement <em>Resource Element</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.ResourceView#getInterfaceElements <em>Interface Elements</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.ResourceView#isDeviceTypeResource <em>Device Type Resource</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.ResourceView#getUIResourceDiagram <em>UI Resource Diagram</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getResourceView()
 * @model
 * @generated
 */
public interface ResourceView extends View {
	/**
	 * Returns the value of the '<em><b>Resource Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Element</em>' reference.
	 * @see #setResourceElement(Resource)
	 * @see org.fordiac.ide.model.ui.UiPackage#getResourceView_ResourceElement()
	 * @model
	 * @generated
	 */
	Resource getResourceElement();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.ResourceView#getResourceElement <em>Resource Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Element</em>' reference.
	 * @see #getResourceElement()
	 * @generated
	 */
	void setResourceElement(Resource value);

	/**
	 * Returns the value of the '<em><b>Interface Elements</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.ui.InterfaceElementView}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface Elements</em>' containment reference list.
	 * @see org.fordiac.ide.model.ui.UiPackage#getResourceView_InterfaceElements()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<InterfaceElementView> getInterfaceElements();

	/**
	 * Returns the value of the '<em><b>Device Type Resource</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Device Type Resource</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device Type Resource</em>' attribute.
	 * @see #setDeviceTypeResource(boolean)
	 * @see org.fordiac.ide.model.ui.UiPackage#getResourceView_DeviceTypeResource()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isDeviceTypeResource();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.ResourceView#isDeviceTypeResource <em>Device Type Resource</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Device Type Resource</em>' attribute.
	 * @see #isDeviceTypeResource()
	 * @generated
	 */
	void setDeviceTypeResource(boolean value);

	/**
	 * Returns the value of the '<em><b>UI Resource Diagram</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>UI Resource Diagram</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UI Resource Diagram</em>' containment reference.
	 * @see #setUIResourceDiagram(UIResourceEditor)
	 * @see org.fordiac.ide.model.ui.UiPackage#getResourceView_UIResourceDiagram()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	UIResourceEditor getUIResourceDiagram();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.ResourceView#getUIResourceDiagram <em>UI Resource Diagram</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UI Resource Diagram</em>' containment reference.
	 * @see #getUIResourceDiagram()
	 * @generated
	 */
	void setUIResourceDiagram(UIResourceEditor value);

} // ResourceView
