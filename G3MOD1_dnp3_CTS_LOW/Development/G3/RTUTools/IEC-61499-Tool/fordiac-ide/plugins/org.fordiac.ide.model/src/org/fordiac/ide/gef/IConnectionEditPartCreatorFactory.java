package org.fordiac.ide.gef;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.ConnectionEditPart;

public interface IConnectionEditPartCreatorFactory {

	boolean supportsElement(Class<?> type);
	
	ConnectionEditPart getEditpart(String diagram, EObject element);
	
}
