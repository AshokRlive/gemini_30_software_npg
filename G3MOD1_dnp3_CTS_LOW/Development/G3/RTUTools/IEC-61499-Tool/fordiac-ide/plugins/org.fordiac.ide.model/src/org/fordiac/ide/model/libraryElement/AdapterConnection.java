/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Adapter Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.AdapterConnection#getSource <em>Source</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.AdapterConnection#getDestination <em>Destination</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getAdapterConnection()
 * @model
 * @generated
 */
public interface AdapterConnection extends Connection {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(AdapterDeclaration)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getAdapterConnection_Source()
	 * @model
	 * @generated
	 */
	AdapterDeclaration getSource();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.AdapterConnection#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(AdapterDeclaration value);

	/**
	 * Returns the value of the '<em><b>Destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination</em>' reference.
	 * @see #setDestination(AdapterDeclaration)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getAdapterConnection_Destination()
	 * @model
	 * @generated
	 */
	AdapterDeclaration getDestination();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.AdapterConnection#getDestination <em>Destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Destination</em>' reference.
	 * @see #getDestination()
	 * @generated
	 */
	void setDestination(AdapterDeclaration value);

} // AdapterConnection
