/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model;

import java.util.Hashtable;

import org.fordiac.ide.model.libraryElement.SubAppNetwork;

/**
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at
 *
 */
public class MappingManager {

	private static MappingManager instance;
	
	public static MappingManager getInstance() {
		if (instance == null)
			instance = new MappingManager();
		return instance;
	}
	
	// FBNetworks of Applications
	private Hashtable<String, SubAppNetwork> fBNetworks = new Hashtable<String, SubAppNetwork>();
	// FBNetworks of Resources
	private Hashtable<String, SubAppNetwork> resourcesFBNetworks = new Hashtable<String, SubAppNetwork>();
	
	
	public SubAppNetwork getResourceForId(String id) {
		return resourcesFBNetworks.get(id);
	}
	
	public SubAppNetwork getFBNetworkForId(String id) {
		return fBNetworks.get(id);
	}
	
	public void addFbFBNetworkPair(String fbID, SubAppNetwork fbn) {
		this.fBNetworks.put(fbID, fbn);
	}
	public void addFBResourcePair(String fbID, SubAppNetwork fbn) {
		this.resourcesFBNetworks.put(fbID, fbn);
		
	}
	
}
