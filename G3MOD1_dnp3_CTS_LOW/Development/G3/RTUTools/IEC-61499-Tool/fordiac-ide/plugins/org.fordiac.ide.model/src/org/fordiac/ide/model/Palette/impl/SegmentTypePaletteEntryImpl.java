/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.Palette.impl;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EClass;
import org.fordiac.ide.model.Activator;
import org.fordiac.ide.model.Palette.PalettePackage;
import org.fordiac.ide.model.Palette.SegmentTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.model.libraryElement.SegmentType;
import org.fordiac.ide.typeimport.SEGImporter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Segment Type Palette Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SegmentTypePaletteEntryImpl extends PaletteEntryImpl implements SegmentTypePaletteEntry {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SegmentTypePaletteEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PalettePackage.Literals.SEGMENT_TYPE_PALETTE_ENTRY;
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SegmentType getSegmentType() {
		LibraryElement type = getType();
		if((null !=type) && (type instanceof SegmentType)){
		   return (SegmentType) type;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(final LibraryElement type) {
		if((null != type) && (type instanceof SegmentType)){
			super.setType(type);
		}else{
			super.setType(null);
			if(null != type){
				Status exception = new Status(IStatus.ERROR, Activator.PLUGIN_ID, "tried to set no SegmentType as type entry for SegmentTypePaletteEntry");
				Activator.getDefault().getLog().log(exception);
			}
		}
	}

	protected LibraryElement loadType(){
		return SEGImporter.importSEGType(getFile());
	}
} //SegmentTypePaletteEntryImpl
