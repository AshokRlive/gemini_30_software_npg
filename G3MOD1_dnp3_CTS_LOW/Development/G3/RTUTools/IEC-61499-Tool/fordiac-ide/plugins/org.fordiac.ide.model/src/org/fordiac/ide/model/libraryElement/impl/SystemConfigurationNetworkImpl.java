/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.Link;
import org.fordiac.ide.model.libraryElement.Segment;
import org.fordiac.ide.model.libraryElement.SystemConfiguration;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>System Configuration Network</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SystemConfigurationNetworkImpl#getDevices <em>Devices</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SystemConfigurationNetworkImpl#getSegments <em>Segments</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SystemConfigurationNetworkImpl#getLinks <em>Links</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SystemConfigurationNetworkImpl#getSystemConfiguration <em>System Configuration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SystemConfigurationNetworkImpl extends I4DIACElementImpl implements SystemConfigurationNetwork {
	/**
	 * The cached value of the '{@link #getDevices() <em>Devices</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDevices()
	 * @generated
	 * @ordered
	 */
	protected EList<Device> devices;

	/**
	 * The cached value of the '{@link #getSegments() <em>Segments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegments()
	 * @generated
	 * @ordered
	 */
	protected EList<Segment> segments;

	/**
	 * The cached value of the '{@link #getLinks() <em>Links</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<Link> links;

	/**
	 * The cached value of the '{@link #getSystemConfiguration() <em>System Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemConfiguration()
	 * @generated
	 * @ordered
	 */
	protected SystemConfiguration systemConfiguration;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemConfigurationNetworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibraryElementPackage.Literals.SYSTEM_CONFIGURATION_NETWORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Device> getDevices() {
		if (devices == null) {
			devices = new EObjectContainmentWithInverseEList<Device>(Device.class, this, LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__DEVICES, LibraryElementPackage.DEVICE__SYSTEM_CONFIGURATION_NETWORK);
		}
		return devices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Segment> getSegments() {
		if (segments == null) {
			segments = new EObjectContainmentEList<Segment>(Segment.class, this, LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__SEGMENTS);
		}
		return segments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Link> getLinks() {
		if (links == null) {
			links = new EObjectContainmentEList<Link>(Link.class, this, LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__LINKS);
		}
		return links;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemConfiguration getSystemConfiguration() {
		return systemConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemConfiguration(SystemConfiguration newSystemConfiguration) {
		SystemConfiguration oldSystemConfiguration = systemConfiguration;
		systemConfiguration = newSystemConfiguration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__SYSTEM_CONFIGURATION, oldSystemConfiguration, systemConfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AutomationSystem getAutomationSystem() {
		return getSystemConfiguration().getAutomationSystem();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__DEVICES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDevices()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__DEVICES:
				return ((InternalEList<?>)getDevices()).basicRemove(otherEnd, msgs);
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__SEGMENTS:
				return ((InternalEList<?>)getSegments()).basicRemove(otherEnd, msgs);
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__LINKS:
				return ((InternalEList<?>)getLinks()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__DEVICES:
				return getDevices();
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__SEGMENTS:
				return getSegments();
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__LINKS:
				return getLinks();
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__SYSTEM_CONFIGURATION:
				return getSystemConfiguration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__DEVICES:
				getDevices().clear();
				getDevices().addAll((Collection<? extends Device>)newValue);
				return;
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__SEGMENTS:
				getSegments().clear();
				getSegments().addAll((Collection<? extends Segment>)newValue);
				return;
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__LINKS:
				getLinks().clear();
				getLinks().addAll((Collection<? extends Link>)newValue);
				return;
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__SYSTEM_CONFIGURATION:
				setSystemConfiguration((SystemConfiguration)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__DEVICES:
				getDevices().clear();
				return;
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__SEGMENTS:
				getSegments().clear();
				return;
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__LINKS:
				getLinks().clear();
				return;
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__SYSTEM_CONFIGURATION:
				setSystemConfiguration((SystemConfiguration)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__DEVICES:
				return devices != null && !devices.isEmpty();
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__SEGMENTS:
				return segments != null && !segments.isEmpty();
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__LINKS:
				return links != null && !links.isEmpty();
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__SYSTEM_CONFIGURATION:
				return systemConfiguration != null;
		}
		return super.eIsSet(featureID);
	}

} //SystemConfigurationNetworkImpl
