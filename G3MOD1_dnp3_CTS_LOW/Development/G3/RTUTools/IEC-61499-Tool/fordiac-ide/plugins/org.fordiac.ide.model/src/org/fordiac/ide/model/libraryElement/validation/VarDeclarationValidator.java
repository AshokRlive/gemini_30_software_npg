/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.validation;

import org.eclipse.emf.common.util.EList;
import org.fordiac.ide.model.data.DataType;
import org.fordiac.ide.model.data.VarInitialization;
import org.fordiac.ide.model.libraryElement.DataConnection;


/**
 * A sample validator interface for {@link org.fordiac.ide.model.libraryElement.VarDeclaration}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface VarDeclarationValidator {
	boolean validate();

	boolean validateArraySize(int value);

	boolean validateArraySize(String value);
	boolean validateComment(String value);
	boolean validateInitialValue(String value);
	boolean validateName(String value);
	boolean validateType(String value);

	boolean validateInputConnections(EList<DataConnection> value);

	boolean validateOutputConnections(EList<DataConnection> value);

	boolean validateType(DataType value);

	boolean validateVarInitialization(VarInitialization value);

	boolean validateTypeName(String value);
}
