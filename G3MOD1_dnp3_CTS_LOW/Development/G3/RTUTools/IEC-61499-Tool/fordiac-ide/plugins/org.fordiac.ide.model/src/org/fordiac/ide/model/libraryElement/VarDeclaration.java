/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.common.util.EList;
import org.fordiac.ide.model.data.DataType;
import org.fordiac.ide.model.data.VarInitialization;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Var Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.VarDeclaration#getArraySize <em>Array Size</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.VarDeclaration#getInputConnections <em>Input Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.VarDeclaration#getOutputConnections <em>Output Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.VarDeclaration#getType <em>Type</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.VarDeclaration#getVarInitialization <em>Var Initialization</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.VarDeclaration#getTypeName <em>Type Name</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.VarDeclaration#getWiths <em>Withs</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.VarDeclaration#isArray <em>Array</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getVarDeclaration()
 * @model
 * @generated
 */
public interface VarDeclaration extends IInterfaceElement {
	/**
	 * Returns the value of the '<em><b>Array Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Array Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Array Size</em>' attribute.
	 * @see #setArraySize(int)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getVarDeclaration_ArraySize()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='attribute' name='ArraySize'"
	 * @generated
	 */
	int getArraySize();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.VarDeclaration#getArraySize <em>Array Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Array Size</em>' attribute.
	 * @see #getArraySize()
	 * @generated
	 */
	void setArraySize(int value);

	/**
	 * Returns the value of the '<em><b>Input Connections</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.DataConnection}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.DataConnection#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Connections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Connections</em>' reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getVarDeclaration_InputConnections()
	 * @see org.fordiac.ide.model.libraryElement.DataConnection#getDestination
	 * @model opposite="destination"
	 * @generated
	 */
	EList<DataConnection> getInputConnections();

	/**
	 * Returns the value of the '<em><b>Output Connections</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.DataConnection}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.DataConnection#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Connections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Connections</em>' reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getVarDeclaration_OutputConnections()
	 * @see org.fordiac.ide.model.libraryElement.DataConnection#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<DataConnection> getOutputConnections();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(DataType)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getVarDeclaration_Type()
	 * @model required="true" transient="true"
	 * @generated
	 */
	DataType getType();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.VarDeclaration#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(DataType value);

	/**
	 * Returns the value of the '<em><b>Var Initialization</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Initialization</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Initialization</em>' containment reference.
	 * @see #setVarInitialization(VarInitialization)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getVarDeclaration_VarInitialization()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	VarInitialization getVarInitialization();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.VarDeclaration#getVarInitialization <em>Var Initialization</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Initialization</em>' containment reference.
	 * @see #getVarInitialization()
	 * @generated
	 */
	void setVarInitialization(VarInitialization value);

	/**
	 * Returns the value of the '<em><b>Type Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Name</em>' attribute.
	 * @see #setTypeName(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getVarDeclaration_TypeName()
	 * @model
	 * @generated
	 */
	String getTypeName();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.VarDeclaration#getTypeName <em>Type Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Name</em>' attribute.
	 * @see #getTypeName()
	 * @generated
	 */
	void setTypeName(String value);

	/**
	 * Returns the value of the '<em><b>Withs</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.With}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.With#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Withs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Withs</em>' reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getVarDeclaration_Withs()
	 * @see org.fordiac.ide.model.libraryElement.With#getVariables
	 * @model opposite="variables"
	 * @generated
	 */
	EList<With> getWiths();

	/**
	 * Returns the value of the '<em><b>Array</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Array</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Array</em>' attribute.
	 * @see #setArray(boolean)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getVarDeclaration_Array()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isArray();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.VarDeclaration#isArray <em>Array</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Array</em>' attribute.
	 * @see #isArray()
	 * @generated
	 */
	void setArray(boolean value);

} // VarDeclaration
