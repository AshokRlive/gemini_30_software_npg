/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource FB Network</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.ResourceFBNetwork#getMappedFBs <em>Mapped FBs</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.ResourceFBNetwork#getMappedSubApps <em>Mapped Sub Apps</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getResourceFBNetwork()
 * @model
 * @generated
 */
public interface ResourceFBNetwork extends FBNetwork {
	/**
	 * Returns the value of the '<em><b>Mapped FBs</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.FB}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.FB#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapped FBs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mapped FBs</em>' reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getResourceFBNetwork_MappedFBs()
	 * @see org.fordiac.ide.model.libraryElement.FB#getResource
	 * @model opposite="resource"
	 * @generated
	 */
	EList<FB> getMappedFBs();

	/**
	 * Returns the value of the '<em><b>Mapped Sub Apps</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.SubApp}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.SubApp#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapped Sub Apps</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mapped Sub Apps</em>' reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getResourceFBNetwork_MappedSubApps()
	 * @see org.fordiac.ide.model.libraryElement.SubApp#getResource
	 * @model opposite="resource"
	 * @generated
	 */
	EList<SubApp> getMappedSubApps();

} // ResourceFBNetwork
