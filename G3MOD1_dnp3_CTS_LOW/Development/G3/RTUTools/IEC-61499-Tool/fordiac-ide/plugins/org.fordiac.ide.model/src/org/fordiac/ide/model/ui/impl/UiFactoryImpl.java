/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.fordiac.ide.model.ui.*;
import org.fordiac.ide.model.ui.Color;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.LinkView;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.ResizeUIFBNetwork;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.SegmentView;
import org.fordiac.ide.model.ui.Size;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.model.ui.UiPackage;
import org.fordiac.ide.model.ui.View;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class UiFactoryImpl extends EFactoryImpl implements UiFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static UiFactory init() {
		try {
			UiFactory theUiFactory = (UiFactory)EPackage.Registry.INSTANCE.getEFactory(UiPackage.eNS_URI);
			if (theUiFactory != null) {
				return theUiFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new UiFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public UiFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> gebenh: required for backward compatibility <!--
	 * end-user-doc -->
	 */
	@Override
	public EObject create(final EClass eClass) {
		switch (eClass.getClassifierID()) {
		case UiPackage.ICONNECTION_VIEW:
			return createConnectionView();
		}
		return createGen(eClass);

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EObject createGen(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case UiPackage.VIEW: return createView();
			case UiPackage.UIFB_NETWORK: return createUIFBNetwork();
			case UiPackage.FB_VIEW: return createFBView();
			case UiPackage.POSITION: return createPosition();
			case UiPackage.INTERFACE_ELEMENT_VIEW: return createInterfaceElementView();
			case UiPackage.CONNECTION_VIEW: return createConnectionView();
			case UiPackage.SUB_APP_VIEW: return createSubAppView();
			case UiPackage.SUB_APP_INTERFACE_ELEMENT_VIEW: return createSubAppInterfaceElementView();
			case UiPackage.UI_SUB_APP_NETWORK: return createUISubAppNetwork();
			case UiPackage.INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW: return createInternalSubAppInterfaceElementView();
			case UiPackage.COLOR: return createColor();
			case UiPackage.UI_SYSTEM_CONFIGURATION: return createUISystemConfiguration();
			case UiPackage.DEVICE_VIEW: return createDeviceView();
			case UiPackage.RESOURCE_VIEW: return createResourceView();
			case UiPackage.RESOURCE_CONTAINER_VIEW: return createResourceContainerView();
			case UiPackage.UI_RESOURCE_EDITOR: return createUIResourceEditor();
			case UiPackage.RESIZE_UIFB_NETWORK: return createResizeUIFBNetwork();
			case UiPackage.SIZE: return createSize();
			case UiPackage.MAPPED_SUB_APP_VIEW: return createMappedSubAppView();
			case UiPackage.MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW: return createMappedSubAppInterfaceElementView();
			case UiPackage.SEGMENT_VIEW: return createSegmentView();
			case UiPackage.LINK_VIEW: return createLinkView();
			case UiPackage.COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW: return createCompositeInternalInterfaceElementView();
			case UiPackage.MONITORING_VIEW: return createMonitoringView();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public View createView() {
		ViewImpl view = new ViewImpl();
		return view;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public UIFBNetwork createUIFBNetwork() {
		UIFBNetworkImpl uifbNetwork = new UIFBNetworkImpl();
		return uifbNetwork;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public FBView createFBView() {
		FBViewImpl fbView = new FBViewImpl();
		return fbView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Position createPosition() {
		PositionImpl position = new PositionImpl();
		return position;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceElementView createInterfaceElementView() {
		InterfaceElementViewImpl interfaceElementView = new InterfaceElementViewImpl();
		return interfaceElementView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionView createConnectionView() {
		ConnectionViewImpl connectionView = new ConnectionViewImpl();
		return connectionView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppView createSubAppView() {
		SubAppViewImpl subAppView = new SubAppViewImpl();
		return subAppView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppInterfaceElementView createSubAppInterfaceElementView() {
		SubAppInterfaceElementViewImpl subAppInterfaceElementView = new SubAppInterfaceElementViewImpl();
		return subAppInterfaceElementView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public UISubAppNetwork createUISubAppNetwork() {
		UISubAppNetworkImpl uiSubAppNetwork = new UISubAppNetworkImpl();
		return uiSubAppNetwork;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public InternalSubAppInterfaceElementView createInternalSubAppInterfaceElementView() {
		InternalSubAppInterfaceElementViewImpl internalSubAppInterfaceElementView = new InternalSubAppInterfaceElementViewImpl();
		return internalSubAppInterfaceElementView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Color createColor() {
		ColorImpl color = new ColorImpl();
		return color;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public UISystemConfiguration createUISystemConfiguration() {
		UISystemConfigurationImpl uiSystemConfiguration = new UISystemConfigurationImpl();
		return uiSystemConfiguration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public DeviceView createDeviceView() {
		DeviceViewImpl deviceView = new DeviceViewImpl();
		return deviceView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceView createResourceView() {
		ResourceViewImpl resourceView = new ResourceViewImpl();
		return resourceView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceContainerView createResourceContainerView() {
		ResourceContainerViewImpl resourceContainerView = new ResourceContainerViewImpl();
		return resourceContainerView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public UIResourceEditor createUIResourceEditor() {
		UIResourceEditorImpl uiResourceEditor = new UIResourceEditorImpl();
		return uiResourceEditor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ResizeUIFBNetwork createResizeUIFBNetwork() {
		ResizeUIFBNetworkImpl resizeUIFBNetwork = new ResizeUIFBNetworkImpl();
		return resizeUIFBNetwork;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Size createSize() {
		SizeImpl size = new SizeImpl();
		return size;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MappedSubAppView createMappedSubAppView() {
		MappedSubAppViewImpl mappedSubAppView = new MappedSubAppViewImpl();
		return mappedSubAppView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MappedSubAppInterfaceElementView createMappedSubAppInterfaceElementView() {
		MappedSubAppInterfaceElementViewImpl mappedSubAppInterfaceElementView = new MappedSubAppInterfaceElementViewImpl();
		return mappedSubAppInterfaceElementView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SegmentView createSegmentView() {
		SegmentViewImpl segmentView = new SegmentViewImpl();
		return segmentView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LinkView createLinkView() {
		LinkViewImpl linkView = new LinkViewImpl();
		return linkView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeInternalInterfaceElementView createCompositeInternalInterfaceElementView() {
		CompositeInternalInterfaceElementViewImpl compositeInternalInterfaceElementView = new CompositeInternalInterfaceElementViewImpl();
		return compositeInternalInterfaceElementView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitoringView createMonitoringView() {
		MonitoringViewImpl monitoringView = new MonitoringViewImpl();
		return monitoringView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public UiPackage getUiPackage() {
		return (UiPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static UiPackage getPackage() {
		return UiPackage.eINSTANCE;
	}

} // UiFactoryImpl
