package org.fordiac.ide.gef;

import org.eclipse.core.resources.IFile;
import org.fordiac.ide.model.Palette.impl.PaletteEntryImpl;


/**
 *  Objects implementing this element can create a palette entry if the file type can be handled. 
 * 
 * @author eisenmenger
 *
 */
public interface IPaletteEntryCreator {

	/**
	 * Tests whether the file type can be handled.
	 * 
	 * @param file file type
	 * @return true if the file can handled and false if not.
	 */
	 boolean canHandle(IFile file);
	 

	/**
	 * Creates the palette entry
	 * 
	 * @return the created palette entry
	 */
	PaletteEntryImpl createPaletteEntry();

}
