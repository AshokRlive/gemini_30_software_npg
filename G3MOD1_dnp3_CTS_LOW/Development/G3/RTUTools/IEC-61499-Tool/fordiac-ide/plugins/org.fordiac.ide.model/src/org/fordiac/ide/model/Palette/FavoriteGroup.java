/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.Palette;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Favorite Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.Palette.FavoriteGroup#getFavoriteEntries <em>Favorite Entries</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.Palette.PalettePackage#getFavoriteGroup()
 * @model
 * @generated
 */
public interface FavoriteGroup extends PaletteGroup {
	/**
	 * Returns the value of the '<em><b>Favorite Entries</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.Palette.FavoriteEntry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Favorite Entries</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Favorite Entries</em>' containment reference list.
	 * @see org.fordiac.ide.model.Palette.PalettePackage#getFavoriteGroup_FavoriteEntries()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<FavoriteEntry> getFavoriteEntries();

} // FavoriteGroup
