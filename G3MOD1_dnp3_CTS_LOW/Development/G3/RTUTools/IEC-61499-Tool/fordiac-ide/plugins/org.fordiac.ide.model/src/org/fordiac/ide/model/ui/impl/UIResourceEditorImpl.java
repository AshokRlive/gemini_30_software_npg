/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.ui.ResizeUIFBNetwork;
import org.fordiac.ide.model.ui.Size;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;

import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UI Resource Editor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.UIResourceEditorImpl#getResourceElement <em>Resource Element</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.UIResourceEditorImpl#getFbNetworks <em>Fb Networks</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.UIResourceEditorImpl#isShowResizeUIFBNetworks <em>Show Resize UIFB Networks</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.UIResourceEditorImpl#getActiveNetwork <em>Active Network</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.UIResourceEditorImpl#getMappingEditorSize <em>Mapping Editor Size</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.UIResourceEditorImpl#getSystemConfiguration <em>System Configuration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UIResourceEditorImpl extends DiagramImpl implements UIResourceEditor {
	/**
	 * The cached value of the '{@link #getResourceElement() <em>Resource Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceElement()
	 * @generated
	 * @ordered
	 */
	protected Resource resourceElement;

	/**
	 * The cached value of the '{@link #getFbNetworks() <em>Fb Networks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFbNetworks()
	 * @generated
	 * @ordered
	 */
	protected EList<ResizeUIFBNetwork> fbNetworks;

	/**
	 * The default value of the '{@link #isShowResizeUIFBNetworks() <em>Show Resize UIFB Networks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowResizeUIFBNetworks()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SHOW_RESIZE_UIFB_NETWORKS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isShowResizeUIFBNetworks() <em>Show Resize UIFB Networks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowResizeUIFBNetworks()
	 * @generated
	 * @ordered
	 */
	protected boolean showResizeUIFBNetworks = SHOW_RESIZE_UIFB_NETWORKS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getActiveNetwork() <em>Active Network</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveNetwork()
	 * @generated
	 * @ordered
	 */
	protected ResizeUIFBNetwork activeNetwork;

	/**
	 * The cached value of the '{@link #getMappingEditorSize() <em>Mapping Editor Size</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMappingEditorSize()
	 * @generated
	 * @ordered
	 */
	protected Size mappingEditorSize;

	/**
	 * The cached value of the '{@link #getSystemConfiguration() <em>System Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemConfiguration()
	 * @generated
	 * @ordered
	 */
	protected UISystemConfiguration systemConfiguration;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UIResourceEditorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.UI_RESOURCE_EDITOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource getResourceElement() {
		if (resourceElement != null && resourceElement.eIsProxy()) {
			InternalEObject oldResourceElement = (InternalEObject)resourceElement;
			resourceElement = (Resource)eResolveProxy(oldResourceElement);
			if (resourceElement != oldResourceElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.UI_RESOURCE_EDITOR__RESOURCE_ELEMENT, oldResourceElement, resourceElement));
			}
		}
		return resourceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource basicGetResourceElement() {
		return resourceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourceElement(Resource newResourceElement) {
		Resource oldResourceElement = resourceElement;
		resourceElement = newResourceElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.UI_RESOURCE_EDITOR__RESOURCE_ELEMENT, oldResourceElement, resourceElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ResizeUIFBNetwork> getFbNetworks() {
		if (fbNetworks == null) {
			fbNetworks = new EObjectContainmentEList.Resolving<ResizeUIFBNetwork>(ResizeUIFBNetwork.class, this, UiPackage.UI_RESOURCE_EDITOR__FB_NETWORKS);
		}
		return fbNetworks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isShowResizeUIFBNetworks() {
		return showResizeUIFBNetworks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowResizeUIFBNetworks(boolean newShowResizeUIFBNetworks) {
		boolean oldShowResizeUIFBNetworks = showResizeUIFBNetworks;
		showResizeUIFBNetworks = newShowResizeUIFBNetworks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.UI_RESOURCE_EDITOR__SHOW_RESIZE_UIFB_NETWORKS, oldShowResizeUIFBNetworks, showResizeUIFBNetworks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResizeUIFBNetwork getActiveNetwork() {
		if (activeNetwork != null && activeNetwork.eIsProxy()) {
			InternalEObject oldActiveNetwork = (InternalEObject)activeNetwork;
			activeNetwork = (ResizeUIFBNetwork)eResolveProxy(oldActiveNetwork);
			if (activeNetwork != oldActiveNetwork) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.UI_RESOURCE_EDITOR__ACTIVE_NETWORK, oldActiveNetwork, activeNetwork));
			}
		}
		return activeNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResizeUIFBNetwork basicGetActiveNetwork() {
		return activeNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActiveNetwork(ResizeUIFBNetwork newActiveNetwork) {
		ResizeUIFBNetwork oldActiveNetwork = activeNetwork;
		activeNetwork = newActiveNetwork;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.UI_RESOURCE_EDITOR__ACTIVE_NETWORK, oldActiveNetwork, activeNetwork));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Size getMappingEditorSize() {
		if (mappingEditorSize != null && mappingEditorSize.eIsProxy()) {
			InternalEObject oldMappingEditorSize = (InternalEObject)mappingEditorSize;
			mappingEditorSize = (Size)eResolveProxy(oldMappingEditorSize);
			if (mappingEditorSize != oldMappingEditorSize) {
				InternalEObject newMappingEditorSize = (InternalEObject)mappingEditorSize;
				NotificationChain msgs = oldMappingEditorSize.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE, null, null);
				if (newMappingEditorSize.eInternalContainer() == null) {
					msgs = newMappingEditorSize.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE, oldMappingEditorSize, mappingEditorSize));
			}
		}
		return mappingEditorSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Size basicGetMappingEditorSize() {
		return mappingEditorSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMappingEditorSize(Size newMappingEditorSize, NotificationChain msgs) {
		Size oldMappingEditorSize = mappingEditorSize;
		mappingEditorSize = newMappingEditorSize;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE, oldMappingEditorSize, newMappingEditorSize);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMappingEditorSize(Size newMappingEditorSize) {
		if (newMappingEditorSize != mappingEditorSize) {
			NotificationChain msgs = null;
			if (mappingEditorSize != null)
				msgs = ((InternalEObject)mappingEditorSize).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE, null, msgs);
			if (newMappingEditorSize != null)
				msgs = ((InternalEObject)newMappingEditorSize).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE, null, msgs);
			msgs = basicSetMappingEditorSize(newMappingEditorSize, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE, newMappingEditorSize, newMappingEditorSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UISystemConfiguration getSystemConfiguration() {
		if (systemConfiguration != null && systemConfiguration.eIsProxy()) {
			InternalEObject oldSystemConfiguration = (InternalEObject)systemConfiguration;
			systemConfiguration = (UISystemConfiguration)eResolveProxy(oldSystemConfiguration);
			if (systemConfiguration != oldSystemConfiguration) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.UI_RESOURCE_EDITOR__SYSTEM_CONFIGURATION, oldSystemConfiguration, systemConfiguration));
			}
		}
		return systemConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UISystemConfiguration basicGetSystemConfiguration() {
		return systemConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemConfiguration(UISystemConfiguration newSystemConfiguration) {
		UISystemConfiguration oldSystemConfiguration = systemConfiguration;
		systemConfiguration = newSystemConfiguration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.UI_RESOURCE_EDITOR__SYSTEM_CONFIGURATION, oldSystemConfiguration, systemConfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.UI_RESOURCE_EDITOR__FB_NETWORKS:
				return ((InternalEList<?>)getFbNetworks()).basicRemove(otherEnd, msgs);
			case UiPackage.UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE:
				return basicSetMappingEditorSize(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.UI_RESOURCE_EDITOR__RESOURCE_ELEMENT:
				if (resolve) return getResourceElement();
				return basicGetResourceElement();
			case UiPackage.UI_RESOURCE_EDITOR__FB_NETWORKS:
				return getFbNetworks();
			case UiPackage.UI_RESOURCE_EDITOR__SHOW_RESIZE_UIFB_NETWORKS:
				return isShowResizeUIFBNetworks();
			case UiPackage.UI_RESOURCE_EDITOR__ACTIVE_NETWORK:
				if (resolve) return getActiveNetwork();
				return basicGetActiveNetwork();
			case UiPackage.UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE:
				if (resolve) return getMappingEditorSize();
				return basicGetMappingEditorSize();
			case UiPackage.UI_RESOURCE_EDITOR__SYSTEM_CONFIGURATION:
				if (resolve) return getSystemConfiguration();
				return basicGetSystemConfiguration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.UI_RESOURCE_EDITOR__RESOURCE_ELEMENT:
				setResourceElement((Resource)newValue);
				return;
			case UiPackage.UI_RESOURCE_EDITOR__FB_NETWORKS:
				getFbNetworks().clear();
				getFbNetworks().addAll((Collection<? extends ResizeUIFBNetwork>)newValue);
				return;
			case UiPackage.UI_RESOURCE_EDITOR__SHOW_RESIZE_UIFB_NETWORKS:
				setShowResizeUIFBNetworks((Boolean)newValue);
				return;
			case UiPackage.UI_RESOURCE_EDITOR__ACTIVE_NETWORK:
				setActiveNetwork((ResizeUIFBNetwork)newValue);
				return;
			case UiPackage.UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE:
				setMappingEditorSize((Size)newValue);
				return;
			case UiPackage.UI_RESOURCE_EDITOR__SYSTEM_CONFIGURATION:
				setSystemConfiguration((UISystemConfiguration)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.UI_RESOURCE_EDITOR__RESOURCE_ELEMENT:
				setResourceElement((Resource)null);
				return;
			case UiPackage.UI_RESOURCE_EDITOR__FB_NETWORKS:
				getFbNetworks().clear();
				return;
			case UiPackage.UI_RESOURCE_EDITOR__SHOW_RESIZE_UIFB_NETWORKS:
				setShowResizeUIFBNetworks(SHOW_RESIZE_UIFB_NETWORKS_EDEFAULT);
				return;
			case UiPackage.UI_RESOURCE_EDITOR__ACTIVE_NETWORK:
				setActiveNetwork((ResizeUIFBNetwork)null);
				return;
			case UiPackage.UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE:
				setMappingEditorSize((Size)null);
				return;
			case UiPackage.UI_RESOURCE_EDITOR__SYSTEM_CONFIGURATION:
				setSystemConfiguration((UISystemConfiguration)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.UI_RESOURCE_EDITOR__RESOURCE_ELEMENT:
				return resourceElement != null;
			case UiPackage.UI_RESOURCE_EDITOR__FB_NETWORKS:
				return fbNetworks != null && !fbNetworks.isEmpty();
			case UiPackage.UI_RESOURCE_EDITOR__SHOW_RESIZE_UIFB_NETWORKS:
				return showResizeUIFBNetworks != SHOW_RESIZE_UIFB_NETWORKS_EDEFAULT;
			case UiPackage.UI_RESOURCE_EDITOR__ACTIVE_NETWORK:
				return activeNetwork != null;
			case UiPackage.UI_RESOURCE_EDITOR__MAPPING_EDITOR_SIZE:
				return mappingEditorSize != null;
			case UiPackage.UI_RESOURCE_EDITOR__SYSTEM_CONFIGURATION:
				return systemConfiguration != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (showResizeUIFBNetworks: ");
		result.append(showResizeUIFBNetworks);
		result.append(')');
		return result.toString();
	}

	public SubAppNetwork getFunctionBlockNetwork() {
		return getResourceElement().getFBNetwork();
//		return null;
	}

	public SubAppNetwork getNetwork() {
		return getResourceElement().getFBNetwork();
//		return null;
	}

} //UIResourceEditorImpl
