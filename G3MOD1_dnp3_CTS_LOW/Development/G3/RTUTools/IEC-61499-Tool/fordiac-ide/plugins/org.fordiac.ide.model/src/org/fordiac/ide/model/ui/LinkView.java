/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.fordiac.ide.model.libraryElement.Link;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.LinkView#getSource <em>Source</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.LinkView#getLink <em>Link</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.LinkView#getDestination <em>Destination</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getLinkView()
 * @model
 * @generated
 */
public interface LinkView extends IConnectionView {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.SegmentView#getOutConnections <em>Out Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(SegmentView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getLinkView_Source()
	 * @see org.fordiac.ide.model.ui.SegmentView#getOutConnections
	 * @model opposite="outConnections"
	 * @generated
	 */
	SegmentView getSource();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.LinkView#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(SegmentView value);

	/**
	 * Returns the value of the '<em><b>Link</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link</em>' reference.
	 * @see #setLink(Link)
	 * @see org.fordiac.ide.model.ui.UiPackage#getLinkView_Link()
	 * @model
	 * @generated
	 */
	Link getLink();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.LinkView#getLink <em>Link</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Link</em>' reference.
	 * @see #getLink()
	 * @generated
	 */
	void setLink(Link value);

	/**
	 * Returns the value of the '<em><b>Destination</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.DeviceView#getInConnections <em>In Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination</em>' reference.
	 * @see #setDestination(DeviceView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getLinkView_Destination()
	 * @see org.fordiac.ide.model.ui.DeviceView#getInConnections
	 * @model opposite="inConnections"
	 * @generated
	 */
	DeviceView getDestination();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.LinkView#getDestination <em>Destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Destination</em>' reference.
	 * @see #getDestination()
	 * @generated
	 */
	void setDestination(DeviceView value);

} // LinkView
