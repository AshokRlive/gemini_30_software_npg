/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.fordiac.ide.model.ui.*;
import org.fordiac.ide.model.ui.Color;
import org.fordiac.ide.model.ui.CompositeInternalInterfaceElementView;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.IConnectionView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.LinkView;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.MonitoringView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.ResizeUIFBNetwork;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.SegmentView;
import org.fordiac.ide.model.ui.Size;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.UiPackage;
import org.fordiac.ide.model.ui.View;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.fordiac.ide.model.ui.UiPackage
 * @generated
 */
public class UiSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static UiPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiSwitch() {
		if (modelPackage == null) {
			modelPackage = UiPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case UiPackage.VIEW: {
				View view = (View)theEObject;
				T result = caseView(view);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.DIAGRAM: {
				Diagram diagram = (Diagram)theEObject;
				T result = caseDiagram(diagram);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.UIFB_NETWORK: {
				UIFBNetwork uifbNetwork = (UIFBNetwork)theEObject;
				T result = caseUIFBNetwork(uifbNetwork);
				if (result == null) result = caseDiagram(uifbNetwork);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.FB_VIEW: {
				FBView fbView = (FBView)theEObject;
				T result = caseFBView(fbView);
				if (result == null) result = caseView(fbView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.POSITION: {
				Position position = (Position)theEObject;
				T result = casePosition(position);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.INTERFACE_ELEMENT_VIEW: {
				InterfaceElementView interfaceElementView = (InterfaceElementView)theEObject;
				T result = caseInterfaceElementView(interfaceElementView);
				if (result == null) result = caseView(interfaceElementView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.CONNECTION_VIEW: {
				ConnectionView connectionView = (ConnectionView)theEObject;
				T result = caseConnectionView(connectionView);
				if (result == null) result = caseIConnectionView(connectionView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.SUB_APP_VIEW: {
				SubAppView subAppView = (SubAppView)theEObject;
				T result = caseSubAppView(subAppView);
				if (result == null) result = caseView(subAppView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.SUB_APP_INTERFACE_ELEMENT_VIEW: {
				SubAppInterfaceElementView subAppInterfaceElementView = (SubAppInterfaceElementView)theEObject;
				T result = caseSubAppInterfaceElementView(subAppInterfaceElementView);
				if (result == null) result = caseInterfaceElementView(subAppInterfaceElementView);
				if (result == null) result = caseView(subAppInterfaceElementView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.UI_SUB_APP_NETWORK: {
				UISubAppNetwork uiSubAppNetwork = (UISubAppNetwork)theEObject;
				T result = caseUISubAppNetwork(uiSubAppNetwork);
				if (result == null) result = caseDiagram(uiSubAppNetwork);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.INTERNAL_SUB_APP_INTERFACE_ELEMENT_VIEW: {
				InternalSubAppInterfaceElementView internalSubAppInterfaceElementView = (InternalSubAppInterfaceElementView)theEObject;
				T result = caseInternalSubAppInterfaceElementView(internalSubAppInterfaceElementView);
				if (result == null) result = caseSubAppInterfaceElementView(internalSubAppInterfaceElementView);
				if (result == null) result = caseInterfaceElementView(internalSubAppInterfaceElementView);
				if (result == null) result = caseView(internalSubAppInterfaceElementView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.COLOR: {
				Color color = (Color)theEObject;
				T result = caseColor(color);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.UI_SYSTEM_CONFIGURATION: {
				UISystemConfiguration uiSystemConfiguration = (UISystemConfiguration)theEObject;
				T result = caseUISystemConfiguration(uiSystemConfiguration);
				if (result == null) result = caseDiagram(uiSystemConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.DEVICE_VIEW: {
				DeviceView deviceView = (DeviceView)theEObject;
				T result = caseDeviceView(deviceView);
				if (result == null) result = caseView(deviceView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.RESOURCE_VIEW: {
				ResourceView resourceView = (ResourceView)theEObject;
				T result = caseResourceView(resourceView);
				if (result == null) result = caseView(resourceView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.RESOURCE_CONTAINER_VIEW: {
				ResourceContainerView resourceContainerView = (ResourceContainerView)theEObject;
				T result = caseResourceContainerView(resourceContainerView);
				if (result == null) result = caseView(resourceContainerView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.UI_RESOURCE_EDITOR: {
				UIResourceEditor uiResourceEditor = (UIResourceEditor)theEObject;
				T result = caseUIResourceEditor(uiResourceEditor);
				if (result == null) result = caseDiagram(uiResourceEditor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.RESIZE_UIFB_NETWORK: {
				ResizeUIFBNetwork resizeUIFBNetwork = (ResizeUIFBNetwork)theEObject;
				T result = caseResizeUIFBNetwork(resizeUIFBNetwork);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.SIZE: {
				Size size = (Size)theEObject;
				T result = caseSize(size);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.MAPPED_SUB_APP_VIEW: {
				MappedSubAppView mappedSubAppView = (MappedSubAppView)theEObject;
				T result = caseMappedSubAppView(mappedSubAppView);
				if (result == null) result = caseView(mappedSubAppView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.MAPPED_SUB_APP_INTERFACE_ELEMENT_VIEW: {
				MappedSubAppInterfaceElementView mappedSubAppInterfaceElementView = (MappedSubAppInterfaceElementView)theEObject;
				T result = caseMappedSubAppInterfaceElementView(mappedSubAppInterfaceElementView);
				if (result == null) result = caseSubAppInterfaceElementView(mappedSubAppInterfaceElementView);
				if (result == null) result = caseInterfaceElementView(mappedSubAppInterfaceElementView);
				if (result == null) result = caseView(mappedSubAppInterfaceElementView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.SEGMENT_VIEW: {
				SegmentView segmentView = (SegmentView)theEObject;
				T result = caseSegmentView(segmentView);
				if (result == null) result = caseView(segmentView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.LINK_VIEW: {
				LinkView linkView = (LinkView)theEObject;
				T result = caseLinkView(linkView);
				if (result == null) result = caseIConnectionView(linkView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.ICONNECTION_VIEW: {
				IConnectionView iConnectionView = (IConnectionView)theEObject;
				T result = caseIConnectionView(iConnectionView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.COMPOSITE_INTERNAL_INTERFACE_ELEMENT_VIEW: {
				CompositeInternalInterfaceElementView compositeInternalInterfaceElementView = (CompositeInternalInterfaceElementView)theEObject;
				T result = caseCompositeInternalInterfaceElementView(compositeInternalInterfaceElementView);
				if (result == null) result = caseInterfaceElementView(compositeInternalInterfaceElementView);
				if (result == null) result = caseView(compositeInternalInterfaceElementView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UiPackage.MONITORING_VIEW: {
				MonitoringView monitoringView = (MonitoringView)theEObject;
				T result = caseMonitoringView(monitoringView);
				if (result == null) result = caseView(monitoringView);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseView(View object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Diagram</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Diagram</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiagram(Diagram object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UIFB Network</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UIFB Network</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUIFBNetwork(UIFBNetwork object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FB View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FB View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFBView(FBView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Position</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Position</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePosition(Position object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interface Element View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interface Element View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterfaceElementView(InterfaceElementView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionView(ConnectionView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sub App View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sub App View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubAppView(SubAppView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sub App Interface Element View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sub App Interface Element View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubAppInterfaceElementView(SubAppInterfaceElementView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UI Sub App Network</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UI Sub App Network</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUISubAppNetwork(UISubAppNetwork object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Internal Sub App Interface Element View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Internal Sub App Interface Element View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInternalSubAppInterfaceElementView(InternalSubAppInterfaceElementView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Color</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Color</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseColor(Color object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UI System Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UI System Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUISystemConfiguration(UISystemConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Device View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Device View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeviceView(DeviceView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceView(ResourceView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Container View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Container View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceContainerView(ResourceContainerView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UI Resource Editor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UI Resource Editor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUIResourceEditor(UIResourceEditor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resize UIFB Network</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resize UIFB Network</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResizeUIFBNetwork(ResizeUIFBNetwork object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Size</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Size</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSize(Size object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mapped Sub App View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mapped Sub App View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMappedSubAppView(MappedSubAppView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mapped Sub App Interface Element View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mapped Sub App Interface Element View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMappedSubAppInterfaceElementView(MappedSubAppInterfaceElementView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Segment View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Segment View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSegmentView(SegmentView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Link View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Link View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLinkView(LinkView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IConnection View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IConnection View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIConnectionView(IConnectionView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Internal Interface Element View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Internal Interface Element View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompositeInternalInterfaceElementView(CompositeInternalInterfaceElementView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Monitoring View</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Monitoring View</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMonitoringView(MonitoringView object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //UiSwitch
