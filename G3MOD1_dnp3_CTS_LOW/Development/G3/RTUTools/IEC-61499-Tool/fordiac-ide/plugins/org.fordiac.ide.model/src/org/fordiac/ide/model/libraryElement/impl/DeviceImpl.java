/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.Palette.DeviceTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.DeviceType;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.Link;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Device</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.DeviceImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.DeviceImpl#getFBNetwork <em>FB Network</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.DeviceImpl#getX <em>X</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.DeviceImpl#getY <em>Y</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.DeviceImpl#getVarDeclarations <em>Var Declarations</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.DeviceImpl#getProfile <em>Profile</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.DeviceImpl#getInConnections <em>In Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.DeviceImpl#getTypePath <em>Type Path</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.DeviceImpl#getSystemConfigurationNetwork <em>System Configuration Network</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeviceImpl extends TypedConfigureableObjectImpl implements Device {
	/**
	 * The cached value of the '{@link #getResource() <em>Resource</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getResource()
	 * @generated
	 * @ordered
	 */
	protected EList<Resource> resource;

	/**
	 * The cached value of the '{@link #getFBNetwork() <em>FB Network</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getFBNetwork()
	 * @generated
	 * @ordered
	 */
	protected FBNetwork fBNetwork;

	/**
	 * The default value of the '{@link #getX() <em>X</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getX()
	 * @generated
	 * @ordered
	 */
	protected static final String X_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getX() <em>X</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getX()
	 * @generated
	 * @ordered
	 */
	protected String x = X_EDEFAULT;

	/**
	 * The default value of the '{@link #getY() <em>Y</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getY()
	 * @generated
	 * @ordered
	 */
	protected static final String Y_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getY() <em>Y</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getY()
	 * @generated
	 * @ordered
	 */
	protected String y = Y_EDEFAULT;

	/**
	 * The cached value of the '{@link #getVarDeclarations() <em>Var Declarations</em>}' containment reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getVarDeclarations()
	 * @generated
	 * @ordered
	 */
	protected EList<VarDeclaration> varDeclarations;

	/**
	 * The default value of the '{@link #getProfile() <em>Profile</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getProfile()
	 * @generated
	 * @ordered
	 */
	protected static final String PROFILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProfile() <em>Profile</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getProfile()
	 * @generated
	 * @ordered
	 */
	protected String profile = PROFILE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInConnections() <em>In Connections</em>}' reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getInConnections()
	 * @generated
	 * @ordered
	 */
	protected EList<Link> inConnections;

	/**
	 * The default value of the '{@link #getTypePath() <em>Type Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypePath()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypePath() <em>Type Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypePath()
	 * @generated
	 * @ordered
	 */
	protected String typePath = TYPE_PATH_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected DeviceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibraryElementPackage.Literals.DEVICE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Resource> getResource() {
		if (resource == null) {
			resource = new EObjectContainmentWithInverseEList<Resource>(Resource.class, this, LibraryElementPackage.DEVICE__RESOURCE, LibraryElementPackage.RESOURCE__DEVICE);
		}
		return resource;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public FBNetwork getFBNetwork() {
		return fBNetwork;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFBNetwork(FBNetwork newFBNetwork,
			NotificationChain msgs) {
		FBNetwork oldFBNetwork = fBNetwork;
		fBNetwork = newFBNetwork;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LibraryElementPackage.DEVICE__FB_NETWORK, oldFBNetwork, newFBNetwork);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setFBNetwork(FBNetwork newFBNetwork) {
		if (newFBNetwork != fBNetwork) {
			NotificationChain msgs = null;
			if (fBNetwork != null)
				msgs = ((InternalEObject)fBNetwork).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LibraryElementPackage.DEVICE__FB_NETWORK, null, msgs);
			if (newFBNetwork != null)
				msgs = ((InternalEObject)newFBNetwork).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LibraryElementPackage.DEVICE__FB_NETWORK, null, msgs);
			msgs = basicSetFBNetwork(newFBNetwork, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.DEVICE__FB_NETWORK, newFBNetwork, newFBNetwork));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getX() {
		return x;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setX(String newX) {
		String oldX = x;
		x = newX;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.DEVICE__X, oldX, x));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getY() {
		return y;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setY(String newY) {
		String oldY = y;
		y = newY;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.DEVICE__Y, oldY, y));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VarDeclaration> getVarDeclarations() {
		if (varDeclarations == null) {
			varDeclarations = new EObjectContainmentEList.Resolving<VarDeclaration>(VarDeclaration.class, this, LibraryElementPackage.DEVICE__VAR_DECLARATIONS);
		}
		return varDeclarations;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getProfile() {
		return profile;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setProfile(String newProfile) {
		String oldProfile = profile;
		profile = newProfile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.DEVICE__PROFILE, oldProfile, profile));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Link> getInConnections() {
		if (inConnections == null) {
			inConnections = new EObjectWithInverseResolvingEList<Link>(Link.class, this, LibraryElementPackage.DEVICE__IN_CONNECTIONS, LibraryElementPackage.LINK__DEVICE);
		}
		return inConnections;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTypePath() {
		return typePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypePath(String newTypePath) {
		String oldTypePath = typePath;
		typePath = newTypePath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.DEVICE__TYPE_PATH, oldTypePath, typePath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemConfigurationNetwork getSystemConfigurationNetwork() {
		if (eContainerFeatureID() != LibraryElementPackage.DEVICE__SYSTEM_CONFIGURATION_NETWORK) return null;
		return (SystemConfigurationNetwork)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemConfigurationNetwork basicGetSystemConfigurationNetwork() {
		if (eContainerFeatureID() != LibraryElementPackage.DEVICE__SYSTEM_CONFIGURATION_NETWORK) return null;
		return (SystemConfigurationNetwork)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSystemConfigurationNetwork(SystemConfigurationNetwork newSystemConfigurationNetwork, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newSystemConfigurationNetwork, LibraryElementPackage.DEVICE__SYSTEM_CONFIGURATION_NETWORK, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemConfigurationNetwork(SystemConfigurationNetwork newSystemConfigurationNetwork) {
		if (newSystemConfigurationNetwork != eInternalContainer() || (eContainerFeatureID() != LibraryElementPackage.DEVICE__SYSTEM_CONFIGURATION_NETWORK && newSystemConfigurationNetwork != null)) {
			if (EcoreUtil.isAncestor(this, newSystemConfigurationNetwork))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSystemConfigurationNetwork != null)
				msgs = ((InternalEObject)newSystemConfigurationNetwork).eInverseAdd(this, LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__DEVICES, SystemConfigurationNetwork.class, msgs);
			msgs = basicSetSystemConfigurationNetwork(newSystemConfigurationNetwork, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.DEVICE__SYSTEM_CONFIGURATION_NETWORK, newSystemConfigurationNetwork, newSystemConfigurationNetwork));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AutomationSystem getAutomationSystem() {
		return getSystemConfigurationNetwork().getAutomationSystem();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID,
			NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.DEVICE__RESOURCE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getResource()).basicAdd(otherEnd, msgs);
			case LibraryElementPackage.DEVICE__IN_CONNECTIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInConnections()).basicAdd(otherEnd, msgs);
			case LibraryElementPackage.DEVICE__SYSTEM_CONFIGURATION_NETWORK:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetSystemConfigurationNetwork((SystemConfigurationNetwork)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.DEVICE__RESOURCE:
				return ((InternalEList<?>)getResource()).basicRemove(otherEnd, msgs);
			case LibraryElementPackage.DEVICE__FB_NETWORK:
				return basicSetFBNetwork(null, msgs);
			case LibraryElementPackage.DEVICE__VAR_DECLARATIONS:
				return ((InternalEList<?>)getVarDeclarations()).basicRemove(otherEnd, msgs);
			case LibraryElementPackage.DEVICE__IN_CONNECTIONS:
				return ((InternalEList<?>)getInConnections()).basicRemove(otherEnd, msgs);
			case LibraryElementPackage.DEVICE__SYSTEM_CONFIGURATION_NETWORK:
				return basicSetSystemConfigurationNetwork(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case LibraryElementPackage.DEVICE__SYSTEM_CONFIGURATION_NETWORK:
				return eInternalContainer().eInverseRemove(this, LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK__DEVICES, SystemConfigurationNetwork.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibraryElementPackage.DEVICE__RESOURCE:
				return getResource();
			case LibraryElementPackage.DEVICE__FB_NETWORK:
				return getFBNetwork();
			case LibraryElementPackage.DEVICE__X:
				return getX();
			case LibraryElementPackage.DEVICE__Y:
				return getY();
			case LibraryElementPackage.DEVICE__VAR_DECLARATIONS:
				return getVarDeclarations();
			case LibraryElementPackage.DEVICE__PROFILE:
				return getProfile();
			case LibraryElementPackage.DEVICE__IN_CONNECTIONS:
				return getInConnections();
			case LibraryElementPackage.DEVICE__TYPE_PATH:
				return getTypePath();
			case LibraryElementPackage.DEVICE__SYSTEM_CONFIGURATION_NETWORK:
				if (resolve) return getSystemConfigurationNetwork();
				return basicGetSystemConfigurationNetwork();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibraryElementPackage.DEVICE__RESOURCE:
				getResource().clear();
				getResource().addAll((Collection<? extends Resource>)newValue);
				return;
			case LibraryElementPackage.DEVICE__FB_NETWORK:
				setFBNetwork((FBNetwork)newValue);
				return;
			case LibraryElementPackage.DEVICE__X:
				setX((String)newValue);
				return;
			case LibraryElementPackage.DEVICE__Y:
				setY((String)newValue);
				return;
			case LibraryElementPackage.DEVICE__VAR_DECLARATIONS:
				getVarDeclarations().clear();
				getVarDeclarations().addAll((Collection<? extends VarDeclaration>)newValue);
				return;
			case LibraryElementPackage.DEVICE__PROFILE:
				setProfile((String)newValue);
				return;
			case LibraryElementPackage.DEVICE__IN_CONNECTIONS:
				getInConnections().clear();
				getInConnections().addAll((Collection<? extends Link>)newValue);
				return;
			case LibraryElementPackage.DEVICE__TYPE_PATH:
				setTypePath((String)newValue);
				return;
			case LibraryElementPackage.DEVICE__SYSTEM_CONFIGURATION_NETWORK:
				setSystemConfigurationNetwork((SystemConfigurationNetwork)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.DEVICE__RESOURCE:
				getResource().clear();
				return;
			case LibraryElementPackage.DEVICE__FB_NETWORK:
				setFBNetwork((FBNetwork)null);
				return;
			case LibraryElementPackage.DEVICE__X:
				setX(X_EDEFAULT);
				return;
			case LibraryElementPackage.DEVICE__Y:
				setY(Y_EDEFAULT);
				return;
			case LibraryElementPackage.DEVICE__VAR_DECLARATIONS:
				getVarDeclarations().clear();
				return;
			case LibraryElementPackage.DEVICE__PROFILE:
				setProfile(PROFILE_EDEFAULT);
				return;
			case LibraryElementPackage.DEVICE__IN_CONNECTIONS:
				getInConnections().clear();
				return;
			case LibraryElementPackage.DEVICE__TYPE_PATH:
				setTypePath(TYPE_PATH_EDEFAULT);
				return;
			case LibraryElementPackage.DEVICE__SYSTEM_CONFIGURATION_NETWORK:
				setSystemConfigurationNetwork((SystemConfigurationNetwork)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.DEVICE__RESOURCE:
				return resource != null && !resource.isEmpty();
			case LibraryElementPackage.DEVICE__FB_NETWORK:
				return fBNetwork != null;
			case LibraryElementPackage.DEVICE__X:
				return X_EDEFAULT == null ? x != null : !X_EDEFAULT.equals(x);
			case LibraryElementPackage.DEVICE__Y:
				return Y_EDEFAULT == null ? y != null : !Y_EDEFAULT.equals(y);
			case LibraryElementPackage.DEVICE__VAR_DECLARATIONS:
				return varDeclarations != null && !varDeclarations.isEmpty();
			case LibraryElementPackage.DEVICE__PROFILE:
				return PROFILE_EDEFAULT == null ? profile != null : !PROFILE_EDEFAULT.equals(profile);
			case LibraryElementPackage.DEVICE__IN_CONNECTIONS:
				return inConnections != null && !inConnections.isEmpty();
			case LibraryElementPackage.DEVICE__TYPE_PATH:
				return TYPE_PATH_EDEFAULT == null ? typePath != null : !TYPE_PATH_EDEFAULT.equals(typePath);
			case LibraryElementPackage.DEVICE__SYSTEM_CONFIGURATION_NETWORK:
				return basicGetSystemConfigurationNetwork() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (x: ");
		result.append(x);
		result.append(", y: ");
		result.append(y);
		result.append(", profile: ");
		result.append(profile);
		result.append(", typePath: ");
		result.append(typePath);
		result.append(')');
		return result.toString();
	}

	@Override
	public void setName(String newName) {
		if (newName.equals(name)) {
			return;
		}
		String oldName = name;
		
		//TODO consider moving this to a rename device command, however then also the device create command needs to be adjusted
		name = NameRepository.getUniqueDeviceInstanceName(this, newName);			

		NameRepository.checkNameIdentifier(this);
		
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET,
					LibraryElementPackage.ID_OBJECT__NAME, oldName, name));
		}
	}

	public Resource getResourceForName(String name) {
		for (Resource res : getResource()) {			
			if (res.getName().equals(name)) {
				return res;
			}
		}
		return null;
	}
	
	public DeviceType getDeviceType(){
		DeviceType retVal = null;
		if(null != getPaletteEntry()){
			if(getPaletteEntry() instanceof DeviceTypePaletteEntry){
			   retVal = ((DeviceTypePaletteEntry)getPaletteEntry()).getDeviceType();	
			}				
		}
		return retVal;
	}
} // DeviceImpl
