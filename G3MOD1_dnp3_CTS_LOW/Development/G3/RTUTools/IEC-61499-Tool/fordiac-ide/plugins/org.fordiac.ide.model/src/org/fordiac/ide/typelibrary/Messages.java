/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.typelibrary;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.typelibrary.messages"; //$NON-NLS-1$

	public static String TypeLibrary_ImportDataTypeFileDialogTitle;
	
	public static String TypeLibrary_LoadReferencedFile_DialogTitle;
	
	public static String TypeLibrary_OverwriteMessage;
	
	public static String TypeLibrary_ImportAbortByUser;

	public static String TypeLibrary_FBTImportException;
	
	public static String TypeLibrary_ERROR_ReferencedDataTypeNotFound;
	
	public static String TypeLibrary_ERROR_ReferencedTypeNotFound;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
