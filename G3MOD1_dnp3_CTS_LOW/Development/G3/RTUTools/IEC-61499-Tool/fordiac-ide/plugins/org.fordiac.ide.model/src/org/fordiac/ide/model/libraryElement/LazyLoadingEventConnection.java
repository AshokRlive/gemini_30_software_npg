/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lazy Loading Event Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.LazyLoadingEventConnection#getSourceName <em>Source Name</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.LazyLoadingEventConnection#getDestName <em>Dest Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getLazyLoadingEventConnection()
 * @model
 * @generated
 */
public interface LazyLoadingEventConnection extends EventConnection {
	/**
	 * Returns the value of the '<em><b>Source Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Name</em>' attribute.
	 * @see #setSourceName(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getLazyLoadingEventConnection_SourceName()
	 * @model
	 * @generated
	 */
	String getSourceName();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.LazyLoadingEventConnection#getSourceName <em>Source Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Name</em>' attribute.
	 * @see #getSourceName()
	 * @generated
	 */
	void setSourceName(String value);

	/**
	 * Returns the value of the '<em><b>Dest Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dest Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dest Name</em>' attribute.
	 * @see #setDestName(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getLazyLoadingEventConnection_DestName()
	 * @model
	 * @generated
	 */
	String getDestName();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.LazyLoadingEventConnection#getDestName <em>Dest Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dest Name</em>' attribute.
	 * @see #getDestName()
	 * @generated
	 */
	void setDestName(String value);

} // LazyLoadingEventConnection
