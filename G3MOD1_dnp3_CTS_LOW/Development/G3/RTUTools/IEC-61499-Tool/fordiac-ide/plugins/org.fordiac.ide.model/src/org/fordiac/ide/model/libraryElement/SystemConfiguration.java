/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SystemConfiguration#getSystemConfigurationNetwork <em>System Configuration Network</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SystemConfiguration#getAutomationSystem <em>Automation System</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSystemConfiguration()
 * @model
 * @generated
 */
public interface SystemConfiguration extends I4DIACElement {
	/**
	 * Returns the value of the '<em><b>System Configuration Network</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Configuration Network</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Configuration Network</em>' reference.
	 * @see #setSystemConfigurationNetwork(SystemConfigurationNetwork)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSystemConfiguration_SystemConfigurationNetwork()
	 * @model
	 * @generated
	 */
	SystemConfigurationNetwork getSystemConfigurationNetwork();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.SystemConfiguration#getSystemConfigurationNetwork <em>System Configuration Network</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Configuration Network</em>' reference.
	 * @see #getSystemConfigurationNetwork()
	 * @generated
	 */
	void setSystemConfigurationNetwork(SystemConfigurationNetwork value);

	/**
	 * Returns the value of the '<em><b>Automation System</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.AutomationSystem#getSystemConfiguration <em>System Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Automation System</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Automation System</em>' container reference.
	 * @see #setAutomationSystem(AutomationSystem)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSystemConfiguration_AutomationSystem()
	 * @see org.fordiac.ide.model.libraryElement.AutomationSystem#getSystemConfiguration
	 * @model opposite="systemConfiguration"
	 * @generated
	 */
	AutomationSystem getAutomationSystem();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.SystemConfiguration#getAutomationSystem <em>Automation System</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Automation System</em>' container reference.
	 * @see #getAutomationSystem()
	 * @generated
	 */
	void setAutomationSystem(AutomationSystem value);

} // SystemConfiguration
