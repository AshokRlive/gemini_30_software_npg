/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.fordiac.ide.model.libraryElement.Device;

import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.LinkView;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Device View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.DeviceViewImpl#getDeviceElement <em>Device Element</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.DeviceViewImpl#isShowResorceList <em>Show Resorce List</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.DeviceViewImpl#getInterfaceElements <em>Interface Elements</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.DeviceViewImpl#getResourceContainerView <em>Resource Container View</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.DeviceViewImpl#getInConnections <em>In Connections</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeviceViewImpl extends ViewImpl implements DeviceView {
	/**
	 * The cached value of the '{@link #getDeviceElement() <em>Device Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceElement()
	 * @generated
	 * @ordered
	 */
	protected Device deviceElement;

	/**
	 * The default value of the '{@link #isShowResorceList() <em>Show Resorce List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowResorceList()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SHOW_RESORCE_LIST_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isShowResorceList() <em>Show Resorce List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowResorceList()
	 * @generated
	 * @ordered
	 */
	protected boolean showResorceList = SHOW_RESORCE_LIST_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInterfaceElements() <em>Interface Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaceElements()
	 * @generated
	 * @ordered
	 */
	protected EList<InterfaceElementView> interfaceElements;

	/**
	 * The cached value of the '{@link #getResourceContainerView() <em>Resource Container View</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceContainerView()
	 * @generated
	 * @ordered
	 */
	protected ResourceContainerView resourceContainerView;

	/**
	 * The cached value of the '{@link #getInConnections() <em>In Connections</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInConnections()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkView> inConnections;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeviceViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.DEVICE_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Device getDeviceElement() {
		if (deviceElement != null && deviceElement.eIsProxy()) {
			InternalEObject oldDeviceElement = (InternalEObject)deviceElement;
			deviceElement = (Device)eResolveProxy(oldDeviceElement);
			if (deviceElement != oldDeviceElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.DEVICE_VIEW__DEVICE_ELEMENT, oldDeviceElement, deviceElement));
			}
		}
		return deviceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Device basicGetDeviceElement() {
		return deviceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeviceElement(Device newDeviceElement) {
		Device oldDeviceElement = deviceElement;
		deviceElement = newDeviceElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.DEVICE_VIEW__DEVICE_ELEMENT, oldDeviceElement, deviceElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isShowResorceList() {
		return showResorceList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowResorceList(boolean newShowResorceList) {
		boolean oldShowResorceList = showResorceList;
		showResorceList = newShowResorceList;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.DEVICE_VIEW__SHOW_RESORCE_LIST, oldShowResorceList, showResorceList));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterfaceElementView> getInterfaceElements() {
		if (interfaceElements == null) {
			interfaceElements = new EObjectContainmentEList.Resolving<InterfaceElementView>(InterfaceElementView.class, this, UiPackage.DEVICE_VIEW__INTERFACE_ELEMENTS);
		}
		return interfaceElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceContainerView getResourceContainerView() {
		if (resourceContainerView != null && resourceContainerView.eIsProxy()) {
			InternalEObject oldResourceContainerView = (InternalEObject)resourceContainerView;
			resourceContainerView = (ResourceContainerView)eResolveProxy(oldResourceContainerView);
			if (resourceContainerView != oldResourceContainerView) {
				InternalEObject newResourceContainerView = (InternalEObject)resourceContainerView;
				NotificationChain msgs = oldResourceContainerView.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.DEVICE_VIEW__RESOURCE_CONTAINER_VIEW, null, null);
				if (newResourceContainerView.eInternalContainer() == null) {
					msgs = newResourceContainerView.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.DEVICE_VIEW__RESOURCE_CONTAINER_VIEW, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.DEVICE_VIEW__RESOURCE_CONTAINER_VIEW, oldResourceContainerView, resourceContainerView));
			}
		}
		return resourceContainerView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceContainerView basicGetResourceContainerView() {
		return resourceContainerView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResourceContainerView(ResourceContainerView newResourceContainerView, NotificationChain msgs) {
		ResourceContainerView oldResourceContainerView = resourceContainerView;
		resourceContainerView = newResourceContainerView;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.DEVICE_VIEW__RESOURCE_CONTAINER_VIEW, oldResourceContainerView, newResourceContainerView);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourceContainerView(ResourceContainerView newResourceContainerView) {
		if (newResourceContainerView != resourceContainerView) {
			NotificationChain msgs = null;
			if (resourceContainerView != null)
				msgs = ((InternalEObject)resourceContainerView).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.DEVICE_VIEW__RESOURCE_CONTAINER_VIEW, null, msgs);
			if (newResourceContainerView != null)
				msgs = ((InternalEObject)newResourceContainerView).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.DEVICE_VIEW__RESOURCE_CONTAINER_VIEW, null, msgs);
			msgs = basicSetResourceContainerView(newResourceContainerView, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.DEVICE_VIEW__RESOURCE_CONTAINER_VIEW, newResourceContainerView, newResourceContainerView));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkView> getInConnections() {
		if (inConnections == null) {
			inConnections = new EObjectWithInverseResolvingEList<LinkView>(LinkView.class, this, UiPackage.DEVICE_VIEW__IN_CONNECTIONS, UiPackage.LINK_VIEW__DESTINATION);
		}
		return inConnections;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.DEVICE_VIEW__IN_CONNECTIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInConnections()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.DEVICE_VIEW__INTERFACE_ELEMENTS:
				return ((InternalEList<?>)getInterfaceElements()).basicRemove(otherEnd, msgs);
			case UiPackage.DEVICE_VIEW__RESOURCE_CONTAINER_VIEW:
				return basicSetResourceContainerView(null, msgs);
			case UiPackage.DEVICE_VIEW__IN_CONNECTIONS:
				return ((InternalEList<?>)getInConnections()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.DEVICE_VIEW__DEVICE_ELEMENT:
				if (resolve) return getDeviceElement();
				return basicGetDeviceElement();
			case UiPackage.DEVICE_VIEW__SHOW_RESORCE_LIST:
				return isShowResorceList();
			case UiPackage.DEVICE_VIEW__INTERFACE_ELEMENTS:
				return getInterfaceElements();
			case UiPackage.DEVICE_VIEW__RESOURCE_CONTAINER_VIEW:
				if (resolve) return getResourceContainerView();
				return basicGetResourceContainerView();
			case UiPackage.DEVICE_VIEW__IN_CONNECTIONS:
				return getInConnections();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.DEVICE_VIEW__DEVICE_ELEMENT:
				setDeviceElement((Device)newValue);
				return;
			case UiPackage.DEVICE_VIEW__SHOW_RESORCE_LIST:
				setShowResorceList((Boolean)newValue);
				return;
			case UiPackage.DEVICE_VIEW__INTERFACE_ELEMENTS:
				getInterfaceElements().clear();
				getInterfaceElements().addAll((Collection<? extends InterfaceElementView>)newValue);
				return;
			case UiPackage.DEVICE_VIEW__RESOURCE_CONTAINER_VIEW:
				setResourceContainerView((ResourceContainerView)newValue);
				return;
			case UiPackage.DEVICE_VIEW__IN_CONNECTIONS:
				getInConnections().clear();
				getInConnections().addAll((Collection<? extends LinkView>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.DEVICE_VIEW__DEVICE_ELEMENT:
				setDeviceElement((Device)null);
				return;
			case UiPackage.DEVICE_VIEW__SHOW_RESORCE_LIST:
				setShowResorceList(SHOW_RESORCE_LIST_EDEFAULT);
				return;
			case UiPackage.DEVICE_VIEW__INTERFACE_ELEMENTS:
				getInterfaceElements().clear();
				return;
			case UiPackage.DEVICE_VIEW__RESOURCE_CONTAINER_VIEW:
				setResourceContainerView((ResourceContainerView)null);
				return;
			case UiPackage.DEVICE_VIEW__IN_CONNECTIONS:
				getInConnections().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.DEVICE_VIEW__DEVICE_ELEMENT:
				return deviceElement != null;
			case UiPackage.DEVICE_VIEW__SHOW_RESORCE_LIST:
				return showResorceList != SHOW_RESORCE_LIST_EDEFAULT;
			case UiPackage.DEVICE_VIEW__INTERFACE_ELEMENTS:
				return interfaceElements != null && !interfaceElements.isEmpty();
			case UiPackage.DEVICE_VIEW__RESOURCE_CONTAINER_VIEW:
				return resourceContainerView != null;
			case UiPackage.DEVICE_VIEW__IN_CONNECTIONS:
				return inConnections != null && !inConnections.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (showResorceList: ");
		result.append(showResorceList);
		result.append(')');
		return result.toString();
	}

} //DeviceViewImpl
