package org.fordiac.ide.typelibrary;

public interface TypeLibraryTags {

	public static final String TOOL_LIBRARY_PROJECT_NAME = "Tool Library";

	/** The Constant for the file ending of FB type files. */
	public static final String FB_TYPE_FILE_ENDING = "FBT"; //$NON-NLS-1$

	public static final String FB_TYPE_FILE_ENDING_WITH_DOT = "."+ FB_TYPE_FILE_ENDING; //$NON-NLS-1$

	public static final String ADAPTER_TYPE_FILE_ENDING = "ADP";

	public static final String ADAPTER_TYPE_FILE_ENDING_WITH_DOT = "." + ADAPTER_TYPE_FILE_ENDING;

	public static final String DEVICE_TYPE_FILE_ENDING = "DEV";

	public static final String DEVICE_TYPE_FILE_ENDING_WITH_DOT = "." + DEVICE_TYPE_FILE_ENDING;

	public static final String RESOURCE_TYPE_FILE_ENDING = "RES";

	public static final String RESOURCE_TYPE_FILE_ENDING_WITH_DOT = "." + RESOURCE_TYPE_FILE_ENDING;

	public static final String SEGMENT_TYPE_FILE_ENDING = "SEG";

	public static final String SEGMENT_TYPE_FILE_ENDING_WITH_DOT = "." + SEGMENT_TYPE_FILE_ENDING;

	public static final String SUBAPP_TYPE_FILE_ENDING = "SUB";

	public static final String SUBAPP_TYPE_FILE_ENDING_WITH_DOT = "." + SUBAPP_TYPE_FILE_ENDING;

	/** The Constant TYPE_LIBRARY. */
	public static final String TYPE_LIBRARY = "typelibrary";//$NON-NLS-1$

	/** The Constant TOOL_TYPE_DIR. */
	public static final String TOOL_TYPE_DIR = TYPE_LIBRARY + "/";//$NON-NLS-1$
	
}
