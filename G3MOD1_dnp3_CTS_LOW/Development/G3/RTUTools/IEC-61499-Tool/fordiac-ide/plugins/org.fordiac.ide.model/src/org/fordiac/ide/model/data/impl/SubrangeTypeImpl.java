/**
 * *******************************************************************************
 *  * Copyright (c) 2007 - 2012 4DIAC - consortium.
 *  * All rights reserved. This program and the accompanying materials
 *  * are made available under the terms of the Eclipse Public License v1.0
 *  * which accompanies this distribution, and is available at
 *  * http://www.eclipse.org/legal/epl-v10.html
 *  *
 *  *******************************************************************************
 */
package org.fordiac.ide.model.data.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.fordiac.ide.model.data.DataPackage;
import org.fordiac.ide.model.data.Subrange;
import org.fordiac.ide.model.data.SubrangeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Subrange Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.data.impl.SubrangeTypeImpl#getSubrange <em>Subrange</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubrangeTypeImpl extends DerivedTypeImpl implements SubrangeType {
	/**
	 * The cached value of the '{@link #getSubrange() <em>Subrange</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubrange()
	 * @generated
	 * @ordered
	 */
	protected Subrange subrange;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubrangeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.SUBRANGE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Subrange getSubrange() {
		return subrange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubrange(Subrange newSubrange, NotificationChain msgs) {
		Subrange oldSubrange = subrange;
		subrange = newSubrange;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DataPackage.SUBRANGE_TYPE__SUBRANGE, oldSubrange, newSubrange);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubrange(Subrange newSubrange) {
		if (newSubrange != subrange) {
			NotificationChain msgs = null;
			if (subrange != null)
				msgs = ((InternalEObject)subrange).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DataPackage.SUBRANGE_TYPE__SUBRANGE, null, msgs);
			if (newSubrange != null)
				msgs = ((InternalEObject)newSubrange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DataPackage.SUBRANGE_TYPE__SUBRANGE, null, msgs);
			msgs = basicSetSubrange(newSubrange, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SUBRANGE_TYPE__SUBRANGE, newSubrange, newSubrange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DataPackage.SUBRANGE_TYPE__SUBRANGE:
				return basicSetSubrange(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DataPackage.SUBRANGE_TYPE__SUBRANGE:
				return getSubrange();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DataPackage.SUBRANGE_TYPE__SUBRANGE:
				setSubrange((Subrange)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DataPackage.SUBRANGE_TYPE__SUBRANGE:
				setSubrange((Subrange)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DataPackage.SUBRANGE_TYPE__SUBRANGE:
				return subrange != null;
		}
		return super.eIsSet(featureID);
	}

} //SubrangeTypeImpl
