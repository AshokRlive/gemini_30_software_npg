/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.ResourceFBNetwork;
import org.fordiac.ide.model.libraryElement.SubApp;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Resource FB Network</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.ResourceFBNetworkImpl#getMappedFBs <em>Mapped FBs</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.ResourceFBNetworkImpl#getMappedSubApps <em>Mapped Sub Apps</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResourceFBNetworkImpl extends FBNetworkImpl implements
		ResourceFBNetwork {
	/**
	 * The cached value of the '{@link #getMappedFBs() <em>Mapped FBs</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMappedFBs()
	 * @generated
	 * @ordered
	 */
	protected EList<FB> mappedFBs;

	/**
	 * The cached value of the '{@link #getMappedSubApps() <em>Mapped Sub Apps</em>}' reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getMappedSubApps()
	 * @generated
	 * @ordered
	 */
	protected EList<SubApp> mappedSubApps;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceFBNetworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibraryElementPackage.Literals.RESOURCE_FB_NETWORK;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FB> getMappedFBs() {
		if (mappedFBs == null) {
			mappedFBs = new EObjectWithInverseResolvingEList<FB>(FB.class, this, LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_FBS, LibraryElementPackage.FB__RESOURCE);
		}
		return mappedFBs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubApp> getMappedSubApps() {
		if (mappedSubApps == null) {
			mappedSubApps = new EObjectWithInverseResolvingEList<SubApp>(SubApp.class, this, LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_SUB_APPS, LibraryElementPackage.SUB_APP__RESOURCE);
		}
		return mappedSubApps;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID,
			NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_FBS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMappedFBs()).basicAdd(otherEnd, msgs);
			case LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_SUB_APPS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMappedSubApps()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_FBS:
				return ((InternalEList<?>)getMappedFBs()).basicRemove(otherEnd, msgs);
			case LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_SUB_APPS:
				return ((InternalEList<?>)getMappedSubApps()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_FBS:
				return getMappedFBs();
			case LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_SUB_APPS:
				return getMappedSubApps();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_FBS:
				getMappedFBs().clear();
				getMappedFBs().addAll((Collection<? extends FB>)newValue);
				return;
			case LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_SUB_APPS:
				getMappedSubApps().clear();
				getMappedSubApps().addAll((Collection<? extends SubApp>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_FBS:
				getMappedFBs().clear();
				return;
			case LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_SUB_APPS:
				getMappedSubApps().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_FBS:
				return mappedFBs != null && !mappedFBs.isEmpty();
			case LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_SUB_APPS:
				return mappedSubApps != null && !mappedSubApps.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	@Override
	public FB getFB(String name) {
		FB fb = super.getFB(name);
		if (fb == null) {
			for (Iterator<FB> iterator = getMappedFBs().iterator(); iterator.hasNext();) {
				FB mappedFB = iterator.next();
				if (mappedFB.getName().equals(name)) {
					return mappedFB;
				}
			}
		}
		return fb;
	}

} // ResourceFBNetworkImpl
