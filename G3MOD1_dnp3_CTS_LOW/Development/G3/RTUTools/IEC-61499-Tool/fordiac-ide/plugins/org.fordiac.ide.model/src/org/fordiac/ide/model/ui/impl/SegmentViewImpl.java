/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.fordiac.ide.model.libraryElement.Segment;

import org.fordiac.ide.model.ui.LinkView;
import org.fordiac.ide.model.ui.SegmentView;
import org.fordiac.ide.model.ui.Size;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Segment View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.SegmentViewImpl#getSegment <em>Segment</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.SegmentViewImpl#getOutConnections <em>Out Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.SegmentViewImpl#getSize <em>Size</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SegmentViewImpl extends ViewImpl implements SegmentView {
	/**
	 * The cached value of the '{@link #getSegment() <em>Segment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegment()
	 * @generated
	 * @ordered
	 */
	protected Segment segment;

	/**
	 * The cached value of the '{@link #getOutConnections() <em>Out Connections</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutConnections()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkView> outConnections;

	/**
	 * The cached value of the '{@link #getSize() <em>Size</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected Size size;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SegmentViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.SEGMENT_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Segment getSegment() {
		if (segment != null && segment.eIsProxy()) {
			InternalEObject oldSegment = (InternalEObject)segment;
			segment = (Segment)eResolveProxy(oldSegment);
			if (segment != oldSegment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.SEGMENT_VIEW__SEGMENT, oldSegment, segment));
			}
		}
		return segment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Segment basicGetSegment() {
		return segment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSegment(Segment newSegment) {
		Segment oldSegment = segment;
		segment = newSegment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.SEGMENT_VIEW__SEGMENT, oldSegment, segment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkView> getOutConnections() {
		if (outConnections == null) {
			outConnections = new EObjectWithInverseResolvingEList<LinkView>(LinkView.class, this, UiPackage.SEGMENT_VIEW__OUT_CONNECTIONS, UiPackage.LINK_VIEW__SOURCE);
		}
		return outConnections;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Size getSize() {
		if (size != null && size.eIsProxy()) {
			InternalEObject oldSize = (InternalEObject)size;
			size = (Size)eResolveProxy(oldSize);
			if (size != oldSize) {
				InternalEObject newSize = (InternalEObject)size;
				NotificationChain msgs = oldSize.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.SEGMENT_VIEW__SIZE, null, null);
				if (newSize.eInternalContainer() == null) {
					msgs = newSize.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.SEGMENT_VIEW__SIZE, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.SEGMENT_VIEW__SIZE, oldSize, size));
			}
		}
		return size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Size basicGetSize() {
		return size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSize(Size newSize, NotificationChain msgs) {
		Size oldSize = size;
		size = newSize;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.SEGMENT_VIEW__SIZE, oldSize, newSize);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSize(Size newSize) {
		if (newSize != size) {
			NotificationChain msgs = null;
			if (size != null)
				msgs = ((InternalEObject)size).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.SEGMENT_VIEW__SIZE, null, msgs);
			if (newSize != null)
				msgs = ((InternalEObject)newSize).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.SEGMENT_VIEW__SIZE, null, msgs);
			msgs = basicSetSize(newSize, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.SEGMENT_VIEW__SIZE, newSize, newSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.SEGMENT_VIEW__OUT_CONNECTIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutConnections()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.SEGMENT_VIEW__OUT_CONNECTIONS:
				return ((InternalEList<?>)getOutConnections()).basicRemove(otherEnd, msgs);
			case UiPackage.SEGMENT_VIEW__SIZE:
				return basicSetSize(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.SEGMENT_VIEW__SEGMENT:
				if (resolve) return getSegment();
				return basicGetSegment();
			case UiPackage.SEGMENT_VIEW__OUT_CONNECTIONS:
				return getOutConnections();
			case UiPackage.SEGMENT_VIEW__SIZE:
				if (resolve) return getSize();
				return basicGetSize();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.SEGMENT_VIEW__SEGMENT:
				setSegment((Segment)newValue);
				return;
			case UiPackage.SEGMENT_VIEW__OUT_CONNECTIONS:
				getOutConnections().clear();
				getOutConnections().addAll((Collection<? extends LinkView>)newValue);
				return;
			case UiPackage.SEGMENT_VIEW__SIZE:
				setSize((Size)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.SEGMENT_VIEW__SEGMENT:
				setSegment((Segment)null);
				return;
			case UiPackage.SEGMENT_VIEW__OUT_CONNECTIONS:
				getOutConnections().clear();
				return;
			case UiPackage.SEGMENT_VIEW__SIZE:
				setSize((Size)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.SEGMENT_VIEW__SEGMENT:
				return segment != null;
			case UiPackage.SEGMENT_VIEW__OUT_CONNECTIONS:
				return outConnections != null && !outConnections.isEmpty();
			case UiPackage.SEGMENT_VIEW__SIZE:
				return size != null;
		}
		return super.eIsSet(featureID);
	}

} //SegmentViewImpl
