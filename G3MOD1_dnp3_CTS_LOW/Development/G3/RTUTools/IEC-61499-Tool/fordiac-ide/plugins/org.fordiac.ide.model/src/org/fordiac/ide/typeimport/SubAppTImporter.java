/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.typeimport;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.Palette.SubApplicationTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.Position;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.SubAppType;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.typeimport.exceptions.ReferencedTypeNotFoundException;
import org.fordiac.ide.typeimport.exceptions.SubAppTImportException;
import org.fordiac.ide.typeimport.exceptions.TypeImportException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Managing class for importing SubApplication files.
 * 
 * @author Martijn Rooker (martijn.rooker@profactor.at)
 */
public class SubAppTImporter extends FBTImporter{

	
	/**
	 * Import sub app type.
	 * 
	 * @param subapptFile the subappt file
	 * @param palette the palette
	 * 
	 * @return the sub app type
	 * 
	 * @throws TypeImportException the FBT import exception
	 * @throws ReferencedTypeNotFoundException the referenced type not found exception
	 */
	public SubAppType importSubAppType(final IFile subapptFile,
			final Palette palette) throws ReferencedTypeNotFoundException {
		FBType newType = importType(subapptFile, palette);
		if((null != newType) && (type instanceof SubAppType)){
			return (SubAppType)type;
		}		
		return null;
	}
	
	@Override
	protected FBType createType(){
		return LibraryElementFactory.eINSTANCE.createSubAppType();
	}

	
	
	@Override
	protected FBType parseType(Node rootNode) throws TypeImportException,
			ReferencedTypeNotFoundException, ParseException {
		if (rootNode.getNodeName().equals(LibraryElementTags.SUBAPPTYPE_ELEMENT)) {
			parseTypeNameAndComment(rootNode);
			
			NodeList childNodes = rootNode.getChildNodes();
			for (int i = 0; i < childNodes.getLength(); i++) {
				Node n = childNodes.item(i);
				if (n.getNodeName().equals(IDENTIFICATION_ELEMENT)) {
					type.setIdentification(CommonElementImporter
							.parseIdentification(type, n));
				}
				if (n.getNodeName().equals(VERSION_INFO_ELEMENT)) {
					type.getVersionInfo().add(
							CommonElementImporter.parseVersionInfo(type, n));
				}
				if (n.getNodeName().equals(COMPILER_INFO_ELEMENT)) {
					type.setCompilerInfo(CompilableElementImporter
							.parseCompilerInfo(type, n));
					// parseCompilerInfo(type, n);
				}
				if (n.getNodeName().equals(SUBAPPINTERFACE_LIST_ELEMENT)) {
					parseInterfaceList(type, n);
				}
				
				if (n.getNodeName().equals(SERVICE_ELEMENT)) {
					parseService(type, n);
				}
				
				if (n.getNodeName().equals(LibraryElementTags.SUBAPPNETWORK_ELEMENT)) {
					parseSubAppNetWork(n);
				}
			
			}
			
			return type;
		}else {
			throw new ParseException(Messages.SubAppTImporter_ERROR, 0);
		}
	}

	

	/**
	 * Parses the sub app net work.
	 * 
	 * @param type
	 *          the type
	 * @param node
	 *          the node
	 * 
	 * @throws SubAppTImportException
	 *           the sub app t import exception
	 * @throws TypeImportException
	 *           the FBT import exception
	 * @throws ReferencedTypeNotFoundException
	 *           the referenced type not found exception
	 */
	private void parseSubAppNetWork(final Node subAppNode) throws TypeImportException, ReferencedTypeNotFoundException {
		NodeList childNodes = subAppNode.getChildNodes();
		
		FBNetwork subAppNetwork = LibraryElementFactory.eINSTANCE.createFBNetwork();
		((SubAppType)type).setFBNetwork(subAppNetwork);
		
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node node = childNodes.item(i);
			if (node.getNodeName().equals(LibraryElementTags.SUBAPP_ELEMENT)) {
				subAppNetwork.getSubApps().add(parseSubApp(node));
			}
			if (node.getNodeName().equals(LibraryElementTags.FB_ELEMENT)) {
				FB fb = parseFB(node, type, palette);
				if(null != fb){
					fb.setResourceTypeFB(false);
					subAppNetwork.getFBs().add(fb);
				}
			}
			if (node.getNodeName().equals(LibraryElementTags.EVENT_CONNECTIONS_ELEMENT)) {
				parseEventConnections(node, subAppNetwork.getEventConnections());
			}
			if (node.getNodeName().equals(LibraryElementTags.DATA_CONNECTIONS_ELEMENT)) {
				parseDataConnection(node, subAppNetwork.getDataConnections(),
						subAppNetwork.getFBs());
			}
		}
	}

	


	/**
	 * Parses the sub app.
	 * 
	 * @param node
	 *          the node
	 * 
	 * @return the sub app
	 * 
	 * @throws SubAppTImportException
	 *           the sub app t import exception
	 */
	private SubApp parseSubApp(final Node node)
			throws TypeImportException {
		SubApp subApp = LibraryElementFactory.eINSTANCE.createSubApp();
		subApp.setId(EcoreUtil.generateUUID());
		subApp.setIdentifier(true);
		
		SubAppNetwork subAppNetwork = LibraryElementFactory.eINSTANCE.createSubAppNetwork();
		subApp.setSubAppNetwork(subAppNetwork);

		
		NamedNodeMap map = node.getAttributes();
		Node name = map.getNamedItem(LibraryElementTags.NAME_ATTRIBUTE);
		if (name != null) {
			subApp.setName(name.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.SubAppTImporter_ERROR_SubAppName);
		}
		Node type = map.getNamedItem(LibraryElementTags.TYPE_ATTRIBUTE);
		if (type != null) {
			configureSubAppInterface(subApp, node, type.getNodeValue());					
		} else {
			throw new TypeImportException(
					Messages.SubAppTImporter_ERROR_SubAppType);
		}
		Node comment = map.getNamedItem(LibraryElementTags.COMMENT_ATTRIBUTE);
		if (comment != null) {
			subApp.setComment(comment.getNodeValue());
		}
		
		Position pos = getXandY(map);
		subApp.setX(Integer.toString(pos.getX()));
		subApp.setY(Integer.toString(pos.getY()));
		
		return subApp;
	}

	private void configureSubAppInterface(SubApp subApp, Node node, String typeName) throws TypeImportException {
		List<PaletteEntry> entries = palette.getTypeEntries(typeName);
		PaletteEntry entry = null;
		
		if (entries.size() > 0) {
			entry = entries.get(0);
		}
		
		if(entry instanceof SubApplicationTypePaletteEntry){
			subApp.setPaletteEntry(entry);
			SubApplicationTypePaletteEntry subEntry = (SubApplicationTypePaletteEntry)entry;
			
			subApp.setInterface((InterfaceList) EcoreUtil.copy(
					subEntry.getSubApplicationType().getInterfaceList()));
			
			configureParameters(subApp.getInterface(), node.getChildNodes());
		}		
	}

	@Override
	protected String getEventOutputElement() {
		return LibraryElementTags.SUBAPP_EVENTOUTPUTS_ELEMENT;
	}

	@Override
	protected String getEventInputElement() {
		return LibraryElementTags.SUBAPP_EVENTINPUTS_ELEMENT;
	}

	@Override
	protected String getEventElement() {
		return LibraryElementTags.SUBAPP_EVENT_ELEMENT;
	}

	@Override
	public void parseWithConstructs(NodeList childNodes,
			HashMap<String, Event> eventInputs,
			HashMap<String, Event> eventOutputs,
			HashMap<String, VarDeclaration> variables) {
		//supapps may not have a with construct. Therefore we are doing nothing here
	}

	@Override
	protected Event getEvent(String componentName, String eventName, boolean input){
		Event retVal = null;		
		SubApp subApp = findSubApp(componentName);
		if(null != subApp){
			retVal = subApp.getInterface().getEvent(eventName);
			if((null != retVal) && (retVal.isIsInput() != input)){
				retVal = null;
			}
		}else{
			retVal = super.getEvent(componentName, eventName, input);
		}
		return retVal;
	}
	
	@Override
	protected VarDeclaration getVarDeclaration(String componentName, String varName, final EList<FB> functionBlocks, boolean input){
		VarDeclaration retVal = null;
		
		SubApp subApp = findSubApp(componentName);
		if(null != subApp){
			retVal = subApp.getInterface().getVariable(varName);
			if((null != retVal) && (retVal.isIsInput() != input)){
				retVal = null;
			}
		}else{
			retVal = super.getVarDeclaration(componentName, varName, functionBlocks, input);
		}
		return retVal;
	}
			
	/**searches the subapps in the subapps fbnetwork and tries to find one with the given name
	 * 
	 * @param subAppName name of the looked for subapp
	 * @return the found subapp or null
	 */
	private SubApp findSubApp(String subAppName){
		for (SubApp subApp : ((SubAppType)type).getFBNetwork().getSubApps()) {
			if (subApp.getName().equals(subAppName)) {				
				return subApp;
			}
		}
		return null;
	}
	
	
	
}
