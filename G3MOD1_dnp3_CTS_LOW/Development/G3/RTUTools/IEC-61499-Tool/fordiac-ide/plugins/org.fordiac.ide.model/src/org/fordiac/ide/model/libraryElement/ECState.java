/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EC State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.ECState#getECAction <em>EC Action</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.ECState#getPosition <em>Position</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.ECState#getOutTransitions <em>Out Transitions</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.ECState#getInTransitions <em>In Transitions</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getECState()
 * @model
 * @generated
 */
public interface ECState extends INamedElement {
	/**
	 * Returns the value of the '<em><b>EC Action</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.ECAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EC Action</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EC Action</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getECState_ECAction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ECAction' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ECAction> getECAction();

	/**
	 * Returns the value of the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' containment reference.
	 * @see #setPosition(Position)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getECState_Position()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Position getPosition();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.ECState#getPosition <em>Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position</em>' containment reference.
	 * @see #getPosition()
	 * @generated
	 */
	void setPosition(Position value);

	/**
	 * Returns the value of the '<em><b>Out Transitions</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.ECTransition}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.ECTransition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Transitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Transitions</em>' reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getECState_OutTransitions()
	 * @see org.fordiac.ide.model.libraryElement.ECTransition#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<ECTransition> getOutTransitions();

	/**
	 * Returns the value of the '<em><b>In Transitions</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.ECTransition}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.ECTransition#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Transitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Transitions</em>' reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getECState_InTransitions()
	 * @see org.fordiac.ide.model.libraryElement.ECTransition#getDestination
	 * @model opposite="destination"
	 * @generated
	 */
	EList<ECTransition> getInTransitions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="org.eclipse.emf.ecore.xml.type.Boolean" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if(null != eContainer()){\n\treturn this.equals(((ECC)eContainer()).getStart());\n}\nreturn false;'"
	 * @generated
	 */
	boolean isStartState();

} // ECState
