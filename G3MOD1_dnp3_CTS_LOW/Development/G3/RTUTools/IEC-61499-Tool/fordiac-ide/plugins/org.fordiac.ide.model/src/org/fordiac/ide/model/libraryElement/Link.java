/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Link#getCommResource <em>Comm Resource</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Link#getSegmentName <em>Segment Name</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Link#getSegment <em>Segment</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Link#getDevice <em>Device</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getLink()
 * @model
 * @generated
 */
public interface Link extends Connection {
	/**
	 * Returns the value of the '<em><b>Comm Resource</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comm Resource</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comm Resource</em>' attribute.
	 * @see #setCommResource(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getLink_CommResource()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='CommResource'"
	 * @generated
	 */
	String getCommResource();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Link#getCommResource <em>Comm Resource</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comm Resource</em>' attribute.
	 * @see #getCommResource()
	 * @generated
	 */
	void setCommResource(String value);

	/**
	 * Returns the value of the '<em><b>Segment Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment Name</em>' attribute.
	 * @see #setSegmentName(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getLink_SegmentName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='SegmentName'"
	 * @generated
	 */
	String getSegmentName();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Link#getSegmentName <em>Segment Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Segment Name</em>' attribute.
	 * @see #getSegmentName()
	 * @generated
	 */
	void setSegmentName(String value);

	/**
	 * Returns the value of the '<em><b>Segment</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.Segment#getOutConnections <em>Out Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment</em>' reference.
	 * @see #setSegment(Segment)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getLink_Segment()
	 * @see org.fordiac.ide.model.libraryElement.Segment#getOutConnections
	 * @model opposite="outConnections"
	 * @generated
	 */
	Segment getSegment();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Link#getSegment <em>Segment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Segment</em>' reference.
	 * @see #getSegment()
	 * @generated
	 */
	void setSegment(Segment value);

	/**
	 * Returns the value of the '<em><b>Device</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.Device#getInConnections <em>In Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Device</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device</em>' reference.
	 * @see #setDevice(Device)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getLink_Device()
	 * @see org.fordiac.ide.model.libraryElement.Device#getInConnections
	 * @model opposite="inConnections"
	 * @generated
	 */
	Device getDevice();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Link#getDevice <em>Device</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Device</em>' reference.
	 * @see #getDevice()
	 * @generated
	 */
	void setDevice(Device value);

} // Link
