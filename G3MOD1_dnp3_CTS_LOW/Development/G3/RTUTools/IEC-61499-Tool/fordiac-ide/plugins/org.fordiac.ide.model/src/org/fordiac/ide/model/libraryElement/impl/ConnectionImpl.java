/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connection</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.ConnectionImpl#getDx1 <em>Dx1</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.ConnectionImpl#getDx2 <em>Dx2</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.ConnectionImpl#getDy <em>Dy</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.ConnectionImpl#isResTypeConnection <em>Res Type Connection</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.ConnectionImpl#isResourceConnection <em>Resource Connection</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.ConnectionImpl#isBrokenConnection <em>Broken Connection</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ConnectionImpl extends ConfigurableObjectImpl implements Connection {
	/**
	 * The default value of the '{@link #getDx1() <em>Dx1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDx1()
	 * @generated
	 * @ordered
	 */
	protected static final int DX1_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDx1() <em>Dx1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDx1()
	 * @generated
	 * @ordered
	 */
	protected int dx1 = DX1_EDEFAULT;

	/**
	 * The default value of the '{@link #getDx2() <em>Dx2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDx2()
	 * @generated
	 * @ordered
	 */
	protected static final int DX2_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDx2() <em>Dx2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDx2()
	 * @generated
	 * @ordered
	 */
	protected int dx2 = DX2_EDEFAULT;

	/**
	 * The default value of the '{@link #getDy() <em>Dy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDy()
	 * @generated
	 * @ordered
	 */
	protected static final int DY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDy() <em>Dy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDy()
	 * @generated
	 * @ordered
	 */
	protected int dy = DY_EDEFAULT;

	/**
	 * The default value of the '{@link #isResTypeConnection() <em>Res Type Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResTypeConnection()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RES_TYPE_CONNECTION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isResTypeConnection() <em>Res Type Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResTypeConnection()
	 * @generated
	 * @ordered
	 */
	protected boolean resTypeConnection = RES_TYPE_CONNECTION_EDEFAULT;

	/**
	 * The default value of the '{@link #isResourceConnection() <em>Resource Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResourceConnection()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RESOURCE_CONNECTION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isResourceConnection() <em>Resource Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResourceConnection()
	 * @generated
	 * @ordered
	 */
	protected boolean resourceConnection = RESOURCE_CONNECTION_EDEFAULT;

	/**
	 * The default value of the '{@link #isBrokenConnection() <em>Broken Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBrokenConnection()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BROKEN_CONNECTION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isBrokenConnection() <em>Broken Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBrokenConnection()
	 * @generated
	 * @ordered
	 */
	protected boolean brokenConnection = BROKEN_CONNECTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibraryElementPackage.Literals.CONNECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDx1() {
		return dx1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDx1(int newDx1) {
		int oldDx1 = dx1;
		dx1 = newDx1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.CONNECTION__DX1, oldDx1, dx1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDx2() {
		return dx2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDx2(int newDx2) {
		int oldDx2 = dx2;
		dx2 = newDx2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.CONNECTION__DX2, oldDx2, dx2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDy() {
		return dy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDy(int newDy) {
		int oldDy = dy;
		dy = newDy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.CONNECTION__DY, oldDy, dy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isResTypeConnection() {
		return resTypeConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResTypeConnection(boolean newResTypeConnection) {
		boolean oldResTypeConnection = resTypeConnection;
		resTypeConnection = newResTypeConnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.CONNECTION__RES_TYPE_CONNECTION, oldResTypeConnection, resTypeConnection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isResourceConnection() {
		return resourceConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourceConnection(boolean newResourceConnection) {
		boolean oldResourceConnection = resourceConnection;
		resourceConnection = newResourceConnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.CONNECTION__RESOURCE_CONNECTION, oldResourceConnection, resourceConnection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBrokenConnection() {
		return brokenConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBrokenConnection(boolean newBrokenConnection) {
		boolean oldBrokenConnection = brokenConnection;
		brokenConnection = newBrokenConnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.CONNECTION__BROKEN_CONNECTION, oldBrokenConnection, brokenConnection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibraryElementPackage.CONNECTION__DX1:
				return getDx1();
			case LibraryElementPackage.CONNECTION__DX2:
				return getDx2();
			case LibraryElementPackage.CONNECTION__DY:
				return getDy();
			case LibraryElementPackage.CONNECTION__RES_TYPE_CONNECTION:
				return isResTypeConnection();
			case LibraryElementPackage.CONNECTION__RESOURCE_CONNECTION:
				return isResourceConnection();
			case LibraryElementPackage.CONNECTION__BROKEN_CONNECTION:
				return isBrokenConnection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibraryElementPackage.CONNECTION__DX1:
				setDx1((Integer)newValue);
				return;
			case LibraryElementPackage.CONNECTION__DX2:
				setDx2((Integer)newValue);
				return;
			case LibraryElementPackage.CONNECTION__DY:
				setDy((Integer)newValue);
				return;
			case LibraryElementPackage.CONNECTION__RES_TYPE_CONNECTION:
				setResTypeConnection((Boolean)newValue);
				return;
			case LibraryElementPackage.CONNECTION__RESOURCE_CONNECTION:
				setResourceConnection((Boolean)newValue);
				return;
			case LibraryElementPackage.CONNECTION__BROKEN_CONNECTION:
				setBrokenConnection((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.CONNECTION__DX1:
				setDx1(DX1_EDEFAULT);
				return;
			case LibraryElementPackage.CONNECTION__DX2:
				setDx2(DX2_EDEFAULT);
				return;
			case LibraryElementPackage.CONNECTION__DY:
				setDy(DY_EDEFAULT);
				return;
			case LibraryElementPackage.CONNECTION__RES_TYPE_CONNECTION:
				setResTypeConnection(RES_TYPE_CONNECTION_EDEFAULT);
				return;
			case LibraryElementPackage.CONNECTION__RESOURCE_CONNECTION:
				setResourceConnection(RESOURCE_CONNECTION_EDEFAULT);
				return;
			case LibraryElementPackage.CONNECTION__BROKEN_CONNECTION:
				setBrokenConnection(BROKEN_CONNECTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.CONNECTION__DX1:
				return dx1 != DX1_EDEFAULT;
			case LibraryElementPackage.CONNECTION__DX2:
				return dx2 != DX2_EDEFAULT;
			case LibraryElementPackage.CONNECTION__DY:
				return dy != DY_EDEFAULT;
			case LibraryElementPackage.CONNECTION__RES_TYPE_CONNECTION:
				return resTypeConnection != RES_TYPE_CONNECTION_EDEFAULT;
			case LibraryElementPackage.CONNECTION__RESOURCE_CONNECTION:
				return resourceConnection != RESOURCE_CONNECTION_EDEFAULT;
			case LibraryElementPackage.CONNECTION__BROKEN_CONNECTION:
				return brokenConnection != BROKEN_CONNECTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dx1: ");
		result.append(dx1);
		result.append(", dx2: ");
		result.append(dx2);
		result.append(", dy: ");
		result.append(dy);
		result.append(", resTypeConnection: ");
		result.append(resTypeConnection);
		result.append(", resourceConnection: ");
		result.append(resourceConnection);
		result.append(", brokenConnection: ");
		result.append(brokenConnection);
		result.append(')');
		return result.toString();
	}

} //ConnectionImpl
