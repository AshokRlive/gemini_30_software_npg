/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.typeexport;

import org.eclipse.emf.common.util.EList;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.Palette.SubApplicationTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppType;
import org.fordiac.ide.typeimport.LibraryElementTags;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

class SubApplicationTypeExporter extends CommonElementExporter {

	@Override
	protected FBType getType(PaletteEntry entry) {
		return ((SubApplicationTypePaletteEntry)entry).getSubApplicationType();
	}

	@Override
	protected void addType(Document dom, FBType fbType) {
		
		Element rootElement = createRootElement(dom, fbType, LibraryElementTags.SUBAPPTYPE_ELEMENT);
		addCompileAbleTypeData(dom, rootElement, fbType);
		
		addInterfaceList(dom, rootElement, fbType.getInterfaceList(), null, fbType);
		
		addFBNetwork(dom, rootElement, ((SubAppType)fbType).getFBNetwork());
		
		addService(dom, rootElement, fbType);
	}
	
	private void addFBNetwork(final Document dom, final Element rootEle, final FBNetwork fbNetWork) {
		Element fbNetworkElement = dom.createElement(LibraryElementTags.SUBAPPNETWORK_ELEMENT);
		
		addSubApps(dom, fbNetworkElement, fbNetWork.getSubApps()); 
		
		addFBs(dom, fbNetworkElement, fbNetWork);
		addConnections(dom, fbNetworkElement, fbNetWork.getDataConnections());
		addConnections(dom, fbNetworkElement, fbNetWork.getEventConnections());
		//TODO adapter connections 
		rootEle.appendChild(fbNetworkElement);
	}

	private void addSubApps(Document dom, Element fbNetworkElement,
			EList<SubApp> subApps) {
		for (SubApp subApp : subApps) {
			Element subAppElement = dom.createElement(LibraryElementTags.SUBAPP_ELEMENT);
			
			setNameAttribute(subAppElement, subApp.getName());
			setTypeAttribute(subAppElement, subApp.getPaletteEntry().getLabel());
			setCommentAttribute(subAppElement, subApp);
			
			setXYAttributes(subAppElement, Integer.parseInt(subApp.getX()), Integer.parseInt(subApp.getY()));
			
			addParamsConfig(dom, subAppElement, subApp.getInterface().getInputVars());

			fbNetworkElement.appendChild(subAppElement);
		}
		
	}

	@Override
	protected String getInterfaceListElementName() {
		return LibraryElementTags.SUBAPPINTERFACE_LIST_ELEMENT;
	}

	@Override
	protected String getEventOutputsElementName() {
		return LibraryElementTags.SUBAPP_EVENTOUTPUTS_ELEMENT;
	}

	@Override
	protected String getEventInputsElementName() {
		return LibraryElementTags.SUBAPP_EVENTINPUTS_ELEMENT;
	}

	@Override
	protected String getEventElementName() {
		return LibraryElementTags.SUBAPP_EVENT_ELEMENT;
	}
	
	

}
