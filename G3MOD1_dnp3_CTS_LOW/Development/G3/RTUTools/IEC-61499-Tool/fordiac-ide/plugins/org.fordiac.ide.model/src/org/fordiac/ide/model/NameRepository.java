/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.fordiac.ide.model.data.DataType;
import org.fordiac.ide.model.libraryElement.Algorithm;
import org.fordiac.ide.model.libraryElement.Annotation;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.ECC;
import org.fordiac.ide.model.libraryElement.ECState;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.ResourceFBNetwork;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.typelibrary.DataTypeLibrary;

/**
 * @author gebenh
 * 
 */
public class NameRepository {

	private final Hashtable<AutomationSystem, Hashtable<String, String>> fbUniqueSystemInstanceNames = new Hashtable<AutomationSystem, Hashtable<String, String>>();

	private static NameRepository instance;

	public static final Set<String> RESERVED_KEYWORDS =
			Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(new String[] {
					"VAR", "END_VAR", "CONSTANT",
					"SUPER", "RETURN",
					"IF", "THEN", "END_IF", "ELSIF", "ELSE",
					"CASE", "OF", "END_CASE",
					"EXIT", "CONTINUE",
					"FOR", "TO", "BY", "DO", "END_FOR",
					"WHILE", "END_WHILE",
					"REPEAT", "UNTIL", "END_REPEAT",
					"OR", "XOR", "AND", "MOD", "NOT",
					"E", "D", "H", "M", "S", "MS", "US", "NS",
					"DINT", "INT", "SINT", "LINT", "UINT", "USINT", "UDINT", "ULINT",
					"REAL", "LREAL",
					"STRING", "WSTRING",
					"CHAR", "WCHAR",
					"TIME", "LTIME",
					"TIME_OF_DAY", "LTIME_OF_DAY", "TOD", "LTOD",
					"DATE", "LDATE",
					"DATE_AND_TIME", "LDATE_AND_TIME",
					"BOOL"
					})));

	private NameRepository() {
		// empty private constructor
	}

	public static NameRepository getInstance() {
		if (instance == null) {
			instance = new NameRepository();
		}
		return instance;
	}
	
	
	public static boolean isValidIdentifier(String identifierName){
		return identifierName.matches("[a-zA-Z_]{1}[\\w]*");
	}
	
	public static void checkNameIdentifier(INamedElement element){
		element.getAnnotations().clear();
		if(!isValidIdentifier(element.getName())){
			Annotation ano = element.createAnnotation("Name: " + element.getName() + " is not a valid identifier!");
			ano.setServity(2); // 2 means error!
		}
	}

	public void removeInstanceName(FB fb) {
		if (fb != null) {
			EObject object = fb.eContainer();
			if (object instanceof FBNetwork) {
				Application app = ((FBNetwork) object).getApplication();
				if (app != null
						&& app.eContainer() instanceof AutomationSystem) {
					Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
							.get(app.eContainer());
					uniqueNames.remove(fb.getName());
				}
			}
		}
	}

	public String removeSystemUniqueInstanceName(
			AutomationSystem automationSystem, final String name) {
		if (automationSystem != null) {
			Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
					.get(automationSystem);
			if (uniqueNames == null) {
				uniqueNames = new Hashtable<String, String>();
				fbUniqueSystemInstanceNames.put(automationSystem, uniqueNames);
			}
			if (name != null) {
				return uniqueNames.remove(name);
			}
		}
		return null;
	}

	public void clearUniqueFBInstanceNames(AutomationSystem system) {
		if (system != null) {
			Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
					.get(system);
			if (uniqueNames != null) {
				uniqueNames.clear();
			}
		}

	}
	
	
	public boolean addSystemUniqueFBInstanceName(FB fb) {
		if (fb != null) {
			EObject object = fb.eContainer();
			if (object instanceof FBNetwork) {
				Application app = ((FBNetwork) object).getApplication();
				if (app != null
						&& app.eContainer() instanceof AutomationSystem) {
					return addSystemUniqueFBInstanceName((AutomationSystem)app.eContainer(), fb.getName(), fb.getId());
				}
			}
		}
		return false;
	}

	public boolean addSystemUniqueFBInstanceName(
			final AutomationSystem automationSystem, final String name,
			final String id) {
		if (automationSystem != null) {
			Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
					.get(automationSystem);
			if (uniqueNames == null) {
				uniqueNames = new Hashtable<String, String>();
				fbUniqueSystemInstanceNames.put(automationSystem, uniqueNames);
			}
			if (uniqueNames.containsKey(name)) {
				return uniqueNames.get(name).equals(id);
			} else {
				uniqueNames.put(name, id);
				return true;
			}
		}
		return false;
	}

	public boolean isSystemUniqueFBInstanceName(AutomationSystem system,
			String name, String id) {

		Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
				.get(system);
		if (uniqueNames.containsKey(name)) {
			if (uniqueNames.get(name).equals(id)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Checks whether the name is unique
	 * 
	 * @param fb
	 * @return
	 */
	public boolean isSystemUniqueFBInstanceName(FB fb) {
		if (fb != null) {
			EObject object = fb.eContainer();
			if (!(object instanceof FBNetwork) && object instanceof SubAppNetwork) {
				//get the Parent FBNetwork
				FBNetwork parentNet = getRootFBNetwork(((SubAppNetwork) fb.eContainer()).getParentSubApp());
				object = parentNet;
			}
			if (object instanceof FBNetwork) {
				Application app = ((FBNetwork) object).getApplication();
				AutomationSystem sys = null;
				if (app != null && app.eContainer() instanceof AutomationSystem) {
					sys = (AutomationSystem)app.eContainer();
				}
				else if(object instanceof ResourceFBNetwork){
					sys = ((Resource)((ResourceFBNetwork)object).eContainer()).getAutomationSystem();
				}
				
				if(null != sys){
					Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
							.get(sys);
					if (uniqueNames.containsKey(fb.getName())) {
						if (uniqueNames.get(fb.getName()).equals(fb.getId())) {
							return true;
						}
					} else {
						uniqueNames.put(fb.getName(), fb.getId());
						return true;
					}
				} else if (fb.getParentCompositeFBType() != null) {// fb inside
																	// a
																	// composite,
					Hashtable<String, String> uniqueNames = new Hashtable<String, String>();
					for (FB fbx : fb.getParentCompositeFBType().getFBNetwork()
							.getFBs()) {
						uniqueNames.put(fbx.getName(), fbx.getId());
					}
					String id = fb.getId();
					if (uniqueNames.containsKey(fb.getName())) {
						if (uniqueNames.get(fb.getName()).equals(id)) {
							return true;
						} else {
							return false;
						}
					} else {
						return true;
					}
				}
			}
		}
		return false;
	}

	public FBNetwork getRootFBNetwork(EObject childElement){
		
		FBNetwork rootNet = null;
		EObject currentParent;
		
		if(childElement != null && childElement instanceof SubApp){
			currentParent = ((SubApp) childElement).eContainer();
			rootNet = getRootFBNetwork(currentParent);
		}
		else if(childElement != null && childElement instanceof FBNetwork){
			rootNet = (FBNetwork)childElement;
		}
		else if(childElement != null && childElement instanceof SubAppNetwork){
			currentParent = ((SubAppNetwork) childElement).getParentSubApp();
			currentParent = ((SubApp) currentParent).eContainer();
			rootNet = getRootFBNetwork(currentParent);
		}
		
		return rootNet;
	}
	
	public String getNetworkUniqueSubApplicationName(final SubAppNetwork theParentSubnetwork, final String name){
		if (theParentSubnetwork != null) {
			HashSet<String> uniqueNames = new HashSet<String>();
	
			for (FB fb : theParentSubnetwork.getFBs()) {
				uniqueNames.add(fb.getName());
			}
			
			for(SubApp currentSubApp : theParentSubnetwork.getSubApps()){
				uniqueNames.add(currentSubApp.getName());
			}
	
			return getUniqueName(uniqueNames, name);
		}
		return name;
	}
	
	
	/** Generating a unique name for a name proposal which is definitely not in the list of given existing names
	 * 
	 * If the proposed name is already found in the list an '_' and a consecutive number is appended to the proposed name. 
	 * The number incremented until a unique name is found.  
	 * 
	 * @param existingNameList the list of names already existing in the context 
	 * @param nameProposal a proposal for a name as starting point
	 * @return a unique name
	 */
	static private String getUniqueName(HashSet<String> existingNameList, String nameProposal){
		String temp = nameProposal;
		
		int i = 1;
		while (existingNameList.contains(temp)) {
			temp = nameProposal + "_" + i;
			i++;
		}
		return temp;
	}
	
	/**
	 * Returns a unique Instance name
	 * 
	 * @param automationSystem
	 * @param name
	 * @param id
	 * @return
	 */
	public String getSystemUniqueFBInstanceName(
			final AutomationSystem automationSystem, final String name,
			final String id) {
		if (automationSystem != null) {
			Hashtable<String, String> uniqueNames = fbUniqueSystemInstanceNames
					.get(automationSystem);
			if (uniqueNames == null) {
				uniqueNames = new Hashtable<String, String>();
				fbUniqueSystemInstanceNames.put(automationSystem, uniqueNames);
			}
			String temp = name;
			int i = 0;
			if (uniqueNames.containsKey(name)
					&& uniqueNames.get(name).equals(id)) {
				return name;
			}
			while (uniqueNames.containsKey(temp)
					&& !uniqueNames.get(temp).equals(id)) {
				temp = name + "_" + i;
				i++;
			}
			uniqueNames.put(temp, id);
			return temp;
		}
		return name;
	}

	/**
	 * Returns a unique Instance name for a composite FB
	 * 
	 * @param compositeParent
	 * @param newFB
	 * @return
	 */
	public String getCompositeUniqueFBInstanceName(
			CompositeFBType compositeParent, String nameProposal) {
		if (compositeParent != null) {
			//TODO consider to also add the referenced FB here so that the old name is not considered
			HashSet<String> uniqueNames = getNameList(null, compositeParent.getFBNetwork().getFBs()); 
			return getUniqueName(uniqueNames, nameProposal);
		}
		return nameProposal;
	}
	
	public static String getResourceUniqueFBInstanceName(final Resource resourceUT, final String name) {
		HashSet<String> resourceNames = getNameList(resourceUT, resourceUT.getFBs());
		return getUniqueName(resourceNames, name);
	}

	public static String getUniqueResourceInstanceName(final Resource resourceUT, final String name) {
		if(null != resourceUT.eContainer()){
			HashSet<String> resourceNames = getNameList(resourceUT,resourceUT.getDevice().getResource());
			return getUniqueName(resourceNames, name);
		}
		return name;
	}

	public static String getUniqueDeviceInstanceName(final Device deviceUT, final String name) {
		if(null != deviceUT.eContainer()){
			HashSet<String> deviceNames = getNameList(deviceUT, deviceUT.getAutomationSystem().getSystemConfiguration().getSystemConfigurationNetwork().getDevices());
			return getUniqueName(deviceNames, name);
		}
		return name;
	}

	public static String getUniqueElementName(INamedElement element, EObject object, String name){
		if(element instanceof IInterfaceElement){
			return getUniqueInterfaceElementName((IInterfaceElement)element, (FBType)object, name);
		}
		if(element instanceof ECState){
			return getUniqueECCStateName((ECState)element, (ECC)object, name);
		}
		if(element instanceof Algorithm){
			return getUniqueAlgorithmName((Algorithm)element, (BasicFBType)object, name);
		}
		return name;
	}
	
	public static String getUniqueECCStateName(ECState state, ECC ecc, String name) {
		HashSet<String> stateNames = getNameList(state, ecc.getECState());
		return getUniqueName(stateNames, name);
	}

	public static String getUniqueInterfaceElementName(IInterfaceElement iElement, FBType fbType, String name) {
		ArrayList<INamedElement> elements = new ArrayList<INamedElement>();
		elements.addAll(fbType.getInterfaceList().getEventInputs());
		elements.addAll(fbType.getInterfaceList().getEventOutputs());
		elements.addAll(fbType.getInterfaceList().getInputVars());
		elements.addAll(fbType.getInterfaceList().getOutputVars());
		elements.addAll(fbType.getInterfaceList().getPlugs());
		elements.addAll(fbType.getInterfaceList().getSockets());
		if(fbType instanceof BasicFBType){
			elements.addAll(((BasicFBType)fbType).getInternalVars());
		}
		String retVal = checkReservedKeyWords(name);
		int i = 1;
		while (!isUnique(iElement, retVal, elements))  {
			retVal = name + i;
			i++;
		}
		return retVal;
	}

	public static String getUniqueAlgorithmName(Algorithm algorithm, BasicFBType fbType, String name){
		ArrayList<INamedElement> elements = new ArrayList<INamedElement>();
		elements.addAll(fbType.getAlgorithm());
		
		String retVal = name;
		int i = 1;
		while (!isUnique(algorithm, retVal, elements))  {
			retVal = name + i;
			i++;
		}
		return retVal;
	}

	private static boolean isUnique(INamedElement iElement, String name, ArrayList<INamedElement> elements) {
		for (INamedElement element : elements) {
			 if (!element.equals(iElement) && element.getName()
					.toUpperCase().equals(name.toUpperCase())) {
				 return false;
			 }
		}
		return true;
	}
	
	private static String checkReservedKeyWords(String name) {
		if(RESERVED_KEYWORDS.contains(name.toUpperCase())) {
			return name + "1";
		}
		for (DataType dataType : DataTypeLibrary.getInstance().getDataTypesSorted()) {
			if(dataType.getName().equalsIgnoreCase(name)){
				return name + "1";
			}
		}
		return name;
	}
	
	private static HashSet<String> getNameList(INamedElement refElement, EList<?extends INamedElement> elementsList){
		HashSet<String> nameList = new HashSet<>();		
		for (INamedElement element : elementsList) {
			if(!element.equals(refElement)){
				nameList.add(element.getName());
			}
		}		
		return nameList;
	}

}
