/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.validation;

import org.fordiac.ide.model.libraryElement.Connection;

import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;

/**
 * A sample validator interface for {@link org.fordiac.ide.model.ui.ConnectionView}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ConnectionViewValidator {
	boolean validate();

	boolean validateConnectionElement(Connection value);
	boolean validateSource(InterfaceElementView value);
	boolean validateDestination(InterfaceElementView value);

	boolean validateExternalInterfaceElementView(SubAppInterfaceElementView value);

	boolean validateInternalInterfaceElementview(InternalSubAppInterfaceElementView value);
}
