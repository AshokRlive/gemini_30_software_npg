/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.utils.virtualDNS;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Management</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link org.fordiac.ide.utils.virtualDNS.VirtualDNSManagement#getAvailableDNSCollections
 * <em>Available DNS Collections</em>}</li>
 * <li>
 * {@link org.fordiac.ide.utils.virtualDNS.VirtualDNSManagement#getActiveVirtualDNS
 * <em>Active Virtual DNS</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.fordiac.ide.utils.virtualDNS.VirtualDNSPackage#getVirtualDNSManagement()
 * @model
 * @generated
 */
public interface VirtualDNSManagement extends EObject {
	/**
	 * Returns the value of the '<em><b>Available DNS Collections</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link org.fordiac.ide.utils.virtualDNS.VirtualDNSCollection}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Available DNS Collections</em>' containment
	 * reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Available DNS Collections</em>' containment
	 *         reference list.
	 * @see org.fordiac.ide.utils.virtualDNS.VirtualDNSPackage#getVirtualDNSManagement_AvailableDNSCollections()
	 * @model containment="true"
	 * @generated
	 */
	EList<VirtualDNSCollection> getAvailableDNSCollections();

	/**
	 * Returns the value of the '<em><b>Active Virtual DNS</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Virtual DNS</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Active Virtual DNS</em>' reference.
	 * @see #setActiveVirtualDNS(VirtualDNSCollection)
	 * @see org.fordiac.ide.utils.virtualDNS.VirtualDNSPackage#getVirtualDNSManagement_ActiveVirtualDNS()
	 * @model
	 * @generated
	 */
	VirtualDNSCollection getActiveVirtualDNS();

	/**
	 * Sets the value of the '
	 * {@link org.fordiac.ide.utils.virtualDNS.VirtualDNSManagement#getActiveVirtualDNS
	 * <em>Active Virtual DNS</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *          the new value of the '<em>Active Virtual DNS</em>' reference.
	 * @see #getActiveVirtualDNS()
	 * @generated
	 */
	void setActiveVirtualDNS(VirtualDNSCollection value);

	String getReplacedString(String value);

} // VirtualDNSManagement
