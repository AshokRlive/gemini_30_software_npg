/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub App Network</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubAppNetwork#getSubApps <em>Sub Apps</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubAppNetwork#getFBs <em>FBs</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubAppNetwork#getDataConnections <em>Data Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubAppNetwork#getEventConnections <em>Event Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubAppNetwork#getName <em>Name</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.SubAppNetwork#getParentSubApp <em>Parent Sub App</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubAppNetwork()
 * @model
 * @generated
 */
public interface SubAppNetwork extends EObject {
	/**
	 * Returns the value of the '<em><b>Sub Apps</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.SubApp}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Apps</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Apps</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubAppNetwork_SubApps()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SubApp' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<SubApp> getSubApps();

	/**
	 * Returns the value of the '<em><b>FBs</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.FB}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>FBs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FBs</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubAppNetwork_FBs()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FB' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<FB> getFBs();

	/**
	 * Returns the value of the '<em><b>Data Connections</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.DataConnection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Connections</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Connections</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubAppNetwork_DataConnections()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataConnections' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DataConnection> getDataConnections();

	/**
	 * Returns the value of the '<em><b>Event Connections</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.EventConnection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Connections</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Connections</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubAppNetwork_EventConnections()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<EventConnection> getEventConnections();
	
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubAppNetwork_Name()
	 * @model default="" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.SubAppNetwork#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Parent Sub App</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.SubApp#getSubAppNetwork <em>Sub App Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Sub App</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Sub App</em>' reference.
	 * @see #setParentSubApp(SubApp)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getSubAppNetwork_ParentSubApp()
	 * @see org.fordiac.ide.model.libraryElement.SubApp#getSubAppNetwork
	 * @model opposite="subAppNetwork" required="true"
	 * @generated
	 */
	SubApp getParentSubApp();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.SubAppNetwork#getParentSubApp <em>Parent Sub App</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Sub App</em>' reference.
	 * @see #getParentSubApp()
	 * @generated
	 */
	void setParentSubApp(SubApp value);

	/**
	 * Returns all data and eventconnections
	 * @return all data and eventconnections
	 */
//	List<Connection> getConnections();

} // SubAppNetwork
