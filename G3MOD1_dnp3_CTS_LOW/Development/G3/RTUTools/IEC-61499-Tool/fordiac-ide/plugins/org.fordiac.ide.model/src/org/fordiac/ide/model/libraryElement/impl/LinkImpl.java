/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.Link;
import org.fordiac.ide.model.libraryElement.Segment;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.LinkImpl#getCommResource <em>Comm Resource</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.LinkImpl#getSegmentName <em>Segment Name</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.LinkImpl#getSegment <em>Segment</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.LinkImpl#getDevice <em>Device</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LinkImpl extends ConnectionImpl implements Link {
	/**
	 * The default value of the '{@link #getCommResource() <em>Comm Resource</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCommResource()
	 * @generated
	 * @ordered
	 */
	protected static final String COMM_RESOURCE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCommResource() <em>Comm Resource</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCommResource()
	 * @generated
	 * @ordered
	 */
	protected String commResource = COMM_RESOURCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSegmentName() <em>Segment Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSegmentName()
	 * @generated
	 * @ordered
	 */
	protected static final String SEGMENT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSegmentName() <em>Segment Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSegmentName()
	 * @generated
	 * @ordered
	 */
	protected String segmentName = SEGMENT_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSegment() <em>Segment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegment()
	 * @generated
	 * @ordered
	 */
	protected Segment segment;

	/**
	 * The cached value of the '{@link #getDevice() <em>Device</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDevice()
	 * @generated
	 * @ordered
	 */
	protected Device device;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected LinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibraryElementPackage.Literals.LINK;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getCommResource() {
		return commResource;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommResource(String newCommResource) {
		String oldCommResource = commResource;
		commResource = newCommResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.LINK__COMM_RESOURCE, oldCommResource, commResource));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getSegmentName() {
		return segmentName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSegmentName(String newSegmentName) {
		String oldSegmentName = segmentName;
		segmentName = newSegmentName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.LINK__SEGMENT_NAME, oldSegmentName, segmentName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Segment getSegment() {
		if (segment != null && segment.eIsProxy()) {
			InternalEObject oldSegment = (InternalEObject)segment;
			segment = (Segment)eResolveProxy(oldSegment);
			if (segment != oldSegment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LibraryElementPackage.LINK__SEGMENT, oldSegment, segment));
			}
		}
		return segment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Segment basicGetSegment() {
		return segment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSegment(Segment newSegment, NotificationChain msgs) {
		Segment oldSegment = segment;
		segment = newSegment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LibraryElementPackage.LINK__SEGMENT, oldSegment, newSegment);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSegment(Segment newSegment) {
		if (newSegment != segment) {
			NotificationChain msgs = null;
			if (segment != null)
				msgs = ((InternalEObject)segment).eInverseRemove(this, LibraryElementPackage.SEGMENT__OUT_CONNECTIONS, Segment.class, msgs);
			if (newSegment != null)
				msgs = ((InternalEObject)newSegment).eInverseAdd(this, LibraryElementPackage.SEGMENT__OUT_CONNECTIONS, Segment.class, msgs);
			msgs = basicSetSegment(newSegment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.LINK__SEGMENT, newSegment, newSegment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Device getDevice() {
		if (device != null && device.eIsProxy()) {
			InternalEObject oldDevice = (InternalEObject)device;
			device = (Device)eResolveProxy(oldDevice);
			if (device != oldDevice) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LibraryElementPackage.LINK__DEVICE, oldDevice, device));
			}
		}
		return device;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Device basicGetDevice() {
		return device;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDevice(Device newDevice, NotificationChain msgs) {
		Device oldDevice = device;
		device = newDevice;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LibraryElementPackage.LINK__DEVICE, oldDevice, newDevice);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDevice(Device newDevice) {
		if (newDevice != device) {
			NotificationChain msgs = null;
			if (device != null)
				msgs = ((InternalEObject)device).eInverseRemove(this, LibraryElementPackage.DEVICE__IN_CONNECTIONS, Device.class, msgs);
			if (newDevice != null)
				msgs = ((InternalEObject)newDevice).eInverseAdd(this, LibraryElementPackage.DEVICE__IN_CONNECTIONS, Device.class, msgs);
			msgs = basicSetDevice(newDevice, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.LINK__DEVICE, newDevice, newDevice));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.LINK__SEGMENT:
				if (segment != null)
					msgs = ((InternalEObject)segment).eInverseRemove(this, LibraryElementPackage.SEGMENT__OUT_CONNECTIONS, Segment.class, msgs);
				return basicSetSegment((Segment)otherEnd, msgs);
			case LibraryElementPackage.LINK__DEVICE:
				if (device != null)
					msgs = ((InternalEObject)device).eInverseRemove(this, LibraryElementPackage.DEVICE__IN_CONNECTIONS, Device.class, msgs);
				return basicSetDevice((Device)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.LINK__SEGMENT:
				return basicSetSegment(null, msgs);
			case LibraryElementPackage.LINK__DEVICE:
				return basicSetDevice(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibraryElementPackage.LINK__COMM_RESOURCE:
				return getCommResource();
			case LibraryElementPackage.LINK__SEGMENT_NAME:
				return getSegmentName();
			case LibraryElementPackage.LINK__SEGMENT:
				if (resolve) return getSegment();
				return basicGetSegment();
			case LibraryElementPackage.LINK__DEVICE:
				if (resolve) return getDevice();
				return basicGetDevice();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibraryElementPackage.LINK__COMM_RESOURCE:
				setCommResource((String)newValue);
				return;
			case LibraryElementPackage.LINK__SEGMENT_NAME:
				setSegmentName((String)newValue);
				return;
			case LibraryElementPackage.LINK__SEGMENT:
				setSegment((Segment)newValue);
				return;
			case LibraryElementPackage.LINK__DEVICE:
				setDevice((Device)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.LINK__COMM_RESOURCE:
				setCommResource(COMM_RESOURCE_EDEFAULT);
				return;
			case LibraryElementPackage.LINK__SEGMENT_NAME:
				setSegmentName(SEGMENT_NAME_EDEFAULT);
				return;
			case LibraryElementPackage.LINK__SEGMENT:
				setSegment((Segment)null);
				return;
			case LibraryElementPackage.LINK__DEVICE:
				setDevice((Device)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.LINK__COMM_RESOURCE:
				return COMM_RESOURCE_EDEFAULT == null ? commResource != null : !COMM_RESOURCE_EDEFAULT.equals(commResource);
			case LibraryElementPackage.LINK__SEGMENT_NAME:
				return SEGMENT_NAME_EDEFAULT == null ? segmentName != null : !SEGMENT_NAME_EDEFAULT.equals(segmentName);
			case LibraryElementPackage.LINK__SEGMENT:
				return segment != null;
			case LibraryElementPackage.LINK__DEVICE:
				return device != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (commResource: ");
		result.append(commResource);
		result.append(", segmentName: ");
		result.append(segmentName);
		result.append(')');
		return result.toString();
	}

	public IInterfaceElement getIInterfaceElementDestination() {
		return null;
	}

	public IInterfaceElement getIInterfaceElementSource() {
		return null;
	}

	public void setDestination(final IInterfaceElement dest) {
	}

	public void setSource(final IInterfaceElement source) {
	}

} // LinkImpl
