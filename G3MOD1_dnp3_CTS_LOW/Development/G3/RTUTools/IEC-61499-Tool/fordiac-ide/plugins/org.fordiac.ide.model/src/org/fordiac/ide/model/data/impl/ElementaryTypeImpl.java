/**
 * *******************************************************************************
 *  * Copyright (c) 2007 - 2012 4DIAC - consortium.
 *  * All rights reserved. This program and the accompanying materials
 *  * are made available under the terms of the Eclipse Public License v1.0
 *  * which accompanies this distribution, and is available at
 *  * http://www.eclipse.org/legal/epl-v10.html
 *  *
 *  *******************************************************************************
 */
package org.fordiac.ide.model.data.impl;

import org.eclipse.emf.ecore.EClass;

import org.fordiac.ide.model.data.DataPackage;
import org.fordiac.ide.model.data.ElementaryType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Elementary Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ElementaryTypeImpl extends ValueTypeImpl implements ElementaryType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementaryTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.ELEMENTARY_TYPE;
	}

} //ElementaryTypeImpl
