/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.typeimport;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.fordiac.ide.model.Activator;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.Palette.FBTypePaletteEntry;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.AdapterConnection;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.AdapterEvent;
import org.fordiac.ide.model.libraryElement.AdapterFB;
import org.fordiac.ide.model.libraryElement.AdapterType;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.ECAction;
import org.fordiac.ide.model.libraryElement.ECC;
import org.fordiac.ide.model.libraryElement.ECState;
import org.fordiac.ide.model.libraryElement.ECTransition;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBD;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.InputPrimitive;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LD;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.OtherAlgorithm;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;
import org.fordiac.ide.model.libraryElement.Position;
import org.fordiac.ide.model.libraryElement.Rung;
import org.fordiac.ide.model.libraryElement.STAlgorithm;
import org.fordiac.ide.model.libraryElement.Service;
import org.fordiac.ide.model.libraryElement.ServiceInterface;
import org.fordiac.ide.model.libraryElement.ServiceInterfaceFBType;
import org.fordiac.ide.model.libraryElement.ServiceSequence;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;
import org.fordiac.ide.model.libraryElement.SubAppInterfaceList;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.With;
import org.fordiac.ide.typeimport.exceptions.ReferencedTypeNotFoundException;
import org.fordiac.ide.typeimport.exceptions.TypeImportException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Managing class for importing *.fbt files
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 * @author Martijn Rooker (martijn.rooker@profactor.at)
 */

public class FBTImporter implements LibraryElementTags {

	/** The variables. */
	public HashMap<String, VarDeclaration> variables = new HashMap<String, VarDeclaration>();

	/** The internal variables. */
	public HashMap<String, VarDeclaration> internalVariables = new HashMap<String, VarDeclaration>();

	/** The input events. */
	public HashMap<String, Event> inputEvents = new HashMap<String, Event>();
	
	/** The output events. */
	public HashMap<String, Event> outputEvents = new HashMap<String, Event>();

	/** The adapters. */
	public HashMap<String, AdapterDeclaration> adapters = new HashMap<String, AdapterDeclaration>();
	public HashMap<String, Position> adapterPositions = new HashMap<String, Position>();

	/** The algorithm name ec action mapping. */
	public HashMap<String, ArrayList<ECAction>> algorithmNameECActionMapping = new HashMap<String, ArrayList<ECAction>>();

	/** The ec states. */
	public HashMap<String, ECState> ecStates = new HashMap<String, ECState>();
	
	protected FBType type;

	private IFile file;
	
	protected Palette palette;

	/**
	 * Import fb type.
	 * 
	 * @param fbtFile
	 *            the fbt file
	 * @param parseNetwork
	 *            the parse network
	 * @param palette
	 *            the palette
	 * 
	 * @return the fB type
	 * 
	 * @throws ReferencedTypeNotFoundException
	 *             the referenced type not found exception
	 */
	public FBType importType(final IFile fbtFile, final Palette palette){
		
		this.palette = palette;
		file = fbtFile;
		prepareParseDataBuffers();

		try {
			Document document = createDocument();
			if(null != document){
				Element rootNode = document.getDocumentElement();
				type = createType();
				
				configureType();
				
				// parse document and fill type
				return parseType(rootNode);
			}

		} catch (Exception e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		
		return null;

	}

	
	/* Perform basic setup of the type's data structures
	 */
	protected void configureType() {
		Service service = LibraryElementFactory.eINSTANCE.createService();
		type.setService(service);
	}
	
	protected FBType createType(){
		return LibraryElementFactory.eINSTANCE.createFBType();
	}

	protected void prepareParseDataBuffers() {
		algorithmNameECActionMapping.clear();
		inputEvents.clear();
		outputEvents.clear();
		variables.clear();
		ecStates.clear();
		adapters.clear();
		adapterPositions.clear();
	}

	protected Document createDocument() throws ParserConfigurationException,
			SAXException, IOException, CoreException {
		if (file.exists()) {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(false);
			DocumentBuilder db;
			// TODO: set local dtd for validating!
			dbf
					.setAttribute(
							"http://apache.org/xml/features/nonvalidating/load-external-dtd", //$NON-NLS-1$
							Boolean.FALSE);
			db = dbf.newDocumentBuilder();
			Document document = db.parse(file.getContents());
			return document;
		}
		return null;
	}
		
	/**
	 * This method parses the DTD of the imported FBType.
	 * 
	 * @param rootNode
	 *            - The root node in the DTD
	 * @return the FBType that is parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 * @throws ReferencedTypeNotFoundException
	 *             the referenced type not found exception
	 * @throws ParseException 
	 */
	protected FBType parseType(final Node rootNode)
			throws TypeImportException, ReferencedTypeNotFoundException, ParseException {
		if (rootNode.getNodeName().equals(FBTYPE_ELEMENT)) {
			parseTypeNameAndComment(rootNode);
			
			NodeList childNodes = rootNode.getChildNodes();
			for (int i = 0; i < childNodes.getLength(); i++) {
				Node n = childNodes.item(i);
				if (n.getNodeName().equals(IDENTIFICATION_ELEMENT)) {
					type.setIdentification(CommonElementImporter
							.parseIdentification(type, n));
				}
				if (n.getNodeName().equals(VERSION_INFO_ELEMENT)) {
					type.getVersionInfo().add(
							CommonElementImporter.parseVersionInfo(type, n));
				}
				if (n.getNodeName().equals(COMPILER_INFO_ELEMENT)) {
					type.setCompilerInfo(CompilableElementImporter
							.parseCompilerInfo(type, n));
					// parseCompilerInfo(type, n);
				}
				if (n.getNodeName().equals(INTERFACE_LIST_ELEMENT)) {
					parseInterfaceList(type, n);
				}
				if (n.getNodeName().equals(BASIC_F_B_ELEMENT)) {
					type = convertoToBasicType(type);
					parseBasicFB((BasicFBType) type, n);
				}
				if (n.getNodeName().equals(FBNETWORK_ELEMENT)) {
					// parse the composite FBs as last
					type = convertToCompositeType(type);
					parseFBNetwork((CompositeFBType) type, n);
				}			
				
				if (n.getNodeName().equals(SERVICE_ELEMENT)) {
					// type = convertToServiceInterfaceType(type); // FIX:
					// gebenh: every
					// fbtype can have a service
					parseService(type, n);
				}
			}
			if ((type instanceof BasicFBType)
					|| (type instanceof CompositeFBType)
					|| (type instanceof ServiceInterfaceFBType)) {
				return type;
			} else {
				type = convertToServiceInterfaceType(type);
				return type;

			}
		} else {
			throw new ParseException(
					Messages.FBTImporter_PARSE_FBTYPE_PARSEEXCEPTION, 0);
		}
	}

	protected void parseTypeNameAndComment(final Node rootNode) {
		NamedNodeMap map = rootNode.getAttributes();
		Node name = map.getNamedItem(NAME_ATTRIBUTE);
		if (name != null) {
			type.setName(name.getNodeValue());
		}
		Node comment = map.getNamedItem(COMMENT_ATTRIBUTE);
		if (comment != null) {
			type.setComment(comment.getNodeValue());
		}
	}

	/**
	 * This method parses the DTD of a ServiceInterfaceFBType.
	 * 
	 * @param type
	 *            - The ServiceInterfaceFBType that is being parsed
	 * @param n
	 *            - the node of the ServiceInterfaceFBType in the DTD
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	public void parseService(final FBType type, final Node n)
			throws TypeImportException {
		NodeList childNodes = n.getChildNodes();
		NamedNodeMap map = n.getAttributes();
		
		Node rightInterface = map.getNamedItem(RIGHT_INTERFACE_ATTRIBUTE);
		if (rightInterface != null) {
			ServiceInterface rightInter = LibraryElementFactory.eINSTANCE
					.createServiceInterface();
			rightInter.setName(rightInterface.getNodeValue());
			type.getService().setRightInterface(rightInter);
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_SERVICE_INTERFACE_RIGHTINTERFACE_EXCEPTION);
		}
		Node leftInterface = map.getNamedItem(LEFT_INTERFACE_ATTRIBUTE);
		if (leftInterface != null) {
			ServiceInterface leftInter = LibraryElementFactory.eINSTANCE
					.createServiceInterface();
			leftInter.setName(leftInterface.getNodeValue());
			type.getService().setLeftInterface(leftInter);
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_SERVICE_INTERFACE_LEFTINTERFACE_EXCEPTION);
		}
		Node comment = map.getNamedItem(COMMENT_ATTRIBUTE);
		if (comment != null) {
			type.setComment(comment.getNodeValue());
		}
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node node = childNodes.item(i);
			if (node.getNodeName().equals(SERVICE_SEQUENCE_ELEMENT)) {
				parseServiceSequence(type, node);
			}
		}
	}

	/**
	 * This method parses the ServiceSequence of a ServiceInterfaceFBType.
	 * 
	 * @param type
	 *            - The ServiceInterfaceFBType from which the ServiceSequence
	 *            will be parsed
	 * @param node
	 *            - The node in the DTD of the ServiceSequence of the
	 *            ServiceInterfaceFBType that is being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseServiceSequence(final FBType type, final Node node)
			throws TypeImportException {
		ServiceSequence serviceSequence = LibraryElementFactory.eINSTANCE
				.createServiceSequence();
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(SERVICE_TRANSACTION_ELEMENT)) {
				parseServiceTransaction(serviceSequence, n, type);
			}
		}
		NamedNodeMap map = node.getAttributes();
		Node name = map.getNamedItem(NAME_ATTRIBUTE);
		if (name != null) {
			serviceSequence.setName(name.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_SERVICE_SEQUENCE_NAME_EXCEPTION);
		}
		Node comment = map.getNamedItem(COMMENT_ATTRIBUTE);
		if (comment != null) {
			serviceSequence.setComment(comment.getNodeValue());
		}
		type.getService().getServiceSequence().add(serviceSequence);
	}

	/**
	 * This method parses the ServiceTransaction of a ServiceSequence.
	 * 
	 * @param serviceSequence
	 *            - The serviceSequence containing the serviceTransaction that
	 *            is being parsed
	 * @param node
	 *            - The node in the DTD of the serviceTransaction of the
	 *            serviceSequence that is being parsed
	 * @param type
	 *            - The serviceInterfaceFBType containing the serviceTransaction
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseServiceTransaction(final ServiceSequence serviceSequence,
			final Node node, final FBType type) throws TypeImportException {
		ServiceTransaction serviceTransaction = LibraryElementFactory.eINSTANCE
				.createServiceTransaction();
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(INPUT_PRIMITIVE_ELEMENT)) {
				parseInputPrimitive(serviceTransaction, n, type);
			}
			if (n.getNodeName().equals(OUTPUT_PRIMITIVE_ELEMENT)) {
				parseOutputPrimitive(serviceTransaction, n, type);
			}
			serviceSequence.getServiceTransaction().add(serviceTransaction);
		}

	}

	/**
	 * This method parses the OutputPrimitive of a ServiceTransaction.
	 * 
	 * @param serviceTransaction
	 *            - The serviceTransaction containing the OutputPrimitive that
	 *            is being parsed
	 * @param n
	 *            - the node in the DTD of the OutputPrimitive of the
	 *            serviceTransaction that is being parsed
	 * @param type
	 *            - the serviceInterfaceFBType containing the OutputPrimitive
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseOutputPrimitive(
			final ServiceTransaction serviceTransaction, final Node n,
			final FBType type) throws TypeImportException {
		OutputPrimitive outputPrimitive = LibraryElementFactory.eINSTANCE
				.createOutputPrimitive();
		NamedNodeMap map = n.getAttributes();
		Node interFace = map.getNamedItem(INTERFACE_ATTRIBUTE);
		if (interFace != null) {
			if (interFace.getNodeValue().equals(
					type.getService().getLeftInterface().getName())) {
				outputPrimitive.setInterface(type.getService().getLeftInterface());
			} else if (interFace.getNodeValue().equals(
					type.getService().getRightInterface().getName())) {
				outputPrimitive.setInterface(type.getService().getRightInterface());
			}
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_OUTPUT_PRIMITIVE_EXCEPTION);
		}
		Node event = map.getNamedItem(getEventElement());
		if (event != null) {
			outputPrimitive.setEvent(event.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_OUTPUT_PRIMITIVE_EVENT_EXCEPTION);
		}
		Node parameters = map.getNamedItem(PARAMETERS_ATTRIBUTE);
		if (parameters != null) {
			outputPrimitive.setParameters(parameters.getNodeValue());
		}

		serviceTransaction.getOutputPrimitive().add(outputPrimitive);
	}

	/**
	 * This method parses the InputPrimitive of a ServiceTransaction.
	 * 
	 * @param serviceTransaction
	 *            - The serviceTransaction containing the InputPrimitive that is
	 *            being parsed
	 * @param n
	 *            - the node in the DTD of the InputPrimitive of the
	 *            serviceTransaction that is being parsed
	 * @param type
	 *            - the serviceInterfaceFBType containing the InputPrimitive
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseInputPrimitive(
			final ServiceTransaction serviceTransaction, final Node n,
			final FBType type) throws TypeImportException {
		InputPrimitive inputPrimitive = LibraryElementFactory.eINSTANCE
				.createInputPrimitive();
		NamedNodeMap map = n.getAttributes();
		Node interFace = map.getNamedItem(INTERFACE_ATTRIBUTE);
		if (interFace != null) {
			if (interFace.getNodeValue().equals(
					type.getService().getLeftInterface().getName())) {
				inputPrimitive.setInterface(type.getService().getLeftInterface());
			} else if (interFace.getNodeValue().equals(
					type.getService().getRightInterface().getName())) {
				inputPrimitive.setInterface(type.getService().getRightInterface());
			}
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_INPUT_PRIMITIVE_EXCEPTION);
		}
		Node event = map.getNamedItem(getEventElement());
		if (event != null) {
			inputPrimitive.setEvent(event.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_INPUT_PRIMITIVE_EVENT_EXCEPTION);
		}
		Node parameters = map.getNamedItem(PARAMETERS_ATTRIBUTE);
		if (parameters != null) {
			inputPrimitive.setParameters(parameters.getNodeValue());
		}

		serviceTransaction.setInputPrimitive(inputPrimitive);
	}

	/**
	 * This method converts a FBType to a ServiceInterfaceFBType.
	 * 
	 * @param type
	 *            - The FBType that is being converted to ServiceInterfaceFBType
	 * 
	 * @return - A FBType that is converted
	 */
	private FBType convertToServiceInterfaceType(final FBType type) {
		ServiceInterfaceFBType serviceType = LibraryElementFactory.eINSTANCE
				.createServiceInterfaceFBType();
		copyBasicTypeInformation(serviceType, type);
		return serviceType;
	}

	private void copyBasicTypeInformation(FBType dstType, FBType srcType) {
		dstType.setName(srcType.getName());
		dstType.setComment(srcType.getComment());
		dstType.setCompilerInfo(srcType.getCompilerInfo());
		dstType.setInterfaceList(srcType.getInterfaceList());
		dstType.setIdentification(srcType.getIdentification());
		dstType.getVersionInfo().addAll(srcType.getVersionInfo());
		dstType.setService(srcType.getService());
	}

	/**
	 * This method parses a compositeFBType.
	 * 
	 * @param type
	 *            - the CompositeFBType that is being parsed
	 * @param node
	 *            - the node in the DTD of the CompositeFBType that is being
	 *            parsed
	 * @param palette
	 *            the palette
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 * @throws ReferencedTypeNotFoundException
	 *             the referenced type not found exception
	 */
	private void parseFBNetwork(final CompositeFBType type, final Node node) throws TypeImportException,
			ReferencedTypeNotFoundException {
		NodeList childNodes = node.getChildNodes();
		FBNetwork fbNetwork = LibraryElementFactory.eINSTANCE.createFBNetwork();
		type.setFBNetwork(fbNetwork);
		
		if (adapters.size() > 0) {
			for (AdapterDeclaration adapter : adapters.values()) {
				if (!adapter.isIsInput()) {
					if (adapter.getType() instanceof AdapterType) {
						addAdapterFB(type, fbNetwork, adapter, true, palette);
					}
				} else {
					if (adapter.getType() instanceof AdapterType) {
						addAdapterFB(type, fbNetwork, adapter, false, palette);
					}
				}
			}
		}

		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(FB_ELEMENT)) {
				FB fb = parseFB(n, type, palette);
				if( null != fb){
					fb.setResourceTypeFB(false);
					type.getFBNetwork().getFBs().add(fb);
				}
			}
			if (n.getNodeName().equals(EVENT_CONNECTIONS_ELEMENT)) {
				parseEventConnections(n, type.getFBNetwork()
						.getEventConnections());
			}
			if (n.getNodeName().equals(DATA_CONNECTIONS_ELEMENT)) {
				parseDataConnection(n,
						type.getFBNetwork().getDataConnections(), type
								.getFBNetwork().getFBs());
			}
			if (n.getNodeName().equals(ADAPTERCONNECTIONS_ELEMENT)) {
				parseAdapterConnection(n, type.getFBNetwork().getAdapterConnections());
			}

		}
		
	}

	private void addAdapterFB(final CompositeFBType type, FBNetwork fbNetwork,
			AdapterDeclaration adapter, boolean plug, Palette palette) {

		AdapterFB aFB = LibraryElementFactory.eINSTANCE.createAdapterFB();
		aFB.setPaletteEntry(getAdapterPaletEntry(adapter.getTypeName(), palette));
		aFB.setFbtPath("virtual Type");
		aFB.setPlug(plug);
		
		aFB.setResourceTypeFB(true);
		aFB.setName(adapter.getName());
		Position position = adapterPositions.get(adapter.getName());
		if (position == null) {
			position = LibraryElementFactory.eINSTANCE.createPosition();
			position.setX(10);
			position.setY(10);
		}
		
		aFB.setPosition(position);
		aFB.setInterface((InterfaceList) EcoreUtil.copy(aFB.getFBType().getInterfaceList()));

		type.getFBNetwork().getFBs().add(aFB);
	}

	private AdapterTypePaletteEntry getAdapterPaletEntry(String name, Palette palette) {
		List<PaletteEntry> entries = palette.getTypeEntries(name);
		AdapterTypePaletteEntry entry = null;
		if ((entries.size() > 0) && ( entries.get(0) instanceof AdapterTypePaletteEntry)) {
			entry = (AdapterTypePaletteEntry) entries.get(0);
		}
		return entry;
	}

	/**
	 * Parses the fb network.
	 * 
	 * @param fbNetwork
	 *            the fb network
	 * @param node
	 *            the node
	 * @param palette
	 *            the palette
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 * @throws ReferencedTypeNotFoundException
	 *             the referenced type not found exception
	 */
	public void parseFBNetwork(final FBNetwork fbNetwork, final Node node,
			final Palette palette) throws TypeImportException,
			ReferencedTypeNotFoundException {
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(LibraryElementTags.FB_ELEMENT)) {
				FB fb = parseFB(n, null, palette);
				if(null != fb){
					fbNetwork.getFBs().add(fb);
				}
			}
			if (n.getNodeName().equals(
					LibraryElementTags.EVENT_CONNECTIONS_ELEMENT)) {
				parseEventConnections(n, fbNetwork.getEventConnections());
			}
			if (n.getNodeName().equals(
					LibraryElementTags.DATA_CONNECTIONS_ELEMENT)) {
				parseDataConnection(n, fbNetwork.getDataConnections(),
						fbNetwork.getFBs());
			}
			if (n.getNodeName().equals(
					LibraryElementTags.ADAPTERCONNECTIONS_ELEMENT)) {
				parseAdapterConnection(n, fbNetwork.getAdapterConnections());
			}

		}
	}

	/**
	 * This method parses an AdapterConnection.
	 * 
	 * @param n
	 *            - the node in the DTD of the AdapterConnection that is being
	 *            parsed
	 * @param adapterConnections
	 *            - a list of AdapterConnections that are being parsed
	 * @param functionBlocks
	 *            - a list of FBs containing the Source and Destination of the
	 *            AdapterConnection being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseAdapterConnection(final Node n,
			final EList<Connection> adapterConnections) throws TypeImportException {
		NodeList childNodes = n.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node node = childNodes.item(i);
			if (node.getNodeName().equals(CONNECTION_ELEMENT)) {
				AdapterConnection adapterCon = LibraryElementFactory.eINSTANCE
						.createAdapterConnection();
				adapterCon.setResTypeConnection(true);
				NamedNodeMap map = node.getAttributes();
				Node source = map.getNamedItem(SOURCE_ATTRIBUTE);
				if (source != null) {
					String s = source.getNodeValue();
					String[] split = s.split("\\."); //$NON-NLS-1$
					String sourceFB = ""; //$NON-NLS-1$
					String adapter = ""; //$NON-NLS-1$
					if (split.length == 2) {
						sourceFB = split[0];
						adapter = split[1];
					}
					AdapterDeclaration adapt = getAdapter(sourceFB, adapter);
					if (adapt != null) {
						adapterCon.setSource(adapt);
					}
				} else {
					throw new TypeImportException(
							Messages.FBTImporter_ADAPTER_CONNECTION_SOURCE_EXCEPTION);
				}

				Node dest = map.getNamedItem(DESTINATION_ATTRIBUTE);
				if (dest != null) {
					String s = dest.getNodeValue();
					String[] split = s.split("\\."); //$NON-NLS-1$
					String destFB = ""; //$NON-NLS-1$
					String adapter = ""; //$NON-NLS-1$
					if (split.length == 2) {
						destFB = split[0];
						adapter = split[1];
					}
					
					AdapterDeclaration adapt = getAdapter(destFB, adapter);
					if (adapt != null) {
						adapterCon.setDestination(adapt);
					}
				} else {
					throw new TypeImportException(
							Messages.FBTImporter_ADAPTERCONNECTION_DEST_EXCEPTION);
				}

				parseAdditionalConnectionParams(adapterCon, map);
				adapterConnections.add(adapterCon);
			}
		}

	}
	
	/** get the adapter element for the named element
	 * 
	 * @param componentName may be an Fb or subapp name
	 * @param adapterName name of the adapter
	 * @return
	 */
	protected AdapterDeclaration getAdapter(String componentName, String adapterName){
		AdapterDeclaration retVal = null;
		FB fb = findFB(getFBNetwork().getFBs(), componentName);
		if(null != fb){
			retVal = fb.getInterface().getAdapter(adapterName);
		}
		return retVal;
	}
	

	/** parses the comment the dx1, dx2, and the dy attribute of a connection
	 */
	private void parseAdditionalConnectionParams(
			Connection connnection, NamedNodeMap nodeMap) {
		Node comment = nodeMap.getNamedItem(COMMENT_ATTRIBUTE);
		if (comment != null) {
			connnection.setComment(comment.getNodeValue());
		}
		Node dx1 = nodeMap.getNamedItem(DX1_ATTRIBUTE);
		if (dx1 != null) {
			connnection.setDx1(ImportUtils.parseConnectionValue(dx1.getNodeValue()));
		}
		Node dx2 = nodeMap.getNamedItem(DX2_ATTRIBUTE);
		if (dx2 != null) {
			connnection.setDx2(ImportUtils.parseConnectionValue(dx2.getNodeValue()));
		}
		Node dy = nodeMap.getNamedItem(DY_ATTRIBUTE);
		if (dy != null) {
			connnection.setDy(ImportUtils.parseConnectionValue(dy.getNodeValue()));
		}
	}

	/**
	 * This method parses an EventConnection.
	 * 
	 * @param n
	 *            - the node in the DTD of the EventConnection that is being
	 *            parsed
	 * @param eventConnections
	 *            - a list of EventConnections that are being parsed
	 * @param functionBlocks
	 *            - a list of FBs containing the Source and Destination of the
	 *            EventConnection being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	protected void parseEventConnections(final Node n,
			final EList<EventConnection> eventConnections) throws TypeImportException {
		NodeList childNodes = n.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node node = childNodes.item(i);
			if (node.getNodeName().equals(CONNECTION_ELEMENT)) {
				try {
					EventConnection eventCon = LibraryElementFactory.eINSTANCE
							.createEventConnection();
					eventCon.setResTypeConnection(true);
					NamedNodeMap map = node.getAttributes();
					Node source = map.getNamedItem(SOURCE_ATTRIBUTE);
					if (source != null) {
						String s = source.getNodeValue();
						String[] split = s.split("\\."); //$NON-NLS-1$
						String sourceFB = ""; //$NON-NLS-1$
						String event = ""; //$NON-NLS-1$
						if (split.length == 1) {
							event = s;
						}
						if (split.length == 2) {
							sourceFB = split[0];
							event = split[1];
						}
						if (sourceFB.equals("") && !event.equals("")) { //$NON-NLS-1$ //$NON-NLS-2$
							Event ev = inputEvents.get(event);
							if (ev != null) {
								eventCon.setSource(ev);
							}
						} else if (!sourceFB.equals("")) { //$NON-NLS-1$
							Event ev = getEvent(sourceFB, event, false);
							if (ev != null) {
								eventCon.setSource(ev);
							}
						} else {
							// TODO check whether it is necessary to throw an
							// exception
						}
					} else {
						throw new TypeImportException(
								Messages.FBTImporter_EVENT_CONNECTION_SOURCE_EXCEPTIOn);
					}

					Node dest = map.getNamedItem(DESTINATION_ATTRIBUTE);
					if (dest != null) {
						String s = dest.getNodeValue();
						String[] split = s.split("\\."); //$NON-NLS-1$
						String destFB = ""; //$NON-NLS-1$
						String event = ""; //$NON-NLS-1$
						if (split.length == 1) {
							event = s;
						}
						if (split.length == 2) {
							destFB = split[0];
							event = split[1];
						}
						if (destFB.equals("") && !event.equals("")) {//$NON-NLS-1$ //$NON-NLS-2$
							Event ev = outputEvents.get(event);
							if (ev != null) {
								eventCon.setDestination(ev);
							}
						} else if (!destFB.equals("")) { //$NON-NLS-1$
							Event ev = getEvent(destFB, event, true); 
							if (ev != null) {
								eventCon.setDestination(ev);
							}
						} else {
							// TODO check whether it is necessary to throw an
							// exception
						}
					} else {
						throw new TypeImportException(
								Messages.FBTImporter_EVENT_CONNECTION_DEST_EXCEPTION);
					}

					parseAdditionalConnectionParams(eventCon, map);
					eventConnections.add(eventCon);
				} catch (TypeImportException ex) {
					Activator.getDefault().logError( " Skipped connection! ("
									+ (file != null ? (file.getName() + ")")
											: "N/A"),
											ex);
				}
			}
		}
	}
	
	/** get the event element for the named element
	 * 
	 * @param componentName may be an Fb or subapp name
	 * @param eventName name of the event
	 * @param input flag indicating if an input or output event is desired
	 * @return
	 */
	protected Event getEvent(String componentName, String eventName, boolean input){
		Event retVal = null;
		FB fb = findFB(getFBNetwork().getFBs(), componentName);
		if(null != fb){
			if(null != fb.getPaletteEntry()){
				//we have a typed FB
				retVal = fb.getInterface().getEvent(eventName);
				if((null != retVal) && (retVal.isIsInput() != input)){
					retVal = null;
				}
			}else{
				retVal = createEvent(fb, eventName, input);
			}
		}
		return retVal;
	}
	
	private Event createEvent(FB fb, String eventName, boolean input) {
		Event evt = LibraryElementFactory.eINSTANCE.createEvent();
		evt.setName(eventName);
		evt.setIsInput(input);
		if(input){
			fb.getInterface().getEventInputs().add(evt);
		}else{
			fb.getInterface().getEventOutputs().add(evt);
		}
		return evt;
	}

	protected FBNetwork getFBNetwork(){
		if(type instanceof CompositeFBType){
			return ((CompositeFBType)type).getFBNetwork();
		}
		return null;
	}

	/**
	 * This method converts a FBType to a CompositeFBType.
	 * 
	 * @param type
	 *            - The FBType that is being converted to CompositeFBType
	 * 
	 * @return - A FBType that is converted
	 */
	private FBType convertToCompositeType(final FBType type) {
		CompositeFBType compositeType = LibraryElementFactory.eINSTANCE
				.createCompositeFBType();
		copyBasicTypeInformation(compositeType, type);
		return compositeType;
	}

	/**
	 * This method parses a BasicFBType.
	 * 
	 * @param type
	 *            - the basicFBType that is being parsed
	 * @param node
	 *            - the node in the DTD of the BasicFBType that is being parsed
	 * @param palette
	 *            the palette
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 * @throws ReferencedTypeNotFoundException
	 *             the referenced type not found exception
	 */
	private void parseBasicFB(final BasicFBType type, final Node node) throws TypeImportException,
			ReferencedTypeNotFoundException {
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(INTERNAL_VARS_ELEMENT)) {
				parseInternalVars(type, n);
			}
			if (n.getNodeName().equals(ECC_ELEMENT)) {
				parseECC(type, n);
			}
			if (n.getNodeName().equals(ALGORITHM_ELEMENT)) {
				parseAlgorithm(type, n, palette);
			}
		}
	}

	/**
	 * This method parses an Algorithm.
	 * 
	 * @param type
	 *            - the BasicFBType containing the Algorithm
	 * @param n
	 *            - the node in the DTD of the algorithm that is being parsed
	 * @param palette
	 *            the palette
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 * @throws ReferencedTypeNotFoundException
	 *             the referenced type not found exception
	 */
	private void parseAlgorithm(final BasicFBType type, final Node n,
			final Palette palette) throws TypeImportException,
			ReferencedTypeNotFoundException {
		NamedNodeMap map = n.getAttributes();
		Node nameNode = map.getNamedItem(NAME_ATTRIBUTE);
		Node commentNode = map.getNamedItem(COMMENT_ATTRIBUTE);
		String name = null;
		String comment = null;
		if (nameNode != null) {
			name = nameNode.getNodeValue();
		}
		if (commentNode != null) {
			comment = commentNode.getNodeValue();
		}

		NodeList childNodes = n.getChildNodes(); // should have only one
		// child node
		for (int i = 0; i < childNodes.getLength(); i++) {

			Node node = childNodes.item(i);
			String nodeName = node.getNodeName();
			if (nodeName.equals(FBD_ELEMENT)) {
				FBD fbd = LibraryElementFactory.eINSTANCE.createFBD();
				fbd.setName(name);
				fbd.setComment(comment);
				parseFBD(fbd, node, palette);
				type.getAlgorithm().add(fbd);
				ArrayList<ECAction> list = algorithmNameECActionMapping.get(fbd
						.getName());
				if (list != null) {
					for (ECAction action : list) {
						action.setAlgorithm(fbd);
					}
				}
			} else if (nodeName.equals(ST_ELEMENT)) {
				STAlgorithm sT = LibraryElementFactory.eINSTANCE
						.createSTAlgorithm();
				sT.setName(name);
				sT.setComment(comment);
				parseST(sT, node);
				type.getAlgorithm().add(sT);
				ArrayList<ECAction> list = algorithmNameECActionMapping.get(sT
						.getName());
				if (list != null) {
					for (ECAction action : list) {
						action.setAlgorithm(sT);
					}
				}
			} else if (nodeName.equals(LD_ELEMENT)) {
				LD lD = LibraryElementFactory.eINSTANCE.createLD();
				lD.setName(name);
				lD.setComment(comment);
				parseLD(lD, node);
				type.getAlgorithm().add(lD);
				ArrayList<ECAction> list = algorithmNameECActionMapping.get(lD
						.getName());
				if (list != null) {
					for (ECAction action : list) {
						action.setAlgorithm(lD);
					}
				}
			} else if (nodeName.equals(OTHER_ELEMENT)) {
				OtherAlgorithm other = LibraryElementFactory.eINSTANCE
						.createOtherAlgorithm();
				other.setName(name);
				other.setComment(comment);
				parseOtherAlg(other, node);
				type.getAlgorithm().add(other);
				ArrayList<ECAction> list = algorithmNameECActionMapping
						.get(other.getName());
				if (list != null) {
					for (ECAction action : list) {
						action.setAlgorithm(other);
					}
				}
			}

			// throw new FBTImportException(
			// "Algorithm: Unsupported Algorithmtype (only FBD, ST, LD and Other
			// possible)!");
		}
	}

	/**
	 * Parses the other alg.
	 * 
	 * @param other
	 *            the other
	 * @param node
	 *            the node
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseOtherAlg(final OtherAlgorithm other, final Node node)
			throws TypeImportException {
		NamedNodeMap map = node.getAttributes();
		Node language = map.getNamedItem(LANGUAGE_ATTRIBUTE);
		if (language != null) {
			other.setLanguage(language.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_OTHER_ALG_MISSING_LANG_EXCEPTION);
		}

		Node text = map.getNamedItem(TEXT_ATTRIBUTE);
		if (text != null) {
			other.setText(text.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_OTHER_ALG_MISSING_TEXT_EXCEPTION);
		}
	}

	/**
	 * This method parses a Ladder Diagram (LD).
	 * 
	 * @param ld
	 *            - the LD that is being parsed
	 * @param node
	 *            - the node in the DTD of the LD that is being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseLD(final LD ld, final Node node)
			throws TypeImportException {
		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node n = nodeList.item(i);
			if (n.getNodeName().equals(RUNG_ELEMENT)) {
				parseRung(ld, n);
			}
		}
	}

	/**
	 * This method parses a Rung.
	 * 
	 * @param ld
	 *            - the LD containing the Rung that is being parsed
	 * @param node
	 *            - the node in the DTD of the Rung that is being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseRung(final LD ld, final Node node)
			throws TypeImportException {
		Rung rung = LibraryElementFactory.eINSTANCE.createRung();
		NamedNodeMap map = node.getAttributes();
		Node output = map.getNamedItem(OUTPUT_ATTRIBUTE);
		if (output != null) {
			VarDeclaration outputVar = variables.get(output.getNodeValue());
			if (outputVar != null) {
				rung.setOutput(outputVar);
			} else {
				outputVar = internalVariables.get(output.getNodeValue());
				if (outputVar != null) {
					rung.setOutput(outputVar);
				} else {
					throw new TypeImportException(
							Messages.FBTImporter_RUNG_OUTPUTNAME_EXCEPTION);
				}
			}
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_RUNG_OUTPUT_EXCEPTION);
		}

		Node expression = map.getNamedItem(EXPRESSION_ATTRIBUTE);
		if (expression != null) {
			rung.setExpression(expression.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_RUNG_EXPRESSION_EXCEPTION);
		}

		Node comment = map.getNamedItem(COMMENT_ATTRIBUTE);
		if (comment != null) {
			rung.setComment(comment.getNodeValue());
		}
		ld.getRung().add(rung);
	}

	/**
	 * This method parses a STAlgorithm.
	 * 
	 * @param st
	 *            - the STAlgorithm being parsed
	 * @param node
	 *            - the node in the DTD of the STAlgorithm that is being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseST(final STAlgorithm st, final Node node)
			throws TypeImportException {
		NamedNodeMap map = node.getAttributes();
		Node text = map.getNamedItem(TEXT_ATTRIBUTE);
		if (text != null) {
			st.setText(text.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_ST_TEXTNOTSET_EXCEPTION);
		}

	}

	/**
	 * This method parses a Function Block Diagram (FBD).
	 * 
	 * @param fbd
	 *            - the FBD being parsed
	 * @param node
	 *            - the node in the DTD of the FBD that is being parsed
	 * @param palette
	 *            the palette
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 * @throws ReferencedTypeNotFoundException
	 *             the referenced type not found exception
	 */
	private void parseFBD(final FBD fbd, final Node node, final Palette palette)
			throws TypeImportException, ReferencedTypeNotFoundException {
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(FB_ELEMENT)) {
				FB fb = parseFB(n, null, palette);
				if(null != fb){
					fbd.getFB().add(fb);
				}
			}
			if (n.getNodeName().equals(DATA_CONNECTIONS_ELEMENT)) {
				parseDataConnection(n, fbd.getDataConnections(), fbd.getFB());
			}
		}

	}

	/**
	 * Parses the data connection.
	 * 
	 * @param n
	 *            the n
	 * @param dataConnections
	 *            the data connections
	 * @param functionBlocks
	 *            the function blocks
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	protected void parseDataConnection(final Node n,
			final EList<DataConnection> dataConnections,
			final EList<FB> functionBlocks) throws TypeImportException {
		NodeList childNodes = n.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node node = childNodes.item(i);
			if (node.getNodeName().equals(CONNECTION_ELEMENT)) {
				try {
					DataConnection dataCon = LibraryElementFactory.eINSTANCE
							.createDataConnection();
					dataCon.setResTypeConnection(true);
					NamedNodeMap map = node.getAttributes();
					Node source = map.getNamedItem(SOURCE_ATTRIBUTE);
					if (source != null) {
						VarDeclaration varDecl = getConnectionEndPoint(source.getNodeValue(), functionBlocks, false);
						if (null != varDecl) {
							dataCon.setSource(varDecl);
						} else {
							//TODO create error marker and some entity for showing the missing variable in the fb network
						}
					} else {
						throw new TypeImportException(
								Messages.FBTImporter_DATA_CONNECTION_SOURCE_EXCEPTION);
					}
					Node dest = map.getNamedItem(DESTINATION_ATTRIBUTE);
					if (dest != null) {
						VarDeclaration varDecl = getConnectionEndPoint(dest.getNodeValue(), functionBlocks, true);
						if (null != varDecl) {
							dataCon.setDestination(varDecl);
						} else {
							//TODO create error marker and some entity for showing the missing variable in the fb network
						}
					} else {
						throw new TypeImportException(
								Messages.FBTImporter_DATA_CONNECTION_DESTINATION_EXCEPTION);
					}
					parseAdditionalConnectionParams(dataCon, map);					
					dataConnections.add(dataCon);

				} catch (TypeImportException ex) {
					Activator.getDefault().logError(ex.getMessage() + " Skipped connection! ("+ (file != null ? (file.getName() + ")")
											: "N/A"), ex);
				}
			}
		}

	}	
	
	private VarDeclaration getConnectionEndPoint(String endPointIdentifier, final EList<FB> functionBlocks, boolean input) {
		String[] split = endPointIdentifier.split("\\."); //$NON-NLS-1$
		String sourceFB = ""; //$NON-NLS-1$
		String var = ""; //$NON-NLS-1$
		if (split.length == 1) {
			var = endPointIdentifier;
		}
		if (split.length == 2) {
			sourceFB = split[0];
			var = split[1];
		}
		
		VarDeclaration retVal = null;
		if (sourceFB.equals("") && !var.equals("")) {//$NON-NLS-1$ //$NON-NLS-2$
			retVal = variables.get(var);
			if (null == retVal) {
				//check if it is maybe an adapter declaration
				retVal = adapters.get(var);
			}
			if(null != retVal && retVal.isIsInput() == input){
				//for interface elements the is input defintion needs to be reverted i.e., if we search for an input then this means it should be a cfb output
				retVal = null;
			}
		} else if (!sourceFB.equals("")) { //$NON-NLS-1$
			retVal = getVarDeclaration(sourceFB, var, functionBlocks, input);
		} else {
			// TODO check whether it is necessary to throw an
			// exception
		}
		return retVal;
	}


	/** Get the variable from the interface of the element of given name
	 * 
	 * @param componentName may be an Fb or subapp name
	 * @param varName name of the event
	 * @param functionBlocks the list of potential function blocks is needed as otherwise FBD algorithms parsing would be broken 
	 *        TODO check how this could be improved
	 * @param input flag indicating if we are looking for an input or output
	 * @return
	 */
	protected VarDeclaration getVarDeclaration(String componentName, String varName, final EList<FB> functionBlocks, boolean input){
		VarDeclaration retVal = null;
		FB found = findFB(functionBlocks, componentName);
		if (found != null) {
			retVal = getVarNamed(found.getInterface(), varName, input);
		}		
		return retVal;
	}

	private VarDeclaration getVarNamed(SubAppInterfaceList subAppInterfaceList, String varName, boolean input) {
		VarDeclaration retVal;
		boolean hasType = true;
		
		if(subAppInterfaceList.eContainer() instanceof FB){
			//only if it is an FB check if it is typed
			hasType = (null != ((FB)subAppInterfaceList.eContainer()).getPaletteEntry());  
		}

		if(hasType){
			//we have a typed FB
			retVal = subAppInterfaceList.getVariable(varName);
			if((null != retVal) && (retVal.isIsInput() != input)){
				retVal = null;
			}
		}else{
			//if we couldn't load the type create the interface entry
			retVal = createVarDecl(subAppInterfaceList, varName, input);
		}
		return retVal;
	}

	private VarDeclaration createVarDecl(SubAppInterfaceList subAppInterfaceList, String varName, boolean input) {
		VarDeclaration var = LibraryElementFactory.eINSTANCE.createVarDeclaration();
		var.setName(varName);
		var.setIsInput(input);
		if(input){
			subAppInterfaceList.getInputVars().add(var);
		}else{
			subAppInterfaceList.getOutputVars().add(var);
		}
		return var;
	}

	/**
	 * This method searches for a FB in the collection of FBs.
	 * 
	 * @param functionBlocks
	 *            - a list of FBs
	 * @param fbName
	 *            - the name of the FB that is being searched for
	 * 
	 * @return - the found FB, or null if the FB is not in the list
	 */
	private static FB findFB(final EList<FB> functionBlocks, final String fbName) {
		for (FB fb : functionBlocks) {
			if (fb.getName().equals(fbName)) {
				return fb;
			}
		}
		return null;
	}

	/**
	 * Parses the fb.
	 * 
	 * @param n
	 *            the n
	 * @param fbType
	 *            the fb type
	 * @param palette
	 *            the palette
	 * 
	 * @return the fB
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 * @throws ReferencedTypeNotFoundException
	 *             the referenced type not found exception
	 */
	public FB parseFB(final Node n, final FBType fbType, final Palette palette)
			throws TypeImportException, ReferencedTypeNotFoundException {
		
		FB fb = LibraryElementFactory.eINSTANCE.createFB();
		if (fbType instanceof CompositeFBType){
			fb.setParentCompositeFBType((CompositeFBType)fbType);
		}
		fb.setResourceTypeFB(true);
		
		NamedNodeMap map = n.getAttributes();
		Node name = map.getNamedItem(NAME_ATTRIBUTE);
		if (name != null) {
			fb.setName(name.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_INSTANCENAME_EXCEPTION);
		}
		Node type = map.getNamedItem(TYPE_ATTRIBUTE);
		if (type != null) {
			// FIXME this can lead to problems if typename exists several times!
			List<PaletteEntry> entries = palette.getTypeEntries(type.getNodeValue());
			PaletteEntry entry = null;
			
			if (entries.size() > 0) {
				entry = entries.get(0);
			}

			if (entry instanceof FBTypePaletteEntry) {
				fb.setPaletteEntry(entry);
				fb.setFbtPath(entry.getProjectRelativeTypePath());				
				fb.setInterface((InterfaceList) EcoreUtil.copy(fb.getFBType().getInterfaceList()));				
			} else {
				createFBTypeProblemMarker(IMarker.SEVERITY_ERROR, Messages.FBTImporter_REQUIRED_FB_TYPE_EXCEPTION + type.getNodeValue() + " not available");
				//as we don't have type information we create an empty interface list 
				InterfaceList interfaceList = LibraryElementFactory.eINSTANCE.createInterfaceList();
				fb.setInterface(interfaceList);				
				//TODO add attribute value for missing instance name and indicate that FB is missing for usage in outline views
			}
			configureParameters(fb.getInterface(), n.getChildNodes()); 
			// #933 CompositeEditor - setting constant values not possible
			// initialize the value element of all vardeclarations which have no parameters 
			
			for (VarDeclaration var : fb.getInterface().getInputVars()) {
				if (var.getValue() == null) {
					var.setValue(LibraryElementFactory.eINSTANCE.createValue());
				}
			}
		} else {
			throw new TypeImportException(Messages.FBTImporter_FB_TYPE_EXCEPTION);
		}

		Node comment = map.getNamedItem(COMMENT_ATTRIBUTE);
		if (comment != null) {
			fb.setComment(comment.getNodeValue());
		}
		Position pos = getXandY(map);
		fb.setPosition(pos);
		return fb;
	}

	private IMarker createFBTypeProblemMarker(int severity, String message) {
		IMarker marker = null;		
		if(null != file){
			try {
				marker = file.createMarker(IMarker.PROBLEM);
				marker.setAttribute(IMarker.SEVERITY, severity);
				marker.setAttribute(IMarker.MESSAGE, message);
			} catch (CoreException e) {
				Activator.getDefault().logError(e.getMessage(), e);
			}
		}
		return marker;
	}

	protected void configureParameters(SubAppInterfaceList subAppInterfaceList, NodeList childNodes) throws TypeImportException {
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node node = childNodes.item(i);
			if (node.getNodeName().equals(PARAMETER_ELEMENT)) {
				VarDeclaration paramter = ImportUtils.parseParameter(node);				
				VarDeclaration vInput = getVarNamed(subAppInterfaceList, paramter.getName(), true);
				if(null != vInput){
					vInput.setValue(paramter.getValue());
				}
			}
		}
	}

	/**
	 * Gets the xand y.
	 * 
	 * @param map
	 *            the map
	 * 
	 * @return the xand y
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	protected Position getXandY(final NamedNodeMap map)
			throws TypeImportException {
		Position pos = LibraryElementFactory.eINSTANCE.createPosition();
		try {
			Node x = map.getNamedItem(X_ATTRIBUTE);
			if (x != null) {
				String xValueString = x.getNodeValue();
				double xValue = 0.0;
				if ((null != xValueString) && (xValueString.length() != 0)){
					xValue = ImportUtils.convertCoordinate(Double
							.parseDouble(x.getNodeValue()));
				
				}
				else{
					Activator.getDefault().logWarning(Messages.FBTImporter_POSITION_X_WRONG);
				}
				pos.setX((int) xValue);
			}
			Node y = map.getNamedItem(Y_ATTRIBUTE);
			if (y != null) {
				String yValueString = y.getNodeValue();
				double yValue = 0;
				if ((null != yValueString) && (yValueString.length() != 0)){
					yValue = ImportUtils.convertCoordinate(Double
							.parseDouble(y.getNodeValue()));					
				}
				else{
					Activator.getDefault().logWarning(Messages.FBTImporter_POSITION_Y_WRONG);
				}
				pos.setY((int) yValue);
			}
		} catch (NumberFormatException nfe) {
			throw new TypeImportException(
					Messages.FBTImporter_POSITION_EXCEPTION);
		}
		return pos;
	}

	/**
	 * This method parses an ECC.
	 * 
	 * @param type
	 *            - the BasicFBType containing the ECC being parsed
	 * @param n
	 *            - the node in the DTD of the ECC being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseECC(final BasicFBType type, final Node n)
			throws TypeImportException {
		NodeList childNodes = n.getChildNodes();
		ECC ecc = LibraryElementFactory.eINSTANCE.createECC();
		boolean first = true;
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node node = childNodes.item(i);
			if (node.getNodeName().equals(ECSTATE_ELEMENT)) {
				parseECState(ecc, node, first); // IEC 61499 ->
				// "START" state is the
				// first in the list
				first = false;
			}
			if (node.getNodeName().equals(ECTRANSITION_ELEMENT)) {
				parseECTransition(ecc, node);
			}
		}
		type.setECC(ecc);
	}

	/**
	 * This method parses an ECTransition.
	 * 
	 * @param ecc
	 *            - the ECC containing the ECTransition being parsed
	 * @param node
	 *            - the node in the DTD of the ECTransition being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseECTransition(final ECC ecc, final Node node)
			throws TypeImportException {
		NamedNodeMap map = node.getAttributes();
		ECTransition ecTransition = LibraryElementFactory.eINSTANCE
				.createECTransition();
		ecc.getECTransition().add(ecTransition);
		Node source = map.getNamedItem(SOURCE_ATTRIBUTE);
		if (source != null) {
			ECState state = ecStates.get(source.getNodeValue());
			if (state != null) {
				ecTransition.setSource(state);
			}
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_ECTRANSITION_SOURCE_EXCEPTION);
		}
		Node destination = map.getNamedItem(DESTINATION_ATTRIBUTE);
		if (destination != null) {
			ECState state = ecStates.get(destination.getNodeValue());
			if (state != null) {
				ecTransition.setDestination(state);
			}

		} else {
			throw new TypeImportException(
					Messages.FBTImporter_ECTRANSITION_DEST_EXCEPTION);
		}
		Node condition = map.getNamedItem(CONDITION_ATTRIBUTE);
		if (condition != null) {
			validateTransitionCondition(ecTransition, condition.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_ECTRANASITION_CONDITION_EXCEPTION);
		}
		Node comment = map.getNamedItem(COMMENT_ATTRIBUTE);
		if (comment != null) {
			ecTransition.setComment(comment.getNodeValue());
		}
		Position pos = getXandY(map);
		ecTransition.setPosition(pos);
	}

	private void validateTransitionCondition(ECTransition ecTransition,
			String condition) throws TypeImportException {
		Event event;
		String expression;
		
		// first, try splitting according to 1st edition
		String[] split = condition.split("&", 2);
		event = inputEvents.get(split[0].trim());
		if(event != null) {
			// remainder is expression
			expression = split.length > 1 ? split[1].trim() : "";
		}
		else { // otherwise, try splitting according to 2nd edition
			split = condition.split("\\[", 2);
			event = inputEvents.get(split[0].trim());
			if(event != null) {
				// remainder is expression (except trailing ']')
				expression = split.length > 1 ? split[1].substring(0, split[1].lastIndexOf("]")).trim() : "";
			}
			else {
				// no match (all is expression)
				if(condition.startsWith("[")){
					expression = condition.substring(1, condition.lastIndexOf("]")); 
				}else{
					expression = condition;
				}
			}
		}
		
		ecTransition.setConditionEvent(event);
		ecTransition.setConditionExpression(expression);
	}

	/**
	 * This method parses an ECState.
	 * 
	 * @param ecc
	 *            - the ECC containing the ECState being parsed
	 * @param node
	 *            - the node in the DTD of the ECState being parsed
	 * @param initialState
	 *            the initial state
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseECState(final ECC ecc, final Node node,
			final Boolean initialState) throws TypeImportException {
		NodeList childNodes = node.getChildNodes();
		ECState state = LibraryElementFactory.eINSTANCE.createECState();
		ecc.getECState().add(state);
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(ECACTION_ELEMENT)) {
				parseECAction(state, n);
			}
		}
		NamedNodeMap map = node.getAttributes();
		Node name = map.getNamedItem(NAME_ATTRIBUTE);
		if (name != null) {
			state.setName(name.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_ECSTATE_NAME_EXCEPTION);
		}
		Node comment = map.getNamedItem(COMMENT_ATTRIBUTE);
		if (comment != null) {
			state.setComment(comment.getNodeValue());
		}
		Position pos = getXandY(map);
		state.setPosition(pos);
		ecStates.put(state.getName(), state);
		if (initialState) {
			// have to be "START"
			ecc.setStart(state);
		}
	}

	/**
	 * This method parses an ECAction.
	 * 
	 * @param type
	 *            - the ECState belonging to the ECAction being parsed
	 * @param n
	 *            - the node in the DTD of the ECAction being parsed
	 * 
	 * @throws TypeImportException
	 */
	private void parseECAction(final ECState type, final Node n) {
		NamedNodeMap map = n.getAttributes();
		ECAction ecAction = LibraryElementFactory.eINSTANCE.createECAction();
		type.getECAction().add(ecAction);
		Node algorithm = map.getNamedItem(ALGORITHM_ELEMENT);
		if (algorithm != null) {
			if (algorithmNameECActionMapping.containsKey(algorithm
					.getNodeValue())) {
				algorithmNameECActionMapping.get(algorithm.getNodeValue()).add(
						ecAction);
			} else {
				ArrayList<ECAction> temp = new ArrayList<ECAction>();
				temp.add(ecAction);
				algorithmNameECActionMapping
						.put(algorithm.getNodeValue(), temp);
			}
		}
		Node output = map.getNamedItem(OUTPUT_ATTRIBUTE);
		if (output != null) {
			Event outp = outputEvents.get(output.getNodeValue());
			if (outp != null) {
				ecAction.setOutput(outp);
			}
		}

	}

	/**
	 * This method parses Internal Variables of a BasicFBType.
	 * 
	 * @param type
	 *            - the BasicFBType of which the Internal Variables will be
	 *            parsed
	 * @param n
	 *            - the node in the DTD of the Internal Variable being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseInternalVars(final BasicFBType type, final Node n)
			throws TypeImportException {
		NodeList childNodes = n.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node node = childNodes.item(i);
			if (node.getNodeName().equals(VAR_DECLARATION_ELEMENT)) {
				VarDeclaration v = ImportUtils.parseVarDeclaration(node);
				type.getInternalVars().add(v);
				internalVariables.put(v.getName(), v);
			}
		}
	}

	/**
	 * This method parses a FBType to a BasicFBType.
	 * 
	 * @param type
	 *            - the FBType being parsed to BasicFBType
	 * 
	 * @return the basicFBType
	 */
	private FBType convertoToBasicType(final FBType type) {
		BasicFBType basicType = LibraryElementFactory.eINSTANCE
				.createBasicFBType();
		copyBasicTypeInformation(basicType, type);
		return basicType;
	}

	/**
	 * This method parses the InterfaceList of a FBType.
	 * 
	 * @param type
	 *            - the FBType containing the InterfaceList being parsed
	 * @param node
	 *            - the node in the DTD of the interfaceList being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	protected void parseInterfaceList(final FBType type, final Node node) throws TypeImportException {
		InterfaceList interfaceList = LibraryElementFactory.eINSTANCE
				.createInterfaceList();
		type.setInterfaceList(interfaceList);
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(getEventInputElement())) {
				parseEventInputs(type, n);
			}
			if (n.getNodeName().equals(getEventOutputElement())) {
				parseEventOutputs(type, n);
			}
			if (n.getNodeName().equals(INPUT_VARS_ELEMENT)) {
				List<VarDeclaration> vars = ImportUtils.parseInputVariables(n);
				for (Iterator<VarDeclaration> iterator = vars.iterator(); iterator
						.hasNext();) {
					VarDeclaration v = iterator.next();
					interfaceList.getInputVars().add(v);
					variables.put(v.getName(), v);
				}
			}
			if (n.getNodeName().equals(OUTPUT_VARS_ELEMENT)) {
				List<VarDeclaration> vars = ImportUtils.parseOutputVariables(n);
				for (Iterator<VarDeclaration> iterator = vars.iterator(); iterator
						.hasNext();) {
					VarDeclaration v = iterator.next();
					interfaceList.getOutputVars().add(v);
					variables.put(v.getName(), v);
				}
			}
			if (n.getNodeName().equals(SOCKETS_ELEMENT)) {
				parseSockets(type, n, palette);
			}
			if (n.getNodeName().equals(PLUGS_ELEMENT)) {
				parsePlugs(type, n, palette);
			}
		}
		parseWithConstructs(childNodes);
	}

	protected String getEventOutputElement() {
		return EVENT_OUTPUTS;
	}

	protected String getEventInputElement() {
		return EVENT_INPUTS_ELEMENT;
	}

	/**
	 * This method parses Plugs of a FBType.
	 * 
	 * @param type
	 *            - the FBType containing the Plugs
	 * @param node
	 *            - the node in the DTD of the Plug being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parsePlugs(final FBType type, final Node node, Palette palette)
			throws TypeImportException {
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(ADAPTER_DECLARATION_ELEMENT)) {
				AdapterDeclaration a = parseAdapterDeclaration(n, palette);
				a.setIsInput(false);
				adapters.put(a.getName(), a);
				type.getInterfaceList().getPlugs().add(a);
				type.getInterfaceList().getOutputVars().add(a);
				if (a.getType() instanceof AdapterType) {
					addAdapterEventInputs(((AdapterType)a.getType()).getInterfaceList().getEventInputs(), a);
					addAdapterEventOutputs(((AdapterType)a.getType()).getInterfaceList().getEventOutputs(), a);
				}
			}

		}
	}

	private void addAdapterEventOutputs(EList<Event> eventOutputs,
			AdapterDeclaration a) {
		for (Event event : eventOutputs) {
			AdapterEvent ae = ImportUtils.createAdapterEvent(event, a);
			outputEvents.put(ae.getName(), ae);
		}
		
	}

	private void addAdapterEventInputs(EList<Event> eventInputs,
			AdapterDeclaration a) {
		for (Event event : eventInputs) {
			AdapterEvent ae = ImportUtils.createAdapterEvent(event, a);
			inputEvents.put(ae.getName(), ae);
		}		
	}

	/**
	 * This method parses Sockets of a FBType.
	 * 
	 * @param type
	 *            - the FBType containing the Sockets
	 * @param node
	 *            - the node in the DTD of the Socket being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseSockets(final FBType type, final Node node,
			Palette palette) throws TypeImportException {
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(ADAPTER_DECLARATION_ELEMENT)) {
				AdapterDeclaration a = parseAdapterDeclaration(n, palette);
				//TODO handle if type was not found!
				a.setIsInput(true);
				adapters.put(a.getName(), a);
				type.getInterfaceList().getSockets().add(a);
				type.getInterfaceList().getInputVars().add(a);
				if ((AdapterType)a.getType() != null && ((AdapterType)a.getType()).getInterfaceList() != null) {
					addAdapterEventInputs(((AdapterType)a.getType()).getInterfaceList().getEventOutputs(), a);	
				}
				if ((AdapterType)a.getType() != null && ((AdapterType)a.getType()).getInterfaceList() != null) {
					addAdapterEventOutputs(((AdapterType)a.getType()).getInterfaceList().getEventInputs(), a);	
				}
				
			}
		}

	}

	/**
	 * This method parses AdapterDeclaration.
	 * 
	 * @param node
	 *            - the node in the DTD of the AdapterDeclaration being parsed
	 * 
	 * @return a - the AdapterDeclaration
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private AdapterDeclaration parseAdapterDeclaration(final Node node,
			Palette palette) throws TypeImportException {
		AdapterDeclaration a = LibraryElementFactory.eINSTANCE
				.createAdapterDeclaration();
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(PARAMETER_ELEMENT)) {
				// TODO !! import parameter of adapterDeclaration
				// Parameter p = ImportUtils.parseParameter(node);
				// a.getParameter().add(p);
			}
		}
		NamedNodeMap map = node.getAttributes();
		Node name = map.getNamedItem(NAME_ATTRIBUTE);
		if (name != null) {
			a.setName(name.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_ADAPTERDECLARATION_NAME_EXCEPTION);
		}
		Node type = map.getNamedItem(LibraryElementTags.TYPE_ATTRIBUTE);
		if (type != null) {
			AdapterTypePaletteEntry entry = getAdapterPaletEntry(type.getNodeValue(), palette);
			AdapterType dataType = null;
			if (entry != null) {
				dataType = entry.getAdapterType();
			}
			a.setTypeName(type.getNodeValue());
			if (dataType != null) {
				a.setType(dataType);
			}
		} else {
			throw new TypeImportException(
					Messages.FBTImporter_ADAPTER_DECLARATION_TYPE_EXCEPTION);
		}
		Node comment = map.getNamedItem(COMMENT_ATTRIBUTE);
		if (comment != null) {
			a.setComment(comment.getNodeValue());
		}
		Position pos = getXandY(map);
		adapterPositions.put(a.getName(), pos);
		return a;
	}

	/**
	 * This method parses WithConstructs.
	 * 
	 * @param childNodes
	 *            - the childNodes in the DTD containing the WithConstructs
	 *            being parsed
	 */
	private void parseWithConstructs(final NodeList childNodes) {
		parseWithConstructs(childNodes, inputEvents, outputEvents, variables);
	}

	public void parseWithConstructs(final NodeList childNodes,
			HashMap<String, Event> eventInputs,
			HashMap<String, Event> eventOutputs,
			HashMap<String, VarDeclaration> variables) {
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(getEventInputElement())) {
				NodeList inputEvents = n.getChildNodes();
				for (int j = 0; j < inputEvents.getLength(); j++) {
					Node eventNode = inputEvents.item(j);
					if (eventNode.getNodeName().equals(getEventElement())) {
						NamedNodeMap map = eventNode.getAttributes();
						Node name = map.getNamedItem(NAME_ATTRIBUTE);
						Event e = eventInputs.get(name.getNodeValue());
						NodeList withs = eventNode.getChildNodes();
						for (int k = 0; k < withs.getLength(); k++) {
							Node with = withs.item(k);
							if (with.getNodeName().equals(WITH_ELEMENT)) {
								NamedNodeMap withAttributes = with
										.getAttributes();
								Node var = withAttributes
										.getNamedItem(VAR_ATTRIBUTE);
								if (var != null) {
									With withConstruct = LibraryElementFactory.eINSTANCE
											.createWith();
									e.getWith().add(withConstruct);
									VarDeclaration v = variables.get(var
											.getNodeValue());
									withConstruct.setVariables(v);
								}
							}
						}
					}
				}
			}
			if (n.getNodeName().equals(getEventOutputElement())) {
				NodeList outputEvents = n.getChildNodes();
				for (int j = 0; j < outputEvents.getLength(); j++) {
					Node eventNode = outputEvents.item(j);
					if (eventNode.getNodeName().equals(getEventElement())) {
						NamedNodeMap map = eventNode.getAttributes();
						Node name = map.getNamedItem(NAME_ATTRIBUTE);
						Event e = eventOutputs.get(name.getNodeValue());
						NodeList withs = eventNode.getChildNodes();
						for (int k = 0; k < withs.getLength(); k++) {
							Node with = withs.item(k);
							if (with.getNodeName().equals(WITH_ELEMENT)) {
								NamedNodeMap withAttributes = with
										.getAttributes();
								Node var = withAttributes
										.getNamedItem(VAR_ATTRIBUTE);
								if (var != null) {
									With withConstruct = LibraryElementFactory.eINSTANCE
											.createWith();
									e.getWith().add(withConstruct);
									VarDeclaration v = variables.get(var
											.getNodeValue());
									withConstruct.setVariables(v);
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * This method parses EventInputs of FBTypes.
	 * 
	 * @param fBtype
	 *            - the FBType containing the EventInputs being parsed
	 * @param node
	 *            - the node in the DTD of the EventInputs being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseEventInputs(final FBType fBtype, final Node node)
			throws TypeImportException {
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(getEventElement())) {
				Event e = ImportUtils.parseEvent(n);
				((IInterfaceElement) e).setIsInput(true);
				inputEvents.put(e.getName(), e);
				fBtype.getInterfaceList().getEventInputs().add(e);
			}
		}
	}

	protected String getEventElement() {
		return EVENT_ELEMENT;
	}

	/**
	 * This method parses EventOutputs of FBTypes.
	 * 
	 * @param fBtype
	 *            - the FBType containing the EventOutputs being parsed
	 * @param node
	 *            - the node in the DTD of the EventOutputs being parsed
	 * 
	 * @throws TypeImportException
	 *             the FBT import exception
	 */
	private void parseEventOutputs(final FBType fBtype, final Node node)
			throws TypeImportException {
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);
			if (n.getNodeName().equals(getEventElement())) {

				Event e = ImportUtils.parseEvent(n);
				outputEvents.put(e.getName(), e);
				((IInterfaceElement) e).setIsInput(false);
				fBtype.getInterfaceList().getEventOutputs().add(e);
			}
		}
	}

	/**
	 * This method returns a list with all the data types that are referenced by
	 * the imported FBTypes.
	 * 
	 * @param file
	 *            - the file that is being checked if it has references
	 * 
	 * @return references - a list containing all the references
	 */
	public static List<String> getReferencedDataTypes(final File file) {
		ArrayList<String> references = new ArrayList<String>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;

		dbf
				.setAttribute(
						"http://apache.org/xml/features/nonvalidating/load-external-dtd", //$NON-NLS-1$
						Boolean.FALSE);
		try {
			db = dbf.newDocumentBuilder();
			Document document = db.parse(file);
			// parse document for "FBNetwork" tag
			Node rootNode = document.getDocumentElement();
			NodeList childNodes = rootNode.getChildNodes();
			for (int i = 0; i < childNodes.getLength(); i++) {
				Node n = childNodes.item(i);
				if (n.getNodeName().equals(VAR_DECLARATION_ELEMENT)) {
					String dataType = ""; //$NON-NLS-1$
					dataType = n.getAttributes().getNamedItem(TYPE_ATTRIBUTE)
							.getNodeValue();
					references.add(dataType);
				}

			}
		} catch (Exception e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		return references;

	}

	/**
	 * This method returns a list with all the types that are referenced by the
	 * imported FBTypes.
	 * 
	 * @param file
	 *            - the file that is being checked if it has references
	 * 
	 * @return references - a list containing all the references
	 */
	public static List<String> getReferencedFBTypes(final File file) {
		ArrayList<String> references = new ArrayList<String>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;

		dbf
				.setAttribute(
						"http://apache.org/xml/features/nonvalidating/load-external-dtd", //$NON-NLS-1$
						Boolean.FALSE);
		try {
			db = dbf.newDocumentBuilder();
			Document document = db.parse(file);
			// parse document for "FBNetwork" tag
			Node rootNode = document.getDocumentElement();
			NodeList childNodes = rootNode.getChildNodes();
			for (int i = 0; i < childNodes.getLength(); i++) {
				Node n = childNodes.item(i);
				if (n.getNodeName().equals(FBNETWORK_ELEMENT)) {
					// add nodes to NodeList
					for (int j = 0; j < n.getChildNodes().getLength(); j++) {
						Node node = n.getChildNodes().item(j);
						if (node.getNodeName().equals(FB_ELEMENT)) {
							String fbType = ""; //$NON-NLS-1$
							fbType = node.getAttributes().getNamedItem(
									TYPE_ATTRIBUTE).getNodeValue();
							references.add(fbType);
						}

					}
				}
			}
		} catch (Exception e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		return references;
	}

}
