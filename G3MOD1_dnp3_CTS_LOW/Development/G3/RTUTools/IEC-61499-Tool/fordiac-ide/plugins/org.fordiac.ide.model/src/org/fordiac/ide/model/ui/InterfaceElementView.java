/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.eclipse.emf.common.util.EList;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface Element View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.InterfaceElementView#getIInterfaceElement <em>IInterface Element</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.InterfaceElementView#getInConnections <em>In Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.InterfaceElementView#getOutConnections <em>Out Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.InterfaceElementView#getLabel <em>Label</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.InterfaceElementView#getMappedInterfaceElement <em>Mapped Interface Element</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.InterfaceElementView#getFbNetwork <em>Fb Network</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.InterfaceElementView#getApplicationInterfaceElement <em>Application Interface Element</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getInterfaceElementView()
 * @model
 * @generated
 */
public interface InterfaceElementView extends View {
	/**
	 * Returns the value of the '<em><b>IInterface Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>IInterface Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IInterface Element</em>' reference.
	 * @see #setIInterfaceElement(IInterfaceElement)
	 * @see org.fordiac.ide.model.ui.UiPackage#getInterfaceElementView_IInterfaceElement()
	 * @model required="true"
	 * @generated
	 */
	IInterfaceElement getIInterfaceElement();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.InterfaceElementView#getIInterfaceElement <em>IInterface Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IInterface Element</em>' reference.
	 * @see #getIInterfaceElement()
	 * @generated
	 */
	void setIInterfaceElement(IInterfaceElement value);

	/**
	 * Returns the value of the '<em><b>In Connections</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.ui.ConnectionView}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.ConnectionView#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Connections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Connections</em>' reference list.
	 * @see org.fordiac.ide.model.ui.UiPackage#getInterfaceElementView_InConnections()
	 * @see org.fordiac.ide.model.ui.ConnectionView#getDestination
	 * @model opposite="destination"
	 * @generated
	 */
	EList<ConnectionView> getInConnections();

	/**
	 * Returns the value of the '<em><b>Out Connections</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.ui.ConnectionView}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.ConnectionView#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Connections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Connections</em>' reference list.
	 * @see org.fordiac.ide.model.ui.UiPackage#getInterfaceElementView_OutConnections()
	 * @see org.fordiac.ide.model.ui.ConnectionView#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<ConnectionView> getOutConnections();

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.fordiac.ide.model.ui.UiPackage#getInterfaceElementView_Label()
	 * @model
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.InterfaceElementView#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Mapped Interface Element</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.InterfaceElementView#getApplicationInterfaceElement <em>Application Interface Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapped Interface Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mapped Interface Element</em>' reference.
	 * @see #setMappedInterfaceElement(InterfaceElementView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getInterfaceElementView_MappedInterfaceElement()
	 * @see org.fordiac.ide.model.ui.InterfaceElementView#getApplicationInterfaceElement
	 * @model opposite="applicationInterfaceElement"
	 * @generated
	 */
	InterfaceElementView getMappedInterfaceElement();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.InterfaceElementView#getMappedInterfaceElement <em>Mapped Interface Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mapped Interface Element</em>' reference.
	 * @see #getMappedInterfaceElement()
	 * @generated
	 */
	void setMappedInterfaceElement(InterfaceElementView value);

	/**
	 * Returns the value of the '<em><b>Fb Network</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fb Network</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fb Network</em>' reference.
	 * @see #setFbNetwork(SubAppNetwork)
	 * @see org.fordiac.ide.model.ui.UiPackage#getInterfaceElementView_FbNetwork()
	 * @model
	 * @generated
	 */
	SubAppNetwork getFbNetwork();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.InterfaceElementView#getFbNetwork <em>Fb Network</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fb Network</em>' reference.
	 * @see #getFbNetwork()
	 * @generated
	 */
	void setFbNetwork(SubAppNetwork value);

	/**
	 * Returns the value of the '<em><b>Application Interface Element</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.InterfaceElementView#getMappedInterfaceElement <em>Mapped Interface Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Application Interface Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Application Interface Element</em>' reference.
	 * @see #setApplicationInterfaceElement(InterfaceElementView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getInterfaceElementView_ApplicationInterfaceElement()
	 * @see org.fordiac.ide.model.ui.InterfaceElementView#getMappedInterfaceElement
	 * @model opposite="mappedInterfaceElement"
	 * @generated
	 */
	InterfaceElementView getApplicationInterfaceElement();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.InterfaceElementView#getApplicationInterfaceElement <em>Application Interface Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Application Interface Element</em>' reference.
	 * @see #getApplicationInterfaceElement()
	 * @generated
	 */
	void setApplicationInterfaceElement(InterfaceElementView value);

} // InterfaceElementView
