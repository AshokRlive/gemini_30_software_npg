/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.typeimport;

import org.fordiac.ide.model.libraryElement.Identification;
import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.VersionInfo;
import org.fordiac.ide.typeimport.Messages;
import org.fordiac.ide.typeimport.exceptions.TypeImportException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * The Class CommonElementImporter.
 */
public class CommonElementImporter {

	/**
	 * Parses the identification.
	 * 
	 * @param elem the elem
	 * @param node the node
	 * 
	 * @return the identification
	 */
	public static Identification parseIdentification(final LibraryElement elem,
			final Node node) {
		NamedNodeMap map = node.getAttributes();
		Identification ident = LibraryElementFactory.eINSTANCE
				.createIdentification();
		elem.setIdentification(ident);
		Node standard = map.getNamedItem(LibraryElementTags.STANDARD_ATTRIBUTE);
		if (standard != null) {
			ident.setStandard(standard.getNodeValue());
		}
		Node classification = map
				.getNamedItem(LibraryElementTags.CLASSIFICATION_ATTRIBUTE);
		if (classification != null) {
			ident.setClassification(classification.getNodeValue());
		}
		Node applicationDomain = map
				.getNamedItem(LibraryElementTags.APPLICATION_DOMAIN_ATTRIBUTE);
		if (applicationDomain != null) {
			ident.setApplicationDomain(applicationDomain.getNodeValue());
		}
		Node function = map.getNamedItem(LibraryElementTags.FUNCTION_ELEMENT);
		if (function != null) {
			ident.setFunction(function.getNodeValue());
		}
		Node type = map.getNamedItem(LibraryElementTags.TYPE_ATTRIBUTE);
		if (type != null) {
			ident.setType(type.getNodeValue());
		}
		Node description = map
				.getNamedItem(LibraryElementTags.DESCRIPTION_ELEMENT);
		if (description != null) {
			ident.setDescription(description.getNodeValue());
		}

		return ident;
	}

	/**
	 * Parses the version info.
	 * 
	 * @param elem the elem
	 * @param node the node
	 * 
	 * @return the version info
	 * 
	 * @throws TypeImportException the FBT import exception
	 */
	public static VersionInfo parseVersionInfo(final LibraryElement elem,
			final Node node) throws TypeImportException {
		NamedNodeMap map = node.getAttributes();
		VersionInfo versionInfo = LibraryElementFactory.eINSTANCE
				.createVersionInfo();
		elem.getVersionInfo().add(versionInfo);
		Node organization = map
				.getNamedItem(LibraryElementTags.ORGANIZATION_ATTRIBUTE);
		if (organization != null) {
			versionInfo.setOrganization(organization.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.CommonElementImporter_ERROR_Missing_Organization);
		}
		Node version = map.getNamedItem(LibraryElementTags.VERSION_ATTRIBUTE);
		if (version != null) {
			versionInfo.setVersion(version.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.CommonElementImporter_ERROR_MissingVersionInfo);
		}
		Node author = map.getNamedItem(LibraryElementTags.AUTHOR_ATTRIBUTE);
		if (author != null) {
			versionInfo.setAuthor(author.getNodeValue());
		} else {
			throw new TypeImportException(
					Messages.CommonElementImporter_ERROR_MissingAuthorInfo);
		}
		Node date = map.getNamedItem(LibraryElementTags.DATE_ATTRIBUTE);
		if (date != null) {
			versionInfo.setDate(date.getNodeValue()); // TODO: check whether
			// it is better to
			// change type to Date
		} else {
			throw new TypeImportException(
					Messages.CommonElementImporter_ERROR_Missing_Organization);
		}
		Node remarks = map.getNamedItem(LibraryElementTags.REMARKS_ATTRIBUTE);
		if (remarks != null) {
			versionInfo.setRemarks(remarks.getNodeValue());
		}else{
			versionInfo.setRemarks(""); //$NON-NLS-1$
		}

		return versionInfo;
	}
}
