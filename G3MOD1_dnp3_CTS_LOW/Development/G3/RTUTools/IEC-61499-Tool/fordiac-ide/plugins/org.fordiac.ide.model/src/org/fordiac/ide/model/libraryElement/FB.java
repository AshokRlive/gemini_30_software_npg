/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;


/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>FB</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.FB#getInterface <em>Interface</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.FB#getPosition <em>Position</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.FB#getResource <em>Resource</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.FB#isResourceTypeFB <em>Resource Type FB</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.FB#isResourceFB <em>Resource FB</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.FB#getParentCompositeFBType <em>Parent Composite FB Type</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.FB#getFbtPath <em>Fbt Path</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFB()
 * @model
 * @generated
 */
public interface FB extends TypedConfigureableObject {
	/**
	 * Returns the value of the '<em><b>Interface</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.InterfaceList#getFB <em>FB</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface</em>' containment reference.
	 * @see #setInterface(InterfaceList)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFB_Interface()
	 * @see org.fordiac.ide.model.libraryElement.InterfaceList#getFB
	 * @model opposite="fB" containment="true" resolveProxies="true"
	 * @generated
	 */
	InterfaceList getInterface();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.FB#getInterface <em>Interface</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Interface</em>' containment reference.
	 * @see #getInterface()
	 * @generated
	 */
	void setInterface(InterfaceList value);

	/**
	 * Returns the value of the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' containment reference.
	 * @see #setPosition(Position)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFB_Position()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Position getPosition();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.FB#getPosition <em>Position</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Position</em>' containment reference.
	 * @see #getPosition()
	 * @generated
	 */
	void setPosition(Position value);

	/**
	 * Returns the value of the '<em><b>Resource</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.ResourceFBNetwork#getMappedFBs <em>Mapped FBs</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' reference.
	 * @see #setResource(ResourceFBNetwork)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFB_Resource()
	 * @see org.fordiac.ide.model.libraryElement.ResourceFBNetwork#getMappedFBs
	 * @model opposite="mappedFBs"
	 * @generated
	 */
	ResourceFBNetwork getResource();

	/**
	 * Sets the value of the '
	 * {@link org.fordiac.ide.model.libraryElement.FB#getResource
	 * <em>Resource</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *          the new value of the '<em>Resource</em>' reference.
	 * @see #getResource()
	 * @generated
	 */
	void setResource(ResourceFBNetwork value);

	/**
	 * Returns the value of the '<em><b>Resource Type FB</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Type FB</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Type FB</em>' attribute.
	 * @see #setResourceTypeFB(boolean)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFB_ResourceTypeFB()
	 * @model default="false" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isResourceTypeFB();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.FB#isResourceTypeFB <em>Resource Type FB</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Resource Type FB</em>' attribute.
	 * @see #isResourceTypeFB()
	 * @generated
	 */
	void setResourceTypeFB(boolean value);

	/**
	 * Returns the value of the '<em><b>Resource FB</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource FB</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource FB</em>' attribute.
	 * @see #setResourceFB(boolean)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFB_ResourceFB()
	 * @model default="false" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isResourceFB();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.FB#isResourceFB <em>Resource FB</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource FB</em>' attribute.
	 * @see #isResourceFB()
	 * @generated
	 */
	void setResourceFB(boolean value);

	/**
	 * Returns the value of the '<em><b>Parent Composite FB Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Composite FB Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Composite FB Type</em>' reference.
	 * @see #setParentCompositeFBType(CompositeFBType)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFB_ParentCompositeFBType()
	 * @model
	 * @generated
	 */
	CompositeFBType getParentCompositeFBType();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.FB#getParentCompositeFBType <em>Parent Composite FB Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Composite FB Type</em>' reference.
	 * @see #getParentCompositeFBType()
	 * @generated
	 */
	void setParentCompositeFBType(CompositeFBType value);

	/**
	 * Returns the value of the '<em><b>Fbt Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fbt Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fbt Path</em>' attribute.
	 * @see #setFbtPath(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFB_FbtPath()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getFbtPath();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.FB#getFbtPath <em>Fbt Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fbt Path</em>' attribute.
	 * @see #getFbtPath()
	 * @generated
	 */
	void setFbtPath(String value);

	IInterfaceElement getInterfaceElement(String name);
	
	FBType getFBType();

} // FB
