/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub App Interface Element View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.SubAppInterfaceElementView#isVisible <em>Visible</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.SubAppInterfaceElementView#getLabelSubstitute <em>Label Substitute</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getSubAppInterfaceElementView()
 * @model
 * @generated
 */
public interface SubAppInterfaceElementView extends InterfaceElementView {
	/**
	 * Returns the value of the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visible</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visible</em>' attribute.
	 * @see #setVisible(boolean)
	 * @see org.fordiac.ide.model.ui.UiPackage#getSubAppInterfaceElementView_Visible()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isVisible();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.SubAppInterfaceElementView#isVisible <em>Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visible</em>' attribute.
	 * @see #isVisible()
	 * @generated
	 */
	void setVisible(boolean value);

	/**
	 * Returns the value of the '<em><b>Label Substitute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label Substitute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label Substitute</em>' attribute.
	 * @see #setLabelSubstitute(String)
	 * @see org.fordiac.ide.model.ui.UiPackage#getSubAppInterfaceElementView_LabelSubstitute()
	 * @model
	 * @generated
	 */
	String getLabelSubstitute();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.SubAppInterfaceElementView#getLabelSubstitute <em>Label Substitute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label Substitute</em>' attribute.
	 * @see #getLabelSubstitute()
	 * @generated
	 */
	void setLabelSubstitute(String value);
	
} // SubAppInterfaceElementView
