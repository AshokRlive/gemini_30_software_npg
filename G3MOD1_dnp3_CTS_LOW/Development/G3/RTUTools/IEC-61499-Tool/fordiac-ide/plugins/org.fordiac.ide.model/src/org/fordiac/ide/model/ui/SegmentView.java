/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.eclipse.emf.common.util.EList;
import org.fordiac.ide.model.libraryElement.Segment;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Segment View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.SegmentView#getSegment <em>Segment</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.SegmentView#getOutConnections <em>Out Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.SegmentView#getSize <em>Size</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getSegmentView()
 * @model
 * @generated
 */
public interface SegmentView extends View {
	/**
	 * Returns the value of the '<em><b>Segment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment</em>' reference.
	 * @see #setSegment(Segment)
	 * @see org.fordiac.ide.model.ui.UiPackage#getSegmentView_Segment()
	 * @model
	 * @generated
	 */
	Segment getSegment();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.SegmentView#getSegment <em>Segment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Segment</em>' reference.
	 * @see #getSegment()
	 * @generated
	 */
	void setSegment(Segment value);

	/**
	 * Returns the value of the '<em><b>Out Connections</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.ui.LinkView}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.LinkView#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Connections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Connections</em>' reference list.
	 * @see org.fordiac.ide.model.ui.UiPackage#getSegmentView_OutConnections()
	 * @see org.fordiac.ide.model.ui.LinkView#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<LinkView> getOutConnections();

	/**
	 * Returns the value of the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size</em>' containment reference.
	 * @see #setSize(Size)
	 * @see org.fordiac.ide.model.ui.UiPackage#getSegmentView_Size()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	Size getSize();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.SegmentView#getSize <em>Size</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size</em>' containment reference.
	 * @see #getSize()
	 * @generated
	 */
	void setSize(Size value);

} // SegmentView
