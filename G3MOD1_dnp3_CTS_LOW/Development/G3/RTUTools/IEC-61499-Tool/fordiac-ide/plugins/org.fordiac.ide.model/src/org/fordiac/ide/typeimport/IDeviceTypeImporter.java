/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.typeimport;

import org.eclipse.core.resources.IFile;
import org.fordiac.ide.model.libraryElement.DeviceType;

/**
 * The Interface IDeviceTypeImporter.
 */
public interface IDeviceTypeImporter {

	/**
	 * Import device type.
	 * 
	 * @param file the file
	 * 
	 * @return the device type
	 */
	DeviceType importDEVType(final IFile file);

	/**
	 * Supports type.
	 * 
	 * @param type the type
	 * 
	 * @return true, if successful
	 */
	boolean supportsType(String type);

}
