/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LD</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.LD#getRung <em>Rung</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getLD()
 * @model
 * @generated
 */
public interface LD extends Algorithm {
	/**
	 * Returns the value of the '<em><b>Rung</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.Rung}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rung</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rung</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getLD_Rung()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Rung' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Rung> getRung();

} // LD
