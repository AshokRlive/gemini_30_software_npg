/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Device</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Device#getResource <em>Resource</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Device#getFBNetwork <em>FB Network</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Device#getX <em>X</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Device#getY <em>Y</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Device#getVarDeclarations <em>Var Declarations</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Device#getProfile <em>Profile</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Device#getInConnections <em>In Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Device#getTypePath <em>Type Path</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Device#getSystemConfigurationNetwork <em>System Configuration Network</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getDevice()
 * @model
 * @generated
 */
public interface Device extends TypedConfigureableObject {
	/**
	 * Returns the value of the '<em><b>Resource</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.Resource}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.Resource#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' containment reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getDevice_Resource()
	 * @see org.fordiac.ide.model.libraryElement.Resource#getDevice
	 * @model opposite="device" containment="true"
	 *        extendedMetaData="kind='element' name='Resource' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Resource> getResource();

	/**
	 * Returns the value of the '<em><b>FB Network</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>FB Network</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FB Network</em>' containment reference.
	 * @see #setFBNetwork(FBNetwork)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getDevice_FBNetwork()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FBNetwork' namespace='##targetNamespace'"
	 * @generated
	 */
	FBNetwork getFBNetwork();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Device#getFBNetwork <em>FB Network</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>FB Network</em>' containment reference.
	 * @see #getFBNetwork()
	 * @generated
	 */
	void setFBNetwork(FBNetwork value);

	/**
	 * Returns the value of the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>X</em>' attribute isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X</em>' attribute.
	 * @see #setX(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getDevice_X()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='x'"
	 * @generated
	 */
	String getX();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Device#getX <em>X</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>X</em>' attribute.
	 * @see #getX()
	 * @generated
	 */
	void setX(String value);

	/**
	 * Returns the value of the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Y</em>' attribute isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y</em>' attribute.
	 * @see #setY(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getDevice_Y()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='y'"
	 * @generated
	 */
	String getY();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Device#getY <em>Y</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y</em>' attribute.
	 * @see #getY()
	 * @generated
	 */
	void setY(String value);

	/**
	 * Returns the value of the '<em><b>Var Declarations</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link org.fordiac.ide.model.libraryElement.VarDeclaration}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Declarations</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Var Declarations</em>' containment reference
	 *         list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getDevice_VarDeclarations()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<VarDeclaration> getVarDeclarations();

	/**
	 * Returns the value of the '<em><b>Profile</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Profile</em>' attribute.
	 * @see #setProfile(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getDevice_Profile()
	 * @model
	 * @generated
	 */
	String getProfile();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Device#getProfile <em>Profile</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Profile</em>' attribute.
	 * @see #getProfile()
	 * @generated
	 */
	void setProfile(String value);

	/**
	 * Returns the value of the '<em><b>In Connections</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.Link}.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.Link#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Connections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Connections</em>' reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getDevice_InConnections()
	 * @see org.fordiac.ide.model.libraryElement.Link#getDevice
	 * @model opposite="device"
	 * @generated
	 */
	EList<Link> getInConnections();

	/**
	 * Returns the value of the '<em><b>Type Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Path</em>' attribute.
	 * @see #setTypePath(String)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getDevice_TypePath()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 * @generated
	 */
	String getTypePath();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Device#getTypePath <em>Type Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Path</em>' attribute.
	 * @see #getTypePath()
	 * @generated
	 */
	void setTypePath(String value);

	/**
	 * Returns the value of the '<em><b>System Configuration Network</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork#getDevices <em>Devices</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Configuration Network</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Configuration Network</em>' container reference.
	 * @see #setSystemConfigurationNetwork(SystemConfigurationNetwork)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getDevice_SystemConfigurationNetwork()
	 * @see org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork#getDevices
	 * @model opposite="devices"
	 * @generated
	 */
	SystemConfigurationNetwork getSystemConfigurationNetwork();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Device#getSystemConfigurationNetwork <em>System Configuration Network</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Configuration Network</em>' container reference.
	 * @see #getSystemConfigurationNetwork()
	 * @generated
	 */
	void setSystemConfigurationNetwork(SystemConfigurationNetwork value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getSystemConfigurationNetwork().getAutomationSystem();'"
	 * @generated
	 */
	AutomationSystem getAutomationSystem();

	Resource getResourceForName(String name);
	
	DeviceType getDeviceType();

} // Device
