/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.typeexport;

import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.typeimport.LibraryElementTags;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

class AdapterExporter extends CommonElementExporter {

	protected void addType(final Document dom, final FBType fbType){
		Element rootElement = createRootElement(dom, fbType, LibraryElementTags.ADAPTER_TYPE);
		addCompileAbleTypeData(dom, rootElement, fbType);
		
		addInterfaceList(dom, rootElement, fbType.getInterfaceList(), null, fbType);
		addService(dom, rootElement, fbType);
	}
	
	protected FBType getType(PaletteEntry entry){
		return ((AdapterTypePaletteEntry)entry).getAdapterType().getAdapterFBType();
	}
}
