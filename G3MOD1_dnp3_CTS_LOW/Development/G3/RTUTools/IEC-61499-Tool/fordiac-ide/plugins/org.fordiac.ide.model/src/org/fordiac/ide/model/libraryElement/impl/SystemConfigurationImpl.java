/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.SystemConfiguration;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>System Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SystemConfigurationImpl#getSystemConfigurationNetwork <em>System Configuration Network</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SystemConfigurationImpl#getAutomationSystem <em>Automation System</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SystemConfigurationImpl extends I4DIACElementImpl implements SystemConfiguration {
	/**
	 * The cached value of the '{@link #getSystemConfigurationNetwork() <em>System Configuration Network</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemConfigurationNetwork()
	 * @generated
	 * @ordered
	 */
	protected SystemConfigurationNetwork systemConfigurationNetwork;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibraryElementPackage.Literals.SYSTEM_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemConfigurationNetwork getSystemConfigurationNetwork() {
		if (systemConfigurationNetwork != null && systemConfigurationNetwork.eIsProxy()) {
			InternalEObject oldSystemConfigurationNetwork = (InternalEObject)systemConfigurationNetwork;
			systemConfigurationNetwork = (SystemConfigurationNetwork)eResolveProxy(oldSystemConfigurationNetwork);
			if (systemConfigurationNetwork != oldSystemConfigurationNetwork) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LibraryElementPackage.SYSTEM_CONFIGURATION__SYSTEM_CONFIGURATION_NETWORK, oldSystemConfigurationNetwork, systemConfigurationNetwork));
			}
		}
		return systemConfigurationNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemConfigurationNetwork basicGetSystemConfigurationNetwork() {
		return systemConfigurationNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemConfigurationNetworkGen(SystemConfigurationNetwork newSystemConfigurationNetwork) {
		SystemConfigurationNetwork oldSystemConfigurationNetwork = systemConfigurationNetwork;
		systemConfigurationNetwork = newSystemConfigurationNetwork;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.SYSTEM_CONFIGURATION__SYSTEM_CONFIGURATION_NETWORK, oldSystemConfigurationNetwork, systemConfigurationNetwork));
	}
	
	public void setSystemConfigurationNetwork(SystemConfigurationNetwork newSystemConfigurationNetwork) {
		setSystemConfigurationNetworkGen(newSystemConfigurationNetwork);
		getSystemConfigurationNetwork().setSystemConfiguration(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AutomationSystem getAutomationSystem() {
		if (eContainerFeatureID() != LibraryElementPackage.SYSTEM_CONFIGURATION__AUTOMATION_SYSTEM) return null;
		return (AutomationSystem)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AutomationSystem basicGetAutomationSystem() {
		if (eContainerFeatureID() != LibraryElementPackage.SYSTEM_CONFIGURATION__AUTOMATION_SYSTEM) return null;
		return (AutomationSystem)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAutomationSystem(AutomationSystem newAutomationSystem, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newAutomationSystem, LibraryElementPackage.SYSTEM_CONFIGURATION__AUTOMATION_SYSTEM, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAutomationSystem(AutomationSystem newAutomationSystem) {
		if (newAutomationSystem != eInternalContainer() || (eContainerFeatureID() != LibraryElementPackage.SYSTEM_CONFIGURATION__AUTOMATION_SYSTEM && newAutomationSystem != null)) {
			if (EcoreUtil.isAncestor(this, newAutomationSystem))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAutomationSystem != null)
				msgs = ((InternalEObject)newAutomationSystem).eInverseAdd(this, LibraryElementPackage.AUTOMATION_SYSTEM__SYSTEM_CONFIGURATION, AutomationSystem.class, msgs);
			msgs = basicSetAutomationSystem(newAutomationSystem, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.SYSTEM_CONFIGURATION__AUTOMATION_SYSTEM, newAutomationSystem, newAutomationSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.SYSTEM_CONFIGURATION__AUTOMATION_SYSTEM:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetAutomationSystem((AutomationSystem)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.SYSTEM_CONFIGURATION__AUTOMATION_SYSTEM:
				return basicSetAutomationSystem(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case LibraryElementPackage.SYSTEM_CONFIGURATION__AUTOMATION_SYSTEM:
				return eInternalContainer().eInverseRemove(this, LibraryElementPackage.AUTOMATION_SYSTEM__SYSTEM_CONFIGURATION, AutomationSystem.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibraryElementPackage.SYSTEM_CONFIGURATION__SYSTEM_CONFIGURATION_NETWORK:
				if (resolve) return getSystemConfigurationNetwork();
				return basicGetSystemConfigurationNetwork();
			case LibraryElementPackage.SYSTEM_CONFIGURATION__AUTOMATION_SYSTEM:
				if (resolve) return getAutomationSystem();
				return basicGetAutomationSystem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibraryElementPackage.SYSTEM_CONFIGURATION__SYSTEM_CONFIGURATION_NETWORK:
				setSystemConfigurationNetwork((SystemConfigurationNetwork)newValue);
				return;
			case LibraryElementPackage.SYSTEM_CONFIGURATION__AUTOMATION_SYSTEM:
				setAutomationSystem((AutomationSystem)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.SYSTEM_CONFIGURATION__SYSTEM_CONFIGURATION_NETWORK:
				setSystemConfigurationNetwork((SystemConfigurationNetwork)null);
				return;
			case LibraryElementPackage.SYSTEM_CONFIGURATION__AUTOMATION_SYSTEM:
				setAutomationSystem((AutomationSystem)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.SYSTEM_CONFIGURATION__SYSTEM_CONFIGURATION_NETWORK:
				return systemConfigurationNetwork != null;
			case LibraryElementPackage.SYSTEM_CONFIGURATION__AUTOMATION_SYSTEM:
				return basicGetAutomationSystem() != null;
		}
		return super.eIsSet(featureID);
	}

} //SystemConfigurationImpl
