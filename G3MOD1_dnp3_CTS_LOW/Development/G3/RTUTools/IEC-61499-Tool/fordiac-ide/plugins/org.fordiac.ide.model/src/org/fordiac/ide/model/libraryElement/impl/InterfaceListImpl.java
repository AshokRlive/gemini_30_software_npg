/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interface List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.InterfaceListImpl#getPlugs <em>Plugs</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.InterfaceListImpl#getSockets <em>Sockets</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.InterfaceListImpl#getFB <em>FB</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InterfaceListImpl extends SubAppInterfaceListImpl implements InterfaceList {
	/**
	 * The cached value of the '{@link #getPlugs() <em>Plugs</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlugs()
	 * @generated
	 * @ordered
	 */
	protected EList<AdapterDeclaration> plugs;

	/**
	 * The cached value of the '{@link #getSockets() <em>Sockets</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSockets()
	 * @generated
	 * @ordered
	 */
	protected EList<AdapterDeclaration> sockets;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterfaceListImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibraryElementPackage.Literals.INTERFACE_LIST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AdapterDeclaration> getPlugs() {
		if (plugs == null) {
			plugs = new EObjectEList<AdapterDeclaration>(AdapterDeclaration.class, this, LibraryElementPackage.INTERFACE_LIST__PLUGS);
		}
		return plugs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AdapterDeclaration> getSockets() {
		if (sockets == null) {
			sockets = new EObjectEList<AdapterDeclaration>(AdapterDeclaration.class, this, LibraryElementPackage.INTERFACE_LIST__SOCKETS);
		}
		return sockets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FB getFB() {
		if (eContainerFeatureID() != LibraryElementPackage.INTERFACE_LIST__FB) return null;
		return (FB)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FB basicGetFB() {
		if (eContainerFeatureID() != LibraryElementPackage.INTERFACE_LIST__FB) return null;
		return (FB)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFB(FB newFB, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newFB, LibraryElementPackage.INTERFACE_LIST__FB, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFB(FB newFB) {
		if (newFB != eInternalContainer() || (eContainerFeatureID() != LibraryElementPackage.INTERFACE_LIST__FB && newFB != null)) {
			if (EcoreUtil.isAncestor(this, newFB))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newFB != null)
				msgs = ((InternalEObject)newFB).eInverseAdd(this, LibraryElementPackage.FB__INTERFACE, FB.class, msgs);
			msgs = basicSetFB(newFB, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.INTERFACE_LIST__FB, newFB, newFB));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public INamedElement getContainingNamedElement() {
		return (INamedElement)eContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.INTERFACE_LIST__FB:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetFB((FB)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.INTERFACE_LIST__FB:
				return basicSetFB(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case LibraryElementPackage.INTERFACE_LIST__FB:
				return eInternalContainer().eInverseRemove(this, LibraryElementPackage.FB__INTERFACE, FB.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibraryElementPackage.INTERFACE_LIST__PLUGS:
				return getPlugs();
			case LibraryElementPackage.INTERFACE_LIST__SOCKETS:
				return getSockets();
			case LibraryElementPackage.INTERFACE_LIST__FB:
				if (resolve) return getFB();
				return basicGetFB();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibraryElementPackage.INTERFACE_LIST__PLUGS:
				getPlugs().clear();
				getPlugs().addAll((Collection<? extends AdapterDeclaration>)newValue);
				return;
			case LibraryElementPackage.INTERFACE_LIST__SOCKETS:
				getSockets().clear();
				getSockets().addAll((Collection<? extends AdapterDeclaration>)newValue);
				return;
			case LibraryElementPackage.INTERFACE_LIST__FB:
				setFB((FB)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.INTERFACE_LIST__PLUGS:
				getPlugs().clear();
				return;
			case LibraryElementPackage.INTERFACE_LIST__SOCKETS:
				getSockets().clear();
				return;
			case LibraryElementPackage.INTERFACE_LIST__FB:
				setFB((FB)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.INTERFACE_LIST__PLUGS:
				return plugs != null && !plugs.isEmpty();
			case LibraryElementPackage.INTERFACE_LIST__SOCKETS:
				return sockets != null && !sockets.isEmpty();
			case LibraryElementPackage.INTERFACE_LIST__FB:
				return basicGetFB() != null;
		}
		return super.eIsSet(featureID);
	}

	public AdapterDeclaration getAdapter(String name) {
		for (AdapterDeclaration adapt : getPlugs()) {
			if (adapt.getName().equals(name))
				return adapt;
		}
		for (AdapterDeclaration adapt : getSockets()) {
			if (adapt.getName().equals(name))
				return adapt;
		}
		return null;
	}

} //InterfaceListImpl
