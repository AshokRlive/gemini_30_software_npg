/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Sub App Network</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SubAppNetworkImpl#getSubApps <em>Sub Apps</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SubAppNetworkImpl#getFBs <em>FBs</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SubAppNetworkImpl#getDataConnections <em>Data Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SubAppNetworkImpl#getEventConnections <em>Event Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SubAppNetworkImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SubAppNetworkImpl#getParentSubApp <em>Parent Sub App</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubAppNetworkImpl extends EObjectImpl implements SubAppNetwork {
	/**
	 * The cached value of the '{@link #getSubApps() <em>Sub Apps</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSubApps()
	 * @generated
	 * @ordered
	 */
	protected EList<SubApp> subApps;

	/**
	 * The cached value of the '{@link #getFBs() <em>FBs</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getFBs()
	 * @generated
	 * @ordered
	 */
	protected EList<FB> fBs;

	/**
	 * The cached value of the '{@link #getDataConnections() <em>Data Connections</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDataConnections()
	 * @generated
	 * @ordered
	 */
	protected EList<DataConnection> dataConnections;

	/**
	 * The cached value of the '{@link #getEventConnections() <em>Event Connections</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEventConnections()
	 * @generated
	 * @ordered
	 */
	protected EList<EventConnection> eventConnections;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParentSubApp() <em>Parent Sub App</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParentSubApp()
	 * @generated
	 * @ordered
	 */
	protected SubApp parentSubApp;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected SubAppNetworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibraryElementPackage.Literals.SUB_APP_NETWORK;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubApp> getSubApps() {
		if (subApps == null) {
			subApps = new EObjectContainmentEList<SubApp>(SubApp.class, this, LibraryElementPackage.SUB_APP_NETWORK__SUB_APPS);
		}
		return subApps;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FB> getFBs() {
		if (fBs == null) {
			fBs = new EObjectContainmentEList<FB>(FB.class, this, LibraryElementPackage.SUB_APP_NETWORK__FBS);
		}
		return fBs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataConnection> getDataConnections() {
		if (dataConnections == null) {
			dataConnections = new EObjectContainmentEList<DataConnection>(DataConnection.class, this, LibraryElementPackage.SUB_APP_NETWORK__DATA_CONNECTIONS);
		}
		return dataConnections;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EventConnection> getEventConnections() {
		if (eventConnections == null) {
			eventConnections = new EObjectContainmentEList.Resolving<EventConnection>(EventConnection.class, this, LibraryElementPackage.SUB_APP_NETWORK__EVENT_CONNECTIONS);
		}
		return eventConnections;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.SUB_APP_NETWORK__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubApp getParentSubApp() {
		if (parentSubApp != null && parentSubApp.eIsProxy()) {
			InternalEObject oldParentSubApp = (InternalEObject)parentSubApp;
			parentSubApp = (SubApp)eResolveProxy(oldParentSubApp);
			if (parentSubApp != oldParentSubApp) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LibraryElementPackage.SUB_APP_NETWORK__PARENT_SUB_APP, oldParentSubApp, parentSubApp));
			}
		}
		return parentSubApp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubApp basicGetParentSubApp() {
		return parentSubApp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentSubApp(SubApp newParentSubApp, NotificationChain msgs) {
		SubApp oldParentSubApp = parentSubApp;
		parentSubApp = newParentSubApp;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LibraryElementPackage.SUB_APP_NETWORK__PARENT_SUB_APP, oldParentSubApp, newParentSubApp);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentSubApp(SubApp newParentSubApp) {
		if (newParentSubApp != parentSubApp) {
			NotificationChain msgs = null;
			if (parentSubApp != null)
				msgs = ((InternalEObject)parentSubApp).eInverseRemove(this, LibraryElementPackage.SUB_APP__SUB_APP_NETWORK, SubApp.class, msgs);
			if (newParentSubApp != null)
				msgs = ((InternalEObject)newParentSubApp).eInverseAdd(this, LibraryElementPackage.SUB_APP__SUB_APP_NETWORK, SubApp.class, msgs);
			msgs = basicSetParentSubApp(newParentSubApp, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.SUB_APP_NETWORK__PARENT_SUB_APP, newParentSubApp, newParentSubApp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.SUB_APP_NETWORK__PARENT_SUB_APP:
				if (parentSubApp != null)
					msgs = ((InternalEObject)parentSubApp).eInverseRemove(this, LibraryElementPackage.SUB_APP__SUB_APP_NETWORK, SubApp.class, msgs);
				return basicSetParentSubApp((SubApp)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.SUB_APP_NETWORK__SUB_APPS:
				return ((InternalEList<?>)getSubApps()).basicRemove(otherEnd, msgs);
			case LibraryElementPackage.SUB_APP_NETWORK__FBS:
				return ((InternalEList<?>)getFBs()).basicRemove(otherEnd, msgs);
			case LibraryElementPackage.SUB_APP_NETWORK__DATA_CONNECTIONS:
				return ((InternalEList<?>)getDataConnections()).basicRemove(otherEnd, msgs);
			case LibraryElementPackage.SUB_APP_NETWORK__EVENT_CONNECTIONS:
				return ((InternalEList<?>)getEventConnections()).basicRemove(otherEnd, msgs);
			case LibraryElementPackage.SUB_APP_NETWORK__PARENT_SUB_APP:
				return basicSetParentSubApp(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibraryElementPackage.SUB_APP_NETWORK__SUB_APPS:
				return getSubApps();
			case LibraryElementPackage.SUB_APP_NETWORK__FBS:
				return getFBs();
			case LibraryElementPackage.SUB_APP_NETWORK__DATA_CONNECTIONS:
				return getDataConnections();
			case LibraryElementPackage.SUB_APP_NETWORK__EVENT_CONNECTIONS:
				return getEventConnections();
			case LibraryElementPackage.SUB_APP_NETWORK__NAME:
				return getName();
			case LibraryElementPackage.SUB_APP_NETWORK__PARENT_SUB_APP:
				if (resolve) return getParentSubApp();
				return basicGetParentSubApp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibraryElementPackage.SUB_APP_NETWORK__SUB_APPS:
				getSubApps().clear();
				getSubApps().addAll((Collection<? extends SubApp>)newValue);
				return;
			case LibraryElementPackage.SUB_APP_NETWORK__FBS:
				getFBs().clear();
				getFBs().addAll((Collection<? extends FB>)newValue);
				return;
			case LibraryElementPackage.SUB_APP_NETWORK__DATA_CONNECTIONS:
				getDataConnections().clear();
				getDataConnections().addAll((Collection<? extends DataConnection>)newValue);
				return;
			case LibraryElementPackage.SUB_APP_NETWORK__EVENT_CONNECTIONS:
				getEventConnections().clear();
				getEventConnections().addAll((Collection<? extends EventConnection>)newValue);
				return;
			case LibraryElementPackage.SUB_APP_NETWORK__NAME:
				setName((String)newValue);
				return;
			case LibraryElementPackage.SUB_APP_NETWORK__PARENT_SUB_APP:
				setParentSubApp((SubApp)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.SUB_APP_NETWORK__SUB_APPS:
				getSubApps().clear();
				return;
			case LibraryElementPackage.SUB_APP_NETWORK__FBS:
				getFBs().clear();
				return;
			case LibraryElementPackage.SUB_APP_NETWORK__DATA_CONNECTIONS:
				getDataConnections().clear();
				return;
			case LibraryElementPackage.SUB_APP_NETWORK__EVENT_CONNECTIONS:
				getEventConnections().clear();
				return;
			case LibraryElementPackage.SUB_APP_NETWORK__NAME:
				setName(NAME_EDEFAULT);
				return;
			case LibraryElementPackage.SUB_APP_NETWORK__PARENT_SUB_APP:
				setParentSubApp((SubApp)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.SUB_APP_NETWORK__SUB_APPS:
				return subApps != null && !subApps.isEmpty();
			case LibraryElementPackage.SUB_APP_NETWORK__FBS:
				return fBs != null && !fBs.isEmpty();
			case LibraryElementPackage.SUB_APP_NETWORK__DATA_CONNECTIONS:
				return dataConnections != null && !dataConnections.isEmpty();
			case LibraryElementPackage.SUB_APP_NETWORK__EVENT_CONNECTIONS:
				return eventConnections != null && !eventConnections.isEmpty();
			case LibraryElementPackage.SUB_APP_NETWORK__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case LibraryElementPackage.SUB_APP_NETWORK__PARENT_SUB_APP:
				return parentSubApp != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

//	public List<Connection> getConnections() {
//		ArrayList<Connection>temp = new ArrayList<Connection>();
//		temp.addAll(getEventConnections());
//		temp.addAll(getDataConnections());
//		return temp;
//	}

} //SubAppNetworkImpl
