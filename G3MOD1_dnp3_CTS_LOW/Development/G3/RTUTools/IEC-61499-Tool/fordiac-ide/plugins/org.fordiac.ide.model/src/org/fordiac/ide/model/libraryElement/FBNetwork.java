/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>FB Network</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.FBNetwork#getAdapterConnections <em>Adapter Connections</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.FBNetwork#getApplication <em>Application</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFBNetwork()
 * @model
 * @generated
 */
public interface FBNetwork extends SubAppNetwork {
	/**
	 * Returns the value of the '<em><b>Adapter Connections</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link org.fordiac.ide.model.libraryElement.Connection}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adapter Connections</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Adapter Connections</em>' containment
	 *         reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFBNetwork_AdapterConnections()
	 * @model containment="true" extendedMetaData=
	 *        "kind='element' name='AdapterConnections' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Connection> getAdapterConnections();

	/**
	 * Returns the value of the '<em><b>Application</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.Application#getFBNetwork <em>FB Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Application</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Application</em>' reference.
	 * @see #setApplication(Application)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFBNetwork_Application()
	 * @see org.fordiac.ide.model.libraryElement.Application#getFBNetwork
	 * @model opposite="fBNetwork" required="true"
	 * @generated
	 */
	Application getApplication();

	/**
	 * Sets the value of the '
	 * {@link org.fordiac.ide.model.libraryElement.FBNetwork#getApplication
	 * <em>Application</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *          the new value of the '<em>Application</em>' reference.
	 * @see #getApplication()
	 * @generated
	 */
	void setApplication(Application value);

	FB getFB(String name);

} // FBNetwork
