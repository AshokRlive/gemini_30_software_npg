/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Internal Sub App Interface Element View</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getInternalSubAppInterfaceElementView()
 * @model
 * @generated
 */
public interface InternalSubAppInterfaceElementView extends SubAppInterfaceElementView {
	// empty interface
} // InternalSubAppInterfaceElementView
