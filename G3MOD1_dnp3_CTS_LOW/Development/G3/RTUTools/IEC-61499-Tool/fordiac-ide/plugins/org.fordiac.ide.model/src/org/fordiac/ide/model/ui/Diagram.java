/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Diagram</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.Diagram#getChildren <em>Children</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.Diagram#getConnections <em>Connections</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getDiagram()
 * @model abstract="true"
 * @generated
 */
public interface Diagram extends EObject {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.ui.View}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see org.fordiac.ide.model.ui.UiPackage#getDiagram_Children()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<View> getChildren();

	/**
	 * Returns the value of the '<em><b>Connections</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.ui.IConnectionView}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connections</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connections</em>' containment reference list.
	 * @see org.fordiac.ide.model.ui.UiPackage#getDiagram_Connections()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<IConnectionView> getConnections();
	
	SubAppNetwork getNetwork();
	
	SubAppNetwork getFunctionBlockNetwork();

} // Diagram
