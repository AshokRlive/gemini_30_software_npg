/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.fordiac.ide.model.libraryElement.Connection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.ConnectionView#getConnectionElement <em>Connection Element</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.ConnectionView#getSource <em>Source</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.ConnectionView#getDestination <em>Destination</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.ConnectionView#getExternalDestinationInterfaceElementView <em>External Destination Interface Element View</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.ConnectionView#getInternalDestinationInterfaceElementView <em>Internal Destination Interface Element View</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.ConnectionView#getExternalSourceInterfaceElementView <em>External Source Interface Element View</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.ConnectionView#getInternalSourceInterfaceElementView <em>Internal Source Interface Element View</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getConnectionView()
 * @model
 * @generated
 */
public interface ConnectionView extends IConnectionView {
	/**
	 * Returns the value of the '<em><b>Connection Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection Element</em>' reference.
	 * @see #setConnectionElement(Connection)
	 * @see org.fordiac.ide.model.ui.UiPackage#getConnectionView_ConnectionElement()
	 * @model
	 * @generated
	 */
	Connection getConnectionElement();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.ConnectionView#getConnectionElement <em>Connection Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connection Element</em>' reference.
	 * @see #getConnectionElement()
	 * @generated
	 */
	void setConnectionElement(Connection value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.InterfaceElementView#getOutConnections <em>Out Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(InterfaceElementView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getConnectionView_Source()
	 * @see org.fordiac.ide.model.ui.InterfaceElementView#getOutConnections
	 * @model opposite="outConnections"
	 * @generated
	 */
	InterfaceElementView getSource();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.ConnectionView#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(InterfaceElementView value);

	/**
	 * Returns the value of the '<em><b>Destination</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.InterfaceElementView#getInConnections <em>In Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination</em>' reference.
	 * @see #setDestination(InterfaceElementView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getConnectionView_Destination()
	 * @see org.fordiac.ide.model.ui.InterfaceElementView#getInConnections
	 * @model opposite="inConnections"
	 * @generated
	 */
	InterfaceElementView getDestination();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.ConnectionView#getDestination <em>Destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Destination</em>' reference.
	 * @see #getDestination()
	 * @generated
	 */
	void setDestination(InterfaceElementView value);

	/**
	 * Returns the value of the '<em><b>External Destination Interface Element View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Destination Interface Element View</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Destination Interface Element View</em>' reference.
	 * @see #setExternalDestinationInterfaceElementView(SubAppInterfaceElementView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getConnectionView_ExternalDestinationInterfaceElementView()
	 * @model
	 * @generated
	 */
	SubAppInterfaceElementView getExternalDestinationInterfaceElementView();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.ConnectionView#getExternalDestinationInterfaceElementView <em>External Destination Interface Element View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Destination Interface Element View</em>' reference.
	 * @see #getExternalDestinationInterfaceElementView()
	 * @generated
	 */
	void setExternalDestinationInterfaceElementView(SubAppInterfaceElementView value);

	/**
	 * Returns the value of the '<em><b>Internal Destination Interface Element View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Destination Interface Element View</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Destination Interface Element View</em>' reference.
	 * @see #setInternalDestinationInterfaceElementView(InternalSubAppInterfaceElementView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getConnectionView_InternalDestinationInterfaceElementView()
	 * @model
	 * @generated
	 */
	InternalSubAppInterfaceElementView getInternalDestinationInterfaceElementView();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.ConnectionView#getInternalDestinationInterfaceElementView <em>Internal Destination Interface Element View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal Destination Interface Element View</em>' reference.
	 * @see #getInternalDestinationInterfaceElementView()
	 * @generated
	 */
	void setInternalDestinationInterfaceElementView(InternalSubAppInterfaceElementView value);

	/**
	 * Returns the value of the '<em><b>External Source Interface Element View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Source Interface Element View</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Source Interface Element View</em>' reference.
	 * @see #setExternalSourceInterfaceElementView(SubAppInterfaceElementView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getConnectionView_ExternalSourceInterfaceElementView()
	 * @model
	 * @generated
	 */
	SubAppInterfaceElementView getExternalSourceInterfaceElementView();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.ConnectionView#getExternalSourceInterfaceElementView <em>External Source Interface Element View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Source Interface Element View</em>' reference.
	 * @see #getExternalSourceInterfaceElementView()
	 * @generated
	 */
	void setExternalSourceInterfaceElementView(SubAppInterfaceElementView value);

	/**
	 * Returns the value of the '<em><b>Internal Source Interface Element View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Source Interface Element View</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Source Interface Element View</em>' reference.
	 * @see #setInternalSourceInterfaceElementView(InternalSubAppInterfaceElementView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getConnectionView_InternalSourceInterfaceElementView()
	 * @model
	 * @generated
	 */
	InternalSubAppInterfaceElementView getInternalSourceInterfaceElementView();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.ConnectionView#getInternalSourceInterfaceElementView <em>Internal Source Interface Element View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal Source Interface Element View</em>' reference.
	 * @see #getInternalSourceInterfaceElementView()
	 * @generated
	 */
	void setInternalSourceInterfaceElementView(InternalSubAppInterfaceElementView value);

} // ConnectionView
