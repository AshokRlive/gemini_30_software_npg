/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.utils.virtualDNS;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Collection</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link org.fordiac.ide.utils.virtualDNS.VirtualDNSCollection#getVirtualDNSEntries
 * <em>Virtual DNS Entries</em>}</li>
 * <li>{@link org.fordiac.ide.utils.virtualDNS.VirtualDNSCollection#getName <em>
 * Name</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.fordiac.ide.utils.virtualDNS.VirtualDNSPackage#getVirtualDNSCollection()
 * @model
 * @generated
 */
public interface VirtualDNSCollection extends EObject {
	/**
	 * Returns the value of the '<em><b>Virtual DNS Entries</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link org.fordiac.ide.utils.virtualDNS.VirtualDNSEntry}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Virtual DNS Entries</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Virtual DNS Entries</em>' containment
	 *         reference list.
	 * @see org.fordiac.ide.utils.virtualDNS.VirtualDNSPackage#getVirtualDNSCollection_VirtualDNSEntries()
	 * @model containment="true"
	 * @generated
	 */
	EList<VirtualDNSEntry> getVirtualDNSEntries();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.fordiac.ide.utils.virtualDNS.VirtualDNSPackage#getVirtualDNSCollection_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '
	 * {@link org.fordiac.ide.utils.virtualDNS.VirtualDNSCollection#getName
	 * <em>Name</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *          the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	String getValueFor(String name);

} // VirtualDNSCollection
