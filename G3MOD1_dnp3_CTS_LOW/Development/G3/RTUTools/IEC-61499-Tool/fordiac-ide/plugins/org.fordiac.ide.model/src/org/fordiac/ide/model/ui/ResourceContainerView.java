/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Container View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.ResourceContainerView#isShowContent <em>Show Content</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.ResourceContainerView#getResources <em>Resources</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getResourceContainerView()
 * @model
 * @generated
 */
public interface ResourceContainerView extends View {
	/**
	 * Returns the value of the '<em><b>Show Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Content</em>' attribute.
	 * @see #setShowContent(boolean)
	 * @see org.fordiac.ide.model.ui.UiPackage#getResourceContainerView_ShowContent()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isShowContent();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.ResourceContainerView#isShowContent <em>Show Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Content</em>' attribute.
	 * @see #isShowContent()
	 * @generated
	 */
	void setShowContent(boolean value);

	/**
	 * Returns the value of the '<em><b>Resources</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.ui.ResourceView}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resources</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resources</em>' containment reference list.
	 * @see org.fordiac.ide.model.ui.UiPackage#getResourceContainerView_Resources()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<ResourceView> getResources();

} // ResourceContainerView
