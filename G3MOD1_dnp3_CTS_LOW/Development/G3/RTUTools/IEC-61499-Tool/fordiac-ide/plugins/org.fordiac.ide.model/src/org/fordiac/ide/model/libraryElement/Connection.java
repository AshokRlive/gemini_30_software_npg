/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Connection#getDx1 <em>Dx1</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Connection#getDx2 <em>Dx2</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Connection#getDy <em>Dy</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Connection#isResTypeConnection <em>Res Type Connection</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Connection#isResourceConnection <em>Resource Connection</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.Connection#isBrokenConnection <em>Broken Connection</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getConnection()
 * @model abstract="true"
 * @generated
 */
public interface Connection extends ConfigurableObject {
	/**
	 * Returns the value of the '<em><b>Dx1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dx1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dx1</em>' attribute.
	 * @see #setDx1(int)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getConnection_Dx1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='attribute' name='dx1'"
	 * @generated
	 */
	int getDx1();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Connection#getDx1 <em>Dx1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dx1</em>' attribute.
	 * @see #getDx1()
	 * @generated
	 */
	void setDx1(int value);

	/**
	 * Returns the value of the '<em><b>Dx2</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dx2</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dx2</em>' attribute.
	 * @see #setDx2(int)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getConnection_Dx2()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='attribute' name='dx2'"
	 * @generated
	 */
	int getDx2();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Connection#getDx2 <em>Dx2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dx2</em>' attribute.
	 * @see #getDx2()
	 * @generated
	 */
	void setDx2(int value);

	/**
	 * Returns the value of the '<em><b>Dy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dy</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dy</em>' attribute.
	 * @see #setDy(int)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getConnection_Dy()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='attribute' name='dy'"
	 * @generated
	 */
	int getDy();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Connection#getDy <em>Dy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dy</em>' attribute.
	 * @see #getDy()
	 * @generated
	 */
	void setDy(int value);

	/**
	 * Returns the value of the '<em><b>Res Type Connection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Res Type Connection</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Res Type Connection</em>' attribute.
	 * @see #setResTypeConnection(boolean)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getConnection_ResTypeConnection()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isResTypeConnection();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Connection#isResTypeConnection <em>Res Type Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Res Type Connection</em>' attribute.
	 * @see #isResTypeConnection()
	 * @generated
	 */
	void setResTypeConnection(boolean value);

	/**
	 * Returns the value of the '<em><b>Resource Connection</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Connection</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Connection</em>' attribute.
	 * @see #setResourceConnection(boolean)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getConnection_ResourceConnection()
	 * @model default="false" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isResourceConnection();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Connection#isResourceConnection <em>Resource Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Connection</em>' attribute.
	 * @see #isResourceConnection()
	 * @generated
	 */
	void setResourceConnection(boolean value);

	/**
	 * Returns the value of the '<em><b>Broken Connection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Broken Connection</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Broken Connection</em>' attribute.
	 * @see #setBrokenConnection(boolean)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getConnection_BrokenConnection()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isBrokenConnection();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.Connection#isBrokenConnection <em>Broken Connection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Broken Connection</em>' attribute.
	 * @see #isBrokenConnection()
	 * @generated
	 */
	void setBrokenConnection(boolean value);

	void setSource(IInterfaceElement source);
	
	void setDestination(IInterfaceElement dest);
	
	IInterfaceElement getIInterfaceElementSource();
	
	IInterfaceElement getIInterfaceElementDestination();


} // Connection
