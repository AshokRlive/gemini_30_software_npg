/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>With</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.With#getVariables <em>Variables</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getWith()
 * @model
 * @generated
 */
public interface With extends EObject {
	/**
	 * Returns the value of the '<em><b>Variables</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.VarDeclaration#getWiths <em>Withs</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variables</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variables</em>' reference.
	 * @see #setVariables(VarDeclaration)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getWith_Variables()
	 * @see org.fordiac.ide.model.libraryElement.VarDeclaration#getWiths
	 * @model opposite="withs" required="true"
	 * @generated
	 */
	VarDeclaration getVariables();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.With#getVariables <em>Variables</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variables</em>' reference.
	 * @see #getVariables()
	 * @generated
	 */
	void setVariables(VarDeclaration value);

} // With
