package org.fordiac.ide.gef;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;

public interface IEditPartCreatorFactory {

	boolean supportsElement(Class<?> type);
	
	EditPart getEditpart(String diagram, EObject element);
	
}
