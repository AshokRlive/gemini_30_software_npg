/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.Palette.FBTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.Annotation;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.Position;
import org.fordiac.ide.model.libraryElement.ResourceFBNetwork;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>FB</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.FBImpl#getInterface <em>Interface</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.FBImpl#getPosition <em>Position</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.FBImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.FBImpl#isResourceTypeFB <em>Resource Type FB</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.FBImpl#isResourceFB <em>Resource FB</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.FBImpl#getParentCompositeFBType <em>Parent Composite FB Type</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.FBImpl#getFbtPath <em>Fbt Path</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FBImpl extends TypedConfigureableObjectImpl implements FB {
	/**
	 * The cached value of the '{@link #getInterface() <em>Interface</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getInterface()
	 * @generated
	 * @ordered
	 */
	protected InterfaceList interface_;

	/**
	 * The cached value of the '{@link #getPosition() <em>Position</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected Position position;

	/**
	 * The cached value of the '{@link #getResource() <em>Resource</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getResource()
	 * @generated
	 * @ordered
	 */
	protected ResourceFBNetwork resource;

	/**
	 * The default value of the '{@link #isResourceTypeFB() <em>Resource Type FB</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isResourceTypeFB()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RESOURCE_TYPE_FB_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isResourceTypeFB() <em>Resource Type FB</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isResourceTypeFB()
	 * @generated
	 * @ordered
	 */
	protected boolean resourceTypeFB = RESOURCE_TYPE_FB_EDEFAULT;

	/**
	 * The default value of the '{@link #isResourceFB() <em>Resource FB</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isResourceFB()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RESOURCE_FB_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isResourceFB() <em>Resource FB</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isResourceFB()
	 * @generated
	 * @ordered
	 */
	protected boolean resourceFB = RESOURCE_FB_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParentCompositeFBType() <em>Parent Composite FB Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParentCompositeFBType()
	 * @generated
	 * @ordered
	 */
	protected CompositeFBType parentCompositeFBType;

	/**
	 * The default value of the '{@link #getFbtPath() <em>Fbt Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFbtPath()
	 * @generated
	 * @ordered
	 */
	protected static final String FBT_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFbtPath() <em>Fbt Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFbtPath()
	 * @generated
	 * @ordered
	 */
	protected String fbtPath = FBT_PATH_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected FBImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibraryElementPackage.Literals.FB;
	}

	public FBType getFBType() {
		FBType retVal = null;
		
		if(null != getPaletteEntry()){
			if(getPaletteEntry() instanceof FBTypePaletteEntry){
				retVal = ((FBTypePaletteEntry)getPaletteEntry()).getFBType();
			}else if(getPaletteEntry() instanceof AdapterTypePaletteEntry){
				retVal = ((AdapterTypePaletteEntry)getPaletteEntry()).getAdapterType().getAdapterFBType();
			}
		}		
		return retVal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceList getInterface() {
		if (interface_ != null && interface_.eIsProxy()) {
			InternalEObject oldInterface = (InternalEObject)interface_;
			interface_ = (InterfaceList)eResolveProxy(oldInterface);
			if (interface_ != oldInterface) {
				InternalEObject newInterface = (InternalEObject)interface_;
				NotificationChain msgs =  oldInterface.eInverseRemove(this, LibraryElementPackage.INTERFACE_LIST__FB, InterfaceList.class, null);
				if (newInterface.eInternalContainer() == null) {
					msgs =  newInterface.eInverseAdd(this, LibraryElementPackage.INTERFACE_LIST__FB, InterfaceList.class, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LibraryElementPackage.FB__INTERFACE, oldInterface, interface_));
			}
		}
		return interface_;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceList basicGetInterface() {
		return interface_;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterface(InterfaceList newInterface,
			NotificationChain msgs) {
		InterfaceList oldInterface = interface_;
		interface_ = newInterface;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LibraryElementPackage.FB__INTERFACE, oldInterface, newInterface);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterface(InterfaceList newInterface) {
		if (newInterface != interface_) {
			NotificationChain msgs = null;
			if (interface_ != null)
				msgs = ((InternalEObject)interface_).eInverseRemove(this, LibraryElementPackage.INTERFACE_LIST__FB, InterfaceList.class, msgs);
			if (newInterface != null)
				msgs = ((InternalEObject)newInterface).eInverseAdd(this, LibraryElementPackage.INTERFACE_LIST__FB, InterfaceList.class, msgs);
			msgs = basicSetInterface(newInterface, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.FB__INTERFACE, newInterface, newInterface));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Position getPosition() {
		if (position != null && position.eIsProxy()) {
			InternalEObject oldPosition = (InternalEObject)position;
			position = (Position)eResolveProxy(oldPosition);
			if (position != oldPosition) {
				InternalEObject newPosition = (InternalEObject)position;
				NotificationChain msgs = oldPosition.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LibraryElementPackage.FB__POSITION, null, null);
				if (newPosition.eInternalContainer() == null) {
					msgs = newPosition.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LibraryElementPackage.FB__POSITION, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LibraryElementPackage.FB__POSITION, oldPosition, position));
			}
		}
		return position;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Position basicGetPosition() {
		return position;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPosition(Position newPosition,
			NotificationChain msgs) {
		Position oldPosition = position;
		position = newPosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LibraryElementPackage.FB__POSITION, oldPosition, newPosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(Position newPosition) {
		if (newPosition != position) {
			NotificationChain msgs = null;
			if (position != null)
				msgs = ((InternalEObject)position).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LibraryElementPackage.FB__POSITION, null, msgs);
			if (newPosition != null)
				msgs = ((InternalEObject)newPosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LibraryElementPackage.FB__POSITION, null, msgs);
			msgs = basicSetPosition(newPosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.FB__POSITION, newPosition, newPosition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceFBNetwork getResource() {
		if (resource != null && resource.eIsProxy()) {
			InternalEObject oldResource = (InternalEObject)resource;
			resource = (ResourceFBNetwork)eResolveProxy(oldResource);
			if (resource != oldResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LibraryElementPackage.FB__RESOURCE, oldResource, resource));
			}
		}
		return resource;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceFBNetwork basicGetResource() {
		return resource;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResource(ResourceFBNetwork newResource,
			NotificationChain msgs) {
		ResourceFBNetwork oldResource = resource;
		resource = newResource;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LibraryElementPackage.FB__RESOURCE, oldResource, newResource);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setResource(ResourceFBNetwork newResource) {
		if (newResource != resource) {
			NotificationChain msgs = null;
			if (resource != null)
				msgs = ((InternalEObject)resource).eInverseRemove(this, LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_FBS, ResourceFBNetwork.class, msgs);
			if (newResource != null)
				msgs = ((InternalEObject)newResource).eInverseAdd(this, LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_FBS, ResourceFBNetwork.class, msgs);
			msgs = basicSetResource(newResource, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.FB__RESOURCE, newResource, newResource));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isResourceTypeFB() {
		return resourceTypeFB;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourceTypeFB(boolean newResourceTypeFB) {
		boolean oldResourceTypeFB = resourceTypeFB;
		resourceTypeFB = newResourceTypeFB;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.FB__RESOURCE_TYPE_FB, oldResourceTypeFB, resourceTypeFB));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isResourceFB() {
		return resourceFB;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourceFB(boolean newResourceFB) {
		boolean oldResourceFB = resourceFB;
		resourceFB = newResourceFB;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.FB__RESOURCE_FB, oldResourceFB, resourceFB));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeFBType getParentCompositeFBType() {
		if (parentCompositeFBType != null && parentCompositeFBType.eIsProxy()) {
			InternalEObject oldParentCompositeFBType = (InternalEObject)parentCompositeFBType;
			parentCompositeFBType = (CompositeFBType)eResolveProxy(oldParentCompositeFBType);
			if (parentCompositeFBType != oldParentCompositeFBType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LibraryElementPackage.FB__PARENT_COMPOSITE_FB_TYPE, oldParentCompositeFBType, parentCompositeFBType));
			}
		}
		return parentCompositeFBType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeFBType basicGetParentCompositeFBType() {
		return parentCompositeFBType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentCompositeFBType(
			CompositeFBType newParentCompositeFBType) {
		CompositeFBType oldParentCompositeFBType = parentCompositeFBType;
		parentCompositeFBType = newParentCompositeFBType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.FB__PARENT_COMPOSITE_FB_TYPE, oldParentCompositeFBType, parentCompositeFBType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFbtPath() {
		return fbtPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFbtPath(String newFbtPath) {
		String oldFbtPath = fbtPath;
		fbtPath = newFbtPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.FB__FBT_PATH, oldFbtPath, fbtPath));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.FB__INTERFACE:
				if (interface_ != null)
					msgs = ((InternalEObject)interface_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LibraryElementPackage.FB__INTERFACE, null, msgs);
				return basicSetInterface((InterfaceList)otherEnd, msgs);
			case LibraryElementPackage.FB__RESOURCE:
				if (resource != null)
					msgs = ((InternalEObject)resource).eInverseRemove(this, LibraryElementPackage.RESOURCE_FB_NETWORK__MAPPED_FBS, ResourceFBNetwork.class, msgs);
				return basicSetResource((ResourceFBNetwork)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.FB__INTERFACE:
				return basicSetInterface(null, msgs);
			case LibraryElementPackage.FB__POSITION:
				return basicSetPosition(null, msgs);
			case LibraryElementPackage.FB__RESOURCE:
				return basicSetResource(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibraryElementPackage.FB__INTERFACE:
				if (resolve) return getInterface();
				return basicGetInterface();
			case LibraryElementPackage.FB__POSITION:
				if (resolve) return getPosition();
				return basicGetPosition();
			case LibraryElementPackage.FB__RESOURCE:
				if (resolve) return getResource();
				return basicGetResource();
			case LibraryElementPackage.FB__RESOURCE_TYPE_FB:
				return isResourceTypeFB();
			case LibraryElementPackage.FB__RESOURCE_FB:
				return isResourceFB();
			case LibraryElementPackage.FB__PARENT_COMPOSITE_FB_TYPE:
				if (resolve) return getParentCompositeFBType();
				return basicGetParentCompositeFBType();
			case LibraryElementPackage.FB__FBT_PATH:
				return getFbtPath();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibraryElementPackage.FB__INTERFACE:
				setInterface((InterfaceList)newValue);
				return;
			case LibraryElementPackage.FB__POSITION:
				setPosition((Position)newValue);
				return;
			case LibraryElementPackage.FB__RESOURCE:
				setResource((ResourceFBNetwork)newValue);
				return;
			case LibraryElementPackage.FB__RESOURCE_TYPE_FB:
				setResourceTypeFB((Boolean)newValue);
				return;
			case LibraryElementPackage.FB__RESOURCE_FB:
				setResourceFB((Boolean)newValue);
				return;
			case LibraryElementPackage.FB__PARENT_COMPOSITE_FB_TYPE:
				setParentCompositeFBType((CompositeFBType)newValue);
				return;
			case LibraryElementPackage.FB__FBT_PATH:
				setFbtPath((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.FB__INTERFACE:
				setInterface((InterfaceList)null);
				return;
			case LibraryElementPackage.FB__POSITION:
				setPosition((Position)null);
				return;
			case LibraryElementPackage.FB__RESOURCE:
				setResource((ResourceFBNetwork)null);
				return;
			case LibraryElementPackage.FB__RESOURCE_TYPE_FB:
				setResourceTypeFB(RESOURCE_TYPE_FB_EDEFAULT);
				return;
			case LibraryElementPackage.FB__RESOURCE_FB:
				setResourceFB(RESOURCE_FB_EDEFAULT);
				return;
			case LibraryElementPackage.FB__PARENT_COMPOSITE_FB_TYPE:
				setParentCompositeFBType((CompositeFBType)null);
				return;
			case LibraryElementPackage.FB__FBT_PATH:
				setFbtPath(FBT_PATH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.FB__INTERFACE:
				return interface_ != null;
			case LibraryElementPackage.FB__POSITION:
				return position != null;
			case LibraryElementPackage.FB__RESOURCE:
				return resource != null;
			case LibraryElementPackage.FB__RESOURCE_TYPE_FB:
				return resourceTypeFB != RESOURCE_TYPE_FB_EDEFAULT;
			case LibraryElementPackage.FB__RESOURCE_FB:
				return resourceFB != RESOURCE_FB_EDEFAULT;
			case LibraryElementPackage.FB__PARENT_COMPOSITE_FB_TYPE:
				return parentCompositeFBType != null;
			case LibraryElementPackage.FB__FBT_PATH:
				return FBT_PATH_EDEFAULT == null ? fbtPath != null : !FBT_PATH_EDEFAULT.equals(fbtPath);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (resourceTypeFB: ");
		result.append(resourceTypeFB);
		result.append(", resourceFB: ");
		result.append(resourceFB);
		result.append(", fbtPath: ");
		result.append(fbtPath);
		result.append(')');
		return result.toString();
	}

	@Override
	public void setName(final String newName) {
		String oldName = name;
		name = newName;

		getAnnotations().clear();
		
		NameRepository.checkNameIdentifier(this);
		
		// unique check needs to be before sending the notification in order
		// that the
		// annotations are correctly displayed in the graphical editors

		if (getParentCompositeFBType() != null) {
			for (FB fb : getParentCompositeFBType().getFBNetwork().getFBs()) {
				if (!fb.equals(this)) {
					if (newName.equals(fb.getName())) {
						Annotation ano = createAnnotation("FB Name not Unique");
						ano.setServity(2); // 2 means error!
					}
				}
			}
		} else {
			boolean unique = NameRepository.getInstance()
					.isSystemUniqueFBInstanceName(this);
			if (!unique) {
				Annotation ano = createAnnotation("FB Name not Unique");
				ano.setServity(2); // 2 means error!
			}
		}
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibraryElementPackage.ID_OBJECT__NAME, oldName, name));
	}

	public IInterfaceElement getInterfaceElement(String name) {
		if (getInterface() != null) {
			IInterfaceElement element = getInterface().getEvent(name);
			if (element == null) {
				element = getInterface().getVariable(name);
			}
			return element;
		}
		return null;
	}
} // FBImpl
