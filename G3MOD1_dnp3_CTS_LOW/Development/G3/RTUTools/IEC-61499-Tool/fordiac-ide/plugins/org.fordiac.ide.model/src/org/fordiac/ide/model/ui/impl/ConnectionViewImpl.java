/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connection View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ConnectionViewImpl#getConnectionElement <em>Connection Element</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ConnectionViewImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ConnectionViewImpl#getDestination <em>Destination</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ConnectionViewImpl#getExternalDestinationInterfaceElementView <em>External Destination Interface Element View</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ConnectionViewImpl#getInternalDestinationInterfaceElementView <em>Internal Destination Interface Element View</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ConnectionViewImpl#getExternalSourceInterfaceElementView <em>External Source Interface Element View</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ConnectionViewImpl#getInternalSourceInterfaceElementView <em>Internal Source Interface Element View</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConnectionViewImpl extends EObjectImpl implements ConnectionView {
	/**
	 * The cached value of the '{@link #getConnectionElement() <em>Connection Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectionElement()
	 * @generated
	 * @ordered
	 */
	protected Connection connectionElement;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected InterfaceElementView source;

	/**
	 * The cached value of the '{@link #getDestination() <em>Destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestination()
	 * @generated
	 * @ordered
	 */
	protected InterfaceElementView destination;

	/**
	 * The cached value of the '{@link #getExternalDestinationInterfaceElementView() <em>External Destination Interface Element View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalDestinationInterfaceElementView()
	 * @generated
	 * @ordered
	 */
	protected SubAppInterfaceElementView externalDestinationInterfaceElementView;

	/**
	 * The cached value of the '{@link #getInternalDestinationInterfaceElementView() <em>Internal Destination Interface Element View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalDestinationInterfaceElementView()
	 * @generated
	 * @ordered
	 */
	protected InternalSubAppInterfaceElementView internalDestinationInterfaceElementView;

	/**
	 * The cached value of the '{@link #getExternalSourceInterfaceElementView() <em>External Source Interface Element View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalSourceInterfaceElementView()
	 * @generated
	 * @ordered
	 */
	protected SubAppInterfaceElementView externalSourceInterfaceElementView;

	/**
	 * The cached value of the '{@link #getInternalSourceInterfaceElementView() <em>Internal Source Interface Element View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalSourceInterfaceElementView()
	 * @generated
	 * @ordered
	 */
	protected InternalSubAppInterfaceElementView internalSourceInterfaceElementView;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectionViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.CONNECTION_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connection getConnectionElement() {
		if (connectionElement != null && connectionElement.eIsProxy()) {
			InternalEObject oldConnectionElement = (InternalEObject)connectionElement;
			connectionElement = (Connection)eResolveProxy(oldConnectionElement);
			if (connectionElement != oldConnectionElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.CONNECTION_VIEW__CONNECTION_ELEMENT, oldConnectionElement, connectionElement));
			}
		}
		return connectionElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connection basicGetConnectionElement() {
		return connectionElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnectionElement(Connection newConnectionElement) {
		Connection oldConnectionElement = connectionElement;
		connectionElement = newConnectionElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.CONNECTION_VIEW__CONNECTION_ELEMENT, oldConnectionElement, connectionElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceElementView getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject)source;
			source = (InterfaceElementView)eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.CONNECTION_VIEW__SOURCE, oldSource, source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceElementView basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSource(InterfaceElementView newSource, NotificationChain msgs) {
		InterfaceElementView oldSource = source;
		source = newSource;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.CONNECTION_VIEW__SOURCE, oldSource, newSource);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(InterfaceElementView newSource) {
		if (newSource != source) {
			NotificationChain msgs = null;
			if (source != null)
				msgs = ((InternalEObject)source).eInverseRemove(this, UiPackage.INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS, InterfaceElementView.class, msgs);
			if (newSource != null)
				msgs = ((InternalEObject)newSource).eInverseAdd(this, UiPackage.INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS, InterfaceElementView.class, msgs);
			msgs = basicSetSource(newSource, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.CONNECTION_VIEW__SOURCE, newSource, newSource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceElementView getDestination() {
		if (destination != null && destination.eIsProxy()) {
			InternalEObject oldDestination = (InternalEObject)destination;
			destination = (InterfaceElementView)eResolveProxy(oldDestination);
			if (destination != oldDestination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.CONNECTION_VIEW__DESTINATION, oldDestination, destination));
			}
		}
		return destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceElementView basicGetDestination() {
		return destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDestination(InterfaceElementView newDestination, NotificationChain msgs) {
		InterfaceElementView oldDestination = destination;
		destination = newDestination;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.CONNECTION_VIEW__DESTINATION, oldDestination, newDestination);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDestination(InterfaceElementView newDestination) {
		if (newDestination != destination) {
			NotificationChain msgs = null;
			if (destination != null)
				msgs = ((InternalEObject)destination).eInverseRemove(this, UiPackage.INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS, InterfaceElementView.class, msgs);
			if (newDestination != null)
				msgs = ((InternalEObject)newDestination).eInverseAdd(this, UiPackage.INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS, InterfaceElementView.class, msgs);
			msgs = basicSetDestination(newDestination, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.CONNECTION_VIEW__DESTINATION, newDestination, newDestination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppInterfaceElementView getExternalDestinationInterfaceElementView() {
		if (externalDestinationInterfaceElementView != null && externalDestinationInterfaceElementView.eIsProxy()) {
			InternalEObject oldExternalDestinationInterfaceElementView = (InternalEObject)externalDestinationInterfaceElementView;
			externalDestinationInterfaceElementView = (SubAppInterfaceElementView)eResolveProxy(oldExternalDestinationInterfaceElementView);
			if (externalDestinationInterfaceElementView != oldExternalDestinationInterfaceElementView) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.CONNECTION_VIEW__EXTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW, oldExternalDestinationInterfaceElementView, externalDestinationInterfaceElementView));
			}
		}
		return externalDestinationInterfaceElementView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppInterfaceElementView basicGetExternalDestinationInterfaceElementView() {
		return externalDestinationInterfaceElementView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalDestinationInterfaceElementView(SubAppInterfaceElementView newExternalDestinationInterfaceElementView) {
		SubAppInterfaceElementView oldExternalDestinationInterfaceElementView = externalDestinationInterfaceElementView;
		externalDestinationInterfaceElementView = newExternalDestinationInterfaceElementView;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.CONNECTION_VIEW__EXTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW, oldExternalDestinationInterfaceElementView, externalDestinationInterfaceElementView));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalSubAppInterfaceElementView getInternalDestinationInterfaceElementView() {
		if (internalDestinationInterfaceElementView != null && internalDestinationInterfaceElementView.eIsProxy()) {
			InternalEObject oldInternalDestinationInterfaceElementView = (InternalEObject)internalDestinationInterfaceElementView;
			internalDestinationInterfaceElementView = (InternalSubAppInterfaceElementView)eResolveProxy(oldInternalDestinationInterfaceElementView);
			if (internalDestinationInterfaceElementView != oldInternalDestinationInterfaceElementView) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.CONNECTION_VIEW__INTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW, oldInternalDestinationInterfaceElementView, internalDestinationInterfaceElementView));
			}
		}
		return internalDestinationInterfaceElementView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalSubAppInterfaceElementView basicGetInternalDestinationInterfaceElementView() {
		return internalDestinationInterfaceElementView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalDestinationInterfaceElementView(InternalSubAppInterfaceElementView newInternalDestinationInterfaceElementView) {
		InternalSubAppInterfaceElementView oldInternalDestinationInterfaceElementView = internalDestinationInterfaceElementView;
		internalDestinationInterfaceElementView = newInternalDestinationInterfaceElementView;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.CONNECTION_VIEW__INTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW, oldInternalDestinationInterfaceElementView, internalDestinationInterfaceElementView));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppInterfaceElementView getExternalSourceInterfaceElementView() {
		if (externalSourceInterfaceElementView != null && externalSourceInterfaceElementView.eIsProxy()) {
			InternalEObject oldExternalSourceInterfaceElementView = (InternalEObject)externalSourceInterfaceElementView;
			externalSourceInterfaceElementView = (SubAppInterfaceElementView)eResolveProxy(oldExternalSourceInterfaceElementView);
			if (externalSourceInterfaceElementView != oldExternalSourceInterfaceElementView) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.CONNECTION_VIEW__EXTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW, oldExternalSourceInterfaceElementView, externalSourceInterfaceElementView));
			}
		}
		return externalSourceInterfaceElementView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppInterfaceElementView basicGetExternalSourceInterfaceElementView() {
		return externalSourceInterfaceElementView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalSourceInterfaceElementView(SubAppInterfaceElementView newExternalSourceInterfaceElementView) {
		SubAppInterfaceElementView oldExternalSourceInterfaceElementView = externalSourceInterfaceElementView;
		externalSourceInterfaceElementView = newExternalSourceInterfaceElementView;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.CONNECTION_VIEW__EXTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW, oldExternalSourceInterfaceElementView, externalSourceInterfaceElementView));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalSubAppInterfaceElementView getInternalSourceInterfaceElementView() {
		if (internalSourceInterfaceElementView != null && internalSourceInterfaceElementView.eIsProxy()) {
			InternalEObject oldInternalSourceInterfaceElementView = (InternalEObject)internalSourceInterfaceElementView;
			internalSourceInterfaceElementView = (InternalSubAppInterfaceElementView)eResolveProxy(oldInternalSourceInterfaceElementView);
			if (internalSourceInterfaceElementView != oldInternalSourceInterfaceElementView) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.CONNECTION_VIEW__INTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW, oldInternalSourceInterfaceElementView, internalSourceInterfaceElementView));
			}
		}
		return internalSourceInterfaceElementView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalSubAppInterfaceElementView basicGetInternalSourceInterfaceElementView() {
		return internalSourceInterfaceElementView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalSourceInterfaceElementView(InternalSubAppInterfaceElementView newInternalSourceInterfaceElementView) {
		InternalSubAppInterfaceElementView oldInternalSourceInterfaceElementView = internalSourceInterfaceElementView;
		internalSourceInterfaceElementView = newInternalSourceInterfaceElementView;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.CONNECTION_VIEW__INTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW, oldInternalSourceInterfaceElementView, internalSourceInterfaceElementView));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.CONNECTION_VIEW__SOURCE:
				if (source != null)
					msgs = ((InternalEObject)source).eInverseRemove(this, UiPackage.INTERFACE_ELEMENT_VIEW__OUT_CONNECTIONS, InterfaceElementView.class, msgs);
				return basicSetSource((InterfaceElementView)otherEnd, msgs);
			case UiPackage.CONNECTION_VIEW__DESTINATION:
				if (destination != null)
					msgs = ((InternalEObject)destination).eInverseRemove(this, UiPackage.INTERFACE_ELEMENT_VIEW__IN_CONNECTIONS, InterfaceElementView.class, msgs);
				return basicSetDestination((InterfaceElementView)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.CONNECTION_VIEW__SOURCE:
				return basicSetSource(null, msgs);
			case UiPackage.CONNECTION_VIEW__DESTINATION:
				return basicSetDestination(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.CONNECTION_VIEW__CONNECTION_ELEMENT:
				if (resolve) return getConnectionElement();
				return basicGetConnectionElement();
			case UiPackage.CONNECTION_VIEW__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case UiPackage.CONNECTION_VIEW__DESTINATION:
				if (resolve) return getDestination();
				return basicGetDestination();
			case UiPackage.CONNECTION_VIEW__EXTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW:
				if (resolve) return getExternalDestinationInterfaceElementView();
				return basicGetExternalDestinationInterfaceElementView();
			case UiPackage.CONNECTION_VIEW__INTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW:
				if (resolve) return getInternalDestinationInterfaceElementView();
				return basicGetInternalDestinationInterfaceElementView();
			case UiPackage.CONNECTION_VIEW__EXTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW:
				if (resolve) return getExternalSourceInterfaceElementView();
				return basicGetExternalSourceInterfaceElementView();
			case UiPackage.CONNECTION_VIEW__INTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW:
				if (resolve) return getInternalSourceInterfaceElementView();
				return basicGetInternalSourceInterfaceElementView();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.CONNECTION_VIEW__CONNECTION_ELEMENT:
				setConnectionElement((Connection)newValue);
				return;
			case UiPackage.CONNECTION_VIEW__SOURCE:
				setSource((InterfaceElementView)newValue);
				return;
			case UiPackage.CONNECTION_VIEW__DESTINATION:
				setDestination((InterfaceElementView)newValue);
				return;
			case UiPackage.CONNECTION_VIEW__EXTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW:
				setExternalDestinationInterfaceElementView((SubAppInterfaceElementView)newValue);
				return;
			case UiPackage.CONNECTION_VIEW__INTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW:
				setInternalDestinationInterfaceElementView((InternalSubAppInterfaceElementView)newValue);
				return;
			case UiPackage.CONNECTION_VIEW__EXTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW:
				setExternalSourceInterfaceElementView((SubAppInterfaceElementView)newValue);
				return;
			case UiPackage.CONNECTION_VIEW__INTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW:
				setInternalSourceInterfaceElementView((InternalSubAppInterfaceElementView)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.CONNECTION_VIEW__CONNECTION_ELEMENT:
				setConnectionElement((Connection)null);
				return;
			case UiPackage.CONNECTION_VIEW__SOURCE:
				setSource((InterfaceElementView)null);
				return;
			case UiPackage.CONNECTION_VIEW__DESTINATION:
				setDestination((InterfaceElementView)null);
				return;
			case UiPackage.CONNECTION_VIEW__EXTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW:
				setExternalDestinationInterfaceElementView((SubAppInterfaceElementView)null);
				return;
			case UiPackage.CONNECTION_VIEW__INTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW:
				setInternalDestinationInterfaceElementView((InternalSubAppInterfaceElementView)null);
				return;
			case UiPackage.CONNECTION_VIEW__EXTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW:
				setExternalSourceInterfaceElementView((SubAppInterfaceElementView)null);
				return;
			case UiPackage.CONNECTION_VIEW__INTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW:
				setInternalSourceInterfaceElementView((InternalSubAppInterfaceElementView)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.CONNECTION_VIEW__CONNECTION_ELEMENT:
				return connectionElement != null;
			case UiPackage.CONNECTION_VIEW__SOURCE:
				return source != null;
			case UiPackage.CONNECTION_VIEW__DESTINATION:
				return destination != null;
			case UiPackage.CONNECTION_VIEW__EXTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW:
				return externalDestinationInterfaceElementView != null;
			case UiPackage.CONNECTION_VIEW__INTERNAL_DESTINATION_INTERFACE_ELEMENT_VIEW:
				return internalDestinationInterfaceElementView != null;
			case UiPackage.CONNECTION_VIEW__EXTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW:
				return externalSourceInterfaceElementView != null;
			case UiPackage.CONNECTION_VIEW__INTERNAL_SOURCE_INTERFACE_ELEMENT_VIEW:
				return internalSourceInterfaceElementView != null;
		}
		return super.eIsSet(featureID);
	}

} //ConnectionViewImpl
