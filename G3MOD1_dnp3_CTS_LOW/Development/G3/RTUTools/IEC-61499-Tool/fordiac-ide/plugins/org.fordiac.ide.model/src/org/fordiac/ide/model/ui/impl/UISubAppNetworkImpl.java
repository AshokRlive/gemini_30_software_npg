/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UI Sub App Network</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.UISubAppNetworkImpl#getSubAppNetwork <em>Sub App Network</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.UISubAppNetworkImpl#getFileName <em>File Name</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.UISubAppNetworkImpl#getInterfaceElements <em>Interface Elements</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.UISubAppNetworkImpl#getSubAppView <em>Sub App View</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.UISubAppNetworkImpl#getRootApplication <em>Root Application</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UISubAppNetworkImpl extends DiagramImpl implements UISubAppNetwork {
	/**
	 * The cached value of the '{@link #getSubAppNetwork() <em>Sub App Network</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubAppNetwork()
	 * @generated
	 * @ordered
	 */
	protected SubAppNetwork subAppNetwork;

	/**
	 * The default value of the '{@link #getFileName() <em>File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFileName()
	 * @generated
	 * @ordered
	 */
	protected static final String FILE_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getFileName() <em>File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFileName()
	 * @generated
	 * @ordered
	 */
	protected String fileName = FILE_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInterfaceElements() <em>Interface Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaceElements()
	 * @generated
	 * @ordered
	 */
	protected EList<InternalSubAppInterfaceElementView> interfaceElements;

	/**
	 * The cached value of the '{@link #getRootApplication() <em>Root Application</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootApplication()
	 * @generated
	 * @ordered
	 */
	protected UIFBNetwork rootApplication;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UISubAppNetworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.UI_SUB_APP_NETWORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppNetwork getSubAppNetwork() {
		if (subAppNetwork != null && subAppNetwork.eIsProxy()) {
			InternalEObject oldSubAppNetwork = (InternalEObject)subAppNetwork;
			subAppNetwork = (SubAppNetwork)eResolveProxy(oldSubAppNetwork);
			if (subAppNetwork != oldSubAppNetwork) {
				InternalEObject newSubAppNetwork = (InternalEObject)subAppNetwork;
				NotificationChain msgs = oldSubAppNetwork.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.UI_SUB_APP_NETWORK__SUB_APP_NETWORK, null, null);
				if (newSubAppNetwork.eInternalContainer() == null) {
					msgs = newSubAppNetwork.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.UI_SUB_APP_NETWORK__SUB_APP_NETWORK, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.UI_SUB_APP_NETWORK__SUB_APP_NETWORK, oldSubAppNetwork, subAppNetwork));
			}
		}
		return subAppNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppNetwork basicGetSubAppNetwork() {
		return subAppNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubAppNetwork(SubAppNetwork newSubAppNetwork, NotificationChain msgs) {
		SubAppNetwork oldSubAppNetwork = subAppNetwork;
		subAppNetwork = newSubAppNetwork;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.UI_SUB_APP_NETWORK__SUB_APP_NETWORK, oldSubAppNetwork, newSubAppNetwork);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubAppNetwork(SubAppNetwork newSubAppNetwork) {
		if (newSubAppNetwork != subAppNetwork) {
			NotificationChain msgs = null;
			if (subAppNetwork != null)
				msgs = ((InternalEObject)subAppNetwork).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.UI_SUB_APP_NETWORK__SUB_APP_NETWORK, null, msgs);
			if (newSubAppNetwork != null)
				msgs = ((InternalEObject)newSubAppNetwork).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.UI_SUB_APP_NETWORK__SUB_APP_NETWORK, null, msgs);
			msgs = basicSetSubAppNetwork(newSubAppNetwork, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.UI_SUB_APP_NETWORK__SUB_APP_NETWORK, newSubAppNetwork, newSubAppNetwork));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFileName(String newFileName) {
		String oldFileName = fileName;
		fileName = newFileName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.UI_SUB_APP_NETWORK__FILE_NAME, oldFileName, fileName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InternalSubAppInterfaceElementView> getInterfaceElements() {
		if (interfaceElements == null) {
			interfaceElements = new EObjectContainmentEList.Resolving<InternalSubAppInterfaceElementView>(InternalSubAppInterfaceElementView.class, this, UiPackage.UI_SUB_APP_NETWORK__INTERFACE_ELEMENTS);
		}
		return interfaceElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppView getSubAppView() {
		if (eContainerFeatureID() != UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW) return null;
		return (SubAppView)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppView basicGetSubAppView() {
		if (eContainerFeatureID() != UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW) return null;
		return (SubAppView)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubAppView(SubAppView newSubAppView, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newSubAppView, UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubAppView(SubAppView newSubAppView) {
		if (newSubAppView != eInternalContainer() || (eContainerFeatureID() != UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW && newSubAppView != null)) {
			if (EcoreUtil.isAncestor(this, newSubAppView))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSubAppView != null)
				msgs = ((InternalEObject)newSubAppView).eInverseAdd(this, UiPackage.SUB_APP_VIEW__UI_SUB_APP_NETWORK, SubAppView.class, msgs);
			msgs = basicSetSubAppView(newSubAppView, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW, newSubAppView, newSubAppView));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UIFBNetwork getRootApplication() {
		if (rootApplication != null && rootApplication.eIsProxy()) {
			InternalEObject oldRootApplication = (InternalEObject)rootApplication;
			rootApplication = (UIFBNetwork)eResolveProxy(oldRootApplication);
			if (rootApplication != oldRootApplication) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.UI_SUB_APP_NETWORK__ROOT_APPLICATION, oldRootApplication, rootApplication));
			}
		}
		return rootApplication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UIFBNetwork basicGetRootApplication() {
		return rootApplication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootApplication(UIFBNetwork newRootApplication) {
		UIFBNetwork oldRootApplication = rootApplication;
		rootApplication = newRootApplication;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.UI_SUB_APP_NETWORK__ROOT_APPLICATION, oldRootApplication, rootApplication));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetSubAppView((SubAppView)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.UI_SUB_APP_NETWORK__SUB_APP_NETWORK:
				return basicSetSubAppNetwork(null, msgs);
			case UiPackage.UI_SUB_APP_NETWORK__INTERFACE_ELEMENTS:
				return ((InternalEList<?>)getInterfaceElements()).basicRemove(otherEnd, msgs);
			case UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW:
				return basicSetSubAppView(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW:
				return eInternalContainer().eInverseRemove(this, UiPackage.SUB_APP_VIEW__UI_SUB_APP_NETWORK, SubAppView.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.UI_SUB_APP_NETWORK__SUB_APP_NETWORK:
				if (resolve) return getSubAppNetwork();
				return basicGetSubAppNetwork();
			case UiPackage.UI_SUB_APP_NETWORK__FILE_NAME:
				return getFileName();
			case UiPackage.UI_SUB_APP_NETWORK__INTERFACE_ELEMENTS:
				return getInterfaceElements();
			case UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW:
				if (resolve) return getSubAppView();
				return basicGetSubAppView();
			case UiPackage.UI_SUB_APP_NETWORK__ROOT_APPLICATION:
				if (resolve) return getRootApplication();
				return basicGetRootApplication();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.UI_SUB_APP_NETWORK__SUB_APP_NETWORK:
				setSubAppNetwork((SubAppNetwork)newValue);
				return;
			case UiPackage.UI_SUB_APP_NETWORK__FILE_NAME:
				setFileName((String)newValue);
				return;
			case UiPackage.UI_SUB_APP_NETWORK__INTERFACE_ELEMENTS:
				getInterfaceElements().clear();
				getInterfaceElements().addAll((Collection<? extends InternalSubAppInterfaceElementView>)newValue);
				return;
			case UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW:
				setSubAppView((SubAppView)newValue);
				return;
			case UiPackage.UI_SUB_APP_NETWORK__ROOT_APPLICATION:
				setRootApplication((UIFBNetwork)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.UI_SUB_APP_NETWORK__SUB_APP_NETWORK:
				setSubAppNetwork((SubAppNetwork)null);
				return;
			case UiPackage.UI_SUB_APP_NETWORK__FILE_NAME:
				setFileName(FILE_NAME_EDEFAULT);
				return;
			case UiPackage.UI_SUB_APP_NETWORK__INTERFACE_ELEMENTS:
				getInterfaceElements().clear();
				return;
			case UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW:
				setSubAppView((SubAppView)null);
				return;
			case UiPackage.UI_SUB_APP_NETWORK__ROOT_APPLICATION:
				setRootApplication((UIFBNetwork)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.UI_SUB_APP_NETWORK__SUB_APP_NETWORK:
				return subAppNetwork != null;
			case UiPackage.UI_SUB_APP_NETWORK__FILE_NAME:
				return FILE_NAME_EDEFAULT == null ? fileName != null : !FILE_NAME_EDEFAULT.equals(fileName);
			case UiPackage.UI_SUB_APP_NETWORK__INTERFACE_ELEMENTS:
				return interfaceElements != null && !interfaceElements.isEmpty();
			case UiPackage.UI_SUB_APP_NETWORK__SUB_APP_VIEW:
				return basicGetSubAppView() != null;
			case UiPackage.UI_SUB_APP_NETWORK__ROOT_APPLICATION:
				return rootApplication != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (fileName: ");
		result.append(fileName);
		result.append(')');
		return result.toString();
	}

	public SubAppNetwork getNetwork() {
		return getSubAppNetwork();
	}

	public SubAppNetwork getFunctionBlockNetwork() {
		return getNetwork();
	}

} //UISubAppNetworkImpl
