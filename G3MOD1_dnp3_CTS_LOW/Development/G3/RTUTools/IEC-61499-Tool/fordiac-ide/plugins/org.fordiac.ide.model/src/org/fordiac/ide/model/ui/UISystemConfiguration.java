/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UI System Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.UISystemConfiguration#getSystemConfigNetwork <em>System Config Network</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getUISystemConfiguration()
 * @model
 * @generated
 */
public interface UISystemConfiguration extends Diagram {
	/**
	 * Returns the value of the '<em><b>System Config Network</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Config Network</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Config Network</em>' containment reference.
	 * @see #setSystemConfigNetwork(SystemConfigurationNetwork)
	 * @see org.fordiac.ide.model.ui.UiPackage#getUISystemConfiguration_SystemConfigNetwork()
	 * @model containment="true" resolveProxies="true" required="true"
	 * @generated
	 */
	SystemConfigurationNetwork getSystemConfigNetwork();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.UISystemConfiguration#getSystemConfigNetwork <em>System Config Network</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Config Network</em>' containment reference.
	 * @see #getSystemConfigNetwork()
	 * @generated
	 */
	void setSystemConfigNetwork(SystemConfigurationNetwork value);

} // UISystemConfiguration
