/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.utils.iec61499;

/**
 * This class provides static methods to check whether a string is a valid IEC
 * 61499 compliant identifier.
 */
public class IdentifierVerifyer {

	/**
	 * Checks if is valid identifier.
	 * 
	 * @param identifier
	 *            the identifier
	 * 
	 * @return true, if is valid identifier
	 */
	public static boolean isValidIdentifier(String identifier) {
		if (identifier.length() < 1) {
			return false;
		}
		char firstChar = identifier.charAt(0);
		if (firstChar != '_' && !Character.isLetter(firstChar)) {
			return false;
		}
		for (int i = 0; i < identifier.length(); i++) {
			Character myChar = identifier.charAt(i);
			if ((!Character.isLetter(myChar) && !Character.isDigit(myChar) && myChar != '_')) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if is valid identifier and returns and error message why the
	 * identifier is not valid.
	 * 
	 * @param identifier
	 *            the identifier
	 * 
	 * @return null if it is an valid identifier otherwise an Error message
	 */
	public static String isValidIdentifierWithErrorMessage(String identifier) {
		if (identifier.length() < 1) {
			return "Length < 1";
		}
		char firstChar = identifier.charAt(0);
		if (firstChar != '_' && !Character.isLetter(firstChar)) {
			return "Identifier has to start with '_' or a character";
		}
		for (int i = 0; i < identifier.length(); i++) {
			Character myChar = identifier.charAt(i);
			if ((!Character.isLetter(myChar) && !Character.isDigit(myChar) && myChar != '_')) {
				return "The char: " + myChar
						+ " is not allowed within identifiers";
			}
		}
		return null;
	}
}
