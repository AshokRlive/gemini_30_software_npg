/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.Palette;

import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.model.libraryElement.SegmentType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Segment Type Palette Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.fordiac.ide.model.Palette.PalettePackage#getSegmentTypePaletteEntry()
 * @model
 * @generated
 */
public interface SegmentTypePaletteEntry extends PaletteEntry {
	
	SegmentType getSegmentType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if((null != type) && (type instanceof SegmentType)){\r\n\tsuper.setType(type);\r\n}else{\r\n\tsuper.setType(null);\r\n\tif(null != type){\r\n\t\t<%org.eclipse.core.runtime.Status%> exception = new Status(<%org.eclipse.core.runtime.IStatus%>.ERROR, Activator.PLUGIN_ID, \"tried to set no SegmentType as type entry for SegmentTypePaletteEntry\");\r\n\t\tActivator.getDefault().getLog().log(exception);\r\n\t}\r\n}'"
	 * @generated
	 */
	void setType(LibraryElement type);

} // SegmentTypePaletteEntry
