/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.Palette.impl;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EClass;
import org.fordiac.ide.model.Activator;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.PalettePackage;
import org.fordiac.ide.model.Palette.ResourceTypeEntry;
import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.model.libraryElement.ResourceType;
import org.fordiac.ide.typeimport.RESImporter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource Type Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ResourceTypeEntryImpl extends PaletteEntryImpl implements ResourceTypeEntry {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceTypeEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PalettePackage.Literals.RESOURCE_TYPE_ENTRY;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceType getResourceType() {
		LibraryElement type = getType();
		if((null !=type) && (type instanceof ResourceType)){
		   return (ResourceType) type;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(final LibraryElement type) {
		if((null != type) && (type instanceof ResourceType)){
			super.setType(type);
		}else{
			super.setType(null);
			if(null != type){
				Status exception = new Status(IStatus.ERROR, Activator.PLUGIN_ID, "tried to set no ResourceType as type entry for ResourceTypeEntry");
				Activator.getDefault().getLog().log(exception);
			}
		}
	}

	protected LibraryElement loadType() {
		Palette palette = getGroup().getPallete();
		return RESImporter.importResType(getFile(), palette);
	}

} //ResourceTypeEntryImpl
