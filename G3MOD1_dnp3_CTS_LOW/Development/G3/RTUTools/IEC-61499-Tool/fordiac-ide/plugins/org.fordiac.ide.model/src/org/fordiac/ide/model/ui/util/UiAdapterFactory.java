/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.fordiac.ide.model.ui.*;
import org.fordiac.ide.model.ui.Color;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.DeviceView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.ResizeUIFBNetwork;
import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.Size;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.UiPackage;
import org.fordiac.ide.model.ui.View;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.fordiac.ide.model.ui.UiPackage
 * @generated
 */
public class UiAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static UiPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UiAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = UiPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UiSwitch<Adapter> modelSwitch =
		new UiSwitch<Adapter>() {
			@Override
			public Adapter caseView(View object) {
				return createViewAdapter();
			}
			@Override
			public Adapter caseDiagram(Diagram object) {
				return createDiagramAdapter();
			}
			@Override
			public Adapter caseUIFBNetwork(UIFBNetwork object) {
				return createUIFBNetworkAdapter();
			}
			@Override
			public Adapter caseFBView(FBView object) {
				return createFBViewAdapter();
			}
			@Override
			public Adapter casePosition(Position object) {
				return createPositionAdapter();
			}
			@Override
			public Adapter caseInterfaceElementView(InterfaceElementView object) {
				return createInterfaceElementViewAdapter();
			}
			@Override
			public Adapter caseConnectionView(ConnectionView object) {
				return createConnectionViewAdapter();
			}
			@Override
			public Adapter caseSubAppView(SubAppView object) {
				return createSubAppViewAdapter();
			}
			@Override
			public Adapter caseSubAppInterfaceElementView(SubAppInterfaceElementView object) {
				return createSubAppInterfaceElementViewAdapter();
			}
			@Override
			public Adapter caseUISubAppNetwork(UISubAppNetwork object) {
				return createUISubAppNetworkAdapter();
			}
			@Override
			public Adapter caseInternalSubAppInterfaceElementView(InternalSubAppInterfaceElementView object) {
				return createInternalSubAppInterfaceElementViewAdapter();
			}
			@Override
			public Adapter caseColor(Color object) {
				return createColorAdapter();
			}
			@Override
			public Adapter caseUISystemConfiguration(UISystemConfiguration object) {
				return createUISystemConfigurationAdapter();
			}
			@Override
			public Adapter caseDeviceView(DeviceView object) {
				return createDeviceViewAdapter();
			}
			@Override
			public Adapter caseResourceView(ResourceView object) {
				return createResourceViewAdapter();
			}
			@Override
			public Adapter caseResourceContainerView(ResourceContainerView object) {
				return createResourceContainerViewAdapter();
			}
			@Override
			public Adapter caseUIResourceEditor(UIResourceEditor object) {
				return createUIResourceEditorAdapter();
			}
			@Override
			public Adapter caseResizeUIFBNetwork(ResizeUIFBNetwork object) {
				return createResizeUIFBNetworkAdapter();
			}
			@Override
			public Adapter caseSize(Size object) {
				return createSizeAdapter();
			}
			@Override
			public Adapter caseMappedSubAppView(MappedSubAppView object) {
				return createMappedSubAppViewAdapter();
			}
			@Override
			public Adapter caseMappedSubAppInterfaceElementView(MappedSubAppInterfaceElementView object) {
				return createMappedSubAppInterfaceElementViewAdapter();
			}
			@Override
			public Adapter caseSegmentView(SegmentView object) {
				return createSegmentViewAdapter();
			}
			@Override
			public Adapter caseLinkView(LinkView object) {
				return createLinkViewAdapter();
			}
			@Override
			public Adapter caseIConnectionView(IConnectionView object) {
				return createIConnectionViewAdapter();
			}
			@Override
			public Adapter caseCompositeInternalInterfaceElementView(CompositeInternalInterfaceElementView object) {
				return createCompositeInternalInterfaceElementViewAdapter();
			}
			@Override
			public Adapter caseMonitoringView(MonitoringView object) {
				return createMonitoringViewAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.View <em>View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.View
	 * @generated
	 */
	public Adapter createViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.Diagram <em>Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.Diagram
	 * @generated
	 */
	public Adapter createDiagramAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.UIFBNetwork <em>UIFB Network</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.UIFBNetwork
	 * @generated
	 */
	public Adapter createUIFBNetworkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.FBView <em>FB View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.FBView
	 * @generated
	 */
	public Adapter createFBViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.Position <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.Position
	 * @generated
	 */
	public Adapter createPositionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.InterfaceElementView <em>Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.InterfaceElementView
	 * @generated
	 */
	public Adapter createInterfaceElementViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.ConnectionView <em>Connection View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.ConnectionView
	 * @generated
	 */
	public Adapter createConnectionViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.SubAppView <em>Sub App View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.SubAppView
	 * @generated
	 */
	public Adapter createSubAppViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.SubAppInterfaceElementView <em>Sub App Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.SubAppInterfaceElementView
	 * @generated
	 */
	public Adapter createSubAppInterfaceElementViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.UISubAppNetwork <em>UI Sub App Network</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.UISubAppNetwork
	 * @generated
	 */
	public Adapter createUISubAppNetworkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView <em>Internal Sub App Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView
	 * @generated
	 */
	public Adapter createInternalSubAppInterfaceElementViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.Color <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.Color
	 * @generated
	 */
	public Adapter createColorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.UISystemConfiguration <em>UI System Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.UISystemConfiguration
	 * @generated
	 */
	public Adapter createUISystemConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.DeviceView <em>Device View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.DeviceView
	 * @generated
	 */
	public Adapter createDeviceViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.ResourceView <em>Resource View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.ResourceView
	 * @generated
	 */
	public Adapter createResourceViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.ResourceContainerView <em>Resource Container View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.ResourceContainerView
	 * @generated
	 */
	public Adapter createResourceContainerViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.UIResourceEditor <em>UI Resource Editor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.UIResourceEditor
	 * @generated
	 */
	public Adapter createUIResourceEditorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.ResizeUIFBNetwork <em>Resize UIFB Network</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.ResizeUIFBNetwork
	 * @generated
	 */
	public Adapter createResizeUIFBNetworkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.Size <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.Size
	 * @generated
	 */
	public Adapter createSizeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.MappedSubAppView <em>Mapped Sub App View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.MappedSubAppView
	 * @generated
	 */
	public Adapter createMappedSubAppViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView <em>Mapped Sub App Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView
	 * @generated
	 */
	public Adapter createMappedSubAppInterfaceElementViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.SegmentView <em>Segment View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.SegmentView
	 * @generated
	 */
	public Adapter createSegmentViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.LinkView <em>Link View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.LinkView
	 * @generated
	 */
	public Adapter createLinkViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.IConnectionView <em>IConnection View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.IConnectionView
	 * @generated
	 */
	public Adapter createIConnectionViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.CompositeInternalInterfaceElementView <em>Composite Internal Interface Element View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.CompositeInternalInterfaceElementView
	 * @generated
	 */
	public Adapter createCompositeInternalInterfaceElementViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.fordiac.ide.model.ui.MonitoringView <em>Monitoring View</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.fordiac.ide.model.ui.MonitoringView
	 * @generated
	 */
	public Adapter createMonitoringViewAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //UiAdapterFactory
