/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import org.eclipse.emf.ecore.EClass;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.ServiceInterfaceFBType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Interface FB Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ServiceInterfaceFBTypeImpl extends FBTypeImpl implements ServiceInterfaceFBType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceInterfaceFBTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibraryElementPackage.Literals.SERVICE_INTERFACE_FB_TYPE;
	}

} //ServiceInterfaceFBTypeImpl
