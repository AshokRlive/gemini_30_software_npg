/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.InterfaceList#getPlugs <em>Plugs</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.InterfaceList#getSockets <em>Sockets</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.InterfaceList#getFB <em>FB</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getInterfaceList()
 * @model
 * @generated
 */
public interface InterfaceList extends SubAppInterfaceList {
	/**
	 * Returns the value of the '<em><b>Plugs</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.AdapterDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Plugs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plugs</em>' reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getInterfaceList_Plugs()
	 * @model resolveProxies="false"
	 *        extendedMetaData="kind='element' name='AdapterDeclaration' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<AdapterDeclaration> getPlugs();

	/**
	 * Returns the value of the '<em><b>Sockets</b></em>' reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.AdapterDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sockets</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sockets</em>' reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getInterfaceList_Sockets()
	 * @model resolveProxies="false"
	 *        extendedMetaData="kind='element' name='AdapterDeclaration' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<AdapterDeclaration> getSockets();
	
	/**
	 * Returns the value of the '<em><b>FB</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.libraryElement.FB#getInterface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>FB</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FB</em>' container reference.
	 * @see #setFB(FB)
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getInterfaceList_FB()
	 * @see org.fordiac.ide.model.libraryElement.FB#getInterface
	 * @model opposite="interface" transient="false"
	 * @generated
	 */
	FB getFB();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.libraryElement.InterfaceList#getFB <em>FB</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FB</em>' container reference.
	 * @see #getFB()
	 * @generated
	 */
	void setFB(FB value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return (INamedElement)eContainer;'"
	 * @generated
	 */
	INamedElement getContainingNamedElement();

	/**
	 * Returns the Adapter with the name
	 * @param adapterName
	 * @return the Adapter with the name or null
	 */
	AdapterDeclaration getAdapter(String name);

} // InterfaceList
