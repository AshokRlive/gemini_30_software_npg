/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FBD</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.FBD#getFB <em>FB</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.FBD#getDataConnections <em>Data Connections</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFBD()
 * @model
 * @generated
 */
public interface FBD extends Algorithm {
	/**
	 * Returns the value of the '<em><b>FB</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.FB}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>FB</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FB</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFBD_FB()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='FB' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<FB> getFB();

	/**
	 * Returns the value of the '<em><b>Data Connections</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.libraryElement.DataConnection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Connections</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Connections</em>' containment reference list.
	 * @see org.fordiac.ide.model.libraryElement.LibraryElementPackage#getFBD_DataConnections()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataConnections' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DataConnection> getDataConnections();

} // FBD
