package org.fordiac.ide.typelibrary;

import org.eclipse.core.resources.IFile;
import org.fordiac.ide.gef.IPaletteEntryCreator;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.Palette.PaletteFactory;
import org.fordiac.ide.model.Palette.impl.PaletteEntryImpl;

public class CreateAdapterTypePaletteEntry implements IPaletteEntryCreator, TypeLibraryTags {
	
	@Override
	public boolean canHandle(IFile file) {
		 if (ADAPTER_TYPE_FILE_ENDING.equalsIgnoreCase(file.getFileExtension())){
			 return true;
		 } else
			 return false;
	}

	@Override
	public PaletteEntryImpl createPaletteEntry() {
		AdapterTypePaletteEntry entry = PaletteFactory.eINSTANCE.createAdapterTypePaletteEntry();
			
		return (PaletteEntryImpl) entry;
	}

}
