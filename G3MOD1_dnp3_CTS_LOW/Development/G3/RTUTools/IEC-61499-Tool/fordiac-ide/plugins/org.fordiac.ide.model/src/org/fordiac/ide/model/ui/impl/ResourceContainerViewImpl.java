/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.fordiac.ide.model.ui.ResourceContainerView;
import org.fordiac.ide.model.ui.ResourceView;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource Container View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ResourceContainerViewImpl#isShowContent <em>Show Content</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.impl.ResourceContainerViewImpl#getResources <em>Resources</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResourceContainerViewImpl extends ViewImpl implements ResourceContainerView {
	/**
	 * The default value of the '{@link #isShowContent() <em>Show Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowContent()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SHOW_CONTENT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isShowContent() <em>Show Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShowContent()
	 * @generated
	 * @ordered
	 */
	protected boolean showContent = SHOW_CONTENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getResources() <em>Resources</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResources()
	 * @generated
	 * @ordered
	 */
	protected EList<ResourceView> resources;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceContainerViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.RESOURCE_CONTAINER_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isShowContent() {
		return showContent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowContent(boolean newShowContent) {
		boolean oldShowContent = showContent;
		showContent = newShowContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.RESOURCE_CONTAINER_VIEW__SHOW_CONTENT, oldShowContent, showContent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ResourceView> getResources() {
		if (resources == null) {
			resources = new EObjectContainmentEList.Resolving<ResourceView>(ResourceView.class, this, UiPackage.RESOURCE_CONTAINER_VIEW__RESOURCES);
		}
		return resources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.RESOURCE_CONTAINER_VIEW__RESOURCES:
				return ((InternalEList<?>)getResources()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.RESOURCE_CONTAINER_VIEW__SHOW_CONTENT:
				return isShowContent();
			case UiPackage.RESOURCE_CONTAINER_VIEW__RESOURCES:
				return getResources();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.RESOURCE_CONTAINER_VIEW__SHOW_CONTENT:
				setShowContent((Boolean)newValue);
				return;
			case UiPackage.RESOURCE_CONTAINER_VIEW__RESOURCES:
				getResources().clear();
				getResources().addAll((Collection<? extends ResourceView>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.RESOURCE_CONTAINER_VIEW__SHOW_CONTENT:
				setShowContent(SHOW_CONTENT_EDEFAULT);
				return;
			case UiPackage.RESOURCE_CONTAINER_VIEW__RESOURCES:
				getResources().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.RESOURCE_CONTAINER_VIEW__SHOW_CONTENT:
				return showContent != SHOW_CONTENT_EDEFAULT;
			case UiPackage.RESOURCE_CONTAINER_VIEW__RESOURCES:
				return resources != null && !resources.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (showContent: ");
		result.append(showContent);
		result.append(')');
		return result.toString();
	}

} //ResourceContainerViewImpl
