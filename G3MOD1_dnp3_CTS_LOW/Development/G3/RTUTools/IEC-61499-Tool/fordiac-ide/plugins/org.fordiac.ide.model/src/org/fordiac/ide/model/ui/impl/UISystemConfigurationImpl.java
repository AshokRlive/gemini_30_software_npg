/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;

import org.fordiac.ide.model.ui.UISystemConfiguration;
import org.fordiac.ide.model.ui.UiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UI System Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.impl.UISystemConfigurationImpl#getSystemConfigNetwork <em>System Config Network</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UISystemConfigurationImpl extends DiagramImpl implements UISystemConfiguration {
	/**
	 * The cached value of the '{@link #getSystemConfigNetwork() <em>System Config Network</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemConfigNetwork()
	 * @generated
	 * @ordered
	 */
	protected SystemConfigurationNetwork systemConfigNetwork;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UISystemConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UiPackage.Literals.UI_SYSTEM_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemConfigurationNetwork getSystemConfigNetwork() {
		if (systemConfigNetwork != null && systemConfigNetwork.eIsProxy()) {
			InternalEObject oldSystemConfigNetwork = (InternalEObject)systemConfigNetwork;
			systemConfigNetwork = (SystemConfigurationNetwork)eResolveProxy(oldSystemConfigNetwork);
			if (systemConfigNetwork != oldSystemConfigNetwork) {
				InternalEObject newSystemConfigNetwork = (InternalEObject)systemConfigNetwork;
				NotificationChain msgs = oldSystemConfigNetwork.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK, null, null);
				if (newSystemConfigNetwork.eInternalContainer() == null) {
					msgs = newSystemConfigNetwork.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UiPackage.UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK, oldSystemConfigNetwork, systemConfigNetwork));
			}
		}
		return systemConfigNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemConfigurationNetwork basicGetSystemConfigNetwork() {
		return systemConfigNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSystemConfigNetwork(SystemConfigurationNetwork newSystemConfigNetwork, NotificationChain msgs) {
		SystemConfigurationNetwork oldSystemConfigNetwork = systemConfigNetwork;
		systemConfigNetwork = newSystemConfigNetwork;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UiPackage.UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK, oldSystemConfigNetwork, newSystemConfigNetwork);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemConfigNetwork(SystemConfigurationNetwork newSystemConfigNetwork) {
		if (newSystemConfigNetwork != systemConfigNetwork) {
			NotificationChain msgs = null;
			if (systemConfigNetwork != null)
				msgs = ((InternalEObject)systemConfigNetwork).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UiPackage.UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK, null, msgs);
			if (newSystemConfigNetwork != null)
				msgs = ((InternalEObject)newSystemConfigNetwork).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UiPackage.UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK, null, msgs);
			msgs = basicSetSystemConfigNetwork(newSystemConfigNetwork, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UiPackage.UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK, newSystemConfigNetwork, newSystemConfigNetwork));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UiPackage.UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK:
				return basicSetSystemConfigNetwork(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UiPackage.UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK:
				if (resolve) return getSystemConfigNetwork();
				return basicGetSystemConfigNetwork();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UiPackage.UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK:
				setSystemConfigNetwork((SystemConfigurationNetwork)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UiPackage.UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK:
				setSystemConfigNetwork((SystemConfigurationNetwork)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UiPackage.UI_SYSTEM_CONFIGURATION__SYSTEM_CONFIG_NETWORK:
				return systemConfigNetwork != null;
		}
		return super.eIsSet(featureID);
	}

	public SubAppNetwork getFunctionBlockNetwork() {
		return null;
	}

	public SubAppNetwork getNetwork() {
		return null;
	}

} //UISystemConfigurationImpl
