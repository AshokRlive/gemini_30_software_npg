/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import org.eclipse.core.resources.IProject;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.fordiac.ide.model.libraryElement.AdapterConnection;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.AdapterEvent;
import org.fordiac.ide.model.libraryElement.AdapterFB;
import org.fordiac.ide.model.libraryElement.AdapterFBType;
import org.fordiac.ide.model.libraryElement.AdapterType;
import org.fordiac.ide.model.libraryElement.Annotation;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.BasicFBType;
import org.fordiac.ide.model.libraryElement.CompilableType;
import org.fordiac.ide.model.libraryElement.CompilerInfo;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.ConfigurableObject;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.DeviceType;
import org.fordiac.ide.model.libraryElement.ECAction;
import org.fordiac.ide.model.libraryElement.ECC;
import org.fordiac.ide.model.libraryElement.ECState;
import org.fordiac.ide.model.libraryElement.ECTransition;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBD;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IDObject;
import org.fordiac.ide.model.libraryElement.Identification;
import org.fordiac.ide.model.libraryElement.InputPrimitive;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LD;
import org.fordiac.ide.model.libraryElement.Language;
import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.Link;
import org.fordiac.ide.model.libraryElement.Mapping;
import org.fordiac.ide.model.libraryElement.OtherAlgorithm;
import org.fordiac.ide.model.libraryElement.OutputPrimitive;
import org.fordiac.ide.model.libraryElement.Parameter;
import org.fordiac.ide.model.libraryElement.Position;
import org.fordiac.ide.model.libraryElement.Primitive;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.ResourceFBNetwork;
import org.fordiac.ide.model.libraryElement.ResourceType;
import org.fordiac.ide.model.libraryElement.ResourceTypeFB;
import org.fordiac.ide.model.libraryElement.ResourceTypeName;
import org.fordiac.ide.model.libraryElement.Rung;
import org.fordiac.ide.model.libraryElement.STAlgorithm;
import org.fordiac.ide.model.libraryElement.Segment;
import org.fordiac.ide.model.libraryElement.SegmentType;
import org.fordiac.ide.model.libraryElement.Service;
import org.fordiac.ide.model.libraryElement.ServiceInterface;
import org.fordiac.ide.model.libraryElement.ServiceInterfaceFBType;
import org.fordiac.ide.model.libraryElement.ServiceSequence;
import org.fordiac.ide.model.libraryElement.ServiceTransaction;
import org.fordiac.ide.model.libraryElement.SubApp;
import org.fordiac.ide.model.libraryElement.SubAppInterfaceList;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.SubAppType;
import org.fordiac.ide.model.libraryElement.SystemConfiguration;
import org.fordiac.ide.model.libraryElement.SystemConfigurationNetwork;
import org.fordiac.ide.model.libraryElement.TypedConfigureableObject;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.VersionInfo;
import org.fordiac.ide.model.libraryElement.With;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class LibraryElementFactoryImpl extends EFactoryImpl implements
		LibraryElementFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static LibraryElementFactory init() {
		try {
			LibraryElementFactory theLibraryElementFactory = (LibraryElementFactory)EPackage.Registry.INSTANCE.getEFactory(LibraryElementPackage.eNS_URI);
			if (theLibraryElementFactory != null) {
				return theLibraryElementFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new LibraryElementFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public LibraryElementFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case LibraryElementPackage.ADAPTER_DECLARATION: return createAdapterDeclaration();
			case LibraryElementPackage.ADAPTER_TYPE: return createAdapterType();
			case LibraryElementPackage.APPLICATION: return createApplication();
			case LibraryElementPackage.BASIC_FB_TYPE: return createBasicFBType();
			case LibraryElementPackage.COMPILER_INFO: return createCompilerInfo();
			case LibraryElementPackage.COMPILER: return createCompiler();
			case LibraryElementPackage.DEVICE: return createDevice();
			case LibraryElementPackage.DEVICE_TYPE: return createDeviceType();
			case LibraryElementPackage.EC_ACTION: return createECAction();
			case LibraryElementPackage.ECC: return createECC();
			case LibraryElementPackage.EC_STATE: return createECState();
			case LibraryElementPackage.EC_TRANSITION: return createECTransition();
			case LibraryElementPackage.EVENT: return createEvent();
			case LibraryElementPackage.FBD: return createFBD();
			case LibraryElementPackage.FB_NETWORK: return createFBNetwork();
			case LibraryElementPackage.FB: return createFB();
			case LibraryElementPackage.FB_TYPE: return createFBType();
			case LibraryElementPackage.IDENTIFICATION: return createIdentification();
			case LibraryElementPackage.INPUT_PRIMITIVE: return createInputPrimitive();
			case LibraryElementPackage.INTERFACE_LIST: return createInterfaceList();
			case LibraryElementPackage.LD: return createLD();
			case LibraryElementPackage.LINK: return createLink();
			case LibraryElementPackage.MAPPING: return createMapping();
			case LibraryElementPackage.OTHER_ALGORITHM: return createOtherAlgorithm();
			case LibraryElementPackage.OUTPUT_PRIMITIVE: return createOutputPrimitive();
			case LibraryElementPackage.PARAMETER: return createParameter();
			case LibraryElementPackage.RESOURCE: return createResource();
			case LibraryElementPackage.RESOURCE_TYPE_NAME: return createResourceTypeName();
			case LibraryElementPackage.RESOURCE_TYPE: return createResourceType();
			case LibraryElementPackage.RUNG: return createRung();
			case LibraryElementPackage.SEGMENT: return createSegment();
			case LibraryElementPackage.SERVICE_SEQUENCE: return createServiceSequence();
			case LibraryElementPackage.SERVICE_TRANSACTION: return createServiceTransaction();
			case LibraryElementPackage.SERVICE_INTERFACE_FB_TYPE: return createServiceInterfaceFBType();
			case LibraryElementPackage.ST_ALGORITHM: return createSTAlgorithm();
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST: return createSubAppInterfaceList();
			case LibraryElementPackage.SUB_APP_NETWORK: return createSubAppNetwork();
			case LibraryElementPackage.SUB_APP: return createSubApp();
			case LibraryElementPackage.SUB_APP_TYPE: return createSubAppType();
			case LibraryElementPackage.AUTOMATION_SYSTEM: return createAutomationSystem();
			case LibraryElementPackage.VAR_DECLARATION: return createVarDeclaration();
			case LibraryElementPackage.VERSION_INFO: return createVersionInfo();
			case LibraryElementPackage.WITH: return createWith();
			case LibraryElementPackage.LIBRARY_ELEMENT: return createLibraryElement();
			case LibraryElementPackage.COMPILABLE_TYPE: return createCompilableType();
			case LibraryElementPackage.CONFIGURABLE_OBJECT: return createConfigurableObject();
			case LibraryElementPackage.COMPOSITE_FB_TYPE: return createCompositeFBType();
			case LibraryElementPackage.POSITION: return createPosition();
			case LibraryElementPackage.DATA_CONNECTION: return createDataConnection();
			case LibraryElementPackage.EVENT_CONNECTION: return createEventConnection();
			case LibraryElementPackage.ADAPTER_CONNECTION: return createAdapterConnection();
			case LibraryElementPackage.SERVICE_INTERFACE: return createServiceInterface();
			case LibraryElementPackage.VALUE: return createValue();
			case LibraryElementPackage.SYSTEM_CONFIGURATION_NETWORK: return createSystemConfigurationNetwork();
			case LibraryElementPackage.SYSTEM_CONFIGURATION: return createSystemConfiguration();
			case LibraryElementPackage.ID_OBJECT: return createIDObject();
			case LibraryElementPackage.RESOURCE_FB_NETWORK: return createResourceFBNetwork();
			case LibraryElementPackage.RESOURCE_TYPE_FB: return createResourceTypeFB();
			case LibraryElementPackage.SEGMENT_TYPE: return createSegmentType();
			case LibraryElementPackage.ADAPTER_FB_TYPE: return createAdapterFBType();
			case LibraryElementPackage.ANNOTATION: return createAnnotation();
			case LibraryElementPackage.ADAPTER_EVENT: return createAdapterEvent();
			case LibraryElementPackage.SERVICE: return createService();
			case LibraryElementPackage.TYPED_CONFIGUREABLE_OBJECT: return createTypedConfigureableObject();
			case LibraryElementPackage.ADAPTER_FB: return createAdapterFB();
			case LibraryElementPackage.PRIMITIVE: return createPrimitive();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case LibraryElementPackage.LANGUAGE:
				return createLanguageFromString(eDataType, initialValue);
			case LibraryElementPackage.IPROJECT:
				return createIProjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case LibraryElementPackage.LANGUAGE:
				return convertLanguageToString(eDataType, instanceValue);
			case LibraryElementPackage.IPROJECT:
				return convertIProjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AdapterDeclaration createAdapterDeclaration() {
		AdapterDeclarationImpl adapterDeclaration = new AdapterDeclarationImpl();
		return adapterDeclaration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AdapterType createAdapterType() {
		AdapterTypeImpl adapterType = new AdapterTypeImpl();
		return adapterType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Application createApplication() {
		ApplicationImpl application = new ApplicationImpl();
		return application;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public BasicFBType createBasicFBType() {
		BasicFBTypeImpl basicFBType = new BasicFBTypeImpl();
		return basicFBType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CompilerInfo createCompilerInfo() {
		CompilerInfoImpl compilerInfo = new CompilerInfoImpl();
		return compilerInfo;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public org.fordiac.ide.model.libraryElement.Compiler createCompiler() {
		CompilerImpl compiler = new CompilerImpl();
		return compiler;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Device createDevice() {
		DeviceImpl device = new DeviceImpl();
		return device;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public DeviceType createDeviceType() {
		DeviceTypeImpl deviceType = new DeviceTypeImpl();
		return deviceType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ECAction createECAction() {
		ECActionImpl ecAction = new ECActionImpl();
		return ecAction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ECC createECC() {
		ECCImpl ecc = new ECCImpl();
		return ecc;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ECState createECState() {
		ECStateImpl ecState = new ECStateImpl();
		return ecState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ECTransition createECTransition() {
		ECTransitionImpl ecTransition = new ECTransitionImpl();
		return ecTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Event createEvent() {
		EventImpl event = new EventImpl();
		return event;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public FBD createFBD() {
		FBDImpl fbd = new FBDImpl();
		return fbd;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public FBNetwork createFBNetwork() {
		FBNetworkImpl fbNetwork = new FBNetworkImpl();
		return fbNetwork;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public FB createFB() {
		FBImpl fb = new FBImpl();
		return fb;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public FBType createFBType() {
		FBTypeImpl fbType = new FBTypeImpl();
		return fbType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Identification createIdentification() {
		IdentificationImpl identification = new IdentificationImpl();
		return identification;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public InputPrimitive createInputPrimitive() {
		InputPrimitiveImpl inputPrimitive = new InputPrimitiveImpl();
		return inputPrimitive;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceList createInterfaceList() {
		InterfaceListImpl interfaceList = new InterfaceListImpl();
		return interfaceList;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LD createLD() {
		LDImpl ld = new LDImpl();
		return ld;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Link createLink() {
		LinkImpl link = new LinkImpl();
		return link;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Mapping createMapping() {
		MappingImpl mapping = new MappingImpl();
		return mapping;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public OtherAlgorithm createOtherAlgorithm() {
		OtherAlgorithmImpl otherAlgorithm = new OtherAlgorithmImpl();
		return otherAlgorithm;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public OutputPrimitive createOutputPrimitive() {
		OutputPrimitiveImpl outputPrimitive = new OutputPrimitiveImpl();
		return outputPrimitive;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter createParameter() {
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Resource createResource() {
		ResourceImpl resource = new ResourceImpl();
		return resource;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceTypeName createResourceTypeName() {
		ResourceTypeNameImpl resourceTypeName = new ResourceTypeNameImpl();
		return resourceTypeName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceType createResourceType() {
		ResourceTypeImpl resourceType = new ResourceTypeImpl();
		return resourceType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Rung createRung() {
		RungImpl rung = new RungImpl();
		return rung;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Segment createSegment() {
		SegmentImpl segment = new SegmentImpl();
		return segment;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceSequence createServiceSequence() {
		ServiceSequenceImpl serviceSequence = new ServiceSequenceImpl();
		return serviceSequence;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceTransaction createServiceTransaction() {
		ServiceTransactionImpl serviceTransaction = new ServiceTransactionImpl();
		return serviceTransaction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceInterfaceFBType createServiceInterfaceFBType() {
		ServiceInterfaceFBTypeImpl serviceInterfaceFBType = new ServiceInterfaceFBTypeImpl();
		return serviceInterfaceFBType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public STAlgorithm createSTAlgorithm() {
		STAlgorithmImpl stAlgorithm = new STAlgorithmImpl();
		return stAlgorithm;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppInterfaceList createSubAppInterfaceList() {
		SubAppInterfaceListImpl subAppInterfaceList = new SubAppInterfaceListImpl();
		return subAppInterfaceList;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppNetwork createSubAppNetwork() {
		SubAppNetworkImpl subAppNetwork = new SubAppNetworkImpl();
		return subAppNetwork;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SubApp createSubApp() {
		SubAppImpl subApp = new SubAppImpl();
		return subApp;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SubAppType createSubAppType() {
		SubAppTypeImpl subAppType = new SubAppTypeImpl();
		return subAppType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AutomationSystem createAutomationSystem() {
		AutomationSystemImpl automationSystem = new AutomationSystemImpl();
		return automationSystem;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public VarDeclaration createVarDeclaration() {
		VarDeclarationImpl varDeclaration = new VarDeclarationImpl();
		return varDeclaration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public VersionInfo createVersionInfo() {
		VersionInfoImpl versionInfo = new VersionInfoImpl();
		return versionInfo;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public With createWith() {
		WithImpl with = new WithImpl();
		return with;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LibraryElement createLibraryElement() {
		LibraryElementImpl libraryElement = new LibraryElementImpl();
		return libraryElement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CompilableType createCompilableType() {
		CompilableTypeImpl compilableType = new CompilableTypeImpl();
		return compilableType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurableObject createConfigurableObject() {
		ConfigurableObjectImpl configurableObject = new ConfigurableObjectImpl();
		return configurableObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeFBType createCompositeFBType() {
		CompositeFBTypeImpl compositeFBType = new CompositeFBTypeImpl();
		return compositeFBType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Position createPosition() {
		PositionImpl position = new PositionImpl();
		return position;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public DataConnection createDataConnection() {
		DataConnectionImpl dataConnection = new DataConnectionImpl();
		return dataConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EventConnection createEventConnection() {
		EventConnectionImpl eventConnection = new EventConnectionImpl();
		return eventConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public AdapterConnection createAdapterConnection() {
		AdapterConnectionImpl adapterConnection = new AdapterConnectionImpl();
		return adapterConnection;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceInterface createServiceInterface() {
		ServiceInterfaceImpl serviceInterface = new ServiceInterfaceImpl();
		return serviceInterface;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Value createValue() {
		ValueImpl value = new ValueImpl();
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SystemConfigurationNetwork createSystemConfigurationNetwork() {
		SystemConfigurationNetworkImpl systemConfigurationNetwork = new SystemConfigurationNetworkImpl();
		return systemConfigurationNetwork;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SystemConfiguration createSystemConfiguration() {
		SystemConfigurationImpl systemConfiguration = new SystemConfigurationImpl();
		return systemConfiguration;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public IDObject createIDObject() {
		IDObjectImpl idObject = new IDObjectImpl();
		return idObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceFBNetwork createResourceFBNetwork() {
		ResourceFBNetworkImpl resourceFBNetwork = new ResourceFBNetworkImpl();
		return resourceFBNetwork;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceTypeFB createResourceTypeFB() {
		ResourceTypeFBImpl resourceTypeFB = new ResourceTypeFBImpl();
		return resourceTypeFB;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public SegmentType createSegmentType() {
		SegmentTypeImpl segmentType = new SegmentTypeImpl();
		return segmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdapterFBType createAdapterFBType() {
		AdapterFBTypeImpl adapterFBType = new AdapterFBTypeImpl();
		return adapterFBType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Annotation createAnnotation() {
		AnnotationImpl annotation = new AnnotationImpl();
		return annotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdapterEvent createAdapterEvent() {
		AdapterEventImpl adapterEvent = new AdapterEventImpl();
		return adapterEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Service createService() {
		ServiceImpl service = new ServiceImpl();
		return service;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypedConfigureableObject createTypedConfigureableObject() {
		TypedConfigureableObjectImpl typedConfigureableObject = new TypedConfigureableObjectImpl();
		return typedConfigureableObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdapterFB createAdapterFB() {
		AdapterFBImpl adapterFB = new AdapterFBImpl();
		return adapterFB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Primitive createPrimitive() {
		PrimitiveImpl primitive = new PrimitiveImpl();
		return primitive;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Language createLanguageFromString(EDataType eDataType,
			String initialValue) {
		Language result = Language.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLanguageToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IProject createIProjectFromString(EDataType eDataType, String initialValue) {
		return (IProject)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIProjectToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LibraryElementPackage getLibraryElementPackage() {
		return (LibraryElementPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static LibraryElementPackage getPackage() {
		return LibraryElementPackage.eINSTANCE;
	}

} // LibraryElementFactoryImpl
