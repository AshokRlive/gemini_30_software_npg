/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.Palette;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Favorite Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.fordiac.ide.model.Palette.FavoriteEntry#getEntry <em>Entry</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.fordiac.ide.model.Palette.PalettePackage#getFavoriteEntry()
 * @model
 * @generated
 */
public interface FavoriteEntry extends PaletteEntry {
	/**
	 * Returns the value of the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry</em>' containment reference.
	 * @see #setEntry(PaletteEntry)
	 * @see org.fordiac.ide.model.Palette.PalettePackage#getFavoriteEntry_Entry()
	 * @model containment="true" resolveProxies="true" required="true"
	 * @generated
	 */
	PaletteEntry getEntry();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.Palette.FavoriteEntry#getEntry <em>Entry</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry</em>' containment reference.
	 * @see #getEntry()
	 * @generated
	 */
	void setEntry(PaletteEntry value);

} // FavoriteEntry
