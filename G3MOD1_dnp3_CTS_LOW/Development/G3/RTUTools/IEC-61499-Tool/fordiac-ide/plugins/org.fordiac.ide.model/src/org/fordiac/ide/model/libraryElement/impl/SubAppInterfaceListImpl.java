/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.libraryElement.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;
import org.fordiac.ide.model.libraryElement.SubAppInterfaceList;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub App Interface List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SubAppInterfaceListImpl#getEventInputs <em>Event Inputs</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SubAppInterfaceListImpl#getEventOutputs <em>Event Outputs</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SubAppInterfaceListImpl#getInputVars <em>Input Vars</em>}</li>
 *   <li>{@link org.fordiac.ide.model.libraryElement.impl.SubAppInterfaceListImpl#getOutputVars <em>Output Vars</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubAppInterfaceListImpl extends EObjectImpl implements SubAppInterfaceList {
	/**
	 * The cached value of the '{@link #getEventInputs() <em>Event Inputs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventInputs()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> eventInputs;

	/**
	 * The cached value of the '{@link #getEventOutputs() <em>Event Outputs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventOutputs()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> eventOutputs;

	/**
	 * The cached value of the '{@link #getInputVars() <em>Input Vars</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputVars()
	 * @generated
	 * @ordered
	 */
	protected EList<VarDeclaration> inputVars;

	/**
	 * The cached value of the '{@link #getOutputVars() <em>Output Vars</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputVars()
	 * @generated
	 * @ordered
	 */
	protected EList<VarDeclaration> outputVars;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubAppInterfaceListImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibraryElementPackage.Literals.SUB_APP_INTERFACE_LIST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEventInputs() {
		if (eventInputs == null) {
			eventInputs = new EObjectContainmentEList<Event>(Event.class, this, LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_INPUTS);
		}
		return eventInputs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEventOutputs() {
		if (eventOutputs == null) {
			eventOutputs = new EObjectContainmentEList<Event>(Event.class, this, LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_OUTPUTS);
		}
		return eventOutputs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VarDeclaration> getInputVars() {
		if (inputVars == null) {
			inputVars = new EObjectContainmentEList<VarDeclaration>(VarDeclaration.class, this, LibraryElementPackage.SUB_APP_INTERFACE_LIST__INPUT_VARS);
		}
		return inputVars;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VarDeclaration> getOutputVars() {
		if (outputVars == null) {
			outputVars = new EObjectContainmentEList<VarDeclaration>(VarDeclaration.class, this, LibraryElementPackage.SUB_APP_INTERFACE_LIST__OUTPUT_VARS);
		}
		return outputVars;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_INPUTS:
				return ((InternalEList<?>)getEventInputs()).basicRemove(otherEnd, msgs);
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_OUTPUTS:
				return ((InternalEList<?>)getEventOutputs()).basicRemove(otherEnd, msgs);
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__INPUT_VARS:
				return ((InternalEList<?>)getInputVars()).basicRemove(otherEnd, msgs);
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__OUTPUT_VARS:
				return ((InternalEList<?>)getOutputVars()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_INPUTS:
				return getEventInputs();
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_OUTPUTS:
				return getEventOutputs();
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__INPUT_VARS:
				return getInputVars();
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__OUTPUT_VARS:
				return getOutputVars();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_INPUTS:
				getEventInputs().clear();
				getEventInputs().addAll((Collection<? extends Event>)newValue);
				return;
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_OUTPUTS:
				getEventOutputs().clear();
				getEventOutputs().addAll((Collection<? extends Event>)newValue);
				return;
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__INPUT_VARS:
				getInputVars().clear();
				getInputVars().addAll((Collection<? extends VarDeclaration>)newValue);
				return;
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__OUTPUT_VARS:
				getOutputVars().clear();
				getOutputVars().addAll((Collection<? extends VarDeclaration>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_INPUTS:
				getEventInputs().clear();
				return;
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_OUTPUTS:
				getEventOutputs().clear();
				return;
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__INPUT_VARS:
				getInputVars().clear();
				return;
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__OUTPUT_VARS:
				getOutputVars().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_INPUTS:
				return eventInputs != null && !eventInputs.isEmpty();
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__EVENT_OUTPUTS:
				return eventOutputs != null && !eventOutputs.isEmpty();
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__INPUT_VARS:
				return inputVars != null && !inputVars.isEmpty();
			case LibraryElementPackage.SUB_APP_INTERFACE_LIST__OUTPUT_VARS:
				return outputVars != null && !outputVars.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	public VarDeclaration getVariable(String name) {
		for (VarDeclaration var : getInputVars()) {
			if (var.getName().equals(name))
				return var;
		}
		for (VarDeclaration var : getOutputVars()) {
			if (var.getName().equals(name))
				return var;
		}
		return null;
	}

	public Event getEvent(String name) {
		for (Event event : getEventInputs()) {
			if (event.getName().equals(name))
				return event;
		}
		for (Event event : getEventOutputs()) {
			if (event.getName().equals(name))
				return event;
		}
		return null;
	}
	
} //SubAppInterfaceListImpl
