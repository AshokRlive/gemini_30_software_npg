/*******************************************************************************
 * Copyright (c) 2016 fortiss GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Alois Zoitl - initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.fordiac.ide.model.structuredtext.ui;

import org.fordiac.ide.model.structuredtext.ui.internal.StructuredTextActivator;
import org.osgi.framework.BundleContext;

import com.google.inject.Module;


/**An extended activator which extends the standard generated xtext activator for 
 * expressions as sublanguage
 */
public class ExtendedStructuredTextActivator extends StructuredTextActivator {
	
	public static final String ORG_FORDIAC_IDE_MODEL_STRUCTUREDTEXT_EXPRESSION = "org.fordiac.ide.model.structuredtext.Expression";
	
	private static StructuredTextActivator INSTANCE;
	
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		INSTANCE = this;
	}
	
	@Override
	public void stop(BundleContext context) throws Exception {
		INSTANCE = null;
		super.stop(context);
	}
	
	public static StructuredTextActivator getInstance() {
		return INSTANCE;
	}

	@Override
	protected Module getRuntimeModule(String grammar) {
		if (ORG_FORDIAC_IDE_MODEL_STRUCTUREDTEXT_EXPRESSION.equals(grammar)) {
			return new org.fordiac.ide.model.structuredtext.ExpressionRuntimeModule();
		}
		return super.getRuntimeModule(grammar);
	}

	@Override
	protected Module getUiModule(String grammar) {
		if (ORG_FORDIAC_IDE_MODEL_STRUCTUREDTEXT_EXPRESSION.equals(grammar)) {
			return new org.fordiac.ide.model.structuredtext.ui.ExpressionUiModule(this);
		}
		return super.getUiModule(grammar);
	}
	
	

}
