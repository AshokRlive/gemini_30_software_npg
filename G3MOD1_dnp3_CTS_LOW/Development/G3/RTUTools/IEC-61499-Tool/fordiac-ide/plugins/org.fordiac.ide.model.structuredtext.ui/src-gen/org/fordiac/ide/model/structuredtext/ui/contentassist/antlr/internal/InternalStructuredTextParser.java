package org.fordiac.ide.model.structuredtext.ui.contentassist.antlr.internal; 

import java.util.Map;
import java.util.HashMap;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.fordiac.ide.model.structuredtext.services.StructuredTextGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalStructuredTextParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "LDATE_AND_TIME", "DATE_AND_TIME", "LTIME_OF_DAY", "TIME_OF_DAY", "END_REPEAT", "END_WHILE", "CONSTANT", "CONTINUE", "END_CASE", "END_FOR", "END_VAR", "WSTRING", "END_IF", "REPEAT", "RETURN", "STRING", "DWORD", "ELSIF", "FALSE", "LDATE", "LREAL", "LTIME", "LWORD", "SUPER", "UDINT", "ULINT", "UNTIL", "USINT", "WCHAR", "WHILE", "BOOL", "BYTE", "CASE", "CHAR", "DATE", "DINT", "ELSE", "EXIT", "LINT", "LTOD", "REAL", "SINT", "THEN", "TIME", "TRUE", "UINT", "WORD", "AND", "FOR", "INT", "LDT", "MOD", "NOT", "TOD", "VAR", "XOR", "AsteriskAsterisk", "ColonEqualsSign", "LessThanSignEqualsSign", "LessThanSignGreaterThanSign", "EqualsSignGreaterThanSign", "GreaterThanSignEqualsSign", "BY", "DO", "DT", "IF", "LD", "LT", "OF", "OR", "TO", "Ms", "Ns", "Us", "NumberSign", "Ampersand", "LeftParenthesis", "RightParenthesis", "Asterisk", "PlusSign", "Comma", "HyphenMinus", "FullStop", "Solidus", "Colon", "Semicolon", "LessThanSign", "EqualsSign", "GreaterThanSign", "E", "T", "LeftSquareBracket", "RightSquareBracket", "KW__", "D_1", "H", "M", "S", "RULE_LETTER", "RULE_DIGIT", "RULE_BIT", "RULE_OCTAL_DIGIT", "RULE_HEX_DIGIT", "RULE_ID", "RULE_BINARY_INT", "RULE_OCTAL_INT", "RULE_HEX_INT", "RULE_UNSIGNED_INT", "RULE_S_BYTE_CHAR_VALUE", "RULE_S_BYTE_CHAR_STR", "RULE_D_BYTE_CHAR_VALUE", "RULE_D_BYTE_CHAR_STR", "RULE_COMMON_CHAR_VALUE", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER"
    };
    public static final int LWORD=26;
    public static final int LessThanSignGreaterThanSign=63;
    public static final int EqualsSignGreaterThanSign=64;
    public static final int VAR=58;
    public static final int END_IF=16;
    public static final int ULINT=29;
    public static final int END_CASE=12;
    public static final int LessThanSign=90;
    public static final int RULE_BIT=104;
    public static final int LeftParenthesis=80;
    public static final int BYTE=35;
    public static final int ELSE=40;
    public static final int IF=69;
    public static final int LINT=42;
    public static final int GreaterThanSign=92;
    public static final int WORD=50;
    public static final int DATE_AND_TIME=5;
    public static final int RULE_ID=107;
    public static final int TOD=57;
    public static final int RULE_DIGIT=103;
    public static final int DINT=39;
    public static final int UDINT=28;
    public static final int CASE=36;
    public static final int GreaterThanSignEqualsSign=65;
    public static final int RULE_OCTAL_INT=109;
    public static final int PlusSign=83;
    public static final int END_FOR=13;
    public static final int RULE_ML_COMMENT=117;
    public static final int THEN=46;
    public static final int XOR=59;
    public static final int LeftSquareBracket=95;
    public static final int EXIT=41;
    public static final int TIME_OF_DAY=7;
    public static final int LDATE_AND_TIME=4;
    public static final int REPEAT=17;
    public static final int E=93;
    public static final int H=99;
    public static final int CHAR=37;
    public static final int RULE_D_BYTE_CHAR_STR=115;
    public static final int RULE_UNSIGNED_INT=111;
    public static final int M=100;
    public static final int LTIME=25;
    public static final int Comma=84;
    public static final int HyphenMinus=85;
    public static final int S=101;
    public static final int T=94;
    public static final int BY=66;
    public static final int ELSIF=21;
    public static final int END_REPEAT=8;
    public static final int LessThanSignEqualsSign=62;
    public static final int Solidus=87;
    public static final int RULE_OCTAL_DIGIT=105;
    public static final int RULE_HEX_INT=110;
    public static final int FullStop=86;
    public static final int CONSTANT=10;
    public static final int KW__=97;
    public static final int CONTINUE=11;
    public static final int Semicolon=89;
    public static final int RULE_LETTER=102;
    public static final int LD=70;
    public static final int STRING=19;
    public static final int RULE_HEX_DIGIT=106;
    public static final int TO=74;
    public static final int UINT=49;
    public static final int LTOD=43;
    public static final int LT=71;
    public static final int DO=67;
    public static final int WSTRING=15;
    public static final int DT=68;
    public static final int END_VAR=14;
    public static final int Ampersand=79;
    public static final int RightSquareBracket=96;
    public static final int RULE_BINARY_INT=108;
    public static final int USINT=31;
    public static final int DWORD=20;
    public static final int FOR=52;
    public static final int RightParenthesis=81;
    public static final int TRUE=48;
    public static final int ColonEqualsSign=61;
    public static final int END_WHILE=9;
    public static final int DATE=38;
    public static final int NOT=56;
    public static final int LDATE=23;
    public static final int AND=51;
    public static final int NumberSign=78;
    public static final int REAL=44;
    public static final int AsteriskAsterisk=60;
    public static final int SINT=45;
    public static final int LTIME_OF_DAY=6;
    public static final int Us=77;
    public static final int LREAL=24;
    public static final int WCHAR=32;
    public static final int Ms=75;
    public static final int INT=53;
    public static final int RULE_SL_COMMENT=118;
    public static final int RETURN=18;
    public static final int EqualsSign=91;
    public static final int RULE_COMMON_CHAR_VALUE=116;
    public static final int OF=72;
    public static final int Colon=88;
    public static final int EOF=-1;
    public static final int LDT=54;
    public static final int Asterisk=82;
    public static final int SUPER=27;
    public static final int MOD=55;
    public static final int OR=73;
    public static final int RULE_S_BYTE_CHAR_VALUE=112;
    public static final int Ns=76;
    public static final int RULE_WS=119;
    public static final int TIME=47;
    public static final int RULE_ANY_OTHER=120;
    public static final int UNTIL=30;
    public static final int RULE_S_BYTE_CHAR_STR=113;
    public static final int BOOL=34;
    public static final int D_1=98;
    public static final int FALSE=22;
    public static final int WHILE=33;
    public static final int RULE_D_BYTE_CHAR_VALUE=114;

    // delegates
    // delegators


        public InternalStructuredTextParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalStructuredTextParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalStructuredTextParser.tokenNames; }
    public String getGrammarFileName() { return "InternalStructuredTextParser.g"; }


     
     	private StructuredTextGrammarAccess grammarAccess;
     	
     	private final Map<String, String> tokenNameToValue = new HashMap<String, String>();
     	
     	{
    		tokenNameToValue.put("NumberSign", "'#'");
    		tokenNameToValue.put("Ampersand", "'&'");
    		tokenNameToValue.put("LeftParenthesis", "'('");
    		tokenNameToValue.put("RightParenthesis", "')'");
    		tokenNameToValue.put("Asterisk", "'*'");
    		tokenNameToValue.put("PlusSign", "'+'");
    		tokenNameToValue.put("Comma", "','");
    		tokenNameToValue.put("HyphenMinus", "'-'");
    		tokenNameToValue.put("FullStop", "'.'");
    		tokenNameToValue.put("Solidus", "'/'");
    		tokenNameToValue.put("Colon", "':'");
    		tokenNameToValue.put("Semicolon", "';'");
    		tokenNameToValue.put("LessThanSign", "'<'");
    		tokenNameToValue.put("EqualsSign", "'='");
    		tokenNameToValue.put("GreaterThanSign", "'>'");
    		tokenNameToValue.put("D_1", "'D'");
    		tokenNameToValue.put("E", "'E'");
    		tokenNameToValue.put("T", "'T'");
    		tokenNameToValue.put("LeftSquareBracket", "'['");
    		tokenNameToValue.put("RightSquareBracket", "']'");
    		tokenNameToValue.put("KW__", "'_'");
    		tokenNameToValue.put("D_1", "'d'");
    		tokenNameToValue.put("H", "'h'");
    		tokenNameToValue.put("M", "'m'");
    		tokenNameToValue.put("S", "'s'");
    		tokenNameToValue.put("AsteriskAsterisk", "'**'");
    		tokenNameToValue.put("ColonEqualsSign", "':='");
    		tokenNameToValue.put("LessThanSignEqualsSign", "'<='");
    		tokenNameToValue.put("LessThanSignGreaterThanSign", "'<>'");
    		tokenNameToValue.put("EqualsSignGreaterThanSign", "'=>'");
    		tokenNameToValue.put("GreaterThanSignEqualsSign", "'>='");
    		tokenNameToValue.put("BY", "'BY'");
    		tokenNameToValue.put("DO", "'DO'");
    		tokenNameToValue.put("DT", "'DT'");
    		tokenNameToValue.put("IF", "'IF'");
    		tokenNameToValue.put("LD", "'LD'");
    		tokenNameToValue.put("LT", "'LT'");
    		tokenNameToValue.put("OF", "'OF'");
    		tokenNameToValue.put("OR", "'OR'");
    		tokenNameToValue.put("TO", "'TO'");
    		tokenNameToValue.put("Ms", "'ms'");
    		tokenNameToValue.put("Ns", "'ns'");
    		tokenNameToValue.put("Us", "'us'");
    		tokenNameToValue.put("AND", "'AND'");
    		tokenNameToValue.put("FOR", "'FOR'");
    		tokenNameToValue.put("INT", "'INT'");
    		tokenNameToValue.put("LDT", "'LDT'");
    		tokenNameToValue.put("MOD", "'MOD'");
    		tokenNameToValue.put("NOT", "'NOT'");
    		tokenNameToValue.put("TOD", "'TOD'");
    		tokenNameToValue.put("VAR", "'VAR'");
    		tokenNameToValue.put("XOR", "'XOR'");
    		tokenNameToValue.put("BOOL", "'BOOL'");
    		tokenNameToValue.put("CASE", "'CASE'");
    		tokenNameToValue.put("CHAR", "'CHAR'");
    		tokenNameToValue.put("DATE", "'DATE'");
    		tokenNameToValue.put("DINT", "'DINT'");
    		tokenNameToValue.put("ELSE", "'ELSE'");
    		tokenNameToValue.put("EXIT", "'EXIT'");
    		tokenNameToValue.put("LINT", "'LINT'");
    		tokenNameToValue.put("LTOD", "'LTOD'");
    		tokenNameToValue.put("REAL", "'REAL'");
    		tokenNameToValue.put("SINT", "'SINT'");
    		tokenNameToValue.put("THEN", "'THEN'");
    		tokenNameToValue.put("TIME", "'TIME'");
    		tokenNameToValue.put("TRUE", "'TRUE'");
    		tokenNameToValue.put("UINT", "'UINT'");
    		tokenNameToValue.put("ELSIF", "'ELSIF'");
    		tokenNameToValue.put("FALSE", "'FALSE'");
    		tokenNameToValue.put("LDATE", "'LDATE'");
    		tokenNameToValue.put("LREAL", "'LREAL'");
    		tokenNameToValue.put("LTIME", "'LTIME'");
    		tokenNameToValue.put("SUPER", "'SUPER'");
    		tokenNameToValue.put("UDINT", "'UDINT'");
    		tokenNameToValue.put("ULINT", "'ULINT'");
    		tokenNameToValue.put("UNTIL", "'UNTIL'");
    		tokenNameToValue.put("USINT", "'USINT'");
    		tokenNameToValue.put("WCHAR", "'WCHAR'");
    		tokenNameToValue.put("WHILE", "'WHILE'");
    		tokenNameToValue.put("END_IF", "'END_IF'");
    		tokenNameToValue.put("REPEAT", "'REPEAT'");
    		tokenNameToValue.put("RETURN", "'RETURN'");
    		tokenNameToValue.put("STRING", "'STRING'");
    		tokenNameToValue.put("END_FOR", "'END_FOR'");
    		tokenNameToValue.put("END_VAR", "'END_VAR'");
    		tokenNameToValue.put("WSTRING", "'WSTRING'");
    		tokenNameToValue.put("CONSTANT", "'CONSTANT'");
    		tokenNameToValue.put("CONTINUE", "'CONTINUE'");
    		tokenNameToValue.put("END_CASE", "'END_CASE'");
    		tokenNameToValue.put("END_WHILE", "'END_WHILE'");
    		tokenNameToValue.put("END_REPEAT", "'END_REPEAT'");
    		tokenNameToValue.put("TIME_OF_DAY", "'TIME_OF_DAY'");
    		tokenNameToValue.put("LTIME_OF_DAY", "'LTIME_OF_DAY'");
    		tokenNameToValue.put("DATE_AND_TIME", "'DATE_AND_TIME'");
    		tokenNameToValue.put("LDATE_AND_TIME", "'LDATE_AND_TIME'");
     	}
     	
        public void setGrammarAccess(StructuredTextGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }

    	@Override
        protected String getValueForTokenName(String tokenName) {
        	String result = tokenNameToValue.get(tokenName);
        	if (result == null)
        		result = tokenName;
        	return result;
        }



    // $ANTLR start "entryRuleStructuredTextAlgorithm"
    // InternalStructuredTextParser.g:158:1: entryRuleStructuredTextAlgorithm : ruleStructuredTextAlgorithm EOF ;
    public final void entryRuleStructuredTextAlgorithm() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:159:1: ( ruleStructuredTextAlgorithm EOF )
            // InternalStructuredTextParser.g:160:1: ruleStructuredTextAlgorithm EOF
            {
             before(grammarAccess.getStructuredTextAlgorithmRule()); 
            pushFollow(FOLLOW_1);
            ruleStructuredTextAlgorithm();

            state._fsp--;

             after(grammarAccess.getStructuredTextAlgorithmRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStructuredTextAlgorithm"


    // $ANTLR start "ruleStructuredTextAlgorithm"
    // InternalStructuredTextParser.g:167:1: ruleStructuredTextAlgorithm : ( ( rule__StructuredTextAlgorithm__Group__0 ) ) ;
    public final void ruleStructuredTextAlgorithm() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:171:5: ( ( ( rule__StructuredTextAlgorithm__Group__0 ) ) )
            // InternalStructuredTextParser.g:172:1: ( ( rule__StructuredTextAlgorithm__Group__0 ) )
            {
            // InternalStructuredTextParser.g:172:1: ( ( rule__StructuredTextAlgorithm__Group__0 ) )
            // InternalStructuredTextParser.g:173:1: ( rule__StructuredTextAlgorithm__Group__0 )
            {
             before(grammarAccess.getStructuredTextAlgorithmAccess().getGroup()); 
            // InternalStructuredTextParser.g:174:1: ( rule__StructuredTextAlgorithm__Group__0 )
            // InternalStructuredTextParser.g:174:2: rule__StructuredTextAlgorithm__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StructuredTextAlgorithm__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStructuredTextAlgorithmAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStructuredTextAlgorithm"


    // $ANTLR start "entryRuleVar_Decl_Init"
    // InternalStructuredTextParser.g:186:1: entryRuleVar_Decl_Init : ruleVar_Decl_Init EOF ;
    public final void entryRuleVar_Decl_Init() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:187:1: ( ruleVar_Decl_Init EOF )
            // InternalStructuredTextParser.g:188:1: ruleVar_Decl_Init EOF
            {
             before(grammarAccess.getVar_Decl_InitRule()); 
            pushFollow(FOLLOW_1);
            ruleVar_Decl_Init();

            state._fsp--;

             after(grammarAccess.getVar_Decl_InitRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVar_Decl_Init"


    // $ANTLR start "ruleVar_Decl_Init"
    // InternalStructuredTextParser.g:195:1: ruleVar_Decl_Init : ( ( rule__Var_Decl_Init__Group__0 ) ) ;
    public final void ruleVar_Decl_Init() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:199:5: ( ( ( rule__Var_Decl_Init__Group__0 ) ) )
            // InternalStructuredTextParser.g:200:1: ( ( rule__Var_Decl_Init__Group__0 ) )
            {
            // InternalStructuredTextParser.g:200:1: ( ( rule__Var_Decl_Init__Group__0 ) )
            // InternalStructuredTextParser.g:201:1: ( rule__Var_Decl_Init__Group__0 )
            {
             before(grammarAccess.getVar_Decl_InitAccess().getGroup()); 
            // InternalStructuredTextParser.g:202:1: ( rule__Var_Decl_Init__Group__0 )
            // InternalStructuredTextParser.g:202:2: rule__Var_Decl_Init__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Var_Decl_Init__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVar_Decl_InitAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVar_Decl_Init"


    // $ANTLR start "entryRuleStmt_List"
    // InternalStructuredTextParser.g:214:1: entryRuleStmt_List : ruleStmt_List EOF ;
    public final void entryRuleStmt_List() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:215:1: ( ruleStmt_List EOF )
            // InternalStructuredTextParser.g:216:1: ruleStmt_List EOF
            {
             before(grammarAccess.getStmt_ListRule()); 
            pushFollow(FOLLOW_1);
            ruleStmt_List();

            state._fsp--;

             after(grammarAccess.getStmt_ListRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStmt_List"


    // $ANTLR start "ruleStmt_List"
    // InternalStructuredTextParser.g:223:1: ruleStmt_List : ( ( rule__Stmt_List__Group__0 ) ) ;
    public final void ruleStmt_List() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:227:5: ( ( ( rule__Stmt_List__Group__0 ) ) )
            // InternalStructuredTextParser.g:228:1: ( ( rule__Stmt_List__Group__0 ) )
            {
            // InternalStructuredTextParser.g:228:1: ( ( rule__Stmt_List__Group__0 ) )
            // InternalStructuredTextParser.g:229:1: ( rule__Stmt_List__Group__0 )
            {
             before(grammarAccess.getStmt_ListAccess().getGroup()); 
            // InternalStructuredTextParser.g:230:1: ( rule__Stmt_List__Group__0 )
            // InternalStructuredTextParser.g:230:2: rule__Stmt_List__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Stmt_List__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStmt_ListAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStmt_List"


    // $ANTLR start "entryRuleStmt"
    // InternalStructuredTextParser.g:242:1: entryRuleStmt : ruleStmt EOF ;
    public final void entryRuleStmt() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:243:1: ( ruleStmt EOF )
            // InternalStructuredTextParser.g:244:1: ruleStmt EOF
            {
             before(grammarAccess.getStmtRule()); 
            pushFollow(FOLLOW_1);
            ruleStmt();

            state._fsp--;

             after(grammarAccess.getStmtRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStmt"


    // $ANTLR start "ruleStmt"
    // InternalStructuredTextParser.g:251:1: ruleStmt : ( ( rule__Stmt__Alternatives ) ) ;
    public final void ruleStmt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:255:5: ( ( ( rule__Stmt__Alternatives ) ) )
            // InternalStructuredTextParser.g:256:1: ( ( rule__Stmt__Alternatives ) )
            {
            // InternalStructuredTextParser.g:256:1: ( ( rule__Stmt__Alternatives ) )
            // InternalStructuredTextParser.g:257:1: ( rule__Stmt__Alternatives )
            {
             before(grammarAccess.getStmtAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:258:1: ( rule__Stmt__Alternatives )
            // InternalStructuredTextParser.g:258:2: rule__Stmt__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Stmt__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getStmtAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStmt"


    // $ANTLR start "entryRuleAssign_Stmt"
    // InternalStructuredTextParser.g:270:1: entryRuleAssign_Stmt : ruleAssign_Stmt EOF ;
    public final void entryRuleAssign_Stmt() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:271:1: ( ruleAssign_Stmt EOF )
            // InternalStructuredTextParser.g:272:1: ruleAssign_Stmt EOF
            {
             before(grammarAccess.getAssign_StmtRule()); 
            pushFollow(FOLLOW_1);
            ruleAssign_Stmt();

            state._fsp--;

             after(grammarAccess.getAssign_StmtRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAssign_Stmt"


    // $ANTLR start "ruleAssign_Stmt"
    // InternalStructuredTextParser.g:279:1: ruleAssign_Stmt : ( ( rule__Assign_Stmt__Group__0 ) ) ;
    public final void ruleAssign_Stmt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:283:5: ( ( ( rule__Assign_Stmt__Group__0 ) ) )
            // InternalStructuredTextParser.g:284:1: ( ( rule__Assign_Stmt__Group__0 ) )
            {
            // InternalStructuredTextParser.g:284:1: ( ( rule__Assign_Stmt__Group__0 ) )
            // InternalStructuredTextParser.g:285:1: ( rule__Assign_Stmt__Group__0 )
            {
             before(grammarAccess.getAssign_StmtAccess().getGroup()); 
            // InternalStructuredTextParser.g:286:1: ( rule__Assign_Stmt__Group__0 )
            // InternalStructuredTextParser.g:286:2: rule__Assign_Stmt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Assign_Stmt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAssign_StmtAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAssign_Stmt"


    // $ANTLR start "entryRuleSubprog_Ctrl_Stmt"
    // InternalStructuredTextParser.g:298:1: entryRuleSubprog_Ctrl_Stmt : ruleSubprog_Ctrl_Stmt EOF ;
    public final void entryRuleSubprog_Ctrl_Stmt() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:299:1: ( ruleSubprog_Ctrl_Stmt EOF )
            // InternalStructuredTextParser.g:300:1: ruleSubprog_Ctrl_Stmt EOF
            {
             before(grammarAccess.getSubprog_Ctrl_StmtRule()); 
            pushFollow(FOLLOW_1);
            ruleSubprog_Ctrl_Stmt();

            state._fsp--;

             after(grammarAccess.getSubprog_Ctrl_StmtRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSubprog_Ctrl_Stmt"


    // $ANTLR start "ruleSubprog_Ctrl_Stmt"
    // InternalStructuredTextParser.g:307:1: ruleSubprog_Ctrl_Stmt : ( ( rule__Subprog_Ctrl_Stmt__Alternatives ) ) ;
    public final void ruleSubprog_Ctrl_Stmt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:311:5: ( ( ( rule__Subprog_Ctrl_Stmt__Alternatives ) ) )
            // InternalStructuredTextParser.g:312:1: ( ( rule__Subprog_Ctrl_Stmt__Alternatives ) )
            {
            // InternalStructuredTextParser.g:312:1: ( ( rule__Subprog_Ctrl_Stmt__Alternatives ) )
            // InternalStructuredTextParser.g:313:1: ( rule__Subprog_Ctrl_Stmt__Alternatives )
            {
             before(grammarAccess.getSubprog_Ctrl_StmtAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:314:1: ( rule__Subprog_Ctrl_Stmt__Alternatives )
            // InternalStructuredTextParser.g:314:2: rule__Subprog_Ctrl_Stmt__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Subprog_Ctrl_Stmt__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSubprog_Ctrl_StmtAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSubprog_Ctrl_Stmt"


    // $ANTLR start "entryRuleSelection_Stmt"
    // InternalStructuredTextParser.g:326:1: entryRuleSelection_Stmt : ruleSelection_Stmt EOF ;
    public final void entryRuleSelection_Stmt() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:327:1: ( ruleSelection_Stmt EOF )
            // InternalStructuredTextParser.g:328:1: ruleSelection_Stmt EOF
            {
             before(grammarAccess.getSelection_StmtRule()); 
            pushFollow(FOLLOW_1);
            ruleSelection_Stmt();

            state._fsp--;

             after(grammarAccess.getSelection_StmtRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSelection_Stmt"


    // $ANTLR start "ruleSelection_Stmt"
    // InternalStructuredTextParser.g:335:1: ruleSelection_Stmt : ( ( rule__Selection_Stmt__Alternatives ) ) ;
    public final void ruleSelection_Stmt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:339:5: ( ( ( rule__Selection_Stmt__Alternatives ) ) )
            // InternalStructuredTextParser.g:340:1: ( ( rule__Selection_Stmt__Alternatives ) )
            {
            // InternalStructuredTextParser.g:340:1: ( ( rule__Selection_Stmt__Alternatives ) )
            // InternalStructuredTextParser.g:341:1: ( rule__Selection_Stmt__Alternatives )
            {
             before(grammarAccess.getSelection_StmtAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:342:1: ( rule__Selection_Stmt__Alternatives )
            // InternalStructuredTextParser.g:342:2: rule__Selection_Stmt__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Selection_Stmt__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSelection_StmtAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSelection_Stmt"


    // $ANTLR start "entryRuleIF_Stmt"
    // InternalStructuredTextParser.g:354:1: entryRuleIF_Stmt : ruleIF_Stmt EOF ;
    public final void entryRuleIF_Stmt() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:355:1: ( ruleIF_Stmt EOF )
            // InternalStructuredTextParser.g:356:1: ruleIF_Stmt EOF
            {
             before(grammarAccess.getIF_StmtRule()); 
            pushFollow(FOLLOW_1);
            ruleIF_Stmt();

            state._fsp--;

             after(grammarAccess.getIF_StmtRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIF_Stmt"


    // $ANTLR start "ruleIF_Stmt"
    // InternalStructuredTextParser.g:363:1: ruleIF_Stmt : ( ( rule__IF_Stmt__Group__0 ) ) ;
    public final void ruleIF_Stmt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:367:5: ( ( ( rule__IF_Stmt__Group__0 ) ) )
            // InternalStructuredTextParser.g:368:1: ( ( rule__IF_Stmt__Group__0 ) )
            {
            // InternalStructuredTextParser.g:368:1: ( ( rule__IF_Stmt__Group__0 ) )
            // InternalStructuredTextParser.g:369:1: ( rule__IF_Stmt__Group__0 )
            {
             before(grammarAccess.getIF_StmtAccess().getGroup()); 
            // InternalStructuredTextParser.g:370:1: ( rule__IF_Stmt__Group__0 )
            // InternalStructuredTextParser.g:370:2: rule__IF_Stmt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__IF_Stmt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIF_StmtAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIF_Stmt"


    // $ANTLR start "entryRuleELSIF_Clause"
    // InternalStructuredTextParser.g:382:1: entryRuleELSIF_Clause : ruleELSIF_Clause EOF ;
    public final void entryRuleELSIF_Clause() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:383:1: ( ruleELSIF_Clause EOF )
            // InternalStructuredTextParser.g:384:1: ruleELSIF_Clause EOF
            {
             before(grammarAccess.getELSIF_ClauseRule()); 
            pushFollow(FOLLOW_1);
            ruleELSIF_Clause();

            state._fsp--;

             after(grammarAccess.getELSIF_ClauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleELSIF_Clause"


    // $ANTLR start "ruleELSIF_Clause"
    // InternalStructuredTextParser.g:391:1: ruleELSIF_Clause : ( ( rule__ELSIF_Clause__Group__0 ) ) ;
    public final void ruleELSIF_Clause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:395:5: ( ( ( rule__ELSIF_Clause__Group__0 ) ) )
            // InternalStructuredTextParser.g:396:1: ( ( rule__ELSIF_Clause__Group__0 ) )
            {
            // InternalStructuredTextParser.g:396:1: ( ( rule__ELSIF_Clause__Group__0 ) )
            // InternalStructuredTextParser.g:397:1: ( rule__ELSIF_Clause__Group__0 )
            {
             before(grammarAccess.getELSIF_ClauseAccess().getGroup()); 
            // InternalStructuredTextParser.g:398:1: ( rule__ELSIF_Clause__Group__0 )
            // InternalStructuredTextParser.g:398:2: rule__ELSIF_Clause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ELSIF_Clause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getELSIF_ClauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleELSIF_Clause"


    // $ANTLR start "entryRuleELSE_Clause"
    // InternalStructuredTextParser.g:410:1: entryRuleELSE_Clause : ruleELSE_Clause EOF ;
    public final void entryRuleELSE_Clause() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:411:1: ( ruleELSE_Clause EOF )
            // InternalStructuredTextParser.g:412:1: ruleELSE_Clause EOF
            {
             before(grammarAccess.getELSE_ClauseRule()); 
            pushFollow(FOLLOW_1);
            ruleELSE_Clause();

            state._fsp--;

             after(grammarAccess.getELSE_ClauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleELSE_Clause"


    // $ANTLR start "ruleELSE_Clause"
    // InternalStructuredTextParser.g:419:1: ruleELSE_Clause : ( ( rule__ELSE_Clause__Group__0 ) ) ;
    public final void ruleELSE_Clause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:423:5: ( ( ( rule__ELSE_Clause__Group__0 ) ) )
            // InternalStructuredTextParser.g:424:1: ( ( rule__ELSE_Clause__Group__0 ) )
            {
            // InternalStructuredTextParser.g:424:1: ( ( rule__ELSE_Clause__Group__0 ) )
            // InternalStructuredTextParser.g:425:1: ( rule__ELSE_Clause__Group__0 )
            {
             before(grammarAccess.getELSE_ClauseAccess().getGroup()); 
            // InternalStructuredTextParser.g:426:1: ( rule__ELSE_Clause__Group__0 )
            // InternalStructuredTextParser.g:426:2: rule__ELSE_Clause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ELSE_Clause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getELSE_ClauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleELSE_Clause"


    // $ANTLR start "entryRuleCase_Stmt"
    // InternalStructuredTextParser.g:438:1: entryRuleCase_Stmt : ruleCase_Stmt EOF ;
    public final void entryRuleCase_Stmt() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:439:1: ( ruleCase_Stmt EOF )
            // InternalStructuredTextParser.g:440:1: ruleCase_Stmt EOF
            {
             before(grammarAccess.getCase_StmtRule()); 
            pushFollow(FOLLOW_1);
            ruleCase_Stmt();

            state._fsp--;

             after(grammarAccess.getCase_StmtRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCase_Stmt"


    // $ANTLR start "ruleCase_Stmt"
    // InternalStructuredTextParser.g:447:1: ruleCase_Stmt : ( ( rule__Case_Stmt__Group__0 ) ) ;
    public final void ruleCase_Stmt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:451:5: ( ( ( rule__Case_Stmt__Group__0 ) ) )
            // InternalStructuredTextParser.g:452:1: ( ( rule__Case_Stmt__Group__0 ) )
            {
            // InternalStructuredTextParser.g:452:1: ( ( rule__Case_Stmt__Group__0 ) )
            // InternalStructuredTextParser.g:453:1: ( rule__Case_Stmt__Group__0 )
            {
             before(grammarAccess.getCase_StmtAccess().getGroup()); 
            // InternalStructuredTextParser.g:454:1: ( rule__Case_Stmt__Group__0 )
            // InternalStructuredTextParser.g:454:2: rule__Case_Stmt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Case_Stmt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCase_StmtAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCase_Stmt"


    // $ANTLR start "entryRuleCase_Selection"
    // InternalStructuredTextParser.g:466:1: entryRuleCase_Selection : ruleCase_Selection EOF ;
    public final void entryRuleCase_Selection() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:467:1: ( ruleCase_Selection EOF )
            // InternalStructuredTextParser.g:468:1: ruleCase_Selection EOF
            {
             before(grammarAccess.getCase_SelectionRule()); 
            pushFollow(FOLLOW_1);
            ruleCase_Selection();

            state._fsp--;

             after(grammarAccess.getCase_SelectionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCase_Selection"


    // $ANTLR start "ruleCase_Selection"
    // InternalStructuredTextParser.g:475:1: ruleCase_Selection : ( ( rule__Case_Selection__Group__0 ) ) ;
    public final void ruleCase_Selection() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:479:5: ( ( ( rule__Case_Selection__Group__0 ) ) )
            // InternalStructuredTextParser.g:480:1: ( ( rule__Case_Selection__Group__0 ) )
            {
            // InternalStructuredTextParser.g:480:1: ( ( rule__Case_Selection__Group__0 ) )
            // InternalStructuredTextParser.g:481:1: ( rule__Case_Selection__Group__0 )
            {
             before(grammarAccess.getCase_SelectionAccess().getGroup()); 
            // InternalStructuredTextParser.g:482:1: ( rule__Case_Selection__Group__0 )
            // InternalStructuredTextParser.g:482:2: rule__Case_Selection__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Case_Selection__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCase_SelectionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCase_Selection"


    // $ANTLR start "entryRuleIteration_Stmt"
    // InternalStructuredTextParser.g:494:1: entryRuleIteration_Stmt : ruleIteration_Stmt EOF ;
    public final void entryRuleIteration_Stmt() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:495:1: ( ruleIteration_Stmt EOF )
            // InternalStructuredTextParser.g:496:1: ruleIteration_Stmt EOF
            {
             before(grammarAccess.getIteration_StmtRule()); 
            pushFollow(FOLLOW_1);
            ruleIteration_Stmt();

            state._fsp--;

             after(grammarAccess.getIteration_StmtRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIteration_Stmt"


    // $ANTLR start "ruleIteration_Stmt"
    // InternalStructuredTextParser.g:503:1: ruleIteration_Stmt : ( ( rule__Iteration_Stmt__Alternatives ) ) ;
    public final void ruleIteration_Stmt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:507:5: ( ( ( rule__Iteration_Stmt__Alternatives ) ) )
            // InternalStructuredTextParser.g:508:1: ( ( rule__Iteration_Stmt__Alternatives ) )
            {
            // InternalStructuredTextParser.g:508:1: ( ( rule__Iteration_Stmt__Alternatives ) )
            // InternalStructuredTextParser.g:509:1: ( rule__Iteration_Stmt__Alternatives )
            {
             before(grammarAccess.getIteration_StmtAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:510:1: ( rule__Iteration_Stmt__Alternatives )
            // InternalStructuredTextParser.g:510:2: rule__Iteration_Stmt__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Iteration_Stmt__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getIteration_StmtAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIteration_Stmt"


    // $ANTLR start "entryRuleFor_Stmt"
    // InternalStructuredTextParser.g:522:1: entryRuleFor_Stmt : ruleFor_Stmt EOF ;
    public final void entryRuleFor_Stmt() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:523:1: ( ruleFor_Stmt EOF )
            // InternalStructuredTextParser.g:524:1: ruleFor_Stmt EOF
            {
             before(grammarAccess.getFor_StmtRule()); 
            pushFollow(FOLLOW_1);
            ruleFor_Stmt();

            state._fsp--;

             after(grammarAccess.getFor_StmtRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFor_Stmt"


    // $ANTLR start "ruleFor_Stmt"
    // InternalStructuredTextParser.g:531:1: ruleFor_Stmt : ( ( rule__For_Stmt__Group__0 ) ) ;
    public final void ruleFor_Stmt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:535:5: ( ( ( rule__For_Stmt__Group__0 ) ) )
            // InternalStructuredTextParser.g:536:1: ( ( rule__For_Stmt__Group__0 ) )
            {
            // InternalStructuredTextParser.g:536:1: ( ( rule__For_Stmt__Group__0 ) )
            // InternalStructuredTextParser.g:537:1: ( rule__For_Stmt__Group__0 )
            {
             before(grammarAccess.getFor_StmtAccess().getGroup()); 
            // InternalStructuredTextParser.g:538:1: ( rule__For_Stmt__Group__0 )
            // InternalStructuredTextParser.g:538:2: rule__For_Stmt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__For_Stmt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFor_StmtAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFor_Stmt"


    // $ANTLR start "entryRuleWhile_Stmt"
    // InternalStructuredTextParser.g:550:1: entryRuleWhile_Stmt : ruleWhile_Stmt EOF ;
    public final void entryRuleWhile_Stmt() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:551:1: ( ruleWhile_Stmt EOF )
            // InternalStructuredTextParser.g:552:1: ruleWhile_Stmt EOF
            {
             before(grammarAccess.getWhile_StmtRule()); 
            pushFollow(FOLLOW_1);
            ruleWhile_Stmt();

            state._fsp--;

             after(grammarAccess.getWhile_StmtRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWhile_Stmt"


    // $ANTLR start "ruleWhile_Stmt"
    // InternalStructuredTextParser.g:559:1: ruleWhile_Stmt : ( ( rule__While_Stmt__Group__0 ) ) ;
    public final void ruleWhile_Stmt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:563:5: ( ( ( rule__While_Stmt__Group__0 ) ) )
            // InternalStructuredTextParser.g:564:1: ( ( rule__While_Stmt__Group__0 ) )
            {
            // InternalStructuredTextParser.g:564:1: ( ( rule__While_Stmt__Group__0 ) )
            // InternalStructuredTextParser.g:565:1: ( rule__While_Stmt__Group__0 )
            {
             before(grammarAccess.getWhile_StmtAccess().getGroup()); 
            // InternalStructuredTextParser.g:566:1: ( rule__While_Stmt__Group__0 )
            // InternalStructuredTextParser.g:566:2: rule__While_Stmt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__While_Stmt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWhile_StmtAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWhile_Stmt"


    // $ANTLR start "entryRuleRepeat_Stmt"
    // InternalStructuredTextParser.g:578:1: entryRuleRepeat_Stmt : ruleRepeat_Stmt EOF ;
    public final void entryRuleRepeat_Stmt() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:579:1: ( ruleRepeat_Stmt EOF )
            // InternalStructuredTextParser.g:580:1: ruleRepeat_Stmt EOF
            {
             before(grammarAccess.getRepeat_StmtRule()); 
            pushFollow(FOLLOW_1);
            ruleRepeat_Stmt();

            state._fsp--;

             after(grammarAccess.getRepeat_StmtRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRepeat_Stmt"


    // $ANTLR start "ruleRepeat_Stmt"
    // InternalStructuredTextParser.g:587:1: ruleRepeat_Stmt : ( ( rule__Repeat_Stmt__Group__0 ) ) ;
    public final void ruleRepeat_Stmt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:591:5: ( ( ( rule__Repeat_Stmt__Group__0 ) ) )
            // InternalStructuredTextParser.g:592:1: ( ( rule__Repeat_Stmt__Group__0 ) )
            {
            // InternalStructuredTextParser.g:592:1: ( ( rule__Repeat_Stmt__Group__0 ) )
            // InternalStructuredTextParser.g:593:1: ( rule__Repeat_Stmt__Group__0 )
            {
             before(grammarAccess.getRepeat_StmtAccess().getGroup()); 
            // InternalStructuredTextParser.g:594:1: ( rule__Repeat_Stmt__Group__0 )
            // InternalStructuredTextParser.g:594:2: rule__Repeat_Stmt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Repeat_Stmt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRepeat_StmtAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRepeat_Stmt"


    // $ANTLR start "entryRuleExpression"
    // InternalStructuredTextParser.g:606:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:607:1: ( ruleExpression EOF )
            // InternalStructuredTextParser.g:608:1: ruleExpression EOF
            {
             before(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalStructuredTextParser.g:615:1: ruleExpression : ( ruleOr_Expression ) ;
    public final void ruleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:619:5: ( ( ruleOr_Expression ) )
            // InternalStructuredTextParser.g:620:1: ( ruleOr_Expression )
            {
            // InternalStructuredTextParser.g:620:1: ( ruleOr_Expression )
            // InternalStructuredTextParser.g:621:1: ruleOr_Expression
            {
             before(grammarAccess.getExpressionAccess().getOr_ExpressionParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleOr_Expression();

            state._fsp--;

             after(grammarAccess.getExpressionAccess().getOr_ExpressionParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleOr_Expression"
    // InternalStructuredTextParser.g:634:1: entryRuleOr_Expression : ruleOr_Expression EOF ;
    public final void entryRuleOr_Expression() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:635:1: ( ruleOr_Expression EOF )
            // InternalStructuredTextParser.g:636:1: ruleOr_Expression EOF
            {
             before(grammarAccess.getOr_ExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleOr_Expression();

            state._fsp--;

             after(grammarAccess.getOr_ExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr_Expression"


    // $ANTLR start "ruleOr_Expression"
    // InternalStructuredTextParser.g:643:1: ruleOr_Expression : ( ( rule__Or_Expression__Group__0 ) ) ;
    public final void ruleOr_Expression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:647:5: ( ( ( rule__Or_Expression__Group__0 ) ) )
            // InternalStructuredTextParser.g:648:1: ( ( rule__Or_Expression__Group__0 ) )
            {
            // InternalStructuredTextParser.g:648:1: ( ( rule__Or_Expression__Group__0 ) )
            // InternalStructuredTextParser.g:649:1: ( rule__Or_Expression__Group__0 )
            {
             before(grammarAccess.getOr_ExpressionAccess().getGroup()); 
            // InternalStructuredTextParser.g:650:1: ( rule__Or_Expression__Group__0 )
            // InternalStructuredTextParser.g:650:2: rule__Or_Expression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Or_Expression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOr_ExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr_Expression"


    // $ANTLR start "entryRuleXor_Expr"
    // InternalStructuredTextParser.g:662:1: entryRuleXor_Expr : ruleXor_Expr EOF ;
    public final void entryRuleXor_Expr() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:663:1: ( ruleXor_Expr EOF )
            // InternalStructuredTextParser.g:664:1: ruleXor_Expr EOF
            {
             before(grammarAccess.getXor_ExprRule()); 
            pushFollow(FOLLOW_1);
            ruleXor_Expr();

            state._fsp--;

             after(grammarAccess.getXor_ExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXor_Expr"


    // $ANTLR start "ruleXor_Expr"
    // InternalStructuredTextParser.g:671:1: ruleXor_Expr : ( ( rule__Xor_Expr__Group__0 ) ) ;
    public final void ruleXor_Expr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:675:5: ( ( ( rule__Xor_Expr__Group__0 ) ) )
            // InternalStructuredTextParser.g:676:1: ( ( rule__Xor_Expr__Group__0 ) )
            {
            // InternalStructuredTextParser.g:676:1: ( ( rule__Xor_Expr__Group__0 ) )
            // InternalStructuredTextParser.g:677:1: ( rule__Xor_Expr__Group__0 )
            {
             before(grammarAccess.getXor_ExprAccess().getGroup()); 
            // InternalStructuredTextParser.g:678:1: ( rule__Xor_Expr__Group__0 )
            // InternalStructuredTextParser.g:678:2: rule__Xor_Expr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Xor_Expr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXor_ExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXor_Expr"


    // $ANTLR start "entryRuleAnd_Expr"
    // InternalStructuredTextParser.g:690:1: entryRuleAnd_Expr : ruleAnd_Expr EOF ;
    public final void entryRuleAnd_Expr() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:691:1: ( ruleAnd_Expr EOF )
            // InternalStructuredTextParser.g:692:1: ruleAnd_Expr EOF
            {
             before(grammarAccess.getAnd_ExprRule()); 
            pushFollow(FOLLOW_1);
            ruleAnd_Expr();

            state._fsp--;

             after(grammarAccess.getAnd_ExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd_Expr"


    // $ANTLR start "ruleAnd_Expr"
    // InternalStructuredTextParser.g:699:1: ruleAnd_Expr : ( ( rule__And_Expr__Group__0 ) ) ;
    public final void ruleAnd_Expr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:703:5: ( ( ( rule__And_Expr__Group__0 ) ) )
            // InternalStructuredTextParser.g:704:1: ( ( rule__And_Expr__Group__0 ) )
            {
            // InternalStructuredTextParser.g:704:1: ( ( rule__And_Expr__Group__0 ) )
            // InternalStructuredTextParser.g:705:1: ( rule__And_Expr__Group__0 )
            {
             before(grammarAccess.getAnd_ExprAccess().getGroup()); 
            // InternalStructuredTextParser.g:706:1: ( rule__And_Expr__Group__0 )
            // InternalStructuredTextParser.g:706:2: rule__And_Expr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__And_Expr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAnd_ExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd_Expr"


    // $ANTLR start "entryRuleCompare_Expr"
    // InternalStructuredTextParser.g:718:1: entryRuleCompare_Expr : ruleCompare_Expr EOF ;
    public final void entryRuleCompare_Expr() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:719:1: ( ruleCompare_Expr EOF )
            // InternalStructuredTextParser.g:720:1: ruleCompare_Expr EOF
            {
             before(grammarAccess.getCompare_ExprRule()); 
            pushFollow(FOLLOW_1);
            ruleCompare_Expr();

            state._fsp--;

             after(grammarAccess.getCompare_ExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCompare_Expr"


    // $ANTLR start "ruleCompare_Expr"
    // InternalStructuredTextParser.g:727:1: ruleCompare_Expr : ( ( rule__Compare_Expr__Group__0 ) ) ;
    public final void ruleCompare_Expr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:731:5: ( ( ( rule__Compare_Expr__Group__0 ) ) )
            // InternalStructuredTextParser.g:732:1: ( ( rule__Compare_Expr__Group__0 ) )
            {
            // InternalStructuredTextParser.g:732:1: ( ( rule__Compare_Expr__Group__0 ) )
            // InternalStructuredTextParser.g:733:1: ( rule__Compare_Expr__Group__0 )
            {
             before(grammarAccess.getCompare_ExprAccess().getGroup()); 
            // InternalStructuredTextParser.g:734:1: ( rule__Compare_Expr__Group__0 )
            // InternalStructuredTextParser.g:734:2: rule__Compare_Expr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Compare_Expr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCompare_ExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCompare_Expr"


    // $ANTLR start "entryRuleEqu_Expr"
    // InternalStructuredTextParser.g:746:1: entryRuleEqu_Expr : ruleEqu_Expr EOF ;
    public final void entryRuleEqu_Expr() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:747:1: ( ruleEqu_Expr EOF )
            // InternalStructuredTextParser.g:748:1: ruleEqu_Expr EOF
            {
             before(grammarAccess.getEqu_ExprRule()); 
            pushFollow(FOLLOW_1);
            ruleEqu_Expr();

            state._fsp--;

             after(grammarAccess.getEqu_ExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEqu_Expr"


    // $ANTLR start "ruleEqu_Expr"
    // InternalStructuredTextParser.g:755:1: ruleEqu_Expr : ( ( rule__Equ_Expr__Group__0 ) ) ;
    public final void ruleEqu_Expr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:759:5: ( ( ( rule__Equ_Expr__Group__0 ) ) )
            // InternalStructuredTextParser.g:760:1: ( ( rule__Equ_Expr__Group__0 ) )
            {
            // InternalStructuredTextParser.g:760:1: ( ( rule__Equ_Expr__Group__0 ) )
            // InternalStructuredTextParser.g:761:1: ( rule__Equ_Expr__Group__0 )
            {
             before(grammarAccess.getEqu_ExprAccess().getGroup()); 
            // InternalStructuredTextParser.g:762:1: ( rule__Equ_Expr__Group__0 )
            // InternalStructuredTextParser.g:762:2: rule__Equ_Expr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Equ_Expr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEqu_ExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEqu_Expr"


    // $ANTLR start "entryRuleAdd_Expr"
    // InternalStructuredTextParser.g:774:1: entryRuleAdd_Expr : ruleAdd_Expr EOF ;
    public final void entryRuleAdd_Expr() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:775:1: ( ruleAdd_Expr EOF )
            // InternalStructuredTextParser.g:776:1: ruleAdd_Expr EOF
            {
             before(grammarAccess.getAdd_ExprRule()); 
            pushFollow(FOLLOW_1);
            ruleAdd_Expr();

            state._fsp--;

             after(grammarAccess.getAdd_ExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAdd_Expr"


    // $ANTLR start "ruleAdd_Expr"
    // InternalStructuredTextParser.g:783:1: ruleAdd_Expr : ( ( rule__Add_Expr__Group__0 ) ) ;
    public final void ruleAdd_Expr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:787:5: ( ( ( rule__Add_Expr__Group__0 ) ) )
            // InternalStructuredTextParser.g:788:1: ( ( rule__Add_Expr__Group__0 ) )
            {
            // InternalStructuredTextParser.g:788:1: ( ( rule__Add_Expr__Group__0 ) )
            // InternalStructuredTextParser.g:789:1: ( rule__Add_Expr__Group__0 )
            {
             before(grammarAccess.getAdd_ExprAccess().getGroup()); 
            // InternalStructuredTextParser.g:790:1: ( rule__Add_Expr__Group__0 )
            // InternalStructuredTextParser.g:790:2: rule__Add_Expr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Add_Expr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAdd_ExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAdd_Expr"


    // $ANTLR start "entryRuleTerm"
    // InternalStructuredTextParser.g:802:1: entryRuleTerm : ruleTerm EOF ;
    public final void entryRuleTerm() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:803:1: ( ruleTerm EOF )
            // InternalStructuredTextParser.g:804:1: ruleTerm EOF
            {
             before(grammarAccess.getTermRule()); 
            pushFollow(FOLLOW_1);
            ruleTerm();

            state._fsp--;

             after(grammarAccess.getTermRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTerm"


    // $ANTLR start "ruleTerm"
    // InternalStructuredTextParser.g:811:1: ruleTerm : ( ( rule__Term__Group__0 ) ) ;
    public final void ruleTerm() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:815:5: ( ( ( rule__Term__Group__0 ) ) )
            // InternalStructuredTextParser.g:816:1: ( ( rule__Term__Group__0 ) )
            {
            // InternalStructuredTextParser.g:816:1: ( ( rule__Term__Group__0 ) )
            // InternalStructuredTextParser.g:817:1: ( rule__Term__Group__0 )
            {
             before(grammarAccess.getTermAccess().getGroup()); 
            // InternalStructuredTextParser.g:818:1: ( rule__Term__Group__0 )
            // InternalStructuredTextParser.g:818:2: rule__Term__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Term__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTermAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTerm"


    // $ANTLR start "entryRulePower_Expr"
    // InternalStructuredTextParser.g:830:1: entryRulePower_Expr : rulePower_Expr EOF ;
    public final void entryRulePower_Expr() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:831:1: ( rulePower_Expr EOF )
            // InternalStructuredTextParser.g:832:1: rulePower_Expr EOF
            {
             before(grammarAccess.getPower_ExprRule()); 
            pushFollow(FOLLOW_1);
            rulePower_Expr();

            state._fsp--;

             after(grammarAccess.getPower_ExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePower_Expr"


    // $ANTLR start "rulePower_Expr"
    // InternalStructuredTextParser.g:839:1: rulePower_Expr : ( ( rule__Power_Expr__Group__0 ) ) ;
    public final void rulePower_Expr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:843:5: ( ( ( rule__Power_Expr__Group__0 ) ) )
            // InternalStructuredTextParser.g:844:1: ( ( rule__Power_Expr__Group__0 ) )
            {
            // InternalStructuredTextParser.g:844:1: ( ( rule__Power_Expr__Group__0 ) )
            // InternalStructuredTextParser.g:845:1: ( rule__Power_Expr__Group__0 )
            {
             before(grammarAccess.getPower_ExprAccess().getGroup()); 
            // InternalStructuredTextParser.g:846:1: ( rule__Power_Expr__Group__0 )
            // InternalStructuredTextParser.g:846:2: rule__Power_Expr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Power_Expr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPower_ExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePower_Expr"


    // $ANTLR start "entryRuleUnary_Expr"
    // InternalStructuredTextParser.g:858:1: entryRuleUnary_Expr : ruleUnary_Expr EOF ;
    public final void entryRuleUnary_Expr() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:859:1: ( ruleUnary_Expr EOF )
            // InternalStructuredTextParser.g:860:1: ruleUnary_Expr EOF
            {
             before(grammarAccess.getUnary_ExprRule()); 
            pushFollow(FOLLOW_1);
            ruleUnary_Expr();

            state._fsp--;

             after(grammarAccess.getUnary_ExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnary_Expr"


    // $ANTLR start "ruleUnary_Expr"
    // InternalStructuredTextParser.g:867:1: ruleUnary_Expr : ( ( rule__Unary_Expr__Alternatives ) ) ;
    public final void ruleUnary_Expr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:871:5: ( ( ( rule__Unary_Expr__Alternatives ) ) )
            // InternalStructuredTextParser.g:872:1: ( ( rule__Unary_Expr__Alternatives ) )
            {
            // InternalStructuredTextParser.g:872:1: ( ( rule__Unary_Expr__Alternatives ) )
            // InternalStructuredTextParser.g:873:1: ( rule__Unary_Expr__Alternatives )
            {
             before(grammarAccess.getUnary_ExprAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:874:1: ( rule__Unary_Expr__Alternatives )
            // InternalStructuredTextParser.g:874:2: rule__Unary_Expr__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Unary_Expr__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUnary_ExprAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnary_Expr"


    // $ANTLR start "entryRulePrimary_Expr"
    // InternalStructuredTextParser.g:886:1: entryRulePrimary_Expr : rulePrimary_Expr EOF ;
    public final void entryRulePrimary_Expr() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:887:1: ( rulePrimary_Expr EOF )
            // InternalStructuredTextParser.g:888:1: rulePrimary_Expr EOF
            {
             before(grammarAccess.getPrimary_ExprRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary_Expr();

            state._fsp--;

             after(grammarAccess.getPrimary_ExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary_Expr"


    // $ANTLR start "rulePrimary_Expr"
    // InternalStructuredTextParser.g:895:1: rulePrimary_Expr : ( ( rule__Primary_Expr__Alternatives ) ) ;
    public final void rulePrimary_Expr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:899:5: ( ( ( rule__Primary_Expr__Alternatives ) ) )
            // InternalStructuredTextParser.g:900:1: ( ( rule__Primary_Expr__Alternatives ) )
            {
            // InternalStructuredTextParser.g:900:1: ( ( rule__Primary_Expr__Alternatives ) )
            // InternalStructuredTextParser.g:901:1: ( rule__Primary_Expr__Alternatives )
            {
             before(grammarAccess.getPrimary_ExprAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:902:1: ( rule__Primary_Expr__Alternatives )
            // InternalStructuredTextParser.g:902:2: rule__Primary_Expr__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary_Expr__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimary_ExprAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary_Expr"


    // $ANTLR start "entryRuleFunc_Call"
    // InternalStructuredTextParser.g:914:1: entryRuleFunc_Call : ruleFunc_Call EOF ;
    public final void entryRuleFunc_Call() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:915:1: ( ruleFunc_Call EOF )
            // InternalStructuredTextParser.g:916:1: ruleFunc_Call EOF
            {
             before(grammarAccess.getFunc_CallRule()); 
            pushFollow(FOLLOW_1);
            ruleFunc_Call();

            state._fsp--;

             after(grammarAccess.getFunc_CallRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunc_Call"


    // $ANTLR start "ruleFunc_Call"
    // InternalStructuredTextParser.g:923:1: ruleFunc_Call : ( ( rule__Func_Call__Group__0 ) ) ;
    public final void ruleFunc_Call() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:927:5: ( ( ( rule__Func_Call__Group__0 ) ) )
            // InternalStructuredTextParser.g:928:1: ( ( rule__Func_Call__Group__0 ) )
            {
            // InternalStructuredTextParser.g:928:1: ( ( rule__Func_Call__Group__0 ) )
            // InternalStructuredTextParser.g:929:1: ( rule__Func_Call__Group__0 )
            {
             before(grammarAccess.getFunc_CallAccess().getGroup()); 
            // InternalStructuredTextParser.g:930:1: ( rule__Func_Call__Group__0 )
            // InternalStructuredTextParser.g:930:2: rule__Func_Call__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Func_Call__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFunc_CallAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunc_Call"


    // $ANTLR start "entryRuleParam_Assign"
    // InternalStructuredTextParser.g:942:1: entryRuleParam_Assign : ruleParam_Assign EOF ;
    public final void entryRuleParam_Assign() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:943:1: ( ruleParam_Assign EOF )
            // InternalStructuredTextParser.g:944:1: ruleParam_Assign EOF
            {
             before(grammarAccess.getParam_AssignRule()); 
            pushFollow(FOLLOW_1);
            ruleParam_Assign();

            state._fsp--;

             after(grammarAccess.getParam_AssignRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParam_Assign"


    // $ANTLR start "ruleParam_Assign"
    // InternalStructuredTextParser.g:951:1: ruleParam_Assign : ( ( rule__Param_Assign__Alternatives ) ) ;
    public final void ruleParam_Assign() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:955:5: ( ( ( rule__Param_Assign__Alternatives ) ) )
            // InternalStructuredTextParser.g:956:1: ( ( rule__Param_Assign__Alternatives ) )
            {
            // InternalStructuredTextParser.g:956:1: ( ( rule__Param_Assign__Alternatives ) )
            // InternalStructuredTextParser.g:957:1: ( rule__Param_Assign__Alternatives )
            {
             before(grammarAccess.getParam_AssignAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:958:1: ( rule__Param_Assign__Alternatives )
            // InternalStructuredTextParser.g:958:2: rule__Param_Assign__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Param_Assign__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getParam_AssignAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParam_Assign"


    // $ANTLR start "entryRuleParam_Assign_In"
    // InternalStructuredTextParser.g:970:1: entryRuleParam_Assign_In : ruleParam_Assign_In EOF ;
    public final void entryRuleParam_Assign_In() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:971:1: ( ruleParam_Assign_In EOF )
            // InternalStructuredTextParser.g:972:1: ruleParam_Assign_In EOF
            {
             before(grammarAccess.getParam_Assign_InRule()); 
            pushFollow(FOLLOW_1);
            ruleParam_Assign_In();

            state._fsp--;

             after(grammarAccess.getParam_Assign_InRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParam_Assign_In"


    // $ANTLR start "ruleParam_Assign_In"
    // InternalStructuredTextParser.g:979:1: ruleParam_Assign_In : ( ( rule__Param_Assign_In__Group__0 ) ) ;
    public final void ruleParam_Assign_In() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:983:5: ( ( ( rule__Param_Assign_In__Group__0 ) ) )
            // InternalStructuredTextParser.g:984:1: ( ( rule__Param_Assign_In__Group__0 ) )
            {
            // InternalStructuredTextParser.g:984:1: ( ( rule__Param_Assign_In__Group__0 ) )
            // InternalStructuredTextParser.g:985:1: ( rule__Param_Assign_In__Group__0 )
            {
             before(grammarAccess.getParam_Assign_InAccess().getGroup()); 
            // InternalStructuredTextParser.g:986:1: ( rule__Param_Assign_In__Group__0 )
            // InternalStructuredTextParser.g:986:2: rule__Param_Assign_In__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Param_Assign_In__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getParam_Assign_InAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParam_Assign_In"


    // $ANTLR start "entryRuleParam_Assign_Out"
    // InternalStructuredTextParser.g:998:1: entryRuleParam_Assign_Out : ruleParam_Assign_Out EOF ;
    public final void entryRuleParam_Assign_Out() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:999:1: ( ruleParam_Assign_Out EOF )
            // InternalStructuredTextParser.g:1000:1: ruleParam_Assign_Out EOF
            {
             before(grammarAccess.getParam_Assign_OutRule()); 
            pushFollow(FOLLOW_1);
            ruleParam_Assign_Out();

            state._fsp--;

             after(grammarAccess.getParam_Assign_OutRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParam_Assign_Out"


    // $ANTLR start "ruleParam_Assign_Out"
    // InternalStructuredTextParser.g:1007:1: ruleParam_Assign_Out : ( ( rule__Param_Assign_Out__Group__0 ) ) ;
    public final void ruleParam_Assign_Out() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1011:5: ( ( ( rule__Param_Assign_Out__Group__0 ) ) )
            // InternalStructuredTextParser.g:1012:1: ( ( rule__Param_Assign_Out__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1012:1: ( ( rule__Param_Assign_Out__Group__0 ) )
            // InternalStructuredTextParser.g:1013:1: ( rule__Param_Assign_Out__Group__0 )
            {
             before(grammarAccess.getParam_Assign_OutAccess().getGroup()); 
            // InternalStructuredTextParser.g:1014:1: ( rule__Param_Assign_Out__Group__0 )
            // InternalStructuredTextParser.g:1014:2: rule__Param_Assign_Out__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Param_Assign_Out__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getParam_Assign_OutAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParam_Assign_Out"


    // $ANTLR start "entryRuleVariable"
    // InternalStructuredTextParser.g:1026:1: entryRuleVariable : ruleVariable EOF ;
    public final void entryRuleVariable() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1027:1: ( ruleVariable EOF )
            // InternalStructuredTextParser.g:1028:1: ruleVariable EOF
            {
             before(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getVariableRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalStructuredTextParser.g:1035:1: ruleVariable : ( ruleVariable_Subscript ) ;
    public final void ruleVariable() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1039:5: ( ( ruleVariable_Subscript ) )
            // InternalStructuredTextParser.g:1040:1: ( ruleVariable_Subscript )
            {
            // InternalStructuredTextParser.g:1040:1: ( ruleVariable_Subscript )
            // InternalStructuredTextParser.g:1041:1: ruleVariable_Subscript
            {
             before(grammarAccess.getVariableAccess().getVariable_SubscriptParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleVariable_Subscript();

            state._fsp--;

             after(grammarAccess.getVariableAccess().getVariable_SubscriptParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleVariable_Subscript"
    // InternalStructuredTextParser.g:1054:1: entryRuleVariable_Subscript : ruleVariable_Subscript EOF ;
    public final void entryRuleVariable_Subscript() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1055:1: ( ruleVariable_Subscript EOF )
            // InternalStructuredTextParser.g:1056:1: ruleVariable_Subscript EOF
            {
             before(grammarAccess.getVariable_SubscriptRule()); 
            pushFollow(FOLLOW_1);
            ruleVariable_Subscript();

            state._fsp--;

             after(grammarAccess.getVariable_SubscriptRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable_Subscript"


    // $ANTLR start "ruleVariable_Subscript"
    // InternalStructuredTextParser.g:1063:1: ruleVariable_Subscript : ( ( rule__Variable_Subscript__Group__0 ) ) ;
    public final void ruleVariable_Subscript() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1067:5: ( ( ( rule__Variable_Subscript__Group__0 ) ) )
            // InternalStructuredTextParser.g:1068:1: ( ( rule__Variable_Subscript__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1068:1: ( ( rule__Variable_Subscript__Group__0 ) )
            // InternalStructuredTextParser.g:1069:1: ( rule__Variable_Subscript__Group__0 )
            {
             before(grammarAccess.getVariable_SubscriptAccess().getGroup()); 
            // InternalStructuredTextParser.g:1070:1: ( rule__Variable_Subscript__Group__0 )
            // InternalStructuredTextParser.g:1070:2: rule__Variable_Subscript__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Variable_Subscript__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVariable_SubscriptAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable_Subscript"


    // $ANTLR start "entryRuleVariable_Adapter"
    // InternalStructuredTextParser.g:1082:1: entryRuleVariable_Adapter : ruleVariable_Adapter EOF ;
    public final void entryRuleVariable_Adapter() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1083:1: ( ruleVariable_Adapter EOF )
            // InternalStructuredTextParser.g:1084:1: ruleVariable_Adapter EOF
            {
             before(grammarAccess.getVariable_AdapterRule()); 
            pushFollow(FOLLOW_1);
            ruleVariable_Adapter();

            state._fsp--;

             after(grammarAccess.getVariable_AdapterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable_Adapter"


    // $ANTLR start "ruleVariable_Adapter"
    // InternalStructuredTextParser.g:1091:1: ruleVariable_Adapter : ( ( rule__Variable_Adapter__Group__0 ) ) ;
    public final void ruleVariable_Adapter() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1095:5: ( ( ( rule__Variable_Adapter__Group__0 ) ) )
            // InternalStructuredTextParser.g:1096:1: ( ( rule__Variable_Adapter__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1096:1: ( ( rule__Variable_Adapter__Group__0 ) )
            // InternalStructuredTextParser.g:1097:1: ( rule__Variable_Adapter__Group__0 )
            {
             before(grammarAccess.getVariable_AdapterAccess().getGroup()); 
            // InternalStructuredTextParser.g:1098:1: ( rule__Variable_Adapter__Group__0 )
            // InternalStructuredTextParser.g:1098:2: rule__Variable_Adapter__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Variable_Adapter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVariable_AdapterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable_Adapter"


    // $ANTLR start "entryRuleAdapter_Name"
    // InternalStructuredTextParser.g:1110:1: entryRuleAdapter_Name : ruleAdapter_Name EOF ;
    public final void entryRuleAdapter_Name() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1111:1: ( ruleAdapter_Name EOF )
            // InternalStructuredTextParser.g:1112:1: ruleAdapter_Name EOF
            {
             before(grammarAccess.getAdapter_NameRule()); 
            pushFollow(FOLLOW_1);
            ruleAdapter_Name();

            state._fsp--;

             after(grammarAccess.getAdapter_NameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAdapter_Name"


    // $ANTLR start "ruleAdapter_Name"
    // InternalStructuredTextParser.g:1119:1: ruleAdapter_Name : ( ( rule__Adapter_Name__Alternatives ) ) ;
    public final void ruleAdapter_Name() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1123:5: ( ( ( rule__Adapter_Name__Alternatives ) ) )
            // InternalStructuredTextParser.g:1124:1: ( ( rule__Adapter_Name__Alternatives ) )
            {
            // InternalStructuredTextParser.g:1124:1: ( ( rule__Adapter_Name__Alternatives ) )
            // InternalStructuredTextParser.g:1125:1: ( rule__Adapter_Name__Alternatives )
            {
             before(grammarAccess.getAdapter_NameAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:1126:1: ( rule__Adapter_Name__Alternatives )
            // InternalStructuredTextParser.g:1126:2: rule__Adapter_Name__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Adapter_Name__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAdapter_NameAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAdapter_Name"


    // $ANTLR start "entryRuleVariable_Primary"
    // InternalStructuredTextParser.g:1138:1: entryRuleVariable_Primary : ruleVariable_Primary EOF ;
    public final void entryRuleVariable_Primary() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1139:1: ( ruleVariable_Primary EOF )
            // InternalStructuredTextParser.g:1140:1: ruleVariable_Primary EOF
            {
             before(grammarAccess.getVariable_PrimaryRule()); 
            pushFollow(FOLLOW_1);
            ruleVariable_Primary();

            state._fsp--;

             after(grammarAccess.getVariable_PrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable_Primary"


    // $ANTLR start "ruleVariable_Primary"
    // InternalStructuredTextParser.g:1147:1: ruleVariable_Primary : ( ( rule__Variable_Primary__VarAssignment ) ) ;
    public final void ruleVariable_Primary() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1151:5: ( ( ( rule__Variable_Primary__VarAssignment ) ) )
            // InternalStructuredTextParser.g:1152:1: ( ( rule__Variable_Primary__VarAssignment ) )
            {
            // InternalStructuredTextParser.g:1152:1: ( ( rule__Variable_Primary__VarAssignment ) )
            // InternalStructuredTextParser.g:1153:1: ( rule__Variable_Primary__VarAssignment )
            {
             before(grammarAccess.getVariable_PrimaryAccess().getVarAssignment()); 
            // InternalStructuredTextParser.g:1154:1: ( rule__Variable_Primary__VarAssignment )
            // InternalStructuredTextParser.g:1154:2: rule__Variable_Primary__VarAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Variable_Primary__VarAssignment();

            state._fsp--;


            }

             after(grammarAccess.getVariable_PrimaryAccess().getVarAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable_Primary"


    // $ANTLR start "entryRuleVariable_Name"
    // InternalStructuredTextParser.g:1166:1: entryRuleVariable_Name : ruleVariable_Name EOF ;
    public final void entryRuleVariable_Name() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1167:1: ( ruleVariable_Name EOF )
            // InternalStructuredTextParser.g:1168:1: ruleVariable_Name EOF
            {
             before(grammarAccess.getVariable_NameRule()); 
            pushFollow(FOLLOW_1);
            ruleVariable_Name();

            state._fsp--;

             after(grammarAccess.getVariable_NameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable_Name"


    // $ANTLR start "ruleVariable_Name"
    // InternalStructuredTextParser.g:1175:1: ruleVariable_Name : ( ( rule__Variable_Name__Alternatives ) ) ;
    public final void ruleVariable_Name() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1179:5: ( ( ( rule__Variable_Name__Alternatives ) ) )
            // InternalStructuredTextParser.g:1180:1: ( ( rule__Variable_Name__Alternatives ) )
            {
            // InternalStructuredTextParser.g:1180:1: ( ( rule__Variable_Name__Alternatives ) )
            // InternalStructuredTextParser.g:1181:1: ( rule__Variable_Name__Alternatives )
            {
             before(grammarAccess.getVariable_NameAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:1182:1: ( rule__Variable_Name__Alternatives )
            // InternalStructuredTextParser.g:1182:2: rule__Variable_Name__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Variable_Name__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getVariable_NameAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable_Name"


    // $ANTLR start "entryRuleConstant"
    // InternalStructuredTextParser.g:1194:1: entryRuleConstant : ruleConstant EOF ;
    public final void entryRuleConstant() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1195:1: ( ruleConstant EOF )
            // InternalStructuredTextParser.g:1196:1: ruleConstant EOF
            {
             before(grammarAccess.getConstantRule()); 
            pushFollow(FOLLOW_1);
            ruleConstant();

            state._fsp--;

             after(grammarAccess.getConstantRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // InternalStructuredTextParser.g:1203:1: ruleConstant : ( ( rule__Constant__Alternatives ) ) ;
    public final void ruleConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1207:5: ( ( ( rule__Constant__Alternatives ) ) )
            // InternalStructuredTextParser.g:1208:1: ( ( rule__Constant__Alternatives ) )
            {
            // InternalStructuredTextParser.g:1208:1: ( ( rule__Constant__Alternatives ) )
            // InternalStructuredTextParser.g:1209:1: ( rule__Constant__Alternatives )
            {
             before(grammarAccess.getConstantAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:1210:1: ( rule__Constant__Alternatives )
            // InternalStructuredTextParser.g:1210:2: rule__Constant__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Constant__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getConstantAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "entryRuleNumeric_Literal"
    // InternalStructuredTextParser.g:1222:1: entryRuleNumeric_Literal : ruleNumeric_Literal EOF ;
    public final void entryRuleNumeric_Literal() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1223:1: ( ruleNumeric_Literal EOF )
            // InternalStructuredTextParser.g:1224:1: ruleNumeric_Literal EOF
            {
             before(grammarAccess.getNumeric_LiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleNumeric_Literal();

            state._fsp--;

             after(grammarAccess.getNumeric_LiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNumeric_Literal"


    // $ANTLR start "ruleNumeric_Literal"
    // InternalStructuredTextParser.g:1231:1: ruleNumeric_Literal : ( ( rule__Numeric_Literal__Alternatives ) ) ;
    public final void ruleNumeric_Literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1235:5: ( ( ( rule__Numeric_Literal__Alternatives ) ) )
            // InternalStructuredTextParser.g:1236:1: ( ( rule__Numeric_Literal__Alternatives ) )
            {
            // InternalStructuredTextParser.g:1236:1: ( ( rule__Numeric_Literal__Alternatives ) )
            // InternalStructuredTextParser.g:1237:1: ( rule__Numeric_Literal__Alternatives )
            {
             before(grammarAccess.getNumeric_LiteralAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:1238:1: ( rule__Numeric_Literal__Alternatives )
            // InternalStructuredTextParser.g:1238:2: rule__Numeric_Literal__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Numeric_Literal__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNumeric_LiteralAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNumeric_Literal"


    // $ANTLR start "entryRuleInt_Literal"
    // InternalStructuredTextParser.g:1250:1: entryRuleInt_Literal : ruleInt_Literal EOF ;
    public final void entryRuleInt_Literal() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1251:1: ( ruleInt_Literal EOF )
            // InternalStructuredTextParser.g:1252:1: ruleInt_Literal EOF
            {
             before(grammarAccess.getInt_LiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleInt_Literal();

            state._fsp--;

             after(grammarAccess.getInt_LiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInt_Literal"


    // $ANTLR start "ruleInt_Literal"
    // InternalStructuredTextParser.g:1259:1: ruleInt_Literal : ( ( rule__Int_Literal__Group__0 ) ) ;
    public final void ruleInt_Literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1263:5: ( ( ( rule__Int_Literal__Group__0 ) ) )
            // InternalStructuredTextParser.g:1264:1: ( ( rule__Int_Literal__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1264:1: ( ( rule__Int_Literal__Group__0 ) )
            // InternalStructuredTextParser.g:1265:1: ( rule__Int_Literal__Group__0 )
            {
             before(grammarAccess.getInt_LiteralAccess().getGroup()); 
            // InternalStructuredTextParser.g:1266:1: ( rule__Int_Literal__Group__0 )
            // InternalStructuredTextParser.g:1266:2: rule__Int_Literal__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Int_Literal__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInt_LiteralAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInt_Literal"


    // $ANTLR start "entryRuleSigned_Int"
    // InternalStructuredTextParser.g:1278:1: entryRuleSigned_Int : ruleSigned_Int EOF ;
    public final void entryRuleSigned_Int() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1279:1: ( ruleSigned_Int EOF )
            // InternalStructuredTextParser.g:1280:1: ruleSigned_Int EOF
            {
             before(grammarAccess.getSigned_IntRule()); 
            pushFollow(FOLLOW_1);
            ruleSigned_Int();

            state._fsp--;

             after(grammarAccess.getSigned_IntRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSigned_Int"


    // $ANTLR start "ruleSigned_Int"
    // InternalStructuredTextParser.g:1287:1: ruleSigned_Int : ( ( rule__Signed_Int__Group__0 ) ) ;
    public final void ruleSigned_Int() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1291:5: ( ( ( rule__Signed_Int__Group__0 ) ) )
            // InternalStructuredTextParser.g:1292:1: ( ( rule__Signed_Int__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1292:1: ( ( rule__Signed_Int__Group__0 ) )
            // InternalStructuredTextParser.g:1293:1: ( rule__Signed_Int__Group__0 )
            {
             before(grammarAccess.getSigned_IntAccess().getGroup()); 
            // InternalStructuredTextParser.g:1294:1: ( rule__Signed_Int__Group__0 )
            // InternalStructuredTextParser.g:1294:2: rule__Signed_Int__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Signed_Int__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSigned_IntAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSigned_Int"


    // $ANTLR start "entryRuleReal_Literal"
    // InternalStructuredTextParser.g:1306:1: entryRuleReal_Literal : ruleReal_Literal EOF ;
    public final void entryRuleReal_Literal() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1307:1: ( ruleReal_Literal EOF )
            // InternalStructuredTextParser.g:1308:1: ruleReal_Literal EOF
            {
             before(grammarAccess.getReal_LiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleReal_Literal();

            state._fsp--;

             after(grammarAccess.getReal_LiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReal_Literal"


    // $ANTLR start "ruleReal_Literal"
    // InternalStructuredTextParser.g:1315:1: ruleReal_Literal : ( ( rule__Real_Literal__Group__0 ) ) ;
    public final void ruleReal_Literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1319:5: ( ( ( rule__Real_Literal__Group__0 ) ) )
            // InternalStructuredTextParser.g:1320:1: ( ( rule__Real_Literal__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1320:1: ( ( rule__Real_Literal__Group__0 ) )
            // InternalStructuredTextParser.g:1321:1: ( rule__Real_Literal__Group__0 )
            {
             before(grammarAccess.getReal_LiteralAccess().getGroup()); 
            // InternalStructuredTextParser.g:1322:1: ( rule__Real_Literal__Group__0 )
            // InternalStructuredTextParser.g:1322:2: rule__Real_Literal__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Real_Literal__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReal_LiteralAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReal_Literal"


    // $ANTLR start "entryRuleReal_Value"
    // InternalStructuredTextParser.g:1334:1: entryRuleReal_Value : ruleReal_Value EOF ;
    public final void entryRuleReal_Value() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1335:1: ( ruleReal_Value EOF )
            // InternalStructuredTextParser.g:1336:1: ruleReal_Value EOF
            {
             before(grammarAccess.getReal_ValueRule()); 
            pushFollow(FOLLOW_1);
            ruleReal_Value();

            state._fsp--;

             after(grammarAccess.getReal_ValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReal_Value"


    // $ANTLR start "ruleReal_Value"
    // InternalStructuredTextParser.g:1343:1: ruleReal_Value : ( ( rule__Real_Value__Group__0 ) ) ;
    public final void ruleReal_Value() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1347:5: ( ( ( rule__Real_Value__Group__0 ) ) )
            // InternalStructuredTextParser.g:1348:1: ( ( rule__Real_Value__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1348:1: ( ( rule__Real_Value__Group__0 ) )
            // InternalStructuredTextParser.g:1349:1: ( rule__Real_Value__Group__0 )
            {
             before(grammarAccess.getReal_ValueAccess().getGroup()); 
            // InternalStructuredTextParser.g:1350:1: ( rule__Real_Value__Group__0 )
            // InternalStructuredTextParser.g:1350:2: rule__Real_Value__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Real_Value__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReal_ValueAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReal_Value"


    // $ANTLR start "entryRuleBool_Literal"
    // InternalStructuredTextParser.g:1362:1: entryRuleBool_Literal : ruleBool_Literal EOF ;
    public final void entryRuleBool_Literal() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1363:1: ( ruleBool_Literal EOF )
            // InternalStructuredTextParser.g:1364:1: ruleBool_Literal EOF
            {
             before(grammarAccess.getBool_LiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleBool_Literal();

            state._fsp--;

             after(grammarAccess.getBool_LiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBool_Literal"


    // $ANTLR start "ruleBool_Literal"
    // InternalStructuredTextParser.g:1371:1: ruleBool_Literal : ( ( rule__Bool_Literal__Group__0 ) ) ;
    public final void ruleBool_Literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1375:5: ( ( ( rule__Bool_Literal__Group__0 ) ) )
            // InternalStructuredTextParser.g:1376:1: ( ( rule__Bool_Literal__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1376:1: ( ( rule__Bool_Literal__Group__0 ) )
            // InternalStructuredTextParser.g:1377:1: ( rule__Bool_Literal__Group__0 )
            {
             before(grammarAccess.getBool_LiteralAccess().getGroup()); 
            // InternalStructuredTextParser.g:1378:1: ( rule__Bool_Literal__Group__0 )
            // InternalStructuredTextParser.g:1378:2: rule__Bool_Literal__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Bool_Literal__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBool_LiteralAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBool_Literal"


    // $ANTLR start "entryRuleBool_Value"
    // InternalStructuredTextParser.g:1390:1: entryRuleBool_Value : ruleBool_Value EOF ;
    public final void entryRuleBool_Value() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1391:1: ( ruleBool_Value EOF )
            // InternalStructuredTextParser.g:1392:1: ruleBool_Value EOF
            {
             before(grammarAccess.getBool_ValueRule()); 
            pushFollow(FOLLOW_1);
            ruleBool_Value();

            state._fsp--;

             after(grammarAccess.getBool_ValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBool_Value"


    // $ANTLR start "ruleBool_Value"
    // InternalStructuredTextParser.g:1399:1: ruleBool_Value : ( ( rule__Bool_Value__Alternatives ) ) ;
    public final void ruleBool_Value() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1403:5: ( ( ( rule__Bool_Value__Alternatives ) ) )
            // InternalStructuredTextParser.g:1404:1: ( ( rule__Bool_Value__Alternatives ) )
            {
            // InternalStructuredTextParser.g:1404:1: ( ( rule__Bool_Value__Alternatives ) )
            // InternalStructuredTextParser.g:1405:1: ( rule__Bool_Value__Alternatives )
            {
             before(grammarAccess.getBool_ValueAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:1406:1: ( rule__Bool_Value__Alternatives )
            // InternalStructuredTextParser.g:1406:2: rule__Bool_Value__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Bool_Value__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBool_ValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBool_Value"


    // $ANTLR start "entryRuleChar_Literal"
    // InternalStructuredTextParser.g:1418:1: entryRuleChar_Literal : ruleChar_Literal EOF ;
    public final void entryRuleChar_Literal() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1419:1: ( ruleChar_Literal EOF )
            // InternalStructuredTextParser.g:1420:1: ruleChar_Literal EOF
            {
             before(grammarAccess.getChar_LiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleChar_Literal();

            state._fsp--;

             after(grammarAccess.getChar_LiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleChar_Literal"


    // $ANTLR start "ruleChar_Literal"
    // InternalStructuredTextParser.g:1427:1: ruleChar_Literal : ( ( rule__Char_Literal__Group__0 ) ) ;
    public final void ruleChar_Literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1431:5: ( ( ( rule__Char_Literal__Group__0 ) ) )
            // InternalStructuredTextParser.g:1432:1: ( ( rule__Char_Literal__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1432:1: ( ( rule__Char_Literal__Group__0 ) )
            // InternalStructuredTextParser.g:1433:1: ( rule__Char_Literal__Group__0 )
            {
             before(grammarAccess.getChar_LiteralAccess().getGroup()); 
            // InternalStructuredTextParser.g:1434:1: ( rule__Char_Literal__Group__0 )
            // InternalStructuredTextParser.g:1434:2: rule__Char_Literal__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Char_Literal__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getChar_LiteralAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleChar_Literal"


    // $ANTLR start "entryRuleChar_Str"
    // InternalStructuredTextParser.g:1446:1: entryRuleChar_Str : ruleChar_Str EOF ;
    public final void entryRuleChar_Str() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1447:1: ( ruleChar_Str EOF )
            // InternalStructuredTextParser.g:1448:1: ruleChar_Str EOF
            {
             before(grammarAccess.getChar_StrRule()); 
            pushFollow(FOLLOW_1);
            ruleChar_Str();

            state._fsp--;

             after(grammarAccess.getChar_StrRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleChar_Str"


    // $ANTLR start "ruleChar_Str"
    // InternalStructuredTextParser.g:1455:1: ruleChar_Str : ( ( rule__Char_Str__Alternatives ) ) ;
    public final void ruleChar_Str() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1459:5: ( ( ( rule__Char_Str__Alternatives ) ) )
            // InternalStructuredTextParser.g:1460:1: ( ( rule__Char_Str__Alternatives ) )
            {
            // InternalStructuredTextParser.g:1460:1: ( ( rule__Char_Str__Alternatives ) )
            // InternalStructuredTextParser.g:1461:1: ( rule__Char_Str__Alternatives )
            {
             before(grammarAccess.getChar_StrAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:1462:1: ( rule__Char_Str__Alternatives )
            // InternalStructuredTextParser.g:1462:2: rule__Char_Str__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Char_Str__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getChar_StrAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleChar_Str"


    // $ANTLR start "entryRuleTime_Literal"
    // InternalStructuredTextParser.g:1474:1: entryRuleTime_Literal : ruleTime_Literal EOF ;
    public final void entryRuleTime_Literal() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1475:1: ( ruleTime_Literal EOF )
            // InternalStructuredTextParser.g:1476:1: ruleTime_Literal EOF
            {
             before(grammarAccess.getTime_LiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleTime_Literal();

            state._fsp--;

             after(grammarAccess.getTime_LiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTime_Literal"


    // $ANTLR start "ruleTime_Literal"
    // InternalStructuredTextParser.g:1483:1: ruleTime_Literal : ( ( rule__Time_Literal__Alternatives ) ) ;
    public final void ruleTime_Literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1487:5: ( ( ( rule__Time_Literal__Alternatives ) ) )
            // InternalStructuredTextParser.g:1488:1: ( ( rule__Time_Literal__Alternatives ) )
            {
            // InternalStructuredTextParser.g:1488:1: ( ( rule__Time_Literal__Alternatives ) )
            // InternalStructuredTextParser.g:1489:1: ( rule__Time_Literal__Alternatives )
            {
             before(grammarAccess.getTime_LiteralAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:1490:1: ( rule__Time_Literal__Alternatives )
            // InternalStructuredTextParser.g:1490:2: rule__Time_Literal__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Time_Literal__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTime_LiteralAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTime_Literal"


    // $ANTLR start "entryRuleDuration"
    // InternalStructuredTextParser.g:1502:1: entryRuleDuration : ruleDuration EOF ;
    public final void entryRuleDuration() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1503:1: ( ruleDuration EOF )
            // InternalStructuredTextParser.g:1504:1: ruleDuration EOF
            {
             before(grammarAccess.getDurationRule()); 
            pushFollow(FOLLOW_1);
            ruleDuration();

            state._fsp--;

             after(grammarAccess.getDurationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDuration"


    // $ANTLR start "ruleDuration"
    // InternalStructuredTextParser.g:1511:1: ruleDuration : ( ( rule__Duration__Group__0 ) ) ;
    public final void ruleDuration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1515:5: ( ( ( rule__Duration__Group__0 ) ) )
            // InternalStructuredTextParser.g:1516:1: ( ( rule__Duration__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1516:1: ( ( rule__Duration__Group__0 ) )
            // InternalStructuredTextParser.g:1517:1: ( rule__Duration__Group__0 )
            {
             before(grammarAccess.getDurationAccess().getGroup()); 
            // InternalStructuredTextParser.g:1518:1: ( rule__Duration__Group__0 )
            // InternalStructuredTextParser.g:1518:2: rule__Duration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Duration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDurationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDuration"


    // $ANTLR start "entryRuleDuration_Value"
    // InternalStructuredTextParser.g:1530:1: entryRuleDuration_Value : ruleDuration_Value EOF ;
    public final void entryRuleDuration_Value() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1531:1: ( ruleDuration_Value EOF )
            // InternalStructuredTextParser.g:1532:1: ruleDuration_Value EOF
            {
             before(grammarAccess.getDuration_ValueRule()); 
            pushFollow(FOLLOW_1);
            ruleDuration_Value();

            state._fsp--;

             after(grammarAccess.getDuration_ValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDuration_Value"


    // $ANTLR start "ruleDuration_Value"
    // InternalStructuredTextParser.g:1539:1: ruleDuration_Value : ( ( rule__Duration_Value__Group__0 ) ) ;
    public final void ruleDuration_Value() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1543:5: ( ( ( rule__Duration_Value__Group__0 ) ) )
            // InternalStructuredTextParser.g:1544:1: ( ( rule__Duration_Value__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1544:1: ( ( rule__Duration_Value__Group__0 ) )
            // InternalStructuredTextParser.g:1545:1: ( rule__Duration_Value__Group__0 )
            {
             before(grammarAccess.getDuration_ValueAccess().getGroup()); 
            // InternalStructuredTextParser.g:1546:1: ( rule__Duration_Value__Group__0 )
            // InternalStructuredTextParser.g:1546:2: rule__Duration_Value__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Duration_Value__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDuration_ValueAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDuration_Value"


    // $ANTLR start "entryRuleFix_Point"
    // InternalStructuredTextParser.g:1558:1: entryRuleFix_Point : ruleFix_Point EOF ;
    public final void entryRuleFix_Point() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1559:1: ( ruleFix_Point EOF )
            // InternalStructuredTextParser.g:1560:1: ruleFix_Point EOF
            {
             before(grammarAccess.getFix_PointRule()); 
            pushFollow(FOLLOW_1);
            ruleFix_Point();

            state._fsp--;

             after(grammarAccess.getFix_PointRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFix_Point"


    // $ANTLR start "ruleFix_Point"
    // InternalStructuredTextParser.g:1567:1: ruleFix_Point : ( ( rule__Fix_Point__Group__0 ) ) ;
    public final void ruleFix_Point() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1571:5: ( ( ( rule__Fix_Point__Group__0 ) ) )
            // InternalStructuredTextParser.g:1572:1: ( ( rule__Fix_Point__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1572:1: ( ( rule__Fix_Point__Group__0 ) )
            // InternalStructuredTextParser.g:1573:1: ( rule__Fix_Point__Group__0 )
            {
             before(grammarAccess.getFix_PointAccess().getGroup()); 
            // InternalStructuredTextParser.g:1574:1: ( rule__Fix_Point__Group__0 )
            // InternalStructuredTextParser.g:1574:2: rule__Fix_Point__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Fix_Point__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFix_PointAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFix_Point"


    // $ANTLR start "entryRuleTime_Of_Day"
    // InternalStructuredTextParser.g:1586:1: entryRuleTime_Of_Day : ruleTime_Of_Day EOF ;
    public final void entryRuleTime_Of_Day() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1587:1: ( ruleTime_Of_Day EOF )
            // InternalStructuredTextParser.g:1588:1: ruleTime_Of_Day EOF
            {
             before(grammarAccess.getTime_Of_DayRule()); 
            pushFollow(FOLLOW_1);
            ruleTime_Of_Day();

            state._fsp--;

             after(grammarAccess.getTime_Of_DayRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTime_Of_Day"


    // $ANTLR start "ruleTime_Of_Day"
    // InternalStructuredTextParser.g:1595:1: ruleTime_Of_Day : ( ( rule__Time_Of_Day__Group__0 ) ) ;
    public final void ruleTime_Of_Day() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1599:5: ( ( ( rule__Time_Of_Day__Group__0 ) ) )
            // InternalStructuredTextParser.g:1600:1: ( ( rule__Time_Of_Day__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1600:1: ( ( rule__Time_Of_Day__Group__0 ) )
            // InternalStructuredTextParser.g:1601:1: ( rule__Time_Of_Day__Group__0 )
            {
             before(grammarAccess.getTime_Of_DayAccess().getGroup()); 
            // InternalStructuredTextParser.g:1602:1: ( rule__Time_Of_Day__Group__0 )
            // InternalStructuredTextParser.g:1602:2: rule__Time_Of_Day__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Time_Of_Day__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTime_Of_DayAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTime_Of_Day"


    // $ANTLR start "entryRuleDaytime"
    // InternalStructuredTextParser.g:1614:1: entryRuleDaytime : ruleDaytime EOF ;
    public final void entryRuleDaytime() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1615:1: ( ruleDaytime EOF )
            // InternalStructuredTextParser.g:1616:1: ruleDaytime EOF
            {
             before(grammarAccess.getDaytimeRule()); 
            pushFollow(FOLLOW_1);
            ruleDaytime();

            state._fsp--;

             after(grammarAccess.getDaytimeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDaytime"


    // $ANTLR start "ruleDaytime"
    // InternalStructuredTextParser.g:1623:1: ruleDaytime : ( ( rule__Daytime__Group__0 ) ) ;
    public final void ruleDaytime() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1627:5: ( ( ( rule__Daytime__Group__0 ) ) )
            // InternalStructuredTextParser.g:1628:1: ( ( rule__Daytime__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1628:1: ( ( rule__Daytime__Group__0 ) )
            // InternalStructuredTextParser.g:1629:1: ( rule__Daytime__Group__0 )
            {
             before(grammarAccess.getDaytimeAccess().getGroup()); 
            // InternalStructuredTextParser.g:1630:1: ( rule__Daytime__Group__0 )
            // InternalStructuredTextParser.g:1630:2: rule__Daytime__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Daytime__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDaytimeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDaytime"


    // $ANTLR start "entryRuleDay_Hour"
    // InternalStructuredTextParser.g:1642:1: entryRuleDay_Hour : ruleDay_Hour EOF ;
    public final void entryRuleDay_Hour() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1643:1: ( ruleDay_Hour EOF )
            // InternalStructuredTextParser.g:1644:1: ruleDay_Hour EOF
            {
             before(grammarAccess.getDay_HourRule()); 
            pushFollow(FOLLOW_1);
            ruleDay_Hour();

            state._fsp--;

             after(grammarAccess.getDay_HourRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDay_Hour"


    // $ANTLR start "ruleDay_Hour"
    // InternalStructuredTextParser.g:1651:1: ruleDay_Hour : ( RULE_UNSIGNED_INT ) ;
    public final void ruleDay_Hour() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1655:5: ( ( RULE_UNSIGNED_INT ) )
            // InternalStructuredTextParser.g:1656:1: ( RULE_UNSIGNED_INT )
            {
            // InternalStructuredTextParser.g:1656:1: ( RULE_UNSIGNED_INT )
            // InternalStructuredTextParser.g:1657:1: RULE_UNSIGNED_INT
            {
             before(grammarAccess.getDay_HourAccess().getUNSIGNED_INTTerminalRuleCall()); 
            match(input,RULE_UNSIGNED_INT,FOLLOW_2); 
             after(grammarAccess.getDay_HourAccess().getUNSIGNED_INTTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDay_Hour"


    // $ANTLR start "entryRuleDay_Minute"
    // InternalStructuredTextParser.g:1670:1: entryRuleDay_Minute : ruleDay_Minute EOF ;
    public final void entryRuleDay_Minute() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1671:1: ( ruleDay_Minute EOF )
            // InternalStructuredTextParser.g:1672:1: ruleDay_Minute EOF
            {
             before(grammarAccess.getDay_MinuteRule()); 
            pushFollow(FOLLOW_1);
            ruleDay_Minute();

            state._fsp--;

             after(grammarAccess.getDay_MinuteRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDay_Minute"


    // $ANTLR start "ruleDay_Minute"
    // InternalStructuredTextParser.g:1679:1: ruleDay_Minute : ( RULE_UNSIGNED_INT ) ;
    public final void ruleDay_Minute() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1683:5: ( ( RULE_UNSIGNED_INT ) )
            // InternalStructuredTextParser.g:1684:1: ( RULE_UNSIGNED_INT )
            {
            // InternalStructuredTextParser.g:1684:1: ( RULE_UNSIGNED_INT )
            // InternalStructuredTextParser.g:1685:1: RULE_UNSIGNED_INT
            {
             before(grammarAccess.getDay_MinuteAccess().getUNSIGNED_INTTerminalRuleCall()); 
            match(input,RULE_UNSIGNED_INT,FOLLOW_2); 
             after(grammarAccess.getDay_MinuteAccess().getUNSIGNED_INTTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDay_Minute"


    // $ANTLR start "entryRuleDay_Second"
    // InternalStructuredTextParser.g:1698:1: entryRuleDay_Second : ruleDay_Second EOF ;
    public final void entryRuleDay_Second() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1699:1: ( ruleDay_Second EOF )
            // InternalStructuredTextParser.g:1700:1: ruleDay_Second EOF
            {
             before(grammarAccess.getDay_SecondRule()); 
            pushFollow(FOLLOW_1);
            ruleDay_Second();

            state._fsp--;

             after(grammarAccess.getDay_SecondRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDay_Second"


    // $ANTLR start "ruleDay_Second"
    // InternalStructuredTextParser.g:1707:1: ruleDay_Second : ( ruleFix_Point ) ;
    public final void ruleDay_Second() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1711:5: ( ( ruleFix_Point ) )
            // InternalStructuredTextParser.g:1712:1: ( ruleFix_Point )
            {
            // InternalStructuredTextParser.g:1712:1: ( ruleFix_Point )
            // InternalStructuredTextParser.g:1713:1: ruleFix_Point
            {
             before(grammarAccess.getDay_SecondAccess().getFix_PointParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleFix_Point();

            state._fsp--;

             after(grammarAccess.getDay_SecondAccess().getFix_PointParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDay_Second"


    // $ANTLR start "entryRuleDate"
    // InternalStructuredTextParser.g:1726:1: entryRuleDate : ruleDate EOF ;
    public final void entryRuleDate() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1727:1: ( ruleDate EOF )
            // InternalStructuredTextParser.g:1728:1: ruleDate EOF
            {
             before(grammarAccess.getDateRule()); 
            pushFollow(FOLLOW_1);
            ruleDate();

            state._fsp--;

             after(grammarAccess.getDateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDate"


    // $ANTLR start "ruleDate"
    // InternalStructuredTextParser.g:1735:1: ruleDate : ( ( rule__Date__Group__0 ) ) ;
    public final void ruleDate() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1739:5: ( ( ( rule__Date__Group__0 ) ) )
            // InternalStructuredTextParser.g:1740:1: ( ( rule__Date__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1740:1: ( ( rule__Date__Group__0 ) )
            // InternalStructuredTextParser.g:1741:1: ( rule__Date__Group__0 )
            {
             before(grammarAccess.getDateAccess().getGroup()); 
            // InternalStructuredTextParser.g:1742:1: ( rule__Date__Group__0 )
            // InternalStructuredTextParser.g:1742:2: rule__Date__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Date__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDate"


    // $ANTLR start "entryRuleDate_Literal"
    // InternalStructuredTextParser.g:1754:1: entryRuleDate_Literal : ruleDate_Literal EOF ;
    public final void entryRuleDate_Literal() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1755:1: ( ruleDate_Literal EOF )
            // InternalStructuredTextParser.g:1756:1: ruleDate_Literal EOF
            {
             before(grammarAccess.getDate_LiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleDate_Literal();

            state._fsp--;

             after(grammarAccess.getDate_LiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDate_Literal"


    // $ANTLR start "ruleDate_Literal"
    // InternalStructuredTextParser.g:1763:1: ruleDate_Literal : ( ( rule__Date_Literal__Group__0 ) ) ;
    public final void ruleDate_Literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1767:5: ( ( ( rule__Date_Literal__Group__0 ) ) )
            // InternalStructuredTextParser.g:1768:1: ( ( rule__Date_Literal__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1768:1: ( ( rule__Date_Literal__Group__0 ) )
            // InternalStructuredTextParser.g:1769:1: ( rule__Date_Literal__Group__0 )
            {
             before(grammarAccess.getDate_LiteralAccess().getGroup()); 
            // InternalStructuredTextParser.g:1770:1: ( rule__Date_Literal__Group__0 )
            // InternalStructuredTextParser.g:1770:2: rule__Date_Literal__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Date_Literal__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDate_LiteralAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDate_Literal"


    // $ANTLR start "entryRuleYear"
    // InternalStructuredTextParser.g:1782:1: entryRuleYear : ruleYear EOF ;
    public final void entryRuleYear() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1783:1: ( ruleYear EOF )
            // InternalStructuredTextParser.g:1784:1: ruleYear EOF
            {
             before(grammarAccess.getYearRule()); 
            pushFollow(FOLLOW_1);
            ruleYear();

            state._fsp--;

             after(grammarAccess.getYearRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleYear"


    // $ANTLR start "ruleYear"
    // InternalStructuredTextParser.g:1791:1: ruleYear : ( RULE_UNSIGNED_INT ) ;
    public final void ruleYear() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1795:5: ( ( RULE_UNSIGNED_INT ) )
            // InternalStructuredTextParser.g:1796:1: ( RULE_UNSIGNED_INT )
            {
            // InternalStructuredTextParser.g:1796:1: ( RULE_UNSIGNED_INT )
            // InternalStructuredTextParser.g:1797:1: RULE_UNSIGNED_INT
            {
             before(grammarAccess.getYearAccess().getUNSIGNED_INTTerminalRuleCall()); 
            match(input,RULE_UNSIGNED_INT,FOLLOW_2); 
             after(grammarAccess.getYearAccess().getUNSIGNED_INTTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleYear"


    // $ANTLR start "entryRuleMonth"
    // InternalStructuredTextParser.g:1810:1: entryRuleMonth : ruleMonth EOF ;
    public final void entryRuleMonth() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1811:1: ( ruleMonth EOF )
            // InternalStructuredTextParser.g:1812:1: ruleMonth EOF
            {
             before(grammarAccess.getMonthRule()); 
            pushFollow(FOLLOW_1);
            ruleMonth();

            state._fsp--;

             after(grammarAccess.getMonthRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMonth"


    // $ANTLR start "ruleMonth"
    // InternalStructuredTextParser.g:1819:1: ruleMonth : ( RULE_UNSIGNED_INT ) ;
    public final void ruleMonth() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1823:5: ( ( RULE_UNSIGNED_INT ) )
            // InternalStructuredTextParser.g:1824:1: ( RULE_UNSIGNED_INT )
            {
            // InternalStructuredTextParser.g:1824:1: ( RULE_UNSIGNED_INT )
            // InternalStructuredTextParser.g:1825:1: RULE_UNSIGNED_INT
            {
             before(grammarAccess.getMonthAccess().getUNSIGNED_INTTerminalRuleCall()); 
            match(input,RULE_UNSIGNED_INT,FOLLOW_2); 
             after(grammarAccess.getMonthAccess().getUNSIGNED_INTTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMonth"


    // $ANTLR start "entryRuleDay"
    // InternalStructuredTextParser.g:1838:1: entryRuleDay : ruleDay EOF ;
    public final void entryRuleDay() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1839:1: ( ruleDay EOF )
            // InternalStructuredTextParser.g:1840:1: ruleDay EOF
            {
             before(grammarAccess.getDayRule()); 
            pushFollow(FOLLOW_1);
            ruleDay();

            state._fsp--;

             after(grammarAccess.getDayRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDay"


    // $ANTLR start "ruleDay"
    // InternalStructuredTextParser.g:1847:1: ruleDay : ( RULE_UNSIGNED_INT ) ;
    public final void ruleDay() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1851:5: ( ( RULE_UNSIGNED_INT ) )
            // InternalStructuredTextParser.g:1852:1: ( RULE_UNSIGNED_INT )
            {
            // InternalStructuredTextParser.g:1852:1: ( RULE_UNSIGNED_INT )
            // InternalStructuredTextParser.g:1853:1: RULE_UNSIGNED_INT
            {
             before(grammarAccess.getDayAccess().getUNSIGNED_INTTerminalRuleCall()); 
            match(input,RULE_UNSIGNED_INT,FOLLOW_2); 
             after(grammarAccess.getDayAccess().getUNSIGNED_INTTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDay"


    // $ANTLR start "entryRuleDate_And_Time"
    // InternalStructuredTextParser.g:1866:1: entryRuleDate_And_Time : ruleDate_And_Time EOF ;
    public final void entryRuleDate_And_Time() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1867:1: ( ruleDate_And_Time EOF )
            // InternalStructuredTextParser.g:1868:1: ruleDate_And_Time EOF
            {
             before(grammarAccess.getDate_And_TimeRule()); 
            pushFollow(FOLLOW_1);
            ruleDate_And_Time();

            state._fsp--;

             after(grammarAccess.getDate_And_TimeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDate_And_Time"


    // $ANTLR start "ruleDate_And_Time"
    // InternalStructuredTextParser.g:1875:1: ruleDate_And_Time : ( ( rule__Date_And_Time__Group__0 ) ) ;
    public final void ruleDate_And_Time() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1879:5: ( ( ( rule__Date_And_Time__Group__0 ) ) )
            // InternalStructuredTextParser.g:1880:1: ( ( rule__Date_And_Time__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1880:1: ( ( rule__Date_And_Time__Group__0 ) )
            // InternalStructuredTextParser.g:1881:1: ( rule__Date_And_Time__Group__0 )
            {
             before(grammarAccess.getDate_And_TimeAccess().getGroup()); 
            // InternalStructuredTextParser.g:1882:1: ( rule__Date_And_Time__Group__0 )
            // InternalStructuredTextParser.g:1882:2: rule__Date_And_Time__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Date_And_Time__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDate_And_TimeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDate_And_Time"


    // $ANTLR start "entryRuleDate_And_Time_Value"
    // InternalStructuredTextParser.g:1894:1: entryRuleDate_And_Time_Value : ruleDate_And_Time_Value EOF ;
    public final void entryRuleDate_And_Time_Value() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1895:1: ( ruleDate_And_Time_Value EOF )
            // InternalStructuredTextParser.g:1896:1: ruleDate_And_Time_Value EOF
            {
             before(grammarAccess.getDate_And_Time_ValueRule()); 
            pushFollow(FOLLOW_1);
            ruleDate_And_Time_Value();

            state._fsp--;

             after(grammarAccess.getDate_And_Time_ValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDate_And_Time_Value"


    // $ANTLR start "ruleDate_And_Time_Value"
    // InternalStructuredTextParser.g:1903:1: ruleDate_And_Time_Value : ( ( rule__Date_And_Time_Value__Group__0 ) ) ;
    public final void ruleDate_And_Time_Value() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1907:5: ( ( ( rule__Date_And_Time_Value__Group__0 ) ) )
            // InternalStructuredTextParser.g:1908:1: ( ( rule__Date_And_Time_Value__Group__0 ) )
            {
            // InternalStructuredTextParser.g:1908:1: ( ( rule__Date_And_Time_Value__Group__0 ) )
            // InternalStructuredTextParser.g:1909:1: ( rule__Date_And_Time_Value__Group__0 )
            {
             before(grammarAccess.getDate_And_Time_ValueAccess().getGroup()); 
            // InternalStructuredTextParser.g:1910:1: ( rule__Date_And_Time_Value__Group__0 )
            // InternalStructuredTextParser.g:1910:2: rule__Date_And_Time_Value__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Date_And_Time_Value__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDate_And_Time_ValueAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDate_And_Time_Value"


    // $ANTLR start "entryRuleType_Name"
    // InternalStructuredTextParser.g:1922:1: entryRuleType_Name : ruleType_Name EOF ;
    public final void entryRuleType_Name() throws RecognitionException {
        try {
            // InternalStructuredTextParser.g:1923:1: ( ruleType_Name EOF )
            // InternalStructuredTextParser.g:1924:1: ruleType_Name EOF
            {
             before(grammarAccess.getType_NameRule()); 
            pushFollow(FOLLOW_1);
            ruleType_Name();

            state._fsp--;

             after(grammarAccess.getType_NameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType_Name"


    // $ANTLR start "ruleType_Name"
    // InternalStructuredTextParser.g:1931:1: ruleType_Name : ( ( rule__Type_Name__Alternatives ) ) ;
    public final void ruleType_Name() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1935:5: ( ( ( rule__Type_Name__Alternatives ) ) )
            // InternalStructuredTextParser.g:1936:1: ( ( rule__Type_Name__Alternatives ) )
            {
            // InternalStructuredTextParser.g:1936:1: ( ( rule__Type_Name__Alternatives ) )
            // InternalStructuredTextParser.g:1937:1: ( rule__Type_Name__Alternatives )
            {
             before(grammarAccess.getType_NameAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:1938:1: ( rule__Type_Name__Alternatives )
            // InternalStructuredTextParser.g:1938:2: rule__Type_Name__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Type_Name__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getType_NameAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType_Name"


    // $ANTLR start "ruleOr_Operator"
    // InternalStructuredTextParser.g:1951:1: ruleOr_Operator : ( ( OR ) ) ;
    public final void ruleOr_Operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1955:1: ( ( ( OR ) ) )
            // InternalStructuredTextParser.g:1956:1: ( ( OR ) )
            {
            // InternalStructuredTextParser.g:1956:1: ( ( OR ) )
            // InternalStructuredTextParser.g:1957:1: ( OR )
            {
             before(grammarAccess.getOr_OperatorAccess().getOREnumLiteralDeclaration()); 
            // InternalStructuredTextParser.g:1958:1: ( OR )
            // InternalStructuredTextParser.g:1958:3: OR
            {
            match(input,OR,FOLLOW_2); 

            }

             after(grammarAccess.getOr_OperatorAccess().getOREnumLiteralDeclaration()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr_Operator"


    // $ANTLR start "ruleXor_Operator"
    // InternalStructuredTextParser.g:1971:1: ruleXor_Operator : ( ( XOR ) ) ;
    public final void ruleXor_Operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1975:1: ( ( ( XOR ) ) )
            // InternalStructuredTextParser.g:1976:1: ( ( XOR ) )
            {
            // InternalStructuredTextParser.g:1976:1: ( ( XOR ) )
            // InternalStructuredTextParser.g:1977:1: ( XOR )
            {
             before(grammarAccess.getXor_OperatorAccess().getXOREnumLiteralDeclaration()); 
            // InternalStructuredTextParser.g:1978:1: ( XOR )
            // InternalStructuredTextParser.g:1978:3: XOR
            {
            match(input,XOR,FOLLOW_2); 

            }

             after(grammarAccess.getXor_OperatorAccess().getXOREnumLiteralDeclaration()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXor_Operator"


    // $ANTLR start "ruleAnd_Operator"
    // InternalStructuredTextParser.g:1991:1: ruleAnd_Operator : ( ( rule__And_Operator__Alternatives ) ) ;
    public final void ruleAnd_Operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:1995:1: ( ( ( rule__And_Operator__Alternatives ) ) )
            // InternalStructuredTextParser.g:1996:1: ( ( rule__And_Operator__Alternatives ) )
            {
            // InternalStructuredTextParser.g:1996:1: ( ( rule__And_Operator__Alternatives ) )
            // InternalStructuredTextParser.g:1997:1: ( rule__And_Operator__Alternatives )
            {
             before(grammarAccess.getAnd_OperatorAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:1998:1: ( rule__And_Operator__Alternatives )
            // InternalStructuredTextParser.g:1998:2: rule__And_Operator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__And_Operator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAnd_OperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd_Operator"


    // $ANTLR start "ruleCompare_Operator"
    // InternalStructuredTextParser.g:2010:1: ruleCompare_Operator : ( ( rule__Compare_Operator__Alternatives ) ) ;
    public final void ruleCompare_Operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2014:1: ( ( ( rule__Compare_Operator__Alternatives ) ) )
            // InternalStructuredTextParser.g:2015:1: ( ( rule__Compare_Operator__Alternatives ) )
            {
            // InternalStructuredTextParser.g:2015:1: ( ( rule__Compare_Operator__Alternatives ) )
            // InternalStructuredTextParser.g:2016:1: ( rule__Compare_Operator__Alternatives )
            {
             before(grammarAccess.getCompare_OperatorAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:2017:1: ( rule__Compare_Operator__Alternatives )
            // InternalStructuredTextParser.g:2017:2: rule__Compare_Operator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Compare_Operator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getCompare_OperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCompare_Operator"


    // $ANTLR start "ruleEqu_Operator"
    // InternalStructuredTextParser.g:2029:1: ruleEqu_Operator : ( ( rule__Equ_Operator__Alternatives ) ) ;
    public final void ruleEqu_Operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2033:1: ( ( ( rule__Equ_Operator__Alternatives ) ) )
            // InternalStructuredTextParser.g:2034:1: ( ( rule__Equ_Operator__Alternatives ) )
            {
            // InternalStructuredTextParser.g:2034:1: ( ( rule__Equ_Operator__Alternatives ) )
            // InternalStructuredTextParser.g:2035:1: ( rule__Equ_Operator__Alternatives )
            {
             before(grammarAccess.getEqu_OperatorAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:2036:1: ( rule__Equ_Operator__Alternatives )
            // InternalStructuredTextParser.g:2036:2: rule__Equ_Operator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Equ_Operator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEqu_OperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEqu_Operator"


    // $ANTLR start "ruleAdd_Operator"
    // InternalStructuredTextParser.g:2048:1: ruleAdd_Operator : ( ( rule__Add_Operator__Alternatives ) ) ;
    public final void ruleAdd_Operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2052:1: ( ( ( rule__Add_Operator__Alternatives ) ) )
            // InternalStructuredTextParser.g:2053:1: ( ( rule__Add_Operator__Alternatives ) )
            {
            // InternalStructuredTextParser.g:2053:1: ( ( rule__Add_Operator__Alternatives ) )
            // InternalStructuredTextParser.g:2054:1: ( rule__Add_Operator__Alternatives )
            {
             before(grammarAccess.getAdd_OperatorAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:2055:1: ( rule__Add_Operator__Alternatives )
            // InternalStructuredTextParser.g:2055:2: rule__Add_Operator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Add_Operator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAdd_OperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAdd_Operator"


    // $ANTLR start "ruleTerm_Operator"
    // InternalStructuredTextParser.g:2067:1: ruleTerm_Operator : ( ( rule__Term_Operator__Alternatives ) ) ;
    public final void ruleTerm_Operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2071:1: ( ( ( rule__Term_Operator__Alternatives ) ) )
            // InternalStructuredTextParser.g:2072:1: ( ( rule__Term_Operator__Alternatives ) )
            {
            // InternalStructuredTextParser.g:2072:1: ( ( rule__Term_Operator__Alternatives ) )
            // InternalStructuredTextParser.g:2073:1: ( rule__Term_Operator__Alternatives )
            {
             before(grammarAccess.getTerm_OperatorAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:2074:1: ( rule__Term_Operator__Alternatives )
            // InternalStructuredTextParser.g:2074:2: rule__Term_Operator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Term_Operator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTerm_OperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTerm_Operator"


    // $ANTLR start "rulePower_Operator"
    // InternalStructuredTextParser.g:2086:1: rulePower_Operator : ( ( AsteriskAsterisk ) ) ;
    public final void rulePower_Operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2090:1: ( ( ( AsteriskAsterisk ) ) )
            // InternalStructuredTextParser.g:2091:1: ( ( AsteriskAsterisk ) )
            {
            // InternalStructuredTextParser.g:2091:1: ( ( AsteriskAsterisk ) )
            // InternalStructuredTextParser.g:2092:1: ( AsteriskAsterisk )
            {
             before(grammarAccess.getPower_OperatorAccess().getPOWEREnumLiteralDeclaration()); 
            // InternalStructuredTextParser.g:2093:1: ( AsteriskAsterisk )
            // InternalStructuredTextParser.g:2093:3: AsteriskAsterisk
            {
            match(input,AsteriskAsterisk,FOLLOW_2); 

            }

             after(grammarAccess.getPower_OperatorAccess().getPOWEREnumLiteralDeclaration()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePower_Operator"


    // $ANTLR start "ruleUnary_Operator"
    // InternalStructuredTextParser.g:2106:1: ruleUnary_Operator : ( ( rule__Unary_Operator__Alternatives ) ) ;
    public final void ruleUnary_Operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2110:1: ( ( ( rule__Unary_Operator__Alternatives ) ) )
            // InternalStructuredTextParser.g:2111:1: ( ( rule__Unary_Operator__Alternatives ) )
            {
            // InternalStructuredTextParser.g:2111:1: ( ( rule__Unary_Operator__Alternatives ) )
            // InternalStructuredTextParser.g:2112:1: ( rule__Unary_Operator__Alternatives )
            {
             before(grammarAccess.getUnary_OperatorAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:2113:1: ( rule__Unary_Operator__Alternatives )
            // InternalStructuredTextParser.g:2113:2: rule__Unary_Operator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Unary_Operator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUnary_OperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnary_Operator"


    // $ANTLR start "ruleDuration_Unit"
    // InternalStructuredTextParser.g:2125:1: ruleDuration_Unit : ( ( rule__Duration_Unit__Alternatives ) ) ;
    public final void ruleDuration_Unit() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2129:1: ( ( ( rule__Duration_Unit__Alternatives ) ) )
            // InternalStructuredTextParser.g:2130:1: ( ( rule__Duration_Unit__Alternatives ) )
            {
            // InternalStructuredTextParser.g:2130:1: ( ( rule__Duration_Unit__Alternatives ) )
            // InternalStructuredTextParser.g:2131:1: ( rule__Duration_Unit__Alternatives )
            {
             before(grammarAccess.getDuration_UnitAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:2132:1: ( rule__Duration_Unit__Alternatives )
            // InternalStructuredTextParser.g:2132:2: rule__Duration_Unit__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Duration_Unit__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDuration_UnitAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDuration_Unit"


    // $ANTLR start "ruleInt_Type_Name"
    // InternalStructuredTextParser.g:2144:1: ruleInt_Type_Name : ( ( rule__Int_Type_Name__Alternatives ) ) ;
    public final void ruleInt_Type_Name() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2148:1: ( ( ( rule__Int_Type_Name__Alternatives ) ) )
            // InternalStructuredTextParser.g:2149:1: ( ( rule__Int_Type_Name__Alternatives ) )
            {
            // InternalStructuredTextParser.g:2149:1: ( ( rule__Int_Type_Name__Alternatives ) )
            // InternalStructuredTextParser.g:2150:1: ( rule__Int_Type_Name__Alternatives )
            {
             before(grammarAccess.getInt_Type_NameAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:2151:1: ( rule__Int_Type_Name__Alternatives )
            // InternalStructuredTextParser.g:2151:2: rule__Int_Type_Name__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Int_Type_Name__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getInt_Type_NameAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInt_Type_Name"


    // $ANTLR start "ruleReal_Type_Name"
    // InternalStructuredTextParser.g:2163:1: ruleReal_Type_Name : ( ( rule__Real_Type_Name__Alternatives ) ) ;
    public final void ruleReal_Type_Name() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2167:1: ( ( ( rule__Real_Type_Name__Alternatives ) ) )
            // InternalStructuredTextParser.g:2168:1: ( ( rule__Real_Type_Name__Alternatives ) )
            {
            // InternalStructuredTextParser.g:2168:1: ( ( rule__Real_Type_Name__Alternatives ) )
            // InternalStructuredTextParser.g:2169:1: ( rule__Real_Type_Name__Alternatives )
            {
             before(grammarAccess.getReal_Type_NameAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:2170:1: ( rule__Real_Type_Name__Alternatives )
            // InternalStructuredTextParser.g:2170:2: rule__Real_Type_Name__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Real_Type_Name__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getReal_Type_NameAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReal_Type_Name"


    // $ANTLR start "ruleString_Type_Name"
    // InternalStructuredTextParser.g:2182:1: ruleString_Type_Name : ( ( rule__String_Type_Name__Alternatives ) ) ;
    public final void ruleString_Type_Name() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2186:1: ( ( ( rule__String_Type_Name__Alternatives ) ) )
            // InternalStructuredTextParser.g:2187:1: ( ( rule__String_Type_Name__Alternatives ) )
            {
            // InternalStructuredTextParser.g:2187:1: ( ( rule__String_Type_Name__Alternatives ) )
            // InternalStructuredTextParser.g:2188:1: ( rule__String_Type_Name__Alternatives )
            {
             before(grammarAccess.getString_Type_NameAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:2189:1: ( rule__String_Type_Name__Alternatives )
            // InternalStructuredTextParser.g:2189:2: rule__String_Type_Name__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__String_Type_Name__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getString_Type_NameAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleString_Type_Name"


    // $ANTLR start "ruleTime_Type_Name"
    // InternalStructuredTextParser.g:2201:1: ruleTime_Type_Name : ( ( rule__Time_Type_Name__Alternatives ) ) ;
    public final void ruleTime_Type_Name() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2205:1: ( ( ( rule__Time_Type_Name__Alternatives ) ) )
            // InternalStructuredTextParser.g:2206:1: ( ( rule__Time_Type_Name__Alternatives ) )
            {
            // InternalStructuredTextParser.g:2206:1: ( ( rule__Time_Type_Name__Alternatives ) )
            // InternalStructuredTextParser.g:2207:1: ( rule__Time_Type_Name__Alternatives )
            {
             before(grammarAccess.getTime_Type_NameAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:2208:1: ( rule__Time_Type_Name__Alternatives )
            // InternalStructuredTextParser.g:2208:2: rule__Time_Type_Name__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Time_Type_Name__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTime_Type_NameAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTime_Type_Name"


    // $ANTLR start "ruleTod_Type_Name"
    // InternalStructuredTextParser.g:2220:1: ruleTod_Type_Name : ( ( rule__Tod_Type_Name__Alternatives ) ) ;
    public final void ruleTod_Type_Name() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2224:1: ( ( ( rule__Tod_Type_Name__Alternatives ) ) )
            // InternalStructuredTextParser.g:2225:1: ( ( rule__Tod_Type_Name__Alternatives ) )
            {
            // InternalStructuredTextParser.g:2225:1: ( ( rule__Tod_Type_Name__Alternatives ) )
            // InternalStructuredTextParser.g:2226:1: ( rule__Tod_Type_Name__Alternatives )
            {
             before(grammarAccess.getTod_Type_NameAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:2227:1: ( rule__Tod_Type_Name__Alternatives )
            // InternalStructuredTextParser.g:2227:2: rule__Tod_Type_Name__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Tod_Type_Name__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTod_Type_NameAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTod_Type_Name"


    // $ANTLR start "ruleDate_Type_Name"
    // InternalStructuredTextParser.g:2239:1: ruleDate_Type_Name : ( ( rule__Date_Type_Name__Alternatives ) ) ;
    public final void ruleDate_Type_Name() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2243:1: ( ( ( rule__Date_Type_Name__Alternatives ) ) )
            // InternalStructuredTextParser.g:2244:1: ( ( rule__Date_Type_Name__Alternatives ) )
            {
            // InternalStructuredTextParser.g:2244:1: ( ( rule__Date_Type_Name__Alternatives ) )
            // InternalStructuredTextParser.g:2245:1: ( rule__Date_Type_Name__Alternatives )
            {
             before(grammarAccess.getDate_Type_NameAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:2246:1: ( rule__Date_Type_Name__Alternatives )
            // InternalStructuredTextParser.g:2246:2: rule__Date_Type_Name__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Date_Type_Name__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDate_Type_NameAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDate_Type_Name"


    // $ANTLR start "ruleDT_Type_Name"
    // InternalStructuredTextParser.g:2258:1: ruleDT_Type_Name : ( ( rule__DT_Type_Name__Alternatives ) ) ;
    public final void ruleDT_Type_Name() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2262:1: ( ( ( rule__DT_Type_Name__Alternatives ) ) )
            // InternalStructuredTextParser.g:2263:1: ( ( rule__DT_Type_Name__Alternatives ) )
            {
            // InternalStructuredTextParser.g:2263:1: ( ( rule__DT_Type_Name__Alternatives ) )
            // InternalStructuredTextParser.g:2264:1: ( rule__DT_Type_Name__Alternatives )
            {
             before(grammarAccess.getDT_Type_NameAccess().getAlternatives()); 
            // InternalStructuredTextParser.g:2265:1: ( rule__DT_Type_Name__Alternatives )
            // InternalStructuredTextParser.g:2265:2: rule__DT_Type_Name__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__DT_Type_Name__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDT_Type_NameAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDT_Type_Name"


    // $ANTLR start "ruleBool_Type_Name"
    // InternalStructuredTextParser.g:2277:1: ruleBool_Type_Name : ( ( BOOL ) ) ;
    public final void ruleBool_Type_Name() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2281:1: ( ( ( BOOL ) ) )
            // InternalStructuredTextParser.g:2282:1: ( ( BOOL ) )
            {
            // InternalStructuredTextParser.g:2282:1: ( ( BOOL ) )
            // InternalStructuredTextParser.g:2283:1: ( BOOL )
            {
             before(grammarAccess.getBool_Type_NameAccess().getBOOLEnumLiteralDeclaration()); 
            // InternalStructuredTextParser.g:2284:1: ( BOOL )
            // InternalStructuredTextParser.g:2284:3: BOOL
            {
            match(input,BOOL,FOLLOW_2); 

            }

             after(grammarAccess.getBool_Type_NameAccess().getBOOLEnumLiteralDeclaration()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBool_Type_Name"


    // $ANTLR start "rule__Stmt__Alternatives"
    // InternalStructuredTextParser.g:2298:1: rule__Stmt__Alternatives : ( ( ruleAssign_Stmt ) | ( ruleSubprog_Ctrl_Stmt ) | ( ruleSelection_Stmt ) | ( ruleIteration_Stmt ) );
    public final void rule__Stmt__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2302:1: ( ( ruleAssign_Stmt ) | ( ruleSubprog_Ctrl_Stmt ) | ( ruleSelection_Stmt ) | ( ruleIteration_Stmt ) )
            int alt1=4;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                int LA1_1 = input.LA(2);

                if ( (LA1_1==LeftParenthesis) ) {
                    alt1=2;
                }
                else if ( (LA1_1==ColonEqualsSign||LA1_1==FullStop||LA1_1==LeftSquareBracket) ) {
                    alt1=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;
                }
                }
                break;
            case DT:
            case LT:
            case T:
                {
                alt1=1;
                }
                break;
            case RETURN:
            case SUPER:
            case TIME:
                {
                alt1=2;
                }
                break;
            case CASE:
            case IF:
                {
                alt1=3;
                }
                break;
            case CONTINUE:
            case REPEAT:
            case WHILE:
            case EXIT:
            case FOR:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalStructuredTextParser.g:2303:1: ( ruleAssign_Stmt )
                    {
                    // InternalStructuredTextParser.g:2303:1: ( ruleAssign_Stmt )
                    // InternalStructuredTextParser.g:2304:1: ruleAssign_Stmt
                    {
                     before(grammarAccess.getStmtAccess().getAssign_StmtParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleAssign_Stmt();

                    state._fsp--;

                     after(grammarAccess.getStmtAccess().getAssign_StmtParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2309:6: ( ruleSubprog_Ctrl_Stmt )
                    {
                    // InternalStructuredTextParser.g:2309:6: ( ruleSubprog_Ctrl_Stmt )
                    // InternalStructuredTextParser.g:2310:1: ruleSubprog_Ctrl_Stmt
                    {
                     before(grammarAccess.getStmtAccess().getSubprog_Ctrl_StmtParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleSubprog_Ctrl_Stmt();

                    state._fsp--;

                     after(grammarAccess.getStmtAccess().getSubprog_Ctrl_StmtParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:2315:6: ( ruleSelection_Stmt )
                    {
                    // InternalStructuredTextParser.g:2315:6: ( ruleSelection_Stmt )
                    // InternalStructuredTextParser.g:2316:1: ruleSelection_Stmt
                    {
                     before(grammarAccess.getStmtAccess().getSelection_StmtParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleSelection_Stmt();

                    state._fsp--;

                     after(grammarAccess.getStmtAccess().getSelection_StmtParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:2321:6: ( ruleIteration_Stmt )
                    {
                    // InternalStructuredTextParser.g:2321:6: ( ruleIteration_Stmt )
                    // InternalStructuredTextParser.g:2322:1: ruleIteration_Stmt
                    {
                     before(grammarAccess.getStmtAccess().getIteration_StmtParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleIteration_Stmt();

                    state._fsp--;

                     after(grammarAccess.getStmtAccess().getIteration_StmtParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stmt__Alternatives"


    // $ANTLR start "rule__Subprog_Ctrl_Stmt__Alternatives"
    // InternalStructuredTextParser.g:2332:1: rule__Subprog_Ctrl_Stmt__Alternatives : ( ( ruleFunc_Call ) | ( ( rule__Subprog_Ctrl_Stmt__Group_1__0 ) ) | ( ( rule__Subprog_Ctrl_Stmt__Group_2__0 ) ) );
    public final void rule__Subprog_Ctrl_Stmt__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2336:1: ( ( ruleFunc_Call ) | ( ( rule__Subprog_Ctrl_Stmt__Group_1__0 ) ) | ( ( rule__Subprog_Ctrl_Stmt__Group_2__0 ) ) )
            int alt2=3;
            switch ( input.LA(1) ) {
            case TIME:
            case RULE_ID:
                {
                alt2=1;
                }
                break;
            case SUPER:
                {
                alt2=2;
                }
                break;
            case RETURN:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalStructuredTextParser.g:2337:1: ( ruleFunc_Call )
                    {
                    // InternalStructuredTextParser.g:2337:1: ( ruleFunc_Call )
                    // InternalStructuredTextParser.g:2338:1: ruleFunc_Call
                    {
                     before(grammarAccess.getSubprog_Ctrl_StmtAccess().getFunc_CallParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleFunc_Call();

                    state._fsp--;

                     after(grammarAccess.getSubprog_Ctrl_StmtAccess().getFunc_CallParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2343:6: ( ( rule__Subprog_Ctrl_Stmt__Group_1__0 ) )
                    {
                    // InternalStructuredTextParser.g:2343:6: ( ( rule__Subprog_Ctrl_Stmt__Group_1__0 ) )
                    // InternalStructuredTextParser.g:2344:1: ( rule__Subprog_Ctrl_Stmt__Group_1__0 )
                    {
                     before(grammarAccess.getSubprog_Ctrl_StmtAccess().getGroup_1()); 
                    // InternalStructuredTextParser.g:2345:1: ( rule__Subprog_Ctrl_Stmt__Group_1__0 )
                    // InternalStructuredTextParser.g:2345:2: rule__Subprog_Ctrl_Stmt__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Subprog_Ctrl_Stmt__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSubprog_Ctrl_StmtAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:2349:6: ( ( rule__Subprog_Ctrl_Stmt__Group_2__0 ) )
                    {
                    // InternalStructuredTextParser.g:2349:6: ( ( rule__Subprog_Ctrl_Stmt__Group_2__0 ) )
                    // InternalStructuredTextParser.g:2350:1: ( rule__Subprog_Ctrl_Stmt__Group_2__0 )
                    {
                     before(grammarAccess.getSubprog_Ctrl_StmtAccess().getGroup_2()); 
                    // InternalStructuredTextParser.g:2351:1: ( rule__Subprog_Ctrl_Stmt__Group_2__0 )
                    // InternalStructuredTextParser.g:2351:2: rule__Subprog_Ctrl_Stmt__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Subprog_Ctrl_Stmt__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSubprog_Ctrl_StmtAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subprog_Ctrl_Stmt__Alternatives"


    // $ANTLR start "rule__Selection_Stmt__Alternatives"
    // InternalStructuredTextParser.g:2360:1: rule__Selection_Stmt__Alternatives : ( ( ruleIF_Stmt ) | ( ruleCase_Stmt ) );
    public final void rule__Selection_Stmt__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2364:1: ( ( ruleIF_Stmt ) | ( ruleCase_Stmt ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==IF) ) {
                alt3=1;
            }
            else if ( (LA3_0==CASE) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalStructuredTextParser.g:2365:1: ( ruleIF_Stmt )
                    {
                    // InternalStructuredTextParser.g:2365:1: ( ruleIF_Stmt )
                    // InternalStructuredTextParser.g:2366:1: ruleIF_Stmt
                    {
                     before(grammarAccess.getSelection_StmtAccess().getIF_StmtParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleIF_Stmt();

                    state._fsp--;

                     after(grammarAccess.getSelection_StmtAccess().getIF_StmtParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2371:6: ( ruleCase_Stmt )
                    {
                    // InternalStructuredTextParser.g:2371:6: ( ruleCase_Stmt )
                    // InternalStructuredTextParser.g:2372:1: ruleCase_Stmt
                    {
                     before(grammarAccess.getSelection_StmtAccess().getCase_StmtParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleCase_Stmt();

                    state._fsp--;

                     after(grammarAccess.getSelection_StmtAccess().getCase_StmtParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Selection_Stmt__Alternatives"


    // $ANTLR start "rule__Iteration_Stmt__Alternatives"
    // InternalStructuredTextParser.g:2382:1: rule__Iteration_Stmt__Alternatives : ( ( ruleFor_Stmt ) | ( ruleWhile_Stmt ) | ( ruleRepeat_Stmt ) | ( ( rule__Iteration_Stmt__Group_3__0 ) ) | ( ( rule__Iteration_Stmt__Group_4__0 ) ) );
    public final void rule__Iteration_Stmt__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2386:1: ( ( ruleFor_Stmt ) | ( ruleWhile_Stmt ) | ( ruleRepeat_Stmt ) | ( ( rule__Iteration_Stmt__Group_3__0 ) ) | ( ( rule__Iteration_Stmt__Group_4__0 ) ) )
            int alt4=5;
            switch ( input.LA(1) ) {
            case FOR:
                {
                alt4=1;
                }
                break;
            case WHILE:
                {
                alt4=2;
                }
                break;
            case REPEAT:
                {
                alt4=3;
                }
                break;
            case EXIT:
                {
                alt4=4;
                }
                break;
            case CONTINUE:
                {
                alt4=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalStructuredTextParser.g:2387:1: ( ruleFor_Stmt )
                    {
                    // InternalStructuredTextParser.g:2387:1: ( ruleFor_Stmt )
                    // InternalStructuredTextParser.g:2388:1: ruleFor_Stmt
                    {
                     before(grammarAccess.getIteration_StmtAccess().getFor_StmtParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleFor_Stmt();

                    state._fsp--;

                     after(grammarAccess.getIteration_StmtAccess().getFor_StmtParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2393:6: ( ruleWhile_Stmt )
                    {
                    // InternalStructuredTextParser.g:2393:6: ( ruleWhile_Stmt )
                    // InternalStructuredTextParser.g:2394:1: ruleWhile_Stmt
                    {
                     before(grammarAccess.getIteration_StmtAccess().getWhile_StmtParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleWhile_Stmt();

                    state._fsp--;

                     after(grammarAccess.getIteration_StmtAccess().getWhile_StmtParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:2399:6: ( ruleRepeat_Stmt )
                    {
                    // InternalStructuredTextParser.g:2399:6: ( ruleRepeat_Stmt )
                    // InternalStructuredTextParser.g:2400:1: ruleRepeat_Stmt
                    {
                     before(grammarAccess.getIteration_StmtAccess().getRepeat_StmtParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleRepeat_Stmt();

                    state._fsp--;

                     after(grammarAccess.getIteration_StmtAccess().getRepeat_StmtParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:2405:6: ( ( rule__Iteration_Stmt__Group_3__0 ) )
                    {
                    // InternalStructuredTextParser.g:2405:6: ( ( rule__Iteration_Stmt__Group_3__0 ) )
                    // InternalStructuredTextParser.g:2406:1: ( rule__Iteration_Stmt__Group_3__0 )
                    {
                     before(grammarAccess.getIteration_StmtAccess().getGroup_3()); 
                    // InternalStructuredTextParser.g:2407:1: ( rule__Iteration_Stmt__Group_3__0 )
                    // InternalStructuredTextParser.g:2407:2: rule__Iteration_Stmt__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Iteration_Stmt__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getIteration_StmtAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalStructuredTextParser.g:2411:6: ( ( rule__Iteration_Stmt__Group_4__0 ) )
                    {
                    // InternalStructuredTextParser.g:2411:6: ( ( rule__Iteration_Stmt__Group_4__0 ) )
                    // InternalStructuredTextParser.g:2412:1: ( rule__Iteration_Stmt__Group_4__0 )
                    {
                     before(grammarAccess.getIteration_StmtAccess().getGroup_4()); 
                    // InternalStructuredTextParser.g:2413:1: ( rule__Iteration_Stmt__Group_4__0 )
                    // InternalStructuredTextParser.g:2413:2: rule__Iteration_Stmt__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Iteration_Stmt__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getIteration_StmtAccess().getGroup_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iteration_Stmt__Alternatives"


    // $ANTLR start "rule__Unary_Expr__Alternatives"
    // InternalStructuredTextParser.g:2422:1: rule__Unary_Expr__Alternatives : ( ( ( rule__Unary_Expr__Group_0__0 ) ) | ( rulePrimary_Expr ) | ( ruleConstant ) );
    public final void rule__Unary_Expr__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2426:1: ( ( ( rule__Unary_Expr__Group_0__0 ) ) | ( rulePrimary_Expr ) | ( ruleConstant ) )
            int alt5=3;
            alt5 = dfa5.predict(input);
            switch (alt5) {
                case 1 :
                    // InternalStructuredTextParser.g:2427:1: ( ( rule__Unary_Expr__Group_0__0 ) )
                    {
                    // InternalStructuredTextParser.g:2427:1: ( ( rule__Unary_Expr__Group_0__0 ) )
                    // InternalStructuredTextParser.g:2428:1: ( rule__Unary_Expr__Group_0__0 )
                    {
                     before(grammarAccess.getUnary_ExprAccess().getGroup_0()); 
                    // InternalStructuredTextParser.g:2429:1: ( rule__Unary_Expr__Group_0__0 )
                    // InternalStructuredTextParser.g:2429:2: rule__Unary_Expr__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Unary_Expr__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnary_ExprAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2433:6: ( rulePrimary_Expr )
                    {
                    // InternalStructuredTextParser.g:2433:6: ( rulePrimary_Expr )
                    // InternalStructuredTextParser.g:2434:1: rulePrimary_Expr
                    {
                     before(grammarAccess.getUnary_ExprAccess().getPrimary_ExprParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    rulePrimary_Expr();

                    state._fsp--;

                     after(grammarAccess.getUnary_ExprAccess().getPrimary_ExprParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:2439:6: ( ruleConstant )
                    {
                    // InternalStructuredTextParser.g:2439:6: ( ruleConstant )
                    // InternalStructuredTextParser.g:2440:1: ruleConstant
                    {
                     before(grammarAccess.getUnary_ExprAccess().getConstantParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleConstant();

                    state._fsp--;

                     after(grammarAccess.getUnary_ExprAccess().getConstantParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_Expr__Alternatives"


    // $ANTLR start "rule__Primary_Expr__Alternatives"
    // InternalStructuredTextParser.g:2450:1: rule__Primary_Expr__Alternatives : ( ( ruleVariable ) | ( ruleFunc_Call ) | ( ( rule__Primary_Expr__Group_2__0 ) ) );
    public final void rule__Primary_Expr__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2454:1: ( ( ruleVariable ) | ( ruleFunc_Call ) | ( ( rule__Primary_Expr__Group_2__0 ) ) )
            int alt6=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==LeftParenthesis) ) {
                    alt6=2;
                }
                else if ( (LA6_1==EOF||LA6_1==END_REPEAT||LA6_1==THEN||LA6_1==AND||LA6_1==MOD||(LA6_1>=XOR && LA6_1<=AsteriskAsterisk)||(LA6_1>=LessThanSignEqualsSign && LA6_1<=LessThanSignGreaterThanSign)||(LA6_1>=GreaterThanSignEqualsSign && LA6_1<=DO)||(LA6_1>=OF && LA6_1<=TO)||LA6_1==Ampersand||(LA6_1>=RightParenthesis && LA6_1<=Solidus)||(LA6_1>=Semicolon && LA6_1<=GreaterThanSign)||(LA6_1>=LeftSquareBracket && LA6_1<=RightSquareBracket)) ) {
                    alt6=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 1, input);

                    throw nvae;
                }
                }
                break;
            case DT:
            case LT:
            case T:
                {
                alt6=1;
                }
                break;
            case TIME:
                {
                alt6=2;
                }
                break;
            case LeftParenthesis:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalStructuredTextParser.g:2455:1: ( ruleVariable )
                    {
                    // InternalStructuredTextParser.g:2455:1: ( ruleVariable )
                    // InternalStructuredTextParser.g:2456:1: ruleVariable
                    {
                     before(grammarAccess.getPrimary_ExprAccess().getVariableParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleVariable();

                    state._fsp--;

                     after(grammarAccess.getPrimary_ExprAccess().getVariableParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2461:6: ( ruleFunc_Call )
                    {
                    // InternalStructuredTextParser.g:2461:6: ( ruleFunc_Call )
                    // InternalStructuredTextParser.g:2462:1: ruleFunc_Call
                    {
                     before(grammarAccess.getPrimary_ExprAccess().getFunc_CallParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleFunc_Call();

                    state._fsp--;

                     after(grammarAccess.getPrimary_ExprAccess().getFunc_CallParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:2467:6: ( ( rule__Primary_Expr__Group_2__0 ) )
                    {
                    // InternalStructuredTextParser.g:2467:6: ( ( rule__Primary_Expr__Group_2__0 ) )
                    // InternalStructuredTextParser.g:2468:1: ( rule__Primary_Expr__Group_2__0 )
                    {
                     before(grammarAccess.getPrimary_ExprAccess().getGroup_2()); 
                    // InternalStructuredTextParser.g:2469:1: ( rule__Primary_Expr__Group_2__0 )
                    // InternalStructuredTextParser.g:2469:2: rule__Primary_Expr__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary_Expr__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimary_ExprAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary_Expr__Alternatives"


    // $ANTLR start "rule__Func_Call__FuncAlternatives_0_0"
    // InternalStructuredTextParser.g:2478:1: rule__Func_Call__FuncAlternatives_0_0 : ( ( RULE_ID ) | ( TIME ) );
    public final void rule__Func_Call__FuncAlternatives_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2482:1: ( ( RULE_ID ) | ( TIME ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_ID) ) {
                alt7=1;
            }
            else if ( (LA7_0==TIME) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalStructuredTextParser.g:2483:1: ( RULE_ID )
                    {
                    // InternalStructuredTextParser.g:2483:1: ( RULE_ID )
                    // InternalStructuredTextParser.g:2484:1: RULE_ID
                    {
                     before(grammarAccess.getFunc_CallAccess().getFuncIDTerminalRuleCall_0_0_0()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getFunc_CallAccess().getFuncIDTerminalRuleCall_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2489:6: ( TIME )
                    {
                    // InternalStructuredTextParser.g:2489:6: ( TIME )
                    // InternalStructuredTextParser.g:2490:1: TIME
                    {
                     before(grammarAccess.getFunc_CallAccess().getFuncTIMEKeyword_0_0_1()); 
                    match(input,TIME,FOLLOW_2); 
                     after(grammarAccess.getFunc_CallAccess().getFuncTIMEKeyword_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__FuncAlternatives_0_0"


    // $ANTLR start "rule__Param_Assign__Alternatives"
    // InternalStructuredTextParser.g:2502:1: rule__Param_Assign__Alternatives : ( ( ruleParam_Assign_In ) | ( ruleParam_Assign_Out ) );
    public final void rule__Param_Assign__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2506:1: ( ( ruleParam_Assign_In ) | ( ruleParam_Assign_Out ) )
            int alt8=2;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                int LA8_1 = input.LA(2);

                if ( (LA8_1==EOF||LA8_1==AND||LA8_1==MOD||(LA8_1>=XOR && LA8_1<=LessThanSignGreaterThanSign)||LA8_1==GreaterThanSignEqualsSign||LA8_1==OR||(LA8_1>=Ampersand && LA8_1<=Solidus)||(LA8_1>=LessThanSign && LA8_1<=GreaterThanSign)||LA8_1==LeftSquareBracket) ) {
                    alt8=1;
                }
                else if ( (LA8_1==EqualsSignGreaterThanSign) ) {
                    alt8=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 1, input);

                    throw nvae;
                }
                }
                break;
            case LDATE_AND_TIME:
            case DATE_AND_TIME:
            case LTIME_OF_DAY:
            case TIME_OF_DAY:
            case WSTRING:
            case STRING:
            case FALSE:
            case LDATE:
            case LREAL:
            case LTIME:
            case UDINT:
            case ULINT:
            case USINT:
            case WCHAR:
            case BOOL:
            case CHAR:
            case DATE:
            case DINT:
            case LINT:
            case LTOD:
            case REAL:
            case SINT:
            case TIME:
            case TRUE:
            case UINT:
            case INT:
            case LDT:
            case TOD:
            case DT:
            case LD:
            case LT:
            case LeftParenthesis:
            case PlusSign:
            case HyphenMinus:
            case T:
            case D_1:
            case RULE_BINARY_INT:
            case RULE_OCTAL_INT:
            case RULE_HEX_INT:
            case RULE_UNSIGNED_INT:
            case RULE_S_BYTE_CHAR_STR:
            case RULE_D_BYTE_CHAR_STR:
                {
                alt8=1;
                }
                break;
            case NOT:
                {
                int LA8_3 = input.LA(2);

                if ( (LA8_3==RULE_ID) ) {
                    int LA8_5 = input.LA(3);

                    if ( (LA8_5==EOF||LA8_5==AND||LA8_5==MOD||(LA8_5>=XOR && LA8_5<=AsteriskAsterisk)||(LA8_5>=LessThanSignEqualsSign && LA8_5<=LessThanSignGreaterThanSign)||LA8_5==GreaterThanSignEqualsSign||LA8_5==OR||(LA8_5>=Ampersand && LA8_5<=Solidus)||(LA8_5>=LessThanSign && LA8_5<=GreaterThanSign)||LA8_5==LeftSquareBracket) ) {
                        alt8=1;
                    }
                    else if ( (LA8_5==EqualsSignGreaterThanSign) ) {
                        alt8=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 8, 5, input);

                        throw nvae;
                    }
                }
                else if ( (LA8_3==TIME||LA8_3==DT||LA8_3==LT||LA8_3==LeftParenthesis||LA8_3==T) ) {
                    alt8=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 3, input);

                    throw nvae;
                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalStructuredTextParser.g:2507:1: ( ruleParam_Assign_In )
                    {
                    // InternalStructuredTextParser.g:2507:1: ( ruleParam_Assign_In )
                    // InternalStructuredTextParser.g:2508:1: ruleParam_Assign_In
                    {
                     before(grammarAccess.getParam_AssignAccess().getParam_Assign_InParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleParam_Assign_In();

                    state._fsp--;

                     after(grammarAccess.getParam_AssignAccess().getParam_Assign_InParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2513:6: ( ruleParam_Assign_Out )
                    {
                    // InternalStructuredTextParser.g:2513:6: ( ruleParam_Assign_Out )
                    // InternalStructuredTextParser.g:2514:1: ruleParam_Assign_Out
                    {
                     before(grammarAccess.getParam_AssignAccess().getParam_Assign_OutParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleParam_Assign_Out();

                    state._fsp--;

                     after(grammarAccess.getParam_AssignAccess().getParam_Assign_OutParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign__Alternatives"


    // $ANTLR start "rule__Variable_Subscript__Alternatives_0"
    // InternalStructuredTextParser.g:2524:1: rule__Variable_Subscript__Alternatives_0 : ( ( ruleVariable_Primary ) | ( ruleVariable_Adapter ) );
    public final void rule__Variable_Subscript__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2528:1: ( ( ruleVariable_Primary ) | ( ruleVariable_Adapter ) )
            int alt9=2;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                int LA9_1 = input.LA(2);

                if ( (LA9_1==EOF||LA9_1==END_REPEAT||LA9_1==THEN||LA9_1==AND||LA9_1==MOD||(LA9_1>=XOR && LA9_1<=LessThanSignGreaterThanSign)||(LA9_1>=GreaterThanSignEqualsSign && LA9_1<=DO)||(LA9_1>=OF && LA9_1<=TO)||LA9_1==Ampersand||(LA9_1>=RightParenthesis && LA9_1<=HyphenMinus)||LA9_1==Solidus||(LA9_1>=Semicolon && LA9_1<=GreaterThanSign)||(LA9_1>=LeftSquareBracket && LA9_1<=RightSquareBracket)) ) {
                    alt9=1;
                }
                else if ( (LA9_1==FullStop) ) {
                    alt9=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 1, input);

                    throw nvae;
                }
                }
                break;
            case T:
                {
                int LA9_2 = input.LA(2);

                if ( (LA9_2==EOF||LA9_2==END_REPEAT||LA9_2==THEN||LA9_2==AND||LA9_2==MOD||(LA9_2>=XOR && LA9_2<=LessThanSignGreaterThanSign)||(LA9_2>=GreaterThanSignEqualsSign && LA9_2<=DO)||(LA9_2>=OF && LA9_2<=TO)||LA9_2==Ampersand||(LA9_2>=RightParenthesis && LA9_2<=HyphenMinus)||LA9_2==Solidus||(LA9_2>=Semicolon && LA9_2<=GreaterThanSign)||(LA9_2>=LeftSquareBracket && LA9_2<=RightSquareBracket)) ) {
                    alt9=1;
                }
                else if ( (LA9_2==FullStop) ) {
                    alt9=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 2, input);

                    throw nvae;
                }
                }
                break;
            case LT:
                {
                int LA9_3 = input.LA(2);

                if ( (LA9_3==EOF||LA9_3==END_REPEAT||LA9_3==THEN||LA9_3==AND||LA9_3==MOD||(LA9_3>=XOR && LA9_3<=LessThanSignGreaterThanSign)||(LA9_3>=GreaterThanSignEqualsSign && LA9_3<=DO)||(LA9_3>=OF && LA9_3<=TO)||LA9_3==Ampersand||(LA9_3>=RightParenthesis && LA9_3<=HyphenMinus)||LA9_3==Solidus||(LA9_3>=Semicolon && LA9_3<=GreaterThanSign)||(LA9_3>=LeftSquareBracket && LA9_3<=RightSquareBracket)) ) {
                    alt9=1;
                }
                else if ( (LA9_3==FullStop) ) {
                    alt9=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 3, input);

                    throw nvae;
                }
                }
                break;
            case DT:
                {
                int LA9_4 = input.LA(2);

                if ( (LA9_4==EOF||LA9_4==END_REPEAT||LA9_4==THEN||LA9_4==AND||LA9_4==MOD||(LA9_4>=XOR && LA9_4<=LessThanSignGreaterThanSign)||(LA9_4>=GreaterThanSignEqualsSign && LA9_4<=DO)||(LA9_4>=OF && LA9_4<=TO)||LA9_4==Ampersand||(LA9_4>=RightParenthesis && LA9_4<=HyphenMinus)||LA9_4==Solidus||(LA9_4>=Semicolon && LA9_4<=GreaterThanSign)||(LA9_4>=LeftSquareBracket && LA9_4<=RightSquareBracket)) ) {
                    alt9=1;
                }
                else if ( (LA9_4==FullStop) ) {
                    alt9=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 4, input);

                    throw nvae;
                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalStructuredTextParser.g:2529:1: ( ruleVariable_Primary )
                    {
                    // InternalStructuredTextParser.g:2529:1: ( ruleVariable_Primary )
                    // InternalStructuredTextParser.g:2530:1: ruleVariable_Primary
                    {
                     before(grammarAccess.getVariable_SubscriptAccess().getVariable_PrimaryParserRuleCall_0_0()); 
                    pushFollow(FOLLOW_2);
                    ruleVariable_Primary();

                    state._fsp--;

                     after(grammarAccess.getVariable_SubscriptAccess().getVariable_PrimaryParserRuleCall_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2535:6: ( ruleVariable_Adapter )
                    {
                    // InternalStructuredTextParser.g:2535:6: ( ruleVariable_Adapter )
                    // InternalStructuredTextParser.g:2536:1: ruleVariable_Adapter
                    {
                     before(grammarAccess.getVariable_SubscriptAccess().getVariable_AdapterParserRuleCall_0_1()); 
                    pushFollow(FOLLOW_2);
                    ruleVariable_Adapter();

                    state._fsp--;

                     after(grammarAccess.getVariable_SubscriptAccess().getVariable_AdapterParserRuleCall_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Alternatives_0"


    // $ANTLR start "rule__Adapter_Name__Alternatives"
    // InternalStructuredTextParser.g:2546:1: rule__Adapter_Name__Alternatives : ( ( RULE_ID ) | ( T ) | ( LT ) | ( DT ) );
    public final void rule__Adapter_Name__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2550:1: ( ( RULE_ID ) | ( T ) | ( LT ) | ( DT ) )
            int alt10=4;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt10=1;
                }
                break;
            case T:
                {
                alt10=2;
                }
                break;
            case LT:
                {
                alt10=3;
                }
                break;
            case DT:
                {
                alt10=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalStructuredTextParser.g:2551:1: ( RULE_ID )
                    {
                    // InternalStructuredTextParser.g:2551:1: ( RULE_ID )
                    // InternalStructuredTextParser.g:2552:1: RULE_ID
                    {
                     before(grammarAccess.getAdapter_NameAccess().getIDTerminalRuleCall_0()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getAdapter_NameAccess().getIDTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2557:6: ( T )
                    {
                    // InternalStructuredTextParser.g:2557:6: ( T )
                    // InternalStructuredTextParser.g:2558:1: T
                    {
                     before(grammarAccess.getAdapter_NameAccess().getTKeyword_1()); 
                    match(input,T,FOLLOW_2); 
                     after(grammarAccess.getAdapter_NameAccess().getTKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:2565:6: ( LT )
                    {
                    // InternalStructuredTextParser.g:2565:6: ( LT )
                    // InternalStructuredTextParser.g:2566:1: LT
                    {
                     before(grammarAccess.getAdapter_NameAccess().getLTKeyword_2()); 
                    match(input,LT,FOLLOW_2); 
                     after(grammarAccess.getAdapter_NameAccess().getLTKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:2573:6: ( DT )
                    {
                    // InternalStructuredTextParser.g:2573:6: ( DT )
                    // InternalStructuredTextParser.g:2574:1: DT
                    {
                     before(grammarAccess.getAdapter_NameAccess().getDTKeyword_3()); 
                    match(input,DT,FOLLOW_2); 
                     after(grammarAccess.getAdapter_NameAccess().getDTKeyword_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter_Name__Alternatives"


    // $ANTLR start "rule__Variable_Name__Alternatives"
    // InternalStructuredTextParser.g:2586:1: rule__Variable_Name__Alternatives : ( ( RULE_ID ) | ( T ) | ( LT ) | ( DT ) );
    public final void rule__Variable_Name__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2590:1: ( ( RULE_ID ) | ( T ) | ( LT ) | ( DT ) )
            int alt11=4;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt11=1;
                }
                break;
            case T:
                {
                alt11=2;
                }
                break;
            case LT:
                {
                alt11=3;
                }
                break;
            case DT:
                {
                alt11=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // InternalStructuredTextParser.g:2591:1: ( RULE_ID )
                    {
                    // InternalStructuredTextParser.g:2591:1: ( RULE_ID )
                    // InternalStructuredTextParser.g:2592:1: RULE_ID
                    {
                     before(grammarAccess.getVariable_NameAccess().getIDTerminalRuleCall_0()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getVariable_NameAccess().getIDTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2597:6: ( T )
                    {
                    // InternalStructuredTextParser.g:2597:6: ( T )
                    // InternalStructuredTextParser.g:2598:1: T
                    {
                     before(grammarAccess.getVariable_NameAccess().getTKeyword_1()); 
                    match(input,T,FOLLOW_2); 
                     after(grammarAccess.getVariable_NameAccess().getTKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:2605:6: ( LT )
                    {
                    // InternalStructuredTextParser.g:2605:6: ( LT )
                    // InternalStructuredTextParser.g:2606:1: LT
                    {
                     before(grammarAccess.getVariable_NameAccess().getLTKeyword_2()); 
                    match(input,LT,FOLLOW_2); 
                     after(grammarAccess.getVariable_NameAccess().getLTKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:2613:6: ( DT )
                    {
                    // InternalStructuredTextParser.g:2613:6: ( DT )
                    // InternalStructuredTextParser.g:2614:1: DT
                    {
                     before(grammarAccess.getVariable_NameAccess().getDTKeyword_3()); 
                    match(input,DT,FOLLOW_2); 
                     after(grammarAccess.getVariable_NameAccess().getDTKeyword_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Name__Alternatives"


    // $ANTLR start "rule__Constant__Alternatives"
    // InternalStructuredTextParser.g:2626:1: rule__Constant__Alternatives : ( ( ruleNumeric_Literal ) | ( ruleChar_Literal ) | ( ruleTime_Literal ) | ( ruleBool_Literal ) );
    public final void rule__Constant__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2630:1: ( ( ruleNumeric_Literal ) | ( ruleChar_Literal ) | ( ruleTime_Literal ) | ( ruleBool_Literal ) )
            int alt12=4;
            switch ( input.LA(1) ) {
            case LREAL:
            case UDINT:
            case ULINT:
            case USINT:
            case DINT:
            case LINT:
            case REAL:
            case SINT:
            case UINT:
            case INT:
            case PlusSign:
            case HyphenMinus:
            case RULE_BINARY_INT:
            case RULE_OCTAL_INT:
            case RULE_HEX_INT:
            case RULE_UNSIGNED_INT:
                {
                alt12=1;
                }
                break;
            case WSTRING:
            case STRING:
            case WCHAR:
            case CHAR:
            case RULE_S_BYTE_CHAR_STR:
            case RULE_D_BYTE_CHAR_STR:
                {
                alt12=2;
                }
                break;
            case LDATE_AND_TIME:
            case DATE_AND_TIME:
            case LTIME_OF_DAY:
            case TIME_OF_DAY:
            case LDATE:
            case LTIME:
            case DATE:
            case LTOD:
            case TIME:
            case LDT:
            case TOD:
            case DT:
            case LD:
            case LT:
            case T:
            case D_1:
                {
                alt12=3;
                }
                break;
            case FALSE:
            case BOOL:
            case TRUE:
                {
                alt12=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalStructuredTextParser.g:2631:1: ( ruleNumeric_Literal )
                    {
                    // InternalStructuredTextParser.g:2631:1: ( ruleNumeric_Literal )
                    // InternalStructuredTextParser.g:2632:1: ruleNumeric_Literal
                    {
                     before(grammarAccess.getConstantAccess().getNumeric_LiteralParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleNumeric_Literal();

                    state._fsp--;

                     after(grammarAccess.getConstantAccess().getNumeric_LiteralParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2637:6: ( ruleChar_Literal )
                    {
                    // InternalStructuredTextParser.g:2637:6: ( ruleChar_Literal )
                    // InternalStructuredTextParser.g:2638:1: ruleChar_Literal
                    {
                     before(grammarAccess.getConstantAccess().getChar_LiteralParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleChar_Literal();

                    state._fsp--;

                     after(grammarAccess.getConstantAccess().getChar_LiteralParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:2643:6: ( ruleTime_Literal )
                    {
                    // InternalStructuredTextParser.g:2643:6: ( ruleTime_Literal )
                    // InternalStructuredTextParser.g:2644:1: ruleTime_Literal
                    {
                     before(grammarAccess.getConstantAccess().getTime_LiteralParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleTime_Literal();

                    state._fsp--;

                     after(grammarAccess.getConstantAccess().getTime_LiteralParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:2649:6: ( ruleBool_Literal )
                    {
                    // InternalStructuredTextParser.g:2649:6: ( ruleBool_Literal )
                    // InternalStructuredTextParser.g:2650:1: ruleBool_Literal
                    {
                     before(grammarAccess.getConstantAccess().getBool_LiteralParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleBool_Literal();

                    state._fsp--;

                     after(grammarAccess.getConstantAccess().getBool_LiteralParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Alternatives"


    // $ANTLR start "rule__Numeric_Literal__Alternatives"
    // InternalStructuredTextParser.g:2660:1: rule__Numeric_Literal__Alternatives : ( ( ruleInt_Literal ) | ( ruleReal_Literal ) );
    public final void rule__Numeric_Literal__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2664:1: ( ( ruleInt_Literal ) | ( ruleReal_Literal ) )
            int alt13=2;
            switch ( input.LA(1) ) {
            case UDINT:
            case ULINT:
            case USINT:
            case DINT:
            case LINT:
            case SINT:
            case UINT:
            case INT:
            case RULE_BINARY_INT:
            case RULE_OCTAL_INT:
            case RULE_HEX_INT:
                {
                alt13=1;
                }
                break;
            case PlusSign:
                {
                int LA13_2 = input.LA(2);

                if ( (LA13_2==RULE_UNSIGNED_INT) ) {
                    int LA13_4 = input.LA(3);

                    if ( (LA13_4==FullStop) ) {
                        alt13=2;
                    }
                    else if ( (LA13_4==EOF||LA13_4==END_REPEAT||LA13_4==THEN||LA13_4==AND||LA13_4==MOD||(LA13_4>=XOR && LA13_4<=AsteriskAsterisk)||(LA13_4>=LessThanSignEqualsSign && LA13_4<=LessThanSignGreaterThanSign)||(LA13_4>=GreaterThanSignEqualsSign && LA13_4<=DO)||(LA13_4>=OF && LA13_4<=TO)||LA13_4==Ampersand||(LA13_4>=RightParenthesis && LA13_4<=HyphenMinus)||(LA13_4>=Solidus && LA13_4<=GreaterThanSign)||LA13_4==RightSquareBracket) ) {
                        alt13=1;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 13, 4, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 2, input);

                    throw nvae;
                }
                }
                break;
            case HyphenMinus:
                {
                int LA13_3 = input.LA(2);

                if ( (LA13_3==RULE_UNSIGNED_INT) ) {
                    int LA13_4 = input.LA(3);

                    if ( (LA13_4==FullStop) ) {
                        alt13=2;
                    }
                    else if ( (LA13_4==EOF||LA13_4==END_REPEAT||LA13_4==THEN||LA13_4==AND||LA13_4==MOD||(LA13_4>=XOR && LA13_4<=AsteriskAsterisk)||(LA13_4>=LessThanSignEqualsSign && LA13_4<=LessThanSignGreaterThanSign)||(LA13_4>=GreaterThanSignEqualsSign && LA13_4<=DO)||(LA13_4>=OF && LA13_4<=TO)||LA13_4==Ampersand||(LA13_4>=RightParenthesis && LA13_4<=HyphenMinus)||(LA13_4>=Solidus && LA13_4<=GreaterThanSign)||LA13_4==RightSquareBracket) ) {
                        alt13=1;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 13, 4, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 3, input);

                    throw nvae;
                }
                }
                break;
            case RULE_UNSIGNED_INT:
                {
                int LA13_4 = input.LA(2);

                if ( (LA13_4==FullStop) ) {
                    alt13=2;
                }
                else if ( (LA13_4==EOF||LA13_4==END_REPEAT||LA13_4==THEN||LA13_4==AND||LA13_4==MOD||(LA13_4>=XOR && LA13_4<=AsteriskAsterisk)||(LA13_4>=LessThanSignEqualsSign && LA13_4<=LessThanSignGreaterThanSign)||(LA13_4>=GreaterThanSignEqualsSign && LA13_4<=DO)||(LA13_4>=OF && LA13_4<=TO)||LA13_4==Ampersand||(LA13_4>=RightParenthesis && LA13_4<=HyphenMinus)||(LA13_4>=Solidus && LA13_4<=GreaterThanSign)||LA13_4==RightSquareBracket) ) {
                    alt13=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 4, input);

                    throw nvae;
                }
                }
                break;
            case LREAL:
            case REAL:
                {
                alt13=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // InternalStructuredTextParser.g:2665:1: ( ruleInt_Literal )
                    {
                    // InternalStructuredTextParser.g:2665:1: ( ruleInt_Literal )
                    // InternalStructuredTextParser.g:2666:1: ruleInt_Literal
                    {
                     before(grammarAccess.getNumeric_LiteralAccess().getInt_LiteralParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleInt_Literal();

                    state._fsp--;

                     after(grammarAccess.getNumeric_LiteralAccess().getInt_LiteralParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2671:6: ( ruleReal_Literal )
                    {
                    // InternalStructuredTextParser.g:2671:6: ( ruleReal_Literal )
                    // InternalStructuredTextParser.g:2672:1: ruleReal_Literal
                    {
                     before(grammarAccess.getNumeric_LiteralAccess().getReal_LiteralParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleReal_Literal();

                    state._fsp--;

                     after(grammarAccess.getNumeric_LiteralAccess().getReal_LiteralParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeric_Literal__Alternatives"


    // $ANTLR start "rule__Int_Literal__ValueAlternatives_1_0"
    // InternalStructuredTextParser.g:2682:1: rule__Int_Literal__ValueAlternatives_1_0 : ( ( ruleSigned_Int ) | ( RULE_BINARY_INT ) | ( RULE_OCTAL_INT ) | ( RULE_HEX_INT ) );
    public final void rule__Int_Literal__ValueAlternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2686:1: ( ( ruleSigned_Int ) | ( RULE_BINARY_INT ) | ( RULE_OCTAL_INT ) | ( RULE_HEX_INT ) )
            int alt14=4;
            switch ( input.LA(1) ) {
            case PlusSign:
            case HyphenMinus:
            case RULE_UNSIGNED_INT:
                {
                alt14=1;
                }
                break;
            case RULE_BINARY_INT:
                {
                alt14=2;
                }
                break;
            case RULE_OCTAL_INT:
                {
                alt14=3;
                }
                break;
            case RULE_HEX_INT:
                {
                alt14=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalStructuredTextParser.g:2687:1: ( ruleSigned_Int )
                    {
                    // InternalStructuredTextParser.g:2687:1: ( ruleSigned_Int )
                    // InternalStructuredTextParser.g:2688:1: ruleSigned_Int
                    {
                     before(grammarAccess.getInt_LiteralAccess().getValueSigned_IntParserRuleCall_1_0_0()); 
                    pushFollow(FOLLOW_2);
                    ruleSigned_Int();

                    state._fsp--;

                     after(grammarAccess.getInt_LiteralAccess().getValueSigned_IntParserRuleCall_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2693:6: ( RULE_BINARY_INT )
                    {
                    // InternalStructuredTextParser.g:2693:6: ( RULE_BINARY_INT )
                    // InternalStructuredTextParser.g:2694:1: RULE_BINARY_INT
                    {
                     before(grammarAccess.getInt_LiteralAccess().getValueBINARY_INTTerminalRuleCall_1_0_1()); 
                    match(input,RULE_BINARY_INT,FOLLOW_2); 
                     after(grammarAccess.getInt_LiteralAccess().getValueBINARY_INTTerminalRuleCall_1_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:2699:6: ( RULE_OCTAL_INT )
                    {
                    // InternalStructuredTextParser.g:2699:6: ( RULE_OCTAL_INT )
                    // InternalStructuredTextParser.g:2700:1: RULE_OCTAL_INT
                    {
                     before(grammarAccess.getInt_LiteralAccess().getValueOCTAL_INTTerminalRuleCall_1_0_2()); 
                    match(input,RULE_OCTAL_INT,FOLLOW_2); 
                     after(grammarAccess.getInt_LiteralAccess().getValueOCTAL_INTTerminalRuleCall_1_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:2705:6: ( RULE_HEX_INT )
                    {
                    // InternalStructuredTextParser.g:2705:6: ( RULE_HEX_INT )
                    // InternalStructuredTextParser.g:2706:1: RULE_HEX_INT
                    {
                     before(grammarAccess.getInt_LiteralAccess().getValueHEX_INTTerminalRuleCall_1_0_3()); 
                    match(input,RULE_HEX_INT,FOLLOW_2); 
                     after(grammarAccess.getInt_LiteralAccess().getValueHEX_INTTerminalRuleCall_1_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Int_Literal__ValueAlternatives_1_0"


    // $ANTLR start "rule__Signed_Int__Alternatives_0"
    // InternalStructuredTextParser.g:2716:1: rule__Signed_Int__Alternatives_0 : ( ( PlusSign ) | ( HyphenMinus ) );
    public final void rule__Signed_Int__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2720:1: ( ( PlusSign ) | ( HyphenMinus ) )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==PlusSign) ) {
                alt15=1;
            }
            else if ( (LA15_0==HyphenMinus) ) {
                alt15=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalStructuredTextParser.g:2721:1: ( PlusSign )
                    {
                    // InternalStructuredTextParser.g:2721:1: ( PlusSign )
                    // InternalStructuredTextParser.g:2722:1: PlusSign
                    {
                     before(grammarAccess.getSigned_IntAccess().getPlusSignKeyword_0_0()); 
                    match(input,PlusSign,FOLLOW_2); 
                     after(grammarAccess.getSigned_IntAccess().getPlusSignKeyword_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2729:6: ( HyphenMinus )
                    {
                    // InternalStructuredTextParser.g:2729:6: ( HyphenMinus )
                    // InternalStructuredTextParser.g:2730:1: HyphenMinus
                    {
                     before(grammarAccess.getSigned_IntAccess().getHyphenMinusKeyword_0_1()); 
                    match(input,HyphenMinus,FOLLOW_2); 
                     after(grammarAccess.getSigned_IntAccess().getHyphenMinusKeyword_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Signed_Int__Alternatives_0"


    // $ANTLR start "rule__Bool_Value__Alternatives"
    // InternalStructuredTextParser.g:2742:1: rule__Bool_Value__Alternatives : ( ( FALSE ) | ( TRUE ) );
    public final void rule__Bool_Value__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2746:1: ( ( FALSE ) | ( TRUE ) )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==FALSE) ) {
                alt16=1;
            }
            else if ( (LA16_0==TRUE) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // InternalStructuredTextParser.g:2747:1: ( FALSE )
                    {
                    // InternalStructuredTextParser.g:2747:1: ( FALSE )
                    // InternalStructuredTextParser.g:2748:1: FALSE
                    {
                     before(grammarAccess.getBool_ValueAccess().getFALSEKeyword_0()); 
                    match(input,FALSE,FOLLOW_2); 
                     after(grammarAccess.getBool_ValueAccess().getFALSEKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2755:6: ( TRUE )
                    {
                    // InternalStructuredTextParser.g:2755:6: ( TRUE )
                    // InternalStructuredTextParser.g:2756:1: TRUE
                    {
                     before(grammarAccess.getBool_ValueAccess().getTRUEKeyword_1()); 
                    match(input,TRUE,FOLLOW_2); 
                     after(grammarAccess.getBool_ValueAccess().getTRUEKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Bool_Value__Alternatives"


    // $ANTLR start "rule__Char_Str__Alternatives"
    // InternalStructuredTextParser.g:2768:1: rule__Char_Str__Alternatives : ( ( RULE_S_BYTE_CHAR_STR ) | ( RULE_D_BYTE_CHAR_STR ) );
    public final void rule__Char_Str__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2772:1: ( ( RULE_S_BYTE_CHAR_STR ) | ( RULE_D_BYTE_CHAR_STR ) )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_S_BYTE_CHAR_STR) ) {
                alt17=1;
            }
            else if ( (LA17_0==RULE_D_BYTE_CHAR_STR) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // InternalStructuredTextParser.g:2773:1: ( RULE_S_BYTE_CHAR_STR )
                    {
                    // InternalStructuredTextParser.g:2773:1: ( RULE_S_BYTE_CHAR_STR )
                    // InternalStructuredTextParser.g:2774:1: RULE_S_BYTE_CHAR_STR
                    {
                     before(grammarAccess.getChar_StrAccess().getS_BYTE_CHAR_STRTerminalRuleCall_0()); 
                    match(input,RULE_S_BYTE_CHAR_STR,FOLLOW_2); 
                     after(grammarAccess.getChar_StrAccess().getS_BYTE_CHAR_STRTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2779:6: ( RULE_D_BYTE_CHAR_STR )
                    {
                    // InternalStructuredTextParser.g:2779:6: ( RULE_D_BYTE_CHAR_STR )
                    // InternalStructuredTextParser.g:2780:1: RULE_D_BYTE_CHAR_STR
                    {
                     before(grammarAccess.getChar_StrAccess().getD_BYTE_CHAR_STRTerminalRuleCall_1()); 
                    match(input,RULE_D_BYTE_CHAR_STR,FOLLOW_2); 
                     after(grammarAccess.getChar_StrAccess().getD_BYTE_CHAR_STRTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Str__Alternatives"


    // $ANTLR start "rule__Time_Literal__Alternatives"
    // InternalStructuredTextParser.g:2790:1: rule__Time_Literal__Alternatives : ( ( ruleDuration ) | ( ruleTime_Of_Day ) | ( ruleDate ) | ( ruleDate_And_Time ) );
    public final void rule__Time_Literal__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2794:1: ( ( ruleDuration ) | ( ruleTime_Of_Day ) | ( ruleDate ) | ( ruleDate_And_Time ) )
            int alt18=4;
            switch ( input.LA(1) ) {
            case LTIME:
            case TIME:
            case LT:
            case T:
                {
                alt18=1;
                }
                break;
            case LTIME_OF_DAY:
            case TIME_OF_DAY:
            case LTOD:
            case TOD:
                {
                alt18=2;
                }
                break;
            case LDATE:
            case DATE:
            case LD:
            case D_1:
                {
                alt18=3;
                }
                break;
            case LDATE_AND_TIME:
            case DATE_AND_TIME:
            case LDT:
            case DT:
                {
                alt18=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // InternalStructuredTextParser.g:2795:1: ( ruleDuration )
                    {
                    // InternalStructuredTextParser.g:2795:1: ( ruleDuration )
                    // InternalStructuredTextParser.g:2796:1: ruleDuration
                    {
                     before(grammarAccess.getTime_LiteralAccess().getDurationParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleDuration();

                    state._fsp--;

                     after(grammarAccess.getTime_LiteralAccess().getDurationParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2801:6: ( ruleTime_Of_Day )
                    {
                    // InternalStructuredTextParser.g:2801:6: ( ruleTime_Of_Day )
                    // InternalStructuredTextParser.g:2802:1: ruleTime_Of_Day
                    {
                     before(grammarAccess.getTime_LiteralAccess().getTime_Of_DayParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleTime_Of_Day();

                    state._fsp--;

                     after(grammarAccess.getTime_LiteralAccess().getTime_Of_DayParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:2807:6: ( ruleDate )
                    {
                    // InternalStructuredTextParser.g:2807:6: ( ruleDate )
                    // InternalStructuredTextParser.g:2808:1: ruleDate
                    {
                     before(grammarAccess.getTime_LiteralAccess().getDateParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleDate();

                    state._fsp--;

                     after(grammarAccess.getTime_LiteralAccess().getDateParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:2813:6: ( ruleDate_And_Time )
                    {
                    // InternalStructuredTextParser.g:2813:6: ( ruleDate_And_Time )
                    // InternalStructuredTextParser.g:2814:1: ruleDate_And_Time
                    {
                     before(grammarAccess.getTime_LiteralAccess().getDate_And_TimeParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleDate_And_Time();

                    state._fsp--;

                     after(grammarAccess.getTime_LiteralAccess().getDate_And_TimeParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Time_Literal__Alternatives"


    // $ANTLR start "rule__Duration__Alternatives_2"
    // InternalStructuredTextParser.g:2824:1: rule__Duration__Alternatives_2 : ( ( PlusSign ) | ( ( rule__Duration__NegativeAssignment_2_1 ) ) );
    public final void rule__Duration__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2828:1: ( ( PlusSign ) | ( ( rule__Duration__NegativeAssignment_2_1 ) ) )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==PlusSign) ) {
                alt19=1;
            }
            else if ( (LA19_0==HyphenMinus) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalStructuredTextParser.g:2829:1: ( PlusSign )
                    {
                    // InternalStructuredTextParser.g:2829:1: ( PlusSign )
                    // InternalStructuredTextParser.g:2830:1: PlusSign
                    {
                     before(grammarAccess.getDurationAccess().getPlusSignKeyword_2_0()); 
                    match(input,PlusSign,FOLLOW_2); 
                     after(grammarAccess.getDurationAccess().getPlusSignKeyword_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2837:6: ( ( rule__Duration__NegativeAssignment_2_1 ) )
                    {
                    // InternalStructuredTextParser.g:2837:6: ( ( rule__Duration__NegativeAssignment_2_1 ) )
                    // InternalStructuredTextParser.g:2838:1: ( rule__Duration__NegativeAssignment_2_1 )
                    {
                     before(grammarAccess.getDurationAccess().getNegativeAssignment_2_1()); 
                    // InternalStructuredTextParser.g:2839:1: ( rule__Duration__NegativeAssignment_2_1 )
                    // InternalStructuredTextParser.g:2839:2: rule__Duration__NegativeAssignment_2_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Duration__NegativeAssignment_2_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getDurationAccess().getNegativeAssignment_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Alternatives_2"


    // $ANTLR start "rule__Type_Name__Alternatives"
    // InternalStructuredTextParser.g:2848:1: rule__Type_Name__Alternatives : ( ( RULE_ID ) | ( DINT ) | ( INT ) | ( SINT ) | ( LINT ) | ( UINT ) | ( USINT ) | ( UDINT ) | ( ULINT ) | ( REAL ) | ( LREAL ) | ( STRING ) | ( WSTRING ) | ( CHAR ) | ( WCHAR ) | ( TIME ) | ( LTIME ) | ( TIME_OF_DAY ) | ( LTIME_OF_DAY ) | ( TOD ) | ( LTOD ) | ( DATE ) | ( LDATE ) | ( DATE_AND_TIME ) | ( LDATE_AND_TIME ) | ( BOOL ) );
    public final void rule__Type_Name__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:2852:1: ( ( RULE_ID ) | ( DINT ) | ( INT ) | ( SINT ) | ( LINT ) | ( UINT ) | ( USINT ) | ( UDINT ) | ( ULINT ) | ( REAL ) | ( LREAL ) | ( STRING ) | ( WSTRING ) | ( CHAR ) | ( WCHAR ) | ( TIME ) | ( LTIME ) | ( TIME_OF_DAY ) | ( LTIME_OF_DAY ) | ( TOD ) | ( LTOD ) | ( DATE ) | ( LDATE ) | ( DATE_AND_TIME ) | ( LDATE_AND_TIME ) | ( BOOL ) )
            int alt20=26;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt20=1;
                }
                break;
            case DINT:
                {
                alt20=2;
                }
                break;
            case INT:
                {
                alt20=3;
                }
                break;
            case SINT:
                {
                alt20=4;
                }
                break;
            case LINT:
                {
                alt20=5;
                }
                break;
            case UINT:
                {
                alt20=6;
                }
                break;
            case USINT:
                {
                alt20=7;
                }
                break;
            case UDINT:
                {
                alt20=8;
                }
                break;
            case ULINT:
                {
                alt20=9;
                }
                break;
            case REAL:
                {
                alt20=10;
                }
                break;
            case LREAL:
                {
                alt20=11;
                }
                break;
            case STRING:
                {
                alt20=12;
                }
                break;
            case WSTRING:
                {
                alt20=13;
                }
                break;
            case CHAR:
                {
                alt20=14;
                }
                break;
            case WCHAR:
                {
                alt20=15;
                }
                break;
            case TIME:
                {
                alt20=16;
                }
                break;
            case LTIME:
                {
                alt20=17;
                }
                break;
            case TIME_OF_DAY:
                {
                alt20=18;
                }
                break;
            case LTIME_OF_DAY:
                {
                alt20=19;
                }
                break;
            case TOD:
                {
                alt20=20;
                }
                break;
            case LTOD:
                {
                alt20=21;
                }
                break;
            case DATE:
                {
                alt20=22;
                }
                break;
            case LDATE:
                {
                alt20=23;
                }
                break;
            case DATE_AND_TIME:
                {
                alt20=24;
                }
                break;
            case LDATE_AND_TIME:
                {
                alt20=25;
                }
                break;
            case BOOL:
                {
                alt20=26;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }

            switch (alt20) {
                case 1 :
                    // InternalStructuredTextParser.g:2853:1: ( RULE_ID )
                    {
                    // InternalStructuredTextParser.g:2853:1: ( RULE_ID )
                    // InternalStructuredTextParser.g:2854:1: RULE_ID
                    {
                     before(grammarAccess.getType_NameAccess().getIDTerminalRuleCall_0()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getIDTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:2859:6: ( DINT )
                    {
                    // InternalStructuredTextParser.g:2859:6: ( DINT )
                    // InternalStructuredTextParser.g:2860:1: DINT
                    {
                     before(grammarAccess.getType_NameAccess().getDINTKeyword_1()); 
                    match(input,DINT,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getDINTKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:2867:6: ( INT )
                    {
                    // InternalStructuredTextParser.g:2867:6: ( INT )
                    // InternalStructuredTextParser.g:2868:1: INT
                    {
                     before(grammarAccess.getType_NameAccess().getINTKeyword_2()); 
                    match(input,INT,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getINTKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:2875:6: ( SINT )
                    {
                    // InternalStructuredTextParser.g:2875:6: ( SINT )
                    // InternalStructuredTextParser.g:2876:1: SINT
                    {
                     before(grammarAccess.getType_NameAccess().getSINTKeyword_3()); 
                    match(input,SINT,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getSINTKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalStructuredTextParser.g:2883:6: ( LINT )
                    {
                    // InternalStructuredTextParser.g:2883:6: ( LINT )
                    // InternalStructuredTextParser.g:2884:1: LINT
                    {
                     before(grammarAccess.getType_NameAccess().getLINTKeyword_4()); 
                    match(input,LINT,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getLINTKeyword_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalStructuredTextParser.g:2891:6: ( UINT )
                    {
                    // InternalStructuredTextParser.g:2891:6: ( UINT )
                    // InternalStructuredTextParser.g:2892:1: UINT
                    {
                     before(grammarAccess.getType_NameAccess().getUINTKeyword_5()); 
                    match(input,UINT,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getUINTKeyword_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalStructuredTextParser.g:2899:6: ( USINT )
                    {
                    // InternalStructuredTextParser.g:2899:6: ( USINT )
                    // InternalStructuredTextParser.g:2900:1: USINT
                    {
                     before(grammarAccess.getType_NameAccess().getUSINTKeyword_6()); 
                    match(input,USINT,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getUSINTKeyword_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalStructuredTextParser.g:2907:6: ( UDINT )
                    {
                    // InternalStructuredTextParser.g:2907:6: ( UDINT )
                    // InternalStructuredTextParser.g:2908:1: UDINT
                    {
                     before(grammarAccess.getType_NameAccess().getUDINTKeyword_7()); 
                    match(input,UDINT,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getUDINTKeyword_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalStructuredTextParser.g:2915:6: ( ULINT )
                    {
                    // InternalStructuredTextParser.g:2915:6: ( ULINT )
                    // InternalStructuredTextParser.g:2916:1: ULINT
                    {
                     before(grammarAccess.getType_NameAccess().getULINTKeyword_8()); 
                    match(input,ULINT,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getULINTKeyword_8()); 

                    }


                    }
                    break;
                case 10 :
                    // InternalStructuredTextParser.g:2923:6: ( REAL )
                    {
                    // InternalStructuredTextParser.g:2923:6: ( REAL )
                    // InternalStructuredTextParser.g:2924:1: REAL
                    {
                     before(grammarAccess.getType_NameAccess().getREALKeyword_9()); 
                    match(input,REAL,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getREALKeyword_9()); 

                    }


                    }
                    break;
                case 11 :
                    // InternalStructuredTextParser.g:2931:6: ( LREAL )
                    {
                    // InternalStructuredTextParser.g:2931:6: ( LREAL )
                    // InternalStructuredTextParser.g:2932:1: LREAL
                    {
                     before(grammarAccess.getType_NameAccess().getLREALKeyword_10()); 
                    match(input,LREAL,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getLREALKeyword_10()); 

                    }


                    }
                    break;
                case 12 :
                    // InternalStructuredTextParser.g:2939:6: ( STRING )
                    {
                    // InternalStructuredTextParser.g:2939:6: ( STRING )
                    // InternalStructuredTextParser.g:2940:1: STRING
                    {
                     before(grammarAccess.getType_NameAccess().getSTRINGKeyword_11()); 
                    match(input,STRING,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getSTRINGKeyword_11()); 

                    }


                    }
                    break;
                case 13 :
                    // InternalStructuredTextParser.g:2947:6: ( WSTRING )
                    {
                    // InternalStructuredTextParser.g:2947:6: ( WSTRING )
                    // InternalStructuredTextParser.g:2948:1: WSTRING
                    {
                     before(grammarAccess.getType_NameAccess().getWSTRINGKeyword_12()); 
                    match(input,WSTRING,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getWSTRINGKeyword_12()); 

                    }


                    }
                    break;
                case 14 :
                    // InternalStructuredTextParser.g:2955:6: ( CHAR )
                    {
                    // InternalStructuredTextParser.g:2955:6: ( CHAR )
                    // InternalStructuredTextParser.g:2956:1: CHAR
                    {
                     before(grammarAccess.getType_NameAccess().getCHARKeyword_13()); 
                    match(input,CHAR,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getCHARKeyword_13()); 

                    }


                    }
                    break;
                case 15 :
                    // InternalStructuredTextParser.g:2963:6: ( WCHAR )
                    {
                    // InternalStructuredTextParser.g:2963:6: ( WCHAR )
                    // InternalStructuredTextParser.g:2964:1: WCHAR
                    {
                     before(grammarAccess.getType_NameAccess().getWCHARKeyword_14()); 
                    match(input,WCHAR,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getWCHARKeyword_14()); 

                    }


                    }
                    break;
                case 16 :
                    // InternalStructuredTextParser.g:2971:6: ( TIME )
                    {
                    // InternalStructuredTextParser.g:2971:6: ( TIME )
                    // InternalStructuredTextParser.g:2972:1: TIME
                    {
                     before(grammarAccess.getType_NameAccess().getTIMEKeyword_15()); 
                    match(input,TIME,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getTIMEKeyword_15()); 

                    }


                    }
                    break;
                case 17 :
                    // InternalStructuredTextParser.g:2979:6: ( LTIME )
                    {
                    // InternalStructuredTextParser.g:2979:6: ( LTIME )
                    // InternalStructuredTextParser.g:2980:1: LTIME
                    {
                     before(grammarAccess.getType_NameAccess().getLTIMEKeyword_16()); 
                    match(input,LTIME,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getLTIMEKeyword_16()); 

                    }


                    }
                    break;
                case 18 :
                    // InternalStructuredTextParser.g:2987:6: ( TIME_OF_DAY )
                    {
                    // InternalStructuredTextParser.g:2987:6: ( TIME_OF_DAY )
                    // InternalStructuredTextParser.g:2988:1: TIME_OF_DAY
                    {
                     before(grammarAccess.getType_NameAccess().getTIME_OF_DAYKeyword_17()); 
                    match(input,TIME_OF_DAY,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getTIME_OF_DAYKeyword_17()); 

                    }


                    }
                    break;
                case 19 :
                    // InternalStructuredTextParser.g:2995:6: ( LTIME_OF_DAY )
                    {
                    // InternalStructuredTextParser.g:2995:6: ( LTIME_OF_DAY )
                    // InternalStructuredTextParser.g:2996:1: LTIME_OF_DAY
                    {
                     before(grammarAccess.getType_NameAccess().getLTIME_OF_DAYKeyword_18()); 
                    match(input,LTIME_OF_DAY,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getLTIME_OF_DAYKeyword_18()); 

                    }


                    }
                    break;
                case 20 :
                    // InternalStructuredTextParser.g:3003:6: ( TOD )
                    {
                    // InternalStructuredTextParser.g:3003:6: ( TOD )
                    // InternalStructuredTextParser.g:3004:1: TOD
                    {
                     before(grammarAccess.getType_NameAccess().getTODKeyword_19()); 
                    match(input,TOD,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getTODKeyword_19()); 

                    }


                    }
                    break;
                case 21 :
                    // InternalStructuredTextParser.g:3011:6: ( LTOD )
                    {
                    // InternalStructuredTextParser.g:3011:6: ( LTOD )
                    // InternalStructuredTextParser.g:3012:1: LTOD
                    {
                     before(grammarAccess.getType_NameAccess().getLTODKeyword_20()); 
                    match(input,LTOD,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getLTODKeyword_20()); 

                    }


                    }
                    break;
                case 22 :
                    // InternalStructuredTextParser.g:3019:6: ( DATE )
                    {
                    // InternalStructuredTextParser.g:3019:6: ( DATE )
                    // InternalStructuredTextParser.g:3020:1: DATE
                    {
                     before(grammarAccess.getType_NameAccess().getDATEKeyword_21()); 
                    match(input,DATE,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getDATEKeyword_21()); 

                    }


                    }
                    break;
                case 23 :
                    // InternalStructuredTextParser.g:3027:6: ( LDATE )
                    {
                    // InternalStructuredTextParser.g:3027:6: ( LDATE )
                    // InternalStructuredTextParser.g:3028:1: LDATE
                    {
                     before(grammarAccess.getType_NameAccess().getLDATEKeyword_22()); 
                    match(input,LDATE,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getLDATEKeyword_22()); 

                    }


                    }
                    break;
                case 24 :
                    // InternalStructuredTextParser.g:3035:6: ( DATE_AND_TIME )
                    {
                    // InternalStructuredTextParser.g:3035:6: ( DATE_AND_TIME )
                    // InternalStructuredTextParser.g:3036:1: DATE_AND_TIME
                    {
                     before(grammarAccess.getType_NameAccess().getDATE_AND_TIMEKeyword_23()); 
                    match(input,DATE_AND_TIME,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getDATE_AND_TIMEKeyword_23()); 

                    }


                    }
                    break;
                case 25 :
                    // InternalStructuredTextParser.g:3043:6: ( LDATE_AND_TIME )
                    {
                    // InternalStructuredTextParser.g:3043:6: ( LDATE_AND_TIME )
                    // InternalStructuredTextParser.g:3044:1: LDATE_AND_TIME
                    {
                     before(grammarAccess.getType_NameAccess().getLDATE_AND_TIMEKeyword_24()); 
                    match(input,LDATE_AND_TIME,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getLDATE_AND_TIMEKeyword_24()); 

                    }


                    }
                    break;
                case 26 :
                    // InternalStructuredTextParser.g:3051:6: ( BOOL )
                    {
                    // InternalStructuredTextParser.g:3051:6: ( BOOL )
                    // InternalStructuredTextParser.g:3052:1: BOOL
                    {
                     before(grammarAccess.getType_NameAccess().getBOOLKeyword_25()); 
                    match(input,BOOL,FOLLOW_2); 
                     after(grammarAccess.getType_NameAccess().getBOOLKeyword_25()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type_Name__Alternatives"


    // $ANTLR start "rule__And_Operator__Alternatives"
    // InternalStructuredTextParser.g:3064:1: rule__And_Operator__Alternatives : ( ( ( AND ) ) | ( ( Ampersand ) ) );
    public final void rule__And_Operator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3068:1: ( ( ( AND ) ) | ( ( Ampersand ) ) )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==AND) ) {
                alt21=1;
            }
            else if ( (LA21_0==Ampersand) ) {
                alt21=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // InternalStructuredTextParser.g:3069:1: ( ( AND ) )
                    {
                    // InternalStructuredTextParser.g:3069:1: ( ( AND ) )
                    // InternalStructuredTextParser.g:3070:1: ( AND )
                    {
                     before(grammarAccess.getAnd_OperatorAccess().getANDEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3071:1: ( AND )
                    // InternalStructuredTextParser.g:3071:3: AND
                    {
                    match(input,AND,FOLLOW_2); 

                    }

                     after(grammarAccess.getAnd_OperatorAccess().getANDEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3076:6: ( ( Ampersand ) )
                    {
                    // InternalStructuredTextParser.g:3076:6: ( ( Ampersand ) )
                    // InternalStructuredTextParser.g:3077:1: ( Ampersand )
                    {
                     before(grammarAccess.getAnd_OperatorAccess().getANDEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3078:1: ( Ampersand )
                    // InternalStructuredTextParser.g:3078:3: Ampersand
                    {
                    match(input,Ampersand,FOLLOW_2); 

                    }

                     after(grammarAccess.getAnd_OperatorAccess().getANDEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And_Operator__Alternatives"


    // $ANTLR start "rule__Compare_Operator__Alternatives"
    // InternalStructuredTextParser.g:3088:1: rule__Compare_Operator__Alternatives : ( ( ( EqualsSign ) ) | ( ( LessThanSignGreaterThanSign ) ) );
    public final void rule__Compare_Operator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3092:1: ( ( ( EqualsSign ) ) | ( ( LessThanSignGreaterThanSign ) ) )
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==EqualsSign) ) {
                alt22=1;
            }
            else if ( (LA22_0==LessThanSignGreaterThanSign) ) {
                alt22=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }
            switch (alt22) {
                case 1 :
                    // InternalStructuredTextParser.g:3093:1: ( ( EqualsSign ) )
                    {
                    // InternalStructuredTextParser.g:3093:1: ( ( EqualsSign ) )
                    // InternalStructuredTextParser.g:3094:1: ( EqualsSign )
                    {
                     before(grammarAccess.getCompare_OperatorAccess().getEQEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3095:1: ( EqualsSign )
                    // InternalStructuredTextParser.g:3095:3: EqualsSign
                    {
                    match(input,EqualsSign,FOLLOW_2); 

                    }

                     after(grammarAccess.getCompare_OperatorAccess().getEQEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3100:6: ( ( LessThanSignGreaterThanSign ) )
                    {
                    // InternalStructuredTextParser.g:3100:6: ( ( LessThanSignGreaterThanSign ) )
                    // InternalStructuredTextParser.g:3101:1: ( LessThanSignGreaterThanSign )
                    {
                     before(grammarAccess.getCompare_OperatorAccess().getNEEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3102:1: ( LessThanSignGreaterThanSign )
                    // InternalStructuredTextParser.g:3102:3: LessThanSignGreaterThanSign
                    {
                    match(input,LessThanSignGreaterThanSign,FOLLOW_2); 

                    }

                     after(grammarAccess.getCompare_OperatorAccess().getNEEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compare_Operator__Alternatives"


    // $ANTLR start "rule__Equ_Operator__Alternatives"
    // InternalStructuredTextParser.g:3112:1: rule__Equ_Operator__Alternatives : ( ( ( LessThanSign ) ) | ( ( LessThanSignEqualsSign ) ) | ( ( GreaterThanSign ) ) | ( ( GreaterThanSignEqualsSign ) ) );
    public final void rule__Equ_Operator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3116:1: ( ( ( LessThanSign ) ) | ( ( LessThanSignEqualsSign ) ) | ( ( GreaterThanSign ) ) | ( ( GreaterThanSignEqualsSign ) ) )
            int alt23=4;
            switch ( input.LA(1) ) {
            case LessThanSign:
                {
                alt23=1;
                }
                break;
            case LessThanSignEqualsSign:
                {
                alt23=2;
                }
                break;
            case GreaterThanSign:
                {
                alt23=3;
                }
                break;
            case GreaterThanSignEqualsSign:
                {
                alt23=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // InternalStructuredTextParser.g:3117:1: ( ( LessThanSign ) )
                    {
                    // InternalStructuredTextParser.g:3117:1: ( ( LessThanSign ) )
                    // InternalStructuredTextParser.g:3118:1: ( LessThanSign )
                    {
                     before(grammarAccess.getEqu_OperatorAccess().getLTEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3119:1: ( LessThanSign )
                    // InternalStructuredTextParser.g:3119:3: LessThanSign
                    {
                    match(input,LessThanSign,FOLLOW_2); 

                    }

                     after(grammarAccess.getEqu_OperatorAccess().getLTEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3124:6: ( ( LessThanSignEqualsSign ) )
                    {
                    // InternalStructuredTextParser.g:3124:6: ( ( LessThanSignEqualsSign ) )
                    // InternalStructuredTextParser.g:3125:1: ( LessThanSignEqualsSign )
                    {
                     before(grammarAccess.getEqu_OperatorAccess().getLEEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3126:1: ( LessThanSignEqualsSign )
                    // InternalStructuredTextParser.g:3126:3: LessThanSignEqualsSign
                    {
                    match(input,LessThanSignEqualsSign,FOLLOW_2); 

                    }

                     after(grammarAccess.getEqu_OperatorAccess().getLEEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:3131:6: ( ( GreaterThanSign ) )
                    {
                    // InternalStructuredTextParser.g:3131:6: ( ( GreaterThanSign ) )
                    // InternalStructuredTextParser.g:3132:1: ( GreaterThanSign )
                    {
                     before(grammarAccess.getEqu_OperatorAccess().getGTEnumLiteralDeclaration_2()); 
                    // InternalStructuredTextParser.g:3133:1: ( GreaterThanSign )
                    // InternalStructuredTextParser.g:3133:3: GreaterThanSign
                    {
                    match(input,GreaterThanSign,FOLLOW_2); 

                    }

                     after(grammarAccess.getEqu_OperatorAccess().getGTEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:3138:6: ( ( GreaterThanSignEqualsSign ) )
                    {
                    // InternalStructuredTextParser.g:3138:6: ( ( GreaterThanSignEqualsSign ) )
                    // InternalStructuredTextParser.g:3139:1: ( GreaterThanSignEqualsSign )
                    {
                     before(grammarAccess.getEqu_OperatorAccess().getGEEnumLiteralDeclaration_3()); 
                    // InternalStructuredTextParser.g:3140:1: ( GreaterThanSignEqualsSign )
                    // InternalStructuredTextParser.g:3140:3: GreaterThanSignEqualsSign
                    {
                    match(input,GreaterThanSignEqualsSign,FOLLOW_2); 

                    }

                     after(grammarAccess.getEqu_OperatorAccess().getGEEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equ_Operator__Alternatives"


    // $ANTLR start "rule__Add_Operator__Alternatives"
    // InternalStructuredTextParser.g:3150:1: rule__Add_Operator__Alternatives : ( ( ( PlusSign ) ) | ( ( HyphenMinus ) ) );
    public final void rule__Add_Operator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3154:1: ( ( ( PlusSign ) ) | ( ( HyphenMinus ) ) )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==PlusSign) ) {
                alt24=1;
            }
            else if ( (LA24_0==HyphenMinus) ) {
                alt24=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }
            switch (alt24) {
                case 1 :
                    // InternalStructuredTextParser.g:3155:1: ( ( PlusSign ) )
                    {
                    // InternalStructuredTextParser.g:3155:1: ( ( PlusSign ) )
                    // InternalStructuredTextParser.g:3156:1: ( PlusSign )
                    {
                     before(grammarAccess.getAdd_OperatorAccess().getADDEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3157:1: ( PlusSign )
                    // InternalStructuredTextParser.g:3157:3: PlusSign
                    {
                    match(input,PlusSign,FOLLOW_2); 

                    }

                     after(grammarAccess.getAdd_OperatorAccess().getADDEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3162:6: ( ( HyphenMinus ) )
                    {
                    // InternalStructuredTextParser.g:3162:6: ( ( HyphenMinus ) )
                    // InternalStructuredTextParser.g:3163:1: ( HyphenMinus )
                    {
                     before(grammarAccess.getAdd_OperatorAccess().getSUBEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3164:1: ( HyphenMinus )
                    // InternalStructuredTextParser.g:3164:3: HyphenMinus
                    {
                    match(input,HyphenMinus,FOLLOW_2); 

                    }

                     after(grammarAccess.getAdd_OperatorAccess().getSUBEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add_Operator__Alternatives"


    // $ANTLR start "rule__Term_Operator__Alternatives"
    // InternalStructuredTextParser.g:3174:1: rule__Term_Operator__Alternatives : ( ( ( Asterisk ) ) | ( ( Solidus ) ) | ( ( MOD ) ) );
    public final void rule__Term_Operator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3178:1: ( ( ( Asterisk ) ) | ( ( Solidus ) ) | ( ( MOD ) ) )
            int alt25=3;
            switch ( input.LA(1) ) {
            case Asterisk:
                {
                alt25=1;
                }
                break;
            case Solidus:
                {
                alt25=2;
                }
                break;
            case MOD:
                {
                alt25=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }

            switch (alt25) {
                case 1 :
                    // InternalStructuredTextParser.g:3179:1: ( ( Asterisk ) )
                    {
                    // InternalStructuredTextParser.g:3179:1: ( ( Asterisk ) )
                    // InternalStructuredTextParser.g:3180:1: ( Asterisk )
                    {
                     before(grammarAccess.getTerm_OperatorAccess().getMULEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3181:1: ( Asterisk )
                    // InternalStructuredTextParser.g:3181:3: Asterisk
                    {
                    match(input,Asterisk,FOLLOW_2); 

                    }

                     after(grammarAccess.getTerm_OperatorAccess().getMULEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3186:6: ( ( Solidus ) )
                    {
                    // InternalStructuredTextParser.g:3186:6: ( ( Solidus ) )
                    // InternalStructuredTextParser.g:3187:1: ( Solidus )
                    {
                     before(grammarAccess.getTerm_OperatorAccess().getDIVEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3188:1: ( Solidus )
                    // InternalStructuredTextParser.g:3188:3: Solidus
                    {
                    match(input,Solidus,FOLLOW_2); 

                    }

                     after(grammarAccess.getTerm_OperatorAccess().getDIVEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:3193:6: ( ( MOD ) )
                    {
                    // InternalStructuredTextParser.g:3193:6: ( ( MOD ) )
                    // InternalStructuredTextParser.g:3194:1: ( MOD )
                    {
                     before(grammarAccess.getTerm_OperatorAccess().getMODEnumLiteralDeclaration_2()); 
                    // InternalStructuredTextParser.g:3195:1: ( MOD )
                    // InternalStructuredTextParser.g:3195:3: MOD
                    {
                    match(input,MOD,FOLLOW_2); 

                    }

                     after(grammarAccess.getTerm_OperatorAccess().getMODEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term_Operator__Alternatives"


    // $ANTLR start "rule__Unary_Operator__Alternatives"
    // InternalStructuredTextParser.g:3205:1: rule__Unary_Operator__Alternatives : ( ( ( HyphenMinus ) ) | ( ( PlusSign ) ) | ( ( NOT ) ) );
    public final void rule__Unary_Operator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3209:1: ( ( ( HyphenMinus ) ) | ( ( PlusSign ) ) | ( ( NOT ) ) )
            int alt26=3;
            switch ( input.LA(1) ) {
            case HyphenMinus:
                {
                alt26=1;
                }
                break;
            case PlusSign:
                {
                alt26=2;
                }
                break;
            case NOT:
                {
                alt26=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }

            switch (alt26) {
                case 1 :
                    // InternalStructuredTextParser.g:3210:1: ( ( HyphenMinus ) )
                    {
                    // InternalStructuredTextParser.g:3210:1: ( ( HyphenMinus ) )
                    // InternalStructuredTextParser.g:3211:1: ( HyphenMinus )
                    {
                     before(grammarAccess.getUnary_OperatorAccess().getMINUSEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3212:1: ( HyphenMinus )
                    // InternalStructuredTextParser.g:3212:3: HyphenMinus
                    {
                    match(input,HyphenMinus,FOLLOW_2); 

                    }

                     after(grammarAccess.getUnary_OperatorAccess().getMINUSEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3217:6: ( ( PlusSign ) )
                    {
                    // InternalStructuredTextParser.g:3217:6: ( ( PlusSign ) )
                    // InternalStructuredTextParser.g:3218:1: ( PlusSign )
                    {
                     before(grammarAccess.getUnary_OperatorAccess().getPLUSEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3219:1: ( PlusSign )
                    // InternalStructuredTextParser.g:3219:3: PlusSign
                    {
                    match(input,PlusSign,FOLLOW_2); 

                    }

                     after(grammarAccess.getUnary_OperatorAccess().getPLUSEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:3224:6: ( ( NOT ) )
                    {
                    // InternalStructuredTextParser.g:3224:6: ( ( NOT ) )
                    // InternalStructuredTextParser.g:3225:1: ( NOT )
                    {
                     before(grammarAccess.getUnary_OperatorAccess().getNOTEnumLiteralDeclaration_2()); 
                    // InternalStructuredTextParser.g:3226:1: ( NOT )
                    // InternalStructuredTextParser.g:3226:3: NOT
                    {
                    match(input,NOT,FOLLOW_2); 

                    }

                     after(grammarAccess.getUnary_OperatorAccess().getNOTEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_Operator__Alternatives"


    // $ANTLR start "rule__Duration_Unit__Alternatives"
    // InternalStructuredTextParser.g:3236:1: rule__Duration_Unit__Alternatives : ( ( ( D_1 ) ) | ( ( H ) ) | ( ( M ) ) | ( ( S ) ) | ( ( Ms ) ) | ( ( Us ) ) | ( ( Ns ) ) );
    public final void rule__Duration_Unit__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3240:1: ( ( ( D_1 ) ) | ( ( H ) ) | ( ( M ) ) | ( ( S ) ) | ( ( Ms ) ) | ( ( Us ) ) | ( ( Ns ) ) )
            int alt27=7;
            switch ( input.LA(1) ) {
            case D_1:
                {
                alt27=1;
                }
                break;
            case H:
                {
                alt27=2;
                }
                break;
            case M:
                {
                alt27=3;
                }
                break;
            case S:
                {
                alt27=4;
                }
                break;
            case Ms:
                {
                alt27=5;
                }
                break;
            case Us:
                {
                alt27=6;
                }
                break;
            case Ns:
                {
                alt27=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }

            switch (alt27) {
                case 1 :
                    // InternalStructuredTextParser.g:3241:1: ( ( D_1 ) )
                    {
                    // InternalStructuredTextParser.g:3241:1: ( ( D_1 ) )
                    // InternalStructuredTextParser.g:3242:1: ( D_1 )
                    {
                     before(grammarAccess.getDuration_UnitAccess().getDAYSEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3243:1: ( D_1 )
                    // InternalStructuredTextParser.g:3243:3: D_1
                    {
                    match(input,D_1,FOLLOW_2); 

                    }

                     after(grammarAccess.getDuration_UnitAccess().getDAYSEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3248:6: ( ( H ) )
                    {
                    // InternalStructuredTextParser.g:3248:6: ( ( H ) )
                    // InternalStructuredTextParser.g:3249:1: ( H )
                    {
                     before(grammarAccess.getDuration_UnitAccess().getHOURSEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3250:1: ( H )
                    // InternalStructuredTextParser.g:3250:3: H
                    {
                    match(input,H,FOLLOW_2); 

                    }

                     after(grammarAccess.getDuration_UnitAccess().getHOURSEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:3255:6: ( ( M ) )
                    {
                    // InternalStructuredTextParser.g:3255:6: ( ( M ) )
                    // InternalStructuredTextParser.g:3256:1: ( M )
                    {
                     before(grammarAccess.getDuration_UnitAccess().getMINUTESEnumLiteralDeclaration_2()); 
                    // InternalStructuredTextParser.g:3257:1: ( M )
                    // InternalStructuredTextParser.g:3257:3: M
                    {
                    match(input,M,FOLLOW_2); 

                    }

                     after(grammarAccess.getDuration_UnitAccess().getMINUTESEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:3262:6: ( ( S ) )
                    {
                    // InternalStructuredTextParser.g:3262:6: ( ( S ) )
                    // InternalStructuredTextParser.g:3263:1: ( S )
                    {
                     before(grammarAccess.getDuration_UnitAccess().getSECONDSEnumLiteralDeclaration_3()); 
                    // InternalStructuredTextParser.g:3264:1: ( S )
                    // InternalStructuredTextParser.g:3264:3: S
                    {
                    match(input,S,FOLLOW_2); 

                    }

                     after(grammarAccess.getDuration_UnitAccess().getSECONDSEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalStructuredTextParser.g:3269:6: ( ( Ms ) )
                    {
                    // InternalStructuredTextParser.g:3269:6: ( ( Ms ) )
                    // InternalStructuredTextParser.g:3270:1: ( Ms )
                    {
                     before(grammarAccess.getDuration_UnitAccess().getMILLISEnumLiteralDeclaration_4()); 
                    // InternalStructuredTextParser.g:3271:1: ( Ms )
                    // InternalStructuredTextParser.g:3271:3: Ms
                    {
                    match(input,Ms,FOLLOW_2); 

                    }

                     after(grammarAccess.getDuration_UnitAccess().getMILLISEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalStructuredTextParser.g:3276:6: ( ( Us ) )
                    {
                    // InternalStructuredTextParser.g:3276:6: ( ( Us ) )
                    // InternalStructuredTextParser.g:3277:1: ( Us )
                    {
                     before(grammarAccess.getDuration_UnitAccess().getMICROSEnumLiteralDeclaration_5()); 
                    // InternalStructuredTextParser.g:3278:1: ( Us )
                    // InternalStructuredTextParser.g:3278:3: Us
                    {
                    match(input,Us,FOLLOW_2); 

                    }

                     after(grammarAccess.getDuration_UnitAccess().getMICROSEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalStructuredTextParser.g:3283:6: ( ( Ns ) )
                    {
                    // InternalStructuredTextParser.g:3283:6: ( ( Ns ) )
                    // InternalStructuredTextParser.g:3284:1: ( Ns )
                    {
                     before(grammarAccess.getDuration_UnitAccess().getNANOSEnumLiteralDeclaration_6()); 
                    // InternalStructuredTextParser.g:3285:1: ( Ns )
                    // InternalStructuredTextParser.g:3285:3: Ns
                    {
                    match(input,Ns,FOLLOW_2); 

                    }

                     after(grammarAccess.getDuration_UnitAccess().getNANOSEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration_Unit__Alternatives"


    // $ANTLR start "rule__Int_Type_Name__Alternatives"
    // InternalStructuredTextParser.g:3295:1: rule__Int_Type_Name__Alternatives : ( ( ( DINT ) ) | ( ( INT ) ) | ( ( SINT ) ) | ( ( LINT ) ) | ( ( UINT ) ) | ( ( USINT ) ) | ( ( UDINT ) ) | ( ( ULINT ) ) );
    public final void rule__Int_Type_Name__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3299:1: ( ( ( DINT ) ) | ( ( INT ) ) | ( ( SINT ) ) | ( ( LINT ) ) | ( ( UINT ) ) | ( ( USINT ) ) | ( ( UDINT ) ) | ( ( ULINT ) ) )
            int alt28=8;
            switch ( input.LA(1) ) {
            case DINT:
                {
                alt28=1;
                }
                break;
            case INT:
                {
                alt28=2;
                }
                break;
            case SINT:
                {
                alt28=3;
                }
                break;
            case LINT:
                {
                alt28=4;
                }
                break;
            case UINT:
                {
                alt28=5;
                }
                break;
            case USINT:
                {
                alt28=6;
                }
                break;
            case UDINT:
                {
                alt28=7;
                }
                break;
            case ULINT:
                {
                alt28=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }

            switch (alt28) {
                case 1 :
                    // InternalStructuredTextParser.g:3300:1: ( ( DINT ) )
                    {
                    // InternalStructuredTextParser.g:3300:1: ( ( DINT ) )
                    // InternalStructuredTextParser.g:3301:1: ( DINT )
                    {
                     before(grammarAccess.getInt_Type_NameAccess().getDINTEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3302:1: ( DINT )
                    // InternalStructuredTextParser.g:3302:3: DINT
                    {
                    match(input,DINT,FOLLOW_2); 

                    }

                     after(grammarAccess.getInt_Type_NameAccess().getDINTEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3307:6: ( ( INT ) )
                    {
                    // InternalStructuredTextParser.g:3307:6: ( ( INT ) )
                    // InternalStructuredTextParser.g:3308:1: ( INT )
                    {
                     before(grammarAccess.getInt_Type_NameAccess().getINTEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3309:1: ( INT )
                    // InternalStructuredTextParser.g:3309:3: INT
                    {
                    match(input,INT,FOLLOW_2); 

                    }

                     after(grammarAccess.getInt_Type_NameAccess().getINTEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:3314:6: ( ( SINT ) )
                    {
                    // InternalStructuredTextParser.g:3314:6: ( ( SINT ) )
                    // InternalStructuredTextParser.g:3315:1: ( SINT )
                    {
                     before(grammarAccess.getInt_Type_NameAccess().getSINTEnumLiteralDeclaration_2()); 
                    // InternalStructuredTextParser.g:3316:1: ( SINT )
                    // InternalStructuredTextParser.g:3316:3: SINT
                    {
                    match(input,SINT,FOLLOW_2); 

                    }

                     after(grammarAccess.getInt_Type_NameAccess().getSINTEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:3321:6: ( ( LINT ) )
                    {
                    // InternalStructuredTextParser.g:3321:6: ( ( LINT ) )
                    // InternalStructuredTextParser.g:3322:1: ( LINT )
                    {
                     before(grammarAccess.getInt_Type_NameAccess().getLINTEnumLiteralDeclaration_3()); 
                    // InternalStructuredTextParser.g:3323:1: ( LINT )
                    // InternalStructuredTextParser.g:3323:3: LINT
                    {
                    match(input,LINT,FOLLOW_2); 

                    }

                     after(grammarAccess.getInt_Type_NameAccess().getLINTEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalStructuredTextParser.g:3328:6: ( ( UINT ) )
                    {
                    // InternalStructuredTextParser.g:3328:6: ( ( UINT ) )
                    // InternalStructuredTextParser.g:3329:1: ( UINT )
                    {
                     before(grammarAccess.getInt_Type_NameAccess().getUINTEnumLiteralDeclaration_4()); 
                    // InternalStructuredTextParser.g:3330:1: ( UINT )
                    // InternalStructuredTextParser.g:3330:3: UINT
                    {
                    match(input,UINT,FOLLOW_2); 

                    }

                     after(grammarAccess.getInt_Type_NameAccess().getUINTEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalStructuredTextParser.g:3335:6: ( ( USINT ) )
                    {
                    // InternalStructuredTextParser.g:3335:6: ( ( USINT ) )
                    // InternalStructuredTextParser.g:3336:1: ( USINT )
                    {
                     before(grammarAccess.getInt_Type_NameAccess().getUSINTEnumLiteralDeclaration_5()); 
                    // InternalStructuredTextParser.g:3337:1: ( USINT )
                    // InternalStructuredTextParser.g:3337:3: USINT
                    {
                    match(input,USINT,FOLLOW_2); 

                    }

                     after(grammarAccess.getInt_Type_NameAccess().getUSINTEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalStructuredTextParser.g:3342:6: ( ( UDINT ) )
                    {
                    // InternalStructuredTextParser.g:3342:6: ( ( UDINT ) )
                    // InternalStructuredTextParser.g:3343:1: ( UDINT )
                    {
                     before(grammarAccess.getInt_Type_NameAccess().getUDINTEnumLiteralDeclaration_6()); 
                    // InternalStructuredTextParser.g:3344:1: ( UDINT )
                    // InternalStructuredTextParser.g:3344:3: UDINT
                    {
                    match(input,UDINT,FOLLOW_2); 

                    }

                     after(grammarAccess.getInt_Type_NameAccess().getUDINTEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalStructuredTextParser.g:3349:6: ( ( ULINT ) )
                    {
                    // InternalStructuredTextParser.g:3349:6: ( ( ULINT ) )
                    // InternalStructuredTextParser.g:3350:1: ( ULINT )
                    {
                     before(grammarAccess.getInt_Type_NameAccess().getULINTEnumLiteralDeclaration_7()); 
                    // InternalStructuredTextParser.g:3351:1: ( ULINT )
                    // InternalStructuredTextParser.g:3351:3: ULINT
                    {
                    match(input,ULINT,FOLLOW_2); 

                    }

                     after(grammarAccess.getInt_Type_NameAccess().getULINTEnumLiteralDeclaration_7()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Int_Type_Name__Alternatives"


    // $ANTLR start "rule__Real_Type_Name__Alternatives"
    // InternalStructuredTextParser.g:3361:1: rule__Real_Type_Name__Alternatives : ( ( ( REAL ) ) | ( ( LREAL ) ) );
    public final void rule__Real_Type_Name__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3365:1: ( ( ( REAL ) ) | ( ( LREAL ) ) )
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==REAL) ) {
                alt29=1;
            }
            else if ( (LA29_0==LREAL) ) {
                alt29=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }
            switch (alt29) {
                case 1 :
                    // InternalStructuredTextParser.g:3366:1: ( ( REAL ) )
                    {
                    // InternalStructuredTextParser.g:3366:1: ( ( REAL ) )
                    // InternalStructuredTextParser.g:3367:1: ( REAL )
                    {
                     before(grammarAccess.getReal_Type_NameAccess().getREALEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3368:1: ( REAL )
                    // InternalStructuredTextParser.g:3368:3: REAL
                    {
                    match(input,REAL,FOLLOW_2); 

                    }

                     after(grammarAccess.getReal_Type_NameAccess().getREALEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3373:6: ( ( LREAL ) )
                    {
                    // InternalStructuredTextParser.g:3373:6: ( ( LREAL ) )
                    // InternalStructuredTextParser.g:3374:1: ( LREAL )
                    {
                     before(grammarAccess.getReal_Type_NameAccess().getLREALEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3375:1: ( LREAL )
                    // InternalStructuredTextParser.g:3375:3: LREAL
                    {
                    match(input,LREAL,FOLLOW_2); 

                    }

                     after(grammarAccess.getReal_Type_NameAccess().getLREALEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Type_Name__Alternatives"


    // $ANTLR start "rule__String_Type_Name__Alternatives"
    // InternalStructuredTextParser.g:3385:1: rule__String_Type_Name__Alternatives : ( ( ( STRING ) ) | ( ( WSTRING ) ) | ( ( CHAR ) ) | ( ( WCHAR ) ) );
    public final void rule__String_Type_Name__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3389:1: ( ( ( STRING ) ) | ( ( WSTRING ) ) | ( ( CHAR ) ) | ( ( WCHAR ) ) )
            int alt30=4;
            switch ( input.LA(1) ) {
            case STRING:
                {
                alt30=1;
                }
                break;
            case WSTRING:
                {
                alt30=2;
                }
                break;
            case CHAR:
                {
                alt30=3;
                }
                break;
            case WCHAR:
                {
                alt30=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }

            switch (alt30) {
                case 1 :
                    // InternalStructuredTextParser.g:3390:1: ( ( STRING ) )
                    {
                    // InternalStructuredTextParser.g:3390:1: ( ( STRING ) )
                    // InternalStructuredTextParser.g:3391:1: ( STRING )
                    {
                     before(grammarAccess.getString_Type_NameAccess().getSTRINGEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3392:1: ( STRING )
                    // InternalStructuredTextParser.g:3392:3: STRING
                    {
                    match(input,STRING,FOLLOW_2); 

                    }

                     after(grammarAccess.getString_Type_NameAccess().getSTRINGEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3397:6: ( ( WSTRING ) )
                    {
                    // InternalStructuredTextParser.g:3397:6: ( ( WSTRING ) )
                    // InternalStructuredTextParser.g:3398:1: ( WSTRING )
                    {
                     before(grammarAccess.getString_Type_NameAccess().getWSTRINGEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3399:1: ( WSTRING )
                    // InternalStructuredTextParser.g:3399:3: WSTRING
                    {
                    match(input,WSTRING,FOLLOW_2); 

                    }

                     after(grammarAccess.getString_Type_NameAccess().getWSTRINGEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:3404:6: ( ( CHAR ) )
                    {
                    // InternalStructuredTextParser.g:3404:6: ( ( CHAR ) )
                    // InternalStructuredTextParser.g:3405:1: ( CHAR )
                    {
                     before(grammarAccess.getString_Type_NameAccess().getCHAREnumLiteralDeclaration_2()); 
                    // InternalStructuredTextParser.g:3406:1: ( CHAR )
                    // InternalStructuredTextParser.g:3406:3: CHAR
                    {
                    match(input,CHAR,FOLLOW_2); 

                    }

                     after(grammarAccess.getString_Type_NameAccess().getCHAREnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:3411:6: ( ( WCHAR ) )
                    {
                    // InternalStructuredTextParser.g:3411:6: ( ( WCHAR ) )
                    // InternalStructuredTextParser.g:3412:1: ( WCHAR )
                    {
                     before(grammarAccess.getString_Type_NameAccess().getWCHAREnumLiteralDeclaration_3()); 
                    // InternalStructuredTextParser.g:3413:1: ( WCHAR )
                    // InternalStructuredTextParser.g:3413:3: WCHAR
                    {
                    match(input,WCHAR,FOLLOW_2); 

                    }

                     after(grammarAccess.getString_Type_NameAccess().getWCHAREnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__String_Type_Name__Alternatives"


    // $ANTLR start "rule__Time_Type_Name__Alternatives"
    // InternalStructuredTextParser.g:3423:1: rule__Time_Type_Name__Alternatives : ( ( ( TIME ) ) | ( ( LTIME ) ) | ( ( T ) ) | ( ( LT ) ) );
    public final void rule__Time_Type_Name__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3427:1: ( ( ( TIME ) ) | ( ( LTIME ) ) | ( ( T ) ) | ( ( LT ) ) )
            int alt31=4;
            switch ( input.LA(1) ) {
            case TIME:
                {
                alt31=1;
                }
                break;
            case LTIME:
                {
                alt31=2;
                }
                break;
            case T:
                {
                alt31=3;
                }
                break;
            case LT:
                {
                alt31=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }

            switch (alt31) {
                case 1 :
                    // InternalStructuredTextParser.g:3428:1: ( ( TIME ) )
                    {
                    // InternalStructuredTextParser.g:3428:1: ( ( TIME ) )
                    // InternalStructuredTextParser.g:3429:1: ( TIME )
                    {
                     before(grammarAccess.getTime_Type_NameAccess().getTIMEEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3430:1: ( TIME )
                    // InternalStructuredTextParser.g:3430:3: TIME
                    {
                    match(input,TIME,FOLLOW_2); 

                    }

                     after(grammarAccess.getTime_Type_NameAccess().getTIMEEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3435:6: ( ( LTIME ) )
                    {
                    // InternalStructuredTextParser.g:3435:6: ( ( LTIME ) )
                    // InternalStructuredTextParser.g:3436:1: ( LTIME )
                    {
                     before(grammarAccess.getTime_Type_NameAccess().getLTIMEEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3437:1: ( LTIME )
                    // InternalStructuredTextParser.g:3437:3: LTIME
                    {
                    match(input,LTIME,FOLLOW_2); 

                    }

                     after(grammarAccess.getTime_Type_NameAccess().getLTIMEEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:3442:6: ( ( T ) )
                    {
                    // InternalStructuredTextParser.g:3442:6: ( ( T ) )
                    // InternalStructuredTextParser.g:3443:1: ( T )
                    {
                     before(grammarAccess.getTime_Type_NameAccess().getTEnumLiteralDeclaration_2()); 
                    // InternalStructuredTextParser.g:3444:1: ( T )
                    // InternalStructuredTextParser.g:3444:3: T
                    {
                    match(input,T,FOLLOW_2); 

                    }

                     after(grammarAccess.getTime_Type_NameAccess().getTEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:3449:6: ( ( LT ) )
                    {
                    // InternalStructuredTextParser.g:3449:6: ( ( LT ) )
                    // InternalStructuredTextParser.g:3450:1: ( LT )
                    {
                     before(grammarAccess.getTime_Type_NameAccess().getLTEnumLiteralDeclaration_3()); 
                    // InternalStructuredTextParser.g:3451:1: ( LT )
                    // InternalStructuredTextParser.g:3451:3: LT
                    {
                    match(input,LT,FOLLOW_2); 

                    }

                     after(grammarAccess.getTime_Type_NameAccess().getLTEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Time_Type_Name__Alternatives"


    // $ANTLR start "rule__Tod_Type_Name__Alternatives"
    // InternalStructuredTextParser.g:3461:1: rule__Tod_Type_Name__Alternatives : ( ( ( TIME_OF_DAY ) ) | ( ( LTIME_OF_DAY ) ) | ( ( TOD ) ) | ( ( LTOD ) ) );
    public final void rule__Tod_Type_Name__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3465:1: ( ( ( TIME_OF_DAY ) ) | ( ( LTIME_OF_DAY ) ) | ( ( TOD ) ) | ( ( LTOD ) ) )
            int alt32=4;
            switch ( input.LA(1) ) {
            case TIME_OF_DAY:
                {
                alt32=1;
                }
                break;
            case LTIME_OF_DAY:
                {
                alt32=2;
                }
                break;
            case TOD:
                {
                alt32=3;
                }
                break;
            case LTOD:
                {
                alt32=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }

            switch (alt32) {
                case 1 :
                    // InternalStructuredTextParser.g:3466:1: ( ( TIME_OF_DAY ) )
                    {
                    // InternalStructuredTextParser.g:3466:1: ( ( TIME_OF_DAY ) )
                    // InternalStructuredTextParser.g:3467:1: ( TIME_OF_DAY )
                    {
                     before(grammarAccess.getTod_Type_NameAccess().getTIME_OF_DAYEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3468:1: ( TIME_OF_DAY )
                    // InternalStructuredTextParser.g:3468:3: TIME_OF_DAY
                    {
                    match(input,TIME_OF_DAY,FOLLOW_2); 

                    }

                     after(grammarAccess.getTod_Type_NameAccess().getTIME_OF_DAYEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3473:6: ( ( LTIME_OF_DAY ) )
                    {
                    // InternalStructuredTextParser.g:3473:6: ( ( LTIME_OF_DAY ) )
                    // InternalStructuredTextParser.g:3474:1: ( LTIME_OF_DAY )
                    {
                     before(grammarAccess.getTod_Type_NameAccess().getLTIME_OF_DAYEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3475:1: ( LTIME_OF_DAY )
                    // InternalStructuredTextParser.g:3475:3: LTIME_OF_DAY
                    {
                    match(input,LTIME_OF_DAY,FOLLOW_2); 

                    }

                     after(grammarAccess.getTod_Type_NameAccess().getLTIME_OF_DAYEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:3480:6: ( ( TOD ) )
                    {
                    // InternalStructuredTextParser.g:3480:6: ( ( TOD ) )
                    // InternalStructuredTextParser.g:3481:1: ( TOD )
                    {
                     before(grammarAccess.getTod_Type_NameAccess().getTODEnumLiteralDeclaration_2()); 
                    // InternalStructuredTextParser.g:3482:1: ( TOD )
                    // InternalStructuredTextParser.g:3482:3: TOD
                    {
                    match(input,TOD,FOLLOW_2); 

                    }

                     after(grammarAccess.getTod_Type_NameAccess().getTODEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:3487:6: ( ( LTOD ) )
                    {
                    // InternalStructuredTextParser.g:3487:6: ( ( LTOD ) )
                    // InternalStructuredTextParser.g:3488:1: ( LTOD )
                    {
                     before(grammarAccess.getTod_Type_NameAccess().getLTODEnumLiteralDeclaration_3()); 
                    // InternalStructuredTextParser.g:3489:1: ( LTOD )
                    // InternalStructuredTextParser.g:3489:3: LTOD
                    {
                    match(input,LTOD,FOLLOW_2); 

                    }

                     after(grammarAccess.getTod_Type_NameAccess().getLTODEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Tod_Type_Name__Alternatives"


    // $ANTLR start "rule__Date_Type_Name__Alternatives"
    // InternalStructuredTextParser.g:3499:1: rule__Date_Type_Name__Alternatives : ( ( ( DATE ) ) | ( ( LDATE ) ) | ( ( D_1 ) ) | ( ( LD ) ) );
    public final void rule__Date_Type_Name__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3503:1: ( ( ( DATE ) ) | ( ( LDATE ) ) | ( ( D_1 ) ) | ( ( LD ) ) )
            int alt33=4;
            switch ( input.LA(1) ) {
            case DATE:
                {
                alt33=1;
                }
                break;
            case LDATE:
                {
                alt33=2;
                }
                break;
            case D_1:
                {
                alt33=3;
                }
                break;
            case LD:
                {
                alt33=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }

            switch (alt33) {
                case 1 :
                    // InternalStructuredTextParser.g:3504:1: ( ( DATE ) )
                    {
                    // InternalStructuredTextParser.g:3504:1: ( ( DATE ) )
                    // InternalStructuredTextParser.g:3505:1: ( DATE )
                    {
                     before(grammarAccess.getDate_Type_NameAccess().getDATEEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3506:1: ( DATE )
                    // InternalStructuredTextParser.g:3506:3: DATE
                    {
                    match(input,DATE,FOLLOW_2); 

                    }

                     after(grammarAccess.getDate_Type_NameAccess().getDATEEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3511:6: ( ( LDATE ) )
                    {
                    // InternalStructuredTextParser.g:3511:6: ( ( LDATE ) )
                    // InternalStructuredTextParser.g:3512:1: ( LDATE )
                    {
                     before(grammarAccess.getDate_Type_NameAccess().getLDATEEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3513:1: ( LDATE )
                    // InternalStructuredTextParser.g:3513:3: LDATE
                    {
                    match(input,LDATE,FOLLOW_2); 

                    }

                     after(grammarAccess.getDate_Type_NameAccess().getLDATEEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:3518:6: ( ( D_1 ) )
                    {
                    // InternalStructuredTextParser.g:3518:6: ( ( D_1 ) )
                    // InternalStructuredTextParser.g:3519:1: ( D_1 )
                    {
                     before(grammarAccess.getDate_Type_NameAccess().getDEnumLiteralDeclaration_2()); 
                    // InternalStructuredTextParser.g:3520:1: ( D_1 )
                    // InternalStructuredTextParser.g:3520:3: D_1
                    {
                    match(input,D_1,FOLLOW_2); 

                    }

                     after(grammarAccess.getDate_Type_NameAccess().getDEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:3525:6: ( ( LD ) )
                    {
                    // InternalStructuredTextParser.g:3525:6: ( ( LD ) )
                    // InternalStructuredTextParser.g:3526:1: ( LD )
                    {
                     before(grammarAccess.getDate_Type_NameAccess().getLDEnumLiteralDeclaration_3()); 
                    // InternalStructuredTextParser.g:3527:1: ( LD )
                    // InternalStructuredTextParser.g:3527:3: LD
                    {
                    match(input,LD,FOLLOW_2); 

                    }

                     after(grammarAccess.getDate_Type_NameAccess().getLDEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_Type_Name__Alternatives"


    // $ANTLR start "rule__DT_Type_Name__Alternatives"
    // InternalStructuredTextParser.g:3537:1: rule__DT_Type_Name__Alternatives : ( ( ( DATE_AND_TIME ) ) | ( ( LDATE_AND_TIME ) ) | ( ( DT ) ) | ( ( LDT ) ) );
    public final void rule__DT_Type_Name__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3541:1: ( ( ( DATE_AND_TIME ) ) | ( ( LDATE_AND_TIME ) ) | ( ( DT ) ) | ( ( LDT ) ) )
            int alt34=4;
            switch ( input.LA(1) ) {
            case DATE_AND_TIME:
                {
                alt34=1;
                }
                break;
            case LDATE_AND_TIME:
                {
                alt34=2;
                }
                break;
            case DT:
                {
                alt34=3;
                }
                break;
            case LDT:
                {
                alt34=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 34, 0, input);

                throw nvae;
            }

            switch (alt34) {
                case 1 :
                    // InternalStructuredTextParser.g:3542:1: ( ( DATE_AND_TIME ) )
                    {
                    // InternalStructuredTextParser.g:3542:1: ( ( DATE_AND_TIME ) )
                    // InternalStructuredTextParser.g:3543:1: ( DATE_AND_TIME )
                    {
                     before(grammarAccess.getDT_Type_NameAccess().getDATE_AND_TIMEEnumLiteralDeclaration_0()); 
                    // InternalStructuredTextParser.g:3544:1: ( DATE_AND_TIME )
                    // InternalStructuredTextParser.g:3544:3: DATE_AND_TIME
                    {
                    match(input,DATE_AND_TIME,FOLLOW_2); 

                    }

                     after(grammarAccess.getDT_Type_NameAccess().getDATE_AND_TIMEEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalStructuredTextParser.g:3549:6: ( ( LDATE_AND_TIME ) )
                    {
                    // InternalStructuredTextParser.g:3549:6: ( ( LDATE_AND_TIME ) )
                    // InternalStructuredTextParser.g:3550:1: ( LDATE_AND_TIME )
                    {
                     before(grammarAccess.getDT_Type_NameAccess().getLDATE_AND_TIMEEnumLiteralDeclaration_1()); 
                    // InternalStructuredTextParser.g:3551:1: ( LDATE_AND_TIME )
                    // InternalStructuredTextParser.g:3551:3: LDATE_AND_TIME
                    {
                    match(input,LDATE_AND_TIME,FOLLOW_2); 

                    }

                     after(grammarAccess.getDT_Type_NameAccess().getLDATE_AND_TIMEEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalStructuredTextParser.g:3556:6: ( ( DT ) )
                    {
                    // InternalStructuredTextParser.g:3556:6: ( ( DT ) )
                    // InternalStructuredTextParser.g:3557:1: ( DT )
                    {
                     before(grammarAccess.getDT_Type_NameAccess().getDTEnumLiteralDeclaration_2()); 
                    // InternalStructuredTextParser.g:3558:1: ( DT )
                    // InternalStructuredTextParser.g:3558:3: DT
                    {
                    match(input,DT,FOLLOW_2); 

                    }

                     after(grammarAccess.getDT_Type_NameAccess().getDTEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalStructuredTextParser.g:3563:6: ( ( LDT ) )
                    {
                    // InternalStructuredTextParser.g:3563:6: ( ( LDT ) )
                    // InternalStructuredTextParser.g:3564:1: ( LDT )
                    {
                     before(grammarAccess.getDT_Type_NameAccess().getLDTEnumLiteralDeclaration_3()); 
                    // InternalStructuredTextParser.g:3565:1: ( LDT )
                    // InternalStructuredTextParser.g:3565:3: LDT
                    {
                    match(input,LDT,FOLLOW_2); 

                    }

                     after(grammarAccess.getDT_Type_NameAccess().getLDTEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DT_Type_Name__Alternatives"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group__0"
    // InternalStructuredTextParser.g:3578:1: rule__StructuredTextAlgorithm__Group__0 : rule__StructuredTextAlgorithm__Group__0__Impl rule__StructuredTextAlgorithm__Group__1 ;
    public final void rule__StructuredTextAlgorithm__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3582:1: ( rule__StructuredTextAlgorithm__Group__0__Impl rule__StructuredTextAlgorithm__Group__1 )
            // InternalStructuredTextParser.g:3583:2: rule__StructuredTextAlgorithm__Group__0__Impl rule__StructuredTextAlgorithm__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__StructuredTextAlgorithm__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructuredTextAlgorithm__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group__0"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group__0__Impl"
    // InternalStructuredTextParser.g:3590:1: rule__StructuredTextAlgorithm__Group__0__Impl : ( () ) ;
    public final void rule__StructuredTextAlgorithm__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3594:1: ( ( () ) )
            // InternalStructuredTextParser.g:3595:1: ( () )
            {
            // InternalStructuredTextParser.g:3595:1: ( () )
            // InternalStructuredTextParser.g:3596:1: ()
            {
             before(grammarAccess.getStructuredTextAlgorithmAccess().getStructuredTextAlgorithmAction_0()); 
            // InternalStructuredTextParser.g:3597:1: ()
            // InternalStructuredTextParser.g:3599:1: 
            {
            }

             after(grammarAccess.getStructuredTextAlgorithmAccess().getStructuredTextAlgorithmAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group__0__Impl"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group__1"
    // InternalStructuredTextParser.g:3609:1: rule__StructuredTextAlgorithm__Group__1 : rule__StructuredTextAlgorithm__Group__1__Impl rule__StructuredTextAlgorithm__Group__2 ;
    public final void rule__StructuredTextAlgorithm__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3613:1: ( rule__StructuredTextAlgorithm__Group__1__Impl rule__StructuredTextAlgorithm__Group__2 )
            // InternalStructuredTextParser.g:3614:2: rule__StructuredTextAlgorithm__Group__1__Impl rule__StructuredTextAlgorithm__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__StructuredTextAlgorithm__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructuredTextAlgorithm__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group__1"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group__1__Impl"
    // InternalStructuredTextParser.g:3621:1: rule__StructuredTextAlgorithm__Group__1__Impl : ( ( rule__StructuredTextAlgorithm__Group_1__0 )? ) ;
    public final void rule__StructuredTextAlgorithm__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3625:1: ( ( ( rule__StructuredTextAlgorithm__Group_1__0 )? ) )
            // InternalStructuredTextParser.g:3626:1: ( ( rule__StructuredTextAlgorithm__Group_1__0 )? )
            {
            // InternalStructuredTextParser.g:3626:1: ( ( rule__StructuredTextAlgorithm__Group_1__0 )? )
            // InternalStructuredTextParser.g:3627:1: ( rule__StructuredTextAlgorithm__Group_1__0 )?
            {
             before(grammarAccess.getStructuredTextAlgorithmAccess().getGroup_1()); 
            // InternalStructuredTextParser.g:3628:1: ( rule__StructuredTextAlgorithm__Group_1__0 )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==VAR) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalStructuredTextParser.g:3628:2: rule__StructuredTextAlgorithm__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__StructuredTextAlgorithm__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getStructuredTextAlgorithmAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group__1__Impl"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group__2"
    // InternalStructuredTextParser.g:3638:1: rule__StructuredTextAlgorithm__Group__2 : rule__StructuredTextAlgorithm__Group__2__Impl ;
    public final void rule__StructuredTextAlgorithm__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3642:1: ( rule__StructuredTextAlgorithm__Group__2__Impl )
            // InternalStructuredTextParser.g:3643:2: rule__StructuredTextAlgorithm__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StructuredTextAlgorithm__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group__2"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group__2__Impl"
    // InternalStructuredTextParser.g:3649:1: rule__StructuredTextAlgorithm__Group__2__Impl : ( ( rule__StructuredTextAlgorithm__StatementsAssignment_2 ) ) ;
    public final void rule__StructuredTextAlgorithm__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3653:1: ( ( ( rule__StructuredTextAlgorithm__StatementsAssignment_2 ) ) )
            // InternalStructuredTextParser.g:3654:1: ( ( rule__StructuredTextAlgorithm__StatementsAssignment_2 ) )
            {
            // InternalStructuredTextParser.g:3654:1: ( ( rule__StructuredTextAlgorithm__StatementsAssignment_2 ) )
            // InternalStructuredTextParser.g:3655:1: ( rule__StructuredTextAlgorithm__StatementsAssignment_2 )
            {
             before(grammarAccess.getStructuredTextAlgorithmAccess().getStatementsAssignment_2()); 
            // InternalStructuredTextParser.g:3656:1: ( rule__StructuredTextAlgorithm__StatementsAssignment_2 )
            // InternalStructuredTextParser.g:3656:2: rule__StructuredTextAlgorithm__StatementsAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__StructuredTextAlgorithm__StatementsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getStructuredTextAlgorithmAccess().getStatementsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group__2__Impl"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group_1__0"
    // InternalStructuredTextParser.g:3672:1: rule__StructuredTextAlgorithm__Group_1__0 : rule__StructuredTextAlgorithm__Group_1__0__Impl rule__StructuredTextAlgorithm__Group_1__1 ;
    public final void rule__StructuredTextAlgorithm__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3676:1: ( rule__StructuredTextAlgorithm__Group_1__0__Impl rule__StructuredTextAlgorithm__Group_1__1 )
            // InternalStructuredTextParser.g:3677:2: rule__StructuredTextAlgorithm__Group_1__0__Impl rule__StructuredTextAlgorithm__Group_1__1
            {
            pushFollow(FOLLOW_4);
            rule__StructuredTextAlgorithm__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructuredTextAlgorithm__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group_1__0"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group_1__0__Impl"
    // InternalStructuredTextParser.g:3684:1: rule__StructuredTextAlgorithm__Group_1__0__Impl : ( VAR ) ;
    public final void rule__StructuredTextAlgorithm__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3688:1: ( ( VAR ) )
            // InternalStructuredTextParser.g:3689:1: ( VAR )
            {
            // InternalStructuredTextParser.g:3689:1: ( VAR )
            // InternalStructuredTextParser.g:3690:1: VAR
            {
             before(grammarAccess.getStructuredTextAlgorithmAccess().getVARKeyword_1_0()); 
            match(input,VAR,FOLLOW_2); 
             after(grammarAccess.getStructuredTextAlgorithmAccess().getVARKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group_1__0__Impl"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group_1__1"
    // InternalStructuredTextParser.g:3703:1: rule__StructuredTextAlgorithm__Group_1__1 : rule__StructuredTextAlgorithm__Group_1__1__Impl rule__StructuredTextAlgorithm__Group_1__2 ;
    public final void rule__StructuredTextAlgorithm__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3707:1: ( rule__StructuredTextAlgorithm__Group_1__1__Impl rule__StructuredTextAlgorithm__Group_1__2 )
            // InternalStructuredTextParser.g:3708:2: rule__StructuredTextAlgorithm__Group_1__1__Impl rule__StructuredTextAlgorithm__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__StructuredTextAlgorithm__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructuredTextAlgorithm__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group_1__1"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group_1__1__Impl"
    // InternalStructuredTextParser.g:3715:1: rule__StructuredTextAlgorithm__Group_1__1__Impl : ( ( rule__StructuredTextAlgorithm__Group_1_1__0 )* ) ;
    public final void rule__StructuredTextAlgorithm__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3719:1: ( ( ( rule__StructuredTextAlgorithm__Group_1_1__0 )* ) )
            // InternalStructuredTextParser.g:3720:1: ( ( rule__StructuredTextAlgorithm__Group_1_1__0 )* )
            {
            // InternalStructuredTextParser.g:3720:1: ( ( rule__StructuredTextAlgorithm__Group_1_1__0 )* )
            // InternalStructuredTextParser.g:3721:1: ( rule__StructuredTextAlgorithm__Group_1_1__0 )*
            {
             before(grammarAccess.getStructuredTextAlgorithmAccess().getGroup_1_1()); 
            // InternalStructuredTextParser.g:3722:1: ( rule__StructuredTextAlgorithm__Group_1_1__0 )*
            loop36:
            do {
                int alt36=2;
                int LA36_0 = input.LA(1);

                if ( (LA36_0==CONSTANT||LA36_0==RULE_ID) ) {
                    alt36=1;
                }


                switch (alt36) {
            	case 1 :
            	    // InternalStructuredTextParser.g:3722:2: rule__StructuredTextAlgorithm__Group_1_1__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__StructuredTextAlgorithm__Group_1_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop36;
                }
            } while (true);

             after(grammarAccess.getStructuredTextAlgorithmAccess().getGroup_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group_1__1__Impl"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group_1__2"
    // InternalStructuredTextParser.g:3732:1: rule__StructuredTextAlgorithm__Group_1__2 : rule__StructuredTextAlgorithm__Group_1__2__Impl ;
    public final void rule__StructuredTextAlgorithm__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3736:1: ( rule__StructuredTextAlgorithm__Group_1__2__Impl )
            // InternalStructuredTextParser.g:3737:2: rule__StructuredTextAlgorithm__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StructuredTextAlgorithm__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group_1__2"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group_1__2__Impl"
    // InternalStructuredTextParser.g:3743:1: rule__StructuredTextAlgorithm__Group_1__2__Impl : ( END_VAR ) ;
    public final void rule__StructuredTextAlgorithm__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3747:1: ( ( END_VAR ) )
            // InternalStructuredTextParser.g:3748:1: ( END_VAR )
            {
            // InternalStructuredTextParser.g:3748:1: ( END_VAR )
            // InternalStructuredTextParser.g:3749:1: END_VAR
            {
             before(grammarAccess.getStructuredTextAlgorithmAccess().getEND_VARKeyword_1_2()); 
            match(input,END_VAR,FOLLOW_2); 
             after(grammarAccess.getStructuredTextAlgorithmAccess().getEND_VARKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group_1__2__Impl"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group_1_1__0"
    // InternalStructuredTextParser.g:3768:1: rule__StructuredTextAlgorithm__Group_1_1__0 : rule__StructuredTextAlgorithm__Group_1_1__0__Impl rule__StructuredTextAlgorithm__Group_1_1__1 ;
    public final void rule__StructuredTextAlgorithm__Group_1_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3772:1: ( rule__StructuredTextAlgorithm__Group_1_1__0__Impl rule__StructuredTextAlgorithm__Group_1_1__1 )
            // InternalStructuredTextParser.g:3773:2: rule__StructuredTextAlgorithm__Group_1_1__0__Impl rule__StructuredTextAlgorithm__Group_1_1__1
            {
            pushFollow(FOLLOW_6);
            rule__StructuredTextAlgorithm__Group_1_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructuredTextAlgorithm__Group_1_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group_1_1__0"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group_1_1__0__Impl"
    // InternalStructuredTextParser.g:3780:1: rule__StructuredTextAlgorithm__Group_1_1__0__Impl : ( ( rule__StructuredTextAlgorithm__LocalVariablesAssignment_1_1_0 ) ) ;
    public final void rule__StructuredTextAlgorithm__Group_1_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3784:1: ( ( ( rule__StructuredTextAlgorithm__LocalVariablesAssignment_1_1_0 ) ) )
            // InternalStructuredTextParser.g:3785:1: ( ( rule__StructuredTextAlgorithm__LocalVariablesAssignment_1_1_0 ) )
            {
            // InternalStructuredTextParser.g:3785:1: ( ( rule__StructuredTextAlgorithm__LocalVariablesAssignment_1_1_0 ) )
            // InternalStructuredTextParser.g:3786:1: ( rule__StructuredTextAlgorithm__LocalVariablesAssignment_1_1_0 )
            {
             before(grammarAccess.getStructuredTextAlgorithmAccess().getLocalVariablesAssignment_1_1_0()); 
            // InternalStructuredTextParser.g:3787:1: ( rule__StructuredTextAlgorithm__LocalVariablesAssignment_1_1_0 )
            // InternalStructuredTextParser.g:3787:2: rule__StructuredTextAlgorithm__LocalVariablesAssignment_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__StructuredTextAlgorithm__LocalVariablesAssignment_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getStructuredTextAlgorithmAccess().getLocalVariablesAssignment_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group_1_1__0__Impl"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group_1_1__1"
    // InternalStructuredTextParser.g:3797:1: rule__StructuredTextAlgorithm__Group_1_1__1 : rule__StructuredTextAlgorithm__Group_1_1__1__Impl ;
    public final void rule__StructuredTextAlgorithm__Group_1_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3801:1: ( rule__StructuredTextAlgorithm__Group_1_1__1__Impl )
            // InternalStructuredTextParser.g:3802:2: rule__StructuredTextAlgorithm__Group_1_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StructuredTextAlgorithm__Group_1_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group_1_1__1"


    // $ANTLR start "rule__StructuredTextAlgorithm__Group_1_1__1__Impl"
    // InternalStructuredTextParser.g:3808:1: rule__StructuredTextAlgorithm__Group_1_1__1__Impl : ( Semicolon ) ;
    public final void rule__StructuredTextAlgorithm__Group_1_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3812:1: ( ( Semicolon ) )
            // InternalStructuredTextParser.g:3813:1: ( Semicolon )
            {
            // InternalStructuredTextParser.g:3813:1: ( Semicolon )
            // InternalStructuredTextParser.g:3814:1: Semicolon
            {
             before(grammarAccess.getStructuredTextAlgorithmAccess().getSemicolonKeyword_1_1_1()); 
            match(input,Semicolon,FOLLOW_2); 
             after(grammarAccess.getStructuredTextAlgorithmAccess().getSemicolonKeyword_1_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__Group_1_1__1__Impl"


    // $ANTLR start "rule__Var_Decl_Init__Group__0"
    // InternalStructuredTextParser.g:3831:1: rule__Var_Decl_Init__Group__0 : rule__Var_Decl_Init__Group__0__Impl rule__Var_Decl_Init__Group__1 ;
    public final void rule__Var_Decl_Init__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3835:1: ( rule__Var_Decl_Init__Group__0__Impl rule__Var_Decl_Init__Group__1 )
            // InternalStructuredTextParser.g:3836:2: rule__Var_Decl_Init__Group__0__Impl rule__Var_Decl_Init__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Var_Decl_Init__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Var_Decl_Init__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group__0"


    // $ANTLR start "rule__Var_Decl_Init__Group__0__Impl"
    // InternalStructuredTextParser.g:3843:1: rule__Var_Decl_Init__Group__0__Impl : ( () ) ;
    public final void rule__Var_Decl_Init__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3847:1: ( ( () ) )
            // InternalStructuredTextParser.g:3848:1: ( () )
            {
            // InternalStructuredTextParser.g:3848:1: ( () )
            // InternalStructuredTextParser.g:3849:1: ()
            {
             before(grammarAccess.getVar_Decl_InitAccess().getLocalVariableAction_0()); 
            // InternalStructuredTextParser.g:3850:1: ()
            // InternalStructuredTextParser.g:3852:1: 
            {
            }

             after(grammarAccess.getVar_Decl_InitAccess().getLocalVariableAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group__0__Impl"


    // $ANTLR start "rule__Var_Decl_Init__Group__1"
    // InternalStructuredTextParser.g:3862:1: rule__Var_Decl_Init__Group__1 : rule__Var_Decl_Init__Group__1__Impl rule__Var_Decl_Init__Group__2 ;
    public final void rule__Var_Decl_Init__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3866:1: ( rule__Var_Decl_Init__Group__1__Impl rule__Var_Decl_Init__Group__2 )
            // InternalStructuredTextParser.g:3867:2: rule__Var_Decl_Init__Group__1__Impl rule__Var_Decl_Init__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Var_Decl_Init__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Var_Decl_Init__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group__1"


    // $ANTLR start "rule__Var_Decl_Init__Group__1__Impl"
    // InternalStructuredTextParser.g:3874:1: rule__Var_Decl_Init__Group__1__Impl : ( ( rule__Var_Decl_Init__ConstantAssignment_1 )? ) ;
    public final void rule__Var_Decl_Init__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3878:1: ( ( ( rule__Var_Decl_Init__ConstantAssignment_1 )? ) )
            // InternalStructuredTextParser.g:3879:1: ( ( rule__Var_Decl_Init__ConstantAssignment_1 )? )
            {
            // InternalStructuredTextParser.g:3879:1: ( ( rule__Var_Decl_Init__ConstantAssignment_1 )? )
            // InternalStructuredTextParser.g:3880:1: ( rule__Var_Decl_Init__ConstantAssignment_1 )?
            {
             before(grammarAccess.getVar_Decl_InitAccess().getConstantAssignment_1()); 
            // InternalStructuredTextParser.g:3881:1: ( rule__Var_Decl_Init__ConstantAssignment_1 )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==CONSTANT) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalStructuredTextParser.g:3881:2: rule__Var_Decl_Init__ConstantAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Var_Decl_Init__ConstantAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getVar_Decl_InitAccess().getConstantAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group__1__Impl"


    // $ANTLR start "rule__Var_Decl_Init__Group__2"
    // InternalStructuredTextParser.g:3891:1: rule__Var_Decl_Init__Group__2 : rule__Var_Decl_Init__Group__2__Impl rule__Var_Decl_Init__Group__3 ;
    public final void rule__Var_Decl_Init__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3895:1: ( rule__Var_Decl_Init__Group__2__Impl rule__Var_Decl_Init__Group__3 )
            // InternalStructuredTextParser.g:3896:2: rule__Var_Decl_Init__Group__2__Impl rule__Var_Decl_Init__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Var_Decl_Init__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Var_Decl_Init__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group__2"


    // $ANTLR start "rule__Var_Decl_Init__Group__2__Impl"
    // InternalStructuredTextParser.g:3903:1: rule__Var_Decl_Init__Group__2__Impl : ( ( rule__Var_Decl_Init__NameAssignment_2 ) ) ;
    public final void rule__Var_Decl_Init__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3907:1: ( ( ( rule__Var_Decl_Init__NameAssignment_2 ) ) )
            // InternalStructuredTextParser.g:3908:1: ( ( rule__Var_Decl_Init__NameAssignment_2 ) )
            {
            // InternalStructuredTextParser.g:3908:1: ( ( rule__Var_Decl_Init__NameAssignment_2 ) )
            // InternalStructuredTextParser.g:3909:1: ( rule__Var_Decl_Init__NameAssignment_2 )
            {
             before(grammarAccess.getVar_Decl_InitAccess().getNameAssignment_2()); 
            // InternalStructuredTextParser.g:3910:1: ( rule__Var_Decl_Init__NameAssignment_2 )
            // InternalStructuredTextParser.g:3910:2: rule__Var_Decl_Init__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Var_Decl_Init__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getVar_Decl_InitAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group__2__Impl"


    // $ANTLR start "rule__Var_Decl_Init__Group__3"
    // InternalStructuredTextParser.g:3920:1: rule__Var_Decl_Init__Group__3 : rule__Var_Decl_Init__Group__3__Impl rule__Var_Decl_Init__Group__4 ;
    public final void rule__Var_Decl_Init__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3924:1: ( rule__Var_Decl_Init__Group__3__Impl rule__Var_Decl_Init__Group__4 )
            // InternalStructuredTextParser.g:3925:2: rule__Var_Decl_Init__Group__3__Impl rule__Var_Decl_Init__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__Var_Decl_Init__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Var_Decl_Init__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group__3"


    // $ANTLR start "rule__Var_Decl_Init__Group__3__Impl"
    // InternalStructuredTextParser.g:3932:1: rule__Var_Decl_Init__Group__3__Impl : ( Colon ) ;
    public final void rule__Var_Decl_Init__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3936:1: ( ( Colon ) )
            // InternalStructuredTextParser.g:3937:1: ( Colon )
            {
            // InternalStructuredTextParser.g:3937:1: ( Colon )
            // InternalStructuredTextParser.g:3938:1: Colon
            {
             before(grammarAccess.getVar_Decl_InitAccess().getColonKeyword_3()); 
            match(input,Colon,FOLLOW_2); 
             after(grammarAccess.getVar_Decl_InitAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group__3__Impl"


    // $ANTLR start "rule__Var_Decl_Init__Group__4"
    // InternalStructuredTextParser.g:3951:1: rule__Var_Decl_Init__Group__4 : rule__Var_Decl_Init__Group__4__Impl rule__Var_Decl_Init__Group__5 ;
    public final void rule__Var_Decl_Init__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3955:1: ( rule__Var_Decl_Init__Group__4__Impl rule__Var_Decl_Init__Group__5 )
            // InternalStructuredTextParser.g:3956:2: rule__Var_Decl_Init__Group__4__Impl rule__Var_Decl_Init__Group__5
            {
            pushFollow(FOLLOW_10);
            rule__Var_Decl_Init__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Var_Decl_Init__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group__4"


    // $ANTLR start "rule__Var_Decl_Init__Group__4__Impl"
    // InternalStructuredTextParser.g:3963:1: rule__Var_Decl_Init__Group__4__Impl : ( ( rule__Var_Decl_Init__TypeAssignment_4 ) ) ;
    public final void rule__Var_Decl_Init__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3967:1: ( ( ( rule__Var_Decl_Init__TypeAssignment_4 ) ) )
            // InternalStructuredTextParser.g:3968:1: ( ( rule__Var_Decl_Init__TypeAssignment_4 ) )
            {
            // InternalStructuredTextParser.g:3968:1: ( ( rule__Var_Decl_Init__TypeAssignment_4 ) )
            // InternalStructuredTextParser.g:3969:1: ( rule__Var_Decl_Init__TypeAssignment_4 )
            {
             before(grammarAccess.getVar_Decl_InitAccess().getTypeAssignment_4()); 
            // InternalStructuredTextParser.g:3970:1: ( rule__Var_Decl_Init__TypeAssignment_4 )
            // InternalStructuredTextParser.g:3970:2: rule__Var_Decl_Init__TypeAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Var_Decl_Init__TypeAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getVar_Decl_InitAccess().getTypeAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group__4__Impl"


    // $ANTLR start "rule__Var_Decl_Init__Group__5"
    // InternalStructuredTextParser.g:3980:1: rule__Var_Decl_Init__Group__5 : rule__Var_Decl_Init__Group__5__Impl ;
    public final void rule__Var_Decl_Init__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3984:1: ( rule__Var_Decl_Init__Group__5__Impl )
            // InternalStructuredTextParser.g:3985:2: rule__Var_Decl_Init__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Var_Decl_Init__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group__5"


    // $ANTLR start "rule__Var_Decl_Init__Group__5__Impl"
    // InternalStructuredTextParser.g:3991:1: rule__Var_Decl_Init__Group__5__Impl : ( ( rule__Var_Decl_Init__Group_5__0 )? ) ;
    public final void rule__Var_Decl_Init__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:3995:1: ( ( ( rule__Var_Decl_Init__Group_5__0 )? ) )
            // InternalStructuredTextParser.g:3996:1: ( ( rule__Var_Decl_Init__Group_5__0 )? )
            {
            // InternalStructuredTextParser.g:3996:1: ( ( rule__Var_Decl_Init__Group_5__0 )? )
            // InternalStructuredTextParser.g:3997:1: ( rule__Var_Decl_Init__Group_5__0 )?
            {
             before(grammarAccess.getVar_Decl_InitAccess().getGroup_5()); 
            // InternalStructuredTextParser.g:3998:1: ( rule__Var_Decl_Init__Group_5__0 )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==ColonEqualsSign) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalStructuredTextParser.g:3998:2: rule__Var_Decl_Init__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Var_Decl_Init__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getVar_Decl_InitAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group__5__Impl"


    // $ANTLR start "rule__Var_Decl_Init__Group_5__0"
    // InternalStructuredTextParser.g:4020:1: rule__Var_Decl_Init__Group_5__0 : rule__Var_Decl_Init__Group_5__0__Impl rule__Var_Decl_Init__Group_5__1 ;
    public final void rule__Var_Decl_Init__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4024:1: ( rule__Var_Decl_Init__Group_5__0__Impl rule__Var_Decl_Init__Group_5__1 )
            // InternalStructuredTextParser.g:4025:2: rule__Var_Decl_Init__Group_5__0__Impl rule__Var_Decl_Init__Group_5__1
            {
            pushFollow(FOLLOW_11);
            rule__Var_Decl_Init__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Var_Decl_Init__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group_5__0"


    // $ANTLR start "rule__Var_Decl_Init__Group_5__0__Impl"
    // InternalStructuredTextParser.g:4032:1: rule__Var_Decl_Init__Group_5__0__Impl : ( ColonEqualsSign ) ;
    public final void rule__Var_Decl_Init__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4036:1: ( ( ColonEqualsSign ) )
            // InternalStructuredTextParser.g:4037:1: ( ColonEqualsSign )
            {
            // InternalStructuredTextParser.g:4037:1: ( ColonEqualsSign )
            // InternalStructuredTextParser.g:4038:1: ColonEqualsSign
            {
             before(grammarAccess.getVar_Decl_InitAccess().getColonEqualsSignKeyword_5_0()); 
            match(input,ColonEqualsSign,FOLLOW_2); 
             after(grammarAccess.getVar_Decl_InitAccess().getColonEqualsSignKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group_5__0__Impl"


    // $ANTLR start "rule__Var_Decl_Init__Group_5__1"
    // InternalStructuredTextParser.g:4051:1: rule__Var_Decl_Init__Group_5__1 : rule__Var_Decl_Init__Group_5__1__Impl ;
    public final void rule__Var_Decl_Init__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4055:1: ( rule__Var_Decl_Init__Group_5__1__Impl )
            // InternalStructuredTextParser.g:4056:2: rule__Var_Decl_Init__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Var_Decl_Init__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group_5__1"


    // $ANTLR start "rule__Var_Decl_Init__Group_5__1__Impl"
    // InternalStructuredTextParser.g:4062:1: rule__Var_Decl_Init__Group_5__1__Impl : ( ( rule__Var_Decl_Init__InitialValueAssignment_5_1 ) ) ;
    public final void rule__Var_Decl_Init__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4066:1: ( ( ( rule__Var_Decl_Init__InitialValueAssignment_5_1 ) ) )
            // InternalStructuredTextParser.g:4067:1: ( ( rule__Var_Decl_Init__InitialValueAssignment_5_1 ) )
            {
            // InternalStructuredTextParser.g:4067:1: ( ( rule__Var_Decl_Init__InitialValueAssignment_5_1 ) )
            // InternalStructuredTextParser.g:4068:1: ( rule__Var_Decl_Init__InitialValueAssignment_5_1 )
            {
             before(grammarAccess.getVar_Decl_InitAccess().getInitialValueAssignment_5_1()); 
            // InternalStructuredTextParser.g:4069:1: ( rule__Var_Decl_Init__InitialValueAssignment_5_1 )
            // InternalStructuredTextParser.g:4069:2: rule__Var_Decl_Init__InitialValueAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Var_Decl_Init__InitialValueAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getVar_Decl_InitAccess().getInitialValueAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__Group_5__1__Impl"


    // $ANTLR start "rule__Stmt_List__Group__0"
    // InternalStructuredTextParser.g:4083:1: rule__Stmt_List__Group__0 : rule__Stmt_List__Group__0__Impl rule__Stmt_List__Group__1 ;
    public final void rule__Stmt_List__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4087:1: ( rule__Stmt_List__Group__0__Impl rule__Stmt_List__Group__1 )
            // InternalStructuredTextParser.g:4088:2: rule__Stmt_List__Group__0__Impl rule__Stmt_List__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Stmt_List__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Stmt_List__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stmt_List__Group__0"


    // $ANTLR start "rule__Stmt_List__Group__0__Impl"
    // InternalStructuredTextParser.g:4095:1: rule__Stmt_List__Group__0__Impl : ( () ) ;
    public final void rule__Stmt_List__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4099:1: ( ( () ) )
            // InternalStructuredTextParser.g:4100:1: ( () )
            {
            // InternalStructuredTextParser.g:4100:1: ( () )
            // InternalStructuredTextParser.g:4101:1: ()
            {
             before(grammarAccess.getStmt_ListAccess().getStatementListAction_0()); 
            // InternalStructuredTextParser.g:4102:1: ()
            // InternalStructuredTextParser.g:4104:1: 
            {
            }

             after(grammarAccess.getStmt_ListAccess().getStatementListAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stmt_List__Group__0__Impl"


    // $ANTLR start "rule__Stmt_List__Group__1"
    // InternalStructuredTextParser.g:4114:1: rule__Stmt_List__Group__1 : rule__Stmt_List__Group__1__Impl ;
    public final void rule__Stmt_List__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4118:1: ( rule__Stmt_List__Group__1__Impl )
            // InternalStructuredTextParser.g:4119:2: rule__Stmt_List__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Stmt_List__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stmt_List__Group__1"


    // $ANTLR start "rule__Stmt_List__Group__1__Impl"
    // InternalStructuredTextParser.g:4125:1: rule__Stmt_List__Group__1__Impl : ( ( rule__Stmt_List__Group_1__0 )* ) ;
    public final void rule__Stmt_List__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4129:1: ( ( ( rule__Stmt_List__Group_1__0 )* ) )
            // InternalStructuredTextParser.g:4130:1: ( ( rule__Stmt_List__Group_1__0 )* )
            {
            // InternalStructuredTextParser.g:4130:1: ( ( rule__Stmt_List__Group_1__0 )* )
            // InternalStructuredTextParser.g:4131:1: ( rule__Stmt_List__Group_1__0 )*
            {
             before(grammarAccess.getStmt_ListAccess().getGroup_1()); 
            // InternalStructuredTextParser.g:4132:1: ( rule__Stmt_List__Group_1__0 )*
            loop39:
            do {
                int alt39=2;
                switch ( input.LA(1) ) {
                case TIME:
                    {
                    int LA39_2 = input.LA(2);

                    if ( (LA39_2==LeftParenthesis) ) {
                        alt39=1;
                    }


                    }
                    break;
                case T:
                    {
                    int LA39_3 = input.LA(2);

                    if ( (LA39_3==ColonEqualsSign||LA39_3==FullStop||LA39_3==LeftSquareBracket) ) {
                        alt39=1;
                    }


                    }
                    break;
                case LT:
                    {
                    int LA39_4 = input.LA(2);

                    if ( (LA39_4==ColonEqualsSign||LA39_4==FullStop||LA39_4==LeftSquareBracket) ) {
                        alt39=1;
                    }


                    }
                    break;
                case DT:
                    {
                    int LA39_5 = input.LA(2);

                    if ( (LA39_5==ColonEqualsSign||LA39_5==FullStop||LA39_5==LeftSquareBracket) ) {
                        alt39=1;
                    }


                    }
                    break;
                case CONTINUE:
                case REPEAT:
                case RETURN:
                case SUPER:
                case WHILE:
                case CASE:
                case EXIT:
                case FOR:
                case IF:
                case Semicolon:
                case RULE_ID:
                    {
                    alt39=1;
                    }
                    break;

                }

                switch (alt39) {
            	case 1 :
            	    // InternalStructuredTextParser.g:4132:2: rule__Stmt_List__Group_1__0
            	    {
            	    pushFollow(FOLLOW_12);
            	    rule__Stmt_List__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop39;
                }
            } while (true);

             after(grammarAccess.getStmt_ListAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stmt_List__Group__1__Impl"


    // $ANTLR start "rule__Stmt_List__Group_1__0"
    // InternalStructuredTextParser.g:4146:1: rule__Stmt_List__Group_1__0 : rule__Stmt_List__Group_1__0__Impl rule__Stmt_List__Group_1__1 ;
    public final void rule__Stmt_List__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4150:1: ( rule__Stmt_List__Group_1__0__Impl rule__Stmt_List__Group_1__1 )
            // InternalStructuredTextParser.g:4151:2: rule__Stmt_List__Group_1__0__Impl rule__Stmt_List__Group_1__1
            {
            pushFollow(FOLLOW_13);
            rule__Stmt_List__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Stmt_List__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stmt_List__Group_1__0"


    // $ANTLR start "rule__Stmt_List__Group_1__0__Impl"
    // InternalStructuredTextParser.g:4158:1: rule__Stmt_List__Group_1__0__Impl : ( ( rule__Stmt_List__StatementsAssignment_1_0 )? ) ;
    public final void rule__Stmt_List__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4162:1: ( ( ( rule__Stmt_List__StatementsAssignment_1_0 )? ) )
            // InternalStructuredTextParser.g:4163:1: ( ( rule__Stmt_List__StatementsAssignment_1_0 )? )
            {
            // InternalStructuredTextParser.g:4163:1: ( ( rule__Stmt_List__StatementsAssignment_1_0 )? )
            // InternalStructuredTextParser.g:4164:1: ( rule__Stmt_List__StatementsAssignment_1_0 )?
            {
             before(grammarAccess.getStmt_ListAccess().getStatementsAssignment_1_0()); 
            // InternalStructuredTextParser.g:4165:1: ( rule__Stmt_List__StatementsAssignment_1_0 )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==CONTINUE||(LA40_0>=REPEAT && LA40_0<=RETURN)||LA40_0==SUPER||LA40_0==WHILE||LA40_0==CASE||LA40_0==EXIT||LA40_0==TIME||LA40_0==FOR||(LA40_0>=DT && LA40_0<=IF)||LA40_0==LT||LA40_0==T||LA40_0==RULE_ID) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalStructuredTextParser.g:4165:2: rule__Stmt_List__StatementsAssignment_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Stmt_List__StatementsAssignment_1_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getStmt_ListAccess().getStatementsAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stmt_List__Group_1__0__Impl"


    // $ANTLR start "rule__Stmt_List__Group_1__1"
    // InternalStructuredTextParser.g:4175:1: rule__Stmt_List__Group_1__1 : rule__Stmt_List__Group_1__1__Impl ;
    public final void rule__Stmt_List__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4179:1: ( rule__Stmt_List__Group_1__1__Impl )
            // InternalStructuredTextParser.g:4180:2: rule__Stmt_List__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Stmt_List__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stmt_List__Group_1__1"


    // $ANTLR start "rule__Stmt_List__Group_1__1__Impl"
    // InternalStructuredTextParser.g:4186:1: rule__Stmt_List__Group_1__1__Impl : ( Semicolon ) ;
    public final void rule__Stmt_List__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4190:1: ( ( Semicolon ) )
            // InternalStructuredTextParser.g:4191:1: ( Semicolon )
            {
            // InternalStructuredTextParser.g:4191:1: ( Semicolon )
            // InternalStructuredTextParser.g:4192:1: Semicolon
            {
             before(grammarAccess.getStmt_ListAccess().getSemicolonKeyword_1_1()); 
            match(input,Semicolon,FOLLOW_2); 
             after(grammarAccess.getStmt_ListAccess().getSemicolonKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stmt_List__Group_1__1__Impl"


    // $ANTLR start "rule__Assign_Stmt__Group__0"
    // InternalStructuredTextParser.g:4209:1: rule__Assign_Stmt__Group__0 : rule__Assign_Stmt__Group__0__Impl rule__Assign_Stmt__Group__1 ;
    public final void rule__Assign_Stmt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4213:1: ( rule__Assign_Stmt__Group__0__Impl rule__Assign_Stmt__Group__1 )
            // InternalStructuredTextParser.g:4214:2: rule__Assign_Stmt__Group__0__Impl rule__Assign_Stmt__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Assign_Stmt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Assign_Stmt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assign_Stmt__Group__0"


    // $ANTLR start "rule__Assign_Stmt__Group__0__Impl"
    // InternalStructuredTextParser.g:4221:1: rule__Assign_Stmt__Group__0__Impl : ( ( rule__Assign_Stmt__VariableAssignment_0 ) ) ;
    public final void rule__Assign_Stmt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4225:1: ( ( ( rule__Assign_Stmt__VariableAssignment_0 ) ) )
            // InternalStructuredTextParser.g:4226:1: ( ( rule__Assign_Stmt__VariableAssignment_0 ) )
            {
            // InternalStructuredTextParser.g:4226:1: ( ( rule__Assign_Stmt__VariableAssignment_0 ) )
            // InternalStructuredTextParser.g:4227:1: ( rule__Assign_Stmt__VariableAssignment_0 )
            {
             before(grammarAccess.getAssign_StmtAccess().getVariableAssignment_0()); 
            // InternalStructuredTextParser.g:4228:1: ( rule__Assign_Stmt__VariableAssignment_0 )
            // InternalStructuredTextParser.g:4228:2: rule__Assign_Stmt__VariableAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Assign_Stmt__VariableAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getAssign_StmtAccess().getVariableAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assign_Stmt__Group__0__Impl"


    // $ANTLR start "rule__Assign_Stmt__Group__1"
    // InternalStructuredTextParser.g:4238:1: rule__Assign_Stmt__Group__1 : rule__Assign_Stmt__Group__1__Impl rule__Assign_Stmt__Group__2 ;
    public final void rule__Assign_Stmt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4242:1: ( rule__Assign_Stmt__Group__1__Impl rule__Assign_Stmt__Group__2 )
            // InternalStructuredTextParser.g:4243:2: rule__Assign_Stmt__Group__1__Impl rule__Assign_Stmt__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Assign_Stmt__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Assign_Stmt__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assign_Stmt__Group__1"


    // $ANTLR start "rule__Assign_Stmt__Group__1__Impl"
    // InternalStructuredTextParser.g:4250:1: rule__Assign_Stmt__Group__1__Impl : ( ColonEqualsSign ) ;
    public final void rule__Assign_Stmt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4254:1: ( ( ColonEqualsSign ) )
            // InternalStructuredTextParser.g:4255:1: ( ColonEqualsSign )
            {
            // InternalStructuredTextParser.g:4255:1: ( ColonEqualsSign )
            // InternalStructuredTextParser.g:4256:1: ColonEqualsSign
            {
             before(grammarAccess.getAssign_StmtAccess().getColonEqualsSignKeyword_1()); 
            match(input,ColonEqualsSign,FOLLOW_2); 
             after(grammarAccess.getAssign_StmtAccess().getColonEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assign_Stmt__Group__1__Impl"


    // $ANTLR start "rule__Assign_Stmt__Group__2"
    // InternalStructuredTextParser.g:4269:1: rule__Assign_Stmt__Group__2 : rule__Assign_Stmt__Group__2__Impl ;
    public final void rule__Assign_Stmt__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4273:1: ( rule__Assign_Stmt__Group__2__Impl )
            // InternalStructuredTextParser.g:4274:2: rule__Assign_Stmt__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Assign_Stmt__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assign_Stmt__Group__2"


    // $ANTLR start "rule__Assign_Stmt__Group__2__Impl"
    // InternalStructuredTextParser.g:4280:1: rule__Assign_Stmt__Group__2__Impl : ( ( rule__Assign_Stmt__ExpressionAssignment_2 ) ) ;
    public final void rule__Assign_Stmt__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4284:1: ( ( ( rule__Assign_Stmt__ExpressionAssignment_2 ) ) )
            // InternalStructuredTextParser.g:4285:1: ( ( rule__Assign_Stmt__ExpressionAssignment_2 ) )
            {
            // InternalStructuredTextParser.g:4285:1: ( ( rule__Assign_Stmt__ExpressionAssignment_2 ) )
            // InternalStructuredTextParser.g:4286:1: ( rule__Assign_Stmt__ExpressionAssignment_2 )
            {
             before(grammarAccess.getAssign_StmtAccess().getExpressionAssignment_2()); 
            // InternalStructuredTextParser.g:4287:1: ( rule__Assign_Stmt__ExpressionAssignment_2 )
            // InternalStructuredTextParser.g:4287:2: rule__Assign_Stmt__ExpressionAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Assign_Stmt__ExpressionAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAssign_StmtAccess().getExpressionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assign_Stmt__Group__2__Impl"


    // $ANTLR start "rule__Subprog_Ctrl_Stmt__Group_1__0"
    // InternalStructuredTextParser.g:4303:1: rule__Subprog_Ctrl_Stmt__Group_1__0 : rule__Subprog_Ctrl_Stmt__Group_1__0__Impl rule__Subprog_Ctrl_Stmt__Group_1__1 ;
    public final void rule__Subprog_Ctrl_Stmt__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4307:1: ( rule__Subprog_Ctrl_Stmt__Group_1__0__Impl rule__Subprog_Ctrl_Stmt__Group_1__1 )
            // InternalStructuredTextParser.g:4308:2: rule__Subprog_Ctrl_Stmt__Group_1__0__Impl rule__Subprog_Ctrl_Stmt__Group_1__1
            {
            pushFollow(FOLLOW_15);
            rule__Subprog_Ctrl_Stmt__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Subprog_Ctrl_Stmt__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subprog_Ctrl_Stmt__Group_1__0"


    // $ANTLR start "rule__Subprog_Ctrl_Stmt__Group_1__0__Impl"
    // InternalStructuredTextParser.g:4315:1: rule__Subprog_Ctrl_Stmt__Group_1__0__Impl : ( () ) ;
    public final void rule__Subprog_Ctrl_Stmt__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4319:1: ( ( () ) )
            // InternalStructuredTextParser.g:4320:1: ( () )
            {
            // InternalStructuredTextParser.g:4320:1: ( () )
            // InternalStructuredTextParser.g:4321:1: ()
            {
             before(grammarAccess.getSubprog_Ctrl_StmtAccess().getSuperStatementAction_1_0()); 
            // InternalStructuredTextParser.g:4322:1: ()
            // InternalStructuredTextParser.g:4324:1: 
            {
            }

             after(grammarAccess.getSubprog_Ctrl_StmtAccess().getSuperStatementAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subprog_Ctrl_Stmt__Group_1__0__Impl"


    // $ANTLR start "rule__Subprog_Ctrl_Stmt__Group_1__1"
    // InternalStructuredTextParser.g:4334:1: rule__Subprog_Ctrl_Stmt__Group_1__1 : rule__Subprog_Ctrl_Stmt__Group_1__1__Impl rule__Subprog_Ctrl_Stmt__Group_1__2 ;
    public final void rule__Subprog_Ctrl_Stmt__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4338:1: ( rule__Subprog_Ctrl_Stmt__Group_1__1__Impl rule__Subprog_Ctrl_Stmt__Group_1__2 )
            // InternalStructuredTextParser.g:4339:2: rule__Subprog_Ctrl_Stmt__Group_1__1__Impl rule__Subprog_Ctrl_Stmt__Group_1__2
            {
            pushFollow(FOLLOW_16);
            rule__Subprog_Ctrl_Stmt__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Subprog_Ctrl_Stmt__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subprog_Ctrl_Stmt__Group_1__1"


    // $ANTLR start "rule__Subprog_Ctrl_Stmt__Group_1__1__Impl"
    // InternalStructuredTextParser.g:4346:1: rule__Subprog_Ctrl_Stmt__Group_1__1__Impl : ( SUPER ) ;
    public final void rule__Subprog_Ctrl_Stmt__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4350:1: ( ( SUPER ) )
            // InternalStructuredTextParser.g:4351:1: ( SUPER )
            {
            // InternalStructuredTextParser.g:4351:1: ( SUPER )
            // InternalStructuredTextParser.g:4352:1: SUPER
            {
             before(grammarAccess.getSubprog_Ctrl_StmtAccess().getSUPERKeyword_1_1()); 
            match(input,SUPER,FOLLOW_2); 
             after(grammarAccess.getSubprog_Ctrl_StmtAccess().getSUPERKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subprog_Ctrl_Stmt__Group_1__1__Impl"


    // $ANTLR start "rule__Subprog_Ctrl_Stmt__Group_1__2"
    // InternalStructuredTextParser.g:4365:1: rule__Subprog_Ctrl_Stmt__Group_1__2 : rule__Subprog_Ctrl_Stmt__Group_1__2__Impl rule__Subprog_Ctrl_Stmt__Group_1__3 ;
    public final void rule__Subprog_Ctrl_Stmt__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4369:1: ( rule__Subprog_Ctrl_Stmt__Group_1__2__Impl rule__Subprog_Ctrl_Stmt__Group_1__3 )
            // InternalStructuredTextParser.g:4370:2: rule__Subprog_Ctrl_Stmt__Group_1__2__Impl rule__Subprog_Ctrl_Stmt__Group_1__3
            {
            pushFollow(FOLLOW_17);
            rule__Subprog_Ctrl_Stmt__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Subprog_Ctrl_Stmt__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subprog_Ctrl_Stmt__Group_1__2"


    // $ANTLR start "rule__Subprog_Ctrl_Stmt__Group_1__2__Impl"
    // InternalStructuredTextParser.g:4377:1: rule__Subprog_Ctrl_Stmt__Group_1__2__Impl : ( LeftParenthesis ) ;
    public final void rule__Subprog_Ctrl_Stmt__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4381:1: ( ( LeftParenthesis ) )
            // InternalStructuredTextParser.g:4382:1: ( LeftParenthesis )
            {
            // InternalStructuredTextParser.g:4382:1: ( LeftParenthesis )
            // InternalStructuredTextParser.g:4383:1: LeftParenthesis
            {
             before(grammarAccess.getSubprog_Ctrl_StmtAccess().getLeftParenthesisKeyword_1_2()); 
            match(input,LeftParenthesis,FOLLOW_2); 
             after(grammarAccess.getSubprog_Ctrl_StmtAccess().getLeftParenthesisKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subprog_Ctrl_Stmt__Group_1__2__Impl"


    // $ANTLR start "rule__Subprog_Ctrl_Stmt__Group_1__3"
    // InternalStructuredTextParser.g:4396:1: rule__Subprog_Ctrl_Stmt__Group_1__3 : rule__Subprog_Ctrl_Stmt__Group_1__3__Impl ;
    public final void rule__Subprog_Ctrl_Stmt__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4400:1: ( rule__Subprog_Ctrl_Stmt__Group_1__3__Impl )
            // InternalStructuredTextParser.g:4401:2: rule__Subprog_Ctrl_Stmt__Group_1__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Subprog_Ctrl_Stmt__Group_1__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subprog_Ctrl_Stmt__Group_1__3"


    // $ANTLR start "rule__Subprog_Ctrl_Stmt__Group_1__3__Impl"
    // InternalStructuredTextParser.g:4407:1: rule__Subprog_Ctrl_Stmt__Group_1__3__Impl : ( RightParenthesis ) ;
    public final void rule__Subprog_Ctrl_Stmt__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4411:1: ( ( RightParenthesis ) )
            // InternalStructuredTextParser.g:4412:1: ( RightParenthesis )
            {
            // InternalStructuredTextParser.g:4412:1: ( RightParenthesis )
            // InternalStructuredTextParser.g:4413:1: RightParenthesis
            {
             before(grammarAccess.getSubprog_Ctrl_StmtAccess().getRightParenthesisKeyword_1_3()); 
            match(input,RightParenthesis,FOLLOW_2); 
             after(grammarAccess.getSubprog_Ctrl_StmtAccess().getRightParenthesisKeyword_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subprog_Ctrl_Stmt__Group_1__3__Impl"


    // $ANTLR start "rule__Subprog_Ctrl_Stmt__Group_2__0"
    // InternalStructuredTextParser.g:4434:1: rule__Subprog_Ctrl_Stmt__Group_2__0 : rule__Subprog_Ctrl_Stmt__Group_2__0__Impl rule__Subprog_Ctrl_Stmt__Group_2__1 ;
    public final void rule__Subprog_Ctrl_Stmt__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4438:1: ( rule__Subprog_Ctrl_Stmt__Group_2__0__Impl rule__Subprog_Ctrl_Stmt__Group_2__1 )
            // InternalStructuredTextParser.g:4439:2: rule__Subprog_Ctrl_Stmt__Group_2__0__Impl rule__Subprog_Ctrl_Stmt__Group_2__1
            {
            pushFollow(FOLLOW_18);
            rule__Subprog_Ctrl_Stmt__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Subprog_Ctrl_Stmt__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subprog_Ctrl_Stmt__Group_2__0"


    // $ANTLR start "rule__Subprog_Ctrl_Stmt__Group_2__0__Impl"
    // InternalStructuredTextParser.g:4446:1: rule__Subprog_Ctrl_Stmt__Group_2__0__Impl : ( () ) ;
    public final void rule__Subprog_Ctrl_Stmt__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4450:1: ( ( () ) )
            // InternalStructuredTextParser.g:4451:1: ( () )
            {
            // InternalStructuredTextParser.g:4451:1: ( () )
            // InternalStructuredTextParser.g:4452:1: ()
            {
             before(grammarAccess.getSubprog_Ctrl_StmtAccess().getReturnStatementAction_2_0()); 
            // InternalStructuredTextParser.g:4453:1: ()
            // InternalStructuredTextParser.g:4455:1: 
            {
            }

             after(grammarAccess.getSubprog_Ctrl_StmtAccess().getReturnStatementAction_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subprog_Ctrl_Stmt__Group_2__0__Impl"


    // $ANTLR start "rule__Subprog_Ctrl_Stmt__Group_2__1"
    // InternalStructuredTextParser.g:4465:1: rule__Subprog_Ctrl_Stmt__Group_2__1 : rule__Subprog_Ctrl_Stmt__Group_2__1__Impl ;
    public final void rule__Subprog_Ctrl_Stmt__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4469:1: ( rule__Subprog_Ctrl_Stmt__Group_2__1__Impl )
            // InternalStructuredTextParser.g:4470:2: rule__Subprog_Ctrl_Stmt__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Subprog_Ctrl_Stmt__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subprog_Ctrl_Stmt__Group_2__1"


    // $ANTLR start "rule__Subprog_Ctrl_Stmt__Group_2__1__Impl"
    // InternalStructuredTextParser.g:4476:1: rule__Subprog_Ctrl_Stmt__Group_2__1__Impl : ( RETURN ) ;
    public final void rule__Subprog_Ctrl_Stmt__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4480:1: ( ( RETURN ) )
            // InternalStructuredTextParser.g:4481:1: ( RETURN )
            {
            // InternalStructuredTextParser.g:4481:1: ( RETURN )
            // InternalStructuredTextParser.g:4482:1: RETURN
            {
             before(grammarAccess.getSubprog_Ctrl_StmtAccess().getRETURNKeyword_2_1()); 
            match(input,RETURN,FOLLOW_2); 
             after(grammarAccess.getSubprog_Ctrl_StmtAccess().getRETURNKeyword_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Subprog_Ctrl_Stmt__Group_2__1__Impl"


    // $ANTLR start "rule__IF_Stmt__Group__0"
    // InternalStructuredTextParser.g:4499:1: rule__IF_Stmt__Group__0 : rule__IF_Stmt__Group__0__Impl rule__IF_Stmt__Group__1 ;
    public final void rule__IF_Stmt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4503:1: ( rule__IF_Stmt__Group__0__Impl rule__IF_Stmt__Group__1 )
            // InternalStructuredTextParser.g:4504:2: rule__IF_Stmt__Group__0__Impl rule__IF_Stmt__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__IF_Stmt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IF_Stmt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__0"


    // $ANTLR start "rule__IF_Stmt__Group__0__Impl"
    // InternalStructuredTextParser.g:4511:1: rule__IF_Stmt__Group__0__Impl : ( IF ) ;
    public final void rule__IF_Stmt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4515:1: ( ( IF ) )
            // InternalStructuredTextParser.g:4516:1: ( IF )
            {
            // InternalStructuredTextParser.g:4516:1: ( IF )
            // InternalStructuredTextParser.g:4517:1: IF
            {
             before(grammarAccess.getIF_StmtAccess().getIFKeyword_0()); 
            match(input,IF,FOLLOW_2); 
             after(grammarAccess.getIF_StmtAccess().getIFKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__0__Impl"


    // $ANTLR start "rule__IF_Stmt__Group__1"
    // InternalStructuredTextParser.g:4530:1: rule__IF_Stmt__Group__1 : rule__IF_Stmt__Group__1__Impl rule__IF_Stmt__Group__2 ;
    public final void rule__IF_Stmt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4534:1: ( rule__IF_Stmt__Group__1__Impl rule__IF_Stmt__Group__2 )
            // InternalStructuredTextParser.g:4535:2: rule__IF_Stmt__Group__1__Impl rule__IF_Stmt__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__IF_Stmt__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IF_Stmt__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__1"


    // $ANTLR start "rule__IF_Stmt__Group__1__Impl"
    // InternalStructuredTextParser.g:4542:1: rule__IF_Stmt__Group__1__Impl : ( ( rule__IF_Stmt__ExpressionAssignment_1 ) ) ;
    public final void rule__IF_Stmt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4546:1: ( ( ( rule__IF_Stmt__ExpressionAssignment_1 ) ) )
            // InternalStructuredTextParser.g:4547:1: ( ( rule__IF_Stmt__ExpressionAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:4547:1: ( ( rule__IF_Stmt__ExpressionAssignment_1 ) )
            // InternalStructuredTextParser.g:4548:1: ( rule__IF_Stmt__ExpressionAssignment_1 )
            {
             before(grammarAccess.getIF_StmtAccess().getExpressionAssignment_1()); 
            // InternalStructuredTextParser.g:4549:1: ( rule__IF_Stmt__ExpressionAssignment_1 )
            // InternalStructuredTextParser.g:4549:2: rule__IF_Stmt__ExpressionAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__IF_Stmt__ExpressionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getIF_StmtAccess().getExpressionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__1__Impl"


    // $ANTLR start "rule__IF_Stmt__Group__2"
    // InternalStructuredTextParser.g:4559:1: rule__IF_Stmt__Group__2 : rule__IF_Stmt__Group__2__Impl rule__IF_Stmt__Group__3 ;
    public final void rule__IF_Stmt__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4563:1: ( rule__IF_Stmt__Group__2__Impl rule__IF_Stmt__Group__3 )
            // InternalStructuredTextParser.g:4564:2: rule__IF_Stmt__Group__2__Impl rule__IF_Stmt__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__IF_Stmt__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IF_Stmt__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__2"


    // $ANTLR start "rule__IF_Stmt__Group__2__Impl"
    // InternalStructuredTextParser.g:4571:1: rule__IF_Stmt__Group__2__Impl : ( THEN ) ;
    public final void rule__IF_Stmt__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4575:1: ( ( THEN ) )
            // InternalStructuredTextParser.g:4576:1: ( THEN )
            {
            // InternalStructuredTextParser.g:4576:1: ( THEN )
            // InternalStructuredTextParser.g:4577:1: THEN
            {
             before(grammarAccess.getIF_StmtAccess().getTHENKeyword_2()); 
            match(input,THEN,FOLLOW_2); 
             after(grammarAccess.getIF_StmtAccess().getTHENKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__2__Impl"


    // $ANTLR start "rule__IF_Stmt__Group__3"
    // InternalStructuredTextParser.g:4590:1: rule__IF_Stmt__Group__3 : rule__IF_Stmt__Group__3__Impl rule__IF_Stmt__Group__4 ;
    public final void rule__IF_Stmt__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4594:1: ( rule__IF_Stmt__Group__3__Impl rule__IF_Stmt__Group__4 )
            // InternalStructuredTextParser.g:4595:2: rule__IF_Stmt__Group__3__Impl rule__IF_Stmt__Group__4
            {
            pushFollow(FOLLOW_20);
            rule__IF_Stmt__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IF_Stmt__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__3"


    // $ANTLR start "rule__IF_Stmt__Group__3__Impl"
    // InternalStructuredTextParser.g:4602:1: rule__IF_Stmt__Group__3__Impl : ( ( rule__IF_Stmt__StatmentsAssignment_3 ) ) ;
    public final void rule__IF_Stmt__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4606:1: ( ( ( rule__IF_Stmt__StatmentsAssignment_3 ) ) )
            // InternalStructuredTextParser.g:4607:1: ( ( rule__IF_Stmt__StatmentsAssignment_3 ) )
            {
            // InternalStructuredTextParser.g:4607:1: ( ( rule__IF_Stmt__StatmentsAssignment_3 ) )
            // InternalStructuredTextParser.g:4608:1: ( rule__IF_Stmt__StatmentsAssignment_3 )
            {
             before(grammarAccess.getIF_StmtAccess().getStatmentsAssignment_3()); 
            // InternalStructuredTextParser.g:4609:1: ( rule__IF_Stmt__StatmentsAssignment_3 )
            // InternalStructuredTextParser.g:4609:2: rule__IF_Stmt__StatmentsAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__IF_Stmt__StatmentsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getIF_StmtAccess().getStatmentsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__3__Impl"


    // $ANTLR start "rule__IF_Stmt__Group__4"
    // InternalStructuredTextParser.g:4619:1: rule__IF_Stmt__Group__4 : rule__IF_Stmt__Group__4__Impl rule__IF_Stmt__Group__5 ;
    public final void rule__IF_Stmt__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4623:1: ( rule__IF_Stmt__Group__4__Impl rule__IF_Stmt__Group__5 )
            // InternalStructuredTextParser.g:4624:2: rule__IF_Stmt__Group__4__Impl rule__IF_Stmt__Group__5
            {
            pushFollow(FOLLOW_20);
            rule__IF_Stmt__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IF_Stmt__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__4"


    // $ANTLR start "rule__IF_Stmt__Group__4__Impl"
    // InternalStructuredTextParser.g:4631:1: rule__IF_Stmt__Group__4__Impl : ( ( rule__IF_Stmt__ElseifAssignment_4 )* ) ;
    public final void rule__IF_Stmt__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4635:1: ( ( ( rule__IF_Stmt__ElseifAssignment_4 )* ) )
            // InternalStructuredTextParser.g:4636:1: ( ( rule__IF_Stmt__ElseifAssignment_4 )* )
            {
            // InternalStructuredTextParser.g:4636:1: ( ( rule__IF_Stmt__ElseifAssignment_4 )* )
            // InternalStructuredTextParser.g:4637:1: ( rule__IF_Stmt__ElseifAssignment_4 )*
            {
             before(grammarAccess.getIF_StmtAccess().getElseifAssignment_4()); 
            // InternalStructuredTextParser.g:4638:1: ( rule__IF_Stmt__ElseifAssignment_4 )*
            loop41:
            do {
                int alt41=2;
                int LA41_0 = input.LA(1);

                if ( (LA41_0==ELSIF) ) {
                    alt41=1;
                }


                switch (alt41) {
            	case 1 :
            	    // InternalStructuredTextParser.g:4638:2: rule__IF_Stmt__ElseifAssignment_4
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__IF_Stmt__ElseifAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop41;
                }
            } while (true);

             after(grammarAccess.getIF_StmtAccess().getElseifAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__4__Impl"


    // $ANTLR start "rule__IF_Stmt__Group__5"
    // InternalStructuredTextParser.g:4648:1: rule__IF_Stmt__Group__5 : rule__IF_Stmt__Group__5__Impl rule__IF_Stmt__Group__6 ;
    public final void rule__IF_Stmt__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4652:1: ( rule__IF_Stmt__Group__5__Impl rule__IF_Stmt__Group__6 )
            // InternalStructuredTextParser.g:4653:2: rule__IF_Stmt__Group__5__Impl rule__IF_Stmt__Group__6
            {
            pushFollow(FOLLOW_20);
            rule__IF_Stmt__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IF_Stmt__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__5"


    // $ANTLR start "rule__IF_Stmt__Group__5__Impl"
    // InternalStructuredTextParser.g:4660:1: rule__IF_Stmt__Group__5__Impl : ( ( rule__IF_Stmt__ElseAssignment_5 )? ) ;
    public final void rule__IF_Stmt__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4664:1: ( ( ( rule__IF_Stmt__ElseAssignment_5 )? ) )
            // InternalStructuredTextParser.g:4665:1: ( ( rule__IF_Stmt__ElseAssignment_5 )? )
            {
            // InternalStructuredTextParser.g:4665:1: ( ( rule__IF_Stmt__ElseAssignment_5 )? )
            // InternalStructuredTextParser.g:4666:1: ( rule__IF_Stmt__ElseAssignment_5 )?
            {
             before(grammarAccess.getIF_StmtAccess().getElseAssignment_5()); 
            // InternalStructuredTextParser.g:4667:1: ( rule__IF_Stmt__ElseAssignment_5 )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==ELSE) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalStructuredTextParser.g:4667:2: rule__IF_Stmt__ElseAssignment_5
                    {
                    pushFollow(FOLLOW_2);
                    rule__IF_Stmt__ElseAssignment_5();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getIF_StmtAccess().getElseAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__5__Impl"


    // $ANTLR start "rule__IF_Stmt__Group__6"
    // InternalStructuredTextParser.g:4677:1: rule__IF_Stmt__Group__6 : rule__IF_Stmt__Group__6__Impl ;
    public final void rule__IF_Stmt__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4681:1: ( rule__IF_Stmt__Group__6__Impl )
            // InternalStructuredTextParser.g:4682:2: rule__IF_Stmt__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IF_Stmt__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__6"


    // $ANTLR start "rule__IF_Stmt__Group__6__Impl"
    // InternalStructuredTextParser.g:4688:1: rule__IF_Stmt__Group__6__Impl : ( END_IF ) ;
    public final void rule__IF_Stmt__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4692:1: ( ( END_IF ) )
            // InternalStructuredTextParser.g:4693:1: ( END_IF )
            {
            // InternalStructuredTextParser.g:4693:1: ( END_IF )
            // InternalStructuredTextParser.g:4694:1: END_IF
            {
             before(grammarAccess.getIF_StmtAccess().getEND_IFKeyword_6()); 
            match(input,END_IF,FOLLOW_2); 
             after(grammarAccess.getIF_StmtAccess().getEND_IFKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__Group__6__Impl"


    // $ANTLR start "rule__ELSIF_Clause__Group__0"
    // InternalStructuredTextParser.g:4721:1: rule__ELSIF_Clause__Group__0 : rule__ELSIF_Clause__Group__0__Impl rule__ELSIF_Clause__Group__1 ;
    public final void rule__ELSIF_Clause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4725:1: ( rule__ELSIF_Clause__Group__0__Impl rule__ELSIF_Clause__Group__1 )
            // InternalStructuredTextParser.g:4726:2: rule__ELSIF_Clause__Group__0__Impl rule__ELSIF_Clause__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__ELSIF_Clause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ELSIF_Clause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSIF_Clause__Group__0"


    // $ANTLR start "rule__ELSIF_Clause__Group__0__Impl"
    // InternalStructuredTextParser.g:4733:1: rule__ELSIF_Clause__Group__0__Impl : ( ELSIF ) ;
    public final void rule__ELSIF_Clause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4737:1: ( ( ELSIF ) )
            // InternalStructuredTextParser.g:4738:1: ( ELSIF )
            {
            // InternalStructuredTextParser.g:4738:1: ( ELSIF )
            // InternalStructuredTextParser.g:4739:1: ELSIF
            {
             before(grammarAccess.getELSIF_ClauseAccess().getELSIFKeyword_0()); 
            match(input,ELSIF,FOLLOW_2); 
             after(grammarAccess.getELSIF_ClauseAccess().getELSIFKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSIF_Clause__Group__0__Impl"


    // $ANTLR start "rule__ELSIF_Clause__Group__1"
    // InternalStructuredTextParser.g:4752:1: rule__ELSIF_Clause__Group__1 : rule__ELSIF_Clause__Group__1__Impl rule__ELSIF_Clause__Group__2 ;
    public final void rule__ELSIF_Clause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4756:1: ( rule__ELSIF_Clause__Group__1__Impl rule__ELSIF_Clause__Group__2 )
            // InternalStructuredTextParser.g:4757:2: rule__ELSIF_Clause__Group__1__Impl rule__ELSIF_Clause__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__ELSIF_Clause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ELSIF_Clause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSIF_Clause__Group__1"


    // $ANTLR start "rule__ELSIF_Clause__Group__1__Impl"
    // InternalStructuredTextParser.g:4764:1: rule__ELSIF_Clause__Group__1__Impl : ( ( rule__ELSIF_Clause__ExpressionAssignment_1 ) ) ;
    public final void rule__ELSIF_Clause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4768:1: ( ( ( rule__ELSIF_Clause__ExpressionAssignment_1 ) ) )
            // InternalStructuredTextParser.g:4769:1: ( ( rule__ELSIF_Clause__ExpressionAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:4769:1: ( ( rule__ELSIF_Clause__ExpressionAssignment_1 ) )
            // InternalStructuredTextParser.g:4770:1: ( rule__ELSIF_Clause__ExpressionAssignment_1 )
            {
             before(grammarAccess.getELSIF_ClauseAccess().getExpressionAssignment_1()); 
            // InternalStructuredTextParser.g:4771:1: ( rule__ELSIF_Clause__ExpressionAssignment_1 )
            // InternalStructuredTextParser.g:4771:2: rule__ELSIF_Clause__ExpressionAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ELSIF_Clause__ExpressionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getELSIF_ClauseAccess().getExpressionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSIF_Clause__Group__1__Impl"


    // $ANTLR start "rule__ELSIF_Clause__Group__2"
    // InternalStructuredTextParser.g:4781:1: rule__ELSIF_Clause__Group__2 : rule__ELSIF_Clause__Group__2__Impl rule__ELSIF_Clause__Group__3 ;
    public final void rule__ELSIF_Clause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4785:1: ( rule__ELSIF_Clause__Group__2__Impl rule__ELSIF_Clause__Group__3 )
            // InternalStructuredTextParser.g:4786:2: rule__ELSIF_Clause__Group__2__Impl rule__ELSIF_Clause__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__ELSIF_Clause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ELSIF_Clause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSIF_Clause__Group__2"


    // $ANTLR start "rule__ELSIF_Clause__Group__2__Impl"
    // InternalStructuredTextParser.g:4793:1: rule__ELSIF_Clause__Group__2__Impl : ( THEN ) ;
    public final void rule__ELSIF_Clause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4797:1: ( ( THEN ) )
            // InternalStructuredTextParser.g:4798:1: ( THEN )
            {
            // InternalStructuredTextParser.g:4798:1: ( THEN )
            // InternalStructuredTextParser.g:4799:1: THEN
            {
             before(grammarAccess.getELSIF_ClauseAccess().getTHENKeyword_2()); 
            match(input,THEN,FOLLOW_2); 
             after(grammarAccess.getELSIF_ClauseAccess().getTHENKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSIF_Clause__Group__2__Impl"


    // $ANTLR start "rule__ELSIF_Clause__Group__3"
    // InternalStructuredTextParser.g:4812:1: rule__ELSIF_Clause__Group__3 : rule__ELSIF_Clause__Group__3__Impl ;
    public final void rule__ELSIF_Clause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4816:1: ( rule__ELSIF_Clause__Group__3__Impl )
            // InternalStructuredTextParser.g:4817:2: rule__ELSIF_Clause__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ELSIF_Clause__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSIF_Clause__Group__3"


    // $ANTLR start "rule__ELSIF_Clause__Group__3__Impl"
    // InternalStructuredTextParser.g:4823:1: rule__ELSIF_Clause__Group__3__Impl : ( ( rule__ELSIF_Clause__StatementsAssignment_3 ) ) ;
    public final void rule__ELSIF_Clause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4827:1: ( ( ( rule__ELSIF_Clause__StatementsAssignment_3 ) ) )
            // InternalStructuredTextParser.g:4828:1: ( ( rule__ELSIF_Clause__StatementsAssignment_3 ) )
            {
            // InternalStructuredTextParser.g:4828:1: ( ( rule__ELSIF_Clause__StatementsAssignment_3 ) )
            // InternalStructuredTextParser.g:4829:1: ( rule__ELSIF_Clause__StatementsAssignment_3 )
            {
             before(grammarAccess.getELSIF_ClauseAccess().getStatementsAssignment_3()); 
            // InternalStructuredTextParser.g:4830:1: ( rule__ELSIF_Clause__StatementsAssignment_3 )
            // InternalStructuredTextParser.g:4830:2: rule__ELSIF_Clause__StatementsAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ELSIF_Clause__StatementsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getELSIF_ClauseAccess().getStatementsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSIF_Clause__Group__3__Impl"


    // $ANTLR start "rule__ELSE_Clause__Group__0"
    // InternalStructuredTextParser.g:4848:1: rule__ELSE_Clause__Group__0 : rule__ELSE_Clause__Group__0__Impl rule__ELSE_Clause__Group__1 ;
    public final void rule__ELSE_Clause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4852:1: ( rule__ELSE_Clause__Group__0__Impl rule__ELSE_Clause__Group__1 )
            // InternalStructuredTextParser.g:4853:2: rule__ELSE_Clause__Group__0__Impl rule__ELSE_Clause__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__ELSE_Clause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ELSE_Clause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSE_Clause__Group__0"


    // $ANTLR start "rule__ELSE_Clause__Group__0__Impl"
    // InternalStructuredTextParser.g:4860:1: rule__ELSE_Clause__Group__0__Impl : ( ELSE ) ;
    public final void rule__ELSE_Clause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4864:1: ( ( ELSE ) )
            // InternalStructuredTextParser.g:4865:1: ( ELSE )
            {
            // InternalStructuredTextParser.g:4865:1: ( ELSE )
            // InternalStructuredTextParser.g:4866:1: ELSE
            {
             before(grammarAccess.getELSE_ClauseAccess().getELSEKeyword_0()); 
            match(input,ELSE,FOLLOW_2); 
             after(grammarAccess.getELSE_ClauseAccess().getELSEKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSE_Clause__Group__0__Impl"


    // $ANTLR start "rule__ELSE_Clause__Group__1"
    // InternalStructuredTextParser.g:4879:1: rule__ELSE_Clause__Group__1 : rule__ELSE_Clause__Group__1__Impl ;
    public final void rule__ELSE_Clause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4883:1: ( rule__ELSE_Clause__Group__1__Impl )
            // InternalStructuredTextParser.g:4884:2: rule__ELSE_Clause__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ELSE_Clause__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSE_Clause__Group__1"


    // $ANTLR start "rule__ELSE_Clause__Group__1__Impl"
    // InternalStructuredTextParser.g:4890:1: rule__ELSE_Clause__Group__1__Impl : ( ( rule__ELSE_Clause__StatementsAssignment_1 ) ) ;
    public final void rule__ELSE_Clause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4894:1: ( ( ( rule__ELSE_Clause__StatementsAssignment_1 ) ) )
            // InternalStructuredTextParser.g:4895:1: ( ( rule__ELSE_Clause__StatementsAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:4895:1: ( ( rule__ELSE_Clause__StatementsAssignment_1 ) )
            // InternalStructuredTextParser.g:4896:1: ( rule__ELSE_Clause__StatementsAssignment_1 )
            {
             before(grammarAccess.getELSE_ClauseAccess().getStatementsAssignment_1()); 
            // InternalStructuredTextParser.g:4897:1: ( rule__ELSE_Clause__StatementsAssignment_1 )
            // InternalStructuredTextParser.g:4897:2: rule__ELSE_Clause__StatementsAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ELSE_Clause__StatementsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getELSE_ClauseAccess().getStatementsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSE_Clause__Group__1__Impl"


    // $ANTLR start "rule__Case_Stmt__Group__0"
    // InternalStructuredTextParser.g:4911:1: rule__Case_Stmt__Group__0 : rule__Case_Stmt__Group__0__Impl rule__Case_Stmt__Group__1 ;
    public final void rule__Case_Stmt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4915:1: ( rule__Case_Stmt__Group__0__Impl rule__Case_Stmt__Group__1 )
            // InternalStructuredTextParser.g:4916:2: rule__Case_Stmt__Group__0__Impl rule__Case_Stmt__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__Case_Stmt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case_Stmt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__Group__0"


    // $ANTLR start "rule__Case_Stmt__Group__0__Impl"
    // InternalStructuredTextParser.g:4923:1: rule__Case_Stmt__Group__0__Impl : ( CASE ) ;
    public final void rule__Case_Stmt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4927:1: ( ( CASE ) )
            // InternalStructuredTextParser.g:4928:1: ( CASE )
            {
            // InternalStructuredTextParser.g:4928:1: ( CASE )
            // InternalStructuredTextParser.g:4929:1: CASE
            {
             before(grammarAccess.getCase_StmtAccess().getCASEKeyword_0()); 
            match(input,CASE,FOLLOW_2); 
             after(grammarAccess.getCase_StmtAccess().getCASEKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__Group__0__Impl"


    // $ANTLR start "rule__Case_Stmt__Group__1"
    // InternalStructuredTextParser.g:4942:1: rule__Case_Stmt__Group__1 : rule__Case_Stmt__Group__1__Impl rule__Case_Stmt__Group__2 ;
    public final void rule__Case_Stmt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4946:1: ( rule__Case_Stmt__Group__1__Impl rule__Case_Stmt__Group__2 )
            // InternalStructuredTextParser.g:4947:2: rule__Case_Stmt__Group__1__Impl rule__Case_Stmt__Group__2
            {
            pushFollow(FOLLOW_22);
            rule__Case_Stmt__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case_Stmt__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__Group__1"


    // $ANTLR start "rule__Case_Stmt__Group__1__Impl"
    // InternalStructuredTextParser.g:4954:1: rule__Case_Stmt__Group__1__Impl : ( ( rule__Case_Stmt__ExpressionAssignment_1 ) ) ;
    public final void rule__Case_Stmt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4958:1: ( ( ( rule__Case_Stmt__ExpressionAssignment_1 ) ) )
            // InternalStructuredTextParser.g:4959:1: ( ( rule__Case_Stmt__ExpressionAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:4959:1: ( ( rule__Case_Stmt__ExpressionAssignment_1 ) )
            // InternalStructuredTextParser.g:4960:1: ( rule__Case_Stmt__ExpressionAssignment_1 )
            {
             before(grammarAccess.getCase_StmtAccess().getExpressionAssignment_1()); 
            // InternalStructuredTextParser.g:4961:1: ( rule__Case_Stmt__ExpressionAssignment_1 )
            // InternalStructuredTextParser.g:4961:2: rule__Case_Stmt__ExpressionAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Case_Stmt__ExpressionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCase_StmtAccess().getExpressionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__Group__1__Impl"


    // $ANTLR start "rule__Case_Stmt__Group__2"
    // InternalStructuredTextParser.g:4971:1: rule__Case_Stmt__Group__2 : rule__Case_Stmt__Group__2__Impl rule__Case_Stmt__Group__3 ;
    public final void rule__Case_Stmt__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4975:1: ( rule__Case_Stmt__Group__2__Impl rule__Case_Stmt__Group__3 )
            // InternalStructuredTextParser.g:4976:2: rule__Case_Stmt__Group__2__Impl rule__Case_Stmt__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__Case_Stmt__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case_Stmt__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__Group__2"


    // $ANTLR start "rule__Case_Stmt__Group__2__Impl"
    // InternalStructuredTextParser.g:4983:1: rule__Case_Stmt__Group__2__Impl : ( OF ) ;
    public final void rule__Case_Stmt__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:4987:1: ( ( OF ) )
            // InternalStructuredTextParser.g:4988:1: ( OF )
            {
            // InternalStructuredTextParser.g:4988:1: ( OF )
            // InternalStructuredTextParser.g:4989:1: OF
            {
             before(grammarAccess.getCase_StmtAccess().getOFKeyword_2()); 
            match(input,OF,FOLLOW_2); 
             after(grammarAccess.getCase_StmtAccess().getOFKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__Group__2__Impl"


    // $ANTLR start "rule__Case_Stmt__Group__3"
    // InternalStructuredTextParser.g:5002:1: rule__Case_Stmt__Group__3 : rule__Case_Stmt__Group__3__Impl rule__Case_Stmt__Group__4 ;
    public final void rule__Case_Stmt__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5006:1: ( rule__Case_Stmt__Group__3__Impl rule__Case_Stmt__Group__4 )
            // InternalStructuredTextParser.g:5007:2: rule__Case_Stmt__Group__3__Impl rule__Case_Stmt__Group__4
            {
            pushFollow(FOLLOW_23);
            rule__Case_Stmt__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case_Stmt__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__Group__3"


    // $ANTLR start "rule__Case_Stmt__Group__3__Impl"
    // InternalStructuredTextParser.g:5014:1: rule__Case_Stmt__Group__3__Impl : ( ( ( rule__Case_Stmt__CaseAssignment_3 ) ) ( ( rule__Case_Stmt__CaseAssignment_3 )* ) ) ;
    public final void rule__Case_Stmt__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5018:1: ( ( ( ( rule__Case_Stmt__CaseAssignment_3 ) ) ( ( rule__Case_Stmt__CaseAssignment_3 )* ) ) )
            // InternalStructuredTextParser.g:5019:1: ( ( ( rule__Case_Stmt__CaseAssignment_3 ) ) ( ( rule__Case_Stmt__CaseAssignment_3 )* ) )
            {
            // InternalStructuredTextParser.g:5019:1: ( ( ( rule__Case_Stmt__CaseAssignment_3 ) ) ( ( rule__Case_Stmt__CaseAssignment_3 )* ) )
            // InternalStructuredTextParser.g:5020:1: ( ( rule__Case_Stmt__CaseAssignment_3 ) ) ( ( rule__Case_Stmt__CaseAssignment_3 )* )
            {
            // InternalStructuredTextParser.g:5020:1: ( ( rule__Case_Stmt__CaseAssignment_3 ) )
            // InternalStructuredTextParser.g:5021:1: ( rule__Case_Stmt__CaseAssignment_3 )
            {
             before(grammarAccess.getCase_StmtAccess().getCaseAssignment_3()); 
            // InternalStructuredTextParser.g:5022:1: ( rule__Case_Stmt__CaseAssignment_3 )
            // InternalStructuredTextParser.g:5022:2: rule__Case_Stmt__CaseAssignment_3
            {
            pushFollow(FOLLOW_24);
            rule__Case_Stmt__CaseAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getCase_StmtAccess().getCaseAssignment_3()); 

            }

            // InternalStructuredTextParser.g:5025:1: ( ( rule__Case_Stmt__CaseAssignment_3 )* )
            // InternalStructuredTextParser.g:5026:1: ( rule__Case_Stmt__CaseAssignment_3 )*
            {
             before(grammarAccess.getCase_StmtAccess().getCaseAssignment_3()); 
            // InternalStructuredTextParser.g:5027:1: ( rule__Case_Stmt__CaseAssignment_3 )*
            loop43:
            do {
                int alt43=2;
                int LA43_0 = input.LA(1);

                if ( ((LA43_0>=LDATE_AND_TIME && LA43_0<=TIME_OF_DAY)||LA43_0==WSTRING||LA43_0==STRING||(LA43_0>=FALSE && LA43_0<=LTIME)||(LA43_0>=UDINT && LA43_0<=ULINT)||(LA43_0>=USINT && LA43_0<=WCHAR)||LA43_0==BOOL||(LA43_0>=CHAR && LA43_0<=DINT)||(LA43_0>=LINT && LA43_0<=SINT)||(LA43_0>=TIME && LA43_0<=UINT)||(LA43_0>=INT && LA43_0<=LDT)||LA43_0==TOD||LA43_0==DT||(LA43_0>=LD && LA43_0<=LT)||LA43_0==PlusSign||LA43_0==HyphenMinus||LA43_0==T||LA43_0==D_1||(LA43_0>=RULE_BINARY_INT && LA43_0<=RULE_UNSIGNED_INT)||LA43_0==RULE_S_BYTE_CHAR_STR||LA43_0==RULE_D_BYTE_CHAR_STR) ) {
                    alt43=1;
                }


                switch (alt43) {
            	case 1 :
            	    // InternalStructuredTextParser.g:5027:2: rule__Case_Stmt__CaseAssignment_3
            	    {
            	    pushFollow(FOLLOW_24);
            	    rule__Case_Stmt__CaseAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop43;
                }
            } while (true);

             after(grammarAccess.getCase_StmtAccess().getCaseAssignment_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__Group__3__Impl"


    // $ANTLR start "rule__Case_Stmt__Group__4"
    // InternalStructuredTextParser.g:5038:1: rule__Case_Stmt__Group__4 : rule__Case_Stmt__Group__4__Impl rule__Case_Stmt__Group__5 ;
    public final void rule__Case_Stmt__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5042:1: ( rule__Case_Stmt__Group__4__Impl rule__Case_Stmt__Group__5 )
            // InternalStructuredTextParser.g:5043:2: rule__Case_Stmt__Group__4__Impl rule__Case_Stmt__Group__5
            {
            pushFollow(FOLLOW_23);
            rule__Case_Stmt__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case_Stmt__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__Group__4"


    // $ANTLR start "rule__Case_Stmt__Group__4__Impl"
    // InternalStructuredTextParser.g:5050:1: rule__Case_Stmt__Group__4__Impl : ( ( rule__Case_Stmt__ElseAssignment_4 )? ) ;
    public final void rule__Case_Stmt__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5054:1: ( ( ( rule__Case_Stmt__ElseAssignment_4 )? ) )
            // InternalStructuredTextParser.g:5055:1: ( ( rule__Case_Stmt__ElseAssignment_4 )? )
            {
            // InternalStructuredTextParser.g:5055:1: ( ( rule__Case_Stmt__ElseAssignment_4 )? )
            // InternalStructuredTextParser.g:5056:1: ( rule__Case_Stmt__ElseAssignment_4 )?
            {
             before(grammarAccess.getCase_StmtAccess().getElseAssignment_4()); 
            // InternalStructuredTextParser.g:5057:1: ( rule__Case_Stmt__ElseAssignment_4 )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==ELSE) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalStructuredTextParser.g:5057:2: rule__Case_Stmt__ElseAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Case_Stmt__ElseAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCase_StmtAccess().getElseAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__Group__4__Impl"


    // $ANTLR start "rule__Case_Stmt__Group__5"
    // InternalStructuredTextParser.g:5067:1: rule__Case_Stmt__Group__5 : rule__Case_Stmt__Group__5__Impl ;
    public final void rule__Case_Stmt__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5071:1: ( rule__Case_Stmt__Group__5__Impl )
            // InternalStructuredTextParser.g:5072:2: rule__Case_Stmt__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Case_Stmt__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__Group__5"


    // $ANTLR start "rule__Case_Stmt__Group__5__Impl"
    // InternalStructuredTextParser.g:5078:1: rule__Case_Stmt__Group__5__Impl : ( END_CASE ) ;
    public final void rule__Case_Stmt__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5082:1: ( ( END_CASE ) )
            // InternalStructuredTextParser.g:5083:1: ( END_CASE )
            {
            // InternalStructuredTextParser.g:5083:1: ( END_CASE )
            // InternalStructuredTextParser.g:5084:1: END_CASE
            {
             before(grammarAccess.getCase_StmtAccess().getEND_CASEKeyword_5()); 
            match(input,END_CASE,FOLLOW_2); 
             after(grammarAccess.getCase_StmtAccess().getEND_CASEKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__Group__5__Impl"


    // $ANTLR start "rule__Case_Selection__Group__0"
    // InternalStructuredTextParser.g:5109:1: rule__Case_Selection__Group__0 : rule__Case_Selection__Group__0__Impl rule__Case_Selection__Group__1 ;
    public final void rule__Case_Selection__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5113:1: ( rule__Case_Selection__Group__0__Impl rule__Case_Selection__Group__1 )
            // InternalStructuredTextParser.g:5114:2: rule__Case_Selection__Group__0__Impl rule__Case_Selection__Group__1
            {
            pushFollow(FOLLOW_25);
            rule__Case_Selection__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case_Selection__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__Group__0"


    // $ANTLR start "rule__Case_Selection__Group__0__Impl"
    // InternalStructuredTextParser.g:5121:1: rule__Case_Selection__Group__0__Impl : ( ( rule__Case_Selection__CaseAssignment_0 ) ) ;
    public final void rule__Case_Selection__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5125:1: ( ( ( rule__Case_Selection__CaseAssignment_0 ) ) )
            // InternalStructuredTextParser.g:5126:1: ( ( rule__Case_Selection__CaseAssignment_0 ) )
            {
            // InternalStructuredTextParser.g:5126:1: ( ( rule__Case_Selection__CaseAssignment_0 ) )
            // InternalStructuredTextParser.g:5127:1: ( rule__Case_Selection__CaseAssignment_0 )
            {
             before(grammarAccess.getCase_SelectionAccess().getCaseAssignment_0()); 
            // InternalStructuredTextParser.g:5128:1: ( rule__Case_Selection__CaseAssignment_0 )
            // InternalStructuredTextParser.g:5128:2: rule__Case_Selection__CaseAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Case_Selection__CaseAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getCase_SelectionAccess().getCaseAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__Group__0__Impl"


    // $ANTLR start "rule__Case_Selection__Group__1"
    // InternalStructuredTextParser.g:5138:1: rule__Case_Selection__Group__1 : rule__Case_Selection__Group__1__Impl rule__Case_Selection__Group__2 ;
    public final void rule__Case_Selection__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5142:1: ( rule__Case_Selection__Group__1__Impl rule__Case_Selection__Group__2 )
            // InternalStructuredTextParser.g:5143:2: rule__Case_Selection__Group__1__Impl rule__Case_Selection__Group__2
            {
            pushFollow(FOLLOW_25);
            rule__Case_Selection__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case_Selection__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__Group__1"


    // $ANTLR start "rule__Case_Selection__Group__1__Impl"
    // InternalStructuredTextParser.g:5150:1: rule__Case_Selection__Group__1__Impl : ( ( rule__Case_Selection__Group_1__0 )* ) ;
    public final void rule__Case_Selection__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5154:1: ( ( ( rule__Case_Selection__Group_1__0 )* ) )
            // InternalStructuredTextParser.g:5155:1: ( ( rule__Case_Selection__Group_1__0 )* )
            {
            // InternalStructuredTextParser.g:5155:1: ( ( rule__Case_Selection__Group_1__0 )* )
            // InternalStructuredTextParser.g:5156:1: ( rule__Case_Selection__Group_1__0 )*
            {
             before(grammarAccess.getCase_SelectionAccess().getGroup_1()); 
            // InternalStructuredTextParser.g:5157:1: ( rule__Case_Selection__Group_1__0 )*
            loop45:
            do {
                int alt45=2;
                int LA45_0 = input.LA(1);

                if ( (LA45_0==Comma) ) {
                    alt45=1;
                }


                switch (alt45) {
            	case 1 :
            	    // InternalStructuredTextParser.g:5157:2: rule__Case_Selection__Group_1__0
            	    {
            	    pushFollow(FOLLOW_26);
            	    rule__Case_Selection__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop45;
                }
            } while (true);

             after(grammarAccess.getCase_SelectionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__Group__1__Impl"


    // $ANTLR start "rule__Case_Selection__Group__2"
    // InternalStructuredTextParser.g:5167:1: rule__Case_Selection__Group__2 : rule__Case_Selection__Group__2__Impl rule__Case_Selection__Group__3 ;
    public final void rule__Case_Selection__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5171:1: ( rule__Case_Selection__Group__2__Impl rule__Case_Selection__Group__3 )
            // InternalStructuredTextParser.g:5172:2: rule__Case_Selection__Group__2__Impl rule__Case_Selection__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__Case_Selection__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case_Selection__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__Group__2"


    // $ANTLR start "rule__Case_Selection__Group__2__Impl"
    // InternalStructuredTextParser.g:5179:1: rule__Case_Selection__Group__2__Impl : ( Colon ) ;
    public final void rule__Case_Selection__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5183:1: ( ( Colon ) )
            // InternalStructuredTextParser.g:5184:1: ( Colon )
            {
            // InternalStructuredTextParser.g:5184:1: ( Colon )
            // InternalStructuredTextParser.g:5185:1: Colon
            {
             before(grammarAccess.getCase_SelectionAccess().getColonKeyword_2()); 
            match(input,Colon,FOLLOW_2); 
             after(grammarAccess.getCase_SelectionAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__Group__2__Impl"


    // $ANTLR start "rule__Case_Selection__Group__3"
    // InternalStructuredTextParser.g:5198:1: rule__Case_Selection__Group__3 : rule__Case_Selection__Group__3__Impl ;
    public final void rule__Case_Selection__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5202:1: ( rule__Case_Selection__Group__3__Impl )
            // InternalStructuredTextParser.g:5203:2: rule__Case_Selection__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Case_Selection__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__Group__3"


    // $ANTLR start "rule__Case_Selection__Group__3__Impl"
    // InternalStructuredTextParser.g:5209:1: rule__Case_Selection__Group__3__Impl : ( ( rule__Case_Selection__StatementsAssignment_3 ) ) ;
    public final void rule__Case_Selection__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5213:1: ( ( ( rule__Case_Selection__StatementsAssignment_3 ) ) )
            // InternalStructuredTextParser.g:5214:1: ( ( rule__Case_Selection__StatementsAssignment_3 ) )
            {
            // InternalStructuredTextParser.g:5214:1: ( ( rule__Case_Selection__StatementsAssignment_3 ) )
            // InternalStructuredTextParser.g:5215:1: ( rule__Case_Selection__StatementsAssignment_3 )
            {
             before(grammarAccess.getCase_SelectionAccess().getStatementsAssignment_3()); 
            // InternalStructuredTextParser.g:5216:1: ( rule__Case_Selection__StatementsAssignment_3 )
            // InternalStructuredTextParser.g:5216:2: rule__Case_Selection__StatementsAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Case_Selection__StatementsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getCase_SelectionAccess().getStatementsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__Group__3__Impl"


    // $ANTLR start "rule__Case_Selection__Group_1__0"
    // InternalStructuredTextParser.g:5234:1: rule__Case_Selection__Group_1__0 : rule__Case_Selection__Group_1__0__Impl rule__Case_Selection__Group_1__1 ;
    public final void rule__Case_Selection__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5238:1: ( rule__Case_Selection__Group_1__0__Impl rule__Case_Selection__Group_1__1 )
            // InternalStructuredTextParser.g:5239:2: rule__Case_Selection__Group_1__0__Impl rule__Case_Selection__Group_1__1
            {
            pushFollow(FOLLOW_11);
            rule__Case_Selection__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case_Selection__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__Group_1__0"


    // $ANTLR start "rule__Case_Selection__Group_1__0__Impl"
    // InternalStructuredTextParser.g:5246:1: rule__Case_Selection__Group_1__0__Impl : ( Comma ) ;
    public final void rule__Case_Selection__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5250:1: ( ( Comma ) )
            // InternalStructuredTextParser.g:5251:1: ( Comma )
            {
            // InternalStructuredTextParser.g:5251:1: ( Comma )
            // InternalStructuredTextParser.g:5252:1: Comma
            {
             before(grammarAccess.getCase_SelectionAccess().getCommaKeyword_1_0()); 
            match(input,Comma,FOLLOW_2); 
             after(grammarAccess.getCase_SelectionAccess().getCommaKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__Group_1__0__Impl"


    // $ANTLR start "rule__Case_Selection__Group_1__1"
    // InternalStructuredTextParser.g:5265:1: rule__Case_Selection__Group_1__1 : rule__Case_Selection__Group_1__1__Impl ;
    public final void rule__Case_Selection__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5269:1: ( rule__Case_Selection__Group_1__1__Impl )
            // InternalStructuredTextParser.g:5270:2: rule__Case_Selection__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Case_Selection__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__Group_1__1"


    // $ANTLR start "rule__Case_Selection__Group_1__1__Impl"
    // InternalStructuredTextParser.g:5276:1: rule__Case_Selection__Group_1__1__Impl : ( ( rule__Case_Selection__CaseAssignment_1_1 ) ) ;
    public final void rule__Case_Selection__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5280:1: ( ( ( rule__Case_Selection__CaseAssignment_1_1 ) ) )
            // InternalStructuredTextParser.g:5281:1: ( ( rule__Case_Selection__CaseAssignment_1_1 ) )
            {
            // InternalStructuredTextParser.g:5281:1: ( ( rule__Case_Selection__CaseAssignment_1_1 ) )
            // InternalStructuredTextParser.g:5282:1: ( rule__Case_Selection__CaseAssignment_1_1 )
            {
             before(grammarAccess.getCase_SelectionAccess().getCaseAssignment_1_1()); 
            // InternalStructuredTextParser.g:5283:1: ( rule__Case_Selection__CaseAssignment_1_1 )
            // InternalStructuredTextParser.g:5283:2: rule__Case_Selection__CaseAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Case_Selection__CaseAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getCase_SelectionAccess().getCaseAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__Group_1__1__Impl"


    // $ANTLR start "rule__Iteration_Stmt__Group_3__0"
    // InternalStructuredTextParser.g:5297:1: rule__Iteration_Stmt__Group_3__0 : rule__Iteration_Stmt__Group_3__0__Impl rule__Iteration_Stmt__Group_3__1 ;
    public final void rule__Iteration_Stmt__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5301:1: ( rule__Iteration_Stmt__Group_3__0__Impl rule__Iteration_Stmt__Group_3__1 )
            // InternalStructuredTextParser.g:5302:2: rule__Iteration_Stmt__Group_3__0__Impl rule__Iteration_Stmt__Group_3__1
            {
            pushFollow(FOLLOW_27);
            rule__Iteration_Stmt__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Iteration_Stmt__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iteration_Stmt__Group_3__0"


    // $ANTLR start "rule__Iteration_Stmt__Group_3__0__Impl"
    // InternalStructuredTextParser.g:5309:1: rule__Iteration_Stmt__Group_3__0__Impl : ( () ) ;
    public final void rule__Iteration_Stmt__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5313:1: ( ( () ) )
            // InternalStructuredTextParser.g:5314:1: ( () )
            {
            // InternalStructuredTextParser.g:5314:1: ( () )
            // InternalStructuredTextParser.g:5315:1: ()
            {
             before(grammarAccess.getIteration_StmtAccess().getExitStatementAction_3_0()); 
            // InternalStructuredTextParser.g:5316:1: ()
            // InternalStructuredTextParser.g:5318:1: 
            {
            }

             after(grammarAccess.getIteration_StmtAccess().getExitStatementAction_3_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iteration_Stmt__Group_3__0__Impl"


    // $ANTLR start "rule__Iteration_Stmt__Group_3__1"
    // InternalStructuredTextParser.g:5328:1: rule__Iteration_Stmt__Group_3__1 : rule__Iteration_Stmt__Group_3__1__Impl ;
    public final void rule__Iteration_Stmt__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5332:1: ( rule__Iteration_Stmt__Group_3__1__Impl )
            // InternalStructuredTextParser.g:5333:2: rule__Iteration_Stmt__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Iteration_Stmt__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iteration_Stmt__Group_3__1"


    // $ANTLR start "rule__Iteration_Stmt__Group_3__1__Impl"
    // InternalStructuredTextParser.g:5339:1: rule__Iteration_Stmt__Group_3__1__Impl : ( EXIT ) ;
    public final void rule__Iteration_Stmt__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5343:1: ( ( EXIT ) )
            // InternalStructuredTextParser.g:5344:1: ( EXIT )
            {
            // InternalStructuredTextParser.g:5344:1: ( EXIT )
            // InternalStructuredTextParser.g:5345:1: EXIT
            {
             before(grammarAccess.getIteration_StmtAccess().getEXITKeyword_3_1()); 
            match(input,EXIT,FOLLOW_2); 
             after(grammarAccess.getIteration_StmtAccess().getEXITKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iteration_Stmt__Group_3__1__Impl"


    // $ANTLR start "rule__Iteration_Stmt__Group_4__0"
    // InternalStructuredTextParser.g:5362:1: rule__Iteration_Stmt__Group_4__0 : rule__Iteration_Stmt__Group_4__0__Impl rule__Iteration_Stmt__Group_4__1 ;
    public final void rule__Iteration_Stmt__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5366:1: ( rule__Iteration_Stmt__Group_4__0__Impl rule__Iteration_Stmt__Group_4__1 )
            // InternalStructuredTextParser.g:5367:2: rule__Iteration_Stmt__Group_4__0__Impl rule__Iteration_Stmt__Group_4__1
            {
            pushFollow(FOLLOW_28);
            rule__Iteration_Stmt__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Iteration_Stmt__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iteration_Stmt__Group_4__0"


    // $ANTLR start "rule__Iteration_Stmt__Group_4__0__Impl"
    // InternalStructuredTextParser.g:5374:1: rule__Iteration_Stmt__Group_4__0__Impl : ( () ) ;
    public final void rule__Iteration_Stmt__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5378:1: ( ( () ) )
            // InternalStructuredTextParser.g:5379:1: ( () )
            {
            // InternalStructuredTextParser.g:5379:1: ( () )
            // InternalStructuredTextParser.g:5380:1: ()
            {
             before(grammarAccess.getIteration_StmtAccess().getContinueStatementAction_4_0()); 
            // InternalStructuredTextParser.g:5381:1: ()
            // InternalStructuredTextParser.g:5383:1: 
            {
            }

             after(grammarAccess.getIteration_StmtAccess().getContinueStatementAction_4_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iteration_Stmt__Group_4__0__Impl"


    // $ANTLR start "rule__Iteration_Stmt__Group_4__1"
    // InternalStructuredTextParser.g:5393:1: rule__Iteration_Stmt__Group_4__1 : rule__Iteration_Stmt__Group_4__1__Impl ;
    public final void rule__Iteration_Stmt__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5397:1: ( rule__Iteration_Stmt__Group_4__1__Impl )
            // InternalStructuredTextParser.g:5398:2: rule__Iteration_Stmt__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Iteration_Stmt__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iteration_Stmt__Group_4__1"


    // $ANTLR start "rule__Iteration_Stmt__Group_4__1__Impl"
    // InternalStructuredTextParser.g:5404:1: rule__Iteration_Stmt__Group_4__1__Impl : ( CONTINUE ) ;
    public final void rule__Iteration_Stmt__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5408:1: ( ( CONTINUE ) )
            // InternalStructuredTextParser.g:5409:1: ( CONTINUE )
            {
            // InternalStructuredTextParser.g:5409:1: ( CONTINUE )
            // InternalStructuredTextParser.g:5410:1: CONTINUE
            {
             before(grammarAccess.getIteration_StmtAccess().getCONTINUEKeyword_4_1()); 
            match(input,CONTINUE,FOLLOW_2); 
             after(grammarAccess.getIteration_StmtAccess().getCONTINUEKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iteration_Stmt__Group_4__1__Impl"


    // $ANTLR start "rule__For_Stmt__Group__0"
    // InternalStructuredTextParser.g:5427:1: rule__For_Stmt__Group__0 : rule__For_Stmt__Group__0__Impl rule__For_Stmt__Group__1 ;
    public final void rule__For_Stmt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5431:1: ( rule__For_Stmt__Group__0__Impl rule__For_Stmt__Group__1 )
            // InternalStructuredTextParser.g:5432:2: rule__For_Stmt__Group__0__Impl rule__For_Stmt__Group__1
            {
            pushFollow(FOLLOW_29);
            rule__For_Stmt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__For_Stmt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__0"


    // $ANTLR start "rule__For_Stmt__Group__0__Impl"
    // InternalStructuredTextParser.g:5439:1: rule__For_Stmt__Group__0__Impl : ( FOR ) ;
    public final void rule__For_Stmt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5443:1: ( ( FOR ) )
            // InternalStructuredTextParser.g:5444:1: ( FOR )
            {
            // InternalStructuredTextParser.g:5444:1: ( FOR )
            // InternalStructuredTextParser.g:5445:1: FOR
            {
             before(grammarAccess.getFor_StmtAccess().getFORKeyword_0()); 
            match(input,FOR,FOLLOW_2); 
             after(grammarAccess.getFor_StmtAccess().getFORKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__0__Impl"


    // $ANTLR start "rule__For_Stmt__Group__1"
    // InternalStructuredTextParser.g:5458:1: rule__For_Stmt__Group__1 : rule__For_Stmt__Group__1__Impl rule__For_Stmt__Group__2 ;
    public final void rule__For_Stmt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5462:1: ( rule__For_Stmt__Group__1__Impl rule__For_Stmt__Group__2 )
            // InternalStructuredTextParser.g:5463:2: rule__For_Stmt__Group__1__Impl rule__For_Stmt__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__For_Stmt__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__For_Stmt__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__1"


    // $ANTLR start "rule__For_Stmt__Group__1__Impl"
    // InternalStructuredTextParser.g:5470:1: rule__For_Stmt__Group__1__Impl : ( ( rule__For_Stmt__VariableAssignment_1 ) ) ;
    public final void rule__For_Stmt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5474:1: ( ( ( rule__For_Stmt__VariableAssignment_1 ) ) )
            // InternalStructuredTextParser.g:5475:1: ( ( rule__For_Stmt__VariableAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:5475:1: ( ( rule__For_Stmt__VariableAssignment_1 ) )
            // InternalStructuredTextParser.g:5476:1: ( rule__For_Stmt__VariableAssignment_1 )
            {
             before(grammarAccess.getFor_StmtAccess().getVariableAssignment_1()); 
            // InternalStructuredTextParser.g:5477:1: ( rule__For_Stmt__VariableAssignment_1 )
            // InternalStructuredTextParser.g:5477:2: rule__For_Stmt__VariableAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__For_Stmt__VariableAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFor_StmtAccess().getVariableAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__1__Impl"


    // $ANTLR start "rule__For_Stmt__Group__2"
    // InternalStructuredTextParser.g:5487:1: rule__For_Stmt__Group__2 : rule__For_Stmt__Group__2__Impl rule__For_Stmt__Group__3 ;
    public final void rule__For_Stmt__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5491:1: ( rule__For_Stmt__Group__2__Impl rule__For_Stmt__Group__3 )
            // InternalStructuredTextParser.g:5492:2: rule__For_Stmt__Group__2__Impl rule__For_Stmt__Group__3
            {
            pushFollow(FOLLOW_14);
            rule__For_Stmt__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__For_Stmt__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__2"


    // $ANTLR start "rule__For_Stmt__Group__2__Impl"
    // InternalStructuredTextParser.g:5499:1: rule__For_Stmt__Group__2__Impl : ( ColonEqualsSign ) ;
    public final void rule__For_Stmt__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5503:1: ( ( ColonEqualsSign ) )
            // InternalStructuredTextParser.g:5504:1: ( ColonEqualsSign )
            {
            // InternalStructuredTextParser.g:5504:1: ( ColonEqualsSign )
            // InternalStructuredTextParser.g:5505:1: ColonEqualsSign
            {
             before(grammarAccess.getFor_StmtAccess().getColonEqualsSignKeyword_2()); 
            match(input,ColonEqualsSign,FOLLOW_2); 
             after(grammarAccess.getFor_StmtAccess().getColonEqualsSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__2__Impl"


    // $ANTLR start "rule__For_Stmt__Group__3"
    // InternalStructuredTextParser.g:5518:1: rule__For_Stmt__Group__3 : rule__For_Stmt__Group__3__Impl rule__For_Stmt__Group__4 ;
    public final void rule__For_Stmt__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5522:1: ( rule__For_Stmt__Group__3__Impl rule__For_Stmt__Group__4 )
            // InternalStructuredTextParser.g:5523:2: rule__For_Stmt__Group__3__Impl rule__For_Stmt__Group__4
            {
            pushFollow(FOLLOW_30);
            rule__For_Stmt__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__For_Stmt__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__3"


    // $ANTLR start "rule__For_Stmt__Group__3__Impl"
    // InternalStructuredTextParser.g:5530:1: rule__For_Stmt__Group__3__Impl : ( ( rule__For_Stmt__FromAssignment_3 ) ) ;
    public final void rule__For_Stmt__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5534:1: ( ( ( rule__For_Stmt__FromAssignment_3 ) ) )
            // InternalStructuredTextParser.g:5535:1: ( ( rule__For_Stmt__FromAssignment_3 ) )
            {
            // InternalStructuredTextParser.g:5535:1: ( ( rule__For_Stmt__FromAssignment_3 ) )
            // InternalStructuredTextParser.g:5536:1: ( rule__For_Stmt__FromAssignment_3 )
            {
             before(grammarAccess.getFor_StmtAccess().getFromAssignment_3()); 
            // InternalStructuredTextParser.g:5537:1: ( rule__For_Stmt__FromAssignment_3 )
            // InternalStructuredTextParser.g:5537:2: rule__For_Stmt__FromAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__For_Stmt__FromAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getFor_StmtAccess().getFromAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__3__Impl"


    // $ANTLR start "rule__For_Stmt__Group__4"
    // InternalStructuredTextParser.g:5547:1: rule__For_Stmt__Group__4 : rule__For_Stmt__Group__4__Impl rule__For_Stmt__Group__5 ;
    public final void rule__For_Stmt__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5551:1: ( rule__For_Stmt__Group__4__Impl rule__For_Stmt__Group__5 )
            // InternalStructuredTextParser.g:5552:2: rule__For_Stmt__Group__4__Impl rule__For_Stmt__Group__5
            {
            pushFollow(FOLLOW_14);
            rule__For_Stmt__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__For_Stmt__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__4"


    // $ANTLR start "rule__For_Stmt__Group__4__Impl"
    // InternalStructuredTextParser.g:5559:1: rule__For_Stmt__Group__4__Impl : ( TO ) ;
    public final void rule__For_Stmt__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5563:1: ( ( TO ) )
            // InternalStructuredTextParser.g:5564:1: ( TO )
            {
            // InternalStructuredTextParser.g:5564:1: ( TO )
            // InternalStructuredTextParser.g:5565:1: TO
            {
             before(grammarAccess.getFor_StmtAccess().getTOKeyword_4()); 
            match(input,TO,FOLLOW_2); 
             after(grammarAccess.getFor_StmtAccess().getTOKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__4__Impl"


    // $ANTLR start "rule__For_Stmt__Group__5"
    // InternalStructuredTextParser.g:5578:1: rule__For_Stmt__Group__5 : rule__For_Stmt__Group__5__Impl rule__For_Stmt__Group__6 ;
    public final void rule__For_Stmt__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5582:1: ( rule__For_Stmt__Group__5__Impl rule__For_Stmt__Group__6 )
            // InternalStructuredTextParser.g:5583:2: rule__For_Stmt__Group__5__Impl rule__For_Stmt__Group__6
            {
            pushFollow(FOLLOW_31);
            rule__For_Stmt__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__For_Stmt__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__5"


    // $ANTLR start "rule__For_Stmt__Group__5__Impl"
    // InternalStructuredTextParser.g:5590:1: rule__For_Stmt__Group__5__Impl : ( ( rule__For_Stmt__ToAssignment_5 ) ) ;
    public final void rule__For_Stmt__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5594:1: ( ( ( rule__For_Stmt__ToAssignment_5 ) ) )
            // InternalStructuredTextParser.g:5595:1: ( ( rule__For_Stmt__ToAssignment_5 ) )
            {
            // InternalStructuredTextParser.g:5595:1: ( ( rule__For_Stmt__ToAssignment_5 ) )
            // InternalStructuredTextParser.g:5596:1: ( rule__For_Stmt__ToAssignment_5 )
            {
             before(grammarAccess.getFor_StmtAccess().getToAssignment_5()); 
            // InternalStructuredTextParser.g:5597:1: ( rule__For_Stmt__ToAssignment_5 )
            // InternalStructuredTextParser.g:5597:2: rule__For_Stmt__ToAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__For_Stmt__ToAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getFor_StmtAccess().getToAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__5__Impl"


    // $ANTLR start "rule__For_Stmt__Group__6"
    // InternalStructuredTextParser.g:5607:1: rule__For_Stmt__Group__6 : rule__For_Stmt__Group__6__Impl rule__For_Stmt__Group__7 ;
    public final void rule__For_Stmt__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5611:1: ( rule__For_Stmt__Group__6__Impl rule__For_Stmt__Group__7 )
            // InternalStructuredTextParser.g:5612:2: rule__For_Stmt__Group__6__Impl rule__For_Stmt__Group__7
            {
            pushFollow(FOLLOW_31);
            rule__For_Stmt__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__For_Stmt__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__6"


    // $ANTLR start "rule__For_Stmt__Group__6__Impl"
    // InternalStructuredTextParser.g:5619:1: rule__For_Stmt__Group__6__Impl : ( ( rule__For_Stmt__Group_6__0 )? ) ;
    public final void rule__For_Stmt__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5623:1: ( ( ( rule__For_Stmt__Group_6__0 )? ) )
            // InternalStructuredTextParser.g:5624:1: ( ( rule__For_Stmt__Group_6__0 )? )
            {
            // InternalStructuredTextParser.g:5624:1: ( ( rule__For_Stmt__Group_6__0 )? )
            // InternalStructuredTextParser.g:5625:1: ( rule__For_Stmt__Group_6__0 )?
            {
             before(grammarAccess.getFor_StmtAccess().getGroup_6()); 
            // InternalStructuredTextParser.g:5626:1: ( rule__For_Stmt__Group_6__0 )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==BY) ) {
                alt46=1;
            }
            switch (alt46) {
                case 1 :
                    // InternalStructuredTextParser.g:5626:2: rule__For_Stmt__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__For_Stmt__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFor_StmtAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__6__Impl"


    // $ANTLR start "rule__For_Stmt__Group__7"
    // InternalStructuredTextParser.g:5636:1: rule__For_Stmt__Group__7 : rule__For_Stmt__Group__7__Impl rule__For_Stmt__Group__8 ;
    public final void rule__For_Stmt__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5640:1: ( rule__For_Stmt__Group__7__Impl rule__For_Stmt__Group__8 )
            // InternalStructuredTextParser.g:5641:2: rule__For_Stmt__Group__7__Impl rule__For_Stmt__Group__8
            {
            pushFollow(FOLLOW_3);
            rule__For_Stmt__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__For_Stmt__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__7"


    // $ANTLR start "rule__For_Stmt__Group__7__Impl"
    // InternalStructuredTextParser.g:5648:1: rule__For_Stmt__Group__7__Impl : ( DO ) ;
    public final void rule__For_Stmt__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5652:1: ( ( DO ) )
            // InternalStructuredTextParser.g:5653:1: ( DO )
            {
            // InternalStructuredTextParser.g:5653:1: ( DO )
            // InternalStructuredTextParser.g:5654:1: DO
            {
             before(grammarAccess.getFor_StmtAccess().getDOKeyword_7()); 
            match(input,DO,FOLLOW_2); 
             after(grammarAccess.getFor_StmtAccess().getDOKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__7__Impl"


    // $ANTLR start "rule__For_Stmt__Group__8"
    // InternalStructuredTextParser.g:5667:1: rule__For_Stmt__Group__8 : rule__For_Stmt__Group__8__Impl rule__For_Stmt__Group__9 ;
    public final void rule__For_Stmt__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5671:1: ( rule__For_Stmt__Group__8__Impl rule__For_Stmt__Group__9 )
            // InternalStructuredTextParser.g:5672:2: rule__For_Stmt__Group__8__Impl rule__For_Stmt__Group__9
            {
            pushFollow(FOLLOW_32);
            rule__For_Stmt__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__For_Stmt__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__8"


    // $ANTLR start "rule__For_Stmt__Group__8__Impl"
    // InternalStructuredTextParser.g:5679:1: rule__For_Stmt__Group__8__Impl : ( ( rule__For_Stmt__StatementsAssignment_8 ) ) ;
    public final void rule__For_Stmt__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5683:1: ( ( ( rule__For_Stmt__StatementsAssignment_8 ) ) )
            // InternalStructuredTextParser.g:5684:1: ( ( rule__For_Stmt__StatementsAssignment_8 ) )
            {
            // InternalStructuredTextParser.g:5684:1: ( ( rule__For_Stmt__StatementsAssignment_8 ) )
            // InternalStructuredTextParser.g:5685:1: ( rule__For_Stmt__StatementsAssignment_8 )
            {
             before(grammarAccess.getFor_StmtAccess().getStatementsAssignment_8()); 
            // InternalStructuredTextParser.g:5686:1: ( rule__For_Stmt__StatementsAssignment_8 )
            // InternalStructuredTextParser.g:5686:2: rule__For_Stmt__StatementsAssignment_8
            {
            pushFollow(FOLLOW_2);
            rule__For_Stmt__StatementsAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getFor_StmtAccess().getStatementsAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__8__Impl"


    // $ANTLR start "rule__For_Stmt__Group__9"
    // InternalStructuredTextParser.g:5696:1: rule__For_Stmt__Group__9 : rule__For_Stmt__Group__9__Impl ;
    public final void rule__For_Stmt__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5700:1: ( rule__For_Stmt__Group__9__Impl )
            // InternalStructuredTextParser.g:5701:2: rule__For_Stmt__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__For_Stmt__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__9"


    // $ANTLR start "rule__For_Stmt__Group__9__Impl"
    // InternalStructuredTextParser.g:5707:1: rule__For_Stmt__Group__9__Impl : ( END_FOR ) ;
    public final void rule__For_Stmt__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5711:1: ( ( END_FOR ) )
            // InternalStructuredTextParser.g:5712:1: ( END_FOR )
            {
            // InternalStructuredTextParser.g:5712:1: ( END_FOR )
            // InternalStructuredTextParser.g:5713:1: END_FOR
            {
             before(grammarAccess.getFor_StmtAccess().getEND_FORKeyword_9()); 
            match(input,END_FOR,FOLLOW_2); 
             after(grammarAccess.getFor_StmtAccess().getEND_FORKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group__9__Impl"


    // $ANTLR start "rule__For_Stmt__Group_6__0"
    // InternalStructuredTextParser.g:5746:1: rule__For_Stmt__Group_6__0 : rule__For_Stmt__Group_6__0__Impl rule__For_Stmt__Group_6__1 ;
    public final void rule__For_Stmt__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5750:1: ( rule__For_Stmt__Group_6__0__Impl rule__For_Stmt__Group_6__1 )
            // InternalStructuredTextParser.g:5751:2: rule__For_Stmt__Group_6__0__Impl rule__For_Stmt__Group_6__1
            {
            pushFollow(FOLLOW_14);
            rule__For_Stmt__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__For_Stmt__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group_6__0"


    // $ANTLR start "rule__For_Stmt__Group_6__0__Impl"
    // InternalStructuredTextParser.g:5758:1: rule__For_Stmt__Group_6__0__Impl : ( BY ) ;
    public final void rule__For_Stmt__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5762:1: ( ( BY ) )
            // InternalStructuredTextParser.g:5763:1: ( BY )
            {
            // InternalStructuredTextParser.g:5763:1: ( BY )
            // InternalStructuredTextParser.g:5764:1: BY
            {
             before(grammarAccess.getFor_StmtAccess().getBYKeyword_6_0()); 
            match(input,BY,FOLLOW_2); 
             after(grammarAccess.getFor_StmtAccess().getBYKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group_6__0__Impl"


    // $ANTLR start "rule__For_Stmt__Group_6__1"
    // InternalStructuredTextParser.g:5777:1: rule__For_Stmt__Group_6__1 : rule__For_Stmt__Group_6__1__Impl ;
    public final void rule__For_Stmt__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5781:1: ( rule__For_Stmt__Group_6__1__Impl )
            // InternalStructuredTextParser.g:5782:2: rule__For_Stmt__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__For_Stmt__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group_6__1"


    // $ANTLR start "rule__For_Stmt__Group_6__1__Impl"
    // InternalStructuredTextParser.g:5788:1: rule__For_Stmt__Group_6__1__Impl : ( ( rule__For_Stmt__ByAssignment_6_1 ) ) ;
    public final void rule__For_Stmt__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5792:1: ( ( ( rule__For_Stmt__ByAssignment_6_1 ) ) )
            // InternalStructuredTextParser.g:5793:1: ( ( rule__For_Stmt__ByAssignment_6_1 ) )
            {
            // InternalStructuredTextParser.g:5793:1: ( ( rule__For_Stmt__ByAssignment_6_1 ) )
            // InternalStructuredTextParser.g:5794:1: ( rule__For_Stmt__ByAssignment_6_1 )
            {
             before(grammarAccess.getFor_StmtAccess().getByAssignment_6_1()); 
            // InternalStructuredTextParser.g:5795:1: ( rule__For_Stmt__ByAssignment_6_1 )
            // InternalStructuredTextParser.g:5795:2: rule__For_Stmt__ByAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__For_Stmt__ByAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getFor_StmtAccess().getByAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__Group_6__1__Impl"


    // $ANTLR start "rule__While_Stmt__Group__0"
    // InternalStructuredTextParser.g:5809:1: rule__While_Stmt__Group__0 : rule__While_Stmt__Group__0__Impl rule__While_Stmt__Group__1 ;
    public final void rule__While_Stmt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5813:1: ( rule__While_Stmt__Group__0__Impl rule__While_Stmt__Group__1 )
            // InternalStructuredTextParser.g:5814:2: rule__While_Stmt__Group__0__Impl rule__While_Stmt__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__While_Stmt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__While_Stmt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__While_Stmt__Group__0"


    // $ANTLR start "rule__While_Stmt__Group__0__Impl"
    // InternalStructuredTextParser.g:5821:1: rule__While_Stmt__Group__0__Impl : ( WHILE ) ;
    public final void rule__While_Stmt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5825:1: ( ( WHILE ) )
            // InternalStructuredTextParser.g:5826:1: ( WHILE )
            {
            // InternalStructuredTextParser.g:5826:1: ( WHILE )
            // InternalStructuredTextParser.g:5827:1: WHILE
            {
             before(grammarAccess.getWhile_StmtAccess().getWHILEKeyword_0()); 
            match(input,WHILE,FOLLOW_2); 
             after(grammarAccess.getWhile_StmtAccess().getWHILEKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__While_Stmt__Group__0__Impl"


    // $ANTLR start "rule__While_Stmt__Group__1"
    // InternalStructuredTextParser.g:5840:1: rule__While_Stmt__Group__1 : rule__While_Stmt__Group__1__Impl rule__While_Stmt__Group__2 ;
    public final void rule__While_Stmt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5844:1: ( rule__While_Stmt__Group__1__Impl rule__While_Stmt__Group__2 )
            // InternalStructuredTextParser.g:5845:2: rule__While_Stmt__Group__1__Impl rule__While_Stmt__Group__2
            {
            pushFollow(FOLLOW_33);
            rule__While_Stmt__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__While_Stmt__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__While_Stmt__Group__1"


    // $ANTLR start "rule__While_Stmt__Group__1__Impl"
    // InternalStructuredTextParser.g:5852:1: rule__While_Stmt__Group__1__Impl : ( ( rule__While_Stmt__ExpressionAssignment_1 ) ) ;
    public final void rule__While_Stmt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5856:1: ( ( ( rule__While_Stmt__ExpressionAssignment_1 ) ) )
            // InternalStructuredTextParser.g:5857:1: ( ( rule__While_Stmt__ExpressionAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:5857:1: ( ( rule__While_Stmt__ExpressionAssignment_1 ) )
            // InternalStructuredTextParser.g:5858:1: ( rule__While_Stmt__ExpressionAssignment_1 )
            {
             before(grammarAccess.getWhile_StmtAccess().getExpressionAssignment_1()); 
            // InternalStructuredTextParser.g:5859:1: ( rule__While_Stmt__ExpressionAssignment_1 )
            // InternalStructuredTextParser.g:5859:2: rule__While_Stmt__ExpressionAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__While_Stmt__ExpressionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getWhile_StmtAccess().getExpressionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__While_Stmt__Group__1__Impl"


    // $ANTLR start "rule__While_Stmt__Group__2"
    // InternalStructuredTextParser.g:5869:1: rule__While_Stmt__Group__2 : rule__While_Stmt__Group__2__Impl rule__While_Stmt__Group__3 ;
    public final void rule__While_Stmt__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5873:1: ( rule__While_Stmt__Group__2__Impl rule__While_Stmt__Group__3 )
            // InternalStructuredTextParser.g:5874:2: rule__While_Stmt__Group__2__Impl rule__While_Stmt__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__While_Stmt__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__While_Stmt__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__While_Stmt__Group__2"


    // $ANTLR start "rule__While_Stmt__Group__2__Impl"
    // InternalStructuredTextParser.g:5881:1: rule__While_Stmt__Group__2__Impl : ( DO ) ;
    public final void rule__While_Stmt__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5885:1: ( ( DO ) )
            // InternalStructuredTextParser.g:5886:1: ( DO )
            {
            // InternalStructuredTextParser.g:5886:1: ( DO )
            // InternalStructuredTextParser.g:5887:1: DO
            {
             before(grammarAccess.getWhile_StmtAccess().getDOKeyword_2()); 
            match(input,DO,FOLLOW_2); 
             after(grammarAccess.getWhile_StmtAccess().getDOKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__While_Stmt__Group__2__Impl"


    // $ANTLR start "rule__While_Stmt__Group__3"
    // InternalStructuredTextParser.g:5900:1: rule__While_Stmt__Group__3 : rule__While_Stmt__Group__3__Impl rule__While_Stmt__Group__4 ;
    public final void rule__While_Stmt__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5904:1: ( rule__While_Stmt__Group__3__Impl rule__While_Stmt__Group__4 )
            // InternalStructuredTextParser.g:5905:2: rule__While_Stmt__Group__3__Impl rule__While_Stmt__Group__4
            {
            pushFollow(FOLLOW_34);
            rule__While_Stmt__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__While_Stmt__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__While_Stmt__Group__3"


    // $ANTLR start "rule__While_Stmt__Group__3__Impl"
    // InternalStructuredTextParser.g:5912:1: rule__While_Stmt__Group__3__Impl : ( ( rule__While_Stmt__StatementsAssignment_3 ) ) ;
    public final void rule__While_Stmt__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5916:1: ( ( ( rule__While_Stmt__StatementsAssignment_3 ) ) )
            // InternalStructuredTextParser.g:5917:1: ( ( rule__While_Stmt__StatementsAssignment_3 ) )
            {
            // InternalStructuredTextParser.g:5917:1: ( ( rule__While_Stmt__StatementsAssignment_3 ) )
            // InternalStructuredTextParser.g:5918:1: ( rule__While_Stmt__StatementsAssignment_3 )
            {
             before(grammarAccess.getWhile_StmtAccess().getStatementsAssignment_3()); 
            // InternalStructuredTextParser.g:5919:1: ( rule__While_Stmt__StatementsAssignment_3 )
            // InternalStructuredTextParser.g:5919:2: rule__While_Stmt__StatementsAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__While_Stmt__StatementsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getWhile_StmtAccess().getStatementsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__While_Stmt__Group__3__Impl"


    // $ANTLR start "rule__While_Stmt__Group__4"
    // InternalStructuredTextParser.g:5929:1: rule__While_Stmt__Group__4 : rule__While_Stmt__Group__4__Impl ;
    public final void rule__While_Stmt__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5933:1: ( rule__While_Stmt__Group__4__Impl )
            // InternalStructuredTextParser.g:5934:2: rule__While_Stmt__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__While_Stmt__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__While_Stmt__Group__4"


    // $ANTLR start "rule__While_Stmt__Group__4__Impl"
    // InternalStructuredTextParser.g:5940:1: rule__While_Stmt__Group__4__Impl : ( END_WHILE ) ;
    public final void rule__While_Stmt__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5944:1: ( ( END_WHILE ) )
            // InternalStructuredTextParser.g:5945:1: ( END_WHILE )
            {
            // InternalStructuredTextParser.g:5945:1: ( END_WHILE )
            // InternalStructuredTextParser.g:5946:1: END_WHILE
            {
             before(grammarAccess.getWhile_StmtAccess().getEND_WHILEKeyword_4()); 
            match(input,END_WHILE,FOLLOW_2); 
             after(grammarAccess.getWhile_StmtAccess().getEND_WHILEKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__While_Stmt__Group__4__Impl"


    // $ANTLR start "rule__Repeat_Stmt__Group__0"
    // InternalStructuredTextParser.g:5969:1: rule__Repeat_Stmt__Group__0 : rule__Repeat_Stmt__Group__0__Impl rule__Repeat_Stmt__Group__1 ;
    public final void rule__Repeat_Stmt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5973:1: ( rule__Repeat_Stmt__Group__0__Impl rule__Repeat_Stmt__Group__1 )
            // InternalStructuredTextParser.g:5974:2: rule__Repeat_Stmt__Group__0__Impl rule__Repeat_Stmt__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Repeat_Stmt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Repeat_Stmt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repeat_Stmt__Group__0"


    // $ANTLR start "rule__Repeat_Stmt__Group__0__Impl"
    // InternalStructuredTextParser.g:5981:1: rule__Repeat_Stmt__Group__0__Impl : ( REPEAT ) ;
    public final void rule__Repeat_Stmt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:5985:1: ( ( REPEAT ) )
            // InternalStructuredTextParser.g:5986:1: ( REPEAT )
            {
            // InternalStructuredTextParser.g:5986:1: ( REPEAT )
            // InternalStructuredTextParser.g:5987:1: REPEAT
            {
             before(grammarAccess.getRepeat_StmtAccess().getREPEATKeyword_0()); 
            match(input,REPEAT,FOLLOW_2); 
             after(grammarAccess.getRepeat_StmtAccess().getREPEATKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repeat_Stmt__Group__0__Impl"


    // $ANTLR start "rule__Repeat_Stmt__Group__1"
    // InternalStructuredTextParser.g:6000:1: rule__Repeat_Stmt__Group__1 : rule__Repeat_Stmt__Group__1__Impl rule__Repeat_Stmt__Group__2 ;
    public final void rule__Repeat_Stmt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6004:1: ( rule__Repeat_Stmt__Group__1__Impl rule__Repeat_Stmt__Group__2 )
            // InternalStructuredTextParser.g:6005:2: rule__Repeat_Stmt__Group__1__Impl rule__Repeat_Stmt__Group__2
            {
            pushFollow(FOLLOW_35);
            rule__Repeat_Stmt__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Repeat_Stmt__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repeat_Stmt__Group__1"


    // $ANTLR start "rule__Repeat_Stmt__Group__1__Impl"
    // InternalStructuredTextParser.g:6012:1: rule__Repeat_Stmt__Group__1__Impl : ( ( rule__Repeat_Stmt__StatementsAssignment_1 ) ) ;
    public final void rule__Repeat_Stmt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6016:1: ( ( ( rule__Repeat_Stmt__StatementsAssignment_1 ) ) )
            // InternalStructuredTextParser.g:6017:1: ( ( rule__Repeat_Stmt__StatementsAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:6017:1: ( ( rule__Repeat_Stmt__StatementsAssignment_1 ) )
            // InternalStructuredTextParser.g:6018:1: ( rule__Repeat_Stmt__StatementsAssignment_1 )
            {
             before(grammarAccess.getRepeat_StmtAccess().getStatementsAssignment_1()); 
            // InternalStructuredTextParser.g:6019:1: ( rule__Repeat_Stmt__StatementsAssignment_1 )
            // InternalStructuredTextParser.g:6019:2: rule__Repeat_Stmt__StatementsAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Repeat_Stmt__StatementsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRepeat_StmtAccess().getStatementsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repeat_Stmt__Group__1__Impl"


    // $ANTLR start "rule__Repeat_Stmt__Group__2"
    // InternalStructuredTextParser.g:6029:1: rule__Repeat_Stmt__Group__2 : rule__Repeat_Stmt__Group__2__Impl rule__Repeat_Stmt__Group__3 ;
    public final void rule__Repeat_Stmt__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6033:1: ( rule__Repeat_Stmt__Group__2__Impl rule__Repeat_Stmt__Group__3 )
            // InternalStructuredTextParser.g:6034:2: rule__Repeat_Stmt__Group__2__Impl rule__Repeat_Stmt__Group__3
            {
            pushFollow(FOLLOW_14);
            rule__Repeat_Stmt__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Repeat_Stmt__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repeat_Stmt__Group__2"


    // $ANTLR start "rule__Repeat_Stmt__Group__2__Impl"
    // InternalStructuredTextParser.g:6041:1: rule__Repeat_Stmt__Group__2__Impl : ( UNTIL ) ;
    public final void rule__Repeat_Stmt__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6045:1: ( ( UNTIL ) )
            // InternalStructuredTextParser.g:6046:1: ( UNTIL )
            {
            // InternalStructuredTextParser.g:6046:1: ( UNTIL )
            // InternalStructuredTextParser.g:6047:1: UNTIL
            {
             before(grammarAccess.getRepeat_StmtAccess().getUNTILKeyword_2()); 
            match(input,UNTIL,FOLLOW_2); 
             after(grammarAccess.getRepeat_StmtAccess().getUNTILKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repeat_Stmt__Group__2__Impl"


    // $ANTLR start "rule__Repeat_Stmt__Group__3"
    // InternalStructuredTextParser.g:6060:1: rule__Repeat_Stmt__Group__3 : rule__Repeat_Stmt__Group__3__Impl rule__Repeat_Stmt__Group__4 ;
    public final void rule__Repeat_Stmt__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6064:1: ( rule__Repeat_Stmt__Group__3__Impl rule__Repeat_Stmt__Group__4 )
            // InternalStructuredTextParser.g:6065:2: rule__Repeat_Stmt__Group__3__Impl rule__Repeat_Stmt__Group__4
            {
            pushFollow(FOLLOW_36);
            rule__Repeat_Stmt__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Repeat_Stmt__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repeat_Stmt__Group__3"


    // $ANTLR start "rule__Repeat_Stmt__Group__3__Impl"
    // InternalStructuredTextParser.g:6072:1: rule__Repeat_Stmt__Group__3__Impl : ( ( rule__Repeat_Stmt__ExpressionAssignment_3 ) ) ;
    public final void rule__Repeat_Stmt__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6076:1: ( ( ( rule__Repeat_Stmt__ExpressionAssignment_3 ) ) )
            // InternalStructuredTextParser.g:6077:1: ( ( rule__Repeat_Stmt__ExpressionAssignment_3 ) )
            {
            // InternalStructuredTextParser.g:6077:1: ( ( rule__Repeat_Stmt__ExpressionAssignment_3 ) )
            // InternalStructuredTextParser.g:6078:1: ( rule__Repeat_Stmt__ExpressionAssignment_3 )
            {
             before(grammarAccess.getRepeat_StmtAccess().getExpressionAssignment_3()); 
            // InternalStructuredTextParser.g:6079:1: ( rule__Repeat_Stmt__ExpressionAssignment_3 )
            // InternalStructuredTextParser.g:6079:2: rule__Repeat_Stmt__ExpressionAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Repeat_Stmt__ExpressionAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getRepeat_StmtAccess().getExpressionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repeat_Stmt__Group__3__Impl"


    // $ANTLR start "rule__Repeat_Stmt__Group__4"
    // InternalStructuredTextParser.g:6089:1: rule__Repeat_Stmt__Group__4 : rule__Repeat_Stmt__Group__4__Impl ;
    public final void rule__Repeat_Stmt__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6093:1: ( rule__Repeat_Stmt__Group__4__Impl )
            // InternalStructuredTextParser.g:6094:2: rule__Repeat_Stmt__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Repeat_Stmt__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repeat_Stmt__Group__4"


    // $ANTLR start "rule__Repeat_Stmt__Group__4__Impl"
    // InternalStructuredTextParser.g:6100:1: rule__Repeat_Stmt__Group__4__Impl : ( END_REPEAT ) ;
    public final void rule__Repeat_Stmt__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6104:1: ( ( END_REPEAT ) )
            // InternalStructuredTextParser.g:6105:1: ( END_REPEAT )
            {
            // InternalStructuredTextParser.g:6105:1: ( END_REPEAT )
            // InternalStructuredTextParser.g:6106:1: END_REPEAT
            {
             before(grammarAccess.getRepeat_StmtAccess().getEND_REPEATKeyword_4()); 
            match(input,END_REPEAT,FOLLOW_2); 
             after(grammarAccess.getRepeat_StmtAccess().getEND_REPEATKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repeat_Stmt__Group__4__Impl"


    // $ANTLR start "rule__Or_Expression__Group__0"
    // InternalStructuredTextParser.g:6129:1: rule__Or_Expression__Group__0 : rule__Or_Expression__Group__0__Impl rule__Or_Expression__Group__1 ;
    public final void rule__Or_Expression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6133:1: ( rule__Or_Expression__Group__0__Impl rule__Or_Expression__Group__1 )
            // InternalStructuredTextParser.g:6134:2: rule__Or_Expression__Group__0__Impl rule__Or_Expression__Group__1
            {
            pushFollow(FOLLOW_37);
            rule__Or_Expression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or_Expression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or_Expression__Group__0"


    // $ANTLR start "rule__Or_Expression__Group__0__Impl"
    // InternalStructuredTextParser.g:6141:1: rule__Or_Expression__Group__0__Impl : ( ruleXor_Expr ) ;
    public final void rule__Or_Expression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6145:1: ( ( ruleXor_Expr ) )
            // InternalStructuredTextParser.g:6146:1: ( ruleXor_Expr )
            {
            // InternalStructuredTextParser.g:6146:1: ( ruleXor_Expr )
            // InternalStructuredTextParser.g:6147:1: ruleXor_Expr
            {
             before(grammarAccess.getOr_ExpressionAccess().getXor_ExprParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleXor_Expr();

            state._fsp--;

             after(grammarAccess.getOr_ExpressionAccess().getXor_ExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or_Expression__Group__0__Impl"


    // $ANTLR start "rule__Or_Expression__Group__1"
    // InternalStructuredTextParser.g:6158:1: rule__Or_Expression__Group__1 : rule__Or_Expression__Group__1__Impl ;
    public final void rule__Or_Expression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6162:1: ( rule__Or_Expression__Group__1__Impl )
            // InternalStructuredTextParser.g:6163:2: rule__Or_Expression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or_Expression__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or_Expression__Group__1"


    // $ANTLR start "rule__Or_Expression__Group__1__Impl"
    // InternalStructuredTextParser.g:6169:1: rule__Or_Expression__Group__1__Impl : ( ( rule__Or_Expression__Group_1__0 )* ) ;
    public final void rule__Or_Expression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6173:1: ( ( ( rule__Or_Expression__Group_1__0 )* ) )
            // InternalStructuredTextParser.g:6174:1: ( ( rule__Or_Expression__Group_1__0 )* )
            {
            // InternalStructuredTextParser.g:6174:1: ( ( rule__Or_Expression__Group_1__0 )* )
            // InternalStructuredTextParser.g:6175:1: ( rule__Or_Expression__Group_1__0 )*
            {
             before(grammarAccess.getOr_ExpressionAccess().getGroup_1()); 
            // InternalStructuredTextParser.g:6176:1: ( rule__Or_Expression__Group_1__0 )*
            loop47:
            do {
                int alt47=2;
                int LA47_0 = input.LA(1);

                if ( (LA47_0==OR) ) {
                    alt47=1;
                }


                switch (alt47) {
            	case 1 :
            	    // InternalStructuredTextParser.g:6176:2: rule__Or_Expression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_38);
            	    rule__Or_Expression__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop47;
                }
            } while (true);

             after(grammarAccess.getOr_ExpressionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or_Expression__Group__1__Impl"


    // $ANTLR start "rule__Or_Expression__Group_1__0"
    // InternalStructuredTextParser.g:6190:1: rule__Or_Expression__Group_1__0 : rule__Or_Expression__Group_1__0__Impl rule__Or_Expression__Group_1__1 ;
    public final void rule__Or_Expression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6194:1: ( rule__Or_Expression__Group_1__0__Impl rule__Or_Expression__Group_1__1 )
            // InternalStructuredTextParser.g:6195:2: rule__Or_Expression__Group_1__0__Impl rule__Or_Expression__Group_1__1
            {
            pushFollow(FOLLOW_37);
            rule__Or_Expression__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or_Expression__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or_Expression__Group_1__0"


    // $ANTLR start "rule__Or_Expression__Group_1__0__Impl"
    // InternalStructuredTextParser.g:6202:1: rule__Or_Expression__Group_1__0__Impl : ( () ) ;
    public final void rule__Or_Expression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6206:1: ( ( () ) )
            // InternalStructuredTextParser.g:6207:1: ( () )
            {
            // InternalStructuredTextParser.g:6207:1: ( () )
            // InternalStructuredTextParser.g:6208:1: ()
            {
             before(grammarAccess.getOr_ExpressionAccess().getBinaryExpressionLeftAction_1_0()); 
            // InternalStructuredTextParser.g:6209:1: ()
            // InternalStructuredTextParser.g:6211:1: 
            {
            }

             after(grammarAccess.getOr_ExpressionAccess().getBinaryExpressionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or_Expression__Group_1__0__Impl"


    // $ANTLR start "rule__Or_Expression__Group_1__1"
    // InternalStructuredTextParser.g:6221:1: rule__Or_Expression__Group_1__1 : rule__Or_Expression__Group_1__1__Impl rule__Or_Expression__Group_1__2 ;
    public final void rule__Or_Expression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6225:1: ( rule__Or_Expression__Group_1__1__Impl rule__Or_Expression__Group_1__2 )
            // InternalStructuredTextParser.g:6226:2: rule__Or_Expression__Group_1__1__Impl rule__Or_Expression__Group_1__2
            {
            pushFollow(FOLLOW_14);
            rule__Or_Expression__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or_Expression__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or_Expression__Group_1__1"


    // $ANTLR start "rule__Or_Expression__Group_1__1__Impl"
    // InternalStructuredTextParser.g:6233:1: rule__Or_Expression__Group_1__1__Impl : ( ( rule__Or_Expression__OperatorAssignment_1_1 ) ) ;
    public final void rule__Or_Expression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6237:1: ( ( ( rule__Or_Expression__OperatorAssignment_1_1 ) ) )
            // InternalStructuredTextParser.g:6238:1: ( ( rule__Or_Expression__OperatorAssignment_1_1 ) )
            {
            // InternalStructuredTextParser.g:6238:1: ( ( rule__Or_Expression__OperatorAssignment_1_1 ) )
            // InternalStructuredTextParser.g:6239:1: ( rule__Or_Expression__OperatorAssignment_1_1 )
            {
             before(grammarAccess.getOr_ExpressionAccess().getOperatorAssignment_1_1()); 
            // InternalStructuredTextParser.g:6240:1: ( rule__Or_Expression__OperatorAssignment_1_1 )
            // InternalStructuredTextParser.g:6240:2: rule__Or_Expression__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Or_Expression__OperatorAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getOr_ExpressionAccess().getOperatorAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or_Expression__Group_1__1__Impl"


    // $ANTLR start "rule__Or_Expression__Group_1__2"
    // InternalStructuredTextParser.g:6250:1: rule__Or_Expression__Group_1__2 : rule__Or_Expression__Group_1__2__Impl ;
    public final void rule__Or_Expression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6254:1: ( rule__Or_Expression__Group_1__2__Impl )
            // InternalStructuredTextParser.g:6255:2: rule__Or_Expression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or_Expression__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or_Expression__Group_1__2"


    // $ANTLR start "rule__Or_Expression__Group_1__2__Impl"
    // InternalStructuredTextParser.g:6261:1: rule__Or_Expression__Group_1__2__Impl : ( ( rule__Or_Expression__RightAssignment_1_2 ) ) ;
    public final void rule__Or_Expression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6265:1: ( ( ( rule__Or_Expression__RightAssignment_1_2 ) ) )
            // InternalStructuredTextParser.g:6266:1: ( ( rule__Or_Expression__RightAssignment_1_2 ) )
            {
            // InternalStructuredTextParser.g:6266:1: ( ( rule__Or_Expression__RightAssignment_1_2 ) )
            // InternalStructuredTextParser.g:6267:1: ( rule__Or_Expression__RightAssignment_1_2 )
            {
             before(grammarAccess.getOr_ExpressionAccess().getRightAssignment_1_2()); 
            // InternalStructuredTextParser.g:6268:1: ( rule__Or_Expression__RightAssignment_1_2 )
            // InternalStructuredTextParser.g:6268:2: rule__Or_Expression__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Or_Expression__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOr_ExpressionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or_Expression__Group_1__2__Impl"


    // $ANTLR start "rule__Xor_Expr__Group__0"
    // InternalStructuredTextParser.g:6284:1: rule__Xor_Expr__Group__0 : rule__Xor_Expr__Group__0__Impl rule__Xor_Expr__Group__1 ;
    public final void rule__Xor_Expr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6288:1: ( rule__Xor_Expr__Group__0__Impl rule__Xor_Expr__Group__1 )
            // InternalStructuredTextParser.g:6289:2: rule__Xor_Expr__Group__0__Impl rule__Xor_Expr__Group__1
            {
            pushFollow(FOLLOW_39);
            rule__Xor_Expr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Xor_Expr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xor_Expr__Group__0"


    // $ANTLR start "rule__Xor_Expr__Group__0__Impl"
    // InternalStructuredTextParser.g:6296:1: rule__Xor_Expr__Group__0__Impl : ( ruleAnd_Expr ) ;
    public final void rule__Xor_Expr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6300:1: ( ( ruleAnd_Expr ) )
            // InternalStructuredTextParser.g:6301:1: ( ruleAnd_Expr )
            {
            // InternalStructuredTextParser.g:6301:1: ( ruleAnd_Expr )
            // InternalStructuredTextParser.g:6302:1: ruleAnd_Expr
            {
             before(grammarAccess.getXor_ExprAccess().getAnd_ExprParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd_Expr();

            state._fsp--;

             after(grammarAccess.getXor_ExprAccess().getAnd_ExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xor_Expr__Group__0__Impl"


    // $ANTLR start "rule__Xor_Expr__Group__1"
    // InternalStructuredTextParser.g:6313:1: rule__Xor_Expr__Group__1 : rule__Xor_Expr__Group__1__Impl ;
    public final void rule__Xor_Expr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6317:1: ( rule__Xor_Expr__Group__1__Impl )
            // InternalStructuredTextParser.g:6318:2: rule__Xor_Expr__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Xor_Expr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xor_Expr__Group__1"


    // $ANTLR start "rule__Xor_Expr__Group__1__Impl"
    // InternalStructuredTextParser.g:6324:1: rule__Xor_Expr__Group__1__Impl : ( ( rule__Xor_Expr__Group_1__0 )* ) ;
    public final void rule__Xor_Expr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6328:1: ( ( ( rule__Xor_Expr__Group_1__0 )* ) )
            // InternalStructuredTextParser.g:6329:1: ( ( rule__Xor_Expr__Group_1__0 )* )
            {
            // InternalStructuredTextParser.g:6329:1: ( ( rule__Xor_Expr__Group_1__0 )* )
            // InternalStructuredTextParser.g:6330:1: ( rule__Xor_Expr__Group_1__0 )*
            {
             before(grammarAccess.getXor_ExprAccess().getGroup_1()); 
            // InternalStructuredTextParser.g:6331:1: ( rule__Xor_Expr__Group_1__0 )*
            loop48:
            do {
                int alt48=2;
                int LA48_0 = input.LA(1);

                if ( (LA48_0==XOR) ) {
                    alt48=1;
                }


                switch (alt48) {
            	case 1 :
            	    // InternalStructuredTextParser.g:6331:2: rule__Xor_Expr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_40);
            	    rule__Xor_Expr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop48;
                }
            } while (true);

             after(grammarAccess.getXor_ExprAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xor_Expr__Group__1__Impl"


    // $ANTLR start "rule__Xor_Expr__Group_1__0"
    // InternalStructuredTextParser.g:6345:1: rule__Xor_Expr__Group_1__0 : rule__Xor_Expr__Group_1__0__Impl rule__Xor_Expr__Group_1__1 ;
    public final void rule__Xor_Expr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6349:1: ( rule__Xor_Expr__Group_1__0__Impl rule__Xor_Expr__Group_1__1 )
            // InternalStructuredTextParser.g:6350:2: rule__Xor_Expr__Group_1__0__Impl rule__Xor_Expr__Group_1__1
            {
            pushFollow(FOLLOW_39);
            rule__Xor_Expr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Xor_Expr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xor_Expr__Group_1__0"


    // $ANTLR start "rule__Xor_Expr__Group_1__0__Impl"
    // InternalStructuredTextParser.g:6357:1: rule__Xor_Expr__Group_1__0__Impl : ( () ) ;
    public final void rule__Xor_Expr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6361:1: ( ( () ) )
            // InternalStructuredTextParser.g:6362:1: ( () )
            {
            // InternalStructuredTextParser.g:6362:1: ( () )
            // InternalStructuredTextParser.g:6363:1: ()
            {
             before(grammarAccess.getXor_ExprAccess().getBinaryExpressionLeftAction_1_0()); 
            // InternalStructuredTextParser.g:6364:1: ()
            // InternalStructuredTextParser.g:6366:1: 
            {
            }

             after(grammarAccess.getXor_ExprAccess().getBinaryExpressionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xor_Expr__Group_1__0__Impl"


    // $ANTLR start "rule__Xor_Expr__Group_1__1"
    // InternalStructuredTextParser.g:6376:1: rule__Xor_Expr__Group_1__1 : rule__Xor_Expr__Group_1__1__Impl rule__Xor_Expr__Group_1__2 ;
    public final void rule__Xor_Expr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6380:1: ( rule__Xor_Expr__Group_1__1__Impl rule__Xor_Expr__Group_1__2 )
            // InternalStructuredTextParser.g:6381:2: rule__Xor_Expr__Group_1__1__Impl rule__Xor_Expr__Group_1__2
            {
            pushFollow(FOLLOW_14);
            rule__Xor_Expr__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Xor_Expr__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xor_Expr__Group_1__1"


    // $ANTLR start "rule__Xor_Expr__Group_1__1__Impl"
    // InternalStructuredTextParser.g:6388:1: rule__Xor_Expr__Group_1__1__Impl : ( ( rule__Xor_Expr__OperatorAssignment_1_1 ) ) ;
    public final void rule__Xor_Expr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6392:1: ( ( ( rule__Xor_Expr__OperatorAssignment_1_1 ) ) )
            // InternalStructuredTextParser.g:6393:1: ( ( rule__Xor_Expr__OperatorAssignment_1_1 ) )
            {
            // InternalStructuredTextParser.g:6393:1: ( ( rule__Xor_Expr__OperatorAssignment_1_1 ) )
            // InternalStructuredTextParser.g:6394:1: ( rule__Xor_Expr__OperatorAssignment_1_1 )
            {
             before(grammarAccess.getXor_ExprAccess().getOperatorAssignment_1_1()); 
            // InternalStructuredTextParser.g:6395:1: ( rule__Xor_Expr__OperatorAssignment_1_1 )
            // InternalStructuredTextParser.g:6395:2: rule__Xor_Expr__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Xor_Expr__OperatorAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getXor_ExprAccess().getOperatorAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xor_Expr__Group_1__1__Impl"


    // $ANTLR start "rule__Xor_Expr__Group_1__2"
    // InternalStructuredTextParser.g:6405:1: rule__Xor_Expr__Group_1__2 : rule__Xor_Expr__Group_1__2__Impl ;
    public final void rule__Xor_Expr__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6409:1: ( rule__Xor_Expr__Group_1__2__Impl )
            // InternalStructuredTextParser.g:6410:2: rule__Xor_Expr__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Xor_Expr__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xor_Expr__Group_1__2"


    // $ANTLR start "rule__Xor_Expr__Group_1__2__Impl"
    // InternalStructuredTextParser.g:6416:1: rule__Xor_Expr__Group_1__2__Impl : ( ( rule__Xor_Expr__RightAssignment_1_2 ) ) ;
    public final void rule__Xor_Expr__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6420:1: ( ( ( rule__Xor_Expr__RightAssignment_1_2 ) ) )
            // InternalStructuredTextParser.g:6421:1: ( ( rule__Xor_Expr__RightAssignment_1_2 ) )
            {
            // InternalStructuredTextParser.g:6421:1: ( ( rule__Xor_Expr__RightAssignment_1_2 ) )
            // InternalStructuredTextParser.g:6422:1: ( rule__Xor_Expr__RightAssignment_1_2 )
            {
             before(grammarAccess.getXor_ExprAccess().getRightAssignment_1_2()); 
            // InternalStructuredTextParser.g:6423:1: ( rule__Xor_Expr__RightAssignment_1_2 )
            // InternalStructuredTextParser.g:6423:2: rule__Xor_Expr__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Xor_Expr__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getXor_ExprAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xor_Expr__Group_1__2__Impl"


    // $ANTLR start "rule__And_Expr__Group__0"
    // InternalStructuredTextParser.g:6439:1: rule__And_Expr__Group__0 : rule__And_Expr__Group__0__Impl rule__And_Expr__Group__1 ;
    public final void rule__And_Expr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6443:1: ( rule__And_Expr__Group__0__Impl rule__And_Expr__Group__1 )
            // InternalStructuredTextParser.g:6444:2: rule__And_Expr__Group__0__Impl rule__And_Expr__Group__1
            {
            pushFollow(FOLLOW_41);
            rule__And_Expr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And_Expr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And_Expr__Group__0"


    // $ANTLR start "rule__And_Expr__Group__0__Impl"
    // InternalStructuredTextParser.g:6451:1: rule__And_Expr__Group__0__Impl : ( ruleCompare_Expr ) ;
    public final void rule__And_Expr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6455:1: ( ( ruleCompare_Expr ) )
            // InternalStructuredTextParser.g:6456:1: ( ruleCompare_Expr )
            {
            // InternalStructuredTextParser.g:6456:1: ( ruleCompare_Expr )
            // InternalStructuredTextParser.g:6457:1: ruleCompare_Expr
            {
             before(grammarAccess.getAnd_ExprAccess().getCompare_ExprParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleCompare_Expr();

            state._fsp--;

             after(grammarAccess.getAnd_ExprAccess().getCompare_ExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And_Expr__Group__0__Impl"


    // $ANTLR start "rule__And_Expr__Group__1"
    // InternalStructuredTextParser.g:6468:1: rule__And_Expr__Group__1 : rule__And_Expr__Group__1__Impl ;
    public final void rule__And_Expr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6472:1: ( rule__And_Expr__Group__1__Impl )
            // InternalStructuredTextParser.g:6473:2: rule__And_Expr__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And_Expr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And_Expr__Group__1"


    // $ANTLR start "rule__And_Expr__Group__1__Impl"
    // InternalStructuredTextParser.g:6479:1: rule__And_Expr__Group__1__Impl : ( ( rule__And_Expr__Group_1__0 )* ) ;
    public final void rule__And_Expr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6483:1: ( ( ( rule__And_Expr__Group_1__0 )* ) )
            // InternalStructuredTextParser.g:6484:1: ( ( rule__And_Expr__Group_1__0 )* )
            {
            // InternalStructuredTextParser.g:6484:1: ( ( rule__And_Expr__Group_1__0 )* )
            // InternalStructuredTextParser.g:6485:1: ( rule__And_Expr__Group_1__0 )*
            {
             before(grammarAccess.getAnd_ExprAccess().getGroup_1()); 
            // InternalStructuredTextParser.g:6486:1: ( rule__And_Expr__Group_1__0 )*
            loop49:
            do {
                int alt49=2;
                int LA49_0 = input.LA(1);

                if ( (LA49_0==AND||LA49_0==Ampersand) ) {
                    alt49=1;
                }


                switch (alt49) {
            	case 1 :
            	    // InternalStructuredTextParser.g:6486:2: rule__And_Expr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_42);
            	    rule__And_Expr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop49;
                }
            } while (true);

             after(grammarAccess.getAnd_ExprAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And_Expr__Group__1__Impl"


    // $ANTLR start "rule__And_Expr__Group_1__0"
    // InternalStructuredTextParser.g:6500:1: rule__And_Expr__Group_1__0 : rule__And_Expr__Group_1__0__Impl rule__And_Expr__Group_1__1 ;
    public final void rule__And_Expr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6504:1: ( rule__And_Expr__Group_1__0__Impl rule__And_Expr__Group_1__1 )
            // InternalStructuredTextParser.g:6505:2: rule__And_Expr__Group_1__0__Impl rule__And_Expr__Group_1__1
            {
            pushFollow(FOLLOW_41);
            rule__And_Expr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And_Expr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And_Expr__Group_1__0"


    // $ANTLR start "rule__And_Expr__Group_1__0__Impl"
    // InternalStructuredTextParser.g:6512:1: rule__And_Expr__Group_1__0__Impl : ( () ) ;
    public final void rule__And_Expr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6516:1: ( ( () ) )
            // InternalStructuredTextParser.g:6517:1: ( () )
            {
            // InternalStructuredTextParser.g:6517:1: ( () )
            // InternalStructuredTextParser.g:6518:1: ()
            {
             before(grammarAccess.getAnd_ExprAccess().getBinaryExpressionLeftAction_1_0()); 
            // InternalStructuredTextParser.g:6519:1: ()
            // InternalStructuredTextParser.g:6521:1: 
            {
            }

             after(grammarAccess.getAnd_ExprAccess().getBinaryExpressionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And_Expr__Group_1__0__Impl"


    // $ANTLR start "rule__And_Expr__Group_1__1"
    // InternalStructuredTextParser.g:6531:1: rule__And_Expr__Group_1__1 : rule__And_Expr__Group_1__1__Impl rule__And_Expr__Group_1__2 ;
    public final void rule__And_Expr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6535:1: ( rule__And_Expr__Group_1__1__Impl rule__And_Expr__Group_1__2 )
            // InternalStructuredTextParser.g:6536:2: rule__And_Expr__Group_1__1__Impl rule__And_Expr__Group_1__2
            {
            pushFollow(FOLLOW_14);
            rule__And_Expr__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And_Expr__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And_Expr__Group_1__1"


    // $ANTLR start "rule__And_Expr__Group_1__1__Impl"
    // InternalStructuredTextParser.g:6543:1: rule__And_Expr__Group_1__1__Impl : ( ( rule__And_Expr__OperatorAssignment_1_1 ) ) ;
    public final void rule__And_Expr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6547:1: ( ( ( rule__And_Expr__OperatorAssignment_1_1 ) ) )
            // InternalStructuredTextParser.g:6548:1: ( ( rule__And_Expr__OperatorAssignment_1_1 ) )
            {
            // InternalStructuredTextParser.g:6548:1: ( ( rule__And_Expr__OperatorAssignment_1_1 ) )
            // InternalStructuredTextParser.g:6549:1: ( rule__And_Expr__OperatorAssignment_1_1 )
            {
             before(grammarAccess.getAnd_ExprAccess().getOperatorAssignment_1_1()); 
            // InternalStructuredTextParser.g:6550:1: ( rule__And_Expr__OperatorAssignment_1_1 )
            // InternalStructuredTextParser.g:6550:2: rule__And_Expr__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__And_Expr__OperatorAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAnd_ExprAccess().getOperatorAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And_Expr__Group_1__1__Impl"


    // $ANTLR start "rule__And_Expr__Group_1__2"
    // InternalStructuredTextParser.g:6560:1: rule__And_Expr__Group_1__2 : rule__And_Expr__Group_1__2__Impl ;
    public final void rule__And_Expr__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6564:1: ( rule__And_Expr__Group_1__2__Impl )
            // InternalStructuredTextParser.g:6565:2: rule__And_Expr__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And_Expr__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And_Expr__Group_1__2"


    // $ANTLR start "rule__And_Expr__Group_1__2__Impl"
    // InternalStructuredTextParser.g:6571:1: rule__And_Expr__Group_1__2__Impl : ( ( rule__And_Expr__RightAssignment_1_2 ) ) ;
    public final void rule__And_Expr__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6575:1: ( ( ( rule__And_Expr__RightAssignment_1_2 ) ) )
            // InternalStructuredTextParser.g:6576:1: ( ( rule__And_Expr__RightAssignment_1_2 ) )
            {
            // InternalStructuredTextParser.g:6576:1: ( ( rule__And_Expr__RightAssignment_1_2 ) )
            // InternalStructuredTextParser.g:6577:1: ( rule__And_Expr__RightAssignment_1_2 )
            {
             before(grammarAccess.getAnd_ExprAccess().getRightAssignment_1_2()); 
            // InternalStructuredTextParser.g:6578:1: ( rule__And_Expr__RightAssignment_1_2 )
            // InternalStructuredTextParser.g:6578:2: rule__And_Expr__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__And_Expr__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAnd_ExprAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And_Expr__Group_1__2__Impl"


    // $ANTLR start "rule__Compare_Expr__Group__0"
    // InternalStructuredTextParser.g:6594:1: rule__Compare_Expr__Group__0 : rule__Compare_Expr__Group__0__Impl rule__Compare_Expr__Group__1 ;
    public final void rule__Compare_Expr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6598:1: ( rule__Compare_Expr__Group__0__Impl rule__Compare_Expr__Group__1 )
            // InternalStructuredTextParser.g:6599:2: rule__Compare_Expr__Group__0__Impl rule__Compare_Expr__Group__1
            {
            pushFollow(FOLLOW_43);
            rule__Compare_Expr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Compare_Expr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compare_Expr__Group__0"


    // $ANTLR start "rule__Compare_Expr__Group__0__Impl"
    // InternalStructuredTextParser.g:6606:1: rule__Compare_Expr__Group__0__Impl : ( ruleEqu_Expr ) ;
    public final void rule__Compare_Expr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6610:1: ( ( ruleEqu_Expr ) )
            // InternalStructuredTextParser.g:6611:1: ( ruleEqu_Expr )
            {
            // InternalStructuredTextParser.g:6611:1: ( ruleEqu_Expr )
            // InternalStructuredTextParser.g:6612:1: ruleEqu_Expr
            {
             before(grammarAccess.getCompare_ExprAccess().getEqu_ExprParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleEqu_Expr();

            state._fsp--;

             after(grammarAccess.getCompare_ExprAccess().getEqu_ExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compare_Expr__Group__0__Impl"


    // $ANTLR start "rule__Compare_Expr__Group__1"
    // InternalStructuredTextParser.g:6623:1: rule__Compare_Expr__Group__1 : rule__Compare_Expr__Group__1__Impl ;
    public final void rule__Compare_Expr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6627:1: ( rule__Compare_Expr__Group__1__Impl )
            // InternalStructuredTextParser.g:6628:2: rule__Compare_Expr__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Compare_Expr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compare_Expr__Group__1"


    // $ANTLR start "rule__Compare_Expr__Group__1__Impl"
    // InternalStructuredTextParser.g:6634:1: rule__Compare_Expr__Group__1__Impl : ( ( rule__Compare_Expr__Group_1__0 )* ) ;
    public final void rule__Compare_Expr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6638:1: ( ( ( rule__Compare_Expr__Group_1__0 )* ) )
            // InternalStructuredTextParser.g:6639:1: ( ( rule__Compare_Expr__Group_1__0 )* )
            {
            // InternalStructuredTextParser.g:6639:1: ( ( rule__Compare_Expr__Group_1__0 )* )
            // InternalStructuredTextParser.g:6640:1: ( rule__Compare_Expr__Group_1__0 )*
            {
             before(grammarAccess.getCompare_ExprAccess().getGroup_1()); 
            // InternalStructuredTextParser.g:6641:1: ( rule__Compare_Expr__Group_1__0 )*
            loop50:
            do {
                int alt50=2;
                int LA50_0 = input.LA(1);

                if ( (LA50_0==LessThanSignGreaterThanSign||LA50_0==EqualsSign) ) {
                    alt50=1;
                }


                switch (alt50) {
            	case 1 :
            	    // InternalStructuredTextParser.g:6641:2: rule__Compare_Expr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_44);
            	    rule__Compare_Expr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop50;
                }
            } while (true);

             after(grammarAccess.getCompare_ExprAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compare_Expr__Group__1__Impl"


    // $ANTLR start "rule__Compare_Expr__Group_1__0"
    // InternalStructuredTextParser.g:6655:1: rule__Compare_Expr__Group_1__0 : rule__Compare_Expr__Group_1__0__Impl rule__Compare_Expr__Group_1__1 ;
    public final void rule__Compare_Expr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6659:1: ( rule__Compare_Expr__Group_1__0__Impl rule__Compare_Expr__Group_1__1 )
            // InternalStructuredTextParser.g:6660:2: rule__Compare_Expr__Group_1__0__Impl rule__Compare_Expr__Group_1__1
            {
            pushFollow(FOLLOW_43);
            rule__Compare_Expr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Compare_Expr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compare_Expr__Group_1__0"


    // $ANTLR start "rule__Compare_Expr__Group_1__0__Impl"
    // InternalStructuredTextParser.g:6667:1: rule__Compare_Expr__Group_1__0__Impl : ( () ) ;
    public final void rule__Compare_Expr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6671:1: ( ( () ) )
            // InternalStructuredTextParser.g:6672:1: ( () )
            {
            // InternalStructuredTextParser.g:6672:1: ( () )
            // InternalStructuredTextParser.g:6673:1: ()
            {
             before(grammarAccess.getCompare_ExprAccess().getBinaryExpressionLeftAction_1_0()); 
            // InternalStructuredTextParser.g:6674:1: ()
            // InternalStructuredTextParser.g:6676:1: 
            {
            }

             after(grammarAccess.getCompare_ExprAccess().getBinaryExpressionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compare_Expr__Group_1__0__Impl"


    // $ANTLR start "rule__Compare_Expr__Group_1__1"
    // InternalStructuredTextParser.g:6686:1: rule__Compare_Expr__Group_1__1 : rule__Compare_Expr__Group_1__1__Impl rule__Compare_Expr__Group_1__2 ;
    public final void rule__Compare_Expr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6690:1: ( rule__Compare_Expr__Group_1__1__Impl rule__Compare_Expr__Group_1__2 )
            // InternalStructuredTextParser.g:6691:2: rule__Compare_Expr__Group_1__1__Impl rule__Compare_Expr__Group_1__2
            {
            pushFollow(FOLLOW_14);
            rule__Compare_Expr__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Compare_Expr__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compare_Expr__Group_1__1"


    // $ANTLR start "rule__Compare_Expr__Group_1__1__Impl"
    // InternalStructuredTextParser.g:6698:1: rule__Compare_Expr__Group_1__1__Impl : ( ( rule__Compare_Expr__OperatorAssignment_1_1 ) ) ;
    public final void rule__Compare_Expr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6702:1: ( ( ( rule__Compare_Expr__OperatorAssignment_1_1 ) ) )
            // InternalStructuredTextParser.g:6703:1: ( ( rule__Compare_Expr__OperatorAssignment_1_1 ) )
            {
            // InternalStructuredTextParser.g:6703:1: ( ( rule__Compare_Expr__OperatorAssignment_1_1 ) )
            // InternalStructuredTextParser.g:6704:1: ( rule__Compare_Expr__OperatorAssignment_1_1 )
            {
             before(grammarAccess.getCompare_ExprAccess().getOperatorAssignment_1_1()); 
            // InternalStructuredTextParser.g:6705:1: ( rule__Compare_Expr__OperatorAssignment_1_1 )
            // InternalStructuredTextParser.g:6705:2: rule__Compare_Expr__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Compare_Expr__OperatorAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getCompare_ExprAccess().getOperatorAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compare_Expr__Group_1__1__Impl"


    // $ANTLR start "rule__Compare_Expr__Group_1__2"
    // InternalStructuredTextParser.g:6715:1: rule__Compare_Expr__Group_1__2 : rule__Compare_Expr__Group_1__2__Impl ;
    public final void rule__Compare_Expr__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6719:1: ( rule__Compare_Expr__Group_1__2__Impl )
            // InternalStructuredTextParser.g:6720:2: rule__Compare_Expr__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Compare_Expr__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compare_Expr__Group_1__2"


    // $ANTLR start "rule__Compare_Expr__Group_1__2__Impl"
    // InternalStructuredTextParser.g:6726:1: rule__Compare_Expr__Group_1__2__Impl : ( ( rule__Compare_Expr__RightAssignment_1_2 ) ) ;
    public final void rule__Compare_Expr__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6730:1: ( ( ( rule__Compare_Expr__RightAssignment_1_2 ) ) )
            // InternalStructuredTextParser.g:6731:1: ( ( rule__Compare_Expr__RightAssignment_1_2 ) )
            {
            // InternalStructuredTextParser.g:6731:1: ( ( rule__Compare_Expr__RightAssignment_1_2 ) )
            // InternalStructuredTextParser.g:6732:1: ( rule__Compare_Expr__RightAssignment_1_2 )
            {
             before(grammarAccess.getCompare_ExprAccess().getRightAssignment_1_2()); 
            // InternalStructuredTextParser.g:6733:1: ( rule__Compare_Expr__RightAssignment_1_2 )
            // InternalStructuredTextParser.g:6733:2: rule__Compare_Expr__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Compare_Expr__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getCompare_ExprAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compare_Expr__Group_1__2__Impl"


    // $ANTLR start "rule__Equ_Expr__Group__0"
    // InternalStructuredTextParser.g:6749:1: rule__Equ_Expr__Group__0 : rule__Equ_Expr__Group__0__Impl rule__Equ_Expr__Group__1 ;
    public final void rule__Equ_Expr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6753:1: ( rule__Equ_Expr__Group__0__Impl rule__Equ_Expr__Group__1 )
            // InternalStructuredTextParser.g:6754:2: rule__Equ_Expr__Group__0__Impl rule__Equ_Expr__Group__1
            {
            pushFollow(FOLLOW_45);
            rule__Equ_Expr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Equ_Expr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equ_Expr__Group__0"


    // $ANTLR start "rule__Equ_Expr__Group__0__Impl"
    // InternalStructuredTextParser.g:6761:1: rule__Equ_Expr__Group__0__Impl : ( ruleAdd_Expr ) ;
    public final void rule__Equ_Expr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6765:1: ( ( ruleAdd_Expr ) )
            // InternalStructuredTextParser.g:6766:1: ( ruleAdd_Expr )
            {
            // InternalStructuredTextParser.g:6766:1: ( ruleAdd_Expr )
            // InternalStructuredTextParser.g:6767:1: ruleAdd_Expr
            {
             before(grammarAccess.getEqu_ExprAccess().getAdd_ExprParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAdd_Expr();

            state._fsp--;

             after(grammarAccess.getEqu_ExprAccess().getAdd_ExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equ_Expr__Group__0__Impl"


    // $ANTLR start "rule__Equ_Expr__Group__1"
    // InternalStructuredTextParser.g:6778:1: rule__Equ_Expr__Group__1 : rule__Equ_Expr__Group__1__Impl ;
    public final void rule__Equ_Expr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6782:1: ( rule__Equ_Expr__Group__1__Impl )
            // InternalStructuredTextParser.g:6783:2: rule__Equ_Expr__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Equ_Expr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equ_Expr__Group__1"


    // $ANTLR start "rule__Equ_Expr__Group__1__Impl"
    // InternalStructuredTextParser.g:6789:1: rule__Equ_Expr__Group__1__Impl : ( ( rule__Equ_Expr__Group_1__0 )* ) ;
    public final void rule__Equ_Expr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6793:1: ( ( ( rule__Equ_Expr__Group_1__0 )* ) )
            // InternalStructuredTextParser.g:6794:1: ( ( rule__Equ_Expr__Group_1__0 )* )
            {
            // InternalStructuredTextParser.g:6794:1: ( ( rule__Equ_Expr__Group_1__0 )* )
            // InternalStructuredTextParser.g:6795:1: ( rule__Equ_Expr__Group_1__0 )*
            {
             before(grammarAccess.getEqu_ExprAccess().getGroup_1()); 
            // InternalStructuredTextParser.g:6796:1: ( rule__Equ_Expr__Group_1__0 )*
            loop51:
            do {
                int alt51=2;
                int LA51_0 = input.LA(1);

                if ( (LA51_0==LessThanSignEqualsSign||LA51_0==GreaterThanSignEqualsSign||LA51_0==LessThanSign||LA51_0==GreaterThanSign) ) {
                    alt51=1;
                }


                switch (alt51) {
            	case 1 :
            	    // InternalStructuredTextParser.g:6796:2: rule__Equ_Expr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_46);
            	    rule__Equ_Expr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop51;
                }
            } while (true);

             after(grammarAccess.getEqu_ExprAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equ_Expr__Group__1__Impl"


    // $ANTLR start "rule__Equ_Expr__Group_1__0"
    // InternalStructuredTextParser.g:6810:1: rule__Equ_Expr__Group_1__0 : rule__Equ_Expr__Group_1__0__Impl rule__Equ_Expr__Group_1__1 ;
    public final void rule__Equ_Expr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6814:1: ( rule__Equ_Expr__Group_1__0__Impl rule__Equ_Expr__Group_1__1 )
            // InternalStructuredTextParser.g:6815:2: rule__Equ_Expr__Group_1__0__Impl rule__Equ_Expr__Group_1__1
            {
            pushFollow(FOLLOW_45);
            rule__Equ_Expr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Equ_Expr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equ_Expr__Group_1__0"


    // $ANTLR start "rule__Equ_Expr__Group_1__0__Impl"
    // InternalStructuredTextParser.g:6822:1: rule__Equ_Expr__Group_1__0__Impl : ( () ) ;
    public final void rule__Equ_Expr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6826:1: ( ( () ) )
            // InternalStructuredTextParser.g:6827:1: ( () )
            {
            // InternalStructuredTextParser.g:6827:1: ( () )
            // InternalStructuredTextParser.g:6828:1: ()
            {
             before(grammarAccess.getEqu_ExprAccess().getBinaryExpressionLeftAction_1_0()); 
            // InternalStructuredTextParser.g:6829:1: ()
            // InternalStructuredTextParser.g:6831:1: 
            {
            }

             after(grammarAccess.getEqu_ExprAccess().getBinaryExpressionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equ_Expr__Group_1__0__Impl"


    // $ANTLR start "rule__Equ_Expr__Group_1__1"
    // InternalStructuredTextParser.g:6841:1: rule__Equ_Expr__Group_1__1 : rule__Equ_Expr__Group_1__1__Impl rule__Equ_Expr__Group_1__2 ;
    public final void rule__Equ_Expr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6845:1: ( rule__Equ_Expr__Group_1__1__Impl rule__Equ_Expr__Group_1__2 )
            // InternalStructuredTextParser.g:6846:2: rule__Equ_Expr__Group_1__1__Impl rule__Equ_Expr__Group_1__2
            {
            pushFollow(FOLLOW_14);
            rule__Equ_Expr__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Equ_Expr__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equ_Expr__Group_1__1"


    // $ANTLR start "rule__Equ_Expr__Group_1__1__Impl"
    // InternalStructuredTextParser.g:6853:1: rule__Equ_Expr__Group_1__1__Impl : ( ( rule__Equ_Expr__OperatorAssignment_1_1 ) ) ;
    public final void rule__Equ_Expr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6857:1: ( ( ( rule__Equ_Expr__OperatorAssignment_1_1 ) ) )
            // InternalStructuredTextParser.g:6858:1: ( ( rule__Equ_Expr__OperatorAssignment_1_1 ) )
            {
            // InternalStructuredTextParser.g:6858:1: ( ( rule__Equ_Expr__OperatorAssignment_1_1 ) )
            // InternalStructuredTextParser.g:6859:1: ( rule__Equ_Expr__OperatorAssignment_1_1 )
            {
             before(grammarAccess.getEqu_ExprAccess().getOperatorAssignment_1_1()); 
            // InternalStructuredTextParser.g:6860:1: ( rule__Equ_Expr__OperatorAssignment_1_1 )
            // InternalStructuredTextParser.g:6860:2: rule__Equ_Expr__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Equ_Expr__OperatorAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getEqu_ExprAccess().getOperatorAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equ_Expr__Group_1__1__Impl"


    // $ANTLR start "rule__Equ_Expr__Group_1__2"
    // InternalStructuredTextParser.g:6870:1: rule__Equ_Expr__Group_1__2 : rule__Equ_Expr__Group_1__2__Impl ;
    public final void rule__Equ_Expr__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6874:1: ( rule__Equ_Expr__Group_1__2__Impl )
            // InternalStructuredTextParser.g:6875:2: rule__Equ_Expr__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Equ_Expr__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equ_Expr__Group_1__2"


    // $ANTLR start "rule__Equ_Expr__Group_1__2__Impl"
    // InternalStructuredTextParser.g:6881:1: rule__Equ_Expr__Group_1__2__Impl : ( ( rule__Equ_Expr__RightAssignment_1_2 ) ) ;
    public final void rule__Equ_Expr__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6885:1: ( ( ( rule__Equ_Expr__RightAssignment_1_2 ) ) )
            // InternalStructuredTextParser.g:6886:1: ( ( rule__Equ_Expr__RightAssignment_1_2 ) )
            {
            // InternalStructuredTextParser.g:6886:1: ( ( rule__Equ_Expr__RightAssignment_1_2 ) )
            // InternalStructuredTextParser.g:6887:1: ( rule__Equ_Expr__RightAssignment_1_2 )
            {
             before(grammarAccess.getEqu_ExprAccess().getRightAssignment_1_2()); 
            // InternalStructuredTextParser.g:6888:1: ( rule__Equ_Expr__RightAssignment_1_2 )
            // InternalStructuredTextParser.g:6888:2: rule__Equ_Expr__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Equ_Expr__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getEqu_ExprAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equ_Expr__Group_1__2__Impl"


    // $ANTLR start "rule__Add_Expr__Group__0"
    // InternalStructuredTextParser.g:6904:1: rule__Add_Expr__Group__0 : rule__Add_Expr__Group__0__Impl rule__Add_Expr__Group__1 ;
    public final void rule__Add_Expr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6908:1: ( rule__Add_Expr__Group__0__Impl rule__Add_Expr__Group__1 )
            // InternalStructuredTextParser.g:6909:2: rule__Add_Expr__Group__0__Impl rule__Add_Expr__Group__1
            {
            pushFollow(FOLLOW_47);
            rule__Add_Expr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Add_Expr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add_Expr__Group__0"


    // $ANTLR start "rule__Add_Expr__Group__0__Impl"
    // InternalStructuredTextParser.g:6916:1: rule__Add_Expr__Group__0__Impl : ( ruleTerm ) ;
    public final void rule__Add_Expr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6920:1: ( ( ruleTerm ) )
            // InternalStructuredTextParser.g:6921:1: ( ruleTerm )
            {
            // InternalStructuredTextParser.g:6921:1: ( ruleTerm )
            // InternalStructuredTextParser.g:6922:1: ruleTerm
            {
             before(grammarAccess.getAdd_ExprAccess().getTermParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleTerm();

            state._fsp--;

             after(grammarAccess.getAdd_ExprAccess().getTermParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add_Expr__Group__0__Impl"


    // $ANTLR start "rule__Add_Expr__Group__1"
    // InternalStructuredTextParser.g:6933:1: rule__Add_Expr__Group__1 : rule__Add_Expr__Group__1__Impl ;
    public final void rule__Add_Expr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6937:1: ( rule__Add_Expr__Group__1__Impl )
            // InternalStructuredTextParser.g:6938:2: rule__Add_Expr__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Add_Expr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add_Expr__Group__1"


    // $ANTLR start "rule__Add_Expr__Group__1__Impl"
    // InternalStructuredTextParser.g:6944:1: rule__Add_Expr__Group__1__Impl : ( ( rule__Add_Expr__Group_1__0 )* ) ;
    public final void rule__Add_Expr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6948:1: ( ( ( rule__Add_Expr__Group_1__0 )* ) )
            // InternalStructuredTextParser.g:6949:1: ( ( rule__Add_Expr__Group_1__0 )* )
            {
            // InternalStructuredTextParser.g:6949:1: ( ( rule__Add_Expr__Group_1__0 )* )
            // InternalStructuredTextParser.g:6950:1: ( rule__Add_Expr__Group_1__0 )*
            {
             before(grammarAccess.getAdd_ExprAccess().getGroup_1()); 
            // InternalStructuredTextParser.g:6951:1: ( rule__Add_Expr__Group_1__0 )*
            loop52:
            do {
                int alt52=2;
                int LA52_0 = input.LA(1);

                if ( (LA52_0==PlusSign||LA52_0==HyphenMinus) ) {
                    alt52=1;
                }


                switch (alt52) {
            	case 1 :
            	    // InternalStructuredTextParser.g:6951:2: rule__Add_Expr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_48);
            	    rule__Add_Expr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop52;
                }
            } while (true);

             after(grammarAccess.getAdd_ExprAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add_Expr__Group__1__Impl"


    // $ANTLR start "rule__Add_Expr__Group_1__0"
    // InternalStructuredTextParser.g:6965:1: rule__Add_Expr__Group_1__0 : rule__Add_Expr__Group_1__0__Impl rule__Add_Expr__Group_1__1 ;
    public final void rule__Add_Expr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6969:1: ( rule__Add_Expr__Group_1__0__Impl rule__Add_Expr__Group_1__1 )
            // InternalStructuredTextParser.g:6970:2: rule__Add_Expr__Group_1__0__Impl rule__Add_Expr__Group_1__1
            {
            pushFollow(FOLLOW_47);
            rule__Add_Expr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Add_Expr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add_Expr__Group_1__0"


    // $ANTLR start "rule__Add_Expr__Group_1__0__Impl"
    // InternalStructuredTextParser.g:6977:1: rule__Add_Expr__Group_1__0__Impl : ( () ) ;
    public final void rule__Add_Expr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:6981:1: ( ( () ) )
            // InternalStructuredTextParser.g:6982:1: ( () )
            {
            // InternalStructuredTextParser.g:6982:1: ( () )
            // InternalStructuredTextParser.g:6983:1: ()
            {
             before(grammarAccess.getAdd_ExprAccess().getBinaryExpressionLeftAction_1_0()); 
            // InternalStructuredTextParser.g:6984:1: ()
            // InternalStructuredTextParser.g:6986:1: 
            {
            }

             after(grammarAccess.getAdd_ExprAccess().getBinaryExpressionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add_Expr__Group_1__0__Impl"


    // $ANTLR start "rule__Add_Expr__Group_1__1"
    // InternalStructuredTextParser.g:6996:1: rule__Add_Expr__Group_1__1 : rule__Add_Expr__Group_1__1__Impl rule__Add_Expr__Group_1__2 ;
    public final void rule__Add_Expr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7000:1: ( rule__Add_Expr__Group_1__1__Impl rule__Add_Expr__Group_1__2 )
            // InternalStructuredTextParser.g:7001:2: rule__Add_Expr__Group_1__1__Impl rule__Add_Expr__Group_1__2
            {
            pushFollow(FOLLOW_14);
            rule__Add_Expr__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Add_Expr__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add_Expr__Group_1__1"


    // $ANTLR start "rule__Add_Expr__Group_1__1__Impl"
    // InternalStructuredTextParser.g:7008:1: rule__Add_Expr__Group_1__1__Impl : ( ( rule__Add_Expr__OperatorAssignment_1_1 ) ) ;
    public final void rule__Add_Expr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7012:1: ( ( ( rule__Add_Expr__OperatorAssignment_1_1 ) ) )
            // InternalStructuredTextParser.g:7013:1: ( ( rule__Add_Expr__OperatorAssignment_1_1 ) )
            {
            // InternalStructuredTextParser.g:7013:1: ( ( rule__Add_Expr__OperatorAssignment_1_1 ) )
            // InternalStructuredTextParser.g:7014:1: ( rule__Add_Expr__OperatorAssignment_1_1 )
            {
             before(grammarAccess.getAdd_ExprAccess().getOperatorAssignment_1_1()); 
            // InternalStructuredTextParser.g:7015:1: ( rule__Add_Expr__OperatorAssignment_1_1 )
            // InternalStructuredTextParser.g:7015:2: rule__Add_Expr__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Add_Expr__OperatorAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAdd_ExprAccess().getOperatorAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add_Expr__Group_1__1__Impl"


    // $ANTLR start "rule__Add_Expr__Group_1__2"
    // InternalStructuredTextParser.g:7025:1: rule__Add_Expr__Group_1__2 : rule__Add_Expr__Group_1__2__Impl ;
    public final void rule__Add_Expr__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7029:1: ( rule__Add_Expr__Group_1__2__Impl )
            // InternalStructuredTextParser.g:7030:2: rule__Add_Expr__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Add_Expr__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add_Expr__Group_1__2"


    // $ANTLR start "rule__Add_Expr__Group_1__2__Impl"
    // InternalStructuredTextParser.g:7036:1: rule__Add_Expr__Group_1__2__Impl : ( ( rule__Add_Expr__RightAssignment_1_2 ) ) ;
    public final void rule__Add_Expr__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7040:1: ( ( ( rule__Add_Expr__RightAssignment_1_2 ) ) )
            // InternalStructuredTextParser.g:7041:1: ( ( rule__Add_Expr__RightAssignment_1_2 ) )
            {
            // InternalStructuredTextParser.g:7041:1: ( ( rule__Add_Expr__RightAssignment_1_2 ) )
            // InternalStructuredTextParser.g:7042:1: ( rule__Add_Expr__RightAssignment_1_2 )
            {
             before(grammarAccess.getAdd_ExprAccess().getRightAssignment_1_2()); 
            // InternalStructuredTextParser.g:7043:1: ( rule__Add_Expr__RightAssignment_1_2 )
            // InternalStructuredTextParser.g:7043:2: rule__Add_Expr__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Add_Expr__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAdd_ExprAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add_Expr__Group_1__2__Impl"


    // $ANTLR start "rule__Term__Group__0"
    // InternalStructuredTextParser.g:7059:1: rule__Term__Group__0 : rule__Term__Group__0__Impl rule__Term__Group__1 ;
    public final void rule__Term__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7063:1: ( rule__Term__Group__0__Impl rule__Term__Group__1 )
            // InternalStructuredTextParser.g:7064:2: rule__Term__Group__0__Impl rule__Term__Group__1
            {
            pushFollow(FOLLOW_49);
            rule__Term__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Term__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group__0"


    // $ANTLR start "rule__Term__Group__0__Impl"
    // InternalStructuredTextParser.g:7071:1: rule__Term__Group__0__Impl : ( rulePower_Expr ) ;
    public final void rule__Term__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7075:1: ( ( rulePower_Expr ) )
            // InternalStructuredTextParser.g:7076:1: ( rulePower_Expr )
            {
            // InternalStructuredTextParser.g:7076:1: ( rulePower_Expr )
            // InternalStructuredTextParser.g:7077:1: rulePower_Expr
            {
             before(grammarAccess.getTermAccess().getPower_ExprParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            rulePower_Expr();

            state._fsp--;

             after(grammarAccess.getTermAccess().getPower_ExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group__0__Impl"


    // $ANTLR start "rule__Term__Group__1"
    // InternalStructuredTextParser.g:7088:1: rule__Term__Group__1 : rule__Term__Group__1__Impl ;
    public final void rule__Term__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7092:1: ( rule__Term__Group__1__Impl )
            // InternalStructuredTextParser.g:7093:2: rule__Term__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Term__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group__1"


    // $ANTLR start "rule__Term__Group__1__Impl"
    // InternalStructuredTextParser.g:7099:1: rule__Term__Group__1__Impl : ( ( rule__Term__Group_1__0 )* ) ;
    public final void rule__Term__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7103:1: ( ( ( rule__Term__Group_1__0 )* ) )
            // InternalStructuredTextParser.g:7104:1: ( ( rule__Term__Group_1__0 )* )
            {
            // InternalStructuredTextParser.g:7104:1: ( ( rule__Term__Group_1__0 )* )
            // InternalStructuredTextParser.g:7105:1: ( rule__Term__Group_1__0 )*
            {
             before(grammarAccess.getTermAccess().getGroup_1()); 
            // InternalStructuredTextParser.g:7106:1: ( rule__Term__Group_1__0 )*
            loop53:
            do {
                int alt53=2;
                int LA53_0 = input.LA(1);

                if ( (LA53_0==MOD||LA53_0==Asterisk||LA53_0==Solidus) ) {
                    alt53=1;
                }


                switch (alt53) {
            	case 1 :
            	    // InternalStructuredTextParser.g:7106:2: rule__Term__Group_1__0
            	    {
            	    pushFollow(FOLLOW_50);
            	    rule__Term__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop53;
                }
            } while (true);

             after(grammarAccess.getTermAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group__1__Impl"


    // $ANTLR start "rule__Term__Group_1__0"
    // InternalStructuredTextParser.g:7120:1: rule__Term__Group_1__0 : rule__Term__Group_1__0__Impl rule__Term__Group_1__1 ;
    public final void rule__Term__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7124:1: ( rule__Term__Group_1__0__Impl rule__Term__Group_1__1 )
            // InternalStructuredTextParser.g:7125:2: rule__Term__Group_1__0__Impl rule__Term__Group_1__1
            {
            pushFollow(FOLLOW_49);
            rule__Term__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Term__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_1__0"


    // $ANTLR start "rule__Term__Group_1__0__Impl"
    // InternalStructuredTextParser.g:7132:1: rule__Term__Group_1__0__Impl : ( () ) ;
    public final void rule__Term__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7136:1: ( ( () ) )
            // InternalStructuredTextParser.g:7137:1: ( () )
            {
            // InternalStructuredTextParser.g:7137:1: ( () )
            // InternalStructuredTextParser.g:7138:1: ()
            {
             before(grammarAccess.getTermAccess().getBinaryExpressionLeftAction_1_0()); 
            // InternalStructuredTextParser.g:7139:1: ()
            // InternalStructuredTextParser.g:7141:1: 
            {
            }

             after(grammarAccess.getTermAccess().getBinaryExpressionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_1__0__Impl"


    // $ANTLR start "rule__Term__Group_1__1"
    // InternalStructuredTextParser.g:7151:1: rule__Term__Group_1__1 : rule__Term__Group_1__1__Impl rule__Term__Group_1__2 ;
    public final void rule__Term__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7155:1: ( rule__Term__Group_1__1__Impl rule__Term__Group_1__2 )
            // InternalStructuredTextParser.g:7156:2: rule__Term__Group_1__1__Impl rule__Term__Group_1__2
            {
            pushFollow(FOLLOW_14);
            rule__Term__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Term__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_1__1"


    // $ANTLR start "rule__Term__Group_1__1__Impl"
    // InternalStructuredTextParser.g:7163:1: rule__Term__Group_1__1__Impl : ( ( rule__Term__OperatorAssignment_1_1 ) ) ;
    public final void rule__Term__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7167:1: ( ( ( rule__Term__OperatorAssignment_1_1 ) ) )
            // InternalStructuredTextParser.g:7168:1: ( ( rule__Term__OperatorAssignment_1_1 ) )
            {
            // InternalStructuredTextParser.g:7168:1: ( ( rule__Term__OperatorAssignment_1_1 ) )
            // InternalStructuredTextParser.g:7169:1: ( rule__Term__OperatorAssignment_1_1 )
            {
             before(grammarAccess.getTermAccess().getOperatorAssignment_1_1()); 
            // InternalStructuredTextParser.g:7170:1: ( rule__Term__OperatorAssignment_1_1 )
            // InternalStructuredTextParser.g:7170:2: rule__Term__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Term__OperatorAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getTermAccess().getOperatorAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_1__1__Impl"


    // $ANTLR start "rule__Term__Group_1__2"
    // InternalStructuredTextParser.g:7180:1: rule__Term__Group_1__2 : rule__Term__Group_1__2__Impl ;
    public final void rule__Term__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7184:1: ( rule__Term__Group_1__2__Impl )
            // InternalStructuredTextParser.g:7185:2: rule__Term__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Term__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_1__2"


    // $ANTLR start "rule__Term__Group_1__2__Impl"
    // InternalStructuredTextParser.g:7191:1: rule__Term__Group_1__2__Impl : ( ( rule__Term__RightAssignment_1_2 ) ) ;
    public final void rule__Term__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7195:1: ( ( ( rule__Term__RightAssignment_1_2 ) ) )
            // InternalStructuredTextParser.g:7196:1: ( ( rule__Term__RightAssignment_1_2 ) )
            {
            // InternalStructuredTextParser.g:7196:1: ( ( rule__Term__RightAssignment_1_2 ) )
            // InternalStructuredTextParser.g:7197:1: ( rule__Term__RightAssignment_1_2 )
            {
             before(grammarAccess.getTermAccess().getRightAssignment_1_2()); 
            // InternalStructuredTextParser.g:7198:1: ( rule__Term__RightAssignment_1_2 )
            // InternalStructuredTextParser.g:7198:2: rule__Term__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Term__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getTermAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_1__2__Impl"


    // $ANTLR start "rule__Power_Expr__Group__0"
    // InternalStructuredTextParser.g:7214:1: rule__Power_Expr__Group__0 : rule__Power_Expr__Group__0__Impl rule__Power_Expr__Group__1 ;
    public final void rule__Power_Expr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7218:1: ( rule__Power_Expr__Group__0__Impl rule__Power_Expr__Group__1 )
            // InternalStructuredTextParser.g:7219:2: rule__Power_Expr__Group__0__Impl rule__Power_Expr__Group__1
            {
            pushFollow(FOLLOW_51);
            rule__Power_Expr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Power_Expr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Power_Expr__Group__0"


    // $ANTLR start "rule__Power_Expr__Group__0__Impl"
    // InternalStructuredTextParser.g:7226:1: rule__Power_Expr__Group__0__Impl : ( ruleUnary_Expr ) ;
    public final void rule__Power_Expr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7230:1: ( ( ruleUnary_Expr ) )
            // InternalStructuredTextParser.g:7231:1: ( ruleUnary_Expr )
            {
            // InternalStructuredTextParser.g:7231:1: ( ruleUnary_Expr )
            // InternalStructuredTextParser.g:7232:1: ruleUnary_Expr
            {
             before(grammarAccess.getPower_ExprAccess().getUnary_ExprParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleUnary_Expr();

            state._fsp--;

             after(grammarAccess.getPower_ExprAccess().getUnary_ExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Power_Expr__Group__0__Impl"


    // $ANTLR start "rule__Power_Expr__Group__1"
    // InternalStructuredTextParser.g:7243:1: rule__Power_Expr__Group__1 : rule__Power_Expr__Group__1__Impl ;
    public final void rule__Power_Expr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7247:1: ( rule__Power_Expr__Group__1__Impl )
            // InternalStructuredTextParser.g:7248:2: rule__Power_Expr__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Power_Expr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Power_Expr__Group__1"


    // $ANTLR start "rule__Power_Expr__Group__1__Impl"
    // InternalStructuredTextParser.g:7254:1: rule__Power_Expr__Group__1__Impl : ( ( rule__Power_Expr__Group_1__0 )* ) ;
    public final void rule__Power_Expr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7258:1: ( ( ( rule__Power_Expr__Group_1__0 )* ) )
            // InternalStructuredTextParser.g:7259:1: ( ( rule__Power_Expr__Group_1__0 )* )
            {
            // InternalStructuredTextParser.g:7259:1: ( ( rule__Power_Expr__Group_1__0 )* )
            // InternalStructuredTextParser.g:7260:1: ( rule__Power_Expr__Group_1__0 )*
            {
             before(grammarAccess.getPower_ExprAccess().getGroup_1()); 
            // InternalStructuredTextParser.g:7261:1: ( rule__Power_Expr__Group_1__0 )*
            loop54:
            do {
                int alt54=2;
                int LA54_0 = input.LA(1);

                if ( (LA54_0==AsteriskAsterisk) ) {
                    alt54=1;
                }


                switch (alt54) {
            	case 1 :
            	    // InternalStructuredTextParser.g:7261:2: rule__Power_Expr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_52);
            	    rule__Power_Expr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop54;
                }
            } while (true);

             after(grammarAccess.getPower_ExprAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Power_Expr__Group__1__Impl"


    // $ANTLR start "rule__Power_Expr__Group_1__0"
    // InternalStructuredTextParser.g:7275:1: rule__Power_Expr__Group_1__0 : rule__Power_Expr__Group_1__0__Impl rule__Power_Expr__Group_1__1 ;
    public final void rule__Power_Expr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7279:1: ( rule__Power_Expr__Group_1__0__Impl rule__Power_Expr__Group_1__1 )
            // InternalStructuredTextParser.g:7280:2: rule__Power_Expr__Group_1__0__Impl rule__Power_Expr__Group_1__1
            {
            pushFollow(FOLLOW_51);
            rule__Power_Expr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Power_Expr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Power_Expr__Group_1__0"


    // $ANTLR start "rule__Power_Expr__Group_1__0__Impl"
    // InternalStructuredTextParser.g:7287:1: rule__Power_Expr__Group_1__0__Impl : ( () ) ;
    public final void rule__Power_Expr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7291:1: ( ( () ) )
            // InternalStructuredTextParser.g:7292:1: ( () )
            {
            // InternalStructuredTextParser.g:7292:1: ( () )
            // InternalStructuredTextParser.g:7293:1: ()
            {
             before(grammarAccess.getPower_ExprAccess().getBinaryExpressionLeftAction_1_0()); 
            // InternalStructuredTextParser.g:7294:1: ()
            // InternalStructuredTextParser.g:7296:1: 
            {
            }

             after(grammarAccess.getPower_ExprAccess().getBinaryExpressionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Power_Expr__Group_1__0__Impl"


    // $ANTLR start "rule__Power_Expr__Group_1__1"
    // InternalStructuredTextParser.g:7306:1: rule__Power_Expr__Group_1__1 : rule__Power_Expr__Group_1__1__Impl rule__Power_Expr__Group_1__2 ;
    public final void rule__Power_Expr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7310:1: ( rule__Power_Expr__Group_1__1__Impl rule__Power_Expr__Group_1__2 )
            // InternalStructuredTextParser.g:7311:2: rule__Power_Expr__Group_1__1__Impl rule__Power_Expr__Group_1__2
            {
            pushFollow(FOLLOW_14);
            rule__Power_Expr__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Power_Expr__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Power_Expr__Group_1__1"


    // $ANTLR start "rule__Power_Expr__Group_1__1__Impl"
    // InternalStructuredTextParser.g:7318:1: rule__Power_Expr__Group_1__1__Impl : ( ( rule__Power_Expr__OperatorAssignment_1_1 ) ) ;
    public final void rule__Power_Expr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7322:1: ( ( ( rule__Power_Expr__OperatorAssignment_1_1 ) ) )
            // InternalStructuredTextParser.g:7323:1: ( ( rule__Power_Expr__OperatorAssignment_1_1 ) )
            {
            // InternalStructuredTextParser.g:7323:1: ( ( rule__Power_Expr__OperatorAssignment_1_1 ) )
            // InternalStructuredTextParser.g:7324:1: ( rule__Power_Expr__OperatorAssignment_1_1 )
            {
             before(grammarAccess.getPower_ExprAccess().getOperatorAssignment_1_1()); 
            // InternalStructuredTextParser.g:7325:1: ( rule__Power_Expr__OperatorAssignment_1_1 )
            // InternalStructuredTextParser.g:7325:2: rule__Power_Expr__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Power_Expr__OperatorAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getPower_ExprAccess().getOperatorAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Power_Expr__Group_1__1__Impl"


    // $ANTLR start "rule__Power_Expr__Group_1__2"
    // InternalStructuredTextParser.g:7335:1: rule__Power_Expr__Group_1__2 : rule__Power_Expr__Group_1__2__Impl ;
    public final void rule__Power_Expr__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7339:1: ( rule__Power_Expr__Group_1__2__Impl )
            // InternalStructuredTextParser.g:7340:2: rule__Power_Expr__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Power_Expr__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Power_Expr__Group_1__2"


    // $ANTLR start "rule__Power_Expr__Group_1__2__Impl"
    // InternalStructuredTextParser.g:7346:1: rule__Power_Expr__Group_1__2__Impl : ( ( rule__Power_Expr__RightAssignment_1_2 ) ) ;
    public final void rule__Power_Expr__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7350:1: ( ( ( rule__Power_Expr__RightAssignment_1_2 ) ) )
            // InternalStructuredTextParser.g:7351:1: ( ( rule__Power_Expr__RightAssignment_1_2 ) )
            {
            // InternalStructuredTextParser.g:7351:1: ( ( rule__Power_Expr__RightAssignment_1_2 ) )
            // InternalStructuredTextParser.g:7352:1: ( rule__Power_Expr__RightAssignment_1_2 )
            {
             before(grammarAccess.getPower_ExprAccess().getRightAssignment_1_2()); 
            // InternalStructuredTextParser.g:7353:1: ( rule__Power_Expr__RightAssignment_1_2 )
            // InternalStructuredTextParser.g:7353:2: rule__Power_Expr__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Power_Expr__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getPower_ExprAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Power_Expr__Group_1__2__Impl"


    // $ANTLR start "rule__Unary_Expr__Group_0__0"
    // InternalStructuredTextParser.g:7369:1: rule__Unary_Expr__Group_0__0 : rule__Unary_Expr__Group_0__0__Impl rule__Unary_Expr__Group_0__1 ;
    public final void rule__Unary_Expr__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7373:1: ( rule__Unary_Expr__Group_0__0__Impl rule__Unary_Expr__Group_0__1 )
            // InternalStructuredTextParser.g:7374:2: rule__Unary_Expr__Group_0__0__Impl rule__Unary_Expr__Group_0__1
            {
            pushFollow(FOLLOW_53);
            rule__Unary_Expr__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Unary_Expr__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_Expr__Group_0__0"


    // $ANTLR start "rule__Unary_Expr__Group_0__0__Impl"
    // InternalStructuredTextParser.g:7381:1: rule__Unary_Expr__Group_0__0__Impl : ( () ) ;
    public final void rule__Unary_Expr__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7385:1: ( ( () ) )
            // InternalStructuredTextParser.g:7386:1: ( () )
            {
            // InternalStructuredTextParser.g:7386:1: ( () )
            // InternalStructuredTextParser.g:7387:1: ()
            {
             before(grammarAccess.getUnary_ExprAccess().getUnaryExpressionAction_0_0()); 
            // InternalStructuredTextParser.g:7388:1: ()
            // InternalStructuredTextParser.g:7390:1: 
            {
            }

             after(grammarAccess.getUnary_ExprAccess().getUnaryExpressionAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_Expr__Group_0__0__Impl"


    // $ANTLR start "rule__Unary_Expr__Group_0__1"
    // InternalStructuredTextParser.g:7400:1: rule__Unary_Expr__Group_0__1 : rule__Unary_Expr__Group_0__1__Impl rule__Unary_Expr__Group_0__2 ;
    public final void rule__Unary_Expr__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7404:1: ( rule__Unary_Expr__Group_0__1__Impl rule__Unary_Expr__Group_0__2 )
            // InternalStructuredTextParser.g:7405:2: rule__Unary_Expr__Group_0__1__Impl rule__Unary_Expr__Group_0__2
            {
            pushFollow(FOLLOW_54);
            rule__Unary_Expr__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Unary_Expr__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_Expr__Group_0__1"


    // $ANTLR start "rule__Unary_Expr__Group_0__1__Impl"
    // InternalStructuredTextParser.g:7412:1: rule__Unary_Expr__Group_0__1__Impl : ( ( rule__Unary_Expr__OperatorAssignment_0_1 ) ) ;
    public final void rule__Unary_Expr__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7416:1: ( ( ( rule__Unary_Expr__OperatorAssignment_0_1 ) ) )
            // InternalStructuredTextParser.g:7417:1: ( ( rule__Unary_Expr__OperatorAssignment_0_1 ) )
            {
            // InternalStructuredTextParser.g:7417:1: ( ( rule__Unary_Expr__OperatorAssignment_0_1 ) )
            // InternalStructuredTextParser.g:7418:1: ( rule__Unary_Expr__OperatorAssignment_0_1 )
            {
             before(grammarAccess.getUnary_ExprAccess().getOperatorAssignment_0_1()); 
            // InternalStructuredTextParser.g:7419:1: ( rule__Unary_Expr__OperatorAssignment_0_1 )
            // InternalStructuredTextParser.g:7419:2: rule__Unary_Expr__OperatorAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Unary_Expr__OperatorAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getUnary_ExprAccess().getOperatorAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_Expr__Group_0__1__Impl"


    // $ANTLR start "rule__Unary_Expr__Group_0__2"
    // InternalStructuredTextParser.g:7429:1: rule__Unary_Expr__Group_0__2 : rule__Unary_Expr__Group_0__2__Impl ;
    public final void rule__Unary_Expr__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7433:1: ( rule__Unary_Expr__Group_0__2__Impl )
            // InternalStructuredTextParser.g:7434:2: rule__Unary_Expr__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Unary_Expr__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_Expr__Group_0__2"


    // $ANTLR start "rule__Unary_Expr__Group_0__2__Impl"
    // InternalStructuredTextParser.g:7440:1: rule__Unary_Expr__Group_0__2__Impl : ( ( rule__Unary_Expr__ExpressionAssignment_0_2 ) ) ;
    public final void rule__Unary_Expr__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7444:1: ( ( ( rule__Unary_Expr__ExpressionAssignment_0_2 ) ) )
            // InternalStructuredTextParser.g:7445:1: ( ( rule__Unary_Expr__ExpressionAssignment_0_2 ) )
            {
            // InternalStructuredTextParser.g:7445:1: ( ( rule__Unary_Expr__ExpressionAssignment_0_2 ) )
            // InternalStructuredTextParser.g:7446:1: ( rule__Unary_Expr__ExpressionAssignment_0_2 )
            {
             before(grammarAccess.getUnary_ExprAccess().getExpressionAssignment_0_2()); 
            // InternalStructuredTextParser.g:7447:1: ( rule__Unary_Expr__ExpressionAssignment_0_2 )
            // InternalStructuredTextParser.g:7447:2: rule__Unary_Expr__ExpressionAssignment_0_2
            {
            pushFollow(FOLLOW_2);
            rule__Unary_Expr__ExpressionAssignment_0_2();

            state._fsp--;


            }

             after(grammarAccess.getUnary_ExprAccess().getExpressionAssignment_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_Expr__Group_0__2__Impl"


    // $ANTLR start "rule__Primary_Expr__Group_2__0"
    // InternalStructuredTextParser.g:7463:1: rule__Primary_Expr__Group_2__0 : rule__Primary_Expr__Group_2__0__Impl rule__Primary_Expr__Group_2__1 ;
    public final void rule__Primary_Expr__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7467:1: ( rule__Primary_Expr__Group_2__0__Impl rule__Primary_Expr__Group_2__1 )
            // InternalStructuredTextParser.g:7468:2: rule__Primary_Expr__Group_2__0__Impl rule__Primary_Expr__Group_2__1
            {
            pushFollow(FOLLOW_14);
            rule__Primary_Expr__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary_Expr__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary_Expr__Group_2__0"


    // $ANTLR start "rule__Primary_Expr__Group_2__0__Impl"
    // InternalStructuredTextParser.g:7475:1: rule__Primary_Expr__Group_2__0__Impl : ( LeftParenthesis ) ;
    public final void rule__Primary_Expr__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7479:1: ( ( LeftParenthesis ) )
            // InternalStructuredTextParser.g:7480:1: ( LeftParenthesis )
            {
            // InternalStructuredTextParser.g:7480:1: ( LeftParenthesis )
            // InternalStructuredTextParser.g:7481:1: LeftParenthesis
            {
             before(grammarAccess.getPrimary_ExprAccess().getLeftParenthesisKeyword_2_0()); 
            match(input,LeftParenthesis,FOLLOW_2); 
             after(grammarAccess.getPrimary_ExprAccess().getLeftParenthesisKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary_Expr__Group_2__0__Impl"


    // $ANTLR start "rule__Primary_Expr__Group_2__1"
    // InternalStructuredTextParser.g:7494:1: rule__Primary_Expr__Group_2__1 : rule__Primary_Expr__Group_2__1__Impl rule__Primary_Expr__Group_2__2 ;
    public final void rule__Primary_Expr__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7498:1: ( rule__Primary_Expr__Group_2__1__Impl rule__Primary_Expr__Group_2__2 )
            // InternalStructuredTextParser.g:7499:2: rule__Primary_Expr__Group_2__1__Impl rule__Primary_Expr__Group_2__2
            {
            pushFollow(FOLLOW_17);
            rule__Primary_Expr__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary_Expr__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary_Expr__Group_2__1"


    // $ANTLR start "rule__Primary_Expr__Group_2__1__Impl"
    // InternalStructuredTextParser.g:7506:1: rule__Primary_Expr__Group_2__1__Impl : ( ruleExpression ) ;
    public final void rule__Primary_Expr__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7510:1: ( ( ruleExpression ) )
            // InternalStructuredTextParser.g:7511:1: ( ruleExpression )
            {
            // InternalStructuredTextParser.g:7511:1: ( ruleExpression )
            // InternalStructuredTextParser.g:7512:1: ruleExpression
            {
             before(grammarAccess.getPrimary_ExprAccess().getExpressionParserRuleCall_2_1()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getPrimary_ExprAccess().getExpressionParserRuleCall_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary_Expr__Group_2__1__Impl"


    // $ANTLR start "rule__Primary_Expr__Group_2__2"
    // InternalStructuredTextParser.g:7523:1: rule__Primary_Expr__Group_2__2 : rule__Primary_Expr__Group_2__2__Impl ;
    public final void rule__Primary_Expr__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7527:1: ( rule__Primary_Expr__Group_2__2__Impl )
            // InternalStructuredTextParser.g:7528:2: rule__Primary_Expr__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary_Expr__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary_Expr__Group_2__2"


    // $ANTLR start "rule__Primary_Expr__Group_2__2__Impl"
    // InternalStructuredTextParser.g:7534:1: rule__Primary_Expr__Group_2__2__Impl : ( RightParenthesis ) ;
    public final void rule__Primary_Expr__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7538:1: ( ( RightParenthesis ) )
            // InternalStructuredTextParser.g:7539:1: ( RightParenthesis )
            {
            // InternalStructuredTextParser.g:7539:1: ( RightParenthesis )
            // InternalStructuredTextParser.g:7540:1: RightParenthesis
            {
             before(grammarAccess.getPrimary_ExprAccess().getRightParenthesisKeyword_2_2()); 
            match(input,RightParenthesis,FOLLOW_2); 
             after(grammarAccess.getPrimary_ExprAccess().getRightParenthesisKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary_Expr__Group_2__2__Impl"


    // $ANTLR start "rule__Func_Call__Group__0"
    // InternalStructuredTextParser.g:7559:1: rule__Func_Call__Group__0 : rule__Func_Call__Group__0__Impl rule__Func_Call__Group__1 ;
    public final void rule__Func_Call__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7563:1: ( rule__Func_Call__Group__0__Impl rule__Func_Call__Group__1 )
            // InternalStructuredTextParser.g:7564:2: rule__Func_Call__Group__0__Impl rule__Func_Call__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__Func_Call__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Func_Call__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group__0"


    // $ANTLR start "rule__Func_Call__Group__0__Impl"
    // InternalStructuredTextParser.g:7571:1: rule__Func_Call__Group__0__Impl : ( ( rule__Func_Call__FuncAssignment_0 ) ) ;
    public final void rule__Func_Call__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7575:1: ( ( ( rule__Func_Call__FuncAssignment_0 ) ) )
            // InternalStructuredTextParser.g:7576:1: ( ( rule__Func_Call__FuncAssignment_0 ) )
            {
            // InternalStructuredTextParser.g:7576:1: ( ( rule__Func_Call__FuncAssignment_0 ) )
            // InternalStructuredTextParser.g:7577:1: ( rule__Func_Call__FuncAssignment_0 )
            {
             before(grammarAccess.getFunc_CallAccess().getFuncAssignment_0()); 
            // InternalStructuredTextParser.g:7578:1: ( rule__Func_Call__FuncAssignment_0 )
            // InternalStructuredTextParser.g:7578:2: rule__Func_Call__FuncAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Func_Call__FuncAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getFunc_CallAccess().getFuncAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group__0__Impl"


    // $ANTLR start "rule__Func_Call__Group__1"
    // InternalStructuredTextParser.g:7588:1: rule__Func_Call__Group__1 : rule__Func_Call__Group__1__Impl rule__Func_Call__Group__2 ;
    public final void rule__Func_Call__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7592:1: ( rule__Func_Call__Group__1__Impl rule__Func_Call__Group__2 )
            // InternalStructuredTextParser.g:7593:2: rule__Func_Call__Group__1__Impl rule__Func_Call__Group__2
            {
            pushFollow(FOLLOW_55);
            rule__Func_Call__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Func_Call__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group__1"


    // $ANTLR start "rule__Func_Call__Group__1__Impl"
    // InternalStructuredTextParser.g:7600:1: rule__Func_Call__Group__1__Impl : ( LeftParenthesis ) ;
    public final void rule__Func_Call__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7604:1: ( ( LeftParenthesis ) )
            // InternalStructuredTextParser.g:7605:1: ( LeftParenthesis )
            {
            // InternalStructuredTextParser.g:7605:1: ( LeftParenthesis )
            // InternalStructuredTextParser.g:7606:1: LeftParenthesis
            {
             before(grammarAccess.getFunc_CallAccess().getLeftParenthesisKeyword_1()); 
            match(input,LeftParenthesis,FOLLOW_2); 
             after(grammarAccess.getFunc_CallAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group__1__Impl"


    // $ANTLR start "rule__Func_Call__Group__2"
    // InternalStructuredTextParser.g:7619:1: rule__Func_Call__Group__2 : rule__Func_Call__Group__2__Impl rule__Func_Call__Group__3 ;
    public final void rule__Func_Call__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7623:1: ( rule__Func_Call__Group__2__Impl rule__Func_Call__Group__3 )
            // InternalStructuredTextParser.g:7624:2: rule__Func_Call__Group__2__Impl rule__Func_Call__Group__3
            {
            pushFollow(FOLLOW_55);
            rule__Func_Call__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Func_Call__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group__2"


    // $ANTLR start "rule__Func_Call__Group__2__Impl"
    // InternalStructuredTextParser.g:7631:1: rule__Func_Call__Group__2__Impl : ( ( rule__Func_Call__Group_2__0 )? ) ;
    public final void rule__Func_Call__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7635:1: ( ( ( rule__Func_Call__Group_2__0 )? ) )
            // InternalStructuredTextParser.g:7636:1: ( ( rule__Func_Call__Group_2__0 )? )
            {
            // InternalStructuredTextParser.g:7636:1: ( ( rule__Func_Call__Group_2__0 )? )
            // InternalStructuredTextParser.g:7637:1: ( rule__Func_Call__Group_2__0 )?
            {
             before(grammarAccess.getFunc_CallAccess().getGroup_2()); 
            // InternalStructuredTextParser.g:7638:1: ( rule__Func_Call__Group_2__0 )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( ((LA55_0>=LDATE_AND_TIME && LA55_0<=TIME_OF_DAY)||LA55_0==WSTRING||LA55_0==STRING||(LA55_0>=FALSE && LA55_0<=LTIME)||(LA55_0>=UDINT && LA55_0<=ULINT)||(LA55_0>=USINT && LA55_0<=WCHAR)||LA55_0==BOOL||(LA55_0>=CHAR && LA55_0<=DINT)||(LA55_0>=LINT && LA55_0<=SINT)||(LA55_0>=TIME && LA55_0<=UINT)||(LA55_0>=INT && LA55_0<=LDT)||(LA55_0>=NOT && LA55_0<=TOD)||LA55_0==DT||(LA55_0>=LD && LA55_0<=LT)||LA55_0==LeftParenthesis||LA55_0==PlusSign||LA55_0==HyphenMinus||LA55_0==T||LA55_0==D_1||(LA55_0>=RULE_ID && LA55_0<=RULE_UNSIGNED_INT)||LA55_0==RULE_S_BYTE_CHAR_STR||LA55_0==RULE_D_BYTE_CHAR_STR) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalStructuredTextParser.g:7638:2: rule__Func_Call__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Func_Call__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFunc_CallAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group__2__Impl"


    // $ANTLR start "rule__Func_Call__Group__3"
    // InternalStructuredTextParser.g:7648:1: rule__Func_Call__Group__3 : rule__Func_Call__Group__3__Impl ;
    public final void rule__Func_Call__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7652:1: ( rule__Func_Call__Group__3__Impl )
            // InternalStructuredTextParser.g:7653:2: rule__Func_Call__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Func_Call__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group__3"


    // $ANTLR start "rule__Func_Call__Group__3__Impl"
    // InternalStructuredTextParser.g:7659:1: rule__Func_Call__Group__3__Impl : ( RightParenthesis ) ;
    public final void rule__Func_Call__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7663:1: ( ( RightParenthesis ) )
            // InternalStructuredTextParser.g:7664:1: ( RightParenthesis )
            {
            // InternalStructuredTextParser.g:7664:1: ( RightParenthesis )
            // InternalStructuredTextParser.g:7665:1: RightParenthesis
            {
             before(grammarAccess.getFunc_CallAccess().getRightParenthesisKeyword_3()); 
            match(input,RightParenthesis,FOLLOW_2); 
             after(grammarAccess.getFunc_CallAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group__3__Impl"


    // $ANTLR start "rule__Func_Call__Group_2__0"
    // InternalStructuredTextParser.g:7686:1: rule__Func_Call__Group_2__0 : rule__Func_Call__Group_2__0__Impl rule__Func_Call__Group_2__1 ;
    public final void rule__Func_Call__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7690:1: ( rule__Func_Call__Group_2__0__Impl rule__Func_Call__Group_2__1 )
            // InternalStructuredTextParser.g:7691:2: rule__Func_Call__Group_2__0__Impl rule__Func_Call__Group_2__1
            {
            pushFollow(FOLLOW_56);
            rule__Func_Call__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Func_Call__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group_2__0"


    // $ANTLR start "rule__Func_Call__Group_2__0__Impl"
    // InternalStructuredTextParser.g:7698:1: rule__Func_Call__Group_2__0__Impl : ( ( rule__Func_Call__ArgsAssignment_2_0 ) ) ;
    public final void rule__Func_Call__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7702:1: ( ( ( rule__Func_Call__ArgsAssignment_2_0 ) ) )
            // InternalStructuredTextParser.g:7703:1: ( ( rule__Func_Call__ArgsAssignment_2_0 ) )
            {
            // InternalStructuredTextParser.g:7703:1: ( ( rule__Func_Call__ArgsAssignment_2_0 ) )
            // InternalStructuredTextParser.g:7704:1: ( rule__Func_Call__ArgsAssignment_2_0 )
            {
             before(grammarAccess.getFunc_CallAccess().getArgsAssignment_2_0()); 
            // InternalStructuredTextParser.g:7705:1: ( rule__Func_Call__ArgsAssignment_2_0 )
            // InternalStructuredTextParser.g:7705:2: rule__Func_Call__ArgsAssignment_2_0
            {
            pushFollow(FOLLOW_2);
            rule__Func_Call__ArgsAssignment_2_0();

            state._fsp--;


            }

             after(grammarAccess.getFunc_CallAccess().getArgsAssignment_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group_2__0__Impl"


    // $ANTLR start "rule__Func_Call__Group_2__1"
    // InternalStructuredTextParser.g:7715:1: rule__Func_Call__Group_2__1 : rule__Func_Call__Group_2__1__Impl ;
    public final void rule__Func_Call__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7719:1: ( rule__Func_Call__Group_2__1__Impl )
            // InternalStructuredTextParser.g:7720:2: rule__Func_Call__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Func_Call__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group_2__1"


    // $ANTLR start "rule__Func_Call__Group_2__1__Impl"
    // InternalStructuredTextParser.g:7726:1: rule__Func_Call__Group_2__1__Impl : ( ( rule__Func_Call__Group_2_1__0 )* ) ;
    public final void rule__Func_Call__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7730:1: ( ( ( rule__Func_Call__Group_2_1__0 )* ) )
            // InternalStructuredTextParser.g:7731:1: ( ( rule__Func_Call__Group_2_1__0 )* )
            {
            // InternalStructuredTextParser.g:7731:1: ( ( rule__Func_Call__Group_2_1__0 )* )
            // InternalStructuredTextParser.g:7732:1: ( rule__Func_Call__Group_2_1__0 )*
            {
             before(grammarAccess.getFunc_CallAccess().getGroup_2_1()); 
            // InternalStructuredTextParser.g:7733:1: ( rule__Func_Call__Group_2_1__0 )*
            loop56:
            do {
                int alt56=2;
                int LA56_0 = input.LA(1);

                if ( (LA56_0==Comma) ) {
                    alt56=1;
                }


                switch (alt56) {
            	case 1 :
            	    // InternalStructuredTextParser.g:7733:2: rule__Func_Call__Group_2_1__0
            	    {
            	    pushFollow(FOLLOW_26);
            	    rule__Func_Call__Group_2_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop56;
                }
            } while (true);

             after(grammarAccess.getFunc_CallAccess().getGroup_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group_2__1__Impl"


    // $ANTLR start "rule__Func_Call__Group_2_1__0"
    // InternalStructuredTextParser.g:7747:1: rule__Func_Call__Group_2_1__0 : rule__Func_Call__Group_2_1__0__Impl rule__Func_Call__Group_2_1__1 ;
    public final void rule__Func_Call__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7751:1: ( rule__Func_Call__Group_2_1__0__Impl rule__Func_Call__Group_2_1__1 )
            // InternalStructuredTextParser.g:7752:2: rule__Func_Call__Group_2_1__0__Impl rule__Func_Call__Group_2_1__1
            {
            pushFollow(FOLLOW_14);
            rule__Func_Call__Group_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Func_Call__Group_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group_2_1__0"


    // $ANTLR start "rule__Func_Call__Group_2_1__0__Impl"
    // InternalStructuredTextParser.g:7759:1: rule__Func_Call__Group_2_1__0__Impl : ( Comma ) ;
    public final void rule__Func_Call__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7763:1: ( ( Comma ) )
            // InternalStructuredTextParser.g:7764:1: ( Comma )
            {
            // InternalStructuredTextParser.g:7764:1: ( Comma )
            // InternalStructuredTextParser.g:7765:1: Comma
            {
             before(grammarAccess.getFunc_CallAccess().getCommaKeyword_2_1_0()); 
            match(input,Comma,FOLLOW_2); 
             after(grammarAccess.getFunc_CallAccess().getCommaKeyword_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group_2_1__0__Impl"


    // $ANTLR start "rule__Func_Call__Group_2_1__1"
    // InternalStructuredTextParser.g:7778:1: rule__Func_Call__Group_2_1__1 : rule__Func_Call__Group_2_1__1__Impl ;
    public final void rule__Func_Call__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7782:1: ( rule__Func_Call__Group_2_1__1__Impl )
            // InternalStructuredTextParser.g:7783:2: rule__Func_Call__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Func_Call__Group_2_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group_2_1__1"


    // $ANTLR start "rule__Func_Call__Group_2_1__1__Impl"
    // InternalStructuredTextParser.g:7789:1: rule__Func_Call__Group_2_1__1__Impl : ( ( rule__Func_Call__ArgsAssignment_2_1_1 ) ) ;
    public final void rule__Func_Call__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7793:1: ( ( ( rule__Func_Call__ArgsAssignment_2_1_1 ) ) )
            // InternalStructuredTextParser.g:7794:1: ( ( rule__Func_Call__ArgsAssignment_2_1_1 ) )
            {
            // InternalStructuredTextParser.g:7794:1: ( ( rule__Func_Call__ArgsAssignment_2_1_1 ) )
            // InternalStructuredTextParser.g:7795:1: ( rule__Func_Call__ArgsAssignment_2_1_1 )
            {
             before(grammarAccess.getFunc_CallAccess().getArgsAssignment_2_1_1()); 
            // InternalStructuredTextParser.g:7796:1: ( rule__Func_Call__ArgsAssignment_2_1_1 )
            // InternalStructuredTextParser.g:7796:2: rule__Func_Call__ArgsAssignment_2_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Func_Call__ArgsAssignment_2_1_1();

            state._fsp--;


            }

             after(grammarAccess.getFunc_CallAccess().getArgsAssignment_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__Group_2_1__1__Impl"


    // $ANTLR start "rule__Param_Assign_In__Group__0"
    // InternalStructuredTextParser.g:7810:1: rule__Param_Assign_In__Group__0 : rule__Param_Assign_In__Group__0__Impl rule__Param_Assign_In__Group__1 ;
    public final void rule__Param_Assign_In__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7814:1: ( rule__Param_Assign_In__Group__0__Impl rule__Param_Assign_In__Group__1 )
            // InternalStructuredTextParser.g:7815:2: rule__Param_Assign_In__Group__0__Impl rule__Param_Assign_In__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__Param_Assign_In__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Param_Assign_In__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_In__Group__0"


    // $ANTLR start "rule__Param_Assign_In__Group__0__Impl"
    // InternalStructuredTextParser.g:7822:1: rule__Param_Assign_In__Group__0__Impl : ( ( rule__Param_Assign_In__Group_0__0 )? ) ;
    public final void rule__Param_Assign_In__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7826:1: ( ( ( rule__Param_Assign_In__Group_0__0 )? ) )
            // InternalStructuredTextParser.g:7827:1: ( ( rule__Param_Assign_In__Group_0__0 )? )
            {
            // InternalStructuredTextParser.g:7827:1: ( ( rule__Param_Assign_In__Group_0__0 )? )
            // InternalStructuredTextParser.g:7828:1: ( rule__Param_Assign_In__Group_0__0 )?
            {
             before(grammarAccess.getParam_Assign_InAccess().getGroup_0()); 
            // InternalStructuredTextParser.g:7829:1: ( rule__Param_Assign_In__Group_0__0 )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==RULE_ID) ) {
                int LA57_1 = input.LA(2);

                if ( (LA57_1==ColonEqualsSign) ) {
                    alt57=1;
                }
            }
            switch (alt57) {
                case 1 :
                    // InternalStructuredTextParser.g:7829:2: rule__Param_Assign_In__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Param_Assign_In__Group_0__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getParam_Assign_InAccess().getGroup_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_In__Group__0__Impl"


    // $ANTLR start "rule__Param_Assign_In__Group__1"
    // InternalStructuredTextParser.g:7839:1: rule__Param_Assign_In__Group__1 : rule__Param_Assign_In__Group__1__Impl ;
    public final void rule__Param_Assign_In__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7843:1: ( rule__Param_Assign_In__Group__1__Impl )
            // InternalStructuredTextParser.g:7844:2: rule__Param_Assign_In__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Param_Assign_In__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_In__Group__1"


    // $ANTLR start "rule__Param_Assign_In__Group__1__Impl"
    // InternalStructuredTextParser.g:7850:1: rule__Param_Assign_In__Group__1__Impl : ( ( rule__Param_Assign_In__ExprAssignment_1 ) ) ;
    public final void rule__Param_Assign_In__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7854:1: ( ( ( rule__Param_Assign_In__ExprAssignment_1 ) ) )
            // InternalStructuredTextParser.g:7855:1: ( ( rule__Param_Assign_In__ExprAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:7855:1: ( ( rule__Param_Assign_In__ExprAssignment_1 ) )
            // InternalStructuredTextParser.g:7856:1: ( rule__Param_Assign_In__ExprAssignment_1 )
            {
             before(grammarAccess.getParam_Assign_InAccess().getExprAssignment_1()); 
            // InternalStructuredTextParser.g:7857:1: ( rule__Param_Assign_In__ExprAssignment_1 )
            // InternalStructuredTextParser.g:7857:2: rule__Param_Assign_In__ExprAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Param_Assign_In__ExprAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getParam_Assign_InAccess().getExprAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_In__Group__1__Impl"


    // $ANTLR start "rule__Param_Assign_In__Group_0__0"
    // InternalStructuredTextParser.g:7871:1: rule__Param_Assign_In__Group_0__0 : rule__Param_Assign_In__Group_0__0__Impl rule__Param_Assign_In__Group_0__1 ;
    public final void rule__Param_Assign_In__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7875:1: ( rule__Param_Assign_In__Group_0__0__Impl rule__Param_Assign_In__Group_0__1 )
            // InternalStructuredTextParser.g:7876:2: rule__Param_Assign_In__Group_0__0__Impl rule__Param_Assign_In__Group_0__1
            {
            pushFollow(FOLLOW_10);
            rule__Param_Assign_In__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Param_Assign_In__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_In__Group_0__0"


    // $ANTLR start "rule__Param_Assign_In__Group_0__0__Impl"
    // InternalStructuredTextParser.g:7883:1: rule__Param_Assign_In__Group_0__0__Impl : ( ( rule__Param_Assign_In__VarAssignment_0_0 ) ) ;
    public final void rule__Param_Assign_In__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7887:1: ( ( ( rule__Param_Assign_In__VarAssignment_0_0 ) ) )
            // InternalStructuredTextParser.g:7888:1: ( ( rule__Param_Assign_In__VarAssignment_0_0 ) )
            {
            // InternalStructuredTextParser.g:7888:1: ( ( rule__Param_Assign_In__VarAssignment_0_0 ) )
            // InternalStructuredTextParser.g:7889:1: ( rule__Param_Assign_In__VarAssignment_0_0 )
            {
             before(grammarAccess.getParam_Assign_InAccess().getVarAssignment_0_0()); 
            // InternalStructuredTextParser.g:7890:1: ( rule__Param_Assign_In__VarAssignment_0_0 )
            // InternalStructuredTextParser.g:7890:2: rule__Param_Assign_In__VarAssignment_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Param_Assign_In__VarAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getParam_Assign_InAccess().getVarAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_In__Group_0__0__Impl"


    // $ANTLR start "rule__Param_Assign_In__Group_0__1"
    // InternalStructuredTextParser.g:7900:1: rule__Param_Assign_In__Group_0__1 : rule__Param_Assign_In__Group_0__1__Impl ;
    public final void rule__Param_Assign_In__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7904:1: ( rule__Param_Assign_In__Group_0__1__Impl )
            // InternalStructuredTextParser.g:7905:2: rule__Param_Assign_In__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Param_Assign_In__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_In__Group_0__1"


    // $ANTLR start "rule__Param_Assign_In__Group_0__1__Impl"
    // InternalStructuredTextParser.g:7911:1: rule__Param_Assign_In__Group_0__1__Impl : ( ColonEqualsSign ) ;
    public final void rule__Param_Assign_In__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7915:1: ( ( ColonEqualsSign ) )
            // InternalStructuredTextParser.g:7916:1: ( ColonEqualsSign )
            {
            // InternalStructuredTextParser.g:7916:1: ( ColonEqualsSign )
            // InternalStructuredTextParser.g:7917:1: ColonEqualsSign
            {
             before(grammarAccess.getParam_Assign_InAccess().getColonEqualsSignKeyword_0_1()); 
            match(input,ColonEqualsSign,FOLLOW_2); 
             after(grammarAccess.getParam_Assign_InAccess().getColonEqualsSignKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_In__Group_0__1__Impl"


    // $ANTLR start "rule__Param_Assign_Out__Group__0"
    // InternalStructuredTextParser.g:7934:1: rule__Param_Assign_Out__Group__0 : rule__Param_Assign_Out__Group__0__Impl rule__Param_Assign_Out__Group__1 ;
    public final void rule__Param_Assign_Out__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7938:1: ( rule__Param_Assign_Out__Group__0__Impl rule__Param_Assign_Out__Group__1 )
            // InternalStructuredTextParser.g:7939:2: rule__Param_Assign_Out__Group__0__Impl rule__Param_Assign_Out__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__Param_Assign_Out__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Param_Assign_Out__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_Out__Group__0"


    // $ANTLR start "rule__Param_Assign_Out__Group__0__Impl"
    // InternalStructuredTextParser.g:7946:1: rule__Param_Assign_Out__Group__0__Impl : ( ( rule__Param_Assign_Out__NotAssignment_0 )? ) ;
    public final void rule__Param_Assign_Out__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7950:1: ( ( ( rule__Param_Assign_Out__NotAssignment_0 )? ) )
            // InternalStructuredTextParser.g:7951:1: ( ( rule__Param_Assign_Out__NotAssignment_0 )? )
            {
            // InternalStructuredTextParser.g:7951:1: ( ( rule__Param_Assign_Out__NotAssignment_0 )? )
            // InternalStructuredTextParser.g:7952:1: ( rule__Param_Assign_Out__NotAssignment_0 )?
            {
             before(grammarAccess.getParam_Assign_OutAccess().getNotAssignment_0()); 
            // InternalStructuredTextParser.g:7953:1: ( rule__Param_Assign_Out__NotAssignment_0 )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( (LA58_0==NOT) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // InternalStructuredTextParser.g:7953:2: rule__Param_Assign_Out__NotAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Param_Assign_Out__NotAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getParam_Assign_OutAccess().getNotAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_Out__Group__0__Impl"


    // $ANTLR start "rule__Param_Assign_Out__Group__1"
    // InternalStructuredTextParser.g:7963:1: rule__Param_Assign_Out__Group__1 : rule__Param_Assign_Out__Group__1__Impl rule__Param_Assign_Out__Group__2 ;
    public final void rule__Param_Assign_Out__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7967:1: ( rule__Param_Assign_Out__Group__1__Impl rule__Param_Assign_Out__Group__2 )
            // InternalStructuredTextParser.g:7968:2: rule__Param_Assign_Out__Group__1__Impl rule__Param_Assign_Out__Group__2
            {
            pushFollow(FOLLOW_57);
            rule__Param_Assign_Out__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Param_Assign_Out__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_Out__Group__1"


    // $ANTLR start "rule__Param_Assign_Out__Group__1__Impl"
    // InternalStructuredTextParser.g:7975:1: rule__Param_Assign_Out__Group__1__Impl : ( ( rule__Param_Assign_Out__VarAssignment_1 ) ) ;
    public final void rule__Param_Assign_Out__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7979:1: ( ( ( rule__Param_Assign_Out__VarAssignment_1 ) ) )
            // InternalStructuredTextParser.g:7980:1: ( ( rule__Param_Assign_Out__VarAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:7980:1: ( ( rule__Param_Assign_Out__VarAssignment_1 ) )
            // InternalStructuredTextParser.g:7981:1: ( rule__Param_Assign_Out__VarAssignment_1 )
            {
             before(grammarAccess.getParam_Assign_OutAccess().getVarAssignment_1()); 
            // InternalStructuredTextParser.g:7982:1: ( rule__Param_Assign_Out__VarAssignment_1 )
            // InternalStructuredTextParser.g:7982:2: rule__Param_Assign_Out__VarAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Param_Assign_Out__VarAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getParam_Assign_OutAccess().getVarAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_Out__Group__1__Impl"


    // $ANTLR start "rule__Param_Assign_Out__Group__2"
    // InternalStructuredTextParser.g:7992:1: rule__Param_Assign_Out__Group__2 : rule__Param_Assign_Out__Group__2__Impl rule__Param_Assign_Out__Group__3 ;
    public final void rule__Param_Assign_Out__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:7996:1: ( rule__Param_Assign_Out__Group__2__Impl rule__Param_Assign_Out__Group__3 )
            // InternalStructuredTextParser.g:7997:2: rule__Param_Assign_Out__Group__2__Impl rule__Param_Assign_Out__Group__3
            {
            pushFollow(FOLLOW_29);
            rule__Param_Assign_Out__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Param_Assign_Out__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_Out__Group__2"


    // $ANTLR start "rule__Param_Assign_Out__Group__2__Impl"
    // InternalStructuredTextParser.g:8004:1: rule__Param_Assign_Out__Group__2__Impl : ( EqualsSignGreaterThanSign ) ;
    public final void rule__Param_Assign_Out__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8008:1: ( ( EqualsSignGreaterThanSign ) )
            // InternalStructuredTextParser.g:8009:1: ( EqualsSignGreaterThanSign )
            {
            // InternalStructuredTextParser.g:8009:1: ( EqualsSignGreaterThanSign )
            // InternalStructuredTextParser.g:8010:1: EqualsSignGreaterThanSign
            {
             before(grammarAccess.getParam_Assign_OutAccess().getEqualsSignGreaterThanSignKeyword_2()); 
            match(input,EqualsSignGreaterThanSign,FOLLOW_2); 
             after(grammarAccess.getParam_Assign_OutAccess().getEqualsSignGreaterThanSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_Out__Group__2__Impl"


    // $ANTLR start "rule__Param_Assign_Out__Group__3"
    // InternalStructuredTextParser.g:8023:1: rule__Param_Assign_Out__Group__3 : rule__Param_Assign_Out__Group__3__Impl ;
    public final void rule__Param_Assign_Out__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8027:1: ( rule__Param_Assign_Out__Group__3__Impl )
            // InternalStructuredTextParser.g:8028:2: rule__Param_Assign_Out__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Param_Assign_Out__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_Out__Group__3"


    // $ANTLR start "rule__Param_Assign_Out__Group__3__Impl"
    // InternalStructuredTextParser.g:8034:1: rule__Param_Assign_Out__Group__3__Impl : ( ( rule__Param_Assign_Out__ExprAssignment_3 ) ) ;
    public final void rule__Param_Assign_Out__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8038:1: ( ( ( rule__Param_Assign_Out__ExprAssignment_3 ) ) )
            // InternalStructuredTextParser.g:8039:1: ( ( rule__Param_Assign_Out__ExprAssignment_3 ) )
            {
            // InternalStructuredTextParser.g:8039:1: ( ( rule__Param_Assign_Out__ExprAssignment_3 ) )
            // InternalStructuredTextParser.g:8040:1: ( rule__Param_Assign_Out__ExprAssignment_3 )
            {
             before(grammarAccess.getParam_Assign_OutAccess().getExprAssignment_3()); 
            // InternalStructuredTextParser.g:8041:1: ( rule__Param_Assign_Out__ExprAssignment_3 )
            // InternalStructuredTextParser.g:8041:2: rule__Param_Assign_Out__ExprAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Param_Assign_Out__ExprAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getParam_Assign_OutAccess().getExprAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_Out__Group__3__Impl"


    // $ANTLR start "rule__Variable_Subscript__Group__0"
    // InternalStructuredTextParser.g:8059:1: rule__Variable_Subscript__Group__0 : rule__Variable_Subscript__Group__0__Impl rule__Variable_Subscript__Group__1 ;
    public final void rule__Variable_Subscript__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8063:1: ( rule__Variable_Subscript__Group__0__Impl rule__Variable_Subscript__Group__1 )
            // InternalStructuredTextParser.g:8064:2: rule__Variable_Subscript__Group__0__Impl rule__Variable_Subscript__Group__1
            {
            pushFollow(FOLLOW_58);
            rule__Variable_Subscript__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable_Subscript__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group__0"


    // $ANTLR start "rule__Variable_Subscript__Group__0__Impl"
    // InternalStructuredTextParser.g:8071:1: rule__Variable_Subscript__Group__0__Impl : ( ( rule__Variable_Subscript__Alternatives_0 ) ) ;
    public final void rule__Variable_Subscript__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8075:1: ( ( ( rule__Variable_Subscript__Alternatives_0 ) ) )
            // InternalStructuredTextParser.g:8076:1: ( ( rule__Variable_Subscript__Alternatives_0 ) )
            {
            // InternalStructuredTextParser.g:8076:1: ( ( rule__Variable_Subscript__Alternatives_0 ) )
            // InternalStructuredTextParser.g:8077:1: ( rule__Variable_Subscript__Alternatives_0 )
            {
             before(grammarAccess.getVariable_SubscriptAccess().getAlternatives_0()); 
            // InternalStructuredTextParser.g:8078:1: ( rule__Variable_Subscript__Alternatives_0 )
            // InternalStructuredTextParser.g:8078:2: rule__Variable_Subscript__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__Variable_Subscript__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getVariable_SubscriptAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group__0__Impl"


    // $ANTLR start "rule__Variable_Subscript__Group__1"
    // InternalStructuredTextParser.g:8088:1: rule__Variable_Subscript__Group__1 : rule__Variable_Subscript__Group__1__Impl ;
    public final void rule__Variable_Subscript__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8092:1: ( rule__Variable_Subscript__Group__1__Impl )
            // InternalStructuredTextParser.g:8093:2: rule__Variable_Subscript__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Variable_Subscript__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group__1"


    // $ANTLR start "rule__Variable_Subscript__Group__1__Impl"
    // InternalStructuredTextParser.g:8099:1: rule__Variable_Subscript__Group__1__Impl : ( ( rule__Variable_Subscript__Group_1__0 )? ) ;
    public final void rule__Variable_Subscript__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8103:1: ( ( ( rule__Variable_Subscript__Group_1__0 )? ) )
            // InternalStructuredTextParser.g:8104:1: ( ( rule__Variable_Subscript__Group_1__0 )? )
            {
            // InternalStructuredTextParser.g:8104:1: ( ( rule__Variable_Subscript__Group_1__0 )? )
            // InternalStructuredTextParser.g:8105:1: ( rule__Variable_Subscript__Group_1__0 )?
            {
             before(grammarAccess.getVariable_SubscriptAccess().getGroup_1()); 
            // InternalStructuredTextParser.g:8106:1: ( rule__Variable_Subscript__Group_1__0 )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==LeftSquareBracket) ) {
                alt59=1;
            }
            switch (alt59) {
                case 1 :
                    // InternalStructuredTextParser.g:8106:2: rule__Variable_Subscript__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Variable_Subscript__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getVariable_SubscriptAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group__1__Impl"


    // $ANTLR start "rule__Variable_Subscript__Group_1__0"
    // InternalStructuredTextParser.g:8120:1: rule__Variable_Subscript__Group_1__0 : rule__Variable_Subscript__Group_1__0__Impl rule__Variable_Subscript__Group_1__1 ;
    public final void rule__Variable_Subscript__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8124:1: ( rule__Variable_Subscript__Group_1__0__Impl rule__Variable_Subscript__Group_1__1 )
            // InternalStructuredTextParser.g:8125:2: rule__Variable_Subscript__Group_1__0__Impl rule__Variable_Subscript__Group_1__1
            {
            pushFollow(FOLLOW_58);
            rule__Variable_Subscript__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable_Subscript__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1__0"


    // $ANTLR start "rule__Variable_Subscript__Group_1__0__Impl"
    // InternalStructuredTextParser.g:8132:1: rule__Variable_Subscript__Group_1__0__Impl : ( () ) ;
    public final void rule__Variable_Subscript__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8136:1: ( ( () ) )
            // InternalStructuredTextParser.g:8137:1: ( () )
            {
            // InternalStructuredTextParser.g:8137:1: ( () )
            // InternalStructuredTextParser.g:8138:1: ()
            {
             before(grammarAccess.getVariable_SubscriptAccess().getArrayVariableArrayAction_1_0()); 
            // InternalStructuredTextParser.g:8139:1: ()
            // InternalStructuredTextParser.g:8141:1: 
            {
            }

             after(grammarAccess.getVariable_SubscriptAccess().getArrayVariableArrayAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1__0__Impl"


    // $ANTLR start "rule__Variable_Subscript__Group_1__1"
    // InternalStructuredTextParser.g:8151:1: rule__Variable_Subscript__Group_1__1 : rule__Variable_Subscript__Group_1__1__Impl rule__Variable_Subscript__Group_1__2 ;
    public final void rule__Variable_Subscript__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8155:1: ( rule__Variable_Subscript__Group_1__1__Impl rule__Variable_Subscript__Group_1__2 )
            // InternalStructuredTextParser.g:8156:2: rule__Variable_Subscript__Group_1__1__Impl rule__Variable_Subscript__Group_1__2
            {
            pushFollow(FOLLOW_14);
            rule__Variable_Subscript__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable_Subscript__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1__1"


    // $ANTLR start "rule__Variable_Subscript__Group_1__1__Impl"
    // InternalStructuredTextParser.g:8163:1: rule__Variable_Subscript__Group_1__1__Impl : ( LeftSquareBracket ) ;
    public final void rule__Variable_Subscript__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8167:1: ( ( LeftSquareBracket ) )
            // InternalStructuredTextParser.g:8168:1: ( LeftSquareBracket )
            {
            // InternalStructuredTextParser.g:8168:1: ( LeftSquareBracket )
            // InternalStructuredTextParser.g:8169:1: LeftSquareBracket
            {
             before(grammarAccess.getVariable_SubscriptAccess().getLeftSquareBracketKeyword_1_1()); 
            match(input,LeftSquareBracket,FOLLOW_2); 
             after(grammarAccess.getVariable_SubscriptAccess().getLeftSquareBracketKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1__1__Impl"


    // $ANTLR start "rule__Variable_Subscript__Group_1__2"
    // InternalStructuredTextParser.g:8182:1: rule__Variable_Subscript__Group_1__2 : rule__Variable_Subscript__Group_1__2__Impl rule__Variable_Subscript__Group_1__3 ;
    public final void rule__Variable_Subscript__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8186:1: ( rule__Variable_Subscript__Group_1__2__Impl rule__Variable_Subscript__Group_1__3 )
            // InternalStructuredTextParser.g:8187:2: rule__Variable_Subscript__Group_1__2__Impl rule__Variable_Subscript__Group_1__3
            {
            pushFollow(FOLLOW_59);
            rule__Variable_Subscript__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable_Subscript__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1__2"


    // $ANTLR start "rule__Variable_Subscript__Group_1__2__Impl"
    // InternalStructuredTextParser.g:8194:1: rule__Variable_Subscript__Group_1__2__Impl : ( ( rule__Variable_Subscript__IndexAssignment_1_2 ) ) ;
    public final void rule__Variable_Subscript__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8198:1: ( ( ( rule__Variable_Subscript__IndexAssignment_1_2 ) ) )
            // InternalStructuredTextParser.g:8199:1: ( ( rule__Variable_Subscript__IndexAssignment_1_2 ) )
            {
            // InternalStructuredTextParser.g:8199:1: ( ( rule__Variable_Subscript__IndexAssignment_1_2 ) )
            // InternalStructuredTextParser.g:8200:1: ( rule__Variable_Subscript__IndexAssignment_1_2 )
            {
             before(grammarAccess.getVariable_SubscriptAccess().getIndexAssignment_1_2()); 
            // InternalStructuredTextParser.g:8201:1: ( rule__Variable_Subscript__IndexAssignment_1_2 )
            // InternalStructuredTextParser.g:8201:2: rule__Variable_Subscript__IndexAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Variable_Subscript__IndexAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getVariable_SubscriptAccess().getIndexAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1__2__Impl"


    // $ANTLR start "rule__Variable_Subscript__Group_1__3"
    // InternalStructuredTextParser.g:8211:1: rule__Variable_Subscript__Group_1__3 : rule__Variable_Subscript__Group_1__3__Impl rule__Variable_Subscript__Group_1__4 ;
    public final void rule__Variable_Subscript__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8215:1: ( rule__Variable_Subscript__Group_1__3__Impl rule__Variable_Subscript__Group_1__4 )
            // InternalStructuredTextParser.g:8216:2: rule__Variable_Subscript__Group_1__3__Impl rule__Variable_Subscript__Group_1__4
            {
            pushFollow(FOLLOW_59);
            rule__Variable_Subscript__Group_1__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable_Subscript__Group_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1__3"


    // $ANTLR start "rule__Variable_Subscript__Group_1__3__Impl"
    // InternalStructuredTextParser.g:8223:1: rule__Variable_Subscript__Group_1__3__Impl : ( ( rule__Variable_Subscript__Group_1_3__0 )* ) ;
    public final void rule__Variable_Subscript__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8227:1: ( ( ( rule__Variable_Subscript__Group_1_3__0 )* ) )
            // InternalStructuredTextParser.g:8228:1: ( ( rule__Variable_Subscript__Group_1_3__0 )* )
            {
            // InternalStructuredTextParser.g:8228:1: ( ( rule__Variable_Subscript__Group_1_3__0 )* )
            // InternalStructuredTextParser.g:8229:1: ( rule__Variable_Subscript__Group_1_3__0 )*
            {
             before(grammarAccess.getVariable_SubscriptAccess().getGroup_1_3()); 
            // InternalStructuredTextParser.g:8230:1: ( rule__Variable_Subscript__Group_1_3__0 )*
            loop60:
            do {
                int alt60=2;
                int LA60_0 = input.LA(1);

                if ( (LA60_0==Comma) ) {
                    alt60=1;
                }


                switch (alt60) {
            	case 1 :
            	    // InternalStructuredTextParser.g:8230:2: rule__Variable_Subscript__Group_1_3__0
            	    {
            	    pushFollow(FOLLOW_26);
            	    rule__Variable_Subscript__Group_1_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop60;
                }
            } while (true);

             after(grammarAccess.getVariable_SubscriptAccess().getGroup_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1__3__Impl"


    // $ANTLR start "rule__Variable_Subscript__Group_1__4"
    // InternalStructuredTextParser.g:8240:1: rule__Variable_Subscript__Group_1__4 : rule__Variable_Subscript__Group_1__4__Impl ;
    public final void rule__Variable_Subscript__Group_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8244:1: ( rule__Variable_Subscript__Group_1__4__Impl )
            // InternalStructuredTextParser.g:8245:2: rule__Variable_Subscript__Group_1__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Variable_Subscript__Group_1__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1__4"


    // $ANTLR start "rule__Variable_Subscript__Group_1__4__Impl"
    // InternalStructuredTextParser.g:8251:1: rule__Variable_Subscript__Group_1__4__Impl : ( RightSquareBracket ) ;
    public final void rule__Variable_Subscript__Group_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8255:1: ( ( RightSquareBracket ) )
            // InternalStructuredTextParser.g:8256:1: ( RightSquareBracket )
            {
            // InternalStructuredTextParser.g:8256:1: ( RightSquareBracket )
            // InternalStructuredTextParser.g:8257:1: RightSquareBracket
            {
             before(grammarAccess.getVariable_SubscriptAccess().getRightSquareBracketKeyword_1_4()); 
            match(input,RightSquareBracket,FOLLOW_2); 
             after(grammarAccess.getVariable_SubscriptAccess().getRightSquareBracketKeyword_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1__4__Impl"


    // $ANTLR start "rule__Variable_Subscript__Group_1_3__0"
    // InternalStructuredTextParser.g:8280:1: rule__Variable_Subscript__Group_1_3__0 : rule__Variable_Subscript__Group_1_3__0__Impl rule__Variable_Subscript__Group_1_3__1 ;
    public final void rule__Variable_Subscript__Group_1_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8284:1: ( rule__Variable_Subscript__Group_1_3__0__Impl rule__Variable_Subscript__Group_1_3__1 )
            // InternalStructuredTextParser.g:8285:2: rule__Variable_Subscript__Group_1_3__0__Impl rule__Variable_Subscript__Group_1_3__1
            {
            pushFollow(FOLLOW_14);
            rule__Variable_Subscript__Group_1_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable_Subscript__Group_1_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1_3__0"


    // $ANTLR start "rule__Variable_Subscript__Group_1_3__0__Impl"
    // InternalStructuredTextParser.g:8292:1: rule__Variable_Subscript__Group_1_3__0__Impl : ( Comma ) ;
    public final void rule__Variable_Subscript__Group_1_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8296:1: ( ( Comma ) )
            // InternalStructuredTextParser.g:8297:1: ( Comma )
            {
            // InternalStructuredTextParser.g:8297:1: ( Comma )
            // InternalStructuredTextParser.g:8298:1: Comma
            {
             before(grammarAccess.getVariable_SubscriptAccess().getCommaKeyword_1_3_0()); 
            match(input,Comma,FOLLOW_2); 
             after(grammarAccess.getVariable_SubscriptAccess().getCommaKeyword_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1_3__0__Impl"


    // $ANTLR start "rule__Variable_Subscript__Group_1_3__1"
    // InternalStructuredTextParser.g:8311:1: rule__Variable_Subscript__Group_1_3__1 : rule__Variable_Subscript__Group_1_3__1__Impl ;
    public final void rule__Variable_Subscript__Group_1_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8315:1: ( rule__Variable_Subscript__Group_1_3__1__Impl )
            // InternalStructuredTextParser.g:8316:2: rule__Variable_Subscript__Group_1_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Variable_Subscript__Group_1_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1_3__1"


    // $ANTLR start "rule__Variable_Subscript__Group_1_3__1__Impl"
    // InternalStructuredTextParser.g:8322:1: rule__Variable_Subscript__Group_1_3__1__Impl : ( ( rule__Variable_Subscript__IndexAssignment_1_3_1 ) ) ;
    public final void rule__Variable_Subscript__Group_1_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8326:1: ( ( ( rule__Variable_Subscript__IndexAssignment_1_3_1 ) ) )
            // InternalStructuredTextParser.g:8327:1: ( ( rule__Variable_Subscript__IndexAssignment_1_3_1 ) )
            {
            // InternalStructuredTextParser.g:8327:1: ( ( rule__Variable_Subscript__IndexAssignment_1_3_1 ) )
            // InternalStructuredTextParser.g:8328:1: ( rule__Variable_Subscript__IndexAssignment_1_3_1 )
            {
             before(grammarAccess.getVariable_SubscriptAccess().getIndexAssignment_1_3_1()); 
            // InternalStructuredTextParser.g:8329:1: ( rule__Variable_Subscript__IndexAssignment_1_3_1 )
            // InternalStructuredTextParser.g:8329:2: rule__Variable_Subscript__IndexAssignment_1_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Variable_Subscript__IndexAssignment_1_3_1();

            state._fsp--;


            }

             after(grammarAccess.getVariable_SubscriptAccess().getIndexAssignment_1_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__Group_1_3__1__Impl"


    // $ANTLR start "rule__Variable_Adapter__Group__0"
    // InternalStructuredTextParser.g:8343:1: rule__Variable_Adapter__Group__0 : rule__Variable_Adapter__Group__0__Impl rule__Variable_Adapter__Group__1 ;
    public final void rule__Variable_Adapter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8347:1: ( rule__Variable_Adapter__Group__0__Impl rule__Variable_Adapter__Group__1 )
            // InternalStructuredTextParser.g:8348:2: rule__Variable_Adapter__Group__0__Impl rule__Variable_Adapter__Group__1
            {
            pushFollow(FOLLOW_29);
            rule__Variable_Adapter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable_Adapter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Adapter__Group__0"


    // $ANTLR start "rule__Variable_Adapter__Group__0__Impl"
    // InternalStructuredTextParser.g:8355:1: rule__Variable_Adapter__Group__0__Impl : ( () ) ;
    public final void rule__Variable_Adapter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8359:1: ( ( () ) )
            // InternalStructuredTextParser.g:8360:1: ( () )
            {
            // InternalStructuredTextParser.g:8360:1: ( () )
            // InternalStructuredTextParser.g:8361:1: ()
            {
             before(grammarAccess.getVariable_AdapterAccess().getAdapterVariableAction_0()); 
            // InternalStructuredTextParser.g:8362:1: ()
            // InternalStructuredTextParser.g:8364:1: 
            {
            }

             after(grammarAccess.getVariable_AdapterAccess().getAdapterVariableAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Adapter__Group__0__Impl"


    // $ANTLR start "rule__Variable_Adapter__Group__1"
    // InternalStructuredTextParser.g:8374:1: rule__Variable_Adapter__Group__1 : rule__Variable_Adapter__Group__1__Impl rule__Variable_Adapter__Group__2 ;
    public final void rule__Variable_Adapter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8378:1: ( rule__Variable_Adapter__Group__1__Impl rule__Variable_Adapter__Group__2 )
            // InternalStructuredTextParser.g:8379:2: rule__Variable_Adapter__Group__1__Impl rule__Variable_Adapter__Group__2
            {
            pushFollow(FOLLOW_60);
            rule__Variable_Adapter__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable_Adapter__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Adapter__Group__1"


    // $ANTLR start "rule__Variable_Adapter__Group__1__Impl"
    // InternalStructuredTextParser.g:8386:1: rule__Variable_Adapter__Group__1__Impl : ( ( rule__Variable_Adapter__AdapterAssignment_1 ) ) ;
    public final void rule__Variable_Adapter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8390:1: ( ( ( rule__Variable_Adapter__AdapterAssignment_1 ) ) )
            // InternalStructuredTextParser.g:8391:1: ( ( rule__Variable_Adapter__AdapterAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:8391:1: ( ( rule__Variable_Adapter__AdapterAssignment_1 ) )
            // InternalStructuredTextParser.g:8392:1: ( rule__Variable_Adapter__AdapterAssignment_1 )
            {
             before(grammarAccess.getVariable_AdapterAccess().getAdapterAssignment_1()); 
            // InternalStructuredTextParser.g:8393:1: ( rule__Variable_Adapter__AdapterAssignment_1 )
            // InternalStructuredTextParser.g:8393:2: rule__Variable_Adapter__AdapterAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Variable_Adapter__AdapterAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getVariable_AdapterAccess().getAdapterAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Adapter__Group__1__Impl"


    // $ANTLR start "rule__Variable_Adapter__Group__2"
    // InternalStructuredTextParser.g:8403:1: rule__Variable_Adapter__Group__2 : rule__Variable_Adapter__Group__2__Impl rule__Variable_Adapter__Group__3 ;
    public final void rule__Variable_Adapter__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8407:1: ( rule__Variable_Adapter__Group__2__Impl rule__Variable_Adapter__Group__3 )
            // InternalStructuredTextParser.g:8408:2: rule__Variable_Adapter__Group__2__Impl rule__Variable_Adapter__Group__3
            {
            pushFollow(FOLLOW_29);
            rule__Variable_Adapter__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable_Adapter__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Adapter__Group__2"


    // $ANTLR start "rule__Variable_Adapter__Group__2__Impl"
    // InternalStructuredTextParser.g:8415:1: rule__Variable_Adapter__Group__2__Impl : ( FullStop ) ;
    public final void rule__Variable_Adapter__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8419:1: ( ( FullStop ) )
            // InternalStructuredTextParser.g:8420:1: ( FullStop )
            {
            // InternalStructuredTextParser.g:8420:1: ( FullStop )
            // InternalStructuredTextParser.g:8421:1: FullStop
            {
             before(grammarAccess.getVariable_AdapterAccess().getFullStopKeyword_2()); 
            match(input,FullStop,FOLLOW_2); 
             after(grammarAccess.getVariable_AdapterAccess().getFullStopKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Adapter__Group__2__Impl"


    // $ANTLR start "rule__Variable_Adapter__Group__3"
    // InternalStructuredTextParser.g:8434:1: rule__Variable_Adapter__Group__3 : rule__Variable_Adapter__Group__3__Impl ;
    public final void rule__Variable_Adapter__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8438:1: ( rule__Variable_Adapter__Group__3__Impl )
            // InternalStructuredTextParser.g:8439:2: rule__Variable_Adapter__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Variable_Adapter__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Adapter__Group__3"


    // $ANTLR start "rule__Variable_Adapter__Group__3__Impl"
    // InternalStructuredTextParser.g:8445:1: rule__Variable_Adapter__Group__3__Impl : ( ( rule__Variable_Adapter__VarAssignment_3 ) ) ;
    public final void rule__Variable_Adapter__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8449:1: ( ( ( rule__Variable_Adapter__VarAssignment_3 ) ) )
            // InternalStructuredTextParser.g:8450:1: ( ( rule__Variable_Adapter__VarAssignment_3 ) )
            {
            // InternalStructuredTextParser.g:8450:1: ( ( rule__Variable_Adapter__VarAssignment_3 ) )
            // InternalStructuredTextParser.g:8451:1: ( rule__Variable_Adapter__VarAssignment_3 )
            {
             before(grammarAccess.getVariable_AdapterAccess().getVarAssignment_3()); 
            // InternalStructuredTextParser.g:8452:1: ( rule__Variable_Adapter__VarAssignment_3 )
            // InternalStructuredTextParser.g:8452:2: rule__Variable_Adapter__VarAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Variable_Adapter__VarAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getVariable_AdapterAccess().getVarAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Adapter__Group__3__Impl"


    // $ANTLR start "rule__Int_Literal__Group__0"
    // InternalStructuredTextParser.g:8470:1: rule__Int_Literal__Group__0 : rule__Int_Literal__Group__0__Impl rule__Int_Literal__Group__1 ;
    public final void rule__Int_Literal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8474:1: ( rule__Int_Literal__Group__0__Impl rule__Int_Literal__Group__1 )
            // InternalStructuredTextParser.g:8475:2: rule__Int_Literal__Group__0__Impl rule__Int_Literal__Group__1
            {
            pushFollow(FOLLOW_61);
            rule__Int_Literal__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Int_Literal__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Int_Literal__Group__0"


    // $ANTLR start "rule__Int_Literal__Group__0__Impl"
    // InternalStructuredTextParser.g:8482:1: rule__Int_Literal__Group__0__Impl : ( ( rule__Int_Literal__Group_0__0 )? ) ;
    public final void rule__Int_Literal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8486:1: ( ( ( rule__Int_Literal__Group_0__0 )? ) )
            // InternalStructuredTextParser.g:8487:1: ( ( rule__Int_Literal__Group_0__0 )? )
            {
            // InternalStructuredTextParser.g:8487:1: ( ( rule__Int_Literal__Group_0__0 )? )
            // InternalStructuredTextParser.g:8488:1: ( rule__Int_Literal__Group_0__0 )?
            {
             before(grammarAccess.getInt_LiteralAccess().getGroup_0()); 
            // InternalStructuredTextParser.g:8489:1: ( rule__Int_Literal__Group_0__0 )?
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( ((LA61_0>=UDINT && LA61_0<=ULINT)||LA61_0==USINT||LA61_0==DINT||LA61_0==LINT||LA61_0==SINT||LA61_0==UINT||LA61_0==INT) ) {
                alt61=1;
            }
            switch (alt61) {
                case 1 :
                    // InternalStructuredTextParser.g:8489:2: rule__Int_Literal__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Int_Literal__Group_0__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getInt_LiteralAccess().getGroup_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Int_Literal__Group__0__Impl"


    // $ANTLR start "rule__Int_Literal__Group__1"
    // InternalStructuredTextParser.g:8499:1: rule__Int_Literal__Group__1 : rule__Int_Literal__Group__1__Impl ;
    public final void rule__Int_Literal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8503:1: ( rule__Int_Literal__Group__1__Impl )
            // InternalStructuredTextParser.g:8504:2: rule__Int_Literal__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Int_Literal__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Int_Literal__Group__1"


    // $ANTLR start "rule__Int_Literal__Group__1__Impl"
    // InternalStructuredTextParser.g:8510:1: rule__Int_Literal__Group__1__Impl : ( ( rule__Int_Literal__ValueAssignment_1 ) ) ;
    public final void rule__Int_Literal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8514:1: ( ( ( rule__Int_Literal__ValueAssignment_1 ) ) )
            // InternalStructuredTextParser.g:8515:1: ( ( rule__Int_Literal__ValueAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:8515:1: ( ( rule__Int_Literal__ValueAssignment_1 ) )
            // InternalStructuredTextParser.g:8516:1: ( rule__Int_Literal__ValueAssignment_1 )
            {
             before(grammarAccess.getInt_LiteralAccess().getValueAssignment_1()); 
            // InternalStructuredTextParser.g:8517:1: ( rule__Int_Literal__ValueAssignment_1 )
            // InternalStructuredTextParser.g:8517:2: rule__Int_Literal__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Int_Literal__ValueAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getInt_LiteralAccess().getValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Int_Literal__Group__1__Impl"


    // $ANTLR start "rule__Int_Literal__Group_0__0"
    // InternalStructuredTextParser.g:8531:1: rule__Int_Literal__Group_0__0 : rule__Int_Literal__Group_0__0__Impl rule__Int_Literal__Group_0__1 ;
    public final void rule__Int_Literal__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8535:1: ( rule__Int_Literal__Group_0__0__Impl rule__Int_Literal__Group_0__1 )
            // InternalStructuredTextParser.g:8536:2: rule__Int_Literal__Group_0__0__Impl rule__Int_Literal__Group_0__1
            {
            pushFollow(FOLLOW_62);
            rule__Int_Literal__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Int_Literal__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Int_Literal__Group_0__0"


    // $ANTLR start "rule__Int_Literal__Group_0__0__Impl"
    // InternalStructuredTextParser.g:8543:1: rule__Int_Literal__Group_0__0__Impl : ( ( rule__Int_Literal__TypeAssignment_0_0 ) ) ;
    public final void rule__Int_Literal__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8547:1: ( ( ( rule__Int_Literal__TypeAssignment_0_0 ) ) )
            // InternalStructuredTextParser.g:8548:1: ( ( rule__Int_Literal__TypeAssignment_0_0 ) )
            {
            // InternalStructuredTextParser.g:8548:1: ( ( rule__Int_Literal__TypeAssignment_0_0 ) )
            // InternalStructuredTextParser.g:8549:1: ( rule__Int_Literal__TypeAssignment_0_0 )
            {
             before(grammarAccess.getInt_LiteralAccess().getTypeAssignment_0_0()); 
            // InternalStructuredTextParser.g:8550:1: ( rule__Int_Literal__TypeAssignment_0_0 )
            // InternalStructuredTextParser.g:8550:2: rule__Int_Literal__TypeAssignment_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Int_Literal__TypeAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getInt_LiteralAccess().getTypeAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Int_Literal__Group_0__0__Impl"


    // $ANTLR start "rule__Int_Literal__Group_0__1"
    // InternalStructuredTextParser.g:8560:1: rule__Int_Literal__Group_0__1 : rule__Int_Literal__Group_0__1__Impl ;
    public final void rule__Int_Literal__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8564:1: ( rule__Int_Literal__Group_0__1__Impl )
            // InternalStructuredTextParser.g:8565:2: rule__Int_Literal__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Int_Literal__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Int_Literal__Group_0__1"


    // $ANTLR start "rule__Int_Literal__Group_0__1__Impl"
    // InternalStructuredTextParser.g:8571:1: rule__Int_Literal__Group_0__1__Impl : ( NumberSign ) ;
    public final void rule__Int_Literal__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8575:1: ( ( NumberSign ) )
            // InternalStructuredTextParser.g:8576:1: ( NumberSign )
            {
            // InternalStructuredTextParser.g:8576:1: ( NumberSign )
            // InternalStructuredTextParser.g:8577:1: NumberSign
            {
             before(grammarAccess.getInt_LiteralAccess().getNumberSignKeyword_0_1()); 
            match(input,NumberSign,FOLLOW_2); 
             after(grammarAccess.getInt_LiteralAccess().getNumberSignKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Int_Literal__Group_0__1__Impl"


    // $ANTLR start "rule__Signed_Int__Group__0"
    // InternalStructuredTextParser.g:8594:1: rule__Signed_Int__Group__0 : rule__Signed_Int__Group__0__Impl rule__Signed_Int__Group__1 ;
    public final void rule__Signed_Int__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8598:1: ( rule__Signed_Int__Group__0__Impl rule__Signed_Int__Group__1 )
            // InternalStructuredTextParser.g:8599:2: rule__Signed_Int__Group__0__Impl rule__Signed_Int__Group__1
            {
            pushFollow(FOLLOW_63);
            rule__Signed_Int__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Signed_Int__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Signed_Int__Group__0"


    // $ANTLR start "rule__Signed_Int__Group__0__Impl"
    // InternalStructuredTextParser.g:8606:1: rule__Signed_Int__Group__0__Impl : ( ( rule__Signed_Int__Alternatives_0 )? ) ;
    public final void rule__Signed_Int__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8610:1: ( ( ( rule__Signed_Int__Alternatives_0 )? ) )
            // InternalStructuredTextParser.g:8611:1: ( ( rule__Signed_Int__Alternatives_0 )? )
            {
            // InternalStructuredTextParser.g:8611:1: ( ( rule__Signed_Int__Alternatives_0 )? )
            // InternalStructuredTextParser.g:8612:1: ( rule__Signed_Int__Alternatives_0 )?
            {
             before(grammarAccess.getSigned_IntAccess().getAlternatives_0()); 
            // InternalStructuredTextParser.g:8613:1: ( rule__Signed_Int__Alternatives_0 )?
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==PlusSign||LA62_0==HyphenMinus) ) {
                alt62=1;
            }
            switch (alt62) {
                case 1 :
                    // InternalStructuredTextParser.g:8613:2: rule__Signed_Int__Alternatives_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Signed_Int__Alternatives_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSigned_IntAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Signed_Int__Group__0__Impl"


    // $ANTLR start "rule__Signed_Int__Group__1"
    // InternalStructuredTextParser.g:8623:1: rule__Signed_Int__Group__1 : rule__Signed_Int__Group__1__Impl ;
    public final void rule__Signed_Int__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8627:1: ( rule__Signed_Int__Group__1__Impl )
            // InternalStructuredTextParser.g:8628:2: rule__Signed_Int__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Signed_Int__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Signed_Int__Group__1"


    // $ANTLR start "rule__Signed_Int__Group__1__Impl"
    // InternalStructuredTextParser.g:8634:1: rule__Signed_Int__Group__1__Impl : ( RULE_UNSIGNED_INT ) ;
    public final void rule__Signed_Int__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8638:1: ( ( RULE_UNSIGNED_INT ) )
            // InternalStructuredTextParser.g:8639:1: ( RULE_UNSIGNED_INT )
            {
            // InternalStructuredTextParser.g:8639:1: ( RULE_UNSIGNED_INT )
            // InternalStructuredTextParser.g:8640:1: RULE_UNSIGNED_INT
            {
             before(grammarAccess.getSigned_IntAccess().getUNSIGNED_INTTerminalRuleCall_1()); 
            match(input,RULE_UNSIGNED_INT,FOLLOW_2); 
             after(grammarAccess.getSigned_IntAccess().getUNSIGNED_INTTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Signed_Int__Group__1__Impl"


    // $ANTLR start "rule__Real_Literal__Group__0"
    // InternalStructuredTextParser.g:8655:1: rule__Real_Literal__Group__0 : rule__Real_Literal__Group__0__Impl rule__Real_Literal__Group__1 ;
    public final void rule__Real_Literal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8659:1: ( rule__Real_Literal__Group__0__Impl rule__Real_Literal__Group__1 )
            // InternalStructuredTextParser.g:8660:2: rule__Real_Literal__Group__0__Impl rule__Real_Literal__Group__1
            {
            pushFollow(FOLLOW_64);
            rule__Real_Literal__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_Literal__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Literal__Group__0"


    // $ANTLR start "rule__Real_Literal__Group__0__Impl"
    // InternalStructuredTextParser.g:8667:1: rule__Real_Literal__Group__0__Impl : ( ( rule__Real_Literal__Group_0__0 )? ) ;
    public final void rule__Real_Literal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8671:1: ( ( ( rule__Real_Literal__Group_0__0 )? ) )
            // InternalStructuredTextParser.g:8672:1: ( ( rule__Real_Literal__Group_0__0 )? )
            {
            // InternalStructuredTextParser.g:8672:1: ( ( rule__Real_Literal__Group_0__0 )? )
            // InternalStructuredTextParser.g:8673:1: ( rule__Real_Literal__Group_0__0 )?
            {
             before(grammarAccess.getReal_LiteralAccess().getGroup_0()); 
            // InternalStructuredTextParser.g:8674:1: ( rule__Real_Literal__Group_0__0 )?
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==LREAL||LA63_0==REAL) ) {
                alt63=1;
            }
            switch (alt63) {
                case 1 :
                    // InternalStructuredTextParser.g:8674:2: rule__Real_Literal__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Real_Literal__Group_0__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReal_LiteralAccess().getGroup_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Literal__Group__0__Impl"


    // $ANTLR start "rule__Real_Literal__Group__1"
    // InternalStructuredTextParser.g:8684:1: rule__Real_Literal__Group__1 : rule__Real_Literal__Group__1__Impl ;
    public final void rule__Real_Literal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8688:1: ( rule__Real_Literal__Group__1__Impl )
            // InternalStructuredTextParser.g:8689:2: rule__Real_Literal__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Real_Literal__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Literal__Group__1"


    // $ANTLR start "rule__Real_Literal__Group__1__Impl"
    // InternalStructuredTextParser.g:8695:1: rule__Real_Literal__Group__1__Impl : ( ( rule__Real_Literal__ValueAssignment_1 ) ) ;
    public final void rule__Real_Literal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8699:1: ( ( ( rule__Real_Literal__ValueAssignment_1 ) ) )
            // InternalStructuredTextParser.g:8700:1: ( ( rule__Real_Literal__ValueAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:8700:1: ( ( rule__Real_Literal__ValueAssignment_1 ) )
            // InternalStructuredTextParser.g:8701:1: ( rule__Real_Literal__ValueAssignment_1 )
            {
             before(grammarAccess.getReal_LiteralAccess().getValueAssignment_1()); 
            // InternalStructuredTextParser.g:8702:1: ( rule__Real_Literal__ValueAssignment_1 )
            // InternalStructuredTextParser.g:8702:2: rule__Real_Literal__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Real_Literal__ValueAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getReal_LiteralAccess().getValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Literal__Group__1__Impl"


    // $ANTLR start "rule__Real_Literal__Group_0__0"
    // InternalStructuredTextParser.g:8716:1: rule__Real_Literal__Group_0__0 : rule__Real_Literal__Group_0__0__Impl rule__Real_Literal__Group_0__1 ;
    public final void rule__Real_Literal__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8720:1: ( rule__Real_Literal__Group_0__0__Impl rule__Real_Literal__Group_0__1 )
            // InternalStructuredTextParser.g:8721:2: rule__Real_Literal__Group_0__0__Impl rule__Real_Literal__Group_0__1
            {
            pushFollow(FOLLOW_62);
            rule__Real_Literal__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_Literal__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Literal__Group_0__0"


    // $ANTLR start "rule__Real_Literal__Group_0__0__Impl"
    // InternalStructuredTextParser.g:8728:1: rule__Real_Literal__Group_0__0__Impl : ( ( rule__Real_Literal__TypeAssignment_0_0 ) ) ;
    public final void rule__Real_Literal__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8732:1: ( ( ( rule__Real_Literal__TypeAssignment_0_0 ) ) )
            // InternalStructuredTextParser.g:8733:1: ( ( rule__Real_Literal__TypeAssignment_0_0 ) )
            {
            // InternalStructuredTextParser.g:8733:1: ( ( rule__Real_Literal__TypeAssignment_0_0 ) )
            // InternalStructuredTextParser.g:8734:1: ( rule__Real_Literal__TypeAssignment_0_0 )
            {
             before(grammarAccess.getReal_LiteralAccess().getTypeAssignment_0_0()); 
            // InternalStructuredTextParser.g:8735:1: ( rule__Real_Literal__TypeAssignment_0_0 )
            // InternalStructuredTextParser.g:8735:2: rule__Real_Literal__TypeAssignment_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Real_Literal__TypeAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getReal_LiteralAccess().getTypeAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Literal__Group_0__0__Impl"


    // $ANTLR start "rule__Real_Literal__Group_0__1"
    // InternalStructuredTextParser.g:8745:1: rule__Real_Literal__Group_0__1 : rule__Real_Literal__Group_0__1__Impl ;
    public final void rule__Real_Literal__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8749:1: ( rule__Real_Literal__Group_0__1__Impl )
            // InternalStructuredTextParser.g:8750:2: rule__Real_Literal__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Real_Literal__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Literal__Group_0__1"


    // $ANTLR start "rule__Real_Literal__Group_0__1__Impl"
    // InternalStructuredTextParser.g:8756:1: rule__Real_Literal__Group_0__1__Impl : ( NumberSign ) ;
    public final void rule__Real_Literal__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8760:1: ( ( NumberSign ) )
            // InternalStructuredTextParser.g:8761:1: ( NumberSign )
            {
            // InternalStructuredTextParser.g:8761:1: ( NumberSign )
            // InternalStructuredTextParser.g:8762:1: NumberSign
            {
             before(grammarAccess.getReal_LiteralAccess().getNumberSignKeyword_0_1()); 
            match(input,NumberSign,FOLLOW_2); 
             after(grammarAccess.getReal_LiteralAccess().getNumberSignKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Literal__Group_0__1__Impl"


    // $ANTLR start "rule__Real_Value__Group__0"
    // InternalStructuredTextParser.g:8779:1: rule__Real_Value__Group__0 : rule__Real_Value__Group__0__Impl rule__Real_Value__Group__1 ;
    public final void rule__Real_Value__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8783:1: ( rule__Real_Value__Group__0__Impl rule__Real_Value__Group__1 )
            // InternalStructuredTextParser.g:8784:2: rule__Real_Value__Group__0__Impl rule__Real_Value__Group__1
            {
            pushFollow(FOLLOW_60);
            rule__Real_Value__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_Value__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Value__Group__0"


    // $ANTLR start "rule__Real_Value__Group__0__Impl"
    // InternalStructuredTextParser.g:8791:1: rule__Real_Value__Group__0__Impl : ( ruleSigned_Int ) ;
    public final void rule__Real_Value__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8795:1: ( ( ruleSigned_Int ) )
            // InternalStructuredTextParser.g:8796:1: ( ruleSigned_Int )
            {
            // InternalStructuredTextParser.g:8796:1: ( ruleSigned_Int )
            // InternalStructuredTextParser.g:8797:1: ruleSigned_Int
            {
             before(grammarAccess.getReal_ValueAccess().getSigned_IntParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleSigned_Int();

            state._fsp--;

             after(grammarAccess.getReal_ValueAccess().getSigned_IntParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Value__Group__0__Impl"


    // $ANTLR start "rule__Real_Value__Group__1"
    // InternalStructuredTextParser.g:8808:1: rule__Real_Value__Group__1 : rule__Real_Value__Group__1__Impl rule__Real_Value__Group__2 ;
    public final void rule__Real_Value__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8812:1: ( rule__Real_Value__Group__1__Impl rule__Real_Value__Group__2 )
            // InternalStructuredTextParser.g:8813:2: rule__Real_Value__Group__1__Impl rule__Real_Value__Group__2
            {
            pushFollow(FOLLOW_65);
            rule__Real_Value__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_Value__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Value__Group__1"


    // $ANTLR start "rule__Real_Value__Group__1__Impl"
    // InternalStructuredTextParser.g:8820:1: rule__Real_Value__Group__1__Impl : ( FullStop ) ;
    public final void rule__Real_Value__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8824:1: ( ( FullStop ) )
            // InternalStructuredTextParser.g:8825:1: ( FullStop )
            {
            // InternalStructuredTextParser.g:8825:1: ( FullStop )
            // InternalStructuredTextParser.g:8826:1: FullStop
            {
             before(grammarAccess.getReal_ValueAccess().getFullStopKeyword_1()); 
            match(input,FullStop,FOLLOW_2); 
             after(grammarAccess.getReal_ValueAccess().getFullStopKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Value__Group__1__Impl"


    // $ANTLR start "rule__Real_Value__Group__2"
    // InternalStructuredTextParser.g:8839:1: rule__Real_Value__Group__2 : rule__Real_Value__Group__2__Impl rule__Real_Value__Group__3 ;
    public final void rule__Real_Value__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8843:1: ( rule__Real_Value__Group__2__Impl rule__Real_Value__Group__3 )
            // InternalStructuredTextParser.g:8844:2: rule__Real_Value__Group__2__Impl rule__Real_Value__Group__3
            {
            pushFollow(FOLLOW_66);
            rule__Real_Value__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_Value__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Value__Group__2"


    // $ANTLR start "rule__Real_Value__Group__2__Impl"
    // InternalStructuredTextParser.g:8851:1: rule__Real_Value__Group__2__Impl : ( RULE_UNSIGNED_INT ) ;
    public final void rule__Real_Value__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8855:1: ( ( RULE_UNSIGNED_INT ) )
            // InternalStructuredTextParser.g:8856:1: ( RULE_UNSIGNED_INT )
            {
            // InternalStructuredTextParser.g:8856:1: ( RULE_UNSIGNED_INT )
            // InternalStructuredTextParser.g:8857:1: RULE_UNSIGNED_INT
            {
             before(grammarAccess.getReal_ValueAccess().getUNSIGNED_INTTerminalRuleCall_2()); 
            match(input,RULE_UNSIGNED_INT,FOLLOW_2); 
             after(grammarAccess.getReal_ValueAccess().getUNSIGNED_INTTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Value__Group__2__Impl"


    // $ANTLR start "rule__Real_Value__Group__3"
    // InternalStructuredTextParser.g:8868:1: rule__Real_Value__Group__3 : rule__Real_Value__Group__3__Impl ;
    public final void rule__Real_Value__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8872:1: ( rule__Real_Value__Group__3__Impl )
            // InternalStructuredTextParser.g:8873:2: rule__Real_Value__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Real_Value__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Value__Group__3"


    // $ANTLR start "rule__Real_Value__Group__3__Impl"
    // InternalStructuredTextParser.g:8879:1: rule__Real_Value__Group__3__Impl : ( ( rule__Real_Value__Group_3__0 )? ) ;
    public final void rule__Real_Value__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8883:1: ( ( ( rule__Real_Value__Group_3__0 )? ) )
            // InternalStructuredTextParser.g:8884:1: ( ( rule__Real_Value__Group_3__0 )? )
            {
            // InternalStructuredTextParser.g:8884:1: ( ( rule__Real_Value__Group_3__0 )? )
            // InternalStructuredTextParser.g:8885:1: ( rule__Real_Value__Group_3__0 )?
            {
             before(grammarAccess.getReal_ValueAccess().getGroup_3()); 
            // InternalStructuredTextParser.g:8886:1: ( rule__Real_Value__Group_3__0 )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==E) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // InternalStructuredTextParser.g:8886:2: rule__Real_Value__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Real_Value__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReal_ValueAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Value__Group__3__Impl"


    // $ANTLR start "rule__Real_Value__Group_3__0"
    // InternalStructuredTextParser.g:8904:1: rule__Real_Value__Group_3__0 : rule__Real_Value__Group_3__0__Impl rule__Real_Value__Group_3__1 ;
    public final void rule__Real_Value__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8908:1: ( rule__Real_Value__Group_3__0__Impl rule__Real_Value__Group_3__1 )
            // InternalStructuredTextParser.g:8909:2: rule__Real_Value__Group_3__0__Impl rule__Real_Value__Group_3__1
            {
            pushFollow(FOLLOW_63);
            rule__Real_Value__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_Value__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Value__Group_3__0"


    // $ANTLR start "rule__Real_Value__Group_3__0__Impl"
    // InternalStructuredTextParser.g:8916:1: rule__Real_Value__Group_3__0__Impl : ( E ) ;
    public final void rule__Real_Value__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8920:1: ( ( E ) )
            // InternalStructuredTextParser.g:8921:1: ( E )
            {
            // InternalStructuredTextParser.g:8921:1: ( E )
            // InternalStructuredTextParser.g:8922:1: E
            {
             before(grammarAccess.getReal_ValueAccess().getEKeyword_3_0()); 
            match(input,E,FOLLOW_2); 
             after(grammarAccess.getReal_ValueAccess().getEKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Value__Group_3__0__Impl"


    // $ANTLR start "rule__Real_Value__Group_3__1"
    // InternalStructuredTextParser.g:8935:1: rule__Real_Value__Group_3__1 : rule__Real_Value__Group_3__1__Impl ;
    public final void rule__Real_Value__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8939:1: ( rule__Real_Value__Group_3__1__Impl )
            // InternalStructuredTextParser.g:8940:2: rule__Real_Value__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Real_Value__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Value__Group_3__1"


    // $ANTLR start "rule__Real_Value__Group_3__1__Impl"
    // InternalStructuredTextParser.g:8946:1: rule__Real_Value__Group_3__1__Impl : ( ruleSigned_Int ) ;
    public final void rule__Real_Value__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8950:1: ( ( ruleSigned_Int ) )
            // InternalStructuredTextParser.g:8951:1: ( ruleSigned_Int )
            {
            // InternalStructuredTextParser.g:8951:1: ( ruleSigned_Int )
            // InternalStructuredTextParser.g:8952:1: ruleSigned_Int
            {
             before(grammarAccess.getReal_ValueAccess().getSigned_IntParserRuleCall_3_1()); 
            pushFollow(FOLLOW_2);
            ruleSigned_Int();

            state._fsp--;

             after(grammarAccess.getReal_ValueAccess().getSigned_IntParserRuleCall_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Value__Group_3__1__Impl"


    // $ANTLR start "rule__Bool_Literal__Group__0"
    // InternalStructuredTextParser.g:8967:1: rule__Bool_Literal__Group__0 : rule__Bool_Literal__Group__0__Impl rule__Bool_Literal__Group__1 ;
    public final void rule__Bool_Literal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8971:1: ( rule__Bool_Literal__Group__0__Impl rule__Bool_Literal__Group__1 )
            // InternalStructuredTextParser.g:8972:2: rule__Bool_Literal__Group__0__Impl rule__Bool_Literal__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Bool_Literal__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Bool_Literal__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Bool_Literal__Group__0"


    // $ANTLR start "rule__Bool_Literal__Group__0__Impl"
    // InternalStructuredTextParser.g:8979:1: rule__Bool_Literal__Group__0__Impl : ( ( rule__Bool_Literal__Group_0__0 )? ) ;
    public final void rule__Bool_Literal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:8983:1: ( ( ( rule__Bool_Literal__Group_0__0 )? ) )
            // InternalStructuredTextParser.g:8984:1: ( ( rule__Bool_Literal__Group_0__0 )? )
            {
            // InternalStructuredTextParser.g:8984:1: ( ( rule__Bool_Literal__Group_0__0 )? )
            // InternalStructuredTextParser.g:8985:1: ( rule__Bool_Literal__Group_0__0 )?
            {
             before(grammarAccess.getBool_LiteralAccess().getGroup_0()); 
            // InternalStructuredTextParser.g:8986:1: ( rule__Bool_Literal__Group_0__0 )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==BOOL) ) {
                alt65=1;
            }
            switch (alt65) {
                case 1 :
                    // InternalStructuredTextParser.g:8986:2: rule__Bool_Literal__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Bool_Literal__Group_0__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBool_LiteralAccess().getGroup_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Bool_Literal__Group__0__Impl"


    // $ANTLR start "rule__Bool_Literal__Group__1"
    // InternalStructuredTextParser.g:8996:1: rule__Bool_Literal__Group__1 : rule__Bool_Literal__Group__1__Impl ;
    public final void rule__Bool_Literal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9000:1: ( rule__Bool_Literal__Group__1__Impl )
            // InternalStructuredTextParser.g:9001:2: rule__Bool_Literal__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Bool_Literal__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Bool_Literal__Group__1"


    // $ANTLR start "rule__Bool_Literal__Group__1__Impl"
    // InternalStructuredTextParser.g:9007:1: rule__Bool_Literal__Group__1__Impl : ( ( rule__Bool_Literal__ValueAssignment_1 ) ) ;
    public final void rule__Bool_Literal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9011:1: ( ( ( rule__Bool_Literal__ValueAssignment_1 ) ) )
            // InternalStructuredTextParser.g:9012:1: ( ( rule__Bool_Literal__ValueAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:9012:1: ( ( rule__Bool_Literal__ValueAssignment_1 ) )
            // InternalStructuredTextParser.g:9013:1: ( rule__Bool_Literal__ValueAssignment_1 )
            {
             before(grammarAccess.getBool_LiteralAccess().getValueAssignment_1()); 
            // InternalStructuredTextParser.g:9014:1: ( rule__Bool_Literal__ValueAssignment_1 )
            // InternalStructuredTextParser.g:9014:2: rule__Bool_Literal__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Bool_Literal__ValueAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBool_LiteralAccess().getValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Bool_Literal__Group__1__Impl"


    // $ANTLR start "rule__Bool_Literal__Group_0__0"
    // InternalStructuredTextParser.g:9028:1: rule__Bool_Literal__Group_0__0 : rule__Bool_Literal__Group_0__0__Impl rule__Bool_Literal__Group_0__1 ;
    public final void rule__Bool_Literal__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9032:1: ( rule__Bool_Literal__Group_0__0__Impl rule__Bool_Literal__Group_0__1 )
            // InternalStructuredTextParser.g:9033:2: rule__Bool_Literal__Group_0__0__Impl rule__Bool_Literal__Group_0__1
            {
            pushFollow(FOLLOW_62);
            rule__Bool_Literal__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Bool_Literal__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Bool_Literal__Group_0__0"


    // $ANTLR start "rule__Bool_Literal__Group_0__0__Impl"
    // InternalStructuredTextParser.g:9040:1: rule__Bool_Literal__Group_0__0__Impl : ( ( rule__Bool_Literal__TypeAssignment_0_0 ) ) ;
    public final void rule__Bool_Literal__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9044:1: ( ( ( rule__Bool_Literal__TypeAssignment_0_0 ) ) )
            // InternalStructuredTextParser.g:9045:1: ( ( rule__Bool_Literal__TypeAssignment_0_0 ) )
            {
            // InternalStructuredTextParser.g:9045:1: ( ( rule__Bool_Literal__TypeAssignment_0_0 ) )
            // InternalStructuredTextParser.g:9046:1: ( rule__Bool_Literal__TypeAssignment_0_0 )
            {
             before(grammarAccess.getBool_LiteralAccess().getTypeAssignment_0_0()); 
            // InternalStructuredTextParser.g:9047:1: ( rule__Bool_Literal__TypeAssignment_0_0 )
            // InternalStructuredTextParser.g:9047:2: rule__Bool_Literal__TypeAssignment_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Bool_Literal__TypeAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getBool_LiteralAccess().getTypeAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Bool_Literal__Group_0__0__Impl"


    // $ANTLR start "rule__Bool_Literal__Group_0__1"
    // InternalStructuredTextParser.g:9057:1: rule__Bool_Literal__Group_0__1 : rule__Bool_Literal__Group_0__1__Impl ;
    public final void rule__Bool_Literal__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9061:1: ( rule__Bool_Literal__Group_0__1__Impl )
            // InternalStructuredTextParser.g:9062:2: rule__Bool_Literal__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Bool_Literal__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Bool_Literal__Group_0__1"


    // $ANTLR start "rule__Bool_Literal__Group_0__1__Impl"
    // InternalStructuredTextParser.g:9068:1: rule__Bool_Literal__Group_0__1__Impl : ( NumberSign ) ;
    public final void rule__Bool_Literal__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9072:1: ( ( NumberSign ) )
            // InternalStructuredTextParser.g:9073:1: ( NumberSign )
            {
            // InternalStructuredTextParser.g:9073:1: ( NumberSign )
            // InternalStructuredTextParser.g:9074:1: NumberSign
            {
             before(grammarAccess.getBool_LiteralAccess().getNumberSignKeyword_0_1()); 
            match(input,NumberSign,FOLLOW_2); 
             after(grammarAccess.getBool_LiteralAccess().getNumberSignKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Bool_Literal__Group_0__1__Impl"


    // $ANTLR start "rule__Char_Literal__Group__0"
    // InternalStructuredTextParser.g:9091:1: rule__Char_Literal__Group__0 : rule__Char_Literal__Group__0__Impl rule__Char_Literal__Group__1 ;
    public final void rule__Char_Literal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9095:1: ( rule__Char_Literal__Group__0__Impl rule__Char_Literal__Group__1 )
            // InternalStructuredTextParser.g:9096:2: rule__Char_Literal__Group__0__Impl rule__Char_Literal__Group__1
            {
            pushFollow(FOLLOW_67);
            rule__Char_Literal__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Char_Literal__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Literal__Group__0"


    // $ANTLR start "rule__Char_Literal__Group__0__Impl"
    // InternalStructuredTextParser.g:9103:1: rule__Char_Literal__Group__0__Impl : ( ( rule__Char_Literal__Group_0__0 )? ) ;
    public final void rule__Char_Literal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9107:1: ( ( ( rule__Char_Literal__Group_0__0 )? ) )
            // InternalStructuredTextParser.g:9108:1: ( ( rule__Char_Literal__Group_0__0 )? )
            {
            // InternalStructuredTextParser.g:9108:1: ( ( rule__Char_Literal__Group_0__0 )? )
            // InternalStructuredTextParser.g:9109:1: ( rule__Char_Literal__Group_0__0 )?
            {
             before(grammarAccess.getChar_LiteralAccess().getGroup_0()); 
            // InternalStructuredTextParser.g:9110:1: ( rule__Char_Literal__Group_0__0 )?
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==WSTRING||LA66_0==STRING||LA66_0==WCHAR||LA66_0==CHAR) ) {
                alt66=1;
            }
            switch (alt66) {
                case 1 :
                    // InternalStructuredTextParser.g:9110:2: rule__Char_Literal__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Char_Literal__Group_0__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getChar_LiteralAccess().getGroup_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Literal__Group__0__Impl"


    // $ANTLR start "rule__Char_Literal__Group__1"
    // InternalStructuredTextParser.g:9120:1: rule__Char_Literal__Group__1 : rule__Char_Literal__Group__1__Impl ;
    public final void rule__Char_Literal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9124:1: ( rule__Char_Literal__Group__1__Impl )
            // InternalStructuredTextParser.g:9125:2: rule__Char_Literal__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Char_Literal__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Literal__Group__1"


    // $ANTLR start "rule__Char_Literal__Group__1__Impl"
    // InternalStructuredTextParser.g:9131:1: rule__Char_Literal__Group__1__Impl : ( ( rule__Char_Literal__ValueAssignment_1 ) ) ;
    public final void rule__Char_Literal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9135:1: ( ( ( rule__Char_Literal__ValueAssignment_1 ) ) )
            // InternalStructuredTextParser.g:9136:1: ( ( rule__Char_Literal__ValueAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:9136:1: ( ( rule__Char_Literal__ValueAssignment_1 ) )
            // InternalStructuredTextParser.g:9137:1: ( rule__Char_Literal__ValueAssignment_1 )
            {
             before(grammarAccess.getChar_LiteralAccess().getValueAssignment_1()); 
            // InternalStructuredTextParser.g:9138:1: ( rule__Char_Literal__ValueAssignment_1 )
            // InternalStructuredTextParser.g:9138:2: rule__Char_Literal__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Char_Literal__ValueAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getChar_LiteralAccess().getValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Literal__Group__1__Impl"


    // $ANTLR start "rule__Char_Literal__Group_0__0"
    // InternalStructuredTextParser.g:9152:1: rule__Char_Literal__Group_0__0 : rule__Char_Literal__Group_0__0__Impl rule__Char_Literal__Group_0__1 ;
    public final void rule__Char_Literal__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9156:1: ( rule__Char_Literal__Group_0__0__Impl rule__Char_Literal__Group_0__1 )
            // InternalStructuredTextParser.g:9157:2: rule__Char_Literal__Group_0__0__Impl rule__Char_Literal__Group_0__1
            {
            pushFollow(FOLLOW_68);
            rule__Char_Literal__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Char_Literal__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Literal__Group_0__0"


    // $ANTLR start "rule__Char_Literal__Group_0__0__Impl"
    // InternalStructuredTextParser.g:9164:1: rule__Char_Literal__Group_0__0__Impl : ( ( rule__Char_Literal__TypeAssignment_0_0 ) ) ;
    public final void rule__Char_Literal__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9168:1: ( ( ( rule__Char_Literal__TypeAssignment_0_0 ) ) )
            // InternalStructuredTextParser.g:9169:1: ( ( rule__Char_Literal__TypeAssignment_0_0 ) )
            {
            // InternalStructuredTextParser.g:9169:1: ( ( rule__Char_Literal__TypeAssignment_0_0 ) )
            // InternalStructuredTextParser.g:9170:1: ( rule__Char_Literal__TypeAssignment_0_0 )
            {
             before(grammarAccess.getChar_LiteralAccess().getTypeAssignment_0_0()); 
            // InternalStructuredTextParser.g:9171:1: ( rule__Char_Literal__TypeAssignment_0_0 )
            // InternalStructuredTextParser.g:9171:2: rule__Char_Literal__TypeAssignment_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Char_Literal__TypeAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getChar_LiteralAccess().getTypeAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Literal__Group_0__0__Impl"


    // $ANTLR start "rule__Char_Literal__Group_0__1"
    // InternalStructuredTextParser.g:9181:1: rule__Char_Literal__Group_0__1 : rule__Char_Literal__Group_0__1__Impl rule__Char_Literal__Group_0__2 ;
    public final void rule__Char_Literal__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9185:1: ( rule__Char_Literal__Group_0__1__Impl rule__Char_Literal__Group_0__2 )
            // InternalStructuredTextParser.g:9186:2: rule__Char_Literal__Group_0__1__Impl rule__Char_Literal__Group_0__2
            {
            pushFollow(FOLLOW_68);
            rule__Char_Literal__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Char_Literal__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Literal__Group_0__1"


    // $ANTLR start "rule__Char_Literal__Group_0__1__Impl"
    // InternalStructuredTextParser.g:9193:1: rule__Char_Literal__Group_0__1__Impl : ( ( rule__Char_Literal__LengthAssignment_0_1 )? ) ;
    public final void rule__Char_Literal__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9197:1: ( ( ( rule__Char_Literal__LengthAssignment_0_1 )? ) )
            // InternalStructuredTextParser.g:9198:1: ( ( rule__Char_Literal__LengthAssignment_0_1 )? )
            {
            // InternalStructuredTextParser.g:9198:1: ( ( rule__Char_Literal__LengthAssignment_0_1 )? )
            // InternalStructuredTextParser.g:9199:1: ( rule__Char_Literal__LengthAssignment_0_1 )?
            {
             before(grammarAccess.getChar_LiteralAccess().getLengthAssignment_0_1()); 
            // InternalStructuredTextParser.g:9200:1: ( rule__Char_Literal__LengthAssignment_0_1 )?
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( (LA67_0==RULE_UNSIGNED_INT) ) {
                alt67=1;
            }
            switch (alt67) {
                case 1 :
                    // InternalStructuredTextParser.g:9200:2: rule__Char_Literal__LengthAssignment_0_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Char_Literal__LengthAssignment_0_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getChar_LiteralAccess().getLengthAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Literal__Group_0__1__Impl"


    // $ANTLR start "rule__Char_Literal__Group_0__2"
    // InternalStructuredTextParser.g:9210:1: rule__Char_Literal__Group_0__2 : rule__Char_Literal__Group_0__2__Impl ;
    public final void rule__Char_Literal__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9214:1: ( rule__Char_Literal__Group_0__2__Impl )
            // InternalStructuredTextParser.g:9215:2: rule__Char_Literal__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Char_Literal__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Literal__Group_0__2"


    // $ANTLR start "rule__Char_Literal__Group_0__2__Impl"
    // InternalStructuredTextParser.g:9221:1: rule__Char_Literal__Group_0__2__Impl : ( NumberSign ) ;
    public final void rule__Char_Literal__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9225:1: ( ( NumberSign ) )
            // InternalStructuredTextParser.g:9226:1: ( NumberSign )
            {
            // InternalStructuredTextParser.g:9226:1: ( NumberSign )
            // InternalStructuredTextParser.g:9227:1: NumberSign
            {
             before(grammarAccess.getChar_LiteralAccess().getNumberSignKeyword_0_2()); 
            match(input,NumberSign,FOLLOW_2); 
             after(grammarAccess.getChar_LiteralAccess().getNumberSignKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Literal__Group_0__2__Impl"


    // $ANTLR start "rule__Duration__Group__0"
    // InternalStructuredTextParser.g:9246:1: rule__Duration__Group__0 : rule__Duration__Group__0__Impl rule__Duration__Group__1 ;
    public final void rule__Duration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9250:1: ( rule__Duration__Group__0__Impl rule__Duration__Group__1 )
            // InternalStructuredTextParser.g:9251:2: rule__Duration__Group__0__Impl rule__Duration__Group__1
            {
            pushFollow(FOLLOW_62);
            rule__Duration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Duration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group__0"


    // $ANTLR start "rule__Duration__Group__0__Impl"
    // InternalStructuredTextParser.g:9258:1: rule__Duration__Group__0__Impl : ( ( rule__Duration__TypeAssignment_0 ) ) ;
    public final void rule__Duration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9262:1: ( ( ( rule__Duration__TypeAssignment_0 ) ) )
            // InternalStructuredTextParser.g:9263:1: ( ( rule__Duration__TypeAssignment_0 ) )
            {
            // InternalStructuredTextParser.g:9263:1: ( ( rule__Duration__TypeAssignment_0 ) )
            // InternalStructuredTextParser.g:9264:1: ( rule__Duration__TypeAssignment_0 )
            {
             before(grammarAccess.getDurationAccess().getTypeAssignment_0()); 
            // InternalStructuredTextParser.g:9265:1: ( rule__Duration__TypeAssignment_0 )
            // InternalStructuredTextParser.g:9265:2: rule__Duration__TypeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Duration__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDurationAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group__0__Impl"


    // $ANTLR start "rule__Duration__Group__1"
    // InternalStructuredTextParser.g:9275:1: rule__Duration__Group__1 : rule__Duration__Group__1__Impl rule__Duration__Group__2 ;
    public final void rule__Duration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9279:1: ( rule__Duration__Group__1__Impl rule__Duration__Group__2 )
            // InternalStructuredTextParser.g:9280:2: rule__Duration__Group__1__Impl rule__Duration__Group__2
            {
            pushFollow(FOLLOW_63);
            rule__Duration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Duration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group__1"


    // $ANTLR start "rule__Duration__Group__1__Impl"
    // InternalStructuredTextParser.g:9287:1: rule__Duration__Group__1__Impl : ( NumberSign ) ;
    public final void rule__Duration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9291:1: ( ( NumberSign ) )
            // InternalStructuredTextParser.g:9292:1: ( NumberSign )
            {
            // InternalStructuredTextParser.g:9292:1: ( NumberSign )
            // InternalStructuredTextParser.g:9293:1: NumberSign
            {
             before(grammarAccess.getDurationAccess().getNumberSignKeyword_1()); 
            match(input,NumberSign,FOLLOW_2); 
             after(grammarAccess.getDurationAccess().getNumberSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group__1__Impl"


    // $ANTLR start "rule__Duration__Group__2"
    // InternalStructuredTextParser.g:9306:1: rule__Duration__Group__2 : rule__Duration__Group__2__Impl rule__Duration__Group__3 ;
    public final void rule__Duration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9310:1: ( rule__Duration__Group__2__Impl rule__Duration__Group__3 )
            // InternalStructuredTextParser.g:9311:2: rule__Duration__Group__2__Impl rule__Duration__Group__3
            {
            pushFollow(FOLLOW_63);
            rule__Duration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Duration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group__2"


    // $ANTLR start "rule__Duration__Group__2__Impl"
    // InternalStructuredTextParser.g:9318:1: rule__Duration__Group__2__Impl : ( ( rule__Duration__Alternatives_2 )? ) ;
    public final void rule__Duration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9322:1: ( ( ( rule__Duration__Alternatives_2 )? ) )
            // InternalStructuredTextParser.g:9323:1: ( ( rule__Duration__Alternatives_2 )? )
            {
            // InternalStructuredTextParser.g:9323:1: ( ( rule__Duration__Alternatives_2 )? )
            // InternalStructuredTextParser.g:9324:1: ( rule__Duration__Alternatives_2 )?
            {
             before(grammarAccess.getDurationAccess().getAlternatives_2()); 
            // InternalStructuredTextParser.g:9325:1: ( rule__Duration__Alternatives_2 )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( (LA68_0==PlusSign||LA68_0==HyphenMinus) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // InternalStructuredTextParser.g:9325:2: rule__Duration__Alternatives_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Duration__Alternatives_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDurationAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group__2__Impl"


    // $ANTLR start "rule__Duration__Group__3"
    // InternalStructuredTextParser.g:9335:1: rule__Duration__Group__3 : rule__Duration__Group__3__Impl rule__Duration__Group__4 ;
    public final void rule__Duration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9339:1: ( rule__Duration__Group__3__Impl rule__Duration__Group__4 )
            // InternalStructuredTextParser.g:9340:2: rule__Duration__Group__3__Impl rule__Duration__Group__4
            {
            pushFollow(FOLLOW_69);
            rule__Duration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Duration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group__3"


    // $ANTLR start "rule__Duration__Group__3__Impl"
    // InternalStructuredTextParser.g:9347:1: rule__Duration__Group__3__Impl : ( ( rule__Duration__ValueAssignment_3 ) ) ;
    public final void rule__Duration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9351:1: ( ( ( rule__Duration__ValueAssignment_3 ) ) )
            // InternalStructuredTextParser.g:9352:1: ( ( rule__Duration__ValueAssignment_3 ) )
            {
            // InternalStructuredTextParser.g:9352:1: ( ( rule__Duration__ValueAssignment_3 ) )
            // InternalStructuredTextParser.g:9353:1: ( rule__Duration__ValueAssignment_3 )
            {
             before(grammarAccess.getDurationAccess().getValueAssignment_3()); 
            // InternalStructuredTextParser.g:9354:1: ( rule__Duration__ValueAssignment_3 )
            // InternalStructuredTextParser.g:9354:2: rule__Duration__ValueAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Duration__ValueAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getDurationAccess().getValueAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group__3__Impl"


    // $ANTLR start "rule__Duration__Group__4"
    // InternalStructuredTextParser.g:9364:1: rule__Duration__Group__4 : rule__Duration__Group__4__Impl ;
    public final void rule__Duration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9368:1: ( rule__Duration__Group__4__Impl )
            // InternalStructuredTextParser.g:9369:2: rule__Duration__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Duration__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group__4"


    // $ANTLR start "rule__Duration__Group__4__Impl"
    // InternalStructuredTextParser.g:9375:1: rule__Duration__Group__4__Impl : ( ( rule__Duration__Group_4__0 )* ) ;
    public final void rule__Duration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9379:1: ( ( ( rule__Duration__Group_4__0 )* ) )
            // InternalStructuredTextParser.g:9380:1: ( ( rule__Duration__Group_4__0 )* )
            {
            // InternalStructuredTextParser.g:9380:1: ( ( rule__Duration__Group_4__0 )* )
            // InternalStructuredTextParser.g:9381:1: ( rule__Duration__Group_4__0 )*
            {
             before(grammarAccess.getDurationAccess().getGroup_4()); 
            // InternalStructuredTextParser.g:9382:1: ( rule__Duration__Group_4__0 )*
            loop69:
            do {
                int alt69=2;
                int LA69_0 = input.LA(1);

                if ( (LA69_0==KW__) ) {
                    alt69=1;
                }


                switch (alt69) {
            	case 1 :
            	    // InternalStructuredTextParser.g:9382:2: rule__Duration__Group_4__0
            	    {
            	    pushFollow(FOLLOW_70);
            	    rule__Duration__Group_4__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop69;
                }
            } while (true);

             after(grammarAccess.getDurationAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group__4__Impl"


    // $ANTLR start "rule__Duration__Group_4__0"
    // InternalStructuredTextParser.g:9402:1: rule__Duration__Group_4__0 : rule__Duration__Group_4__0__Impl rule__Duration__Group_4__1 ;
    public final void rule__Duration__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9406:1: ( rule__Duration__Group_4__0__Impl rule__Duration__Group_4__1 )
            // InternalStructuredTextParser.g:9407:2: rule__Duration__Group_4__0__Impl rule__Duration__Group_4__1
            {
            pushFollow(FOLLOW_63);
            rule__Duration__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Duration__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group_4__0"


    // $ANTLR start "rule__Duration__Group_4__0__Impl"
    // InternalStructuredTextParser.g:9414:1: rule__Duration__Group_4__0__Impl : ( KW__ ) ;
    public final void rule__Duration__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9418:1: ( ( KW__ ) )
            // InternalStructuredTextParser.g:9419:1: ( KW__ )
            {
            // InternalStructuredTextParser.g:9419:1: ( KW__ )
            // InternalStructuredTextParser.g:9420:1: KW__
            {
             before(grammarAccess.getDurationAccess().get_Keyword_4_0()); 
            match(input,KW__,FOLLOW_2); 
             after(grammarAccess.getDurationAccess().get_Keyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group_4__0__Impl"


    // $ANTLR start "rule__Duration__Group_4__1"
    // InternalStructuredTextParser.g:9433:1: rule__Duration__Group_4__1 : rule__Duration__Group_4__1__Impl ;
    public final void rule__Duration__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9437:1: ( rule__Duration__Group_4__1__Impl )
            // InternalStructuredTextParser.g:9438:2: rule__Duration__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Duration__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group_4__1"


    // $ANTLR start "rule__Duration__Group_4__1__Impl"
    // InternalStructuredTextParser.g:9444:1: rule__Duration__Group_4__1__Impl : ( ( rule__Duration__ValueAssignment_4_1 ) ) ;
    public final void rule__Duration__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9448:1: ( ( ( rule__Duration__ValueAssignment_4_1 ) ) )
            // InternalStructuredTextParser.g:9449:1: ( ( rule__Duration__ValueAssignment_4_1 ) )
            {
            // InternalStructuredTextParser.g:9449:1: ( ( rule__Duration__ValueAssignment_4_1 ) )
            // InternalStructuredTextParser.g:9450:1: ( rule__Duration__ValueAssignment_4_1 )
            {
             before(grammarAccess.getDurationAccess().getValueAssignment_4_1()); 
            // InternalStructuredTextParser.g:9451:1: ( rule__Duration__ValueAssignment_4_1 )
            // InternalStructuredTextParser.g:9451:2: rule__Duration__ValueAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Duration__ValueAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getDurationAccess().getValueAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Group_4__1__Impl"


    // $ANTLR start "rule__Duration_Value__Group__0"
    // InternalStructuredTextParser.g:9465:1: rule__Duration_Value__Group__0 : rule__Duration_Value__Group__0__Impl rule__Duration_Value__Group__1 ;
    public final void rule__Duration_Value__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9469:1: ( rule__Duration_Value__Group__0__Impl rule__Duration_Value__Group__1 )
            // InternalStructuredTextParser.g:9470:2: rule__Duration_Value__Group__0__Impl rule__Duration_Value__Group__1
            {
            pushFollow(FOLLOW_71);
            rule__Duration_Value__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Duration_Value__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration_Value__Group__0"


    // $ANTLR start "rule__Duration_Value__Group__0__Impl"
    // InternalStructuredTextParser.g:9477:1: rule__Duration_Value__Group__0__Impl : ( ( rule__Duration_Value__ValueAssignment_0 ) ) ;
    public final void rule__Duration_Value__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9481:1: ( ( ( rule__Duration_Value__ValueAssignment_0 ) ) )
            // InternalStructuredTextParser.g:9482:1: ( ( rule__Duration_Value__ValueAssignment_0 ) )
            {
            // InternalStructuredTextParser.g:9482:1: ( ( rule__Duration_Value__ValueAssignment_0 ) )
            // InternalStructuredTextParser.g:9483:1: ( rule__Duration_Value__ValueAssignment_0 )
            {
             before(grammarAccess.getDuration_ValueAccess().getValueAssignment_0()); 
            // InternalStructuredTextParser.g:9484:1: ( rule__Duration_Value__ValueAssignment_0 )
            // InternalStructuredTextParser.g:9484:2: rule__Duration_Value__ValueAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Duration_Value__ValueAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDuration_ValueAccess().getValueAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration_Value__Group__0__Impl"


    // $ANTLR start "rule__Duration_Value__Group__1"
    // InternalStructuredTextParser.g:9494:1: rule__Duration_Value__Group__1 : rule__Duration_Value__Group__1__Impl ;
    public final void rule__Duration_Value__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9498:1: ( rule__Duration_Value__Group__1__Impl )
            // InternalStructuredTextParser.g:9499:2: rule__Duration_Value__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Duration_Value__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration_Value__Group__1"


    // $ANTLR start "rule__Duration_Value__Group__1__Impl"
    // InternalStructuredTextParser.g:9505:1: rule__Duration_Value__Group__1__Impl : ( ( rule__Duration_Value__UnitAssignment_1 ) ) ;
    public final void rule__Duration_Value__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9509:1: ( ( ( rule__Duration_Value__UnitAssignment_1 ) ) )
            // InternalStructuredTextParser.g:9510:1: ( ( rule__Duration_Value__UnitAssignment_1 ) )
            {
            // InternalStructuredTextParser.g:9510:1: ( ( rule__Duration_Value__UnitAssignment_1 ) )
            // InternalStructuredTextParser.g:9511:1: ( rule__Duration_Value__UnitAssignment_1 )
            {
             before(grammarAccess.getDuration_ValueAccess().getUnitAssignment_1()); 
            // InternalStructuredTextParser.g:9512:1: ( rule__Duration_Value__UnitAssignment_1 )
            // InternalStructuredTextParser.g:9512:2: rule__Duration_Value__UnitAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Duration_Value__UnitAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDuration_ValueAccess().getUnitAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration_Value__Group__1__Impl"


    // $ANTLR start "rule__Fix_Point__Group__0"
    // InternalStructuredTextParser.g:9526:1: rule__Fix_Point__Group__0 : rule__Fix_Point__Group__0__Impl rule__Fix_Point__Group__1 ;
    public final void rule__Fix_Point__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9530:1: ( rule__Fix_Point__Group__0__Impl rule__Fix_Point__Group__1 )
            // InternalStructuredTextParser.g:9531:2: rule__Fix_Point__Group__0__Impl rule__Fix_Point__Group__1
            {
            pushFollow(FOLLOW_60);
            rule__Fix_Point__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Fix_Point__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fix_Point__Group__0"


    // $ANTLR start "rule__Fix_Point__Group__0__Impl"
    // InternalStructuredTextParser.g:9538:1: rule__Fix_Point__Group__0__Impl : ( RULE_UNSIGNED_INT ) ;
    public final void rule__Fix_Point__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9542:1: ( ( RULE_UNSIGNED_INT ) )
            // InternalStructuredTextParser.g:9543:1: ( RULE_UNSIGNED_INT )
            {
            // InternalStructuredTextParser.g:9543:1: ( RULE_UNSIGNED_INT )
            // InternalStructuredTextParser.g:9544:1: RULE_UNSIGNED_INT
            {
             before(grammarAccess.getFix_PointAccess().getUNSIGNED_INTTerminalRuleCall_0()); 
            match(input,RULE_UNSIGNED_INT,FOLLOW_2); 
             after(grammarAccess.getFix_PointAccess().getUNSIGNED_INTTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fix_Point__Group__0__Impl"


    // $ANTLR start "rule__Fix_Point__Group__1"
    // InternalStructuredTextParser.g:9555:1: rule__Fix_Point__Group__1 : rule__Fix_Point__Group__1__Impl ;
    public final void rule__Fix_Point__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9559:1: ( rule__Fix_Point__Group__1__Impl )
            // InternalStructuredTextParser.g:9560:2: rule__Fix_Point__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Fix_Point__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fix_Point__Group__1"


    // $ANTLR start "rule__Fix_Point__Group__1__Impl"
    // InternalStructuredTextParser.g:9566:1: rule__Fix_Point__Group__1__Impl : ( ( rule__Fix_Point__Group_1__0 )? ) ;
    public final void rule__Fix_Point__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9570:1: ( ( ( rule__Fix_Point__Group_1__0 )? ) )
            // InternalStructuredTextParser.g:9571:1: ( ( rule__Fix_Point__Group_1__0 )? )
            {
            // InternalStructuredTextParser.g:9571:1: ( ( rule__Fix_Point__Group_1__0 )? )
            // InternalStructuredTextParser.g:9572:1: ( rule__Fix_Point__Group_1__0 )?
            {
             before(grammarAccess.getFix_PointAccess().getGroup_1()); 
            // InternalStructuredTextParser.g:9573:1: ( rule__Fix_Point__Group_1__0 )?
            int alt70=2;
            int LA70_0 = input.LA(1);

            if ( (LA70_0==FullStop) ) {
                alt70=1;
            }
            switch (alt70) {
                case 1 :
                    // InternalStructuredTextParser.g:9573:2: rule__Fix_Point__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Fix_Point__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFix_PointAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fix_Point__Group__1__Impl"


    // $ANTLR start "rule__Fix_Point__Group_1__0"
    // InternalStructuredTextParser.g:9587:1: rule__Fix_Point__Group_1__0 : rule__Fix_Point__Group_1__0__Impl rule__Fix_Point__Group_1__1 ;
    public final void rule__Fix_Point__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9591:1: ( rule__Fix_Point__Group_1__0__Impl rule__Fix_Point__Group_1__1 )
            // InternalStructuredTextParser.g:9592:2: rule__Fix_Point__Group_1__0__Impl rule__Fix_Point__Group_1__1
            {
            pushFollow(FOLLOW_65);
            rule__Fix_Point__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Fix_Point__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fix_Point__Group_1__0"


    // $ANTLR start "rule__Fix_Point__Group_1__0__Impl"
    // InternalStructuredTextParser.g:9599:1: rule__Fix_Point__Group_1__0__Impl : ( FullStop ) ;
    public final void rule__Fix_Point__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9603:1: ( ( FullStop ) )
            // InternalStructuredTextParser.g:9604:1: ( FullStop )
            {
            // InternalStructuredTextParser.g:9604:1: ( FullStop )
            // InternalStructuredTextParser.g:9605:1: FullStop
            {
             before(grammarAccess.getFix_PointAccess().getFullStopKeyword_1_0()); 
            match(input,FullStop,FOLLOW_2); 
             after(grammarAccess.getFix_PointAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fix_Point__Group_1__0__Impl"


    // $ANTLR start "rule__Fix_Point__Group_1__1"
    // InternalStructuredTextParser.g:9618:1: rule__Fix_Point__Group_1__1 : rule__Fix_Point__Group_1__1__Impl ;
    public final void rule__Fix_Point__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9622:1: ( rule__Fix_Point__Group_1__1__Impl )
            // InternalStructuredTextParser.g:9623:2: rule__Fix_Point__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Fix_Point__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fix_Point__Group_1__1"


    // $ANTLR start "rule__Fix_Point__Group_1__1__Impl"
    // InternalStructuredTextParser.g:9629:1: rule__Fix_Point__Group_1__1__Impl : ( RULE_UNSIGNED_INT ) ;
    public final void rule__Fix_Point__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9633:1: ( ( RULE_UNSIGNED_INT ) )
            // InternalStructuredTextParser.g:9634:1: ( RULE_UNSIGNED_INT )
            {
            // InternalStructuredTextParser.g:9634:1: ( RULE_UNSIGNED_INT )
            // InternalStructuredTextParser.g:9635:1: RULE_UNSIGNED_INT
            {
             before(grammarAccess.getFix_PointAccess().getUNSIGNED_INTTerminalRuleCall_1_1()); 
            match(input,RULE_UNSIGNED_INT,FOLLOW_2); 
             after(grammarAccess.getFix_PointAccess().getUNSIGNED_INTTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fix_Point__Group_1__1__Impl"


    // $ANTLR start "rule__Time_Of_Day__Group__0"
    // InternalStructuredTextParser.g:9650:1: rule__Time_Of_Day__Group__0 : rule__Time_Of_Day__Group__0__Impl rule__Time_Of_Day__Group__1 ;
    public final void rule__Time_Of_Day__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9654:1: ( rule__Time_Of_Day__Group__0__Impl rule__Time_Of_Day__Group__1 )
            // InternalStructuredTextParser.g:9655:2: rule__Time_Of_Day__Group__0__Impl rule__Time_Of_Day__Group__1
            {
            pushFollow(FOLLOW_62);
            rule__Time_Of_Day__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Time_Of_Day__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Time_Of_Day__Group__0"


    // $ANTLR start "rule__Time_Of_Day__Group__0__Impl"
    // InternalStructuredTextParser.g:9662:1: rule__Time_Of_Day__Group__0__Impl : ( ( rule__Time_Of_Day__TypeAssignment_0 ) ) ;
    public final void rule__Time_Of_Day__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9666:1: ( ( ( rule__Time_Of_Day__TypeAssignment_0 ) ) )
            // InternalStructuredTextParser.g:9667:1: ( ( rule__Time_Of_Day__TypeAssignment_0 ) )
            {
            // InternalStructuredTextParser.g:9667:1: ( ( rule__Time_Of_Day__TypeAssignment_0 ) )
            // InternalStructuredTextParser.g:9668:1: ( rule__Time_Of_Day__TypeAssignment_0 )
            {
             before(grammarAccess.getTime_Of_DayAccess().getTypeAssignment_0()); 
            // InternalStructuredTextParser.g:9669:1: ( rule__Time_Of_Day__TypeAssignment_0 )
            // InternalStructuredTextParser.g:9669:2: rule__Time_Of_Day__TypeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Time_Of_Day__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTime_Of_DayAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Time_Of_Day__Group__0__Impl"


    // $ANTLR start "rule__Time_Of_Day__Group__1"
    // InternalStructuredTextParser.g:9679:1: rule__Time_Of_Day__Group__1 : rule__Time_Of_Day__Group__1__Impl rule__Time_Of_Day__Group__2 ;
    public final void rule__Time_Of_Day__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9683:1: ( rule__Time_Of_Day__Group__1__Impl rule__Time_Of_Day__Group__2 )
            // InternalStructuredTextParser.g:9684:2: rule__Time_Of_Day__Group__1__Impl rule__Time_Of_Day__Group__2
            {
            pushFollow(FOLLOW_65);
            rule__Time_Of_Day__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Time_Of_Day__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Time_Of_Day__Group__1"


    // $ANTLR start "rule__Time_Of_Day__Group__1__Impl"
    // InternalStructuredTextParser.g:9691:1: rule__Time_Of_Day__Group__1__Impl : ( NumberSign ) ;
    public final void rule__Time_Of_Day__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9695:1: ( ( NumberSign ) )
            // InternalStructuredTextParser.g:9696:1: ( NumberSign )
            {
            // InternalStructuredTextParser.g:9696:1: ( NumberSign )
            // InternalStructuredTextParser.g:9697:1: NumberSign
            {
             before(grammarAccess.getTime_Of_DayAccess().getNumberSignKeyword_1()); 
            match(input,NumberSign,FOLLOW_2); 
             after(grammarAccess.getTime_Of_DayAccess().getNumberSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Time_Of_Day__Group__1__Impl"


    // $ANTLR start "rule__Time_Of_Day__Group__2"
    // InternalStructuredTextParser.g:9710:1: rule__Time_Of_Day__Group__2 : rule__Time_Of_Day__Group__2__Impl ;
    public final void rule__Time_Of_Day__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9714:1: ( rule__Time_Of_Day__Group__2__Impl )
            // InternalStructuredTextParser.g:9715:2: rule__Time_Of_Day__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Time_Of_Day__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Time_Of_Day__Group__2"


    // $ANTLR start "rule__Time_Of_Day__Group__2__Impl"
    // InternalStructuredTextParser.g:9721:1: rule__Time_Of_Day__Group__2__Impl : ( ( rule__Time_Of_Day__ValueAssignment_2 ) ) ;
    public final void rule__Time_Of_Day__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9725:1: ( ( ( rule__Time_Of_Day__ValueAssignment_2 ) ) )
            // InternalStructuredTextParser.g:9726:1: ( ( rule__Time_Of_Day__ValueAssignment_2 ) )
            {
            // InternalStructuredTextParser.g:9726:1: ( ( rule__Time_Of_Day__ValueAssignment_2 ) )
            // InternalStructuredTextParser.g:9727:1: ( rule__Time_Of_Day__ValueAssignment_2 )
            {
             before(grammarAccess.getTime_Of_DayAccess().getValueAssignment_2()); 
            // InternalStructuredTextParser.g:9728:1: ( rule__Time_Of_Day__ValueAssignment_2 )
            // InternalStructuredTextParser.g:9728:2: rule__Time_Of_Day__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Time_Of_Day__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getTime_Of_DayAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Time_Of_Day__Group__2__Impl"


    // $ANTLR start "rule__Daytime__Group__0"
    // InternalStructuredTextParser.g:9744:1: rule__Daytime__Group__0 : rule__Daytime__Group__0__Impl rule__Daytime__Group__1 ;
    public final void rule__Daytime__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9748:1: ( rule__Daytime__Group__0__Impl rule__Daytime__Group__1 )
            // InternalStructuredTextParser.g:9749:2: rule__Daytime__Group__0__Impl rule__Daytime__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Daytime__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Daytime__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Daytime__Group__0"


    // $ANTLR start "rule__Daytime__Group__0__Impl"
    // InternalStructuredTextParser.g:9756:1: rule__Daytime__Group__0__Impl : ( ruleDay_Hour ) ;
    public final void rule__Daytime__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9760:1: ( ( ruleDay_Hour ) )
            // InternalStructuredTextParser.g:9761:1: ( ruleDay_Hour )
            {
            // InternalStructuredTextParser.g:9761:1: ( ruleDay_Hour )
            // InternalStructuredTextParser.g:9762:1: ruleDay_Hour
            {
             before(grammarAccess.getDaytimeAccess().getDay_HourParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleDay_Hour();

            state._fsp--;

             after(grammarAccess.getDaytimeAccess().getDay_HourParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Daytime__Group__0__Impl"


    // $ANTLR start "rule__Daytime__Group__1"
    // InternalStructuredTextParser.g:9773:1: rule__Daytime__Group__1 : rule__Daytime__Group__1__Impl rule__Daytime__Group__2 ;
    public final void rule__Daytime__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9777:1: ( rule__Daytime__Group__1__Impl rule__Daytime__Group__2 )
            // InternalStructuredTextParser.g:9778:2: rule__Daytime__Group__1__Impl rule__Daytime__Group__2
            {
            pushFollow(FOLLOW_65);
            rule__Daytime__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Daytime__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Daytime__Group__1"


    // $ANTLR start "rule__Daytime__Group__1__Impl"
    // InternalStructuredTextParser.g:9785:1: rule__Daytime__Group__1__Impl : ( Colon ) ;
    public final void rule__Daytime__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9789:1: ( ( Colon ) )
            // InternalStructuredTextParser.g:9790:1: ( Colon )
            {
            // InternalStructuredTextParser.g:9790:1: ( Colon )
            // InternalStructuredTextParser.g:9791:1: Colon
            {
             before(grammarAccess.getDaytimeAccess().getColonKeyword_1()); 
            match(input,Colon,FOLLOW_2); 
             after(grammarAccess.getDaytimeAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Daytime__Group__1__Impl"


    // $ANTLR start "rule__Daytime__Group__2"
    // InternalStructuredTextParser.g:9804:1: rule__Daytime__Group__2 : rule__Daytime__Group__2__Impl rule__Daytime__Group__3 ;
    public final void rule__Daytime__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9808:1: ( rule__Daytime__Group__2__Impl rule__Daytime__Group__3 )
            // InternalStructuredTextParser.g:9809:2: rule__Daytime__Group__2__Impl rule__Daytime__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Daytime__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Daytime__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Daytime__Group__2"


    // $ANTLR start "rule__Daytime__Group__2__Impl"
    // InternalStructuredTextParser.g:9816:1: rule__Daytime__Group__2__Impl : ( ruleDay_Minute ) ;
    public final void rule__Daytime__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9820:1: ( ( ruleDay_Minute ) )
            // InternalStructuredTextParser.g:9821:1: ( ruleDay_Minute )
            {
            // InternalStructuredTextParser.g:9821:1: ( ruleDay_Minute )
            // InternalStructuredTextParser.g:9822:1: ruleDay_Minute
            {
             before(grammarAccess.getDaytimeAccess().getDay_MinuteParserRuleCall_2()); 
            pushFollow(FOLLOW_2);
            ruleDay_Minute();

            state._fsp--;

             after(grammarAccess.getDaytimeAccess().getDay_MinuteParserRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Daytime__Group__2__Impl"


    // $ANTLR start "rule__Daytime__Group__3"
    // InternalStructuredTextParser.g:9833:1: rule__Daytime__Group__3 : rule__Daytime__Group__3__Impl rule__Daytime__Group__4 ;
    public final void rule__Daytime__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9837:1: ( rule__Daytime__Group__3__Impl rule__Daytime__Group__4 )
            // InternalStructuredTextParser.g:9838:2: rule__Daytime__Group__3__Impl rule__Daytime__Group__4
            {
            pushFollow(FOLLOW_63);
            rule__Daytime__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Daytime__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Daytime__Group__3"


    // $ANTLR start "rule__Daytime__Group__3__Impl"
    // InternalStructuredTextParser.g:9845:1: rule__Daytime__Group__3__Impl : ( Colon ) ;
    public final void rule__Daytime__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9849:1: ( ( Colon ) )
            // InternalStructuredTextParser.g:9850:1: ( Colon )
            {
            // InternalStructuredTextParser.g:9850:1: ( Colon )
            // InternalStructuredTextParser.g:9851:1: Colon
            {
             before(grammarAccess.getDaytimeAccess().getColonKeyword_3()); 
            match(input,Colon,FOLLOW_2); 
             after(grammarAccess.getDaytimeAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Daytime__Group__3__Impl"


    // $ANTLR start "rule__Daytime__Group__4"
    // InternalStructuredTextParser.g:9864:1: rule__Daytime__Group__4 : rule__Daytime__Group__4__Impl ;
    public final void rule__Daytime__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9868:1: ( rule__Daytime__Group__4__Impl )
            // InternalStructuredTextParser.g:9869:2: rule__Daytime__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Daytime__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Daytime__Group__4"


    // $ANTLR start "rule__Daytime__Group__4__Impl"
    // InternalStructuredTextParser.g:9875:1: rule__Daytime__Group__4__Impl : ( ruleDay_Second ) ;
    public final void rule__Daytime__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9879:1: ( ( ruleDay_Second ) )
            // InternalStructuredTextParser.g:9880:1: ( ruleDay_Second )
            {
            // InternalStructuredTextParser.g:9880:1: ( ruleDay_Second )
            // InternalStructuredTextParser.g:9881:1: ruleDay_Second
            {
             before(grammarAccess.getDaytimeAccess().getDay_SecondParserRuleCall_4()); 
            pushFollow(FOLLOW_2);
            ruleDay_Second();

            state._fsp--;

             after(grammarAccess.getDaytimeAccess().getDay_SecondParserRuleCall_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Daytime__Group__4__Impl"


    // $ANTLR start "rule__Date__Group__0"
    // InternalStructuredTextParser.g:9902:1: rule__Date__Group__0 : rule__Date__Group__0__Impl rule__Date__Group__1 ;
    public final void rule__Date__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9906:1: ( rule__Date__Group__0__Impl rule__Date__Group__1 )
            // InternalStructuredTextParser.g:9907:2: rule__Date__Group__0__Impl rule__Date__Group__1
            {
            pushFollow(FOLLOW_62);
            rule__Date__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Date__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date__Group__0"


    // $ANTLR start "rule__Date__Group__0__Impl"
    // InternalStructuredTextParser.g:9914:1: rule__Date__Group__0__Impl : ( ( rule__Date__TypeAssignment_0 ) ) ;
    public final void rule__Date__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9918:1: ( ( ( rule__Date__TypeAssignment_0 ) ) )
            // InternalStructuredTextParser.g:9919:1: ( ( rule__Date__TypeAssignment_0 ) )
            {
            // InternalStructuredTextParser.g:9919:1: ( ( rule__Date__TypeAssignment_0 ) )
            // InternalStructuredTextParser.g:9920:1: ( rule__Date__TypeAssignment_0 )
            {
             before(grammarAccess.getDateAccess().getTypeAssignment_0()); 
            // InternalStructuredTextParser.g:9921:1: ( rule__Date__TypeAssignment_0 )
            // InternalStructuredTextParser.g:9921:2: rule__Date__TypeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Date__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDateAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date__Group__0__Impl"


    // $ANTLR start "rule__Date__Group__1"
    // InternalStructuredTextParser.g:9931:1: rule__Date__Group__1 : rule__Date__Group__1__Impl rule__Date__Group__2 ;
    public final void rule__Date__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9935:1: ( rule__Date__Group__1__Impl rule__Date__Group__2 )
            // InternalStructuredTextParser.g:9936:2: rule__Date__Group__1__Impl rule__Date__Group__2
            {
            pushFollow(FOLLOW_65);
            rule__Date__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Date__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date__Group__1"


    // $ANTLR start "rule__Date__Group__1__Impl"
    // InternalStructuredTextParser.g:9943:1: rule__Date__Group__1__Impl : ( NumberSign ) ;
    public final void rule__Date__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9947:1: ( ( NumberSign ) )
            // InternalStructuredTextParser.g:9948:1: ( NumberSign )
            {
            // InternalStructuredTextParser.g:9948:1: ( NumberSign )
            // InternalStructuredTextParser.g:9949:1: NumberSign
            {
             before(grammarAccess.getDateAccess().getNumberSignKeyword_1()); 
            match(input,NumberSign,FOLLOW_2); 
             after(grammarAccess.getDateAccess().getNumberSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date__Group__1__Impl"


    // $ANTLR start "rule__Date__Group__2"
    // InternalStructuredTextParser.g:9962:1: rule__Date__Group__2 : rule__Date__Group__2__Impl ;
    public final void rule__Date__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9966:1: ( rule__Date__Group__2__Impl )
            // InternalStructuredTextParser.g:9967:2: rule__Date__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Date__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date__Group__2"


    // $ANTLR start "rule__Date__Group__2__Impl"
    // InternalStructuredTextParser.g:9973:1: rule__Date__Group__2__Impl : ( ( rule__Date__ValueAssignment_2 ) ) ;
    public final void rule__Date__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:9977:1: ( ( ( rule__Date__ValueAssignment_2 ) ) )
            // InternalStructuredTextParser.g:9978:1: ( ( rule__Date__ValueAssignment_2 ) )
            {
            // InternalStructuredTextParser.g:9978:1: ( ( rule__Date__ValueAssignment_2 ) )
            // InternalStructuredTextParser.g:9979:1: ( rule__Date__ValueAssignment_2 )
            {
             before(grammarAccess.getDateAccess().getValueAssignment_2()); 
            // InternalStructuredTextParser.g:9980:1: ( rule__Date__ValueAssignment_2 )
            // InternalStructuredTextParser.g:9980:2: rule__Date__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Date__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDateAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date__Group__2__Impl"


    // $ANTLR start "rule__Date_Literal__Group__0"
    // InternalStructuredTextParser.g:9996:1: rule__Date_Literal__Group__0 : rule__Date_Literal__Group__0__Impl rule__Date_Literal__Group__1 ;
    public final void rule__Date_Literal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10000:1: ( rule__Date_Literal__Group__0__Impl rule__Date_Literal__Group__1 )
            // InternalStructuredTextParser.g:10001:2: rule__Date_Literal__Group__0__Impl rule__Date_Literal__Group__1
            {
            pushFollow(FOLLOW_72);
            rule__Date_Literal__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Date_Literal__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_Literal__Group__0"


    // $ANTLR start "rule__Date_Literal__Group__0__Impl"
    // InternalStructuredTextParser.g:10008:1: rule__Date_Literal__Group__0__Impl : ( ruleYear ) ;
    public final void rule__Date_Literal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10012:1: ( ( ruleYear ) )
            // InternalStructuredTextParser.g:10013:1: ( ruleYear )
            {
            // InternalStructuredTextParser.g:10013:1: ( ruleYear )
            // InternalStructuredTextParser.g:10014:1: ruleYear
            {
             before(grammarAccess.getDate_LiteralAccess().getYearParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleYear();

            state._fsp--;

             after(grammarAccess.getDate_LiteralAccess().getYearParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_Literal__Group__0__Impl"


    // $ANTLR start "rule__Date_Literal__Group__1"
    // InternalStructuredTextParser.g:10025:1: rule__Date_Literal__Group__1 : rule__Date_Literal__Group__1__Impl rule__Date_Literal__Group__2 ;
    public final void rule__Date_Literal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10029:1: ( rule__Date_Literal__Group__1__Impl rule__Date_Literal__Group__2 )
            // InternalStructuredTextParser.g:10030:2: rule__Date_Literal__Group__1__Impl rule__Date_Literal__Group__2
            {
            pushFollow(FOLLOW_65);
            rule__Date_Literal__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Date_Literal__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_Literal__Group__1"


    // $ANTLR start "rule__Date_Literal__Group__1__Impl"
    // InternalStructuredTextParser.g:10037:1: rule__Date_Literal__Group__1__Impl : ( HyphenMinus ) ;
    public final void rule__Date_Literal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10041:1: ( ( HyphenMinus ) )
            // InternalStructuredTextParser.g:10042:1: ( HyphenMinus )
            {
            // InternalStructuredTextParser.g:10042:1: ( HyphenMinus )
            // InternalStructuredTextParser.g:10043:1: HyphenMinus
            {
             before(grammarAccess.getDate_LiteralAccess().getHyphenMinusKeyword_1()); 
            match(input,HyphenMinus,FOLLOW_2); 
             after(grammarAccess.getDate_LiteralAccess().getHyphenMinusKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_Literal__Group__1__Impl"


    // $ANTLR start "rule__Date_Literal__Group__2"
    // InternalStructuredTextParser.g:10056:1: rule__Date_Literal__Group__2 : rule__Date_Literal__Group__2__Impl rule__Date_Literal__Group__3 ;
    public final void rule__Date_Literal__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10060:1: ( rule__Date_Literal__Group__2__Impl rule__Date_Literal__Group__3 )
            // InternalStructuredTextParser.g:10061:2: rule__Date_Literal__Group__2__Impl rule__Date_Literal__Group__3
            {
            pushFollow(FOLLOW_72);
            rule__Date_Literal__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Date_Literal__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_Literal__Group__2"


    // $ANTLR start "rule__Date_Literal__Group__2__Impl"
    // InternalStructuredTextParser.g:10068:1: rule__Date_Literal__Group__2__Impl : ( ruleMonth ) ;
    public final void rule__Date_Literal__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10072:1: ( ( ruleMonth ) )
            // InternalStructuredTextParser.g:10073:1: ( ruleMonth )
            {
            // InternalStructuredTextParser.g:10073:1: ( ruleMonth )
            // InternalStructuredTextParser.g:10074:1: ruleMonth
            {
             before(grammarAccess.getDate_LiteralAccess().getMonthParserRuleCall_2()); 
            pushFollow(FOLLOW_2);
            ruleMonth();

            state._fsp--;

             after(grammarAccess.getDate_LiteralAccess().getMonthParserRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_Literal__Group__2__Impl"


    // $ANTLR start "rule__Date_Literal__Group__3"
    // InternalStructuredTextParser.g:10085:1: rule__Date_Literal__Group__3 : rule__Date_Literal__Group__3__Impl rule__Date_Literal__Group__4 ;
    public final void rule__Date_Literal__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10089:1: ( rule__Date_Literal__Group__3__Impl rule__Date_Literal__Group__4 )
            // InternalStructuredTextParser.g:10090:2: rule__Date_Literal__Group__3__Impl rule__Date_Literal__Group__4
            {
            pushFollow(FOLLOW_65);
            rule__Date_Literal__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Date_Literal__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_Literal__Group__3"


    // $ANTLR start "rule__Date_Literal__Group__3__Impl"
    // InternalStructuredTextParser.g:10097:1: rule__Date_Literal__Group__3__Impl : ( HyphenMinus ) ;
    public final void rule__Date_Literal__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10101:1: ( ( HyphenMinus ) )
            // InternalStructuredTextParser.g:10102:1: ( HyphenMinus )
            {
            // InternalStructuredTextParser.g:10102:1: ( HyphenMinus )
            // InternalStructuredTextParser.g:10103:1: HyphenMinus
            {
             before(grammarAccess.getDate_LiteralAccess().getHyphenMinusKeyword_3()); 
            match(input,HyphenMinus,FOLLOW_2); 
             after(grammarAccess.getDate_LiteralAccess().getHyphenMinusKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_Literal__Group__3__Impl"


    // $ANTLR start "rule__Date_Literal__Group__4"
    // InternalStructuredTextParser.g:10116:1: rule__Date_Literal__Group__4 : rule__Date_Literal__Group__4__Impl ;
    public final void rule__Date_Literal__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10120:1: ( rule__Date_Literal__Group__4__Impl )
            // InternalStructuredTextParser.g:10121:2: rule__Date_Literal__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Date_Literal__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_Literal__Group__4"


    // $ANTLR start "rule__Date_Literal__Group__4__Impl"
    // InternalStructuredTextParser.g:10127:1: rule__Date_Literal__Group__4__Impl : ( ruleDay ) ;
    public final void rule__Date_Literal__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10131:1: ( ( ruleDay ) )
            // InternalStructuredTextParser.g:10132:1: ( ruleDay )
            {
            // InternalStructuredTextParser.g:10132:1: ( ruleDay )
            // InternalStructuredTextParser.g:10133:1: ruleDay
            {
             before(grammarAccess.getDate_LiteralAccess().getDayParserRuleCall_4()); 
            pushFollow(FOLLOW_2);
            ruleDay();

            state._fsp--;

             after(grammarAccess.getDate_LiteralAccess().getDayParserRuleCall_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_Literal__Group__4__Impl"


    // $ANTLR start "rule__Date_And_Time__Group__0"
    // InternalStructuredTextParser.g:10154:1: rule__Date_And_Time__Group__0 : rule__Date_And_Time__Group__0__Impl rule__Date_And_Time__Group__1 ;
    public final void rule__Date_And_Time__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10158:1: ( rule__Date_And_Time__Group__0__Impl rule__Date_And_Time__Group__1 )
            // InternalStructuredTextParser.g:10159:2: rule__Date_And_Time__Group__0__Impl rule__Date_And_Time__Group__1
            {
            pushFollow(FOLLOW_62);
            rule__Date_And_Time__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Date_And_Time__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time__Group__0"


    // $ANTLR start "rule__Date_And_Time__Group__0__Impl"
    // InternalStructuredTextParser.g:10166:1: rule__Date_And_Time__Group__0__Impl : ( ( rule__Date_And_Time__TypeAssignment_0 ) ) ;
    public final void rule__Date_And_Time__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10170:1: ( ( ( rule__Date_And_Time__TypeAssignment_0 ) ) )
            // InternalStructuredTextParser.g:10171:1: ( ( rule__Date_And_Time__TypeAssignment_0 ) )
            {
            // InternalStructuredTextParser.g:10171:1: ( ( rule__Date_And_Time__TypeAssignment_0 ) )
            // InternalStructuredTextParser.g:10172:1: ( rule__Date_And_Time__TypeAssignment_0 )
            {
             before(grammarAccess.getDate_And_TimeAccess().getTypeAssignment_0()); 
            // InternalStructuredTextParser.g:10173:1: ( rule__Date_And_Time__TypeAssignment_0 )
            // InternalStructuredTextParser.g:10173:2: rule__Date_And_Time__TypeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Date_And_Time__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDate_And_TimeAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time__Group__0__Impl"


    // $ANTLR start "rule__Date_And_Time__Group__1"
    // InternalStructuredTextParser.g:10183:1: rule__Date_And_Time__Group__1 : rule__Date_And_Time__Group__1__Impl rule__Date_And_Time__Group__2 ;
    public final void rule__Date_And_Time__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10187:1: ( rule__Date_And_Time__Group__1__Impl rule__Date_And_Time__Group__2 )
            // InternalStructuredTextParser.g:10188:2: rule__Date_And_Time__Group__1__Impl rule__Date_And_Time__Group__2
            {
            pushFollow(FOLLOW_65);
            rule__Date_And_Time__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Date_And_Time__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time__Group__1"


    // $ANTLR start "rule__Date_And_Time__Group__1__Impl"
    // InternalStructuredTextParser.g:10195:1: rule__Date_And_Time__Group__1__Impl : ( NumberSign ) ;
    public final void rule__Date_And_Time__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10199:1: ( ( NumberSign ) )
            // InternalStructuredTextParser.g:10200:1: ( NumberSign )
            {
            // InternalStructuredTextParser.g:10200:1: ( NumberSign )
            // InternalStructuredTextParser.g:10201:1: NumberSign
            {
             before(grammarAccess.getDate_And_TimeAccess().getNumberSignKeyword_1()); 
            match(input,NumberSign,FOLLOW_2); 
             after(grammarAccess.getDate_And_TimeAccess().getNumberSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time__Group__1__Impl"


    // $ANTLR start "rule__Date_And_Time__Group__2"
    // InternalStructuredTextParser.g:10214:1: rule__Date_And_Time__Group__2 : rule__Date_And_Time__Group__2__Impl ;
    public final void rule__Date_And_Time__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10218:1: ( rule__Date_And_Time__Group__2__Impl )
            // InternalStructuredTextParser.g:10219:2: rule__Date_And_Time__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Date_And_Time__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time__Group__2"


    // $ANTLR start "rule__Date_And_Time__Group__2__Impl"
    // InternalStructuredTextParser.g:10225:1: rule__Date_And_Time__Group__2__Impl : ( ( rule__Date_And_Time__ValueAssignment_2 ) ) ;
    public final void rule__Date_And_Time__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10229:1: ( ( ( rule__Date_And_Time__ValueAssignment_2 ) ) )
            // InternalStructuredTextParser.g:10230:1: ( ( rule__Date_And_Time__ValueAssignment_2 ) )
            {
            // InternalStructuredTextParser.g:10230:1: ( ( rule__Date_And_Time__ValueAssignment_2 ) )
            // InternalStructuredTextParser.g:10231:1: ( rule__Date_And_Time__ValueAssignment_2 )
            {
             before(grammarAccess.getDate_And_TimeAccess().getValueAssignment_2()); 
            // InternalStructuredTextParser.g:10232:1: ( rule__Date_And_Time__ValueAssignment_2 )
            // InternalStructuredTextParser.g:10232:2: rule__Date_And_Time__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Date_And_Time__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDate_And_TimeAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time__Group__2__Impl"


    // $ANTLR start "rule__Date_And_Time_Value__Group__0"
    // InternalStructuredTextParser.g:10248:1: rule__Date_And_Time_Value__Group__0 : rule__Date_And_Time_Value__Group__0__Impl rule__Date_And_Time_Value__Group__1 ;
    public final void rule__Date_And_Time_Value__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10252:1: ( rule__Date_And_Time_Value__Group__0__Impl rule__Date_And_Time_Value__Group__1 )
            // InternalStructuredTextParser.g:10253:2: rule__Date_And_Time_Value__Group__0__Impl rule__Date_And_Time_Value__Group__1
            {
            pushFollow(FOLLOW_72);
            rule__Date_And_Time_Value__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Date_And_Time_Value__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time_Value__Group__0"


    // $ANTLR start "rule__Date_And_Time_Value__Group__0__Impl"
    // InternalStructuredTextParser.g:10260:1: rule__Date_And_Time_Value__Group__0__Impl : ( ruleDate_Literal ) ;
    public final void rule__Date_And_Time_Value__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10264:1: ( ( ruleDate_Literal ) )
            // InternalStructuredTextParser.g:10265:1: ( ruleDate_Literal )
            {
            // InternalStructuredTextParser.g:10265:1: ( ruleDate_Literal )
            // InternalStructuredTextParser.g:10266:1: ruleDate_Literal
            {
             before(grammarAccess.getDate_And_Time_ValueAccess().getDate_LiteralParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleDate_Literal();

            state._fsp--;

             after(grammarAccess.getDate_And_Time_ValueAccess().getDate_LiteralParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time_Value__Group__0__Impl"


    // $ANTLR start "rule__Date_And_Time_Value__Group__1"
    // InternalStructuredTextParser.g:10277:1: rule__Date_And_Time_Value__Group__1 : rule__Date_And_Time_Value__Group__1__Impl rule__Date_And_Time_Value__Group__2 ;
    public final void rule__Date_And_Time_Value__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10281:1: ( rule__Date_And_Time_Value__Group__1__Impl rule__Date_And_Time_Value__Group__2 )
            // InternalStructuredTextParser.g:10282:2: rule__Date_And_Time_Value__Group__1__Impl rule__Date_And_Time_Value__Group__2
            {
            pushFollow(FOLLOW_65);
            rule__Date_And_Time_Value__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Date_And_Time_Value__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time_Value__Group__1"


    // $ANTLR start "rule__Date_And_Time_Value__Group__1__Impl"
    // InternalStructuredTextParser.g:10289:1: rule__Date_And_Time_Value__Group__1__Impl : ( HyphenMinus ) ;
    public final void rule__Date_And_Time_Value__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10293:1: ( ( HyphenMinus ) )
            // InternalStructuredTextParser.g:10294:1: ( HyphenMinus )
            {
            // InternalStructuredTextParser.g:10294:1: ( HyphenMinus )
            // InternalStructuredTextParser.g:10295:1: HyphenMinus
            {
             before(grammarAccess.getDate_And_Time_ValueAccess().getHyphenMinusKeyword_1()); 
            match(input,HyphenMinus,FOLLOW_2); 
             after(grammarAccess.getDate_And_Time_ValueAccess().getHyphenMinusKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time_Value__Group__1__Impl"


    // $ANTLR start "rule__Date_And_Time_Value__Group__2"
    // InternalStructuredTextParser.g:10308:1: rule__Date_And_Time_Value__Group__2 : rule__Date_And_Time_Value__Group__2__Impl ;
    public final void rule__Date_And_Time_Value__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10312:1: ( rule__Date_And_Time_Value__Group__2__Impl )
            // InternalStructuredTextParser.g:10313:2: rule__Date_And_Time_Value__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Date_And_Time_Value__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time_Value__Group__2"


    // $ANTLR start "rule__Date_And_Time_Value__Group__2__Impl"
    // InternalStructuredTextParser.g:10319:1: rule__Date_And_Time_Value__Group__2__Impl : ( ruleDaytime ) ;
    public final void rule__Date_And_Time_Value__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10323:1: ( ( ruleDaytime ) )
            // InternalStructuredTextParser.g:10324:1: ( ruleDaytime )
            {
            // InternalStructuredTextParser.g:10324:1: ( ruleDaytime )
            // InternalStructuredTextParser.g:10325:1: ruleDaytime
            {
             before(grammarAccess.getDate_And_Time_ValueAccess().getDaytimeParserRuleCall_2()); 
            pushFollow(FOLLOW_2);
            ruleDaytime();

            state._fsp--;

             after(grammarAccess.getDate_And_Time_ValueAccess().getDaytimeParserRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time_Value__Group__2__Impl"


    // $ANTLR start "rule__StructuredTextAlgorithm__LocalVariablesAssignment_1_1_0"
    // InternalStructuredTextParser.g:10343:1: rule__StructuredTextAlgorithm__LocalVariablesAssignment_1_1_0 : ( ruleVar_Decl_Init ) ;
    public final void rule__StructuredTextAlgorithm__LocalVariablesAssignment_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10347:1: ( ( ruleVar_Decl_Init ) )
            // InternalStructuredTextParser.g:10348:1: ( ruleVar_Decl_Init )
            {
            // InternalStructuredTextParser.g:10348:1: ( ruleVar_Decl_Init )
            // InternalStructuredTextParser.g:10349:1: ruleVar_Decl_Init
            {
             before(grammarAccess.getStructuredTextAlgorithmAccess().getLocalVariablesVar_Decl_InitParserRuleCall_1_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleVar_Decl_Init();

            state._fsp--;

             after(grammarAccess.getStructuredTextAlgorithmAccess().getLocalVariablesVar_Decl_InitParserRuleCall_1_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__LocalVariablesAssignment_1_1_0"


    // $ANTLR start "rule__StructuredTextAlgorithm__StatementsAssignment_2"
    // InternalStructuredTextParser.g:10358:1: rule__StructuredTextAlgorithm__StatementsAssignment_2 : ( ruleStmt_List ) ;
    public final void rule__StructuredTextAlgorithm__StatementsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10362:1: ( ( ruleStmt_List ) )
            // InternalStructuredTextParser.g:10363:1: ( ruleStmt_List )
            {
            // InternalStructuredTextParser.g:10363:1: ( ruleStmt_List )
            // InternalStructuredTextParser.g:10364:1: ruleStmt_List
            {
             before(grammarAccess.getStructuredTextAlgorithmAccess().getStatementsStmt_ListParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleStmt_List();

            state._fsp--;

             after(grammarAccess.getStructuredTextAlgorithmAccess().getStatementsStmt_ListParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuredTextAlgorithm__StatementsAssignment_2"


    // $ANTLR start "rule__Var_Decl_Init__ConstantAssignment_1"
    // InternalStructuredTextParser.g:10373:1: rule__Var_Decl_Init__ConstantAssignment_1 : ( ( CONSTANT ) ) ;
    public final void rule__Var_Decl_Init__ConstantAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10377:1: ( ( ( CONSTANT ) ) )
            // InternalStructuredTextParser.g:10378:1: ( ( CONSTANT ) )
            {
            // InternalStructuredTextParser.g:10378:1: ( ( CONSTANT ) )
            // InternalStructuredTextParser.g:10379:1: ( CONSTANT )
            {
             before(grammarAccess.getVar_Decl_InitAccess().getConstantCONSTANTKeyword_1_0()); 
            // InternalStructuredTextParser.g:10380:1: ( CONSTANT )
            // InternalStructuredTextParser.g:10381:1: CONSTANT
            {
             before(grammarAccess.getVar_Decl_InitAccess().getConstantCONSTANTKeyword_1_0()); 
            match(input,CONSTANT,FOLLOW_2); 
             after(grammarAccess.getVar_Decl_InitAccess().getConstantCONSTANTKeyword_1_0()); 

            }

             after(grammarAccess.getVar_Decl_InitAccess().getConstantCONSTANTKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__ConstantAssignment_1"


    // $ANTLR start "rule__Var_Decl_Init__NameAssignment_2"
    // InternalStructuredTextParser.g:10396:1: rule__Var_Decl_Init__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Var_Decl_Init__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10400:1: ( ( RULE_ID ) )
            // InternalStructuredTextParser.g:10401:1: ( RULE_ID )
            {
            // InternalStructuredTextParser.g:10401:1: ( RULE_ID )
            // InternalStructuredTextParser.g:10402:1: RULE_ID
            {
             before(grammarAccess.getVar_Decl_InitAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getVar_Decl_InitAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__NameAssignment_2"


    // $ANTLR start "rule__Var_Decl_Init__TypeAssignment_4"
    // InternalStructuredTextParser.g:10411:1: rule__Var_Decl_Init__TypeAssignment_4 : ( ( ruleType_Name ) ) ;
    public final void rule__Var_Decl_Init__TypeAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10415:1: ( ( ( ruleType_Name ) ) )
            // InternalStructuredTextParser.g:10416:1: ( ( ruleType_Name ) )
            {
            // InternalStructuredTextParser.g:10416:1: ( ( ruleType_Name ) )
            // InternalStructuredTextParser.g:10417:1: ( ruleType_Name )
            {
             before(grammarAccess.getVar_Decl_InitAccess().getTypeDataTypeCrossReference_4_0()); 
            // InternalStructuredTextParser.g:10418:1: ( ruleType_Name )
            // InternalStructuredTextParser.g:10419:1: ruleType_Name
            {
             before(grammarAccess.getVar_Decl_InitAccess().getTypeDataTypeType_NameParserRuleCall_4_0_1()); 
            pushFollow(FOLLOW_2);
            ruleType_Name();

            state._fsp--;

             after(grammarAccess.getVar_Decl_InitAccess().getTypeDataTypeType_NameParserRuleCall_4_0_1()); 

            }

             after(grammarAccess.getVar_Decl_InitAccess().getTypeDataTypeCrossReference_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__TypeAssignment_4"


    // $ANTLR start "rule__Var_Decl_Init__InitialValueAssignment_5_1"
    // InternalStructuredTextParser.g:10430:1: rule__Var_Decl_Init__InitialValueAssignment_5_1 : ( ruleConstant ) ;
    public final void rule__Var_Decl_Init__InitialValueAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10434:1: ( ( ruleConstant ) )
            // InternalStructuredTextParser.g:10435:1: ( ruleConstant )
            {
            // InternalStructuredTextParser.g:10435:1: ( ruleConstant )
            // InternalStructuredTextParser.g:10436:1: ruleConstant
            {
             before(grammarAccess.getVar_Decl_InitAccess().getInitialValueConstantParserRuleCall_5_1_0()); 
            pushFollow(FOLLOW_2);
            ruleConstant();

            state._fsp--;

             after(grammarAccess.getVar_Decl_InitAccess().getInitialValueConstantParserRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Var_Decl_Init__InitialValueAssignment_5_1"


    // $ANTLR start "rule__Stmt_List__StatementsAssignment_1_0"
    // InternalStructuredTextParser.g:10445:1: rule__Stmt_List__StatementsAssignment_1_0 : ( ruleStmt ) ;
    public final void rule__Stmt_List__StatementsAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10449:1: ( ( ruleStmt ) )
            // InternalStructuredTextParser.g:10450:1: ( ruleStmt )
            {
            // InternalStructuredTextParser.g:10450:1: ( ruleStmt )
            // InternalStructuredTextParser.g:10451:1: ruleStmt
            {
             before(grammarAccess.getStmt_ListAccess().getStatementsStmtParserRuleCall_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleStmt();

            state._fsp--;

             after(grammarAccess.getStmt_ListAccess().getStatementsStmtParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stmt_List__StatementsAssignment_1_0"


    // $ANTLR start "rule__Assign_Stmt__VariableAssignment_0"
    // InternalStructuredTextParser.g:10460:1: rule__Assign_Stmt__VariableAssignment_0 : ( ruleVariable ) ;
    public final void rule__Assign_Stmt__VariableAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10464:1: ( ( ruleVariable ) )
            // InternalStructuredTextParser.g:10465:1: ( ruleVariable )
            {
            // InternalStructuredTextParser.g:10465:1: ( ruleVariable )
            // InternalStructuredTextParser.g:10466:1: ruleVariable
            {
             before(grammarAccess.getAssign_StmtAccess().getVariableVariableParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getAssign_StmtAccess().getVariableVariableParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assign_Stmt__VariableAssignment_0"


    // $ANTLR start "rule__Assign_Stmt__ExpressionAssignment_2"
    // InternalStructuredTextParser.g:10475:1: rule__Assign_Stmt__ExpressionAssignment_2 : ( ruleExpression ) ;
    public final void rule__Assign_Stmt__ExpressionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10479:1: ( ( ruleExpression ) )
            // InternalStructuredTextParser.g:10480:1: ( ruleExpression )
            {
            // InternalStructuredTextParser.g:10480:1: ( ruleExpression )
            // InternalStructuredTextParser.g:10481:1: ruleExpression
            {
             before(grammarAccess.getAssign_StmtAccess().getExpressionExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getAssign_StmtAccess().getExpressionExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assign_Stmt__ExpressionAssignment_2"


    // $ANTLR start "rule__IF_Stmt__ExpressionAssignment_1"
    // InternalStructuredTextParser.g:10490:1: rule__IF_Stmt__ExpressionAssignment_1 : ( ruleExpression ) ;
    public final void rule__IF_Stmt__ExpressionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10494:1: ( ( ruleExpression ) )
            // InternalStructuredTextParser.g:10495:1: ( ruleExpression )
            {
            // InternalStructuredTextParser.g:10495:1: ( ruleExpression )
            // InternalStructuredTextParser.g:10496:1: ruleExpression
            {
             before(grammarAccess.getIF_StmtAccess().getExpressionExpressionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getIF_StmtAccess().getExpressionExpressionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__ExpressionAssignment_1"


    // $ANTLR start "rule__IF_Stmt__StatmentsAssignment_3"
    // InternalStructuredTextParser.g:10505:1: rule__IF_Stmt__StatmentsAssignment_3 : ( ruleStmt_List ) ;
    public final void rule__IF_Stmt__StatmentsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10509:1: ( ( ruleStmt_List ) )
            // InternalStructuredTextParser.g:10510:1: ( ruleStmt_List )
            {
            // InternalStructuredTextParser.g:10510:1: ( ruleStmt_List )
            // InternalStructuredTextParser.g:10511:1: ruleStmt_List
            {
             before(grammarAccess.getIF_StmtAccess().getStatmentsStmt_ListParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleStmt_List();

            state._fsp--;

             after(grammarAccess.getIF_StmtAccess().getStatmentsStmt_ListParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__StatmentsAssignment_3"


    // $ANTLR start "rule__IF_Stmt__ElseifAssignment_4"
    // InternalStructuredTextParser.g:10520:1: rule__IF_Stmt__ElseifAssignment_4 : ( ruleELSIF_Clause ) ;
    public final void rule__IF_Stmt__ElseifAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10524:1: ( ( ruleELSIF_Clause ) )
            // InternalStructuredTextParser.g:10525:1: ( ruleELSIF_Clause )
            {
            // InternalStructuredTextParser.g:10525:1: ( ruleELSIF_Clause )
            // InternalStructuredTextParser.g:10526:1: ruleELSIF_Clause
            {
             before(grammarAccess.getIF_StmtAccess().getElseifELSIF_ClauseParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleELSIF_Clause();

            state._fsp--;

             after(grammarAccess.getIF_StmtAccess().getElseifELSIF_ClauseParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__ElseifAssignment_4"


    // $ANTLR start "rule__IF_Stmt__ElseAssignment_5"
    // InternalStructuredTextParser.g:10535:1: rule__IF_Stmt__ElseAssignment_5 : ( ruleELSE_Clause ) ;
    public final void rule__IF_Stmt__ElseAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10539:1: ( ( ruleELSE_Clause ) )
            // InternalStructuredTextParser.g:10540:1: ( ruleELSE_Clause )
            {
            // InternalStructuredTextParser.g:10540:1: ( ruleELSE_Clause )
            // InternalStructuredTextParser.g:10541:1: ruleELSE_Clause
            {
             before(grammarAccess.getIF_StmtAccess().getElseELSE_ClauseParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleELSE_Clause();

            state._fsp--;

             after(grammarAccess.getIF_StmtAccess().getElseELSE_ClauseParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IF_Stmt__ElseAssignment_5"


    // $ANTLR start "rule__ELSIF_Clause__ExpressionAssignment_1"
    // InternalStructuredTextParser.g:10550:1: rule__ELSIF_Clause__ExpressionAssignment_1 : ( ruleExpression ) ;
    public final void rule__ELSIF_Clause__ExpressionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10554:1: ( ( ruleExpression ) )
            // InternalStructuredTextParser.g:10555:1: ( ruleExpression )
            {
            // InternalStructuredTextParser.g:10555:1: ( ruleExpression )
            // InternalStructuredTextParser.g:10556:1: ruleExpression
            {
             before(grammarAccess.getELSIF_ClauseAccess().getExpressionExpressionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getELSIF_ClauseAccess().getExpressionExpressionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSIF_Clause__ExpressionAssignment_1"


    // $ANTLR start "rule__ELSIF_Clause__StatementsAssignment_3"
    // InternalStructuredTextParser.g:10565:1: rule__ELSIF_Clause__StatementsAssignment_3 : ( ruleStmt_List ) ;
    public final void rule__ELSIF_Clause__StatementsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10569:1: ( ( ruleStmt_List ) )
            // InternalStructuredTextParser.g:10570:1: ( ruleStmt_List )
            {
            // InternalStructuredTextParser.g:10570:1: ( ruleStmt_List )
            // InternalStructuredTextParser.g:10571:1: ruleStmt_List
            {
             before(grammarAccess.getELSIF_ClauseAccess().getStatementsStmt_ListParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleStmt_List();

            state._fsp--;

             after(grammarAccess.getELSIF_ClauseAccess().getStatementsStmt_ListParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSIF_Clause__StatementsAssignment_3"


    // $ANTLR start "rule__ELSE_Clause__StatementsAssignment_1"
    // InternalStructuredTextParser.g:10580:1: rule__ELSE_Clause__StatementsAssignment_1 : ( ruleStmt_List ) ;
    public final void rule__ELSE_Clause__StatementsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10584:1: ( ( ruleStmt_List ) )
            // InternalStructuredTextParser.g:10585:1: ( ruleStmt_List )
            {
            // InternalStructuredTextParser.g:10585:1: ( ruleStmt_List )
            // InternalStructuredTextParser.g:10586:1: ruleStmt_List
            {
             before(grammarAccess.getELSE_ClauseAccess().getStatementsStmt_ListParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleStmt_List();

            state._fsp--;

             after(grammarAccess.getELSE_ClauseAccess().getStatementsStmt_ListParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ELSE_Clause__StatementsAssignment_1"


    // $ANTLR start "rule__Case_Stmt__ExpressionAssignment_1"
    // InternalStructuredTextParser.g:10595:1: rule__Case_Stmt__ExpressionAssignment_1 : ( ruleExpression ) ;
    public final void rule__Case_Stmt__ExpressionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10599:1: ( ( ruleExpression ) )
            // InternalStructuredTextParser.g:10600:1: ( ruleExpression )
            {
            // InternalStructuredTextParser.g:10600:1: ( ruleExpression )
            // InternalStructuredTextParser.g:10601:1: ruleExpression
            {
             before(grammarAccess.getCase_StmtAccess().getExpressionExpressionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getCase_StmtAccess().getExpressionExpressionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__ExpressionAssignment_1"


    // $ANTLR start "rule__Case_Stmt__CaseAssignment_3"
    // InternalStructuredTextParser.g:10610:1: rule__Case_Stmt__CaseAssignment_3 : ( ruleCase_Selection ) ;
    public final void rule__Case_Stmt__CaseAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10614:1: ( ( ruleCase_Selection ) )
            // InternalStructuredTextParser.g:10615:1: ( ruleCase_Selection )
            {
            // InternalStructuredTextParser.g:10615:1: ( ruleCase_Selection )
            // InternalStructuredTextParser.g:10616:1: ruleCase_Selection
            {
             before(grammarAccess.getCase_StmtAccess().getCaseCase_SelectionParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleCase_Selection();

            state._fsp--;

             after(grammarAccess.getCase_StmtAccess().getCaseCase_SelectionParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__CaseAssignment_3"


    // $ANTLR start "rule__Case_Stmt__ElseAssignment_4"
    // InternalStructuredTextParser.g:10625:1: rule__Case_Stmt__ElseAssignment_4 : ( ruleELSE_Clause ) ;
    public final void rule__Case_Stmt__ElseAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10629:1: ( ( ruleELSE_Clause ) )
            // InternalStructuredTextParser.g:10630:1: ( ruleELSE_Clause )
            {
            // InternalStructuredTextParser.g:10630:1: ( ruleELSE_Clause )
            // InternalStructuredTextParser.g:10631:1: ruleELSE_Clause
            {
             before(grammarAccess.getCase_StmtAccess().getElseELSE_ClauseParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleELSE_Clause();

            state._fsp--;

             after(grammarAccess.getCase_StmtAccess().getElseELSE_ClauseParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Stmt__ElseAssignment_4"


    // $ANTLR start "rule__Case_Selection__CaseAssignment_0"
    // InternalStructuredTextParser.g:10640:1: rule__Case_Selection__CaseAssignment_0 : ( ruleConstant ) ;
    public final void rule__Case_Selection__CaseAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10644:1: ( ( ruleConstant ) )
            // InternalStructuredTextParser.g:10645:1: ( ruleConstant )
            {
            // InternalStructuredTextParser.g:10645:1: ( ruleConstant )
            // InternalStructuredTextParser.g:10646:1: ruleConstant
            {
             before(grammarAccess.getCase_SelectionAccess().getCaseConstantParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleConstant();

            state._fsp--;

             after(grammarAccess.getCase_SelectionAccess().getCaseConstantParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__CaseAssignment_0"


    // $ANTLR start "rule__Case_Selection__CaseAssignment_1_1"
    // InternalStructuredTextParser.g:10655:1: rule__Case_Selection__CaseAssignment_1_1 : ( ruleConstant ) ;
    public final void rule__Case_Selection__CaseAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10659:1: ( ( ruleConstant ) )
            // InternalStructuredTextParser.g:10660:1: ( ruleConstant )
            {
            // InternalStructuredTextParser.g:10660:1: ( ruleConstant )
            // InternalStructuredTextParser.g:10661:1: ruleConstant
            {
             before(grammarAccess.getCase_SelectionAccess().getCaseConstantParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleConstant();

            state._fsp--;

             after(grammarAccess.getCase_SelectionAccess().getCaseConstantParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__CaseAssignment_1_1"


    // $ANTLR start "rule__Case_Selection__StatementsAssignment_3"
    // InternalStructuredTextParser.g:10670:1: rule__Case_Selection__StatementsAssignment_3 : ( ruleStmt_List ) ;
    public final void rule__Case_Selection__StatementsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10674:1: ( ( ruleStmt_List ) )
            // InternalStructuredTextParser.g:10675:1: ( ruleStmt_List )
            {
            // InternalStructuredTextParser.g:10675:1: ( ruleStmt_List )
            // InternalStructuredTextParser.g:10676:1: ruleStmt_List
            {
             before(grammarAccess.getCase_SelectionAccess().getStatementsStmt_ListParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleStmt_List();

            state._fsp--;

             after(grammarAccess.getCase_SelectionAccess().getStatementsStmt_ListParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case_Selection__StatementsAssignment_3"


    // $ANTLR start "rule__For_Stmt__VariableAssignment_1"
    // InternalStructuredTextParser.g:10685:1: rule__For_Stmt__VariableAssignment_1 : ( ruleVariable_Primary ) ;
    public final void rule__For_Stmt__VariableAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10689:1: ( ( ruleVariable_Primary ) )
            // InternalStructuredTextParser.g:10690:1: ( ruleVariable_Primary )
            {
            // InternalStructuredTextParser.g:10690:1: ( ruleVariable_Primary )
            // InternalStructuredTextParser.g:10691:1: ruleVariable_Primary
            {
             before(grammarAccess.getFor_StmtAccess().getVariableVariable_PrimaryParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleVariable_Primary();

            state._fsp--;

             after(grammarAccess.getFor_StmtAccess().getVariableVariable_PrimaryParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__VariableAssignment_1"


    // $ANTLR start "rule__For_Stmt__FromAssignment_3"
    // InternalStructuredTextParser.g:10700:1: rule__For_Stmt__FromAssignment_3 : ( ruleExpression ) ;
    public final void rule__For_Stmt__FromAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10704:1: ( ( ruleExpression ) )
            // InternalStructuredTextParser.g:10705:1: ( ruleExpression )
            {
            // InternalStructuredTextParser.g:10705:1: ( ruleExpression )
            // InternalStructuredTextParser.g:10706:1: ruleExpression
            {
             before(grammarAccess.getFor_StmtAccess().getFromExpressionParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getFor_StmtAccess().getFromExpressionParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__FromAssignment_3"


    // $ANTLR start "rule__For_Stmt__ToAssignment_5"
    // InternalStructuredTextParser.g:10715:1: rule__For_Stmt__ToAssignment_5 : ( ruleExpression ) ;
    public final void rule__For_Stmt__ToAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10719:1: ( ( ruleExpression ) )
            // InternalStructuredTextParser.g:10720:1: ( ruleExpression )
            {
            // InternalStructuredTextParser.g:10720:1: ( ruleExpression )
            // InternalStructuredTextParser.g:10721:1: ruleExpression
            {
             before(grammarAccess.getFor_StmtAccess().getToExpressionParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getFor_StmtAccess().getToExpressionParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__ToAssignment_5"


    // $ANTLR start "rule__For_Stmt__ByAssignment_6_1"
    // InternalStructuredTextParser.g:10730:1: rule__For_Stmt__ByAssignment_6_1 : ( ruleExpression ) ;
    public final void rule__For_Stmt__ByAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10734:1: ( ( ruleExpression ) )
            // InternalStructuredTextParser.g:10735:1: ( ruleExpression )
            {
            // InternalStructuredTextParser.g:10735:1: ( ruleExpression )
            // InternalStructuredTextParser.g:10736:1: ruleExpression
            {
             before(grammarAccess.getFor_StmtAccess().getByExpressionParserRuleCall_6_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getFor_StmtAccess().getByExpressionParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__ByAssignment_6_1"


    // $ANTLR start "rule__For_Stmt__StatementsAssignment_8"
    // InternalStructuredTextParser.g:10745:1: rule__For_Stmt__StatementsAssignment_8 : ( ruleStmt_List ) ;
    public final void rule__For_Stmt__StatementsAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10749:1: ( ( ruleStmt_List ) )
            // InternalStructuredTextParser.g:10750:1: ( ruleStmt_List )
            {
            // InternalStructuredTextParser.g:10750:1: ( ruleStmt_List )
            // InternalStructuredTextParser.g:10751:1: ruleStmt_List
            {
             before(grammarAccess.getFor_StmtAccess().getStatementsStmt_ListParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleStmt_List();

            state._fsp--;

             after(grammarAccess.getFor_StmtAccess().getStatementsStmt_ListParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__For_Stmt__StatementsAssignment_8"


    // $ANTLR start "rule__While_Stmt__ExpressionAssignment_1"
    // InternalStructuredTextParser.g:10760:1: rule__While_Stmt__ExpressionAssignment_1 : ( ruleExpression ) ;
    public final void rule__While_Stmt__ExpressionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10764:1: ( ( ruleExpression ) )
            // InternalStructuredTextParser.g:10765:1: ( ruleExpression )
            {
            // InternalStructuredTextParser.g:10765:1: ( ruleExpression )
            // InternalStructuredTextParser.g:10766:1: ruleExpression
            {
             before(grammarAccess.getWhile_StmtAccess().getExpressionExpressionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getWhile_StmtAccess().getExpressionExpressionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__While_Stmt__ExpressionAssignment_1"


    // $ANTLR start "rule__While_Stmt__StatementsAssignment_3"
    // InternalStructuredTextParser.g:10775:1: rule__While_Stmt__StatementsAssignment_3 : ( ruleStmt_List ) ;
    public final void rule__While_Stmt__StatementsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10779:1: ( ( ruleStmt_List ) )
            // InternalStructuredTextParser.g:10780:1: ( ruleStmt_List )
            {
            // InternalStructuredTextParser.g:10780:1: ( ruleStmt_List )
            // InternalStructuredTextParser.g:10781:1: ruleStmt_List
            {
             before(grammarAccess.getWhile_StmtAccess().getStatementsStmt_ListParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleStmt_List();

            state._fsp--;

             after(grammarAccess.getWhile_StmtAccess().getStatementsStmt_ListParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__While_Stmt__StatementsAssignment_3"


    // $ANTLR start "rule__Repeat_Stmt__StatementsAssignment_1"
    // InternalStructuredTextParser.g:10790:1: rule__Repeat_Stmt__StatementsAssignment_1 : ( ruleStmt_List ) ;
    public final void rule__Repeat_Stmt__StatementsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10794:1: ( ( ruleStmt_List ) )
            // InternalStructuredTextParser.g:10795:1: ( ruleStmt_List )
            {
            // InternalStructuredTextParser.g:10795:1: ( ruleStmt_List )
            // InternalStructuredTextParser.g:10796:1: ruleStmt_List
            {
             before(grammarAccess.getRepeat_StmtAccess().getStatementsStmt_ListParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleStmt_List();

            state._fsp--;

             after(grammarAccess.getRepeat_StmtAccess().getStatementsStmt_ListParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repeat_Stmt__StatementsAssignment_1"


    // $ANTLR start "rule__Repeat_Stmt__ExpressionAssignment_3"
    // InternalStructuredTextParser.g:10805:1: rule__Repeat_Stmt__ExpressionAssignment_3 : ( ruleExpression ) ;
    public final void rule__Repeat_Stmt__ExpressionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10809:1: ( ( ruleExpression ) )
            // InternalStructuredTextParser.g:10810:1: ( ruleExpression )
            {
            // InternalStructuredTextParser.g:10810:1: ( ruleExpression )
            // InternalStructuredTextParser.g:10811:1: ruleExpression
            {
             before(grammarAccess.getRepeat_StmtAccess().getExpressionExpressionParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getRepeat_StmtAccess().getExpressionExpressionParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repeat_Stmt__ExpressionAssignment_3"


    // $ANTLR start "rule__Or_Expression__OperatorAssignment_1_1"
    // InternalStructuredTextParser.g:10820:1: rule__Or_Expression__OperatorAssignment_1_1 : ( ruleOr_Operator ) ;
    public final void rule__Or_Expression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10824:1: ( ( ruleOr_Operator ) )
            // InternalStructuredTextParser.g:10825:1: ( ruleOr_Operator )
            {
            // InternalStructuredTextParser.g:10825:1: ( ruleOr_Operator )
            // InternalStructuredTextParser.g:10826:1: ruleOr_Operator
            {
             before(grammarAccess.getOr_ExpressionAccess().getOperatorOr_OperatorEnumRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleOr_Operator();

            state._fsp--;

             after(grammarAccess.getOr_ExpressionAccess().getOperatorOr_OperatorEnumRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or_Expression__OperatorAssignment_1_1"


    // $ANTLR start "rule__Or_Expression__RightAssignment_1_2"
    // InternalStructuredTextParser.g:10835:1: rule__Or_Expression__RightAssignment_1_2 : ( ruleXor_Expr ) ;
    public final void rule__Or_Expression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10839:1: ( ( ruleXor_Expr ) )
            // InternalStructuredTextParser.g:10840:1: ( ruleXor_Expr )
            {
            // InternalStructuredTextParser.g:10840:1: ( ruleXor_Expr )
            // InternalStructuredTextParser.g:10841:1: ruleXor_Expr
            {
             before(grammarAccess.getOr_ExpressionAccess().getRightXor_ExprParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleXor_Expr();

            state._fsp--;

             after(grammarAccess.getOr_ExpressionAccess().getRightXor_ExprParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or_Expression__RightAssignment_1_2"


    // $ANTLR start "rule__Xor_Expr__OperatorAssignment_1_1"
    // InternalStructuredTextParser.g:10850:1: rule__Xor_Expr__OperatorAssignment_1_1 : ( ruleXor_Operator ) ;
    public final void rule__Xor_Expr__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10854:1: ( ( ruleXor_Operator ) )
            // InternalStructuredTextParser.g:10855:1: ( ruleXor_Operator )
            {
            // InternalStructuredTextParser.g:10855:1: ( ruleXor_Operator )
            // InternalStructuredTextParser.g:10856:1: ruleXor_Operator
            {
             before(grammarAccess.getXor_ExprAccess().getOperatorXor_OperatorEnumRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleXor_Operator();

            state._fsp--;

             after(grammarAccess.getXor_ExprAccess().getOperatorXor_OperatorEnumRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xor_Expr__OperatorAssignment_1_1"


    // $ANTLR start "rule__Xor_Expr__RightAssignment_1_2"
    // InternalStructuredTextParser.g:10865:1: rule__Xor_Expr__RightAssignment_1_2 : ( ruleAnd_Expr ) ;
    public final void rule__Xor_Expr__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10869:1: ( ( ruleAnd_Expr ) )
            // InternalStructuredTextParser.g:10870:1: ( ruleAnd_Expr )
            {
            // InternalStructuredTextParser.g:10870:1: ( ruleAnd_Expr )
            // InternalStructuredTextParser.g:10871:1: ruleAnd_Expr
            {
             before(grammarAccess.getXor_ExprAccess().getRightAnd_ExprParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd_Expr();

            state._fsp--;

             after(grammarAccess.getXor_ExprAccess().getRightAnd_ExprParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Xor_Expr__RightAssignment_1_2"


    // $ANTLR start "rule__And_Expr__OperatorAssignment_1_1"
    // InternalStructuredTextParser.g:10880:1: rule__And_Expr__OperatorAssignment_1_1 : ( ruleAnd_Operator ) ;
    public final void rule__And_Expr__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10884:1: ( ( ruleAnd_Operator ) )
            // InternalStructuredTextParser.g:10885:1: ( ruleAnd_Operator )
            {
            // InternalStructuredTextParser.g:10885:1: ( ruleAnd_Operator )
            // InternalStructuredTextParser.g:10886:1: ruleAnd_Operator
            {
             before(grammarAccess.getAnd_ExprAccess().getOperatorAnd_OperatorEnumRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd_Operator();

            state._fsp--;

             after(grammarAccess.getAnd_ExprAccess().getOperatorAnd_OperatorEnumRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And_Expr__OperatorAssignment_1_1"


    // $ANTLR start "rule__And_Expr__RightAssignment_1_2"
    // InternalStructuredTextParser.g:10895:1: rule__And_Expr__RightAssignment_1_2 : ( ruleCompare_Expr ) ;
    public final void rule__And_Expr__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10899:1: ( ( ruleCompare_Expr ) )
            // InternalStructuredTextParser.g:10900:1: ( ruleCompare_Expr )
            {
            // InternalStructuredTextParser.g:10900:1: ( ruleCompare_Expr )
            // InternalStructuredTextParser.g:10901:1: ruleCompare_Expr
            {
             before(grammarAccess.getAnd_ExprAccess().getRightCompare_ExprParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleCompare_Expr();

            state._fsp--;

             after(grammarAccess.getAnd_ExprAccess().getRightCompare_ExprParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And_Expr__RightAssignment_1_2"


    // $ANTLR start "rule__Compare_Expr__OperatorAssignment_1_1"
    // InternalStructuredTextParser.g:10910:1: rule__Compare_Expr__OperatorAssignment_1_1 : ( ruleCompare_Operator ) ;
    public final void rule__Compare_Expr__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10914:1: ( ( ruleCompare_Operator ) )
            // InternalStructuredTextParser.g:10915:1: ( ruleCompare_Operator )
            {
            // InternalStructuredTextParser.g:10915:1: ( ruleCompare_Operator )
            // InternalStructuredTextParser.g:10916:1: ruleCompare_Operator
            {
             before(grammarAccess.getCompare_ExprAccess().getOperatorCompare_OperatorEnumRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleCompare_Operator();

            state._fsp--;

             after(grammarAccess.getCompare_ExprAccess().getOperatorCompare_OperatorEnumRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compare_Expr__OperatorAssignment_1_1"


    // $ANTLR start "rule__Compare_Expr__RightAssignment_1_2"
    // InternalStructuredTextParser.g:10925:1: rule__Compare_Expr__RightAssignment_1_2 : ( ruleEqu_Expr ) ;
    public final void rule__Compare_Expr__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10929:1: ( ( ruleEqu_Expr ) )
            // InternalStructuredTextParser.g:10930:1: ( ruleEqu_Expr )
            {
            // InternalStructuredTextParser.g:10930:1: ( ruleEqu_Expr )
            // InternalStructuredTextParser.g:10931:1: ruleEqu_Expr
            {
             before(grammarAccess.getCompare_ExprAccess().getRightEqu_ExprParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEqu_Expr();

            state._fsp--;

             after(grammarAccess.getCompare_ExprAccess().getRightEqu_ExprParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compare_Expr__RightAssignment_1_2"


    // $ANTLR start "rule__Equ_Expr__OperatorAssignment_1_1"
    // InternalStructuredTextParser.g:10940:1: rule__Equ_Expr__OperatorAssignment_1_1 : ( ruleEqu_Operator ) ;
    public final void rule__Equ_Expr__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10944:1: ( ( ruleEqu_Operator ) )
            // InternalStructuredTextParser.g:10945:1: ( ruleEqu_Operator )
            {
            // InternalStructuredTextParser.g:10945:1: ( ruleEqu_Operator )
            // InternalStructuredTextParser.g:10946:1: ruleEqu_Operator
            {
             before(grammarAccess.getEqu_ExprAccess().getOperatorEqu_OperatorEnumRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEqu_Operator();

            state._fsp--;

             after(grammarAccess.getEqu_ExprAccess().getOperatorEqu_OperatorEnumRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equ_Expr__OperatorAssignment_1_1"


    // $ANTLR start "rule__Equ_Expr__RightAssignment_1_2"
    // InternalStructuredTextParser.g:10955:1: rule__Equ_Expr__RightAssignment_1_2 : ( ruleAdd_Expr ) ;
    public final void rule__Equ_Expr__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10959:1: ( ( ruleAdd_Expr ) )
            // InternalStructuredTextParser.g:10960:1: ( ruleAdd_Expr )
            {
            // InternalStructuredTextParser.g:10960:1: ( ruleAdd_Expr )
            // InternalStructuredTextParser.g:10961:1: ruleAdd_Expr
            {
             before(grammarAccess.getEqu_ExprAccess().getRightAdd_ExprParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAdd_Expr();

            state._fsp--;

             after(grammarAccess.getEqu_ExprAccess().getRightAdd_ExprParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Equ_Expr__RightAssignment_1_2"


    // $ANTLR start "rule__Add_Expr__OperatorAssignment_1_1"
    // InternalStructuredTextParser.g:10970:1: rule__Add_Expr__OperatorAssignment_1_1 : ( ruleAdd_Operator ) ;
    public final void rule__Add_Expr__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10974:1: ( ( ruleAdd_Operator ) )
            // InternalStructuredTextParser.g:10975:1: ( ruleAdd_Operator )
            {
            // InternalStructuredTextParser.g:10975:1: ( ruleAdd_Operator )
            // InternalStructuredTextParser.g:10976:1: ruleAdd_Operator
            {
             before(grammarAccess.getAdd_ExprAccess().getOperatorAdd_OperatorEnumRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAdd_Operator();

            state._fsp--;

             after(grammarAccess.getAdd_ExprAccess().getOperatorAdd_OperatorEnumRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add_Expr__OperatorAssignment_1_1"


    // $ANTLR start "rule__Add_Expr__RightAssignment_1_2"
    // InternalStructuredTextParser.g:10985:1: rule__Add_Expr__RightAssignment_1_2 : ( ruleTerm ) ;
    public final void rule__Add_Expr__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:10989:1: ( ( ruleTerm ) )
            // InternalStructuredTextParser.g:10990:1: ( ruleTerm )
            {
            // InternalStructuredTextParser.g:10990:1: ( ruleTerm )
            // InternalStructuredTextParser.g:10991:1: ruleTerm
            {
             before(grammarAccess.getAdd_ExprAccess().getRightTermParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTerm();

            state._fsp--;

             after(grammarAccess.getAdd_ExprAccess().getRightTermParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add_Expr__RightAssignment_1_2"


    // $ANTLR start "rule__Term__OperatorAssignment_1_1"
    // InternalStructuredTextParser.g:11000:1: rule__Term__OperatorAssignment_1_1 : ( ruleTerm_Operator ) ;
    public final void rule__Term__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11004:1: ( ( ruleTerm_Operator ) )
            // InternalStructuredTextParser.g:11005:1: ( ruleTerm_Operator )
            {
            // InternalStructuredTextParser.g:11005:1: ( ruleTerm_Operator )
            // InternalStructuredTextParser.g:11006:1: ruleTerm_Operator
            {
             before(grammarAccess.getTermAccess().getOperatorTerm_OperatorEnumRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTerm_Operator();

            state._fsp--;

             after(grammarAccess.getTermAccess().getOperatorTerm_OperatorEnumRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__OperatorAssignment_1_1"


    // $ANTLR start "rule__Term__RightAssignment_1_2"
    // InternalStructuredTextParser.g:11015:1: rule__Term__RightAssignment_1_2 : ( rulePower_Expr ) ;
    public final void rule__Term__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11019:1: ( ( rulePower_Expr ) )
            // InternalStructuredTextParser.g:11020:1: ( rulePower_Expr )
            {
            // InternalStructuredTextParser.g:11020:1: ( rulePower_Expr )
            // InternalStructuredTextParser.g:11021:1: rulePower_Expr
            {
             before(grammarAccess.getTermAccess().getRightPower_ExprParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            rulePower_Expr();

            state._fsp--;

             after(grammarAccess.getTermAccess().getRightPower_ExprParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__RightAssignment_1_2"


    // $ANTLR start "rule__Power_Expr__OperatorAssignment_1_1"
    // InternalStructuredTextParser.g:11030:1: rule__Power_Expr__OperatorAssignment_1_1 : ( rulePower_Operator ) ;
    public final void rule__Power_Expr__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11034:1: ( ( rulePower_Operator ) )
            // InternalStructuredTextParser.g:11035:1: ( rulePower_Operator )
            {
            // InternalStructuredTextParser.g:11035:1: ( rulePower_Operator )
            // InternalStructuredTextParser.g:11036:1: rulePower_Operator
            {
             before(grammarAccess.getPower_ExprAccess().getOperatorPower_OperatorEnumRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            rulePower_Operator();

            state._fsp--;

             after(grammarAccess.getPower_ExprAccess().getOperatorPower_OperatorEnumRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Power_Expr__OperatorAssignment_1_1"


    // $ANTLR start "rule__Power_Expr__RightAssignment_1_2"
    // InternalStructuredTextParser.g:11045:1: rule__Power_Expr__RightAssignment_1_2 : ( ruleUnary_Expr ) ;
    public final void rule__Power_Expr__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11049:1: ( ( ruleUnary_Expr ) )
            // InternalStructuredTextParser.g:11050:1: ( ruleUnary_Expr )
            {
            // InternalStructuredTextParser.g:11050:1: ( ruleUnary_Expr )
            // InternalStructuredTextParser.g:11051:1: ruleUnary_Expr
            {
             before(grammarAccess.getPower_ExprAccess().getRightUnary_ExprParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleUnary_Expr();

            state._fsp--;

             after(grammarAccess.getPower_ExprAccess().getRightUnary_ExprParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Power_Expr__RightAssignment_1_2"


    // $ANTLR start "rule__Unary_Expr__OperatorAssignment_0_1"
    // InternalStructuredTextParser.g:11060:1: rule__Unary_Expr__OperatorAssignment_0_1 : ( ruleUnary_Operator ) ;
    public final void rule__Unary_Expr__OperatorAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11064:1: ( ( ruleUnary_Operator ) )
            // InternalStructuredTextParser.g:11065:1: ( ruleUnary_Operator )
            {
            // InternalStructuredTextParser.g:11065:1: ( ruleUnary_Operator )
            // InternalStructuredTextParser.g:11066:1: ruleUnary_Operator
            {
             before(grammarAccess.getUnary_ExprAccess().getOperatorUnary_OperatorEnumRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleUnary_Operator();

            state._fsp--;

             after(grammarAccess.getUnary_ExprAccess().getOperatorUnary_OperatorEnumRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_Expr__OperatorAssignment_0_1"


    // $ANTLR start "rule__Unary_Expr__ExpressionAssignment_0_2"
    // InternalStructuredTextParser.g:11075:1: rule__Unary_Expr__ExpressionAssignment_0_2 : ( rulePrimary_Expr ) ;
    public final void rule__Unary_Expr__ExpressionAssignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11079:1: ( ( rulePrimary_Expr ) )
            // InternalStructuredTextParser.g:11080:1: ( rulePrimary_Expr )
            {
            // InternalStructuredTextParser.g:11080:1: ( rulePrimary_Expr )
            // InternalStructuredTextParser.g:11081:1: rulePrimary_Expr
            {
             before(grammarAccess.getUnary_ExprAccess().getExpressionPrimary_ExprParserRuleCall_0_2_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary_Expr();

            state._fsp--;

             after(grammarAccess.getUnary_ExprAccess().getExpressionPrimary_ExprParserRuleCall_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_Expr__ExpressionAssignment_0_2"


    // $ANTLR start "rule__Func_Call__FuncAssignment_0"
    // InternalStructuredTextParser.g:11090:1: rule__Func_Call__FuncAssignment_0 : ( ( rule__Func_Call__FuncAlternatives_0_0 ) ) ;
    public final void rule__Func_Call__FuncAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11094:1: ( ( ( rule__Func_Call__FuncAlternatives_0_0 ) ) )
            // InternalStructuredTextParser.g:11095:1: ( ( rule__Func_Call__FuncAlternatives_0_0 ) )
            {
            // InternalStructuredTextParser.g:11095:1: ( ( rule__Func_Call__FuncAlternatives_0_0 ) )
            // InternalStructuredTextParser.g:11096:1: ( rule__Func_Call__FuncAlternatives_0_0 )
            {
             before(grammarAccess.getFunc_CallAccess().getFuncAlternatives_0_0()); 
            // InternalStructuredTextParser.g:11097:1: ( rule__Func_Call__FuncAlternatives_0_0 )
            // InternalStructuredTextParser.g:11097:2: rule__Func_Call__FuncAlternatives_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Func_Call__FuncAlternatives_0_0();

            state._fsp--;


            }

             after(grammarAccess.getFunc_CallAccess().getFuncAlternatives_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__FuncAssignment_0"


    // $ANTLR start "rule__Func_Call__ArgsAssignment_2_0"
    // InternalStructuredTextParser.g:11106:1: rule__Func_Call__ArgsAssignment_2_0 : ( ruleParam_Assign ) ;
    public final void rule__Func_Call__ArgsAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11110:1: ( ( ruleParam_Assign ) )
            // InternalStructuredTextParser.g:11111:1: ( ruleParam_Assign )
            {
            // InternalStructuredTextParser.g:11111:1: ( ruleParam_Assign )
            // InternalStructuredTextParser.g:11112:1: ruleParam_Assign
            {
             before(grammarAccess.getFunc_CallAccess().getArgsParam_AssignParserRuleCall_2_0_0()); 
            pushFollow(FOLLOW_2);
            ruleParam_Assign();

            state._fsp--;

             after(grammarAccess.getFunc_CallAccess().getArgsParam_AssignParserRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__ArgsAssignment_2_0"


    // $ANTLR start "rule__Func_Call__ArgsAssignment_2_1_1"
    // InternalStructuredTextParser.g:11121:1: rule__Func_Call__ArgsAssignment_2_1_1 : ( ruleParam_Assign ) ;
    public final void rule__Func_Call__ArgsAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11125:1: ( ( ruleParam_Assign ) )
            // InternalStructuredTextParser.g:11126:1: ( ruleParam_Assign )
            {
            // InternalStructuredTextParser.g:11126:1: ( ruleParam_Assign )
            // InternalStructuredTextParser.g:11127:1: ruleParam_Assign
            {
             before(grammarAccess.getFunc_CallAccess().getArgsParam_AssignParserRuleCall_2_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParam_Assign();

            state._fsp--;

             after(grammarAccess.getFunc_CallAccess().getArgsParam_AssignParserRuleCall_2_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Func_Call__ArgsAssignment_2_1_1"


    // $ANTLR start "rule__Param_Assign_In__VarAssignment_0_0"
    // InternalStructuredTextParser.g:11136:1: rule__Param_Assign_In__VarAssignment_0_0 : ( RULE_ID ) ;
    public final void rule__Param_Assign_In__VarAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11140:1: ( ( RULE_ID ) )
            // InternalStructuredTextParser.g:11141:1: ( RULE_ID )
            {
            // InternalStructuredTextParser.g:11141:1: ( RULE_ID )
            // InternalStructuredTextParser.g:11142:1: RULE_ID
            {
             before(grammarAccess.getParam_Assign_InAccess().getVarIDTerminalRuleCall_0_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getParam_Assign_InAccess().getVarIDTerminalRuleCall_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_In__VarAssignment_0_0"


    // $ANTLR start "rule__Param_Assign_In__ExprAssignment_1"
    // InternalStructuredTextParser.g:11151:1: rule__Param_Assign_In__ExprAssignment_1 : ( ruleExpression ) ;
    public final void rule__Param_Assign_In__ExprAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11155:1: ( ( ruleExpression ) )
            // InternalStructuredTextParser.g:11156:1: ( ruleExpression )
            {
            // InternalStructuredTextParser.g:11156:1: ( ruleExpression )
            // InternalStructuredTextParser.g:11157:1: ruleExpression
            {
             before(grammarAccess.getParam_Assign_InAccess().getExprExpressionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getParam_Assign_InAccess().getExprExpressionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_In__ExprAssignment_1"


    // $ANTLR start "rule__Param_Assign_Out__NotAssignment_0"
    // InternalStructuredTextParser.g:11166:1: rule__Param_Assign_Out__NotAssignment_0 : ( ( NOT ) ) ;
    public final void rule__Param_Assign_Out__NotAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11170:1: ( ( ( NOT ) ) )
            // InternalStructuredTextParser.g:11171:1: ( ( NOT ) )
            {
            // InternalStructuredTextParser.g:11171:1: ( ( NOT ) )
            // InternalStructuredTextParser.g:11172:1: ( NOT )
            {
             before(grammarAccess.getParam_Assign_OutAccess().getNotNOTKeyword_0_0()); 
            // InternalStructuredTextParser.g:11173:1: ( NOT )
            // InternalStructuredTextParser.g:11174:1: NOT
            {
             before(grammarAccess.getParam_Assign_OutAccess().getNotNOTKeyword_0_0()); 
            match(input,NOT,FOLLOW_2); 
             after(grammarAccess.getParam_Assign_OutAccess().getNotNOTKeyword_0_0()); 

            }

             after(grammarAccess.getParam_Assign_OutAccess().getNotNOTKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_Out__NotAssignment_0"


    // $ANTLR start "rule__Param_Assign_Out__VarAssignment_1"
    // InternalStructuredTextParser.g:11189:1: rule__Param_Assign_Out__VarAssignment_1 : ( RULE_ID ) ;
    public final void rule__Param_Assign_Out__VarAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11193:1: ( ( RULE_ID ) )
            // InternalStructuredTextParser.g:11194:1: ( RULE_ID )
            {
            // InternalStructuredTextParser.g:11194:1: ( RULE_ID )
            // InternalStructuredTextParser.g:11195:1: RULE_ID
            {
             before(grammarAccess.getParam_Assign_OutAccess().getVarIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getParam_Assign_OutAccess().getVarIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_Out__VarAssignment_1"


    // $ANTLR start "rule__Param_Assign_Out__ExprAssignment_3"
    // InternalStructuredTextParser.g:11204:1: rule__Param_Assign_Out__ExprAssignment_3 : ( ruleVariable ) ;
    public final void rule__Param_Assign_Out__ExprAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11208:1: ( ( ruleVariable ) )
            // InternalStructuredTextParser.g:11209:1: ( ruleVariable )
            {
            // InternalStructuredTextParser.g:11209:1: ( ruleVariable )
            // InternalStructuredTextParser.g:11210:1: ruleVariable
            {
             before(grammarAccess.getParam_Assign_OutAccess().getExprVariableParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getParam_Assign_OutAccess().getExprVariableParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Param_Assign_Out__ExprAssignment_3"


    // $ANTLR start "rule__Variable_Subscript__IndexAssignment_1_2"
    // InternalStructuredTextParser.g:11219:1: rule__Variable_Subscript__IndexAssignment_1_2 : ( ruleExpression ) ;
    public final void rule__Variable_Subscript__IndexAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11223:1: ( ( ruleExpression ) )
            // InternalStructuredTextParser.g:11224:1: ( ruleExpression )
            {
            // InternalStructuredTextParser.g:11224:1: ( ruleExpression )
            // InternalStructuredTextParser.g:11225:1: ruleExpression
            {
             before(grammarAccess.getVariable_SubscriptAccess().getIndexExpressionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getVariable_SubscriptAccess().getIndexExpressionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__IndexAssignment_1_2"


    // $ANTLR start "rule__Variable_Subscript__IndexAssignment_1_3_1"
    // InternalStructuredTextParser.g:11234:1: rule__Variable_Subscript__IndexAssignment_1_3_1 : ( ruleExpression ) ;
    public final void rule__Variable_Subscript__IndexAssignment_1_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11238:1: ( ( ruleExpression ) )
            // InternalStructuredTextParser.g:11239:1: ( ruleExpression )
            {
            // InternalStructuredTextParser.g:11239:1: ( ruleExpression )
            // InternalStructuredTextParser.g:11240:1: ruleExpression
            {
             before(grammarAccess.getVariable_SubscriptAccess().getIndexExpressionParserRuleCall_1_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getVariable_SubscriptAccess().getIndexExpressionParserRuleCall_1_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Subscript__IndexAssignment_1_3_1"


    // $ANTLR start "rule__Variable_Adapter__AdapterAssignment_1"
    // InternalStructuredTextParser.g:11249:1: rule__Variable_Adapter__AdapterAssignment_1 : ( ( ruleAdapter_Name ) ) ;
    public final void rule__Variable_Adapter__AdapterAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11253:1: ( ( ( ruleAdapter_Name ) ) )
            // InternalStructuredTextParser.g:11254:1: ( ( ruleAdapter_Name ) )
            {
            // InternalStructuredTextParser.g:11254:1: ( ( ruleAdapter_Name ) )
            // InternalStructuredTextParser.g:11255:1: ( ruleAdapter_Name )
            {
             before(grammarAccess.getVariable_AdapterAccess().getAdapterAdapterDeclarationCrossReference_1_0()); 
            // InternalStructuredTextParser.g:11256:1: ( ruleAdapter_Name )
            // InternalStructuredTextParser.g:11257:1: ruleAdapter_Name
            {
             before(grammarAccess.getVariable_AdapterAccess().getAdapterAdapterDeclarationAdapter_NameParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleAdapter_Name();

            state._fsp--;

             after(grammarAccess.getVariable_AdapterAccess().getAdapterAdapterDeclarationAdapter_NameParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getVariable_AdapterAccess().getAdapterAdapterDeclarationCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Adapter__AdapterAssignment_1"


    // $ANTLR start "rule__Variable_Adapter__VarAssignment_3"
    // InternalStructuredTextParser.g:11268:1: rule__Variable_Adapter__VarAssignment_3 : ( ( ruleVariable_Name ) ) ;
    public final void rule__Variable_Adapter__VarAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11272:1: ( ( ( ruleVariable_Name ) ) )
            // InternalStructuredTextParser.g:11273:1: ( ( ruleVariable_Name ) )
            {
            // InternalStructuredTextParser.g:11273:1: ( ( ruleVariable_Name ) )
            // InternalStructuredTextParser.g:11274:1: ( ruleVariable_Name )
            {
             before(grammarAccess.getVariable_AdapterAccess().getVarVarDeclarationCrossReference_3_0()); 
            // InternalStructuredTextParser.g:11275:1: ( ruleVariable_Name )
            // InternalStructuredTextParser.g:11276:1: ruleVariable_Name
            {
             before(grammarAccess.getVariable_AdapterAccess().getVarVarDeclarationVariable_NameParserRuleCall_3_0_1()); 
            pushFollow(FOLLOW_2);
            ruleVariable_Name();

            state._fsp--;

             after(grammarAccess.getVariable_AdapterAccess().getVarVarDeclarationVariable_NameParserRuleCall_3_0_1()); 

            }

             after(grammarAccess.getVariable_AdapterAccess().getVarVarDeclarationCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Adapter__VarAssignment_3"


    // $ANTLR start "rule__Variable_Primary__VarAssignment"
    // InternalStructuredTextParser.g:11287:1: rule__Variable_Primary__VarAssignment : ( ( ruleVariable_Name ) ) ;
    public final void rule__Variable_Primary__VarAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11291:1: ( ( ( ruleVariable_Name ) ) )
            // InternalStructuredTextParser.g:11292:1: ( ( ruleVariable_Name ) )
            {
            // InternalStructuredTextParser.g:11292:1: ( ( ruleVariable_Name ) )
            // InternalStructuredTextParser.g:11293:1: ( ruleVariable_Name )
            {
             before(grammarAccess.getVariable_PrimaryAccess().getVarVarDeclarationCrossReference_0()); 
            // InternalStructuredTextParser.g:11294:1: ( ruleVariable_Name )
            // InternalStructuredTextParser.g:11295:1: ruleVariable_Name
            {
             before(grammarAccess.getVariable_PrimaryAccess().getVarVarDeclarationVariable_NameParserRuleCall_0_1()); 
            pushFollow(FOLLOW_2);
            ruleVariable_Name();

            state._fsp--;

             after(grammarAccess.getVariable_PrimaryAccess().getVarVarDeclarationVariable_NameParserRuleCall_0_1()); 

            }

             after(grammarAccess.getVariable_PrimaryAccess().getVarVarDeclarationCrossReference_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable_Primary__VarAssignment"


    // $ANTLR start "rule__Int_Literal__TypeAssignment_0_0"
    // InternalStructuredTextParser.g:11306:1: rule__Int_Literal__TypeAssignment_0_0 : ( ruleInt_Type_Name ) ;
    public final void rule__Int_Literal__TypeAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11310:1: ( ( ruleInt_Type_Name ) )
            // InternalStructuredTextParser.g:11311:1: ( ruleInt_Type_Name )
            {
            // InternalStructuredTextParser.g:11311:1: ( ruleInt_Type_Name )
            // InternalStructuredTextParser.g:11312:1: ruleInt_Type_Name
            {
             before(grammarAccess.getInt_LiteralAccess().getTypeInt_Type_NameEnumRuleCall_0_0_0()); 
            pushFollow(FOLLOW_2);
            ruleInt_Type_Name();

            state._fsp--;

             after(grammarAccess.getInt_LiteralAccess().getTypeInt_Type_NameEnumRuleCall_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Int_Literal__TypeAssignment_0_0"


    // $ANTLR start "rule__Int_Literal__ValueAssignment_1"
    // InternalStructuredTextParser.g:11321:1: rule__Int_Literal__ValueAssignment_1 : ( ( rule__Int_Literal__ValueAlternatives_1_0 ) ) ;
    public final void rule__Int_Literal__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11325:1: ( ( ( rule__Int_Literal__ValueAlternatives_1_0 ) ) )
            // InternalStructuredTextParser.g:11326:1: ( ( rule__Int_Literal__ValueAlternatives_1_0 ) )
            {
            // InternalStructuredTextParser.g:11326:1: ( ( rule__Int_Literal__ValueAlternatives_1_0 ) )
            // InternalStructuredTextParser.g:11327:1: ( rule__Int_Literal__ValueAlternatives_1_0 )
            {
             before(grammarAccess.getInt_LiteralAccess().getValueAlternatives_1_0()); 
            // InternalStructuredTextParser.g:11328:1: ( rule__Int_Literal__ValueAlternatives_1_0 )
            // InternalStructuredTextParser.g:11328:2: rule__Int_Literal__ValueAlternatives_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Int_Literal__ValueAlternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getInt_LiteralAccess().getValueAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Int_Literal__ValueAssignment_1"


    // $ANTLR start "rule__Real_Literal__TypeAssignment_0_0"
    // InternalStructuredTextParser.g:11337:1: rule__Real_Literal__TypeAssignment_0_0 : ( ruleReal_Type_Name ) ;
    public final void rule__Real_Literal__TypeAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11341:1: ( ( ruleReal_Type_Name ) )
            // InternalStructuredTextParser.g:11342:1: ( ruleReal_Type_Name )
            {
            // InternalStructuredTextParser.g:11342:1: ( ruleReal_Type_Name )
            // InternalStructuredTextParser.g:11343:1: ruleReal_Type_Name
            {
             before(grammarAccess.getReal_LiteralAccess().getTypeReal_Type_NameEnumRuleCall_0_0_0()); 
            pushFollow(FOLLOW_2);
            ruleReal_Type_Name();

            state._fsp--;

             after(grammarAccess.getReal_LiteralAccess().getTypeReal_Type_NameEnumRuleCall_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Literal__TypeAssignment_0_0"


    // $ANTLR start "rule__Real_Literal__ValueAssignment_1"
    // InternalStructuredTextParser.g:11352:1: rule__Real_Literal__ValueAssignment_1 : ( ruleReal_Value ) ;
    public final void rule__Real_Literal__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11356:1: ( ( ruleReal_Value ) )
            // InternalStructuredTextParser.g:11357:1: ( ruleReal_Value )
            {
            // InternalStructuredTextParser.g:11357:1: ( ruleReal_Value )
            // InternalStructuredTextParser.g:11358:1: ruleReal_Value
            {
             before(grammarAccess.getReal_LiteralAccess().getValueReal_ValueParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleReal_Value();

            state._fsp--;

             after(grammarAccess.getReal_LiteralAccess().getValueReal_ValueParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_Literal__ValueAssignment_1"


    // $ANTLR start "rule__Bool_Literal__TypeAssignment_0_0"
    // InternalStructuredTextParser.g:11367:1: rule__Bool_Literal__TypeAssignment_0_0 : ( ruleBool_Type_Name ) ;
    public final void rule__Bool_Literal__TypeAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11371:1: ( ( ruleBool_Type_Name ) )
            // InternalStructuredTextParser.g:11372:1: ( ruleBool_Type_Name )
            {
            // InternalStructuredTextParser.g:11372:1: ( ruleBool_Type_Name )
            // InternalStructuredTextParser.g:11373:1: ruleBool_Type_Name
            {
             before(grammarAccess.getBool_LiteralAccess().getTypeBool_Type_NameEnumRuleCall_0_0_0()); 
            pushFollow(FOLLOW_2);
            ruleBool_Type_Name();

            state._fsp--;

             after(grammarAccess.getBool_LiteralAccess().getTypeBool_Type_NameEnumRuleCall_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Bool_Literal__TypeAssignment_0_0"


    // $ANTLR start "rule__Bool_Literal__ValueAssignment_1"
    // InternalStructuredTextParser.g:11382:1: rule__Bool_Literal__ValueAssignment_1 : ( ruleBool_Value ) ;
    public final void rule__Bool_Literal__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11386:1: ( ( ruleBool_Value ) )
            // InternalStructuredTextParser.g:11387:1: ( ruleBool_Value )
            {
            // InternalStructuredTextParser.g:11387:1: ( ruleBool_Value )
            // InternalStructuredTextParser.g:11388:1: ruleBool_Value
            {
             before(grammarAccess.getBool_LiteralAccess().getValueBool_ValueParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBool_Value();

            state._fsp--;

             after(grammarAccess.getBool_LiteralAccess().getValueBool_ValueParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Bool_Literal__ValueAssignment_1"


    // $ANTLR start "rule__Char_Literal__TypeAssignment_0_0"
    // InternalStructuredTextParser.g:11397:1: rule__Char_Literal__TypeAssignment_0_0 : ( ruleString_Type_Name ) ;
    public final void rule__Char_Literal__TypeAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11401:1: ( ( ruleString_Type_Name ) )
            // InternalStructuredTextParser.g:11402:1: ( ruleString_Type_Name )
            {
            // InternalStructuredTextParser.g:11402:1: ( ruleString_Type_Name )
            // InternalStructuredTextParser.g:11403:1: ruleString_Type_Name
            {
             before(grammarAccess.getChar_LiteralAccess().getTypeString_Type_NameEnumRuleCall_0_0_0()); 
            pushFollow(FOLLOW_2);
            ruleString_Type_Name();

            state._fsp--;

             after(grammarAccess.getChar_LiteralAccess().getTypeString_Type_NameEnumRuleCall_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Literal__TypeAssignment_0_0"


    // $ANTLR start "rule__Char_Literal__LengthAssignment_0_1"
    // InternalStructuredTextParser.g:11412:1: rule__Char_Literal__LengthAssignment_0_1 : ( RULE_UNSIGNED_INT ) ;
    public final void rule__Char_Literal__LengthAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11416:1: ( ( RULE_UNSIGNED_INT ) )
            // InternalStructuredTextParser.g:11417:1: ( RULE_UNSIGNED_INT )
            {
            // InternalStructuredTextParser.g:11417:1: ( RULE_UNSIGNED_INT )
            // InternalStructuredTextParser.g:11418:1: RULE_UNSIGNED_INT
            {
             before(grammarAccess.getChar_LiteralAccess().getLengthUNSIGNED_INTTerminalRuleCall_0_1_0()); 
            match(input,RULE_UNSIGNED_INT,FOLLOW_2); 
             after(grammarAccess.getChar_LiteralAccess().getLengthUNSIGNED_INTTerminalRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Literal__LengthAssignment_0_1"


    // $ANTLR start "rule__Char_Literal__ValueAssignment_1"
    // InternalStructuredTextParser.g:11427:1: rule__Char_Literal__ValueAssignment_1 : ( ruleChar_Str ) ;
    public final void rule__Char_Literal__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11431:1: ( ( ruleChar_Str ) )
            // InternalStructuredTextParser.g:11432:1: ( ruleChar_Str )
            {
            // InternalStructuredTextParser.g:11432:1: ( ruleChar_Str )
            // InternalStructuredTextParser.g:11433:1: ruleChar_Str
            {
             before(grammarAccess.getChar_LiteralAccess().getValueChar_StrParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleChar_Str();

            state._fsp--;

             after(grammarAccess.getChar_LiteralAccess().getValueChar_StrParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Char_Literal__ValueAssignment_1"


    // $ANTLR start "rule__Duration__TypeAssignment_0"
    // InternalStructuredTextParser.g:11442:1: rule__Duration__TypeAssignment_0 : ( ruleTime_Type_Name ) ;
    public final void rule__Duration__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11446:1: ( ( ruleTime_Type_Name ) )
            // InternalStructuredTextParser.g:11447:1: ( ruleTime_Type_Name )
            {
            // InternalStructuredTextParser.g:11447:1: ( ruleTime_Type_Name )
            // InternalStructuredTextParser.g:11448:1: ruleTime_Type_Name
            {
             before(grammarAccess.getDurationAccess().getTypeTime_Type_NameEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleTime_Type_Name();

            state._fsp--;

             after(grammarAccess.getDurationAccess().getTypeTime_Type_NameEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__TypeAssignment_0"


    // $ANTLR start "rule__Duration__NegativeAssignment_2_1"
    // InternalStructuredTextParser.g:11457:1: rule__Duration__NegativeAssignment_2_1 : ( ( HyphenMinus ) ) ;
    public final void rule__Duration__NegativeAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11461:1: ( ( ( HyphenMinus ) ) )
            // InternalStructuredTextParser.g:11462:1: ( ( HyphenMinus ) )
            {
            // InternalStructuredTextParser.g:11462:1: ( ( HyphenMinus ) )
            // InternalStructuredTextParser.g:11463:1: ( HyphenMinus )
            {
             before(grammarAccess.getDurationAccess().getNegativeHyphenMinusKeyword_2_1_0()); 
            // InternalStructuredTextParser.g:11464:1: ( HyphenMinus )
            // InternalStructuredTextParser.g:11465:1: HyphenMinus
            {
             before(grammarAccess.getDurationAccess().getNegativeHyphenMinusKeyword_2_1_0()); 
            match(input,HyphenMinus,FOLLOW_2); 
             after(grammarAccess.getDurationAccess().getNegativeHyphenMinusKeyword_2_1_0()); 

            }

             after(grammarAccess.getDurationAccess().getNegativeHyphenMinusKeyword_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__NegativeAssignment_2_1"


    // $ANTLR start "rule__Duration__ValueAssignment_3"
    // InternalStructuredTextParser.g:11480:1: rule__Duration__ValueAssignment_3 : ( ruleDuration_Value ) ;
    public final void rule__Duration__ValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11484:1: ( ( ruleDuration_Value ) )
            // InternalStructuredTextParser.g:11485:1: ( ruleDuration_Value )
            {
            // InternalStructuredTextParser.g:11485:1: ( ruleDuration_Value )
            // InternalStructuredTextParser.g:11486:1: ruleDuration_Value
            {
             before(grammarAccess.getDurationAccess().getValueDuration_ValueParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleDuration_Value();

            state._fsp--;

             after(grammarAccess.getDurationAccess().getValueDuration_ValueParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__ValueAssignment_3"


    // $ANTLR start "rule__Duration__ValueAssignment_4_1"
    // InternalStructuredTextParser.g:11495:1: rule__Duration__ValueAssignment_4_1 : ( ruleDuration_Value ) ;
    public final void rule__Duration__ValueAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11499:1: ( ( ruleDuration_Value ) )
            // InternalStructuredTextParser.g:11500:1: ( ruleDuration_Value )
            {
            // InternalStructuredTextParser.g:11500:1: ( ruleDuration_Value )
            // InternalStructuredTextParser.g:11501:1: ruleDuration_Value
            {
             before(grammarAccess.getDurationAccess().getValueDuration_ValueParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDuration_Value();

            state._fsp--;

             after(grammarAccess.getDurationAccess().getValueDuration_ValueParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__ValueAssignment_4_1"


    // $ANTLR start "rule__Duration_Value__ValueAssignment_0"
    // InternalStructuredTextParser.g:11510:1: rule__Duration_Value__ValueAssignment_0 : ( ruleFix_Point ) ;
    public final void rule__Duration_Value__ValueAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11514:1: ( ( ruleFix_Point ) )
            // InternalStructuredTextParser.g:11515:1: ( ruleFix_Point )
            {
            // InternalStructuredTextParser.g:11515:1: ( ruleFix_Point )
            // InternalStructuredTextParser.g:11516:1: ruleFix_Point
            {
             before(grammarAccess.getDuration_ValueAccess().getValueFix_PointParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleFix_Point();

            state._fsp--;

             after(grammarAccess.getDuration_ValueAccess().getValueFix_PointParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration_Value__ValueAssignment_0"


    // $ANTLR start "rule__Duration_Value__UnitAssignment_1"
    // InternalStructuredTextParser.g:11525:1: rule__Duration_Value__UnitAssignment_1 : ( ruleDuration_Unit ) ;
    public final void rule__Duration_Value__UnitAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11529:1: ( ( ruleDuration_Unit ) )
            // InternalStructuredTextParser.g:11530:1: ( ruleDuration_Unit )
            {
            // InternalStructuredTextParser.g:11530:1: ( ruleDuration_Unit )
            // InternalStructuredTextParser.g:11531:1: ruleDuration_Unit
            {
             before(grammarAccess.getDuration_ValueAccess().getUnitDuration_UnitEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDuration_Unit();

            state._fsp--;

             after(grammarAccess.getDuration_ValueAccess().getUnitDuration_UnitEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration_Value__UnitAssignment_1"


    // $ANTLR start "rule__Time_Of_Day__TypeAssignment_0"
    // InternalStructuredTextParser.g:11540:1: rule__Time_Of_Day__TypeAssignment_0 : ( ruleTod_Type_Name ) ;
    public final void rule__Time_Of_Day__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11544:1: ( ( ruleTod_Type_Name ) )
            // InternalStructuredTextParser.g:11545:1: ( ruleTod_Type_Name )
            {
            // InternalStructuredTextParser.g:11545:1: ( ruleTod_Type_Name )
            // InternalStructuredTextParser.g:11546:1: ruleTod_Type_Name
            {
             before(grammarAccess.getTime_Of_DayAccess().getTypeTod_Type_NameEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleTod_Type_Name();

            state._fsp--;

             after(grammarAccess.getTime_Of_DayAccess().getTypeTod_Type_NameEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Time_Of_Day__TypeAssignment_0"


    // $ANTLR start "rule__Time_Of_Day__ValueAssignment_2"
    // InternalStructuredTextParser.g:11555:1: rule__Time_Of_Day__ValueAssignment_2 : ( ruleDaytime ) ;
    public final void rule__Time_Of_Day__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11559:1: ( ( ruleDaytime ) )
            // InternalStructuredTextParser.g:11560:1: ( ruleDaytime )
            {
            // InternalStructuredTextParser.g:11560:1: ( ruleDaytime )
            // InternalStructuredTextParser.g:11561:1: ruleDaytime
            {
             before(grammarAccess.getTime_Of_DayAccess().getValueDaytimeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleDaytime();

            state._fsp--;

             after(grammarAccess.getTime_Of_DayAccess().getValueDaytimeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Time_Of_Day__ValueAssignment_2"


    // $ANTLR start "rule__Date__TypeAssignment_0"
    // InternalStructuredTextParser.g:11570:1: rule__Date__TypeAssignment_0 : ( ruleDate_Type_Name ) ;
    public final void rule__Date__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11574:1: ( ( ruleDate_Type_Name ) )
            // InternalStructuredTextParser.g:11575:1: ( ruleDate_Type_Name )
            {
            // InternalStructuredTextParser.g:11575:1: ( ruleDate_Type_Name )
            // InternalStructuredTextParser.g:11576:1: ruleDate_Type_Name
            {
             before(grammarAccess.getDateAccess().getTypeDate_Type_NameEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDate_Type_Name();

            state._fsp--;

             after(grammarAccess.getDateAccess().getTypeDate_Type_NameEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date__TypeAssignment_0"


    // $ANTLR start "rule__Date__ValueAssignment_2"
    // InternalStructuredTextParser.g:11585:1: rule__Date__ValueAssignment_2 : ( ruleDate_Literal ) ;
    public final void rule__Date__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11589:1: ( ( ruleDate_Literal ) )
            // InternalStructuredTextParser.g:11590:1: ( ruleDate_Literal )
            {
            // InternalStructuredTextParser.g:11590:1: ( ruleDate_Literal )
            // InternalStructuredTextParser.g:11591:1: ruleDate_Literal
            {
             before(grammarAccess.getDateAccess().getValueDate_LiteralParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleDate_Literal();

            state._fsp--;

             after(grammarAccess.getDateAccess().getValueDate_LiteralParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date__ValueAssignment_2"


    // $ANTLR start "rule__Date_And_Time__TypeAssignment_0"
    // InternalStructuredTextParser.g:11600:1: rule__Date_And_Time__TypeAssignment_0 : ( ruleDT_Type_Name ) ;
    public final void rule__Date_And_Time__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11604:1: ( ( ruleDT_Type_Name ) )
            // InternalStructuredTextParser.g:11605:1: ( ruleDT_Type_Name )
            {
            // InternalStructuredTextParser.g:11605:1: ( ruleDT_Type_Name )
            // InternalStructuredTextParser.g:11606:1: ruleDT_Type_Name
            {
             before(grammarAccess.getDate_And_TimeAccess().getTypeDT_Type_NameEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDT_Type_Name();

            state._fsp--;

             after(grammarAccess.getDate_And_TimeAccess().getTypeDT_Type_NameEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time__TypeAssignment_0"


    // $ANTLR start "rule__Date_And_Time__ValueAssignment_2"
    // InternalStructuredTextParser.g:11615:1: rule__Date_And_Time__ValueAssignment_2 : ( ruleDate_And_Time_Value ) ;
    public final void rule__Date_And_Time__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalStructuredTextParser.g:11619:1: ( ( ruleDate_And_Time_Value ) )
            // InternalStructuredTextParser.g:11620:1: ( ruleDate_And_Time_Value )
            {
            // InternalStructuredTextParser.g:11620:1: ( ruleDate_And_Time_Value )
            // InternalStructuredTextParser.g:11621:1: ruleDate_And_Time_Value
            {
             before(grammarAccess.getDate_And_TimeAccess().getValueDate_And_Time_ValueParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleDate_And_Time_Value();

            state._fsp--;

             after(grammarAccess.getDate_And_TimeAccess().getValueDate_And_Time_ValueParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Date_And_Time__ValueAssignment_2"

    // Delegated rules


    protected DFA5 dfa5 = new DFA5(this);
    static final String dfa_1s = "\12\uffff";
    static final String dfa_2s = "\5\uffff\3\4\2\uffff";
    static final String dfa_3s = "\1\4\2\57\2\uffff\3\10\1\116\1\uffff";
    static final String dfa_4s = "\1\163\2\157\2\uffff\3\140\1\120\1\uffff";
    static final String dfa_5s = "\3\uffff\1\1\1\2\4\uffff\1\3";
    static final String dfa_6s = "\12\uffff}>";
    static final String[] dfa_7s = {
            "\4\11\7\uffff\1\11\3\uffff\1\11\2\uffff\4\11\2\uffff\2\11\1\uffff\2\11\1\uffff\1\11\2\uffff\3\11\2\uffff\4\11\1\uffff\1\10\2\11\3\uffff\2\11\1\uffff\1\3\1\11\12\uffff\1\7\1\uffff\1\11\1\6\10\uffff\1\4\2\uffff\1\2\1\uffff\1\1\10\uffff\1\5\3\uffff\1\11\10\uffff\1\4\4\11\1\uffff\1\11\1\uffff\1\11",
            "\1\3\24\uffff\1\3\2\uffff\1\3\10\uffff\1\3\15\uffff\1\3\14\uffff\1\3\3\uffff\1\11",
            "\1\3\24\uffff\1\3\2\uffff\1\3\10\uffff\1\3\15\uffff\1\3\14\uffff\1\3\3\uffff\1\11",
            "",
            "",
            "\1\4\45\uffff\1\4\4\uffff\1\4\3\uffff\1\4\3\uffff\2\4\1\uffff\2\4\1\uffff\3\4\4\uffff\3\4\3\uffff\1\11\1\4\1\uffff\7\4\1\uffff\4\4\2\uffff\2\4",
            "\1\4\45\uffff\1\4\4\uffff\1\4\3\uffff\1\4\3\uffff\2\4\1\uffff\2\4\1\uffff\3\4\4\uffff\3\4\3\uffff\1\11\1\4\1\uffff\7\4\1\uffff\4\4\2\uffff\2\4",
            "\1\4\45\uffff\1\4\4\uffff\1\4\3\uffff\1\4\3\uffff\2\4\1\uffff\2\4\1\uffff\3\4\4\uffff\3\4\3\uffff\1\11\1\4\1\uffff\7\4\1\uffff\4\4\2\uffff\2\4",
            "\1\11\1\uffff\1\4",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA5 extends DFA {

        public DFA5(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 5;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "2422:1: rule__Unary_Expr__Alternatives : ( ( ( rule__Unary_Expr__Group_0__0 ) ) | ( rulePrimary_Expr ) | ( ruleConstant ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0410821208060800L,0x00000800420000B0L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000004400L,0x0000080000000000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000402L,0x0000080000000000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000000L,0x0000000002000000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000400L,0x0000080000000000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000000L,0x0000000001000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0222BCE5B38880F0L,0x0000080000000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0263BCE5B3C880F0L,0x000AF004402800D0L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0010821208060802L,0x00000800420000B0L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0010821208060800L,0x00000800420000B0L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0363BCE5B3C880F0L,0x000AF804402900D0L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000000L,0x0000000000010000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000800008040000L,0x0000080000000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000010000210000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000010000001000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0263BCE5B3C880F2L,0x000AF004402800D0L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000000000L,0x0000000001100000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000002L,0x0000000000100000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0010821208060800L,0x00000800400000B0L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000000000L,0x0000080040000090L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000000000L,0x000000000000000CL});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000200L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0800000000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0800000000000002L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0008000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0008000000000002L,0x0000000000008000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x8000000000000000L,0x0000000008000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x8000000000000002L,0x0000000008000000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x4000000000000000L,0x0000000014000002L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x4000000000000002L,0x0000000014000002L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000000000000L,0x0000000000280000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000000000002L,0x0000000000280000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0080000000000000L,0x0000000000840000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0080000000000002L,0x0000000000840000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x1000000000000000L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x1000000000000002L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0100000000000000L,0x0000000000280000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0000800000000000L,0x0000080040010090L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0363BCE5B3C880F0L,0x000AF804402B00D0L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0000000000000000L,0x0000000000100000L});
    public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x0000000000000000L,0x0000000080000000L});
    public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x0000000000000000L,0x0000000100100000L});
    public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
    public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x00222480B0000000L,0x0000F00000280000L});
    public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
    public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x0000000000000000L,0x0000800000280000L});
    public static final BitSet FOLLOW_64 = new BitSet(new long[]{0x00223480B1000000L,0x0000F00000280000L});
    public static final BitSet FOLLOW_65 = new BitSet(new long[]{0x0000000000000000L,0x0000800000000000L});
    public static final BitSet FOLLOW_66 = new BitSet(new long[]{0x0000000000000000L,0x0000000020000000L});
    public static final BitSet FOLLOW_67 = new BitSet(new long[]{0x0000002100088000L,0x000A000000000000L});
    public static final BitSet FOLLOW_68 = new BitSet(new long[]{0x0000000000000000L,0x0000800000004000L});
    public static final BitSet FOLLOW_69 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
    public static final BitSet FOLLOW_70 = new BitSet(new long[]{0x0000000000000002L,0x0000000200000000L});
    public static final BitSet FOLLOW_71 = new BitSet(new long[]{0x0000000000000000L,0x0000003C00003800L});
    public static final BitSet FOLLOW_72 = new BitSet(new long[]{0x0000000000000000L,0x0000000000200000L});

}