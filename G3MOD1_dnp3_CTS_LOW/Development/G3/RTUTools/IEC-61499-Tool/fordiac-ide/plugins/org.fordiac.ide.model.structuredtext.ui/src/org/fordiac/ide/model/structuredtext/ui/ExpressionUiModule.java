/*******************************************************************************
 * Copyright (c) 2016 fortiss GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Alois Zoitl, Martin Jobst - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.fordiac.ide.model.structuredtext.ui;

import org.eclipse.jface.text.source.IOverviewRuler;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.ui.editor.XtextSourceViewer;
import org.fordiac.ide.model.structuredtext.ui.contentassist.antlr.ExpressionParser;

public class ExpressionUiModule extends StructuredTextUiModule {
	
	public ExpressionUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
	
	
	public Class<? extends org.eclipse.xtext.ui.editor.contentassist.antlr.IContentAssistParser> bindIContentAssistParser() {
		return ExpressionParser.class;
	}
	
	
	public Class<?extends XtextSourceViewer.Factory> bindXtextSourceViewer$Factory() {
		return ExpresionSourceViewerFactory.class;
	}
	
	public static class ExpresionSourceViewerFactory extends XtextSourceViewer.DefaultFactory  {
		@Override
		public XtextSourceViewer createSourceViewer(Composite parent, IVerticalRuler ruler,
				IOverviewRuler overviewRuler, boolean showsAnnotationOverview, int styles) {			
			return super.createSourceViewer(parent, null, null, false, SWT.BORDER | SWT.SINGLE);
		}
	}
	
	//For now we don't want code templates for expressions 
	@SuppressWarnings("restriction")
	@Override
	public com.google.inject.Provider<org.eclipse.xtext.ui.codetemplates.ui.preferences.TemplatesLanguageConfiguration> provideTemplatesLanguageConfiguration() {
		return null;
	}

	//For now we don't want code templates for expressions 
	@SuppressWarnings("restriction")
	@Override
	public com.google.inject.Provider<org.eclipse.xtext.ui.codetemplates.ui.registry.LanguageRegistry> provideLanguageRegistry() {
		return null;
	}

	//For now we don't want code templates for expressions 
	@SuppressWarnings("restriction")
	@Override
	@org.eclipse.xtext.service.SingletonBinding(eager=true)	public Class<? extends org.eclipse.xtext.ui.codetemplates.ui.registry.LanguageRegistrar> bindLanguageRegistrar() {
		return null;
	}

	//For now we don't want code templates for expressions 
	@Override
	public Class<? extends org.eclipse.xtext.ui.editor.templates.XtextTemplatePreferencePage> bindXtextTemplatePreferencePage() {
		return null;
	}

	//For now we don't want code templates for expressions 
	@SuppressWarnings("restriction")
	@Override
	public Class<? extends org.eclipse.xtext.ui.codetemplates.ui.partialEditing.IPartialContentAssistParser> bindIPartialContentAssistParser() {
		return null;
	}
}
