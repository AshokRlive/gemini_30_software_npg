/**
 * generated by Xtext
 */
package org.fordiac.ide.model.structuredtext.ui.outline;

import org.eclipse.xtext.ui.editor.outline.impl.DefaultOutlineTreeProvider;

/**
 * Customization of the default outline structure.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#outline
 */
@SuppressWarnings("all")
public class StructuredTextOutlineTreeProvider extends DefaultOutlineTreeProvider {
}
