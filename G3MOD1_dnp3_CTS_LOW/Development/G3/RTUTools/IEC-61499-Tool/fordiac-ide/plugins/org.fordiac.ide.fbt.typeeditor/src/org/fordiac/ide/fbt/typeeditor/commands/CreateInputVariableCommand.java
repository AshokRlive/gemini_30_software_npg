/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

/**
 * The Class CreateInputVariableCommand.
 */
public class CreateInputVariableCommand extends Command {
	
	/** The fb type. */
	private final FBType fbType;

	private VarDeclaration varDecl;

	private int pos;
	
	/**
	 * Instantiates a new creates the input variable command.
	 * 
	 * @param dataType
	 *            the data type
	 * @param fbType
	 *            the fb type
	 */
	public CreateInputVariableCommand(VarDeclaration varDecl, final FBType fbType, int pos) {
		this.varDecl = varDecl;
		this.fbType = fbType;
		this.pos = pos;
	}

	/**
	 * Gets the i interface element.
	 * 
	 * @return the i interface element
	 */
	public IInterfaceElement getIInterfaceElement() {
		return varDecl;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		String name = varDecl.getType().getName();
		name = NameRepository.getUniqueInterfaceElementName(varDecl, fbType, name);
		varDecl.setName(name);

		varDecl.setIsInput(true);
		
		if (pos < 0 || pos > fbType.getInterfaceList().getInputVars().size()) {
			pos = fbType.getInterfaceList().getInputVars().size();
		}
		
		fbType.getInterfaceList().getInputVars().add(pos, varDecl);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		fbType.getInterfaceList().getInputVars().remove(varDecl);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		fbType.getInterfaceList().getInputVars().add(pos, varDecl);
	}
}
