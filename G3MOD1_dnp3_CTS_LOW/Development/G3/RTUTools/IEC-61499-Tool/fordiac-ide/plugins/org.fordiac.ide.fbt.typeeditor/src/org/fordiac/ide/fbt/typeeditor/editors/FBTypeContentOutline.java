/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.editors;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.views.contentoutline.ContentOutlinePage;
import org.fordiac.ide.model.data.provider.DataItemProviderAdapterFactory;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.provider.LibraryElementItemProviderAdapterFactory;

public class FBTypeContentOutline extends ContentOutlinePage {
	
	protected TreeViewer contentOutlineViewer;
	
	protected LibraryElementItemProviderAdapterFactory adapterFactory = new LibraryElementItemProviderAdapterFactory();
	protected DataItemProviderAdapterFactory dataFactory = new DataItemProviderAdapterFactory();
	
	protected ComposedAdapterFactory caf = new ComposedAdapterFactory();
	private CommandStack commandStack;
	FBType fbType;
	FBTypeEditor editor;
	
	private final EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					getTreeViewer().expandAll();
				}
			});
		}
	};
	
	public CommandStack getCommandStack() {
		return commandStack;
	}
	
	FBTypeContentOutline(FBType fbType, FBTypeEditor editor){
		this.fbType = fbType;
		this.editor = editor;
		this.commandStack = editor.getCommandStack();
	}
		
	public void createControl(Composite parent) {
		super.createControl(parent);

		caf.addAdapterFactory(adapterFactory);
		caf.addAdapterFactory(dataFactory);
		
		
		contentOutlineViewer = getTreeViewer();
		contentOutlineViewer.addSelectionChangedListener(this);
		

		// Set up the tree viewer.
		//
		contentOutlineViewer.setContentProvider(new AdapterFactoryContentProvider(caf));
		contentOutlineViewer.setLabelProvider(new AdapterFactoryLabelProvider(caf));
		contentOutlineViewer.setInput(fbType);
		contentOutlineViewer.expandAll();
		
		fbType.eAdapters().add(adapter);
		
	}
	
	@Override
	public void dispose() {
		caf.removeAdapterFactory(adapterFactory);
		caf.removeAdapterFactory(dataFactory);
		fbType.eAdapters().remove(adapter);
		contentOutlineViewer.removeSelectionChangedListener(this);
		super.dispose();
	}

}
