/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.data.DataType;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

/**
 * The Class ChangeTypeCommand.
 */
public class ChangeTypeCommand extends Command {

	/** The interface element. */
	private final VarDeclaration interfaceElement;

	/** The data type. */
	private DataType dataType;

	/** The old data type. */
	private DataType oldDataType;

	/**
	 * Instantiates a new change type command.
	 * 
	 * @param interfaceElement the interface element
	 * @param dataType the data type
	 */
	public ChangeTypeCommand(final VarDeclaration interfaceElement,
			final DataType dataType) {
		super();
		this.interfaceElement = interfaceElement;
		this.dataType = dataType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldDataType = interfaceElement.getType();
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		interfaceElement.setType(oldDataType);
		interfaceElement.setTypeName(oldDataType.getName());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		interfaceElement.setType(dataType);
		interfaceElement.setTypeName(dataType.getName());
	}
}
