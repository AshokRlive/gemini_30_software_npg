/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

public class ChangeArraySizeCommand extends Command {
	
	VarDeclaration variable;
	
	int oldArraySize;
	int newArraySize;
	
	String newArraySizeString;
	
	
	public ChangeArraySizeCommand(final VarDeclaration variable, final String newArraySizeString){
		super();
		this.variable = variable;
		this.newArraySizeString = newArraySizeString;
	}
	
	public boolean canExecute() {
		return (null != variable)&&(null != newArraySizeString);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		
		if(variable.isArray()){
			oldArraySize = variable.getArraySize();
		}
		else{
			oldArraySize = 0;
		}
		
		if (newArraySizeString.length() == 0) {
			newArraySize = 0;
		} else if (newArraySizeString.length() > 0) {
			try {
				newArraySize = Integer.parseInt(newArraySizeString);
			} catch (NumberFormatException nfe) {
				newArraySize = 0;
			} 
		}
		
		setArraySize(newArraySize);
	}

	private void setArraySize(int arraySize) {
		variable.setArray(0 != arraySize);
		variable.setArraySize(arraySize);		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		setArraySize(oldArraySize);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		setArraySize(newArraySize);
	}

}
