/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor;

import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IEditorPart;

/** An edit domain that allows to share a command stack accross all editor pages of the FB type editor
 * 
 */
public class FBTypeEditDomain extends DefaultEditDomain {

	/** The shared command stack 
	 */
	private CommandStack commandStack;
	
	
	public FBTypeEditDomain(IEditorPart editorPart, CommandStack commandStack) {
		super(editorPart);
		this.commandStack = commandStack;
		loadDefaultTool(); //redo this here as the first invocation in the super constructor will not work as the command stack is not correctly set the first time
	}


	@Override
	public org.eclipse.gef.commands.CommandStack getCommandStack() {
		return commandStack;
	}


	@Override
	public void loadDefaultTool() {
		if(null != commandStack){
			super.loadDefaultTool();
		}
	}
	
	

}
