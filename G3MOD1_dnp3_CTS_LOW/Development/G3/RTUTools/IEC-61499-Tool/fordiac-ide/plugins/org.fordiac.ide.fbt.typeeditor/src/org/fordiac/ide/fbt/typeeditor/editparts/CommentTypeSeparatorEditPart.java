package org.fordiac.ide.fbt.typeeditor.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

public class CommentTypeSeparatorEditPart extends AbstractGraphicalEditPart implements
EditPart{
	protected CommentTypeField.CommentTypeSeparator getCastedModel() {
		return (CommentTypeField.CommentTypeSeparator) getModel();
	}
	
	@Override
	protected IFigure createFigure() {
		return new Label(getCastedModel().getLabel());
	}

	@Override
	protected void createEditPolicies() {
			// no editpart necessary
	}
	
}
