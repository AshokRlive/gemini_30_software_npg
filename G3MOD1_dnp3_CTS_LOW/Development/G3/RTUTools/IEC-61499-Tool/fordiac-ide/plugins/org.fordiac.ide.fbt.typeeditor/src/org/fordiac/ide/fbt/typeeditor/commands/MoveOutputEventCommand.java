/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FBType;

/**
 * The Class MoveOutputEventCommand.
 */
public class MoveOutputEventCommand extends Command {

	/** The child. */
	private final Event child;

	/** The parent. */
	private final FBType parent;

	/** The old index. */
	private final int oldIndex;

	/** The new index. */
	private int newIndex;

	/**
	 * Instantiates a new move output event command.
	 * 
	 * @param child the child
	 * @param parent the parent
	 * @param oldIndex the old index
	 * @param newIndex the new index
	 */
	public MoveOutputEventCommand(final Event child, final FBType parent,
			final int oldIndex, final int newIndex) {
		super();
		this.child = child;
		this.parent = parent;
		this.oldIndex = oldIndex;
		this.newIndex = newIndex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		if (newIndex > oldIndex) {
			newIndex--;
		}
		parent.getInterfaceList().getEventOutputs().move(newIndex, child);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		parent.getInterfaceList().getEventOutputs().move(oldIndex, child);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		parent.getInterfaceList().getEventOutputs().move(newIndex, child);
	}

}
