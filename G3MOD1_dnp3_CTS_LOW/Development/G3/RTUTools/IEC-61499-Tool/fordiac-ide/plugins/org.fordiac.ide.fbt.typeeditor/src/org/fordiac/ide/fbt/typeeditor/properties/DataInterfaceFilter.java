/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.properties;

import org.eclipse.jface.viewers.IFilter;
import org.fordiac.ide.fbt.typeeditor.editparts.CommentEditPart;
import org.fordiac.ide.fbt.typeeditor.editparts.InterfaceEditPart;
import org.fordiac.ide.fbt.typeeditor.editparts.TypeEditPart;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

public class DataInterfaceFilter implements IFilter {

	@Override
	public boolean select(Object toTest) {
		if(toTest instanceof InterfaceEditPart){
			if(((InterfaceEditPart) toTest).getCastedModel() instanceof AdapterDeclaration || ((InterfaceEditPart) toTest).getCastedModel() instanceof Event){
				return false;
			}
			return true;
		}
		if(toTest instanceof TypeEditPart){
			if(((TypeEditPart) toTest).getCastedModel() instanceof AdapterDeclaration || ((TypeEditPart) toTest).getCastedModel() instanceof Event){
				return false;
			}
			return true;
		}
		if(toTest instanceof CommentEditPart){
			if(((CommentEditPart) toTest).getCastedModel() instanceof AdapterDeclaration || ((CommentEditPart) toTest).getCastedModel() instanceof Event){
				return false;
			}
			return true;
		}
		if(!(toTest instanceof AdapterDeclaration || toTest instanceof Event) && toTest instanceof VarDeclaration){
			return true;
		}
		return false;
	}
}
