/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.data.DataFactory;
import org.fordiac.ide.model.data.VarInitialization;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

public class ChangeInitialValueCommand extends Command {
	
	VarDeclaration variable;
	String newInitialValue;
	String oldInitialValue;
	
	
	public ChangeInitialValueCommand(final VarDeclaration variable, final String newInitialValue){
		super();
		this.variable = variable;
		this.newInitialValue = newInitialValue;
	}
	
	public boolean canExecute() {
		return (null != variable)&&(null != newInitialValue);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		
		if (variable.getVarInitialization() != null) {
			oldInitialValue = variable.getVarInitialization().getInitialValue();
		} else {
			VarInitialization varInitialization = DataFactory.eINSTANCE
					.createVarInitialization();
			variable.setVarInitialization(varInitialization);
		}
		variable.getVarInitialization().setInitialValue(newInitialValue);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		variable.getVarInitialization().setInitialValue(oldInitialValue);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		variable.getVarInitialization().setInitialValue(newInitialValue);
	}

}
