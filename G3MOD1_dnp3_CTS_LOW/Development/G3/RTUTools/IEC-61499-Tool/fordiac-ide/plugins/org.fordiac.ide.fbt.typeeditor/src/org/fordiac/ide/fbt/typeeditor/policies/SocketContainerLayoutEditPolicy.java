/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.policies;

import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.FlowLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.jface.preference.IPreferenceStore;
import org.fordiac.ide.fbt.typeeditor.commands.CreateSocketCommand;
import org.fordiac.ide.fbt.typeeditor.commands.MoveSocketCommand;
import org.fordiac.ide.fbt.typeeditor.editparts.AdapterInterfaceEditPart;
import org.fordiac.ide.fbt.typeeditor.editparts.SocketContainer;
import org.fordiac.ide.gef.Activator;
import org.fordiac.ide.gef.policies.ModifiedNonResizeableEditPolicy;
import org.fordiac.ide.gef.preferences.DiagramPreferences;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.data.EventType;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.FBType;

/**
 * The Class SocketContainerLayoutEditPolicy.
 */
public class SocketContainerLayoutEditPolicy extends
		FlowLayoutEditPolicy {
	@Override
	protected EditPolicy createChildEditPolicy(EditPart child) {
		IPreferenceStore pf = Activator.getDefault().getPreferenceStore();
		int cornerDim = pf.getInt(DiagramPreferences.CORNER_DIM);
		if (cornerDim > 1) {
			cornerDim = cornerDim / 2;
		}
		return new ModifiedNonResizeableEditPolicy(cornerDim, new Insets(1));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.OrderedLayoutEditPolicy#createAddCommand(org
	 * .eclipse.gef.EditPart, org.eclipse.gef.EditPart)
	 */
	@Override
	protected Command createAddCommand(final EditPart child, final EditPart after) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.OrderedLayoutEditPolicy#createMoveChildCommand
	 * (org.eclipse.gef.EditPart, org.eclipse.gef.EditPart)
	 */
	@Override
	protected Command createMoveChildCommand(final EditPart child,
			final EditPart after) {

		if (child instanceof AdapterInterfaceEditPart) {
			AdapterInterfaceEditPart childEP = (AdapterInterfaceEditPart) child;
			AdapterInterfaceEditPart afterEP = null;
			if (after != null) {
				afterEP = (AdapterInterfaceEditPart) after;
			}
			if (childEP.isAdapter() && childEP.isInput()
					&& (afterEP == null || (afterEP.isAdapter() && afterEP.isInput()))) {
				int oldIndex = getHost().getChildren().indexOf(child);
				int newIndex = -1;
				if (after == null) {
					newIndex = getHost().getChildren().size();
				} else {
					newIndex = getHost().getChildren().indexOf(after);
				}
				Object model = getHost().getModel();
				FBType type = null;
				if (model instanceof SocketContainer) {
					type = ((SocketContainer) model).getFbType();
				}
				return new MoveSocketCommand(
						(AdapterDeclaration) childEP.getModel(), type, oldIndex, newIndex);
			}

		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.LayoutEditPolicy#getCreateCommand(org.eclipse
	 * .gef.requests.CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(final CreateRequest request) {
		Object childClass = request.getNewObjectType();
		Object model = getHost().getModel();
		FBType type = null;
		if (model instanceof SocketContainer) {
			type = ((SocketContainer) model).getFbType();
		}
		if (childClass instanceof AdapterTypePaletteEntry && type != null
				&& !(childClass instanceof EventType)) {
			
			int index = -1;
			EditPart ref = getInsertionReference(request);		
			if (ref != null) {
				index = type.getInterfaceList().getSockets().indexOf(ref.getModel());
			}
			CreateSocketCommand cmd = new CreateSocketCommand(
					(AdapterTypePaletteEntry) childClass, (AdapterDeclaration)request.getNewObject(), type, index);
			return cmd;
		}
		return null;
	}
}
