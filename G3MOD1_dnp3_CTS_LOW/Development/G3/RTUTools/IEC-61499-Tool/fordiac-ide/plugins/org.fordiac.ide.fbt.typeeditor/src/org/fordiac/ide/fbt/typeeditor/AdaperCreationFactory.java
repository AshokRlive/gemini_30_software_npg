/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor;

import org.fordiac.ide.gef.utilities.TemplateCreationFactory;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;

public class AdaperCreationFactory extends TemplateCreationFactory {
	
	//** buffer for the new type that has been created last. Required that after creation selection is working correctly.
	AdapterDeclaration newAdapter = null;

	public AdaperCreationFactory(AdapterTypePaletteEntry typeTemplate) {
		super(typeTemplate);
	}
	
	@Override
	public Object getNewObject() {
		if(null == newAdapter){
			newAdapter = createNewAdapterDeclaration((AdapterTypePaletteEntry)super.getObjectType());
		}		
		return newAdapter;
	}
	

	@Override
	public Object getObjectType() {
		newAdapter = null;
		return super.getObjectType();
	}

	public static AdapterDeclaration createNewAdapterDeclaration(AdapterTypePaletteEntry adapterTypePaletteEntry) {
		AdapterDeclaration adapterDecl = LibraryElementFactory.eINSTANCE.createAdapterDeclaration();
		adapterDecl.setType(adapterTypePaletteEntry.getAdapterType()); 
		adapterDecl.setTypeName(adapterTypePaletteEntry.getLabel());		
		return adapterDecl;
	}
}
