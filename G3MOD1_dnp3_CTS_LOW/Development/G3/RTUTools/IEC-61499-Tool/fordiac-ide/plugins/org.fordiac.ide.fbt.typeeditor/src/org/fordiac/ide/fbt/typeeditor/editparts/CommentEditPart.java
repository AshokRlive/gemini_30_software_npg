/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.DirectEditRequest;
import org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart;
import org.fordiac.ide.gef.policies.INamedElementRenameEditPolicy;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.util.commands.ChangeCommentCommand;

/**
 * The Class CommentEditPart.
 * 
 * @author gebenh
 */
public class CommentEditPart extends AbstractInterfaceElementEditPart implements
		EditPart {

	
	private Label comment;

	public IInterfaceElement getCastedModel() {
		return (IInterfaceElement) ((CommentField)getModel()).getReferencedElement();
	}

	@Override
	protected IFigure createFigure() {
		comment = new Label();
		update();
		return comment;
	}

	protected void update() {
		comment.setText(((CommentField)getModel()).getLabel());
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#
	 * createEditPolicies ()
	 */
	@Override
	protected void createEditPolicies() {
		// super.createEditPolicies();
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
				new INamedElementRenameEditPolicy() {
					@Override
					protected Command getDirectEditCommand(
							final DirectEditRequest request) {
						if (getHost() instanceof AbstractDirectEditableEditPart) {
							ChangeCommentCommand cmd = new ChangeCommentCommand(
									getCastedModel(), (String) request
											.getCellEditor().getValue());
							return cmd;
						}
						return null;
					}

				});
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractEditPart#performRequest(org.eclipse.gef
	 * .Request)
	 */
	@Override
	public void performRequest(final Request request) {
		// REQ_DIRECT_EDIT -> first select 0.4 sec pause -> click -> edit
		// REQ_OPEN -> doubleclick

		if (request.getType() == RequestConstants.REQ_OPEN) {
			//Perform double click as direct edit
			request.setType(RequestConstants.REQ_DIRECT_EDIT); 
		}
		super.performRequest(request);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#getNameLabel
	 * ()
	 */
	@Override
	public Label getNameLabel() {
		return (Label) getFigure();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#getINamedElement
	 * ()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel();
	}

}
