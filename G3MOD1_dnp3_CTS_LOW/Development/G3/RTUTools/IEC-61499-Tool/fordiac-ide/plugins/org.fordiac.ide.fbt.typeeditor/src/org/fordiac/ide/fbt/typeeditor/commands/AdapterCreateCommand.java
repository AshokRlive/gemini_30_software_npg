package org.fordiac.ide.fbt.typeeditor.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.fordiac.ide.application.commands.FBCreateCommand;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.AdapterFB;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;

public class AdapterCreateCommand extends FBCreateCommand {

	AdapterTypePaletteEntry adapterPaletteEntry;
	
	boolean plug;
	
	public AdapterCreateCommand(AdapterTypePaletteEntry adapterPaletteEntry, Rectangle bounds, boolean plug, CompositeFBType parentComposite) {
		super(null, parentComposite.getFBNetwork(), bounds);
		
		this.adapterPaletteEntry = adapterPaletteEntry;
		this.plug = plug;
		setParentComposite(parentComposite);
	}
	
	protected void createFB() {
		AdapterFB aFB = LibraryElementFactory.eINSTANCE.createAdapterFB();
		aFB.setPaletteEntry(adapterPaletteEntry);
		aFB.setPlug(plug);
		aFB.setResourceTypeFB(true);
		fB = aFB;
	}

}
