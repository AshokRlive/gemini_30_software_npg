/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.policies;

import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.ReconnectRequest;
import org.fordiac.ide.fbt.typeeditor.commands.WithCreateCommand;
import org.fordiac.ide.fbt.typeeditor.editparts.InterfaceEditPart;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

/**
 * The Class WithNodeEditPolicy.
 */
public class WithNodeEditPolicy extends GraphicalNodeEditPolicy implements
		EditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getConnectionCompleteCommand(org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCompleteCommand(
			final CreateConnectionRequest request) {
		if (request.getStartCommand() instanceof WithCreateCommand) {
			WithCreateCommand command = (WithCreateCommand) request
					.getStartCommand();
			if (command.isForwardCreation()) {
				if (((InterfaceEditPart) getHost()).isEvent()) {
					command.setVarDeclaration(null);
				} else {
					command
							.setVarDeclaration((VarDeclaration) ((InterfaceEditPart) getHost())
									.getCastedModel());
				}
			} else {
				if (((InterfaceEditPart) getHost()).isEvent()) {
					command.setEvent((Event) ((InterfaceEditPart) getHost())
							.getCastedModel());
				} else {
					command.setEvent(null);
				}
			}
			return command;
		}
		return null;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getConnectionCreateCommand(org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCreateCommand(
			final CreateConnectionRequest request) {

		WithCreateCommand cmd = new WithCreateCommand();
		if (getHost() instanceof InterfaceEditPart) {
			if (((InterfaceEditPart) getHost()).isEvent()) {
				cmd.setEvent((Event) ((InterfaceEditPart) getHost())
						.getCastedModel());
				cmd.setForwardCreation(true);
			} else {
				cmd
						.setVarDeclaration((VarDeclaration) ((InterfaceEditPart) getHost())
								.getCastedModel());
				cmd.setForwardCreation(false);
			}

		}
		request.setStartCommand(cmd);
		return cmd;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getReconnectSourceCommand(org.eclipse.gef.requests.ReconnectRequest)
	 */
	@Override
	protected Command getReconnectSourceCommand(final ReconnectRequest request) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getReconnectTargetCommand(org.eclipse.gef.requests.ReconnectRequest)
	 */
	@Override
	protected Command getReconnectTargetCommand(final ReconnectRequest request) {
		return null;
	}

}
