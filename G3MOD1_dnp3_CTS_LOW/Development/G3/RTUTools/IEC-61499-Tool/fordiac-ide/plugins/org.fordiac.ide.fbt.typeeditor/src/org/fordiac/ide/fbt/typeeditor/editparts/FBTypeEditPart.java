/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.fordiac.ide.fbt.typeeditor.figures.FBTypeFigure;
import org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.LibraryElementPackage;

/**
 * The Class FBTypeEditPart.
 */
public class FBTypeEditPart extends AbstractDirectEditableEditPart{
	
	private ZoomManager zoomManager;
	
	private final EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			if(Notification.REMOVING_ADAPTER != notification.getEventType()){
				Object feature = notification.getFeature();
				if((LibraryElementPackage.eINSTANCE.getVersionInfo().equals(feature)) || 
						(LibraryElementPackage.eINSTANCE.getVersionInfo_Version().equals(feature))){
					getCastedFigure().updateVersionInfoLabel();
				}

				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						if((null != getFigure()) && (getFigure().isShowing())){
							refresh();
						}
					}
				});
			}
		}

	};
	
	@Override
	public void activate() {
		super.activate();
		getCastedModel().eAdapters().add(adapter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		super.deactivate();
		getCastedModel().eAdapters().remove(adapter);
	}

	public FBType getCastedModel() {
		return (FBType) getModel();
	}

	public FBTypeEditPart(ZoomManager zoomManager) {
		super();
		this.zoomManager = zoomManager;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		return new FBTypeFigure((FBType) getModel(), zoomManager);
	}

	/** The control listener. */
	private ControlListener controlListener;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#setSelected(int)
	 */
	@Override
	public void setSelected(int value) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#
	 * createEditPolicies ()
	 */
	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
	}

	
	protected void refreshName() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				getNameLabel().setText(getINamedElement().getName());
			}
		});
	}

	/** The eic. */
	EventInputContainer eic;

	/** The eoc. */
	EventOutputContainer eoc;

	/** The vic. */
	VariableInputContainer vic;
	
	SocketContainer socketcont;

	/** The voc. */
	VariableOutputContainer voc;
	
	PlugContainer plugcont;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected List getModelChildren() {
		if (eic == null) {
			eic = new EventInputContainer(getCastedModel());
		}
		if (eoc == null) {
			eoc = new EventOutputContainer(getCastedModel());
		}
		if (vic == null) {
			vic = new VariableInputContainer(getCastedModel());
		}
		
		if (socketcont == null) {
			socketcont = new SocketContainer(getCastedModel());
		}
		
		if (voc == null) {
			voc = new VariableOutputContainer(getCastedModel());
		}
		if (plugcont == null) {
			plugcont = new PlugContainer(getCastedModel());
		}
		ArrayList<Object> temp = new ArrayList<Object>();
		temp.add(eic);
		temp.add(eoc);
		temp.add(vic);
		temp.add(socketcont);
		temp.add(voc);
		temp.add(plugcont);

		return temp;
		// return super.getModelChildren();
	}

	/**
	 * Gets the casted figure.
	 * 
	 * @return the casted figure
	 */
	FBTypeFigure getCastedFigure() {
		return (FBTypeFigure) getFigure();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractGraphicalEditPart#addChildVisual(org.
	 * eclipse.gef.EditPart, int)
	 */
	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof InterfaceContainerEditPart) {
			if (childEditPart.getModel() instanceof EventInputContainer) {
				getCastedFigure().getInputEvents().add(child);
			}
			if (childEditPart.getModel() instanceof EventOutputContainer) {
				getCastedFigure().getOutputEvents().add(child);
			}
			if (childEditPart.getModel() instanceof VariableInputContainer) {
				getCastedFigure().getInputVariables().add(child);
			}
			if (childEditPart.getModel() instanceof VariableOutputContainer) {
				getCastedFigure().getOutputVariables().add(child);
			}
			if (childEditPart.getModel() instanceof SocketContainer) {
				getCastedFigure().getSockets().add(child);
			}
			if (childEditPart.getModel() instanceof PlugContainer) {
				getCastedFigure().getPlugs().add(child);
			}
		} else {
			super.addChildVisual(childEditPart, index);
		}
	}
	
	@Override
	protected void removeChildVisual(EditPart childEditPart) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		if (childEditPart instanceof InterfaceContainerEditPart) {
			if (childEditPart.getModel() instanceof EventInputContainer) {
				getCastedFigure().getInputEvents().remove(child);
			}
			if (childEditPart.getModel() instanceof EventOutputContainer) {
				getCastedFigure().getOutputEvents().remove(child);
			}
			if (childEditPart.getModel() instanceof VariableInputContainer) {
				getCastedFigure().getInputVariables().remove(child);
			}
			if (childEditPart.getModel() instanceof VariableOutputContainer) {
				getCastedFigure().getOutputVariables().remove(child);
			}
			if (childEditPart.getModel() instanceof SocketContainer) {
				getCastedFigure().getSockets().remove(child);
			}
			if (childEditPart.getModel() instanceof PlugContainer) {
				getCastedFigure().getPlugs().remove(child);
			}
		} else {
			super.removeChildVisual(childEditPart);
		}
	}
	

	/**
	 * Update.
	 * 
	 * @param bounds
	 *            the bounds
	 */
	protected void update(final Rectangle bounds) {
		((GraphicalEditPart) getParent()).setLayoutConstraint(this,
				getFigure(), bounds);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		if (controlListener == null) {
			controlListener = new ControlListener() {

				public void controlResized(final ControlEvent e) {
					Point p = getParent().getViewer().getControl().getSize();
					Dimension dim = getFigure().getPreferredSize(-1, -1);

					Rectangle rect = new Rectangle(p.x / 2 - dim.width / 2, p.y
							/ 2 - dim.height / 2, -1, -1);
					// rectangle rect = new Rectangle()

					update(rect);
				}

				public void controlMoved(final ControlEvent e) {

				}

			};
			getParent().getViewer().getControl()
					.addControlListener(controlListener);

		}
		Point p = getParent().getViewer().getControl().getSize();
		Dimension dim = getFigure().getPreferredSize(-1, -1);

		Rectangle rect = new Rectangle(p.x / 2 - dim.width / 2, p.y / 2
				- dim.height / 2, -1, -1);

		update(rect);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#getINamedElement
	 * ()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#getNameLabel
	 * ()
	 */
	@Override
	public Label getNameLabel() {
		return getCastedFigure().getTypeNameLabel();
	}
}
