/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.properties;

import org.eclipse.jface.viewers.IFilter;
import org.fordiac.ide.fbt.typeeditor.editparts.CommentEditPart;
import org.fordiac.ide.fbt.typeeditor.editparts.InterfaceEditPart;
import org.fordiac.ide.fbt.typeeditor.editparts.TypeEditPart;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;

public class AdapterInterfaceFilter implements IFilter{

	@Override
	public boolean select(Object toTest) {
		if(toTest instanceof InterfaceEditPart && ((InterfaceEditPart) toTest).getCastedModel() instanceof AdapterDeclaration){
			return true;
		}
		if(toTest instanceof TypeEditPart && ((TypeEditPart) toTest).getCastedModel() instanceof AdapterDeclaration){
			return true;
		}
		if(toTest instanceof CommentEditPart && ((CommentEditPart) toTest).getCastedModel() instanceof AdapterDeclaration){
			return true;
		}
		if(toTest instanceof AdapterDeclaration){
			return true;
		}
		return false;
	}

}
