/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.editparts;

import java.util.ArrayList;
import java.util.List;

import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;

/**
 * The Class SocketContainer.
 */
public class PlugContainer implements IContainerElement{

	/** The fb type. */
	private final FBType fbType;

	/**
	 * Instantiates a new variable input container.
	 * 
	 * @param fbtype the fbtype
	 */
	public PlugContainer(final FBType fbtype) {
		this.fbType = fbtype;
	}

	/**
	 * Gets the children.
	 * 
	 * @return the children
	 */
	public List<IInterfaceElement> getChildren() {
		ArrayList<IInterfaceElement> plugs = new ArrayList<IInterfaceElement>();
		
		for (IInterfaceElement iInterfaceElement : fbType.getInterfaceList().getPlugs()) {
			plugs.add(iInterfaceElement);
		}		
		return plugs;
	}

	/**
	 * Gets the fb type.
	 * 
	 * @return the fb type
	 */
	public FBType getFbType() {
		return fbType;
	}
}
