/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;

/**
 * The Class OutputWithAnchor.
 */
public class OutputWithAnchor extends WithAnchor {
	

	public OutputWithAnchor(IFigure figure, int pos, EditPart editPart) {
		super(figure, pos, editPart);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.ChopboxAnchor#getLocation(org.eclipse.draw2d.geometry.Point)
	 */
	@Override
	public Point getLocation(final Point reference) {
		Rectangle r = Rectangle.SINGLETON;
		r.setBounds(getBox());
		r.translate(-1, -1);
		r.resize(1, 1);
		
		getOwner().translateToAbsolute(r);
		int leftX = (int)(r.x + r.width + (float)((10.0 * getZoomFactor()) * pos));
		int centerY = (int)(r.y + 0.5f * r.height);

		return new Point(leftX, centerY);

	}

}
