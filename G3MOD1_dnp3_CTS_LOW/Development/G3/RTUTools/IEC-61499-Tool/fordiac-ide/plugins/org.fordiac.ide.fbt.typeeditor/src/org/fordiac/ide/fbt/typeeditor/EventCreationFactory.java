/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor;

import org.fordiac.ide.gef.utilities.TemplateCreationFactory;
import org.fordiac.ide.model.data.EventType;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;


public class EventCreationFactory extends TemplateCreationFactory {

	//** buffer for the new type that has been created last. Required that after creation selection is working correctly.
	Event newType = null;
	
	public EventCreationFactory(EventType typeTemplate) {
		super(typeTemplate);
	}

	@Override
	public Object getNewObject() {
		if(null == newType){
			newType =  LibraryElementFactory.eINSTANCE.createEvent();
		}
		
		return newType;
	}
	
	@Override
	public Object getObjectType() {
		newType = null;
		return super.getObjectType();
	}
	
	
}
