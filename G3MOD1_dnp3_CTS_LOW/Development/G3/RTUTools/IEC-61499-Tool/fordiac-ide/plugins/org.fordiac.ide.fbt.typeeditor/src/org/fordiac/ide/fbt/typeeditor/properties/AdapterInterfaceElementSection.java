/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.properties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.core.runtime.Assert;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.contentoutline.ContentOutline;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.fordiac.ide.fbt.typeeditor.commands.ChangeTypeCommand;
import org.fordiac.ide.fbt.typeeditor.editors.FBTypeContentOutline;
import org.fordiac.ide.fbt.typeeditor.editors.FBTypeEditor;
import org.fordiac.ide.fbt.typeeditor.editparts.CommentEditPart;
import org.fordiac.ide.fbt.typeeditor.editparts.InterfaceEditPart;
import org.fordiac.ide.fbt.typeeditor.editparts.TypeEditPart;
import org.fordiac.ide.gef.commands.ChangeNameCommand;
import org.fordiac.ide.gef.properties.AbstractSection;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.data.DataType;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.util.IdentifierVerifyListener;
import org.fordiac.ide.util.commands.ChangeCommentCommand;

public class AdapterInterfaceElementSection extends AbstractSection {
	private Text nameText;
	private Text commentText;
	protected Combo typeCombo;
	
	protected IInterfaceElement getInputType(Object input) {
		if(input instanceof InterfaceEditPart){
			return ((InterfaceEditPart) input).getCastedModel();	
		}
		if(input instanceof TypeEditPart){
			return ((TypeEditPart) input).getCastedModel();	
		}
		if(input instanceof CommentEditPart){
			return ((CommentEditPart) input).getCastedModel();	
		}
		if(input instanceof Event){
			return (Event) input;	
		}
		if(input instanceof VarDeclaration){
			return (VarDeclaration) input;	
		}
		return null;
	}
	
	protected CommandStack getCommandStack(IWorkbenchPart part, Object input) {
		if(part instanceof FBTypeEditor){
			return ((FBTypeEditor)part).getCommandStack();
		}
		if(part instanceof ContentOutline){
			return ((FBTypeContentOutline) ((ContentOutline)part).getCurrentPage()).getCommandStack();
		}
		return null;
	}
	
	public void createControls(final Composite parent, final TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);	
		createTypeAndCommentSection(leftComposite);	
	}
	
	private void createTypeAndCommentSection(Composite parent) {
		Composite composite = getWidgetFactory().createComposite(parent);
		composite.setLayout(new GridLayout(2, false));
		composite.setLayoutData(new GridData(SWT.FILL, 0, true, false));
		getWidgetFactory().createCLabel(composite, "Name:"); 
		nameText = createGroupText(composite, true);	
		nameText.addVerifyListener(new IdentifierVerifyListener());
		nameText.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				removeContentAdapter();
				executeCommand(new ChangeNameCommand(getType(), nameText.getText()));
				addContentAdapter();
			}
		});
		getWidgetFactory().createCLabel(composite, "Comment:"); 
		commentText = createGroupText(composite, true);
		commentText.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				removeContentAdapter();
				executeCommand(new ChangeCommentCommand(getType(), commentText.getText()));
				addContentAdapter();
			}
		});
		getWidgetFactory().createCLabel(composite, "Type: ");
		typeCombo = new Combo(composite, SWT.SINGLE | SWT.READ_ONLY | SWT.BORDER);
		GridData languageComboGridData = new GridData(SWT.FILL, 0, true, false);
		typeCombo.setLayoutData(languageComboGridData);	
		typeCombo.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				DataType newType = getTypeForSelection(typeCombo.getText());
				if(null != newType){
					executeCommand(new ChangeTypeCommand((VarDeclaration)type, newType));
					//refresh();
				}
			}
			
			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
			}
		});
	}
	
	@Override
	public void setInput(final IWorkbenchPart part, final ISelection selection) {
		Assert.isTrue(selection instanceof IStructuredSelection);
		Object input = ((IStructuredSelection) selection).getFirstElement();
		commandStack = getCommandStack(part, input);
		if(null == commandStack){ //disable all fields
			nameText.setEnabled(false);
			commentText.setEnabled(false);
			typeCombo.removeAll();
		}
		setType(input);
	}	

	@Override
	public void refresh() {
		CommandStack commandStackBuffer = commandStack;
		commandStack = null;
		if(null != type) {
			nameText.setText(getType().getName() != null ? getType().getName() : "");
			commentText.setText(getType().getComment() != null ? getType().getComment() : "");
			setTypeDropdown();
		}
		commandStack = commandStackBuffer;
	}
	
	Collection<DataType> typeList; 
	
	protected void setTypeDropdown(){
		typeCombo.removeAll();
		typeList = getTypes();
		if(null != typeList){
			ArrayList<String> typeNames = new ArrayList<String>();
			for (DataType type : typeList) {
				typeNames.add(type.getName());
			}	
			Collections.sort(typeNames);
			String currTypeName = (null != ((VarDeclaration)type).getType()) ? ((VarDeclaration)type).getType().getName() : "";  //this handles gracefully the case when the adpater type could not be loaded 
			for (int i = 0; i < typeNames.size(); i++) {
				typeCombo.add(typeNames.get(i));
				if(typeNames.get(i).equals(currTypeName)){
					typeCombo.select(i);
				}	
			}	
		}
	}
	
	protected Collection<DataType> getTypes() {
		ArrayList<DataType> types = new ArrayList<DataType>();
		FBType fbType = (FBType)getType().eContainer().eContainer();
		PaletteEntry entry = fbType.getPaletteEntry();
		for (AdapterTypePaletteEntry adaptertype : TypeEditPart.getAdapterTypes(entry.getGroup().getPallete())) {
			types.add(adaptertype.getAdapterType());
		}
		return types;
	}

	protected DataType getTypeForSelection(String text) {
		if(null != typeList){
			for (DataType dataType : typeList) {
				if(dataType.getName().equals(text)){
					return dataType;
				}
			}
		}
		return null;
	}

	@Override
	protected IInterfaceElement getType() {
		return (IInterfaceElement)type;
	}
}
