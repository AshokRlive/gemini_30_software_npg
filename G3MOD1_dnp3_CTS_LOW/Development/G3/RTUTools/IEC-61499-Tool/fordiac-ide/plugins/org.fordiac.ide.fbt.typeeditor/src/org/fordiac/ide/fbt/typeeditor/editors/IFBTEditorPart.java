/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.editors;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISelectionListener;

/**
 * The Interface IFBTEditorPart.
 */
public interface IFBTEditorPart extends ISelectionListener, IEditorPart {

	/**
	 * Inform the FBTEditorPart that an element has been selected in the outline view. 
	 * If the selected element is handled by the FBTEditorPart the FBTEditorPart has to perform
	 * measure to show the selected element. By returning true the FBTypeEditor can perform
	 * measure to activate the correct tab.
	 * 
	 * @param selectedElement  the element that has been selected in the FB outline view
	 * @return true if the selected element is handled in this editor part
	 */
	boolean outlineSelectionChanged(Object selectedElement);
	
	/**This allows to coordinating multipageeditpart to share a command stack among all sub-editor pages
	 * 
	 * @param commandStack the shared command stack
	 */
	void setCommonCommandStack(CommandStack commandStack);

}
