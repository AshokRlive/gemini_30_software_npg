/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.editparts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.impl.AdapterDeclarationImpl;
import org.fordiac.ide.model.libraryElement.impl.EventImpl;
import org.fordiac.ide.model.libraryElement.impl.VarDeclarationImpl;

public abstract class AbstractInterfaceElementEditPart extends AbstractDirectEditableEditPart {
	/** The adapter. */
	private final EContentAdapter adapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			refresh();
			update();
		}

	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#activate()
	 */
	@Override
	public void activate() {
		super.activate();
		getCastedModel().eAdapters().add(adapter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		super.deactivate();
		getCastedModel().eAdapters().remove(adapter);
	}
	
	/**
	 * Checks if is input.
	 * 
	 * @return true, if is input
	 */
	public boolean isInput() {
		return getCastedModel().isIsInput();
	}

	/**
	 * Checks if is event.
	 * 
	 * @return true, if is event
	 */
	public boolean isEvent() {
		return getCastedModel() instanceof EventImpl;
	}

	/**
	 * Checks if is variable.
	 * 
	 * @return true, if is variable
	 */
	public boolean isVariable() {
		return getCastedModel() instanceof VarDeclarationImpl;
	}
	
	public boolean isAdapter() {
		return getCastedModel() instanceof AdapterDeclarationImpl;
	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	abstract public IInterfaceElement getCastedModel();

	protected abstract void update();
}
