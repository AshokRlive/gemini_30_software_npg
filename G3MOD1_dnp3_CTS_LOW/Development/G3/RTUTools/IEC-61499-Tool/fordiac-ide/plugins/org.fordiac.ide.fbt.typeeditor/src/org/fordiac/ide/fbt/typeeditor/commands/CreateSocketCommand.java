/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.fordiac.ide.application.commands.FBCreateCommand;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.SubAppType;

/**
 * The Class CreateSocketCommand.
 */
public class CreateSocketCommand extends Command {
	
	/** The data type. */
	private final AdapterTypePaletteEntry paletteEntry;

	/** The fb type. */
	private final FBType fbType;

	private AdapterDeclaration adapterDecl;

	private FBCreateCommand createSocket;

	private int pos;
	
	/**
	 * Instantiates a new creates the input variable command.
	 * 
	 * @param dataType
	 *            the data type
	 * @param fbType
	 *            the fb type
	 */
	public CreateSocketCommand(final AdapterTypePaletteEntry paletteEntry, AdapterDeclaration adapterDecl,
			final FBType fbType, int pos) {
		this.paletteEntry = paletteEntry;
		this.adapterDecl = adapterDecl;
		this.fbType = fbType;
		this.pos = pos;
	}

	/**
	 * Gets the i interface element.
	 * 
	 * @return the i interface element
	 */
	public IInterfaceElement getIInterfaceElement() {
		return adapterDecl;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		String name = paletteEntry.getLabel();
		name = NameRepository.getUniqueInterfaceElementName(adapterDecl, fbType, name);
		adapterDecl.setName(name);
		adapterDecl.setIsInput(true);
		
		if (pos < 0 || pos > fbType.getInterfaceList().getSockets().size()) {
			pos = fbType.getInterfaceList().getSockets().size();
		}
		
		fbType.getInterfaceList().getSockets().add(pos, (AdapterDeclaration) adapterDecl);		
		fbType.getInterfaceList().getInputVars().add(adapterDecl);

		//TODO move this to the manager of the composite network 	
		if (fbType instanceof CompositeFBType && !(fbType instanceof SubAppType)) {
			createSocket = new AdapterCreateCommand(paletteEntry, new Rectangle(10, 10, 10, 10), false, (CompositeFBType)fbType);
			createSocket.execute();
		}		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		fbType.getInterfaceList().getInputVars().remove(adapterDecl);
		if (createSocket != null) {
			fbType.getInterfaceList().getSockets().remove(adapterDecl);
			createSocket.undo();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		fbType.getInterfaceList().getInputVars().add(adapterDecl);
		if (createSocket != null && adapterDecl instanceof AdapterDeclaration) {
			fbType.getInterfaceList().getPlugs().add(pos,
					(AdapterDeclaration) adapterDecl);
			createSocket.redo();
		}
	}
}
