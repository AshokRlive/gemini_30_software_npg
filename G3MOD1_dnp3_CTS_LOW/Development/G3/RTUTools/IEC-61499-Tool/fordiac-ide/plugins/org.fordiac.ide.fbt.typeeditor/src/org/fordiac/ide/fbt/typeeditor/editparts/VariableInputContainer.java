/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.editparts;

import java.util.ArrayList;
import java.util.List;

import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;

/**
 * The Class VariableInputContainer.
 */
public class VariableInputContainer implements IContainerElement{

	/** The fb type. */
	private final FBType fbType;

	/**
	 * Instantiates a new variable input container.
	 * 
	 * @param fbtype the fbtype
	 */
	public VariableInputContainer(final FBType fbtype) {
		this.fbType = fbtype;
	}

	/**
	 * Gets the children.
	 * 
	 * @return the children
	 */
	public List<IInterfaceElement> getChildren() {
		ArrayList<IInterfaceElement> varInputs = new ArrayList<IInterfaceElement>();
		
		for (IInterfaceElement iInterfaceElement : fbType.getInterfaceList().getInputVars()) {
			if(!(iInterfaceElement instanceof AdapterDeclaration)){
				varInputs.add(iInterfaceElement);
			}
		}		
		return varInputs;
	}

	/**
	 * Gets the fb type.
	 * 
	 * @return the fb type
	 */
	public FBType getFbType() {
		return fbType;
	}
}
