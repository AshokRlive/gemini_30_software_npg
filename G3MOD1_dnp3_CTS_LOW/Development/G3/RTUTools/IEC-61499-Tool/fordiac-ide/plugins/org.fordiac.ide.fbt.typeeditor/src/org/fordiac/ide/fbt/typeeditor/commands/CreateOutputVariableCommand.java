/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

/**
 * The Class CreateOutputVariableCommand.
 */
public class CreateOutputVariableCommand extends Command {

	/** The fb type. */
	private final FBType fbType;

	private VarDeclaration varDecl;
	
	private int pos;

	/**
	 * Instantiates a new creates the output variable command.
	 * 
	 * @param varDecl the var declaration template created by the VarCrationFactory
	 * @param fbType the fb type
	 */
	public CreateOutputVariableCommand(VarDeclaration varDecl,
			final FBType fbType, int pos) {
		this.varDecl = varDecl;
		this.fbType = fbType;
		this.pos = pos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		String name = varDecl.getType().getName();
		name = NameRepository.getUniqueInterfaceElementName(varDecl, fbType, name);
		varDecl.setName(name);
		varDecl.setIsInput(false);
		
		if (pos < 0 || pos > fbType.getInterfaceList().getOutputVars().size()) {
			pos = fbType.getInterfaceList().getOutputVars().size();
		}
		
		fbType.getInterfaceList().getOutputVars().add(pos, varDecl);
	}

	/**
	 * Gets the i interface element.
	 * 
	 * @return the i interface element
	 */
	public IInterfaceElement getIInterfaceElement() {
		return varDecl;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		fbType.getInterfaceList().getOutputVars().remove(varDecl);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		fbType.getInterfaceList().getOutputVars().add(pos, varDecl);
	}

}
