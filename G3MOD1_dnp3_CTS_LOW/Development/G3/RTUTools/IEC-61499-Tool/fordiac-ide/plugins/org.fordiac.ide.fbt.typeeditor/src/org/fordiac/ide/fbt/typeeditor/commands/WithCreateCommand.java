/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.commands;

import java.util.Iterator;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.model.libraryElement.AdapterType;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.With;

/**
 * The Class WithCreateCommand.
 */
public class WithCreateCommand extends Command {

	/** The event. */
	private Event event;

	/** The var declaration. */
	private VarDeclaration varDeclaration;

	/** The forward creation. */
	private boolean forwardCreation;

	/** The with. */
	private With with;

	/**
	 * Gets the event.
	 * 
	 * @return the event
	 */
	public Event getEvent() {
		return event;
	}

	/**
	 * Sets the event.
	 * 
	 * @param event the new event
	 */
	public void setEvent(final Event event) {
		this.event = event;
	}

	/**
	 * Gets the var declaration.
	 * 
	 * @return the var declaration
	 */
	public VarDeclaration getVarDeclaration() {
		return varDeclaration;
	}

	/**
	 * Sets the var declaration.
	 * 
	 * @param varDeclaration the new var declaration
	 */
	public void setVarDeclaration(final VarDeclaration varDeclaration) {
		this.varDeclaration = varDeclaration;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		if (event == null || varDeclaration == null) {
			return false;
		}
		if (varDeclaration.getType() instanceof AdapterType) {
			ApplicationPlugin.statusLineErrorMessage("It is not allowed to connect Adapter!");
			return false;
		}
		for (Iterator<With> iterator = varDeclaration.getWiths().iterator(); iterator
				.hasNext();) {
			With with = iterator.next();
			if (with.eContainer().equals(event)) {
				ApplicationPlugin.statusLineErrorMessage("With Construct already exists!");
				return false;
			}
		}
		if ((varDeclaration.isIsInput() && event.isIsInput())
				|| (!varDeclaration.isIsInput() && !event.isIsInput())) {
			ApplicationPlugin.statusLineErrorMessage(null);
			return true;
		}
		return false;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		with = LibraryElementFactory.eINSTANCE.createWith();
		event.getWith().add(with);
		with.setVariables(varDeclaration);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		with.setVariables(null);
		event.getWith().remove(with);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		event.getWith().add(with);
		with.setVariables(varDeclaration);
	}

	/**
	 * Checks if is forward creation.
	 * 
	 * @return true, if is forward creation
	 */
	public boolean isForwardCreation() {
		return forwardCreation;
	}

	/**
	 * Sets the forward creation.
	 * 
	 * @param forwardCreation the new forward creation
	 */
	public void setForwardCreation(final boolean forwardCreation) {
		this.forwardCreation = forwardCreation;
	}

}
