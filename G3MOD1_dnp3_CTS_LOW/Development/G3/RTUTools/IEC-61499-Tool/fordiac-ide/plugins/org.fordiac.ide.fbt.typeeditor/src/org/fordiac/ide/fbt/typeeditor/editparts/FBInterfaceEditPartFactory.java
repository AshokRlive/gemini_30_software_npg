/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.editparts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.ui.parts.GraphicalEditor;
import org.fordiac.ide.gef.editparts.Abstract4diacEditPartFactory;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.With;

/**
 * A factory for creating FBInterfaceEditPart objects.
 */
public class FBInterfaceEditPartFactory extends Abstract4diacEditPartFactory {

    Palette systemPalette;
    protected ZoomManager zoomManager;
    
	public FBInterfaceEditPartFactory(GraphicalEditor editor, Palette systemPalette, ZoomManager zoomManager) {
		super(editor);
		this.systemPalette = systemPalette;
		this.zoomManager = zoomManager;
	}
	
	@Override
	protected EditPart getPartForElement(final EditPart context,
			final Object modelElement) {
		if (modelElement instanceof FBType && context == null) {
			return new FBTypeRootEditPart();
		}
		if (modelElement instanceof FBType
				&& context instanceof FBTypeRootEditPart) {
			return new FBTypeEditPart(zoomManager);
		}
		if (modelElement instanceof EventInputContainer
				|| modelElement instanceof EventOutputContainer
				|| modelElement instanceof VariableInputContainer
				|| modelElement instanceof VariableOutputContainer
				|| modelElement instanceof SocketContainer
			    || modelElement instanceof PlugContainer) {
			return new InterfaceContainerEditPart();
		}

		if (modelElement instanceof Event) {
			return new InterfaceEditPart();
		}
		if (modelElement instanceof VarDeclaration) {
			if (modelElement instanceof AdapterDeclaration){
				return new AdapterInterfaceEditPart(systemPalette);
			}
			else{
				return createInterfaceEditPart();
			}
		}
		if (modelElement instanceof With) {
			return new WithEditPart();
		}
		if (modelElement instanceof CommentTypeField) {
			return new CommentTypeEditPart();
		}
		if (modelElement instanceof CommentTypeField.CommentTypeSeparator) {
			return new CommentTypeSeparatorEditPart();
		}
		if (modelElement instanceof CommentField){
			return new CommentEditPart();
		}
		if (modelElement instanceof TypeField){
			return new TypeEditPart(systemPalette);
		}
		throw createEditpartCreationException(modelElement);
	}
	
	protected EditPart createInterfaceEditPart() {
		return new InterfaceEditPart();
	}
}
