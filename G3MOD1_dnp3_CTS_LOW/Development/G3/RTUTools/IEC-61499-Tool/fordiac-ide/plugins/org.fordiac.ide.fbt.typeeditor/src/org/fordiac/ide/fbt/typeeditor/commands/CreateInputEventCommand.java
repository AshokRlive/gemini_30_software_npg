/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.data.EventType;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;

/**
 * The Class CreateInputEventCommand.
 */
public class CreateInputEventCommand extends Command {

	/** The event type. */
	private final EventType eventType;

	/** The fb type. */
	private final FBType fbType;

	private Event event;

	private int pos;

	/**
	 * Instantiates a new creates the input event command.
	 * 
	 * @param eventType
	 *            the event type
	 * @param fbType
	 *            the fb type
	 *            
	 */
	public CreateInputEventCommand(final EventType eventType, final Event event,
			final FBType fbType, int pos) {
		this.eventType = eventType;
		this.event = event;
		this.fbType = fbType;
		this.pos = pos;
	}
	
	public CreateInputEventCommand(final EventType eventType, final FBType fbType, int pos) {
		this.eventType = eventType;
		this.event = LibraryElementFactory.eINSTANCE.createEvent();
		this.fbType = fbType;
		this.pos = pos;
	}

	/**
	 * Gets the i interface element.
	 * 
	 * @return the i interface element
	 */
	public IInterfaceElement getIInterfaceElement() {
		return event;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {

		String name = eventType.getName();
		name = NameRepository.getUniqueInterfaceElementName(event, fbType, name);
		event.setName(name);

		event.setIsInput(true);

		if (pos < 0 || pos > fbType.getInterfaceList().getEventInputs().size()) {
			pos = fbType.getInterfaceList().getEventInputs().size();
		}

		fbType.getInterfaceList().getEventInputs().add(pos, event);

	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		fbType.getInterfaceList().getEventInputs().remove(event);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		fbType.getInterfaceList().getEventInputs().add(pos, event);
	}

}
