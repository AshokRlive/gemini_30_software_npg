/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.properties;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.contentoutline.ContentOutline;
import org.fordiac.ide.fbt.typeeditor.editors.FBTypeContentOutline;
import org.fordiac.ide.fbt.typeeditor.editors.FBTypeEditor;
import org.fordiac.ide.fbt.typeeditor.editparts.FBTypeEditPart;
import org.fordiac.ide.fbt.typeeditor.editparts.FBTypeRootEditPart;
import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.model.libraryElement.provider.PropertiesItemProvider;
import org.fordiac.ide.util.properties.CompilableTypeInfoSection;

public class FBTypeInfoSection extends CompilableTypeInfoSection {

	@Override
	protected CommandStack getCommandStack(IWorkbenchPart part, Object input) {
		if(part instanceof FBTypeEditor){
			return ((FBTypeEditor)part).getCommandStack();
		}
		if(part instanceof ContentOutline){
			return ((FBTypeContentOutline) ((ContentOutline)part).getCurrentPage()).getCommandStack();
		}
		return null;
	}

	@Override
	protected LibraryElement getInputType(Object input) {
		if(input instanceof FBTypeEditPart){
			return ((FBTypeEditPart) input).getCastedModel();	
		}
		if(input instanceof FBTypeRootEditPart){
				return ((FBTypeRootEditPart) input).getCastedFBTypeModel();
		}
		if(input instanceof PropertiesItemProvider){
			return (LibraryElement) ((PropertiesItemProvider) input).getTarget();
		}
		return null;
	}
}
