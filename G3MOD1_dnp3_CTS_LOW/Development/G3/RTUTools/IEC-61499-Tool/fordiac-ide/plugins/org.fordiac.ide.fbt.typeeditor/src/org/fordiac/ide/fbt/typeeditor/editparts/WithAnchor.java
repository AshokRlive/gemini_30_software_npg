package org.fordiac.ide.fbt.typeeditor.editparts;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;



public class WithAnchor extends ChopboxAnchor {
	/** The pos. */
	protected final int pos;
	
	protected final EditPart editPart;

	/**
	 * Instantiates a new output with anchor.
	 * 
	 * @param figure the figure
	 * @param pos the pos
	 */
	public WithAnchor(final IFigure figure, final int pos, final EditPart editPart) {
		super(figure);
		this.pos = pos;
		this.editPart = editPart;
	}
	
	/** 
	 * Return the current zoom factor if it is a scalable editor. if not return 1.0
	 * @return zoom factor
	 */
	protected double getZoomFactor(){
		double zoom = 1.0;
		if(editPart.getRoot() instanceof ScalableFreeformRootEditPart){
			zoom = ((ScalableFreeformRootEditPart)editPart.getRoot()).getZoomManager().getZoom();
		}
		return zoom;
	}
}
