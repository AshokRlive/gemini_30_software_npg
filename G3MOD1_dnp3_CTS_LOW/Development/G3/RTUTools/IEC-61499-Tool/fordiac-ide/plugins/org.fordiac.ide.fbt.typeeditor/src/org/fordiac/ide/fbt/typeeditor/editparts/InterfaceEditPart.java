/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.editparts;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.AncestorListener;
import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.SelectionEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.eclipse.gef.tools.DirectEditManager;
import org.eclipse.jface.viewers.TextCellEditor;
import org.fordiac.ide.fbt.typeeditor.policies.DeleteInterfaceEditPolicy;
import org.fordiac.ide.fbt.typeeditor.policies.WithNodeEditPolicy;
import org.fordiac.ide.gef.commands.ChangeNameCommand;
import org.fordiac.ide.gef.draw2d.ConnectorBorder;
import org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart;
import org.fordiac.ide.gef.editparts.LabelDirectEditManager;
import org.fordiac.ide.gef.editparts.NameCellEditorLocator;
import org.fordiac.ide.gef.figures.InteractionStyleFigure;
import org.fordiac.ide.gef.policies.INamedElementRenameEditPolicy;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.With;
import org.fordiac.ide.util.IdentifierVerifyListener;

/**
 * The Class InterfaceEditPart.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class InterfaceEditPart extends AbstractInterfaceElementEditPart
		implements NodeEditPart {

	protected static final String CONNECTION_HANDLES_POLICY = "ConnectionHandlesPolicy";

	public InterfaceEditPart() {
		setConnectable(true);
	}
	
	
	/**
	 * The Class InterfaceFigure.
	 */
	public class InterfaceFigure extends Label implements
			InteractionStyleFigure {

		/**
		 * Instantiates a new interface figure.
		 */
		public InterfaceFigure() {
			super();
			setText(getINamedElement().getName());
			setBorder(new ConnectorBorder(getCastedModel()));
			setOpaque(false);
			if (isInput()) {
				setLabelAlignment(PositionConstants.LEFT);
				setTextAlignment(PositionConstants.LEFT);
			} else {
				setLabelAlignment(PositionConstants.RIGHT);
				setTextAlignment(PositionConstants.RIGHT);
			}
		}

		@Override
		public int getIntersectionStyle(Point location) {
			if (isInput()) {
				Rectangle bounds = getBounds().getCopy();
				bounds.width = 5;
				if (bounds.intersects(new Rectangle(location, new Dimension(1,
						1)))) {
					return InteractionStyleFigure.REGION_CONNECTION;
				} else {
					return InteractionStyleFigure.REGION_DRAG;
				}
			} else {
				Rectangle bounds = getBounds().getCopy();
				bounds.x = bounds.x + (bounds.width-5);
				bounds.width = 5;
				if (bounds.intersects(new Rectangle(location, new Dimension(1,
						1)))) {
					return InteractionStyleFigure.REGION_CONNECTION;
				} else {
					return InteractionStyleFigure.REGION_DRAG;
				}
				
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {

		IFigure fig = new InterfaceFigure();
		fig.addAncestorListener(new AncestorListener() {

			@Override
			public void ancestorRemoved(IFigure ancestor) {

			}

			@Override
			public void ancestorMoved(IFigure ancestor) {
				update();
			}

			@Override
			public void ancestorAdded(IFigure ancestor) {
				update();
			}

		});

		return fig;
	}

	protected void update() {

		EditPart parent = getParent();
		while (parent != null && !(parent instanceof FBTypeRootEditPart)) {
			parent = parent.getParent();
		}
		if (parent != null) {
			FBTypeRootEditPart fbcep = (FBTypeRootEditPart) parent;
			CommentTypeField commentField = fbcep
					.getCommentField(getCastedModel());
			InterfaceList interfaceList = ((InterfaceList) getCastedModel()
					.eContainer());
			if (interfaceList == null) { // can occur if the interfaceelement
				// gets
				// deleted
				return;
			}
			int nrOfInputEvents = interfaceList.getEventInputs().size();
			int nrOfOutputEvents = interfaceList.getEventOutputs().size();
			if (commentField != null) {
				Object o = getViewer().getEditPartRegistry().get(commentField);
				String label = commentField.getLabel();
				int x = 0;
				Rectangle bounds = getFigure().getBounds();
				if (o instanceof CommentTypeEditPart) {
					if (isInput()) {
						x = bounds.x
								- 15
								- FigureUtilities.getTextWidth(label,
										getNameLabel().getFont())
								- nrOfInputEvents * 10;
					} else {
						x = bounds.x + bounds.width + 15 + nrOfOutputEvents
								* 10;

					}
					((CommentTypeEditPart) o).getFigure().setLocation(
							new Point(x, bounds.y));
					((CommentTypeEditPart) o).getFigure().setSize(
							FigureUtilities.getTextWidth(label, getNameLabel()
									.getFont()), bounds.height);
				}

			}
		}
		updateWiths();
	}

	public IInterfaceElement getCastedModel() {
		return (IInterfaceElement) getModel();
	}

	/**
	 * Sets the in out connections with.
	 * 
	 * @param with
	 *            the new in out connections with
	 */

	@SuppressWarnings("rawtypes")
	public void setInOutConnectionsWith(int with) {
		for (Iterator iterator = getSourceConnections().iterator(); iterator
				.hasNext();) {
			ConnectionEditPart cep = (ConnectionEditPart) iterator.next();
			if (cep.getFigure() instanceof PolylineConnection) {
				((PolylineConnection) cep.getFigure()).setLineWidth(with);
			}
		}
		for (Iterator iterator = getTargetConnections().iterator(); iterator
				.hasNext();) {
			ConnectionEditPart cep = (ConnectionEditPart) iterator.next();
			if (cep.getFigure() instanceof PolylineConnection) {
				((PolylineConnection) cep.getFigure()).setLineWidth(with);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#
	 * createEditPolicies ()
	 */
	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
				new INamedElementRenameEditPolicy() {
					@Override
					protected Command getDirectEditCommand(
							final DirectEditRequest request) {
						if (getHost() instanceof AbstractDirectEditableEditPart) {
							ChangeNameCommand cmd = new ChangeNameCommand(
									getCastedModel(), (String) request
											.getCellEditor().getValue());
							return cmd;
						}
						return null;
					}

				});
		// allow delete of a FB
		installEditPolicy(EditPolicy.COMPONENT_ROLE,
				new DeleteInterfaceEditPolicy());

		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE,
				new WithNodeEditPolicy());

		// installEditPolicy(CONNECTION_HANDLES_POLICY,
		// new WithConnectionHandleEditPolicy());

		installEditPolicy("AdvancedSelectionRole", new SelectionEditPolicy() {

			@Override
			protected void showSelection() {

				ConnectorBorder cb = new ConnectorBorder(getCastedModel(),
						false);
				getFigure().setBorder(cb);
				setInOutConnectionsWith(2);
			}

			@Override
			protected void hideSelection() {
				ConnectorBorder cb = new ConnectorBorder(getCastedModel(),
						false);
				getFigure().setBorder(cb);
				setInOutConnectionsWith(0);
			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractEditPart#performRequest(org.eclipse.gef
	 * .Request)
	 */
	@Override
	public void performRequest(final Request request) {
		// REQ_DIRECT_EDIT -> first select 0.4 sec pause -> click -> edit
		// REQ_OPEN -> doubleclick

		if (request.getType() == RequestConstants.REQ_OPEN) {
			// Perform double click as direct edit
			request.setType(RequestConstants.REQ_DIRECT_EDIT);
		}
		super.performRequest(request);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractGraphicalEditPart#getModelSourceConnections
	 * ()
	 */
	@Override
	protected List<With> getModelSourceConnections() {
		if (isEvent()) {
			return ((Event) getModel()).getWith();
		}
		return Collections.emptyList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractGraphicalEditPart#getModelTargetConnections
	 * ()
	 */
	@Override
	protected List<With> getModelTargetConnections() {
		if (isVariable()) {
			return ((VarDeclaration) getModel()).getWiths();
		}
		return Collections.emptyList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.
	 * ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(
			final ConnectionEditPart connection) {
		if (isInput()) {
			int pos = 1;
			pos = calculateInputWithPos(connection);
			return new InputWithAnchor(getFigure(), pos, this);

		} else {
			int pos = 1;

			pos = calculateOutputWithPos(connection);
			return new OutputWithAnchor(getFigure(), pos, this);
		}
	}

	private int calculateOutputWithPos(final ConnectionEditPart connection) {
		int pos;
		With with = (With) connection.getModel();
		Event event = (Event) with.eContainer();

		InterfaceList interfaceList = (InterfaceList) event.eContainer();
		pos = interfaceList.getEventOutputs().indexOf(event) + 1;
		return pos;
	}

	private int calculateInputWithPos(final ConnectionEditPart connection) {
		int pos;
		With with = (With) connection.getModel();
		Event event = (Event) with.eContainer();
		InterfaceList interfaceList = (InterfaceList) event.eContainer();
		pos = interfaceList.getEventInputs().indexOf(event) + 1;
		return pos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.
	 * Request)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(final Request request) {
		return new ChopboxAnchor(getFigure());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.
	 * ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(
			final ConnectionEditPart connection) {
		if (isInput()) {
			int pos = 1;
			pos = calculateInputWithPos(connection);
			return new InputWithAnchor(getFigure(), pos, this);
		} else {
			int pos = 1;
			pos = calculateOutputWithPos(connection);
			return new OutputWithAnchor(getFigure(), pos, this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.
	 * Request)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(final Request request) {
		return new ChopboxAnchor(getFigure());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#getNameLabel
	 * ()
	 */
	@Override
	public Label getNameLabel() {
		return (Label) getFigure();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#getINamedElement
	 * ()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel();
	}

	private void updateWiths() {
		if (getCastedModel() instanceof Event) {
			if (null != sourceConnections) {
				for (Object con : sourceConnections) {
					WithEditPart with = (WithEditPart) con;
					with.updateWithPos();
				}
			}
		}
	}

	/**
	 * Gets the manager.
	 * 
	 * @return the manager
	 */
	@Override
	public DirectEditManager getManager() {
		if (manager == null) {
			Label l = getNameLabel();
			manager = new LabelDirectEditManager(this, TextCellEditor.class,
					new NameCellEditorLocator(l), l,
					new IdentifierVerifyListener()) {

				@Override
				protected void bringDown() {
					if (getEditPart() instanceof InterfaceEditPart) {
						((InterfaceEditPart) getEditPart()).refreshName();
					}
					super.bringDown();
				}
			}; // ensures that interface elements are only valid identifiers
		}

		return manager;
	}
}
