package org.fordiac.ide.fbt.typeeditor.editparts;

import org.fordiac.ide.model.libraryElement.IInterfaceElement;

public class CommentField {
	private final IInterfaceElement referencedElement;

	/**
	 * Gets the referenced element.
	 * 
	 * @return IInterfaceElement - the referenced
	 */
	public IInterfaceElement getReferencedElement() {
		return referencedElement;
	}

	/**
	 * Helper object to display comment of an in/output.
	 * 
	 * @param referencedElement the referenced element
	 */
	public CommentField(IInterfaceElement referencedElement) {
		this.referencedElement = referencedElement;
	}
	
	/**
	 * Gets the label.
	 * 
	 * @return the label
	 */
	public String getLabel() {
		return getReferencedElement().getComment();
	}
}
