/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ComponentEditPolicy;
import org.eclipse.gef.requests.GroupRequest;
import org.fordiac.ide.fbt.typeeditor.commands.DeleteInterfaceCommand;
import org.fordiac.ide.fbt.typeeditor.editparts.InterfaceEditPart;

/**
 * The Class DeleteInterfaceEditPolicy.
 */
public class DeleteInterfaceEditPolicy extends ComponentEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.ComponentEditPolicy#getDeleteCommand(org.eclipse.gef.requests.GroupRequest)
	 */
	@Override
	protected Command getDeleteCommand(final GroupRequest request) {
		if (getHost() instanceof InterfaceEditPart) {
			InterfaceEditPart iep = (InterfaceEditPart) getHost();
			DeleteInterfaceCommand c = new DeleteInterfaceCommand(iep
					.getCastedModel());
			return c;
		}
		return super.getDeleteCommand(request);
	}

}
