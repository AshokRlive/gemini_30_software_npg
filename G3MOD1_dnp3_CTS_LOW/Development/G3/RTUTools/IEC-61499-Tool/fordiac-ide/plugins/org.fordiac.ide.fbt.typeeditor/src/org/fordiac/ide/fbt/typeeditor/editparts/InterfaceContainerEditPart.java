/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.editparts;

import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.TextUtilities;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.fordiac.ide.fbt.typeeditor.policies.EventInputContainerLayoutEditPolicy;
import org.fordiac.ide.fbt.typeeditor.policies.EventOutputContainerLayoutEditPolicy;
import org.fordiac.ide.fbt.typeeditor.policies.PlugContainerLayoutEditPolicy;
import org.fordiac.ide.fbt.typeeditor.policies.SocketContainerLayoutEditPolicy;
import org.fordiac.ide.fbt.typeeditor.policies.VariableInputContainerLayoutEditPolicy;
import org.fordiac.ide.fbt.typeeditor.policies.VariableOutputContainerLayoutEditPolicy;

/**
 * The Class VariableOutputContainerEditPart.
 */
public class InterfaceContainerEditPart extends AbstractGraphicalEditPart {

	/**
	 * The Class VariableOutputContainerFigure.
	 */
	public class InterfaceContainerFigure extends Figure {

		/**
		 * Instantiates a new variable output container figure.
		 */
		public InterfaceContainerFigure() {
			FlowLayout layout = new FlowLayout();
			layout.setMajorSpacing(0);
			layout.setMinorSpacing(0);
			layout.setHorizontal(false);
			layout.setStretchMinorAxis(true);
			if (getModel() instanceof VariableInputContainer || 
					getModel() instanceof SocketContainer){
			  layout.setMinorAlignment(FlowLayout.ALIGN_BOTTOMRIGHT);
			}
			setLayoutManager(layout);
			setPreferredSize(30, 10);
		}
	}

	/** The econtent adapter. */
	private final EContentAdapter econtentAdapter = new EContentAdapter() {

		@Override
		public void notifyChanged(Notification notification) {
			refreshChildren();
			super.notifyChanged(notification);
		}

	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		super.activate();
		getCastedModel().getFbType().eAdapters().add(econtentAdapter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		super.deactivate();
		getCastedModel().getFbType().eAdapters().remove(econtentAdapter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		return new InterfaceContainerFigure();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		if (getModel() instanceof EventInputContainer) {
			installEditPolicy(EditPolicy.LAYOUT_ROLE,
					new EventInputContainerLayoutEditPolicy());
		}
		if (getModel() instanceof EventOutputContainer) {
			installEditPolicy(EditPolicy.LAYOUT_ROLE,
					new EventOutputContainerLayoutEditPolicy());
		}
		if (getModel() instanceof VariableInputContainer){
			installEditPolicy(EditPolicy.LAYOUT_ROLE,
					new VariableInputContainerLayoutEditPolicy());
		}
		if(getModel() instanceof SocketContainer){
			installEditPolicy(EditPolicy.LAYOUT_ROLE,
					new SocketContainerLayoutEditPolicy());
		}
		if (getModel() instanceof VariableOutputContainer ) {
			installEditPolicy(EditPolicy.LAYOUT_ROLE,
					new VariableOutputContainerLayoutEditPolicy());
		}
		if(getModel() instanceof PlugContainer) {
			installEditPolicy(EditPolicy.LAYOUT_ROLE,
					new PlugContainerLayoutEditPolicy());
		}

	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public IContainerElement getCastedModel() {
		return (IContainerElement) getModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected List getModelChildren() {
		return getCastedModel().getChildren();
	}

	@Override
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (getContentPane().getChildren().size() == 0) {
			getContentPane().setPreferredSize(null);
		}
		super.addChildVisual(childEditPart, index);
	}

	@Override
	protected void removeChildVisual(EditPart childEditPart) {
		super.removeChildVisual(childEditPart);
		if (getContentPane().getChildren().size() == 0) {
			Dimension dim = TextUtilities.INSTANCE.getTextExtents("INT", getContentPane().getFont());
			dim.height =(int) (dim.height * 0.66);
			getContentPane().setPreferredSize(dim);
		}
	}
}
