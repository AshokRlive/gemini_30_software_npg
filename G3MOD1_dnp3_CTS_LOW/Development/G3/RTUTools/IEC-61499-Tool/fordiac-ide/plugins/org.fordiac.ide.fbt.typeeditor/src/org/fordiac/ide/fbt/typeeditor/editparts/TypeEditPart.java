package org.fordiac.ide.fbt.typeeditor.editparts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.DirectEditRequest;
import org.eclipse.gef.tools.DirectEditManager;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.fordiac.ide.fbt.typeeditor.commands.ChangeTypeCommand;
import org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart;
import org.fordiac.ide.gef.editparts.ComboCellEditorLocator;
import org.fordiac.ide.gef.editparts.ComboDirectEditManager;
import org.fordiac.ide.gef.policies.INamedElementRenameEditPolicy;
import org.fordiac.ide.model.Palette.AdapterTypePaletteEntry;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.Palette.PaletteGroup;
import org.fordiac.ide.model.data.DataType;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.typelibrary.DataTypeLibrary;
import org.fordiac.ide.typelibrary.TypeLibrary;

public class TypeEditPart extends AbstractInterfaceElementEditPart implements
EditPart {

	Palette systemPalette;
	
	public TypeEditPart(Palette systemPalette) {
		super();
		this.systemPalette = systemPalette;
	}
	
	/**
	 * The Class InterfaceFigure.
	 */
	public class TypeFigure extends Label {
		/**
		 * Instantiates a new interface figure.
		 */
		public TypeFigure() {
			super();
		}
		
		@Override
		public void setText(String s) {
			if (getCastedModel() instanceof VarDeclaration){
				//if is array append array size
				VarDeclaration varDec =  (VarDeclaration)getCastedModel();
				if(varDec.isArray()){
					s = s + "[" + varDec.getArraySize() + "]";
				}
			}
			super.setText(s);
		}

	}	
	
	
	private Label comment;

	public IInterfaceElement getCastedModel() {
		return (IInterfaceElement) ((TypeField)getModel()).getReferencedElement();
	}

	@Override
	protected IFigure createFigure() {
		comment = new TypeFigure();
		update();
		//comment.setSize(-1, -1);
		//comment.set
		return comment;
	}

	protected void update() {
		comment.setText(getTypeName());
	}
	
	private String getTypeName(){
		return ((TypeField) getModel()).getLabel();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#
	 * createEditPolicies ()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
				new INamedElementRenameEditPolicy() {
					@Override
					protected void showCurrentEditValue(
							DirectEditRequest request) {
						// nothing to do
					}
			
					@Override
					protected Command getDirectEditCommand(
							final DirectEditRequest request) {
						if (getHost() instanceof AbstractDirectEditableEditPart) {
							int index = (Integer)request.getCellEditor().getValue();
							if(index > 0 && index < ((ComboDirectEditManager)getManager()).getComboBox().getItemCount()){
								String typeName = ((ComboDirectEditManager)getManager()).getComboBox().getItem(index);								
								ChangeTypeCommand cmd;
								if(getCastedModel() instanceof AdapterDeclaration){
									//TODO change to own command in order to update cfb internals
									cmd = new ChangeTypeCommand(
											(VarDeclaration) getCastedModel(),
											getAdapterTypeEntry(systemPalette, typeName).getAdapterType());
								}
								else{
									cmd = new ChangeTypeCommand(
										(VarDeclaration) getCastedModel(),
										DataTypeLibrary.getInstance().getType(typeName));
								}
								return cmd;
							}
						}
						return null;
					}

				});
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractEditPart#performRequest(org.eclipse.gef
	 * .Request)
	 */
	@Override
	public void performRequest(final Request request) {
		// REQ_DIRECT_EDIT -> first select 0.4 sec pause -> click -> edit
		// REQ_OPEN -> doubleclick
		
		if(request.getType() == RequestConstants.REQ_OPEN){
			//transform doubleclick to direct edit
			request.setType(RequestConstants.REQ_DIRECT_EDIT);
		}

		if(request.getType() == RequestConstants.REQ_DIRECT_EDIT){
			//allow direct edit only for VarDeclarations
			if (getCastedModel() instanceof VarDeclaration){
				super.performRequest(request);
			}			
		}
		else{
			super.performRequest(request);
		}
	}
	
	/**
	 * Gets the manager.
	 * 
	 * @return the manager
	 */
	public DirectEditManager getManager() {
		if (manager == null) {
			manager = new ComboDirectEditManager(this, ComboBoxCellEditor.class,
					new ComboCellEditorLocator(comment), comment);
		}

		return manager;
	}

	/**
	 * performs the directEdit.
	 */
	public void performDirectEdit() {
		//CCombo combo = getManager().getComboBox();
		//First update the list of available types	

		ArrayList<String> dataTypeNames = new ArrayList<String>();
		
		if(getCastedModel() instanceof AdapterDeclaration){
			for (AdapterTypePaletteEntry adapterType : getAdapterTypes(systemPalette)) {
				dataTypeNames.add(adapterType.getLabel());	
			}			
			Collections.sort(dataTypeNames);
		}
		else{		
			for (DataType dataType : DataTypeLibrary.getInstance().getDataTypesSorted()) {
				dataTypeNames.add(dataType.getName());
			}			
		}
		
		
		((ComboDirectEditManager)getManager()).updateComboData(dataTypeNames);		
		((ComboDirectEditManager)getManager()).setSelectedItem(dataTypeNames.indexOf(getTypeName()));
		getManager().show();
	}
	
	public static AdapterTypePaletteEntry getAdapterTypeEntry(final Palette systemPalette, final String typeName){
		List<PaletteEntry> entries = systemPalette.getTypeEntries(typeName);
		
		if(!entries.isEmpty()){
			return ((AdapterTypePaletteEntry) entries.get(0));
		}	
		
		return null;
	}
	
	public static ArrayList<AdapterTypePaletteEntry> getAdapterTypes(final Palette systemPalette){
		ArrayList<AdapterTypePaletteEntry> retVal = new ArrayList<AdapterTypePaletteEntry>();
		
		Palette pal = systemPalette;
		if(null == pal){
			pal = TypeLibrary.getInstance().getPalette();
		}
				
		retVal.addAll(getAdapterGroup(pal.getRootGroup()));
		
		return retVal;
	}
	
	private static ArrayList<AdapterTypePaletteEntry> getAdapterGroup(final org.fordiac.ide.model.Palette.PaletteGroup group){
		ArrayList<AdapterTypePaletteEntry> retVal = new ArrayList<AdapterTypePaletteEntry>();
	
		for (Iterator<PaletteGroup> iterator = group.getSubGroups().iterator(); iterator
			.hasNext();) {
			PaletteGroup paletteGroup = iterator.next();
			retVal.addAll(getAdapterGroup(paletteGroup));		
		}
		
		retVal.addAll(getAdapterGroupEntries(group));
		
		return retVal;
	}
	
	
	private static ArrayList<AdapterTypePaletteEntry> getAdapterGroupEntries(final org.fordiac.ide.model.Palette.PaletteGroup group){
		ArrayList<AdapterTypePaletteEntry> retVal = new ArrayList<AdapterTypePaletteEntry>();
		
		for (PaletteEntry entry : group.getEntries()) {
			if(entry instanceof AdapterTypePaletteEntry){
				retVal.add((AdapterTypePaletteEntry) entry);				
			}
		}
		return retVal;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#getNameLabel
	 * ()
	 */
	@Override
	public Label getNameLabel() {
		return (Label) getFigure();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractDirectEditableEditPart#getINamedElement
	 * ()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel();
	}
	
}
