package org.fordiac.ide.fbt.typeeditor.editparts;

import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

public class TypeField {
	private final IInterfaceElement referencedElement;

	/**
	 * Gets the referenced element.
	 * 
	 * @return IInterfaceElement - the referenced
	 */
	public IInterfaceElement getReferencedElement() {
		return referencedElement;
	}

	/**
	 * Helper object to display type of an in/output.
	 * 
	 * @param referencedElement the referenced element
	 */
	public TypeField(IInterfaceElement referencedElement) {
		this.referencedElement = referencedElement;
	}
	
	/**
	 * Gets the label.
	 * 
	 * @return the label
	 */
	public String getLabel() {
		String type = "";
		if (getReferencedElement() instanceof Event) {
//			Event event = (Event) getReferencedElement();
			type = "Event";
		} else if (getReferencedElement() instanceof VarDeclaration) {
			VarDeclaration varDecl = (VarDeclaration) getReferencedElement();
			type = varDecl.getTypeName();
		}
		return type;
	}
	
	public String getArrayLabel(){
		String typeLabel = getLabel();
		if (referencedElement instanceof VarDeclaration){
			//if is array append array size
			VarDeclaration varDec =  (VarDeclaration)referencedElement;
			if(varDec.isArray()){
				typeLabel = typeLabel + "[" + varDec.getArraySize() + "]";
			}
		}
		return typeLabel;
	}
}
