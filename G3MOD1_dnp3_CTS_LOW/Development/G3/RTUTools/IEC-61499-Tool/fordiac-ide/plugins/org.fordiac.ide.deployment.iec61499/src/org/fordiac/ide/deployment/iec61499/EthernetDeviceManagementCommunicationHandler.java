/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment.iec61499;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.MessageFormat;

import org.fordiac.ide.deployment.AbstractDeviceManagementCommunicationHandler;
import org.fordiac.ide.deployment.Activator;
import org.fordiac.ide.deployment.exceptions.DisconnectException;
import org.fordiac.ide.deployment.exceptions.InvalidMgmtID;
import org.fordiac.ide.deployment.iec61499.preferences.HoloblocDeploymentPreferences;

public class EthernetDeviceManagementCommunicationHandler extends
		AbstractDeviceManagementCommunicationHandler {
	
	/** The Constant ASN1_TAG_IECSTRING. */
	private static final int ASN1_TAG_IECSTRING = 80;

	/**
	 * The Class MgrInformation.
	 */
	private class MgrInformation {

		/** The IP. */
		public String iP;

		/** The port. */
		public Integer port;

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return iP + ":" + port;
		}
	}

	/** The mgr info. */
	private MgrInformation mgrInfo;

	/** The socket. */
	private Socket socket;

	/** The output stream. */
	private DataOutputStream outputStream;

	/** The input stream. */
	private DataInputStream inputStream;

	@Override
	public void connect(String address) throws InvalidMgmtID,
			UnknownHostException, IOException {
		mgrInfo = getValidMgrInformation(address);
		socket = new Socket();
		int timeout = Activator.getDefault().getPreferenceStore().getInt(HoloblocDeploymentPreferences.CONNECTION_TIMEOUT);
		
		SocketAddress sockaddr = new InetSocketAddress(mgrInfo.iP, mgrInfo.port);
		
		if (timeout > 0) {
			socket.connect(sockaddr, timeout);
			socket.setSoTimeout(timeout);
		}else{
			socket.connect(sockaddr, 3000); //if we have no timeout given take 3s as timeout
		}
		
		outputStream = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
		inputStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
	}
	
	@Override
	public void disconnect() throws DisconnectException {
		try {
			outputStream.close();
			inputStream.close();
			socket.close();
			// TODO check this sleep!
			Thread.sleep(50);
		} catch (IOException e) {
			throw new DisconnectException(MessageFormat.format(
					Messages.DeploymentExecutor_DisconnectFailed, new Object[] {}));
		} catch (InterruptedException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
	}

	@Override
	public void sendREQ(String destination, String request)
			throws IOException {
		if (outputStream != null && inputStream != null) {
			outputStream.writeByte(ASN1_TAG_IECSTRING);
			outputStream.writeShort(destination.length());
			outputStream.writeBytes(destination);
			// out.flush();
			// Do NOT flush here, all data should be sent within 1 ethernet
			// frame
			// in case packet fragmentation is not properly handled by server

			outputStream.writeByte(ASN1_TAG_IECSTRING);
			outputStream.writeShort(request.length());
			outputStream.writeBytes(request);
			outputStream.flush();
			
			String info = mgrInfo.toString();
			if(!destination.equals("")){
				info += ": " + destination;
			}

			postCommandSent(info, destination, request);
			String response = ""; //$NON-NLS-1$

			@SuppressWarnings("unused")
			byte b = inputStream.readByte();

			short size = inputStream.readShort();

			for (int i = 0; i < size; i++) {
				response += (char) inputStream.readByte();
			}
			if (!response.equals("")) {
				responseReceived(response, info);
			}
			
			// TODO error handling
		}
	}
	
	/**
	 * returns a valid MgrInformation if the mgrID contains valid destination
	 * string (e.g. localhost:61499) else null is returned valid ports are between
	 * 1024 - 65535
	 * 
	 * @param mgrID
	 *          the mgr id
	 * 
	 * @return the valid mgr information
	 * @throws InvalidMgmtID when the given ide is no valid ip address port compbination
	 */
	private MgrInformation getValidMgrInformation(final String mgrID) throws InvalidMgmtID {
		if (null != mgrID) {
			String id = mgrID;
			if (id.startsWith("\"")){ 
				id = id.substring(1, id.length());
			}
			if(id.endsWith("\"")) {
				id = id.substring(0, id.length()-1);
			}
			String[] splitID = id.split(":"); //$NON-NLS-1$
			Integer port;
			MgrInformation mgrInfo = new MgrInformation();
			if (splitID.length == 2) {
				try {
					InetAddress adress = InetAddress.getByName(splitID[0]);
					mgrInfo.iP = adress.getHostAddress();
					port = Integer.parseInt(splitID[1]);
				} catch (NumberFormatException | UnknownHostException  e) {				
					throw new InvalidMgmtID(mgrID);
				}
				if (1023 < port && port < 65536) {
					mgrInfo.port = port;
					return mgrInfo;
				}
			}
		}
		throw new InvalidMgmtID(mgrID);
	}

}
