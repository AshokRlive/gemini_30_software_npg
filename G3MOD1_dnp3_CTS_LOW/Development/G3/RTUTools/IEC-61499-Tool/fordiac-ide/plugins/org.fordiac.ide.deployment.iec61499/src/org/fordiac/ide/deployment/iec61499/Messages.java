/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment.iec61499;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.deployment.iec61499.messages"; //$NON-NLS-1$

	/** The Deployment executor_ create data connection failed. */
	public static String DeploymentExecutor_CreateDataConnectionFailed;

	/** The Deployment executor_ create event connection failed. */
	public static String DeploymentExecutor_CreateEventConnectionFailed;

	/** The Deployment executor_ create fb instance failed. */
	public static String DeploymentExecutor_CreateFBInstanceFailed;
	
	/** The Deployment executor_ create fb instance failed if type is null. */
	public static String DeploymentExecutor_CreateFBInstanceFailedNoTypeFound;

	/** The Deployment executor_ create resource failed. */
	public static String DeploymentExecutor_CreateResourceFailed;
	
	/** The Deployment executor_ connection to device has been lost. */
	public static String DeploymentExecutor_DeviceConnectionClosed;

	/** The Deployment executor_ create resource instance. */
	public static String DeploymentExecutor_CreateResourceInstance;

	/** The Deployment executor_ create fb instance. */
	public static String DeploymentExecutor_CreateFBInstance;

	/** The Deployment executor_ create event connection. */
	public static String DeploymentExecutor_CreateEventConnection;

	/** The Deployment executor_ create data connection. */
	public static String DeploymentExecutor_CreateDataConnection;

	/** The Deployment executor_ starting resource failed. */
	public static String DeploymentExecutor_StartingResourceFailed;
	
	/** The Deployment executor_ starting fb failed. */
	public static String DeploymentExecutor_StartingFBFailed;

	/** The Deployment executor_ starting device failed. */
	public static String DeploymentExecutor_StartingDeviceFailed;

	/** The Deployment executor_ write fb parameter failed. */
	public static String DeploymentExecutor_WriteFBParameterFailed;

	/** The Deployment executor_ write parameter. */
	public static String DeploymentExecutor_WriteParameter;

	/** The Deployment executor_ start. */
	public static String DeploymentExecutor_Start;

	/** The Deployment executor_ start fb. */
	public static String DeploymentExecutor_StartFB;

	/** The Deployment executor_ kill fb. */
	public static String DeploymentExecutor_KillFB;
	
	/** The Deployment executor_ kill device. */
	public static String DeploymentExecutor_KillDevice;

	/** The Deployment executor_ stop fb. */
	public static String DeploymentExecutor_StopFB;

	/** The Deployment executor_ delete fb. */
	public static String DeploymentExecutor_DeleteFB;
	
	public static String DeploymentExecutor_DeleteConnection;

	/** The Deployment executor_ write resource parameter failed. */
	public static String DeploymentExecutor_WriteResourceParameterFailed;

	/** The Deployment executor_ write device parameter failed. */
	public static String DeploymentExecutor_WriteDeviceParameterFailed;

	/** The Deployment executor_ disconnect failed. */
	public static String DeploymentExecutor_DisconnectFailed;

	/** The Deployment executor_ kill fb failed. */
	public static String DeploymentExecutor_KillFBFailed;
	
	/** The Deployment executor_ kill device failed. */
	public static String DeploymentExecutor_KillDeviceFailed;

	/** The Deployment executor_ delete fb failed. */
	public static String DeploymentExecutor_DeleteFBFailed;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
		// empty private constructor
	}
}
