/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment.iec61499;

import java.io.EOFException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashSet;

import org.fordiac.ide.deployment.AbstractDeviceManagementCommunicationHandler;
import org.fordiac.ide.deployment.IDeploymentExecutor;
import org.fordiac.ide.deployment.exceptions.CreateConnectionException;
import org.fordiac.ide.deployment.exceptions.CreateFBInstanceException;
import org.fordiac.ide.deployment.exceptions.CreateResourceInstanceException;
import org.fordiac.ide.deployment.exceptions.StartException;
import org.fordiac.ide.deployment.exceptions.WriteDeviceParameterException;
import org.fordiac.ide.deployment.exceptions.WriteFBParameterException;
import org.fordiac.ide.deployment.exceptions.WriteResourceParameterException;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.Device;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

/**
 * The Class DeploymentExecutor.
 */
public class DeploymentExecutor implements IDeploymentExecutor {

	/** The Constant PROFILE_NAME. */
	private static final String PROFILE_NAME = "HOLOBLOC"; //$NON-NLS-1$

	/** The generic FBs. */
	private final HashSet<String> genFBs = new HashSet<String>();
	
	AbstractDeviceManagementCommunicationHandler devMgmCommHandler = null;
	
	
	@Override
	public void setDeviceManagementCommunicationHandler(
			AbstractDeviceManagementCommunicationHandler handler) {
		devMgmCommHandler = handler;
	}
	
	@Override
	public AbstractDeviceManagementCommunicationHandler getDevMgmComHandler() {
		return devMgmCommHandler;
	}

	/**
	 * Instantiates a new deployment executor.
	 */
	public DeploymentExecutor() {

		genFBs.add("PUBLISH"); //$NON-NLS-1$
		genFBs.add("SUBSCRIBE");

		genFBs.add("PUBL");
		genFBs.add("SUBL");

		genFBs.add("SERVER");
		genFBs.add("CLIENT");
	}

	

	/**
	 * Gets the valid type.
	 * 
	 * @param fb
	 *          the fb
	 * 
	 * @return the valid type or null if fb or its type is null
	 */
	private String getValidType(final FB fb) {
		if (fb != null && fb.getPaletteEntry() != null) {
			return fb.getPaletteEntry().getLabel();
		}
		return null;
	}

	/** The id. */
	private int id = 0;


	public void sendREQ(final String destination, final String request) throws IOException {
		if(null != devMgmCommHandler){
			devMgmCommHandler.sendREQ(destination, request);	
		}
		//TODO maybe an error message would be good
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.deployment.IDeploymentExecutor#supports(java.lang.String)
	 */
	@Override
	public boolean supports(final String profile) {
		return profile.equalsIgnoreCase(getProfileName());
	}
	
	public String getProfileName(){
		return PROFILE_NAME;
	}
	
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.deployment.IDeploymentExecutor#createResource(org.fordiac
	 * .ide.model.libraryElement.Resource)
	 */
	@Override
	public void createResource(final Resource resource)
			throws CreateResourceInstanceException {
		String request = MessageFormat.format(
				Messages.DeploymentExecutor_CreateResourceInstance, new Object[] {
						this.id++, resource.getName(), resource.getResourceType().getName() });
		try {
			sendREQ("", request);
		}catch (EOFException e){
			throw new CreateResourceInstanceException(MessageFormat.format(
					Messages.DeploymentExecutor_DeviceConnectionClosed,
					new Object[] { resource.getName() }));
		}
		catch (IOException e) {
			throw new CreateResourceInstanceException(MessageFormat.format(
					Messages.DeploymentExecutor_CreateResourceFailed,
					new Object[] { resource.getName() }));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.deployment.IDeploymentExecutor#createFBInstance(org.fordiac
	 * .ide.model.libraryElement.FB,
	 * org.fordiac.ide.model.libraryElement.Resource)
	 */
	@Override
	public void createFBInstance(final FB fb, final Resource res)
			throws CreateFBInstanceException {
		String fbType = getValidType(fb);
		if (fbType == null || fbType.equals("")) {
			throw new CreateFBInstanceException((MessageFormat.format(
					Messages.DeploymentExecutor_CreateFBInstanceFailedNoTypeFound, new Object[] { fb
							.getName() })));
		}
		String request = MessageFormat.format(
				Messages.DeploymentExecutor_CreateFBInstance, new Object[] { this.id++,
						fb.getName(), fbType });
		try {
			sendREQ(res.getName(), request);
		} catch (IOException e) {
			throw new CreateFBInstanceException(MessageFormat.format(
					Messages.DeploymentExecutor_CreateFBInstanceFailed, new Object[] { fb
							.getName() }));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.deployment.IDeploymentExecutor#writeResourceParameter
	 * (org.fordiac.ide.model.libraryElement.Resource, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void writeResourceParameter(final Resource resource,
			final String parameter, final String value)
			throws WriteResourceParameterException {
		
		String encodedValue = encodeXMLChars(value);
		String request = generateWriteParmaRequest(resource.getName(), parameter, encodedValue); 
		try {
			sendREQ("", request);
		} catch (IOException e) {
			throw new WriteResourceParameterException(MessageFormat.format(
					Messages.DeploymentExecutor_WriteResourceParameterFailed,
					new Object[] { resource.getName(), parameter }));
		}

	}

	protected String generateWriteParmaRequest(final String targetElementName,
			final String parameter, final String value) {
		return MessageFormat.format(
				getWriteParameterMessage(), new Object[] { this.id++,
						value, targetElementName + "." + parameter }); //$NON-NLS-1$
	}

	protected String getWriteParameterMessage() {
		return Messages.DeploymentExecutor_WriteParameter;
	}

	/** 
	 * 
	 * @param value
	 * @return
	 */
	private String encodeXMLChars(final String value) {
		String retVal = value;
		
		retVal= retVal.replaceAll("\"", "&quot;");
		retVal= retVal.replaceAll("'", "&apos;");
		retVal= retVal.replaceAll(">", "&lt;");
		retVal= retVal.replaceAll(">", "&gt;");
		
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.deployment.IDeploymentExecutor#writeFBParameter(org.fordiac
	 * .ide.model.libraryElement.Resource,
	 * org.fordiac.ide.model.libraryElement.Value,
	 * org.fordiac.ide.model.libraryElement.FB,
	 * org.fordiac.ide.model.libraryElement.VarDeclaration)
	 */
	@Override
	public void writeFBParameter(final Resource resource, final String value,
			final FB fb, final VarDeclaration varDecl)
			throws WriteFBParameterException {
		String encodedValue = encodeXMLChars(value);
		String request = generateWriteParmaRequest(fb.getName(), varDecl.getName(), encodedValue);
		try {
			sendREQ(resource.getName(), request);
		} catch (IOException e) {
			throw new WriteFBParameterException(MessageFormat.format(
					Messages.DeploymentExecutor_WriteFBParameterFailed, new Object[] {
							resource.getName(), varDecl.getName() }));
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.deployment.IDeploymentExecutor#createEventConnection(
	 * org.fordiac.ide.model.libraryElement.Resource,
	 * org.fordiac.ide.model.libraryElement.EventConnection)
	 */
	@Override
	public void createEventConnection(final Resource resource,
			final EventConnection eventCon) throws CreateConnectionException {
		if (eventCon.getSource() != null
				&& eventCon.getSource().eContainer() != null
				&& eventCon.getSource().eContainer().eContainer() != null
				&& eventCon.getDestination() != null
				&& eventCon.getDestination().eContainer() != null
				&& eventCon.getDestination().eContainer().eContainer() != null) {
			FB sourceFB = (FB) eventCon.getSource().eContainer().eContainer();
			FB destFB = (FB) eventCon.getDestination().eContainer().eContainer();

			String request = MessageFormat.format(
					Messages.DeploymentExecutor_CreateEventConnection, new Object[] {
							this.id++, sourceFB.getName() + "." //$NON-NLS-1$
									+ eventCon.getSource().getName(), destFB.getName() + "." //$NON-NLS-1$
									+ eventCon.getDestination().getName() });
			try {
				sendREQ(resource.getName(), request);
			} catch (IOException e) {
				throw new CreateConnectionException(MessageFormat.format(
						Messages.DeploymentExecutor_CreateEventConnectionFailed,
						new Object[] {}));
			}
		} else {
			throw new CreateConnectionException(MessageFormat.format(
					Messages.DeploymentExecutor_CreateEventConnectionFailed,
					new Object[] {}));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.deployment.IDeploymentExecutor#createDataConnection(org
	 * .fordiac.ide.model.libraryElement.Resource,
	 * org.fordiac.ide.model.libraryElement.DataConnection)
	 */
	@Override
	public void createDataConnection(final Resource resource,
			final DataConnection dataCon) throws CreateConnectionException {
		if (dataCon.getSource() != null && dataCon.getSource().eContainer() != null
				&& dataCon.getSource().eContainer().eContainer() != null
				&& dataCon.getDestination() != null
				&& dataCon.getDestination().eContainer() != null
				&& dataCon.getDestination().eContainer().eContainer() != null) {
			FB sourceFB = (FB) dataCon.getSource().eContainer().eContainer();
			FB destFB = (FB) dataCon.getDestination().eContainer().eContainer();

			String request = MessageFormat.format(
					Messages.DeploymentExecutor_CreateDataConnection, new Object[] {
							this.id++, sourceFB.getName() + "." //$NON-NLS-1$
									+ dataCon.getSource().getName(), destFB.getName() + "." //$NON-NLS-1$
									+ dataCon.getDestination().getName() });
			try {
				sendREQ(resource.getName(), request);
			} catch (IOException e) {
				throw new CreateConnectionException(MessageFormat.format(
						Messages.DeploymentExecutor_CreateDataConnectionFailed,
						new Object[] {}));
			}
		} else {
			throw new CreateConnectionException(MessageFormat.format(
					Messages.DeploymentExecutor_CreateDataConnectionFailed,
					new Object[] {}));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.deployment.IDeploymentExecutor#startResource(org.fordiac
	 * .ide.model.libraryElement.Resource)
	 */
	@Override
	public void startResource(final Resource res) throws StartException {
		String request = MessageFormat.format(Messages.DeploymentExecutor_Start,
				new Object[] { this.id++ });
		try {
			sendREQ(res.getName(), request);
		} catch (IOException e) {
			throw new StartException(MessageFormat.format(
					Messages.DeploymentExecutor_StartingResourceFailed,
					new Object[] { res.getName() }));
		}
	}

	@Override
	public void startDevice(Device dev) throws StartException {
		String request = MessageFormat.format(Messages.DeploymentExecutor_Start,
				new Object[] { this.id++ });
		try {
			sendREQ("", request);
		} catch (IOException e) {
			throw new StartException(MessageFormat.format(
					Messages.DeploymentExecutor_StartingDeviceFailed, new Object[] { dev
							.getName() }));
		}
	}


	@Override
	public void writeDeviceParameter(Device device, String parameter, String value)
			throws WriteDeviceParameterException {
		String request = MessageFormat.format(
				getWriteParameterMessage(), new Object[] { this.id++,
						value, parameter });
		try {
			sendREQ("", request);
		} catch (IOException e) {
			throw new WriteDeviceParameterException(MessageFormat.format(
					Messages.DeploymentExecutor_WriteDeviceParameterFailed, new Object[] {
							device.getName(), parameter }));
		}
	}

	public void deleteResource(Resource res) throws Exception {
		String kill = MessageFormat.format(Messages.DeploymentExecutor_KillFB,
				new Object[] { this.id++, res.getName() });
		String delete = MessageFormat.format(Messages.DeploymentExecutor_DeleteFB,
				new Object[] { this.id++, res.getName() });
		try {
			sendREQ("", kill);
		} catch (IOException e) {
			throw new WriteDeviceParameterException(MessageFormat.format(
					Messages.DeploymentExecutor_KillFBFailed, new Object[] { res
							.getName(), }));
		}
		try {
			sendREQ("", delete);
		} catch (IOException e) {
			throw new WriteDeviceParameterException(MessageFormat.format(
					Messages.DeploymentExecutor_DeleteFBFailed, new Object[] { res
							.getName(), }));
		}
	}

	@Override
	public void clearDevice(Device dev) throws Exception {
		for (Resource res: dev.getResource()) {
			if (!res.isDeviceTypeResource()) {
				deleteResource(res);
			}
		}

	}

	@Override
	public void deleteConnection(Resource res, Connection con) throws Exception {
		
	}

	@Override
	public void deleteFB(Resource res, FB fb) throws Exception {
		
	}

	@Override
	public void startFB(Resource res, FB fb) throws StartException {
		String request = MessageFormat.format(
				Messages.DeploymentExecutor_StartFB, new Object[] { this.id++,
						fb.getName(), fb.getPaletteEntry().getLabel()});
		try {
			sendREQ(res.getName(), request);
		} catch (IOException e) {
			throw new StartException(MessageFormat.format(
					Messages.DeploymentExecutor_StartingFBFailed,
					new Object[] { fb.getName() }));
		}
	}
	
	@Override
	public void killDevice(Device dev) throws Exception {
		String kill = MessageFormat.format(
				Messages.DeploymentExecutor_KillDevice,
				new Object[] { this.id++ });
		try {
			sendREQ("", kill);
		} catch (IOException e) {
			if (e instanceof EOFException) {
				// exception can be ignored, as no response is returnd by forte
			} else {
				throw new Exception(MessageFormat.format(
						Messages.DeploymentExecutor_KillDeviceFailed,
						new Object[] { dev.getName() }));
			}
		}
	}
	
}
