/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.subapptypeeditor.editparts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.ui.parts.GraphicalEditor;
import org.fordiac.ide.fbt.typeeditor.editparts.FBInterfaceEditPartFactory;
import org.fordiac.ide.fbt.typeeditor.editparts.InterfaceEditPart;
import org.fordiac.ide.model.Palette.Palette;

/**
 * A factory for creating FBInterfaceEditPart objects.
 */
public class SubAppInterfaceEditPartFactory extends FBInterfaceEditPartFactory {

	public SubAppInterfaceEditPartFactory(GraphicalEditor editor, Palette systemPalette, ZoomManager zoomManager) {
		super(editor, systemPalette, zoomManager);
	}

	@Override
	protected EditPart createInterfaceEditPart() {
		return new InterfaceEditPart(){
			@Override
			protected void createEditPolicies() {
				super.createEditPolicies();
			
				//supapplications don't have a with construct therefore remove connection handles
				removeEditPolicy(CONNECTION_HANDLES_POLICY);
				removeEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE);
			}
		};
	}

}
