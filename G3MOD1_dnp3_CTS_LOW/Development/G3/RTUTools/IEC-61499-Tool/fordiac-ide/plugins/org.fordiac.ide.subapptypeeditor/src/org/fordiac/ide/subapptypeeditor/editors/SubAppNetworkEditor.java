/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.subapptypeeditor.editors;

import org.eclipse.ui.IEditorInput;
import org.fordiac.ide.fbt.typeeditor.FBTypeEditDomain;
import org.fordiac.ide.fbt.typeeditor.network.CFBUIFBNetworkManager;
import org.fordiac.ide.fbt.typeeditor.network.CompositeNetworkEditor;
import org.fordiac.ide.fbt.typeeditor.network.editparts.CompositeNetworkEditPartFactory;
import org.fordiac.ide.fbt.typemanagement.FBTypeEditorInput;
import org.fordiac.ide.model.libraryElement.SubAppType;
import org.fordiac.ide.subapptypeeditor.editparts.SubAppTypeNetworkEditPartFactory;
import org.fordiac.ide.util.imageprovider.FordiacImage;

public class SubAppNetworkEditor extends CompositeNetworkEditor{

	@Override
	protected void setModel(IEditorInput input) {
		if (input instanceof FBTypeEditorInput) {
			FBTypeEditorInput untypedInput = (FBTypeEditorInput) input;
			if (untypedInput.getContent() instanceof SubAppType) {
				fbType = (SubAppType)untypedInput.getContent();
				setDiagramModelManger(createDiagramModelManger());				
				configurePalette(untypedInput);
			}
		}

		setPartName("Subapplication Network");
		setTitleImage(FordiacImage.ICON_FBNetwork.getImage());
		setEditDomain(new FBTypeEditDomain(this, commandStack));
	}
	
	@Override
	protected CFBUIFBNetworkManager createDiagramModelManger() {
		return new SubAppUIFBNetworkManager((SubAppType)fbType);
	}
	
	@Override
	protected CompositeNetworkEditPartFactory getEditPartFactory() {
		return new SubAppTypeNetworkEditPartFactory(this, fbType, getZoomManger());
	}
}
