/*******************************************************************************
 * Copyright (c) 2007 - 2014 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.subapptypeeditor.editparts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;
import org.eclipse.gef.ui.parts.GraphicalEditor;
import org.fordiac.ide.application.editparts.InterfaceEditPart;
import org.fordiac.ide.application.editparts.SubAppForFBNetworkEditPart;
import org.fordiac.ide.fbt.typeeditor.network.editparts.CompositeNetworkEditPart;
import org.fordiac.ide.fbt.typeeditor.network.editparts.CompositeNetworkEditPartFactory;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.subapptypeeditor.policies.SubAppTypeFBNetworkLayoutEditPolicy;

public class SubAppTypeNetworkEditPartFactory extends
		CompositeNetworkEditPartFactory {

	public SubAppTypeNetworkEditPartFactory(GraphicalEditor editor, CompositeFBType compositeType,
			ZoomManager zoomManager) {
		super(editor, compositeType, zoomManager);
	}
	
	@Override
	protected EditPart getPartForElement(EditPart context,
			Object modelElement) {
		if (modelElement instanceof UIFBNetwork) {
			CompositeNetworkEditPart compositeNetEP = new CompositeNetworkEditPart(){

				@Override
				protected void createEditPolicies() {
					installEditPolicy(EditPolicy.COMPONENT_ROLE,
							new RootComponentEditPolicy());
					// handles constraint changes of model elements and creation of new
					// model elements
					installEditPolicy(EditPolicy.LAYOUT_ROLE,
							new SubAppTypeFBNetworkLayoutEditPolicy());
				}
				
			};
			compositeNetEP.setFbType(compositeType);
			return compositeNetEP;

		}
		
		if (modelElement instanceof SubAppView) {
			return new SubAppForFBNetworkEditPart();
		}
		if (modelElement instanceof SubAppInterfaceElementView) {
			return new InterfaceEditPart(){
				//TODO with new sub app design this may not be necessary in the future
				@Override
				protected String getLabelText(){
					if (getCastedModel().getIInterfaceElement() != null) {
						return getCastedModel().getIInterfaceElement().getName();
					}
					return "";
				}
			};
		}
		
		return super.getPartForElement(context, modelElement);
	}
	
	

}
