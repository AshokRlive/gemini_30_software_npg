/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util.comm.datatypes;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fordiac.ide.util.Activator;

/**
 * @author Oliver Hummer (hummer@acin.tuwien.ac.at)
 * @author Ingo Hegny (hegny@acin.tuwien.ac.at)
 */
public class IEC_WSTRING extends IEC_ANY {

	protected String value;

	public IEC_WSTRING() {
		super();
		value="";
	}


	public IEC_WSTRING(DataInputStream in) {
		super(in);
	}

	public IEC_WSTRING(String initial) {
		value=initial;
	}
	
	/* (non-Javadoc)
	 * @see org.fordiac.ide.util.comm.datatypes.IEC_ANY#decodeValueFrom(java.io.DataInputStream)
	 */
	@Override
	public void decodeValueFrom(DataInputStream in) {
		String result = "";
		int size;
		try {
			size = in.readShort();
			for (int i = 0; i < size; i++) {
				result += in.readChar();
			}
		} catch (IOException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		value = result;

	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.util.comm.datatypes.IEC_ANY#encodeTag()
	 */
	@Override
	public byte[] encodeTag() {
		byte[] retval = new byte[1];
		retval[0] = (0xff & (ASN1.APPLICATION + ASN1.WSTRING));
		return retval;
	}

	@Override
	public byte[] encodeValue() {
		ByteArrayOutputStream myOut=new ByteArrayOutputStream();
		DataOutputStream DOS=new DataOutputStream(myOut);
		
		try {
			DOS.writeShort(value.length());
			DOS.writeChars(value);
		} catch (IOException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		
		return myOut.toByteArray();
	}
	
	/**
	 * @see java.lang.String#toString()
	 */
	public String toString() {
		return value;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public boolean setValue(String value) {
		this.value = value;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IEC_WSTRING) {
			return ((IEC_WSTRING) obj).value.equals(this.value);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return value.hashCode();
	}

	@Override
	public boolean setValue(IEC_ANY source) {
		boolean retval=false;
		if (source.getClass().equals(this.getClass())) {
			this.value = ((IEC_WSTRING)source).getValue();
			retval=true; 
		}
		return retval;
	}
	
}
