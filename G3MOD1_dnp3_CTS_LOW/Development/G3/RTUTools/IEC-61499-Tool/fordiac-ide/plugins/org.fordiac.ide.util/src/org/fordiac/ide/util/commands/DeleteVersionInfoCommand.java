/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.model.libraryElement.VersionInfo;

/**
 * The Class AddNewVersionInfoCommand.
 */
public class DeleteVersionInfoCommand extends Command {
	
	/** The type. */
	private LibraryElement type;

	/** The new version info. */
	private VersionInfo info;
	
	/** The old index. */
	private int oldIndex;

	/**
	 * Instantiates a new change comment command.
	 * 
	 * @param interfaceElement the interface element
	 * @param comment the comment
	 */
	public DeleteVersionInfoCommand(final LibraryElement type, final VersionInfo info) {
		super();
		this.type = type;
		this.info = info;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldIndex = type.getVersionInfo().indexOf(info);		
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		type.getVersionInfo().add(oldIndex, info);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		type.getVersionInfo().remove(info);
	}

}
