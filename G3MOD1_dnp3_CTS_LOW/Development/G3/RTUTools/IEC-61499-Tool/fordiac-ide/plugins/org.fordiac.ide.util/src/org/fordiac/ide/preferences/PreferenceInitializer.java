/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.swt.graphics.RGB;
import org.fordiac.ide.util.Activator;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#
	 * initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(
				PreferenceConstants.P_VISIBILITY_OF_SUB_APP_INTERFACE_ELEMENTS,
				true);
		PreferenceConverter
				.setDefault(store, PreferenceConstants.P_EVENT_CONNECTOR_COLOR,
						new RGB(255, 0, 0));
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_DATA_CONNECTOR_COLOR, new RGB(0, 0, 255));
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_ADAPTER_CONNECTOR_COLOR, new RGB(0, 174,
						0));
	}
}
