/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.fordiac.ide.util.Activator;

/**
 * The Class FordiacPreferencePage.
 */
public class FordiacPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	/**
	 * Instantiates a new fordiac preference page.
	 */
	public FordiacPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription(Messages.FordiacPreferencePage_LABEL_PreferencePageDescription);
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		addField(new BooleanFieldEditor(
				PreferenceConstants.P_VISIBILITY_OF_SUB_APP_INTERFACE_ELEMENTS,
				Messages.FordiacPreferencePage_LABEL_InterfaceEelementOfSubAppsVisibility,
				getFieldEditorParent()));
		addField(new ColorFieldEditor(
				PreferenceConstants.P_EVENT_CONNECTOR_COLOR,
				Messages.FordiacPreferencePage_LABEL_DefaultEventConnectorColor,
				getFieldEditorParent()));
		addField(new ColorFieldEditor(
				PreferenceConstants.P_DATA_CONNECTOR_COLOR,
				Messages.FordiacPreferencePage_LABEL_DefaultDataConnectorColor,
				getFieldEditorParent()));
		addField(new ColorFieldEditor(
				PreferenceConstants.P_ADAPTER_CONNECTOR_COLOR,
				Messages.FordiacPreferencePage_LABEL_DefaultAdapterConnectorColor,
				getFieldEditorParent()));

		addField(new ComboFieldEditor(
				PreferenceConstants.P_DEFAULT_COMPLIANCE_PROFILE,
				Messages.FordiacPreferencePage_LABEL_DefaultComplianceProfile,
				getSupportedProfiles(), getFieldEditorParent()));

	}

	private String[][] getSupportedProfiles() {
		// FIXME return installed/supported profiles
		return new String[][] { { "HOLOBLOC", "HOLOBLOC" } }; //$NON-NLS-1$ //$NON-NLS-2$

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(final IWorkbench workbench) {
		// nothing to do
	}
	
}