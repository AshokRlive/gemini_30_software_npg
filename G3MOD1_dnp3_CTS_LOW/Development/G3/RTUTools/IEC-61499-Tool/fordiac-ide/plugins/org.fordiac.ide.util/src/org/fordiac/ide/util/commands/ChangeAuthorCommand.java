/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.VersionInfo;

/**
 * The Class ChangeAuthorCommand.
 */
public class ChangeAuthorCommand extends Command {
	
	VersionInfo versionInfo;
	
	/** The new ApplicationDomain value. */
	private String newAuthor;

	/** The old ApplicationDomain value. */
	private String oldAuthor;

	public ChangeAuthorCommand(final VersionInfo VersionInfo, final String newAuthor) {
		super();
		this.versionInfo = VersionInfo;
		this.newAuthor = newAuthor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldAuthor = versionInfo.getAuthor();
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		versionInfo.setAuthor(oldAuthor);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		versionInfo.setAuthor(newAuthor);
	}

}
