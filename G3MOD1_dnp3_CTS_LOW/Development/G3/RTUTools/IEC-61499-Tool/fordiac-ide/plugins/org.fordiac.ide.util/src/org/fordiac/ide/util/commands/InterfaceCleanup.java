/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.ui.InterfaceElementView;

/**
 * The Class InterfaceCleanup.
 */
public class InterfaceCleanup extends Command {

	/** The interface element. */
	private final InterfaceElementView interfaceElement;

	/** The app interface element. */
	private InterfaceElementView appInterfaceElement;

	/** The mapped interface element. */
	private InterfaceElementView mappedInterfaceElement;

	/**
	 * Instantiates a new interface cleanup.
	 * 
	 * @param interfaceElement the interface element
	 */
	public InterfaceCleanup(final InterfaceElementView interfaceElement) {
		this.interfaceElement = interfaceElement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		if (appInterfaceElement != null) {
			appInterfaceElement.setMappedInterfaceElement(mappedInterfaceElement);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		if (appInterfaceElement != null) {
			appInterfaceElement.setMappedInterfaceElement(null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		interfaceCleanUp();
	}

	/**
	 * Interface clean up.
	 */
	private void interfaceCleanUp() {
		// set applicationInterfaceElements mappedInterfaceElement to null ->
		// for
		// avoiding DanglingHrefExceptions!
		if (interfaceElement.getApplicationInterfaceElement() != null) {
			appInterfaceElement = interfaceElement.getApplicationInterfaceElement();
			mappedInterfaceElement = interfaceElement
					.getApplicationInterfaceElement().getMappedInterfaceElement();
			interfaceElement.getApplicationInterfaceElement()
					.setMappedInterfaceElement(null);
		}
	}
}