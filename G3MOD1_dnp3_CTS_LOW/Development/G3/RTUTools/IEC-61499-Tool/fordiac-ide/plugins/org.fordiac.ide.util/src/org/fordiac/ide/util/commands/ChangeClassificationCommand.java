/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import org.fordiac.ide.model.libraryElement.LibraryElement;

/**
 * The Class ChangeCommentCommand.
 */
public class ChangeClassificationCommand extends ChangeIdentificationCommand {
	
	/** The new classification value. */
	private String newClassification;

	/** The old classification value. */
	private String oldClassification;

	public ChangeClassificationCommand(LibraryElement type,
			final String newClassification) {
		super(type);
		this.newClassification = newClassification;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldClassification = identification.getClassification();
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		identification.setClassification(oldClassification);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		identification.setClassification(newClassification);
	}

}
