/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util.comm.channels.udp;

import java.io.IOException;

import org.fordiac.ide.util.comm.channels.CCommThread;
import org.fordiac.ide.util.comm.channels.IIecReceivable;



/**
 * This class implements {@link CCommThread} for UDP communication. The receiving channel is established within this Thread, 
 * 
 * @author Oliver Hummer (hummer@acin.tuwien.ac.at)
 * @author Ingo Hegny (hegny@acin.tuwien.ac.at)
 * 
 */
public class UDPCommThread extends CCommThread {

	private UDPChannel channel;
	private IIecReceivable receiver;

	public UDPCommThread(UDPChannel channel, IIecReceivable receiver) {
		this.channel = channel;
		this.receiver=receiver;
	}

	@Override
	public void run() {
		while (!isInterrupted() && channel.getSocket() != null) {
			try {
				receiver.ReceiveIECData(channel.receiveFrom());
			} catch (IOException e) {

				return;
			}
		}
	}

}
