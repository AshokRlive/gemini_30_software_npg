/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/


package org.fordiac.ide.util.comm;

import org.fordiac.ide.util.comm.channels.IChannel;


public class UDPSubscriber extends IIecNetCommRcv  {

	public UDPSubscriber() {
		super();
	}
	
	@Override
	public boolean Initialize(String pa_sID) {
		boolean retval=false;
		retval=Initialize(pa_sID, IChannel.UDP);
		return retval;
	}

}