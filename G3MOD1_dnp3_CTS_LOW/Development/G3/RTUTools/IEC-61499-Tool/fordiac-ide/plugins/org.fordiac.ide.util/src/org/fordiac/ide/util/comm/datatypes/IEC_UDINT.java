/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util.comm.datatypes;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fordiac.ide.util.Activator;

/**
 * @author Oliver Hummer (hummer@acin.tuwien.ac.at)
 * @author Ingo Hegny (hegny@acin.tuwien.ac.at)
 */
public class IEC_UDINT extends IEC_LINT {

	protected static final long BOUNDS_MASK = 0xFFFFFFFF00000000L;
	
	public IEC_UDINT() {
		super();
	}
	
	public IEC_UDINT(long initial){
		super(initial);
	}

	public IEC_UDINT(DataInputStream in) {
		super(in);
	}

	@Override
	public void decodeValueFrom(DataInputStream in) {
		try {
			 int firstByte = (0x000000FF & ((int)in.readByte()));
			 int secondByte = (0x000000FF & ((int)in.readByte()));
		     int thirdByte = (0x000000FF & ((int)in.readByte()));
		     int fourthByte = (0x000000FF & ((int)in.readByte()));
		     value = ((long) (firstByte << 24
	                | secondByte << 16
                    | thirdByte << 8
                    | fourthByte))
                   & 0xFFFFFFFFL;
		} catch (IOException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fordiac.ide.util.comm.datatypes.IEC_ANY#encodeTag()
	 */
	@Override
	public byte[] encodeTag() {
		byte[] retval = new byte[1];
		retval[0] = (0xff & (ASN1.APPLICATION + ASN1.UDINT));
		return retval;
	}

	@Override
	public byte[] encodeValue() {
		ByteArrayOutputStream myOut=new ByteArrayOutputStream();
		DataOutputStream DOS=new DataOutputStream(myOut);
		
		try {
		     int tempByte = (int)(value & 0xFFL);
		     DOS.writeByte(tempByte);
		     tempByte = (int)((value & 0xFF00L)>>8);
		     DOS.writeByte(tempByte);
		     
		     tempByte = (int)((value & 0xFF0000L)>>16);
		     DOS.writeByte(tempByte);
		     tempByte = (int)((value & 0xFF000000L)>>24);
		     DOS.writeByte(tempByte);
		} catch (IOException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		
		return myOut.toByteArray();
	}

	@Override
	public boolean setValue(IEC_ANY source) {
		boolean retval=false;
		if (source.getClass().equals(this.getClass())) {
			this.value = ((IEC_UDINT)source).getValue();
			retval=true; 
		}
		return retval;
	}
	
}
