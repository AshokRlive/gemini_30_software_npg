/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.util.Utils;

/**
 * A command for deleting Connections.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class DeleteConnectionCommand extends Command {

	/** The selected ConnectionView. */
	private final ConnectionView selectedConView;

	/**
	 * Instantiates a new delete connection command.
	 * 
	 * @param selectedConView
	 *            the selected ConnectionView
	 */
	public DeleteConnectionCommand(final ConnectionView selectedConView) {
		super("Delete Connection");
		this.selectedConView = selectedConView;
	}
	
	public ConnectionView getConnectionView(){
		return selectedConView;
	}

	/** The editor. */
	private IEditorPart editor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());

	}

	/**
	 * delete an existing UIConnection.
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		connectionView = selectedConView;
		sourceView = connectionView.getSource();
		destinationView = connectionView.getDestination();

		InterfaceElementView startView = sourceView;
		while (sourceView instanceof InternalSubAppInterfaceElementView) {
			SubAppInterfaceElementView ext = connectionView.getExternalSourceInterfaceElementView();
			for (Iterator<ConnectionView> iterator = ext.getInConnections()
					.iterator(); iterator.hasNext();) {
				ConnectionView connectionView = iterator.next();
				if (connectionView.getSource().getIInterfaceElement()
						.equals(sourceView.getIInterfaceElement())) {
					this.sourceView = connectionView.getSource();
					this.connectionView = connectionView;
					this.destinationView = connectionView.getDestination();
				}
			}
			if (sourceView == startView) {
				// This is an abnormal case, which means the connection is broken.
				// Since the connection is inside a subapp make sure the internal 
				// interace is deleted as well.
				InternalSubAppInterfaceElementView i = (InternalSubAppInterfaceElementView) sourceView;
				if (i.getOutConnections().size() == 1 && i.getOutConnections().get(0) == connectionView) {
					((UISubAppNetwork) connectionView.eContainer()).getInterfaceElements().remove(i);
				}
				break;
			}
		}
		startView = destinationView;
		while (destinationView instanceof InternalSubAppInterfaceElementView) {
			SubAppInterfaceElementView ext = connectionView.getExternalDestinationInterfaceElementView();
			for (Iterator<ConnectionView> iterator = ext.getOutConnections()
					.iterator(); iterator.hasNext();) {
				ConnectionView connectionView = iterator.next();
				if (connectionView.getDestination().getIInterfaceElement()
						.equals(destinationView.getIInterfaceElement())) {
					this.destinationView = connectionView.getDestination();
					this.connectionView = connectionView;
					this.sourceView = connectionView.getSource();
				}
			}
			if (destinationView == startView) {
				// This is an abnormal case, which means the connection is broken.
				// Since the connection is inside a subapp make sure the internal 
				// interace is deleted as well.
				InternalSubAppInterfaceElementView i = (InternalSubAppInterfaceElementView) destinationView;
				if (i.getInConnections().size() == 1 && i.getInConnections().get(0) == connectionView) {
					((UISubAppNetwork) connectionView.eContainer()).getInterfaceElements().remove(i);
				}
				break;
			}
		}

		connection = connectionView.getConnectionElement();
		if (connection instanceof EventConnection) {
			source = ((EventConnection) connection).getSource();
			destination = ((EventConnection) connection).getDestination();
		}
		if (connection instanceof DataConnection) {
			source = ((DataConnection) connection).getSource();
			destination = ((DataConnection) connection).getDestination();
		}
		if (connection != null) {
			connectionParent = connection.eContainer();
		}
		connectionViewParent = connectionView.eContainer();
		if(null != connectionView.getInternalDestinationInterfaceElementView() ||
				null != connectionView.getInternalSourceInterfaceElementView()){
			if (sourceView instanceof SubAppInterfaceElementView) {
				SubAppInterfaceElementView subAppInterfaceElementViewSource = (SubAppInterfaceElementView) sourceView;
	
				if(subAppInterfaceElementViewSource instanceof MappedSubAppInterfaceElementView){
					subAppInterfaceElementViewSource = (SubAppInterfaceElementView)((MappedSubAppInterfaceElementView) subAppInterfaceElementViewSource).getApplicationInterfaceElement();
				}
				
				if(null == ((SubAppView) subAppInterfaceElementViewSource.eContainer()).getSubApp().getPaletteEntry()){
					ArrayList<ConnectionView> internalOutConnections = new ArrayList<ConnectionView>(
							connectionView.getInternalSourceInterfaceElementView().getInConnections());
					internalConnections.addAll(removeInternalInConnections(
							internalOutConnections,
							(SubAppInterfaceElementView) subAppInterfaceElementViewSource, connectionView));				
				}
			}
	
			if (destinationView instanceof SubAppInterfaceElementView) {
				SubAppInterfaceElementView subAppInterfaceElementViewDest = (SubAppInterfaceElementView) destinationView;
	
				if(subAppInterfaceElementViewDest instanceof MappedSubAppInterfaceElementView){
					subAppInterfaceElementViewDest = (SubAppInterfaceElementView)((MappedSubAppInterfaceElementView) subAppInterfaceElementViewDest).getApplicationInterfaceElement();
				}
				
				if(null == ((SubAppView) subAppInterfaceElementViewDest.eContainer()).getSubApp().getPaletteEntry()){
					ArrayList<ConnectionView> internalOutConnections = new ArrayList<ConnectionView>(
							connectionView.getInternalDestinationInterfaceElementView().getOutConnections());
					internalConnections.addAll(removeInternalOutConnections(
							internalOutConnections,
							(SubAppInterfaceElementView) subAppInterfaceElementViewDest,
							connectionView));
				}
			}
		}
		redo();
	}

	/** The internal connections. */
	ArrayList<InternalConnection> internalConnections = new ArrayList<InternalConnection>();

	/**
	 * Removes the internal out connections.
	 * 
	 * @param internalOutConnections
	 *            the internal out connections
	 * @param destinationView
	 *            the destination view
	 * @param connectionView
	 *            the connection view
	 * 
	 * @return the list< internal connection>
	 */
	private List<InternalConnection> removeInternalOutConnections(
			final ArrayList<ConnectionView> internalOutConnections,
			final SubAppInterfaceElementView destinationView,
			final ConnectionView connectionView) {
		ArrayList<InternalConnection> internalConnections = new ArrayList<InternalConnection>();

		for (Iterator<ConnectionView> iterator = internalOutConnections
				.iterator(); iterator.hasNext();) {
			ConnectionView internalConnectionView = iterator.next();

			if (internalConnectionView.getConnectionElement()
					.getIInterfaceElementDestination()
					.equals(destinationView.getIInterfaceElement())) {
				InternalConnection internalConnection = new InternalConnection();
				internalConnection
						.setInternalInterfaceElementView(connectionView
								.getInternalDestinationInterfaceElementView());
				internalConnection
						.setSubAppInterfaceElementView(destinationView);
				internalConnection.setConnectionView(internalConnectionView);
				internalConnection
						.setSource(internalConnectionView.getSource());
				internalConnection.setDest(internalConnectionView
						.getDestination());
				internalConnection
						.setUiSubAppNetwork((UISubAppNetwork) internalConnectionView
								.eContainer());
				internalConnections.add(internalConnection);

				if (internalConnectionView.getDestination() instanceof SubAppInterfaceElementView) {
					ArrayList<ConnectionView> temp = new ArrayList<ConnectionView>(
							internalConnectionView.getInternalDestinationInterfaceElementView()
									.getOutConnections());
					internalConnections.addAll(removeInternalOutConnections(
							temp,
							(SubAppInterfaceElementView) internalConnectionView
									.getDestination(), internalConnectionView));
				}

			}

		}
		// ConnectionUtil.undoInternalConnection(internalConnections);
		return internalConnections;
	}

	/**
	 * Removes the internal in connections.
	 * 
	 * @param internalInConnections
	 *            the internal in connections
	 * @param sourceView
	 *            the source view
	 * @param connectionView
	 *            the connection view
	 * 
	 * @return the list< internal connection>
	 */
	private List<InternalConnection> removeInternalInConnections(
			final ArrayList<ConnectionView> internalInConnections,
			final SubAppInterfaceElementView sourceView,
			final ConnectionView connectionView) {
		ArrayList<InternalConnection> internalConnections = new ArrayList<InternalConnection>();

		for (Iterator<ConnectionView> iterator = internalInConnections
				.iterator(); iterator.hasNext();) {
			ConnectionView internalConnectionView = iterator.next();
			if (internalConnectionView.getConnectionElement()
					.getIInterfaceElementSource()
					.equals(sourceView.getIInterfaceElement())) {
				InternalConnection internalConnection = new InternalConnection();
				internalConnection
						.setInternalInterfaceElementView(connectionView.getInternalSourceInterfaceElementView());
				internalConnection.setSubAppInterfaceElementView(sourceView);
				internalConnection.setConnectionView(internalConnectionView);
				internalConnection
						.setSource(internalConnectionView.getSource());
				internalConnection.setDest(internalConnectionView
						.getDestination());
				internalConnection
						.setUiSubAppNetwork((UISubAppNetwork) internalConnectionView
								.eContainer());
				internalConnections.add(internalConnection);
				if (internalConnectionView.getSource() instanceof SubAppInterfaceElementView) {
					ArrayList<ConnectionView> temp = new ArrayList<ConnectionView>(
							internalConnectionView
									.getInternalSourceInterfaceElementView()
									.getInConnections());
					internalConnections.addAll(removeInternalInConnections(
							temp,
							(SubAppInterfaceElementView) internalConnectionView
									.getSource(), internalConnectionView));
				}
			}

		}
		// ConnectionUtil.undoInternalConnection(internalConnections);
		return internalConnections;
	}

	/** The source. */
	private IInterfaceElement source;

	/** The destination. */
	private IInterfaceElement destination;

	/** The connection. */
	private Connection connection;

	/** The connection view. */
	private ConnectionView connectionView;

	/** The source view. */
	private InterfaceElementView sourceView;

	/** The destination view. */
	private InterfaceElementView destinationView;

	/** The connection parent. */
	private EObject connectionParent;

	/** The connection view parent. */
	private EObject connectionViewParent;

	private InterfaceElementView mappedSource;
	private InterfaceElementView mappedDest;
	private InterfaceElementView appSource;
	private InterfaceElementView appDest;

	/** The source conn view. */
	ConnectionView sourceConnView = null;

	/** The dest conn view. */
	ConnectionView destConnView = null;

	/** The app source conn view. */
	ConnectionView appSourceConnView = null;

	/** The app dest conn view. */
	ConnectionView appDestConnView = null;

	/** The diagram. */
	Diagram diagram;

	/** The app diagram. */
	Diagram appDiagram;

	/**
	 * Redo.
	 * 
	 * @see DeleteConnectionCommand#execute()
	 */
	@Override
	public void redo() {

		ConnectionUtil.undoInternalConnection(internalConnections);

		if (connection instanceof EventConnection) {
			if (((EventConnection) connection).getSource() != null) {
				((EventConnection) connection).getSource()
						.getInputConnections().remove(connection);
			}
			if (((EventConnection) connection).getDestination() != null) {
				((EventConnection) connection).getDestination()
						.getOutputConnections().remove(connection);
			}
			((EventConnection) connection).setSource(null);
			((EventConnection) connection).setDestination(null);
		}
		if (connection instanceof DataConnection) {
			if (((DataConnection) connection).getSource() != null) {
				((DataConnection) connection).getSource().getInputConnections()
						.remove(connection);
			}
			if (((DataConnection) connection).getDestination() != null) {
				((DataConnection) connection).getDestination()
						.getOutputConnections().remove(connection);
			}
			((DataConnection) connection).setSource(null);
			((DataConnection) connection).setDestination(null);
		}

		((Diagram) connectionViewParent).getConnections()
				.remove(connectionView);

		// delete mapped connection
		mappedSource = connectionView.getSource().getMappedInterfaceElement();
		mappedDest = connectionView.getDestination()
				.getMappedInterfaceElement();

		if (mappedSource != null && mappedDest != null) {
			for (Iterator<ConnectionView> iterator = mappedSource
					.getOutConnections().iterator(); iterator.hasNext();) {
				ConnectionView connView = iterator.next();
				if (connView.getConnectionElement().equals(
						connectionView.getConnectionElement())) {
					connView.setSource(null);
					sourceConnView = connView;
					break;
				}
			}
			for (Iterator<ConnectionView> iterator = mappedDest
					.getInConnections().iterator(); iterator.hasNext();) {
				ConnectionView connView = iterator.next();
				if (connView.getConnectionElement().equals(
						connectionView.getConnectionElement())) {
					connView.setDestination(null);
					destConnView = connView;
					break;
				}
			}
		}
		if (sourceConnView != null && sourceConnView.equals(destConnView)) {
			diagram = ((UIResourceEditor) mappedSource.eContainer()
					.eContainer());
			diagram.getConnections().remove(sourceConnView);
		}

		// delete app connection (if in resource the connection is deleted)
		appSource = connectionView.getSource().getApplicationInterfaceElement();
		appDest = connectionView.getDestination()
				.getApplicationInterfaceElement();
		if (appSource != null && appDest != null) {

			for (Iterator<ConnectionView> iterator = appSource
					.getOutConnections().iterator(); iterator.hasNext();) {
				ConnectionView connView = iterator.next();
				if (connView.getConnectionElement().equals(
						connectionView.getConnectionElement())) {
					connView.setSource(null);
					appSourceConnView = connView;
					break;
				}
			}
			for (Iterator<ConnectionView> iterator = appDest.getInConnections()
					.iterator(); iterator.hasNext();) {
				ConnectionView connView = iterator.next();
				if (connView.getConnectionElement().equals(
						connectionView.getConnectionElement())) {
					connView.setDestination(null);
					appDestConnView = connView;
					break;
				}
			}
		}
		if (appSourceConnView != null
				&& appSourceConnView.equals(appDestConnView)) {
			appDiagram = ((UIFBNetwork) appSource.eContainer().eContainer());
			appDiagram.getConnections().remove(appSourceConnView);
		}

		connectionView.setDestination(null);
		connectionView.setSource(null);

		if (connection instanceof EventConnection && connectionParent != null) {
			((SubAppNetwork) connectionParent).getEventConnections().remove(
					connection);
		} else if (connection instanceof DataConnection
				&& connectionParent != null) {
			((SubAppNetwork) connectionParent).getDataConnections().remove(
					connection);
		}

	}

	/**
	 * undo deleting the connection.
	 */
	@Override
	public void undo() {

		ConnectionUtil.redoInternalConnection(internalConnections);

		if (connection instanceof EventConnection) {
			((EventConnection) connection).setSource((Event) source);
			((EventConnection) connection).setDestination((Event) destination);
		}
		if (connection instanceof DataConnection) {
			((DataConnection) connection).setSource((VarDeclaration) source);
			((DataConnection) connection)
					.setDestination((VarDeclaration) destination);
		}

		((Diagram) connectionViewParent).getConnections().add(connectionView);
		connectionView.setDestination(destinationView);
		connectionView.setSource(sourceView);
		if (connection instanceof EventConnection && connectionParent != null) {
			((SubAppNetwork) connectionParent).getEventConnections().add(
					(EventConnection) connection);
		}
		if (connection instanceof DataConnection && connectionParent != null) {
			((SubAppNetwork) connectionParent).getDataConnections().add(
					(DataConnection) connection);
		}

		if (sourceConnView != null && sourceConnView.equals(destConnView)) {
			sourceConnView.setSource(mappedSource);
			sourceConnView.setDestination(mappedDest);
			diagram.getConnections().add(sourceConnView);
		}
		if (appSourceConnView != null
				&& appSourceConnView.equals(appDestConnView)) {
			appSourceConnView.setSource(appSource);
			appSourceConnView.setDestination(appDest);
			appDiagram.getConnections().add(appSourceConnView);
		}
	}

}
