/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.VersionInfo;

/**
 * The Class ChangeRemarksCommand.
 */
public class ChangeRemarksCommand extends Command {
	
	VersionInfo versionInfo;
	
	/** The new ApplicationDomain value. */
	private String newRemarks;

	/** The old ApplicationDomain value. */
	private String oldRemarks;

	public ChangeRemarksCommand(final VersionInfo VersionInfo, final String newRemarks) {
		super();
		this.versionInfo = VersionInfo;
		this.newRemarks = newRemarks;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldRemarks = versionInfo.getRemarks();
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		versionInfo.setRemarks(oldRemarks);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		versionInfo.setRemarks(newRemarks);
	}

}
