/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util.comm.channels;

import java.util.List;

import org.fordiac.ide.util.comm.datatypes.IEC_ANY;


/**
 * Interface for Callback classes, which are called after receiving data
 * @author Oliver Hummer (hummer@acin.tuwien.ac.at)
 * @author Ingo Hegny (hegny@acin.tuwien.ac.at)
 *
 */
public interface IIecReceivable extends IIecCommObject {

	/**
	 * method is called by {@link CCommThread} for evaluation (and decoding) of received data 
	 * @param inList
	 */
	public abstract void ReceiveIECData(List<IEC_ANY> inList);
	
	public abstract void setMyReceiveData(List<IEC_ANY> pa_loReceiveData);
}
