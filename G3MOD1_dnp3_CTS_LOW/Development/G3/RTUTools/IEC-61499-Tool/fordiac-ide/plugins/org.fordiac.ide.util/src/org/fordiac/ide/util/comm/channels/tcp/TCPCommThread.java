/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util.comm.channels.tcp;

import java.io.IOException;

import org.fordiac.ide.util.Activator;
import org.fordiac.ide.util.comm.channels.CCommThread;
import org.fordiac.ide.util.comm.channels.IIecReceivable;
import org.fordiac.ide.util.comm.exceptions.CommException;


/**
 * @author Oliver Hummer (hummer@acin.tuwien.ac.at)
 * @author Ingo Hegny (hegny@acin.tuwien.ac.at)
 * 
 */
public class TCPCommThread extends CCommThread {

	private TCPChannel channel;
	private IIecReceivable receiver;

	public TCPCommThread(TCPChannel channel, IIecReceivable receiver) {
		this.channel = channel;
		this.receiver = receiver;
	}

	@Override
	public void run() {
		while (!isInterrupted() && channel.getSocket() != null) {
			try {

				if (!channel.getSocket().isClosed())
					receiver.ReceiveIECData(channel.receiveFrom());
			} catch (IOException e) {
				if (!isInterrupted()) {
					Activator.getDefault().logError(e.getMessage(), e);
				}
				return;
			} catch (CommException e) {
				// Socket closed by peer
				return;
			}

		}
	}

}
