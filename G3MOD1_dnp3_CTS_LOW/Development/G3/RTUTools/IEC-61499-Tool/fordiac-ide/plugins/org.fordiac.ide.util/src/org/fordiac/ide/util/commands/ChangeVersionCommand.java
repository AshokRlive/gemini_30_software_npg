/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.VersionInfo;

/**
 * The Class ChangeCommentCommand.
 */
public class ChangeVersionCommand extends Command {
	
	VersionInfo versionInfo;
	
	/** The new ApplicationDomain value. */
	private String newVersion;

	/** The old ApplicationDomain value. */
	private String oldVersion;

	public ChangeVersionCommand(final VersionInfo versionInfo, final String newVersion) {
		super();
		this.versionInfo = versionInfo;
		this.newVersion = newVersion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldVersion = versionInfo.getVersion();
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		versionInfo.setVersion(oldVersion);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		versionInfo.setVersion(newVersion);
	}

}
