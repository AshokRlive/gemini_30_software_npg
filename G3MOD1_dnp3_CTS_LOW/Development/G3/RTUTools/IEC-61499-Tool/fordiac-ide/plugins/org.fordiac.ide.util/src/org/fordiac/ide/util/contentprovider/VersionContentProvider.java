/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.contentprovider;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.fordiac.ide.model.libraryElement.FBType;

public class VersionContentProvider implements IStructuredContentProvider {
	@Override
	public Object[] getElements(final Object inputElement) {
		if(inputElement instanceof FBType){
			return ((FBType)inputElement).getVersionInfo().toArray();
		}
		return new Object[] {};
	}
}
