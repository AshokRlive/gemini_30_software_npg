/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

public class ElementSelector {
	/**
	 * Selects the provided objects in the current editor
	 * 
	 * @param viewObjects list with objects to select
	 */
	@SuppressWarnings("rawtypes")
	public void selectViewObjects(List viewObjects) {
		IWorkbenchPart part = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePart();
		GraphicalViewer viewer = (GraphicalViewer)part.getAdapter(GraphicalViewer.class);
		if (viewer != null) {
			viewer.setSelection(new StructuredSelection(getSelectableEditParts(viewer, viewObjects)));
		}
		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private List getSelectableEditParts(GraphicalViewer viewer, List viewObjects) {
		List selectableChildren = new ArrayList();
		List children = viewer.getContents().getChildren();
		for (Iterator viewIter = viewObjects.iterator(); viewIter.hasNext();) {
			Object view = viewIter.next();
			for (Iterator iter = children.iterator(); iter.hasNext();) {
				Object child = iter.next();
				if (child instanceof EditPart) {
					EditPart childPart = (EditPart) child;
					if (childPart.getModel().equals(view)) {
						selectableChildren.add(childPart);
						break;
					}
				}
			}
		}
		
		return selectableChildren;
	}
}
