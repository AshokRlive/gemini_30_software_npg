/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UIResourceEditor;

/**
 * The Class UnmapCommandRemoveConnection.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class UnmapCommandRemoveConnection extends Command {

	/** The ui resource editor. */
	private final UIResourceEditor uiResourceEditor;

	/** The conn view. */
	private final ConnectionView connView;

	/** The connection. */
	private Connection connection;

	/** The source. */
	private IInterfaceElement source;

	/** The dest. */
	private IInterfaceElement dest;

	/** The original parent. */
	private SubAppNetwork originalParent;

	/** The orig source view. */
	private InterfaceElementView origSourceView;

	/** The orig dest view. */
	private InterfaceElementView origDestView;

	/** The orig connection. */
	private Connection origConnection;

	/**
	 * Instantiates a new unmap command remove connection.
	 * 
	 * @param uiResourceEditor the ui resource editor
	 * @param connView the conn view
	 */
	public UnmapCommandRemoveConnection(final UIResourceEditor uiResourceEditor,
			final ConnectionView connView) {
		this.uiResourceEditor = uiResourceEditor;
		this.connView = connView;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		removeMappedConnection(uiResourceEditor, connView);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		if (connection != null) {
			if (connection instanceof EventConnection) {
				((EventConnection) connection).setSource(source);
				((EventConnection) connection).setDestination(dest);
				uiResourceEditor.getFunctionBlockNetwork().getEventConnections().add(
						(EventConnection) connection);
			}
			if (connection instanceof DataConnection) {
				((DataConnection) connection).setSource(source);
				((DataConnection) connection).setDestination(dest);
				uiResourceEditor.getFunctionBlockNetwork().getDataConnections().add(
						(DataConnection) connection);
			}
		} else {
			addConnectionToOrigNetwork();
		}
		connView.setConnectionElement(origConnection);
		connView.setSource(origSourceView);
		connView.setDestination(origDestView);
		uiResourceEditor.getConnections().add(connView);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		if (connection != null) {
			source = connection.getIInterfaceElementSource();
			dest = connection.getIInterfaceElementDestination();
			if (connection instanceof EventConnection) {
				uiResourceEditor.getFunctionBlockNetwork().getEventConnections()
						.remove(connection);
				((EventConnection) connection).setSource(null);
				((EventConnection) connection).setDestination(null);
			}
			if (connection instanceof DataConnection) {
				uiResourceEditor.getFunctionBlockNetwork().getDataConnections().remove(
						connection);
				((DataConnection) connection).setSource(null);
				((DataConnection) connection).setDestination(null);
			}
		} else {
			addConnectionToFBNetwork(connView.getConnectionElement(), connView
					.getSource().getApplicationInterfaceElement().getFbNetwork());
		}
		connView.setSource(null);
		connView.setDestination(null);
		connView.setConnectionElement(null);
		uiResourceEditor.getConnections().remove(connView);
	}

	/**
	 * Removes the mapped connection.
	 * 
	 * @param parent
	 *          the parent
	 * @param connView
	 *          the conn view
	 */
	private void removeMappedConnection(final UIResourceEditor parent,
			final ConnectionView connView) {
		if (connView.getSource() == null) {
			parent.getConnections().remove(connView);
			return;
		}

		if (connView.getSource().getApplicationInterfaceElement() == null
				|| connView.getSource().getApplicationInterfaceElement().getFbNetwork() == null
				|| connView.getDestination() == null
				|| connView.getDestination().getApplicationInterfaceElement() == null
				|| !connView.getSource().getApplicationInterfaceElement()
						.getFbNetwork().equals(
								connView.getDestination().getApplicationInterfaceElement()
										.getFbNetwork())) {
			// a connection that was added later in the resource --> delete the
			// ConnectionImpl
			connection = connView.getConnectionElement();
			if (connection != null) {
				source = connection.getIInterfaceElementSource();
				dest = connection.getIInterfaceElementDestination();
				if (connection instanceof EventConnection) {
					parent.getFunctionBlockNetwork().getEventConnections().remove(
							connection);
					((EventConnection) connection).setSource(null);
					((EventConnection) connection).setDestination(null);
				}
				if (connection instanceof DataConnection) {
					parent.getFunctionBlockNetwork().getDataConnections().remove(
							connection);
					((DataConnection) connection).setSource(null);
					((DataConnection) connection).setDestination(null);
				}
			}
		} else {
			addConnectionToFBNetwork(connView.getConnectionElement(), connView
					.getSource().getApplicationInterfaceElement().getFbNetwork());
		}
		origSourceView = connView.getSource();
		origDestView = connView.getDestination();
		origConnection = connView.getConnectionElement();

		connView.setSource(null);
		connView.setDestination(null);
		connView.setConnectionElement(null);
		parent.getConnections().remove(connView);
	}

	/**
	 * Adds the connection to orig network.
	 */
	private void addConnectionToOrigNetwork() {
		if (origConnection instanceof EventConnection) {
			originalParent.getEventConnections()
					.add((EventConnection) origConnection);
		}
		if (origConnection instanceof DataConnection) {
			originalParent.getDataConnections().add((DataConnection) origConnection);
		}
	}

	/**
	 * Adds the connection to fb network.
	 * 
	 * @param connectionElement
	 *          the connection element
	 * @param fbNetwork
	 *          the fb network
	 */
	private void addConnectionToFBNetwork(final Connection connectionElement,
			final SubAppNetwork fbNetwork) {
		originalParent = (SubAppNetwork) connectionElement.eContainer();
		if (connectionElement instanceof EventConnection) {
			fbNetwork.getEventConnections().add((EventConnection) connectionElement);
		}
		if (connectionElement instanceof DataConnection) {
			fbNetwork.getDataConnections().add((DataConnection) connectionElement);
		}
	}

}
