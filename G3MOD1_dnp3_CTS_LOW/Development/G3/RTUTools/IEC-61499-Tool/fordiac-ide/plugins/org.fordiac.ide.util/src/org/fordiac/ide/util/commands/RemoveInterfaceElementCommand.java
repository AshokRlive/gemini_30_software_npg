/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;

/**
 * The Class RemoveInterfaceElementCommand.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class RemoveInterfaceElementCommand extends Command {

	/** The sub app view. */
	private final SubAppView subAppView;

	/** The interface element. */
	private final SubAppInterfaceElementView interfaceElement;

	/**
	 * Instantiates a new removes the interface element command.
	 * 
	 * @param subAppView the sub app view
	 * @param interfaceElement the interface element
	 */
	public RemoveInterfaceElementCommand(final SubAppView subAppView,
			final SubAppInterfaceElementView interfaceElement) {
		this.subAppView = subAppView;
		this.interfaceElement = interfaceElement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		MappedSubAppView mappedSubApp = subAppView.getMappedSubApp();
		
		if(null != mappedSubApp){
			subAppView.getMappedSubApp().getInterfaceElements().remove(
				interfaceElement.getMappedInterfaceElement());
		}
		subAppView.getInterfaceElements().remove(interfaceElement);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		subAppView.getInterfaceElements().add(interfaceElement);

		MappedSubAppView mappedSubApp = subAppView.getMappedSubApp();
		
		if(null != mappedSubApp){
			mappedSubApp.getInterfaceElements().add(
				(MappedSubAppInterfaceElementView) interfaceElement
						.getMappedInterfaceElement());
		}
	}

}
