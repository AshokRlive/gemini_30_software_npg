/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.model.libraryElement.ResourceFBNetwork;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.MappedSubAppView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.util.Utils;

/**
 * The Class UnmapSubAppCommand.
 */
public class UnmapSubAppCommand extends Command {

	/** The editor. */
	private IEditorPart editor;

	/** The mappedSubAppView. */
	private final MappedSubAppView mappedSubAppView;

	/** The parent. */
	private UIResourceEditor parent;

	/** The appSubAppView. */
	private SubAppView appSubAppView;

	/** The cmds. */
	private final ArrayList<UnmapCommandRemoveConnection> cmds = new ArrayList<UnmapCommandRemoveConnection>();

	/** The cleanup cmds. */
	private final ArrayList<InterfaceCleanup> cleanupCmds = new ArrayList<InterfaceCleanup>();

	/**
	 * Instantiates a new UnmapSubAppCommand.
	 * 
	 * @param mappedSubAppView the mappedSubAppView
	 */
	public UnmapSubAppCommand(final MappedSubAppView mappedSubAppView) {
		this.mappedSubAppView = mappedSubAppView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return mappedSubAppView != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		editor.equals(Utils.getCurrentActiveEditor()); // required for later
		// enabling of undo
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();

		// FBView mappedFB = fbView.getMappedFB();
		parent = (UIResourceEditor) mappedSubAppView.eContainer();

		ArrayList<ConnectionView> connections = new ArrayList<ConnectionView>();
		// remove resource connections
		for (Iterator<MappedSubAppInterfaceElementView> iterator = mappedSubAppView
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			InterfaceElementView interfaceElement = iterator.next();

			for (Iterator<ConnectionView> iterator2 = interfaceElement
					.getInConnections().iterator(); iterator2.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				if (connectionView.eContainer().equals(
						mappedSubAppView.eContainer())) {
					// connection is a "resource connection" --> remove it
					connections.add(connectionView);
				}
			}
			for (Iterator<ConnectionView> iterator2 = interfaceElement
					.getOutConnections().iterator(); iterator2.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				if (connectionView.eContainer().equals(
						mappedSubAppView.eContainer())) {
					// connection is a "resource connection" --> remove it
					connections.add(connectionView);
				}
			}
		}

		for (Iterator<ConnectionView> iterator = connections.iterator(); iterator
				.hasNext();) {
			ConnectionView connView = iterator.next();
			UnmapCommandRemoveConnection cmd = new UnmapCommandRemoveConnection(
					parent, connView);
			cmds.add(cmd);
		}
		for (Iterator<UnmapCommandRemoveConnection> iterator = cmds.iterator(); iterator
				.hasNext();) {
			UnmapCommandRemoveConnection cmd = iterator.next();
			if (cmd.canExecute()) {
				cmd.execute();
			}
		}

		for (Iterator<MappedSubAppInterfaceElementView> iterator = mappedSubAppView
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			InterfaceElementView interfaceElement = iterator.next();
			InterfaceCleanup cmd = new InterfaceCleanup(interfaceElement);
			cleanupCmds.add(cmd);
		}

		for (Iterator<InterfaceCleanup> iterator = cleanupCmds.iterator(); iterator
				.hasNext();) {
			InterfaceCleanup cmd = iterator.next();
			if (cmd.canExecute()) {
				cmd.execute();
			}
		}

		appSubAppView = mappedSubAppView.getApplicationSubApp();
		mappedSubAppView.getApplicationSubApp().setMappedSubApp(null);
		// remove fbView fb from its resource where it is mapped
		parent.getNetwork().getSubApps().remove(mappedSubAppView.getSubApp());
		if (parent.getNetwork() instanceof ResourceFBNetwork) {
			((ResourceFBNetwork) parent.getNetwork()).getMappedSubApps()
					.remove(mappedSubAppView.getSubApp());
		}
		// remove fbView from its parent
		parent.getChildren().remove(mappedSubAppView);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		parent.getChildren().add(mappedSubAppView);
		parent.getResourceElement().getFBNetwork().getMappedSubApps().add(
				mappedSubAppView.getSubApp());
		if (parent.getNetwork() instanceof ResourceFBNetwork) {
			((ResourceFBNetwork) parent.getNetwork()).getMappedSubApps().add(
					mappedSubAppView.getSubApp());
		}
		mappedSubAppView.setApplicationSubApp(appSubAppView);
		appSubAppView.setMappedSubApp(mappedSubAppView);

		for (Iterator<UnmapCommandRemoveConnection> iterator = cmds.iterator(); iterator
				.hasNext();) {
			UnmapCommandRemoveConnection cmd = iterator.next();
			cmd.undo();
		}
		for (Iterator<InterfaceCleanup> iterator = cleanupCmds.iterator(); iterator
				.hasNext();) {
			InterfaceCleanup cmd = iterator.next();
			cmd.undo();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		for (Iterator<InterfaceCleanup> iterator = cleanupCmds.iterator(); iterator
				.hasNext();) {
			InterfaceCleanup cmd = iterator.next();
			cmd.redo();
		}
		for (Iterator<UnmapCommandRemoveConnection> iterator = cmds.iterator(); iterator
				.hasNext();) {
			UnmapCommandRemoveConnection cmd = iterator.next();
			cmd.redo();
		}
		mappedSubAppView.getApplicationSubApp().setMappedSubApp(null);
		parent.getResourceElement().getFBNetwork().getMappedSubApps().remove(
				mappedSubAppView.getSubApp());
		if (parent.getNetwork() instanceof ResourceFBNetwork) {
			((ResourceFBNetwork) parent.getNetwork()).getMappedSubApps()
					.remove(mappedSubAppView.getSubApp());
		}
		parent.getChildren().remove(mappedSubAppView);

	}
}
