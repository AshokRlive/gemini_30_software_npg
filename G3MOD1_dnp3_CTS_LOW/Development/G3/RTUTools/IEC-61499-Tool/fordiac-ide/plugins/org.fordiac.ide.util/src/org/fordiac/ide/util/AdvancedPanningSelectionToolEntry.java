/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util;

import org.eclipse.gef.palette.PanningSelectionToolEntry;

/**
 * The Class AdvancedPanningSelectionToolEntry.
 */
public class AdvancedPanningSelectionToolEntry extends
		PanningSelectionToolEntry {

	/**
	 * Constructor for PanningSelectionToolEntry.
	 * 
	 * @param label the label
	 * @param shortDesc the description
	 */
	public AdvancedPanningSelectionToolEntry(String label, String shortDesc) {
		super(label, shortDesc);
		setToolClass(AdvancedPanningSelectionTool.class);
	}
}
