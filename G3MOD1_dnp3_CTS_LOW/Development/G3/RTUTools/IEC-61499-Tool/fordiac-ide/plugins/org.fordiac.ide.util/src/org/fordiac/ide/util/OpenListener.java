package org.fordiac.ide.util;

import org.eclipse.ui.IEditorPart;

public abstract class OpenListener implements IOpenListener {

	protected IEditorPart editor = null;
	
	@Override
	public IEditorPart getOpenedEditor() {
		return editor;
	}
	
}
