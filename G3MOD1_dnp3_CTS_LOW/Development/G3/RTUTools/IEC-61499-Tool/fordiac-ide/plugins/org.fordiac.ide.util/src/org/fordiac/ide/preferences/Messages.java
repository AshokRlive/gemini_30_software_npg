/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.preferences;

import org.eclipse.osgi.util.NLS;

/**
 * The Class Messages.
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.ide.preferences.messages"; //$NON-NLS-1$
	
	/** The Fordiac preference page_ labe l_ default basic fb background color. */
	public static String FordiacPreferencePage_LABEL_DefaultBasicFBBackgroundColor;
	
	/** The Fordiac preference page_ labe l_ default compliance profile. */
	public static String FordiacPreferencePage_LABEL_DefaultComplianceProfile;
	
	/** The Fordiac preference page_ labe l_ default composite fb background color. */
	public static String FordiacPreferencePage_LABEL_DefaultCompositeFBBackgroundColor;
	
	/** The Fordiac preference page_ labe l_ default data connector color. */
	public static String FordiacPreferencePage_LABEL_DefaultDataConnectorColor;
	
	public static String FordiacPreferencePage_LABEL_DefaultAdapterConnectorColor;

	/** The Fordiac preference page_ labe l_ default data connector color. */
	public static String FordiacPreferencePage_LABEL_SyncFBColorWithDeviceColor;
	
	/** The Fordiac preference page_ labe l_ default event connector color. */
	public static String FordiacPreferencePage_LABEL_DefaultEventConnectorColor;
	
	/** The Fordiac preference page_ labe l_ default sifb background color. */
	public static String FordiacPreferencePage_LABEL_DefaultSIFBBackgroundColor;
	
	/** The Fordiac preference page_ labe l_ interface eelement of sub apps visibility. */
	public static String FordiacPreferencePage_LABEL_InterfaceEelementOfSubAppsVisibility;
	
	/** The Fordiac preference page_ labe l_ preference page description. */
	public static String FordiacPreferencePage_LABEL_PreferencePageDescription;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
		// private empty constructor
	}
}
