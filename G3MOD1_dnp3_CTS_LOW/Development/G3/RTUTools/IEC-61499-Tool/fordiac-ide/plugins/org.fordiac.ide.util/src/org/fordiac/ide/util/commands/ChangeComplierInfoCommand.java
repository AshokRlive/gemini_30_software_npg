/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.CompilableType;
import org.fordiac.ide.model.libraryElement.CompilerInfo;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;

/**
 * The Class ChangeComplierInfoCommand.
 */
public abstract class ChangeComplierInfoCommand extends Command {

	/** The identification of the type. */
	protected CompilerInfo compilerInfo;

	/**
	 * Instantiates a new change comment command.
	 * 
	 * @param type which identification information is about to change
	 * @param comment the comment
	 */
	public ChangeComplierInfoCommand(final CompilableType type) {
		super();
		
		if (type.getCompilerInfo() == null) {
			CompilerInfo compilerInfo = LibraryElementFactory.eINSTANCE
					.createCompilerInfo();
			type.setCompilerInfo(compilerInfo);
		}	
		
		this.compilerInfo = type.getCompilerInfo();
	}


}
