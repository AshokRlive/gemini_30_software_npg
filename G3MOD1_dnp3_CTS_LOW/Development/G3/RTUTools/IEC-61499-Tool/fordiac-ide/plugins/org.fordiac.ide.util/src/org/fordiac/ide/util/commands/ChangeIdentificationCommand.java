/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.Identification;
import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;

/**
 * The Class ChangeCommentCommand.
 */
public abstract class ChangeIdentificationCommand extends Command {

	/** The identification of the type. */
	protected Identification identification;

	/**
	 * Instantiates a new change comment command.
	 * 
	 * @param type which identification information is about to change
	 * @param comment the comment
	 */
	public ChangeIdentificationCommand(LibraryElement type) {
		super();
		if (null == type.getIdentification()) {
			type.setIdentification(LibraryElementFactory.eINSTANCE
					.createIdentification());
		}
		identification = type.getIdentification();
	}


}
