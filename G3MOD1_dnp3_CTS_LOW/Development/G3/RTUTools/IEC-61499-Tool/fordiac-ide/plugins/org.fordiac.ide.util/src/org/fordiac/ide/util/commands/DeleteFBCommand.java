/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.model.NameRepository;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.SubAppNetwork;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.util.Utils;

/**
 * DeleteCommand for deleting a FB from a FBNetwork.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class DeleteFBCommand extends Command {

	/** The editor. */
	private IEditorPart editor;

	/** The view parent. */
	private Diagram viewParent;

	/** The fb parent. */
	private SubAppNetwork fbParent;

	/** The view. */
	private FBView view;

	/** The fb. */
	private FB fb;

	/** The cmd. */
	private UnmapCommand cmd;

	/**
	 * Instantiates a new delete fb command.
	 * 
	 * @param view
	 *            the view
	 */
	public DeleteFBCommand(final FBView view) {
		super("Delete FB");
		this.view = view;
		fb = view.getFb();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor != null && editor.equals(Utils.getCurrentActiveEditor());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		boolean deleteAble = !fb.isResourceTypeFB();
		if (!deleteAble) {
			Utils.getCurrentActiveEditor()
					.getEditorSite()
					.getActionBars()
					.getStatusLineManager()
					.setErrorMessage("Can not be deleted -> is ResourceType FB");
		}
		return deleteAble;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		// view = fbEditPart.getCastedModel();
		if (view.getMappedFB() != null) {
			cmd = new UnmapCommand(view.getMappedFB());
			// if (cmd.canExecute()) {
			// cmd.execute();
			// }
		} else if (view.getApplicationFB() != null) {
			view = view.getApplicationFB();
			cmd = new UnmapCommand(view.getMappedFB());
		}
		if (cmd != null && cmd.canExecute()) {
			resourceEditor = (UIResourceEditor) cmd.getMappedFBView()
					.eContainer();
			cmd.execute();
		}

		if (resourceEditor != null) {
			deleteVirtualIOs(resourceEditor, fb);
		}
		viewParent = (Diagram) view.eContainer();
		fbParent = (SubAppNetwork) fb.eContainer();

		deleteConnections(view);


		if (viewParent instanceof UISubAppNetwork) {
			createRemoveInterfaceElementCommands(
					((UISubAppNetwork) viewParent), view);
			removeInterfaceElements();
		}

		NameRepository.getInstance().removeInstanceName(fb);  //this has to be done before it is removed from the parent
		fbParent.getFBs().remove(fb);
	}

	private void deleteVirtualIOs(UIResourceEditor parent, FB fb) {
		// find virtual IOs
		ArrayList<InterfaceElementView> virtualIOs = new ArrayList<InterfaceElementView>();
		for (Iterator<View> iterator = parent.getChildren().iterator(); iterator
				.hasNext();) {
			View view = iterator.next();
			if (view instanceof InterfaceElementView) {
				virtualIOs.add((InterfaceElementView) view);
			}
		}
		connectionViewToBeDeleted = new ArrayList<DeleteVirtualIOConnection>();
		virtualIOsToBeDeleted = new ArrayList<InterfaceElementView>();
		// check if virtual IO has connections to the mappedfb
		for (Iterator<InterfaceElementView> iterator = virtualIOs.iterator(); iterator
				.hasNext();) {
			InterfaceElementView interfaceElementView = iterator.next();
			if (fb.getInterface().getInputVars()
					.contains(interfaceElementView.getIInterfaceElement())
					|| fb.getInterface()
							.getOutputVars()
							.contains(
									interfaceElementView.getIInterfaceElement())
					|| fb.getInterface()
							.getEventInputs()
							.contains(
									interfaceElementView.getIInterfaceElement())
					|| fb.getInterface()
							.getEventOutputs()
							.contains(
									interfaceElementView.getIInterfaceElement())) {

				for (Iterator<ConnectionView> iterator2 = interfaceElementView
						.getInConnections().iterator(); iterator2.hasNext();) {
					ConnectionView connView = iterator2.next();
					connectionViewToBeDeleted
							.add(new DeleteVirtualIOConnection(connView, parent));
				}
				for (Iterator<ConnectionView> iterator2 = interfaceElementView
						.getOutConnections().iterator(); iterator2.hasNext();) {
					ConnectionView connView = iterator2.next();
					connectionViewToBeDeleted
							.add(new DeleteVirtualIOConnection(connView, parent));
				}

				virtualIOsToBeDeleted.add(interfaceElementView);
			}
		}

		for (Iterator<DeleteVirtualIOConnection> iterator = connectionViewToBeDeleted
				.iterator(); iterator.hasNext();) {
			DeleteVirtualIOConnection cmd = iterator.next();
			cmd.execute();
		}

		for (Iterator<InterfaceElementView> iterator = virtualIOsToBeDeleted
				.iterator(); iterator.hasNext();) {
			InterfaceElementView interfaceElementView = iterator.next();
			parent.getChildren().remove(interfaceElementView);
		}
	}

	private class DeleteVirtualIOConnection extends Command {

		private final ConnectionView connectionView;
		private final UIResourceEditor parent;

		private InterfaceElementView source;
		private InterfaceElementView dest;

		public DeleteVirtualIOConnection(ConnectionView connectionView,
				UIResourceEditor parent) {
			this.connectionView = connectionView;
			this.parent = parent;
		}

		@Override
		public void execute() {
			source = connectionView.getSource();
			dest = connectionView.getDestination();
			connectionView.setSource(null);
			connectionView.setDestination(null);
			parent.getConnections().remove(connectionView);
		}

		@Override
		public void undo() {
			connectionView.setSource(source);
			connectionView.setDestination(dest);
			parent.getConnections().add(connectionView);
		}

		@Override
		public void redo() {
			connectionView.setSource(null);
			connectionView.setDestination(null);
			parent.getConnections().remove(connectionView);
		}

	}

	/**
	 * Removes the interface elements.
	 */
	private void removeInterfaceElements() {
		for (Iterator<RemoveInterfaceElementCommand> iterator = removeInterfaceElementCommands
				.iterator(); iterator.hasNext();) {
			RemoveInterfaceElementCommand cmd = iterator.next();
			cmd.execute();
		}
	}

	/** The delete cmds. */
	ArrayList<DeleteConnectionCommand> deleteCmds = new ArrayList<DeleteConnectionCommand>();

	/**
	 * Delete connections.
	 * 
	 * @param view
	 *            the view
	 */
	private void deleteConnections(final FBView view) {
		for (Iterator<InterfaceElementView> iterator = view
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			InterfaceElementView element = iterator.next();
			ArrayList<ConnectionView> temp = new ArrayList<ConnectionView>(
					element.getInConnections());
			for (Iterator<ConnectionView> iterator2 = temp.iterator(); iterator2
					.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				DeleteConnectionCommand dcc = new DeleteConnectionCommand(
						connectionView);
				deleteCmds.add(dcc);
				dcc.execute();
			}
			temp = new ArrayList<ConnectionView>(element.getOutConnections());
			for (Iterator<ConnectionView> iterator2 = temp.iterator(); iterator2
					.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				DeleteConnectionCommand dcc = new DeleteConnectionCommand(
						connectionView);
				deleteCmds.add(dcc);
				dcc.execute();
			}
		}
	}

	/** The remove interface element commands. */
	private final ArrayList<RemoveInterfaceElementCommand> removeInterfaceElementCommands = new ArrayList<RemoveInterfaceElementCommand>();

	private UIResourceEditor resourceEditor;

	private ArrayList<DeleteVirtualIOConnection> connectionViewToBeDeleted;

	private ArrayList<InterfaceElementView> virtualIOsToBeDeleted;

	/**
	 * Creates the remove interface element commands.
	 * 
	 * @param uiSubAppNetwork
	 *            the ui sub app network
	 * @param fbview
	 *            the fbview
	 */
	private void createRemoveInterfaceElementCommands(
			final UISubAppNetwork uiSubAppNetwork, final FBView fbview) {
		List<InterfaceElementView> interfaceList = new ArrayList<InterfaceElementView>(
				fbview.getInterfaceElements());
		for (Iterator<InterfaceElementView> iterator = interfaceList.iterator(); iterator
				.hasNext();) {
			InterfaceElementView interfaceElement = iterator.next();
			for (Iterator<SubAppInterfaceElementView> iterator2 = uiSubAppNetwork
					.getSubAppView().getInterfaceElements().iterator(); iterator2
					.hasNext();) {
				SubAppInterfaceElementView interfaceElementView = iterator2
						.next();
				if (interfaceElementView.getIInterfaceElement().equals(
						interfaceElement.getIInterfaceElement())) {
					RemoveInterfaceElementCommand cmd = new RemoveInterfaceElementCommand(
							uiSubAppNetwork.getSubAppView(),
							interfaceElementView);
					removeInterfaceElementCommands.add(cmd);
				}
			}
		}
		if (uiSubAppNetwork.getSubAppView().eContainer() instanceof UISubAppNetwork) {
			createRemoveInterfaceElementCommands(
					(UISubAppNetwork) uiSubAppNetwork.getSubAppView()
							.eContainer(), fbview);
		}
	}

	/**
	 * Redo.
	 * 
	 * @see DeleteFBCommand#execute()
	 */
	@Override
	public void redo() {

		if (cmd != null) {
			cmd.redo();
		}

		reDeleteConnections();

		removeInterfaceElements();
		NameRepository.getInstance().removeInstanceName(fb);  //this has to be done before the fb is removed from the parent
		fbParent.getFBs().remove(fb);

		if (resourceEditor != null) {
			for (Iterator<DeleteVirtualIOConnection> iterator = connectionViewToBeDeleted
					.iterator(); iterator.hasNext();) {
				DeleteVirtualIOConnection cmd = iterator.next();
				cmd.redo();
			}

			for (Iterator<InterfaceElementView> iterator = virtualIOsToBeDeleted
					.iterator(); iterator.hasNext();) {
				InterfaceElementView interfaceElementView = iterator.next();
				resourceEditor.getChildren().remove(interfaceElementView);
			}
		}
	}

	/**
	 * Re delete connections.
	 */
	private void reDeleteConnections() {
		for (Iterator<DeleteConnectionCommand> iterator = deleteCmds.iterator(); iterator
				.hasNext();) {
			DeleteConnectionCommand cmd = iterator.next();
			cmd.redo();
		}

	}

	/**
	 * Restores the removed Connections.
	 * 
	 * @see DeleteFBCommand#deleteConnections()
	 */
	protected void restoreConnections() {
		for (Iterator<DeleteConnectionCommand> iterator = deleteCmds.iterator(); iterator
				.hasNext();) {
			DeleteConnectionCommand cmd = iterator.next();
			cmd.undo();
		}
	}

	/**
	 * Restores the removed Connections and adds the UIFB to the UIApplication.
	 */
	@Override
	public void undo() {
		restoreInterfaceElements();
		viewParent.getChildren().add(view);
		fbParent.getFBs().add(fb);
		
		NameRepository.getInstance().addSystemUniqueFBInstanceName(fb);
		
		restoreConnections();
		if (cmd != null && cmd.canUndo()) {
			cmd.undo();
		}

		if (resourceEditor != null) {
			for (Iterator<DeleteVirtualIOConnection> iterator = connectionViewToBeDeleted
					.iterator(); iterator.hasNext();) {
				DeleteVirtualIOConnection cmd = iterator.next();
				cmd.undo();
			}

			for (Iterator<InterfaceElementView> iterator = virtualIOsToBeDeleted
					.iterator(); iterator.hasNext();) {
				InterfaceElementView interfaceElementView = iterator.next();
				resourceEditor.getChildren().add(interfaceElementView);
			}
		}
	}

	/**
	 * Restore interface elements.
	 */
	private void restoreInterfaceElements() {
		for (Iterator<RemoveInterfaceElementCommand> iterator = removeInterfaceElementCommands
				.iterator(); iterator.hasNext();) {
			RemoveInterfaceElementCommand cmd = iterator.next();
			cmd.undo();
		}

	}

}
