/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util.comm.channels;

import java.util.List;

import org.fordiac.ide.util.comm.datatypes.IEC_ANY;
import org.fordiac.ide.util.comm.exceptions.CommException;

public interface IIecSender extends IIecCommObject {

	public abstract void SendIECData(List<IEC_ANY> sendData) throws CommException;
}
