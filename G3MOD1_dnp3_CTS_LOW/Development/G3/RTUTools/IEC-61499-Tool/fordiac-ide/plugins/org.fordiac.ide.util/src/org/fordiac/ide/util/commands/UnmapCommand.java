/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.util.Utils;

/**
 * The Class UnmapCommand.
 */
public class UnmapCommand extends Command {

	/** The editor. */
	private IEditorPart editor;

	/** The mapped fb view. */
	private final FBView mappedFBView;

	/**
	 * Gets the mapped fb view.
	 * 
	 * @return the mapped fb view
	 */
	public FBView getMappedFBView() {
		return mappedFBView;
	}

	/** The parent. */
	private UIResourceEditor parent;

	/** The app fb view. */
	private FBView appFBView;

	/** The cmds. */
	private final ArrayList<UnmapCommandRemoveConnection> cmds = new ArrayList<UnmapCommandRemoveConnection>();

	/** The cleanup cmds. */
	private final ArrayList<InterfaceCleanup> cleanupCmds = new ArrayList<InterfaceCleanup>();

	/**
	 * Instantiates a new unmap command.
	 * 
	 * @param mappedFBView
	 *            the mapped fb view
	 */
	public UnmapCommand(final FBView mappedFBView) {
		this.mappedFBView = mappedFBView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		
		return mappedFBView != null && mappedFBView.getFb() != null && !mappedFBView.getFb().isResourceTypeFB();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor != null && editor.equals(Utils.getCurrentActiveEditor());
		// return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();

		// FBView mappedFB = fbView.getMappedFB();
		parent = (UIResourceEditor) mappedFBView.eContainer();

		ArrayList<ConnectionView> connections = new ArrayList<ConnectionView>();

		// remove resource connections
		for (Iterator<InterfaceElementView> iterator = mappedFBView
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			InterfaceElementView interfaceElement = iterator.next();

			for (Iterator<ConnectionView> iterator2 = interfaceElement
					.getInConnections().iterator(); iterator2.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				if (connectionView.eContainer().equals(
						mappedFBView.eContainer())) {
					// connection is a "resource connection" --> remove it
					connections.add(connectionView);
				}
			}
			for (Iterator<ConnectionView> iterator2 = interfaceElement
					.getOutConnections().iterator(); iterator2.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				if (connectionView.eContainer().equals(
						mappedFBView.eContainer())) {
					// connection is a "resource connection" --> remove it
					connections.add(connectionView);
				}
			}
		}

		for (Iterator<ConnectionView> iterator = connections.iterator(); iterator
				.hasNext();) {
			ConnectionView connView = iterator.next();
			UnmapCommandRemoveConnection cmd = new UnmapCommandRemoveConnection(
					parent, connView);
			cmds.add(cmd);
		}
		for (Iterator<UnmapCommandRemoveConnection> iterator = cmds.iterator(); iterator
				.hasNext();) {
			UnmapCommandRemoveConnection cmd = iterator.next();
			if (cmd.canExecute()) {
				cmd.execute();
			}
		}

		for (Iterator<InterfaceElementView> iterator = mappedFBView
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			InterfaceElementView interfaceElement = iterator.next();
			InterfaceCleanup cmd = new InterfaceCleanup(interfaceElement);
			cleanupCmds.add(cmd);
		}
		for (Iterator<InterfaceCleanup> iterator = cleanupCmds.iterator(); iterator
				.hasNext();) {
			InterfaceCleanup cmd = iterator.next();
			if (cmd.canExecute()) {
				cmd.execute();
			}
		}

		appFBView = mappedFBView.getApplicationFB();
		mappedFBView.getApplicationFB().setMappedFB(null);
		// remove fbView fb from its resource where it is mapped
		parent.getResourceElement().getFBNetwork().getMappedFBs().remove(
				mappedFBView.getFb());
		// parent.getResourceElement().getFBNetwork().getFBs().remove(mappedFBView
		// .getFb());
		// remove fbView from its parent
		parent.getChildren().remove(mappedFBView);

		updateBrokenConnectionState();

	}

	private void updateBrokenConnectionState() {
		for (Iterator<InterfaceElementView> iterator = appFBView
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			InterfaceElementView interfaceElement = iterator.next();

			for (Iterator<ConnectionView> iterator2 = interfaceElement
					.getInConnections().iterator(); iterator2.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				if (connectionView.getSource().eContainer() instanceof FBView) {
					FBView sourceFBView = ((FBView) connectionView.getSource()
							.eContainer()).getMappedFB();
					connectionView.getConnectionElement().setBrokenConnection(
							!(sourceFBView == null));
				}
			}
			
			for (Iterator<ConnectionView> iterator2 = interfaceElement
					.getOutConnections().iterator(); iterator2.hasNext();) {
				ConnectionView connectionView = iterator2.next();
				if (connectionView.getDestination().eContainer() instanceof FBView
						&& !connectionView.getSource().eContainer().equals(
								connectionView.getDestination().eContainer())) { 
					FBView destFBView = ((FBView) connectionView
							.getDestination().eContainer()).getMappedFB();
					connectionView.getConnectionElement().setBrokenConnection(
							!(destFBView == null));
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		parent.getChildren().add(mappedFBView);
		parent.getResourceElement().getFBNetwork().getMappedFBs().add(
				mappedFBView.getFb());
		mappedFBView.setApplicationFB(appFBView);
		appFBView.setMappedFB(mappedFBView);

		updateBrokenConnectionState();
		
		for (Iterator<InterfaceCleanup> iterator = cleanupCmds.iterator(); iterator
				.hasNext();) {
			InterfaceCleanup cmd = iterator.next();
			cmd.undo();
		}
		for (Iterator<UnmapCommandRemoveConnection> iterator = cmds.iterator(); iterator
				.hasNext();) {
			UnmapCommandRemoveConnection cmd = iterator.next();
			cmd.undo();
		}
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		for (Iterator<UnmapCommandRemoveConnection> iterator = cmds.iterator(); iterator
				.hasNext();) {
			UnmapCommandRemoveConnection cmd = iterator.next();
			cmd.redo();
		}
		for (Iterator<InterfaceCleanup> iterator = cleanupCmds.iterator(); iterator
				.hasNext();) {
			InterfaceCleanup cmd = iterator.next();
			cmd.redo();
		}
		mappedFBView.getApplicationFB().setMappedFB(null);
		parent.getResourceElement().getFBNetwork().getMappedFBs().remove(
				mappedFBView.getFb());
		parent.getChildren().remove(mappedFBView);

		updateBrokenConnectionState();
	}
}
