/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.libraryElement.DataConnection;
import org.fordiac.ide.model.libraryElement.EventConnection;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.Resource;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppView;
import org.fordiac.ide.model.ui.UIFBNetwork;
import org.fordiac.ide.model.ui.UIResourceEditor;
import org.fordiac.ide.model.ui.UISubAppNetwork;
import org.fordiac.ide.model.ui.UiFactory;
import org.fordiac.ide.model.ui.View;

/**
 * The Class ConnectionUtil.
 */
public class ConnectionUtil {

	/**
	 * Creates the connection in resource.
	 * 
	 * @param mappedSource the mapped source
	 * @param mappedDest the mapped dest
	 * @param origConnection the orig connection
	 * 
	 * @return the connection view
	 */
	public static ConnectionView createConnectionInResource(
			final InterfaceElementView mappedSource,
			final InterfaceElementView mappedDest,
			final ConnectionView origConnection) {
		if (mappedSource != null && mappedDest != null) {
			if (mappedSource.getFbNetwork() != null
					&& mappedSource.getFbNetwork().equals(
							mappedDest.getFbNetwork())) {
				ConnectionView newConnection = (ConnectionView) EcoreUtil
						.copy(origConnection);
				newConnection.setSource(mappedSource);
				newConnection.setDestination(mappedDest);
				UIResourceEditor resourceEditor = ((UIResourceEditor) mappedSource
						.eContainer().eContainer());
				if (resourceEditor != null) {
					resourceEditor.getConnections().add(newConnection);
					ConnectionUtil.addConnectionToResource(origConnection
							.getConnectionElement(), resourceEditor
							.getResourceElement());
				}
				return newConnection;
			}
		}
		return null;
	}

	/**
	 * Adds the connection to resource.
	 * 
	 * @param connectionElement the connection element
	 * @param resourceElement the resource element
	 */
	public static void addConnectionToResource(
			final Connection connectionElement, final Resource resourceElement) {
		if (connectionElement instanceof EventConnection) {
			resourceElement.getFBNetwork().getEventConnections().add(
					(EventConnection) connectionElement);
		}
		if (connectionElement instanceof DataConnection) {
			resourceElement.getFBNetwork().getDataConnections().add(
					(DataConnection) connectionElement);
		}
	}

	/**
	 * Creates the connection in app.
	 * 
	 * @param appSource the app source
	 * @param appDest the app dest
	 * @param origConnection the orig connection
	 * 
	 * @return the connection view
	 */
	public static ConnectionView createConnectionInApp(
			final InterfaceElementView appSource,
			final InterfaceElementView appDest,
			final ConnectionView origConnection) {
		if (appSource != null && appDest != null) {
			if (appSource.getFbNetwork() != null
					&& appSource.getFbNetwork().equals(appDest.getFbNetwork())) {
				ConnectionView newConnection = (ConnectionView) EcoreUtil
						.copy(origConnection);
				newConnection.setSource(appSource);
				newConnection.setDestination(appDest);
				((UIFBNetwork) appSource.eContainer().eContainer())
						.getConnections().add(newConnection);
				return newConnection;
			}
		}
		return null;
	}

	/**
	 * Find interface element.
	 * 
	 * @param uiSubAppNetwork the ui sub app network
	 * @param element the element
	 * 
	 * @return the interface element view
	 */
	public static InterfaceElementView findInterfaceElement(
			final UISubAppNetwork uiSubAppNetwork,
			final IInterfaceElement element) {
		for (Iterator<View> iterator = uiSubAppNetwork.getChildren().iterator(); iterator
				.hasNext();) {
			View view = iterator.next();
			if (view instanceof FBView) {
				FBView fbView = (FBView) view;
				for (Iterator<InterfaceElementView> iterator2 = fbView
						.getInterfaceElements().iterator(); iterator2.hasNext();) {
					InterfaceElementView interfaceElementView = iterator2
							.next();
					if (interfaceElementView.getIInterfaceElement().equals(
							element)) {
						return interfaceElementView;
					}
				}
			}
			if (view instanceof SubAppView) {
				// FIXME what happens if the dest in the subapp?=?????
				SubAppView subAppView = (SubAppView) view;
				for (Iterator<SubAppInterfaceElementView> iterator2 = subAppView
						.getInterfaceElements().iterator(); iterator2.hasNext();) {
					InterfaceElementView interfaceElementView = iterator2
							.next();
					if (interfaceElementView.getIInterfaceElement().equals(
							element)) {
						return interfaceElementView;
					}
				}
			}
		}
		return null;
	}

	/**
	 * searches an InternalSubAppInterfaceElementView in the network, if not
	 * found a new one is returned that is already added to the internal
	 * interface of the network.
	 * 
	 * @param interfaceElement the interface element
	 * @param network the network
	 * 
	 * @return if not found a new one is returned
	 */
	public static InternalSubAppInterfaceElementView findInternalInterfaceElement(
			final IInterfaceElement interfaceElement,
			final UISubAppNetwork network) {
		for (Iterator<InternalSubAppInterfaceElementView> iterator = network
				.getInterfaceElements().iterator(); iterator.hasNext();) {
			InternalSubAppInterfaceElementView internalInterfaceElement = iterator
					.next();
			if (internalInterfaceElement.getIInterfaceElement().equals(
					interfaceElement)) {
				return internalInterfaceElement;
			}
		}
		// not found, return new
		InternalSubAppInterfaceElementView internalInterfaceElement = UiFactory.eINSTANCE
				.createInternalSubAppInterfaceElementView();

		internalInterfaceElement.setIInterfaceElement(interfaceElement);

		network.getInterfaceElements().add(internalInterfaceElement);

		return internalInterfaceElement;
	}

	/**
	 * Creates the internal source connection.
	 * 
	 * @param subAppInterfaceElementViewDest the sub app interface element view dest
	 * @param destNetwork the dest network
	 * @param originalConnection the original connection
	 * 
	 * @return the list< internal connection>
	 */
	public static List<InternalConnection> createInternalSourceConnection(
			final SubAppInterfaceElementView subAppInterfaceElementViewDest,
			final UISubAppNetwork destNetwork,
			final ConnectionView originalConnection) {
		ArrayList<InternalConnection> internalConnections = new ArrayList<InternalConnection>();
		InternalConnection internalConnection = new InternalConnection();
		// copy the existing connection (the connection the user currently
		// modeled)
		ConnectionView newConnectionView = (ConnectionView) EcoreUtil
				.copy(originalConnection);
		internalConnection.setConnectionView(newConnectionView);

		// get the "InternalInterfaceElement" of the uiSubAppNetwork (the
		// UISubAppNetwork of the SubAppView of the target element)
		InternalSubAppInterfaceElementView internalInterfaceElementViewDest = ConnectionUtil
				.findInternalInterfaceElement(originalConnection
						.getDestination().getIInterfaceElement(), destNetwork);
		internalConnection
				.setInternalInterfaceElementView(internalInterfaceElementViewDest);

		// set the internal interfacelementview (mapping between the internal
		// and external interface elements of a subApp)
		originalConnection
				.setInternalSourceInterfaceElementView(internalInterfaceElementViewDest);
		newConnectionView
				.setExternalDestinationInterfaceElementView(subAppInterfaceElementViewDest);

		internalConnection
				.setSubAppInterfaceElementView(subAppInterfaceElementViewDest);

		// set the correct source and dest of the copied connection
		newConnectionView.setSource(ConnectionUtil.findInterfaceElement(
				destNetwork, subAppInterfaceElementViewDest
						.getIInterfaceElement()));
		newConnectionView.setDestination(internalInterfaceElementViewDest);
		internalConnection.setSource(newConnectionView.getSource());
		internalConnection.setDest(newConnectionView.getDestination());

		// add the newly created connection to its container
		destNetwork.getConnections().add(newConnectionView);
		internalConnection.setUiSubAppNetwork(destNetwork);

		// add the internalConnection element to the internal connections list
		// -> required for undo redo
		internalConnections.add(internalConnection);

		// check if the target of the newly created connection is a
		// subAppInterfaceElementView --> further interal connection is
		// requrired

		if (newConnectionView.getSource() instanceof SubAppInterfaceElementView) {
			SubAppInterfaceElementView newSubAppInterfaceElementViewDest = (SubAppInterfaceElementView) newConnectionView
					.getSource();
			UISubAppNetwork newDestNetwork = ((SubAppView) newSubAppInterfaceElementViewDest
					.eContainer()).getUiSubAppNetwork();
			internalConnections.addAll(createInternalSourceConnection(
					newSubAppInterfaceElementViewDest, newDestNetwork,
					newConnectionView));
		}
		return internalConnections;
	}

	/**
	 * Creates the internal dest connection.
	 * 
	 * @param subAppInterfaceElementViewDest the sub app interface element view dest
	 * @param destNetwork the dest network
	 * @param originalConnection the original connection
	 * 
	 * @return the list< internal connection>
	 */
	public static List<InternalConnection> createInternalDestConnection(
			final SubAppInterfaceElementView subAppInterfaceElementViewDest,
			final UISubAppNetwork destNetwork,
			final ConnectionView originalConnection) {
		ArrayList<InternalConnection> internalConnections = new ArrayList<InternalConnection>();
		InternalConnection internalConnection = new InternalConnection();
		// copy the existing connection (the connection the user currently
		// modeled)
		ConnectionView newConnectionView = (ConnectionView) EcoreUtil
				.copy(originalConnection);
		internalConnection.setConnectionView(newConnectionView);

		// get the "InternalInterfaceElement" of the uiSubAppNetwork (the
		// UISubAppNetwork of the SubAppView of the target element)
		InternalSubAppInterfaceElementView internalInterfaceElementViewSrc = ConnectionUtil
				.findInternalInterfaceElement(originalConnection.getSource()
						.getIInterfaceElement(), destNetwork);
		internalConnection
				.setInternalInterfaceElementView(internalInterfaceElementViewSrc);

		// set the internal interfacelementview (mapping between the internal
		// and external interface elements of a subApp)
		originalConnection
				.setInternalDestinationInterfaceElementView(internalInterfaceElementViewSrc);
		newConnectionView
				.setExternalSourceInterfaceElementView(subAppInterfaceElementViewDest);

		internalConnection
				.setSubAppInterfaceElementView(subAppInterfaceElementViewDest);

		// set the correct source and dest of the copied connection
		newConnectionView.setSource(internalInterfaceElementViewSrc);
		newConnectionView.setDestination(ConnectionUtil.findInterfaceElement(
				destNetwork, subAppInterfaceElementViewDest
						.getIInterfaceElement()));
		internalConnection.setSource(newConnectionView.getSource());
		internalConnection.setDest(newConnectionView.getDestination());

		// add the newly created connection to its container
		destNetwork.getConnections().add(newConnectionView);
		internalConnection.setUiSubAppNetwork(destNetwork);

		// add the internalConnection element to the internal connections list
		// -> required for undo redo
		internalConnections.add(internalConnection);

		// check if the target of the newly created connection is a
		// subAppInterfaceElementView --> further interal connection is
		// requrired

		if (newConnectionView.getDestination() instanceof SubAppInterfaceElementView) {
			SubAppInterfaceElementView newSubAppInterfaceElementViewDest = (SubAppInterfaceElementView) newConnectionView
					.getDestination();
			UISubAppNetwork newDestNetwork = ((SubAppView) newSubAppInterfaceElementViewDest
					.eContainer()).getUiSubAppNetwork();
			internalConnections.addAll(createInternalDestConnection(
					newSubAppInterfaceElementViewDest, newDestNetwork,
					newConnectionView));
		}
		return internalConnections;
	}

	/**
	 * Undo internal connection.
	 * 
	 * @param internalConnections the internal connections
	 */
	public static void undoInternalConnection(
			final List<InternalConnection> internalConnections) {
		for (Iterator<InternalConnection> iterator = internalConnections
				.iterator(); iterator.hasNext();) {
			InternalConnection internalConnection = iterator.next();
			internalConnection.getConnectionView().setSource(null);
			internalConnection.getConnectionView().setDestination(null);
			internalConnection.getUiSubAppNetwork().getConnections().remove(
					internalConnection.getConnectionView());
			
			if (internalConnection.getInternalInterfaceElementView()
					.getOutConnections().size() == 0
					&& internalConnection.getInternalInterfaceElementView()
							.getInConnections().size() == 0) {
				internalConnection.getUiSubAppNetwork().getInterfaceElements()
						.remove(
								internalConnection
										.getInternalInterfaceElementView());
				// internalConnection.getSubAppInterfaceElementView()
				// .setInternalInterfaceElementView(null);
			}
		}
	}

	/**
	 * Redo internal connection.
	 * 
	 * @param internalConnections the internal connections
	 */
	public static void redoInternalConnection(
			final List<InternalConnection> internalConnections) {
		for (Iterator<InternalConnection> iterator = internalConnections
				.iterator(); iterator.hasNext();) {
			InternalConnection internalConnection = iterator.next();
			if (internalConnection.getInternalInterfaceElementView()
					.getOutConnections().size() == 0
					&& internalConnection.getInternalInterfaceElementView()
							.getInConnections().size() == 0) {
				internalConnection.getUiSubAppNetwork().getInterfaceElements()
						.add(
								internalConnection
										.getInternalInterfaceElementView());
				// internalConnection.getSubAppInterfaceElementView()
				// .setInternalInterfaceElementView(
				// internalConnection
				// .getInternalInterfaceElementView());
			}
			internalConnection.getConnectionView().setSource(
					internalConnection.getSource());
			internalConnection.getConnectionView().setDestination(
					internalConnection.getDest());
			internalConnection.getUiSubAppNetwork().getConnections().add(
					internalConnection.getConnectionView());
		}
	}

}
