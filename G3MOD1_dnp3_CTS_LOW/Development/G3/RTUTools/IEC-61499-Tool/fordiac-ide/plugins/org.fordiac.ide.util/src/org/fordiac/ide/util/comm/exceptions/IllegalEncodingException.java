/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util.comm.exceptions;

/**
 * @author Oliver Hummer (hummer@acin.tuwien.ac.at)
 * @author Ingo Hegny (hegny@acin.tuwien.ac.at)
 *
 */
public class IllegalEncodingException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	public IllegalEncodingException() {
	}

	/**
	 * @param arg0
	 */
	public IllegalEncodingException(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public IllegalEncodingException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public IllegalEncodingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
