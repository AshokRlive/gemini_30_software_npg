/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.typeimport.ImportUtils;

/**
 * The Class Utils.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class Utils {

	/**
	 * Creats a backup file of the specified file.
	 * 
	 * @param in
	 *            the file that should be backuped
	 * 
	 * @return the backup file
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static File createBakFile(final File in) throws IOException {
		String path = in.getAbsolutePath() + ".bak";
		File f;
		int i = 1;
		String temp = path;
		while ((f = new File(temp)).exists()) {
			temp = path + i;
			i++;
		}
		ImportUtils.copyFile(in, f);
		return f;
	}

	/**
	 * Copies a directory with all sub directories. If targetLocation does not
	 * exist, it will be created.
	 * 
	 * @param sourceLocation
	 *            the source directory
	 * @param targetLocation
	 *            the target directory, if it does not exist it will be created
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void copyDirectory(final File sourceLocation,
			final File targetLocation) throws IOException {

		if (sourceLocation.isDirectory()) {
			if (!targetLocation.exists()) {
				targetLocation.mkdirs();
			}

			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				copyDirectory(new File(sourceLocation, children[i]), new File(
						targetLocation, children[i]));
			}
		} else {
			ImportUtils.copyFile(sourceLocation, targetLocation);
		}
	}

	public static void copyDirectory(final IContainer sourceLocation,
			final IContainer targetLocation) throws IOException, CoreException {

		IProgressMonitor monitor = new NullProgressMonitor();

		if (!targetLocation.exists()) {
			((IFolder) targetLocation).create(true, true, monitor);
			targetLocation.refreshLocal(IResource.DEPTH_INFINITE, monitor);
		}

		for (IResource resource : sourceLocation.members()) {
			if (!resource.getName().startsWith(".")) {
				if (resource instanceof IFolder) {
					IFolder target = targetLocation.getFolder(new Path(resource
							.getName()));
					copyDirectory((IFolder) resource, target);
				} else if (resource instanceof IFile) {
					IFile file = targetLocation.getFile(new Path(resource
							.getName()));
					File in = ((IFile) resource).getLocation().toFile();
					File out = file.getLocation().toFile();
					ImportUtils.copyFile(in, out);
				}
			}
		}
		targetLocation.refreshLocal(IResource.DEPTH_INFINITE, monitor);
	}

	/**
	 * Gets the current active editor.
	 * 
	 * @return the current active editor
	 */
	public static IEditorPart getCurrentActiveEditor() {
		IWorkbenchWindow window = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();
		if (window != null && window.getActivePage() != null) {
			return window.getActivePage().getActiveEditor();
		}
		return null;
	}

	/**
	 * Hex string to int.
	 * 
	 * @param hexString
	 *            the hex string
	 * 
	 * @return the int
	 */
	public static int hexStringToInt(String hexString) {
		if (hexString.startsWith("0x")) {
			hexString = hexString.substring(2);
		}
		return Integer.parseInt(hexString, 16);
	}

	/**
	 * Format binary string.
	 * 
	 * @param binaryString
	 *            the binary string
	 * @param nrOfBits
	 *            the nr of bits
	 * 
	 * @return the string
	 */
	public static String formatBinaryString(final String binaryString,
			final int nrOfBits) {

		String zeros = "";
		int nrOfZeros = nrOfBits - binaryString.length();

		for (int i = 0; i < nrOfZeros; i++) {
			zeros = zeros + "0";
		}
		return zeros + binaryString;
	}

	/**
	 * Default type compatibility check.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * 
	 * @return true, if default type compatibility check
	 */
	public static boolean defaultTypeCompatibilityCheck(
			final VarDeclaration source, final VarDeclaration target) {
		try {
			if (source.getType().getName().toUpperCase().startsWith("ANY")//$NON-NLS-1$
					&& target.getType().getName().toUpperCase()
							.startsWith("ANY")) {//$NON-NLS-1$
				return checkAnyAnyCompatibility();
			}

			if (source.getType().getName().equalsIgnoreCase("ANY") //$NON-NLS-1$
					&& !target.getType().getName().equalsIgnoreCase("ANY")) { //$NON-NLS-1$
				return true;
			}
			if (!source.getType().getName().equalsIgnoreCase("ANY") //$NON-NLS-1$
					&& target.getType().getName().equalsIgnoreCase("ANY")) { //$NON-NLS-1$
				return true;
			}

			if (source.getType().getName().equalsIgnoreCase("ANY") //$NON-NLS-1$
					&& target.getType().getName().equalsIgnoreCase("ANY")) { //$NON-NLS-1$
				return checkAnyAnyCompatibility();
			}

			if (source.getType().getName().equalsIgnoreCase("ANY_ELEMENTARY") //$NON-NLS-1$
					&& !target.getType().getName()
							.equalsIgnoreCase("ANY_ELEMENTARY")) { //$NON-NLS-1$
				return true;
			}
			if (!source.getType().getName().equalsIgnoreCase("ANY_ELEMENTARY") //$NON-NLS-1$
					&& target.getType().getName()
							.equalsIgnoreCase("ANY_ELEMENTARY")) { //$NON-NLS-1$
				return true;
			}

			if (source.getType().getName().equalsIgnoreCase("ANY_ELEMENTARY") //$NON-NLS-1$
					&& target.getType().getName()
							.equalsIgnoreCase("ANY_ELEMENTARY")) { //$NON-NLS-1$
				return checkAnyAnyCompatibility();
			}

			if (source.getType().getName().equalsIgnoreCase("ANY_MAGNITUDE")) {
				return anyMagnitudeCompatibility(target.getType().getName());
			}

			if (target.getType().getName().equalsIgnoreCase("ANY_MAGNITUDE")) {
				return anyMagnitudeCompatibility(source.getType().getName());
			}

			if (source.getType().getName().equalsIgnoreCase("ANY_NUM")) {
				return anyNumCompatibility(target.getType().getName());
			}

			if (target.getType().getName().equalsIgnoreCase("ANY_NUM")) {
				return anyNumCompatibility(source.getType().getName());
			}

			if (source.getType().getName().equalsIgnoreCase("ANY_INT")) {
				return anyIntCompatiblity(target.getType().getName());
			}

			if (target.getType().getName().equalsIgnoreCase("ANY_INT")) {
				return anyIntCompatiblity(source.getType().getName());
			}

			if (source.getType().getName().equalsIgnoreCase("ANY_REAL")) {
				return anyRealCompatibility(target.getType().getName());
			}

			if (target.getType().getName().equalsIgnoreCase("ANY_REAL")) {
				return anyRealCompatibility(source.getType().getName());
			}

			if (source.getType().getName().equalsIgnoreCase("ANY_BIT")) { //$NON-NLS-1$
				return anyBitCompatibility(target.getType().getName());
			}

			if (target.getType().getName().equalsIgnoreCase("ANY_BIT")) { //$NON-NLS-1$
				return anyBitCompatibility(source.getType().getName());
			}

			if ((source.getType().getName().equalsIgnoreCase("ANY_STRING"))//$NON-NLS-1$
					&& (target.getType().getName().equalsIgnoreCase("STRING") || target.getType().getName().equalsIgnoreCase("WSTRING"))) { //$NON-NLS-1$
				return true;
			}
			if ((target.getType().getName().equalsIgnoreCase("ANY_STRING"))//$NON-NLS-1$
					&& (source.getType().getName().equalsIgnoreCase("STRING") || source.getType().getName().equalsIgnoreCase("WSTRING"))) { //$NON-NLS-1$
				return true;
			}
			if (source.getType().getName().equalsIgnoreCase("ANY_STRING") //$NON-NLS-1$
					&& target.getType().getName()
							.equalsIgnoreCase("ANY_STRING")) { //$NON-NLS-1$
				return checkAnyAnyCompatibility();
			}

			if ((source.getType().getName().equalsIgnoreCase("ANY_DATE"))//$NON-NLS-1$
					&& (target.getType().getName().equalsIgnoreCase("DATE")
							|| target.getType().getName()
									.equalsIgnoreCase("DATE_AND_TIME") || target
							.getType().getName()
							.equalsIgnoreCase("TIME_OF_DAY"))) { //$NON-NLS-1$
				return true;
			}
			if ((target.getType().getName().equalsIgnoreCase("ANY_DATE"))//$NON-NLS-1$
					&& (source.getType().getName().equalsIgnoreCase("DATE")
							|| source.getType().getName()
									.equalsIgnoreCase("DATE_AND_TIME") || source
							.getType().getName()
							.equalsIgnoreCase("TIME_OF_DAY"))) { //$NON-NLS-1$
				return true;
			}
			if (source.getType().getName().equalsIgnoreCase("ANY_DATE") //$NON-NLS-1$
					&& target.getType().getName().equalsIgnoreCase("ANY_DATE")) { //$NON-NLS-1$
				return checkAnyAnyCompatibility();
			}

			// TODO move to own adapter connection check section when
			// implementing own adapter connections
			if (((source.getType().getName().equals("ANY_ADAPTER")) && (!target.getType().getName().equals("ANY_ADAPTER"))) || //$NON-NLS-1$
					((!source.getType().getName().equals("ANY_ADAPTER")) && (target.getType().getName().equals("ANY_ADAPTER")))) { //$NON-NLS-1$
				return true;
			}

			if (source.getType().getName()
					.equalsIgnoreCase(target.getType().getName())) {
				return true;
			}

			return source.getType().getName()
					.equalsIgnoreCase(target.getType().getName());
		} catch (NullPointerException e) {
			System.out.println(e);
		}

		return false;
	}

	private static boolean checkAnyAnyCompatibility() {
		return false;
	}

	private static boolean anyBitCompatibility(String name) {
		return (name.equalsIgnoreCase("WORD") || name.equalsIgnoreCase("LWORD")
				|| name.equalsIgnoreCase("DWORD")
				|| name.equalsIgnoreCase("BYTE") || name
					.equalsIgnoreCase("BOOL")); //$NON-NLS-1$
	}

	private static boolean anyMagnitudeCompatibility(String name) {
		return (anyNumCompatibility(name) || name.equalsIgnoreCase("TIME")); //$NON-NLS-1$;
	}

	private static boolean anyNumCompatibility(String name) {
		return (anyIntCompatiblity(name) || anyRealCompatibility(name));
	}

	private static boolean anyRealCompatibility(String name) {
		return (name.equalsIgnoreCase("REAL") || name.equalsIgnoreCase("LREAL")); //$NON-NLS-1$;
	}

	private static boolean anyIntCompatiblity(String name) {
		return (name.equalsIgnoreCase("INT") || name.equalsIgnoreCase("UINT") //$NON-NLS-1$;
				|| name.equalsIgnoreCase("SINT") || name.equalsIgnoreCase("LINT") //$NON-NLS-1$;
				|| name.equalsIgnoreCase("DINT") || name.equalsIgnoreCase("USINT")//$NON-NLS-1$;
				|| name.equalsIgnoreCase("UDINT") || name.equalsIgnoreCase("ULINT"));//$NON-NLS-1$;
	}

}
