/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util.comm.channels;

/**
 * interface provides channel types for low level communication
 * @author Oliver Hummer (hummer@acin.tuwien.ac.at)
 * @author Ingo Hegny (hegny@acin.tuwien.ac.at)
 * 
 */
public interface IChannel {
	
	/**
	 * UDP channel<br>
	 * decision if it is a unicast, multicast or broadcast channel is made on the inetAddress
	 */
	public static final int UDP = 0;
	
	/**
	 * TCP channel<br>
     * TODO: Client or Server Role has to be defined! */
	public static final int TCP = 1;
	
}
