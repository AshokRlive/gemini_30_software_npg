/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util.comm.datatypes;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.fordiac.ide.util.Activator;

/**
 * @author Oliver Hummer (hummer@acin.tuwien.ac.at)
 * @author Ingo Hegny (hegny@acin.tuwien.ac.at)
 */
public class IEC_STRING extends IEC_WSTRING {

	/**
	 * 
	 */
	public IEC_STRING() {
		super();
		value = "";
	}

	public IEC_STRING(String initial) {
			this.value = new String(initial.getBytes());
	}

	/**
	 * @param in
	 */
	public IEC_STRING(DataInputStream in) {
		super(in);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.util.comm.datatypes.IEC_ANY#decodeValueFrom(java.io.DataInputStream)
	 */
	@Override
	public void decodeValueFrom(DataInputStream in) {
		String result = "";
		int size;
		try {
			size = in.readShort();
			for (int i = 0; i < size; i++) {
				result += (char) in.readUnsignedByte();
			}
		} catch (IOException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		value = result;
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.util.comm.datatypes.IEC_ANY#encodeTag()
	 */
	@Override
	public byte[] encodeTag() {
		byte[] retval = new byte[1];
		retval[0] = (0xff & (ASN1.APPLICATION + ASN1.STRING));
		return retval;
	}

	@Override
	public byte[] encodeValue() {
		ByteArrayOutputStream myOut=new ByteArrayOutputStream();
		DataOutputStream DOS=new DataOutputStream(myOut);
		
		try {
			DOS.writeShort(value.length());
			DOS.write(value.getBytes());
		} catch (IOException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		
		return myOut.toByteArray();
	}
	
	/**
	 * @param value
	 *            the value to set
	 */
	public boolean setValue(String value) {
		try {
			this.value = new String(value.getBytes("UTF-8"));
			return true;
		} catch (UnsupportedEncodingException e) {
			Activator.getDefault().logError(e.getMessage(), e);
			return false;			
		}
	}

	@Override
	public boolean setValue(IEC_ANY source) {
		boolean retval=false;
		if (source.getClass().equals(this.getClass())) {
			this.value = ((IEC_STRING)source).getValue();
			retval=true; 
		}
		return retval;
	}

	
	
}
