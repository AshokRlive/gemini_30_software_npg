/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import org.fordiac.ide.model.libraryElement.LibraryElement;

/**
 * The Class ChangeDescriptionCommand.
 */
public class ChangeDescriptionCommand extends ChangeIdentificationCommand {
	
	/** The new Description value. */
	private String newDescription;

	/** The old Description value. */
	private String oldDescription;

	public ChangeDescriptionCommand(LibraryElement type,
			final String newDescription) {
		super(type);
		this.newDescription = newDescription;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldDescription = identification.getDescription();
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		identification.setDescription(oldDescription);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		identification.setDescription(newDescription);
	}

}
