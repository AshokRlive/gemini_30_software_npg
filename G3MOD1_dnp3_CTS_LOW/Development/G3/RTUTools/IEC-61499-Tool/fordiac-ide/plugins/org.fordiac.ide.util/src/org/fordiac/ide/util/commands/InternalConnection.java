/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.InternalSubAppInterfaceElementView;
import org.fordiac.ide.model.ui.SubAppInterfaceElementView;
import org.fordiac.ide.model.ui.UISubAppNetwork;

/**
 * The Class InternalConnection.
 */
public class InternalConnection {

	/** The sub app interface element view. */
	private SubAppInterfaceElementView subAppInterfaceElementView;

	/** The connection view. */
	private ConnectionView connectionView;

	/** The source. */
	private InterfaceElementView source;

	/** The dest. */
	private InterfaceElementView dest;

	/** The internal interface element view. */
	private InternalSubAppInterfaceElementView internalInterfaceElementView;

	/** The uiSubAppNetwork network. */
	private UISubAppNetwork uiSubAppNetwork;

	/**
	 * Gets the sub app interface element view.
	 * 
	 * @return the sub app interface element view
	 */
	public SubAppInterfaceElementView getSubAppInterfaceElementView() {
		return subAppInterfaceElementView;
	}

	/**
	 * Sets the sub app interface element view.
	 * 
	 * @param subAppInterfaceElementView the new sub app interface element view
	 */
	public void setSubAppInterfaceElementView(
			final SubAppInterfaceElementView subAppInterfaceElementView) {
		this.subAppInterfaceElementView = subAppInterfaceElementView;
	}

	/**
	 * Gets the connection view.
	 * 
	 * @return the connection view
	 */
	public ConnectionView getConnectionView() {
		return connectionView;
	}

	/**
	 * Sets the connection view.
	 * 
	 * @param connectionView the new connection view
	 */
	public void setConnectionView(final ConnectionView connectionView) {
		this.connectionView = connectionView;
	}

	/**
	 * Gets the source.
	 * 
	 * @return the source
	 */
	public InterfaceElementView getSource() {
		return source;
	}

	/**
	 * Sets the source.
	 * 
	 * @param source the new source
	 */
	public void setSource(final InterfaceElementView source) {
		this.source = source;
	}

	/**
	 * Gets the dest.
	 * 
	 * @return the dest
	 */
	public InterfaceElementView getDest() {
		return dest;
	}

	/**
	 * Sets the dest.
	 * 
	 * @param dest the new dest
	 */
	public void setDest(final InterfaceElementView dest) {
		this.dest = dest;
	}

	/**
	 * Gets the internal interfaceElementView.
	 * 
	 * @return the internal interfaceElementView
	 */
	public InternalSubAppInterfaceElementView getInternalInterfaceElementView() {
		return internalInterfaceElementView;
	}

	/**
	 * Sets the internal interfaceElementView.
	 * 
	 * @param internalInterfaceElementView the new iinterfaceElementView
	 */
	public void setInternalInterfaceElementView(
			final InternalSubAppInterfaceElementView internalInterfaceElementView) {
		this.internalInterfaceElementView = internalInterfaceElementView;
	}

	/**
	 * Gets the UISubAppNetwork.
	 * 
	 * @return the UISubAppNetwork
	 */
	public UISubAppNetwork getUiSubAppNetwork() {
		return uiSubAppNetwork;
	}

	/**
	 * Sets the UISubAppNetwork.
	 * 
	 * @param uiSubAppNetwork the new UISubAppNetwork
	 */
	public void setUiSubAppNetwork(final UISubAppNetwork uiSubAppNetwork) {
		this.uiSubAppNetwork = uiSubAppNetwork;
	}

}