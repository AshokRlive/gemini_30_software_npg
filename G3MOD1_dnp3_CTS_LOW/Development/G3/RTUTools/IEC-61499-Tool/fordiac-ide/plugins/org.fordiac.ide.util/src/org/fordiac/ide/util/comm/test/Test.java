/*******************************************************************************
 * Copyright (c) 2007 - 2011 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

package org.fordiac.ide.util.comm.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.fordiac.ide.util.Activator;
import org.fordiac.ide.util.comm.TCPClient;
//import org.fordiac.ide.util.comm.UDPSubscriber;
import org.fordiac.ide.util.comm.datatypes.IEC_ANY;
import org.fordiac.ide.util.comm.datatypes.IEC_WSTRING;
import org.fordiac.ide.util.comm.exceptions.CommException;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		 List<IEC_ANY> mySndList = new ArrayList<IEC_ANY>();
		 List<IEC_ANY> myRcvList = new ArrayList<IEC_ANY>();
		
//		myList.add(new IEC_INT(5423));
//		myList.add(new IEC_WSTRING("Hall����"
//				+ String.copyValueOf(Character.toChars(0x6C34))));
//		myList.add(new Palette());
//		myList.add(new IEC_BYTE((byte) (0xFF)));
//		myList.add(new IEC_BYTE((byte) 127));
//		myList.add(new IEC_BYTE((byte) -128));
//		myList.add(new IEC_TIME("t#-22s"));
//		myList.add(new IEC_DATE_AND_TIME());

//		UDPSubscriber myTest = new UDPSubscriber();
		TCPClient myTCPTest = new TCPClient();
		
		//SERVER_1_1 is expected to listen on "localhost:7777"
		myTCPTest.Initialize("localhost:7777");
		myRcvList.add(new IEC_WSTRING("Ober-grml"));
		myTCPTest.setMyReceiveData(myRcvList);
		mySndList.add(new IEC_WSTRING("Hugo"));
		
		try {
			myTCPTest.SendIECData(mySndList);
		} catch (CommException e) {
			Activator.getDefault().logError(e.getMessage(), e);
		}
		
//		myTest.Test("225.0.0.37:61900", new IEC_WSTRING());
//		UDPSubscriber myTest2 = new UDPSubscriber();
		
//		myTest2.Test("225.0.0.37:61901", new IEC_WORD());
//		UDPTest myTest3 = new UDPTest();
//		myTest3.Test("225.0.0.37:61902", new IEC_DWORD());
//		UDPTest myTest4 = new UDPTest();
//		myTest4.Test("239.0.0.255:65535", new IEC_INT());
//		myTest4.setMyReceiveData(myList);
//
//		UDPTest myTest5 = new UDPTest();
//		myTest5.SendIECData("239.0.0.255:65535", myList);
//
//		UDPTest myTest6 = new UDPTest();
//		myTest6.Test("225.0.0.37:61904", new IEC_TIME("t#3s_100ms"));

//		Date date = new Date(2000, 1, 1);
		Calendar c = Calendar.getInstance();
		c.set(2000, 1, 1, 0, 0, 0);
		System.out.println(c.getTimeInMillis());
		c.clear();
		c.set(2007, 10, 16);
//		date.setTime(c.getTimeInMillis());
//		System.out.println(c.getTimeInMillis());
//		System.out.println(new IEC_DATE().toString());
//		System.out.println(new Date().getTime());
//		System.out.println(System.currentTimeMillis());
//		System.out.println(c.getTimeInMillis()-System.currentTimeMillis());

	}
}