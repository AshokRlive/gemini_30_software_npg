/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.fbtester.model;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.gef.EditPart;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.fordiac.fbtester.editparts.TestEditPart;
import org.fordiac.fbtester.editparts.TestEventEditPart;
import org.fordiac.ide.gef.IEditPartCreator;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.ui.View;
import org.fordiac.ide.model.ui.impl.ViewImpl;


/**
 * The Class TestElement.
 */
public class TestElement extends ViewImpl implements IEditPartCreator, View {

	/** The monitoring element. */
	private String monitoringElement;

	/** The element. */
	private IInterfaceElement element;

	/** The value. */
	private String value;
	
	
	private int nrOfHistory = 500;
	private ArrayList<String> historyValues = new ArrayList<String>(nrOfHistory);
	private ArrayList<Integer> historyCycles = new ArrayList<Integer>(nrOfHistory);
	private ArrayList<Long> historySec = new ArrayList<Long>(nrOfHistory);
	private ArrayList<Long> historyUSec = new ArrayList<Long>(nrOfHistory);
	private int currentInt = 0;
	

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	public String getCurrentValue() {
		if (currentInt == 0) {
			return "0";
		}
		return historyValues.get((currentInt % nrOfHistory ) -1 );
	}

	
	/**
	 * Sets the value.
	 * 
	 * @param value
	 *          the new value
	 */
	public void setValue(String value) {
		this.value = value;
		for (Iterator<ISetValueListener> iterator = valueListeners.iterator(); iterator.hasNext();) {
			ISetValueListener valueListener = (ISetValueListener) iterator.next();
			valueListener.setValue(this, value);
		}
	}

	/** The fb. */
	private FB fb;

	/**
	 * Gets the fb.
	 * 
	 * @return the fb
	 */
	public FB getFb() {
		return fb;
	}

	/**
	 * Sets the fb.
	 * 
	 * @param fb
	 *          the new fb
	 */
	public void setFb(FB fb) {
		this.fb = fb;
	}

	/** The monitoring element as array. */
	private String[] monitoringElementAsArray;

	/**
	 * Gets the monitoring element as string.
	 * 
	 * @return the monitoring element as string
	 */
	public String getMonitoringElementAsString() {
		return monitoringElement;
	}

	/**
	 * Sets the monitoring element.
	 * 
	 * @param monitoringElement
	 *          the new monitoring element
	 */
	public void setMonitoringElement(String monitoringElement) {
		this.monitoringElement = monitoringElement;
		monitoringElementAsArray = this.monitoringElement.split("\\.");
	}

	/**
	 * Gets the resource string.
	 * 
	 * @return the resource string
	 */
	public String getResourceString() {
		return getFBString() + "_RES";
	}

	/**
	 * Gets the fB string.
	 * 
	 * @return the fB string
	 */
	public String getFBString() {
		return "_" + getFb().getFBType().getName();
	}

	/**
	 * Gets the port string.
	 * 
	 * @return the port string
	 */
	public String getPortString() {
		return getInterfaceElement().getName();
	}

	/**
	 * Sets the element.
	 * 
	 * @param element
	 *          the new element
	 */
	public void setElement(IInterfaceElement element) {
		this.element = element;
	}

	/**
	 * Gets the interface element.
	 * 
	 * @return the interface element
	 */
	public IInterfaceElement getInterfaceElement() {
		return element;
	}

	/** The part. */
	private TestEditPart part;

	/**
	 * Gets the part.
	 * 
	 * @return the part
	 */
	public TestEditPart getPart() {
		return part;
	}

	public EditPart createEditPart() {
		if (part == null) {
			if (getInterfaceElement() instanceof Event
					&& getInterfaceElement().isIsInput()) {
				part = new TestEventEditPart();
			} else {
				part = new TestEditPart();
			}
		}
		return part;
	};

	/** The value listeners. */
	private final ArrayList<ISetValueListener> valueListeners = new ArrayList<ISetValueListener>();

	/**
	 * Adds the set value listener.
	 * 
	 * @param listener
	 *          the listener
	 */
	public void addSetValueListener(ISetValueListener listener) {
		if (!valueListeners.contains(listener)) {
			valueListeners.add(listener);
		}
	}

	/** The event listners. */
	private final ArrayList<ITriggerEventListener> eventListners = new ArrayList<ITriggerEventListener>();

	/**
	 * Adds the trigger event listener.
	 * 
	 * @param listener
	 *          the listener
	 */
	public void addTriggerEventListener(ITriggerEventListener listener) {
		if (!eventListners.contains(listener)) {
			eventListners.add(listener);
		}
	}

	/**
	 * Send event.
	 */
	public void sendEvent() {
		for (Iterator<ITriggerEventListener> iterator = eventListners.iterator(); iterator.hasNext();) {
			ITriggerEventListener eventListner = (ITriggerEventListener) iterator
					.next();
			eventListner.sendEvent(this);
		}

	}

	/**
	 * Update value.
	 * 
	 * @param value
	 *          the value
	 */
	public void updateValue(final String value, int cycle) {
		System.out.println("currentPos: " + currentInt % nrOfHistory);
		System.out.println("currentValue: " + value);
		historyValues.add(currentInt % nrOfHistory, value);
		historyCycles.add(currentInt % nrOfHistory, cycle);
//		historySec.add(currentInt % nrOfHistory, getSec());
//		historyUSec.add(currentInt % nrOfHistory, getUsec());
		currentInt++;
		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				getPart().setValue(value);

			}
		});
	}


	/**
	 * Sets the background color of the element
	 * @param color
	 */
	public void setColor(final Color color) {
		Display.getDefault().asyncExec(new Runnable() {
			
			@Override
			public void run() {
				part.setBackgroundColor(color);
			}
		});
	}

	@Override
	public EditPart createEditPart(String diagram) {
		return createEditPart();
	}
	
}
