package org.fordiac.fbtester;

import java.util.Hashtable;
import java.util.Iterator;

import org.fordiac.fbtester.model.ISetValueListener;
import org.fordiac.fbtester.model.ITriggerEventListener;
import org.fordiac.fbtester.model.TestElement;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.VarDeclaration;


public class TestingManager {

	private static TestingManager instance;

	public static TestingManager getInstance() {
		if (instance == null) {
			instance = new TestingManager();
		}
		return instance;
	}

	private TestingManager() {
	}

	private final Hashtable<String, TestElement> testElements = new Hashtable<String, TestElement>();
	private final Hashtable<String, TestElement> triggerElements = new Hashtable<String, TestElement>();

	public void addTriggerElement(TestElement element) {
		triggerElements.put(element.getFBString() + "_RES." + element.getFBString()
				+ "." + element.getInterfaceElement().getName(), element);
	}

	public Hashtable<String, TestElement> getTriggerElements(FBType type) {
		Hashtable<String, TestElement> elements = new Hashtable<String, TestElement>();
		for (Iterator<String> iterator = triggerElements.keySet().iterator(); iterator
				.hasNext();) {
			String key = (String) iterator.next();
			TestElement element = triggerElements.get(key);
			if (element.getFb().getFBType().equals(type)) {
				if (element.getInterfaceElement() instanceof Event) {
					elements.put(key, element);
				}
			}
		}
		return elements;
	}

	public void addTestElement(TestElement element) {
		testElements.put(element.getFBString() + "_RES." + element.getFBString()
				+ "." + element.getInterfaceElement().getName(), element);
	}

	public Hashtable<String, TestElement> getTestElements(FBType type,
			ISetValueListener valueListener, ITriggerEventListener eventListener) {
		Hashtable<String, TestElement> elements = new Hashtable<String, TestElement>();
		for (Iterator<String> iterator = testElements.keySet().iterator(); iterator
				.hasNext();) {
			String key = (String) iterator.next();
			TestElement element = testElements.get(key);
			if (element.getFb().getFBType().equals(type)) {
				element.addSetValueListener(valueListener);
				element.addTriggerEventListener(eventListener);
				if (element.getInterfaceElement() instanceof VarDeclaration) {
					elements.put(key, element);
				} else if (element.getInterfaceElement() instanceof Event
						&& !element.getInterfaceElement().isIsInput()) {
					elements.put(key, element);
				}

			}
		}
		return elements;
	}

	public Hashtable<String, TestElement> getTestElements(FBType type) {
		Hashtable<String, TestElement> elements = new Hashtable<String, TestElement>();
		for (Iterator<String> iterator = testElements.keySet().iterator(); iterator
				.hasNext();) {
			String key = (String) iterator.next();
			TestElement element = testElements.get(key);
			if (element.getFb().getFBType().equals(type)) {
				if (element.getInterfaceElement() instanceof VarDeclaration) {
					elements.put(key, element);
				} else if (element.getInterfaceElement() instanceof Event
						&& !element.getInterfaceElement().isIsInput()) {
					elements.put(key, element);
				}

			}
		}
		return elements;
	}


}
