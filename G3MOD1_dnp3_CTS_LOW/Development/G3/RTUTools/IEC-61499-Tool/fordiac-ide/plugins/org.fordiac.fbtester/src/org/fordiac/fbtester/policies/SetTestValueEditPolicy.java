/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.fbtester.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.fordiac.fbtester.editparts.TestEditPart;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;


/**
 * The Class SetTestValueEditPolicy.
 */
public class SetTestValueEditPolicy extends DirectEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.DirectEditPolicy#getDirectEditCommand(org.
	 * eclipse.gef.requests.DirectEditRequest)
	 */
	@Override
	protected Command getDirectEditCommand(DirectEditRequest request) {
		if (getHost() instanceof TestEditPart) {
			TestEditPart testEditPart = (TestEditPart) getHost();
			testEditPart.getCastedModel().setValue((String) request.getCellEditor().getValue());
			testEditPart.refresh();
			
			// return a dummy command needed to fulfill requirements of direct edit of interface value for testing.
			// canExecute is set to false so that it is not put onto the command stack and makes the editor dirty.
			return new Command(){
				public boolean canExecute() {
					return false;
				}
			};
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.DirectEditPolicy#showCurrentEditValue(org.
	 * eclipse.gef.requests.DirectEditRequest)
	 */
	@Override
	protected void showCurrentEditValue(DirectEditRequest request) {
		String value = (String) request.getCellEditor().getValue();
		if (getHost() instanceof AbstractViewEditPart) {
			AbstractViewEditPart viewEditPart = (AbstractViewEditPart) getHost();
			viewEditPart.getNameLabel().setText(value);
		}
	}
}
