/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.fbtester;

import org.eclipse.swt.widgets.Composite;
import org.fordiac.fbtester.model.ISetValueListener;
import org.fordiac.fbtester.model.ITriggerEventListener;
import org.fordiac.ide.model.Palette.PaletteGroup;
import org.fordiac.ide.model.libraryElement.FBType;


public interface IFBTestConfiguratonCreator extends ISetValueListener,
		ITriggerEventListener {

	public IFBTestConfiguration createConfigurationPage(Composite parent);

	public void setType(FBType type);
	
	public void setGroup(PaletteGroup group);

}
