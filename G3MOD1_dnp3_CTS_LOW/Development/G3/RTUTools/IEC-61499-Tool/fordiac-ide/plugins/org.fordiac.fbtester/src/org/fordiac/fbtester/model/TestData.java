/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.fbtester.model;

import java.util.Hashtable;

import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.VarDeclaration;

public class TestData {

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	private final FBType type;

	private String testName;
	private String testInstance;

	private final Hashtable<VarDeclaration, String> valueList = new Hashtable<VarDeclaration, String>();
	private Event event;

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public TestData(FBType type, String dataLine) {
		this.type = type;
		parseData(dataLine);
	}

	private void parseData(String line) {
		System.out.println(line);
		String[] lineArray = line.split(";(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
		System.out.println(lineArray.length);
		if (lineArray.length > 1) {
			testName = lineArray[0];
			testInstance = lineArray[1];
		}

		if (type.getInterfaceList() != null) {
			int nrOfInputs = type.getInterfaceList().getInputVars().size();
			if (lineArray.length > nrOfInputs * 2 + 2) {
				for (int i = 2; i < lineArray.length - 1; i += 2) { // start at pos 2 //
					// after
					// testName and
					// testInstance
					String port = lineArray[i];
					String val = lineArray[i + 1];
					VarDeclaration varDec = type.getInterfaceList().getVariable(port);
					if (varDec != null) {
						valueList.put(varDec, val);
					}
				}
			}

			event = type.getInterfaceList().getEvent(lineArray[lineArray.length - 1]);

		}
	}

	@Override
	public String toString() {
		return testName + "(" + testInstance + ")";
	}

	public String getValueFor(String text) {
		VarDeclaration varDecl = type.getInterfaceList().getVariable(text);
		if (varDecl != null) {
			return valueList.get(varDecl);
		}
		return null;
	}
}
