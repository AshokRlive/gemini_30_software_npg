
package org.fordiac.fbtester.editparts;

import org.eclipse.draw2d.ActionEvent;
import org.eclipse.draw2d.ActionListener;
import org.eclipse.draw2d.Button;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Rectangle;
import org.fordiac.fbtester.TestingManager;
import org.fordiac.ide.application.SpecificLayerEditPart;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.UiFactory;


/**
 * The Class TestEventEditPart.
 */
public class TestEventEditPart extends TestEditPart implements
		SpecificLayerEditPart {

	@Override
	public void activate() {
		super.activate();
		registerTriggerElement();
	}

	/**
	 * Register element.
	 */

	protected void registerTriggerElement() {
		// if (isVariable()) {
		TestingManager.getInstance().addTriggerElement(getCastedModel());
		// }
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.fbtester.editparts.TestEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.fbtester.editparts.TestEditPart#updatePos()
	 */
	@Override
	protected void updatePos() {

		Rectangle bounds = parentPart.getFigure().getBounds();
		Position pos = UiFactory.eINSTANCE.createPosition();
		int x = 0;
		if (isInput()) {

			int width = getFigure().getBounds().width;
			x = bounds.x - 10 - width - 15
					* getCastedModel().getFb().getInterface().getEventInputs().size();
		} else {
			x = bounds.x + bounds.width + 10 + 15
					* getCastedModel().getFb().getInterface().getEventInputs().size();

		}
		int y = bounds.y;
		if (x != oldx || y != oldy) {
			if (getCastedModel().getPosition() != null) {
				pos = getCastedModel().getPosition();
			}
			pos.setX(x);
			pos.setY(y);
			getCastedModel().setPosition(pos);
			oldx = x;
			oldy = y;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.fbtester.editparts.TestEditPart#createFigureForModel()
	 */
	@Override
	protected IFigure createFigureForModel() {
		Button bt = new Button(getCastedModel().getInterfaceElement().getName());
		bt.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				getCastedModel().sendEvent();
				// TestingManager.getInstance().sendEvent(getCastedModel());
			}
		});
		return bt;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.fbtester.editparts.TestEditPart#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String string) {
		// Nothing To do

	}

}
