
package org.fordiac.fbtester.editparts;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.draw2d.AncestorListener;
import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.fordiac.fbtester.TestingManager;
import org.fordiac.fbtester.model.TestElement;
import org.fordiac.fbtester.policies.SetTestValueEditPolicy;
import org.fordiac.ide.application.SpecificLayerEditPart;
import org.fordiac.ide.gef.editparts.AbstractViewEditPart;
import org.fordiac.ide.gef.editparts.ZoomScalableFreeformRootEditPart;
import org.fordiac.ide.model.libraryElement.INamedElement;
import org.fordiac.ide.model.libraryElement.impl.EventImpl;
import org.fordiac.ide.model.libraryElement.impl.VarDeclarationImpl;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.Position;
import org.fordiac.ide.model.ui.UiFactory;


/**
 * The Class TestEditPart.
 */
public class TestEditPart extends AbstractViewEditPart implements
		SpecificLayerEditPart {

	/** The parent part. */
	org.fordiac.fbtester.editparts.InterfaceEditPart parentPart;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#activate()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void activate() {
		super.activate();
		Set set = getViewer().getEditPartRegistry().keySet();
		for (Iterator iterator = set.iterator(); iterator.hasNext();) {
			Object object = iterator.next();
			if (object instanceof InterfaceElementView) {
				if (((InterfaceElementView) object).getIInterfaceElement().equals(
						getCastedModel().getInterfaceElement())) {

					EditPart part = (EditPart) getViewer().getEditPartRegistry().get(
							object);
					if (part instanceof org.fordiac.fbtester.editparts.InterfaceEditPart) {
						parentPart = (org.fordiac.fbtester.editparts.InterfaceEditPart) part;
						IFigure f = parentPart.getFigure();
						f.addAncestorListener(new AncestorListener() {

							@Override
							public void ancestorRemoved(IFigure ancestor) {

							}

							@Override
							public void ancestorMoved(IFigure ancestor) {
								updatePos();

							}

							@Override
							public void ancestorAdded(IFigure ancestor) {

							}
						});
					}

				}
			}
		}
		updatePos();
		registerElement();
	}

	/**
	 * Set the background color of this editparts figure
	 * @param color
	 */
	public void setBackgroundColor(Color color) {
		getFigure().setBackgroundColor(color);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#refreshName()
	 */
	@Override
	protected void refreshName() {
		getNameLabel().setText(getCastedModel().getValue());
		getNameLabel().setToolTip(new Label(getCastedModel().getValue()));
	}

	/**
	 * Register element.
	 */
	protected void registerElement() {
		// if (isVariable()) {
		TestingManager.getInstance().addTestElement(getCastedModel());
		// }
	}

	/**
	 * Checks if is input.
	 * 
	 * @return true, if is input
	 */
	public boolean isInput() {
		return getCastedModel().getInterfaceElement().isIsInput();
	}

	/**
	 * Checks if is event.
	 * 
	 * @return true, if is event
	 */
	public boolean isEvent() {
		return getCastedModel().getInterfaceElement() instanceof EventImpl;
	}

	/**
	 * Checks if is variable.
	 * 
	 * @return true, if is variable
	 */
	public boolean isVariable() {
		return getCastedModel().getInterfaceElement() instanceof VarDeclarationImpl;
	}

	/** The oldx. */
	int oldx;

	/** The oldy. */
	int oldy;

	/**
	 * Update pos.
	 */
	protected void updatePos() {
		String label = ((Label) getFigure()).getText();
		Rectangle bounds = parentPart.getFigure().getBounds();
		Position pos = UiFactory.eINSTANCE.createPosition();
		int x = 0;
		if (isInput()) {
			Font font = ((Label) getFigure()).getFont();
			int width = 50;
			if (font != null) {
				width = FigureUtilities.getTextWidth(label, getFigure().getFont());
				width = Math.max(width, getFigure().getBounds().width);
			}
			x = bounds.x - 10 - width - 15
					* getCastedModel().getFb().getInterface().getEventInputs().size();
		} else {
			x = bounds.x + bounds.width + 10 + 15
					* getCastedModel().getFb().getInterface().getEventInputs().size();

		}
		int y = bounds.y;
		if (x != oldx || y != oldy) {
			if (getCastedModel().getPosition() != null) {
				pos = getCastedModel().getPosition();
			}
			pos.setX(x);
			pos.setY(y);
			getCastedModel().setPosition(pos);
			oldx = x;
			oldy = y;
		}
	}

	/**
	 * Gets the casted model.
	 * 
	 * @return the casted model
	 */
	public TestElement getCastedModel() {
		return (TestElement) getModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// EditPolicy which allows the direct edit of the Instance Name
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new SetTestValueEditPolicy());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#createFigureForModel()
	 */
	@Override
	protected IFigure createFigureForModel() {
		Label l = new Label();
		l.setSize(100, 100);
		l.setOpaque(true);
		l.setBackgroundColor(org.eclipse.draw2d.ColorConstants.yellow);
		l.setPreferredSize(150, 20);
		l.setBorder(new MarginBorder(3, 5, 3, 5));
		// l.setText(getCastedModel().getMonitoringElementAsString());
		if (isInput()) {
			LineBorder lb = new LineBorder() {
				@Override
				public Insets getInsets(IFigure figure) {
					return new Insets(3, 5, 3, 5);
				}
			};
			l.setBorder(lb);
		}
		l.setText("--");
		return l;
	}

	/** The adapter. */
	private EContentAdapter adapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getContentAdapter()
	 */
	@Override
	public EContentAdapter getContentAdapter() {
		if (adapter == null) {
			adapter = new EContentAdapter() {

				@Override
				public void notifyChanged(final Notification notification) {
					super.notifyChanged(notification);

					refreshVisuals();
				}

			};
		}
		return adapter;
	}

	/**
	 * Refresh position.
	 */
	@Override
	protected void refreshPosition() {
		Rectangle bounds = null;
		if (getView().getPosition() != null) {
			// if (getView() != null && getView().getPosition() != null) {
			bounds = new Rectangle(getView().getPosition().getX(), getView()
					.getPosition().getY(), 80, -1);
			// } else {
			// bounds = new Rectangle()
			// }
			((GraphicalEditPart) getParent()).setLayoutConstraint(this, getFigure(),
					bounds);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractEditPart#understandsRequest(org.eclipse
	 * .gef.Request)
	 */
	@Override
	public boolean understandsRequest(Request request) {
		if (request.getType() == RequestConstants.REQ_MOVE) {
			return false;
		}
		if (request.getType() == RequestConstants.REQ_DIRECT_EDIT
				|| request.getType() == RequestConstants.REQ_OPEN) {
			return isInput();
		}
		return super.understandsRequest(request);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#performRequest(org.eclipse
	 * .gef.Request)
	 */
	@Override
	public void performRequest(Request request) {
		if (request.getType() == RequestConstants.REQ_DIRECT_EDIT
				|| request.getType() == RequestConstants.REQ_OPEN) {
			if (!isInput()) {
				return;
			}
		}
		super.performRequest(request);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getINamedElement()
	 */
	@Override
	public INamedElement getINamedElement() {
		return getCastedModel().getInterfaceElement();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getInterfaceElements()
	 */
	@Override
	public List<? extends InterfaceElementView> getInterfaceElements() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.AbstractViewEditPart#getNameLabel()
	 */
	@Override
	public Label getNameLabel() {
		return (Label) getFigure();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.ide.gef.editparts.AbstractViewEditPart#getPreferenceChangeListener
	 * ()
	 */
	@Override
	public IPropertyChangeListener getPreferenceChangeListener() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySource#getEditableValue()
	 */
	@Override
	public Object getEditableValue() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#getPropertyValue(java.lang
	 * .Object)
	 */
	@Override
	public Object getPropertyValue(Object id) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#isPropertySet(java.lang
	 * .Object)
	 */
	@Override
	public boolean isPropertySet(Object id) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#resetPropertyValue(java
	 * .lang.Object)
	 */
	@Override
	public void resetPropertyValue(Object id) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#setPropertyValue(java.lang
	 * .Object, java.lang.Object)
	 */
	@Override
	public void setPropertyValue(Object id, Object value) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.application.SpecificLayerEditPart#getSpecificLayer()
	 */
	public String getSpecificLayer() {
		return ZoomScalableFreeformRootEditPart.TOPLAYER;
	}

	/**
	 * Sets the value.
	 * 
	 * @param string
	 *          the new value
	 */
	public void setValue(String string) {
		if (isActive()) {
			if (getFigure() != null) {
				((Label) getFigure()).setText(string);
			}
		}

	}

}
