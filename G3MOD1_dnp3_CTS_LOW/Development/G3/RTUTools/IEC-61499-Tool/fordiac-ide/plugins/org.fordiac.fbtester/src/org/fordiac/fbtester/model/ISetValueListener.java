
package org.fordiac.fbtester.model;


/**
 * The listener interface for receiving ISetValue events. The class that is
 * interested in processing a ISetValue event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addSetValueListener<code> method. When
 * the ISetValue event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see ISetValueEvent
 */
public interface ISetValueListener {

	/**
	 * Sets the value.
	 * 
	 * @param element
	 *          the element
	 * @param value
	 *          the value
	 */
	public void setValue(TestElement element, String value);

}
