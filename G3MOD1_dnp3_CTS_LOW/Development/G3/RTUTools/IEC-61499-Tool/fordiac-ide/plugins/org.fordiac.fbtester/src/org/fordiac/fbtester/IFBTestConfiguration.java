/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.fbtester;

import java.util.ArrayList;
import java.util.Hashtable;

import org.eclipse.swt.widgets.Control;
import org.fordiac.fbtester.model.TestElement;
import org.fordiac.fbtester.model.testdata.ValuedVarDecl;


public interface IFBTestConfiguration {

	public Control getControl();

	public void newTestConf(ArrayList<TestElement> variables,
			ArrayList<String> values, ArrayList<ValuedVarDecl> resultVars,
			Hashtable<String, Object> params);

}
