/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.fbtester;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.widgets.TableColumn;
import org.fordiac.fbtester.model.testdata.TestData;
import org.fordiac.ide.model.libraryElement.VarDeclaration;


public class TestDataEditingSupport extends EditingSupport {

	private final TableColumn col;

	public TestDataEditingSupport(ColumnViewer viewer, TableColumn column) {
		super(viewer);
		this.col = column;
	}

	@Override
	protected boolean canEdit(Object element) {
		if (col.getData(FBTester.COLUMN_TYPE).equals(FBTester.NAME)) {
			return true;
		} else if (col.getData(FBTester.COLUMN_TYPE)
				.equals(FBTester.INPUT_VARIABLE)) {
			return true;
		} else if (col.getData(FBTester.COLUMN_TYPE).equals(
				FBTester.OUTPUT_VARIABLE)) {
			return true;
		}
		return false;
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		if (col.getData(FBTester.COLUMN_TYPE).equals(FBTester.NAME)
				|| col.getData(FBTester.COLUMN_TYPE).equals(FBTester.INPUT_VARIABLE)
				|| col.getData(FBTester.COLUMN_TYPE).equals(FBTester.OUTPUT_VARIABLE)) {
			return new TextCellEditor(((TableViewer) getViewer()).getTable());
		}
		return null;
	}

	@Override
	protected Object getValue(Object element) {
		if (col.getData(FBTester.COLUMN_TYPE).equals(FBTester.NAME)) {
			return ((TestData) element).getTestName();
		} else if (col.getData(FBTester.COLUMN_TYPE)
				.equals(FBTester.INPUT_VARIABLE)) {
			Object obj = col.getData(FBTester.INPUT_VARIABLE);
			if (obj instanceof VarDeclaration) {
				return ((TestData) element).getValueFor(((VarDeclaration) obj)
						.getName());
			}
		} else if (col.getData(FBTester.COLUMN_TYPE).equals(
				FBTester.OUTPUT_VARIABLE)) {
			Object obj = col.getData(FBTester.OUTPUT_VARIABLE);
			if (obj instanceof VarDeclaration) {
				return ((TestData) element).getResultFor(((VarDeclaration) obj)
						.getName());
			}
		}
		return null;
	}

	@Override
	protected void setValue(Object element, Object value) {
		if (col.getData(FBTester.COLUMN_TYPE).equals(FBTester.INPUT_VARIABLE)) {
			Object obj = col.getData(FBTester.INPUT_VARIABLE);
			if (obj instanceof VarDeclaration) {
				((TestData) element).setValueFor(((VarDeclaration) obj).getName(),
						value.toString());
				getViewer().refresh();
				return;
			}
		}
		if (col.getData(FBTester.COLUMN_TYPE).equals(FBTester.OUTPUT_VARIABLE)) {
			Object obj = col.getData(FBTester.OUTPUT_VARIABLE);
			if (obj instanceof VarDeclaration) {
				((TestData) element).setResultFor(((VarDeclaration) obj).getName(),
						value.toString());
				getViewer().refresh();
				return;
			}
		}
		if (value instanceof String) {
			if (!("".equals(value))) {
				((TestData) element).setTestName(value.toString());
			}
		}

	}

}
