/*******************************************************************************
 * Copyright (c) 2007 - 2008 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     February - April 2008: Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at 
 *     				- initial implementation
 *******************************************************************************/
package org.fordiac.fbtester;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.fordiac.fbtester.messages"; //$NON-NLS-1$
	public static String FBTester_CreateDataConnectionFailed;
	public static String FBTester_CreateEventConnectionFailed;
	public static String FBTester_CreateFBInstanceFailed;
	public static String FBTester_CreateResourceFailed;
	public static String FBTester_CreateResourceInstance;
	public static String FBTester_CreateFBInstance;
	public static String FBTester_CreateEventConnection;
	public static String FBTester_CreateDataConnection;
	public static String FBTester_StartingResourceFailed;
	public static String FBTester_WriteFBParameterFailed;
	public static String FBTester_WriteParameter;
	public static String FBTester_Start;
	public static String FBTester_Kill;
	public static String FBTester_KillFB;
	public static String FBTester_DeleteFB;
	public static String FBTester_StartFB;
	public static String FBTester_WriteResourceParameterFailed;
	public static String FBTester_DisconnectFailed;

	// nxtControl specific
	public static String FBTester_SendAttributeName;
	public static String FBTester_SendAttributeValue;
	public static String FBTester_SendAttributeState;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
		// empty private constructor
	}
}
