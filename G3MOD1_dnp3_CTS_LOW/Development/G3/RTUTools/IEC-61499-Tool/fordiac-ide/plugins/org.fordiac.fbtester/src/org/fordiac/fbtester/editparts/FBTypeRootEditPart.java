
package org.fordiac.fbtester.editparts;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ShortestPathConnectionRouter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;
import org.eclipse.swt.widgets.Display;
import org.fordiac.fbtester.model.TestElement;
import org.fordiac.ide.application.utilities.ApplicationUIFBNetworkManager;
import org.fordiac.ide.fbt.typeeditor.editparts.CommentTypeField;
import org.fordiac.ide.fbt.typemanagement.FBTypeEditorInput;
import org.fordiac.ide.gef.editparts.AbstractDiagramEditPart;
import org.fordiac.ide.gef.policies.EmptyXYLayoutEditPolicy;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.libraryElement.FBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.InterfaceList;
import org.fordiac.ide.model.libraryElement.LibraryElementFactory;
import org.fordiac.ide.model.libraryElement.Position;
import org.fordiac.ide.model.libraryElement.Value;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.model.ui.InterfaceElementView;


/**
 * Edit Part for the visualization of FBs.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class FBTypeRootEditPart extends AbstractDiagramEditPart {

	/** The adapter. */
	private EContentAdapter adapter;

	@Override
	protected IFigure createFigure() {
		IFigure figure = super.createFigure();
		// Create the static router for the connection layer
		ConnectionLayer connLayer = (ConnectionLayer) getLayer(LayerConstants.CONNECTION_LAYER);
		connLayer.setConnectionRouter(new ShortestPathConnectionRouter(figure));
		return figure;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (!isActive()) {
			super.activate();
			((Notifier) getCastedFBTypeModel()).eAdapters().add(getContentAdapter());

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((Notifier) getCastedFBTypeModel()).eAdapters().remove(getContentAdapter());
		}
	}

	/**
	 * Gets the content adapter.
	 * 
	 * @return the content adapter
	 */
	public EContentAdapter getContentAdapter() {
		if (adapter == null) {
			adapter = new EContentAdapter() {
				@Override
				public void notifyChanged(final Notification notification) {
					int type = notification.getEventType();
					switch (type) {
					case Notification.ADD:
					case Notification.ADD_MANY:
					case Notification.REMOVE:
					case Notification.REMOVE_MANY:
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								refreshChildren();
							}
						});
						break;
					case Notification.SET:
						break;
					}
				}
			};
		}
		return adapter;
	}

	/**
	 * Creates the EditPolicies used for this EditPart.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new RootComponentEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new EmptyXYLayoutEditPolicy());

	}

	/**
	 * returns the model object as <code>FBNetwork</code>.
	 * 
	 * @return FBNetwork to be visualized
	 */
	public FBType getCastedFBTypeModel() {
		return getEditorInput().getContent();
	}
	
	private FBTypeEditorInput getEditorInput(){
		return (FBTypeEditorInput)getModel();
	}
	
	private PaletteEntry getPaletteEntry(){
		return getEditorInput().getPaletteEntry();
	}

	private final Hashtable<IInterfaceElement, CommentTypeField> commentMapping = new Hashtable<IInterfaceElement, CommentTypeField>();

	/**
	 * Returns the children of the FBType.
	 * 
	 * @return the list of children s
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@Override
	protected List<?> getModelChildren() {
		ArrayList<Object> children = new ArrayList<Object>();
		FB fB = LibraryElementFactory.eINSTANCE.createFB();
		fB.setPaletteEntry(getPaletteEntry());
		fB.setInterface((InterfaceList) EcoreUtil.copy(getCastedFBTypeModel()
				.getInterfaceList()));
		fB.setName(getCastedFBTypeModel().getName());
		Position pos = LibraryElementFactory.eINSTANCE.createPosition();
		pos.setX(10);
		pos.setY(10);
		fB.setPosition(pos);

		createValues(fB);
		FBView view = ApplicationUIFBNetworkManager.createFBView(fB, null);
		children.add(view);

		for (Iterator<InterfaceElementView> iterator = view.getInterfaceElements().iterator(); iterator
				.hasNext();) {
			InterfaceElementView ieView = (InterfaceElementView) iterator.next();
			children.add(createTestElement(fB, ieView));

		}

		InterfaceList il = getCastedFBTypeModel().getInterfaceList();
		if (il != null) {
			for (Iterator<VarDeclaration> iterator = il.getInputVars().iterator(); iterator.hasNext();) {
				VarDeclaration varDecl = (VarDeclaration) iterator.next();
				CommentTypeField field = new CommentTypeField(varDecl);
				children.add(field);
				commentMapping.put(varDecl, field);
			}
			for (Iterator<Event> iterator = il.getEventInputs().iterator(); iterator
					.hasNext();) {
				Event varDecl = (Event) iterator.next();
				CommentTypeField field = new CommentTypeField(varDecl);
				children.add(field);
				commentMapping.put(varDecl, field);
			}
			for (Iterator<VarDeclaration> iterator = il.getOutputVars().iterator(); iterator
					.hasNext();) {
				VarDeclaration varDecl = (VarDeclaration) iterator.next();
				CommentTypeField field = new CommentTypeField(varDecl);
				children.add(field);
				commentMapping.put(varDecl, field);
			}
			for (Iterator<Event> iterator = il.getEventOutputs().iterator(); iterator
					.hasNext();) {
				Event varDecl = (Event) iterator.next();
				CommentTypeField field = new CommentTypeField(varDecl);
				children.add(field);
				commentMapping.put(varDecl, field);
			}
		} else {
		}

		return children;
	}

	private TestElement createTestElement(FB fb, InterfaceElementView ieView) {

		TestElement element = new TestElement();
		element.setFb(fb);

		IInterfaceElement interfaceElement = ieView.getIInterfaceElement();
		if (interfaceElement == null) {
			// TODO ExceptionHandling
		} else {
			element.setElement(interfaceElement);
		}
		return element;
	}

	/**
	 * Creates the values.
	 */
	protected void createValues(FB fB) {
		ArrayList<IInterfaceElement> iInterfaceElements = new ArrayList<IInterfaceElement>();

		// iInterfaceElements.addAll(fB.getInterface().getEventInputs());
		iInterfaceElements.addAll(fB.getInterface().getInputVars());

		for (Iterator<IInterfaceElement> iterator = iInterfaceElements.iterator(); iterator
				.hasNext();) {
			IInterfaceElement element = iterator.next();
			Value value = LibraryElementFactory.eINSTANCE.createValue();
			element.setValue(value);

		}
	}
}
