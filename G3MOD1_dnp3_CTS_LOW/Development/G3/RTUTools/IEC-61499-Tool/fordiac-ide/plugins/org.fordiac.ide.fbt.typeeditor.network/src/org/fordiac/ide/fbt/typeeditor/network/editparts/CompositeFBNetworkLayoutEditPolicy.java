/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network.editparts;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.CreateRequest;
import org.fordiac.ide.application.commands.FBCreateCommand;
import org.fordiac.ide.application.policies.FBNetworkXYLayoutEditPolicy;
import org.fordiac.ide.model.ui.UIFBNetwork;

/**
 * The Class CompositeFBNetworkLayoutEditPolicy.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public class CompositeFBNetworkLayoutEditPolicy extends
		FBNetworkXYLayoutEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see LayoutEditPolicy#getCreateCommand(CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(final CreateRequest request) {
		if (request == null) {
			return null;
		}
		Object childClass = request.getNewObjectType();
		Rectangle constraint = (Rectangle) getConstraintFor(request);
		if (childClass instanceof org.fordiac.ide.model.Palette.FBTypePaletteEntry) {
			org.fordiac.ide.model.Palette.FBTypePaletteEntry type = (org.fordiac.ide.model.Palette.FBTypePaletteEntry) request
					.getNewObjectType();

			if (getHost().getModel() instanceof UIFBNetwork) {
				// UIFBNetwork uiFBNetwork = FBTImporter
				// .getUINetwork((FBNetwork) getHost().getModel());
				UIFBNetwork uiFBNetwork = (UIFBNetwork) getHost().getModel();
				FBCreateCommand cmd = new FBCreateCommand(type, uiFBNetwork, new Rectangle(
						constraint.getLocation().x, constraint.getLocation().y,
						-1, -1));
				if (getHost() instanceof CompositeNetworkEditPart) {
					cmd.setParentComposite(((CompositeNetworkEditPart)getHost()).getFbType());
				}
				return cmd;
			}
		}
		return null;
	}
}
