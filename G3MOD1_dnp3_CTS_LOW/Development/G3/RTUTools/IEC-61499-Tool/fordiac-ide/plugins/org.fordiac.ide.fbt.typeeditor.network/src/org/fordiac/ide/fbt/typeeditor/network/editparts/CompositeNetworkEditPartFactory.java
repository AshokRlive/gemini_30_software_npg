/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network.editparts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.ui.parts.GraphicalEditor;
import org.fordiac.ide.application.editparts.ElementEditPartFactory;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.ui.CompositeInternalInterfaceElementView;
import org.fordiac.ide.model.ui.UIFBNetwork;

/**
 * A factory for creating ECCEditPart objects.
 */
public class CompositeNetworkEditPartFactory extends ElementEditPartFactory {

	protected CompositeFBType compositeType;
	
	public CompositeNetworkEditPartFactory(GraphicalEditor editor, CompositeFBType compositeType, ZoomManager zoomManager) {
		super(editor, zoomManager);
		this.compositeType = compositeType;
	}

	@Override
	protected EditPart getPartForElement(final EditPart context,
			final Object modelElement) {
		if (modelElement instanceof UIFBNetwork) {
			CompositeNetworkEditPart compositeNetEP = new CompositeNetworkEditPart();
			compositeNetEP.setFbType(compositeType);
			return compositeNetEP;

		}
		if (modelElement instanceof CompositeInternalInterfaceElementView) {
			return new CompositeInternalInterfaceEditPart();
		}
		return super.getPartForElement(context, modelElement);
	}

}
