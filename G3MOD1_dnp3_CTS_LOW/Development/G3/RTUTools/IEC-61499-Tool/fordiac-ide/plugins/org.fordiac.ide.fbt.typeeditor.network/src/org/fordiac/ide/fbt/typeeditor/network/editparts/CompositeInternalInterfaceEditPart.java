/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network.editparts;

import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.fordiac.ide.fbt.typeeditor.network.viewer.CompositeInternalInterfaceEditPartRO;
import org.fordiac.ide.gef.commands.ChangeNameCommand;
import org.fordiac.ide.gef.editparts.ConnCreateDirectEditDragTrackerProxy;
import org.fordiac.ide.gef.policies.INamedElementRenameEditPolicy;

/**
 * The Class CompositeInternalInterfaceEditPart.
 */
public class CompositeInternalInterfaceEditPart extends
		CompositeInternalInterfaceEditPartRO {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getNodeEditPolicy()
	 */
	@Override
	protected GraphicalNodeEditPolicy getNodeEditPolicy() {
		if (isEvent()) {
			return new org.fordiac.ide.fbt.typeeditor.network.policies.EventNodeEditPolicy();
		}
		if (isVariable()) {
			return new org.fordiac.ide.fbt.typeeditor.network.policies.VariableNodeEditPolicy();
		}
		return null;
	}

	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();

		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
				new INamedElementRenameEditPolicy() {
					@Override
					protected Command getDirectEditCommand(
							final DirectEditRequest request) {
						if (getHost() instanceof CompositeInternalInterfaceEditPart) {
							ChangeNameCommand cmd = new ChangeNameCommand(
									getCastedModel().getIInterfaceElement(),
									(String) request.getCellEditor().getValue());
							return cmd;
						}
						return null;
					}

				});

		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractEditPart#performRequest(org.eclipse.gef
	 * .Request)
	 */
	@Override
	public void performRequest(final Request request) {
		// REQ_DIRECT_EDIT -> first select 0.4 sec pause -> click -> edit
		// REQ_OPEN -> doubleclick

		if ((request.getType() == RequestConstants.REQ_OPEN)
				|| (request.getType() == RequestConstants.REQ_DIRECT_EDIT)) {
			performDirectEdit();
		} else {
			super.performRequest(request);
		}
	}
	
	@Override
	public DragTracker getDragTracker(Request request){
		return new ConnCreateDirectEditDragTrackerProxy(this);
	}

}
