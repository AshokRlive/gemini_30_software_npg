package org.fordiac.ide.fbt.typeeditor.network.properties;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.model.libraryElement.LibraryElement;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.util.properties.TypeInfoSection;

public class FBTypeInfoSection extends TypeInfoSection {

	@Override
	protected CommandStack getCommandStack(IWorkbenchPart part, Object input) {
		return null;
	}

	@Override
	protected LibraryElement getInputType(Object input) {
		if(input instanceof FBEditPart){
			return ((FBView)((FBEditPart) input).getModel()).getFb().getFBType();	
		}
		return null;
	}

}
