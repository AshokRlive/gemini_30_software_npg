package org.fordiac.ide.fbt.typeeditor.network.properties;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.contentoutline.ContentOutline;
import org.fordiac.ide.application.properties.InterfaceSection;
import org.fordiac.ide.fbt.typeeditor.editors.FBTypeContentOutline;
import org.fordiac.ide.fbt.typeeditor.editors.FBTypeEditor;

public class CompositeEditorInterfaceSection extends InterfaceSection {

	@Override
	protected CommandStack getCommandStack(IWorkbenchPart part, Object input) {
		if(part instanceof FBTypeEditor){
			return ((FBTypeEditor)part).getCommandStack();
		}
		if(part instanceof ContentOutline){
			return ((FBTypeContentOutline) ((ContentOutline)part).getCurrentPage()).getCommandStack();
		}
		return null;
	}

}
