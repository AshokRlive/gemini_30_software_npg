/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network.actions;

import org.eclipse.gef.EditPart;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.application.actions.PasteEditPartsAction;
import org.fordiac.ide.fbt.typeeditor.network.editparts.CompositeNetworkEditPart;

public class CFBPasteEditPartsAction extends PasteEditPartsAction {
	
	public CFBPasteEditPartsAction(IWorkbenchPart editor) {
		super(editor);
	}

	@Override
	protected boolean isCompatiblePasteTargetEditPart(EditPart part) {
		return (part instanceof CompositeNetworkEditPart);
	}

}
