/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network.viewer;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.ui.parts.GraphicalEditor;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.fbt.typeeditor.network.editparts.CompositeNetworkEditPartFactory;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.ui.CompositeInternalInterfaceElementView;
import org.fordiac.ide.model.ui.UIFBNetwork;

/**
 * A factory for creating ECCEditPart objects.
 */
public class CompositeViewerEditPartFactory extends CompositeNetworkEditPartFactory {

	private FB fbInstance;
	private FBEditPart fbEditPart;
	

	public CompositeViewerEditPartFactory(GraphicalEditor editor, FB fbInstance, FBEditPart fbEditPart, ZoomManager zoomManager) {
		super(editor, (CompositeFBType)fbInstance.getFBType(), zoomManager);
		this.fbInstance = fbInstance;
		this.fbEditPart = fbEditPart;
	}
	
	/**
	 * Maps an object to an EditPart.
	 * 
	 * @param context
	 *          the context
	 * @param modelElement
	 *          the model element
	 * 
	 * @return the part for element
	 * 
	 * @throws RuntimeException
	 *           if no match was found (programming error)
	 */
	protected EditPart getPartForElement(final EditPart context,
			final Object modelElement) {

		if (modelElement instanceof UIFBNetwork) {
			CompositeNetworkViewerEditPart compositeNetEP = new CompositeNetworkViewerEditPart();
			compositeNetEP.setFbType((CompositeFBType)fbInstance.getFBType());
			compositeNetEP.setFbInstance(fbInstance);
			if (fbEditPart.getParent() instanceof CompositeNetworkViewerEditPart) {
				compositeNetEP.setparentInstanceViewerEditPart((CompositeNetworkViewerEditPart)fbEditPart.getParent());	
			}
			return compositeNetEP;

		}
		
		if (modelElement instanceof CompositeInternalInterfaceElementView) {
			return new CompositeInternalInterfaceEditPartRO();
		}
		
		return super.getPartForElement(context, modelElement);
	}

}
