/*******************************************************************************
 * Copyright (c) 2007 - 2015 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network;

import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.palette.PaletteViewerProvider;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.actions.ActionFactory;
import org.fordiac.ide.application.actions.FlattenSubApplicationAction;
import org.fordiac.ide.application.actions.NewSubApplicationAction;
import org.fordiac.ide.application.actions.UnmapAction;
import org.fordiac.ide.application.actions.UnmapAllAction;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.application.editors.FBTypePaletteViewerProvider;
import org.fordiac.ide.fbt.typeeditor.FBTypeEditDomain;
import org.fordiac.ide.fbt.typeeditor.contentprovider.InterfaceContextMenuProvider;
import org.fordiac.ide.fbt.typeeditor.editors.IFBTEditorPart;
import org.fordiac.ide.fbt.typeeditor.network.actions.CFBPasteEditPartsAction;
import org.fordiac.ide.fbt.typeeditor.network.editparts.CompositeNetworkEditPartFactory;
import org.fordiac.ide.fbt.typemanagement.FBTypeEditorInput;
import org.fordiac.ide.model.Palette.Palette;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.FBNetwork;
import org.fordiac.ide.model.ui.ConnectionView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.FBView;
import org.fordiac.ide.util.imageprovider.FordiacImage;

public class CompositeNetworkEditor extends FBNetworkEditor implements IFBTEditorPart {

	protected CompositeFBType fbType;
	protected CommandStack commandStack;
	protected Palette palette;
	protected EContentAdapter adapter = new EContentAdapter() {
		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			// only refresh propertypage (page) if the event is not an
			// REMOVING_ADAPTER event - otherwise, the remove adapter in the
			// dispose method (called when closing the editor) will fail
			if (notification.getEventType() != Notification.REMOVING_ADAPTER
					&& (((notification.getNewValue() == null) && (notification
							.getNewValue() != notification.getOldValue())) || ((notification
							.getNewValue() != null) && !(notification
							.getNewValue().equals(notification.getOldValue()))))) {
				super.notifyChanged(notification);
			}

		}
	};
	
	@Override
	public void setCommonCommandStack(CommandStack commandStack) {
		this.commandStack = commandStack;
	}

	public CompositeNetworkEditor() {}

	protected void initializeGraphicalViewer() {
		super.initializeGraphicalViewer();
	}

	@Override
	protected CompositeNetworkEditPartFactory getEditPartFactory() {
		return new CompositeNetworkEditPartFactory(this, fbType, getZoomManger() );
	}

	@Override
	public void selectionChanged(final IWorkbenchPart part,
			final ISelection selection) {
		super.selectionChanged(part, selection);
		updateActions(getSelectionActions());
	}

	@Override
	public void doSave(final IProgressMonitor monitor) {
		//currently nothing needs to be done here
	}

	@Override
	public void dispose() {
		Diagram diagram = getDiagramModel();
		
		if (diagram != null) {
			diagram.eAdapters().remove(adapter);
		}			
		super.dispose();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void createActions() {
		ActionRegistry registry = getActionRegistry();
		IAction action;
		
		super.createActions();
		
		//remove not suitable application actions
		action = registry.getAction(NewSubApplicationAction.ID);
		registry.removeAction(action);
		
		action = registry.getAction(FlattenSubApplicationAction.ID);
		registry.removeAction(action);
		
		action = registry.getAction(UnmapAction.ID);
		registry.removeAction(action);
		
		action = registry.getAction(UnmapAllAction.ID);
		registry.removeAction(action);
		
		//install an own paste handler
		action = registry.getAction(ActionFactory.PASTE.getId());
		registry.removeAction(action);
		getSelectionActions().remove(action.getId());
		
		action = new CFBPasteEditPartsAction(this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
		getEditorSite().getActionBars().setGlobalActionHandler(ActionFactory.PASTE.getId(), action);
		
		InterfaceContextMenuProvider.createInterfaceEditingActions(this, registry, fbType);
		
		super.createActions();
	}
	
	@Override
	public boolean outlineSelectionChanged(Object selectedElement) {

		EditPart editPart = getEditPartForSelection(selectedElement);
		if (null != editPart) {
			getGraphicalViewer().select(editPart);
			return true;
		}
		if (selectedElement instanceof FBNetwork) {
			return true;
		}

		return false;
	}

	EditPart getEditPartForSelection(Object selectedElement) {
		@SuppressWarnings("rawtypes")
		Map map = getGraphicalViewer().getEditPartRegistry();

		for (Object key : map.keySet()) {
			if (key instanceof FBView) {
				if (((FBView) key).getFb() == selectedElement) {
					selectedElement = key;
					break;
				}
			}
			if (key instanceof ConnectionView) {
				if (((ConnectionView) key).getConnectionElement() == selectedElement) {
					selectedElement = key;
					break;
				}
			}
		}

		Object obj = getGraphicalViewer().getEditPartRegistry().get(
				selectedElement);
		if (obj instanceof EditPart) {
			return (EditPart) obj;
		}
		return null;
	}

	@Override
	protected Palette getPalette() {
		return palette;
	}

	@Override
	public AutomationSystem getSystem() {
		return palette.getAutomationSystem();
	}

	@Override
	public String getFileName() {
		return null;		
	}

	@Override
	protected void setModel(IEditorInput input) {
		if (input instanceof FBTypeEditorInput) {
			FBTypeEditorInput untypedInput = (FBTypeEditorInput) input;
			if (untypedInput.getContent() instanceof CompositeFBType) {
				fbType = (CompositeFBType) untypedInput.getContent();
				setDiagramModelManger(createDiagramModelManger());				
				configurePalette(untypedInput);
			}
		}

		setPartName(Messages.CompositeNetworkEditor_LABEL_CompositeEditor);
		setTitleImage(FordiacImage.ICON_FBNetwork.getImage());

		setEditDomain(new FBTypeEditDomain(this, commandStack));
	}

	protected CFBUIFBNetworkManager createDiagramModelManger() {
		return new CFBUIFBNetworkManager(fbType);
	}
	
	protected void setDiagramModelManger(CFBUIFBNetworkManager model){
		uiFBNetworkManager = model;
		uiFBNetworkManager.setCommandStack(commandStack);
		
		Diagram diagram = getDiagramModel();
		if (diagram != null) {
			diagram.eAdapters().add(adapter);
		}
	}

	protected void configurePalette(FBTypeEditorInput fbTypeEditorInput) {
		Palette fbPallette = fbTypeEditorInput.getPaletteEntry().getGroup().getPallete();
		if (null != fbPallette) {
			palette = (Palette) fbPallette;
		}
	}

	@Override
	protected PaletteRoot getPaletteRoot() {
		return null;  //we are filling a the palete directly in the viewer so we don't need it here
	}

	
	@Override
	protected PaletteViewerProvider createPaletteViewerProvider() {
		return new FBTypePaletteViewerProvider(fbType.getPaletteEntry().getFile().getProject(), getEditDomain());
	}
}
