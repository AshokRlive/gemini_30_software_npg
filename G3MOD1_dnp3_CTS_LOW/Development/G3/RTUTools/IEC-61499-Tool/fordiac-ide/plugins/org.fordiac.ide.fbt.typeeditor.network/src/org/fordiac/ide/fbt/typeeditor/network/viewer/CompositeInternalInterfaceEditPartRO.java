/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network.viewer;

import org.eclipse.draw2d.AbstractBorder;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.jface.preference.IPreferenceStore;
import org.fordiac.ide.gef.FixedAnchor;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.gef.preferences.DiagramPreferences;
import org.fordiac.ide.preferences.PreferenceConstants;
import org.fordiac.ide.preferences.PreferenceGetter;

/**
 * The Class CompositeInternalInterfaceEditPart.
 */
public class CompositeInternalInterfaceEditPartRO extends InterfaceEditPart {

	private Figure figure;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getNodeEditPolicy()
	 */
	@Override
	protected GraphicalNodeEditPolicy getNodeEditPolicy() {
		return null;
	}

	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
	}

	@Override
	public void performRequest(Request request) {
		super.performRequest(request);
	}
	
	@Override
	protected IFigure createFigureForModel() {
		if (figure == null) {
			figure = new InterfaceFigure();
		}
		return figure;
	}

	/**
	 * The Class InterfaceFigure.
	 */
	public class InterfaceFigure extends Label {

		/**
		 * Instantiates a new interface figure.
		 */
		public InterfaceFigure() {
			super();
			// setText(getINamedElement().getName());
			setText(getCastedModel().getLabel());
			// setBorder(new MarginBorder(0, 5, 0, 5));
			setBorder(new ConnectorBorder());
			setOpaque(false);
			if (!isInput()) {
				setLabelAlignment(PositionConstants.LEFT);
				setTextAlignment(PositionConstants.LEFT);
			} else {
				setLabelAlignment(PositionConstants.RIGHT);
				setTextAlignment(PositionConstants.RIGHT);
			}
			// setToolTip(new ToolTipFigure(getIInterfaceElement()));
		}

	}

	/**
	 * The Class ConnectorBorder.
	 */
	public class ConnectorBorder extends AbstractBorder {
		
		private boolean highlight; 
		/**
		 * Instantiates a new connector border.
		 */
		public ConnectorBorder() {
			super();
			highlight = false;
		}
		public ConnectorBorder(boolean highlighted) {
			super();
			highlight = highlighted;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.draw2d.Border#paint(org.eclipse.draw2d.IFigure, org.eclipse.draw2d.Graphics, org.eclipse.draw2d.geometry.Insets)
		 */
		@Override
		public void paint(final IFigure figure, final Graphics graphics,
				final Insets insets) {
			if (isEvent()) {
				graphics.setForegroundColor(PreferenceGetter
						.getColor(PreferenceConstants.P_EVENT_CONNECTOR_COLOR));
				graphics.setBackgroundColor(PreferenceGetter
						.getColor(PreferenceConstants.P_EVENT_CONNECTOR_COLOR));
			} else if (isAdapter()){
				graphics.setForegroundColor(PreferenceGetter
						.getColor(PreferenceConstants.P_ADAPTER_CONNECTOR_COLOR));
				graphics.setBackgroundColor(PreferenceGetter
						.getColor(PreferenceConstants.P_ADAPTER_CONNECTOR_COLOR));
			} else {
				graphics.setForegroundColor(PreferenceGetter
						.getColor(PreferenceConstants.P_DATA_CONNECTOR_COLOR));
				graphics.setBackgroundColor(PreferenceGetter
						.getColor(PreferenceConstants.P_DATA_CONNECTOR_COLOR));
			}

			Rectangle where = null;
			Rectangle r = null;
			if (!isInput()) {
				where = getPaintRectangle(figure, insets);
				r = new Rectangle(where.x, where.y + (where.height / 2) - 1, 4, 4);
				graphics.fillRectangle(r);
			} else {
				where = getPaintRectangle(figure, insets);

				r = new Rectangle(where.width + where.x - 4, where.y
						+ (where.height / 2) - 1, 4, 4);
				graphics.fillRectangle(r);
			}
			if (highlight) {
				graphics.setForegroundColor(ColorConstants.gray);
				graphics.setBackgroundColor(ColorConstants.gray);
				// graphics.drawRectangle(where.x + 4, where.y + 1, where.width
				// - 8,
				// where.height - 2);
				IPreferenceStore pf = org.fordiac.ide.gef.Activator
						.getDefault().getPreferenceStore();
				int cornerDim = pf.getInt(DiagramPreferences.CORNER_DIM);
				if (cornerDim > 1) {
					cornerDim = cornerDim / 2;
				}
				Rectangle rect = getPaintRectangle(figure, new Insets(0, 0, 1,
						1));
				graphics.drawRoundRectangle(new Rectangle(rect.x, rect.y,
						rect.width, rect.height), cornerDim, cornerDim);
			}

		}

		/* (non-Javadoc)
		 * @see org.eclipse.draw2d.Border#getInsets(org.eclipse.draw2d.IFigure)
		 */
		@Override
		public Insets getInsets(final IFigure figure) {
			return new Insets(0, 5, 0, 5);
		}
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getSourceConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(
			final ConnectionEditPart connection) {
		return new FixedAnchor(getFigure(), !isInput(), this);
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getSourceConnectionAnchor(org.eclipse.gef.Request)
	 */
	@Override
	public ConnectionAnchor getSourceConnectionAnchor(final Request request) {
		return new FixedAnchor(getFigure(), !isInput(), this);
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getTargetConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(
			final ConnectionEditPart connection) {
		return new FixedAnchor(getFigure(), !isInput());
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.gef.editparts.InterfaceEditPart#getTargetConnectionAnchor(org.eclipse.gef.Request)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(final Request request) {
		return new FixedAnchor(getFigure(), !isInput());
	}
	
}
