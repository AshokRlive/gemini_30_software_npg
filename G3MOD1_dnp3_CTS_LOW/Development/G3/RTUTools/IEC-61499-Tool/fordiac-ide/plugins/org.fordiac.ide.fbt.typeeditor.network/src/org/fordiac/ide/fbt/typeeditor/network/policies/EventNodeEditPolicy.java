/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network.policies;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.ReconnectRequest;
import org.fordiac.ide.application.commands.AbstractEventConnectionCreateCommand;
import org.fordiac.ide.application.commands.EventConnectionCreateCommand;
import org.fordiac.ide.application.commands.ReconnectEventConnectionCommand;
import org.fordiac.ide.application.editparts.UISubAppNetworkEditPart;
import org.fordiac.ide.fbt.typeeditor.network.editparts.CompositeInternalInterfaceEditPart;
import org.fordiac.ide.gef.editparts.IDiagramEditPart;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;

/**
 * An EditPolicy which allows drawing Connections between EventInterfaces.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class EventNodeEditPolicy extends
		org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getConnectionCompleteCommand(org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCompleteCommand(
			final CreateConnectionRequest request) {
		if (request.getStartCommand() instanceof AbstractEventConnectionCreateCommand) {
			AbstractEventConnectionCreateCommand command = (AbstractEventConnectionCreateCommand) request
					.getStartCommand();
			if (!command.isInternalConnection()) {
				command
						.setInternalConnection(getHost() instanceof CompositeInternalInterfaceEditPart);
			}
			command.setTarget((InterfaceEditPart) getHost());
			return command;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getConnectionCreateCommand(org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCreateCommand(
			final CreateConnectionRequest request) {
		AbstractEventConnectionCreateCommand cmd = new EventConnectionCreateCommand();
		cmd.setSource((InterfaceEditPart) getHost());
		cmd
				.setInternalConnection(getHost() instanceof CompositeInternalInterfaceEditPart);
		EditPart parent = getHost().getParent();
		while (parent != null && !(parent instanceof IDiagramEditPart)
				&& !(parent instanceof UISubAppNetworkEditPart)) {
			parent = parent.getParent();
		}
		if (parent instanceof IDiagramEditPart) { // also means that parent !=
			// null
			cmd.setParent(((IDiagramEditPart) parent).getDiagram());
		}
		// if (parent instanceof UISubAppNetworkEditPart) {
		// cmd.setParent(((UISubAppNetworkEditPart) parent).getCastedModel());
		// }

		request.setStartCommand(cmd);
		return new EventConnectionCreateCommand();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getReconnectTargetCommand(org.eclipse.gef.requests.ReconnectRequest)
	 */
	@Override
	protected Command getReconnectTargetCommand(final ReconnectRequest request) {
		ReconnectEventConnectionCommand cmd = new ReconnectEventConnectionCommand(
				request);
		EditPart parent = getHost().getParent();
		while (parent != null && !(parent instanceof IDiagramEditPart)) {
			parent = parent.getParent();
		}
		if (parent instanceof IDiagramEditPart) { // also means that parent !=
			// null
			cmd.setParent(((IDiagramEditPart) parent).getDiagram());
		}

		return cmd;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getReconnectSourceCommand(org.eclipse.gef.requests.ReconnectRequest)
	 */
	@Override
	protected Command getReconnectSourceCommand(final ReconnectRequest request) {
		ReconnectEventConnectionCommand cmd = new ReconnectEventConnectionCommand(
				request);
		EditPart parent = getHost().getParent();
		while (parent != null && !(parent instanceof IDiagramEditPart)) {
			parent = parent.getParent();
		}
		if (parent instanceof IDiagramEditPart) { // also means that parent !=
			// null
			cmd.setParent(((IDiagramEditPart) parent).getDiagram());
		}
		return cmd;
	}
}
