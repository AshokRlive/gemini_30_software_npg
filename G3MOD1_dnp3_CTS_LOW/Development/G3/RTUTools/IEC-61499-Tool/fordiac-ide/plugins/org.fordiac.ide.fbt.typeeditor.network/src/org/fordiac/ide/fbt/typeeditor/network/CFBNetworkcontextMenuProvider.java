/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network;

import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.jface.action.IMenuManager;
import org.fordiac.ide.application.editors.UIFBNetworkContextMenuProvider;
import org.fordiac.ide.fbt.typeeditor.contentprovider.InterfaceContextMenuProvider;
import org.fordiac.ide.gef.DiagramEditorWithFlyoutPalette;
import org.fordiac.ide.model.Palette.Palette;

public class CFBNetworkcontextMenuProvider extends
		UIFBNetworkContextMenuProvider {

	public CFBNetworkcontextMenuProvider(DiagramEditorWithFlyoutPalette editor,
			ActionRegistry registry, ZoomManager zoomManager, Palette palette) {
		super(editor, registry, zoomManager, palette);
	}

	@Override
	public void buildContextMenu(IMenuManager menu) {		
		super.buildContextMenu(menu);		
		InterfaceContextMenuProvider.buildInterfaceEditEntries(menu, registry);
	}
	
	

}
