package org.fordiac.ide.fbt.typeeditor.network.viewer;

import java.util.EventObject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.jface.util.TransferDropTargetListener;
import org.eclipse.ui.IEditorInput;
import org.fordiac.ide.application.editparts.FBEditPart;
import org.fordiac.ide.application.viewer.composite.CompositeInstanceViewerInput;
import org.fordiac.ide.fbt.typeeditor.network.CFBUIFBNetworkManager;
import org.fordiac.ide.gef.DiagramEditor;
import org.fordiac.ide.gef.ZoomUndoRedoContextMenuProvider;
import org.fordiac.ide.model.libraryElement.AutomationSystem;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.FB;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.util.AdvancedPanningSelectionTool;

public class CompositeInstanceViewer extends DiagramEditor {

	private FB fb;
	private CompositeFBType cfbt;
	private FBEditPart fbEditPart;
	
	private CFBUIFBNetworkManager uiFBNetworkManager;
	
	@Override
	public Diagram getDiagramModel() {
		return uiFBNetworkManager.getUiFBNetwork();		
	}

	@Override
	protected EditPartFactory getEditPartFactory() {
		CompositeViewerEditPartFactory factory = new CompositeViewerEditPartFactory(this, 
				fb, fbEditPart, getZoomManger());
		return factory;
	}

	@Override
	protected ContextMenuProvider getContextMenuProvider(ScrollingGraphicalViewer viewer,
			ZoomManager zoomManager) {
		ZoomUndoRedoContextMenuProvider cmp = new ZoomUndoRedoContextMenuProvider(getGraphicalViewer(),
				zoomManager, getActionRegistry());
		return cmp;
	}

	@Override
	protected TransferDropTargetListener createTransferDropTargetListener() {
		//
		return null;
	}

	@Override
	public AutomationSystem getSystem() {
		return null;
	}

	@Override
	public String getFileName() {
		return null;
	}

	@Override
	protected void setModel(IEditorInput input) {

		setEditDomain(new DefaultEditDomain(this));
		getEditDomain().setDefaultTool(new AdvancedPanningSelectionTool());
		getEditDomain().setActiveTool(getEditDomain().getDefaultTool());

		if (input instanceof CompositeInstanceViewerInput) {
			CompositeInstanceViewerInput untypedInput = (CompositeInstanceViewerInput) input;
			Object content = untypedInput.getContent();
			if (content instanceof FB) {
				if (((FB) content).getFBType() instanceof CompositeFBType) {
					fb = (FB) content;
					//we need to copy the type so that we have an instance specific network TODO consider using here the type
					//cfbt = EcoreUtil.copy((CompositeFBType) fb.getFBType()); 
					cfbt = (CompositeFBType) fb.getFBType();
					this.fbEditPart = untypedInput.getFbEditPart();
					uiFBNetworkManager = new CFBUIFBNetworkManager(cfbt);
				}
			}
		}
	}

	protected void initializeGraphicalViewer() {
		GraphicalViewer viewer = getGraphicalViewer();
		if (cfbt.getFBNetwork() != null) {			
			viewer.setContents(getDiagramModel());
		}
	}
	
	@Override
	public void commandStackChanged(EventObject event) {
		// nothing to do as its a viewer!
	}
	
	@Override
	public void doSave(IProgressMonitor monitor) {
		// nothing to do as its a viewer!
	}
	
	@Override
	public void doSaveAs() {
		super.doSaveAs();
	}
}
