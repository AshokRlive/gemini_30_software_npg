/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.network.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LayoutManager;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;
import org.fordiac.ide.application.editparts.EditorWithInterfaceEditPart;
import org.fordiac.ide.gef.editparts.IDiagramEditPart;
import org.fordiac.ide.gef.editparts.InterfaceEditPart;
import org.fordiac.ide.model.libraryElement.AdapterDeclaration;
import org.fordiac.ide.model.libraryElement.CompositeFBType;
import org.fordiac.ide.model.libraryElement.IInterfaceElement;
import org.fordiac.ide.model.libraryElement.SubAppType;
import org.fordiac.ide.model.ui.CompositeInternalInterfaceElementView;
import org.fordiac.ide.model.ui.Diagram;
import org.fordiac.ide.model.ui.InterfaceElementView;
import org.fordiac.ide.model.ui.UIFBNetwork;

/**
 * Edit Part for the visualization of Composite Networks.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class CompositeNetworkEditPart extends EditorWithInterfaceEditPart
		implements IDiagramEditPart {

	/** The adapter. */
	protected EContentAdapter adapter;
	
	protected CompositeFBType fbType;


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (!isActive()) {
			super.activate();
			((Notifier) getModel()).eAdapters().add(getContentAdapter());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((Notifier) getModel()).eAdapters().remove(getContentAdapter());
		}
	}

	/**
	 * Gets the content adapter.
	 * 
	 * @return the content adapter
	 */
	public EContentAdapter getContentAdapter() {
		if (adapter == null) {
			adapter = new EContentAdapter() {
				@Override
				public void notifyChanged(final Notification notification) {
					int type = notification.getEventType();
					switch (type) {
					case Notification.ADD:
					case Notification.ADD_MANY:
						refreshChildren();
						break;
					case Notification.MOVE:
						if (notification.getNewValue() instanceof CompositeInternalInterfaceElementView) {
							refreshChildren();
						}
						break;
					case Notification.REMOVE:
					case Notification.REMOVE_MANY:
						refreshChildren();
						break;
					case Notification.SET:
						refreshVisuals();
						break;
					}
				}
			};
		}
		return adapter;
	}

	/**
	 * Creates the EditPolicies used for this EditPart.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE,
				new RootComponentEditPolicy());
		// // handles constraint changes of model elements and creation of new
		// // model elements
		installEditPolicy(EditPolicy.LAYOUT_ROLE,
				new CompositeFBNetworkLayoutEditPolicy());

	}

	/**
	 * returns the model object as <code>FBNetwork</code>.
	 * 
	 * @return FBNetwork to be visualized
	 */
	public UIFBNetwork getCastedModel() {
		return (UIFBNetwork) getModel();
	}

	/**
	 * Returns the children of the FBNetwork.
	 * 
	 * @return the list of children s
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@Override
	protected List<?> getModelChildren() {
		ArrayList<Object> children = new ArrayList<Object>();
		if (getCastedModel() != null) {
			children.addAll(getCastedModel().getChildren());
			children.addAll(super.getModelChildren());
		} 
		 return children;
	}

	/**
	 * Adds the childEditParts figure to the corresponding container.
	 * 
	 * @param childEditPart
	 *            the child edit part
	 * @param index
	 *            the index
	 */
	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		boolean visible = true;
		if (childEditPart instanceof InterfaceEditPart) {
			if (childEditPart.getModel() instanceof InterfaceElementView) {
				IInterfaceElement iElement = ((InterfaceElementView) childEditPart.getModel()).getIInterfaceElement();
				if (iElement instanceof AdapterDeclaration)
					//if we are in a subapptype we want to show the adapter as type interface element
					visible = iElement.eContainer().eContainer() instanceof SubAppType;
			}

		}
		
		EditPart refEditPart = null;
		if(index < getChildren().size()){
			refEditPart = (EditPart)getChildren().get(index);
		}
		
		if (childEditPart instanceof InterfaceEditPart
				&& ((InterfaceEditPart) childEditPart).isInput()) {
			IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
			
			if (((InterfaceEditPart) childEditPart).isEvent()) {
				insertChild(getLeftEventInterfaceContainer(), refEditPart, child);
			} else {
				if (visible) { // add adapter interface elemetns directly to the container and set them to visible = false
					insertChild(getLeftVarInterfaceContainer(), refEditPart, child);
				} else {
					insertChild(getLeftInterfaceContainer(), refEditPart, child);
				}
			}
			child.setVisible(visible);
		} else if (childEditPart instanceof InterfaceEditPart
				&& !((InterfaceEditPart) childEditPart).isInput()) {
			IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
			if (((InterfaceEditPart) childEditPart).isEvent()) {
				insertChild(getRightEventInterfaceContainer(), refEditPart, child);
			} else {
				if (visible) { // add adapter interface elemetns directly to the container and set them to visible = false
					insertChild(getRightVarInterfaceContainer(), refEditPart, child);
				} else {
					insertChild(getRightInterfaceContainer(), refEditPart, child);
				}
			}
			child.setVisible(visible);
		} else {
			super.addChildVisual(childEditPart, -1);
		}
	}

	private void insertChild(Figure container,
			EditPart refEditPart, IFigure child) {
		if(null != refEditPart){
			int index = container.getChildren().indexOf(((GraphicalEditPart) refEditPart).getFigure());
			container.add(child, index);
		}else{
			container.add(child);
		}
	}

	/**
	 * Removes the childEditParts figures from the correct container.
	 * 
	 * @param childEditPart
	 *            the child edit part
	 */
	@Override
	protected void removeChildVisual(final EditPart childEditPart) {
		boolean visible = true;
		if (childEditPart instanceof InterfaceEditPart) {
			if (childEditPart.getModel() instanceof InterfaceElementView) {
				if (((InterfaceElementView) childEditPart.getModel())
						.getIInterfaceElement() instanceof AdapterDeclaration)
					visible = false;
			}

		}

		if (childEditPart instanceof InterfaceEditPart
				&& ((InterfaceEditPart) childEditPart).isInput() ) {
			IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
			if (((InterfaceEditPart) childEditPart).isEvent()) {
				getLeftEventInterfaceContainer().remove(child);
			} else {
				if (visible) {
					getLeftVarInterfaceContainer().remove(child);
				} else{
					getLeftInterfaceContainer().remove(child);
				}
			}
		} else if (childEditPart instanceof InterfaceEditPart
				&& !((InterfaceEditPart) childEditPart).isInput()) {
			IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
			if (((InterfaceEditPart) childEditPart).isEvent()) {
				getRightEventInterfaceContainer().remove(child);
			} else {
				if (visible) {
					getRightVarInterfaceContainer().remove(child);
				} else {
					getRightInterfaceContainer().remove(child);
				}
			}
		} else {
			super.removeChildVisual(childEditPart);
		}
	}
	
	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void reorderChild(EditPart editpart, int index) {
		// Save the constraint of the child so that it does not
		// get lost during the remove and re-add.
		IFigure childFigure = ((GraphicalEditPart) editpart).getFigure();
		LayoutManager layout = getContentPane().getLayoutManager();
		Object constraint = null;
		if (layout != null)
			constraint = layout.getConstraint(childFigure);		
		
		removeChildVisual(editpart);
		//addChildvisual needs to be done before the children list is updated in order to allow add child visual to determine the place to put the part
		addChildVisual(editpart, index);  
		List children = getChildren();
		children.remove(editpart);
		children.add(index, editpart);
		
		setLayoutConstraint(editpart, childFigure, constraint);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void addChild(EditPart child, int index) {
		Assert.isNotNull(child);
		if (index == -1)
			index = getChildren().size();
		if (children == null)
			children = new ArrayList(2);

		//addChildvisual needs to be done before the children list is updated in order to allow add child visual to determine the place to put the part
		addChildVisual(child, index);
		children.add(index, child);
		child.setParent(this);
		child.addNotify();

		if (isActive())
			child.activate();
		fireChildAdded(child, index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fordiac.ide.gef.editparts.IDiagramEditPart#getDiagram()
	 */
	@Override
	public Diagram getDiagram() {
		return getCastedModel();
	}
	
	public CompositeFBType getFbType() {
		return fbType;
	}

	public void setFbType(CompositeFBType fbType) {
		this.fbType = fbType;
	}
}
