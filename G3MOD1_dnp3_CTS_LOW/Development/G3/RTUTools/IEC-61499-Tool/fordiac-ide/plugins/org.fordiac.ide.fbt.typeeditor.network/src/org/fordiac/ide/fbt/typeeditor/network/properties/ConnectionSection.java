package org.fordiac.ide.fbt.typeeditor.network.properties;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.contentoutline.ContentOutline;
import org.fordiac.ide.application.editparts.ConnectionEditPart;
import org.fordiac.ide.fbt.typeeditor.editors.FBTypeContentOutline;
import org.fordiac.ide.fbt.typeeditor.editors.FBTypeEditor;
import org.fordiac.ide.model.libraryElement.Connection;
import org.fordiac.ide.model.ui.ConnectionView;

public class ConnectionSection extends org.fordiac.ide.gef.properties.ConnectionSection {

	@Override
	protected CommandStack getCommandStack(IWorkbenchPart part, Object input) {
		if(part instanceof FBTypeEditor){
			return ((FBTypeEditor)part).getCommandStack();
		}
		if(part instanceof ContentOutline){
			return ((FBTypeContentOutline) ((ContentOutline)part).getCurrentPage()).getCommandStack();
		}
		return null;
	}

	@Override
	protected Connection getInputType(Object input) {
		if(input instanceof ConnectionEditPart){
			return ((ConnectionView)((ConnectionEditPart) input).getModel()).getConnectionElement();
		}
		return null;
	}

}
