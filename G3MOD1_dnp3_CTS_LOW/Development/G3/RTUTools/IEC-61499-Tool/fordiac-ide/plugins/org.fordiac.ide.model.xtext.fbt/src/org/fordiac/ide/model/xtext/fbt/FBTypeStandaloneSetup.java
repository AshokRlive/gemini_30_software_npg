/*
* generated by Xtext
*/
package org.fordiac.ide.model.xtext.fbt;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class FBTypeStandaloneSetup extends FBTypeStandaloneSetupGenerated{

	public static void doSetup() {
		new FBTypeStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

