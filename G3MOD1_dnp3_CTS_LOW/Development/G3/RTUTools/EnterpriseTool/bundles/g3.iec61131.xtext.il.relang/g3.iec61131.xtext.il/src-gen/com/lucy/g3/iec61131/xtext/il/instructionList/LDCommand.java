/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LD Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.LDCommand#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.LDCommand#isValue <em>Value</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getLDCommand()
 * @model
 * @generated
 */
public interface LDCommand extends ILCommand
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getLDCommand_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.LDCommand#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(boolean)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getLDCommand_Value()
   * @model
   * @generated
   */
  boolean isValue();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.LDCommand#isValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #isValue()
   * @generated
   */
  void setValue(boolean value);

} // LDCommand
