/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.ObjectReference#getEntity <em>Entity</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getObjectReference()
 * @model
 * @generated
 */
public interface ObjectReference extends Reference
{
  /**
   * Returns the value of the '<em><b>Entity</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Entity</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Entity</em>' reference.
   * @see #setEntity(FunctionBlock)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getObjectReference_Entity()
   * @model
   * @generated
   */
  FunctionBlock getEntity();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ObjectReference#getEntity <em>Entity</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Entity</em>' reference.
   * @see #getEntity()
   * @generated
   */
  void setEntity(FunctionBlock value);

} // ObjectReference
