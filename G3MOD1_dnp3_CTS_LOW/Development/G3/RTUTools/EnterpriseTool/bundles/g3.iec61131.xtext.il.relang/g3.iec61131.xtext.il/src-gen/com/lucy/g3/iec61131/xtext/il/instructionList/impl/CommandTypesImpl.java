/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.impl;

import com.lucy.g3.iec61131.xtext.il.instructionList.CommandTypes;
import com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Command Types</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CommandTypesImpl extends MinimalEObjectImpl.Container implements CommandTypes
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CommandTypesImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return InstructionListPackage.Literals.COMMAND_TYPES;
  }

} //CommandTypesImpl
