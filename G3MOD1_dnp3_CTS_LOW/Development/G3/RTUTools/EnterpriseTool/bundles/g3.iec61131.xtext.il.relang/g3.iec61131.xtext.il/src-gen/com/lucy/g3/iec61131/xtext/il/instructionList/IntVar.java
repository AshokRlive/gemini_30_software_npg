/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.IntVar#getIntType <em>Int Type</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getIntVar()
 * @model
 * @generated
 */
public interface IntVar extends AVar
{
  /**
   * Returns the value of the '<em><b>Int Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Int Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Int Type</em>' containment reference.
   * @see #setIntType(IntResourceType)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getIntVar_IntType()
   * @model containment="true"
   * @generated
   */
  IntResourceType getIntType();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.IntVar#getIntType <em>Int Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Int Type</em>' containment reference.
   * @see #getIntType()
   * @generated
   */
  void setIntType(IntResourceType value);

} // IntVar
