/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.impl;

import com.lucy.g3.iec61131.xtext.il.instructionList.ADDOperator;
import com.lucy.g3.iec61131.xtext.il.instructionList.ANDOperator;
import com.lucy.g3.iec61131.xtext.il.instructionList.AVar;
import com.lucy.g3.iec61131.xtext.il.instructionList.BoolData;
import com.lucy.g3.iec61131.xtext.il.instructionList.BoolVar;
import com.lucy.g3.iec61131.xtext.il.instructionList.BooleanParameter;
import com.lucy.g3.iec61131.xtext.il.instructionList.BooleanResourceType;
import com.lucy.g3.iec61131.xtext.il.instructionList.CallCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.Command;
import com.lucy.g3.iec61131.xtext.il.instructionList.CommandTypes;
import com.lucy.g3.iec61131.xtext.il.instructionList.CustomVar;
import com.lucy.g3.iec61131.xtext.il.instructionList.DIVOperator;
import com.lucy.g3.iec61131.xtext.il.instructionList.DelayCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.DotExpression;
import com.lucy.g3.iec61131.xtext.il.instructionList.EQBool;
import com.lucy.g3.iec61131.xtext.il.instructionList.EQCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.EQInt;
import com.lucy.g3.iec61131.xtext.il.instructionList.EQNBool;
import com.lucy.g3.iec61131.xtext.il.instructionList.EQNInt;
import com.lucy.g3.iec61131.xtext.il.instructionList.ExtendedCommands;
import com.lucy.g3.iec61131.xtext.il.instructionList.Feature;
import com.lucy.g3.iec61131.xtext.il.instructionList.FlagByteResource;
import com.lucy.g3.iec61131.xtext.il.instructionList.Function;
import com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock;
import com.lucy.g3.iec61131.xtext.il.instructionList.FunctionCall;
import com.lucy.g3.iec61131.xtext.il.instructionList.FunctionInputs;
import com.lucy.g3.iec61131.xtext.il.instructionList.ILCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel;
import com.lucy.g3.iec61131.xtext.il.instructionList.ILObject;
import com.lucy.g3.iec61131.xtext.il.instructionList.InitData;
import com.lucy.g3.iec61131.xtext.il.instructionList.InputBitResource;
import com.lucy.g3.iec61131.xtext.il.instructionList.InputVars;
import com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListFactory;
import com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage;
import com.lucy.g3.iec61131.xtext.il.instructionList.IntParameter;
import com.lucy.g3.iec61131.xtext.il.instructionList.IntResourceType;
import com.lucy.g3.iec61131.xtext.il.instructionList.IntVar;
import com.lucy.g3.iec61131.xtext.il.instructionList.JMPCNCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.JMPCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.LDCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.LogicOpCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.MULOperator;
import com.lucy.g3.iec61131.xtext.il.instructionList.MathCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.OROperator;
import com.lucy.g3.iec61131.xtext.il.instructionList.ObjectReference;
import com.lucy.g3.iec61131.xtext.il.instructionList.OutputBitResource;
import com.lucy.g3.iec61131.xtext.il.instructionList.OutputVars;
import com.lucy.g3.iec61131.xtext.il.instructionList.Parameter;
import com.lucy.g3.iec61131.xtext.il.instructionList.Program;
import com.lucy.g3.iec61131.xtext.il.instructionList.RCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.Reference;
import com.lucy.g3.iec61131.xtext.il.instructionList.Resource;
import com.lucy.g3.iec61131.xtext.il.instructionList.ResourceParamater;
import com.lucy.g3.iec61131.xtext.il.instructionList.ResourceType;
import com.lucy.g3.iec61131.xtext.il.instructionList.ResourceVar;
import com.lucy.g3.iec61131.xtext.il.instructionList.ReturnCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.SCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.STCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.SUBOperator;
import com.lucy.g3.iec61131.xtext.il.instructionList.SimpleTypeEnum;
import com.lucy.g3.iec61131.xtext.il.instructionList.TimeData;
import com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand;
import com.lucy.g3.iec61131.xtext.il.instructionList.TimerVar;
import com.lucy.g3.iec61131.xtext.il.instructionList.Type;
import com.lucy.g3.iec61131.xtext.il.instructionList.Vars;
import com.lucy.g3.iec61131.xtext.il.instructionList.XOROperator;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InstructionListPackageImpl extends EPackageImpl implements InstructionListPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ilObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass programEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionBlockEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass featureEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass referenceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass rCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ilLabelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionCallEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionInputsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jmpcnCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jmpCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass eqCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass eqBoolEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass eqnBoolEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass eqIntEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass eqnIntEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass commandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ilCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass extendedCommandsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass delayCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass parameterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass resourceParamaterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass intParameterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanParameterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass stCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ldCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mathCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass addOperatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subOperatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mulOperatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass divOperatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass logicOpCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass andOperatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass orOperatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass xorOperatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass returnCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass callCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass commandTypesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass initDataEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolDataEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass timeDataEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass aVarEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass varsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass inputVarsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass outputVarsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass resourceVarEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass resourceTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass intResourceTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanResourceTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass customVarEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass intVarEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolVarEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass timerVarEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass timerCallCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass resourceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass inputBitResourceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass outputBitResourceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass flagByteResourceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass objectReferenceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass dotExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum simpleTypeEnumEEnum = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private InstructionListPackageImpl()
  {
    super(eNS_URI, InstructionListFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link InstructionListPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static InstructionListPackage init()
  {
    if (isInited) return (InstructionListPackage)EPackage.Registry.INSTANCE.getEPackage(InstructionListPackage.eNS_URI);

    // Obtain or create and register package
    InstructionListPackageImpl theInstructionListPackage = (InstructionListPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof InstructionListPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new InstructionListPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    EcorePackage.eINSTANCE.eClass();

    // Create package meta-data objects
    theInstructionListPackage.createPackageContents();

    // Initialize created meta-data
    theInstructionListPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theInstructionListPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(InstructionListPackage.eNS_URI, theInstructionListPackage);
    return theInstructionListPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getILObject()
  {
    return ilObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getILObject_Main()
  {
    return (EReference)ilObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getILObject_Functions()
  {
    return (EReference)ilObjectEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getILObject_Functionblocks()
  {
    return (EReference)ilObjectEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getProgram()
  {
    return programEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getProgram_Name()
  {
    return (EAttribute)programEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProgram_Vars()
  {
    return (EReference)programEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProgram_InputVars()
  {
    return (EReference)programEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProgram_OutputVars()
  {
    return (EReference)programEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProgram_Commands()
  {
    return (EReference)programEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProgram_Labels()
  {
    return (EReference)programEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionBlock()
  {
    return functionBlockEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFunctionBlock_Name()
  {
    return (EAttribute)functionBlockEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionBlock_Vars()
  {
    return (EReference)functionBlockEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionBlock_InputVars()
  {
    return (EReference)functionBlockEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionBlock_OutputVars()
  {
    return (EReference)functionBlockEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionBlock_Commands()
  {
    return (EReference)functionBlockEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionBlock_Labels()
  {
    return (EReference)functionBlockEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunction()
  {
    return functionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFunction_Name()
  {
    return (EAttribute)functionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunction_Vars()
  {
    return (EReference)functionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunction_InputVars()
  {
    return (EReference)functionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunction_OutputVars()
  {
    return (EReference)functionEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunction_Commands()
  {
    return (EReference)functionEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFeature()
  {
    return featureEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFeature_Vars()
  {
    return (EReference)featureEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReference()
  {
    return referenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRCommand()
  {
    return rCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRCommand_Name()
  {
    return (EAttribute)rCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSCommand()
  {
    return sCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSCommand_Name()
  {
    return (EAttribute)sCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getILLabel()
  {
    return ilLabelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getILLabel_Name()
  {
    return (EAttribute)ilLabelEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getILLabel_InitialCommand()
  {
    return (EReference)ilLabelEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getILLabel_Commands()
  {
    return (EReference)ilLabelEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionCall()
  {
    return functionCallEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFunctionCall_Name()
  {
    return (EAttribute)functionCallEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFunctionCall_Arguments()
  {
    return (EAttribute)functionCallEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionInputs()
  {
    return functionInputsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFunctionInputs_Items()
  {
    return (EAttribute)functionInputsEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getJMPCNCommand()
  {
    return jmpcnCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getJMPCNCommand_Name()
  {
    return (EAttribute)jmpcnCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getJMPCommand()
  {
    return jmpCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getJMPCommand_Name()
  {
    return (EAttribute)jmpCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEQCommand()
  {
    return eqCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getEQCommand_Value()
  {
    return (EAttribute)eqCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEQBool()
  {
    return eqBoolEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEQNBool()
  {
    return eqnBoolEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEQInt()
  {
    return eqIntEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEQNInt()
  {
    return eqnIntEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCommand()
  {
    return commandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getILCommand()
  {
    return ilCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExtendedCommands()
  {
    return extendedCommandsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDelayCommand()
  {
    return delayCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDelayCommand_Name()
  {
    return (EAttribute)delayCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDelayCommand_Value()
  {
    return (EAttribute)delayCommandEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getParameter()
  {
    return parameterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getResourceParamater()
  {
    return resourceParamaterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getResourceParamater_Name()
  {
    return (EAttribute)resourceParamaterEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntParameter()
  {
    return intParameterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIntParameter_Name()
  {
    return (EAttribute)intParameterEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanParameter()
  {
    return booleanParameterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBooleanParameter_Name()
  {
    return (EAttribute)booleanParameterEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSTCommand()
  {
    return stCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSTCommand_Name()
  {
    return (EAttribute)stCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLDCommand()
  {
    return ldCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLDCommand_Name()
  {
    return (EAttribute)ldCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLDCommand_Value()
  {
    return (EAttribute)ldCommandEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMathCommand()
  {
    return mathCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMathCommand_Name()
  {
    return (EAttribute)mathCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getADDOperator()
  {
    return addOperatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSUBOperator()
  {
    return subOperatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMULOperator()
  {
    return mulOperatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDIVOperator()
  {
    return divOperatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLogicOpCommand()
  {
    return logicOpCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLogicOpCommand_Name()
  {
    return (EAttribute)logicOpCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getANDOperator()
  {
    return andOperatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOROperator()
  {
    return orOperatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getXOROperator()
  {
    return xorOperatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReturnCommand()
  {
    return returnCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getReturnCommand_Ret()
  {
    return (EAttribute)returnCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCallCommand()
  {
    return callCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallCommand_Command()
  {
    return (EReference)callCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCommandTypes()
  {
    return commandTypesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInitData()
  {
    return initDataEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolData()
  {
    return boolDataEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBoolData_Value()
  {
    return (EAttribute)boolDataEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTimeData()
  {
    return timeDataEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTimeData_Value()
  {
    return (EAttribute)timeDataEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAVar()
  {
    return aVarEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAVar_Vars()
  {
    return (EReference)aVarEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAVar_Name()
  {
    return (EAttribute)aVarEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getType()
  {
    return typeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getType_Type()
  {
    return (EAttribute)typeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVars()
  {
    return varsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVars_Vars()
  {
    return (EReference)varsEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInputVars()
  {
    return inputVarsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOutputVars()
  {
    return outputVarsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getResourceVar()
  {
    return resourceVarEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getResourceVar_Resource()
  {
    return (EReference)resourceVarEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getResourceVar_Type()
  {
    return (EReference)resourceVarEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getResourceType()
  {
    return resourceTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntResourceType()
  {
    return intResourceTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIntResourceType_Value()
  {
    return (EAttribute)intResourceTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanResourceType()
  {
    return booleanResourceTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBooleanResourceType_Value()
  {
    return (EAttribute)booleanResourceTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCustomVar()
  {
    return customVarEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCustomVar_Type()
  {
    return (EReference)customVarEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCustomVar_Initializer()
  {
    return (EReference)customVarEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntVar()
  {
    return intVarEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIntVar_IntType()
  {
    return (EReference)intVarEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolVar()
  {
    return boolVarEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBoolVar_BoolType()
  {
    return (EReference)boolVarEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTimerVar()
  {
    return timerVarEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTimerVar_Timer()
  {
    return (EAttribute)timerVarEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTimerCallCommand()
  {
    return timerCallCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTimerCallCommand_Name()
  {
    return (EAttribute)timerCallCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTimerCallCommand_In()
  {
    return (EAttribute)timerCallCommandEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTimerCallCommand_Pt()
  {
    return (EAttribute)timerCallCommandEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTimerCallCommand_Q()
  {
    return (EAttribute)timerCallCommandEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTimerCallCommand_Et()
  {
    return (EAttribute)timerCallCommandEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getResource()
  {
    return resourceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getResource_Value()
  {
    return (EAttribute)resourceEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInputBitResource()
  {
    return inputBitResourceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOutputBitResource()
  {
    return outputBitResourceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFlagByteResource()
  {
    return flagByteResourceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getObjectReference()
  {
    return objectReferenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObjectReference_Entity()
  {
    return (EReference)objectReferenceEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDotExpression()
  {
    return dotExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDotExpression_Ref()
  {
    return (EReference)dotExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDotExpression_Tail()
  {
    return (EReference)dotExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getSimpleTypeEnum()
  {
    return simpleTypeEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InstructionListFactory getInstructionListFactory()
  {
    return (InstructionListFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    ilObjectEClass = createEClass(IL_OBJECT);
    createEReference(ilObjectEClass, IL_OBJECT__MAIN);
    createEReference(ilObjectEClass, IL_OBJECT__FUNCTIONS);
    createEReference(ilObjectEClass, IL_OBJECT__FUNCTIONBLOCKS);

    programEClass = createEClass(PROGRAM);
    createEAttribute(programEClass, PROGRAM__NAME);
    createEReference(programEClass, PROGRAM__VARS);
    createEReference(programEClass, PROGRAM__INPUT_VARS);
    createEReference(programEClass, PROGRAM__OUTPUT_VARS);
    createEReference(programEClass, PROGRAM__COMMANDS);
    createEReference(programEClass, PROGRAM__LABELS);

    functionBlockEClass = createEClass(FUNCTION_BLOCK);
    createEAttribute(functionBlockEClass, FUNCTION_BLOCK__NAME);
    createEReference(functionBlockEClass, FUNCTION_BLOCK__VARS);
    createEReference(functionBlockEClass, FUNCTION_BLOCK__INPUT_VARS);
    createEReference(functionBlockEClass, FUNCTION_BLOCK__OUTPUT_VARS);
    createEReference(functionBlockEClass, FUNCTION_BLOCK__COMMANDS);
    createEReference(functionBlockEClass, FUNCTION_BLOCK__LABELS);

    functionEClass = createEClass(FUNCTION);
    createEAttribute(functionEClass, FUNCTION__NAME);
    createEReference(functionEClass, FUNCTION__VARS);
    createEReference(functionEClass, FUNCTION__INPUT_VARS);
    createEReference(functionEClass, FUNCTION__OUTPUT_VARS);
    createEReference(functionEClass, FUNCTION__COMMANDS);

    featureEClass = createEClass(FEATURE);
    createEReference(featureEClass, FEATURE__VARS);

    referenceEClass = createEClass(REFERENCE);

    rCommandEClass = createEClass(RCOMMAND);
    createEAttribute(rCommandEClass, RCOMMAND__NAME);

    sCommandEClass = createEClass(SCOMMAND);
    createEAttribute(sCommandEClass, SCOMMAND__NAME);

    ilLabelEClass = createEClass(IL_LABEL);
    createEAttribute(ilLabelEClass, IL_LABEL__NAME);
    createEReference(ilLabelEClass, IL_LABEL__INITIAL_COMMAND);
    createEReference(ilLabelEClass, IL_LABEL__COMMANDS);

    functionCallEClass = createEClass(FUNCTION_CALL);
    createEAttribute(functionCallEClass, FUNCTION_CALL__NAME);
    createEAttribute(functionCallEClass, FUNCTION_CALL__ARGUMENTS);

    functionInputsEClass = createEClass(FUNCTION_INPUTS);
    createEAttribute(functionInputsEClass, FUNCTION_INPUTS__ITEMS);

    jmpcnCommandEClass = createEClass(JMPCN_COMMAND);
    createEAttribute(jmpcnCommandEClass, JMPCN_COMMAND__NAME);

    jmpCommandEClass = createEClass(JMP_COMMAND);
    createEAttribute(jmpCommandEClass, JMP_COMMAND__NAME);

    eqCommandEClass = createEClass(EQ_COMMAND);
    createEAttribute(eqCommandEClass, EQ_COMMAND__VALUE);

    eqBoolEClass = createEClass(EQ_BOOL);

    eqnBoolEClass = createEClass(EQN_BOOL);

    eqIntEClass = createEClass(EQ_INT);

    eqnIntEClass = createEClass(EQN_INT);

    commandEClass = createEClass(COMMAND);

    ilCommandEClass = createEClass(IL_COMMAND);

    extendedCommandsEClass = createEClass(EXTENDED_COMMANDS);

    delayCommandEClass = createEClass(DELAY_COMMAND);
    createEAttribute(delayCommandEClass, DELAY_COMMAND__NAME);
    createEAttribute(delayCommandEClass, DELAY_COMMAND__VALUE);

    parameterEClass = createEClass(PARAMETER);

    resourceParamaterEClass = createEClass(RESOURCE_PARAMATER);
    createEAttribute(resourceParamaterEClass, RESOURCE_PARAMATER__NAME);

    intParameterEClass = createEClass(INT_PARAMETER);
    createEAttribute(intParameterEClass, INT_PARAMETER__NAME);

    booleanParameterEClass = createEClass(BOOLEAN_PARAMETER);
    createEAttribute(booleanParameterEClass, BOOLEAN_PARAMETER__NAME);

    stCommandEClass = createEClass(ST_COMMAND);
    createEAttribute(stCommandEClass, ST_COMMAND__NAME);

    ldCommandEClass = createEClass(LD_COMMAND);
    createEAttribute(ldCommandEClass, LD_COMMAND__NAME);
    createEAttribute(ldCommandEClass, LD_COMMAND__VALUE);

    mathCommandEClass = createEClass(MATH_COMMAND);
    createEAttribute(mathCommandEClass, MATH_COMMAND__NAME);

    addOperatorEClass = createEClass(ADD_OPERATOR);

    subOperatorEClass = createEClass(SUB_OPERATOR);

    mulOperatorEClass = createEClass(MUL_OPERATOR);

    divOperatorEClass = createEClass(DIV_OPERATOR);

    logicOpCommandEClass = createEClass(LOGIC_OP_COMMAND);
    createEAttribute(logicOpCommandEClass, LOGIC_OP_COMMAND__NAME);

    andOperatorEClass = createEClass(AND_OPERATOR);

    orOperatorEClass = createEClass(OR_OPERATOR);

    xorOperatorEClass = createEClass(XOR_OPERATOR);

    returnCommandEClass = createEClass(RETURN_COMMAND);
    createEAttribute(returnCommandEClass, RETURN_COMMAND__RET);

    callCommandEClass = createEClass(CALL_COMMAND);
    createEReference(callCommandEClass, CALL_COMMAND__COMMAND);

    commandTypesEClass = createEClass(COMMAND_TYPES);

    initDataEClass = createEClass(INIT_DATA);

    boolDataEClass = createEClass(BOOL_DATA);
    createEAttribute(boolDataEClass, BOOL_DATA__VALUE);

    timeDataEClass = createEClass(TIME_DATA);
    createEAttribute(timeDataEClass, TIME_DATA__VALUE);

    aVarEClass = createEClass(AVAR);
    createEReference(aVarEClass, AVAR__VARS);
    createEAttribute(aVarEClass, AVAR__NAME);

    typeEClass = createEClass(TYPE);
    createEAttribute(typeEClass, TYPE__TYPE);

    varsEClass = createEClass(VARS);
    createEReference(varsEClass, VARS__VARS);

    inputVarsEClass = createEClass(INPUT_VARS);

    outputVarsEClass = createEClass(OUTPUT_VARS);

    resourceVarEClass = createEClass(RESOURCE_VAR);
    createEReference(resourceVarEClass, RESOURCE_VAR__RESOURCE);
    createEReference(resourceVarEClass, RESOURCE_VAR__TYPE);

    resourceTypeEClass = createEClass(RESOURCE_TYPE);

    intResourceTypeEClass = createEClass(INT_RESOURCE_TYPE);
    createEAttribute(intResourceTypeEClass, INT_RESOURCE_TYPE__VALUE);

    booleanResourceTypeEClass = createEClass(BOOLEAN_RESOURCE_TYPE);
    createEAttribute(booleanResourceTypeEClass, BOOLEAN_RESOURCE_TYPE__VALUE);

    customVarEClass = createEClass(CUSTOM_VAR);
    createEReference(customVarEClass, CUSTOM_VAR__TYPE);
    createEReference(customVarEClass, CUSTOM_VAR__INITIALIZER);

    intVarEClass = createEClass(INT_VAR);
    createEReference(intVarEClass, INT_VAR__INT_TYPE);

    boolVarEClass = createEClass(BOOL_VAR);
    createEReference(boolVarEClass, BOOL_VAR__BOOL_TYPE);

    timerVarEClass = createEClass(TIMER_VAR);
    createEAttribute(timerVarEClass, TIMER_VAR__TIMER);

    timerCallCommandEClass = createEClass(TIMER_CALL_COMMAND);
    createEAttribute(timerCallCommandEClass, TIMER_CALL_COMMAND__NAME);
    createEAttribute(timerCallCommandEClass, TIMER_CALL_COMMAND__IN);
    createEAttribute(timerCallCommandEClass, TIMER_CALL_COMMAND__PT);
    createEAttribute(timerCallCommandEClass, TIMER_CALL_COMMAND__Q);
    createEAttribute(timerCallCommandEClass, TIMER_CALL_COMMAND__ET);

    resourceEClass = createEClass(RESOURCE);
    createEAttribute(resourceEClass, RESOURCE__VALUE);

    inputBitResourceEClass = createEClass(INPUT_BIT_RESOURCE);

    outputBitResourceEClass = createEClass(OUTPUT_BIT_RESOURCE);

    flagByteResourceEClass = createEClass(FLAG_BYTE_RESOURCE);

    objectReferenceEClass = createEClass(OBJECT_REFERENCE);
    createEReference(objectReferenceEClass, OBJECT_REFERENCE__ENTITY);

    dotExpressionEClass = createEClass(DOT_EXPRESSION);
    createEReference(dotExpressionEClass, DOT_EXPRESSION__REF);
    createEReference(dotExpressionEClass, DOT_EXPRESSION__TAIL);

    // Create enums
    simpleTypeEnumEEnum = createEEnum(SIMPLE_TYPE_ENUM);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    rCommandEClass.getESuperTypes().add(this.getILCommand());
    sCommandEClass.getESuperTypes().add(this.getILCommand());
    functionCallEClass.getESuperTypes().add(this.getILCommand());
    jmpcnCommandEClass.getESuperTypes().add(this.getILCommand());
    jmpCommandEClass.getESuperTypes().add(this.getILCommand());
    eqCommandEClass.getESuperTypes().add(this.getILCommand());
    eqBoolEClass.getESuperTypes().add(this.getEQCommand());
    eqnBoolEClass.getESuperTypes().add(this.getEQCommand());
    eqIntEClass.getESuperTypes().add(this.getEQCommand());
    eqnIntEClass.getESuperTypes().add(this.getEQCommand());
    ilCommandEClass.getESuperTypes().add(this.getCommand());
    extendedCommandsEClass.getESuperTypes().add(this.getCommand());
    delayCommandEClass.getESuperTypes().add(this.getExtendedCommands());
    resourceParamaterEClass.getESuperTypes().add(this.getParameter());
    intParameterEClass.getESuperTypes().add(this.getParameter());
    booleanParameterEClass.getESuperTypes().add(this.getParameter());
    stCommandEClass.getESuperTypes().add(this.getILCommand());
    ldCommandEClass.getESuperTypes().add(this.getILCommand());
    mathCommandEClass.getESuperTypes().add(this.getILCommand());
    addOperatorEClass.getESuperTypes().add(this.getMathCommand());
    subOperatorEClass.getESuperTypes().add(this.getMathCommand());
    mulOperatorEClass.getESuperTypes().add(this.getMathCommand());
    divOperatorEClass.getESuperTypes().add(this.getMathCommand());
    logicOpCommandEClass.getESuperTypes().add(this.getILCommand());
    andOperatorEClass.getESuperTypes().add(this.getLogicOpCommand());
    orOperatorEClass.getESuperTypes().add(this.getLogicOpCommand());
    xorOperatorEClass.getESuperTypes().add(this.getLogicOpCommand());
    returnCommandEClass.getESuperTypes().add(this.getILCommand());
    callCommandEClass.getESuperTypes().add(this.getILCommand());
    boolDataEClass.getESuperTypes().add(this.getInitData());
    timeDataEClass.getESuperTypes().add(this.getInitData());
    inputVarsEClass.getESuperTypes().add(this.getFeature());
    outputVarsEClass.getESuperTypes().add(this.getFeature());
    resourceVarEClass.getESuperTypes().add(this.getAVar());
    intResourceTypeEClass.getESuperTypes().add(this.getResourceType());
    booleanResourceTypeEClass.getESuperTypes().add(this.getResourceType());
    customVarEClass.getESuperTypes().add(this.getAVar());
    intVarEClass.getESuperTypes().add(this.getAVar());
    boolVarEClass.getESuperTypes().add(this.getAVar());
    timerVarEClass.getESuperTypes().add(this.getAVar());
    timerCallCommandEClass.getESuperTypes().add(this.getCommandTypes());
    inputBitResourceEClass.getESuperTypes().add(this.getResource());
    outputBitResourceEClass.getESuperTypes().add(this.getResource());
    flagByteResourceEClass.getESuperTypes().add(this.getResource());
    objectReferenceEClass.getESuperTypes().add(this.getReference());
    dotExpressionEClass.getESuperTypes().add(this.getReference());

    // Initialize classes and features; add operations and parameters
    initEClass(ilObjectEClass, ILObject.class, "ILObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getILObject_Main(), this.getProgram(), null, "main", null, 0, 1, ILObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getILObject_Functions(), this.getFunction(), null, "functions", null, 0, -1, ILObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getILObject_Functionblocks(), this.getFunctionBlock(), null, "functionblocks", null, 0, -1, ILObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(programEClass, Program.class, "Program", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getProgram_Name(), theEcorePackage.getEString(), "name", null, 0, 1, Program.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProgram_Vars(), this.getVars(), null, "vars", null, 0, -1, Program.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProgram_InputVars(), this.getInputVars(), null, "inputVars", null, 0, -1, Program.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProgram_OutputVars(), this.getOutputVars(), null, "outputVars", null, 0, -1, Program.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProgram_Commands(), this.getCommand(), null, "commands", null, 0, -1, Program.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProgram_Labels(), this.getILLabel(), null, "labels", null, 0, -1, Program.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(functionBlockEClass, FunctionBlock.class, "FunctionBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getFunctionBlock_Name(), theEcorePackage.getEString(), "name", null, 0, 1, FunctionBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFunctionBlock_Vars(), this.getVars(), null, "vars", null, 0, -1, FunctionBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFunctionBlock_InputVars(), this.getInputVars(), null, "inputVars", null, 0, -1, FunctionBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFunctionBlock_OutputVars(), this.getOutputVars(), null, "outputVars", null, 0, -1, FunctionBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFunctionBlock_Commands(), this.getCommand(), null, "commands", null, 0, -1, FunctionBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFunctionBlock_Labels(), this.getILLabel(), null, "labels", null, 0, -1, FunctionBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(functionEClass, Function.class, "Function", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getFunction_Name(), theEcorePackage.getEString(), "name", null, 0, 1, Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFunction_Vars(), this.getVars(), null, "vars", null, 0, -1, Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFunction_InputVars(), this.getInputVars(), null, "inputVars", null, 0, -1, Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFunction_OutputVars(), this.getOutputVars(), null, "outputVars", null, 0, -1, Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFunction_Commands(), this.getCommand(), null, "commands", null, 0, -1, Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(featureEClass, Feature.class, "Feature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getFeature_Vars(), this.getAVar(), null, "vars", null, 0, -1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(referenceEClass, Reference.class, "Reference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(rCommandEClass, RCommand.class, "RCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getRCommand_Name(), theEcorePackage.getEString(), "name", null, 0, 1, RCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(sCommandEClass, SCommand.class, "SCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSCommand_Name(), theEcorePackage.getEString(), "name", null, 0, 1, SCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ilLabelEClass, ILLabel.class, "ILLabel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getILLabel_Name(), theEcorePackage.getEString(), "name", null, 0, 1, ILLabel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getILLabel_InitialCommand(), this.getCommand(), null, "initialCommand", null, 0, 1, ILLabel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getILLabel_Commands(), this.getCommand(), null, "commands", null, 0, -1, ILLabel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(functionCallEClass, FunctionCall.class, "FunctionCall", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getFunctionCall_Name(), theEcorePackage.getEString(), "name", null, 0, 1, FunctionCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getFunctionCall_Arguments(), theEcorePackage.getEString(), "arguments", null, 0, -1, FunctionCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(functionInputsEClass, FunctionInputs.class, "FunctionInputs", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getFunctionInputs_Items(), theEcorePackage.getEString(), "items", null, 0, -1, FunctionInputs.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(jmpcnCommandEClass, JMPCNCommand.class, "JMPCNCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getJMPCNCommand_Name(), theEcorePackage.getEString(), "name", null, 0, 1, JMPCNCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(jmpCommandEClass, JMPCommand.class, "JMPCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getJMPCommand_Name(), theEcorePackage.getEString(), "name", null, 0, 1, JMPCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(eqCommandEClass, EQCommand.class, "EQCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getEQCommand_Value(), theEcorePackage.getEString(), "value", null, 0, 1, EQCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(eqBoolEClass, EQBool.class, "EQBool", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(eqnBoolEClass, EQNBool.class, "EQNBool", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(eqIntEClass, EQInt.class, "EQInt", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(eqnIntEClass, EQNInt.class, "EQNInt", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(commandEClass, Command.class, "Command", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(ilCommandEClass, ILCommand.class, "ILCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(extendedCommandsEClass, ExtendedCommands.class, "ExtendedCommands", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(delayCommandEClass, DelayCommand.class, "DelayCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDelayCommand_Name(), theEcorePackage.getEString(), "name", null, 0, 1, DelayCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDelayCommand_Value(), theEcorePackage.getEInt(), "value", null, 0, 1, DelayCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(parameterEClass, Parameter.class, "Parameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(resourceParamaterEClass, ResourceParamater.class, "ResourceParamater", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getResourceParamater_Name(), theEcorePackage.getEString(), "name", null, 0, 1, ResourceParamater.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(intParameterEClass, IntParameter.class, "IntParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getIntParameter_Name(), theEcorePackage.getEInt(), "name", null, 0, 1, IntParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanParameterEClass, BooleanParameter.class, "BooleanParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getBooleanParameter_Name(), theEcorePackage.getEBoolean(), "name", null, 0, 1, BooleanParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(stCommandEClass, STCommand.class, "STCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSTCommand_Name(), theEcorePackage.getEString(), "name", null, 0, 1, STCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ldCommandEClass, LDCommand.class, "LDCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getLDCommand_Name(), theEcorePackage.getEString(), "name", null, 0, 1, LDCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getLDCommand_Value(), theEcorePackage.getEBoolean(), "value", null, 0, 1, LDCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(mathCommandEClass, MathCommand.class, "MathCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getMathCommand_Name(), theEcorePackage.getEString(), "name", null, 0, 1, MathCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(addOperatorEClass, ADDOperator.class, "ADDOperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(subOperatorEClass, SUBOperator.class, "SUBOperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(mulOperatorEClass, MULOperator.class, "MULOperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(divOperatorEClass, DIVOperator.class, "DIVOperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(logicOpCommandEClass, LogicOpCommand.class, "LogicOpCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getLogicOpCommand_Name(), theEcorePackage.getEString(), "name", null, 0, 1, LogicOpCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(andOperatorEClass, ANDOperator.class, "ANDOperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(orOperatorEClass, OROperator.class, "OROperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(xorOperatorEClass, XOROperator.class, "XOROperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(returnCommandEClass, ReturnCommand.class, "ReturnCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getReturnCommand_Ret(), theEcorePackage.getEString(), "ret", null, 0, 1, ReturnCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(callCommandEClass, CallCommand.class, "CallCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCallCommand_Command(), this.getCommandTypes(), null, "command", null, 0, 1, CallCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(commandTypesEClass, CommandTypes.class, "CommandTypes", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(initDataEClass, InitData.class, "InitData", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(boolDataEClass, BoolData.class, "BoolData", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getBoolData_Value(), theEcorePackage.getEBoolean(), "value", null, 0, 1, BoolData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(timeDataEClass, TimeData.class, "TimeData", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getTimeData_Value(), theEcorePackage.getEString(), "value", null, 0, 1, TimeData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(aVarEClass, AVar.class, "AVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAVar_Vars(), this.getAVar(), null, "vars", null, 0, -1, AVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getAVar_Name(), theEcorePackage.getEString(), "name", null, 0, 1, AVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(typeEClass, Type.class, "Type", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getType_Type(), this.getSimpleTypeEnum(), "type", null, 0, 1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(varsEClass, Vars.class, "Vars", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getVars_Vars(), this.getAVar(), null, "vars", null, 0, -1, Vars.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(inputVarsEClass, InputVars.class, "InputVars", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(outputVarsEClass, OutputVars.class, "OutputVars", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(resourceVarEClass, ResourceVar.class, "ResourceVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getResourceVar_Resource(), this.getResource(), null, "resource", null, 0, 1, ResourceVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getResourceVar_Type(), this.getType(), null, "type", null, 0, 1, ResourceVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(resourceTypeEClass, ResourceType.class, "ResourceType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(intResourceTypeEClass, IntResourceType.class, "IntResourceType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getIntResourceType_Value(), theEcorePackage.getEInt(), "value", null, 0, 1, IntResourceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanResourceTypeEClass, BooleanResourceType.class, "BooleanResourceType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getBooleanResourceType_Value(), theEcorePackage.getEBoolean(), "value", null, 0, 1, BooleanResourceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(customVarEClass, CustomVar.class, "CustomVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCustomVar_Type(), this.getType(), null, "type", null, 0, 1, CustomVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCustomVar_Initializer(), this.getInitData(), null, "initializer", null, 0, 1, CustomVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(intVarEClass, IntVar.class, "IntVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIntVar_IntType(), this.getIntResourceType(), null, "intType", null, 0, 1, IntVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(boolVarEClass, BoolVar.class, "BoolVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBoolVar_BoolType(), this.getBooleanResourceType(), null, "boolType", null, 0, 1, BoolVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(timerVarEClass, TimerVar.class, "TimerVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getTimerVar_Timer(), theEcorePackage.getEString(), "timer", null, 0, 1, TimerVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(timerCallCommandEClass, TimerCallCommand.class, "TimerCallCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getTimerCallCommand_Name(), theEcorePackage.getEString(), "name", null, 0, 1, TimerCallCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTimerCallCommand_In(), theEcorePackage.getEString(), "in", null, 0, 1, TimerCallCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTimerCallCommand_Pt(), theEcorePackage.getEString(), "pt", null, 0, 1, TimerCallCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTimerCallCommand_Q(), theEcorePackage.getEString(), "q", null, 0, 1, TimerCallCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTimerCallCommand_Et(), theEcorePackage.getEString(), "et", null, 0, 1, TimerCallCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(resourceEClass, Resource.class, "Resource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getResource_Value(), theEcorePackage.getEInt(), "value", null, 0, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(inputBitResourceEClass, InputBitResource.class, "InputBitResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(outputBitResourceEClass, OutputBitResource.class, "OutputBitResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(flagByteResourceEClass, FlagByteResource.class, "FlagByteResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(objectReferenceEClass, ObjectReference.class, "ObjectReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getObjectReference_Entity(), this.getFunctionBlock(), null, "entity", null, 0, 1, ObjectReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(dotExpressionEClass, DotExpression.class, "DotExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getDotExpression_Ref(), this.getReference(), null, "ref", null, 0, 1, DotExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDotExpression_Tail(), this.getFeature(), null, "tail", null, 0, 1, DotExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Initialize enums and add enum literals
    initEEnum(simpleTypeEnumEEnum, SimpleTypeEnum.class, "SimpleTypeEnum");
    addEEnumLiteral(simpleTypeEnumEEnum, SimpleTypeEnum.BOOL);
    addEEnumLiteral(simpleTypeEnumEEnum, SimpleTypeEnum.SINT);
    addEEnumLiteral(simpleTypeEnumEEnum, SimpleTypeEnum.INT);
    addEEnumLiteral(simpleTypeEnumEEnum, SimpleTypeEnum.DINT);
    addEEnumLiteral(simpleTypeEnumEEnum, SimpleTypeEnum.LINT);
    addEEnumLiteral(simpleTypeEnumEEnum, SimpleTypeEnum.TIME);
    addEEnumLiteral(simpleTypeEnumEEnum, SimpleTypeEnum.DATE);
    addEEnumLiteral(simpleTypeEnumEEnum, SimpleTypeEnum.BYTE);
    addEEnumLiteral(simpleTypeEnumEEnum, SimpleTypeEnum.WORD);
    addEEnumLiteral(simpleTypeEnumEEnum, SimpleTypeEnum.CHAR);

    // Create resource
    createResource(eNS_URI);
  }

} //InstructionListPackageImpl
