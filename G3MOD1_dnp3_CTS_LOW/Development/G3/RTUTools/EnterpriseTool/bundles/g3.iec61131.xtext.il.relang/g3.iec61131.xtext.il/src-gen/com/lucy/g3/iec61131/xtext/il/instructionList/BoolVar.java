/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bool Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.BoolVar#getBoolType <em>Bool Type</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getBoolVar()
 * @model
 * @generated
 */
public interface BoolVar extends AVar
{
  /**
   * Returns the value of the '<em><b>Bool Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bool Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bool Type</em>' containment reference.
   * @see #setBoolType(BooleanResourceType)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getBoolVar_BoolType()
   * @model containment="true"
   * @generated
   */
  BooleanResourceType getBoolType();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.BoolVar#getBoolType <em>Bool Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bool Type</em>' containment reference.
   * @see #getBoolType()
   * @generated
   */
  void setBoolType(BooleanResourceType value);

} // BoolVar
