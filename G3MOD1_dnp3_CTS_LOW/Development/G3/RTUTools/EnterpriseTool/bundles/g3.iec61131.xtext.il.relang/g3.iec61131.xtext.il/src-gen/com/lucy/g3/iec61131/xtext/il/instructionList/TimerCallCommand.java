/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timer Call Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getIn <em>In</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getPt <em>Pt</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getQ <em>Q</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getEt <em>Et</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getTimerCallCommand()
 * @model
 * @generated
 */
public interface TimerCallCommand extends CommandTypes
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getTimerCallCommand_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>In</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>In</em>' attribute.
   * @see #setIn(String)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getTimerCallCommand_In()
   * @model
   * @generated
   */
  String getIn();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getIn <em>In</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>In</em>' attribute.
   * @see #getIn()
   * @generated
   */
  void setIn(String value);

  /**
   * Returns the value of the '<em><b>Pt</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pt</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pt</em>' attribute.
   * @see #setPt(String)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getTimerCallCommand_Pt()
   * @model
   * @generated
   */
  String getPt();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getPt <em>Pt</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pt</em>' attribute.
   * @see #getPt()
   * @generated
   */
  void setPt(String value);

  /**
   * Returns the value of the '<em><b>Q</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Q</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Q</em>' attribute.
   * @see #setQ(String)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getTimerCallCommand_Q()
   * @model
   * @generated
   */
  String getQ();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getQ <em>Q</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Q</em>' attribute.
   * @see #getQ()
   * @generated
   */
  void setQ(String value);

  /**
   * Returns the value of the '<em><b>Et</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Et</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Et</em>' attribute.
   * @see #setEt(String)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getTimerCallCommand_Et()
   * @model
   * @generated
   */
  String getEt();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getEt <em>Et</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Et</em>' attribute.
   * @see #getEt()
   * @generated
   */
  void setEt(String value);

} // TimerCallCommand
