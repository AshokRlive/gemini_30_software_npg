/**
 * Copyright (c) 2015 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package com.lucy.g3.iec61131.xtext.il.formatting2;

import com.lucy.g3.iec61131.xtext.il.instructionList.ILObject;
import java.util.Arrays;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmGenericArrayTypeReference;
import org.eclipse.xtext.common.types.JvmParameterizedTypeReference;
import org.eclipse.xtext.common.types.JvmTypeConstraint;
import org.eclipse.xtext.common.types.JvmTypeParameter;
import org.eclipse.xtext.common.types.JvmWildcardTypeReference;
import org.eclipse.xtext.formatting2.IFormattableDocument;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.xbase.XAssignment;
import org.eclipse.xtext.xbase.XBasicForLoopExpression;
import org.eclipse.xtext.xbase.XBinaryOperation;
import org.eclipse.xtext.xbase.XBlockExpression;
import org.eclipse.xtext.xbase.XCastedExpression;
import org.eclipse.xtext.xbase.XClosure;
import org.eclipse.xtext.xbase.XCollectionLiteral;
import org.eclipse.xtext.xbase.XConstructorCall;
import org.eclipse.xtext.xbase.XDoWhileExpression;
import org.eclipse.xtext.xbase.XExpression;
import org.eclipse.xtext.xbase.XFeatureCall;
import org.eclipse.xtext.xbase.XForLoopExpression;
import org.eclipse.xtext.xbase.XIfExpression;
import org.eclipse.xtext.xbase.XInstanceOfExpression;
import org.eclipse.xtext.xbase.XMemberFeatureCall;
import org.eclipse.xtext.xbase.XPostfixOperation;
import org.eclipse.xtext.xbase.XReturnExpression;
import org.eclipse.xtext.xbase.XSwitchExpression;
import org.eclipse.xtext.xbase.XSynchronizedExpression;
import org.eclipse.xtext.xbase.XThrowExpression;
import org.eclipse.xtext.xbase.XTryCatchFinallyExpression;
import org.eclipse.xtext.xbase.XTypeLiteral;
import org.eclipse.xtext.xbase.XVariableDeclaration;
import org.eclipse.xtext.xbase.XWhileExpression;
import org.eclipse.xtext.xbase.formatting2.XbaseFormatter;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xtype.XFunctionTypeRef;
import org.eclipse.xtext.xtype.XImportDeclaration;
import org.eclipse.xtext.xtype.XImportSection;

/**
 * The formatter is particularly important for languages with semantic whitespace, since it is responsible
 * for producing correct whitespace that reflects the semantic structure. This formatter actually modifies
 * the structure by converting single expressions in control statements to block expressions. For instance,
 * <pre>
 *     if (condition) println
 * </pre>
 * becomes
 * <pre>
 *     if (condition)
 *         println
 * </pre>
 */
@SuppressWarnings("all")
public class RuleEngineFormatter extends XbaseFormatter {
  protected void _format(final ILObject model, @Extension final IFormattableDocument document) {
  }
  
  public void format(final Object model, final IFormattableDocument document) {
    if (model instanceof JvmTypeParameter) {
      _format((JvmTypeParameter)model, document);
      return;
    } else if (model instanceof JvmFormalParameter) {
      _format((JvmFormalParameter)model, document);
      return;
    } else if (model instanceof XtextResource) {
      _format((XtextResource)model, document);
      return;
    } else if (model instanceof XAssignment) {
      _format((XAssignment)model, document);
      return;
    } else if (model instanceof XBinaryOperation) {
      _format((XBinaryOperation)model, document);
      return;
    } else if (model instanceof XDoWhileExpression) {
      _format((XDoWhileExpression)model, document);
      return;
    } else if (model instanceof XFeatureCall) {
      _format((XFeatureCall)model, document);
      return;
    } else if (model instanceof XMemberFeatureCall) {
      _format((XMemberFeatureCall)model, document);
      return;
    } else if (model instanceof XPostfixOperation) {
      _format((XPostfixOperation)model, document);
      return;
    } else if (model instanceof XWhileExpression) {
      _format((XWhileExpression)model, document);
      return;
    } else if (model instanceof XFunctionTypeRef) {
      _format((XFunctionTypeRef)model, document);
      return;
    } else if (model instanceof JvmGenericArrayTypeReference) {
      _format((JvmGenericArrayTypeReference)model, document);
      return;
    } else if (model instanceof JvmParameterizedTypeReference) {
      _format((JvmParameterizedTypeReference)model, document);
      return;
    } else if (model instanceof JvmWildcardTypeReference) {
      _format((JvmWildcardTypeReference)model, document);
      return;
    } else if (model instanceof XBasicForLoopExpression) {
      _format((XBasicForLoopExpression)model, document);
      return;
    } else if (model instanceof XBlockExpression) {
      _format((XBlockExpression)model, document);
      return;
    } else if (model instanceof XCastedExpression) {
      _format((XCastedExpression)model, document);
      return;
    } else if (model instanceof XClosure) {
      _format((XClosure)model, document);
      return;
    } else if (model instanceof XCollectionLiteral) {
      _format((XCollectionLiteral)model, document);
      return;
    } else if (model instanceof XConstructorCall) {
      _format((XConstructorCall)model, document);
      return;
    } else if (model instanceof XForLoopExpression) {
      _format((XForLoopExpression)model, document);
      return;
    } else if (model instanceof XIfExpression) {
      _format((XIfExpression)model, document);
      return;
    } else if (model instanceof XInstanceOfExpression) {
      _format((XInstanceOfExpression)model, document);
      return;
    } else if (model instanceof XReturnExpression) {
      _format((XReturnExpression)model, document);
      return;
    } else if (model instanceof XSwitchExpression) {
      _format((XSwitchExpression)model, document);
      return;
    } else if (model instanceof XSynchronizedExpression) {
      _format((XSynchronizedExpression)model, document);
      return;
    } else if (model instanceof XThrowExpression) {
      _format((XThrowExpression)model, document);
      return;
    } else if (model instanceof XTryCatchFinallyExpression) {
      _format((XTryCatchFinallyExpression)model, document);
      return;
    } else if (model instanceof XTypeLiteral) {
      _format((XTypeLiteral)model, document);
      return;
    } else if (model instanceof XVariableDeclaration) {
      _format((XVariableDeclaration)model, document);
      return;
    } else if (model instanceof ILObject) {
      _format((ILObject)model, document);
      return;
    } else if (model instanceof JvmTypeConstraint) {
      _format((JvmTypeConstraint)model, document);
      return;
    } else if (model instanceof XExpression) {
      _format((XExpression)model, document);
      return;
    } else if (model instanceof XImportDeclaration) {
      _format((XImportDeclaration)model, document);
      return;
    } else if (model instanceof XImportSection) {
      _format((XImportSection)model, document);
      return;
    } else if (model instanceof EObject) {
      _format((EObject)model, document);
      return;
    } else if (model == null) {
      _format((Void)null, document);
      return;
    } else if (model != null) {
      _format(model, document);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(model, document).toString());
    }
  }
}
