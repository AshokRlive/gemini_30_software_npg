/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Vars</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getOutputVars()
 * @model
 * @generated
 */
public interface OutputVars extends Feature
{
} // OutputVars
