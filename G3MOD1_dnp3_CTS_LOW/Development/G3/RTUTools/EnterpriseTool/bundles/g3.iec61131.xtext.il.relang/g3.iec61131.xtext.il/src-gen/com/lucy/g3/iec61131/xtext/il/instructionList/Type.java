/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.Type#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getType()
 * @model
 * @generated
 */
public interface Type extends EObject
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * The literals are from the enumeration {@link com.lucy.g3.iec61131.xtext.il.instructionList.SimpleTypeEnum}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.SimpleTypeEnum
   * @see #setType(SimpleTypeEnum)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getType_Type()
   * @model
   * @generated
   */
  SimpleTypeEnum getType();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Type#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.SimpleTypeEnum
   * @see #getType()
   * @generated
   */
  void setType(SimpleTypeEnum value);

} // Type
