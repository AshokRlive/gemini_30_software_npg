/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IL Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILObject#getMain <em>Main</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILObject#getFunctions <em>Functions</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILObject#getFunctionblocks <em>Functionblocks</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getILObject()
 * @model
 * @generated
 */
public interface ILObject extends EObject
{
  /**
   * Returns the value of the '<em><b>Main</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Main</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Main</em>' containment reference.
   * @see #setMain(Program)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getILObject_Main()
   * @model containment="true"
   * @generated
   */
  Program getMain();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILObject#getMain <em>Main</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Main</em>' containment reference.
   * @see #getMain()
   * @generated
   */
  void setMain(Program value);

  /**
   * Returns the value of the '<em><b>Functions</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61131.xtext.il.instructionList.Function}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Functions</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Functions</em>' containment reference list.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getILObject_Functions()
   * @model containment="true"
   * @generated
   */
  EList<Function> getFunctions();

  /**
   * Returns the value of the '<em><b>Functionblocks</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Functionblocks</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Functionblocks</em>' containment reference list.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getILObject_Functionblocks()
   * @model containment="true"
   * @generated
   */
  EList<FunctionBlock> getFunctionblocks();

} // ILObject
