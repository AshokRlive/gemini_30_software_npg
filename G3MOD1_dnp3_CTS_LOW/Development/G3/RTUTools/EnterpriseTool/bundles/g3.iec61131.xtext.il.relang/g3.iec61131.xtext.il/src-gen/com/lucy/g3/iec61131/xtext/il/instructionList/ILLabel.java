/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IL Label</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel#getInitialCommand <em>Initial Command</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel#getCommands <em>Commands</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getILLabel()
 * @model
 * @generated
 */
public interface ILLabel extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getILLabel_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Initial Command</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Initial Command</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Initial Command</em>' containment reference.
   * @see #setInitialCommand(Command)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getILLabel_InitialCommand()
   * @model containment="true"
   * @generated
   */
  Command getInitialCommand();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel#getInitialCommand <em>Initial Command</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Initial Command</em>' containment reference.
   * @see #getInitialCommand()
   * @generated
   */
  void setInitialCommand(Command value);

  /**
   * Returns the value of the '<em><b>Commands</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61131.xtext.il.instructionList.Command}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Commands</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Commands</em>' containment reference list.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getILLabel_Commands()
   * @model containment="true"
   * @generated
   */
  EList<Command> getCommands();

} // ILLabel
