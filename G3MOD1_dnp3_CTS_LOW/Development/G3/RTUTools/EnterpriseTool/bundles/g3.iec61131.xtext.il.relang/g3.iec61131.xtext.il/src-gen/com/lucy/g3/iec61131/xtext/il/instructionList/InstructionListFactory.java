/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage
 * @generated
 */
public interface InstructionListFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  InstructionListFactory eINSTANCE = com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListFactoryImpl.init();

  /**
   * Returns a new object of class '<em>IL Object</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>IL Object</em>'.
   * @generated
   */
  ILObject createILObject();

  /**
   * Returns a new object of class '<em>Program</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Program</em>'.
   * @generated
   */
  Program createProgram();

  /**
   * Returns a new object of class '<em>Function Block</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Block</em>'.
   * @generated
   */
  FunctionBlock createFunctionBlock();

  /**
   * Returns a new object of class '<em>Function</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function</em>'.
   * @generated
   */
  Function createFunction();

  /**
   * Returns a new object of class '<em>Feature</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Feature</em>'.
   * @generated
   */
  Feature createFeature();

  /**
   * Returns a new object of class '<em>Reference</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Reference</em>'.
   * @generated
   */
  Reference createReference();

  /**
   * Returns a new object of class '<em>RCommand</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>RCommand</em>'.
   * @generated
   */
  RCommand createRCommand();

  /**
   * Returns a new object of class '<em>SCommand</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>SCommand</em>'.
   * @generated
   */
  SCommand createSCommand();

  /**
   * Returns a new object of class '<em>IL Label</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>IL Label</em>'.
   * @generated
   */
  ILLabel createILLabel();

  /**
   * Returns a new object of class '<em>Function Call</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Call</em>'.
   * @generated
   */
  FunctionCall createFunctionCall();

  /**
   * Returns a new object of class '<em>Function Inputs</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Inputs</em>'.
   * @generated
   */
  FunctionInputs createFunctionInputs();

  /**
   * Returns a new object of class '<em>JMPCN Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>JMPCN Command</em>'.
   * @generated
   */
  JMPCNCommand createJMPCNCommand();

  /**
   * Returns a new object of class '<em>JMP Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>JMP Command</em>'.
   * @generated
   */
  JMPCommand createJMPCommand();

  /**
   * Returns a new object of class '<em>EQ Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>EQ Command</em>'.
   * @generated
   */
  EQCommand createEQCommand();

  /**
   * Returns a new object of class '<em>EQ Bool</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>EQ Bool</em>'.
   * @generated
   */
  EQBool createEQBool();

  /**
   * Returns a new object of class '<em>EQN Bool</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>EQN Bool</em>'.
   * @generated
   */
  EQNBool createEQNBool();

  /**
   * Returns a new object of class '<em>EQ Int</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>EQ Int</em>'.
   * @generated
   */
  EQInt createEQInt();

  /**
   * Returns a new object of class '<em>EQN Int</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>EQN Int</em>'.
   * @generated
   */
  EQNInt createEQNInt();

  /**
   * Returns a new object of class '<em>Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Command</em>'.
   * @generated
   */
  Command createCommand();

  /**
   * Returns a new object of class '<em>IL Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>IL Command</em>'.
   * @generated
   */
  ILCommand createILCommand();

  /**
   * Returns a new object of class '<em>Extended Commands</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Extended Commands</em>'.
   * @generated
   */
  ExtendedCommands createExtendedCommands();

  /**
   * Returns a new object of class '<em>Delay Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Delay Command</em>'.
   * @generated
   */
  DelayCommand createDelayCommand();

  /**
   * Returns a new object of class '<em>Parameter</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Parameter</em>'.
   * @generated
   */
  Parameter createParameter();

  /**
   * Returns a new object of class '<em>Resource Paramater</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Resource Paramater</em>'.
   * @generated
   */
  ResourceParamater createResourceParamater();

  /**
   * Returns a new object of class '<em>Int Parameter</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Int Parameter</em>'.
   * @generated
   */
  IntParameter createIntParameter();

  /**
   * Returns a new object of class '<em>Boolean Parameter</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Parameter</em>'.
   * @generated
   */
  BooleanParameter createBooleanParameter();

  /**
   * Returns a new object of class '<em>ST Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>ST Command</em>'.
   * @generated
   */
  STCommand createSTCommand();

  /**
   * Returns a new object of class '<em>LD Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LD Command</em>'.
   * @generated
   */
  LDCommand createLDCommand();

  /**
   * Returns a new object of class '<em>Math Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Math Command</em>'.
   * @generated
   */
  MathCommand createMathCommand();

  /**
   * Returns a new object of class '<em>ADD Operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>ADD Operator</em>'.
   * @generated
   */
  ADDOperator createADDOperator();

  /**
   * Returns a new object of class '<em>SUB Operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>SUB Operator</em>'.
   * @generated
   */
  SUBOperator createSUBOperator();

  /**
   * Returns a new object of class '<em>MUL Operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>MUL Operator</em>'.
   * @generated
   */
  MULOperator createMULOperator();

  /**
   * Returns a new object of class '<em>DIV Operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>DIV Operator</em>'.
   * @generated
   */
  DIVOperator createDIVOperator();

  /**
   * Returns a new object of class '<em>Logic Op Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Logic Op Command</em>'.
   * @generated
   */
  LogicOpCommand createLogicOpCommand();

  /**
   * Returns a new object of class '<em>AND Operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>AND Operator</em>'.
   * @generated
   */
  ANDOperator createANDOperator();

  /**
   * Returns a new object of class '<em>OR Operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>OR Operator</em>'.
   * @generated
   */
  OROperator createOROperator();

  /**
   * Returns a new object of class '<em>XOR Operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>XOR Operator</em>'.
   * @generated
   */
  XOROperator createXOROperator();

  /**
   * Returns a new object of class '<em>Return Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Return Command</em>'.
   * @generated
   */
  ReturnCommand createReturnCommand();

  /**
   * Returns a new object of class '<em>Call Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Call Command</em>'.
   * @generated
   */
  CallCommand createCallCommand();

  /**
   * Returns a new object of class '<em>Command Types</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Command Types</em>'.
   * @generated
   */
  CommandTypes createCommandTypes();

  /**
   * Returns a new object of class '<em>Init Data</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Init Data</em>'.
   * @generated
   */
  InitData createInitData();

  /**
   * Returns a new object of class '<em>Bool Data</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bool Data</em>'.
   * @generated
   */
  BoolData createBoolData();

  /**
   * Returns a new object of class '<em>Time Data</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Time Data</em>'.
   * @generated
   */
  TimeData createTimeData();

  /**
   * Returns a new object of class '<em>AVar</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>AVar</em>'.
   * @generated
   */
  AVar createAVar();

  /**
   * Returns a new object of class '<em>Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type</em>'.
   * @generated
   */
  Type createType();

  /**
   * Returns a new object of class '<em>Vars</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Vars</em>'.
   * @generated
   */
  Vars createVars();

  /**
   * Returns a new object of class '<em>Input Vars</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Input Vars</em>'.
   * @generated
   */
  InputVars createInputVars();

  /**
   * Returns a new object of class '<em>Output Vars</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Output Vars</em>'.
   * @generated
   */
  OutputVars createOutputVars();

  /**
   * Returns a new object of class '<em>Resource Var</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Resource Var</em>'.
   * @generated
   */
  ResourceVar createResourceVar();

  /**
   * Returns a new object of class '<em>Resource Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Resource Type</em>'.
   * @generated
   */
  ResourceType createResourceType();

  /**
   * Returns a new object of class '<em>Int Resource Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Int Resource Type</em>'.
   * @generated
   */
  IntResourceType createIntResourceType();

  /**
   * Returns a new object of class '<em>Boolean Resource Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Resource Type</em>'.
   * @generated
   */
  BooleanResourceType createBooleanResourceType();

  /**
   * Returns a new object of class '<em>Custom Var</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Custom Var</em>'.
   * @generated
   */
  CustomVar createCustomVar();

  /**
   * Returns a new object of class '<em>Int Var</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Int Var</em>'.
   * @generated
   */
  IntVar createIntVar();

  /**
   * Returns a new object of class '<em>Bool Var</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bool Var</em>'.
   * @generated
   */
  BoolVar createBoolVar();

  /**
   * Returns a new object of class '<em>Timer Var</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Timer Var</em>'.
   * @generated
   */
  TimerVar createTimerVar();

  /**
   * Returns a new object of class '<em>Timer Call Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Timer Call Command</em>'.
   * @generated
   */
  TimerCallCommand createTimerCallCommand();

  /**
   * Returns a new object of class '<em>Resource</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Resource</em>'.
   * @generated
   */
  Resource createResource();

  /**
   * Returns a new object of class '<em>Input Bit Resource</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Input Bit Resource</em>'.
   * @generated
   */
  InputBitResource createInputBitResource();

  /**
   * Returns a new object of class '<em>Output Bit Resource</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Output Bit Resource</em>'.
   * @generated
   */
  OutputBitResource createOutputBitResource();

  /**
   * Returns a new object of class '<em>Flag Byte Resource</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Flag Byte Resource</em>'.
   * @generated
   */
  FlagByteResource createFlagByteResource();

  /**
   * Returns a new object of class '<em>Object Reference</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Object Reference</em>'.
   * @generated
   */
  ObjectReference createObjectReference();

  /**
   * Returns a new object of class '<em>Dot Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Dot Expression</em>'.
   * @generated
   */
  DotExpression createDotExpression();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  InstructionListPackage getInstructionListPackage();

} //InstructionListFactory
