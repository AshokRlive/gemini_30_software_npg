/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.impl;

import com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage;
import com.lucy.g3.iec61131.xtext.il.instructionList.IntResourceType;
import com.lucy.g3.iec61131.xtext.il.instructionList.IntVar;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Int Var</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.IntVarImpl#getIntType <em>Int Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IntVarImpl extends AVarImpl implements IntVar
{
  /**
   * The cached value of the '{@link #getIntType() <em>Int Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIntType()
   * @generated
   * @ordered
   */
  protected IntResourceType intType;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IntVarImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return InstructionListPackage.Literals.INT_VAR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntResourceType getIntType()
  {
    return intType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIntType(IntResourceType newIntType, NotificationChain msgs)
  {
    IntResourceType oldIntType = intType;
    intType = newIntType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InstructionListPackage.INT_VAR__INT_TYPE, oldIntType, newIntType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIntType(IntResourceType newIntType)
  {
    if (newIntType != intType)
    {
      NotificationChain msgs = null;
      if (intType != null)
        msgs = ((InternalEObject)intType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InstructionListPackage.INT_VAR__INT_TYPE, null, msgs);
      if (newIntType != null)
        msgs = ((InternalEObject)newIntType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - InstructionListPackage.INT_VAR__INT_TYPE, null, msgs);
      msgs = basicSetIntType(newIntType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, InstructionListPackage.INT_VAR__INT_TYPE, newIntType, newIntType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case InstructionListPackage.INT_VAR__INT_TYPE:
        return basicSetIntType(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case InstructionListPackage.INT_VAR__INT_TYPE:
        return getIntType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case InstructionListPackage.INT_VAR__INT_TYPE:
        setIntType((IntResourceType)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case InstructionListPackage.INT_VAR__INT_TYPE:
        setIntType((IntResourceType)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case InstructionListPackage.INT_VAR__INT_TYPE:
        return intType != null;
    }
    return super.eIsSet(featureID);
  }

} //IntVarImpl
