/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.util;

import com.lucy.g3.iec61131.xtext.il.instructionList.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage
 * @generated
 */
public class InstructionListAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static InstructionListPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InstructionListAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = InstructionListPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InstructionListSwitch<Adapter> modelSwitch =
    new InstructionListSwitch<Adapter>()
    {
      @Override
      public Adapter caseILObject(ILObject object)
      {
        return createILObjectAdapter();
      }
      @Override
      public Adapter caseProgram(Program object)
      {
        return createProgramAdapter();
      }
      @Override
      public Adapter caseFunctionBlock(FunctionBlock object)
      {
        return createFunctionBlockAdapter();
      }
      @Override
      public Adapter caseFunction(Function object)
      {
        return createFunctionAdapter();
      }
      @Override
      public Adapter caseFeature(Feature object)
      {
        return createFeatureAdapter();
      }
      @Override
      public Adapter caseReference(Reference object)
      {
        return createReferenceAdapter();
      }
      @Override
      public Adapter caseRCommand(RCommand object)
      {
        return createRCommandAdapter();
      }
      @Override
      public Adapter caseSCommand(SCommand object)
      {
        return createSCommandAdapter();
      }
      @Override
      public Adapter caseILLabel(ILLabel object)
      {
        return createILLabelAdapter();
      }
      @Override
      public Adapter caseFunctionCall(FunctionCall object)
      {
        return createFunctionCallAdapter();
      }
      @Override
      public Adapter caseFunctionInputs(FunctionInputs object)
      {
        return createFunctionInputsAdapter();
      }
      @Override
      public Adapter caseJMPCNCommand(JMPCNCommand object)
      {
        return createJMPCNCommandAdapter();
      }
      @Override
      public Adapter caseJMPCommand(JMPCommand object)
      {
        return createJMPCommandAdapter();
      }
      @Override
      public Adapter caseEQCommand(EQCommand object)
      {
        return createEQCommandAdapter();
      }
      @Override
      public Adapter caseEQBool(EQBool object)
      {
        return createEQBoolAdapter();
      }
      @Override
      public Adapter caseEQNBool(EQNBool object)
      {
        return createEQNBoolAdapter();
      }
      @Override
      public Adapter caseEQInt(EQInt object)
      {
        return createEQIntAdapter();
      }
      @Override
      public Adapter caseEQNInt(EQNInt object)
      {
        return createEQNIntAdapter();
      }
      @Override
      public Adapter caseCommand(Command object)
      {
        return createCommandAdapter();
      }
      @Override
      public Adapter caseILCommand(ILCommand object)
      {
        return createILCommandAdapter();
      }
      @Override
      public Adapter caseExtendedCommands(ExtendedCommands object)
      {
        return createExtendedCommandsAdapter();
      }
      @Override
      public Adapter caseDelayCommand(DelayCommand object)
      {
        return createDelayCommandAdapter();
      }
      @Override
      public Adapter caseParameter(Parameter object)
      {
        return createParameterAdapter();
      }
      @Override
      public Adapter caseResourceParamater(ResourceParamater object)
      {
        return createResourceParamaterAdapter();
      }
      @Override
      public Adapter caseIntParameter(IntParameter object)
      {
        return createIntParameterAdapter();
      }
      @Override
      public Adapter caseBooleanParameter(BooleanParameter object)
      {
        return createBooleanParameterAdapter();
      }
      @Override
      public Adapter caseSTCommand(STCommand object)
      {
        return createSTCommandAdapter();
      }
      @Override
      public Adapter caseLDCommand(LDCommand object)
      {
        return createLDCommandAdapter();
      }
      @Override
      public Adapter caseMathCommand(MathCommand object)
      {
        return createMathCommandAdapter();
      }
      @Override
      public Adapter caseADDOperator(ADDOperator object)
      {
        return createADDOperatorAdapter();
      }
      @Override
      public Adapter caseSUBOperator(SUBOperator object)
      {
        return createSUBOperatorAdapter();
      }
      @Override
      public Adapter caseMULOperator(MULOperator object)
      {
        return createMULOperatorAdapter();
      }
      @Override
      public Adapter caseDIVOperator(DIVOperator object)
      {
        return createDIVOperatorAdapter();
      }
      @Override
      public Adapter caseLogicOpCommand(LogicOpCommand object)
      {
        return createLogicOpCommandAdapter();
      }
      @Override
      public Adapter caseANDOperator(ANDOperator object)
      {
        return createANDOperatorAdapter();
      }
      @Override
      public Adapter caseOROperator(OROperator object)
      {
        return createOROperatorAdapter();
      }
      @Override
      public Adapter caseXOROperator(XOROperator object)
      {
        return createXOROperatorAdapter();
      }
      @Override
      public Adapter caseReturnCommand(ReturnCommand object)
      {
        return createReturnCommandAdapter();
      }
      @Override
      public Adapter caseCallCommand(CallCommand object)
      {
        return createCallCommandAdapter();
      }
      @Override
      public Adapter caseCommandTypes(CommandTypes object)
      {
        return createCommandTypesAdapter();
      }
      @Override
      public Adapter caseInitData(InitData object)
      {
        return createInitDataAdapter();
      }
      @Override
      public Adapter caseBoolData(BoolData object)
      {
        return createBoolDataAdapter();
      }
      @Override
      public Adapter caseTimeData(TimeData object)
      {
        return createTimeDataAdapter();
      }
      @Override
      public Adapter caseAVar(AVar object)
      {
        return createAVarAdapter();
      }
      @Override
      public Adapter caseType(Type object)
      {
        return createTypeAdapter();
      }
      @Override
      public Adapter caseVars(Vars object)
      {
        return createVarsAdapter();
      }
      @Override
      public Adapter caseInputVars(InputVars object)
      {
        return createInputVarsAdapter();
      }
      @Override
      public Adapter caseOutputVars(OutputVars object)
      {
        return createOutputVarsAdapter();
      }
      @Override
      public Adapter caseResourceVar(ResourceVar object)
      {
        return createResourceVarAdapter();
      }
      @Override
      public Adapter caseResourceType(ResourceType object)
      {
        return createResourceTypeAdapter();
      }
      @Override
      public Adapter caseIntResourceType(IntResourceType object)
      {
        return createIntResourceTypeAdapter();
      }
      @Override
      public Adapter caseBooleanResourceType(BooleanResourceType object)
      {
        return createBooleanResourceTypeAdapter();
      }
      @Override
      public Adapter caseCustomVar(CustomVar object)
      {
        return createCustomVarAdapter();
      }
      @Override
      public Adapter caseIntVar(IntVar object)
      {
        return createIntVarAdapter();
      }
      @Override
      public Adapter caseBoolVar(BoolVar object)
      {
        return createBoolVarAdapter();
      }
      @Override
      public Adapter caseTimerVar(TimerVar object)
      {
        return createTimerVarAdapter();
      }
      @Override
      public Adapter caseTimerCallCommand(TimerCallCommand object)
      {
        return createTimerCallCommandAdapter();
      }
      @Override
      public Adapter caseResource(Resource object)
      {
        return createResourceAdapter();
      }
      @Override
      public Adapter caseInputBitResource(InputBitResource object)
      {
        return createInputBitResourceAdapter();
      }
      @Override
      public Adapter caseOutputBitResource(OutputBitResource object)
      {
        return createOutputBitResourceAdapter();
      }
      @Override
      public Adapter caseFlagByteResource(FlagByteResource object)
      {
        return createFlagByteResourceAdapter();
      }
      @Override
      public Adapter caseObjectReference(ObjectReference object)
      {
        return createObjectReferenceAdapter();
      }
      @Override
      public Adapter caseDotExpression(DotExpression object)
      {
        return createDotExpressionAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILObject <em>IL Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ILObject
   * @generated
   */
  public Adapter createILObjectAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Program <em>Program</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Program
   * @generated
   */
  public Adapter createProgramAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock <em>Function Block</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock
   * @generated
   */
  public Adapter createFunctionBlockAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Function <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Function
   * @generated
   */
  public Adapter createFunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Feature <em>Feature</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Feature
   * @generated
   */
  public Adapter createFeatureAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Reference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Reference
   * @generated
   */
  public Adapter createReferenceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.RCommand <em>RCommand</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.RCommand
   * @generated
   */
  public Adapter createRCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.SCommand <em>SCommand</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.SCommand
   * @generated
   */
  public Adapter createSCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel <em>IL Label</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel
   * @generated
   */
  public Adapter createILLabelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionCall <em>Function Call</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionCall
   * @generated
   */
  public Adapter createFunctionCallAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionInputs <em>Function Inputs</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionInputs
   * @generated
   */
  public Adapter createFunctionInputsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.JMPCNCommand <em>JMPCN Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.JMPCNCommand
   * @generated
   */
  public Adapter createJMPCNCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.JMPCommand <em>JMP Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.JMPCommand
   * @generated
   */
  public Adapter createJMPCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.EQCommand <em>EQ Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.EQCommand
   * @generated
   */
  public Adapter createEQCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.EQBool <em>EQ Bool</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.EQBool
   * @generated
   */
  public Adapter createEQBoolAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.EQNBool <em>EQN Bool</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.EQNBool
   * @generated
   */
  public Adapter createEQNBoolAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.EQInt <em>EQ Int</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.EQInt
   * @generated
   */
  public Adapter createEQIntAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.EQNInt <em>EQN Int</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.EQNInt
   * @generated
   */
  public Adapter createEQNIntAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Command <em>Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Command
   * @generated
   */
  public Adapter createCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILCommand <em>IL Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ILCommand
   * @generated
   */
  public Adapter createILCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ExtendedCommands <em>Extended Commands</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ExtendedCommands
   * @generated
   */
  public Adapter createExtendedCommandsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.DelayCommand <em>Delay Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.DelayCommand
   * @generated
   */
  public Adapter createDelayCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Parameter <em>Parameter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Parameter
   * @generated
   */
  public Adapter createParameterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ResourceParamater <em>Resource Paramater</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ResourceParamater
   * @generated
   */
  public Adapter createResourceParamaterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.IntParameter <em>Int Parameter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.IntParameter
   * @generated
   */
  public Adapter createIntParameterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.BooleanParameter <em>Boolean Parameter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.BooleanParameter
   * @generated
   */
  public Adapter createBooleanParameterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.STCommand <em>ST Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.STCommand
   * @generated
   */
  public Adapter createSTCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.LDCommand <em>LD Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.LDCommand
   * @generated
   */
  public Adapter createLDCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.MathCommand <em>Math Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.MathCommand
   * @generated
   */
  public Adapter createMathCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ADDOperator <em>ADD Operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ADDOperator
   * @generated
   */
  public Adapter createADDOperatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.SUBOperator <em>SUB Operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.SUBOperator
   * @generated
   */
  public Adapter createSUBOperatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.MULOperator <em>MUL Operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.MULOperator
   * @generated
   */
  public Adapter createMULOperatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.DIVOperator <em>DIV Operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.DIVOperator
   * @generated
   */
  public Adapter createDIVOperatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.LogicOpCommand <em>Logic Op Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.LogicOpCommand
   * @generated
   */
  public Adapter createLogicOpCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ANDOperator <em>AND Operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ANDOperator
   * @generated
   */
  public Adapter createANDOperatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.OROperator <em>OR Operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.OROperator
   * @generated
   */
  public Adapter createOROperatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.XOROperator <em>XOR Operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.XOROperator
   * @generated
   */
  public Adapter createXOROperatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ReturnCommand <em>Return Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ReturnCommand
   * @generated
   */
  public Adapter createReturnCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.CallCommand <em>Call Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.CallCommand
   * @generated
   */
  public Adapter createCallCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.CommandTypes <em>Command Types</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.CommandTypes
   * @generated
   */
  public Adapter createCommandTypesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.InitData <em>Init Data</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InitData
   * @generated
   */
  public Adapter createInitDataAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.BoolData <em>Bool Data</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.BoolData
   * @generated
   */
  public Adapter createBoolDataAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimeData <em>Time Data</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.TimeData
   * @generated
   */
  public Adapter createTimeDataAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.AVar <em>AVar</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.AVar
   * @generated
   */
  public Adapter createAVarAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Type
   * @generated
   */
  public Adapter createTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Vars <em>Vars</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Vars
   * @generated
   */
  public Adapter createVarsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.InputVars <em>Input Vars</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InputVars
   * @generated
   */
  public Adapter createInputVarsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.OutputVars <em>Output Vars</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.OutputVars
   * @generated
   */
  public Adapter createOutputVarsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ResourceVar <em>Resource Var</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ResourceVar
   * @generated
   */
  public Adapter createResourceVarAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ResourceType <em>Resource Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ResourceType
   * @generated
   */
  public Adapter createResourceTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.IntResourceType <em>Int Resource Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.IntResourceType
   * @generated
   */
  public Adapter createIntResourceTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.BooleanResourceType <em>Boolean Resource Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.BooleanResourceType
   * @generated
   */
  public Adapter createBooleanResourceTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.CustomVar <em>Custom Var</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.CustomVar
   * @generated
   */
  public Adapter createCustomVarAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.IntVar <em>Int Var</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.IntVar
   * @generated
   */
  public Adapter createIntVarAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.BoolVar <em>Bool Var</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.BoolVar
   * @generated
   */
  public Adapter createBoolVarAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerVar <em>Timer Var</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.TimerVar
   * @generated
   */
  public Adapter createTimerVarAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand <em>Timer Call Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand
   * @generated
   */
  public Adapter createTimerCallCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Resource <em>Resource</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Resource
   * @generated
   */
  public Adapter createResourceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.InputBitResource <em>Input Bit Resource</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InputBitResource
   * @generated
   */
  public Adapter createInputBitResourceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.OutputBitResource <em>Output Bit Resource</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.OutputBitResource
   * @generated
   */
  public Adapter createOutputBitResourceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FlagByteResource <em>Flag Byte Resource</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FlagByteResource
   * @generated
   */
  public Adapter createFlagByteResourceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ObjectReference <em>Object Reference</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ObjectReference
   * @generated
   */
  public Adapter createObjectReferenceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.DotExpression <em>Dot Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.DotExpression
   * @generated
   */
  public Adapter createDotExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //InstructionListAdapterFactory
