/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Custom Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.CustomVar#getType <em>Type</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.CustomVar#getInitializer <em>Initializer</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getCustomVar()
 * @model
 * @generated
 */
public interface CustomVar extends AVar
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getCustomVar_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.CustomVar#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

  /**
   * Returns the value of the '<em><b>Initializer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Initializer</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Initializer</em>' containment reference.
   * @see #setInitializer(InitData)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getCustomVar_Initializer()
   * @model containment="true"
   * @generated
   */
  InitData getInitializer();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.CustomVar#getInitializer <em>Initializer</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Initializer</em>' containment reference.
   * @see #getInitializer()
   * @generated
   */
  void setInitializer(InitData value);

} // CustomVar
