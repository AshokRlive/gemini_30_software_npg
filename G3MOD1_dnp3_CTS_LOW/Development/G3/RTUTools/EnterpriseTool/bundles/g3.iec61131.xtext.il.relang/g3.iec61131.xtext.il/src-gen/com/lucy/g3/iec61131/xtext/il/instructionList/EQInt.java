/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EQ Int</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getEQInt()
 * @model
 * @generated
 */
public interface EQInt extends EQCommand
{
} // EQInt
