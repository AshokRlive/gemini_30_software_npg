/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.impl;

import com.lucy.g3.iec61131.xtext.il.instructionList.BoolVar;
import com.lucy.g3.iec61131.xtext.il.instructionList.BooleanResourceType;
import com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bool Var</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.BoolVarImpl#getBoolType <em>Bool Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BoolVarImpl extends AVarImpl implements BoolVar
{
  /**
   * The cached value of the '{@link #getBoolType() <em>Bool Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBoolType()
   * @generated
   * @ordered
   */
  protected BooleanResourceType boolType;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BoolVarImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return InstructionListPackage.Literals.BOOL_VAR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanResourceType getBoolType()
  {
    return boolType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBoolType(BooleanResourceType newBoolType, NotificationChain msgs)
  {
    BooleanResourceType oldBoolType = boolType;
    boolType = newBoolType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InstructionListPackage.BOOL_VAR__BOOL_TYPE, oldBoolType, newBoolType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBoolType(BooleanResourceType newBoolType)
  {
    if (newBoolType != boolType)
    {
      NotificationChain msgs = null;
      if (boolType != null)
        msgs = ((InternalEObject)boolType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InstructionListPackage.BOOL_VAR__BOOL_TYPE, null, msgs);
      if (newBoolType != null)
        msgs = ((InternalEObject)newBoolType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - InstructionListPackage.BOOL_VAR__BOOL_TYPE, null, msgs);
      msgs = basicSetBoolType(newBoolType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, InstructionListPackage.BOOL_VAR__BOOL_TYPE, newBoolType, newBoolType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case InstructionListPackage.BOOL_VAR__BOOL_TYPE:
        return basicSetBoolType(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case InstructionListPackage.BOOL_VAR__BOOL_TYPE:
        return getBoolType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case InstructionListPackage.BOOL_VAR__BOOL_TYPE:
        setBoolType((BooleanResourceType)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case InstructionListPackage.BOOL_VAR__BOOL_TYPE:
        setBoolType((BooleanResourceType)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case InstructionListPackage.BOOL_VAR__BOOL_TYPE:
        return boolType != null;
    }
    return super.eIsSet(featureID);
  }

} //BoolVarImpl
