/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.impl;

import com.lucy.g3.iec61131.xtext.il.instructionList.Function;
import com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock;
import com.lucy.g3.iec61131.xtext.il.instructionList.ILObject;
import com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage;
import com.lucy.g3.iec61131.xtext.il.instructionList.Program;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IL Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILObjectImpl#getMain <em>Main</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILObjectImpl#getFunctions <em>Functions</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILObjectImpl#getFunctionblocks <em>Functionblocks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ILObjectImpl extends MinimalEObjectImpl.Container implements ILObject
{
  /**
   * The cached value of the '{@link #getMain() <em>Main</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMain()
   * @generated
   * @ordered
   */
  protected Program main;

  /**
   * The cached value of the '{@link #getFunctions() <em>Functions</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunctions()
   * @generated
   * @ordered
   */
  protected EList<Function> functions;

  /**
   * The cached value of the '{@link #getFunctionblocks() <em>Functionblocks</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunctionblocks()
   * @generated
   * @ordered
   */
  protected EList<FunctionBlock> functionblocks;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ILObjectImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return InstructionListPackage.Literals.IL_OBJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Program getMain()
  {
    return main;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMain(Program newMain, NotificationChain msgs)
  {
    Program oldMain = main;
    main = newMain;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InstructionListPackage.IL_OBJECT__MAIN, oldMain, newMain);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMain(Program newMain)
  {
    if (newMain != main)
    {
      NotificationChain msgs = null;
      if (main != null)
        msgs = ((InternalEObject)main).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InstructionListPackage.IL_OBJECT__MAIN, null, msgs);
      if (newMain != null)
        msgs = ((InternalEObject)newMain).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - InstructionListPackage.IL_OBJECT__MAIN, null, msgs);
      msgs = basicSetMain(newMain, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, InstructionListPackage.IL_OBJECT__MAIN, newMain, newMain));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Function> getFunctions()
  {
    if (functions == null)
    {
      functions = new EObjectContainmentEList<Function>(Function.class, this, InstructionListPackage.IL_OBJECT__FUNCTIONS);
    }
    return functions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FunctionBlock> getFunctionblocks()
  {
    if (functionblocks == null)
    {
      functionblocks = new EObjectContainmentEList<FunctionBlock>(FunctionBlock.class, this, InstructionListPackage.IL_OBJECT__FUNCTIONBLOCKS);
    }
    return functionblocks;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case InstructionListPackage.IL_OBJECT__MAIN:
        return basicSetMain(null, msgs);
      case InstructionListPackage.IL_OBJECT__FUNCTIONS:
        return ((InternalEList<?>)getFunctions()).basicRemove(otherEnd, msgs);
      case InstructionListPackage.IL_OBJECT__FUNCTIONBLOCKS:
        return ((InternalEList<?>)getFunctionblocks()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case InstructionListPackage.IL_OBJECT__MAIN:
        return getMain();
      case InstructionListPackage.IL_OBJECT__FUNCTIONS:
        return getFunctions();
      case InstructionListPackage.IL_OBJECT__FUNCTIONBLOCKS:
        return getFunctionblocks();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case InstructionListPackage.IL_OBJECT__MAIN:
        setMain((Program)newValue);
        return;
      case InstructionListPackage.IL_OBJECT__FUNCTIONS:
        getFunctions().clear();
        getFunctions().addAll((Collection<? extends Function>)newValue);
        return;
      case InstructionListPackage.IL_OBJECT__FUNCTIONBLOCKS:
        getFunctionblocks().clear();
        getFunctionblocks().addAll((Collection<? extends FunctionBlock>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case InstructionListPackage.IL_OBJECT__MAIN:
        setMain((Program)null);
        return;
      case InstructionListPackage.IL_OBJECT__FUNCTIONS:
        getFunctions().clear();
        return;
      case InstructionListPackage.IL_OBJECT__FUNCTIONBLOCKS:
        getFunctionblocks().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case InstructionListPackage.IL_OBJECT__MAIN:
        return main != null;
      case InstructionListPackage.IL_OBJECT__FUNCTIONS:
        return functions != null && !functions.isEmpty();
      case InstructionListPackage.IL_OBJECT__FUNCTIONBLOCKS:
        return functionblocks != null && !functionblocks.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ILObjectImpl
