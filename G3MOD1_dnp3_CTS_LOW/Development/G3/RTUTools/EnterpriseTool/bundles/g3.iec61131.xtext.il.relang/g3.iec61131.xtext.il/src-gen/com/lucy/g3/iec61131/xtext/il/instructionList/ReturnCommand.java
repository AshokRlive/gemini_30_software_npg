/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Return Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.ReturnCommand#getRet <em>Ret</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getReturnCommand()
 * @model
 * @generated
 */
public interface ReturnCommand extends ILCommand
{
  /**
   * Returns the value of the '<em><b>Ret</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ret</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ret</em>' attribute.
   * @see #setRet(String)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getReturnCommand_Ret()
   * @model
   * @generated
   */
  String getRet();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ReturnCommand#getRet <em>Ret</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ret</em>' attribute.
   * @see #getRet()
   * @generated
   */
  void setRet(String value);

} // ReturnCommand
