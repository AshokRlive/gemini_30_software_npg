/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionCall#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionCall#getArguments <em>Arguments</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getFunctionCall()
 * @model
 * @generated
 */
public interface FunctionCall extends ILCommand
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getFunctionCall_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionCall#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Arguments</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Arguments</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arguments</em>' attribute list.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getFunctionCall_Arguments()
   * @model unique="false"
   * @generated
   */
  EList<String> getArguments();

} // FunctionCall
