/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.impl;

import com.lucy.g3.iec61131.xtext.il.instructionList.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InstructionListFactoryImpl extends EFactoryImpl implements InstructionListFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static InstructionListFactory init()
  {
    try
    {
      InstructionListFactory theInstructionListFactory = (InstructionListFactory)EPackage.Registry.INSTANCE.getEFactory(InstructionListPackage.eNS_URI);
      if (theInstructionListFactory != null)
      {
        return theInstructionListFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new InstructionListFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InstructionListFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case InstructionListPackage.IL_OBJECT: return createILObject();
      case InstructionListPackage.PROGRAM: return createProgram();
      case InstructionListPackage.FUNCTION_BLOCK: return createFunctionBlock();
      case InstructionListPackage.FUNCTION: return createFunction();
      case InstructionListPackage.FEATURE: return createFeature();
      case InstructionListPackage.REFERENCE: return createReference();
      case InstructionListPackage.RCOMMAND: return createRCommand();
      case InstructionListPackage.SCOMMAND: return createSCommand();
      case InstructionListPackage.IL_LABEL: return createILLabel();
      case InstructionListPackage.FUNCTION_CALL: return createFunctionCall();
      case InstructionListPackage.FUNCTION_INPUTS: return createFunctionInputs();
      case InstructionListPackage.JMPCN_COMMAND: return createJMPCNCommand();
      case InstructionListPackage.JMP_COMMAND: return createJMPCommand();
      case InstructionListPackage.EQ_COMMAND: return createEQCommand();
      case InstructionListPackage.EQ_BOOL: return createEQBool();
      case InstructionListPackage.EQN_BOOL: return createEQNBool();
      case InstructionListPackage.EQ_INT: return createEQInt();
      case InstructionListPackage.EQN_INT: return createEQNInt();
      case InstructionListPackage.COMMAND: return createCommand();
      case InstructionListPackage.IL_COMMAND: return createILCommand();
      case InstructionListPackage.EXTENDED_COMMANDS: return createExtendedCommands();
      case InstructionListPackage.DELAY_COMMAND: return createDelayCommand();
      case InstructionListPackage.PARAMETER: return createParameter();
      case InstructionListPackage.RESOURCE_PARAMATER: return createResourceParamater();
      case InstructionListPackage.INT_PARAMETER: return createIntParameter();
      case InstructionListPackage.BOOLEAN_PARAMETER: return createBooleanParameter();
      case InstructionListPackage.ST_COMMAND: return createSTCommand();
      case InstructionListPackage.LD_COMMAND: return createLDCommand();
      case InstructionListPackage.MATH_COMMAND: return createMathCommand();
      case InstructionListPackage.ADD_OPERATOR: return createADDOperator();
      case InstructionListPackage.SUB_OPERATOR: return createSUBOperator();
      case InstructionListPackage.MUL_OPERATOR: return createMULOperator();
      case InstructionListPackage.DIV_OPERATOR: return createDIVOperator();
      case InstructionListPackage.LOGIC_OP_COMMAND: return createLogicOpCommand();
      case InstructionListPackage.AND_OPERATOR: return createANDOperator();
      case InstructionListPackage.OR_OPERATOR: return createOROperator();
      case InstructionListPackage.XOR_OPERATOR: return createXOROperator();
      case InstructionListPackage.RETURN_COMMAND: return createReturnCommand();
      case InstructionListPackage.CALL_COMMAND: return createCallCommand();
      case InstructionListPackage.COMMAND_TYPES: return createCommandTypes();
      case InstructionListPackage.INIT_DATA: return createInitData();
      case InstructionListPackage.BOOL_DATA: return createBoolData();
      case InstructionListPackage.TIME_DATA: return createTimeData();
      case InstructionListPackage.AVAR: return createAVar();
      case InstructionListPackage.TYPE: return createType();
      case InstructionListPackage.VARS: return createVars();
      case InstructionListPackage.INPUT_VARS: return createInputVars();
      case InstructionListPackage.OUTPUT_VARS: return createOutputVars();
      case InstructionListPackage.RESOURCE_VAR: return createResourceVar();
      case InstructionListPackage.RESOURCE_TYPE: return createResourceType();
      case InstructionListPackage.INT_RESOURCE_TYPE: return createIntResourceType();
      case InstructionListPackage.BOOLEAN_RESOURCE_TYPE: return createBooleanResourceType();
      case InstructionListPackage.CUSTOM_VAR: return createCustomVar();
      case InstructionListPackage.INT_VAR: return createIntVar();
      case InstructionListPackage.BOOL_VAR: return createBoolVar();
      case InstructionListPackage.TIMER_VAR: return createTimerVar();
      case InstructionListPackage.TIMER_CALL_COMMAND: return createTimerCallCommand();
      case InstructionListPackage.RESOURCE: return createResource();
      case InstructionListPackage.INPUT_BIT_RESOURCE: return createInputBitResource();
      case InstructionListPackage.OUTPUT_BIT_RESOURCE: return createOutputBitResource();
      case InstructionListPackage.FLAG_BYTE_RESOURCE: return createFlagByteResource();
      case InstructionListPackage.OBJECT_REFERENCE: return createObjectReference();
      case InstructionListPackage.DOT_EXPRESSION: return createDotExpression();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue)
  {
    switch (eDataType.getClassifierID())
    {
      case InstructionListPackage.SIMPLE_TYPE_ENUM:
        return createSimpleTypeEnumFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
    switch (eDataType.getClassifierID())
    {
      case InstructionListPackage.SIMPLE_TYPE_ENUM:
        return convertSimpleTypeEnumToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ILObject createILObject()
  {
    ILObjectImpl ilObject = new ILObjectImpl();
    return ilObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Program createProgram()
  {
    ProgramImpl program = new ProgramImpl();
    return program;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionBlock createFunctionBlock()
  {
    FunctionBlockImpl functionBlock = new FunctionBlockImpl();
    return functionBlock;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Function createFunction()
  {
    FunctionImpl function = new FunctionImpl();
    return function;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Feature createFeature()
  {
    FeatureImpl feature = new FeatureImpl();
    return feature;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Reference createReference()
  {
    ReferenceImpl reference = new ReferenceImpl();
    return reference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RCommand createRCommand()
  {
    RCommandImpl rCommand = new RCommandImpl();
    return rCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SCommand createSCommand()
  {
    SCommandImpl sCommand = new SCommandImpl();
    return sCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ILLabel createILLabel()
  {
    ILLabelImpl ilLabel = new ILLabelImpl();
    return ilLabel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionCall createFunctionCall()
  {
    FunctionCallImpl functionCall = new FunctionCallImpl();
    return functionCall;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionInputs createFunctionInputs()
  {
    FunctionInputsImpl functionInputs = new FunctionInputsImpl();
    return functionInputs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JMPCNCommand createJMPCNCommand()
  {
    JMPCNCommandImpl jmpcnCommand = new JMPCNCommandImpl();
    return jmpcnCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JMPCommand createJMPCommand()
  {
    JMPCommandImpl jmpCommand = new JMPCommandImpl();
    return jmpCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EQCommand createEQCommand()
  {
    EQCommandImpl eqCommand = new EQCommandImpl();
    return eqCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EQBool createEQBool()
  {
    EQBoolImpl eqBool = new EQBoolImpl();
    return eqBool;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EQNBool createEQNBool()
  {
    EQNBoolImpl eqnBool = new EQNBoolImpl();
    return eqnBool;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EQInt createEQInt()
  {
    EQIntImpl eqInt = new EQIntImpl();
    return eqInt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EQNInt createEQNInt()
  {
    EQNIntImpl eqnInt = new EQNIntImpl();
    return eqnInt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Command createCommand()
  {
    CommandImpl command = new CommandImpl();
    return command;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ILCommand createILCommand()
  {
    ILCommandImpl ilCommand = new ILCommandImpl();
    return ilCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExtendedCommands createExtendedCommands()
  {
    ExtendedCommandsImpl extendedCommands = new ExtendedCommandsImpl();
    return extendedCommands;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DelayCommand createDelayCommand()
  {
    DelayCommandImpl delayCommand = new DelayCommandImpl();
    return delayCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Parameter createParameter()
  {
    ParameterImpl parameter = new ParameterImpl();
    return parameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ResourceParamater createResourceParamater()
  {
    ResourceParamaterImpl resourceParamater = new ResourceParamaterImpl();
    return resourceParamater;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntParameter createIntParameter()
  {
    IntParameterImpl intParameter = new IntParameterImpl();
    return intParameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanParameter createBooleanParameter()
  {
    BooleanParameterImpl booleanParameter = new BooleanParameterImpl();
    return booleanParameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public STCommand createSTCommand()
  {
    STCommandImpl stCommand = new STCommandImpl();
    return stCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LDCommand createLDCommand()
  {
    LDCommandImpl ldCommand = new LDCommandImpl();
    return ldCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MathCommand createMathCommand()
  {
    MathCommandImpl mathCommand = new MathCommandImpl();
    return mathCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ADDOperator createADDOperator()
  {
    ADDOperatorImpl addOperator = new ADDOperatorImpl();
    return addOperator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SUBOperator createSUBOperator()
  {
    SUBOperatorImpl subOperator = new SUBOperatorImpl();
    return subOperator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MULOperator createMULOperator()
  {
    MULOperatorImpl mulOperator = new MULOperatorImpl();
    return mulOperator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DIVOperator createDIVOperator()
  {
    DIVOperatorImpl divOperator = new DIVOperatorImpl();
    return divOperator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LogicOpCommand createLogicOpCommand()
  {
    LogicOpCommandImpl logicOpCommand = new LogicOpCommandImpl();
    return logicOpCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ANDOperator createANDOperator()
  {
    ANDOperatorImpl andOperator = new ANDOperatorImpl();
    return andOperator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OROperator createOROperator()
  {
    OROperatorImpl orOperator = new OROperatorImpl();
    return orOperator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public XOROperator createXOROperator()
  {
    XOROperatorImpl xorOperator = new XOROperatorImpl();
    return xorOperator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReturnCommand createReturnCommand()
  {
    ReturnCommandImpl returnCommand = new ReturnCommandImpl();
    return returnCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallCommand createCallCommand()
  {
    CallCommandImpl callCommand = new CallCommandImpl();
    return callCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandTypes createCommandTypes()
  {
    CommandTypesImpl commandTypes = new CommandTypesImpl();
    return commandTypes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InitData createInitData()
  {
    InitDataImpl initData = new InitDataImpl();
    return initData;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolData createBoolData()
  {
    BoolDataImpl boolData = new BoolDataImpl();
    return boolData;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimeData createTimeData()
  {
    TimeDataImpl timeData = new TimeDataImpl();
    return timeData;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AVar createAVar()
  {
    AVarImpl aVar = new AVarImpl();
    return aVar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type createType()
  {
    TypeImpl type = new TypeImpl();
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Vars createVars()
  {
    VarsImpl vars = new VarsImpl();
    return vars;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InputVars createInputVars()
  {
    InputVarsImpl inputVars = new InputVarsImpl();
    return inputVars;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OutputVars createOutputVars()
  {
    OutputVarsImpl outputVars = new OutputVarsImpl();
    return outputVars;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ResourceVar createResourceVar()
  {
    ResourceVarImpl resourceVar = new ResourceVarImpl();
    return resourceVar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ResourceType createResourceType()
  {
    ResourceTypeImpl resourceType = new ResourceTypeImpl();
    return resourceType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntResourceType createIntResourceType()
  {
    IntResourceTypeImpl intResourceType = new IntResourceTypeImpl();
    return intResourceType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanResourceType createBooleanResourceType()
  {
    BooleanResourceTypeImpl booleanResourceType = new BooleanResourceTypeImpl();
    return booleanResourceType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CustomVar createCustomVar()
  {
    CustomVarImpl customVar = new CustomVarImpl();
    return customVar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntVar createIntVar()
  {
    IntVarImpl intVar = new IntVarImpl();
    return intVar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolVar createBoolVar()
  {
    BoolVarImpl boolVar = new BoolVarImpl();
    return boolVar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerVar createTimerVar()
  {
    TimerVarImpl timerVar = new TimerVarImpl();
    return timerVar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerCallCommand createTimerCallCommand()
  {
    TimerCallCommandImpl timerCallCommand = new TimerCallCommandImpl();
    return timerCallCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Resource createResource()
  {
    ResourceImpl resource = new ResourceImpl();
    return resource;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InputBitResource createInputBitResource()
  {
    InputBitResourceImpl inputBitResource = new InputBitResourceImpl();
    return inputBitResource;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OutputBitResource createOutputBitResource()
  {
    OutputBitResourceImpl outputBitResource = new OutputBitResourceImpl();
    return outputBitResource;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FlagByteResource createFlagByteResource()
  {
    FlagByteResourceImpl flagByteResource = new FlagByteResourceImpl();
    return flagByteResource;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ObjectReference createObjectReference()
  {
    ObjectReferenceImpl objectReference = new ObjectReferenceImpl();
    return objectReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DotExpression createDotExpression()
  {
    DotExpressionImpl dotExpression = new DotExpressionImpl();
    return dotExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimpleTypeEnum createSimpleTypeEnumFromString(EDataType eDataType, String initialValue)
  {
    SimpleTypeEnum result = SimpleTypeEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertSimpleTypeEnumToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InstructionListPackage getInstructionListPackage()
  {
    return (InstructionListPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static InstructionListPackage getPackage()
  {
    return InstructionListPackage.eINSTANCE;
  }

} //InstructionListFactoryImpl
