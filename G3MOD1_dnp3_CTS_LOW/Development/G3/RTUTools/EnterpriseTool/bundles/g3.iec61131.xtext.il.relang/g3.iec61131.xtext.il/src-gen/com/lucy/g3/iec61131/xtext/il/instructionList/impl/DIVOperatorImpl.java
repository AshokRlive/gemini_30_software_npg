/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.impl;

import com.lucy.g3.iec61131.xtext.il.instructionList.DIVOperator;
import com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DIV Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DIVOperatorImpl extends MathCommandImpl implements DIVOperator
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DIVOperatorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return InstructionListPackage.Literals.DIV_OPERATOR;
  }

} //DIVOperatorImpl
