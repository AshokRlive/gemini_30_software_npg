/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getVars <em>Vars</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getInputVars <em>Input Vars</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getOutputVars <em>Output Vars</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getCommands <em>Commands</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getLabels <em>Labels</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getFunctionBlock()
 * @model
 * @generated
 */
public interface FunctionBlock extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getFunctionBlock_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Vars</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61131.xtext.il.instructionList.Vars}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vars</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vars</em>' containment reference list.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getFunctionBlock_Vars()
   * @model containment="true"
   * @generated
   */
  EList<Vars> getVars();

  /**
   * Returns the value of the '<em><b>Input Vars</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61131.xtext.il.instructionList.InputVars}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Input Vars</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Input Vars</em>' containment reference list.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getFunctionBlock_InputVars()
   * @model containment="true"
   * @generated
   */
  EList<InputVars> getInputVars();

  /**
   * Returns the value of the '<em><b>Output Vars</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61131.xtext.il.instructionList.OutputVars}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Output Vars</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Output Vars</em>' containment reference list.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getFunctionBlock_OutputVars()
   * @model containment="true"
   * @generated
   */
  EList<OutputVars> getOutputVars();

  /**
   * Returns the value of the '<em><b>Commands</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61131.xtext.il.instructionList.Command}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Commands</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Commands</em>' containment reference list.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getFunctionBlock_Commands()
   * @model containment="true"
   * @generated
   */
  EList<Command> getCommands();

  /**
   * Returns the value of the '<em><b>Labels</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Labels</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Labels</em>' containment reference list.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getFunctionBlock_Labels()
   * @model containment="true"
   * @generated
   */
  EList<ILLabel> getLabels();

} // FunctionBlock
