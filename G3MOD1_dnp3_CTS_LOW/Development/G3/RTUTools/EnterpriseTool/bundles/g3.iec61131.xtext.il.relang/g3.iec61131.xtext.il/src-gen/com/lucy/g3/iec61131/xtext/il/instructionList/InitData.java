/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Init Data</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getInitData()
 * @model
 * @generated
 */
public interface InitData extends EObject
{
} // InitData
