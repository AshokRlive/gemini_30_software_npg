/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.impl;

import com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage;
import com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timer Call Command</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimerCallCommandImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimerCallCommandImpl#getIn <em>In</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimerCallCommandImpl#getPt <em>Pt</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimerCallCommandImpl#getQ <em>Q</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimerCallCommandImpl#getEt <em>Et</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimerCallCommandImpl extends CommandTypesImpl implements TimerCallCommand
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getIn() <em>In</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIn()
   * @generated
   * @ordered
   */
  protected static final String IN_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getIn() <em>In</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIn()
   * @generated
   * @ordered
   */
  protected String in = IN_EDEFAULT;

  /**
   * The default value of the '{@link #getPt() <em>Pt</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPt()
   * @generated
   * @ordered
   */
  protected static final String PT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPt() <em>Pt</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPt()
   * @generated
   * @ordered
   */
  protected String pt = PT_EDEFAULT;

  /**
   * The default value of the '{@link #getQ() <em>Q</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getQ()
   * @generated
   * @ordered
   */
  protected static final String Q_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getQ() <em>Q</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getQ()
   * @generated
   * @ordered
   */
  protected String q = Q_EDEFAULT;

  /**
   * The default value of the '{@link #getEt() <em>Et</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEt()
   * @generated
   * @ordered
   */
  protected static final String ET_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getEt() <em>Et</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEt()
   * @generated
   * @ordered
   */
  protected String et = ET_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TimerCallCommandImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return InstructionListPackage.Literals.TIMER_CALL_COMMAND;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, InstructionListPackage.TIMER_CALL_COMMAND__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIn()
  {
    return in;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIn(String newIn)
  {
    String oldIn = in;
    in = newIn;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, InstructionListPackage.TIMER_CALL_COMMAND__IN, oldIn, in));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPt()
  {
    return pt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPt(String newPt)
  {
    String oldPt = pt;
    pt = newPt;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, InstructionListPackage.TIMER_CALL_COMMAND__PT, oldPt, pt));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getQ()
  {
    return q;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setQ(String newQ)
  {
    String oldQ = q;
    q = newQ;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, InstructionListPackage.TIMER_CALL_COMMAND__Q, oldQ, q));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getEt()
  {
    return et;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEt(String newEt)
  {
    String oldEt = et;
    et = newEt;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, InstructionListPackage.TIMER_CALL_COMMAND__ET, oldEt, et));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case InstructionListPackage.TIMER_CALL_COMMAND__NAME:
        return getName();
      case InstructionListPackage.TIMER_CALL_COMMAND__IN:
        return getIn();
      case InstructionListPackage.TIMER_CALL_COMMAND__PT:
        return getPt();
      case InstructionListPackage.TIMER_CALL_COMMAND__Q:
        return getQ();
      case InstructionListPackage.TIMER_CALL_COMMAND__ET:
        return getEt();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case InstructionListPackage.TIMER_CALL_COMMAND__NAME:
        setName((String)newValue);
        return;
      case InstructionListPackage.TIMER_CALL_COMMAND__IN:
        setIn((String)newValue);
        return;
      case InstructionListPackage.TIMER_CALL_COMMAND__PT:
        setPt((String)newValue);
        return;
      case InstructionListPackage.TIMER_CALL_COMMAND__Q:
        setQ((String)newValue);
        return;
      case InstructionListPackage.TIMER_CALL_COMMAND__ET:
        setEt((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case InstructionListPackage.TIMER_CALL_COMMAND__NAME:
        setName(NAME_EDEFAULT);
        return;
      case InstructionListPackage.TIMER_CALL_COMMAND__IN:
        setIn(IN_EDEFAULT);
        return;
      case InstructionListPackage.TIMER_CALL_COMMAND__PT:
        setPt(PT_EDEFAULT);
        return;
      case InstructionListPackage.TIMER_CALL_COMMAND__Q:
        setQ(Q_EDEFAULT);
        return;
      case InstructionListPackage.TIMER_CALL_COMMAND__ET:
        setEt(ET_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case InstructionListPackage.TIMER_CALL_COMMAND__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case InstructionListPackage.TIMER_CALL_COMMAND__IN:
        return IN_EDEFAULT == null ? in != null : !IN_EDEFAULT.equals(in);
      case InstructionListPackage.TIMER_CALL_COMMAND__PT:
        return PT_EDEFAULT == null ? pt != null : !PT_EDEFAULT.equals(pt);
      case InstructionListPackage.TIMER_CALL_COMMAND__Q:
        return Q_EDEFAULT == null ? q != null : !Q_EDEFAULT.equals(q);
      case InstructionListPackage.TIMER_CALL_COMMAND__ET:
        return ET_EDEFAULT == null ? et != null : !ET_EDEFAULT.equals(et);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", in: ");
    result.append(in);
    result.append(", pt: ");
    result.append(pt);
    result.append(", q: ");
    result.append(q);
    result.append(", et: ");
    result.append(et);
    result.append(')');
    return result.toString();
  }

} //TimerCallCommandImpl
