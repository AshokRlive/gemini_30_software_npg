/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timer Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerVar#getTimer <em>Timer</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getTimerVar()
 * @model
 * @generated
 */
public interface TimerVar extends AVar
{
  /**
   * Returns the value of the '<em><b>Timer</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer</em>' attribute.
   * @see #setTimer(String)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getTimerVar_Timer()
   * @model
   * @generated
   */
  String getTimer();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerVar#getTimer <em>Timer</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer</em>' attribute.
   * @see #getTimer()
   * @generated
   */
  void setTimer(String value);

} // TimerVar
