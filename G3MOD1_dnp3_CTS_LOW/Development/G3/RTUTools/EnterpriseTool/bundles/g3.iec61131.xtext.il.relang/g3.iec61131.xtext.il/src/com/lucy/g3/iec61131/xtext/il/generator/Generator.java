
package com.lucy.g3.iec61131.xtext.il.generator;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.URIConverter.ReadableInputStream;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.generator.JavaIoFileSystemAccess;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.Issue;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.lucy.g3.iec61131.xtext.il.instructionList.ILObject;

/**
 * Generator to produce C code from IL Text
 */
public class Generator {
  
  @Inject
  private Provider<ResourceSet> resourceSetProvider;
  @Inject
  private IResourceValidator validator;
  @Inject
  private IGenerator generator;
  @Inject
  private JavaIoFileSystemAccess fileAccess;


  protected Generator() {}
  

  /**
   * Generate C code based on the input IL File
   * @param inputFile IL File
   * @param outputPath Destination folder
   * @throws ParseILException 
   * @throws IOException 
   */
  private void generateFile (File inputFile, File outputPath) throws ParseILException, IOException {
    
    Resource resource = createResource(inputFile);    
    ILObject ILObject = (ILObject) resource.getContents().get(0);
    
    fileAccess.setOutputPath(outputPath.getAbsolutePath());
    RuleEngineGenerator ILGen = new RuleEngineGenerator();
    
    validateResource(resource);
   
    // get current file name
    List<String> segments = resource.getURI().segmentsList();
    String newFilename = segments.get(segments.size() - 1).replace(".il", "") + ".c";
    fileAccess.generateFile(newFilename, ILGen.toC(ILObject));
  }
  

  
  private String generateString (File inputName) throws ParseILException, IOException {
    Resource resource = createResource(inputName);
    ILObject ILObject = (ILObject) resource.getContents().get(0);
    validateResource(resource);
    return getStringAsILObject(ILObject);
  }
  
  private String generateString(String textIL) throws ParseILException, IOException {
    Resource resource = createResource(textIL);
    ILObject ILObject = (ILObject) resource.getContents().get(0);
    validateResource(resource);
    return getStringAsILObject(ILObject);
  }
  
  
  
  protected String getStringAsILObject(ILObject ILObject) {
    RuleEngineGenerator ILGen = new RuleEngineGenerator();
    CharSequence genIL = ILGen.toC(ILObject);
    StringBuilder sb = new StringBuilder(genIL.length());
    sb.append(genIL);
    return sb.toString();
  }
  
  /**
   * Create a resource from a given string/text 
   * @param fileIL
   * @return
   * @throws IOException
   */
  private Resource createResource(String textIL) throws IOException {
    ResourceSet set = resourceSetProvider.get();
    Resource resource = set.createResource(URI.createURI("resource.il"));
    ReadableInputStream ins = new URIConverter.ReadableInputStream(textIL, "UTF-8");
    resource.load(ins, Collections.EMPTY_MAP);    
    return resource;
  }
  
  /**
   * Create a resource from a given file 
   * @param fileIL
   * @return
   * @throws IOException
   */
  private Resource createResource(File fileIL) throws IOException {
    if (!fileIL.exists())
      throw new IOException("'" + fileIL +  "' file not found ");
    String relativePath = new File(System.getProperty("user.dir")).toURI()
        .relativize(fileIL.toURI()).getPath();
    ResourceSet set = resourceSetProvider.get();
    Resource resource = set.getResource(URI.createURI(relativePath), true);
    return resource;    
  }
  
  /**
   * Validate IL from loaded Resource
   * @param resource
   * @throws ParseILException 
   */
  private void validateResource(Resource resource) throws ParseILException {
    List<Issue> list = validator.validate(resource, CheckMode.ALL, CancelIndicator.NullImpl);
    if (!list.isEmpty()) {
      for (Issue issue : list) {
        throw new ParseILException(issue);        
      }
    }
  }
  
  /**
   * Static method to generate C code from IL text (file name).
   *  Destination path is set to current directory
   * @param inputILFilename
   * @throws ParseILException
   * @throws IOException
   */
  public static void generate (String inputILFilename) throws ParseILException, IOException {
    generate(inputILFilename, System.getProperty("user.dir"));
  }
  
  /**
   * Static method to generate C code from IL text
   * @param inputILFilename
   * @param outputPath
   * @throws ParseILException
   * @throws IOException
   * @see RuleEngineStandaloneSetup
   */
  public static void generate (String inputILFilename, String outputPath) throws ParseILException, IOException {
    generate(new File(inputILFilename), new File(outputPath));
  }
  
  /**
   * Static method to generate C code from IL text (file)
   * @param inputILFile 
   * @param outputPath Destination path
   * @throws ParseILException
   * @throws IOException
   */
  public static void generate (File inputILFile, File outputPath) throws ParseILException, IOException {
    Injector injector = new com.lucy.g3.iec61131.xtext.il.RuleEngineStandaloneSetup()
        .createInjectorAndDoEMFRegistration();
    Generator gen = injector.getInstance(Generator.class);
    
    gen.generateFile(inputILFile, outputPath);
  }
  
  /**
   * Generate C code internally from a given string IL Code
   * @param textIL - String format
   * @return Generated C Code
   * @throws ParseILException
   * @throws IOException
   * @see RuleEngineStandaloneSetup
   */
  public static String generateText(String textIL) throws ParseILException, IOException {
    Injector injector = new com.lucy.g3.iec61131.xtext.il.RuleEngineStandaloneSetup()
        .createInjectorAndDoEMFRegistration();
    Generator gen = injector.getInstance(Generator.class);
    return gen.generateString(textIL);
  }
  
  /**
   * Generate C code internally from a given IL File
   * @param textIL - String format
   * @return Generated C Code
   * @throws ParseILException
   * @throws IOException
   */
  public static String generateText(File ILFile) throws ParseILException, IOException {
    Injector injector = new com.lucy.g3.iec61131.xtext.il.RuleEngineStandaloneSetup()
        .createInjectorAndDoEMFRegistration();
    Generator gen = injector.getInstance(Generator.class);
    return gen.generateString(ILFile);
  }
  
  
  
}

