/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListFactory
 * @model kind="package"
 * @generated
 */
public interface InstructionListPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "instructionList";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.lucy.org/iec61131/InstructionList";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "instructionList";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  InstructionListPackage eINSTANCE = com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl.init();

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILObjectImpl <em>IL Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILObjectImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getILObject()
   * @generated
   */
  int IL_OBJECT = 0;

  /**
   * The feature id for the '<em><b>Main</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IL_OBJECT__MAIN = 0;

  /**
   * The feature id for the '<em><b>Functions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IL_OBJECT__FUNCTIONS = 1;

  /**
   * The feature id for the '<em><b>Functionblocks</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IL_OBJECT__FUNCTIONBLOCKS = 2;

  /**
   * The number of structural features of the '<em>IL Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IL_OBJECT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ProgramImpl <em>Program</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ProgramImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getProgram()
   * @generated
   */
  int PROGRAM = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__NAME = 0;

  /**
   * The feature id for the '<em><b>Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__VARS = 1;

  /**
   * The feature id for the '<em><b>Input Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__INPUT_VARS = 2;

  /**
   * The feature id for the '<em><b>Output Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__OUTPUT_VARS = 3;

  /**
   * The feature id for the '<em><b>Commands</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__COMMANDS = 4;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__LABELS = 5;

  /**
   * The number of structural features of the '<em>Program</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM_FEATURE_COUNT = 6;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionBlockImpl <em>Function Block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionBlockImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getFunctionBlock()
   * @generated
   */
  int FUNCTION_BLOCK = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_BLOCK__NAME = 0;

  /**
   * The feature id for the '<em><b>Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_BLOCK__VARS = 1;

  /**
   * The feature id for the '<em><b>Input Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_BLOCK__INPUT_VARS = 2;

  /**
   * The feature id for the '<em><b>Output Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_BLOCK__OUTPUT_VARS = 3;

  /**
   * The feature id for the '<em><b>Commands</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_BLOCK__COMMANDS = 4;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_BLOCK__LABELS = 5;

  /**
   * The number of structural features of the '<em>Function Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_BLOCK_FEATURE_COUNT = 6;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionImpl <em>Function</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getFunction()
   * @generated
   */
  int FUNCTION = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__NAME = 0;

  /**
   * The feature id for the '<em><b>Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__VARS = 1;

  /**
   * The feature id for the '<em><b>Input Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__INPUT_VARS = 2;

  /**
   * The feature id for the '<em><b>Output Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__OUTPUT_VARS = 3;

  /**
   * The feature id for the '<em><b>Commands</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__COMMANDS = 4;

  /**
   * The number of structural features of the '<em>Function</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.FeatureImpl <em>Feature</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.FeatureImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getFeature()
   * @generated
   */
  int FEATURE = 4;

  /**
   * The feature id for the '<em><b>Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FEATURE__VARS = 0;

  /**
   * The number of structural features of the '<em>Feature</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FEATURE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ReferenceImpl <em>Reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ReferenceImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getReference()
   * @generated
   */
  int REFERENCE = 5;

  /**
   * The number of structural features of the '<em>Reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.CommandImpl <em>Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.CommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getCommand()
   * @generated
   */
  int COMMAND = 18;

  /**
   * The number of structural features of the '<em>Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMAND_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILCommandImpl <em>IL Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getILCommand()
   * @generated
   */
  int IL_COMMAND = 19;

  /**
   * The number of structural features of the '<em>IL Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IL_COMMAND_FEATURE_COUNT = COMMAND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.RCommandImpl <em>RCommand</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.RCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getRCommand()
   * @generated
   */
  int RCOMMAND = 6;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RCOMMAND__NAME = IL_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>RCommand</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RCOMMAND_FEATURE_COUNT = IL_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.SCommandImpl <em>SCommand</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.SCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getSCommand()
   * @generated
   */
  int SCOMMAND = 7;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCOMMAND__NAME = IL_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>SCommand</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCOMMAND_FEATURE_COUNT = IL_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILLabelImpl <em>IL Label</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILLabelImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getILLabel()
   * @generated
   */
  int IL_LABEL = 8;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IL_LABEL__NAME = 0;

  /**
   * The feature id for the '<em><b>Initial Command</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IL_LABEL__INITIAL_COMMAND = 1;

  /**
   * The feature id for the '<em><b>Commands</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IL_LABEL__COMMANDS = 2;

  /**
   * The number of structural features of the '<em>IL Label</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IL_LABEL_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionCallImpl <em>Function Call</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionCallImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getFunctionCall()
   * @generated
   */
  int FUNCTION_CALL = 9;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_CALL__NAME = IL_COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Arguments</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_CALL__ARGUMENTS = IL_COMMAND_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Function Call</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_CALL_FEATURE_COUNT = IL_COMMAND_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionInputsImpl <em>Function Inputs</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionInputsImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getFunctionInputs()
   * @generated
   */
  int FUNCTION_INPUTS = 10;

  /**
   * The feature id for the '<em><b>Items</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_INPUTS__ITEMS = 0;

  /**
   * The number of structural features of the '<em>Function Inputs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_INPUTS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.JMPCNCommandImpl <em>JMPCN Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.JMPCNCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getJMPCNCommand()
   * @generated
   */
  int JMPCN_COMMAND = 11;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JMPCN_COMMAND__NAME = IL_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>JMPCN Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JMPCN_COMMAND_FEATURE_COUNT = IL_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.JMPCommandImpl <em>JMP Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.JMPCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getJMPCommand()
   * @generated
   */
  int JMP_COMMAND = 12;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JMP_COMMAND__NAME = IL_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>JMP Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JMP_COMMAND_FEATURE_COUNT = IL_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQCommandImpl <em>EQ Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getEQCommand()
   * @generated
   */
  int EQ_COMMAND = 13;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQ_COMMAND__VALUE = IL_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>EQ Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQ_COMMAND_FEATURE_COUNT = IL_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQBoolImpl <em>EQ Bool</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQBoolImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getEQBool()
   * @generated
   */
  int EQ_BOOL = 14;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQ_BOOL__VALUE = EQ_COMMAND__VALUE;

  /**
   * The number of structural features of the '<em>EQ Bool</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQ_BOOL_FEATURE_COUNT = EQ_COMMAND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQNBoolImpl <em>EQN Bool</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQNBoolImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getEQNBool()
   * @generated
   */
  int EQN_BOOL = 15;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQN_BOOL__VALUE = EQ_COMMAND__VALUE;

  /**
   * The number of structural features of the '<em>EQN Bool</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQN_BOOL_FEATURE_COUNT = EQ_COMMAND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQIntImpl <em>EQ Int</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQIntImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getEQInt()
   * @generated
   */
  int EQ_INT = 16;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQ_INT__VALUE = EQ_COMMAND__VALUE;

  /**
   * The number of structural features of the '<em>EQ Int</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQ_INT_FEATURE_COUNT = EQ_COMMAND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQNIntImpl <em>EQN Int</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQNIntImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getEQNInt()
   * @generated
   */
  int EQN_INT = 17;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQN_INT__VALUE = EQ_COMMAND__VALUE;

  /**
   * The number of structural features of the '<em>EQN Int</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQN_INT_FEATURE_COUNT = EQ_COMMAND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ExtendedCommandsImpl <em>Extended Commands</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ExtendedCommandsImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getExtendedCommands()
   * @generated
   */
  int EXTENDED_COMMANDS = 20;

  /**
   * The number of structural features of the '<em>Extended Commands</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENDED_COMMANDS_FEATURE_COUNT = COMMAND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.DelayCommandImpl <em>Delay Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.DelayCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getDelayCommand()
   * @generated
   */
  int DELAY_COMMAND = 21;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DELAY_COMMAND__NAME = EXTENDED_COMMANDS_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DELAY_COMMAND__VALUE = EXTENDED_COMMANDS_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Delay Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DELAY_COMMAND_FEATURE_COUNT = EXTENDED_COMMANDS_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ParameterImpl <em>Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ParameterImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getParameter()
   * @generated
   */
  int PARAMETER = 22;

  /**
   * The number of structural features of the '<em>Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceParamaterImpl <em>Resource Paramater</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceParamaterImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getResourceParamater()
   * @generated
   */
  int RESOURCE_PARAMATER = 23;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESOURCE_PARAMATER__NAME = PARAMETER_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Resource Paramater</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESOURCE_PARAMATER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.IntParameterImpl <em>Int Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.IntParameterImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getIntParameter()
   * @generated
   */
  int INT_PARAMETER = 24;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_PARAMETER__NAME = PARAMETER_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Int Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.BooleanParameterImpl <em>Boolean Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.BooleanParameterImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getBooleanParameter()
   * @generated
   */
  int BOOLEAN_PARAMETER = 25;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_PARAMETER__NAME = PARAMETER_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Boolean Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.STCommandImpl <em>ST Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.STCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getSTCommand()
   * @generated
   */
  int ST_COMMAND = 26;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ST_COMMAND__NAME = IL_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>ST Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ST_COMMAND_FEATURE_COUNT = IL_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.LDCommandImpl <em>LD Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.LDCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getLDCommand()
   * @generated
   */
  int LD_COMMAND = 27;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LD_COMMAND__NAME = IL_COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LD_COMMAND__VALUE = IL_COMMAND_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>LD Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LD_COMMAND_FEATURE_COUNT = IL_COMMAND_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.MathCommandImpl <em>Math Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.MathCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getMathCommand()
   * @generated
   */
  int MATH_COMMAND = 28;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATH_COMMAND__NAME = IL_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Math Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MATH_COMMAND_FEATURE_COUNT = IL_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ADDOperatorImpl <em>ADD Operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ADDOperatorImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getADDOperator()
   * @generated
   */
  int ADD_OPERATOR = 29;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_OPERATOR__NAME = MATH_COMMAND__NAME;

  /**
   * The number of structural features of the '<em>ADD Operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_OPERATOR_FEATURE_COUNT = MATH_COMMAND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.SUBOperatorImpl <em>SUB Operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.SUBOperatorImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getSUBOperator()
   * @generated
   */
  int SUB_OPERATOR = 30;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_OPERATOR__NAME = MATH_COMMAND__NAME;

  /**
   * The number of structural features of the '<em>SUB Operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_OPERATOR_FEATURE_COUNT = MATH_COMMAND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.MULOperatorImpl <em>MUL Operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.MULOperatorImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getMULOperator()
   * @generated
   */
  int MUL_OPERATOR = 31;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUL_OPERATOR__NAME = MATH_COMMAND__NAME;

  /**
   * The number of structural features of the '<em>MUL Operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUL_OPERATOR_FEATURE_COUNT = MATH_COMMAND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.DIVOperatorImpl <em>DIV Operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.DIVOperatorImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getDIVOperator()
   * @generated
   */
  int DIV_OPERATOR = 32;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIV_OPERATOR__NAME = MATH_COMMAND__NAME;

  /**
   * The number of structural features of the '<em>DIV Operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIV_OPERATOR_FEATURE_COUNT = MATH_COMMAND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.LogicOpCommandImpl <em>Logic Op Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.LogicOpCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getLogicOpCommand()
   * @generated
   */
  int LOGIC_OP_COMMAND = 33;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGIC_OP_COMMAND__NAME = IL_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Logic Op Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGIC_OP_COMMAND_FEATURE_COUNT = IL_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ANDOperatorImpl <em>AND Operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ANDOperatorImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getANDOperator()
   * @generated
   */
  int AND_OPERATOR = 34;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_OPERATOR__NAME = LOGIC_OP_COMMAND__NAME;

  /**
   * The number of structural features of the '<em>AND Operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_OPERATOR_FEATURE_COUNT = LOGIC_OP_COMMAND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.OROperatorImpl <em>OR Operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.OROperatorImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getOROperator()
   * @generated
   */
  int OR_OPERATOR = 35;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_OPERATOR__NAME = LOGIC_OP_COMMAND__NAME;

  /**
   * The number of structural features of the '<em>OR Operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_OPERATOR_FEATURE_COUNT = LOGIC_OP_COMMAND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.XOROperatorImpl <em>XOR Operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.XOROperatorImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getXOROperator()
   * @generated
   */
  int XOR_OPERATOR = 36;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XOR_OPERATOR__NAME = LOGIC_OP_COMMAND__NAME;

  /**
   * The number of structural features of the '<em>XOR Operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XOR_OPERATOR_FEATURE_COUNT = LOGIC_OP_COMMAND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ReturnCommandImpl <em>Return Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ReturnCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getReturnCommand()
   * @generated
   */
  int RETURN_COMMAND = 37;

  /**
   * The feature id for the '<em><b>Ret</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETURN_COMMAND__RET = IL_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Return Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETURN_COMMAND_FEATURE_COUNT = IL_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.CallCommandImpl <em>Call Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.CallCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getCallCommand()
   * @generated
   */
  int CALL_COMMAND = 38;

  /**
   * The feature id for the '<em><b>Command</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_COMMAND__COMMAND = IL_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Call Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_COMMAND_FEATURE_COUNT = IL_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.CommandTypesImpl <em>Command Types</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.CommandTypesImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getCommandTypes()
   * @generated
   */
  int COMMAND_TYPES = 39;

  /**
   * The number of structural features of the '<em>Command Types</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMAND_TYPES_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.InitDataImpl <em>Init Data</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InitDataImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getInitData()
   * @generated
   */
  int INIT_DATA = 40;

  /**
   * The number of structural features of the '<em>Init Data</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INIT_DATA_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.BoolDataImpl <em>Bool Data</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.BoolDataImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getBoolData()
   * @generated
   */
  int BOOL_DATA = 41;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_DATA__VALUE = INIT_DATA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Bool Data</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_DATA_FEATURE_COUNT = INIT_DATA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimeDataImpl <em>Time Data</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimeDataImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getTimeData()
   * @generated
   */
  int TIME_DATA = 42;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_DATA__VALUE = INIT_DATA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Time Data</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_DATA_FEATURE_COUNT = INIT_DATA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.AVarImpl <em>AVar</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.AVarImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getAVar()
   * @generated
   */
  int AVAR = 43;

  /**
   * The feature id for the '<em><b>Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AVAR__VARS = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AVAR__NAME = 1;

  /**
   * The number of structural features of the '<em>AVar</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AVAR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.TypeImpl <em>Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.TypeImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getType()
   * @generated
   */
  int TYPE = 44;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__TYPE = 0;

  /**
   * The number of structural features of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.VarsImpl <em>Vars</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.VarsImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getVars()
   * @generated
   */
  int VARS = 45;

  /**
   * The feature id for the '<em><b>Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARS__VARS = 0;

  /**
   * The number of structural features of the '<em>Vars</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.InputVarsImpl <em>Input Vars</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InputVarsImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getInputVars()
   * @generated
   */
  int INPUT_VARS = 46;

  /**
   * The feature id for the '<em><b>Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INPUT_VARS__VARS = FEATURE__VARS;

  /**
   * The number of structural features of the '<em>Input Vars</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INPUT_VARS_FEATURE_COUNT = FEATURE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.OutputVarsImpl <em>Output Vars</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.OutputVarsImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getOutputVars()
   * @generated
   */
  int OUTPUT_VARS = 47;

  /**
   * The feature id for the '<em><b>Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUTPUT_VARS__VARS = FEATURE__VARS;

  /**
   * The number of structural features of the '<em>Output Vars</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUTPUT_VARS_FEATURE_COUNT = FEATURE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceVarImpl <em>Resource Var</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceVarImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getResourceVar()
   * @generated
   */
  int RESOURCE_VAR = 48;

  /**
   * The feature id for the '<em><b>Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESOURCE_VAR__VARS = AVAR__VARS;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESOURCE_VAR__NAME = AVAR__NAME;

  /**
   * The feature id for the '<em><b>Resource</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESOURCE_VAR__RESOURCE = AVAR_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESOURCE_VAR__TYPE = AVAR_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Resource Var</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESOURCE_VAR_FEATURE_COUNT = AVAR_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceTypeImpl <em>Resource Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceTypeImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getResourceType()
   * @generated
   */
  int RESOURCE_TYPE = 49;

  /**
   * The number of structural features of the '<em>Resource Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESOURCE_TYPE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.IntResourceTypeImpl <em>Int Resource Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.IntResourceTypeImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getIntResourceType()
   * @generated
   */
  int INT_RESOURCE_TYPE = 50;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_RESOURCE_TYPE__VALUE = RESOURCE_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Int Resource Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_RESOURCE_TYPE_FEATURE_COUNT = RESOURCE_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.BooleanResourceTypeImpl <em>Boolean Resource Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.BooleanResourceTypeImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getBooleanResourceType()
   * @generated
   */
  int BOOLEAN_RESOURCE_TYPE = 51;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_RESOURCE_TYPE__VALUE = RESOURCE_TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Boolean Resource Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_RESOURCE_TYPE_FEATURE_COUNT = RESOURCE_TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.CustomVarImpl <em>Custom Var</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.CustomVarImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getCustomVar()
   * @generated
   */
  int CUSTOM_VAR = 52;

  /**
   * The feature id for the '<em><b>Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUSTOM_VAR__VARS = AVAR__VARS;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUSTOM_VAR__NAME = AVAR__NAME;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUSTOM_VAR__TYPE = AVAR_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Initializer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUSTOM_VAR__INITIALIZER = AVAR_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Custom Var</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUSTOM_VAR_FEATURE_COUNT = AVAR_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.IntVarImpl <em>Int Var</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.IntVarImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getIntVar()
   * @generated
   */
  int INT_VAR = 53;

  /**
   * The feature id for the '<em><b>Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_VAR__VARS = AVAR__VARS;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_VAR__NAME = AVAR__NAME;

  /**
   * The feature id for the '<em><b>Int Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_VAR__INT_TYPE = AVAR_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Int Var</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_VAR_FEATURE_COUNT = AVAR_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.BoolVarImpl <em>Bool Var</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.BoolVarImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getBoolVar()
   * @generated
   */
  int BOOL_VAR = 54;

  /**
   * The feature id for the '<em><b>Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_VAR__VARS = AVAR__VARS;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_VAR__NAME = AVAR__NAME;

  /**
   * The feature id for the '<em><b>Bool Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_VAR__BOOL_TYPE = AVAR_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Bool Var</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_VAR_FEATURE_COUNT = AVAR_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimerVarImpl <em>Timer Var</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimerVarImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getTimerVar()
   * @generated
   */
  int TIMER_VAR = 55;

  /**
   * The feature id for the '<em><b>Vars</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_VAR__VARS = AVAR__VARS;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_VAR__NAME = AVAR__NAME;

  /**
   * The feature id for the '<em><b>Timer</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_VAR__TIMER = AVAR_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Timer Var</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_VAR_FEATURE_COUNT = AVAR_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimerCallCommandImpl <em>Timer Call Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimerCallCommandImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getTimerCallCommand()
   * @generated
   */
  int TIMER_CALL_COMMAND = 56;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_CALL_COMMAND__NAME = COMMAND_TYPES_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_CALL_COMMAND__IN = COMMAND_TYPES_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Pt</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_CALL_COMMAND__PT = COMMAND_TYPES_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Q</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_CALL_COMMAND__Q = COMMAND_TYPES_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Et</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_CALL_COMMAND__ET = COMMAND_TYPES_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Timer Call Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMER_CALL_COMMAND_FEATURE_COUNT = COMMAND_TYPES_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceImpl <em>Resource</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getResource()
   * @generated
   */
  int RESOURCE = 57;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESOURCE__VALUE = 0;

  /**
   * The number of structural features of the '<em>Resource</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESOURCE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.InputBitResourceImpl <em>Input Bit Resource</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InputBitResourceImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getInputBitResource()
   * @generated
   */
  int INPUT_BIT_RESOURCE = 58;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INPUT_BIT_RESOURCE__VALUE = RESOURCE__VALUE;

  /**
   * The number of structural features of the '<em>Input Bit Resource</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INPUT_BIT_RESOURCE_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.OutputBitResourceImpl <em>Output Bit Resource</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.OutputBitResourceImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getOutputBitResource()
   * @generated
   */
  int OUTPUT_BIT_RESOURCE = 59;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUTPUT_BIT_RESOURCE__VALUE = RESOURCE__VALUE;

  /**
   * The number of structural features of the '<em>Output Bit Resource</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUTPUT_BIT_RESOURCE_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.FlagByteResourceImpl <em>Flag Byte Resource</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.FlagByteResourceImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getFlagByteResource()
   * @generated
   */
  int FLAG_BYTE_RESOURCE = 60;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FLAG_BYTE_RESOURCE__VALUE = RESOURCE__VALUE;

  /**
   * The number of structural features of the '<em>Flag Byte Resource</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FLAG_BYTE_RESOURCE_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ObjectReferenceImpl <em>Object Reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ObjectReferenceImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getObjectReference()
   * @generated
   */
  int OBJECT_REFERENCE = 61;

  /**
   * The feature id for the '<em><b>Entity</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_REFERENCE__ENTITY = REFERENCE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Object Reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_REFERENCE_FEATURE_COUNT = REFERENCE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.DotExpressionImpl <em>Dot Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.DotExpressionImpl
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getDotExpression()
   * @generated
   */
  int DOT_EXPRESSION = 62;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOT_EXPRESSION__REF = REFERENCE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Tail</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOT_EXPRESSION__TAIL = REFERENCE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Dot Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOT_EXPRESSION_FEATURE_COUNT = REFERENCE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.SimpleTypeEnum <em>Simple Type Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.SimpleTypeEnum
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getSimpleTypeEnum()
   * @generated
   */
  int SIMPLE_TYPE_ENUM = 63;


  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILObject <em>IL Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>IL Object</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ILObject
   * @generated
   */
  EClass getILObject();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILObject#getMain <em>Main</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Main</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ILObject#getMain()
   * @see #getILObject()
   * @generated
   */
  EReference getILObject_Main();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILObject#getFunctions <em>Functions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Functions</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ILObject#getFunctions()
   * @see #getILObject()
   * @generated
   */
  EReference getILObject_Functions();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILObject#getFunctionblocks <em>Functionblocks</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Functionblocks</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ILObject#getFunctionblocks()
   * @see #getILObject()
   * @generated
   */
  EReference getILObject_Functionblocks();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Program <em>Program</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Program</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Program
   * @generated
   */
  EClass getProgram();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Program#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Program#getName()
   * @see #getProgram()
   * @generated
   */
  EAttribute getProgram_Name();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Program#getVars <em>Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Program#getVars()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_Vars();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Program#getInputVars <em>Input Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Input Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Program#getInputVars()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_InputVars();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Program#getOutputVars <em>Output Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Output Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Program#getOutputVars()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_OutputVars();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Program#getCommands <em>Commands</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Commands</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Program#getCommands()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_Commands();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Program#getLabels <em>Labels</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Labels</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Program#getLabels()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_Labels();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock <em>Function Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Block</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock
   * @generated
   */
  EClass getFunctionBlock();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getName()
   * @see #getFunctionBlock()
   * @generated
   */
  EAttribute getFunctionBlock_Name();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getVars <em>Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getVars()
   * @see #getFunctionBlock()
   * @generated
   */
  EReference getFunctionBlock_Vars();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getInputVars <em>Input Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Input Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getInputVars()
   * @see #getFunctionBlock()
   * @generated
   */
  EReference getFunctionBlock_InputVars();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getOutputVars <em>Output Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Output Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getOutputVars()
   * @see #getFunctionBlock()
   * @generated
   */
  EReference getFunctionBlock_OutputVars();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getCommands <em>Commands</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Commands</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getCommands()
   * @see #getFunctionBlock()
   * @generated
   */
  EReference getFunctionBlock_Commands();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getLabels <em>Labels</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Labels</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionBlock#getLabels()
   * @see #getFunctionBlock()
   * @generated
   */
  EReference getFunctionBlock_Labels();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Function <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Function
   * @generated
   */
  EClass getFunction();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Function#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Function#getName()
   * @see #getFunction()
   * @generated
   */
  EAttribute getFunction_Name();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Function#getVars <em>Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Function#getVars()
   * @see #getFunction()
   * @generated
   */
  EReference getFunction_Vars();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Function#getInputVars <em>Input Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Input Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Function#getInputVars()
   * @see #getFunction()
   * @generated
   */
  EReference getFunction_InputVars();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Function#getOutputVars <em>Output Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Output Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Function#getOutputVars()
   * @see #getFunction()
   * @generated
   */
  EReference getFunction_OutputVars();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Function#getCommands <em>Commands</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Commands</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Function#getCommands()
   * @see #getFunction()
   * @generated
   */
  EReference getFunction_Commands();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Feature <em>Feature</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Feature</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Feature
   * @generated
   */
  EClass getFeature();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Feature#getVars <em>Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Feature#getVars()
   * @see #getFeature()
   * @generated
   */
  EReference getFeature_Vars();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Reference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Reference</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Reference
   * @generated
   */
  EClass getReference();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.RCommand <em>RCommand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>RCommand</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.RCommand
   * @generated
   */
  EClass getRCommand();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.RCommand#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.RCommand#getName()
   * @see #getRCommand()
   * @generated
   */
  EAttribute getRCommand_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.SCommand <em>SCommand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>SCommand</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.SCommand
   * @generated
   */
  EClass getSCommand();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.SCommand#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.SCommand#getName()
   * @see #getSCommand()
   * @generated
   */
  EAttribute getSCommand_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel <em>IL Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>IL Label</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel
   * @generated
   */
  EClass getILLabel();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel#getName()
   * @see #getILLabel()
   * @generated
   */
  EAttribute getILLabel_Name();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel#getInitialCommand <em>Initial Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Initial Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel#getInitialCommand()
   * @see #getILLabel()
   * @generated
   */
  EReference getILLabel_InitialCommand();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel#getCommands <em>Commands</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Commands</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel#getCommands()
   * @see #getILLabel()
   * @generated
   */
  EReference getILLabel_Commands();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionCall <em>Function Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Call</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionCall
   * @generated
   */
  EClass getFunctionCall();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionCall#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionCall#getName()
   * @see #getFunctionCall()
   * @generated
   */
  EAttribute getFunctionCall_Name();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionCall#getArguments <em>Arguments</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Arguments</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionCall#getArguments()
   * @see #getFunctionCall()
   * @generated
   */
  EAttribute getFunctionCall_Arguments();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionInputs <em>Function Inputs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Inputs</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionInputs
   * @generated
   */
  EClass getFunctionInputs();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FunctionInputs#getItems <em>Items</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Items</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FunctionInputs#getItems()
   * @see #getFunctionInputs()
   * @generated
   */
  EAttribute getFunctionInputs_Items();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.JMPCNCommand <em>JMPCN Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>JMPCN Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.JMPCNCommand
   * @generated
   */
  EClass getJMPCNCommand();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.JMPCNCommand#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.JMPCNCommand#getName()
   * @see #getJMPCNCommand()
   * @generated
   */
  EAttribute getJMPCNCommand_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.JMPCommand <em>JMP Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>JMP Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.JMPCommand
   * @generated
   */
  EClass getJMPCommand();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.JMPCommand#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.JMPCommand#getName()
   * @see #getJMPCommand()
   * @generated
   */
  EAttribute getJMPCommand_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.EQCommand <em>EQ Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>EQ Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.EQCommand
   * @generated
   */
  EClass getEQCommand();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.EQCommand#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.EQCommand#getValue()
   * @see #getEQCommand()
   * @generated
   */
  EAttribute getEQCommand_Value();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.EQBool <em>EQ Bool</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>EQ Bool</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.EQBool
   * @generated
   */
  EClass getEQBool();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.EQNBool <em>EQN Bool</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>EQN Bool</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.EQNBool
   * @generated
   */
  EClass getEQNBool();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.EQInt <em>EQ Int</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>EQ Int</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.EQInt
   * @generated
   */
  EClass getEQInt();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.EQNInt <em>EQN Int</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>EQN Int</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.EQNInt
   * @generated
   */
  EClass getEQNInt();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Command <em>Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Command
   * @generated
   */
  EClass getCommand();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ILCommand <em>IL Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>IL Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ILCommand
   * @generated
   */
  EClass getILCommand();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ExtendedCommands <em>Extended Commands</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Extended Commands</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ExtendedCommands
   * @generated
   */
  EClass getExtendedCommands();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.DelayCommand <em>Delay Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Delay Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.DelayCommand
   * @generated
   */
  EClass getDelayCommand();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.DelayCommand#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.DelayCommand#getName()
   * @see #getDelayCommand()
   * @generated
   */
  EAttribute getDelayCommand_Name();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.DelayCommand#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.DelayCommand#getValue()
   * @see #getDelayCommand()
   * @generated
   */
  EAttribute getDelayCommand_Value();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Parameter <em>Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parameter</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Parameter
   * @generated
   */
  EClass getParameter();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ResourceParamater <em>Resource Paramater</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Resource Paramater</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ResourceParamater
   * @generated
   */
  EClass getResourceParamater();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ResourceParamater#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ResourceParamater#getName()
   * @see #getResourceParamater()
   * @generated
   */
  EAttribute getResourceParamater_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.IntParameter <em>Int Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Int Parameter</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.IntParameter
   * @generated
   */
  EClass getIntParameter();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.IntParameter#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.IntParameter#getName()
   * @see #getIntParameter()
   * @generated
   */
  EAttribute getIntParameter_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.BooleanParameter <em>Boolean Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Parameter</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.BooleanParameter
   * @generated
   */
  EClass getBooleanParameter();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.BooleanParameter#isName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.BooleanParameter#isName()
   * @see #getBooleanParameter()
   * @generated
   */
  EAttribute getBooleanParameter_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.STCommand <em>ST Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>ST Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.STCommand
   * @generated
   */
  EClass getSTCommand();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.STCommand#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.STCommand#getName()
   * @see #getSTCommand()
   * @generated
   */
  EAttribute getSTCommand_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.LDCommand <em>LD Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LD Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.LDCommand
   * @generated
   */
  EClass getLDCommand();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.LDCommand#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.LDCommand#getName()
   * @see #getLDCommand()
   * @generated
   */
  EAttribute getLDCommand_Name();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.LDCommand#isValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.LDCommand#isValue()
   * @see #getLDCommand()
   * @generated
   */
  EAttribute getLDCommand_Value();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.MathCommand <em>Math Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Math Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.MathCommand
   * @generated
   */
  EClass getMathCommand();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.MathCommand#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.MathCommand#getName()
   * @see #getMathCommand()
   * @generated
   */
  EAttribute getMathCommand_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ADDOperator <em>ADD Operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>ADD Operator</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ADDOperator
   * @generated
   */
  EClass getADDOperator();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.SUBOperator <em>SUB Operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>SUB Operator</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.SUBOperator
   * @generated
   */
  EClass getSUBOperator();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.MULOperator <em>MUL Operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>MUL Operator</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.MULOperator
   * @generated
   */
  EClass getMULOperator();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.DIVOperator <em>DIV Operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>DIV Operator</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.DIVOperator
   * @generated
   */
  EClass getDIVOperator();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.LogicOpCommand <em>Logic Op Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Logic Op Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.LogicOpCommand
   * @generated
   */
  EClass getLogicOpCommand();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.LogicOpCommand#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.LogicOpCommand#getName()
   * @see #getLogicOpCommand()
   * @generated
   */
  EAttribute getLogicOpCommand_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ANDOperator <em>AND Operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>AND Operator</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ANDOperator
   * @generated
   */
  EClass getANDOperator();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.OROperator <em>OR Operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>OR Operator</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.OROperator
   * @generated
   */
  EClass getOROperator();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.XOROperator <em>XOR Operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>XOR Operator</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.XOROperator
   * @generated
   */
  EClass getXOROperator();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ReturnCommand <em>Return Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Return Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ReturnCommand
   * @generated
   */
  EClass getReturnCommand();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ReturnCommand#getRet <em>Ret</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ret</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ReturnCommand#getRet()
   * @see #getReturnCommand()
   * @generated
   */
  EAttribute getReturnCommand_Ret();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.CallCommand <em>Call Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Call Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.CallCommand
   * @generated
   */
  EClass getCallCommand();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61131.xtext.il.instructionList.CallCommand#getCommand <em>Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.CallCommand#getCommand()
   * @see #getCallCommand()
   * @generated
   */
  EReference getCallCommand_Command();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.CommandTypes <em>Command Types</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Command Types</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.CommandTypes
   * @generated
   */
  EClass getCommandTypes();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.InitData <em>Init Data</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Init Data</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InitData
   * @generated
   */
  EClass getInitData();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.BoolData <em>Bool Data</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool Data</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.BoolData
   * @generated
   */
  EClass getBoolData();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.BoolData#isValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.BoolData#isValue()
   * @see #getBoolData()
   * @generated
   */
  EAttribute getBoolData_Value();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimeData <em>Time Data</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Time Data</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.TimeData
   * @generated
   */
  EClass getTimeData();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimeData#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.TimeData#getValue()
   * @see #getTimeData()
   * @generated
   */
  EAttribute getTimeData_Value();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.AVar <em>AVar</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>AVar</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.AVar
   * @generated
   */
  EClass getAVar();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.AVar#getVars <em>Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.AVar#getVars()
   * @see #getAVar()
   * @generated
   */
  EReference getAVar_Vars();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.AVar#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.AVar#getName()
   * @see #getAVar()
   * @generated
   */
  EAttribute getAVar_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Type
   * @generated
   */
  EClass getType();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Type#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Type#getType()
   * @see #getType()
   * @generated
   */
  EAttribute getType_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Vars <em>Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Vars
   * @generated
   */
  EClass getVars();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Vars#getVars <em>Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Vars#getVars()
   * @see #getVars()
   * @generated
   */
  EReference getVars_Vars();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.InputVars <em>Input Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Input Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InputVars
   * @generated
   */
  EClass getInputVars();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.OutputVars <em>Output Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Output Vars</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.OutputVars
   * @generated
   */
  EClass getOutputVars();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ResourceVar <em>Resource Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Resource Var</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ResourceVar
   * @generated
   */
  EClass getResourceVar();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ResourceVar#getResource <em>Resource</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Resource</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ResourceVar#getResource()
   * @see #getResourceVar()
   * @generated
   */
  EReference getResourceVar_Resource();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ResourceVar#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ResourceVar#getType()
   * @see #getResourceVar()
   * @generated
   */
  EReference getResourceVar_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ResourceType <em>Resource Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Resource Type</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ResourceType
   * @generated
   */
  EClass getResourceType();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.IntResourceType <em>Int Resource Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Int Resource Type</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.IntResourceType
   * @generated
   */
  EClass getIntResourceType();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.IntResourceType#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.IntResourceType#getValue()
   * @see #getIntResourceType()
   * @generated
   */
  EAttribute getIntResourceType_Value();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.BooleanResourceType <em>Boolean Resource Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Resource Type</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.BooleanResourceType
   * @generated
   */
  EClass getBooleanResourceType();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.BooleanResourceType#isValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.BooleanResourceType#isValue()
   * @see #getBooleanResourceType()
   * @generated
   */
  EAttribute getBooleanResourceType_Value();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.CustomVar <em>Custom Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Custom Var</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.CustomVar
   * @generated
   */
  EClass getCustomVar();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61131.xtext.il.instructionList.CustomVar#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.CustomVar#getType()
   * @see #getCustomVar()
   * @generated
   */
  EReference getCustomVar_Type();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61131.xtext.il.instructionList.CustomVar#getInitializer <em>Initializer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Initializer</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.CustomVar#getInitializer()
   * @see #getCustomVar()
   * @generated
   */
  EReference getCustomVar_Initializer();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.IntVar <em>Int Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Int Var</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.IntVar
   * @generated
   */
  EClass getIntVar();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61131.xtext.il.instructionList.IntVar#getIntType <em>Int Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Int Type</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.IntVar#getIntType()
   * @see #getIntVar()
   * @generated
   */
  EReference getIntVar_IntType();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.BoolVar <em>Bool Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool Var</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.BoolVar
   * @generated
   */
  EClass getBoolVar();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61131.xtext.il.instructionList.BoolVar#getBoolType <em>Bool Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bool Type</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.BoolVar#getBoolType()
   * @see #getBoolVar()
   * @generated
   */
  EReference getBoolVar_BoolType();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerVar <em>Timer Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Timer Var</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.TimerVar
   * @generated
   */
  EClass getTimerVar();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerVar#getTimer <em>Timer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Timer</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.TimerVar#getTimer()
   * @see #getTimerVar()
   * @generated
   */
  EAttribute getTimerVar_Timer();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand <em>Timer Call Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Timer Call Command</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand
   * @generated
   */
  EClass getTimerCallCommand();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getName()
   * @see #getTimerCallCommand()
   * @generated
   */
  EAttribute getTimerCallCommand_Name();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getIn <em>In</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>In</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getIn()
   * @see #getTimerCallCommand()
   * @generated
   */
  EAttribute getTimerCallCommand_In();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getPt <em>Pt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Pt</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getPt()
   * @see #getTimerCallCommand()
   * @generated
   */
  EAttribute getTimerCallCommand_Pt();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getQ <em>Q</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Q</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getQ()
   * @see #getTimerCallCommand()
   * @generated
   */
  EAttribute getTimerCallCommand_Q();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getEt <em>Et</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Et</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.TimerCallCommand#getEt()
   * @see #getTimerCallCommand()
   * @generated
   */
  EAttribute getTimerCallCommand_Et();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Resource <em>Resource</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Resource</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Resource
   * @generated
   */
  EClass getResource();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61131.xtext.il.instructionList.Resource#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.Resource#getValue()
   * @see #getResource()
   * @generated
   */
  EAttribute getResource_Value();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.InputBitResource <em>Input Bit Resource</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Input Bit Resource</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InputBitResource
   * @generated
   */
  EClass getInputBitResource();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.OutputBitResource <em>Output Bit Resource</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Output Bit Resource</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.OutputBitResource
   * @generated
   */
  EClass getOutputBitResource();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.FlagByteResource <em>Flag Byte Resource</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Flag Byte Resource</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.FlagByteResource
   * @generated
   */
  EClass getFlagByteResource();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ObjectReference <em>Object Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Object Reference</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ObjectReference
   * @generated
   */
  EClass getObjectReference();

  /**
   * Returns the meta object for the reference '{@link com.lucy.g3.iec61131.xtext.il.instructionList.ObjectReference#getEntity <em>Entity</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Entity</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.ObjectReference#getEntity()
   * @see #getObjectReference()
   * @generated
   */
  EReference getObjectReference_Entity();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61131.xtext.il.instructionList.DotExpression <em>Dot Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Dot Expression</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.DotExpression
   * @generated
   */
  EClass getDotExpression();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61131.xtext.il.instructionList.DotExpression#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.DotExpression#getRef()
   * @see #getDotExpression()
   * @generated
   */
  EReference getDotExpression_Ref();

  /**
   * Returns the meta object for the reference '{@link com.lucy.g3.iec61131.xtext.il.instructionList.DotExpression#getTail <em>Tail</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Tail</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.DotExpression#getTail()
   * @see #getDotExpression()
   * @generated
   */
  EReference getDotExpression_Tail();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61131.xtext.il.instructionList.SimpleTypeEnum <em>Simple Type Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Simple Type Enum</em>'.
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.SimpleTypeEnum
   * @generated
   */
  EEnum getSimpleTypeEnum();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  InstructionListFactory getInstructionListFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILObjectImpl <em>IL Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILObjectImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getILObject()
     * @generated
     */
    EClass IL_OBJECT = eINSTANCE.getILObject();

    /**
     * The meta object literal for the '<em><b>Main</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IL_OBJECT__MAIN = eINSTANCE.getILObject_Main();

    /**
     * The meta object literal for the '<em><b>Functions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IL_OBJECT__FUNCTIONS = eINSTANCE.getILObject_Functions();

    /**
     * The meta object literal for the '<em><b>Functionblocks</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IL_OBJECT__FUNCTIONBLOCKS = eINSTANCE.getILObject_Functionblocks();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ProgramImpl <em>Program</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ProgramImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getProgram()
     * @generated
     */
    EClass PROGRAM = eINSTANCE.getProgram();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROGRAM__NAME = eINSTANCE.getProgram_Name();

    /**
     * The meta object literal for the '<em><b>Vars</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__VARS = eINSTANCE.getProgram_Vars();

    /**
     * The meta object literal for the '<em><b>Input Vars</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__INPUT_VARS = eINSTANCE.getProgram_InputVars();

    /**
     * The meta object literal for the '<em><b>Output Vars</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__OUTPUT_VARS = eINSTANCE.getProgram_OutputVars();

    /**
     * The meta object literal for the '<em><b>Commands</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__COMMANDS = eINSTANCE.getProgram_Commands();

    /**
     * The meta object literal for the '<em><b>Labels</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__LABELS = eINSTANCE.getProgram_Labels();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionBlockImpl <em>Function Block</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionBlockImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getFunctionBlock()
     * @generated
     */
    EClass FUNCTION_BLOCK = eINSTANCE.getFunctionBlock();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FUNCTION_BLOCK__NAME = eINSTANCE.getFunctionBlock_Name();

    /**
     * The meta object literal for the '<em><b>Vars</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION_BLOCK__VARS = eINSTANCE.getFunctionBlock_Vars();

    /**
     * The meta object literal for the '<em><b>Input Vars</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION_BLOCK__INPUT_VARS = eINSTANCE.getFunctionBlock_InputVars();

    /**
     * The meta object literal for the '<em><b>Output Vars</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION_BLOCK__OUTPUT_VARS = eINSTANCE.getFunctionBlock_OutputVars();

    /**
     * The meta object literal for the '<em><b>Commands</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION_BLOCK__COMMANDS = eINSTANCE.getFunctionBlock_Commands();

    /**
     * The meta object literal for the '<em><b>Labels</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION_BLOCK__LABELS = eINSTANCE.getFunctionBlock_Labels();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionImpl <em>Function</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getFunction()
     * @generated
     */
    EClass FUNCTION = eINSTANCE.getFunction();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FUNCTION__NAME = eINSTANCE.getFunction_Name();

    /**
     * The meta object literal for the '<em><b>Vars</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION__VARS = eINSTANCE.getFunction_Vars();

    /**
     * The meta object literal for the '<em><b>Input Vars</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION__INPUT_VARS = eINSTANCE.getFunction_InputVars();

    /**
     * The meta object literal for the '<em><b>Output Vars</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION__OUTPUT_VARS = eINSTANCE.getFunction_OutputVars();

    /**
     * The meta object literal for the '<em><b>Commands</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION__COMMANDS = eINSTANCE.getFunction_Commands();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.FeatureImpl <em>Feature</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.FeatureImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getFeature()
     * @generated
     */
    EClass FEATURE = eINSTANCE.getFeature();

    /**
     * The meta object literal for the '<em><b>Vars</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FEATURE__VARS = eINSTANCE.getFeature_Vars();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ReferenceImpl <em>Reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ReferenceImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getReference()
     * @generated
     */
    EClass REFERENCE = eINSTANCE.getReference();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.RCommandImpl <em>RCommand</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.RCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getRCommand()
     * @generated
     */
    EClass RCOMMAND = eINSTANCE.getRCommand();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RCOMMAND__NAME = eINSTANCE.getRCommand_Name();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.SCommandImpl <em>SCommand</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.SCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getSCommand()
     * @generated
     */
    EClass SCOMMAND = eINSTANCE.getSCommand();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCOMMAND__NAME = eINSTANCE.getSCommand_Name();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILLabelImpl <em>IL Label</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILLabelImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getILLabel()
     * @generated
     */
    EClass IL_LABEL = eINSTANCE.getILLabel();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IL_LABEL__NAME = eINSTANCE.getILLabel_Name();

    /**
     * The meta object literal for the '<em><b>Initial Command</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IL_LABEL__INITIAL_COMMAND = eINSTANCE.getILLabel_InitialCommand();

    /**
     * The meta object literal for the '<em><b>Commands</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IL_LABEL__COMMANDS = eINSTANCE.getILLabel_Commands();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionCallImpl <em>Function Call</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionCallImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getFunctionCall()
     * @generated
     */
    EClass FUNCTION_CALL = eINSTANCE.getFunctionCall();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FUNCTION_CALL__NAME = eINSTANCE.getFunctionCall_Name();

    /**
     * The meta object literal for the '<em><b>Arguments</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FUNCTION_CALL__ARGUMENTS = eINSTANCE.getFunctionCall_Arguments();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionInputsImpl <em>Function Inputs</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.FunctionInputsImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getFunctionInputs()
     * @generated
     */
    EClass FUNCTION_INPUTS = eINSTANCE.getFunctionInputs();

    /**
     * The meta object literal for the '<em><b>Items</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FUNCTION_INPUTS__ITEMS = eINSTANCE.getFunctionInputs_Items();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.JMPCNCommandImpl <em>JMPCN Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.JMPCNCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getJMPCNCommand()
     * @generated
     */
    EClass JMPCN_COMMAND = eINSTANCE.getJMPCNCommand();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute JMPCN_COMMAND__NAME = eINSTANCE.getJMPCNCommand_Name();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.JMPCommandImpl <em>JMP Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.JMPCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getJMPCommand()
     * @generated
     */
    EClass JMP_COMMAND = eINSTANCE.getJMPCommand();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute JMP_COMMAND__NAME = eINSTANCE.getJMPCommand_Name();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQCommandImpl <em>EQ Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getEQCommand()
     * @generated
     */
    EClass EQ_COMMAND = eINSTANCE.getEQCommand();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EQ_COMMAND__VALUE = eINSTANCE.getEQCommand_Value();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQBoolImpl <em>EQ Bool</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQBoolImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getEQBool()
     * @generated
     */
    EClass EQ_BOOL = eINSTANCE.getEQBool();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQNBoolImpl <em>EQN Bool</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQNBoolImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getEQNBool()
     * @generated
     */
    EClass EQN_BOOL = eINSTANCE.getEQNBool();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQIntImpl <em>EQ Int</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQIntImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getEQInt()
     * @generated
     */
    EClass EQ_INT = eINSTANCE.getEQInt();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQNIntImpl <em>EQN Int</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.EQNIntImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getEQNInt()
     * @generated
     */
    EClass EQN_INT = eINSTANCE.getEQNInt();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.CommandImpl <em>Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.CommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getCommand()
     * @generated
     */
    EClass COMMAND = eINSTANCE.getCommand();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILCommandImpl <em>IL Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ILCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getILCommand()
     * @generated
     */
    EClass IL_COMMAND = eINSTANCE.getILCommand();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ExtendedCommandsImpl <em>Extended Commands</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ExtendedCommandsImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getExtendedCommands()
     * @generated
     */
    EClass EXTENDED_COMMANDS = eINSTANCE.getExtendedCommands();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.DelayCommandImpl <em>Delay Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.DelayCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getDelayCommand()
     * @generated
     */
    EClass DELAY_COMMAND = eINSTANCE.getDelayCommand();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DELAY_COMMAND__NAME = eINSTANCE.getDelayCommand_Name();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DELAY_COMMAND__VALUE = eINSTANCE.getDelayCommand_Value();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ParameterImpl <em>Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ParameterImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getParameter()
     * @generated
     */
    EClass PARAMETER = eINSTANCE.getParameter();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceParamaterImpl <em>Resource Paramater</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceParamaterImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getResourceParamater()
     * @generated
     */
    EClass RESOURCE_PARAMATER = eINSTANCE.getResourceParamater();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RESOURCE_PARAMATER__NAME = eINSTANCE.getResourceParamater_Name();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.IntParameterImpl <em>Int Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.IntParameterImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getIntParameter()
     * @generated
     */
    EClass INT_PARAMETER = eINSTANCE.getIntParameter();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INT_PARAMETER__NAME = eINSTANCE.getIntParameter_Name();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.BooleanParameterImpl <em>Boolean Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.BooleanParameterImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getBooleanParameter()
     * @generated
     */
    EClass BOOLEAN_PARAMETER = eINSTANCE.getBooleanParameter();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_PARAMETER__NAME = eINSTANCE.getBooleanParameter_Name();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.STCommandImpl <em>ST Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.STCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getSTCommand()
     * @generated
     */
    EClass ST_COMMAND = eINSTANCE.getSTCommand();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ST_COMMAND__NAME = eINSTANCE.getSTCommand_Name();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.LDCommandImpl <em>LD Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.LDCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getLDCommand()
     * @generated
     */
    EClass LD_COMMAND = eINSTANCE.getLDCommand();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LD_COMMAND__NAME = eINSTANCE.getLDCommand_Name();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LD_COMMAND__VALUE = eINSTANCE.getLDCommand_Value();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.MathCommandImpl <em>Math Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.MathCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getMathCommand()
     * @generated
     */
    EClass MATH_COMMAND = eINSTANCE.getMathCommand();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MATH_COMMAND__NAME = eINSTANCE.getMathCommand_Name();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ADDOperatorImpl <em>ADD Operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ADDOperatorImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getADDOperator()
     * @generated
     */
    EClass ADD_OPERATOR = eINSTANCE.getADDOperator();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.SUBOperatorImpl <em>SUB Operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.SUBOperatorImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getSUBOperator()
     * @generated
     */
    EClass SUB_OPERATOR = eINSTANCE.getSUBOperator();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.MULOperatorImpl <em>MUL Operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.MULOperatorImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getMULOperator()
     * @generated
     */
    EClass MUL_OPERATOR = eINSTANCE.getMULOperator();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.DIVOperatorImpl <em>DIV Operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.DIVOperatorImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getDIVOperator()
     * @generated
     */
    EClass DIV_OPERATOR = eINSTANCE.getDIVOperator();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.LogicOpCommandImpl <em>Logic Op Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.LogicOpCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getLogicOpCommand()
     * @generated
     */
    EClass LOGIC_OP_COMMAND = eINSTANCE.getLogicOpCommand();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LOGIC_OP_COMMAND__NAME = eINSTANCE.getLogicOpCommand_Name();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ANDOperatorImpl <em>AND Operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ANDOperatorImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getANDOperator()
     * @generated
     */
    EClass AND_OPERATOR = eINSTANCE.getANDOperator();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.OROperatorImpl <em>OR Operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.OROperatorImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getOROperator()
     * @generated
     */
    EClass OR_OPERATOR = eINSTANCE.getOROperator();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.XOROperatorImpl <em>XOR Operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.XOROperatorImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getXOROperator()
     * @generated
     */
    EClass XOR_OPERATOR = eINSTANCE.getXOROperator();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ReturnCommandImpl <em>Return Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ReturnCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getReturnCommand()
     * @generated
     */
    EClass RETURN_COMMAND = eINSTANCE.getReturnCommand();

    /**
     * The meta object literal for the '<em><b>Ret</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RETURN_COMMAND__RET = eINSTANCE.getReturnCommand_Ret();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.CallCommandImpl <em>Call Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.CallCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getCallCommand()
     * @generated
     */
    EClass CALL_COMMAND = eINSTANCE.getCallCommand();

    /**
     * The meta object literal for the '<em><b>Command</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CALL_COMMAND__COMMAND = eINSTANCE.getCallCommand_Command();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.CommandTypesImpl <em>Command Types</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.CommandTypesImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getCommandTypes()
     * @generated
     */
    EClass COMMAND_TYPES = eINSTANCE.getCommandTypes();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.InitDataImpl <em>Init Data</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InitDataImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getInitData()
     * @generated
     */
    EClass INIT_DATA = eINSTANCE.getInitData();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.BoolDataImpl <em>Bool Data</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.BoolDataImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getBoolData()
     * @generated
     */
    EClass BOOL_DATA = eINSTANCE.getBoolData();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOL_DATA__VALUE = eINSTANCE.getBoolData_Value();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimeDataImpl <em>Time Data</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimeDataImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getTimeData()
     * @generated
     */
    EClass TIME_DATA = eINSTANCE.getTimeData();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TIME_DATA__VALUE = eINSTANCE.getTimeData_Value();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.AVarImpl <em>AVar</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.AVarImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getAVar()
     * @generated
     */
    EClass AVAR = eINSTANCE.getAVar();

    /**
     * The meta object literal for the '<em><b>Vars</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AVAR__VARS = eINSTANCE.getAVar_Vars();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute AVAR__NAME = eINSTANCE.getAVar_Name();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.TypeImpl <em>Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.TypeImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getType()
     * @generated
     */
    EClass TYPE = eINSTANCE.getType();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TYPE__TYPE = eINSTANCE.getType_Type();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.VarsImpl <em>Vars</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.VarsImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getVars()
     * @generated
     */
    EClass VARS = eINSTANCE.getVars();

    /**
     * The meta object literal for the '<em><b>Vars</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARS__VARS = eINSTANCE.getVars_Vars();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.InputVarsImpl <em>Input Vars</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InputVarsImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getInputVars()
     * @generated
     */
    EClass INPUT_VARS = eINSTANCE.getInputVars();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.OutputVarsImpl <em>Output Vars</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.OutputVarsImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getOutputVars()
     * @generated
     */
    EClass OUTPUT_VARS = eINSTANCE.getOutputVars();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceVarImpl <em>Resource Var</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceVarImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getResourceVar()
     * @generated
     */
    EClass RESOURCE_VAR = eINSTANCE.getResourceVar();

    /**
     * The meta object literal for the '<em><b>Resource</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RESOURCE_VAR__RESOURCE = eINSTANCE.getResourceVar_Resource();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RESOURCE_VAR__TYPE = eINSTANCE.getResourceVar_Type();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceTypeImpl <em>Resource Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceTypeImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getResourceType()
     * @generated
     */
    EClass RESOURCE_TYPE = eINSTANCE.getResourceType();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.IntResourceTypeImpl <em>Int Resource Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.IntResourceTypeImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getIntResourceType()
     * @generated
     */
    EClass INT_RESOURCE_TYPE = eINSTANCE.getIntResourceType();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INT_RESOURCE_TYPE__VALUE = eINSTANCE.getIntResourceType_Value();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.BooleanResourceTypeImpl <em>Boolean Resource Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.BooleanResourceTypeImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getBooleanResourceType()
     * @generated
     */
    EClass BOOLEAN_RESOURCE_TYPE = eINSTANCE.getBooleanResourceType();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_RESOURCE_TYPE__VALUE = eINSTANCE.getBooleanResourceType_Value();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.CustomVarImpl <em>Custom Var</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.CustomVarImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getCustomVar()
     * @generated
     */
    EClass CUSTOM_VAR = eINSTANCE.getCustomVar();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CUSTOM_VAR__TYPE = eINSTANCE.getCustomVar_Type();

    /**
     * The meta object literal for the '<em><b>Initializer</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CUSTOM_VAR__INITIALIZER = eINSTANCE.getCustomVar_Initializer();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.IntVarImpl <em>Int Var</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.IntVarImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getIntVar()
     * @generated
     */
    EClass INT_VAR = eINSTANCE.getIntVar();

    /**
     * The meta object literal for the '<em><b>Int Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INT_VAR__INT_TYPE = eINSTANCE.getIntVar_IntType();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.BoolVarImpl <em>Bool Var</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.BoolVarImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getBoolVar()
     * @generated
     */
    EClass BOOL_VAR = eINSTANCE.getBoolVar();

    /**
     * The meta object literal for the '<em><b>Bool Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOL_VAR__BOOL_TYPE = eINSTANCE.getBoolVar_BoolType();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimerVarImpl <em>Timer Var</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimerVarImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getTimerVar()
     * @generated
     */
    EClass TIMER_VAR = eINSTANCE.getTimerVar();

    /**
     * The meta object literal for the '<em><b>Timer</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TIMER_VAR__TIMER = eINSTANCE.getTimerVar_Timer();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimerCallCommandImpl <em>Timer Call Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.TimerCallCommandImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getTimerCallCommand()
     * @generated
     */
    EClass TIMER_CALL_COMMAND = eINSTANCE.getTimerCallCommand();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TIMER_CALL_COMMAND__NAME = eINSTANCE.getTimerCallCommand_Name();

    /**
     * The meta object literal for the '<em><b>In</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TIMER_CALL_COMMAND__IN = eINSTANCE.getTimerCallCommand_In();

    /**
     * The meta object literal for the '<em><b>Pt</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TIMER_CALL_COMMAND__PT = eINSTANCE.getTimerCallCommand_Pt();

    /**
     * The meta object literal for the '<em><b>Q</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TIMER_CALL_COMMAND__Q = eINSTANCE.getTimerCallCommand_Q();

    /**
     * The meta object literal for the '<em><b>Et</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TIMER_CALL_COMMAND__ET = eINSTANCE.getTimerCallCommand_Et();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceImpl <em>Resource</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ResourceImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getResource()
     * @generated
     */
    EClass RESOURCE = eINSTANCE.getResource();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RESOURCE__VALUE = eINSTANCE.getResource_Value();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.InputBitResourceImpl <em>Input Bit Resource</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InputBitResourceImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getInputBitResource()
     * @generated
     */
    EClass INPUT_BIT_RESOURCE = eINSTANCE.getInputBitResource();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.OutputBitResourceImpl <em>Output Bit Resource</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.OutputBitResourceImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getOutputBitResource()
     * @generated
     */
    EClass OUTPUT_BIT_RESOURCE = eINSTANCE.getOutputBitResource();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.FlagByteResourceImpl <em>Flag Byte Resource</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.FlagByteResourceImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getFlagByteResource()
     * @generated
     */
    EClass FLAG_BYTE_RESOURCE = eINSTANCE.getFlagByteResource();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ObjectReferenceImpl <em>Object Reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.ObjectReferenceImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getObjectReference()
     * @generated
     */
    EClass OBJECT_REFERENCE = eINSTANCE.getObjectReference();

    /**
     * The meta object literal for the '<em><b>Entity</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBJECT_REFERENCE__ENTITY = eINSTANCE.getObjectReference_Entity();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.DotExpressionImpl <em>Dot Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.DotExpressionImpl
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getDotExpression()
     * @generated
     */
    EClass DOT_EXPRESSION = eINSTANCE.getDotExpression();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOT_EXPRESSION__REF = eINSTANCE.getDotExpression_Ref();

    /**
     * The meta object literal for the '<em><b>Tail</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOT_EXPRESSION__TAIL = eINSTANCE.getDotExpression_Tail();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.SimpleTypeEnum <em>Simple Type Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.SimpleTypeEnum
     * @see com.lucy.g3.iec61131.xtext.il.instructionList.impl.InstructionListPackageImpl#getSimpleTypeEnum()
     * @generated
     */
    EEnum SIMPLE_TYPE_ENUM = eINSTANCE.getSimpleTypeEnum();

  }

} //InstructionListPackage
