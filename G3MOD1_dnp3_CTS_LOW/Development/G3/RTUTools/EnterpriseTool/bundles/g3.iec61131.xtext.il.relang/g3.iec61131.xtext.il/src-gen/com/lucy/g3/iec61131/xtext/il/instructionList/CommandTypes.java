/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Command Types</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getCommandTypes()
 * @model
 * @generated
 */
public interface CommandTypes extends EObject
{
} // CommandTypes
