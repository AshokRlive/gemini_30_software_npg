/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.impl;

import com.lucy.g3.iec61131.xtext.il.instructionList.Command;
import com.lucy.g3.iec61131.xtext.il.instructionList.ILLabel;
import com.lucy.g3.iec61131.xtext.il.instructionList.InputVars;
import com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage;
import com.lucy.g3.iec61131.xtext.il.instructionList.OutputVars;
import com.lucy.g3.iec61131.xtext.il.instructionList.Program;
import com.lucy.g3.iec61131.xtext.il.instructionList.Vars;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Program</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ProgramImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ProgramImpl#getVars <em>Vars</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ProgramImpl#getInputVars <em>Input Vars</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ProgramImpl#getOutputVars <em>Output Vars</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ProgramImpl#getCommands <em>Commands</em>}</li>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.impl.ProgramImpl#getLabels <em>Labels</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProgramImpl extends MinimalEObjectImpl.Container implements Program
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getVars() <em>Vars</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVars()
   * @generated
   * @ordered
   */
  protected EList<Vars> vars;

  /**
   * The cached value of the '{@link #getInputVars() <em>Input Vars</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInputVars()
   * @generated
   * @ordered
   */
  protected EList<InputVars> inputVars;

  /**
   * The cached value of the '{@link #getOutputVars() <em>Output Vars</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOutputVars()
   * @generated
   * @ordered
   */
  protected EList<OutputVars> outputVars;

  /**
   * The cached value of the '{@link #getCommands() <em>Commands</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCommands()
   * @generated
   * @ordered
   */
  protected EList<Command> commands;

  /**
   * The cached value of the '{@link #getLabels() <em>Labels</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLabels()
   * @generated
   * @ordered
   */
  protected EList<ILLabel> labels;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ProgramImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return InstructionListPackage.Literals.PROGRAM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, InstructionListPackage.PROGRAM__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Vars> getVars()
  {
    if (vars == null)
    {
      vars = new EObjectContainmentEList<Vars>(Vars.class, this, InstructionListPackage.PROGRAM__VARS);
    }
    return vars;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<InputVars> getInputVars()
  {
    if (inputVars == null)
    {
      inputVars = new EObjectContainmentEList<InputVars>(InputVars.class, this, InstructionListPackage.PROGRAM__INPUT_VARS);
    }
    return inputVars;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<OutputVars> getOutputVars()
  {
    if (outputVars == null)
    {
      outputVars = new EObjectContainmentEList<OutputVars>(OutputVars.class, this, InstructionListPackage.PROGRAM__OUTPUT_VARS);
    }
    return outputVars;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Command> getCommands()
  {
    if (commands == null)
    {
      commands = new EObjectContainmentEList<Command>(Command.class, this, InstructionListPackage.PROGRAM__COMMANDS);
    }
    return commands;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ILLabel> getLabels()
  {
    if (labels == null)
    {
      labels = new EObjectContainmentEList<ILLabel>(ILLabel.class, this, InstructionListPackage.PROGRAM__LABELS);
    }
    return labels;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case InstructionListPackage.PROGRAM__VARS:
        return ((InternalEList<?>)getVars()).basicRemove(otherEnd, msgs);
      case InstructionListPackage.PROGRAM__INPUT_VARS:
        return ((InternalEList<?>)getInputVars()).basicRemove(otherEnd, msgs);
      case InstructionListPackage.PROGRAM__OUTPUT_VARS:
        return ((InternalEList<?>)getOutputVars()).basicRemove(otherEnd, msgs);
      case InstructionListPackage.PROGRAM__COMMANDS:
        return ((InternalEList<?>)getCommands()).basicRemove(otherEnd, msgs);
      case InstructionListPackage.PROGRAM__LABELS:
        return ((InternalEList<?>)getLabels()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case InstructionListPackage.PROGRAM__NAME:
        return getName();
      case InstructionListPackage.PROGRAM__VARS:
        return getVars();
      case InstructionListPackage.PROGRAM__INPUT_VARS:
        return getInputVars();
      case InstructionListPackage.PROGRAM__OUTPUT_VARS:
        return getOutputVars();
      case InstructionListPackage.PROGRAM__COMMANDS:
        return getCommands();
      case InstructionListPackage.PROGRAM__LABELS:
        return getLabels();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case InstructionListPackage.PROGRAM__NAME:
        setName((String)newValue);
        return;
      case InstructionListPackage.PROGRAM__VARS:
        getVars().clear();
        getVars().addAll((Collection<? extends Vars>)newValue);
        return;
      case InstructionListPackage.PROGRAM__INPUT_VARS:
        getInputVars().clear();
        getInputVars().addAll((Collection<? extends InputVars>)newValue);
        return;
      case InstructionListPackage.PROGRAM__OUTPUT_VARS:
        getOutputVars().clear();
        getOutputVars().addAll((Collection<? extends OutputVars>)newValue);
        return;
      case InstructionListPackage.PROGRAM__COMMANDS:
        getCommands().clear();
        getCommands().addAll((Collection<? extends Command>)newValue);
        return;
      case InstructionListPackage.PROGRAM__LABELS:
        getLabels().clear();
        getLabels().addAll((Collection<? extends ILLabel>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case InstructionListPackage.PROGRAM__NAME:
        setName(NAME_EDEFAULT);
        return;
      case InstructionListPackage.PROGRAM__VARS:
        getVars().clear();
        return;
      case InstructionListPackage.PROGRAM__INPUT_VARS:
        getInputVars().clear();
        return;
      case InstructionListPackage.PROGRAM__OUTPUT_VARS:
        getOutputVars().clear();
        return;
      case InstructionListPackage.PROGRAM__COMMANDS:
        getCommands().clear();
        return;
      case InstructionListPackage.PROGRAM__LABELS:
        getLabels().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case InstructionListPackage.PROGRAM__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case InstructionListPackage.PROGRAM__VARS:
        return vars != null && !vars.isEmpty();
      case InstructionListPackage.PROGRAM__INPUT_VARS:
        return inputVars != null && !inputVars.isEmpty();
      case InstructionListPackage.PROGRAM__OUTPUT_VARS:
        return outputVars != null && !outputVars.isEmpty();
      case InstructionListPackage.PROGRAM__COMMANDS:
        return commands != null && !commands.isEmpty();
      case InstructionListPackage.PROGRAM__LABELS:
        return labels != null && !labels.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //ProgramImpl
