package com.lucy.g3.iec61131.xtext.il.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import com.lucy.g3.iec61131.xtext.il.services.RuleEngineGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRuleEngineParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "END_FUNCTION_BLOCK", "FUNCTION_BLOCK", "END_FUNCTION", "END_PROGRAM", "VAR_OUTPUT", "EQNBOOL", "VAR_INPUT", "EQBOOL", "EQNINT", "FUNCTION", "BOOL_1", "END_VAR", "EQINT", "PROGRAM", "INT_1", "FALSE", "JMPCN", "TIME_1", "BOOL", "BYTE", "CHAR", "DATE", "DINT", "LINT", "SINT", "TIME", "TRUE", "WORD", "SpaceColonSpace", "IX", "MW", "QX", "ADD", "AND", "CAL", "DEL", "DIV", "INT", "JMP", "MUL", "RET", "SUB", "TON", "VAR", "XOR", "ColonEqualsSign", "EqualsSignGreaterThanSign", "AT", "ET", "IN", "LD", "OR", "PT", "ST", "T", "T_1", "Ampersand", "LeftParenthesis", "RightParenthesis", "Comma", "FullStop", "Colon", "Semicolon", "Q", "R", "S", "RULE_INT", "RULE_BEGIN", "RULE_END", "RULE_LETTER", "RULE_DIGIT", "RULE_IDENTIFIER", "RULE_DECIMAL", "RULE_FIX_POINT", "RULE_TIME", "RULE_HEX_DIGIT", "RULE_PARTIAL_ACCESS", "RULE_SL_COMMENT", "RULE_ML_COMMENT", "RULE_WS", "RULE_ID", "RULE_SINGLE_BYTE_CHAR_VALUE", "RULE_STRING", "RULE_CHAR_VALUE", "RULE_TEXT", "RULE_ANY_OTHER"
    };
    public static final int RET=44;
    public static final int ADD=36;
    public static final int BOOL_1=14;
    public static final int RULE_BEGIN=71;
    public static final int PT=56;
    public static final int FUNCTION_BLOCK=5;
    public static final int EqualsSignGreaterThanSign=50;
    public static final int VAR=47;
    public static final int T_1=59;
    public static final int END_VAR=15;
    public static final int LeftParenthesis=61;
    public static final int BYTE=23;
    public static final int RULE_TIME=78;
    public static final int Ampersand=60;
    public static final int SpaceColonSpace=32;
    public static final int LINT=27;
    public static final int SUB=45;
    public static final int WORD=31;
    public static final int RULE_ID=84;
    public static final int MUL=43;
    public static final int IN=53;
    public static final int QX=35;
    public static final int RULE_CHAR_VALUE=87;
    public static final int RightParenthesis=62;
    public static final int TRUE=30;
    public static final int RULE_DIGIT=74;
    public static final int MW=34;
    public static final int DINT=26;
    public static final int ColonEqualsSign=49;
    public static final int RULE_PARTIAL_ACCESS=80;
    public static final int FUNCTION=13;
    public static final int JMPCN=20;
    public static final int IX=33;
    public static final int CAL=38;
    public static final int ET=52;
    public static final int END_FUNCTION=6;
    public static final int RULE_DECIMAL=76;
    public static final int DATE=25;
    public static final int TON=46;
    public static final int AT=51;
    public static final int EQNINT=12;
    public static final int RULE_INT=70;
    public static final int AND=37;
    public static final int RULE_ML_COMMENT=82;
    public static final int RULE_SINGLE_BYTE_CHAR_VALUE=85;
    public static final int RULE_TEXT=88;
    public static final int SINT=28;
    public static final int XOR=48;
    public static final int EQNBOOL=9;
    public static final int PROGRAM=17;
    public static final int TIME_1=21;
    public static final int RULE_END=72;
    public static final int RULE_FIX_POINT=77;
    public static final int EQBOOL=11;
    public static final int END_FUNCTION_BLOCK=4;
    public static final int RULE_IDENTIFIER=75;
    public static final int CHAR=24;
    public static final int RULE_STRING=86;
    public static final int INT=41;
    public static final int RULE_SL_COMMENT=81;
    public static final int EQINT=16;
    public static final int Comma=63;
    public static final int Q=67;
    public static final int R=68;
    public static final int S=69;
    public static final int T=58;
    public static final int Colon=65;
    public static final int VAR_INPUT=10;
    public static final int EOF=-1;
    public static final int ST=57;
    public static final int FullStop=64;
    public static final int OR=55;
    public static final int RULE_WS=83;
    public static final int INT_1=18;
    public static final int TIME=29;
    public static final int DEL=39;
    public static final int RULE_ANY_OTHER=89;
    public static final int JMP=42;
    public static final int DIV=40;
    public static final int Semicolon=66;
    public static final int RULE_LETTER=73;
    public static final int BOOL=22;
    public static final int LD=54;
    public static final int VAR_OUTPUT=8;
    public static final int RULE_HEX_DIGIT=79;
    public static final int FALSE=19;
    public static final int END_PROGRAM=7;

    // delegates
    // delegators


        public InternalRuleEngineParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRuleEngineParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRuleEngineParser.tokenNames; }
    public String getGrammarFileName() { return "InternalRuleEngineParser.g"; }




    	private RuleEngineGrammarAccess grammarAccess;
    	 	
    	public InternalRuleEngineParser(TokenStream input, RuleEngineGrammarAccess grammarAccess) {
    		this(input);
    		this.grammarAccess = grammarAccess;
    		registerRules(grammarAccess.getGrammar());
    	}
    	
    	@Override
    	protected String getFirstRuleName() {
    		return "ILObject";	
    	} 
    	   	   	
    	@Override
    	protected RuleEngineGrammarAccess getGrammarAccess() {
    		return grammarAccess;
    	}



    // $ANTLR start "entryRuleILObject"
    // InternalRuleEngineParser.g:62:1: entryRuleILObject returns [EObject current=null] : iv_ruleILObject= ruleILObject EOF ;
    public final EObject entryRuleILObject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleILObject = null;


        try {
            // InternalRuleEngineParser.g:63:2: (iv_ruleILObject= ruleILObject EOF )
            // InternalRuleEngineParser.g:64:2: iv_ruleILObject= ruleILObject EOF
            {
             newCompositeNode(grammarAccess.getILObjectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleILObject=ruleILObject();

            state._fsp--;

             current =iv_ruleILObject; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleILObject"


    // $ANTLR start "ruleILObject"
    // InternalRuleEngineParser.g:71:1: ruleILObject returns [EObject current=null] : ( ( (lv_main_0_0= ruleMainProgramDeclaration ) ) ( (lv_functions_1_0= ruleFunctionDeclaration ) )* ( (lv_functionblocks_2_0= ruleFunctionBlockDeclaration ) )* ) ;
    public final EObject ruleILObject() throws RecognitionException {
        EObject current = null;

        EObject lv_main_0_0 = null;

        EObject lv_functions_1_0 = null;

        EObject lv_functionblocks_2_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:74:28: ( ( ( (lv_main_0_0= ruleMainProgramDeclaration ) ) ( (lv_functions_1_0= ruleFunctionDeclaration ) )* ( (lv_functionblocks_2_0= ruleFunctionBlockDeclaration ) )* ) )
            // InternalRuleEngineParser.g:75:1: ( ( (lv_main_0_0= ruleMainProgramDeclaration ) ) ( (lv_functions_1_0= ruleFunctionDeclaration ) )* ( (lv_functionblocks_2_0= ruleFunctionBlockDeclaration ) )* )
            {
            // InternalRuleEngineParser.g:75:1: ( ( (lv_main_0_0= ruleMainProgramDeclaration ) ) ( (lv_functions_1_0= ruleFunctionDeclaration ) )* ( (lv_functionblocks_2_0= ruleFunctionBlockDeclaration ) )* )
            // InternalRuleEngineParser.g:75:2: ( (lv_main_0_0= ruleMainProgramDeclaration ) ) ( (lv_functions_1_0= ruleFunctionDeclaration ) )* ( (lv_functionblocks_2_0= ruleFunctionBlockDeclaration ) )*
            {
            // InternalRuleEngineParser.g:75:2: ( (lv_main_0_0= ruleMainProgramDeclaration ) )
            // InternalRuleEngineParser.g:76:1: (lv_main_0_0= ruleMainProgramDeclaration )
            {
            // InternalRuleEngineParser.g:76:1: (lv_main_0_0= ruleMainProgramDeclaration )
            // InternalRuleEngineParser.g:77:3: lv_main_0_0= ruleMainProgramDeclaration
            {
             
            	        newCompositeNode(grammarAccess.getILObjectAccess().getMainMainProgramDeclarationParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_3);
            lv_main_0_0=ruleMainProgramDeclaration();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getILObjectRule());
            	        }
                   		set(
                   			current, 
                   			"main",
                    		lv_main_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.MainProgramDeclaration");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalRuleEngineParser.g:93:2: ( (lv_functions_1_0= ruleFunctionDeclaration ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==FUNCTION) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalRuleEngineParser.g:94:1: (lv_functions_1_0= ruleFunctionDeclaration )
            	    {
            	    // InternalRuleEngineParser.g:94:1: (lv_functions_1_0= ruleFunctionDeclaration )
            	    // InternalRuleEngineParser.g:95:3: lv_functions_1_0= ruleFunctionDeclaration
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getILObjectAccess().getFunctionsFunctionDeclarationParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_3);
            	    lv_functions_1_0=ruleFunctionDeclaration();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getILObjectRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"functions",
            	            		lv_functions_1_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.FunctionDeclaration");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalRuleEngineParser.g:111:3: ( (lv_functionblocks_2_0= ruleFunctionBlockDeclaration ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==FUNCTION_BLOCK) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalRuleEngineParser.g:112:1: (lv_functionblocks_2_0= ruleFunctionBlockDeclaration )
            	    {
            	    // InternalRuleEngineParser.g:112:1: (lv_functionblocks_2_0= ruleFunctionBlockDeclaration )
            	    // InternalRuleEngineParser.g:113:3: lv_functionblocks_2_0= ruleFunctionBlockDeclaration
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getILObjectAccess().getFunctionblocksFunctionBlockDeclarationParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_4);
            	    lv_functionblocks_2_0=ruleFunctionBlockDeclaration();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getILObjectRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"functionblocks",
            	            		lv_functionblocks_2_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.FunctionBlockDeclaration");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleILObject"


    // $ANTLR start "entryRuleMainProgramDeclaration"
    // InternalRuleEngineParser.g:137:1: entryRuleMainProgramDeclaration returns [EObject current=null] : iv_ruleMainProgramDeclaration= ruleMainProgramDeclaration EOF ;
    public final EObject entryRuleMainProgramDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMainProgramDeclaration = null;


        try {
            // InternalRuleEngineParser.g:138:2: (iv_ruleMainProgramDeclaration= ruleMainProgramDeclaration EOF )
            // InternalRuleEngineParser.g:139:2: iv_ruleMainProgramDeclaration= ruleMainProgramDeclaration EOF
            {
             newCompositeNode(grammarAccess.getMainProgramDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMainProgramDeclaration=ruleMainProgramDeclaration();

            state._fsp--;

             current =iv_ruleMainProgramDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMainProgramDeclaration"


    // $ANTLR start "ruleMainProgramDeclaration"
    // InternalRuleEngineParser.g:146:1: ruleMainProgramDeclaration returns [EObject current=null] : (otherlv_0= PROGRAM ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* ( (lv_labels_6_0= ruleILLabel ) )* otherlv_7= END_PROGRAM ) ;
    public final EObject ruleMainProgramDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_7=null;
        EObject lv_vars_2_0 = null;

        EObject lv_inputVars_3_0 = null;

        EObject lv_outputVars_4_0 = null;

        EObject lv_commands_5_0 = null;

        EObject lv_labels_6_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:149:28: ( (otherlv_0= PROGRAM ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* ( (lv_labels_6_0= ruleILLabel ) )* otherlv_7= END_PROGRAM ) )
            // InternalRuleEngineParser.g:150:1: (otherlv_0= PROGRAM ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* ( (lv_labels_6_0= ruleILLabel ) )* otherlv_7= END_PROGRAM )
            {
            // InternalRuleEngineParser.g:150:1: (otherlv_0= PROGRAM ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* ( (lv_labels_6_0= ruleILLabel ) )* otherlv_7= END_PROGRAM )
            // InternalRuleEngineParser.g:151:2: otherlv_0= PROGRAM ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* ( (lv_labels_6_0= ruleILLabel ) )* otherlv_7= END_PROGRAM
            {
            otherlv_0=(Token)match(input,PROGRAM,FOLLOW_5); 

                	newLeafNode(otherlv_0, grammarAccess.getMainProgramDeclarationAccess().getPROGRAMKeyword_0());
                
            // InternalRuleEngineParser.g:155:1: ( (lv_name_1_0= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:156:1: (lv_name_1_0= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:156:1: (lv_name_1_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:157:3: lv_name_1_0= RULE_IDENTIFIER
            {
            lv_name_1_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_6); 

            			newLeafNode(lv_name_1_0, grammarAccess.getMainProgramDeclarationAccess().getNameIDENTIFIERTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getMainProgramDeclarationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
            	    

            }


            }

            // InternalRuleEngineParser.g:173:2: ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )*
            loop3:
            do {
                int alt3=4;
                switch ( input.LA(1) ) {
                case VAR:
                    {
                    alt3=1;
                    }
                    break;
                case VAR_INPUT:
                    {
                    alt3=2;
                    }
                    break;
                case VAR_OUTPUT:
                    {
                    alt3=3;
                    }
                    break;

                }

                switch (alt3) {
            	case 1 :
            	    // InternalRuleEngineParser.g:173:3: ( (lv_vars_2_0= ruleVars ) )
            	    {
            	    // InternalRuleEngineParser.g:173:3: ( (lv_vars_2_0= ruleVars ) )
            	    // InternalRuleEngineParser.g:174:1: (lv_vars_2_0= ruleVars )
            	    {
            	    // InternalRuleEngineParser.g:174:1: (lv_vars_2_0= ruleVars )
            	    // InternalRuleEngineParser.g:175:3: lv_vars_2_0= ruleVars
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMainProgramDeclarationAccess().getVarsVarsParserRuleCall_2_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_6);
            	    lv_vars_2_0=ruleVars();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMainProgramDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"vars",
            	            		lv_vars_2_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Vars");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalRuleEngineParser.g:192:6: ( (lv_inputVars_3_0= ruleInputVars ) )
            	    {
            	    // InternalRuleEngineParser.g:192:6: ( (lv_inputVars_3_0= ruleInputVars ) )
            	    // InternalRuleEngineParser.g:193:1: (lv_inputVars_3_0= ruleInputVars )
            	    {
            	    // InternalRuleEngineParser.g:193:1: (lv_inputVars_3_0= ruleInputVars )
            	    // InternalRuleEngineParser.g:194:3: lv_inputVars_3_0= ruleInputVars
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMainProgramDeclarationAccess().getInputVarsInputVarsParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_6);
            	    lv_inputVars_3_0=ruleInputVars();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMainProgramDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"inputVars",
            	            		lv_inputVars_3_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.InputVars");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalRuleEngineParser.g:211:6: ( (lv_outputVars_4_0= ruleOutputVars ) )
            	    {
            	    // InternalRuleEngineParser.g:211:6: ( (lv_outputVars_4_0= ruleOutputVars ) )
            	    // InternalRuleEngineParser.g:212:1: (lv_outputVars_4_0= ruleOutputVars )
            	    {
            	    // InternalRuleEngineParser.g:212:1: (lv_outputVars_4_0= ruleOutputVars )
            	    // InternalRuleEngineParser.g:213:3: lv_outputVars_4_0= ruleOutputVars
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMainProgramDeclarationAccess().getOutputVarsOutputVarsParserRuleCall_2_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_6);
            	    lv_outputVars_4_0=ruleOutputVars();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMainProgramDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"outputVars",
            	            		lv_outputVars_4_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.OutputVars");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            // InternalRuleEngineParser.g:229:4: ( (lv_commands_5_0= ruleCommand ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==EQNBOOL||(LA4_0>=EQBOOL && LA4_0<=EQNINT)||LA4_0==EQINT||LA4_0==JMPCN||(LA4_0>=ADD && LA4_0<=DIV)||(LA4_0>=JMP && LA4_0<=SUB)||LA4_0==XOR||(LA4_0>=LD && LA4_0<=OR)||LA4_0==ST||LA4_0==Ampersand||(LA4_0>=R && LA4_0<=S)||LA4_0==RULE_ID) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalRuleEngineParser.g:230:1: (lv_commands_5_0= ruleCommand )
            	    {
            	    // InternalRuleEngineParser.g:230:1: (lv_commands_5_0= ruleCommand )
            	    // InternalRuleEngineParser.g:231:3: lv_commands_5_0= ruleCommand
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMainProgramDeclarationAccess().getCommandsCommandParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_7);
            	    lv_commands_5_0=ruleCommand();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMainProgramDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"commands",
            	            		lv_commands_5_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Command");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            // InternalRuleEngineParser.g:247:3: ( (lv_labels_6_0= ruleILLabel ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_IDENTIFIER) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalRuleEngineParser.g:248:1: (lv_labels_6_0= ruleILLabel )
            	    {
            	    // InternalRuleEngineParser.g:248:1: (lv_labels_6_0= ruleILLabel )
            	    // InternalRuleEngineParser.g:249:3: lv_labels_6_0= ruleILLabel
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMainProgramDeclarationAccess().getLabelsILLabelParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_8);
            	    lv_labels_6_0=ruleILLabel();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMainProgramDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"labels",
            	            		lv_labels_6_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ILLabel");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_7=(Token)match(input,END_PROGRAM,FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getMainProgramDeclarationAccess().getEND_PROGRAMKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMainProgramDeclaration"


    // $ANTLR start "entryRuleFunctionBlockDeclaration"
    // InternalRuleEngineParser.g:278:1: entryRuleFunctionBlockDeclaration returns [EObject current=null] : iv_ruleFunctionBlockDeclaration= ruleFunctionBlockDeclaration EOF ;
    public final EObject entryRuleFunctionBlockDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionBlockDeclaration = null;


        try {
            // InternalRuleEngineParser.g:279:2: (iv_ruleFunctionBlockDeclaration= ruleFunctionBlockDeclaration EOF )
            // InternalRuleEngineParser.g:280:2: iv_ruleFunctionBlockDeclaration= ruleFunctionBlockDeclaration EOF
            {
             newCompositeNode(grammarAccess.getFunctionBlockDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunctionBlockDeclaration=ruleFunctionBlockDeclaration();

            state._fsp--;

             current =iv_ruleFunctionBlockDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionBlockDeclaration"


    // $ANTLR start "ruleFunctionBlockDeclaration"
    // InternalRuleEngineParser.g:287:1: ruleFunctionBlockDeclaration returns [EObject current=null] : this_DefinedFunctionBlockDeclaration_0= ruleDefinedFunctionBlockDeclaration ;
    public final EObject ruleFunctionBlockDeclaration() throws RecognitionException {
        EObject current = null;

        EObject this_DefinedFunctionBlockDeclaration_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:290:28: (this_DefinedFunctionBlockDeclaration_0= ruleDefinedFunctionBlockDeclaration )
            // InternalRuleEngineParser.g:292:5: this_DefinedFunctionBlockDeclaration_0= ruleDefinedFunctionBlockDeclaration
            {
             
                    newCompositeNode(grammarAccess.getFunctionBlockDeclarationAccess().getDefinedFunctionBlockDeclarationParserRuleCall()); 
                
            pushFollow(FOLLOW_2);
            this_DefinedFunctionBlockDeclaration_0=ruleDefinedFunctionBlockDeclaration();

            state._fsp--;


                    current = this_DefinedFunctionBlockDeclaration_0;
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionBlockDeclaration"


    // $ANTLR start "entryRuleDefinedFunctionBlockDeclaration"
    // InternalRuleEngineParser.g:308:1: entryRuleDefinedFunctionBlockDeclaration returns [EObject current=null] : iv_ruleDefinedFunctionBlockDeclaration= ruleDefinedFunctionBlockDeclaration EOF ;
    public final EObject entryRuleDefinedFunctionBlockDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefinedFunctionBlockDeclaration = null;


        try {
            // InternalRuleEngineParser.g:309:2: (iv_ruleDefinedFunctionBlockDeclaration= ruleDefinedFunctionBlockDeclaration EOF )
            // InternalRuleEngineParser.g:310:2: iv_ruleDefinedFunctionBlockDeclaration= ruleDefinedFunctionBlockDeclaration EOF
            {
             newCompositeNode(grammarAccess.getDefinedFunctionBlockDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDefinedFunctionBlockDeclaration=ruleDefinedFunctionBlockDeclaration();

            state._fsp--;

             current =iv_ruleDefinedFunctionBlockDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefinedFunctionBlockDeclaration"


    // $ANTLR start "ruleDefinedFunctionBlockDeclaration"
    // InternalRuleEngineParser.g:317:1: ruleDefinedFunctionBlockDeclaration returns [EObject current=null] : (otherlv_0= FUNCTION_BLOCK ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* ( (lv_labels_6_0= ruleILLabel ) )* otherlv_7= END_FUNCTION_BLOCK ) ;
    public final EObject ruleDefinedFunctionBlockDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_7=null;
        EObject lv_vars_2_0 = null;

        EObject lv_inputVars_3_0 = null;

        EObject lv_outputVars_4_0 = null;

        EObject lv_commands_5_0 = null;

        EObject lv_labels_6_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:320:28: ( (otherlv_0= FUNCTION_BLOCK ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* ( (lv_labels_6_0= ruleILLabel ) )* otherlv_7= END_FUNCTION_BLOCK ) )
            // InternalRuleEngineParser.g:321:1: (otherlv_0= FUNCTION_BLOCK ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* ( (lv_labels_6_0= ruleILLabel ) )* otherlv_7= END_FUNCTION_BLOCK )
            {
            // InternalRuleEngineParser.g:321:1: (otherlv_0= FUNCTION_BLOCK ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* ( (lv_labels_6_0= ruleILLabel ) )* otherlv_7= END_FUNCTION_BLOCK )
            // InternalRuleEngineParser.g:322:2: otherlv_0= FUNCTION_BLOCK ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* ( (lv_labels_6_0= ruleILLabel ) )* otherlv_7= END_FUNCTION_BLOCK
            {
            otherlv_0=(Token)match(input,FUNCTION_BLOCK,FOLLOW_5); 

                	newLeafNode(otherlv_0, grammarAccess.getDefinedFunctionBlockDeclarationAccess().getFUNCTION_BLOCKKeyword_0());
                
            // InternalRuleEngineParser.g:326:1: ( (lv_name_1_0= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:327:1: (lv_name_1_0= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:327:1: (lv_name_1_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:328:3: lv_name_1_0= RULE_IDENTIFIER
            {
            lv_name_1_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_9); 

            			newLeafNode(lv_name_1_0, grammarAccess.getDefinedFunctionBlockDeclarationAccess().getNameIDENTIFIERTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDefinedFunctionBlockDeclarationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
            	    

            }


            }

            // InternalRuleEngineParser.g:344:2: ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )*
            loop6:
            do {
                int alt6=4;
                switch ( input.LA(1) ) {
                case VAR:
                    {
                    alt6=1;
                    }
                    break;
                case VAR_INPUT:
                    {
                    alt6=2;
                    }
                    break;
                case VAR_OUTPUT:
                    {
                    alt6=3;
                    }
                    break;

                }

                switch (alt6) {
            	case 1 :
            	    // InternalRuleEngineParser.g:344:3: ( (lv_vars_2_0= ruleVars ) )
            	    {
            	    // InternalRuleEngineParser.g:344:3: ( (lv_vars_2_0= ruleVars ) )
            	    // InternalRuleEngineParser.g:345:1: (lv_vars_2_0= ruleVars )
            	    {
            	    // InternalRuleEngineParser.g:345:1: (lv_vars_2_0= ruleVars )
            	    // InternalRuleEngineParser.g:346:3: lv_vars_2_0= ruleVars
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDefinedFunctionBlockDeclarationAccess().getVarsVarsParserRuleCall_2_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_9);
            	    lv_vars_2_0=ruleVars();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDefinedFunctionBlockDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"vars",
            	            		lv_vars_2_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Vars");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalRuleEngineParser.g:363:6: ( (lv_inputVars_3_0= ruleInputVars ) )
            	    {
            	    // InternalRuleEngineParser.g:363:6: ( (lv_inputVars_3_0= ruleInputVars ) )
            	    // InternalRuleEngineParser.g:364:1: (lv_inputVars_3_0= ruleInputVars )
            	    {
            	    // InternalRuleEngineParser.g:364:1: (lv_inputVars_3_0= ruleInputVars )
            	    // InternalRuleEngineParser.g:365:3: lv_inputVars_3_0= ruleInputVars
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDefinedFunctionBlockDeclarationAccess().getInputVarsInputVarsParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_9);
            	    lv_inputVars_3_0=ruleInputVars();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDefinedFunctionBlockDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"inputVars",
            	            		lv_inputVars_3_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.InputVars");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalRuleEngineParser.g:382:6: ( (lv_outputVars_4_0= ruleOutputVars ) )
            	    {
            	    // InternalRuleEngineParser.g:382:6: ( (lv_outputVars_4_0= ruleOutputVars ) )
            	    // InternalRuleEngineParser.g:383:1: (lv_outputVars_4_0= ruleOutputVars )
            	    {
            	    // InternalRuleEngineParser.g:383:1: (lv_outputVars_4_0= ruleOutputVars )
            	    // InternalRuleEngineParser.g:384:3: lv_outputVars_4_0= ruleOutputVars
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDefinedFunctionBlockDeclarationAccess().getOutputVarsOutputVarsParserRuleCall_2_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_9);
            	    lv_outputVars_4_0=ruleOutputVars();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDefinedFunctionBlockDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"outputVars",
            	            		lv_outputVars_4_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.OutputVars");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // InternalRuleEngineParser.g:400:4: ( (lv_commands_5_0= ruleCommand ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==EQNBOOL||(LA7_0>=EQBOOL && LA7_0<=EQNINT)||LA7_0==EQINT||LA7_0==JMPCN||(LA7_0>=ADD && LA7_0<=DIV)||(LA7_0>=JMP && LA7_0<=SUB)||LA7_0==XOR||(LA7_0>=LD && LA7_0<=OR)||LA7_0==ST||LA7_0==Ampersand||(LA7_0>=R && LA7_0<=S)||LA7_0==RULE_ID) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalRuleEngineParser.g:401:1: (lv_commands_5_0= ruleCommand )
            	    {
            	    // InternalRuleEngineParser.g:401:1: (lv_commands_5_0= ruleCommand )
            	    // InternalRuleEngineParser.g:402:3: lv_commands_5_0= ruleCommand
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDefinedFunctionBlockDeclarationAccess().getCommandsCommandParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_10);
            	    lv_commands_5_0=ruleCommand();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDefinedFunctionBlockDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"commands",
            	            		lv_commands_5_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Command");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            // InternalRuleEngineParser.g:418:3: ( (lv_labels_6_0= ruleILLabel ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==RULE_IDENTIFIER) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalRuleEngineParser.g:419:1: (lv_labels_6_0= ruleILLabel )
            	    {
            	    // InternalRuleEngineParser.g:419:1: (lv_labels_6_0= ruleILLabel )
            	    // InternalRuleEngineParser.g:420:3: lv_labels_6_0= ruleILLabel
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDefinedFunctionBlockDeclarationAccess().getLabelsILLabelParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_11);
            	    lv_labels_6_0=ruleILLabel();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDefinedFunctionBlockDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"labels",
            	            		lv_labels_6_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ILLabel");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            otherlv_7=(Token)match(input,END_FUNCTION_BLOCK,FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getDefinedFunctionBlockDeclarationAccess().getEND_FUNCTION_BLOCKKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefinedFunctionBlockDeclaration"


    // $ANTLR start "entryRuleFunctionDeclaration"
    // InternalRuleEngineParser.g:449:1: entryRuleFunctionDeclaration returns [EObject current=null] : iv_ruleFunctionDeclaration= ruleFunctionDeclaration EOF ;
    public final EObject entryRuleFunctionDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionDeclaration = null;


        try {
            // InternalRuleEngineParser.g:450:2: (iv_ruleFunctionDeclaration= ruleFunctionDeclaration EOF )
            // InternalRuleEngineParser.g:451:2: iv_ruleFunctionDeclaration= ruleFunctionDeclaration EOF
            {
             newCompositeNode(grammarAccess.getFunctionDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunctionDeclaration=ruleFunctionDeclaration();

            state._fsp--;

             current =iv_ruleFunctionDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionDeclaration"


    // $ANTLR start "ruleFunctionDeclaration"
    // InternalRuleEngineParser.g:458:1: ruleFunctionDeclaration returns [EObject current=null] : (otherlv_0= FUNCTION ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* otherlv_6= END_FUNCTION ) ;
    public final EObject ruleFunctionDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_6=null;
        EObject lv_vars_2_0 = null;

        EObject lv_inputVars_3_0 = null;

        EObject lv_outputVars_4_0 = null;

        EObject lv_commands_5_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:461:28: ( (otherlv_0= FUNCTION ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* otherlv_6= END_FUNCTION ) )
            // InternalRuleEngineParser.g:462:1: (otherlv_0= FUNCTION ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* otherlv_6= END_FUNCTION )
            {
            // InternalRuleEngineParser.g:462:1: (otherlv_0= FUNCTION ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* otherlv_6= END_FUNCTION )
            // InternalRuleEngineParser.g:463:2: otherlv_0= FUNCTION ( (lv_name_1_0= RULE_IDENTIFIER ) ) ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )* ( (lv_commands_5_0= ruleCommand ) )* otherlv_6= END_FUNCTION
            {
            otherlv_0=(Token)match(input,FUNCTION,FOLLOW_5); 

                	newLeafNode(otherlv_0, grammarAccess.getFunctionDeclarationAccess().getFUNCTIONKeyword_0());
                
            // InternalRuleEngineParser.g:467:1: ( (lv_name_1_0= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:468:1: (lv_name_1_0= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:468:1: (lv_name_1_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:469:3: lv_name_1_0= RULE_IDENTIFIER
            {
            lv_name_1_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_12); 

            			newLeafNode(lv_name_1_0, grammarAccess.getFunctionDeclarationAccess().getNameIDENTIFIERTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFunctionDeclarationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
            	    

            }


            }

            // InternalRuleEngineParser.g:485:2: ( ( (lv_vars_2_0= ruleVars ) ) | ( (lv_inputVars_3_0= ruleInputVars ) ) | ( (lv_outputVars_4_0= ruleOutputVars ) ) )*
            loop9:
            do {
                int alt9=4;
                switch ( input.LA(1) ) {
                case VAR:
                    {
                    alt9=1;
                    }
                    break;
                case VAR_INPUT:
                    {
                    alt9=2;
                    }
                    break;
                case VAR_OUTPUT:
                    {
                    alt9=3;
                    }
                    break;

                }

                switch (alt9) {
            	case 1 :
            	    // InternalRuleEngineParser.g:485:3: ( (lv_vars_2_0= ruleVars ) )
            	    {
            	    // InternalRuleEngineParser.g:485:3: ( (lv_vars_2_0= ruleVars ) )
            	    // InternalRuleEngineParser.g:486:1: (lv_vars_2_0= ruleVars )
            	    {
            	    // InternalRuleEngineParser.g:486:1: (lv_vars_2_0= ruleVars )
            	    // InternalRuleEngineParser.g:487:3: lv_vars_2_0= ruleVars
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFunctionDeclarationAccess().getVarsVarsParserRuleCall_2_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_12);
            	    lv_vars_2_0=ruleVars();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFunctionDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"vars",
            	            		lv_vars_2_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Vars");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalRuleEngineParser.g:504:6: ( (lv_inputVars_3_0= ruleInputVars ) )
            	    {
            	    // InternalRuleEngineParser.g:504:6: ( (lv_inputVars_3_0= ruleInputVars ) )
            	    // InternalRuleEngineParser.g:505:1: (lv_inputVars_3_0= ruleInputVars )
            	    {
            	    // InternalRuleEngineParser.g:505:1: (lv_inputVars_3_0= ruleInputVars )
            	    // InternalRuleEngineParser.g:506:3: lv_inputVars_3_0= ruleInputVars
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFunctionDeclarationAccess().getInputVarsInputVarsParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_12);
            	    lv_inputVars_3_0=ruleInputVars();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFunctionDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"inputVars",
            	            		lv_inputVars_3_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.InputVars");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalRuleEngineParser.g:523:6: ( (lv_outputVars_4_0= ruleOutputVars ) )
            	    {
            	    // InternalRuleEngineParser.g:523:6: ( (lv_outputVars_4_0= ruleOutputVars ) )
            	    // InternalRuleEngineParser.g:524:1: (lv_outputVars_4_0= ruleOutputVars )
            	    {
            	    // InternalRuleEngineParser.g:524:1: (lv_outputVars_4_0= ruleOutputVars )
            	    // InternalRuleEngineParser.g:525:3: lv_outputVars_4_0= ruleOutputVars
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFunctionDeclarationAccess().getOutputVarsOutputVarsParserRuleCall_2_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_12);
            	    lv_outputVars_4_0=ruleOutputVars();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFunctionDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"outputVars",
            	            		lv_outputVars_4_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.OutputVars");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            // InternalRuleEngineParser.g:541:4: ( (lv_commands_5_0= ruleCommand ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==EQNBOOL||(LA10_0>=EQBOOL && LA10_0<=EQNINT)||LA10_0==EQINT||LA10_0==JMPCN||(LA10_0>=ADD && LA10_0<=DIV)||(LA10_0>=JMP && LA10_0<=SUB)||LA10_0==XOR||(LA10_0>=LD && LA10_0<=OR)||LA10_0==ST||LA10_0==Ampersand||(LA10_0>=R && LA10_0<=S)||LA10_0==RULE_ID) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalRuleEngineParser.g:542:1: (lv_commands_5_0= ruleCommand )
            	    {
            	    // InternalRuleEngineParser.g:542:1: (lv_commands_5_0= ruleCommand )
            	    // InternalRuleEngineParser.g:543:3: lv_commands_5_0= ruleCommand
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFunctionDeclarationAccess().getCommandsCommandParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_13);
            	    lv_commands_5_0=ruleCommand();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFunctionDeclarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"commands",
            	            		lv_commands_5_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Command");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            otherlv_6=(Token)match(input,END_FUNCTION,FOLLOW_2); 

                	newLeafNode(otherlv_6, grammarAccess.getFunctionDeclarationAccess().getEND_FUNCTIONKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionDeclaration"


    // $ANTLR start "entryRuleObjectReference"
    // InternalRuleEngineParser.g:574:1: entryRuleObjectReference returns [EObject current=null] : iv_ruleObjectReference= ruleObjectReference EOF ;
    public final EObject entryRuleObjectReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectReference = null;


        try {
            // InternalRuleEngineParser.g:575:2: (iv_ruleObjectReference= ruleObjectReference EOF )
            // InternalRuleEngineParser.g:576:2: iv_ruleObjectReference= ruleObjectReference EOF
            {
             newCompositeNode(grammarAccess.getObjectReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleObjectReference=ruleObjectReference();

            state._fsp--;

             current =iv_ruleObjectReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectReference"


    // $ANTLR start "ruleObjectReference"
    // InternalRuleEngineParser.g:583:1: ruleObjectReference returns [EObject current=null] : ( () ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleObjectReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:586:28: ( ( () ( (otherlv_1= RULE_ID ) ) ) )
            // InternalRuleEngineParser.g:587:1: ( () ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalRuleEngineParser.g:587:1: ( () ( (otherlv_1= RULE_ID ) ) )
            // InternalRuleEngineParser.g:587:2: () ( (otherlv_1= RULE_ID ) )
            {
            // InternalRuleEngineParser.g:587:2: ()
            // InternalRuleEngineParser.g:588:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getObjectReferenceAccess().getObjectReferenceAction_0(),
                        current);
                

            }

            // InternalRuleEngineParser.g:593:2: ( (otherlv_1= RULE_ID ) )
            // InternalRuleEngineParser.g:594:1: (otherlv_1= RULE_ID )
            {
            // InternalRuleEngineParser.g:594:1: (otherlv_1= RULE_ID )
            // InternalRuleEngineParser.g:595:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getObjectReferenceRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

            		newLeafNode(otherlv_1, grammarAccess.getObjectReferenceAccess().getEntityFunctionBlockCrossReference_1_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectReference"


    // $ANTLR start "entryRuleIntType"
    // InternalRuleEngineParser.g:616:1: entryRuleIntType returns [String current=null] : iv_ruleIntType= ruleIntType EOF ;
    public final String entryRuleIntType() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleIntType = null;


        try {
            // InternalRuleEngineParser.g:617:1: (iv_ruleIntType= ruleIntType EOF )
            // InternalRuleEngineParser.g:618:2: iv_ruleIntType= ruleIntType EOF
            {
             newCompositeNode(grammarAccess.getIntTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntType=ruleIntType();

            state._fsp--;

             current =iv_ruleIntType.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntType"


    // $ANTLR start "ruleIntType"
    // InternalRuleEngineParser.g:625:1: ruleIntType returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INT_0= RULE_INT ;
    public final AntlrDatatypeRuleToken ruleIntType() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:629:6: (this_INT_0= RULE_INT )
            // InternalRuleEngineParser.g:630:5: this_INT_0= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_2); 

            		current.merge(this_INT_0);
                
             
                newLeafNode(this_INT_0, grammarAccess.getIntTypeAccess().getINTTerminalRuleCall()); 
                

            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntType"


    // $ANTLR start "entryRuleRCommand"
    // InternalRuleEngineParser.g:645:1: entryRuleRCommand returns [EObject current=null] : iv_ruleRCommand= ruleRCommand EOF ;
    public final EObject entryRuleRCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRCommand = null;


        try {
            // InternalRuleEngineParser.g:646:2: (iv_ruleRCommand= ruleRCommand EOF )
            // InternalRuleEngineParser.g:647:2: iv_ruleRCommand= ruleRCommand EOF
            {
             newCompositeNode(grammarAccess.getRCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRCommand=ruleRCommand();

            state._fsp--;

             current =iv_ruleRCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRCommand"


    // $ANTLR start "ruleRCommand"
    // InternalRuleEngineParser.g:654:1: ruleRCommand returns [EObject current=null] : (otherlv_0= R ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleRCommand() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:657:28: ( (otherlv_0= R ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalRuleEngineParser.g:658:1: (otherlv_0= R ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalRuleEngineParser.g:658:1: (otherlv_0= R ( (lv_name_1_0= RULE_ID ) ) )
            // InternalRuleEngineParser.g:659:2: otherlv_0= R ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,R,FOLLOW_14); 

                	newLeafNode(otherlv_0, grammarAccess.getRCommandAccess().getRKeyword_0());
                
            // InternalRuleEngineParser.g:663:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalRuleEngineParser.g:664:1: (lv_name_1_0= RULE_ID )
            {
            // InternalRuleEngineParser.g:664:1: (lv_name_1_0= RULE_ID )
            // InternalRuleEngineParser.g:665:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            			newLeafNode(lv_name_1_0, grammarAccess.getRCommandAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRCommandRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRCommand"


    // $ANTLR start "entryRuleSCommand"
    // InternalRuleEngineParser.g:689:1: entryRuleSCommand returns [EObject current=null] : iv_ruleSCommand= ruleSCommand EOF ;
    public final EObject entryRuleSCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSCommand = null;


        try {
            // InternalRuleEngineParser.g:690:2: (iv_ruleSCommand= ruleSCommand EOF )
            // InternalRuleEngineParser.g:691:2: iv_ruleSCommand= ruleSCommand EOF
            {
             newCompositeNode(grammarAccess.getSCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSCommand=ruleSCommand();

            state._fsp--;

             current =iv_ruleSCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSCommand"


    // $ANTLR start "ruleSCommand"
    // InternalRuleEngineParser.g:698:1: ruleSCommand returns [EObject current=null] : (otherlv_0= S ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleSCommand() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:701:28: ( (otherlv_0= S ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalRuleEngineParser.g:702:1: (otherlv_0= S ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalRuleEngineParser.g:702:1: (otherlv_0= S ( (lv_name_1_0= RULE_ID ) ) )
            // InternalRuleEngineParser.g:703:2: otherlv_0= S ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,S,FOLLOW_14); 

                	newLeafNode(otherlv_0, grammarAccess.getSCommandAccess().getSKeyword_0());
                
            // InternalRuleEngineParser.g:707:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalRuleEngineParser.g:708:1: (lv_name_1_0= RULE_ID )
            {
            // InternalRuleEngineParser.g:708:1: (lv_name_1_0= RULE_ID )
            // InternalRuleEngineParser.g:709:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            			newLeafNode(lv_name_1_0, grammarAccess.getSCommandAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSCommandRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSCommand"


    // $ANTLR start "entryRuleILLabel"
    // InternalRuleEngineParser.g:733:1: entryRuleILLabel returns [EObject current=null] : iv_ruleILLabel= ruleILLabel EOF ;
    public final EObject entryRuleILLabel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleILLabel = null;


        try {
            // InternalRuleEngineParser.g:734:2: (iv_ruleILLabel= ruleILLabel EOF )
            // InternalRuleEngineParser.g:735:2: iv_ruleILLabel= ruleILLabel EOF
            {
             newCompositeNode(grammarAccess.getILLabelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleILLabel=ruleILLabel();

            state._fsp--;

             current =iv_ruleILLabel; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleILLabel"


    // $ANTLR start "ruleILLabel"
    // InternalRuleEngineParser.g:742:1: ruleILLabel returns [EObject current=null] : ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_initialCommand_2_0= ruleCommand ) ) (this_BEGIN_3= RULE_BEGIN ( (lv_commands_4_0= ruleCommand ) )* this_END_5= RULE_END )? ) ;
    public final EObject ruleILLabel() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token this_BEGIN_3=null;
        Token this_END_5=null;
        EObject lv_initialCommand_2_0 = null;

        EObject lv_commands_4_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:745:28: ( ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_initialCommand_2_0= ruleCommand ) ) (this_BEGIN_3= RULE_BEGIN ( (lv_commands_4_0= ruleCommand ) )* this_END_5= RULE_END )? ) )
            // InternalRuleEngineParser.g:746:1: ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_initialCommand_2_0= ruleCommand ) ) (this_BEGIN_3= RULE_BEGIN ( (lv_commands_4_0= ruleCommand ) )* this_END_5= RULE_END )? )
            {
            // InternalRuleEngineParser.g:746:1: ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_initialCommand_2_0= ruleCommand ) ) (this_BEGIN_3= RULE_BEGIN ( (lv_commands_4_0= ruleCommand ) )* this_END_5= RULE_END )? )
            // InternalRuleEngineParser.g:746:2: ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_initialCommand_2_0= ruleCommand ) ) (this_BEGIN_3= RULE_BEGIN ( (lv_commands_4_0= ruleCommand ) )* this_END_5= RULE_END )?
            {
            // InternalRuleEngineParser.g:746:2: ( (lv_name_0_0= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:747:1: (lv_name_0_0= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:747:1: (lv_name_0_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:748:3: lv_name_0_0= RULE_IDENTIFIER
            {
            lv_name_0_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_15); 

            			newLeafNode(lv_name_0_0, grammarAccess.getILLabelAccess().getNameIDENTIFIERTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getILLabelRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
            	    

            }


            }

            otherlv_1=(Token)match(input,Colon,FOLLOW_16); 

                	newLeafNode(otherlv_1, grammarAccess.getILLabelAccess().getColonKeyword_1());
                
            // InternalRuleEngineParser.g:769:1: ( (lv_initialCommand_2_0= ruleCommand ) )
            // InternalRuleEngineParser.g:770:1: (lv_initialCommand_2_0= ruleCommand )
            {
            // InternalRuleEngineParser.g:770:1: (lv_initialCommand_2_0= ruleCommand )
            // InternalRuleEngineParser.g:771:3: lv_initialCommand_2_0= ruleCommand
            {
             
            	        newCompositeNode(grammarAccess.getILLabelAccess().getInitialCommandCommandParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_17);
            lv_initialCommand_2_0=ruleCommand();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getILLabelRule());
            	        }
                   		set(
                   			current, 
                   			"initialCommand",
                    		lv_initialCommand_2_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Command");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalRuleEngineParser.g:787:2: (this_BEGIN_3= RULE_BEGIN ( (lv_commands_4_0= ruleCommand ) )* this_END_5= RULE_END )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_BEGIN) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalRuleEngineParser.g:787:3: this_BEGIN_3= RULE_BEGIN ( (lv_commands_4_0= ruleCommand ) )* this_END_5= RULE_END
                    {
                    this_BEGIN_3=(Token)match(input,RULE_BEGIN,FOLLOW_18); 
                     
                        newLeafNode(this_BEGIN_3, grammarAccess.getILLabelAccess().getBEGINTerminalRuleCall_3_0()); 
                        
                    // InternalRuleEngineParser.g:791:1: ( (lv_commands_4_0= ruleCommand ) )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==EQNBOOL||(LA11_0>=EQBOOL && LA11_0<=EQNINT)||LA11_0==EQINT||LA11_0==JMPCN||(LA11_0>=ADD && LA11_0<=DIV)||(LA11_0>=JMP && LA11_0<=SUB)||LA11_0==XOR||(LA11_0>=LD && LA11_0<=OR)||LA11_0==ST||LA11_0==Ampersand||(LA11_0>=R && LA11_0<=S)||LA11_0==RULE_ID) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // InternalRuleEngineParser.g:792:1: (lv_commands_4_0= ruleCommand )
                    	    {
                    	    // InternalRuleEngineParser.g:792:1: (lv_commands_4_0= ruleCommand )
                    	    // InternalRuleEngineParser.g:793:3: lv_commands_4_0= ruleCommand
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getILLabelAccess().getCommandsCommandParserRuleCall_3_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_18);
                    	    lv_commands_4_0=ruleCommand();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getILLabelRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"commands",
                    	            		lv_commands_4_0, 
                    	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Command");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);

                    this_END_5=(Token)match(input,RULE_END,FOLLOW_2); 
                     
                        newLeafNode(this_END_5, grammarAccess.getILLabelAccess().getENDTerminalRuleCall_3_2()); 
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleILLabel"


    // $ANTLR start "entryRuleFunctionCall"
    // InternalRuleEngineParser.g:821:1: entryRuleFunctionCall returns [EObject current=null] : iv_ruleFunctionCall= ruleFunctionCall EOF ;
    public final EObject entryRuleFunctionCall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionCall = null;


        try {
            // InternalRuleEngineParser.g:822:2: (iv_ruleFunctionCall= ruleFunctionCall EOF )
            // InternalRuleEngineParser.g:823:2: iv_ruleFunctionCall= ruleFunctionCall EOF
            {
             newCompositeNode(grammarAccess.getFunctionCallRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunctionCall=ruleFunctionCall();

            state._fsp--;

             current =iv_ruleFunctionCall; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionCall"


    // $ANTLR start "ruleFunctionCall"
    // InternalRuleEngineParser.g:830:1: ruleFunctionCall returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_arguments_1_0= ruleFunctionInput ) ) ) ;
    public final EObject ruleFunctionCall() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        AntlrDatatypeRuleToken lv_arguments_1_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:833:28: ( ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_arguments_1_0= ruleFunctionInput ) ) ) )
            // InternalRuleEngineParser.g:834:1: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_arguments_1_0= ruleFunctionInput ) ) )
            {
            // InternalRuleEngineParser.g:834:1: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_arguments_1_0= ruleFunctionInput ) ) )
            // InternalRuleEngineParser.g:834:2: ( (lv_name_0_0= RULE_ID ) ) ( (lv_arguments_1_0= ruleFunctionInput ) )
            {
            // InternalRuleEngineParser.g:834:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalRuleEngineParser.g:835:1: (lv_name_0_0= RULE_ID )
            {
            // InternalRuleEngineParser.g:835:1: (lv_name_0_0= RULE_ID )
            // InternalRuleEngineParser.g:836:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_14); 

            			newLeafNode(lv_name_0_0, grammarAccess.getFunctionCallAccess().getNameIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFunctionCallRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ID");
            	    

            }


            }

            // InternalRuleEngineParser.g:852:2: ( (lv_arguments_1_0= ruleFunctionInput ) )
            // InternalRuleEngineParser.g:853:1: (lv_arguments_1_0= ruleFunctionInput )
            {
            // InternalRuleEngineParser.g:853:1: (lv_arguments_1_0= ruleFunctionInput )
            // InternalRuleEngineParser.g:854:3: lv_arguments_1_0= ruleFunctionInput
            {
             
            	        newCompositeNode(grammarAccess.getFunctionCallAccess().getArgumentsFunctionInputParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_arguments_1_0=ruleFunctionInput();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFunctionCallRule());
            	        }
                   		add(
                   			current, 
                   			"arguments",
                    		lv_arguments_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.FunctionInput");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionCall"


    // $ANTLR start "entryRuleFunctionInput"
    // InternalRuleEngineParser.g:878:1: entryRuleFunctionInput returns [String current=null] : iv_ruleFunctionInput= ruleFunctionInput EOF ;
    public final String entryRuleFunctionInput() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFunctionInput = null;


        try {
            // InternalRuleEngineParser.g:879:1: (iv_ruleFunctionInput= ruleFunctionInput EOF )
            // InternalRuleEngineParser.g:880:2: iv_ruleFunctionInput= ruleFunctionInput EOF
            {
             newCompositeNode(grammarAccess.getFunctionInputRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunctionInput=ruleFunctionInput();

            state._fsp--;

             current =iv_ruleFunctionInput.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionInput"


    // $ANTLR start "ruleFunctionInput"
    // InternalRuleEngineParser.g:887:1: ruleFunctionInput returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= Comma this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleFunctionInput() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:891:6: ( (this_ID_0= RULE_ID (kw= Comma this_ID_2= RULE_ID )* ) )
            // InternalRuleEngineParser.g:892:1: (this_ID_0= RULE_ID (kw= Comma this_ID_2= RULE_ID )* )
            {
            // InternalRuleEngineParser.g:892:1: (this_ID_0= RULE_ID (kw= Comma this_ID_2= RULE_ID )* )
            // InternalRuleEngineParser.g:892:6: this_ID_0= RULE_ID (kw= Comma this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_19); 

            		current.merge(this_ID_0);
                
             
                newLeafNode(this_ID_0, grammarAccess.getFunctionInputAccess().getIDTerminalRuleCall_0()); 
                
            // InternalRuleEngineParser.g:899:1: (kw= Comma this_ID_2= RULE_ID )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==Comma) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalRuleEngineParser.g:900:2: kw= Comma this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,Comma,FOLLOW_14); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getFunctionInputAccess().getCommaKeyword_1_0()); 
            	        
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_19); 

            	    		current.merge(this_ID_2);
            	        
            	     
            	        newLeafNode(this_ID_2, grammarAccess.getFunctionInputAccess().getIDTerminalRuleCall_1_1()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionInput"


    // $ANTLR start "entryRuleJMPCNCommand"
    // InternalRuleEngineParser.g:922:1: entryRuleJMPCNCommand returns [EObject current=null] : iv_ruleJMPCNCommand= ruleJMPCNCommand EOF ;
    public final EObject entryRuleJMPCNCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJMPCNCommand = null;


        try {
            // InternalRuleEngineParser.g:923:2: (iv_ruleJMPCNCommand= ruleJMPCNCommand EOF )
            // InternalRuleEngineParser.g:924:2: iv_ruleJMPCNCommand= ruleJMPCNCommand EOF
            {
             newCompositeNode(grammarAccess.getJMPCNCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleJMPCNCommand=ruleJMPCNCommand();

            state._fsp--;

             current =iv_ruleJMPCNCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJMPCNCommand"


    // $ANTLR start "ruleJMPCNCommand"
    // InternalRuleEngineParser.g:931:1: ruleJMPCNCommand returns [EObject current=null] : (otherlv_0= JMPCN ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleJMPCNCommand() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:934:28: ( (otherlv_0= JMPCN ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalRuleEngineParser.g:935:1: (otherlv_0= JMPCN ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalRuleEngineParser.g:935:1: (otherlv_0= JMPCN ( (lv_name_1_0= RULE_ID ) ) )
            // InternalRuleEngineParser.g:936:2: otherlv_0= JMPCN ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,JMPCN,FOLLOW_14); 

                	newLeafNode(otherlv_0, grammarAccess.getJMPCNCommandAccess().getJMPCNKeyword_0());
                
            // InternalRuleEngineParser.g:940:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalRuleEngineParser.g:941:1: (lv_name_1_0= RULE_ID )
            {
            // InternalRuleEngineParser.g:941:1: (lv_name_1_0= RULE_ID )
            // InternalRuleEngineParser.g:942:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            			newLeafNode(lv_name_1_0, grammarAccess.getJMPCNCommandAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getJMPCNCommandRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJMPCNCommand"


    // $ANTLR start "entryRuleJMPCommand"
    // InternalRuleEngineParser.g:966:1: entryRuleJMPCommand returns [EObject current=null] : iv_ruleJMPCommand= ruleJMPCommand EOF ;
    public final EObject entryRuleJMPCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJMPCommand = null;


        try {
            // InternalRuleEngineParser.g:967:2: (iv_ruleJMPCommand= ruleJMPCommand EOF )
            // InternalRuleEngineParser.g:968:2: iv_ruleJMPCommand= ruleJMPCommand EOF
            {
             newCompositeNode(grammarAccess.getJMPCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleJMPCommand=ruleJMPCommand();

            state._fsp--;

             current =iv_ruleJMPCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJMPCommand"


    // $ANTLR start "ruleJMPCommand"
    // InternalRuleEngineParser.g:975:1: ruleJMPCommand returns [EObject current=null] : (otherlv_0= JMP ( (lv_name_1_0= RULE_IDENTIFIER ) ) ) ;
    public final EObject ruleJMPCommand() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:978:28: ( (otherlv_0= JMP ( (lv_name_1_0= RULE_IDENTIFIER ) ) ) )
            // InternalRuleEngineParser.g:979:1: (otherlv_0= JMP ( (lv_name_1_0= RULE_IDENTIFIER ) ) )
            {
            // InternalRuleEngineParser.g:979:1: (otherlv_0= JMP ( (lv_name_1_0= RULE_IDENTIFIER ) ) )
            // InternalRuleEngineParser.g:980:2: otherlv_0= JMP ( (lv_name_1_0= RULE_IDENTIFIER ) )
            {
            otherlv_0=(Token)match(input,JMP,FOLLOW_5); 

                	newLeafNode(otherlv_0, grammarAccess.getJMPCommandAccess().getJMPKeyword_0());
                
            // InternalRuleEngineParser.g:984:1: ( (lv_name_1_0= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:985:1: (lv_name_1_0= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:985:1: (lv_name_1_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:986:3: lv_name_1_0= RULE_IDENTIFIER
            {
            lv_name_1_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_2); 

            			newLeafNode(lv_name_1_0, grammarAccess.getJMPCommandAccess().getNameIDENTIFIERTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getJMPCommandRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJMPCommand"


    // $ANTLR start "entryRuleEQCommand"
    // InternalRuleEngineParser.g:1010:1: entryRuleEQCommand returns [EObject current=null] : iv_ruleEQCommand= ruleEQCommand EOF ;
    public final EObject entryRuleEQCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEQCommand = null;


        try {
            // InternalRuleEngineParser.g:1011:2: (iv_ruleEQCommand= ruleEQCommand EOF )
            // InternalRuleEngineParser.g:1012:2: iv_ruleEQCommand= ruleEQCommand EOF
            {
             newCompositeNode(grammarAccess.getEQCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEQCommand=ruleEQCommand();

            state._fsp--;

             current =iv_ruleEQCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEQCommand"


    // $ANTLR start "ruleEQCommand"
    // InternalRuleEngineParser.g:1019:1: ruleEQCommand returns [EObject current=null] : (this_EQBool_0= ruleEQBool | this_EQInt_1= ruleEQInt | this_EQNBool_2= ruleEQNBool | this_EQNInt_3= ruleEQNInt ) ;
    public final EObject ruleEQCommand() throws RecognitionException {
        EObject current = null;

        EObject this_EQBool_0 = null;

        EObject this_EQInt_1 = null;

        EObject this_EQNBool_2 = null;

        EObject this_EQNInt_3 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1022:28: ( (this_EQBool_0= ruleEQBool | this_EQInt_1= ruleEQInt | this_EQNBool_2= ruleEQNBool | this_EQNInt_3= ruleEQNInt ) )
            // InternalRuleEngineParser.g:1023:1: (this_EQBool_0= ruleEQBool | this_EQInt_1= ruleEQInt | this_EQNBool_2= ruleEQNBool | this_EQNInt_3= ruleEQNInt )
            {
            // InternalRuleEngineParser.g:1023:1: (this_EQBool_0= ruleEQBool | this_EQInt_1= ruleEQInt | this_EQNBool_2= ruleEQNBool | this_EQNInt_3= ruleEQNInt )
            int alt14=4;
            switch ( input.LA(1) ) {
            case EQBOOL:
                {
                alt14=1;
                }
                break;
            case EQINT:
                {
                alt14=2;
                }
                break;
            case EQNBOOL:
                {
                alt14=3;
                }
                break;
            case EQNINT:
                {
                alt14=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalRuleEngineParser.g:1024:5: this_EQBool_0= ruleEQBool
                    {
                     
                            newCompositeNode(grammarAccess.getEQCommandAccess().getEQBoolParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_EQBool_0=ruleEQBool();

                    state._fsp--;


                            current = this_EQBool_0;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:1034:5: this_EQInt_1= ruleEQInt
                    {
                     
                            newCompositeNode(grammarAccess.getEQCommandAccess().getEQIntParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_EQInt_1=ruleEQInt();

                    state._fsp--;


                            current = this_EQInt_1;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalRuleEngineParser.g:1044:5: this_EQNBool_2= ruleEQNBool
                    {
                     
                            newCompositeNode(grammarAccess.getEQCommandAccess().getEQNBoolParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_EQNBool_2=ruleEQNBool();

                    state._fsp--;


                            current = this_EQNBool_2;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalRuleEngineParser.g:1054:5: this_EQNInt_3= ruleEQNInt
                    {
                     
                            newCompositeNode(grammarAccess.getEQCommandAccess().getEQNIntParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_2);
                    this_EQNInt_3=ruleEQNInt();

                    state._fsp--;


                            current = this_EQNInt_3;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEQCommand"


    // $ANTLR start "entryRuleEQBool"
    // InternalRuleEngineParser.g:1070:1: entryRuleEQBool returns [EObject current=null] : iv_ruleEQBool= ruleEQBool EOF ;
    public final EObject entryRuleEQBool() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEQBool = null;


        try {
            // InternalRuleEngineParser.g:1071:2: (iv_ruleEQBool= ruleEQBool EOF )
            // InternalRuleEngineParser.g:1072:2: iv_ruleEQBool= ruleEQBool EOF
            {
             newCompositeNode(grammarAccess.getEQBoolRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEQBool=ruleEQBool();

            state._fsp--;

             current =iv_ruleEQBool; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEQBool"


    // $ANTLR start "ruleEQBool"
    // InternalRuleEngineParser.g:1079:1: ruleEQBool returns [EObject current=null] : (otherlv_0= EQBOOL ( (lv_value_1_0= ruleEQBoolValue ) ) ) ;
    public final EObject ruleEQBool() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_value_1_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1082:28: ( (otherlv_0= EQBOOL ( (lv_value_1_0= ruleEQBoolValue ) ) ) )
            // InternalRuleEngineParser.g:1083:1: (otherlv_0= EQBOOL ( (lv_value_1_0= ruleEQBoolValue ) ) )
            {
            // InternalRuleEngineParser.g:1083:1: (otherlv_0= EQBOOL ( (lv_value_1_0= ruleEQBoolValue ) ) )
            // InternalRuleEngineParser.g:1084:2: otherlv_0= EQBOOL ( (lv_value_1_0= ruleEQBoolValue ) )
            {
            otherlv_0=(Token)match(input,EQBOOL,FOLLOW_20); 

                	newLeafNode(otherlv_0, grammarAccess.getEQBoolAccess().getEQBOOLKeyword_0());
                
            // InternalRuleEngineParser.g:1088:1: ( (lv_value_1_0= ruleEQBoolValue ) )
            // InternalRuleEngineParser.g:1089:1: (lv_value_1_0= ruleEQBoolValue )
            {
            // InternalRuleEngineParser.g:1089:1: (lv_value_1_0= ruleEQBoolValue )
            // InternalRuleEngineParser.g:1090:3: lv_value_1_0= ruleEQBoolValue
            {
             
            	        newCompositeNode(grammarAccess.getEQBoolAccess().getValueEQBoolValueParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_value_1_0=ruleEQBoolValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEQBoolRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.EQBoolValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEQBool"


    // $ANTLR start "entryRuleEQNBool"
    // InternalRuleEngineParser.g:1114:1: entryRuleEQNBool returns [EObject current=null] : iv_ruleEQNBool= ruleEQNBool EOF ;
    public final EObject entryRuleEQNBool() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEQNBool = null;


        try {
            // InternalRuleEngineParser.g:1115:2: (iv_ruleEQNBool= ruleEQNBool EOF )
            // InternalRuleEngineParser.g:1116:2: iv_ruleEQNBool= ruleEQNBool EOF
            {
             newCompositeNode(grammarAccess.getEQNBoolRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEQNBool=ruleEQNBool();

            state._fsp--;

             current =iv_ruleEQNBool; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEQNBool"


    // $ANTLR start "ruleEQNBool"
    // InternalRuleEngineParser.g:1123:1: ruleEQNBool returns [EObject current=null] : (otherlv_0= EQNBOOL ( (lv_value_1_0= ruleEQBoolValue ) ) ) ;
    public final EObject ruleEQNBool() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_value_1_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1126:28: ( (otherlv_0= EQNBOOL ( (lv_value_1_0= ruleEQBoolValue ) ) ) )
            // InternalRuleEngineParser.g:1127:1: (otherlv_0= EQNBOOL ( (lv_value_1_0= ruleEQBoolValue ) ) )
            {
            // InternalRuleEngineParser.g:1127:1: (otherlv_0= EQNBOOL ( (lv_value_1_0= ruleEQBoolValue ) ) )
            // InternalRuleEngineParser.g:1128:2: otherlv_0= EQNBOOL ( (lv_value_1_0= ruleEQBoolValue ) )
            {
            otherlv_0=(Token)match(input,EQNBOOL,FOLLOW_20); 

                	newLeafNode(otherlv_0, grammarAccess.getEQNBoolAccess().getEQNBOOLKeyword_0());
                
            // InternalRuleEngineParser.g:1132:1: ( (lv_value_1_0= ruleEQBoolValue ) )
            // InternalRuleEngineParser.g:1133:1: (lv_value_1_0= ruleEQBoolValue )
            {
            // InternalRuleEngineParser.g:1133:1: (lv_value_1_0= ruleEQBoolValue )
            // InternalRuleEngineParser.g:1134:3: lv_value_1_0= ruleEQBoolValue
            {
             
            	        newCompositeNode(grammarAccess.getEQNBoolAccess().getValueEQBoolValueParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_value_1_0=ruleEQBoolValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEQNBoolRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.EQBoolValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEQNBool"


    // $ANTLR start "entryRuleEQInt"
    // InternalRuleEngineParser.g:1158:1: entryRuleEQInt returns [EObject current=null] : iv_ruleEQInt= ruleEQInt EOF ;
    public final EObject entryRuleEQInt() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEQInt = null;


        try {
            // InternalRuleEngineParser.g:1159:2: (iv_ruleEQInt= ruleEQInt EOF )
            // InternalRuleEngineParser.g:1160:2: iv_ruleEQInt= ruleEQInt EOF
            {
             newCompositeNode(grammarAccess.getEQIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEQInt=ruleEQInt();

            state._fsp--;

             current =iv_ruleEQInt; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEQInt"


    // $ANTLR start "ruleEQInt"
    // InternalRuleEngineParser.g:1167:1: ruleEQInt returns [EObject current=null] : (otherlv_0= EQINT ( (lv_value_1_0= ruleEQIntValue ) ) ) ;
    public final EObject ruleEQInt() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_value_1_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1170:28: ( (otherlv_0= EQINT ( (lv_value_1_0= ruleEQIntValue ) ) ) )
            // InternalRuleEngineParser.g:1171:1: (otherlv_0= EQINT ( (lv_value_1_0= ruleEQIntValue ) ) )
            {
            // InternalRuleEngineParser.g:1171:1: (otherlv_0= EQINT ( (lv_value_1_0= ruleEQIntValue ) ) )
            // InternalRuleEngineParser.g:1172:2: otherlv_0= EQINT ( (lv_value_1_0= ruleEQIntValue ) )
            {
            otherlv_0=(Token)match(input,EQINT,FOLLOW_21); 

                	newLeafNode(otherlv_0, grammarAccess.getEQIntAccess().getEQINTKeyword_0());
                
            // InternalRuleEngineParser.g:1176:1: ( (lv_value_1_0= ruleEQIntValue ) )
            // InternalRuleEngineParser.g:1177:1: (lv_value_1_0= ruleEQIntValue )
            {
            // InternalRuleEngineParser.g:1177:1: (lv_value_1_0= ruleEQIntValue )
            // InternalRuleEngineParser.g:1178:3: lv_value_1_0= ruleEQIntValue
            {
             
            	        newCompositeNode(grammarAccess.getEQIntAccess().getValueEQIntValueParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_value_1_0=ruleEQIntValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEQIntRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.EQIntValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEQInt"


    // $ANTLR start "entryRuleEQNInt"
    // InternalRuleEngineParser.g:1202:1: entryRuleEQNInt returns [EObject current=null] : iv_ruleEQNInt= ruleEQNInt EOF ;
    public final EObject entryRuleEQNInt() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEQNInt = null;


        try {
            // InternalRuleEngineParser.g:1203:2: (iv_ruleEQNInt= ruleEQNInt EOF )
            // InternalRuleEngineParser.g:1204:2: iv_ruleEQNInt= ruleEQNInt EOF
            {
             newCompositeNode(grammarAccess.getEQNIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEQNInt=ruleEQNInt();

            state._fsp--;

             current =iv_ruleEQNInt; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEQNInt"


    // $ANTLR start "ruleEQNInt"
    // InternalRuleEngineParser.g:1211:1: ruleEQNInt returns [EObject current=null] : (otherlv_0= EQNINT ( (lv_value_1_0= ruleEQIntValue ) ) ) ;
    public final EObject ruleEQNInt() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_value_1_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1214:28: ( (otherlv_0= EQNINT ( (lv_value_1_0= ruleEQIntValue ) ) ) )
            // InternalRuleEngineParser.g:1215:1: (otherlv_0= EQNINT ( (lv_value_1_0= ruleEQIntValue ) ) )
            {
            // InternalRuleEngineParser.g:1215:1: (otherlv_0= EQNINT ( (lv_value_1_0= ruleEQIntValue ) ) )
            // InternalRuleEngineParser.g:1216:2: otherlv_0= EQNINT ( (lv_value_1_0= ruleEQIntValue ) )
            {
            otherlv_0=(Token)match(input,EQNINT,FOLLOW_21); 

                	newLeafNode(otherlv_0, grammarAccess.getEQNIntAccess().getEQNINTKeyword_0());
                
            // InternalRuleEngineParser.g:1220:1: ( (lv_value_1_0= ruleEQIntValue ) )
            // InternalRuleEngineParser.g:1221:1: (lv_value_1_0= ruleEQIntValue )
            {
            // InternalRuleEngineParser.g:1221:1: (lv_value_1_0= ruleEQIntValue )
            // InternalRuleEngineParser.g:1222:3: lv_value_1_0= ruleEQIntValue
            {
             
            	        newCompositeNode(grammarAccess.getEQNIntAccess().getValueEQIntValueParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_value_1_0=ruleEQIntValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEQNIntRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.EQIntValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEQNInt"


    // $ANTLR start "entryRuleEQBoolValue"
    // InternalRuleEngineParser.g:1246:1: entryRuleEQBoolValue returns [String current=null] : iv_ruleEQBoolValue= ruleEQBoolValue EOF ;
    public final String entryRuleEQBoolValue() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEQBoolValue = null;


        try {
            // InternalRuleEngineParser.g:1247:1: (iv_ruleEQBoolValue= ruleEQBoolValue EOF )
            // InternalRuleEngineParser.g:1248:2: iv_ruleEQBoolValue= ruleEQBoolValue EOF
            {
             newCompositeNode(grammarAccess.getEQBoolValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEQBoolValue=ruleEQBoolValue();

            state._fsp--;

             current =iv_ruleEQBoolValue.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEQBoolValue"


    // $ANTLR start "ruleEQBoolValue"
    // InternalRuleEngineParser.g:1255:1: ruleEQBoolValue returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_BooleanRule_0= ruleBooleanRule | this_IDENTIFIER_1= RULE_IDENTIFIER ) ;
    public final AntlrDatatypeRuleToken ruleEQBoolValue() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_IDENTIFIER_1=null;
        AntlrDatatypeRuleToken this_BooleanRule_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1259:6: ( (this_BooleanRule_0= ruleBooleanRule | this_IDENTIFIER_1= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:1260:1: (this_BooleanRule_0= ruleBooleanRule | this_IDENTIFIER_1= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:1260:1: (this_BooleanRule_0= ruleBooleanRule | this_IDENTIFIER_1= RULE_IDENTIFIER )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==FALSE||LA15_0==TRUE) ) {
                alt15=1;
            }
            else if ( (LA15_0==RULE_IDENTIFIER) ) {
                alt15=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalRuleEngineParser.g:1261:5: this_BooleanRule_0= ruleBooleanRule
                    {
                     
                            newCompositeNode(grammarAccess.getEQBoolValueAccess().getBooleanRuleParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_BooleanRule_0=ruleBooleanRule();

                    state._fsp--;


                    		current.merge(this_BooleanRule_0);
                        
                     
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:1272:10: this_IDENTIFIER_1= RULE_IDENTIFIER
                    {
                    this_IDENTIFIER_1=(Token)match(input,RULE_IDENTIFIER,FOLLOW_2); 

                    		current.merge(this_IDENTIFIER_1);
                        
                     
                        newLeafNode(this_IDENTIFIER_1, grammarAccess.getEQBoolValueAccess().getIDENTIFIERTerminalRuleCall_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEQBoolValue"


    // $ANTLR start "entryRuleEQIntValue"
    // InternalRuleEngineParser.g:1287:1: entryRuleEQIntValue returns [String current=null] : iv_ruleEQIntValue= ruleEQIntValue EOF ;
    public final String entryRuleEQIntValue() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEQIntValue = null;


        try {
            // InternalRuleEngineParser.g:1288:1: (iv_ruleEQIntValue= ruleEQIntValue EOF )
            // InternalRuleEngineParser.g:1289:2: iv_ruleEQIntValue= ruleEQIntValue EOF
            {
             newCompositeNode(grammarAccess.getEQIntValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEQIntValue=ruleEQIntValue();

            state._fsp--;

             current =iv_ruleEQIntValue.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEQIntValue"


    // $ANTLR start "ruleEQIntValue"
    // InternalRuleEngineParser.g:1296:1: ruleEQIntValue returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_IntType_0= ruleIntType | this_IDENTIFIER_1= RULE_IDENTIFIER ) ;
    public final AntlrDatatypeRuleToken ruleEQIntValue() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_IDENTIFIER_1=null;
        AntlrDatatypeRuleToken this_IntType_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1300:6: ( (this_IntType_0= ruleIntType | this_IDENTIFIER_1= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:1301:1: (this_IntType_0= ruleIntType | this_IDENTIFIER_1= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:1301:1: (this_IntType_0= ruleIntType | this_IDENTIFIER_1= RULE_IDENTIFIER )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_INT) ) {
                alt16=1;
            }
            else if ( (LA16_0==RULE_IDENTIFIER) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // InternalRuleEngineParser.g:1302:5: this_IntType_0= ruleIntType
                    {
                     
                            newCompositeNode(grammarAccess.getEQIntValueAccess().getIntTypeParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_IntType_0=ruleIntType();

                    state._fsp--;


                    		current.merge(this_IntType_0);
                        
                     
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:1313:10: this_IDENTIFIER_1= RULE_IDENTIFIER
                    {
                    this_IDENTIFIER_1=(Token)match(input,RULE_IDENTIFIER,FOLLOW_2); 

                    		current.merge(this_IDENTIFIER_1);
                        
                     
                        newLeafNode(this_IDENTIFIER_1, grammarAccess.getEQIntValueAccess().getIDENTIFIERTerminalRuleCall_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEQIntValue"


    // $ANTLR start "entryRuleCommand"
    // InternalRuleEngineParser.g:1328:1: entryRuleCommand returns [EObject current=null] : iv_ruleCommand= ruleCommand EOF ;
    public final EObject entryRuleCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCommand = null;


        try {
            // InternalRuleEngineParser.g:1329:2: (iv_ruleCommand= ruleCommand EOF )
            // InternalRuleEngineParser.g:1330:2: iv_ruleCommand= ruleCommand EOF
            {
             newCompositeNode(grammarAccess.getCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCommand=ruleCommand();

            state._fsp--;

             current =iv_ruleCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCommand"


    // $ANTLR start "ruleCommand"
    // InternalRuleEngineParser.g:1337:1: ruleCommand returns [EObject current=null] : (this_ILCommand_0= ruleILCommand | this_ExtendedCommands_1= ruleExtendedCommands ) ;
    public final EObject ruleCommand() throws RecognitionException {
        EObject current = null;

        EObject this_ILCommand_0 = null;

        EObject this_ExtendedCommands_1 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1340:28: ( (this_ILCommand_0= ruleILCommand | this_ExtendedCommands_1= ruleExtendedCommands ) )
            // InternalRuleEngineParser.g:1341:1: (this_ILCommand_0= ruleILCommand | this_ExtendedCommands_1= ruleExtendedCommands )
            {
            // InternalRuleEngineParser.g:1341:1: (this_ILCommand_0= ruleILCommand | this_ExtendedCommands_1= ruleExtendedCommands )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==EQNBOOL||(LA17_0>=EQBOOL && LA17_0<=EQNINT)||LA17_0==EQINT||LA17_0==JMPCN||(LA17_0>=ADD && LA17_0<=CAL)||LA17_0==DIV||(LA17_0>=JMP && LA17_0<=SUB)||LA17_0==XOR||(LA17_0>=LD && LA17_0<=OR)||LA17_0==ST||LA17_0==Ampersand||(LA17_0>=R && LA17_0<=S)||LA17_0==RULE_ID) ) {
                alt17=1;
            }
            else if ( (LA17_0==DEL) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // InternalRuleEngineParser.g:1342:5: this_ILCommand_0= ruleILCommand
                    {
                     
                            newCompositeNode(grammarAccess.getCommandAccess().getILCommandParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_ILCommand_0=ruleILCommand();

                    state._fsp--;


                            current = this_ILCommand_0;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:1352:5: this_ExtendedCommands_1= ruleExtendedCommands
                    {
                     
                            newCompositeNode(grammarAccess.getCommandAccess().getExtendedCommandsParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_ExtendedCommands_1=ruleExtendedCommands();

                    state._fsp--;


                            current = this_ExtendedCommands_1;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCommand"


    // $ANTLR start "entryRuleILCommand"
    // InternalRuleEngineParser.g:1368:1: entryRuleILCommand returns [EObject current=null] : iv_ruleILCommand= ruleILCommand EOF ;
    public final EObject entryRuleILCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleILCommand = null;


        try {
            // InternalRuleEngineParser.g:1369:2: (iv_ruleILCommand= ruleILCommand EOF )
            // InternalRuleEngineParser.g:1370:2: iv_ruleILCommand= ruleILCommand EOF
            {
             newCompositeNode(grammarAccess.getILCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleILCommand=ruleILCommand();

            state._fsp--;

             current =iv_ruleILCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleILCommand"


    // $ANTLR start "ruleILCommand"
    // InternalRuleEngineParser.g:1377:1: ruleILCommand returns [EObject current=null] : (this_LDCommand_0= ruleLDCommand | this_MathCommand_1= ruleMathCommand | this_LogicOpCommand_2= ruleLogicOpCommand | this_STCommand_3= ruleSTCommand | this_JMPCNCommand_4= ruleJMPCNCommand | this_JMPCommand_5= ruleJMPCommand | this_RCommand_6= ruleRCommand | this_SCommand_7= ruleSCommand | this_EQCommand_8= ruleEQCommand | this_ReturnCommand_9= ruleReturnCommand | this_FunctionCall_10= ruleFunctionCall | this_CallCommand_11= ruleCallCommand ) ;
    public final EObject ruleILCommand() throws RecognitionException {
        EObject current = null;

        EObject this_LDCommand_0 = null;

        EObject this_MathCommand_1 = null;

        EObject this_LogicOpCommand_2 = null;

        EObject this_STCommand_3 = null;

        EObject this_JMPCNCommand_4 = null;

        EObject this_JMPCommand_5 = null;

        EObject this_RCommand_6 = null;

        EObject this_SCommand_7 = null;

        EObject this_EQCommand_8 = null;

        EObject this_ReturnCommand_9 = null;

        EObject this_FunctionCall_10 = null;

        EObject this_CallCommand_11 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1380:28: ( (this_LDCommand_0= ruleLDCommand | this_MathCommand_1= ruleMathCommand | this_LogicOpCommand_2= ruleLogicOpCommand | this_STCommand_3= ruleSTCommand | this_JMPCNCommand_4= ruleJMPCNCommand | this_JMPCommand_5= ruleJMPCommand | this_RCommand_6= ruleRCommand | this_SCommand_7= ruleSCommand | this_EQCommand_8= ruleEQCommand | this_ReturnCommand_9= ruleReturnCommand | this_FunctionCall_10= ruleFunctionCall | this_CallCommand_11= ruleCallCommand ) )
            // InternalRuleEngineParser.g:1381:1: (this_LDCommand_0= ruleLDCommand | this_MathCommand_1= ruleMathCommand | this_LogicOpCommand_2= ruleLogicOpCommand | this_STCommand_3= ruleSTCommand | this_JMPCNCommand_4= ruleJMPCNCommand | this_JMPCommand_5= ruleJMPCommand | this_RCommand_6= ruleRCommand | this_SCommand_7= ruleSCommand | this_EQCommand_8= ruleEQCommand | this_ReturnCommand_9= ruleReturnCommand | this_FunctionCall_10= ruleFunctionCall | this_CallCommand_11= ruleCallCommand )
            {
            // InternalRuleEngineParser.g:1381:1: (this_LDCommand_0= ruleLDCommand | this_MathCommand_1= ruleMathCommand | this_LogicOpCommand_2= ruleLogicOpCommand | this_STCommand_3= ruleSTCommand | this_JMPCNCommand_4= ruleJMPCNCommand | this_JMPCommand_5= ruleJMPCommand | this_RCommand_6= ruleRCommand | this_SCommand_7= ruleSCommand | this_EQCommand_8= ruleEQCommand | this_ReturnCommand_9= ruleReturnCommand | this_FunctionCall_10= ruleFunctionCall | this_CallCommand_11= ruleCallCommand )
            int alt18=12;
            switch ( input.LA(1) ) {
            case LD:
                {
                alt18=1;
                }
                break;
            case ADD:
            case DIV:
            case MUL:
            case SUB:
                {
                alt18=2;
                }
                break;
            case AND:
            case XOR:
            case OR:
            case Ampersand:
                {
                alt18=3;
                }
                break;
            case ST:
                {
                alt18=4;
                }
                break;
            case JMPCN:
                {
                alt18=5;
                }
                break;
            case JMP:
                {
                alt18=6;
                }
                break;
            case R:
                {
                alt18=7;
                }
                break;
            case S:
                {
                alt18=8;
                }
                break;
            case EQNBOOL:
            case EQBOOL:
            case EQNINT:
            case EQINT:
                {
                alt18=9;
                }
                break;
            case RET:
                {
                alt18=10;
                }
                break;
            case RULE_ID:
                {
                alt18=11;
                }
                break;
            case CAL:
                {
                alt18=12;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // InternalRuleEngineParser.g:1382:5: this_LDCommand_0= ruleLDCommand
                    {
                     
                            newCompositeNode(grammarAccess.getILCommandAccess().getLDCommandParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_LDCommand_0=ruleLDCommand();

                    state._fsp--;


                            current = this_LDCommand_0;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:1392:5: this_MathCommand_1= ruleMathCommand
                    {
                     
                            newCompositeNode(grammarAccess.getILCommandAccess().getMathCommandParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_MathCommand_1=ruleMathCommand();

                    state._fsp--;


                            current = this_MathCommand_1;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalRuleEngineParser.g:1402:5: this_LogicOpCommand_2= ruleLogicOpCommand
                    {
                     
                            newCompositeNode(grammarAccess.getILCommandAccess().getLogicOpCommandParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_LogicOpCommand_2=ruleLogicOpCommand();

                    state._fsp--;


                            current = this_LogicOpCommand_2;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalRuleEngineParser.g:1412:5: this_STCommand_3= ruleSTCommand
                    {
                     
                            newCompositeNode(grammarAccess.getILCommandAccess().getSTCommandParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_2);
                    this_STCommand_3=ruleSTCommand();

                    state._fsp--;


                            current = this_STCommand_3;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // InternalRuleEngineParser.g:1422:5: this_JMPCNCommand_4= ruleJMPCNCommand
                    {
                     
                            newCompositeNode(grammarAccess.getILCommandAccess().getJMPCNCommandParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_2);
                    this_JMPCNCommand_4=ruleJMPCNCommand();

                    state._fsp--;


                            current = this_JMPCNCommand_4;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // InternalRuleEngineParser.g:1432:5: this_JMPCommand_5= ruleJMPCommand
                    {
                     
                            newCompositeNode(grammarAccess.getILCommandAccess().getJMPCommandParserRuleCall_5()); 
                        
                    pushFollow(FOLLOW_2);
                    this_JMPCommand_5=ruleJMPCommand();

                    state._fsp--;


                            current = this_JMPCommand_5;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // InternalRuleEngineParser.g:1442:5: this_RCommand_6= ruleRCommand
                    {
                     
                            newCompositeNode(grammarAccess.getILCommandAccess().getRCommandParserRuleCall_6()); 
                        
                    pushFollow(FOLLOW_2);
                    this_RCommand_6=ruleRCommand();

                    state._fsp--;


                            current = this_RCommand_6;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 8 :
                    // InternalRuleEngineParser.g:1452:5: this_SCommand_7= ruleSCommand
                    {
                     
                            newCompositeNode(grammarAccess.getILCommandAccess().getSCommandParserRuleCall_7()); 
                        
                    pushFollow(FOLLOW_2);
                    this_SCommand_7=ruleSCommand();

                    state._fsp--;


                            current = this_SCommand_7;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 9 :
                    // InternalRuleEngineParser.g:1462:5: this_EQCommand_8= ruleEQCommand
                    {
                     
                            newCompositeNode(grammarAccess.getILCommandAccess().getEQCommandParserRuleCall_8()); 
                        
                    pushFollow(FOLLOW_2);
                    this_EQCommand_8=ruleEQCommand();

                    state._fsp--;


                            current = this_EQCommand_8;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 10 :
                    // InternalRuleEngineParser.g:1472:5: this_ReturnCommand_9= ruleReturnCommand
                    {
                     
                            newCompositeNode(grammarAccess.getILCommandAccess().getReturnCommandParserRuleCall_9()); 
                        
                    pushFollow(FOLLOW_2);
                    this_ReturnCommand_9=ruleReturnCommand();

                    state._fsp--;


                            current = this_ReturnCommand_9;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 11 :
                    // InternalRuleEngineParser.g:1482:5: this_FunctionCall_10= ruleFunctionCall
                    {
                     
                            newCompositeNode(grammarAccess.getILCommandAccess().getFunctionCallParserRuleCall_10()); 
                        
                    pushFollow(FOLLOW_2);
                    this_FunctionCall_10=ruleFunctionCall();

                    state._fsp--;


                            current = this_FunctionCall_10;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 12 :
                    // InternalRuleEngineParser.g:1492:5: this_CallCommand_11= ruleCallCommand
                    {
                     
                            newCompositeNode(grammarAccess.getILCommandAccess().getCallCommandParserRuleCall_11()); 
                        
                    pushFollow(FOLLOW_2);
                    this_CallCommand_11=ruleCallCommand();

                    state._fsp--;


                            current = this_CallCommand_11;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleILCommand"


    // $ANTLR start "entryRuleExtendedCommands"
    // InternalRuleEngineParser.g:1508:1: entryRuleExtendedCommands returns [EObject current=null] : iv_ruleExtendedCommands= ruleExtendedCommands EOF ;
    public final EObject entryRuleExtendedCommands() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExtendedCommands = null;


        try {
            // InternalRuleEngineParser.g:1509:2: (iv_ruleExtendedCommands= ruleExtendedCommands EOF )
            // InternalRuleEngineParser.g:1510:2: iv_ruleExtendedCommands= ruleExtendedCommands EOF
            {
             newCompositeNode(grammarAccess.getExtendedCommandsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExtendedCommands=ruleExtendedCommands();

            state._fsp--;

             current =iv_ruleExtendedCommands; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExtendedCommands"


    // $ANTLR start "ruleExtendedCommands"
    // InternalRuleEngineParser.g:1517:1: ruleExtendedCommands returns [EObject current=null] : this_DelayCommand_0= ruleDelayCommand ;
    public final EObject ruleExtendedCommands() throws RecognitionException {
        EObject current = null;

        EObject this_DelayCommand_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1520:28: (this_DelayCommand_0= ruleDelayCommand )
            // InternalRuleEngineParser.g:1522:5: this_DelayCommand_0= ruleDelayCommand
            {
             
                    newCompositeNode(grammarAccess.getExtendedCommandsAccess().getDelayCommandParserRuleCall()); 
                
            pushFollow(FOLLOW_2);
            this_DelayCommand_0=ruleDelayCommand();

            state._fsp--;


                    current = this_DelayCommand_0;
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExtendedCommands"


    // $ANTLR start "entryRuleDelayCommand"
    // InternalRuleEngineParser.g:1538:1: entryRuleDelayCommand returns [EObject current=null] : iv_ruleDelayCommand= ruleDelayCommand EOF ;
    public final EObject entryRuleDelayCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDelayCommand = null;


        try {
            // InternalRuleEngineParser.g:1539:2: (iv_ruleDelayCommand= ruleDelayCommand EOF )
            // InternalRuleEngineParser.g:1540:2: iv_ruleDelayCommand= ruleDelayCommand EOF
            {
             newCompositeNode(grammarAccess.getDelayCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDelayCommand=ruleDelayCommand();

            state._fsp--;

             current =iv_ruleDelayCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDelayCommand"


    // $ANTLR start "ruleDelayCommand"
    // InternalRuleEngineParser.g:1547:1: ruleDelayCommand returns [EObject current=null] : (otherlv_0= DEL ( ( (lv_name_1_0= RULE_IDENTIFIER ) ) | ( (lv_value_2_0= ruleIntType ) ) ) ) ;
    public final EObject ruleDelayCommand() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        AntlrDatatypeRuleToken lv_value_2_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1550:28: ( (otherlv_0= DEL ( ( (lv_name_1_0= RULE_IDENTIFIER ) ) | ( (lv_value_2_0= ruleIntType ) ) ) ) )
            // InternalRuleEngineParser.g:1551:1: (otherlv_0= DEL ( ( (lv_name_1_0= RULE_IDENTIFIER ) ) | ( (lv_value_2_0= ruleIntType ) ) ) )
            {
            // InternalRuleEngineParser.g:1551:1: (otherlv_0= DEL ( ( (lv_name_1_0= RULE_IDENTIFIER ) ) | ( (lv_value_2_0= ruleIntType ) ) ) )
            // InternalRuleEngineParser.g:1552:2: otherlv_0= DEL ( ( (lv_name_1_0= RULE_IDENTIFIER ) ) | ( (lv_value_2_0= ruleIntType ) ) )
            {
            otherlv_0=(Token)match(input,DEL,FOLLOW_21); 

                	newLeafNode(otherlv_0, grammarAccess.getDelayCommandAccess().getDELKeyword_0());
                
            // InternalRuleEngineParser.g:1556:1: ( ( (lv_name_1_0= RULE_IDENTIFIER ) ) | ( (lv_value_2_0= ruleIntType ) ) )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==RULE_IDENTIFIER) ) {
                alt19=1;
            }
            else if ( (LA19_0==RULE_INT) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalRuleEngineParser.g:1556:2: ( (lv_name_1_0= RULE_IDENTIFIER ) )
                    {
                    // InternalRuleEngineParser.g:1556:2: ( (lv_name_1_0= RULE_IDENTIFIER ) )
                    // InternalRuleEngineParser.g:1557:1: (lv_name_1_0= RULE_IDENTIFIER )
                    {
                    // InternalRuleEngineParser.g:1557:1: (lv_name_1_0= RULE_IDENTIFIER )
                    // InternalRuleEngineParser.g:1558:3: lv_name_1_0= RULE_IDENTIFIER
                    {
                    lv_name_1_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_2); 

                    			newLeafNode(lv_name_1_0, grammarAccess.getDelayCommandAccess().getNameIDENTIFIERTerminalRuleCall_1_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getDelayCommandRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_1_0, 
                            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:1575:6: ( (lv_value_2_0= ruleIntType ) )
                    {
                    // InternalRuleEngineParser.g:1575:6: ( (lv_value_2_0= ruleIntType ) )
                    // InternalRuleEngineParser.g:1576:1: (lv_value_2_0= ruleIntType )
                    {
                    // InternalRuleEngineParser.g:1576:1: (lv_value_2_0= ruleIntType )
                    // InternalRuleEngineParser.g:1577:3: lv_value_2_0= ruleIntType
                    {
                     
                    	        newCompositeNode(grammarAccess.getDelayCommandAccess().getValueIntTypeParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_2);
                    lv_value_2_0=ruleIntType();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDelayCommandRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_2_0, 
                            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IntType");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDelayCommand"


    // $ANTLR start "entryRuleResourceParamater"
    // InternalRuleEngineParser.g:1603:1: entryRuleResourceParamater returns [EObject current=null] : iv_ruleResourceParamater= ruleResourceParamater EOF ;
    public final EObject entryRuleResourceParamater() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleResourceParamater = null;


        try {
            // InternalRuleEngineParser.g:1604:2: (iv_ruleResourceParamater= ruleResourceParamater EOF )
            // InternalRuleEngineParser.g:1605:2: iv_ruleResourceParamater= ruleResourceParamater EOF
            {
             newCompositeNode(grammarAccess.getResourceParamaterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleResourceParamater=ruleResourceParamater();

            state._fsp--;

             current =iv_ruleResourceParamater; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleResourceParamater"


    // $ANTLR start "ruleResourceParamater"
    // InternalRuleEngineParser.g:1612:1: ruleResourceParamater returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleResourceParamater() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1615:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalRuleEngineParser.g:1616:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalRuleEngineParser.g:1616:1: ( (lv_name_0_0= RULE_ID ) )
            // InternalRuleEngineParser.g:1617:1: (lv_name_0_0= RULE_ID )
            {
            // InternalRuleEngineParser.g:1617:1: (lv_name_0_0= RULE_ID )
            // InternalRuleEngineParser.g:1618:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            			newLeafNode(lv_name_0_0, grammarAccess.getResourceParamaterAccess().getNameIDTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getResourceParamaterRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ID");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResourceParamater"


    // $ANTLR start "entryRuleIntParameter"
    // InternalRuleEngineParser.g:1642:1: entryRuleIntParameter returns [EObject current=null] : iv_ruleIntParameter= ruleIntParameter EOF ;
    public final EObject entryRuleIntParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntParameter = null;


        try {
            // InternalRuleEngineParser.g:1643:2: (iv_ruleIntParameter= ruleIntParameter EOF )
            // InternalRuleEngineParser.g:1644:2: iv_ruleIntParameter= ruleIntParameter EOF
            {
             newCompositeNode(grammarAccess.getIntParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntParameter=ruleIntParameter();

            state._fsp--;

             current =iv_ruleIntParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntParameter"


    // $ANTLR start "ruleIntParameter"
    // InternalRuleEngineParser.g:1651:1: ruleIntParameter returns [EObject current=null] : ( (lv_name_0_0= ruleIntType ) ) ;
    public final EObject ruleIntParameter() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1654:28: ( ( (lv_name_0_0= ruleIntType ) ) )
            // InternalRuleEngineParser.g:1655:1: ( (lv_name_0_0= ruleIntType ) )
            {
            // InternalRuleEngineParser.g:1655:1: ( (lv_name_0_0= ruleIntType ) )
            // InternalRuleEngineParser.g:1656:1: (lv_name_0_0= ruleIntType )
            {
            // InternalRuleEngineParser.g:1656:1: (lv_name_0_0= ruleIntType )
            // InternalRuleEngineParser.g:1657:3: lv_name_0_0= ruleIntType
            {
             
            	        newCompositeNode(grammarAccess.getIntParameterAccess().getNameIntTypeParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleIntType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntParameterRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IntType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntParameter"


    // $ANTLR start "entryRuleBooleanParameter"
    // InternalRuleEngineParser.g:1681:1: entryRuleBooleanParameter returns [EObject current=null] : iv_ruleBooleanParameter= ruleBooleanParameter EOF ;
    public final EObject entryRuleBooleanParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanParameter = null;


        try {
            // InternalRuleEngineParser.g:1682:2: (iv_ruleBooleanParameter= ruleBooleanParameter EOF )
            // InternalRuleEngineParser.g:1683:2: iv_ruleBooleanParameter= ruleBooleanParameter EOF
            {
             newCompositeNode(grammarAccess.getBooleanParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBooleanParameter=ruleBooleanParameter();

            state._fsp--;

             current =iv_ruleBooleanParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanParameter"


    // $ANTLR start "ruleBooleanParameter"
    // InternalRuleEngineParser.g:1690:1: ruleBooleanParameter returns [EObject current=null] : ( (lv_name_0_0= ruleBooleanRule ) ) ;
    public final EObject ruleBooleanParameter() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1693:28: ( ( (lv_name_0_0= ruleBooleanRule ) ) )
            // InternalRuleEngineParser.g:1694:1: ( (lv_name_0_0= ruleBooleanRule ) )
            {
            // InternalRuleEngineParser.g:1694:1: ( (lv_name_0_0= ruleBooleanRule ) )
            // InternalRuleEngineParser.g:1695:1: (lv_name_0_0= ruleBooleanRule )
            {
            // InternalRuleEngineParser.g:1695:1: (lv_name_0_0= ruleBooleanRule )
            // InternalRuleEngineParser.g:1696:3: lv_name_0_0= ruleBooleanRule
            {
             
            	        newCompositeNode(grammarAccess.getBooleanParameterAccess().getNameBooleanRuleParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleBooleanRule();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanParameterRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.BooleanRule");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanParameter"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalRuleEngineParser.g:1720:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalRuleEngineParser.g:1721:1: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalRuleEngineParser.g:1722:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalRuleEngineParser.g:1729:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_IDENTIFIER_0= RULE_IDENTIFIER (kw= FullStop this_IDENTIFIER_2= RULE_IDENTIFIER )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_IDENTIFIER_0=null;
        Token kw=null;
        Token this_IDENTIFIER_2=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1733:6: ( (this_IDENTIFIER_0= RULE_IDENTIFIER (kw= FullStop this_IDENTIFIER_2= RULE_IDENTIFIER )* ) )
            // InternalRuleEngineParser.g:1734:1: (this_IDENTIFIER_0= RULE_IDENTIFIER (kw= FullStop this_IDENTIFIER_2= RULE_IDENTIFIER )* )
            {
            // InternalRuleEngineParser.g:1734:1: (this_IDENTIFIER_0= RULE_IDENTIFIER (kw= FullStop this_IDENTIFIER_2= RULE_IDENTIFIER )* )
            // InternalRuleEngineParser.g:1734:6: this_IDENTIFIER_0= RULE_IDENTIFIER (kw= FullStop this_IDENTIFIER_2= RULE_IDENTIFIER )*
            {
            this_IDENTIFIER_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_22); 

            		current.merge(this_IDENTIFIER_0);
                
             
                newLeafNode(this_IDENTIFIER_0, grammarAccess.getQualifiedNameAccess().getIDENTIFIERTerminalRuleCall_0()); 
                
            // InternalRuleEngineParser.g:1741:1: (kw= FullStop this_IDENTIFIER_2= RULE_IDENTIFIER )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==FullStop) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalRuleEngineParser.g:1742:2: kw= FullStop this_IDENTIFIER_2= RULE_IDENTIFIER
            	    {
            	    kw=(Token)match(input,FullStop,FOLLOW_5); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            	        
            	    this_IDENTIFIER_2=(Token)match(input,RULE_IDENTIFIER,FOLLOW_22); 

            	    		current.merge(this_IDENTIFIER_2);
            	        
            	     
            	        newLeafNode(this_IDENTIFIER_2, grammarAccess.getQualifiedNameAccess().getIDENTIFIERTerminalRuleCall_1_1()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleSTCommand"
    // InternalRuleEngineParser.g:1762:1: entryRuleSTCommand returns [EObject current=null] : iv_ruleSTCommand= ruleSTCommand EOF ;
    public final EObject entryRuleSTCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSTCommand = null;


        try {
            // InternalRuleEngineParser.g:1763:2: (iv_ruleSTCommand= ruleSTCommand EOF )
            // InternalRuleEngineParser.g:1764:2: iv_ruleSTCommand= ruleSTCommand EOF
            {
             newCompositeNode(grammarAccess.getSTCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSTCommand=ruleSTCommand();

            state._fsp--;

             current =iv_ruleSTCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSTCommand"


    // $ANTLR start "ruleSTCommand"
    // InternalRuleEngineParser.g:1771:1: ruleSTCommand returns [EObject current=null] : (otherlv_0= ST ( (lv_name_1_0= ruleQualifiedName ) ) ) ;
    public final EObject ruleSTCommand() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1774:28: ( (otherlv_0= ST ( (lv_name_1_0= ruleQualifiedName ) ) ) )
            // InternalRuleEngineParser.g:1775:1: (otherlv_0= ST ( (lv_name_1_0= ruleQualifiedName ) ) )
            {
            // InternalRuleEngineParser.g:1775:1: (otherlv_0= ST ( (lv_name_1_0= ruleQualifiedName ) ) )
            // InternalRuleEngineParser.g:1776:2: otherlv_0= ST ( (lv_name_1_0= ruleQualifiedName ) )
            {
            otherlv_0=(Token)match(input,ST,FOLLOW_5); 

                	newLeafNode(otherlv_0, grammarAccess.getSTCommandAccess().getSTKeyword_0());
                
            // InternalRuleEngineParser.g:1780:1: ( (lv_name_1_0= ruleQualifiedName ) )
            // InternalRuleEngineParser.g:1781:1: (lv_name_1_0= ruleQualifiedName )
            {
            // InternalRuleEngineParser.g:1781:1: (lv_name_1_0= ruleQualifiedName )
            // InternalRuleEngineParser.g:1782:3: lv_name_1_0= ruleQualifiedName
            {
             
            	        newCompositeNode(grammarAccess.getSTCommandAccess().getNameQualifiedNameParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleQualifiedName();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSTCommandRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.QualifiedName");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSTCommand"


    // $ANTLR start "entryRuleLDCommand"
    // InternalRuleEngineParser.g:1806:1: entryRuleLDCommand returns [EObject current=null] : iv_ruleLDCommand= ruleLDCommand EOF ;
    public final EObject entryRuleLDCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLDCommand = null;


        try {
            // InternalRuleEngineParser.g:1807:2: (iv_ruleLDCommand= ruleLDCommand EOF )
            // InternalRuleEngineParser.g:1808:2: iv_ruleLDCommand= ruleLDCommand EOF
            {
             newCompositeNode(grammarAccess.getLDCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLDCommand=ruleLDCommand();

            state._fsp--;

             current =iv_ruleLDCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLDCommand"


    // $ANTLR start "ruleLDCommand"
    // InternalRuleEngineParser.g:1815:1: ruleLDCommand returns [EObject current=null] : (otherlv_0= LD ( (lv_name_1_0= ruleQualifiedName ) )? ( (lv_value_2_0= ruleBooleanRule ) )? ) ;
    public final EObject ruleLDCommand() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_value_2_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1818:28: ( (otherlv_0= LD ( (lv_name_1_0= ruleQualifiedName ) )? ( (lv_value_2_0= ruleBooleanRule ) )? ) )
            // InternalRuleEngineParser.g:1819:1: (otherlv_0= LD ( (lv_name_1_0= ruleQualifiedName ) )? ( (lv_value_2_0= ruleBooleanRule ) )? )
            {
            // InternalRuleEngineParser.g:1819:1: (otherlv_0= LD ( (lv_name_1_0= ruleQualifiedName ) )? ( (lv_value_2_0= ruleBooleanRule ) )? )
            // InternalRuleEngineParser.g:1820:2: otherlv_0= LD ( (lv_name_1_0= ruleQualifiedName ) )? ( (lv_value_2_0= ruleBooleanRule ) )?
            {
            otherlv_0=(Token)match(input,LD,FOLLOW_23); 

                	newLeafNode(otherlv_0, grammarAccess.getLDCommandAccess().getLDKeyword_0());
                
            // InternalRuleEngineParser.g:1824:1: ( (lv_name_1_0= ruleQualifiedName ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==RULE_IDENTIFIER) ) {
                int LA21_1 = input.LA(2);

                if ( (LA21_1==EOF||LA21_1==END_FUNCTION_BLOCK||(LA21_1>=END_FUNCTION && LA21_1<=END_PROGRAM)||LA21_1==EQNBOOL||(LA21_1>=EQBOOL && LA21_1<=EQNINT)||LA21_1==EQINT||(LA21_1>=FALSE && LA21_1<=JMPCN)||LA21_1==TRUE||(LA21_1>=ADD && LA21_1<=DIV)||(LA21_1>=JMP && LA21_1<=SUB)||LA21_1==XOR||(LA21_1>=LD && LA21_1<=OR)||LA21_1==ST||LA21_1==Ampersand||LA21_1==FullStop||(LA21_1>=R && LA21_1<=S)||(LA21_1>=RULE_BEGIN && LA21_1<=RULE_END)||LA21_1==RULE_IDENTIFIER||LA21_1==RULE_ID) ) {
                    alt21=1;
                }
            }
            switch (alt21) {
                case 1 :
                    // InternalRuleEngineParser.g:1825:1: (lv_name_1_0= ruleQualifiedName )
                    {
                    // InternalRuleEngineParser.g:1825:1: (lv_name_1_0= ruleQualifiedName )
                    // InternalRuleEngineParser.g:1826:3: lv_name_1_0= ruleQualifiedName
                    {
                     
                    	        newCompositeNode(grammarAccess.getLDCommandAccess().getNameQualifiedNameParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_name_1_0=ruleQualifiedName();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getLDCommandRule());
                    	        }
                           		set(
                           			current, 
                           			"name",
                            		lv_name_1_0, 
                            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.QualifiedName");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // InternalRuleEngineParser.g:1842:3: ( (lv_value_2_0= ruleBooleanRule ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==FALSE||LA22_0==TRUE) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalRuleEngineParser.g:1843:1: (lv_value_2_0= ruleBooleanRule )
                    {
                    // InternalRuleEngineParser.g:1843:1: (lv_value_2_0= ruleBooleanRule )
                    // InternalRuleEngineParser.g:1844:3: lv_value_2_0= ruleBooleanRule
                    {
                     
                    	        newCompositeNode(grammarAccess.getLDCommandAccess().getValueBooleanRuleParserRuleCall_2_0()); 
                    	    
                    pushFollow(FOLLOW_2);
                    lv_value_2_0=ruleBooleanRule();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getLDCommandRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_2_0, 
                            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.BooleanRule");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLDCommand"


    // $ANTLR start "entryRuleMathCommand"
    // InternalRuleEngineParser.g:1868:1: entryRuleMathCommand returns [EObject current=null] : iv_ruleMathCommand= ruleMathCommand EOF ;
    public final EObject entryRuleMathCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMathCommand = null;


        try {
            // InternalRuleEngineParser.g:1869:2: (iv_ruleMathCommand= ruleMathCommand EOF )
            // InternalRuleEngineParser.g:1870:2: iv_ruleMathCommand= ruleMathCommand EOF
            {
             newCompositeNode(grammarAccess.getMathCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMathCommand=ruleMathCommand();

            state._fsp--;

             current =iv_ruleMathCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMathCommand"


    // $ANTLR start "ruleMathCommand"
    // InternalRuleEngineParser.g:1877:1: ruleMathCommand returns [EObject current=null] : (this_ADDOperator_0= ruleADDOperator | this_SUBOperator_1= ruleSUBOperator | this_MULOperator_2= ruleMULOperator | this_DIVOperator_3= ruleDIVOperator ) ;
    public final EObject ruleMathCommand() throws RecognitionException {
        EObject current = null;

        EObject this_ADDOperator_0 = null;

        EObject this_SUBOperator_1 = null;

        EObject this_MULOperator_2 = null;

        EObject this_DIVOperator_3 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1880:28: ( (this_ADDOperator_0= ruleADDOperator | this_SUBOperator_1= ruleSUBOperator | this_MULOperator_2= ruleMULOperator | this_DIVOperator_3= ruleDIVOperator ) )
            // InternalRuleEngineParser.g:1881:1: (this_ADDOperator_0= ruleADDOperator | this_SUBOperator_1= ruleSUBOperator | this_MULOperator_2= ruleMULOperator | this_DIVOperator_3= ruleDIVOperator )
            {
            // InternalRuleEngineParser.g:1881:1: (this_ADDOperator_0= ruleADDOperator | this_SUBOperator_1= ruleSUBOperator | this_MULOperator_2= ruleMULOperator | this_DIVOperator_3= ruleDIVOperator )
            int alt23=4;
            switch ( input.LA(1) ) {
            case ADD:
                {
                alt23=1;
                }
                break;
            case SUB:
                {
                alt23=2;
                }
                break;
            case MUL:
                {
                alt23=3;
                }
                break;
            case DIV:
                {
                alt23=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // InternalRuleEngineParser.g:1882:5: this_ADDOperator_0= ruleADDOperator
                    {
                     
                            newCompositeNode(grammarAccess.getMathCommandAccess().getADDOperatorParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_ADDOperator_0=ruleADDOperator();

                    state._fsp--;


                            current = this_ADDOperator_0;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:1892:5: this_SUBOperator_1= ruleSUBOperator
                    {
                     
                            newCompositeNode(grammarAccess.getMathCommandAccess().getSUBOperatorParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_SUBOperator_1=ruleSUBOperator();

                    state._fsp--;


                            current = this_SUBOperator_1;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalRuleEngineParser.g:1902:5: this_MULOperator_2= ruleMULOperator
                    {
                     
                            newCompositeNode(grammarAccess.getMathCommandAccess().getMULOperatorParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_MULOperator_2=ruleMULOperator();

                    state._fsp--;


                            current = this_MULOperator_2;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalRuleEngineParser.g:1912:5: this_DIVOperator_3= ruleDIVOperator
                    {
                     
                            newCompositeNode(grammarAccess.getMathCommandAccess().getDIVOperatorParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_2);
                    this_DIVOperator_3=ruleDIVOperator();

                    state._fsp--;


                            current = this_DIVOperator_3;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMathCommand"


    // $ANTLR start "entryRuleADDOperator"
    // InternalRuleEngineParser.g:1928:1: entryRuleADDOperator returns [EObject current=null] : iv_ruleADDOperator= ruleADDOperator EOF ;
    public final EObject entryRuleADDOperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleADDOperator = null;


        try {
            // InternalRuleEngineParser.g:1929:2: (iv_ruleADDOperator= ruleADDOperator EOF )
            // InternalRuleEngineParser.g:1930:2: iv_ruleADDOperator= ruleADDOperator EOF
            {
             newCompositeNode(grammarAccess.getADDOperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleADDOperator=ruleADDOperator();

            state._fsp--;

             current =iv_ruleADDOperator; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleADDOperator"


    // $ANTLR start "ruleADDOperator"
    // InternalRuleEngineParser.g:1937:1: ruleADDOperator returns [EObject current=null] : (otherlv_0= ADD ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleADDOperator() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1940:28: ( (otherlv_0= ADD ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalRuleEngineParser.g:1941:1: (otherlv_0= ADD ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalRuleEngineParser.g:1941:1: (otherlv_0= ADD ( (lv_name_1_0= RULE_ID ) ) )
            // InternalRuleEngineParser.g:1942:2: otherlv_0= ADD ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,ADD,FOLLOW_14); 

                	newLeafNode(otherlv_0, grammarAccess.getADDOperatorAccess().getADDKeyword_0());
                
            // InternalRuleEngineParser.g:1946:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalRuleEngineParser.g:1947:1: (lv_name_1_0= RULE_ID )
            {
            // InternalRuleEngineParser.g:1947:1: (lv_name_1_0= RULE_ID )
            // InternalRuleEngineParser.g:1948:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            			newLeafNode(lv_name_1_0, grammarAccess.getADDOperatorAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getADDOperatorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleADDOperator"


    // $ANTLR start "entryRuleSUBOperator"
    // InternalRuleEngineParser.g:1972:1: entryRuleSUBOperator returns [EObject current=null] : iv_ruleSUBOperator= ruleSUBOperator EOF ;
    public final EObject entryRuleSUBOperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSUBOperator = null;


        try {
            // InternalRuleEngineParser.g:1973:2: (iv_ruleSUBOperator= ruleSUBOperator EOF )
            // InternalRuleEngineParser.g:1974:2: iv_ruleSUBOperator= ruleSUBOperator EOF
            {
             newCompositeNode(grammarAccess.getSUBOperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSUBOperator=ruleSUBOperator();

            state._fsp--;

             current =iv_ruleSUBOperator; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSUBOperator"


    // $ANTLR start "ruleSUBOperator"
    // InternalRuleEngineParser.g:1981:1: ruleSUBOperator returns [EObject current=null] : (otherlv_0= SUB ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleSUBOperator() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:1984:28: ( (otherlv_0= SUB ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalRuleEngineParser.g:1985:1: (otherlv_0= SUB ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalRuleEngineParser.g:1985:1: (otherlv_0= SUB ( (lv_name_1_0= RULE_ID ) ) )
            // InternalRuleEngineParser.g:1986:2: otherlv_0= SUB ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,SUB,FOLLOW_14); 

                	newLeafNode(otherlv_0, grammarAccess.getSUBOperatorAccess().getSUBKeyword_0());
                
            // InternalRuleEngineParser.g:1990:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalRuleEngineParser.g:1991:1: (lv_name_1_0= RULE_ID )
            {
            // InternalRuleEngineParser.g:1991:1: (lv_name_1_0= RULE_ID )
            // InternalRuleEngineParser.g:1992:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            			newLeafNode(lv_name_1_0, grammarAccess.getSUBOperatorAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSUBOperatorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSUBOperator"


    // $ANTLR start "entryRuleMULOperator"
    // InternalRuleEngineParser.g:2016:1: entryRuleMULOperator returns [EObject current=null] : iv_ruleMULOperator= ruleMULOperator EOF ;
    public final EObject entryRuleMULOperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMULOperator = null;


        try {
            // InternalRuleEngineParser.g:2017:2: (iv_ruleMULOperator= ruleMULOperator EOF )
            // InternalRuleEngineParser.g:2018:2: iv_ruleMULOperator= ruleMULOperator EOF
            {
             newCompositeNode(grammarAccess.getMULOperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMULOperator=ruleMULOperator();

            state._fsp--;

             current =iv_ruleMULOperator; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMULOperator"


    // $ANTLR start "ruleMULOperator"
    // InternalRuleEngineParser.g:2025:1: ruleMULOperator returns [EObject current=null] : (otherlv_0= MUL ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleMULOperator() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2028:28: ( (otherlv_0= MUL ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalRuleEngineParser.g:2029:1: (otherlv_0= MUL ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalRuleEngineParser.g:2029:1: (otherlv_0= MUL ( (lv_name_1_0= RULE_ID ) ) )
            // InternalRuleEngineParser.g:2030:2: otherlv_0= MUL ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,MUL,FOLLOW_14); 

                	newLeafNode(otherlv_0, grammarAccess.getMULOperatorAccess().getMULKeyword_0());
                
            // InternalRuleEngineParser.g:2034:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalRuleEngineParser.g:2035:1: (lv_name_1_0= RULE_ID )
            {
            // InternalRuleEngineParser.g:2035:1: (lv_name_1_0= RULE_ID )
            // InternalRuleEngineParser.g:2036:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            			newLeafNode(lv_name_1_0, grammarAccess.getMULOperatorAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getMULOperatorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMULOperator"


    // $ANTLR start "entryRuleDIVOperator"
    // InternalRuleEngineParser.g:2060:1: entryRuleDIVOperator returns [EObject current=null] : iv_ruleDIVOperator= ruleDIVOperator EOF ;
    public final EObject entryRuleDIVOperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDIVOperator = null;


        try {
            // InternalRuleEngineParser.g:2061:2: (iv_ruleDIVOperator= ruleDIVOperator EOF )
            // InternalRuleEngineParser.g:2062:2: iv_ruleDIVOperator= ruleDIVOperator EOF
            {
             newCompositeNode(grammarAccess.getDIVOperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDIVOperator=ruleDIVOperator();

            state._fsp--;

             current =iv_ruleDIVOperator; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDIVOperator"


    // $ANTLR start "ruleDIVOperator"
    // InternalRuleEngineParser.g:2069:1: ruleDIVOperator returns [EObject current=null] : (otherlv_0= DIV ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleDIVOperator() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2072:28: ( (otherlv_0= DIV ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalRuleEngineParser.g:2073:1: (otherlv_0= DIV ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalRuleEngineParser.g:2073:1: (otherlv_0= DIV ( (lv_name_1_0= RULE_ID ) ) )
            // InternalRuleEngineParser.g:2074:2: otherlv_0= DIV ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,DIV,FOLLOW_14); 

                	newLeafNode(otherlv_0, grammarAccess.getDIVOperatorAccess().getDIVKeyword_0());
                
            // InternalRuleEngineParser.g:2078:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalRuleEngineParser.g:2079:1: (lv_name_1_0= RULE_ID )
            {
            // InternalRuleEngineParser.g:2079:1: (lv_name_1_0= RULE_ID )
            // InternalRuleEngineParser.g:2080:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            			newLeafNode(lv_name_1_0, grammarAccess.getDIVOperatorAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDIVOperatorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDIVOperator"


    // $ANTLR start "entryRuleLogicOpCommand"
    // InternalRuleEngineParser.g:2104:1: entryRuleLogicOpCommand returns [EObject current=null] : iv_ruleLogicOpCommand= ruleLogicOpCommand EOF ;
    public final EObject entryRuleLogicOpCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLogicOpCommand = null;


        try {
            // InternalRuleEngineParser.g:2105:2: (iv_ruleLogicOpCommand= ruleLogicOpCommand EOF )
            // InternalRuleEngineParser.g:2106:2: iv_ruleLogicOpCommand= ruleLogicOpCommand EOF
            {
             newCompositeNode(grammarAccess.getLogicOpCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLogicOpCommand=ruleLogicOpCommand();

            state._fsp--;

             current =iv_ruleLogicOpCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogicOpCommand"


    // $ANTLR start "ruleLogicOpCommand"
    // InternalRuleEngineParser.g:2113:1: ruleLogicOpCommand returns [EObject current=null] : (this_ANDOperator_0= ruleANDOperator | this_OROperator_1= ruleOROperator | this_XOROperator_2= ruleXOROperator ) ;
    public final EObject ruleLogicOpCommand() throws RecognitionException {
        EObject current = null;

        EObject this_ANDOperator_0 = null;

        EObject this_OROperator_1 = null;

        EObject this_XOROperator_2 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2116:28: ( (this_ANDOperator_0= ruleANDOperator | this_OROperator_1= ruleOROperator | this_XOROperator_2= ruleXOROperator ) )
            // InternalRuleEngineParser.g:2117:1: (this_ANDOperator_0= ruleANDOperator | this_OROperator_1= ruleOROperator | this_XOROperator_2= ruleXOROperator )
            {
            // InternalRuleEngineParser.g:2117:1: (this_ANDOperator_0= ruleANDOperator | this_OROperator_1= ruleOROperator | this_XOROperator_2= ruleXOROperator )
            int alt24=3;
            switch ( input.LA(1) ) {
            case AND:
            case Ampersand:
                {
                alt24=1;
                }
                break;
            case OR:
                {
                alt24=2;
                }
                break;
            case XOR:
                {
                alt24=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }

            switch (alt24) {
                case 1 :
                    // InternalRuleEngineParser.g:2118:5: this_ANDOperator_0= ruleANDOperator
                    {
                     
                            newCompositeNode(grammarAccess.getLogicOpCommandAccess().getANDOperatorParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_ANDOperator_0=ruleANDOperator();

                    state._fsp--;


                            current = this_ANDOperator_0;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:2128:5: this_OROperator_1= ruleOROperator
                    {
                     
                            newCompositeNode(grammarAccess.getLogicOpCommandAccess().getOROperatorParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_OROperator_1=ruleOROperator();

                    state._fsp--;


                            current = this_OROperator_1;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalRuleEngineParser.g:2138:5: this_XOROperator_2= ruleXOROperator
                    {
                     
                            newCompositeNode(grammarAccess.getLogicOpCommandAccess().getXOROperatorParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_XOROperator_2=ruleXOROperator();

                    state._fsp--;


                            current = this_XOROperator_2;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogicOpCommand"


    // $ANTLR start "entryRuleANDOperator"
    // InternalRuleEngineParser.g:2154:1: entryRuleANDOperator returns [EObject current=null] : iv_ruleANDOperator= ruleANDOperator EOF ;
    public final EObject entryRuleANDOperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleANDOperator = null;


        try {
            // InternalRuleEngineParser.g:2155:2: (iv_ruleANDOperator= ruleANDOperator EOF )
            // InternalRuleEngineParser.g:2156:2: iv_ruleANDOperator= ruleANDOperator EOF
            {
             newCompositeNode(grammarAccess.getANDOperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleANDOperator=ruleANDOperator();

            state._fsp--;

             current =iv_ruleANDOperator; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleANDOperator"


    // $ANTLR start "ruleANDOperator"
    // InternalRuleEngineParser.g:2163:1: ruleANDOperator returns [EObject current=null] : ( (otherlv_0= AND | otherlv_1= Ampersand ) ( (lv_name_2_0= RULE_ID ) ) ) ;
    public final EObject ruleANDOperator() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2166:28: ( ( (otherlv_0= AND | otherlv_1= Ampersand ) ( (lv_name_2_0= RULE_ID ) ) ) )
            // InternalRuleEngineParser.g:2167:1: ( (otherlv_0= AND | otherlv_1= Ampersand ) ( (lv_name_2_0= RULE_ID ) ) )
            {
            // InternalRuleEngineParser.g:2167:1: ( (otherlv_0= AND | otherlv_1= Ampersand ) ( (lv_name_2_0= RULE_ID ) ) )
            // InternalRuleEngineParser.g:2167:2: (otherlv_0= AND | otherlv_1= Ampersand ) ( (lv_name_2_0= RULE_ID ) )
            {
            // InternalRuleEngineParser.g:2167:2: (otherlv_0= AND | otherlv_1= Ampersand )
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==AND) ) {
                alt25=1;
            }
            else if ( (LA25_0==Ampersand) ) {
                alt25=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }
            switch (alt25) {
                case 1 :
                    // InternalRuleEngineParser.g:2168:2: otherlv_0= AND
                    {
                    otherlv_0=(Token)match(input,AND,FOLLOW_14); 

                        	newLeafNode(otherlv_0, grammarAccess.getANDOperatorAccess().getANDKeyword_0_0());
                        

                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:2174:2: otherlv_1= Ampersand
                    {
                    otherlv_1=(Token)match(input,Ampersand,FOLLOW_14); 

                        	newLeafNode(otherlv_1, grammarAccess.getANDOperatorAccess().getAmpersandKeyword_0_1());
                        

                    }
                    break;

            }

            // InternalRuleEngineParser.g:2178:2: ( (lv_name_2_0= RULE_ID ) )
            // InternalRuleEngineParser.g:2179:1: (lv_name_2_0= RULE_ID )
            {
            // InternalRuleEngineParser.g:2179:1: (lv_name_2_0= RULE_ID )
            // InternalRuleEngineParser.g:2180:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            			newLeafNode(lv_name_2_0, grammarAccess.getANDOperatorAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getANDOperatorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleANDOperator"


    // $ANTLR start "entryRuleOROperator"
    // InternalRuleEngineParser.g:2204:1: entryRuleOROperator returns [EObject current=null] : iv_ruleOROperator= ruleOROperator EOF ;
    public final EObject entryRuleOROperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOROperator = null;


        try {
            // InternalRuleEngineParser.g:2205:2: (iv_ruleOROperator= ruleOROperator EOF )
            // InternalRuleEngineParser.g:2206:2: iv_ruleOROperator= ruleOROperator EOF
            {
             newCompositeNode(grammarAccess.getOROperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOROperator=ruleOROperator();

            state._fsp--;

             current =iv_ruleOROperator; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOROperator"


    // $ANTLR start "ruleOROperator"
    // InternalRuleEngineParser.g:2213:1: ruleOROperator returns [EObject current=null] : (otherlv_0= OR ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleOROperator() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2216:28: ( (otherlv_0= OR ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalRuleEngineParser.g:2217:1: (otherlv_0= OR ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalRuleEngineParser.g:2217:1: (otherlv_0= OR ( (lv_name_1_0= RULE_ID ) ) )
            // InternalRuleEngineParser.g:2218:2: otherlv_0= OR ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,OR,FOLLOW_14); 

                	newLeafNode(otherlv_0, grammarAccess.getOROperatorAccess().getORKeyword_0());
                
            // InternalRuleEngineParser.g:2222:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalRuleEngineParser.g:2223:1: (lv_name_1_0= RULE_ID )
            {
            // InternalRuleEngineParser.g:2223:1: (lv_name_1_0= RULE_ID )
            // InternalRuleEngineParser.g:2224:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            			newLeafNode(lv_name_1_0, grammarAccess.getOROperatorAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getOROperatorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOROperator"


    // $ANTLR start "entryRuleXOROperator"
    // InternalRuleEngineParser.g:2248:1: entryRuleXOROperator returns [EObject current=null] : iv_ruleXOROperator= ruleXOROperator EOF ;
    public final EObject entryRuleXOROperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXOROperator = null;


        try {
            // InternalRuleEngineParser.g:2249:2: (iv_ruleXOROperator= ruleXOROperator EOF )
            // InternalRuleEngineParser.g:2250:2: iv_ruleXOROperator= ruleXOROperator EOF
            {
             newCompositeNode(grammarAccess.getXOROperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXOROperator=ruleXOROperator();

            state._fsp--;

             current =iv_ruleXOROperator; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXOROperator"


    // $ANTLR start "ruleXOROperator"
    // InternalRuleEngineParser.g:2257:1: ruleXOROperator returns [EObject current=null] : (otherlv_0= XOR ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleXOROperator() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2260:28: ( (otherlv_0= XOR ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalRuleEngineParser.g:2261:1: (otherlv_0= XOR ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalRuleEngineParser.g:2261:1: (otherlv_0= XOR ( (lv_name_1_0= RULE_ID ) ) )
            // InternalRuleEngineParser.g:2262:2: otherlv_0= XOR ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,XOR,FOLLOW_14); 

                	newLeafNode(otherlv_0, grammarAccess.getXOROperatorAccess().getXORKeyword_0());
                
            // InternalRuleEngineParser.g:2266:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalRuleEngineParser.g:2267:1: (lv_name_1_0= RULE_ID )
            {
            // InternalRuleEngineParser.g:2267:1: (lv_name_1_0= RULE_ID )
            // InternalRuleEngineParser.g:2268:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            			newLeafNode(lv_name_1_0, grammarAccess.getXOROperatorAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getXOROperatorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXOROperator"


    // $ANTLR start "entryRuleReturnCommand"
    // InternalRuleEngineParser.g:2292:1: entryRuleReturnCommand returns [EObject current=null] : iv_ruleReturnCommand= ruleReturnCommand EOF ;
    public final EObject entryRuleReturnCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReturnCommand = null;


        try {
            // InternalRuleEngineParser.g:2293:2: (iv_ruleReturnCommand= ruleReturnCommand EOF )
            // InternalRuleEngineParser.g:2294:2: iv_ruleReturnCommand= ruleReturnCommand EOF
            {
             newCompositeNode(grammarAccess.getReturnCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReturnCommand=ruleReturnCommand();

            state._fsp--;

             current =iv_ruleReturnCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReturnCommand"


    // $ANTLR start "ruleReturnCommand"
    // InternalRuleEngineParser.g:2301:1: ruleReturnCommand returns [EObject current=null] : ( (lv_ret_0_0= RET ) ) ;
    public final EObject ruleReturnCommand() throws RecognitionException {
        EObject current = null;

        Token lv_ret_0_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2304:28: ( ( (lv_ret_0_0= RET ) ) )
            // InternalRuleEngineParser.g:2305:1: ( (lv_ret_0_0= RET ) )
            {
            // InternalRuleEngineParser.g:2305:1: ( (lv_ret_0_0= RET ) )
            // InternalRuleEngineParser.g:2306:1: (lv_ret_0_0= RET )
            {
            // InternalRuleEngineParser.g:2306:1: (lv_ret_0_0= RET )
            // InternalRuleEngineParser.g:2307:3: lv_ret_0_0= RET
            {
            lv_ret_0_0=(Token)match(input,RET,FOLLOW_2); 

                    newLeafNode(lv_ret_0_0, grammarAccess.getReturnCommandAccess().getRetRETKeyword_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getReturnCommandRule());
            	        }
                   		setWithLastConsumed(current, "ret", lv_ret_0_0, "RET");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReturnCommand"


    // $ANTLR start "entryRuleCallCommand"
    // InternalRuleEngineParser.g:2329:1: entryRuleCallCommand returns [EObject current=null] : iv_ruleCallCommand= ruleCallCommand EOF ;
    public final EObject entryRuleCallCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCallCommand = null;


        try {
            // InternalRuleEngineParser.g:2330:2: (iv_ruleCallCommand= ruleCallCommand EOF )
            // InternalRuleEngineParser.g:2331:2: iv_ruleCallCommand= ruleCallCommand EOF
            {
             newCompositeNode(grammarAccess.getCallCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCallCommand=ruleCallCommand();

            state._fsp--;

             current =iv_ruleCallCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCallCommand"


    // $ANTLR start "ruleCallCommand"
    // InternalRuleEngineParser.g:2338:1: ruleCallCommand returns [EObject current=null] : (otherlv_0= CAL ( (lv_command_1_0= ruleCommandTypes ) ) ) ;
    public final EObject ruleCallCommand() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_command_1_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2341:28: ( (otherlv_0= CAL ( (lv_command_1_0= ruleCommandTypes ) ) ) )
            // InternalRuleEngineParser.g:2342:1: (otherlv_0= CAL ( (lv_command_1_0= ruleCommandTypes ) ) )
            {
            // InternalRuleEngineParser.g:2342:1: (otherlv_0= CAL ( (lv_command_1_0= ruleCommandTypes ) ) )
            // InternalRuleEngineParser.g:2343:2: otherlv_0= CAL ( (lv_command_1_0= ruleCommandTypes ) )
            {
            otherlv_0=(Token)match(input,CAL,FOLLOW_5); 

                	newLeafNode(otherlv_0, grammarAccess.getCallCommandAccess().getCALKeyword_0());
                
            // InternalRuleEngineParser.g:2347:1: ( (lv_command_1_0= ruleCommandTypes ) )
            // InternalRuleEngineParser.g:2348:1: (lv_command_1_0= ruleCommandTypes )
            {
            // InternalRuleEngineParser.g:2348:1: (lv_command_1_0= ruleCommandTypes )
            // InternalRuleEngineParser.g:2349:3: lv_command_1_0= ruleCommandTypes
            {
             
            	        newCompositeNode(grammarAccess.getCallCommandAccess().getCommandCommandTypesParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_command_1_0=ruleCommandTypes();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCallCommandRule());
            	        }
                   		set(
                   			current, 
                   			"command",
                    		lv_command_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.CommandTypes");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCallCommand"


    // $ANTLR start "entryRuleCommandTypes"
    // InternalRuleEngineParser.g:2373:1: entryRuleCommandTypes returns [EObject current=null] : iv_ruleCommandTypes= ruleCommandTypes EOF ;
    public final EObject entryRuleCommandTypes() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCommandTypes = null;


        try {
            // InternalRuleEngineParser.g:2374:2: (iv_ruleCommandTypes= ruleCommandTypes EOF )
            // InternalRuleEngineParser.g:2375:2: iv_ruleCommandTypes= ruleCommandTypes EOF
            {
             newCompositeNode(grammarAccess.getCommandTypesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCommandTypes=ruleCommandTypes();

            state._fsp--;

             current =iv_ruleCommandTypes; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCommandTypes"


    // $ANTLR start "ruleCommandTypes"
    // InternalRuleEngineParser.g:2382:1: ruleCommandTypes returns [EObject current=null] : this_TimerCallCommand_0= ruleTimerCallCommand ;
    public final EObject ruleCommandTypes() throws RecognitionException {
        EObject current = null;

        EObject this_TimerCallCommand_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2385:28: (this_TimerCallCommand_0= ruleTimerCallCommand )
            // InternalRuleEngineParser.g:2387:5: this_TimerCallCommand_0= ruleTimerCallCommand
            {
             
                    newCompositeNode(grammarAccess.getCommandTypesAccess().getTimerCallCommandParserRuleCall()); 
                
            pushFollow(FOLLOW_2);
            this_TimerCallCommand_0=ruleTimerCallCommand();

            state._fsp--;


                    current = this_TimerCallCommand_0;
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCommandTypes"


    // $ANTLR start "entryRuleInitData"
    // InternalRuleEngineParser.g:2403:1: entryRuleInitData returns [EObject current=null] : iv_ruleInitData= ruleInitData EOF ;
    public final EObject entryRuleInitData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInitData = null;


        try {
            // InternalRuleEngineParser.g:2404:2: (iv_ruleInitData= ruleInitData EOF )
            // InternalRuleEngineParser.g:2405:2: iv_ruleInitData= ruleInitData EOF
            {
             newCompositeNode(grammarAccess.getInitDataRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInitData=ruleInitData();

            state._fsp--;

             current =iv_ruleInitData; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInitData"


    // $ANTLR start "ruleInitData"
    // InternalRuleEngineParser.g:2412:1: ruleInitData returns [EObject current=null] : (this_TimeData_0= ruleTimeData | this_BoolData_1= ruleBoolData ) ;
    public final EObject ruleInitData() throws RecognitionException {
        EObject current = null;

        EObject this_TimeData_0 = null;

        EObject this_BoolData_1 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2415:28: ( (this_TimeData_0= ruleTimeData | this_BoolData_1= ruleBoolData ) )
            // InternalRuleEngineParser.g:2416:1: (this_TimeData_0= ruleTimeData | this_BoolData_1= ruleBoolData )
            {
            // InternalRuleEngineParser.g:2416:1: (this_TimeData_0= ruleTimeData | this_BoolData_1= ruleBoolData )
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==TIME_1||(LA26_0>=T && LA26_0<=T_1)) ) {
                alt26=1;
            }
            else if ( (LA26_0==FALSE||LA26_0==TRUE) ) {
                alt26=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }
            switch (alt26) {
                case 1 :
                    // InternalRuleEngineParser.g:2417:5: this_TimeData_0= ruleTimeData
                    {
                     
                            newCompositeNode(grammarAccess.getInitDataAccess().getTimeDataParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_TimeData_0=ruleTimeData();

                    state._fsp--;


                            current = this_TimeData_0;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:2427:5: this_BoolData_1= ruleBoolData
                    {
                     
                            newCompositeNode(grammarAccess.getInitDataAccess().getBoolDataParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_BoolData_1=ruleBoolData();

                    state._fsp--;


                            current = this_BoolData_1;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInitData"


    // $ANTLR start "entryRuleBoolData"
    // InternalRuleEngineParser.g:2443:1: entryRuleBoolData returns [EObject current=null] : iv_ruleBoolData= ruleBoolData EOF ;
    public final EObject entryRuleBoolData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBoolData = null;


        try {
            // InternalRuleEngineParser.g:2444:2: (iv_ruleBoolData= ruleBoolData EOF )
            // InternalRuleEngineParser.g:2445:2: iv_ruleBoolData= ruleBoolData EOF
            {
             newCompositeNode(grammarAccess.getBoolDataRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBoolData=ruleBoolData();

            state._fsp--;

             current =iv_ruleBoolData; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolData"


    // $ANTLR start "ruleBoolData"
    // InternalRuleEngineParser.g:2452:1: ruleBoolData returns [EObject current=null] : ( (lv_value_0_0= ruleBooleanRule ) ) ;
    public final EObject ruleBoolData() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2455:28: ( ( (lv_value_0_0= ruleBooleanRule ) ) )
            // InternalRuleEngineParser.g:2456:1: ( (lv_value_0_0= ruleBooleanRule ) )
            {
            // InternalRuleEngineParser.g:2456:1: ( (lv_value_0_0= ruleBooleanRule ) )
            // InternalRuleEngineParser.g:2457:1: (lv_value_0_0= ruleBooleanRule )
            {
            // InternalRuleEngineParser.g:2457:1: (lv_value_0_0= ruleBooleanRule )
            // InternalRuleEngineParser.g:2458:3: lv_value_0_0= ruleBooleanRule
            {
             
            	        newCompositeNode(grammarAccess.getBoolDataAccess().getValueBooleanRuleParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_value_0_0=ruleBooleanRule();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBoolDataRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.BooleanRule");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolData"


    // $ANTLR start "entryRuleTimeData"
    // InternalRuleEngineParser.g:2482:1: entryRuleTimeData returns [EObject current=null] : iv_ruleTimeData= ruleTimeData EOF ;
    public final EObject entryRuleTimeData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimeData = null;


        try {
            // InternalRuleEngineParser.g:2483:2: (iv_ruleTimeData= ruleTimeData EOF )
            // InternalRuleEngineParser.g:2484:2: iv_ruleTimeData= ruleTimeData EOF
            {
             newCompositeNode(grammarAccess.getTimeDataRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTimeData=ruleTimeData();

            state._fsp--;

             current =iv_ruleTimeData; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimeData"


    // $ANTLR start "ruleTimeData"
    // InternalRuleEngineParser.g:2491:1: ruleTimeData returns [EObject current=null] : ( (lv_value_0_0= ruleTimeValue ) ) ;
    public final EObject ruleTimeData() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2494:28: ( ( (lv_value_0_0= ruleTimeValue ) ) )
            // InternalRuleEngineParser.g:2495:1: ( (lv_value_0_0= ruleTimeValue ) )
            {
            // InternalRuleEngineParser.g:2495:1: ( (lv_value_0_0= ruleTimeValue ) )
            // InternalRuleEngineParser.g:2496:1: (lv_value_0_0= ruleTimeValue )
            {
            // InternalRuleEngineParser.g:2496:1: (lv_value_0_0= ruleTimeValue )
            // InternalRuleEngineParser.g:2497:3: lv_value_0_0= ruleTimeValue
            {
             
            	        newCompositeNode(grammarAccess.getTimeDataAccess().getValueTimeValueParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_value_0_0=ruleTimeValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTimeDataRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.TimeValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimeData"


    // $ANTLR start "entryRuleTimeValue"
    // InternalRuleEngineParser.g:2521:1: entryRuleTimeValue returns [String current=null] : iv_ruleTimeValue= ruleTimeValue EOF ;
    public final String entryRuleTimeValue() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleTimeValue = null;


        try {
            // InternalRuleEngineParser.g:2522:1: (iv_ruleTimeValue= ruleTimeValue EOF )
            // InternalRuleEngineParser.g:2523:2: iv_ruleTimeValue= ruleTimeValue EOF
            {
             newCompositeNode(grammarAccess.getTimeValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTimeValue=ruleTimeValue();

            state._fsp--;

             current =iv_ruleTimeValue.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimeValue"


    // $ANTLR start "ruleTimeValue"
    // InternalRuleEngineParser.g:2530:1: ruleTimeValue returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= TIME_1 | kw= T | kw= T_1 ) this_TIME_3= RULE_TIME ) ;
    public final AntlrDatatypeRuleToken ruleTimeValue() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_TIME_3=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2534:6: ( ( (kw= TIME_1 | kw= T | kw= T_1 ) this_TIME_3= RULE_TIME ) )
            // InternalRuleEngineParser.g:2535:1: ( (kw= TIME_1 | kw= T | kw= T_1 ) this_TIME_3= RULE_TIME )
            {
            // InternalRuleEngineParser.g:2535:1: ( (kw= TIME_1 | kw= T | kw= T_1 ) this_TIME_3= RULE_TIME )
            // InternalRuleEngineParser.g:2535:2: (kw= TIME_1 | kw= T | kw= T_1 ) this_TIME_3= RULE_TIME
            {
            // InternalRuleEngineParser.g:2535:2: (kw= TIME_1 | kw= T | kw= T_1 )
            int alt27=3;
            switch ( input.LA(1) ) {
            case TIME_1:
                {
                alt27=1;
                }
                break;
            case T:
                {
                alt27=2;
                }
                break;
            case T_1:
                {
                alt27=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }

            switch (alt27) {
                case 1 :
                    // InternalRuleEngineParser.g:2536:2: kw= TIME_1
                    {
                    kw=(Token)match(input,TIME_1,FOLLOW_25); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getTimeValueAccess().getTIMEKeyword_0_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:2543:2: kw= T
                    {
                    kw=(Token)match(input,T,FOLLOW_25); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getTimeValueAccess().getTKeyword_0_1()); 
                        

                    }
                    break;
                case 3 :
                    // InternalRuleEngineParser.g:2550:2: kw= T_1
                    {
                    kw=(Token)match(input,T_1,FOLLOW_25); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getTimeValueAccess().getTKeyword_0_2()); 
                        

                    }
                    break;

            }

            this_TIME_3=(Token)match(input,RULE_TIME,FOLLOW_2); 

            		current.merge(this_TIME_3);
                
             
                newLeafNode(this_TIME_3, grammarAccess.getTimeValueAccess().getTIMETerminalRuleCall_1()); 
                

            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimeValue"


    // $ANTLR start "entryRuleVar"
    // InternalRuleEngineParser.g:2570:1: entryRuleVar returns [EObject current=null] : iv_ruleVar= ruleVar EOF ;
    public final EObject entryRuleVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVar = null;


        try {
            // InternalRuleEngineParser.g:2571:2: (iv_ruleVar= ruleVar EOF )
            // InternalRuleEngineParser.g:2572:2: iv_ruleVar= ruleVar EOF
            {
             newCompositeNode(grammarAccess.getVarRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVar=ruleVar();

            state._fsp--;

             current =iv_ruleVar; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVar"


    // $ANTLR start "ruleVar"
    // InternalRuleEngineParser.g:2579:1: ruleVar returns [EObject current=null] : (this_BEGIN_0= RULE_BEGIN ( (lv_vars_1_0= ruleAVar ) )* this_END_2= RULE_END ) ;
    public final EObject ruleVar() throws RecognitionException {
        EObject current = null;

        Token this_BEGIN_0=null;
        Token this_END_2=null;
        EObject lv_vars_1_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2582:28: ( (this_BEGIN_0= RULE_BEGIN ( (lv_vars_1_0= ruleAVar ) )* this_END_2= RULE_END ) )
            // InternalRuleEngineParser.g:2583:1: (this_BEGIN_0= RULE_BEGIN ( (lv_vars_1_0= ruleAVar ) )* this_END_2= RULE_END )
            {
            // InternalRuleEngineParser.g:2583:1: (this_BEGIN_0= RULE_BEGIN ( (lv_vars_1_0= ruleAVar ) )* this_END_2= RULE_END )
            // InternalRuleEngineParser.g:2583:2: this_BEGIN_0= RULE_BEGIN ( (lv_vars_1_0= ruleAVar ) )* this_END_2= RULE_END
            {
            this_BEGIN_0=(Token)match(input,RULE_BEGIN,FOLLOW_26); 
             
                newLeafNode(this_BEGIN_0, grammarAccess.getVarAccess().getBEGINTerminalRuleCall_0()); 
                
            // InternalRuleEngineParser.g:2587:1: ( (lv_vars_1_0= ruleAVar ) )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==RULE_IDENTIFIER) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalRuleEngineParser.g:2588:1: (lv_vars_1_0= ruleAVar )
            	    {
            	    // InternalRuleEngineParser.g:2588:1: (lv_vars_1_0= ruleAVar )
            	    // InternalRuleEngineParser.g:2589:3: lv_vars_1_0= ruleAVar
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getVarAccess().getVarsAVarParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_26);
            	    lv_vars_1_0=ruleAVar();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getVarRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"vars",
            	            		lv_vars_1_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.AVar");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

            this_END_2=(Token)match(input,RULE_END,FOLLOW_2); 
             
                newLeafNode(this_END_2, grammarAccess.getVarAccess().getENDTerminalRuleCall_2()); 
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVar"


    // $ANTLR start "entryRuleBooleanRule"
    // InternalRuleEngineParser.g:2617:1: entryRuleBooleanRule returns [String current=null] : iv_ruleBooleanRule= ruleBooleanRule EOF ;
    public final String entryRuleBooleanRule() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBooleanRule = null;


        try {
            // InternalRuleEngineParser.g:2618:1: (iv_ruleBooleanRule= ruleBooleanRule EOF )
            // InternalRuleEngineParser.g:2619:2: iv_ruleBooleanRule= ruleBooleanRule EOF
            {
             newCompositeNode(grammarAccess.getBooleanRuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBooleanRule=ruleBooleanRule();

            state._fsp--;

             current =iv_ruleBooleanRule.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanRule"


    // $ANTLR start "ruleBooleanRule"
    // InternalRuleEngineParser.g:2626:1: ruleBooleanRule returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= TRUE | kw= FALSE ) ;
    public final AntlrDatatypeRuleToken ruleBooleanRule() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2630:6: ( (kw= TRUE | kw= FALSE ) )
            // InternalRuleEngineParser.g:2631:1: (kw= TRUE | kw= FALSE )
            {
            // InternalRuleEngineParser.g:2631:1: (kw= TRUE | kw= FALSE )
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==TRUE) ) {
                alt29=1;
            }
            else if ( (LA29_0==FALSE) ) {
                alt29=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }
            switch (alt29) {
                case 1 :
                    // InternalRuleEngineParser.g:2632:2: kw= TRUE
                    {
                    kw=(Token)match(input,TRUE,FOLLOW_2); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getBooleanRuleAccess().getTRUEKeyword_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:2639:2: kw= FALSE
                    {
                    kw=(Token)match(input,FALSE,FOLLOW_2); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getBooleanRuleAccess().getFALSEKeyword_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanRule"


    // $ANTLR start "entryRuleType"
    // InternalRuleEngineParser.g:2652:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalRuleEngineParser.g:2653:2: (iv_ruleType= ruleType EOF )
            // InternalRuleEngineParser.g:2654:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalRuleEngineParser.g:2661:1: ruleType returns [EObject current=null] : this_GenericType_0= ruleGenericType ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject this_GenericType_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2664:28: (this_GenericType_0= ruleGenericType )
            // InternalRuleEngineParser.g:2666:5: this_GenericType_0= ruleGenericType
            {
             
                    newCompositeNode(grammarAccess.getTypeAccess().getGenericTypeParserRuleCall()); 
                
            pushFollow(FOLLOW_2);
            this_GenericType_0=ruleGenericType();

            state._fsp--;


                    current = this_GenericType_0;
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleGenericType"
    // InternalRuleEngineParser.g:2682:1: entryRuleGenericType returns [EObject current=null] : iv_ruleGenericType= ruleGenericType EOF ;
    public final EObject entryRuleGenericType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGenericType = null;


        try {
            // InternalRuleEngineParser.g:2683:2: (iv_ruleGenericType= ruleGenericType EOF )
            // InternalRuleEngineParser.g:2684:2: iv_ruleGenericType= ruleGenericType EOF
            {
             newCompositeNode(grammarAccess.getGenericTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGenericType=ruleGenericType();

            state._fsp--;

             current =iv_ruleGenericType; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGenericType"


    // $ANTLR start "ruleGenericType"
    // InternalRuleEngineParser.g:2691:1: ruleGenericType returns [EObject current=null] : ( (lv_type_0_0= ruleSimpleTypeName ) ) ;
    public final EObject ruleGenericType() throws RecognitionException {
        EObject current = null;

        Enumerator lv_type_0_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2694:28: ( ( (lv_type_0_0= ruleSimpleTypeName ) ) )
            // InternalRuleEngineParser.g:2695:1: ( (lv_type_0_0= ruleSimpleTypeName ) )
            {
            // InternalRuleEngineParser.g:2695:1: ( (lv_type_0_0= ruleSimpleTypeName ) )
            // InternalRuleEngineParser.g:2696:1: (lv_type_0_0= ruleSimpleTypeName )
            {
            // InternalRuleEngineParser.g:2696:1: (lv_type_0_0= ruleSimpleTypeName )
            // InternalRuleEngineParser.g:2697:3: lv_type_0_0= ruleSimpleTypeName
            {
             
            	        newCompositeNode(grammarAccess.getGenericTypeAccess().getTypeSimpleTypeNameEnumRuleCall_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_type_0_0=ruleSimpleTypeName();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getGenericTypeRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.SimpleTypeName");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGenericType"


    // $ANTLR start "entryRuleVars"
    // InternalRuleEngineParser.g:2721:1: entryRuleVars returns [EObject current=null] : iv_ruleVars= ruleVars EOF ;
    public final EObject entryRuleVars() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVars = null;


        try {
            // InternalRuleEngineParser.g:2722:2: (iv_ruleVars= ruleVars EOF )
            // InternalRuleEngineParser.g:2723:2: iv_ruleVars= ruleVars EOF
            {
             newCompositeNode(grammarAccess.getVarsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVars=ruleVars();

            state._fsp--;

             current =iv_ruleVars; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVars"


    // $ANTLR start "ruleVars"
    // InternalRuleEngineParser.g:2730:1: ruleVars returns [EObject current=null] : (otherlv_0= VAR ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR ) ;
    public final EObject ruleVars() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_vars_1_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2733:28: ( (otherlv_0= VAR ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR ) )
            // InternalRuleEngineParser.g:2734:1: (otherlv_0= VAR ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR )
            {
            // InternalRuleEngineParser.g:2734:1: (otherlv_0= VAR ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR )
            // InternalRuleEngineParser.g:2735:2: otherlv_0= VAR ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR
            {
            otherlv_0=(Token)match(input,VAR,FOLLOW_27); 

                	newLeafNode(otherlv_0, grammarAccess.getVarsAccess().getVARKeyword_0());
                
            // InternalRuleEngineParser.g:2739:1: ( (lv_vars_1_0= ruleVar ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==RULE_BEGIN) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalRuleEngineParser.g:2740:1: (lv_vars_1_0= ruleVar )
            	    {
            	    // InternalRuleEngineParser.g:2740:1: (lv_vars_1_0= ruleVar )
            	    // InternalRuleEngineParser.g:2741:3: lv_vars_1_0= ruleVar
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getVarsAccess().getVarsVarParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_27);
            	    lv_vars_1_0=ruleVar();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getVarsRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"vars",
            	            		lv_vars_1_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Var");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

            otherlv_2=(Token)match(input,END_VAR,FOLLOW_2); 

                	newLeafNode(otherlv_2, grammarAccess.getVarsAccess().getEND_VARKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVars"


    // $ANTLR start "entryRuleInputVars"
    // InternalRuleEngineParser.g:2770:1: entryRuleInputVars returns [EObject current=null] : iv_ruleInputVars= ruleInputVars EOF ;
    public final EObject entryRuleInputVars() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputVars = null;


        try {
            // InternalRuleEngineParser.g:2771:2: (iv_ruleInputVars= ruleInputVars EOF )
            // InternalRuleEngineParser.g:2772:2: iv_ruleInputVars= ruleInputVars EOF
            {
             newCompositeNode(grammarAccess.getInputVarsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInputVars=ruleInputVars();

            state._fsp--;

             current =iv_ruleInputVars; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputVars"


    // $ANTLR start "ruleInputVars"
    // InternalRuleEngineParser.g:2779:1: ruleInputVars returns [EObject current=null] : (otherlv_0= VAR_INPUT ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR ) ;
    public final EObject ruleInputVars() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_vars_1_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2782:28: ( (otherlv_0= VAR_INPUT ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR ) )
            // InternalRuleEngineParser.g:2783:1: (otherlv_0= VAR_INPUT ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR )
            {
            // InternalRuleEngineParser.g:2783:1: (otherlv_0= VAR_INPUT ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR )
            // InternalRuleEngineParser.g:2784:2: otherlv_0= VAR_INPUT ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR
            {
            otherlv_0=(Token)match(input,VAR_INPUT,FOLLOW_27); 

                	newLeafNode(otherlv_0, grammarAccess.getInputVarsAccess().getVAR_INPUTKeyword_0());
                
            // InternalRuleEngineParser.g:2788:1: ( (lv_vars_1_0= ruleVar ) )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==RULE_BEGIN) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalRuleEngineParser.g:2789:1: (lv_vars_1_0= ruleVar )
            	    {
            	    // InternalRuleEngineParser.g:2789:1: (lv_vars_1_0= ruleVar )
            	    // InternalRuleEngineParser.g:2790:3: lv_vars_1_0= ruleVar
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getInputVarsAccess().getVarsVarParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_27);
            	    lv_vars_1_0=ruleVar();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getInputVarsRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"vars",
            	            		lv_vars_1_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Var");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

            otherlv_2=(Token)match(input,END_VAR,FOLLOW_2); 

                	newLeafNode(otherlv_2, grammarAccess.getInputVarsAccess().getEND_VARKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputVars"


    // $ANTLR start "entryRuleOutputVars"
    // InternalRuleEngineParser.g:2819:1: entryRuleOutputVars returns [EObject current=null] : iv_ruleOutputVars= ruleOutputVars EOF ;
    public final EObject entryRuleOutputVars() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputVars = null;


        try {
            // InternalRuleEngineParser.g:2820:2: (iv_ruleOutputVars= ruleOutputVars EOF )
            // InternalRuleEngineParser.g:2821:2: iv_ruleOutputVars= ruleOutputVars EOF
            {
             newCompositeNode(grammarAccess.getOutputVarsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOutputVars=ruleOutputVars();

            state._fsp--;

             current =iv_ruleOutputVars; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputVars"


    // $ANTLR start "ruleOutputVars"
    // InternalRuleEngineParser.g:2828:1: ruleOutputVars returns [EObject current=null] : (otherlv_0= VAR_OUTPUT ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR ) ;
    public final EObject ruleOutputVars() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_vars_1_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2831:28: ( (otherlv_0= VAR_OUTPUT ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR ) )
            // InternalRuleEngineParser.g:2832:1: (otherlv_0= VAR_OUTPUT ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR )
            {
            // InternalRuleEngineParser.g:2832:1: (otherlv_0= VAR_OUTPUT ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR )
            // InternalRuleEngineParser.g:2833:2: otherlv_0= VAR_OUTPUT ( (lv_vars_1_0= ruleVar ) )* otherlv_2= END_VAR
            {
            otherlv_0=(Token)match(input,VAR_OUTPUT,FOLLOW_27); 

                	newLeafNode(otherlv_0, grammarAccess.getOutputVarsAccess().getVAR_OUTPUTKeyword_0());
                
            // InternalRuleEngineParser.g:2837:1: ( (lv_vars_1_0= ruleVar ) )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==RULE_BEGIN) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // InternalRuleEngineParser.g:2838:1: (lv_vars_1_0= ruleVar )
            	    {
            	    // InternalRuleEngineParser.g:2838:1: (lv_vars_1_0= ruleVar )
            	    // InternalRuleEngineParser.g:2839:3: lv_vars_1_0= ruleVar
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getOutputVarsAccess().getVarsVarParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_27);
            	    lv_vars_1_0=ruleVar();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getOutputVarsRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"vars",
            	            		lv_vars_1_0, 
            	            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Var");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);

            otherlv_2=(Token)match(input,END_VAR,FOLLOW_2); 

                	newLeafNode(otherlv_2, grammarAccess.getOutputVarsAccess().getEND_VARKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputVars"


    // $ANTLR start "entryRuleAVar"
    // InternalRuleEngineParser.g:2868:1: entryRuleAVar returns [EObject current=null] : iv_ruleAVar= ruleAVar EOF ;
    public final EObject entryRuleAVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAVar = null;


        try {
            // InternalRuleEngineParser.g:2869:2: (iv_ruleAVar= ruleAVar EOF )
            // InternalRuleEngineParser.g:2870:2: iv_ruleAVar= ruleAVar EOF
            {
             newCompositeNode(grammarAccess.getAVarRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAVar=ruleAVar();

            state._fsp--;

             current =iv_ruleAVar; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAVar"


    // $ANTLR start "ruleAVar"
    // InternalRuleEngineParser.g:2877:1: ruleAVar returns [EObject current=null] : (this_IntVar_0= ruleIntVar | this_BoolVar_1= ruleBoolVar | this_TimerVar_2= ruleTimerVar | this_ResourceVar_3= ruleResourceVar | this_CustomVar_4= ruleCustomVar ) ;
    public final EObject ruleAVar() throws RecognitionException {
        EObject current = null;

        EObject this_IntVar_0 = null;

        EObject this_BoolVar_1 = null;

        EObject this_TimerVar_2 = null;

        EObject this_ResourceVar_3 = null;

        EObject this_CustomVar_4 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2880:28: ( (this_IntVar_0= ruleIntVar | this_BoolVar_1= ruleBoolVar | this_TimerVar_2= ruleTimerVar | this_ResourceVar_3= ruleResourceVar | this_CustomVar_4= ruleCustomVar ) )
            // InternalRuleEngineParser.g:2881:1: (this_IntVar_0= ruleIntVar | this_BoolVar_1= ruleBoolVar | this_TimerVar_2= ruleTimerVar | this_ResourceVar_3= ruleResourceVar | this_CustomVar_4= ruleCustomVar )
            {
            // InternalRuleEngineParser.g:2881:1: (this_IntVar_0= ruleIntVar | this_BoolVar_1= ruleBoolVar | this_TimerVar_2= ruleTimerVar | this_ResourceVar_3= ruleResourceVar | this_CustomVar_4= ruleCustomVar )
            int alt33=5;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==RULE_IDENTIFIER) ) {
                int LA33_1 = input.LA(2);

                if ( (LA33_1==Colon) ) {
                    switch ( input.LA(3) ) {
                    case BOOL:
                    case BYTE:
                    case CHAR:
                    case DATE:
                    case DINT:
                    case LINT:
                    case SINT:
                    case TIME:
                    case WORD:
                    case INT:
                        {
                        alt33=5;
                        }
                        break;
                    case TON:
                        {
                        alt33=3;
                        }
                        break;
                    case BOOL_1:
                        {
                        alt33=2;
                        }
                        break;
                    case INT_1:
                        {
                        alt33=1;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 33, 2, input);

                        throw nvae;
                    }

                }
                else if ( (LA33_1==AT) ) {
                    alt33=4;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 33, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }
            switch (alt33) {
                case 1 :
                    // InternalRuleEngineParser.g:2882:5: this_IntVar_0= ruleIntVar
                    {
                     
                            newCompositeNode(grammarAccess.getAVarAccess().getIntVarParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_IntVar_0=ruleIntVar();

                    state._fsp--;


                            current = this_IntVar_0;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:2892:5: this_BoolVar_1= ruleBoolVar
                    {
                     
                            newCompositeNode(grammarAccess.getAVarAccess().getBoolVarParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_BoolVar_1=ruleBoolVar();

                    state._fsp--;


                            current = this_BoolVar_1;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalRuleEngineParser.g:2902:5: this_TimerVar_2= ruleTimerVar
                    {
                     
                            newCompositeNode(grammarAccess.getAVarAccess().getTimerVarParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_TimerVar_2=ruleTimerVar();

                    state._fsp--;


                            current = this_TimerVar_2;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalRuleEngineParser.g:2912:5: this_ResourceVar_3= ruleResourceVar
                    {
                     
                            newCompositeNode(grammarAccess.getAVarAccess().getResourceVarParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_2);
                    this_ResourceVar_3=ruleResourceVar();

                    state._fsp--;


                            current = this_ResourceVar_3;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // InternalRuleEngineParser.g:2922:5: this_CustomVar_4= ruleCustomVar
                    {
                     
                            newCompositeNode(grammarAccess.getAVarAccess().getCustomVarParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_2);
                    this_CustomVar_4=ruleCustomVar();

                    state._fsp--;


                            current = this_CustomVar_4;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAVar"


    // $ANTLR start "entryRuleResourceVar"
    // InternalRuleEngineParser.g:2938:1: entryRuleResourceVar returns [EObject current=null] : iv_ruleResourceVar= ruleResourceVar EOF ;
    public final EObject entryRuleResourceVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleResourceVar = null;


        try {
            // InternalRuleEngineParser.g:2939:2: (iv_ruleResourceVar= ruleResourceVar EOF )
            // InternalRuleEngineParser.g:2940:2: iv_ruleResourceVar= ruleResourceVar EOF
            {
             newCompositeNode(grammarAccess.getResourceVarRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleResourceVar=ruleResourceVar();

            state._fsp--;

             current =iv_ruleResourceVar; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleResourceVar"


    // $ANTLR start "ruleResourceVar"
    // InternalRuleEngineParser.g:2947:1: ruleResourceVar returns [EObject current=null] : ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= AT ( (lv_resource_2_0= ruleResource ) ) otherlv_3= Colon ( (lv_type_4_0= ruleType ) ) otherlv_5= Semicolon ) ;
    public final EObject ruleResourceVar() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_resource_2_0 = null;

        EObject lv_type_4_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:2950:28: ( ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= AT ( (lv_resource_2_0= ruleResource ) ) otherlv_3= Colon ( (lv_type_4_0= ruleType ) ) otherlv_5= Semicolon ) )
            // InternalRuleEngineParser.g:2951:1: ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= AT ( (lv_resource_2_0= ruleResource ) ) otherlv_3= Colon ( (lv_type_4_0= ruleType ) ) otherlv_5= Semicolon )
            {
            // InternalRuleEngineParser.g:2951:1: ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= AT ( (lv_resource_2_0= ruleResource ) ) otherlv_3= Colon ( (lv_type_4_0= ruleType ) ) otherlv_5= Semicolon )
            // InternalRuleEngineParser.g:2951:2: ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= AT ( (lv_resource_2_0= ruleResource ) ) otherlv_3= Colon ( (lv_type_4_0= ruleType ) ) otherlv_5= Semicolon
            {
            // InternalRuleEngineParser.g:2951:2: ( (lv_name_0_0= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:2952:1: (lv_name_0_0= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:2952:1: (lv_name_0_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:2953:3: lv_name_0_0= RULE_IDENTIFIER
            {
            lv_name_0_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_28); 

            			newLeafNode(lv_name_0_0, grammarAccess.getResourceVarAccess().getNameIDENTIFIERTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getResourceVarRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
            	    

            }


            }

            otherlv_1=(Token)match(input,AT,FOLLOW_29); 

                	newLeafNode(otherlv_1, grammarAccess.getResourceVarAccess().getATKeyword_1());
                
            // InternalRuleEngineParser.g:2974:1: ( (lv_resource_2_0= ruleResource ) )
            // InternalRuleEngineParser.g:2975:1: (lv_resource_2_0= ruleResource )
            {
            // InternalRuleEngineParser.g:2975:1: (lv_resource_2_0= ruleResource )
            // InternalRuleEngineParser.g:2976:3: lv_resource_2_0= ruleResource
            {
             
            	        newCompositeNode(grammarAccess.getResourceVarAccess().getResourceResourceParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_15);
            lv_resource_2_0=ruleResource();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getResourceVarRule());
            	        }
                   		set(
                   			current, 
                   			"resource",
                    		lv_resource_2_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Resource");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,Colon,FOLLOW_30); 

                	newLeafNode(otherlv_3, grammarAccess.getResourceVarAccess().getColonKeyword_3());
                
            // InternalRuleEngineParser.g:2997:1: ( (lv_type_4_0= ruleType ) )
            // InternalRuleEngineParser.g:2998:1: (lv_type_4_0= ruleType )
            {
            // InternalRuleEngineParser.g:2998:1: (lv_type_4_0= ruleType )
            // InternalRuleEngineParser.g:2999:3: lv_type_4_0= ruleType
            {
             
            	        newCompositeNode(grammarAccess.getResourceVarAccess().getTypeTypeParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_31);
            lv_type_4_0=ruleType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getResourceVarRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_4_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Type");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_5=(Token)match(input,Semicolon,FOLLOW_2); 

                	newLeafNode(otherlv_5, grammarAccess.getResourceVarAccess().getSemicolonKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResourceVar"


    // $ANTLR start "entryRuleIntResourceType"
    // InternalRuleEngineParser.g:3030:1: entryRuleIntResourceType returns [EObject current=null] : iv_ruleIntResourceType= ruleIntResourceType EOF ;
    public final EObject entryRuleIntResourceType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntResourceType = null;


        try {
            // InternalRuleEngineParser.g:3031:2: (iv_ruleIntResourceType= ruleIntResourceType EOF )
            // InternalRuleEngineParser.g:3032:2: iv_ruleIntResourceType= ruleIntResourceType EOF
            {
             newCompositeNode(grammarAccess.getIntResourceTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntResourceType=ruleIntResourceType();

            state._fsp--;

             current =iv_ruleIntResourceType; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntResourceType"


    // $ANTLR start "ruleIntResourceType"
    // InternalRuleEngineParser.g:3039:1: ruleIntResourceType returns [EObject current=null] : (otherlv_0= INT_1 ( (lv_value_1_0= RULE_INT ) ) ) ;
    public final EObject ruleIntResourceType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_value_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:3042:28: ( (otherlv_0= INT_1 ( (lv_value_1_0= RULE_INT ) ) ) )
            // InternalRuleEngineParser.g:3043:1: (otherlv_0= INT_1 ( (lv_value_1_0= RULE_INT ) ) )
            {
            // InternalRuleEngineParser.g:3043:1: (otherlv_0= INT_1 ( (lv_value_1_0= RULE_INT ) ) )
            // InternalRuleEngineParser.g:3044:2: otherlv_0= INT_1 ( (lv_value_1_0= RULE_INT ) )
            {
            otherlv_0=(Token)match(input,INT_1,FOLLOW_32); 

                	newLeafNode(otherlv_0, grammarAccess.getIntResourceTypeAccess().getINTKeyword_0());
                
            // InternalRuleEngineParser.g:3048:1: ( (lv_value_1_0= RULE_INT ) )
            // InternalRuleEngineParser.g:3049:1: (lv_value_1_0= RULE_INT )
            {
            // InternalRuleEngineParser.g:3049:1: (lv_value_1_0= RULE_INT )
            // InternalRuleEngineParser.g:3050:3: lv_value_1_0= RULE_INT
            {
            lv_value_1_0=(Token)match(input,RULE_INT,FOLLOW_2); 

            			newLeafNode(lv_value_1_0, grammarAccess.getIntResourceTypeAccess().getValueINTTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getIntResourceTypeRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.INT");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntResourceType"


    // $ANTLR start "entryRuleBooleanResourceType"
    // InternalRuleEngineParser.g:3074:1: entryRuleBooleanResourceType returns [EObject current=null] : iv_ruleBooleanResourceType= ruleBooleanResourceType EOF ;
    public final EObject entryRuleBooleanResourceType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanResourceType = null;


        try {
            // InternalRuleEngineParser.g:3075:2: (iv_ruleBooleanResourceType= ruleBooleanResourceType EOF )
            // InternalRuleEngineParser.g:3076:2: iv_ruleBooleanResourceType= ruleBooleanResourceType EOF
            {
             newCompositeNode(grammarAccess.getBooleanResourceTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBooleanResourceType=ruleBooleanResourceType();

            state._fsp--;

             current =iv_ruleBooleanResourceType; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanResourceType"


    // $ANTLR start "ruleBooleanResourceType"
    // InternalRuleEngineParser.g:3083:1: ruleBooleanResourceType returns [EObject current=null] : (otherlv_0= BOOL_1 ( (lv_value_1_0= ruleBooleanRule ) ) ) ;
    public final EObject ruleBooleanResourceType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_value_1_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:3086:28: ( (otherlv_0= BOOL_1 ( (lv_value_1_0= ruleBooleanRule ) ) ) )
            // InternalRuleEngineParser.g:3087:1: (otherlv_0= BOOL_1 ( (lv_value_1_0= ruleBooleanRule ) ) )
            {
            // InternalRuleEngineParser.g:3087:1: (otherlv_0= BOOL_1 ( (lv_value_1_0= ruleBooleanRule ) ) )
            // InternalRuleEngineParser.g:3088:2: otherlv_0= BOOL_1 ( (lv_value_1_0= ruleBooleanRule ) )
            {
            otherlv_0=(Token)match(input,BOOL_1,FOLLOW_33); 

                	newLeafNode(otherlv_0, grammarAccess.getBooleanResourceTypeAccess().getBOOLKeyword_0());
                
            // InternalRuleEngineParser.g:3092:1: ( (lv_value_1_0= ruleBooleanRule ) )
            // InternalRuleEngineParser.g:3093:1: (lv_value_1_0= ruleBooleanRule )
            {
            // InternalRuleEngineParser.g:3093:1: (lv_value_1_0= ruleBooleanRule )
            // InternalRuleEngineParser.g:3094:3: lv_value_1_0= ruleBooleanRule
            {
             
            	        newCompositeNode(grammarAccess.getBooleanResourceTypeAccess().getValueBooleanRuleParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_value_1_0=ruleBooleanRule();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanResourceTypeRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.BooleanRule");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanResourceType"


    // $ANTLR start "entryRuleCustomVar"
    // InternalRuleEngineParser.g:3118:1: entryRuleCustomVar returns [EObject current=null] : iv_ruleCustomVar= ruleCustomVar EOF ;
    public final EObject entryRuleCustomVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCustomVar = null;


        try {
            // InternalRuleEngineParser.g:3119:2: (iv_ruleCustomVar= ruleCustomVar EOF )
            // InternalRuleEngineParser.g:3120:2: iv_ruleCustomVar= ruleCustomVar EOF
            {
             newCompositeNode(grammarAccess.getCustomVarRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCustomVar=ruleCustomVar();

            state._fsp--;

             current =iv_ruleCustomVar; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCustomVar"


    // $ANTLR start "ruleCustomVar"
    // InternalRuleEngineParser.g:3127:1: ruleCustomVar returns [EObject current=null] : ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_type_2_0= ruleType ) ) (otherlv_3= ColonEqualsSign ( (lv_initializer_4_0= ruleInitData ) ) )? otherlv_5= Semicolon ) ;
    public final EObject ruleCustomVar() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_type_2_0 = null;

        EObject lv_initializer_4_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:3130:28: ( ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_type_2_0= ruleType ) ) (otherlv_3= ColonEqualsSign ( (lv_initializer_4_0= ruleInitData ) ) )? otherlv_5= Semicolon ) )
            // InternalRuleEngineParser.g:3131:1: ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_type_2_0= ruleType ) ) (otherlv_3= ColonEqualsSign ( (lv_initializer_4_0= ruleInitData ) ) )? otherlv_5= Semicolon )
            {
            // InternalRuleEngineParser.g:3131:1: ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_type_2_0= ruleType ) ) (otherlv_3= ColonEqualsSign ( (lv_initializer_4_0= ruleInitData ) ) )? otherlv_5= Semicolon )
            // InternalRuleEngineParser.g:3131:2: ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_type_2_0= ruleType ) ) (otherlv_3= ColonEqualsSign ( (lv_initializer_4_0= ruleInitData ) ) )? otherlv_5= Semicolon
            {
            // InternalRuleEngineParser.g:3131:2: ( (lv_name_0_0= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:3132:1: (lv_name_0_0= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:3132:1: (lv_name_0_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:3133:3: lv_name_0_0= RULE_IDENTIFIER
            {
            lv_name_0_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_15); 

            			newLeafNode(lv_name_0_0, grammarAccess.getCustomVarAccess().getNameIDENTIFIERTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getCustomVarRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
            	    

            }


            }

            otherlv_1=(Token)match(input,Colon,FOLLOW_30); 

                	newLeafNode(otherlv_1, grammarAccess.getCustomVarAccess().getColonKeyword_1());
                
            // InternalRuleEngineParser.g:3154:1: ( (lv_type_2_0= ruleType ) )
            // InternalRuleEngineParser.g:3155:1: (lv_type_2_0= ruleType )
            {
            // InternalRuleEngineParser.g:3155:1: (lv_type_2_0= ruleType )
            // InternalRuleEngineParser.g:3156:3: lv_type_2_0= ruleType
            {
             
            	        newCompositeNode(grammarAccess.getCustomVarAccess().getTypeTypeParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_34);
            lv_type_2_0=ruleType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCustomVarRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_2_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.Type");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalRuleEngineParser.g:3172:2: (otherlv_3= ColonEqualsSign ( (lv_initializer_4_0= ruleInitData ) ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==ColonEqualsSign) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalRuleEngineParser.g:3173:2: otherlv_3= ColonEqualsSign ( (lv_initializer_4_0= ruleInitData ) )
                    {
                    otherlv_3=(Token)match(input,ColonEqualsSign,FOLLOW_35); 

                        	newLeafNode(otherlv_3, grammarAccess.getCustomVarAccess().getColonEqualsSignKeyword_3_0());
                        
                    // InternalRuleEngineParser.g:3177:1: ( (lv_initializer_4_0= ruleInitData ) )
                    // InternalRuleEngineParser.g:3178:1: (lv_initializer_4_0= ruleInitData )
                    {
                    // InternalRuleEngineParser.g:3178:1: (lv_initializer_4_0= ruleInitData )
                    // InternalRuleEngineParser.g:3179:3: lv_initializer_4_0= ruleInitData
                    {
                     
                    	        newCompositeNode(grammarAccess.getCustomVarAccess().getInitializerInitDataParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FOLLOW_31);
                    lv_initializer_4_0=ruleInitData();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCustomVarRule());
                    	        }
                           		set(
                           			current, 
                           			"initializer",
                            		lv_initializer_4_0, 
                            		"com.lucy.g3.iec61131.xtext.il.RuleEngine.InitData");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,Semicolon,FOLLOW_2); 

                	newLeafNode(otherlv_5, grammarAccess.getCustomVarAccess().getSemicolonKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCustomVar"


    // $ANTLR start "entryRuleIntVar"
    // InternalRuleEngineParser.g:3208:1: entryRuleIntVar returns [EObject current=null] : iv_ruleIntVar= ruleIntVar EOF ;
    public final EObject entryRuleIntVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntVar = null;


        try {
            // InternalRuleEngineParser.g:3209:2: (iv_ruleIntVar= ruleIntVar EOF )
            // InternalRuleEngineParser.g:3210:2: iv_ruleIntVar= ruleIntVar EOF
            {
             newCompositeNode(grammarAccess.getIntVarRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntVar=ruleIntVar();

            state._fsp--;

             current =iv_ruleIntVar; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntVar"


    // $ANTLR start "ruleIntVar"
    // InternalRuleEngineParser.g:3217:1: ruleIntVar returns [EObject current=null] : ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_intType_2_0= ruleIntResourceType ) ) otherlv_3= Semicolon ) ;
    public final EObject ruleIntVar() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_intType_2_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:3220:28: ( ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_intType_2_0= ruleIntResourceType ) ) otherlv_3= Semicolon ) )
            // InternalRuleEngineParser.g:3221:1: ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_intType_2_0= ruleIntResourceType ) ) otherlv_3= Semicolon )
            {
            // InternalRuleEngineParser.g:3221:1: ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_intType_2_0= ruleIntResourceType ) ) otherlv_3= Semicolon )
            // InternalRuleEngineParser.g:3221:2: ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_intType_2_0= ruleIntResourceType ) ) otherlv_3= Semicolon
            {
            // InternalRuleEngineParser.g:3221:2: ( (lv_name_0_0= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:3222:1: (lv_name_0_0= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:3222:1: (lv_name_0_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:3223:3: lv_name_0_0= RULE_IDENTIFIER
            {
            lv_name_0_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_15); 

            			newLeafNode(lv_name_0_0, grammarAccess.getIntVarAccess().getNameIDENTIFIERTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getIntVarRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
            	    

            }


            }

            otherlv_1=(Token)match(input,Colon,FOLLOW_36); 

                	newLeafNode(otherlv_1, grammarAccess.getIntVarAccess().getColonKeyword_1());
                
            // InternalRuleEngineParser.g:3244:1: ( (lv_intType_2_0= ruleIntResourceType ) )
            // InternalRuleEngineParser.g:3245:1: (lv_intType_2_0= ruleIntResourceType )
            {
            // InternalRuleEngineParser.g:3245:1: (lv_intType_2_0= ruleIntResourceType )
            // InternalRuleEngineParser.g:3246:3: lv_intType_2_0= ruleIntResourceType
            {
             
            	        newCompositeNode(grammarAccess.getIntVarAccess().getIntTypeIntResourceTypeParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_31);
            lv_intType_2_0=ruleIntResourceType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntVarRule());
            	        }
                   		set(
                   			current, 
                   			"intType",
                    		lv_intType_2_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IntResourceType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,Semicolon,FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getIntVarAccess().getSemicolonKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntVar"


    // $ANTLR start "entryRuleBoolVar"
    // InternalRuleEngineParser.g:3275:1: entryRuleBoolVar returns [EObject current=null] : iv_ruleBoolVar= ruleBoolVar EOF ;
    public final EObject entryRuleBoolVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBoolVar = null;


        try {
            // InternalRuleEngineParser.g:3276:2: (iv_ruleBoolVar= ruleBoolVar EOF )
            // InternalRuleEngineParser.g:3277:2: iv_ruleBoolVar= ruleBoolVar EOF
            {
             newCompositeNode(grammarAccess.getBoolVarRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBoolVar=ruleBoolVar();

            state._fsp--;

             current =iv_ruleBoolVar; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolVar"


    // $ANTLR start "ruleBoolVar"
    // InternalRuleEngineParser.g:3284:1: ruleBoolVar returns [EObject current=null] : ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_boolType_2_0= ruleBooleanResourceType ) ) otherlv_3= Semicolon ) ;
    public final EObject ruleBoolVar() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_boolType_2_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:3287:28: ( ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_boolType_2_0= ruleBooleanResourceType ) ) otherlv_3= Semicolon ) )
            // InternalRuleEngineParser.g:3288:1: ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_boolType_2_0= ruleBooleanResourceType ) ) otherlv_3= Semicolon )
            {
            // InternalRuleEngineParser.g:3288:1: ( ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_boolType_2_0= ruleBooleanResourceType ) ) otherlv_3= Semicolon )
            // InternalRuleEngineParser.g:3288:2: ( (lv_name_0_0= RULE_IDENTIFIER ) ) otherlv_1= Colon ( (lv_boolType_2_0= ruleBooleanResourceType ) ) otherlv_3= Semicolon
            {
            // InternalRuleEngineParser.g:3288:2: ( (lv_name_0_0= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:3289:1: (lv_name_0_0= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:3289:1: (lv_name_0_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:3290:3: lv_name_0_0= RULE_IDENTIFIER
            {
            lv_name_0_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_15); 

            			newLeafNode(lv_name_0_0, grammarAccess.getBoolVarAccess().getNameIDENTIFIERTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getBoolVarRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
            	    

            }


            }

            otherlv_1=(Token)match(input,Colon,FOLLOW_37); 

                	newLeafNode(otherlv_1, grammarAccess.getBoolVarAccess().getColonKeyword_1());
                
            // InternalRuleEngineParser.g:3311:1: ( (lv_boolType_2_0= ruleBooleanResourceType ) )
            // InternalRuleEngineParser.g:3312:1: (lv_boolType_2_0= ruleBooleanResourceType )
            {
            // InternalRuleEngineParser.g:3312:1: (lv_boolType_2_0= ruleBooleanResourceType )
            // InternalRuleEngineParser.g:3313:3: lv_boolType_2_0= ruleBooleanResourceType
            {
             
            	        newCompositeNode(grammarAccess.getBoolVarAccess().getBoolTypeBooleanResourceTypeParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_31);
            lv_boolType_2_0=ruleBooleanResourceType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBoolVarRule());
            	        }
                   		set(
                   			current, 
                   			"boolType",
                    		lv_boolType_2_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.BooleanResourceType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,Semicolon,FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getBoolVarAccess().getSemicolonKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolVar"


    // $ANTLR start "entryRuleTimerIdentifier"
    // InternalRuleEngineParser.g:3342:1: entryRuleTimerIdentifier returns [String current=null] : iv_ruleTimerIdentifier= ruleTimerIdentifier EOF ;
    public final String entryRuleTimerIdentifier() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleTimerIdentifier = null;


        try {
            // InternalRuleEngineParser.g:3343:1: (iv_ruleTimerIdentifier= ruleTimerIdentifier EOF )
            // InternalRuleEngineParser.g:3344:2: iv_ruleTimerIdentifier= ruleTimerIdentifier EOF
            {
             newCompositeNode(grammarAccess.getTimerIdentifierRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTimerIdentifier=ruleTimerIdentifier();

            state._fsp--;

             current =iv_ruleTimerIdentifier.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimerIdentifier"


    // $ANTLR start "ruleTimerIdentifier"
    // InternalRuleEngineParser.g:3351:1: ruleTimerIdentifier returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_IDENTIFIER_0= RULE_IDENTIFIER ;
    public final AntlrDatatypeRuleToken ruleTimerIdentifier() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_IDENTIFIER_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:3355:6: (this_IDENTIFIER_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:3356:5: this_IDENTIFIER_0= RULE_IDENTIFIER
            {
            this_IDENTIFIER_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_2); 

            		current.merge(this_IDENTIFIER_0);
                
             
                newLeafNode(this_IDENTIFIER_0, grammarAccess.getTimerIdentifierAccess().getIDENTIFIERTerminalRuleCall()); 
                

            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimerIdentifier"


    // $ANTLR start "entryRuleTimerVar"
    // InternalRuleEngineParser.g:3371:1: entryRuleTimerVar returns [EObject current=null] : iv_ruleTimerVar= ruleTimerVar EOF ;
    public final EObject entryRuleTimerVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimerVar = null;


        try {
            // InternalRuleEngineParser.g:3372:2: (iv_ruleTimerVar= ruleTimerVar EOF )
            // InternalRuleEngineParser.g:3373:2: iv_ruleTimerVar= ruleTimerVar EOF
            {
             newCompositeNode(grammarAccess.getTimerVarRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTimerVar=ruleTimerVar();

            state._fsp--;

             current =iv_ruleTimerVar; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimerVar"


    // $ANTLR start "ruleTimerVar"
    // InternalRuleEngineParser.g:3380:1: ruleTimerVar returns [EObject current=null] : ( ( (lv_name_0_0= ruleTimerIdentifier ) ) otherlv_1= Colon ( (lv_timer_2_0= TON ) ) otherlv_3= Semicolon ) ;
    public final EObject ruleTimerVar() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_timer_2_0=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:3383:28: ( ( ( (lv_name_0_0= ruleTimerIdentifier ) ) otherlv_1= Colon ( (lv_timer_2_0= TON ) ) otherlv_3= Semicolon ) )
            // InternalRuleEngineParser.g:3384:1: ( ( (lv_name_0_0= ruleTimerIdentifier ) ) otherlv_1= Colon ( (lv_timer_2_0= TON ) ) otherlv_3= Semicolon )
            {
            // InternalRuleEngineParser.g:3384:1: ( ( (lv_name_0_0= ruleTimerIdentifier ) ) otherlv_1= Colon ( (lv_timer_2_0= TON ) ) otherlv_3= Semicolon )
            // InternalRuleEngineParser.g:3384:2: ( (lv_name_0_0= ruleTimerIdentifier ) ) otherlv_1= Colon ( (lv_timer_2_0= TON ) ) otherlv_3= Semicolon
            {
            // InternalRuleEngineParser.g:3384:2: ( (lv_name_0_0= ruleTimerIdentifier ) )
            // InternalRuleEngineParser.g:3385:1: (lv_name_0_0= ruleTimerIdentifier )
            {
            // InternalRuleEngineParser.g:3385:1: (lv_name_0_0= ruleTimerIdentifier )
            // InternalRuleEngineParser.g:3386:3: lv_name_0_0= ruleTimerIdentifier
            {
             
            	        newCompositeNode(grammarAccess.getTimerVarAccess().getNameTimerIdentifierParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_15);
            lv_name_0_0=ruleTimerIdentifier();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTimerVarRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.TimerIdentifier");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,Colon,FOLLOW_38); 

                	newLeafNode(otherlv_1, grammarAccess.getTimerVarAccess().getColonKeyword_1());
                
            // InternalRuleEngineParser.g:3407:1: ( (lv_timer_2_0= TON ) )
            // InternalRuleEngineParser.g:3408:1: (lv_timer_2_0= TON )
            {
            // InternalRuleEngineParser.g:3408:1: (lv_timer_2_0= TON )
            // InternalRuleEngineParser.g:3409:3: lv_timer_2_0= TON
            {
            lv_timer_2_0=(Token)match(input,TON,FOLLOW_31); 

                    newLeafNode(lv_timer_2_0, grammarAccess.getTimerVarAccess().getTimerTONKeyword_2_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTimerVarRule());
            	        }
                   		setWithLastConsumed(current, "timer", lv_timer_2_0, "TON");
            	    

            }


            }

            otherlv_3=(Token)match(input,Semicolon,FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getTimerVarAccess().getSemicolonKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimerVar"


    // $ANTLR start "entryRuleTimerCallCommand"
    // InternalRuleEngineParser.g:3436:1: entryRuleTimerCallCommand returns [EObject current=null] : iv_ruleTimerCallCommand= ruleTimerCallCommand EOF ;
    public final EObject entryRuleTimerCallCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimerCallCommand = null;


        try {
            // InternalRuleEngineParser.g:3437:2: (iv_ruleTimerCallCommand= ruleTimerCallCommand EOF )
            // InternalRuleEngineParser.g:3438:2: iv_ruleTimerCallCommand= ruleTimerCallCommand EOF
            {
             newCompositeNode(grammarAccess.getTimerCallCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTimerCallCommand=ruleTimerCallCommand();

            state._fsp--;

             current =iv_ruleTimerCallCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimerCallCommand"


    // $ANTLR start "ruleTimerCallCommand"
    // InternalRuleEngineParser.g:3445:1: ruleTimerCallCommand returns [EObject current=null] : ( ( (lv_name_0_0= ruleTimerIdentifier ) ) otherlv_1= LeftParenthesis otherlv_2= IN otherlv_3= ColonEqualsSign ( (lv_in_4_0= RULE_IDENTIFIER ) ) otherlv_5= Comma otherlv_6= PT otherlv_7= ColonEqualsSign ( (lv_pt_8_0= RULE_IDENTIFIER ) ) otherlv_9= Comma otherlv_10= Q otherlv_11= EqualsSignGreaterThanSign ( (lv_q_12_0= RULE_IDENTIFIER ) ) otherlv_13= Comma otherlv_14= ET otherlv_15= EqualsSignGreaterThanSign ( (lv_et_16_0= RULE_IDENTIFIER ) ) otherlv_17= RightParenthesis ) ;
    public final EObject ruleTimerCallCommand() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_in_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token lv_pt_8_0=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token lv_q_12_0=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token lv_et_16_0=null;
        Token otherlv_17=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:3448:28: ( ( ( (lv_name_0_0= ruleTimerIdentifier ) ) otherlv_1= LeftParenthesis otherlv_2= IN otherlv_3= ColonEqualsSign ( (lv_in_4_0= RULE_IDENTIFIER ) ) otherlv_5= Comma otherlv_6= PT otherlv_7= ColonEqualsSign ( (lv_pt_8_0= RULE_IDENTIFIER ) ) otherlv_9= Comma otherlv_10= Q otherlv_11= EqualsSignGreaterThanSign ( (lv_q_12_0= RULE_IDENTIFIER ) ) otherlv_13= Comma otherlv_14= ET otherlv_15= EqualsSignGreaterThanSign ( (lv_et_16_0= RULE_IDENTIFIER ) ) otherlv_17= RightParenthesis ) )
            // InternalRuleEngineParser.g:3449:1: ( ( (lv_name_0_0= ruleTimerIdentifier ) ) otherlv_1= LeftParenthesis otherlv_2= IN otherlv_3= ColonEqualsSign ( (lv_in_4_0= RULE_IDENTIFIER ) ) otherlv_5= Comma otherlv_6= PT otherlv_7= ColonEqualsSign ( (lv_pt_8_0= RULE_IDENTIFIER ) ) otherlv_9= Comma otherlv_10= Q otherlv_11= EqualsSignGreaterThanSign ( (lv_q_12_0= RULE_IDENTIFIER ) ) otherlv_13= Comma otherlv_14= ET otherlv_15= EqualsSignGreaterThanSign ( (lv_et_16_0= RULE_IDENTIFIER ) ) otherlv_17= RightParenthesis )
            {
            // InternalRuleEngineParser.g:3449:1: ( ( (lv_name_0_0= ruleTimerIdentifier ) ) otherlv_1= LeftParenthesis otherlv_2= IN otherlv_3= ColonEqualsSign ( (lv_in_4_0= RULE_IDENTIFIER ) ) otherlv_5= Comma otherlv_6= PT otherlv_7= ColonEqualsSign ( (lv_pt_8_0= RULE_IDENTIFIER ) ) otherlv_9= Comma otherlv_10= Q otherlv_11= EqualsSignGreaterThanSign ( (lv_q_12_0= RULE_IDENTIFIER ) ) otherlv_13= Comma otherlv_14= ET otherlv_15= EqualsSignGreaterThanSign ( (lv_et_16_0= RULE_IDENTIFIER ) ) otherlv_17= RightParenthesis )
            // InternalRuleEngineParser.g:3449:2: ( (lv_name_0_0= ruleTimerIdentifier ) ) otherlv_1= LeftParenthesis otherlv_2= IN otherlv_3= ColonEqualsSign ( (lv_in_4_0= RULE_IDENTIFIER ) ) otherlv_5= Comma otherlv_6= PT otherlv_7= ColonEqualsSign ( (lv_pt_8_0= RULE_IDENTIFIER ) ) otherlv_9= Comma otherlv_10= Q otherlv_11= EqualsSignGreaterThanSign ( (lv_q_12_0= RULE_IDENTIFIER ) ) otherlv_13= Comma otherlv_14= ET otherlv_15= EqualsSignGreaterThanSign ( (lv_et_16_0= RULE_IDENTIFIER ) ) otherlv_17= RightParenthesis
            {
            // InternalRuleEngineParser.g:3449:2: ( (lv_name_0_0= ruleTimerIdentifier ) )
            // InternalRuleEngineParser.g:3450:1: (lv_name_0_0= ruleTimerIdentifier )
            {
            // InternalRuleEngineParser.g:3450:1: (lv_name_0_0= ruleTimerIdentifier )
            // InternalRuleEngineParser.g:3451:3: lv_name_0_0= ruleTimerIdentifier
            {
             
            	        newCompositeNode(grammarAccess.getTimerCallCommandAccess().getNameTimerIdentifierParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_39);
            lv_name_0_0=ruleTimerIdentifier();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTimerCallCommandRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.TimerIdentifier");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,LeftParenthesis,FOLLOW_40); 

                	newLeafNode(otherlv_1, grammarAccess.getTimerCallCommandAccess().getLeftParenthesisKeyword_1());
                
            otherlv_2=(Token)match(input,IN,FOLLOW_41); 

                	newLeafNode(otherlv_2, grammarAccess.getTimerCallCommandAccess().getINKeyword_2());
                
            otherlv_3=(Token)match(input,ColonEqualsSign,FOLLOW_5); 

                	newLeafNode(otherlv_3, grammarAccess.getTimerCallCommandAccess().getColonEqualsSignKeyword_3());
                
            // InternalRuleEngineParser.g:3482:1: ( (lv_in_4_0= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:3483:1: (lv_in_4_0= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:3483:1: (lv_in_4_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:3484:3: lv_in_4_0= RULE_IDENTIFIER
            {
            lv_in_4_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_42); 

            			newLeafNode(lv_in_4_0, grammarAccess.getTimerCallCommandAccess().getInIDENTIFIERTerminalRuleCall_4_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTimerCallCommandRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"in",
                    		lv_in_4_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
            	    

            }


            }

            otherlv_5=(Token)match(input,Comma,FOLLOW_43); 

                	newLeafNode(otherlv_5, grammarAccess.getTimerCallCommandAccess().getCommaKeyword_5());
                
            otherlv_6=(Token)match(input,PT,FOLLOW_41); 

                	newLeafNode(otherlv_6, grammarAccess.getTimerCallCommandAccess().getPTKeyword_6());
                
            otherlv_7=(Token)match(input,ColonEqualsSign,FOLLOW_5); 

                	newLeafNode(otherlv_7, grammarAccess.getTimerCallCommandAccess().getColonEqualsSignKeyword_7());
                
            // InternalRuleEngineParser.g:3515:1: ( (lv_pt_8_0= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:3516:1: (lv_pt_8_0= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:3516:1: (lv_pt_8_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:3517:3: lv_pt_8_0= RULE_IDENTIFIER
            {
            lv_pt_8_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_42); 

            			newLeafNode(lv_pt_8_0, grammarAccess.getTimerCallCommandAccess().getPtIDENTIFIERTerminalRuleCall_8_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTimerCallCommandRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"pt",
                    		lv_pt_8_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
            	    

            }


            }

            otherlv_9=(Token)match(input,Comma,FOLLOW_44); 

                	newLeafNode(otherlv_9, grammarAccess.getTimerCallCommandAccess().getCommaKeyword_9());
                
            otherlv_10=(Token)match(input,Q,FOLLOW_45); 

                	newLeafNode(otherlv_10, grammarAccess.getTimerCallCommandAccess().getQKeyword_10());
                
            otherlv_11=(Token)match(input,EqualsSignGreaterThanSign,FOLLOW_5); 

                	newLeafNode(otherlv_11, grammarAccess.getTimerCallCommandAccess().getEqualsSignGreaterThanSignKeyword_11());
                
            // InternalRuleEngineParser.g:3548:1: ( (lv_q_12_0= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:3549:1: (lv_q_12_0= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:3549:1: (lv_q_12_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:3550:3: lv_q_12_0= RULE_IDENTIFIER
            {
            lv_q_12_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_42); 

            			newLeafNode(lv_q_12_0, grammarAccess.getTimerCallCommandAccess().getQIDENTIFIERTerminalRuleCall_12_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTimerCallCommandRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"q",
                    		lv_q_12_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
            	    

            }


            }

            otherlv_13=(Token)match(input,Comma,FOLLOW_46); 

                	newLeafNode(otherlv_13, grammarAccess.getTimerCallCommandAccess().getCommaKeyword_13());
                
            otherlv_14=(Token)match(input,ET,FOLLOW_45); 

                	newLeafNode(otherlv_14, grammarAccess.getTimerCallCommandAccess().getETKeyword_14());
                
            otherlv_15=(Token)match(input,EqualsSignGreaterThanSign,FOLLOW_5); 

                	newLeafNode(otherlv_15, grammarAccess.getTimerCallCommandAccess().getEqualsSignGreaterThanSignKeyword_15());
                
            // InternalRuleEngineParser.g:3581:1: ( (lv_et_16_0= RULE_IDENTIFIER ) )
            // InternalRuleEngineParser.g:3582:1: (lv_et_16_0= RULE_IDENTIFIER )
            {
            // InternalRuleEngineParser.g:3582:1: (lv_et_16_0= RULE_IDENTIFIER )
            // InternalRuleEngineParser.g:3583:3: lv_et_16_0= RULE_IDENTIFIER
            {
            lv_et_16_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_47); 

            			newLeafNode(lv_et_16_0, grammarAccess.getTimerCallCommandAccess().getEtIDENTIFIERTerminalRuleCall_16_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTimerCallCommandRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"et",
                    		lv_et_16_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.IDENTIFIER");
            	    

            }


            }

            otherlv_17=(Token)match(input,RightParenthesis,FOLLOW_2); 

                	newLeafNode(otherlv_17, grammarAccess.getTimerCallCommandAccess().getRightParenthesisKeyword_17());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimerCallCommand"


    // $ANTLR start "entryRuleResource"
    // InternalRuleEngineParser.g:3612:1: entryRuleResource returns [EObject current=null] : iv_ruleResource= ruleResource EOF ;
    public final EObject entryRuleResource() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleResource = null;


        try {
            // InternalRuleEngineParser.g:3613:2: (iv_ruleResource= ruleResource EOF )
            // InternalRuleEngineParser.g:3614:2: iv_ruleResource= ruleResource EOF
            {
             newCompositeNode(grammarAccess.getResourceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleResource=ruleResource();

            state._fsp--;

             current =iv_ruleResource; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleResource"


    // $ANTLR start "ruleResource"
    // InternalRuleEngineParser.g:3621:1: ruleResource returns [EObject current=null] : (this_InputBitResource_0= ruleInputBitResource | this_OutputBitResource_1= ruleOutputBitResource | this_FlagByteResource_2= ruleFlagByteResource ) ;
    public final EObject ruleResource() throws RecognitionException {
        EObject current = null;

        EObject this_InputBitResource_0 = null;

        EObject this_OutputBitResource_1 = null;

        EObject this_FlagByteResource_2 = null;


         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:3624:28: ( (this_InputBitResource_0= ruleInputBitResource | this_OutputBitResource_1= ruleOutputBitResource | this_FlagByteResource_2= ruleFlagByteResource ) )
            // InternalRuleEngineParser.g:3625:1: (this_InputBitResource_0= ruleInputBitResource | this_OutputBitResource_1= ruleOutputBitResource | this_FlagByteResource_2= ruleFlagByteResource )
            {
            // InternalRuleEngineParser.g:3625:1: (this_InputBitResource_0= ruleInputBitResource | this_OutputBitResource_1= ruleOutputBitResource | this_FlagByteResource_2= ruleFlagByteResource )
            int alt35=3;
            switch ( input.LA(1) ) {
            case IX:
                {
                alt35=1;
                }
                break;
            case QX:
                {
                alt35=2;
                }
                break;
            case MW:
                {
                alt35=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }

            switch (alt35) {
                case 1 :
                    // InternalRuleEngineParser.g:3626:5: this_InputBitResource_0= ruleInputBitResource
                    {
                     
                            newCompositeNode(grammarAccess.getResourceAccess().getInputBitResourceParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_InputBitResource_0=ruleInputBitResource();

                    state._fsp--;


                            current = this_InputBitResource_0;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:3636:5: this_OutputBitResource_1= ruleOutputBitResource
                    {
                     
                            newCompositeNode(grammarAccess.getResourceAccess().getOutputBitResourceParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_OutputBitResource_1=ruleOutputBitResource();

                    state._fsp--;


                            current = this_OutputBitResource_1;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalRuleEngineParser.g:3646:5: this_FlagByteResource_2= ruleFlagByteResource
                    {
                     
                            newCompositeNode(grammarAccess.getResourceAccess().getFlagByteResourceParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_FlagByteResource_2=ruleFlagByteResource();

                    state._fsp--;


                            current = this_FlagByteResource_2;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResource"


    // $ANTLR start "entryRuleInputBitResource"
    // InternalRuleEngineParser.g:3662:1: entryRuleInputBitResource returns [EObject current=null] : iv_ruleInputBitResource= ruleInputBitResource EOF ;
    public final EObject entryRuleInputBitResource() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputBitResource = null;


        try {
            // InternalRuleEngineParser.g:3663:2: (iv_ruleInputBitResource= ruleInputBitResource EOF )
            // InternalRuleEngineParser.g:3664:2: iv_ruleInputBitResource= ruleInputBitResource EOF
            {
             newCompositeNode(grammarAccess.getInputBitResourceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInputBitResource=ruleInputBitResource();

            state._fsp--;

             current =iv_ruleInputBitResource; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputBitResource"


    // $ANTLR start "ruleInputBitResource"
    // InternalRuleEngineParser.g:3671:1: ruleInputBitResource returns [EObject current=null] : (otherlv_0= IX ( (lv_value_1_0= RULE_INT ) ) ) ;
    public final EObject ruleInputBitResource() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_value_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:3674:28: ( (otherlv_0= IX ( (lv_value_1_0= RULE_INT ) ) ) )
            // InternalRuleEngineParser.g:3675:1: (otherlv_0= IX ( (lv_value_1_0= RULE_INT ) ) )
            {
            // InternalRuleEngineParser.g:3675:1: (otherlv_0= IX ( (lv_value_1_0= RULE_INT ) ) )
            // InternalRuleEngineParser.g:3676:2: otherlv_0= IX ( (lv_value_1_0= RULE_INT ) )
            {
            otherlv_0=(Token)match(input,IX,FOLLOW_32); 

                	newLeafNode(otherlv_0, grammarAccess.getInputBitResourceAccess().getIXKeyword_0());
                
            // InternalRuleEngineParser.g:3680:1: ( (lv_value_1_0= RULE_INT ) )
            // InternalRuleEngineParser.g:3681:1: (lv_value_1_0= RULE_INT )
            {
            // InternalRuleEngineParser.g:3681:1: (lv_value_1_0= RULE_INT )
            // InternalRuleEngineParser.g:3682:3: lv_value_1_0= RULE_INT
            {
            lv_value_1_0=(Token)match(input,RULE_INT,FOLLOW_2); 

            			newLeafNode(lv_value_1_0, grammarAccess.getInputBitResourceAccess().getValueINTTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getInputBitResourceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.INT");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputBitResource"


    // $ANTLR start "entryRuleOutputBitResource"
    // InternalRuleEngineParser.g:3706:1: entryRuleOutputBitResource returns [EObject current=null] : iv_ruleOutputBitResource= ruleOutputBitResource EOF ;
    public final EObject entryRuleOutputBitResource() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputBitResource = null;


        try {
            // InternalRuleEngineParser.g:3707:2: (iv_ruleOutputBitResource= ruleOutputBitResource EOF )
            // InternalRuleEngineParser.g:3708:2: iv_ruleOutputBitResource= ruleOutputBitResource EOF
            {
             newCompositeNode(grammarAccess.getOutputBitResourceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOutputBitResource=ruleOutputBitResource();

            state._fsp--;

             current =iv_ruleOutputBitResource; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputBitResource"


    // $ANTLR start "ruleOutputBitResource"
    // InternalRuleEngineParser.g:3715:1: ruleOutputBitResource returns [EObject current=null] : (otherlv_0= QX ( (lv_value_1_0= RULE_INT ) ) ) ;
    public final EObject ruleOutputBitResource() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_value_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:3718:28: ( (otherlv_0= QX ( (lv_value_1_0= RULE_INT ) ) ) )
            // InternalRuleEngineParser.g:3719:1: (otherlv_0= QX ( (lv_value_1_0= RULE_INT ) ) )
            {
            // InternalRuleEngineParser.g:3719:1: (otherlv_0= QX ( (lv_value_1_0= RULE_INT ) ) )
            // InternalRuleEngineParser.g:3720:2: otherlv_0= QX ( (lv_value_1_0= RULE_INT ) )
            {
            otherlv_0=(Token)match(input,QX,FOLLOW_32); 

                	newLeafNode(otherlv_0, grammarAccess.getOutputBitResourceAccess().getQXKeyword_0());
                
            // InternalRuleEngineParser.g:3724:1: ( (lv_value_1_0= RULE_INT ) )
            // InternalRuleEngineParser.g:3725:1: (lv_value_1_0= RULE_INT )
            {
            // InternalRuleEngineParser.g:3725:1: (lv_value_1_0= RULE_INT )
            // InternalRuleEngineParser.g:3726:3: lv_value_1_0= RULE_INT
            {
            lv_value_1_0=(Token)match(input,RULE_INT,FOLLOW_2); 

            			newLeafNode(lv_value_1_0, grammarAccess.getOutputBitResourceAccess().getValueINTTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getOutputBitResourceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.INT");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputBitResource"


    // $ANTLR start "entryRuleFlagByteResource"
    // InternalRuleEngineParser.g:3750:1: entryRuleFlagByteResource returns [EObject current=null] : iv_ruleFlagByteResource= ruleFlagByteResource EOF ;
    public final EObject entryRuleFlagByteResource() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFlagByteResource = null;


        try {
            // InternalRuleEngineParser.g:3751:2: (iv_ruleFlagByteResource= ruleFlagByteResource EOF )
            // InternalRuleEngineParser.g:3752:2: iv_ruleFlagByteResource= ruleFlagByteResource EOF
            {
             newCompositeNode(grammarAccess.getFlagByteResourceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFlagByteResource=ruleFlagByteResource();

            state._fsp--;

             current =iv_ruleFlagByteResource; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFlagByteResource"


    // $ANTLR start "ruleFlagByteResource"
    // InternalRuleEngineParser.g:3759:1: ruleFlagByteResource returns [EObject current=null] : (otherlv_0= MW ( (lv_value_1_0= RULE_INT ) ) ) ;
    public final EObject ruleFlagByteResource() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_value_1_0=null;

         enterRule(); 
            
        try {
            // InternalRuleEngineParser.g:3762:28: ( (otherlv_0= MW ( (lv_value_1_0= RULE_INT ) ) ) )
            // InternalRuleEngineParser.g:3763:1: (otherlv_0= MW ( (lv_value_1_0= RULE_INT ) ) )
            {
            // InternalRuleEngineParser.g:3763:1: (otherlv_0= MW ( (lv_value_1_0= RULE_INT ) ) )
            // InternalRuleEngineParser.g:3764:2: otherlv_0= MW ( (lv_value_1_0= RULE_INT ) )
            {
            otherlv_0=(Token)match(input,MW,FOLLOW_32); 

                	newLeafNode(otherlv_0, grammarAccess.getFlagByteResourceAccess().getMWKeyword_0());
                
            // InternalRuleEngineParser.g:3768:1: ( (lv_value_1_0= RULE_INT ) )
            // InternalRuleEngineParser.g:3769:1: (lv_value_1_0= RULE_INT )
            {
            // InternalRuleEngineParser.g:3769:1: (lv_value_1_0= RULE_INT )
            // InternalRuleEngineParser.g:3770:3: lv_value_1_0= RULE_INT
            {
            lv_value_1_0=(Token)match(input,RULE_INT,FOLLOW_2); 

            			newLeafNode(lv_value_1_0, grammarAccess.getFlagByteResourceAccess().getValueINTTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFlagByteResourceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"com.lucy.g3.iec61131.xtext.il.RuleEngine.INT");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFlagByteResource"


    // $ANTLR start "ruleSimpleTypeName"
    // InternalRuleEngineParser.g:3794:1: ruleSimpleTypeName returns [Enumerator current=null] : ( (enumLiteral_0= BOOL ) | (enumLiteral_1= SINT ) | (enumLiteral_2= INT ) | (enumLiteral_3= DINT ) | (enumLiteral_4= LINT ) | (enumLiteral_5= TIME ) | (enumLiteral_6= DATE ) | (enumLiteral_7= BYTE ) | (enumLiteral_8= WORD ) | (enumLiteral_9= CHAR ) ) ;
    public final Enumerator ruleSimpleTypeName() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;
        Token enumLiteral_8=null;
        Token enumLiteral_9=null;

         enterRule(); 
        try {
            // InternalRuleEngineParser.g:3796:28: ( ( (enumLiteral_0= BOOL ) | (enumLiteral_1= SINT ) | (enumLiteral_2= INT ) | (enumLiteral_3= DINT ) | (enumLiteral_4= LINT ) | (enumLiteral_5= TIME ) | (enumLiteral_6= DATE ) | (enumLiteral_7= BYTE ) | (enumLiteral_8= WORD ) | (enumLiteral_9= CHAR ) ) )
            // InternalRuleEngineParser.g:3797:1: ( (enumLiteral_0= BOOL ) | (enumLiteral_1= SINT ) | (enumLiteral_2= INT ) | (enumLiteral_3= DINT ) | (enumLiteral_4= LINT ) | (enumLiteral_5= TIME ) | (enumLiteral_6= DATE ) | (enumLiteral_7= BYTE ) | (enumLiteral_8= WORD ) | (enumLiteral_9= CHAR ) )
            {
            // InternalRuleEngineParser.g:3797:1: ( (enumLiteral_0= BOOL ) | (enumLiteral_1= SINT ) | (enumLiteral_2= INT ) | (enumLiteral_3= DINT ) | (enumLiteral_4= LINT ) | (enumLiteral_5= TIME ) | (enumLiteral_6= DATE ) | (enumLiteral_7= BYTE ) | (enumLiteral_8= WORD ) | (enumLiteral_9= CHAR ) )
            int alt36=10;
            switch ( input.LA(1) ) {
            case BOOL:
                {
                alt36=1;
                }
                break;
            case SINT:
                {
                alt36=2;
                }
                break;
            case INT:
                {
                alt36=3;
                }
                break;
            case DINT:
                {
                alt36=4;
                }
                break;
            case LINT:
                {
                alt36=5;
                }
                break;
            case TIME:
                {
                alt36=6;
                }
                break;
            case DATE:
                {
                alt36=7;
                }
                break;
            case BYTE:
                {
                alt36=8;
                }
                break;
            case WORD:
                {
                alt36=9;
                }
                break;
            case CHAR:
                {
                alt36=10;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;
            }

            switch (alt36) {
                case 1 :
                    // InternalRuleEngineParser.g:3797:2: (enumLiteral_0= BOOL )
                    {
                    // InternalRuleEngineParser.g:3797:2: (enumLiteral_0= BOOL )
                    // InternalRuleEngineParser.g:3797:7: enumLiteral_0= BOOL
                    {
                    enumLiteral_0=(Token)match(input,BOOL,FOLLOW_2); 

                            current = grammarAccess.getSimpleTypeNameAccess().getBOOLEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getSimpleTypeNameAccess().getBOOLEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalRuleEngineParser.g:3803:6: (enumLiteral_1= SINT )
                    {
                    // InternalRuleEngineParser.g:3803:6: (enumLiteral_1= SINT )
                    // InternalRuleEngineParser.g:3803:11: enumLiteral_1= SINT
                    {
                    enumLiteral_1=(Token)match(input,SINT,FOLLOW_2); 

                            current = grammarAccess.getSimpleTypeNameAccess().getSINTEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getSimpleTypeNameAccess().getSINTEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // InternalRuleEngineParser.g:3809:6: (enumLiteral_2= INT )
                    {
                    // InternalRuleEngineParser.g:3809:6: (enumLiteral_2= INT )
                    // InternalRuleEngineParser.g:3809:11: enumLiteral_2= INT
                    {
                    enumLiteral_2=(Token)match(input,INT,FOLLOW_2); 

                            current = grammarAccess.getSimpleTypeNameAccess().getINTEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getSimpleTypeNameAccess().getINTEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // InternalRuleEngineParser.g:3815:6: (enumLiteral_3= DINT )
                    {
                    // InternalRuleEngineParser.g:3815:6: (enumLiteral_3= DINT )
                    // InternalRuleEngineParser.g:3815:11: enumLiteral_3= DINT
                    {
                    enumLiteral_3=(Token)match(input,DINT,FOLLOW_2); 

                            current = grammarAccess.getSimpleTypeNameAccess().getDINTEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_3, grammarAccess.getSimpleTypeNameAccess().getDINTEnumLiteralDeclaration_3()); 
                        

                    }


                    }
                    break;
                case 5 :
                    // InternalRuleEngineParser.g:3821:6: (enumLiteral_4= LINT )
                    {
                    // InternalRuleEngineParser.g:3821:6: (enumLiteral_4= LINT )
                    // InternalRuleEngineParser.g:3821:11: enumLiteral_4= LINT
                    {
                    enumLiteral_4=(Token)match(input,LINT,FOLLOW_2); 

                            current = grammarAccess.getSimpleTypeNameAccess().getLINTEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_4, grammarAccess.getSimpleTypeNameAccess().getLINTEnumLiteralDeclaration_4()); 
                        

                    }


                    }
                    break;
                case 6 :
                    // InternalRuleEngineParser.g:3827:6: (enumLiteral_5= TIME )
                    {
                    // InternalRuleEngineParser.g:3827:6: (enumLiteral_5= TIME )
                    // InternalRuleEngineParser.g:3827:11: enumLiteral_5= TIME
                    {
                    enumLiteral_5=(Token)match(input,TIME,FOLLOW_2); 

                            current = grammarAccess.getSimpleTypeNameAccess().getTIMEEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_5, grammarAccess.getSimpleTypeNameAccess().getTIMEEnumLiteralDeclaration_5()); 
                        

                    }


                    }
                    break;
                case 7 :
                    // InternalRuleEngineParser.g:3833:6: (enumLiteral_6= DATE )
                    {
                    // InternalRuleEngineParser.g:3833:6: (enumLiteral_6= DATE )
                    // InternalRuleEngineParser.g:3833:11: enumLiteral_6= DATE
                    {
                    enumLiteral_6=(Token)match(input,DATE,FOLLOW_2); 

                            current = grammarAccess.getSimpleTypeNameAccess().getDATEEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_6, grammarAccess.getSimpleTypeNameAccess().getDATEEnumLiteralDeclaration_6()); 
                        

                    }


                    }
                    break;
                case 8 :
                    // InternalRuleEngineParser.g:3839:6: (enumLiteral_7= BYTE )
                    {
                    // InternalRuleEngineParser.g:3839:6: (enumLiteral_7= BYTE )
                    // InternalRuleEngineParser.g:3839:11: enumLiteral_7= BYTE
                    {
                    enumLiteral_7=(Token)match(input,BYTE,FOLLOW_2); 

                            current = grammarAccess.getSimpleTypeNameAccess().getBYTEEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_7, grammarAccess.getSimpleTypeNameAccess().getBYTEEnumLiteralDeclaration_7()); 
                        

                    }


                    }
                    break;
                case 9 :
                    // InternalRuleEngineParser.g:3845:6: (enumLiteral_8= WORD )
                    {
                    // InternalRuleEngineParser.g:3845:6: (enumLiteral_8= WORD )
                    // InternalRuleEngineParser.g:3845:11: enumLiteral_8= WORD
                    {
                    enumLiteral_8=(Token)match(input,WORD,FOLLOW_2); 

                            current = grammarAccess.getSimpleTypeNameAccess().getWORDEnumLiteralDeclaration_8().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_8, grammarAccess.getSimpleTypeNameAccess().getWORDEnumLiteralDeclaration_8()); 
                        

                    }


                    }
                    break;
                case 10 :
                    // InternalRuleEngineParser.g:3851:6: (enumLiteral_9= CHAR )
                    {
                    // InternalRuleEngineParser.g:3851:6: (enumLiteral_9= CHAR )
                    // InternalRuleEngineParser.g:3851:11: enumLiteral_9= CHAR
                    {
                    enumLiteral_9=(Token)match(input,CHAR,FOLLOW_2); 

                            current = grammarAccess.getSimpleTypeNameAccess().getCHAREnumLiteralDeclaration_9().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_9, grammarAccess.getSimpleTypeNameAccess().getCHAREnumLiteralDeclaration_9()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleTypeName"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000002022L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x12C1BDF000111F80L,0x0000000000100830L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x12C13DF000111A80L,0x0000000000100830L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000080L,0x0000000000000800L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x12C1BDF000111F10L,0x0000000000100830L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x12C13DF000111A10L,0x0000000000100830L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000010L,0x0000000000000800L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x12C1BDF000111F40L,0x0000000000100030L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x12C13DF000111A40L,0x0000000000100030L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000000L,0x0000000000100000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x12C13DF000111A00L,0x0000000000100030L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000080L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x12C13DF000111A00L,0x0000000000100130L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x8000000000000002L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000040080000L,0x0000000000000800L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000840L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000001L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000040080002L,0x0000000000000800L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000040080002L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000900L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000080L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000E00000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x00000200BFC00000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000040080000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0002000000000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0C00000040280000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x8000000000000000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0004000000000000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x4000000000000000L});

}