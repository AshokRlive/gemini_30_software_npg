/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.IntParameter#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getIntParameter()
 * @model
 * @generated
 */
public interface IntParameter extends Parameter
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(int)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getIntParameter_Name()
   * @model
   * @generated
   */
  int getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.IntParameter#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(int value);

} // IntParameter
