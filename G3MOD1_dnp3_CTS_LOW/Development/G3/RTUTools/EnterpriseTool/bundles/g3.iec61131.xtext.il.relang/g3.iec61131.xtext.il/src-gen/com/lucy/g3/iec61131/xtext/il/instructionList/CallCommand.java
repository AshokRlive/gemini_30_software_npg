/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61131.xtext.il.instructionList.CallCommand#getCommand <em>Command</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getCallCommand()
 * @model
 * @generated
 */
public interface CallCommand extends ILCommand
{
  /**
   * Returns the value of the '<em><b>Command</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Command</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Command</em>' containment reference.
   * @see #setCommand(CommandTypes)
   * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage#getCallCommand_Command()
   * @model containment="true"
   * @generated
   */
  CommandTypes getCommand();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61131.xtext.il.instructionList.CallCommand#getCommand <em>Command</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Command</em>' containment reference.
   * @see #getCommand()
   * @generated
   */
  void setCommand(CommandTypes value);

} // CallCommand
