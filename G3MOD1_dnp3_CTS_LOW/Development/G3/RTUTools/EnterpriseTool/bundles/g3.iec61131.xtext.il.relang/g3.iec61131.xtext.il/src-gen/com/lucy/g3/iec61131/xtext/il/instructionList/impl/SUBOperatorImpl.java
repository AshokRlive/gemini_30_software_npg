/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.impl;

import com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage;
import com.lucy.g3.iec61131.xtext.il.instructionList.SUBOperator;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SUB Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SUBOperatorImpl extends MathCommandImpl implements SUBOperator
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SUBOperatorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return InstructionListPackage.Literals.SUB_OPERATOR;
  }

} //SUBOperatorImpl
