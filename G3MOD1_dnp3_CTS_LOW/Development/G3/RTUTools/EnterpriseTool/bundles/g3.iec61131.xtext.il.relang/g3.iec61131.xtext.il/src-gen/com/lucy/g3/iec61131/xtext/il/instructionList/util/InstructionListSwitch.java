/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.util;

import com.lucy.g3.iec61131.xtext.il.instructionList.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage
 * @generated
 */
public class InstructionListSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static InstructionListPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InstructionListSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = InstructionListPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case InstructionListPackage.IL_OBJECT:
      {
        ILObject ilObject = (ILObject)theEObject;
        T result = caseILObject(ilObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.PROGRAM:
      {
        Program program = (Program)theEObject;
        T result = caseProgram(program);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.FUNCTION_BLOCK:
      {
        FunctionBlock functionBlock = (FunctionBlock)theEObject;
        T result = caseFunctionBlock(functionBlock);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.FUNCTION:
      {
        Function function = (Function)theEObject;
        T result = caseFunction(function);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.FEATURE:
      {
        Feature feature = (Feature)theEObject;
        T result = caseFeature(feature);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.REFERENCE:
      {
        Reference reference = (Reference)theEObject;
        T result = caseReference(reference);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.RCOMMAND:
      {
        RCommand rCommand = (RCommand)theEObject;
        T result = caseRCommand(rCommand);
        if (result == null) result = caseILCommand(rCommand);
        if (result == null) result = caseCommand(rCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.SCOMMAND:
      {
        SCommand sCommand = (SCommand)theEObject;
        T result = caseSCommand(sCommand);
        if (result == null) result = caseILCommand(sCommand);
        if (result == null) result = caseCommand(sCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.IL_LABEL:
      {
        ILLabel ilLabel = (ILLabel)theEObject;
        T result = caseILLabel(ilLabel);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.FUNCTION_CALL:
      {
        FunctionCall functionCall = (FunctionCall)theEObject;
        T result = caseFunctionCall(functionCall);
        if (result == null) result = caseILCommand(functionCall);
        if (result == null) result = caseCommand(functionCall);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.FUNCTION_INPUTS:
      {
        FunctionInputs functionInputs = (FunctionInputs)theEObject;
        T result = caseFunctionInputs(functionInputs);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.JMPCN_COMMAND:
      {
        JMPCNCommand jmpcnCommand = (JMPCNCommand)theEObject;
        T result = caseJMPCNCommand(jmpcnCommand);
        if (result == null) result = caseILCommand(jmpcnCommand);
        if (result == null) result = caseCommand(jmpcnCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.JMP_COMMAND:
      {
        JMPCommand jmpCommand = (JMPCommand)theEObject;
        T result = caseJMPCommand(jmpCommand);
        if (result == null) result = caseILCommand(jmpCommand);
        if (result == null) result = caseCommand(jmpCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.EQ_COMMAND:
      {
        EQCommand eqCommand = (EQCommand)theEObject;
        T result = caseEQCommand(eqCommand);
        if (result == null) result = caseILCommand(eqCommand);
        if (result == null) result = caseCommand(eqCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.EQ_BOOL:
      {
        EQBool eqBool = (EQBool)theEObject;
        T result = caseEQBool(eqBool);
        if (result == null) result = caseEQCommand(eqBool);
        if (result == null) result = caseILCommand(eqBool);
        if (result == null) result = caseCommand(eqBool);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.EQN_BOOL:
      {
        EQNBool eqnBool = (EQNBool)theEObject;
        T result = caseEQNBool(eqnBool);
        if (result == null) result = caseEQCommand(eqnBool);
        if (result == null) result = caseILCommand(eqnBool);
        if (result == null) result = caseCommand(eqnBool);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.EQ_INT:
      {
        EQInt eqInt = (EQInt)theEObject;
        T result = caseEQInt(eqInt);
        if (result == null) result = caseEQCommand(eqInt);
        if (result == null) result = caseILCommand(eqInt);
        if (result == null) result = caseCommand(eqInt);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.EQN_INT:
      {
        EQNInt eqnInt = (EQNInt)theEObject;
        T result = caseEQNInt(eqnInt);
        if (result == null) result = caseEQCommand(eqnInt);
        if (result == null) result = caseILCommand(eqnInt);
        if (result == null) result = caseCommand(eqnInt);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.COMMAND:
      {
        Command command = (Command)theEObject;
        T result = caseCommand(command);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.IL_COMMAND:
      {
        ILCommand ilCommand = (ILCommand)theEObject;
        T result = caseILCommand(ilCommand);
        if (result == null) result = caseCommand(ilCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.EXTENDED_COMMANDS:
      {
        ExtendedCommands extendedCommands = (ExtendedCommands)theEObject;
        T result = caseExtendedCommands(extendedCommands);
        if (result == null) result = caseCommand(extendedCommands);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.DELAY_COMMAND:
      {
        DelayCommand delayCommand = (DelayCommand)theEObject;
        T result = caseDelayCommand(delayCommand);
        if (result == null) result = caseExtendedCommands(delayCommand);
        if (result == null) result = caseCommand(delayCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.PARAMETER:
      {
        Parameter parameter = (Parameter)theEObject;
        T result = caseParameter(parameter);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.RESOURCE_PARAMATER:
      {
        ResourceParamater resourceParamater = (ResourceParamater)theEObject;
        T result = caseResourceParamater(resourceParamater);
        if (result == null) result = caseParameter(resourceParamater);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.INT_PARAMETER:
      {
        IntParameter intParameter = (IntParameter)theEObject;
        T result = caseIntParameter(intParameter);
        if (result == null) result = caseParameter(intParameter);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.BOOLEAN_PARAMETER:
      {
        BooleanParameter booleanParameter = (BooleanParameter)theEObject;
        T result = caseBooleanParameter(booleanParameter);
        if (result == null) result = caseParameter(booleanParameter);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.ST_COMMAND:
      {
        STCommand stCommand = (STCommand)theEObject;
        T result = caseSTCommand(stCommand);
        if (result == null) result = caseILCommand(stCommand);
        if (result == null) result = caseCommand(stCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.LD_COMMAND:
      {
        LDCommand ldCommand = (LDCommand)theEObject;
        T result = caseLDCommand(ldCommand);
        if (result == null) result = caseILCommand(ldCommand);
        if (result == null) result = caseCommand(ldCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.MATH_COMMAND:
      {
        MathCommand mathCommand = (MathCommand)theEObject;
        T result = caseMathCommand(mathCommand);
        if (result == null) result = caseILCommand(mathCommand);
        if (result == null) result = caseCommand(mathCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.ADD_OPERATOR:
      {
        ADDOperator addOperator = (ADDOperator)theEObject;
        T result = caseADDOperator(addOperator);
        if (result == null) result = caseMathCommand(addOperator);
        if (result == null) result = caseILCommand(addOperator);
        if (result == null) result = caseCommand(addOperator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.SUB_OPERATOR:
      {
        SUBOperator subOperator = (SUBOperator)theEObject;
        T result = caseSUBOperator(subOperator);
        if (result == null) result = caseMathCommand(subOperator);
        if (result == null) result = caseILCommand(subOperator);
        if (result == null) result = caseCommand(subOperator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.MUL_OPERATOR:
      {
        MULOperator mulOperator = (MULOperator)theEObject;
        T result = caseMULOperator(mulOperator);
        if (result == null) result = caseMathCommand(mulOperator);
        if (result == null) result = caseILCommand(mulOperator);
        if (result == null) result = caseCommand(mulOperator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.DIV_OPERATOR:
      {
        DIVOperator divOperator = (DIVOperator)theEObject;
        T result = caseDIVOperator(divOperator);
        if (result == null) result = caseMathCommand(divOperator);
        if (result == null) result = caseILCommand(divOperator);
        if (result == null) result = caseCommand(divOperator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.LOGIC_OP_COMMAND:
      {
        LogicOpCommand logicOpCommand = (LogicOpCommand)theEObject;
        T result = caseLogicOpCommand(logicOpCommand);
        if (result == null) result = caseILCommand(logicOpCommand);
        if (result == null) result = caseCommand(logicOpCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.AND_OPERATOR:
      {
        ANDOperator andOperator = (ANDOperator)theEObject;
        T result = caseANDOperator(andOperator);
        if (result == null) result = caseLogicOpCommand(andOperator);
        if (result == null) result = caseILCommand(andOperator);
        if (result == null) result = caseCommand(andOperator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.OR_OPERATOR:
      {
        OROperator orOperator = (OROperator)theEObject;
        T result = caseOROperator(orOperator);
        if (result == null) result = caseLogicOpCommand(orOperator);
        if (result == null) result = caseILCommand(orOperator);
        if (result == null) result = caseCommand(orOperator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.XOR_OPERATOR:
      {
        XOROperator xorOperator = (XOROperator)theEObject;
        T result = caseXOROperator(xorOperator);
        if (result == null) result = caseLogicOpCommand(xorOperator);
        if (result == null) result = caseILCommand(xorOperator);
        if (result == null) result = caseCommand(xorOperator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.RETURN_COMMAND:
      {
        ReturnCommand returnCommand = (ReturnCommand)theEObject;
        T result = caseReturnCommand(returnCommand);
        if (result == null) result = caseILCommand(returnCommand);
        if (result == null) result = caseCommand(returnCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.CALL_COMMAND:
      {
        CallCommand callCommand = (CallCommand)theEObject;
        T result = caseCallCommand(callCommand);
        if (result == null) result = caseILCommand(callCommand);
        if (result == null) result = caseCommand(callCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.COMMAND_TYPES:
      {
        CommandTypes commandTypes = (CommandTypes)theEObject;
        T result = caseCommandTypes(commandTypes);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.INIT_DATA:
      {
        InitData initData = (InitData)theEObject;
        T result = caseInitData(initData);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.BOOL_DATA:
      {
        BoolData boolData = (BoolData)theEObject;
        T result = caseBoolData(boolData);
        if (result == null) result = caseInitData(boolData);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.TIME_DATA:
      {
        TimeData timeData = (TimeData)theEObject;
        T result = caseTimeData(timeData);
        if (result == null) result = caseInitData(timeData);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.AVAR:
      {
        AVar aVar = (AVar)theEObject;
        T result = caseAVar(aVar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.TYPE:
      {
        Type type = (Type)theEObject;
        T result = caseType(type);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.VARS:
      {
        Vars vars = (Vars)theEObject;
        T result = caseVars(vars);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.INPUT_VARS:
      {
        InputVars inputVars = (InputVars)theEObject;
        T result = caseInputVars(inputVars);
        if (result == null) result = caseFeature(inputVars);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.OUTPUT_VARS:
      {
        OutputVars outputVars = (OutputVars)theEObject;
        T result = caseOutputVars(outputVars);
        if (result == null) result = caseFeature(outputVars);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.RESOURCE_VAR:
      {
        ResourceVar resourceVar = (ResourceVar)theEObject;
        T result = caseResourceVar(resourceVar);
        if (result == null) result = caseAVar(resourceVar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.RESOURCE_TYPE:
      {
        ResourceType resourceType = (ResourceType)theEObject;
        T result = caseResourceType(resourceType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.INT_RESOURCE_TYPE:
      {
        IntResourceType intResourceType = (IntResourceType)theEObject;
        T result = caseIntResourceType(intResourceType);
        if (result == null) result = caseResourceType(intResourceType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.BOOLEAN_RESOURCE_TYPE:
      {
        BooleanResourceType booleanResourceType = (BooleanResourceType)theEObject;
        T result = caseBooleanResourceType(booleanResourceType);
        if (result == null) result = caseResourceType(booleanResourceType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.CUSTOM_VAR:
      {
        CustomVar customVar = (CustomVar)theEObject;
        T result = caseCustomVar(customVar);
        if (result == null) result = caseAVar(customVar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.INT_VAR:
      {
        IntVar intVar = (IntVar)theEObject;
        T result = caseIntVar(intVar);
        if (result == null) result = caseAVar(intVar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.BOOL_VAR:
      {
        BoolVar boolVar = (BoolVar)theEObject;
        T result = caseBoolVar(boolVar);
        if (result == null) result = caseAVar(boolVar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.TIMER_VAR:
      {
        TimerVar timerVar = (TimerVar)theEObject;
        T result = caseTimerVar(timerVar);
        if (result == null) result = caseAVar(timerVar);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.TIMER_CALL_COMMAND:
      {
        TimerCallCommand timerCallCommand = (TimerCallCommand)theEObject;
        T result = caseTimerCallCommand(timerCallCommand);
        if (result == null) result = caseCommandTypes(timerCallCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.RESOURCE:
      {
        Resource resource = (Resource)theEObject;
        T result = caseResource(resource);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.INPUT_BIT_RESOURCE:
      {
        InputBitResource inputBitResource = (InputBitResource)theEObject;
        T result = caseInputBitResource(inputBitResource);
        if (result == null) result = caseResource(inputBitResource);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.OUTPUT_BIT_RESOURCE:
      {
        OutputBitResource outputBitResource = (OutputBitResource)theEObject;
        T result = caseOutputBitResource(outputBitResource);
        if (result == null) result = caseResource(outputBitResource);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.FLAG_BYTE_RESOURCE:
      {
        FlagByteResource flagByteResource = (FlagByteResource)theEObject;
        T result = caseFlagByteResource(flagByteResource);
        if (result == null) result = caseResource(flagByteResource);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.OBJECT_REFERENCE:
      {
        ObjectReference objectReference = (ObjectReference)theEObject;
        T result = caseObjectReference(objectReference);
        if (result == null) result = caseReference(objectReference);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case InstructionListPackage.DOT_EXPRESSION:
      {
        DotExpression dotExpression = (DotExpression)theEObject;
        T result = caseDotExpression(dotExpression);
        if (result == null) result = caseReference(dotExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>IL Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>IL Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseILObject(ILObject object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Program</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Program</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseProgram(Program object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Block</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Block</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionBlock(FunctionBlock object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunction(Function object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Feature</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Feature</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFeature(Feature object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Reference</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Reference</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReference(Reference object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>RCommand</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>RCommand</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRCommand(RCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>SCommand</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>SCommand</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSCommand(SCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>IL Label</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>IL Label</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseILLabel(ILLabel object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Call</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Call</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionCall(FunctionCall object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Function Inputs</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Function Inputs</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionInputs(FunctionInputs object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>JMPCN Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>JMPCN Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseJMPCNCommand(JMPCNCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>JMP Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>JMP Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseJMPCommand(JMPCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EQ Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EQ Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEQCommand(EQCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EQ Bool</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EQ Bool</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEQBool(EQBool object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EQN Bool</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EQN Bool</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEQNBool(EQNBool object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EQ Int</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EQ Int</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEQInt(EQInt object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EQN Int</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EQN Int</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEQNInt(EQNInt object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCommand(Command object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>IL Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>IL Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseILCommand(ILCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Extended Commands</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Extended Commands</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExtendedCommands(ExtendedCommands object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Delay Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Delay Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDelayCommand(DelayCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParameter(Parameter object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Resource Paramater</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Resource Paramater</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseResourceParamater(ResourceParamater object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Int Parameter</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Int Parameter</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIntParameter(IntParameter object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Parameter</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Parameter</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBooleanParameter(BooleanParameter object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>ST Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>ST Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSTCommand(STCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LD Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LD Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLDCommand(LDCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Math Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Math Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMathCommand(MathCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>ADD Operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>ADD Operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseADDOperator(ADDOperator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>SUB Operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>SUB Operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSUBOperator(SUBOperator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>MUL Operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>MUL Operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMULOperator(MULOperator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>DIV Operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>DIV Operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDIVOperator(DIVOperator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Logic Op Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Logic Op Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLogicOpCommand(LogicOpCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>AND Operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>AND Operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseANDOperator(ANDOperator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>OR Operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>OR Operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOROperator(OROperator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>XOR Operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>XOR Operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseXOROperator(XOROperator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Return Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Return Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReturnCommand(ReturnCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Call Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Call Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCallCommand(CallCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Command Types</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Command Types</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCommandTypes(CommandTypes object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Init Data</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Init Data</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInitData(InitData object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bool Data</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bool Data</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBoolData(BoolData object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Time Data</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Time Data</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTimeData(TimeData object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>AVar</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>AVar</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAVar(AVar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseType(Type object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Vars</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Vars</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVars(Vars object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Input Vars</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Input Vars</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInputVars(InputVars object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Output Vars</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Output Vars</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOutputVars(OutputVars object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Resource Var</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Resource Var</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseResourceVar(ResourceVar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Resource Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Resource Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseResourceType(ResourceType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Int Resource Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Int Resource Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIntResourceType(IntResourceType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Resource Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Resource Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBooleanResourceType(BooleanResourceType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Custom Var</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Custom Var</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCustomVar(CustomVar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Int Var</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Int Var</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIntVar(IntVar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bool Var</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bool Var</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBoolVar(BoolVar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Timer Var</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Timer Var</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTimerVar(TimerVar object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Timer Call Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Timer Call Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTimerCallCommand(TimerCallCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Resource</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Resource</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseResource(Resource object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Input Bit Resource</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Input Bit Resource</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInputBitResource(InputBitResource object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Output Bit Resource</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Output Bit Resource</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOutputBitResource(OutputBitResource object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Flag Byte Resource</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Flag Byte Resource</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFlagByteResource(FlagByteResource object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Object Reference</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Object Reference</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseObjectReference(ObjectReference object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Dot Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Dot Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDotExpression(DotExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //InstructionListSwitch
