/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.impl;

import com.lucy.g3.iec61131.xtext.il.instructionList.InputBitResource;
import com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Input Bit Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InputBitResourceImpl extends ResourceImpl implements InputBitResource
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InputBitResourceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return InstructionListPackage.Literals.INPUT_BIT_RESOURCE;
  }

} //InputBitResourceImpl
