/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.impl;

import com.lucy.g3.iec61131.xtext.il.instructionList.EQNBool;
import com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EQN Bool</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EQNBoolImpl extends EQCommandImpl implements EQNBool
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EQNBoolImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return InstructionListPackage.Literals.EQN_BOOL;
  }

} //EQNBoolImpl
