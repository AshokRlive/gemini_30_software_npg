/**
 */
package com.lucy.g3.iec61131.xtext.il.instructionList.impl;

import com.lucy.g3.iec61131.xtext.il.instructionList.Command;
import com.lucy.g3.iec61131.xtext.il.instructionList.InstructionListPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CommandImpl extends MinimalEObjectImpl.Container implements Command
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CommandImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return InstructionListPackage.Literals.COMMAND;
  }

} //CommandImpl
