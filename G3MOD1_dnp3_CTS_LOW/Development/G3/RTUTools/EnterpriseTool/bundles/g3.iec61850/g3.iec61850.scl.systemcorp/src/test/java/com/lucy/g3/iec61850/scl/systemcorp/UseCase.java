package com.lucy.g3.iec61850.scl.systemcorp;

import java.io.File;
import java.net.URL;

import com.lucy.g3.iec61850.scl.ResourceRegister;

import junit.framework.Assert;
import junit.framework.TestCase;

public class UseCase extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
		ResourceRegister.init();
	}

	public void testTransformStringString() {
		
		String out = "target/testOutput.xml";
		
		try {
			
			URL url = getClass().getResource("/example1.scd");
					
			
			SystemCorpIntegration.transform(url.getFile(), out);
			
			
			
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

}
