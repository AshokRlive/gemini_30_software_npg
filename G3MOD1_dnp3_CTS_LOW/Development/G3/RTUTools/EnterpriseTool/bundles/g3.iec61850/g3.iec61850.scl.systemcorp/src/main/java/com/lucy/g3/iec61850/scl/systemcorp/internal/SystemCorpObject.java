package com.lucy.g3.iec61850.scl.systemcorp.internal;

import com.lucy.g3.iec61850.model.systemcorp.SystemCorpFactory;
import com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject;

public class SystemCorpObject {

	
	public static TGenericPrivateObject generate () {
    	
    	TGenericPrivateObject sys = SystemCorpFactory.eINSTANCE.createTGenericPrivateObject();
		sys.setField1(0);
		sys.setField2(0);
		sys.setField3(0);
		sys.setField4(0);
		sys.setField5(0);
		
		return sys;
		
    }
}
