/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping.impl;

import com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.DocumentRoot;
import com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingFactory;
import com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage;
import com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT;

import com.lucy.g3.iec61850.model.g3ref.G3RefPackage;

import com.lucy.g3.iec61850.model.g3ref.impl.G3RefPackageImpl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;

import com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl;

import com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage;

import com.lucy.g3.iec61850.model.systemcorp.impl.SystemCorpPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class IEC61850MappingPackageImpl extends EPackageImpl implements IEC61850MappingPackage {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass dataAttributeIdTEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass documentRootEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass entryTEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass g3RefTEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mappingRootTEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private IEC61850MappingPackageImpl() {
    super(eNS_URI, IEC61850MappingFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link IEC61850MappingPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static IEC61850MappingPackage init() {
    if (isInited) return (IEC61850MappingPackage)EPackage.Registry.INSTANCE.getEPackage(IEC61850MappingPackage.eNS_URI);

    // Obtain or create and register package
    IEC61850MappingPackageImpl theIEC61850MappingPackage = (IEC61850MappingPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof IEC61850MappingPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new IEC61850MappingPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    XMLTypePackage.eINSTANCE.eClass();

    // Obtain or create and register interdependencies
    G3RefPackageImpl theG3RefPackage = (G3RefPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(G3RefPackage.eNS_URI) instanceof G3RefPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(G3RefPackage.eNS_URI) : G3RefPackage.eINSTANCE);
    SCLPackageImpl theSCLPackage = (SCLPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI) instanceof SCLPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI) : SCLPackage.eINSTANCE);
    SystemCorpPackageImpl theSystemCorpPackage = (SystemCorpPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SystemCorpPackage.eNS_URI) instanceof SystemCorpPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SystemCorpPackage.eNS_URI) : SystemCorpPackage.eINSTANCE);

    // Load packages
    theSCLPackage.loadPackage();

    // Create package meta-data objects
    theIEC61850MappingPackage.createPackageContents();
    theG3RefPackage.createPackageContents();
    theSystemCorpPackage.createPackageContents();

    // Initialize created meta-data
    theIEC61850MappingPackage.initializePackageContents();
    theG3RefPackage.initializePackageContents();
    theSystemCorpPackage.initializePackageContents();

    // Fix loaded packages
    theSCLPackage.fixPackageContents();

    // Mark meta-data to indicate it can't be changed
    theIEC61850MappingPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(IEC61850MappingPackage.eNS_URI, theIEC61850MappingPackage);
    return theIEC61850MappingPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDataAttributeIdT() {
    return dataAttributeIdTEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDataAttributeIdT_Field0() {
    return (EAttribute)dataAttributeIdTEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDataAttributeIdT_Field1() {
    return (EAttribute)dataAttributeIdTEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDataAttributeIdT_Field2() {
    return (EAttribute)dataAttributeIdTEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDataAttributeIdT_Field3() {
    return (EAttribute)dataAttributeIdTEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDataAttributeIdT_Field4() {
    return (EAttribute)dataAttributeIdTEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDataAttributeIdT_Field5() {
    return (EAttribute)dataAttributeIdTEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDocumentRoot() {
    return documentRootEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDocumentRoot_Mixed() {
    return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XMLNSPrefixMap() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XSISchemaLocation() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_MappingRoot() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEntryT() {
    return entryTEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEntryT_G3Ref() {
    return (EReference)entryTEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEntryT_DataAttributeId() {
    return (EReference)entryTEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getEntryT_AttributeRef() {
    return (EAttribute)entryTEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getEntryT_Uuid() {
    return (EAttribute)entryTEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getEntryT_DataType() {
    return (EAttribute)entryTEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getG3RefT() {
    return g3RefTEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getG3RefT_Group() {
    return (EAttribute)g3RefTEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getG3RefT_Id() {
    return (EAttribute)g3RefTEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getG3RefT_Uuid() {
    return (EAttribute)g3RefTEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMappingRootT() {
    return mappingRootTEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMappingRootT_Entry() {
    return (EReference)mappingRootTEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMappingRootT_SchemaVersionMajor() {
    return (EAttribute)mappingRootTEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMappingRootT_SchemaVersionMinor() {
    return (EAttribute)mappingRootTEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IEC61850MappingFactory getIEC61850MappingFactory() {
    return (IEC61850MappingFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents() {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    dataAttributeIdTEClass = createEClass(DATA_ATTRIBUTE_ID_T);
    createEAttribute(dataAttributeIdTEClass, DATA_ATTRIBUTE_ID_T__FIELD0);
    createEAttribute(dataAttributeIdTEClass, DATA_ATTRIBUTE_ID_T__FIELD1);
    createEAttribute(dataAttributeIdTEClass, DATA_ATTRIBUTE_ID_T__FIELD2);
    createEAttribute(dataAttributeIdTEClass, DATA_ATTRIBUTE_ID_T__FIELD3);
    createEAttribute(dataAttributeIdTEClass, DATA_ATTRIBUTE_ID_T__FIELD4);
    createEAttribute(dataAttributeIdTEClass, DATA_ATTRIBUTE_ID_T__FIELD5);

    documentRootEClass = createEClass(DOCUMENT_ROOT);
    createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
    createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
    createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
    createEReference(documentRootEClass, DOCUMENT_ROOT__MAPPING_ROOT);

    entryTEClass = createEClass(ENTRY_T);
    createEReference(entryTEClass, ENTRY_T__G3_REF);
    createEReference(entryTEClass, ENTRY_T__DATA_ATTRIBUTE_ID);
    createEAttribute(entryTEClass, ENTRY_T__ATTRIBUTE_REF);
    createEAttribute(entryTEClass, ENTRY_T__UUID);
    createEAttribute(entryTEClass, ENTRY_T__DATA_TYPE);

    g3RefTEClass = createEClass(G3_REF_T);
    createEAttribute(g3RefTEClass, G3_REF_T__GROUP);
    createEAttribute(g3RefTEClass, G3_REF_T__ID);
    createEAttribute(g3RefTEClass, G3_REF_T__UUID);

    mappingRootTEClass = createEClass(MAPPING_ROOT_T);
    createEReference(mappingRootTEClass, MAPPING_ROOT_T__ENTRY);
    createEAttribute(mappingRootTEClass, MAPPING_ROOT_T__SCHEMA_VERSION_MAJOR);
    createEAttribute(mappingRootTEClass, MAPPING_ROOT_T__SCHEMA_VERSION_MINOR);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents() {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes

    // Initialize classes, features, and operations; add parameters
    initEClass(dataAttributeIdTEClass, DataAttributeIdT.class, "DataAttributeIdT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDataAttributeIdT_Field0(), theXMLTypePackage.getInteger(), "field0", null, 1, 1, DataAttributeIdT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDataAttributeIdT_Field1(), theXMLTypePackage.getInteger(), "field1", null, 1, 1, DataAttributeIdT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDataAttributeIdT_Field2(), theXMLTypePackage.getInteger(), "field2", null, 1, 1, DataAttributeIdT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDataAttributeIdT_Field3(), theXMLTypePackage.getInteger(), "field3", null, 1, 1, DataAttributeIdT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDataAttributeIdT_Field4(), theXMLTypePackage.getInteger(), "field4", null, 1, 1, DataAttributeIdT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDataAttributeIdT_Field5(), theXMLTypePackage.getInteger(), "field5", null, 1, 1, DataAttributeIdT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_MappingRoot(), this.getMappingRootT(), null, "mappingRoot", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

    initEClass(entryTEClass, EntryT.class, "EntryT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getEntryT_G3Ref(), this.getG3RefT(), null, "g3Ref", null, 1, 1, EntryT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getEntryT_DataAttributeId(), this.getDataAttributeIdT(), null, "dataAttributeId", null, 1, 1, EntryT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getEntryT_AttributeRef(), theXMLTypePackage.getString(), "attributeRef", null, 1, 1, EntryT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getEntryT_Uuid(), theXMLTypePackage.getString(), "uuid", null, 1, 1, EntryT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getEntryT_DataType(), theXMLTypePackage.getInteger(), "dataType", null, 1, 1, EntryT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(g3RefTEClass, G3RefT.class, "G3RefT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getG3RefT_Group(), theXMLTypePackage.getInteger(), "group", null, 1, 1, G3RefT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getG3RefT_Id(), theXMLTypePackage.getInteger(), "id", null, 0, 1, G3RefT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getG3RefT_Uuid(), theXMLTypePackage.getString(), "uuid", null, 1, 1, G3RefT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(mappingRootTEClass, MappingRootT.class, "MappingRootT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMappingRootT_Entry(), this.getEntryT(), null, "entry", null, 1, -1, MappingRootT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMappingRootT_SchemaVersionMajor(), theXMLTypePackage.getUnsignedByte(), "schemaVersionMajor", "1", 1, 1, MappingRootT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMappingRootT_SchemaVersionMinor(), theXMLTypePackage.getUnsignedByte(), "schemaVersionMinor", null, 1, 1, MappingRootT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);

    // Create annotations
    // http:///org/eclipse/emf/ecore/util/ExtendedMetaData
    createExtendedMetaDataAnnotations();
  }

  /**
   * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createExtendedMetaDataAnnotations() {
    String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";	
    addAnnotation
      (this, 
       source, 
       new String[] {
       "qualified", "false"
       });	
    addAnnotation
      (dataAttributeIdTEClass, 
       source, 
       new String[] {
       "name", "DataAttributeIdT",
       "kind", "empty"
       });	
    addAnnotation
      (getDataAttributeIdT_Field0(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "field0",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (getDataAttributeIdT_Field1(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "field1",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (getDataAttributeIdT_Field2(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "field2",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (getDataAttributeIdT_Field3(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "field3",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (getDataAttributeIdT_Field4(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "field4",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (getDataAttributeIdT_Field5(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "field5",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (documentRootEClass, 
       source, 
       new String[] {
       "name", "",
       "kind", "mixed"
       });	
    addAnnotation
      (getDocumentRoot_Mixed(), 
       source, 
       new String[] {
       "kind", "elementWildcard",
       "name", ":mixed"
       });	
    addAnnotation
      (getDocumentRoot_XMLNSPrefixMap(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "xmlns:prefix"
       });	
    addAnnotation
      (getDocumentRoot_XSISchemaLocation(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "xsi:schemaLocation"
       });	
    addAnnotation
      (getDocumentRoot_MappingRoot(), 
       source, 
       new String[] {
       "kind", "element",
       "name", "mappingRoot",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (entryTEClass, 
       source, 
       new String[] {
       "name", "EntryT",
       "kind", "elementOnly"
       });	
    addAnnotation
      (getEntryT_G3Ref(), 
       source, 
       new String[] {
       "kind", "element",
       "name", "g3Ref",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (getEntryT_DataAttributeId(), 
       source, 
       new String[] {
       "kind", "element",
       "name", "dataAttributeId",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (getEntryT_AttributeRef(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "attributeRef",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (getEntryT_Uuid(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "attributeRef",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (getEntryT_DataType(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "dataType",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (g3RefTEClass, 
       source, 
       new String[] {
       "name", "G3RefT",
       "kind", "empty"
       });	
    addAnnotation
      (getG3RefT_Group(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "group",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (getG3RefT_Id(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "id",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (getG3RefT_Uuid(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "uuid",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (mappingRootTEClass, 
       source, 
       new String[] {
       "name", "MappingRootT",
       "kind", "elementOnly"
       });	
    addAnnotation
      (getMappingRootT_Entry(), 
       source, 
       new String[] {
       "kind", "element",
       "name", "entry",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (getMappingRootT_SchemaVersionMajor(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "schemaVersionMajor",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (getMappingRootT_SchemaVersionMinor(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "schemaVersionMinor",
       "namespace", "##targetNamespace"
       });
  }

} //IEC61850MappingPackageImpl
