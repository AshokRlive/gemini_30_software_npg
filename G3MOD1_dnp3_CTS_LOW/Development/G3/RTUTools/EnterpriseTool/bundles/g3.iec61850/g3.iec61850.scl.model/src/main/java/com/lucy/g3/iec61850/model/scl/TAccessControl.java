/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TAccess Control</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAccessControl()
 * @model extendedMetaData="name='tAccessControl' kind='mixed'"
 * @generated
 */
public interface TAccessControl extends TAnyContentFromOtherNamespace {
} // TAccessControl
