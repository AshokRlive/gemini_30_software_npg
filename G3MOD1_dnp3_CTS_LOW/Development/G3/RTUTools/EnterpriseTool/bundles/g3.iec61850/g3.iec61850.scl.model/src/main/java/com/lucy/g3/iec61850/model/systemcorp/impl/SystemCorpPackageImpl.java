/**
 */
package com.lucy.g3.iec61850.model.systemcorp.impl;

import com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage;

import com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl;

import com.lucy.g3.iec61850.model.g3ref.G3RefPackage;

import com.lucy.g3.iec61850.model.g3ref.impl.G3RefPackageImpl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;

import com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl;

import com.lucy.g3.iec61850.model.systemcorp.DocumentRoot;
import com.lucy.g3.iec61850.model.systemcorp.SystemCorpFactory;
import com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage;
import com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SystemCorpPackageImpl extends EPackageImpl implements SystemCorpPackage {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass documentRootEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tGenericPrivateObjectEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private SystemCorpPackageImpl() {
    super(eNS_URI, SystemCorpFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link SystemCorpPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static SystemCorpPackage init() {
    if (isInited) return (SystemCorpPackage)EPackage.Registry.INSTANCE.getEPackage(SystemCorpPackage.eNS_URI);

    // Obtain or create and register package
    SystemCorpPackageImpl theSystemCorpPackage = (SystemCorpPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SystemCorpPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SystemCorpPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    XMLTypePackage.eINSTANCE.eClass();

    // Obtain or create and register interdependencies
    G3RefPackageImpl theG3RefPackage = (G3RefPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(G3RefPackage.eNS_URI) instanceof G3RefPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(G3RefPackage.eNS_URI) : G3RefPackage.eINSTANCE);
    IEC61850MappingPackageImpl theIEC61850MappingPackage = (IEC61850MappingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IEC61850MappingPackage.eNS_URI) instanceof IEC61850MappingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IEC61850MappingPackage.eNS_URI) : IEC61850MappingPackage.eINSTANCE);
    SCLPackageImpl theSCLPackage = (SCLPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI) instanceof SCLPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI) : SCLPackage.eINSTANCE);

    // Load packages
    theSCLPackage.loadPackage();

    // Create package meta-data objects
    theSystemCorpPackage.createPackageContents();
    theG3RefPackage.createPackageContents();
    theIEC61850MappingPackage.createPackageContents();

    // Initialize created meta-data
    theSystemCorpPackage.initializePackageContents();
    theG3RefPackage.initializePackageContents();
    theIEC61850MappingPackage.initializePackageContents();

    // Fix loaded packages
    theSCLPackage.fixPackageContents();

    // Mark meta-data to indicate it can't be changed
    theSystemCorpPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(SystemCorpPackage.eNS_URI, theSystemCorpPackage);
    return theSystemCorpPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDocumentRoot() {
    return documentRootEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDocumentRoot_Mixed() {
    return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XMLNSPrefixMap() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XSISchemaLocation() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_GenericPrivateObject() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTGenericPrivateObject() {
    return tGenericPrivateObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTGenericPrivateObject_Field1() {
    return (EAttribute)tGenericPrivateObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTGenericPrivateObject_Field2() {
    return (EAttribute)tGenericPrivateObjectEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTGenericPrivateObject_Field3() {
    return (EAttribute)tGenericPrivateObjectEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTGenericPrivateObject_Field4() {
    return (EAttribute)tGenericPrivateObjectEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTGenericPrivateObject_Field5() {
    return (EAttribute)tGenericPrivateObjectEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SystemCorpFactory getSystemCorpFactory() {
    return (SystemCorpFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents() {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    documentRootEClass = createEClass(DOCUMENT_ROOT);
    createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
    createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
    createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
    createEReference(documentRootEClass, DOCUMENT_ROOT__GENERIC_PRIVATE_OBJECT);

    tGenericPrivateObjectEClass = createEClass(TGENERIC_PRIVATE_OBJECT);
    createEAttribute(tGenericPrivateObjectEClass, TGENERIC_PRIVATE_OBJECT__FIELD1);
    createEAttribute(tGenericPrivateObjectEClass, TGENERIC_PRIVATE_OBJECT__FIELD2);
    createEAttribute(tGenericPrivateObjectEClass, TGENERIC_PRIVATE_OBJECT__FIELD3);
    createEAttribute(tGenericPrivateObjectEClass, TGENERIC_PRIVATE_OBJECT__FIELD4);
    createEAttribute(tGenericPrivateObjectEClass, TGENERIC_PRIVATE_OBJECT__FIELD5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents() {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes

    // Initialize classes, features, and operations; add parameters
    initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_GenericPrivateObject(), this.getTGenericPrivateObject(), null, "genericPrivateObject", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

    initEClass(tGenericPrivateObjectEClass, TGenericPrivateObject.class, "TGenericPrivateObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getTGenericPrivateObject_Field1(), theXMLTypePackage.getUnsignedInt(), "field1", null, 1, 1, TGenericPrivateObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTGenericPrivateObject_Field2(), theXMLTypePackage.getUnsignedInt(), "field2", null, 1, 1, TGenericPrivateObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTGenericPrivateObject_Field3(), theXMLTypePackage.getUnsignedInt(), "field3", null, 1, 1, TGenericPrivateObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTGenericPrivateObject_Field4(), theXMLTypePackage.getUnsignedInt(), "field4", null, 1, 1, TGenericPrivateObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTGenericPrivateObject_Field5(), theXMLTypePackage.getUnsignedInt(), "field5", null, 1, 1, TGenericPrivateObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);

    // Create annotations
    // http:///org/eclipse/emf/ecore/util/ExtendedMetaData
    createExtendedMetaDataAnnotations();
  }

  /**
   * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createExtendedMetaDataAnnotations() {
    String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";	
    addAnnotation
      (documentRootEClass, 
       source, 
       new String[] {
       "name", "",
       "kind", "mixed"
       });	
    addAnnotation
      (getDocumentRoot_Mixed(), 
       source, 
       new String[] {
       "kind", "elementWildcard",
       "name", ":mixed"
       });	
    addAnnotation
      (getDocumentRoot_XMLNSPrefixMap(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "xmlns:prefix"
       });	
    addAnnotation
      (getDocumentRoot_XSISchemaLocation(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "xsi:schemaLocation"
       });	
    addAnnotation
      (getDocumentRoot_GenericPrivateObject(), 
       source, 
       new String[] {
       "kind", "element",
       "name", "GenericPrivateObject",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (tGenericPrivateObjectEClass, 
       source, 
       new String[] {
       "name", "tGenericPrivateObject",
       "kind", "empty"
       });	
    addAnnotation
      (getTGenericPrivateObject_Field1(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "Field1"
       });	
    addAnnotation
      (getTGenericPrivateObject_Field2(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "Field2"
       });	
    addAnnotation
      (getTGenericPrivateObject_Field3(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "Field3"
       });	
    addAnnotation
      (getTGenericPrivateObject_Field4(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "Field4"
       });	
    addAnnotation
      (getTGenericPrivateObject_Field5(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "Field5"
       });
  }

} //SystemCorpPackageImpl
