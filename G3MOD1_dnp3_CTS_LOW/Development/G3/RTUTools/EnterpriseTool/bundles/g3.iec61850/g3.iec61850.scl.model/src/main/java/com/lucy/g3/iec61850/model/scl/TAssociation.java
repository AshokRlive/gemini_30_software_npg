/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TAssociation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAssociation#getAssociationID <em>Association ID</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAssociation#getDesc <em>Desc</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAssociation#getIedName <em>Ied Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAssociation#getKind <em>Kind</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAssociation#getLdInst <em>Ld Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAssociation#getLnClass <em>Ln Class</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAssociation#getLnInst <em>Ln Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAssociation#getPrefix <em>Prefix</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAssociation()
 * @model extendedMetaData="name='tAssociation' kind='empty'"
 * @extends SCLObject
 * @generated
 */
public interface TAssociation extends SCLObject {
  /**
   * Returns the value of the '<em><b>Association ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Association ID</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Association ID</em>' attribute.
   * @see #setAssociationID(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAssociation_AssociationID()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TAssociationID"
   *        extendedMetaData="kind='attribute' name='associationID'"
   * @generated
   */
  String getAssociationID();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getAssociationID <em>Association ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Association ID</em>' attribute.
   * @see #getAssociationID()
   * @generated
   */
  void setAssociationID(String value);

  /**
   * Returns the value of the '<em><b>Desc</b></em>' attribute.
   * The default value is <code>""</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Desc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Desc</em>' attribute.
   * @see #isSetDesc()
   * @see #unsetDesc()
   * @see #setDesc(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAssociation_Desc()
   * @model default="" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
   *        extendedMetaData="kind='attribute' name='desc'"
   * @generated
   */
  String getDesc();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getDesc <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Desc</em>' attribute.
   * @see #isSetDesc()
   * @see #unsetDesc()
   * @see #getDesc()
   * @generated
   */
  void setDesc(String value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getDesc <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetDesc()
   * @see #getDesc()
   * @see #setDesc(String)
   * @generated
   */
  void unsetDesc();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getDesc <em>Desc</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Desc</em>' attribute is set.
   * @see #unsetDesc()
   * @see #getDesc()
   * @see #setDesc(String)
   * @generated
   */
  boolean isSetDesc();

  /**
   * Returns the value of the '<em><b>Ied Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ied Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ied Name</em>' attribute.
   * @see #setIedName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAssociation_IedName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TIEDName" required="true"
   *        extendedMetaData="kind='attribute' name='iedName'"
   * @generated
   */
  String getIedName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getIedName <em>Ied Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ied Name</em>' attribute.
   * @see #getIedName()
   * @generated
   */
  void setIedName(String value);

  /**
   * Returns the value of the '<em><b>Kind</b></em>' attribute.
   * The literals are from the enumeration {@link com.lucy.g3.iec61850.model.scl.TAssociationKindEnum}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Kind</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Kind</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TAssociationKindEnum
   * @see #isSetKind()
   * @see #unsetKind()
   * @see #setKind(TAssociationKindEnum)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAssociation_Kind()
   * @model unsettable="true" required="true"
   *        extendedMetaData="kind='attribute' name='kind'"
   * @generated
   */
  TAssociationKindEnum getKind();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getKind <em>Kind</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Kind</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TAssociationKindEnum
   * @see #isSetKind()
   * @see #unsetKind()
   * @see #getKind()
   * @generated
   */
  void setKind(TAssociationKindEnum value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getKind <em>Kind</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetKind()
   * @see #getKind()
   * @see #setKind(TAssociationKindEnum)
   * @generated
   */
  void unsetKind();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getKind <em>Kind</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Kind</em>' attribute is set.
   * @see #unsetKind()
   * @see #getKind()
   * @see #setKind(TAssociationKindEnum)
   * @generated
   */
  boolean isSetKind();

  /**
   * Returns the value of the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ld Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ld Inst</em>' attribute.
   * @see #setLdInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAssociation_LdInst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLDInst" required="true"
   *        extendedMetaData="kind='attribute' name='ldInst'"
   * @generated
   */
  String getLdInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getLdInst <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ld Inst</em>' attribute.
   * @see #getLdInst()
   * @generated
   */
  void setLdInst(String value);

  /**
   * Returns the value of the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ln Class</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ln Class</em>' attribute.
   * @see #setLnClass(Object)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAssociation_LnClass()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLNClassEnum" required="true"
   *        extendedMetaData="kind='attribute' name='lnClass'"
   * @generated
   */
  Object getLnClass();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getLnClass <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ln Class</em>' attribute.
   * @see #getLnClass()
   * @generated
   */
  void setLnClass(Object value);

  /**
   * Returns the value of the '<em><b>Ln Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ln Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ln Inst</em>' attribute.
   * @see #setLnInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAssociation_LnInst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLNInstOrEmpty" required="true"
   *        extendedMetaData="kind='attribute' name='lnInst'"
   * @generated
   */
  String getLnInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getLnInst <em>Ln Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ln Inst</em>' attribute.
   * @see #getLnInst()
   * @generated
   */
  void setLnInst(String value);

  /**
   * Returns the value of the '<em><b>Prefix</b></em>' attribute.
   * The default value is <code>"Lucy_"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prefix</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prefix</em>' attribute.
   * @see #isSetPrefix()
   * @see #unsetPrefix()
   * @see #setPrefix(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAssociation_Prefix()
   * @model default="Lucy_" unsettable="true" dataType="com.lucy.g3.iec61850.model.scl.TPrefix"
   *        extendedMetaData="kind='attribute' name='prefix'"
   * @generated
   */
  String getPrefix();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getPrefix <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prefix</em>' attribute.
   * @see #isSetPrefix()
   * @see #unsetPrefix()
   * @see #getPrefix()
   * @generated
   */
  void setPrefix(String value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getPrefix <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetPrefix()
   * @see #getPrefix()
   * @see #setPrefix(String)
   * @generated
   */
  void unsetPrefix();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getPrefix <em>Prefix</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Prefix</em>' attribute is set.
   * @see #unsetPrefix()
   * @see #getPrefix()
   * @see #setPrefix(String)
   * @generated
   */
  boolean isSetPrefix();

} // TAssociation
