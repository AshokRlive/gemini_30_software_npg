/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPOSIAP Title</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPOSIAPTitle()
 * @model extendedMetaData="name='tP_OSI-AP-Title' kind='simple'"
 * @generated
 */
public interface TPOSIAPTitle extends TP {
} // TPOSIAPTitle
