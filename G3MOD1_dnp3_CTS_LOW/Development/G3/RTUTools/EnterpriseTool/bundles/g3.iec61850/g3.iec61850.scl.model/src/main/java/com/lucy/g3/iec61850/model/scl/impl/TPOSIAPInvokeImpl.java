/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TPOSIAPInvoke;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TPOSIAP Invoke</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TPOSIAPInvokeImpl extends TPImpl implements TPOSIAPInvoke {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TPOSIAPInvokeImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTPOSIAPInvoke();
  }

} //TPOSIAPInvokeImpl
