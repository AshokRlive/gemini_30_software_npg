/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TLN</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TLN#getInst <em>Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TLN#getLnClass <em>Ln Class</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TLN#getPrefix <em>Prefix</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLN()
 * @model extendedMetaData="name='tLN' kind='elementOnly'"
 * @generated
 */
public interface TLN extends TAnyLN {
  /**
   * Returns the value of the '<em><b>Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Inst</em>' attribute.
   * @see #setInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLN_Inst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLNInst" required="true"
   *        extendedMetaData="kind='attribute' name='inst'"
   * @generated
   */
  String getInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLN#getInst <em>Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Inst</em>' attribute.
   * @see #getInst()
   * @generated
   */
  void setInst(String value);

  /**
   * Returns the value of the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ln Class</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ln Class</em>' attribute.
   * @see #setLnClass(Object)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLN_LnClass()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLNClassEnum" required="true"
   *        extendedMetaData="kind='attribute' name='lnClass'"
   * @generated
   */
  Object getLnClass();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLN#getLnClass <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ln Class</em>' attribute.
   * @see #getLnClass()
   * @generated
   */
  void setLnClass(Object value);

  /**
   * Returns the value of the '<em><b>Prefix</b></em>' attribute.
   * The default value is <code>"Lucy_"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prefix</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prefix</em>' attribute.
   * @see #isSetPrefix()
   * @see #unsetPrefix()
   * @see #setPrefix(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLN_Prefix()
   * @model default="Lucy_" unsettable="true" dataType="com.lucy.g3.iec61850.model.scl.TPrefix"
   *        extendedMetaData="kind='attribute' name='prefix'"
   * @generated
   */
  String getPrefix();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLN#getPrefix <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prefix</em>' attribute.
   * @see #isSetPrefix()
   * @see #unsetPrefix()
   * @see #getPrefix()
   * @generated
   */
  void setPrefix(String value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLN#getPrefix <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetPrefix()
   * @see #getPrefix()
   * @see #setPrefix(String)
   * @generated
   */
  void unsetPrefix();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TLN#getPrefix <em>Prefix</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Prefix</em>' attribute is set.
   * @see #unsetPrefix()
   * @see #getPrefix()
   * @see #setPrefix(String)
   * @generated
   */
  boolean isSetPrefix();

} // TLN
