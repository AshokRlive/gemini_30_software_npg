/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPOSIAP Invoke</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPOSIAPInvoke()
 * @model extendedMetaData="name='tP_OSI-AP-Invoke' kind='simple'"
 * @generated
 */
public interface TPOSIAPInvoke extends TP {
} // TPOSIAPInvoke
