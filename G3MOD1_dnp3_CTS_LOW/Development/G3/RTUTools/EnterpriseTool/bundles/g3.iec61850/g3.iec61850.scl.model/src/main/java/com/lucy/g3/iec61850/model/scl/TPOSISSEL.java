/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPOSISSEL</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPOSISSEL()
 * @model extendedMetaData="name='tP_OSI-SSEL' kind='simple'"
 * @generated
 */
public interface TPOSISSEL extends TP {
} // TPOSISSEL
