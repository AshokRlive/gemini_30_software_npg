/**
 */
package com.lucy.g3.iec61850.model.g3ref.impl;

import com.lucy.g3.iec61850.model.g3ref.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class G3RefFactoryImpl extends EFactoryImpl implements G3RefFactory {
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static G3RefFactory init() {
    try {
      G3RefFactory theG3RefFactory = (G3RefFactory)EPackage.Registry.INSTANCE.getEFactory(G3RefPackage.eNS_URI);
      if (theG3RefFactory != null) {
        return theG3RefFactory;
      }
    }
    catch (Exception exception) {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new G3RefFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public G3RefFactoryImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass) {
    switch (eClass.getClassifierID()) {
      case G3RefPackage.DOCUMENT_ROOT: return (EObject)createDocumentRoot();
      case G3RefPackage.TG3_REF_OBJECT: return (EObject)createTG3RefObject();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DocumentRoot createDocumentRoot() {
    DocumentRootImpl documentRoot = new DocumentRootImpl();
    return documentRoot;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TG3RefObject createTG3RefObject() {
    TG3RefObjectImpl tg3RefObject = new TG3RefObjectImpl();
    return tg3RefObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public G3RefPackage getG3RefPackage() {
    return (G3RefPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static G3RefPackage getPackage() {
    return G3RefPackage.eINSTANCE;
  }

} //G3RefFactoryImpl
