/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPOSIAE Invoke</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPOSIAEInvoke()
 * @model extendedMetaData="name='tP_OSI-AE-Invoke' kind='simple'"
 * @generated
 */
public interface TPOSIAEInvoke extends TP {
} // TPOSIAEInvoke
