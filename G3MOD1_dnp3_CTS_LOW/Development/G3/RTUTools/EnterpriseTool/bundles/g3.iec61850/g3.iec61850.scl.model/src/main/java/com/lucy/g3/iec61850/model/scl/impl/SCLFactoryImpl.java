/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.*;

import org.eclipse.emf.common.util.Enumerator;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.Diagnostician;

import org.eclipse.emf.ecore.xml.type.XMLTypeFactory;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SCLFactoryImpl extends EFactoryImpl implements SCLFactory {
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static SCLFactory init() {
    try {
      SCLFactory theSCLFactory = (SCLFactory)EPackage.Registry.INSTANCE.getEFactory(SCLPackage.eNS_URI);
      if (theSCLFactory != null) {
        return theSCLFactory;
      }
    }
    catch (Exception exception) {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new SCLFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SCLFactoryImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass) {
    switch (eClass.getClassifierID()) {
      case SCLPackage.AUTHENTICATION_TYPE: return (EObject)createAuthenticationType();
      case SCLPackage.DOCUMENT_ROOT: return (EObject)createDocumentRoot();
      case SCLPackage.HISTORY_TYPE: return (EObject)createHistoryType();
      case SCLPackage.IED_NAME_TYPE: return (EObject)createIEDNameType();
      case SCLPackage.LN0_TYPE: return (EObject)createLN0Type();
      case SCLPackage.OPT_FIELDS_TYPE: return (EObject)createOptFieldsType();
      case SCLPackage.PROT_NS_TYPE: return (EObject)createProtNsType();
      case SCLPackage.SCL_TYPE: return (EObject)createSCLType();
      case SCLPackage.SETTING_GROUPS_TYPE: return (EObject)createSettingGroupsType();
      case SCLPackage.SMV_OPTS_TYPE: return (EObject)createSmvOptsType();
      case SCLPackage.TACCESS_CONTROL: return (EObject)createTAccessControl();
      case SCLPackage.TACCESS_POINT: return (EObject)createTAccessPoint();
      case SCLPackage.TADDRESS: return (EObject)createTAddress();
      case SCLPackage.TASSOCIATION: return (EObject)createTAssociation();
      case SCLPackage.TBAY: return (EObject)createTBay();
      case SCLPackage.TBDA: return (EObject)createTBDA();
      case SCLPackage.TBIT_RATE_IN_MB_PER_SEC: return (EObject)createTBitRateInMbPerSec();
      case SCLPackage.TCERT: return (EObject)createTCert();
      case SCLPackage.TCERTIFICATE: return (EObject)createTCertificate();
      case SCLPackage.TCLIENT_LN: return (EObject)createTClientLN();
      case SCLPackage.TCLIENT_SERVICES: return (EObject)createTClientServices();
      case SCLPackage.TCOMMUNICATION: return (EObject)createTCommunication();
      case SCLPackage.TCONDUCTING_EQUIPMENT: return (EObject)createTConductingEquipment();
      case SCLPackage.TCONF_LNS: return (EObject)createTConfLNs();
      case SCLPackage.TCONNECTED_AP: return (EObject)createTConnectedAP();
      case SCLPackage.TCONNECTIVITY_NODE: return (EObject)createTConnectivityNode();
      case SCLPackage.TCONTROL_WITH_IED_NAME: return (EObject)createTControlWithIEDName();
      case SCLPackage.TDA: return (EObject)createTDA();
      case SCLPackage.TDAI: return (EObject)createTDAI();
      case SCLPackage.TDATA_SET: return (EObject)createTDataSet();
      case SCLPackage.TDATA_TYPE_TEMPLATES: return (EObject)createTDataTypeTemplates();
      case SCLPackage.TDA_TYPE: return (EObject)createTDAType();
      case SCLPackage.TDO: return (EObject)createTDO();
      case SCLPackage.TDOI: return (EObject)createTDOI();
      case SCLPackage.TDO_TYPE: return (EObject)createTDOType();
      case SCLPackage.TDURATION_IN_MILLI_SEC: return (EObject)createTDurationInMilliSec();
      case SCLPackage.TDURATION_IN_SEC: return (EObject)createTDurationInSec();
      case SCLPackage.TENUM_TYPE: return (EObject)createTEnumType();
      case SCLPackage.TENUM_VAL: return (EObject)createTEnumVal();
      case SCLPackage.TEXT_REF: return (EObject)createTExtRef();
      case SCLPackage.TFCDA: return (EObject)createTFCDA();
      case SCLPackage.TFUNCTION: return (EObject)createTFunction();
      case SCLPackage.TGENERAL_EQUIPMENT: return (EObject)createTGeneralEquipment();
      case SCLPackage.TGSE: return (EObject)createTGSE();
      case SCLPackage.TGSE_CONTROL: return (EObject)createTGSEControl();
      case SCLPackage.TGSE_SETTINGS: return (EObject)createTGSESettings();
      case SCLPackage.THEADER: return (EObject)createTHeader();
      case SCLPackage.THITEM: return (EObject)createTHitem();
      case SCLPackage.TIED: return (EObject)createTIED();
      case SCLPackage.TINPUTS: return (EObject)createTInputs();
      case SCLPackage.TL_DEVICE: return (EObject)createTLDevice();
      case SCLPackage.TLN: return (EObject)createTLN();
      case SCLPackage.TLN0: return (EObject)createTLN0();
      case SCLPackage.TL_NODE: return (EObject)createTLNode();
      case SCLPackage.TL_NODE_TYPE: return (EObject)createTLNodeType();
      case SCLPackage.TLOG: return (EObject)createTLog();
      case SCLPackage.TLOG_CONTROL: return (EObject)createTLogControl();
      case SCLPackage.TLOG_SETTINGS: return (EObject)createTLogSettings();
      case SCLPackage.TP: return (EObject)createTP();
      case SCLPackage.TPAPPID: return (EObject)createTPAPPID();
      case SCLPackage.TPHYS_CONN: return (EObject)createTPhysConn();
      case SCLPackage.TPIP: return (EObject)createTPIP();
      case SCLPackage.TPIPGATEWAY: return (EObject)createTPIPGATEWAY();
      case SCLPackage.TPIPSUBNET: return (EObject)createTPIPSUBNET();
      case SCLPackage.TPMAC_ADDRESS: return (EObject)createTPMACAddress();
      case SCLPackage.TPMMS_PORT: return (EObject)createTPMMSPort();
      case SCLPackage.TPOSIAE_INVOKE: return (EObject)createTPOSIAEInvoke();
      case SCLPackage.TPOSIAE_QUALIFIER: return (EObject)createTPOSIAEQualifier();
      case SCLPackage.TPOSIAP_INVOKE: return (EObject)createTPOSIAPInvoke();
      case SCLPackage.TPOSIAP_TITLE: return (EObject)createTPOSIAPTitle();
      case SCLPackage.TPOSINSAP: return (EObject)createTPOSINSAP();
      case SCLPackage.TPOSIPSEL: return (EObject)createTPOSIPSEL();
      case SCLPackage.TPOSISSEL: return (EObject)createTPOSISSEL();
      case SCLPackage.TPOSITSEL: return (EObject)createTPOSITSEL();
      case SCLPackage.TPOWER_TRANSFORMER: return (EObject)createTPowerTransformer();
      case SCLPackage.TP_PHYS_CONN: return (EObject)createTPPhysConn();
      case SCLPackage.TPRIVATE: return (EObject)createTPrivate();
      case SCLPackage.TPSNTP_PORT: return (EObject)createTPSNTPPort();
      case SCLPackage.TPVLANID: return (EObject)createTPVLANID();
      case SCLPackage.TPVLANPRIORITY: return (EObject)createTPVLANPRIORITY();
      case SCLPackage.TREPORT_CONTROL: return (EObject)createTReportControl();
      case SCLPackage.TREPORT_SETTINGS: return (EObject)createTReportSettings();
      case SCLPackage.TRPT_ENABLED: return (EObject)createTRptEnabled();
      case SCLPackage.TSAMPLED_VALUE_CONTROL: return (EObject)createTSampledValueControl();
      case SCLPackage.TSDI: return (EObject)createTSDI();
      case SCLPackage.TSDO: return (EObject)createTSDO();
      case SCLPackage.TSERVER: return (EObject)createTServer();
      case SCLPackage.TSERVER_AT: return (EObject)createTServerAt();
      case SCLPackage.TSERVICE_CONF_REPORT_CONTROL: return (EObject)createTServiceConfReportControl();
      case SCLPackage.TSERVICE_FOR_CONF_DATA_SET: return (EObject)createTServiceForConfDataSet();
      case SCLPackage.TSERVICES: return (EObject)createTServices();
      case SCLPackage.TSERVICE_WITH_MAX: return (EObject)createTServiceWithMax();
      case SCLPackage.TSERVICE_WITH_MAX_AND_MAX_ATTRIBUTES: return (EObject)createTServiceWithMaxAndMaxAttributes();
      case SCLPackage.TSERVICE_WITH_MAX_AND_MODIFY: return (EObject)createTServiceWithMaxAndModify();
      case SCLPackage.TSERVICE_WITH_OPTIONAL_MAX: return (EObject)createTServiceWithOptionalMax();
      case SCLPackage.TSERVICE_YES_NO: return (EObject)createTServiceYesNo();
      case SCLPackage.TSETTING_CONTROL: return (EObject)createTSettingControl();
      case SCLPackage.TSMV: return (EObject)createTSMV();
      case SCLPackage.TSMV_SETTINGS: return (EObject)createTSMVSettings();
      case SCLPackage.TSUB_EQUIPMENT: return (EObject)createTSubEquipment();
      case SCLPackage.TSUB_FUNCTION: return (EObject)createTSubFunction();
      case SCLPackage.TSUB_NETWORK: return (EObject)createTSubNetwork();
      case SCLPackage.TSUBSTATION: return (EObject)createTSubstation();
      case SCLPackage.TTAP_CHANGER: return (EObject)createTTapChanger();
      case SCLPackage.TTERMINAL: return (EObject)createTTerminal();
      case SCLPackage.TTEXT: return (EObject)createTText();
      case SCLPackage.TTRANSFORMER_WINDING: return (EObject)createTTransformerWinding();
      case SCLPackage.TTRG_OPS: return (EObject)createTTrgOps();
      case SCLPackage.TVAL: return (EObject)createTVal();
      case SCLPackage.TVALUE_WITH_UNIT: return (EObject)createTValueWithUnit();
      case SCLPackage.TVOLTAGE: return (EObject)createTVoltage();
      case SCLPackage.TVOLTAGE_LEVEL: return (EObject)createTVoltageLevel();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue) {
    switch (eDataType.getClassifierID()) {
      case SCLPackage.BUF_MODE_TYPE:
        return createBufModeTypeFromString(eDataType, initialValue);
      case SCLPackage.NAME_STRUCTURE_TYPE:
        return createNameStructureTypeFromString(eDataType, initialValue);
      case SCLPackage.TASSOCIATION_KIND_ENUM:
        return createTAssociationKindEnumFromString(eDataType, initialValue);
      case SCLPackage.TAUTHENTICATION_ENUM:
        return createTAuthenticationEnumFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_AENUM:
        return createTDomainLNGroupAEnumFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_CENUM:
        return createTDomainLNGroupCEnumFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_GENUM:
        return createTDomainLNGroupGEnumFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_IENUM:
        return createTDomainLNGroupIEnumFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_MENUM:
        return createTDomainLNGroupMEnumFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_PENUM:
        return createTDomainLNGroupPEnumFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_RENUM:
        return createTDomainLNGroupREnumFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_SENUM:
        return createTDomainLNGroupSEnumFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_TENUM:
        return createTDomainLNGroupTEnumFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_XENUM:
        return createTDomainLNGroupXEnumFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_YENUM:
        return createTDomainLNGroupYEnumFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_ZENUM:
        return createTDomainLNGroupZEnumFromString(eDataType, initialValue);
      case SCLPackage.TFC_ENUM:
        return createTFCEnumFromString(eDataType, initialValue);
      case SCLPackage.TGSE_CONTROL_TYPE_ENUM:
        return createTGSEControlTypeEnumFromString(eDataType, initialValue);
      case SCLPackage.TLLN0_ENUM:
        return createTLLN0EnumFromString(eDataType, initialValue);
      case SCLPackage.TLPHD_ENUM:
        return createTLPHDEnumFromString(eDataType, initialValue);
      case SCLPackage.TPHASE_ENUM:
        return createTPhaseEnumFromString(eDataType, initialValue);
      case SCLPackage.TPOWER_TRANSFORMER_ENUM:
        return createTPowerTransformerEnumFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_ATTRIBUTE_NAME_ENUM:
        return createTPredefinedAttributeNameEnumFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_BASIC_TYPE_ENUM:
        return createTPredefinedBasicTypeEnumFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_CDC_ENUM:
        return createTPredefinedCDCEnumFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_COMMON_CONDUCTING_EQUIPMENT_ENUM:
        return createTPredefinedCommonConductingEquipmentEnumFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_GENERAL_EQUIPMENT_ENUM:
        return createTPredefinedGeneralEquipmentEnumFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_PHYS_CONN_TYPE_ENUM:
        return createTPredefinedPhysConnTypeEnumFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_PTYPE_ENUM:
        return createTPredefinedPTypeEnumFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_PTYPE_PHYS_CONN_ENUM:
        return createTPredefinedPTypePhysConnEnumFromString(eDataType, initialValue);
      case SCLPackage.TRIGHT_ENUM:
        return createTRightEnumFromString(eDataType, initialValue);
      case SCLPackage.TSERVICE_SETTINGS_ENUM:
        return createTServiceSettingsEnumFromString(eDataType, initialValue);
      case SCLPackage.TSERVICE_TYPE:
        return createTServiceTypeFromString(eDataType, initialValue);
      case SCLPackage.TSI_UNIT_ENUM:
        return createTSIUnitEnumFromString(eDataType, initialValue);
      case SCLPackage.TSMP_MOD:
        return createTSmpModFromString(eDataType, initialValue);
      case SCLPackage.TTRANSFORMER_WINDING_ENUM:
        return createTTransformerWindingEnumFromString(eDataType, initialValue);
      case SCLPackage.TUNIT_MULTIPLIER_ENUM:
        return createTUnitMultiplierEnumFromString(eDataType, initialValue);
      case SCLPackage.TVAL_KIND_ENUM:
        return createTValKindEnumFromString(eDataType, initialValue);
      case SCLPackage.ACT_SG_TYPE:
        return createActSGTypeFromString(eDataType, initialValue);
      case SCLPackage.ACT_SG_TYPE_OBJECT:
        return createActSGTypeObjectFromString(eDataType, initialValue);
      case SCLPackage.APP_ID_TYPE:
        return createAppIDTypeFromString(eDataType, initialValue);
      case SCLPackage.BUF_MODE_TYPE_OBJECT:
        return createBufModeTypeObjectFromString(eDataType, initialValue);
      case SCLPackage.COMMON_NAME_TYPE:
        return createCommonNameTypeFromString(eDataType, initialValue);
      case SCLPackage.ID_HIERARCHY_TYPE:
        return createIdHierarchyTypeFromString(eDataType, initialValue);
      case SCLPackage.NAME_LENGTH_TYPE:
        return createNameLengthTypeFromString(eDataType, initialValue);
      case SCLPackage.NAME_LENGTH_TYPE_OBJECT:
        return createNameLengthTypeObjectFromString(eDataType, initialValue);
      case SCLPackage.NAME_STRUCTURE_TYPE_OBJECT:
        return createNameStructureTypeObjectFromString(eDataType, initialValue);
      case SCLPackage.NUM_OF_SGS_TYPE:
        return createNumOfSGsTypeFromString(eDataType, initialValue);
      case SCLPackage.NUM_OF_SGS_TYPE_OBJECT:
        return createNumOfSGsTypeObjectFromString(eDataType, initialValue);
      case SCLPackage.SAMPLES_PER_SEC_TYPE:
        return createSamplesPerSecTypeFromString(eDataType, initialValue);
      case SCLPackage.SAMPLES_PER_SEC_TYPE_OBJECT:
        return createSamplesPerSecTypeObjectFromString(eDataType, initialValue);
      case SCLPackage.SEC_PER_SAMPLES_TYPE:
        return createSecPerSamplesTypeFromString(eDataType, initialValue);
      case SCLPackage.SEC_PER_SAMPLES_TYPE_OBJECT:
        return createSecPerSamplesTypeObjectFromString(eDataType, initialValue);
      case SCLPackage.SERIAL_NUMBER_TYPE:
        return createSerialNumberTypeFromString(eDataType, initialValue);
      case SCLPackage.SMP_RATE_TYPE:
        return createSmpRateTypeFromString(eDataType, initialValue);
      case SCLPackage.SMP_RATE_TYPE_OBJECT:
        return createSmpRateTypeObjectFromString(eDataType, initialValue);
      case SCLPackage.SMV_ID_TYPE:
        return createSmvIDTypeFromString(eDataType, initialValue);
      case SCLPackage.TACCESS_POINT_NAME:
        return createTAccessPointNameFromString(eDataType, initialValue);
      case SCLPackage.TACSI_NAME:
        return createTAcsiNameFromString(eDataType, initialValue);
      case SCLPackage.TANY_NAME:
        return createTAnyNameFromString(eDataType, initialValue);
      case SCLPackage.TASSOCIATION_ID:
        return createTAssociationIDFromString(eDataType, initialValue);
      case SCLPackage.TASSOCIATION_KIND_ENUM_OBJECT:
        return createTAssociationKindEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TATTRIBUTE_NAME_ENUM:
        return createTAttributeNameEnumFromString(eDataType, initialValue);
      case SCLPackage.TAUTHENTICATION_ENUM_OBJECT:
        return createTAuthenticationEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TBASIC_TYPE_ENUM:
        return createTBasicTypeEnumFromString(eDataType, initialValue);
      case SCLPackage.TCB_NAME:
        return createTCBNameFromString(eDataType, initialValue);
      case SCLPackage.TCDC_ENUM:
        return createTCDCEnumFromString(eDataType, initialValue);
      case SCLPackage.TCOMMON_CONDUCTING_EQUIPMENT_ENUM:
        return createTCommonConductingEquipmentEnumFromString(eDataType, initialValue);
      case SCLPackage.TDA_COUNT:
        return createTDACountFromString(eDataType, initialValue);
      case SCLPackage.TDATA_NAME:
        return createTDataNameFromString(eDataType, initialValue);
      case SCLPackage.TDATA_SET_NAME:
        return createTDataSetNameFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_ENUM:
        return createTDomainLNEnumFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_AENUM_OBJECT:
        return createTDomainLNGroupAEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_CENUM_OBJECT:
        return createTDomainLNGroupCEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_GENUM_OBJECT:
        return createTDomainLNGroupGEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_IENUM_OBJECT:
        return createTDomainLNGroupIEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_MENUM_OBJECT:
        return createTDomainLNGroupMEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_PENUM_OBJECT:
        return createTDomainLNGroupPEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_RENUM_OBJECT:
        return createTDomainLNGroupREnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_SENUM_OBJECT:
        return createTDomainLNGroupSEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_TENUM_OBJECT:
        return createTDomainLNGroupTEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_XENUM_OBJECT:
        return createTDomainLNGroupXEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_YENUM_OBJECT:
        return createTDomainLNGroupYEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TDOMAIN_LN_GROUP_ZENUM_OBJECT:
        return createTDomainLNGroupZEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TEMPTY:
        return createTEmptyFromString(eDataType, initialValue);
      case SCLPackage.TEXTENSION_ATTRIBUTE_NAME_ENUM:
        return createTExtensionAttributeNameEnumFromString(eDataType, initialValue);
      case SCLPackage.TEXTENSION_EQUIPMENT_ENUM:
        return createTExtensionEquipmentEnumFromString(eDataType, initialValue);
      case SCLPackage.TEXTENSION_GENERAL_EQUIPMENT_ENUM:
        return createTExtensionGeneralEquipmentEnumFromString(eDataType, initialValue);
      case SCLPackage.TEXTENSION_LN_CLASS_ENUM:
        return createTExtensionLNClassEnumFromString(eDataType, initialValue);
      case SCLPackage.TEXTENSION_PHYS_CONN_TYPE_ENUM:
        return createTExtensionPhysConnTypeEnumFromString(eDataType, initialValue);
      case SCLPackage.TEXTENSION_PTYPE_ENUM:
        return createTExtensionPTypeEnumFromString(eDataType, initialValue);
      case SCLPackage.TFC_ENUM_OBJECT:
        return createTFCEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TFULL_ATTRIBUTE_NAME:
        return createTFullAttributeNameFromString(eDataType, initialValue);
      case SCLPackage.TFULL_DO_NAME:
        return createTFullDONameFromString(eDataType, initialValue);
      case SCLPackage.TGENERAL_EQUIPMENT_ENUM:
        return createTGeneralEquipmentEnumFromString(eDataType, initialValue);
      case SCLPackage.TGSE_CONTROL_TYPE_ENUM_OBJECT:
        return createTGSEControlTypeEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TIED_NAME:
        return createTIEDNameFromString(eDataType, initialValue);
      case SCLPackage.TLD_INST:
        return createTLDInstFromString(eDataType, initialValue);
      case SCLPackage.TLD_INST_OR_EMPTY:
        return createTLDInstOrEmptyFromString(eDataType, initialValue);
      case SCLPackage.TLD_NAME:
        return createTLDNameFromString(eDataType, initialValue);
      case SCLPackage.TLLN0_ENUM_OBJECT:
        return createTLLN0EnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TLN_CLASS_ENUM:
        return createTLNClassEnumFromString(eDataType, initialValue);
      case SCLPackage.TLN_INST:
        return createTLNInstFromString(eDataType, initialValue);
      case SCLPackage.TLN_INST_OR_EMPTY:
        return createTLNInstOrEmptyFromString(eDataType, initialValue);
      case SCLPackage.TLOG_NAME:
        return createTLogNameFromString(eDataType, initialValue);
      case SCLPackage.TLPHD_ENUM_OBJECT:
        return createTLPHDEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TNAME:
        return createTNameFromString(eDataType, initialValue);
      case SCLPackage.TP_ADDR:
        return createTPAddrFromString(eDataType, initialValue);
      case SCLPackage.TPHASE_ENUM_OBJECT:
        return createTPhaseEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TPHYS_CONN_TYPE_ENUM:
        return createTPhysConnTypeEnumFromString(eDataType, initialValue);
      case SCLPackage.TPOWER_TRANSFORMER_ENUM_OBJECT:
        return createTPowerTransformerEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_ATTRIBUTE_NAME_ENUM_OBJECT:
        return createTPredefinedAttributeNameEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_BASIC_TYPE_ENUM_OBJECT:
        return createTPredefinedBasicTypeEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_CDC_ENUM_OBJECT:
        return createTPredefinedCDCEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_COMMON_CONDUCTING_EQUIPMENT_ENUM_OBJECT:
        return createTPredefinedCommonConductingEquipmentEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_GENERAL_EQUIPMENT_ENUM_OBJECT:
        return createTPredefinedGeneralEquipmentEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_LN_CLASS_ENUM:
        return createTPredefinedLNClassEnumFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_PHYS_CONN_TYPE_ENUM_OBJECT:
        return createTPredefinedPhysConnTypeEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_PTYPE_ENUM_OBJECT:
        return createTPredefinedPTypeEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TPREDEFINED_PTYPE_PHYS_CONN_ENUM_OBJECT:
        return createTPredefinedPTypePhysConnEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TPREFIX:
        return createTPrefixFromString(eDataType, initialValue);
      case SCLPackage.TP_TYPE_ENUM:
        return createTPTypeEnumFromString(eDataType, initialValue);
      case SCLPackage.TP_TYPE_PHYS_CONN_ENUM:
        return createTPTypePhysConnEnumFromString(eDataType, initialValue);
      case SCLPackage.TREF:
        return createTRefFromString(eDataType, initialValue);
      case SCLPackage.TRESTR_NAME1ST_L:
        return createTRestrName1stLFromString(eDataType, initialValue);
      case SCLPackage.TRESTR_NAME1ST_U:
        return createTRestrName1stUFromString(eDataType, initialValue);
      case SCLPackage.TRIGHT_ENUM_OBJECT:
        return createTRightEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TRPT_ID:
        return createTRptIDFromString(eDataType, initialValue);
      case SCLPackage.TSCL_REVISION:
        return createTSclRevisionFromString(eDataType, initialValue);
      case SCLPackage.TSCL_VERSION:
        return createTSclVersionFromString(eDataType, initialValue);
      case SCLPackage.TSDO_COUNT:
        return createTSDOCountFromString(eDataType, initialValue);
      case SCLPackage.TSERVICE_SETTINGS_ENUM_OBJECT:
        return createTServiceSettingsEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TSERVICE_TYPE_OBJECT:
        return createTServiceTypeObjectFromString(eDataType, initialValue);
      case SCLPackage.TSI_UNIT_ENUM_OBJECT:
        return createTSIUnitEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TSMP_MOD_OBJECT:
        return createTSmpModObjectFromString(eDataType, initialValue);
      case SCLPackage.TTRANSFORMER_WINDING_ENUM_OBJECT:
        return createTTransformerWindingEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TUNIT_MULTIPLIER_ENUM_OBJECT:
        return createTUnitMultiplierEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TVAL_KIND_ENUM_OBJECT:
        return createTValKindEnumObjectFromString(eDataType, initialValue);
      case SCLPackage.TYPE_TYPE:
        return createTypeTypeFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue) {
    switch (eDataType.getClassifierID()) {
      case SCLPackage.BUF_MODE_TYPE:
        return convertBufModeTypeToString(eDataType, instanceValue);
      case SCLPackage.NAME_STRUCTURE_TYPE:
        return convertNameStructureTypeToString(eDataType, instanceValue);
      case SCLPackage.TASSOCIATION_KIND_ENUM:
        return convertTAssociationKindEnumToString(eDataType, instanceValue);
      case SCLPackage.TAUTHENTICATION_ENUM:
        return convertTAuthenticationEnumToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_AENUM:
        return convertTDomainLNGroupAEnumToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_CENUM:
        return convertTDomainLNGroupCEnumToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_GENUM:
        return convertTDomainLNGroupGEnumToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_IENUM:
        return convertTDomainLNGroupIEnumToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_MENUM:
        return convertTDomainLNGroupMEnumToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_PENUM:
        return convertTDomainLNGroupPEnumToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_RENUM:
        return convertTDomainLNGroupREnumToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_SENUM:
        return convertTDomainLNGroupSEnumToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_TENUM:
        return convertTDomainLNGroupTEnumToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_XENUM:
        return convertTDomainLNGroupXEnumToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_YENUM:
        return convertTDomainLNGroupYEnumToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_ZENUM:
        return convertTDomainLNGroupZEnumToString(eDataType, instanceValue);
      case SCLPackage.TFC_ENUM:
        return convertTFCEnumToString(eDataType, instanceValue);
      case SCLPackage.TGSE_CONTROL_TYPE_ENUM:
        return convertTGSEControlTypeEnumToString(eDataType, instanceValue);
      case SCLPackage.TLLN0_ENUM:
        return convertTLLN0EnumToString(eDataType, instanceValue);
      case SCLPackage.TLPHD_ENUM:
        return convertTLPHDEnumToString(eDataType, instanceValue);
      case SCLPackage.TPHASE_ENUM:
        return convertTPhaseEnumToString(eDataType, instanceValue);
      case SCLPackage.TPOWER_TRANSFORMER_ENUM:
        return convertTPowerTransformerEnumToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_ATTRIBUTE_NAME_ENUM:
        return convertTPredefinedAttributeNameEnumToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_BASIC_TYPE_ENUM:
        return convertTPredefinedBasicTypeEnumToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_CDC_ENUM:
        return convertTPredefinedCDCEnumToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_COMMON_CONDUCTING_EQUIPMENT_ENUM:
        return convertTPredefinedCommonConductingEquipmentEnumToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_GENERAL_EQUIPMENT_ENUM:
        return convertTPredefinedGeneralEquipmentEnumToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_PHYS_CONN_TYPE_ENUM:
        return convertTPredefinedPhysConnTypeEnumToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_PTYPE_ENUM:
        return convertTPredefinedPTypeEnumToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_PTYPE_PHYS_CONN_ENUM:
        return convertTPredefinedPTypePhysConnEnumToString(eDataType, instanceValue);
      case SCLPackage.TRIGHT_ENUM:
        return convertTRightEnumToString(eDataType, instanceValue);
      case SCLPackage.TSERVICE_SETTINGS_ENUM:
        return convertTServiceSettingsEnumToString(eDataType, instanceValue);
      case SCLPackage.TSERVICE_TYPE:
        return convertTServiceTypeToString(eDataType, instanceValue);
      case SCLPackage.TSI_UNIT_ENUM:
        return convertTSIUnitEnumToString(eDataType, instanceValue);
      case SCLPackage.TSMP_MOD:
        return convertTSmpModToString(eDataType, instanceValue);
      case SCLPackage.TTRANSFORMER_WINDING_ENUM:
        return convertTTransformerWindingEnumToString(eDataType, instanceValue);
      case SCLPackage.TUNIT_MULTIPLIER_ENUM:
        return convertTUnitMultiplierEnumToString(eDataType, instanceValue);
      case SCLPackage.TVAL_KIND_ENUM:
        return convertTValKindEnumToString(eDataType, instanceValue);
      case SCLPackage.ACT_SG_TYPE:
        return convertActSGTypeToString(eDataType, instanceValue);
      case SCLPackage.ACT_SG_TYPE_OBJECT:
        return convertActSGTypeObjectToString(eDataType, instanceValue);
      case SCLPackage.APP_ID_TYPE:
        return convertAppIDTypeToString(eDataType, instanceValue);
      case SCLPackage.BUF_MODE_TYPE_OBJECT:
        return convertBufModeTypeObjectToString(eDataType, instanceValue);
      case SCLPackage.COMMON_NAME_TYPE:
        return convertCommonNameTypeToString(eDataType, instanceValue);
      case SCLPackage.ID_HIERARCHY_TYPE:
        return convertIdHierarchyTypeToString(eDataType, instanceValue);
      case SCLPackage.NAME_LENGTH_TYPE:
        return convertNameLengthTypeToString(eDataType, instanceValue);
      case SCLPackage.NAME_LENGTH_TYPE_OBJECT:
        return convertNameLengthTypeObjectToString(eDataType, instanceValue);
      case SCLPackage.NAME_STRUCTURE_TYPE_OBJECT:
        return convertNameStructureTypeObjectToString(eDataType, instanceValue);
      case SCLPackage.NUM_OF_SGS_TYPE:
        return convertNumOfSGsTypeToString(eDataType, instanceValue);
      case SCLPackage.NUM_OF_SGS_TYPE_OBJECT:
        return convertNumOfSGsTypeObjectToString(eDataType, instanceValue);
      case SCLPackage.SAMPLES_PER_SEC_TYPE:
        return convertSamplesPerSecTypeToString(eDataType, instanceValue);
      case SCLPackage.SAMPLES_PER_SEC_TYPE_OBJECT:
        return convertSamplesPerSecTypeObjectToString(eDataType, instanceValue);
      case SCLPackage.SEC_PER_SAMPLES_TYPE:
        return convertSecPerSamplesTypeToString(eDataType, instanceValue);
      case SCLPackage.SEC_PER_SAMPLES_TYPE_OBJECT:
        return convertSecPerSamplesTypeObjectToString(eDataType, instanceValue);
      case SCLPackage.SERIAL_NUMBER_TYPE:
        return convertSerialNumberTypeToString(eDataType, instanceValue);
      case SCLPackage.SMP_RATE_TYPE:
        return convertSmpRateTypeToString(eDataType, instanceValue);
      case SCLPackage.SMP_RATE_TYPE_OBJECT:
        return convertSmpRateTypeObjectToString(eDataType, instanceValue);
      case SCLPackage.SMV_ID_TYPE:
        return convertSmvIDTypeToString(eDataType, instanceValue);
      case SCLPackage.TACCESS_POINT_NAME:
        return convertTAccessPointNameToString(eDataType, instanceValue);
      case SCLPackage.TACSI_NAME:
        return convertTAcsiNameToString(eDataType, instanceValue);
      case SCLPackage.TANY_NAME:
        return convertTAnyNameToString(eDataType, instanceValue);
      case SCLPackage.TASSOCIATION_ID:
        return convertTAssociationIDToString(eDataType, instanceValue);
      case SCLPackage.TASSOCIATION_KIND_ENUM_OBJECT:
        return convertTAssociationKindEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TATTRIBUTE_NAME_ENUM:
        return convertTAttributeNameEnumToString(eDataType, instanceValue);
      case SCLPackage.TAUTHENTICATION_ENUM_OBJECT:
        return convertTAuthenticationEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TBASIC_TYPE_ENUM:
        return convertTBasicTypeEnumToString(eDataType, instanceValue);
      case SCLPackage.TCB_NAME:
        return convertTCBNameToString(eDataType, instanceValue);
      case SCLPackage.TCDC_ENUM:
        return convertTCDCEnumToString(eDataType, instanceValue);
      case SCLPackage.TCOMMON_CONDUCTING_EQUIPMENT_ENUM:
        return convertTCommonConductingEquipmentEnumToString(eDataType, instanceValue);
      case SCLPackage.TDA_COUNT:
        return convertTDACountToString(eDataType, instanceValue);
      case SCLPackage.TDATA_NAME:
        return convertTDataNameToString(eDataType, instanceValue);
      case SCLPackage.TDATA_SET_NAME:
        return convertTDataSetNameToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_ENUM:
        return convertTDomainLNEnumToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_AENUM_OBJECT:
        return convertTDomainLNGroupAEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_CENUM_OBJECT:
        return convertTDomainLNGroupCEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_GENUM_OBJECT:
        return convertTDomainLNGroupGEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_IENUM_OBJECT:
        return convertTDomainLNGroupIEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_MENUM_OBJECT:
        return convertTDomainLNGroupMEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_PENUM_OBJECT:
        return convertTDomainLNGroupPEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_RENUM_OBJECT:
        return convertTDomainLNGroupREnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_SENUM_OBJECT:
        return convertTDomainLNGroupSEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_TENUM_OBJECT:
        return convertTDomainLNGroupTEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_XENUM_OBJECT:
        return convertTDomainLNGroupXEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_YENUM_OBJECT:
        return convertTDomainLNGroupYEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TDOMAIN_LN_GROUP_ZENUM_OBJECT:
        return convertTDomainLNGroupZEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TEMPTY:
        return convertTEmptyToString(eDataType, instanceValue);
      case SCLPackage.TEXTENSION_ATTRIBUTE_NAME_ENUM:
        return convertTExtensionAttributeNameEnumToString(eDataType, instanceValue);
      case SCLPackage.TEXTENSION_EQUIPMENT_ENUM:
        return convertTExtensionEquipmentEnumToString(eDataType, instanceValue);
      case SCLPackage.TEXTENSION_GENERAL_EQUIPMENT_ENUM:
        return convertTExtensionGeneralEquipmentEnumToString(eDataType, instanceValue);
      case SCLPackage.TEXTENSION_LN_CLASS_ENUM:
        return convertTExtensionLNClassEnumToString(eDataType, instanceValue);
      case SCLPackage.TEXTENSION_PHYS_CONN_TYPE_ENUM:
        return convertTExtensionPhysConnTypeEnumToString(eDataType, instanceValue);
      case SCLPackage.TEXTENSION_PTYPE_ENUM:
        return convertTExtensionPTypeEnumToString(eDataType, instanceValue);
      case SCLPackage.TFC_ENUM_OBJECT:
        return convertTFCEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TFULL_ATTRIBUTE_NAME:
        return convertTFullAttributeNameToString(eDataType, instanceValue);
      case SCLPackage.TFULL_DO_NAME:
        return convertTFullDONameToString(eDataType, instanceValue);
      case SCLPackage.TGENERAL_EQUIPMENT_ENUM:
        return convertTGeneralEquipmentEnumToString(eDataType, instanceValue);
      case SCLPackage.TGSE_CONTROL_TYPE_ENUM_OBJECT:
        return convertTGSEControlTypeEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TIED_NAME:
        return convertTIEDNameToString(eDataType, instanceValue);
      case SCLPackage.TLD_INST:
        return convertTLDInstToString(eDataType, instanceValue);
      case SCLPackage.TLD_INST_OR_EMPTY:
        return convertTLDInstOrEmptyToString(eDataType, instanceValue);
      case SCLPackage.TLD_NAME:
        return convertTLDNameToString(eDataType, instanceValue);
      case SCLPackage.TLLN0_ENUM_OBJECT:
        return convertTLLN0EnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TLN_CLASS_ENUM:
        return convertTLNClassEnumToString(eDataType, instanceValue);
      case SCLPackage.TLN_INST:
        return convertTLNInstToString(eDataType, instanceValue);
      case SCLPackage.TLN_INST_OR_EMPTY:
        return convertTLNInstOrEmptyToString(eDataType, instanceValue);
      case SCLPackage.TLOG_NAME:
        return convertTLogNameToString(eDataType, instanceValue);
      case SCLPackage.TLPHD_ENUM_OBJECT:
        return convertTLPHDEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TNAME:
        return convertTNameToString(eDataType, instanceValue);
      case SCLPackage.TP_ADDR:
        return convertTPAddrToString(eDataType, instanceValue);
      case SCLPackage.TPHASE_ENUM_OBJECT:
        return convertTPhaseEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TPHYS_CONN_TYPE_ENUM:
        return convertTPhysConnTypeEnumToString(eDataType, instanceValue);
      case SCLPackage.TPOWER_TRANSFORMER_ENUM_OBJECT:
        return convertTPowerTransformerEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_ATTRIBUTE_NAME_ENUM_OBJECT:
        return convertTPredefinedAttributeNameEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_BASIC_TYPE_ENUM_OBJECT:
        return convertTPredefinedBasicTypeEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_CDC_ENUM_OBJECT:
        return convertTPredefinedCDCEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_COMMON_CONDUCTING_EQUIPMENT_ENUM_OBJECT:
        return convertTPredefinedCommonConductingEquipmentEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_GENERAL_EQUIPMENT_ENUM_OBJECT:
        return convertTPredefinedGeneralEquipmentEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_LN_CLASS_ENUM:
        return convertTPredefinedLNClassEnumToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_PHYS_CONN_TYPE_ENUM_OBJECT:
        return convertTPredefinedPhysConnTypeEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_PTYPE_ENUM_OBJECT:
        return convertTPredefinedPTypeEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TPREDEFINED_PTYPE_PHYS_CONN_ENUM_OBJECT:
        return convertTPredefinedPTypePhysConnEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TPREFIX:
        return convertTPrefixToString(eDataType, instanceValue);
      case SCLPackage.TP_TYPE_ENUM:
        return convertTPTypeEnumToString(eDataType, instanceValue);
      case SCLPackage.TP_TYPE_PHYS_CONN_ENUM:
        return convertTPTypePhysConnEnumToString(eDataType, instanceValue);
      case SCLPackage.TREF:
        return convertTRefToString(eDataType, instanceValue);
      case SCLPackage.TRESTR_NAME1ST_L:
        return convertTRestrName1stLToString(eDataType, instanceValue);
      case SCLPackage.TRESTR_NAME1ST_U:
        return convertTRestrName1stUToString(eDataType, instanceValue);
      case SCLPackage.TRIGHT_ENUM_OBJECT:
        return convertTRightEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TRPT_ID:
        return convertTRptIDToString(eDataType, instanceValue);
      case SCLPackage.TSCL_REVISION:
        return convertTSclRevisionToString(eDataType, instanceValue);
      case SCLPackage.TSCL_VERSION:
        return convertTSclVersionToString(eDataType, instanceValue);
      case SCLPackage.TSDO_COUNT:
        return convertTSDOCountToString(eDataType, instanceValue);
      case SCLPackage.TSERVICE_SETTINGS_ENUM_OBJECT:
        return convertTServiceSettingsEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TSERVICE_TYPE_OBJECT:
        return convertTServiceTypeObjectToString(eDataType, instanceValue);
      case SCLPackage.TSI_UNIT_ENUM_OBJECT:
        return convertTSIUnitEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TSMP_MOD_OBJECT:
        return convertTSmpModObjectToString(eDataType, instanceValue);
      case SCLPackage.TTRANSFORMER_WINDING_ENUM_OBJECT:
        return convertTTransformerWindingEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TUNIT_MULTIPLIER_ENUM_OBJECT:
        return convertTUnitMultiplierEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TVAL_KIND_ENUM_OBJECT:
        return convertTValKindEnumObjectToString(eDataType, instanceValue);
      case SCLPackage.TYPE_TYPE:
        return convertTypeTypeToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AuthenticationType createAuthenticationType() {
    AuthenticationTypeImpl authenticationType = new AuthenticationTypeImpl();
    return authenticationType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DocumentRoot createDocumentRoot() {
    DocumentRootImpl documentRoot = new DocumentRootImpl();
    return documentRoot;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HistoryType createHistoryType() {
    HistoryTypeImpl historyType = new HistoryTypeImpl();
    return historyType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IEDNameType createIEDNameType() {
    IEDNameTypeImpl iedNameType = new IEDNameTypeImpl();
    return iedNameType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LN0Type createLN0Type() {
    LN0TypeImpl ln0Type = new LN0TypeImpl();
    return ln0Type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OptFieldsType createOptFieldsType() {
    OptFieldsTypeImpl optFieldsType = new OptFieldsTypeImpl();
    return optFieldsType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProtNsType createProtNsType() {
    ProtNsTypeImpl protNsType = new ProtNsTypeImpl();
    return protNsType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SCLType createSCLType() {
    SCLTypeImpl sclType = new SCLTypeImpl();
    return sclType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SettingGroupsType createSettingGroupsType() {
    SettingGroupsTypeImpl settingGroupsType = new SettingGroupsTypeImpl();
    return settingGroupsType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SmvOptsType createSmvOptsType() {
    SmvOptsTypeImpl smvOptsType = new SmvOptsTypeImpl();
    return smvOptsType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TAccessControl createTAccessControl() {
    TAccessControlImpl tAccessControl = new TAccessControlImpl();
    return tAccessControl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TAccessPoint createTAccessPoint() {
    TAccessPointImpl tAccessPoint = new TAccessPointImpl();
    return tAccessPoint;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TAddress createTAddress() {
    TAddressImpl tAddress = new TAddressImpl();
    return tAddress;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TAssociation createTAssociation() {
    TAssociationImpl tAssociation = new TAssociationImpl();
    return tAssociation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TBay createTBay() {
    TBayImpl tBay = new TBayImpl();
    return tBay;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TBDA createTBDA() {
    TBDAImpl tbda = new TBDAImpl();
    return tbda;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TBitRateInMbPerSec createTBitRateInMbPerSec() {
    TBitRateInMbPerSecImpl tBitRateInMbPerSec = new TBitRateInMbPerSecImpl();
    return tBitRateInMbPerSec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TCert createTCert() {
    TCertImpl tCert = new TCertImpl();
    return tCert;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TCertificate createTCertificate() {
    TCertificateImpl tCertificate = new TCertificateImpl();
    return tCertificate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TClientLN createTClientLN() {
    TClientLNImpl tClientLN = new TClientLNImpl();
    return tClientLN;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TClientServices createTClientServices() {
    TClientServicesImpl tClientServices = new TClientServicesImpl();
    return tClientServices;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TCommunication createTCommunication() {
    TCommunicationImpl tCommunication = new TCommunicationImpl();
    return tCommunication;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TConductingEquipment createTConductingEquipment() {
    TConductingEquipmentImpl tConductingEquipment = new TConductingEquipmentImpl();
    return tConductingEquipment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TConfLNs createTConfLNs() {
    TConfLNsImpl tConfLNs = new TConfLNsImpl();
    return tConfLNs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TConnectedAP createTConnectedAP() {
    TConnectedAPImpl tConnectedAP = new TConnectedAPImpl();
    return tConnectedAP;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TConnectivityNode createTConnectivityNode() {
    TConnectivityNodeImpl tConnectivityNode = new TConnectivityNodeImpl();
    return tConnectivityNode;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TControlWithIEDName createTControlWithIEDName() {
    TControlWithIEDNameImpl tControlWithIEDName = new TControlWithIEDNameImpl();
    return tControlWithIEDName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDA createTDA() {
    TDAImpl tda = new TDAImpl();
    return tda;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDAI createTDAI() {
    TDAIImpl tdai = new TDAIImpl();
    return tdai;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDataSet createTDataSet() {
    TDataSetImpl tDataSet = new TDataSetImpl();
    return tDataSet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDataTypeTemplates createTDataTypeTemplates() {
    TDataTypeTemplatesImpl tDataTypeTemplates = new TDataTypeTemplatesImpl();
    return tDataTypeTemplates;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDAType createTDAType() {
    TDATypeImpl tdaType = new TDATypeImpl();
    return tdaType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDO createTDO() {
    TDOImpl tdo = new TDOImpl();
    return tdo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDOI createTDOI() {
    TDOIImpl tdoi = new TDOIImpl();
    return tdoi;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDOType createTDOType() {
    TDOTypeImpl tdoType = new TDOTypeImpl();
    return tdoType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDurationInMilliSec createTDurationInMilliSec() {
    TDurationInMilliSecImpl tDurationInMilliSec = new TDurationInMilliSecImpl();
    return tDurationInMilliSec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDurationInSec createTDurationInSec() {
    TDurationInSecImpl tDurationInSec = new TDurationInSecImpl();
    return tDurationInSec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TEnumType createTEnumType() {
    TEnumTypeImpl tEnumType = new TEnumTypeImpl();
    return tEnumType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TEnumVal createTEnumVal() {
    TEnumValImpl tEnumVal = new TEnumValImpl();
    return tEnumVal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TExtRef createTExtRef() {
    TExtRefImpl tExtRef = new TExtRefImpl();
    return tExtRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TFCDA createTFCDA() {
    TFCDAImpl tfcda = new TFCDAImpl();
    return tfcda;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TFunction createTFunction() {
    TFunctionImpl tFunction = new TFunctionImpl();
    return tFunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TGeneralEquipment createTGeneralEquipment() {
    TGeneralEquipmentImpl tGeneralEquipment = new TGeneralEquipmentImpl();
    return tGeneralEquipment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TGSE createTGSE() {
    TGSEImpl tgse = new TGSEImpl();
    return tgse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TGSEControl createTGSEControl() {
    TGSEControlImpl tgseControl = new TGSEControlImpl();
    return tgseControl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TGSESettings createTGSESettings() {
    TGSESettingsImpl tgseSettings = new TGSESettingsImpl();
    return tgseSettings;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public THeader createTHeader() {
    THeaderImpl tHeader = new THeaderImpl();
    return tHeader;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public THitem createTHitem() {
    THitemImpl tHitem = new THitemImpl();
    return tHitem;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TIED createTIED() {
    TIEDImpl tied = new TIEDImpl();
    return tied;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TInputs createTInputs() {
    TInputsImpl tInputs = new TInputsImpl();
    return tInputs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TLDevice createTLDevice() {
    TLDeviceImpl tlDevice = new TLDeviceImpl();
    return tlDevice;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TLN createTLN() {
    TLNImpl tln = new TLNImpl();
    return tln;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TLN0 createTLN0() {
    TLN0Impl tln0 = new TLN0Impl();
    return tln0;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TLNode createTLNode() {
    TLNodeImpl tlNode = new TLNodeImpl();
    return tlNode;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TLNodeType createTLNodeType() {
    TLNodeTypeImpl tlNodeType = new TLNodeTypeImpl();
    return tlNodeType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TLog createTLog() {
    TLogImpl tLog = new TLogImpl();
    return tLog;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TLogControl createTLogControl() {
    TLogControlImpl tLogControl = new TLogControlImpl();
    return tLogControl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TLogSettings createTLogSettings() {
    TLogSettingsImpl tLogSettings = new TLogSettingsImpl();
    return tLogSettings;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TP createTP() {
    TPImpl tp = new TPImpl();
    return tp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPAPPID createTPAPPID() {
    TPAPPIDImpl tpappid = new TPAPPIDImpl();
    return tpappid;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPhysConn createTPhysConn() {
    TPhysConnImpl tPhysConn = new TPhysConnImpl();
    return tPhysConn;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPIP createTPIP() {
    TPIPImpl tpip = new TPIPImpl();
    return tpip;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPIPGATEWAY createTPIPGATEWAY() {
    TPIPGATEWAYImpl tpipgateway = new TPIPGATEWAYImpl();
    return tpipgateway;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPIPSUBNET createTPIPSUBNET() {
    TPIPSUBNETImpl tpipsubnet = new TPIPSUBNETImpl();
    return tpipsubnet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPMACAddress createTPMACAddress() {
    TPMACAddressImpl tpmacAddress = new TPMACAddressImpl();
    return tpmacAddress;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPMMSPort createTPMMSPort() {
    TPMMSPortImpl tpmmsPort = new TPMMSPortImpl();
    return tpmmsPort;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPOSIAEInvoke createTPOSIAEInvoke() {
    TPOSIAEInvokeImpl tposiaeInvoke = new TPOSIAEInvokeImpl();
    return tposiaeInvoke;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPOSIAEQualifier createTPOSIAEQualifier() {
    TPOSIAEQualifierImpl tposiaeQualifier = new TPOSIAEQualifierImpl();
    return tposiaeQualifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPOSIAPInvoke createTPOSIAPInvoke() {
    TPOSIAPInvokeImpl tposiapInvoke = new TPOSIAPInvokeImpl();
    return tposiapInvoke;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPOSIAPTitle createTPOSIAPTitle() {
    TPOSIAPTitleImpl tposiapTitle = new TPOSIAPTitleImpl();
    return tposiapTitle;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPOSINSAP createTPOSINSAP() {
    TPOSINSAPImpl tposinsap = new TPOSINSAPImpl();
    return tposinsap;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPOSIPSEL createTPOSIPSEL() {
    TPOSIPSELImpl tposipsel = new TPOSIPSELImpl();
    return tposipsel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPOSISSEL createTPOSISSEL() {
    TPOSISSELImpl tposissel = new TPOSISSELImpl();
    return tposissel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPOSITSEL createTPOSITSEL() {
    TPOSITSELImpl tpositsel = new TPOSITSELImpl();
    return tpositsel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPowerTransformer createTPowerTransformer() {
    TPowerTransformerImpl tPowerTransformer = new TPowerTransformerImpl();
    return tPowerTransformer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPPhysConn createTPPhysConn() {
    TPPhysConnImpl tpPhysConn = new TPPhysConnImpl();
    return tpPhysConn;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPrivate createTPrivate() {
    TPrivateImpl tPrivate = new TPrivateImpl();
    return tPrivate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPSNTPPort createTPSNTPPort() {
    TPSNTPPortImpl tpsntpPort = new TPSNTPPortImpl();
    return tpsntpPort;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPVLANID createTPVLANID() {
    TPVLANIDImpl tpvlanid = new TPVLANIDImpl();
    return tpvlanid;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPVLANPRIORITY createTPVLANPRIORITY() {
    TPVLANPRIORITYImpl tpvlanpriority = new TPVLANPRIORITYImpl();
    return tpvlanpriority;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TReportControl createTReportControl() {
    TReportControlImpl tReportControl = new TReportControlImpl();
    return tReportControl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TReportSettings createTReportSettings() {
    TReportSettingsImpl tReportSettings = new TReportSettingsImpl();
    return tReportSettings;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TRptEnabled createTRptEnabled() {
    TRptEnabledImpl tRptEnabled = new TRptEnabledImpl();
    return tRptEnabled;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSampledValueControl createTSampledValueControl() {
    TSampledValueControlImpl tSampledValueControl = new TSampledValueControlImpl();
    return tSampledValueControl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSDI createTSDI() {
    TSDIImpl tsdi = new TSDIImpl();
    return tsdi;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSDO createTSDO() {
    TSDOImpl tsdo = new TSDOImpl();
    return tsdo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServer createTServer() {
    TServerImpl tServer = new TServerImpl();
    return tServer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServerAt createTServerAt() {
    TServerAtImpl tServerAt = new TServerAtImpl();
    return tServerAt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceConfReportControl createTServiceConfReportControl() {
    TServiceConfReportControlImpl tServiceConfReportControl = new TServiceConfReportControlImpl();
    return tServiceConfReportControl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceForConfDataSet createTServiceForConfDataSet() {
    TServiceForConfDataSetImpl tServiceForConfDataSet = new TServiceForConfDataSetImpl();
    return tServiceForConfDataSet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServices createTServices() {
    TServicesImpl tServices = new TServicesImpl();
    return tServices;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceWithMax createTServiceWithMax() {
    TServiceWithMaxImpl tServiceWithMax = new TServiceWithMaxImpl();
    return tServiceWithMax;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceWithMaxAndMaxAttributes createTServiceWithMaxAndMaxAttributes() {
    TServiceWithMaxAndMaxAttributesImpl tServiceWithMaxAndMaxAttributes = new TServiceWithMaxAndMaxAttributesImpl();
    return tServiceWithMaxAndMaxAttributes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceWithMaxAndModify createTServiceWithMaxAndModify() {
    TServiceWithMaxAndModifyImpl tServiceWithMaxAndModify = new TServiceWithMaxAndModifyImpl();
    return tServiceWithMaxAndModify;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceWithOptionalMax createTServiceWithOptionalMax() {
    TServiceWithOptionalMaxImpl tServiceWithOptionalMax = new TServiceWithOptionalMaxImpl();
    return tServiceWithOptionalMax;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceYesNo createTServiceYesNo() {
    TServiceYesNoImpl tServiceYesNo = new TServiceYesNoImpl();
    return tServiceYesNo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSettingControl createTSettingControl() {
    TSettingControlImpl tSettingControl = new TSettingControlImpl();
    return tSettingControl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSMV createTSMV() {
    TSMVImpl tsmv = new TSMVImpl();
    return tsmv;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSMVSettings createTSMVSettings() {
    TSMVSettingsImpl tsmvSettings = new TSMVSettingsImpl();
    return tsmvSettings;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSubEquipment createTSubEquipment() {
    TSubEquipmentImpl tSubEquipment = new TSubEquipmentImpl();
    return tSubEquipment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSubFunction createTSubFunction() {
    TSubFunctionImpl tSubFunction = new TSubFunctionImpl();
    return tSubFunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSubNetwork createTSubNetwork() {
    TSubNetworkImpl tSubNetwork = new TSubNetworkImpl();
    return tSubNetwork;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSubstation createTSubstation() {
    TSubstationImpl tSubstation = new TSubstationImpl();
    return tSubstation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTapChanger createTTapChanger() {
    TTapChangerImpl tTapChanger = new TTapChangerImpl();
    return tTapChanger;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTerminal createTTerminal() {
    TTerminalImpl tTerminal = new TTerminalImpl();
    return tTerminal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TText createTText() {
    TTextImpl tText = new TTextImpl();
    return tText;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTransformerWinding createTTransformerWinding() {
    TTransformerWindingImpl tTransformerWinding = new TTransformerWindingImpl();
    return tTransformerWinding;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTrgOps createTTrgOps() {
    TTrgOpsImpl tTrgOps = new TTrgOpsImpl();
    return tTrgOps;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TVal createTVal() {
    TValImpl tVal = new TValImpl();
    return tVal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TValueWithUnit createTValueWithUnit() {
    TValueWithUnitImpl tValueWithUnit = new TValueWithUnitImpl();
    return tValueWithUnit;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TVoltage createTVoltage() {
    TVoltageImpl tVoltage = new TVoltageImpl();
    return tVoltage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TVoltageLevel createTVoltageLevel() {
    TVoltageLevelImpl tVoltageLevel = new TVoltageLevelImpl();
    return tVoltageLevel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BufModeType createBufModeTypeFromString(EDataType eDataType, String initialValue) {
    BufModeType result = BufModeType.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertBufModeTypeToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NameStructureType createNameStructureTypeFromString(EDataType eDataType, String initialValue) {
    NameStructureType result = NameStructureType.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertNameStructureTypeToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TAssociationKindEnum createTAssociationKindEnumFromString(EDataType eDataType, String initialValue) {
    TAssociationKindEnum result = TAssociationKindEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTAssociationKindEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TAuthenticationEnum createTAuthenticationEnumFromString(EDataType eDataType, String initialValue) {
    TAuthenticationEnum result = TAuthenticationEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTAuthenticationEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupAEnum createTDomainLNGroupAEnumFromString(EDataType eDataType, String initialValue) {
    TDomainLNGroupAEnum result = TDomainLNGroupAEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupAEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupCEnum createTDomainLNGroupCEnumFromString(EDataType eDataType, String initialValue) {
    TDomainLNGroupCEnum result = TDomainLNGroupCEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupCEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupGEnum createTDomainLNGroupGEnumFromString(EDataType eDataType, String initialValue) {
    TDomainLNGroupGEnum result = TDomainLNGroupGEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupGEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupIEnum createTDomainLNGroupIEnumFromString(EDataType eDataType, String initialValue) {
    TDomainLNGroupIEnum result = TDomainLNGroupIEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupIEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupMEnum createTDomainLNGroupMEnumFromString(EDataType eDataType, String initialValue) {
    TDomainLNGroupMEnum result = TDomainLNGroupMEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupMEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupPEnum createTDomainLNGroupPEnumFromString(EDataType eDataType, String initialValue) {
    TDomainLNGroupPEnum result = TDomainLNGroupPEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupPEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupREnum createTDomainLNGroupREnumFromString(EDataType eDataType, String initialValue) {
    TDomainLNGroupREnum result = TDomainLNGroupREnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupREnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupSEnum createTDomainLNGroupSEnumFromString(EDataType eDataType, String initialValue) {
    TDomainLNGroupSEnum result = TDomainLNGroupSEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupSEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupTEnum createTDomainLNGroupTEnumFromString(EDataType eDataType, String initialValue) {
    TDomainLNGroupTEnum result = TDomainLNGroupTEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupTEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupXEnum createTDomainLNGroupXEnumFromString(EDataType eDataType, String initialValue) {
    TDomainLNGroupXEnum result = TDomainLNGroupXEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupXEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupYEnum createTDomainLNGroupYEnumFromString(EDataType eDataType, String initialValue) {
    TDomainLNGroupYEnum result = TDomainLNGroupYEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupYEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupZEnum createTDomainLNGroupZEnumFromString(EDataType eDataType, String initialValue) {
    TDomainLNGroupZEnum result = TDomainLNGroupZEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupZEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TFCEnum createTFCEnumFromString(EDataType eDataType, String initialValue) {
    TFCEnum result = TFCEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTFCEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TGSEControlTypeEnum createTGSEControlTypeEnumFromString(EDataType eDataType, String initialValue) {
    TGSEControlTypeEnum result = TGSEControlTypeEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTGSEControlTypeEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TLLN0Enum createTLLN0EnumFromString(EDataType eDataType, String initialValue) {
    TLLN0Enum result = TLLN0Enum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTLLN0EnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TLPHDEnum createTLPHDEnumFromString(EDataType eDataType, String initialValue) {
    TLPHDEnum result = TLPHDEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTLPHDEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPhaseEnum createTPhaseEnumFromString(EDataType eDataType, String initialValue) {
    TPhaseEnum result = TPhaseEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPhaseEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPowerTransformerEnum createTPowerTransformerEnumFromString(EDataType eDataType, String initialValue) {
    TPowerTransformerEnum result = TPowerTransformerEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPowerTransformerEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedAttributeNameEnum createTPredefinedAttributeNameEnumFromString(EDataType eDataType, String initialValue) {
    TPredefinedAttributeNameEnum result = TPredefinedAttributeNameEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedAttributeNameEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedBasicTypeEnum createTPredefinedBasicTypeEnumFromString(EDataType eDataType, String initialValue) {
    TPredefinedBasicTypeEnum result = TPredefinedBasicTypeEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedBasicTypeEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedCDCEnum createTPredefinedCDCEnumFromString(EDataType eDataType, String initialValue) {
    TPredefinedCDCEnum result = TPredefinedCDCEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedCDCEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedCommonConductingEquipmentEnum createTPredefinedCommonConductingEquipmentEnumFromString(EDataType eDataType, String initialValue) {
    TPredefinedCommonConductingEquipmentEnum result = TPredefinedCommonConductingEquipmentEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedCommonConductingEquipmentEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedGeneralEquipmentEnum createTPredefinedGeneralEquipmentEnumFromString(EDataType eDataType, String initialValue) {
    TPredefinedGeneralEquipmentEnum result = TPredefinedGeneralEquipmentEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedGeneralEquipmentEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedPhysConnTypeEnum createTPredefinedPhysConnTypeEnumFromString(EDataType eDataType, String initialValue) {
    TPredefinedPhysConnTypeEnum result = TPredefinedPhysConnTypeEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedPhysConnTypeEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedPTypeEnum createTPredefinedPTypeEnumFromString(EDataType eDataType, String initialValue) {
    TPredefinedPTypeEnum result = TPredefinedPTypeEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedPTypeEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedPTypePhysConnEnum createTPredefinedPTypePhysConnEnumFromString(EDataType eDataType, String initialValue) {
    TPredefinedPTypePhysConnEnum result = TPredefinedPTypePhysConnEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedPTypePhysConnEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TRightEnum createTRightEnumFromString(EDataType eDataType, String initialValue) {
    TRightEnum result = TRightEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTRightEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceSettingsEnum createTServiceSettingsEnumFromString(EDataType eDataType, String initialValue) {
    TServiceSettingsEnum result = TServiceSettingsEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTServiceSettingsEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceType createTServiceTypeFromString(EDataType eDataType, String initialValue) {
    TServiceType result = TServiceType.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTServiceTypeToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSIUnitEnum createTSIUnitEnumFromString(EDataType eDataType, String initialValue) {
    TSIUnitEnum result = TSIUnitEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTSIUnitEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSmpMod createTSmpModFromString(EDataType eDataType, String initialValue) {
    TSmpMod result = TSmpMod.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTSmpModToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTransformerWindingEnum createTTransformerWindingEnumFromString(EDataType eDataType, String initialValue) {
    TTransformerWindingEnum result = TTransformerWindingEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTTransformerWindingEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TUnitMultiplierEnum createTUnitMultiplierEnumFromString(EDataType eDataType, String initialValue) {
    TUnitMultiplierEnum result = TUnitMultiplierEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTUnitMultiplierEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TValKindEnum createTValKindEnumFromString(EDataType eDataType, String initialValue) {
    TValKindEnum result = TValKindEnum.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTValKindEnumToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Long createActSGTypeFromString(EDataType eDataType, String initialValue) {
    return (Long)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.UNSIGNED_INT, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertActSGTypeToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.UNSIGNED_INT, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Long createActSGTypeObjectFromString(EDataType eDataType, String initialValue) {
    return createActSGTypeFromString(SCLPackage.eINSTANCE.getActSGType(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertActSGTypeObjectToString(EDataType eDataType, Object instanceValue) {
    return convertActSGTypeToString(SCLPackage.eINSTANCE.getActSGType(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createAppIDTypeFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertAppIDTypeToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BufModeType createBufModeTypeObjectFromString(EDataType eDataType, String initialValue) {
    return createBufModeTypeFromString(SCLPackage.eINSTANCE.getBufModeType(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertBufModeTypeObjectToString(EDataType eDataType, Object instanceValue) {
    return convertBufModeTypeToString(SCLPackage.eINSTANCE.getBufModeType(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createCommonNameTypeFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertCommonNameTypeToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createIdHierarchyTypeFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertIdHierarchyTypeToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Long createNameLengthTypeFromString(EDataType eDataType, String initialValue) {
    return (Long)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.UNSIGNED_INT, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertNameLengthTypeToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.UNSIGNED_INT, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Long createNameLengthTypeObjectFromString(EDataType eDataType, String initialValue) {
    return createNameLengthTypeFromString(SCLPackage.eINSTANCE.getNameLengthType(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertNameLengthTypeObjectToString(EDataType eDataType, Object instanceValue) {
    return convertNameLengthTypeToString(SCLPackage.eINSTANCE.getNameLengthType(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NameStructureType createNameStructureTypeObjectFromString(EDataType eDataType, String initialValue) {
    return createNameStructureTypeFromString(SCLPackage.eINSTANCE.getNameStructureType(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertNameStructureTypeObjectToString(EDataType eDataType, Object instanceValue) {
    return convertNameStructureTypeToString(SCLPackage.eINSTANCE.getNameStructureType(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Long createNumOfSGsTypeFromString(EDataType eDataType, String initialValue) {
    return (Long)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.UNSIGNED_INT, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertNumOfSGsTypeToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.UNSIGNED_INT, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Long createNumOfSGsTypeObjectFromString(EDataType eDataType, String initialValue) {
    return createNumOfSGsTypeFromString(SCLPackage.eINSTANCE.getNumOfSGsType(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertNumOfSGsTypeObjectToString(EDataType eDataType, Object instanceValue) {
    return convertNumOfSGsTypeToString(SCLPackage.eINSTANCE.getNumOfSGsType(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Long createSamplesPerSecTypeFromString(EDataType eDataType, String initialValue) {
    return (Long)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.UNSIGNED_INT, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertSamplesPerSecTypeToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.UNSIGNED_INT, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Long createSamplesPerSecTypeObjectFromString(EDataType eDataType, String initialValue) {
    return createSamplesPerSecTypeFromString(SCLPackage.eINSTANCE.getSamplesPerSecType(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertSamplesPerSecTypeObjectToString(EDataType eDataType, Object instanceValue) {
    return convertSamplesPerSecTypeToString(SCLPackage.eINSTANCE.getSamplesPerSecType(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Long createSecPerSamplesTypeFromString(EDataType eDataType, String initialValue) {
    return (Long)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.UNSIGNED_INT, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertSecPerSamplesTypeToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.UNSIGNED_INT, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Long createSecPerSamplesTypeObjectFromString(EDataType eDataType, String initialValue) {
    return createSecPerSamplesTypeFromString(SCLPackage.eINSTANCE.getSecPerSamplesType(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertSecPerSamplesTypeObjectToString(EDataType eDataType, Object instanceValue) {
    return convertSecPerSamplesTypeToString(SCLPackage.eINSTANCE.getSecPerSamplesType(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createSerialNumberTypeFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertSerialNumberTypeToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Long createSmpRateTypeFromString(EDataType eDataType, String initialValue) {
    return (Long)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.UNSIGNED_INT, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertSmpRateTypeToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.UNSIGNED_INT, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Long createSmpRateTypeObjectFromString(EDataType eDataType, String initialValue) {
    return createSmpRateTypeFromString(SCLPackage.eINSTANCE.getSmpRateType(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertSmpRateTypeObjectToString(EDataType eDataType, Object instanceValue) {
    return convertSmpRateTypeToString(SCLPackage.eINSTANCE.getSmpRateType(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createSmvIDTypeFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertSmvIDTypeToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTAccessPointNameFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTAccessPointNameToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTAcsiNameFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NAME, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTAcsiNameToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NAME, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTAnyNameFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTAnyNameToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTAssociationIDFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTAssociationIDToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TAssociationKindEnum createTAssociationKindEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTAssociationKindEnumFromString(SCLPackage.eINSTANCE.getTAssociationKindEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTAssociationKindEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTAssociationKindEnumToString(SCLPackage.eINSTANCE.getTAssociationKindEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object createTAttributeNameEnumFromString(EDataType eDataType, String initialValue) {
    if (initialValue == null) return null;
    Object result = null;
    RuntimeException exception = null;
    try {
      result = createTPredefinedAttributeNameEnumFromString(SCLPackage.eINSTANCE.getTPredefinedAttributeNameEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = createTExtensionAttributeNameEnumFromString(SCLPackage.eINSTANCE.getTExtensionAttributeNameEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    if (result != null || exception == null) return result;
    
    throw exception;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTAttributeNameEnumToString(EDataType eDataType, Object instanceValue) {
    if (instanceValue == null) return null;
    if (SCLPackage.eINSTANCE.getTPredefinedAttributeNameEnum().isInstance(instanceValue)) {
      try {
        String value = convertTPredefinedAttributeNameEnumToString(SCLPackage.eINSTANCE.getTPredefinedAttributeNameEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTExtensionAttributeNameEnum().isInstance(instanceValue)) {
      try {
        String value = convertTExtensionAttributeNameEnumToString(SCLPackage.eINSTANCE.getTExtensionAttributeNameEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    throw new IllegalArgumentException("Invalid value: '"+instanceValue+"' for datatype :"+eDataType.getName());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TAuthenticationEnum createTAuthenticationEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTAuthenticationEnumFromString(SCLPackage.eINSTANCE.getTAuthenticationEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTAuthenticationEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTAuthenticationEnumToString(SCLPackage.eINSTANCE.getTAuthenticationEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedBasicTypeEnum createTBasicTypeEnumFromString(EDataType eDataType, String initialValue) {
    return createTPredefinedBasicTypeEnumFromString(SCLPackage.eINSTANCE.getTPredefinedBasicTypeEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTBasicTypeEnumToString(EDataType eDataType, Object instanceValue) {
    return convertTPredefinedBasicTypeEnumToString(SCLPackage.eINSTANCE.getTPredefinedBasicTypeEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTCBNameFromString(EDataType eDataType, String initialValue) {
    return createTAcsiNameFromString(SCLPackage.eINSTANCE.getTAcsiName(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTCBNameToString(EDataType eDataType, Object instanceValue) {
    return convertTAcsiNameToString(SCLPackage.eINSTANCE.getTAcsiName(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedCDCEnum createTCDCEnumFromString(EDataType eDataType, String initialValue) {
    return createTPredefinedCDCEnumFromString(SCLPackage.eINSTANCE.getTPredefinedCDCEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTCDCEnumToString(EDataType eDataType, Object instanceValue) {
    return convertTPredefinedCDCEnumToString(SCLPackage.eINSTANCE.getTPredefinedCDCEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object createTCommonConductingEquipmentEnumFromString(EDataType eDataType, String initialValue) {
    if (initialValue == null) return null;
    Object result = null;
    RuntimeException exception = null;
    try {
      result = createTPredefinedCommonConductingEquipmentEnumFromString(SCLPackage.eINSTANCE.getTPredefinedCommonConductingEquipmentEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = createTExtensionEquipmentEnumFromString(SCLPackage.eINSTANCE.getTExtensionEquipmentEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    if (result != null || exception == null) return result;
    
    throw exception;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTCommonConductingEquipmentEnumToString(EDataType eDataType, Object instanceValue) {
    if (instanceValue == null) return null;
    if (SCLPackage.eINSTANCE.getTPredefinedCommonConductingEquipmentEnum().isInstance(instanceValue)) {
      try {
        String value = convertTPredefinedCommonConductingEquipmentEnumToString(SCLPackage.eINSTANCE.getTPredefinedCommonConductingEquipmentEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTExtensionEquipmentEnum().isInstance(instanceValue)) {
      try {
        String value = convertTExtensionEquipmentEnumToString(SCLPackage.eINSTANCE.getTExtensionEquipmentEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    throw new IllegalArgumentException("Invalid value: '"+instanceValue+"' for datatype :"+eDataType.getName());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object createTDACountFromString(EDataType eDataType, String initialValue) {
    if (initialValue == null) return null;
    Object result = null;
    RuntimeException exception = null;
    try {
      result = XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.UNSIGNED_INT, initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = createTAttributeNameEnumFromString(SCLPackage.eINSTANCE.getTAttributeNameEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    if (result != null || exception == null) return result;
    
    throw exception;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDACountToString(EDataType eDataType, Object instanceValue) {
    if (instanceValue == null) return null;
    if (XMLTypePackage.Literals.UNSIGNED_INT.isInstance(instanceValue)) {
      try {
        String value = XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.UNSIGNED_INT, instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTAttributeNameEnum().isInstance(instanceValue)) {
      try {
        String value = convertTAttributeNameEnumToString(SCLPackage.eINSTANCE.getTAttributeNameEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    throw new IllegalArgumentException("Invalid value: '"+instanceValue+"' for datatype :"+eDataType.getName());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTDataNameFromString(EDataType eDataType, String initialValue) {
    return createTRestrName1stUFromString(SCLPackage.eINSTANCE.getTRestrName1stU(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDataNameToString(EDataType eDataType, Object instanceValue) {
    return convertTRestrName1stUToString(SCLPackage.eINSTANCE.getTRestrName1stU(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTDataSetNameFromString(EDataType eDataType, String initialValue) {
    return createTAcsiNameFromString(SCLPackage.eINSTANCE.getTAcsiName(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDataSetNameToString(EDataType eDataType, Object instanceValue) {
    return convertTAcsiNameToString(SCLPackage.eINSTANCE.getTAcsiName(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Enumerator createTDomainLNEnumFromString(EDataType eDataType, String initialValue) {
    if (initialValue == null) return null;
    Enumerator result = null;
    RuntimeException exception = null;
    try {
      result = (Enumerator)createTDomainLNGroupAEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupAEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = (Enumerator)createTDomainLNGroupCEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupCEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = (Enumerator)createTDomainLNGroupGEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupGEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = (Enumerator)createTDomainLNGroupIEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupIEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = (Enumerator)createTDomainLNGroupMEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupMEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = (Enumerator)createTDomainLNGroupPEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupPEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = (Enumerator)createTDomainLNGroupREnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupREnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = (Enumerator)createTDomainLNGroupSEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupSEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = (Enumerator)createTDomainLNGroupTEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupTEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = (Enumerator)createTDomainLNGroupXEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupXEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = (Enumerator)createTDomainLNGroupYEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupYEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = (Enumerator)createTDomainLNGroupZEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupZEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    if (result != null || exception == null) return result;
    
    throw exception;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNEnumToString(EDataType eDataType, Object instanceValue) {
    if (instanceValue == null) return null;
    if (SCLPackage.eINSTANCE.getTDomainLNGroupAEnum().isInstance(instanceValue)) {
      try {
        String value = convertTDomainLNGroupAEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupAEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTDomainLNGroupCEnum().isInstance(instanceValue)) {
      try {
        String value = convertTDomainLNGroupCEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupCEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTDomainLNGroupGEnum().isInstance(instanceValue)) {
      try {
        String value = convertTDomainLNGroupGEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupGEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTDomainLNGroupIEnum().isInstance(instanceValue)) {
      try {
        String value = convertTDomainLNGroupIEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupIEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTDomainLNGroupMEnum().isInstance(instanceValue)) {
      try {
        String value = convertTDomainLNGroupMEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupMEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTDomainLNGroupPEnum().isInstance(instanceValue)) {
      try {
        String value = convertTDomainLNGroupPEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupPEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTDomainLNGroupREnum().isInstance(instanceValue)) {
      try {
        String value = convertTDomainLNGroupREnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupREnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTDomainLNGroupSEnum().isInstance(instanceValue)) {
      try {
        String value = convertTDomainLNGroupSEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupSEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTDomainLNGroupTEnum().isInstance(instanceValue)) {
      try {
        String value = convertTDomainLNGroupTEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupTEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTDomainLNGroupXEnum().isInstance(instanceValue)) {
      try {
        String value = convertTDomainLNGroupXEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupXEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTDomainLNGroupYEnum().isInstance(instanceValue)) {
      try {
        String value = convertTDomainLNGroupYEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupYEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTDomainLNGroupZEnum().isInstance(instanceValue)) {
      try {
        String value = convertTDomainLNGroupZEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupZEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    throw new IllegalArgumentException("Invalid value: '"+instanceValue+"' for datatype :"+eDataType.getName());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupAEnum createTDomainLNGroupAEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTDomainLNGroupAEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupAEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupAEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTDomainLNGroupAEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupAEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupCEnum createTDomainLNGroupCEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTDomainLNGroupCEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupCEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupCEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTDomainLNGroupCEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupCEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupGEnum createTDomainLNGroupGEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTDomainLNGroupGEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupGEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupGEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTDomainLNGroupGEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupGEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupIEnum createTDomainLNGroupIEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTDomainLNGroupIEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupIEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupIEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTDomainLNGroupIEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupIEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupMEnum createTDomainLNGroupMEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTDomainLNGroupMEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupMEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupMEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTDomainLNGroupMEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupMEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupPEnum createTDomainLNGroupPEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTDomainLNGroupPEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupPEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupPEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTDomainLNGroupPEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupPEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupREnum createTDomainLNGroupREnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTDomainLNGroupREnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupREnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupREnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTDomainLNGroupREnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupREnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupSEnum createTDomainLNGroupSEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTDomainLNGroupSEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupSEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupSEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTDomainLNGroupSEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupSEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupTEnum createTDomainLNGroupTEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTDomainLNGroupTEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupTEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupTEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTDomainLNGroupTEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupTEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupXEnum createTDomainLNGroupXEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTDomainLNGroupXEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupXEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupXEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTDomainLNGroupXEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupXEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupYEnum createTDomainLNGroupYEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTDomainLNGroupYEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupYEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupYEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTDomainLNGroupYEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupYEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDomainLNGroupZEnum createTDomainLNGroupZEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTDomainLNGroupZEnumFromString(SCLPackage.eINSTANCE.getTDomainLNGroupZEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTDomainLNGroupZEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTDomainLNGroupZEnumToString(SCLPackage.eINSTANCE.getTDomainLNGroupZEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTEmptyFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTEmptyToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTExtensionAttributeNameEnumFromString(EDataType eDataType, String initialValue) {
    return createTRestrName1stLFromString(SCLPackage.eINSTANCE.getTRestrName1stL(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTExtensionAttributeNameEnumToString(EDataType eDataType, Object instanceValue) {
    return convertTRestrName1stLToString(SCLPackage.eINSTANCE.getTRestrName1stL(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTExtensionEquipmentEnumFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NAME, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTExtensionEquipmentEnumToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NAME, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTExtensionGeneralEquipmentEnumFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NAME, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTExtensionGeneralEquipmentEnumToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NAME, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTExtensionLNClassEnumFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NAME, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTExtensionLNClassEnumToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NAME, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTExtensionPhysConnTypeEnumFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTExtensionPhysConnTypeEnumToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTExtensionPTypeEnumFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTExtensionPTypeEnumToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TFCEnum createTFCEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTFCEnumFromString(SCLPackage.eINSTANCE.getTFCEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTFCEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTFCEnumToString(SCLPackage.eINSTANCE.getTFCEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTFullAttributeNameFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTFullAttributeNameToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTFullDONameFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTFullDONameToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object createTGeneralEquipmentEnumFromString(EDataType eDataType, String initialValue) {
    if (initialValue == null) return null;
    Object result = null;
    RuntimeException exception = null;
    try {
      result = createTPredefinedGeneralEquipmentEnumFromString(SCLPackage.eINSTANCE.getTPredefinedGeneralEquipmentEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = createTExtensionGeneralEquipmentEnumFromString(SCLPackage.eINSTANCE.getTExtensionGeneralEquipmentEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    if (result != null || exception == null) return result;
    
    throw exception;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTGeneralEquipmentEnumToString(EDataType eDataType, Object instanceValue) {
    if (instanceValue == null) return null;
    if (SCLPackage.eINSTANCE.getTPredefinedGeneralEquipmentEnum().isInstance(instanceValue)) {
      try {
        String value = convertTPredefinedGeneralEquipmentEnumToString(SCLPackage.eINSTANCE.getTPredefinedGeneralEquipmentEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTExtensionGeneralEquipmentEnum().isInstance(instanceValue)) {
      try {
        String value = convertTExtensionGeneralEquipmentEnumToString(SCLPackage.eINSTANCE.getTExtensionGeneralEquipmentEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    throw new IllegalArgumentException("Invalid value: '"+instanceValue+"' for datatype :"+eDataType.getName());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TGSEControlTypeEnum createTGSEControlTypeEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTGSEControlTypeEnumFromString(SCLPackage.eINSTANCE.getTGSEControlTypeEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTGSEControlTypeEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTGSEControlTypeEnumToString(SCLPackage.eINSTANCE.getTGSEControlTypeEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTIEDNameFromString(EDataType eDataType, String initialValue) {
    return createTAcsiNameFromString(SCLPackage.eINSTANCE.getTAcsiName(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTIEDNameToString(EDataType eDataType, Object instanceValue) {
    return convertTAcsiNameToString(SCLPackage.eINSTANCE.getTAcsiName(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTLDInstFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTLDInstToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTLDInstOrEmptyFromString(EDataType eDataType, String initialValue) {
    if (initialValue == null) return null;
    String result = null;
    RuntimeException exception = null;
    try {
      result = createTLDInstFromString(SCLPackage.eINSTANCE.getTLDInst(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = createTEmptyFromString(SCLPackage.eINSTANCE.getTEmpty(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    if (result != null || exception == null) return result;
    
    throw exception;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTLDInstOrEmptyToString(EDataType eDataType, Object instanceValue) {
    if (instanceValue == null) return null;
    if (SCLPackage.eINSTANCE.getTLDInst().isInstance(instanceValue)) {
      try {
        String value = convertTLDInstToString(SCLPackage.eINSTANCE.getTLDInst(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTEmpty().isInstance(instanceValue)) {
      try {
        String value = convertTEmptyToString(SCLPackage.eINSTANCE.getTEmpty(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    throw new IllegalArgumentException("Invalid value: '"+instanceValue+"' for datatype :"+eDataType.getName());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTLDNameFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTLDNameToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TLLN0Enum createTLLN0EnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTLLN0EnumFromString(SCLPackage.eINSTANCE.getTLLN0Enum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTLLN0EnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTLLN0EnumToString(SCLPackage.eINSTANCE.getTLLN0Enum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object createTLNClassEnumFromString(EDataType eDataType, String initialValue) {
    if (initialValue == null) return null;
    Object result = null;
    RuntimeException exception = null;
    try {
      result = createTPredefinedLNClassEnumFromString(SCLPackage.eINSTANCE.getTPredefinedLNClassEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = createTExtensionLNClassEnumFromString(SCLPackage.eINSTANCE.getTExtensionLNClassEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    if (result != null || exception == null) return result;
    
    throw exception;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTLNClassEnumToString(EDataType eDataType, Object instanceValue) {
    if (instanceValue == null) return null;
    if (SCLPackage.eINSTANCE.getTPredefinedLNClassEnum().isInstance(instanceValue)) {
      try {
        String value = convertTPredefinedLNClassEnumToString(SCLPackage.eINSTANCE.getTPredefinedLNClassEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTExtensionLNClassEnum().isInstance(instanceValue)) {
      try {
        String value = convertTExtensionLNClassEnumToString(SCLPackage.eINSTANCE.getTExtensionLNClassEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    throw new IllegalArgumentException("Invalid value: '"+instanceValue+"' for datatype :"+eDataType.getName());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTLNInstFromString(EDataType eDataType, String initialValue) {
    return createTLNInstOrEmptyFromString(SCLPackage.eINSTANCE.getTLNInstOrEmpty(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTLNInstToString(EDataType eDataType, Object instanceValue) {
    return convertTLNInstOrEmptyToString(SCLPackage.eINSTANCE.getTLNInstOrEmpty(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTLNInstOrEmptyFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTLNInstOrEmptyToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTLogNameFromString(EDataType eDataType, String initialValue) {
    return createTAcsiNameFromString(SCLPackage.eINSTANCE.getTAcsiName(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTLogNameToString(EDataType eDataType, Object instanceValue) {
    return convertTAcsiNameToString(SCLPackage.eINSTANCE.getTAcsiName(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TLPHDEnum createTLPHDEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTLPHDEnumFromString(SCLPackage.eINSTANCE.getTLPHDEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTLPHDEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTLPHDEnumToString(SCLPackage.eINSTANCE.getTLPHDEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTNameFromString(EDataType eDataType, String initialValue) {
    return createTAnyNameFromString(SCLPackage.eINSTANCE.getTAnyName(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTNameToString(EDataType eDataType, Object instanceValue) {
    return convertTAnyNameToString(SCLPackage.eINSTANCE.getTAnyName(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTPAddrFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPAddrToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPhaseEnum createTPhaseEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTPhaseEnumFromString(SCLPackage.eINSTANCE.getTPhaseEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPhaseEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTPhaseEnumToString(SCLPackage.eINSTANCE.getTPhaseEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object createTPhysConnTypeEnumFromString(EDataType eDataType, String initialValue) {
    if (initialValue == null) return null;
    Object result = null;
    RuntimeException exception = null;
    try {
      result = createTPredefinedPhysConnTypeEnumFromString(SCLPackage.eINSTANCE.getTPredefinedPhysConnTypeEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = createTExtensionPhysConnTypeEnumFromString(SCLPackage.eINSTANCE.getTExtensionPhysConnTypeEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    if (result != null || exception == null) return result;
    
    throw exception;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPhysConnTypeEnumToString(EDataType eDataType, Object instanceValue) {
    if (instanceValue == null) return null;
    if (SCLPackage.eINSTANCE.getTPredefinedPhysConnTypeEnum().isInstance(instanceValue)) {
      try {
        String value = convertTPredefinedPhysConnTypeEnumToString(SCLPackage.eINSTANCE.getTPredefinedPhysConnTypeEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTExtensionPhysConnTypeEnum().isInstance(instanceValue)) {
      try {
        String value = convertTExtensionPhysConnTypeEnumToString(SCLPackage.eINSTANCE.getTExtensionPhysConnTypeEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    throw new IllegalArgumentException("Invalid value: '"+instanceValue+"' for datatype :"+eDataType.getName());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPowerTransformerEnum createTPowerTransformerEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTPowerTransformerEnumFromString(SCLPackage.eINSTANCE.getTPowerTransformerEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPowerTransformerEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTPowerTransformerEnumToString(SCLPackage.eINSTANCE.getTPowerTransformerEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedAttributeNameEnum createTPredefinedAttributeNameEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTPredefinedAttributeNameEnumFromString(SCLPackage.eINSTANCE.getTPredefinedAttributeNameEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedAttributeNameEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTPredefinedAttributeNameEnumToString(SCLPackage.eINSTANCE.getTPredefinedAttributeNameEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedBasicTypeEnum createTPredefinedBasicTypeEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTPredefinedBasicTypeEnumFromString(SCLPackage.eINSTANCE.getTPredefinedBasicTypeEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedBasicTypeEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTPredefinedBasicTypeEnumToString(SCLPackage.eINSTANCE.getTPredefinedBasicTypeEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedCDCEnum createTPredefinedCDCEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTPredefinedCDCEnumFromString(SCLPackage.eINSTANCE.getTPredefinedCDCEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedCDCEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTPredefinedCDCEnumToString(SCLPackage.eINSTANCE.getTPredefinedCDCEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedCommonConductingEquipmentEnum createTPredefinedCommonConductingEquipmentEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTPredefinedCommonConductingEquipmentEnumFromString(SCLPackage.eINSTANCE.getTPredefinedCommonConductingEquipmentEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedCommonConductingEquipmentEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTPredefinedCommonConductingEquipmentEnumToString(SCLPackage.eINSTANCE.getTPredefinedCommonConductingEquipmentEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedGeneralEquipmentEnum createTPredefinedGeneralEquipmentEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTPredefinedGeneralEquipmentEnumFromString(SCLPackage.eINSTANCE.getTPredefinedGeneralEquipmentEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedGeneralEquipmentEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTPredefinedGeneralEquipmentEnumToString(SCLPackage.eINSTANCE.getTPredefinedGeneralEquipmentEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Enumerator createTPredefinedLNClassEnumFromString(EDataType eDataType, String initialValue) {
    if (initialValue == null) return null;
    Enumerator result = null;
    RuntimeException exception = null;
    try {
      result = (Enumerator)createTLPHDEnumFromString(SCLPackage.eINSTANCE.getTLPHDEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = (Enumerator)createTLLN0EnumFromString(SCLPackage.eINSTANCE.getTLLN0Enum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = createTDomainLNEnumFromString(SCLPackage.eINSTANCE.getTDomainLNEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    if (result != null || exception == null) return result;
    
    throw exception;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedLNClassEnumToString(EDataType eDataType, Object instanceValue) {
    if (instanceValue == null) return null;
    if (SCLPackage.eINSTANCE.getTLPHDEnum().isInstance(instanceValue)) {
      try {
        String value = convertTLPHDEnumToString(SCLPackage.eINSTANCE.getTLPHDEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTLLN0Enum().isInstance(instanceValue)) {
      try {
        String value = convertTLLN0EnumToString(SCLPackage.eINSTANCE.getTLLN0Enum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTDomainLNEnum().isInstance(instanceValue)) {
      try {
        String value = convertTDomainLNEnumToString(SCLPackage.eINSTANCE.getTDomainLNEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    throw new IllegalArgumentException("Invalid value: '"+instanceValue+"' for datatype :"+eDataType.getName());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedPhysConnTypeEnum createTPredefinedPhysConnTypeEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTPredefinedPhysConnTypeEnumFromString(SCLPackage.eINSTANCE.getTPredefinedPhysConnTypeEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedPhysConnTypeEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTPredefinedPhysConnTypeEnumToString(SCLPackage.eINSTANCE.getTPredefinedPhysConnTypeEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedPTypeEnum createTPredefinedPTypeEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTPredefinedPTypeEnumFromString(SCLPackage.eINSTANCE.getTPredefinedPTypeEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedPTypeEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTPredefinedPTypeEnumToString(SCLPackage.eINSTANCE.getTPredefinedPTypeEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TPredefinedPTypePhysConnEnum createTPredefinedPTypePhysConnEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTPredefinedPTypePhysConnEnumFromString(SCLPackage.eINSTANCE.getTPredefinedPTypePhysConnEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPredefinedPTypePhysConnEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTPredefinedPTypePhysConnEnumToString(SCLPackage.eINSTANCE.getTPredefinedPTypePhysConnEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTPrefixFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPrefixToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object createTPTypeEnumFromString(EDataType eDataType, String initialValue) {
    if (initialValue == null) return null;
    Object result = null;
    RuntimeException exception = null;
    try {
      result = createTPredefinedPTypeEnumFromString(SCLPackage.eINSTANCE.getTPredefinedPTypeEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = createTExtensionPTypeEnumFromString(SCLPackage.eINSTANCE.getTExtensionPTypeEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    if (result != null || exception == null) return result;
    
    throw exception;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPTypeEnumToString(EDataType eDataType, Object instanceValue) {
    if (instanceValue == null) return null;
    if (SCLPackage.eINSTANCE.getTPredefinedPTypeEnum().isInstance(instanceValue)) {
      try {
        String value = convertTPredefinedPTypeEnumToString(SCLPackage.eINSTANCE.getTPredefinedPTypeEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTExtensionPTypeEnum().isInstance(instanceValue)) {
      try {
        String value = convertTExtensionPTypeEnumToString(SCLPackage.eINSTANCE.getTExtensionPTypeEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    throw new IllegalArgumentException("Invalid value: '"+instanceValue+"' for datatype :"+eDataType.getName());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object createTPTypePhysConnEnumFromString(EDataType eDataType, String initialValue) {
    if (initialValue == null) return null;
    Object result = null;
    RuntimeException exception = null;
    try {
      result = createTPredefinedPTypePhysConnEnumFromString(SCLPackage.eINSTANCE.getTPredefinedPTypePhysConnEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = createTExtensionPTypeEnumFromString(SCLPackage.eINSTANCE.getTExtensionPTypeEnum(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    if (result != null || exception == null) return result;
    
    throw exception;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTPTypePhysConnEnumToString(EDataType eDataType, Object instanceValue) {
    if (instanceValue == null) return null;
    if (SCLPackage.eINSTANCE.getTPredefinedPTypePhysConnEnum().isInstance(instanceValue)) {
      try {
        String value = convertTPredefinedPTypePhysConnEnumToString(SCLPackage.eINSTANCE.getTPredefinedPTypePhysConnEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTExtensionPTypeEnum().isInstance(instanceValue)) {
      try {
        String value = convertTExtensionPTypeEnumToString(SCLPackage.eINSTANCE.getTExtensionPTypeEnum(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    throw new IllegalArgumentException("Invalid value: '"+instanceValue+"' for datatype :"+eDataType.getName());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTRefFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTRefToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTRestrName1stLFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NAME, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTRestrName1stLToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NAME, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTRestrName1stUFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NAME, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTRestrName1stUToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NAME, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TRightEnum createTRightEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTRightEnumFromString(SCLPackage.eINSTANCE.getTRightEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTRightEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTRightEnumToString(SCLPackage.eINSTANCE.getTRightEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTRptIDFromString(EDataType eDataType, String initialValue) {
    return createTNameFromString(SCLPackage.eINSTANCE.getTName(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTRptIDToString(EDataType eDataType, Object instanceValue) {
    return convertTNameToString(SCLPackage.eINSTANCE.getTName(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTSclRevisionFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NAME, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTSclRevisionToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NAME, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTSclVersionFromString(EDataType eDataType, String initialValue) {
    return createTNameFromString(SCLPackage.eINSTANCE.getTName(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTSclVersionToString(EDataType eDataType, Object instanceValue) {
    return convertTNameToString(SCLPackage.eINSTANCE.getTName(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object createTSDOCountFromString(EDataType eDataType, String initialValue) {
    if (initialValue == null) return null;
    Object result = null;
    RuntimeException exception = null;
    try {
      result = XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.UNSIGNED_INT, initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    try {
      result = createTRestrName1stLFromString(SCLPackage.eINSTANCE.getTRestrName1stL(), initialValue);
      if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
        return result;
      }
    }
    catch (RuntimeException e) {
      exception = e;
    }
    if (result != null || exception == null) return result;
    
    throw exception;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTSDOCountToString(EDataType eDataType, Object instanceValue) {
    if (instanceValue == null) return null;
    if (XMLTypePackage.Literals.UNSIGNED_INT.isInstance(instanceValue)) {
      try {
        String value = XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.UNSIGNED_INT, instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    if (SCLPackage.eINSTANCE.getTRestrName1stL().isInstance(instanceValue)) {
      try {
        String value = convertTRestrName1stLToString(SCLPackage.eINSTANCE.getTRestrName1stL(), instanceValue);
        if (value != null) return value;
      }
      catch (Exception e) {
        // Keep trying other member types until all have failed.
      }
    }
    throw new IllegalArgumentException("Invalid value: '"+instanceValue+"' for datatype :"+eDataType.getName());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceSettingsEnum createTServiceSettingsEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTServiceSettingsEnumFromString(SCLPackage.eINSTANCE.getTServiceSettingsEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTServiceSettingsEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTServiceSettingsEnumToString(SCLPackage.eINSTANCE.getTServiceSettingsEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceType createTServiceTypeObjectFromString(EDataType eDataType, String initialValue) {
    return createTServiceTypeFromString(SCLPackage.eINSTANCE.getTServiceType(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTServiceTypeObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTServiceTypeToString(SCLPackage.eINSTANCE.getTServiceType(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSIUnitEnum createTSIUnitEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTSIUnitEnumFromString(SCLPackage.eINSTANCE.getTSIUnitEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTSIUnitEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTSIUnitEnumToString(SCLPackage.eINSTANCE.getTSIUnitEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSmpMod createTSmpModObjectFromString(EDataType eDataType, String initialValue) {
    return createTSmpModFromString(SCLPackage.eINSTANCE.getTSmpMod(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTSmpModObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTSmpModToString(SCLPackage.eINSTANCE.getTSmpMod(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTransformerWindingEnum createTTransformerWindingEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTTransformerWindingEnumFromString(SCLPackage.eINSTANCE.getTTransformerWindingEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTTransformerWindingEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTTransformerWindingEnumToString(SCLPackage.eINSTANCE.getTTransformerWindingEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TUnitMultiplierEnum createTUnitMultiplierEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTUnitMultiplierEnumFromString(SCLPackage.eINSTANCE.getTUnitMultiplierEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTUnitMultiplierEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTUnitMultiplierEnumToString(SCLPackage.eINSTANCE.getTUnitMultiplierEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TValKindEnum createTValKindEnumObjectFromString(EDataType eDataType, String initialValue) {
    return createTValKindEnumFromString(SCLPackage.eINSTANCE.getTValKindEnum(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTValKindEnumObjectToString(EDataType eDataType, Object instanceValue) {
    return convertTValKindEnumToString(SCLPackage.eINSTANCE.getTValKindEnum(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String createTypeTypeFromString(EDataType eDataType, String initialValue) {
    return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertTypeTypeToString(EDataType eDataType, Object instanceValue) {
    return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SCLPackage getSCLPackage() {
    return (SCLPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static SCLPackage getPackage() {
    return SCLPackage.eINSTANCE;
  }

} //SCLFactoryImpl
