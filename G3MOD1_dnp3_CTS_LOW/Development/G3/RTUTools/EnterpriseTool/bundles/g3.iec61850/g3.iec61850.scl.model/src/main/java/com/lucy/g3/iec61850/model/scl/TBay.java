/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TBay</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TBay#getConductingEquipment <em>Conducting Equipment</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TBay#getConnectivityNode <em>Connectivity Node</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TBay#getFunction <em>Function</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBay()
 * @model extendedMetaData="name='tBay' kind='elementOnly'"
 * @generated
 */
public interface TBay extends TEquipmentContainer {
  /**
   * Returns the value of the '<em><b>Conducting Equipment</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TConductingEquipment}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Conducting Equipment</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Conducting Equipment</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBay_ConductingEquipment()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='ConductingEquipment' namespace='##targetNamespace'"
   * @generated
   */
  EList<TConductingEquipment> getConductingEquipment();

  /**
   * Returns the value of the '<em><b>Connectivity Node</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TConnectivityNode}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Connectivity Node</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Connectivity Node</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBay_ConnectivityNode()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='ConnectivityNode' namespace='##targetNamespace'"
   * @generated
   */
  EList<TConnectivityNode> getConnectivityNode();

  /**
   * Returns the value of the '<em><b>Function</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TFunction}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Function</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Function</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBay_Function()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='Function' namespace='##targetNamespace'"
   * @generated
   */
  EList<TFunction> getFunction();

} // TBay
