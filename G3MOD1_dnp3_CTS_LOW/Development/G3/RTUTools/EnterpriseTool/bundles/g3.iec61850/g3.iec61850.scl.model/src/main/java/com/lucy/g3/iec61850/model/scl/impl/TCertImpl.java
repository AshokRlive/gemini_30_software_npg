/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.internal.SCLObjectImpl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TCert;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TCert</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TCertImpl#getCommonName <em>Common Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TCertImpl#getIdHierarchy <em>Id Hierarchy</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TCertImpl extends SCLObjectImpl implements TCert {
  /**
   * The default value of the '{@link #getCommonName() <em>Common Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCommonName()
   * @generated
   * @ordered
   */
  protected static final String COMMON_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCommonName() <em>Common Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCommonName()
   * @generated
   * @ordered
   */
  protected String commonName = COMMON_NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getIdHierarchy() <em>Id Hierarchy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdHierarchy()
   * @generated
   * @ordered
   */
  protected static final String ID_HIERARCHY_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getIdHierarchy() <em>Id Hierarchy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdHierarchy()
   * @generated
   * @ordered
   */
  protected String idHierarchy = ID_HIERARCHY_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TCertImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTCert();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCommonName() {
    return commonName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCommonName(String newCommonName) {
    String oldCommonName = commonName;
    commonName = newCommonName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TCERT__COMMON_NAME, oldCommonName, commonName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIdHierarchy() {
    return idHierarchy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIdHierarchy(String newIdHierarchy) {
    String oldIdHierarchy = idHierarchy;
    idHierarchy = newIdHierarchy;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TCERT__ID_HIERARCHY, oldIdHierarchy, idHierarchy));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TCERT__COMMON_NAME:
        return getCommonName();
      case SCLPackage.TCERT__ID_HIERARCHY:
        return getIdHierarchy();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TCERT__COMMON_NAME:
        setCommonName((String)newValue);
        return;
      case SCLPackage.TCERT__ID_HIERARCHY:
        setIdHierarchy((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TCERT__COMMON_NAME:
        setCommonName(COMMON_NAME_EDEFAULT);
        return;
      case SCLPackage.TCERT__ID_HIERARCHY:
        setIdHierarchy(ID_HIERARCHY_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TCERT__COMMON_NAME:
        return COMMON_NAME_EDEFAULT == null ? commonName != null : !COMMON_NAME_EDEFAULT.equals(commonName);
      case SCLPackage.TCERT__ID_HIERARCHY:
        return ID_HIERARCHY_EDEFAULT == null ? idHierarchy != null : !ID_HIERARCHY_EDEFAULT.equals(idHierarchy);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (commonName: ");
    result.append(commonName);
    result.append(", idHierarchy: ");
    result.append(idHierarchy);
    result.append(')');
    return result.toString();
  }

} //TCertImpl
