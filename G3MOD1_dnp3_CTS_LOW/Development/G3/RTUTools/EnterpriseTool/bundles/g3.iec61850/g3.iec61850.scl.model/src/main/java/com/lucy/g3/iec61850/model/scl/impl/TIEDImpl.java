/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TAccessPoint;
import com.lucy.g3.iec61850.model.scl.TIED;
import com.lucy.g3.iec61850.model.scl.TRightEnum;
import com.lucy.g3.iec61850.model.scl.TServices;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TIED</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TIEDImpl#getServices <em>Services</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TIEDImpl#getAccessPoint <em>Access Point</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TIEDImpl#getConfigVersion <em>Config Version</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TIEDImpl#getEngRight <em>Eng Right</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TIEDImpl#getManufacturer <em>Manufacturer</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TIEDImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TIEDImpl#getOriginalSclRevision <em>Original Scl Revision</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TIEDImpl#getOriginalSclVersion <em>Original Scl Version</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TIEDImpl#getOwner <em>Owner</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TIEDImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TIEDImpl extends TUnNamingImpl implements TIED {
  /**
   * The cached value of the '{@link #getServices() <em>Services</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getServices()
   * @generated
   * @ordered
   */
  protected TServices services;

  /**
   * The cached value of the '{@link #getAccessPoint() <em>Access Point</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAccessPoint()
   * @generated
   * @ordered
   */
  protected EList<TAccessPoint> accessPoint;

  /**
   * The default value of the '{@link #getConfigVersion() <em>Config Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConfigVersion()
   * @generated
   * @ordered
   */
  protected static final String CONFIG_VERSION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getConfigVersion() <em>Config Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConfigVersion()
   * @generated
   * @ordered
   */
  protected String configVersion = CONFIG_VERSION_EDEFAULT;

  /**
   * The default value of the '{@link #getEngRight() <em>Eng Right</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEngRight()
   * @generated
   * @ordered
   */
  protected static final TRightEnum ENG_RIGHT_EDEFAULT = TRightEnum.FULL;

  /**
   * The cached value of the '{@link #getEngRight() <em>Eng Right</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEngRight()
   * @generated
   * @ordered
   */
  protected TRightEnum engRight = ENG_RIGHT_EDEFAULT;

  /**
   * This is true if the Eng Right attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean engRightESet;

  /**
   * The default value of the '{@link #getManufacturer() <em>Manufacturer</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getManufacturer()
   * @generated
   * @ordered
   */
  protected static final String MANUFACTURER_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getManufacturer() <em>Manufacturer</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getManufacturer()
   * @generated
   * @ordered
   */
  protected String manufacturer = MANUFACTURER_EDEFAULT;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getOriginalSclRevision() <em>Original Scl Revision</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOriginalSclRevision()
   * @generated
   * @ordered
   */
  protected static final String ORIGINAL_SCL_REVISION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOriginalSclRevision() <em>Original Scl Revision</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOriginalSclRevision()
   * @generated
   * @ordered
   */
  protected String originalSclRevision = ORIGINAL_SCL_REVISION_EDEFAULT;

  /**
   * The default value of the '{@link #getOriginalSclVersion() <em>Original Scl Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOriginalSclVersion()
   * @generated
   * @ordered
   */
  protected static final String ORIGINAL_SCL_VERSION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOriginalSclVersion() <em>Original Scl Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOriginalSclVersion()
   * @generated
   * @ordered
   */
  protected String originalSclVersion = ORIGINAL_SCL_VERSION_EDEFAULT;

  /**
   * The default value of the '{@link #getOwner() <em>Owner</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOwner()
   * @generated
   * @ordered
   */
  protected static final String OWNER_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOwner() <em>Owner</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOwner()
   * @generated
   * @ordered
   */
  protected String owner = OWNER_EDEFAULT;

  /**
   * The default value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected static final String TYPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected String type = TYPE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TIEDImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTIED();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServices getServices() {
    return services;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetServices(TServices newServices, NotificationChain msgs) {
    TServices oldServices = services;
    services = newServices;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TIED__SERVICES, oldServices, newServices);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setServices(TServices newServices) {
    if (newServices != services) {
      NotificationChain msgs = null;
      if (services != null)
        msgs = ((InternalEObject)services).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TIED__SERVICES, null, msgs);
      if (newServices != null)
        msgs = ((InternalEObject)newServices).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TIED__SERVICES, null, msgs);
      msgs = basicSetServices(newServices, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TIED__SERVICES, newServices, newServices));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TAccessPoint> getAccessPoint() {
    if (accessPoint == null) {
      accessPoint = new EObjectContainmentEList<TAccessPoint>(TAccessPoint.class, this, SCLPackage.TIED__ACCESS_POINT);
    }
    return accessPoint;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getConfigVersion() {
    return configVersion;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConfigVersion(String newConfigVersion) {
    String oldConfigVersion = configVersion;
    configVersion = newConfigVersion;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TIED__CONFIG_VERSION, oldConfigVersion, configVersion));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TRightEnum getEngRight() {
    return engRight;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEngRight(TRightEnum newEngRight) {
    TRightEnum oldEngRight = engRight;
    engRight = newEngRight == null ? ENG_RIGHT_EDEFAULT : newEngRight;
    boolean oldEngRightESet = engRightESet;
    engRightESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TIED__ENG_RIGHT, oldEngRight, engRight, !oldEngRightESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetEngRight() {
    TRightEnum oldEngRight = engRight;
    boolean oldEngRightESet = engRightESet;
    engRight = ENG_RIGHT_EDEFAULT;
    engRightESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TIED__ENG_RIGHT, oldEngRight, ENG_RIGHT_EDEFAULT, oldEngRightESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetEngRight() {
    return engRightESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getManufacturer() {
    return manufacturer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setManufacturer(String newManufacturer) {
    String oldManufacturer = manufacturer;
    manufacturer = newManufacturer;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TIED__MANUFACTURER, oldManufacturer, manufacturer));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName) {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TIED__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getOriginalSclRevision() {
    return originalSclRevision;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOriginalSclRevision(String newOriginalSclRevision) {
    String oldOriginalSclRevision = originalSclRevision;
    originalSclRevision = newOriginalSclRevision;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TIED__ORIGINAL_SCL_REVISION, oldOriginalSclRevision, originalSclRevision));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getOriginalSclVersion() {
    return originalSclVersion;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOriginalSclVersion(String newOriginalSclVersion) {
    String oldOriginalSclVersion = originalSclVersion;
    originalSclVersion = newOriginalSclVersion;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TIED__ORIGINAL_SCL_VERSION, oldOriginalSclVersion, originalSclVersion));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getOwner() {
    return owner;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOwner(String newOwner) {
    String oldOwner = owner;
    owner = newOwner;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TIED__OWNER, oldOwner, owner));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getType() {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(String newType) {
    String oldType = type;
    type = newType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TIED__TYPE, oldType, type));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case SCLPackage.TIED__SERVICES:
        return basicSetServices(null, msgs);
      case SCLPackage.TIED__ACCESS_POINT:
        return ((InternalEList<?>)getAccessPoint()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TIED__SERVICES:
        return getServices();
      case SCLPackage.TIED__ACCESS_POINT:
        return getAccessPoint();
      case SCLPackage.TIED__CONFIG_VERSION:
        return getConfigVersion();
      case SCLPackage.TIED__ENG_RIGHT:
        return getEngRight();
      case SCLPackage.TIED__MANUFACTURER:
        return getManufacturer();
      case SCLPackage.TIED__NAME:
        return getName();
      case SCLPackage.TIED__ORIGINAL_SCL_REVISION:
        return getOriginalSclRevision();
      case SCLPackage.TIED__ORIGINAL_SCL_VERSION:
        return getOriginalSclVersion();
      case SCLPackage.TIED__OWNER:
        return getOwner();
      case SCLPackage.TIED__TYPE:
        return getType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TIED__SERVICES:
        setServices((TServices)newValue);
        return;
      case SCLPackage.TIED__ACCESS_POINT:
        getAccessPoint().clear();
        getAccessPoint().addAll((Collection<? extends TAccessPoint>)newValue);
        return;
      case SCLPackage.TIED__CONFIG_VERSION:
        setConfigVersion((String)newValue);
        return;
      case SCLPackage.TIED__ENG_RIGHT:
        setEngRight((TRightEnum)newValue);
        return;
      case SCLPackage.TIED__MANUFACTURER:
        setManufacturer((String)newValue);
        return;
      case SCLPackage.TIED__NAME:
        setName((String)newValue);
        return;
      case SCLPackage.TIED__ORIGINAL_SCL_REVISION:
        setOriginalSclRevision((String)newValue);
        return;
      case SCLPackage.TIED__ORIGINAL_SCL_VERSION:
        setOriginalSclVersion((String)newValue);
        return;
      case SCLPackage.TIED__OWNER:
        setOwner((String)newValue);
        return;
      case SCLPackage.TIED__TYPE:
        setType((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TIED__SERVICES:
        setServices((TServices)null);
        return;
      case SCLPackage.TIED__ACCESS_POINT:
        getAccessPoint().clear();
        return;
      case SCLPackage.TIED__CONFIG_VERSION:
        setConfigVersion(CONFIG_VERSION_EDEFAULT);
        return;
      case SCLPackage.TIED__ENG_RIGHT:
        unsetEngRight();
        return;
      case SCLPackage.TIED__MANUFACTURER:
        setManufacturer(MANUFACTURER_EDEFAULT);
        return;
      case SCLPackage.TIED__NAME:
        setName(NAME_EDEFAULT);
        return;
      case SCLPackage.TIED__ORIGINAL_SCL_REVISION:
        setOriginalSclRevision(ORIGINAL_SCL_REVISION_EDEFAULT);
        return;
      case SCLPackage.TIED__ORIGINAL_SCL_VERSION:
        setOriginalSclVersion(ORIGINAL_SCL_VERSION_EDEFAULT);
        return;
      case SCLPackage.TIED__OWNER:
        setOwner(OWNER_EDEFAULT);
        return;
      case SCLPackage.TIED__TYPE:
        setType(TYPE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TIED__SERVICES:
        return services != null;
      case SCLPackage.TIED__ACCESS_POINT:
        return accessPoint != null && !accessPoint.isEmpty();
      case SCLPackage.TIED__CONFIG_VERSION:
        return CONFIG_VERSION_EDEFAULT == null ? configVersion != null : !CONFIG_VERSION_EDEFAULT.equals(configVersion);
      case SCLPackage.TIED__ENG_RIGHT:
        return isSetEngRight();
      case SCLPackage.TIED__MANUFACTURER:
        return MANUFACTURER_EDEFAULT == null ? manufacturer != null : !MANUFACTURER_EDEFAULT.equals(manufacturer);
      case SCLPackage.TIED__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case SCLPackage.TIED__ORIGINAL_SCL_REVISION:
        return ORIGINAL_SCL_REVISION_EDEFAULT == null ? originalSclRevision != null : !ORIGINAL_SCL_REVISION_EDEFAULT.equals(originalSclRevision);
      case SCLPackage.TIED__ORIGINAL_SCL_VERSION:
        return ORIGINAL_SCL_VERSION_EDEFAULT == null ? originalSclVersion != null : !ORIGINAL_SCL_VERSION_EDEFAULT.equals(originalSclVersion);
      case SCLPackage.TIED__OWNER:
        return OWNER_EDEFAULT == null ? owner != null : !OWNER_EDEFAULT.equals(owner);
      case SCLPackage.TIED__TYPE:
        return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (configVersion: ");
    result.append(configVersion);
    result.append(", engRight: ");
    if (engRightESet) result.append(engRight); else result.append("<unset>");
    result.append(", manufacturer: ");
    result.append(manufacturer);
    result.append(", name: ");
    result.append(name);
    result.append(", originalSclRevision: ");
    result.append(originalSclRevision);
    result.append(", originalSclVersion: ");
    result.append(originalSclVersion);
    result.append(", owner: ");
    result.append(owner);
    result.append(", type: ");
    result.append(type);
    result.append(')');
    return result.toString();
  }

} //TIEDImpl
