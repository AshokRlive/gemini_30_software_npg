/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPMAC Address</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPMACAddress()
 * @model extendedMetaData="name='tP_MAC-Address' kind='simple'"
 * @generated
 */
public interface TPMACAddress extends TP {
} // TPMACAddress
