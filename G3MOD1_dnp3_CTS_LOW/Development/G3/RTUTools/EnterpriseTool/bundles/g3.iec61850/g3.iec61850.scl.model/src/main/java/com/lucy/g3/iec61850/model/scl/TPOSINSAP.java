/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPOSINSAP</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPOSINSAP()
 * @model extendedMetaData="name='tP_OSI-NSAP' kind='simple'"
 * @generated
 */
public interface TPOSINSAP extends TP {
} // TPOSINSAP
