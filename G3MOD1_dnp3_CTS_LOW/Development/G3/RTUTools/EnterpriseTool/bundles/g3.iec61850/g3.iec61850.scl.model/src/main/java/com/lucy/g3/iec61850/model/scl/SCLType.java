/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.SCLType#getHeader <em>Header</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.SCLType#getSubstation <em>Substation</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.SCLType#getCommunication <em>Communication</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.SCLType#getIED <em>IED</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.SCLType#getDataTypeTemplates <em>Data Type Templates</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.SCLType#getRevision <em>Revision</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.SCLType#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getSCLType()
 * @model extendedMetaData="name='SCL_._type' kind='elementOnly'"
 * @generated
 */
public interface SCLType extends TBaseElement {
  /**
   * Returns the value of the '<em><b>Header</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Header</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Header</em>' containment reference.
   * @see #setHeader(THeader)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getSCLType_Header()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='Header' namespace='##targetNamespace'"
   * @generated
   */
  THeader getHeader();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.SCLType#getHeader <em>Header</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Header</em>' containment reference.
   * @see #getHeader()
   * @generated
   */
  void setHeader(THeader value);

  /**
   * Returns the value of the '<em><b>Substation</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TSubstation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Substation</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Substation</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getSCLType_Substation()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='Substation' namespace='##targetNamespace'"
   * @generated
   */
  EList<TSubstation> getSubstation();

  /**
   * Returns the value of the '<em><b>Communication</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Communication</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Communication</em>' containment reference.
   * @see #setCommunication(TCommunication)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getSCLType_Communication()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='Communication' namespace='##targetNamespace'"
   * @generated
   */
  TCommunication getCommunication();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.SCLType#getCommunication <em>Communication</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Communication</em>' containment reference.
   * @see #getCommunication()
   * @generated
   */
  void setCommunication(TCommunication value);

  /**
   * Returns the value of the '<em><b>IED</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TIED}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>IED</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>IED</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getSCLType_IED()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='IED' namespace='##targetNamespace'"
   * @generated
   */
  EList<TIED> getIED();

  /**
   * Returns the value of the '<em><b>Data Type Templates</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Data Type Templates</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Data Type Templates</em>' containment reference.
   * @see #setDataTypeTemplates(TDataTypeTemplates)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getSCLType_DataTypeTemplates()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='DataTypeTemplates' namespace='##targetNamespace'"
   * @generated
   */
  TDataTypeTemplates getDataTypeTemplates();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.SCLType#getDataTypeTemplates <em>Data Type Templates</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Data Type Templates</em>' containment reference.
   * @see #getDataTypeTemplates()
   * @generated
   */
  void setDataTypeTemplates(TDataTypeTemplates value);

  /**
   * Returns the value of the '<em><b>Revision</b></em>' attribute.
   * The default value is <code>"A"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Revision</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Revision</em>' attribute.
   * @see #isSetRevision()
   * @see #unsetRevision()
   * @see #setRevision(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getSCLType_Revision()
   * @model default="A" unsettable="true" dataType="com.lucy.g3.iec61850.model.scl.TSclRevision" required="true"
   *        extendedMetaData="kind='attribute' name='revision'"
   * @generated
   */
  String getRevision();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.SCLType#getRevision <em>Revision</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Revision</em>' attribute.
   * @see #isSetRevision()
   * @see #unsetRevision()
   * @see #getRevision()
   * @generated
   */
  void setRevision(String value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.SCLType#getRevision <em>Revision</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetRevision()
   * @see #getRevision()
   * @see #setRevision(String)
   * @generated
   */
  void unsetRevision();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.SCLType#getRevision <em>Revision</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Revision</em>' attribute is set.
   * @see #unsetRevision()
   * @see #getRevision()
   * @see #setRevision(String)
   * @generated
   */
  boolean isSetRevision();

  /**
   * Returns the value of the '<em><b>Version</b></em>' attribute.
   * The default value is <code>"2007"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Version</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Version</em>' attribute.
   * @see #isSetVersion()
   * @see #unsetVersion()
   * @see #setVersion(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getSCLType_Version()
   * @model default="2007" unsettable="true" dataType="com.lucy.g3.iec61850.model.scl.TSclVersion" required="true"
   *        extendedMetaData="kind='attribute' name='version'"
   * @generated
   */
  String getVersion();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.SCLType#getVersion <em>Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Version</em>' attribute.
   * @see #isSetVersion()
   * @see #unsetVersion()
   * @see #getVersion()
   * @generated
   */
  void setVersion(String value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.SCLType#getVersion <em>Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetVersion()
   * @see #getVersion()
   * @see #setVersion(String)
   * @generated
   */
  void unsetVersion();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.SCLType#getVersion <em>Version</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Version</em>' attribute is set.
   * @see #unsetVersion()
   * @see #getVersion()
   * @see #setVersion(String)
   * @generated
   */
  boolean isSetVersion();

} // SCLType
