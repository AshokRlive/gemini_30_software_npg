/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TDA Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TDAType#getBDA <em>BDA</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TDAType#getProtNs <em>Prot Ns</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TDAType#getIedType <em>Ied Type</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTDAType()
 * @model extendedMetaData="name='tDAType' kind='elementOnly'"
 * @generated
 */
public interface TDAType extends TIDNaming {
  /**
   * Returns the value of the '<em><b>BDA</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TBDA}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>BDA</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>BDA</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTDAType_BDA()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='BDA' namespace='##targetNamespace'"
   * @generated
   */
  EList<TBDA> getBDA();

  /**
   * Returns the value of the '<em><b>Prot Ns</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.ProtNsType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prot Ns</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prot Ns</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTDAType_ProtNs()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='ProtNs' namespace='##targetNamespace'"
   * @generated
   */
  EList<ProtNsType> getProtNs();

  /**
   * Returns the value of the '<em><b>Ied Type</b></em>' attribute.
   * The default value is <code>""</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ied Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ied Type</em>' attribute.
   * @see #isSetIedType()
   * @see #unsetIedType()
   * @see #setIedType(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTDAType_IedType()
   * @model default="" unsettable="true" dataType="com.lucy.g3.iec61850.model.scl.TAnyName"
   *        extendedMetaData="kind='attribute' name='iedType'"
   * @generated
   */
  String getIedType();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TDAType#getIedType <em>Ied Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ied Type</em>' attribute.
   * @see #isSetIedType()
   * @see #unsetIedType()
   * @see #getIedType()
   * @generated
   */
  void setIedType(String value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TDAType#getIedType <em>Ied Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetIedType()
   * @see #getIedType()
   * @see #setIedType(String)
   * @generated
   */
  void unsetIedType();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TDAType#getIedType <em>Ied Type</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Ied Type</em>' attribute is set.
   * @see #unsetIedType()
   * @see #getIedType()
   * @see #setIedType(String)
   * @generated
   */
  boolean isSetIedType();

} // TDAType
