/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IED Name Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getValue <em>Value</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getApRef <em>Ap Ref</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getLdInst <em>Ld Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getLnClass <em>Ln Class</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getLnInst <em>Ln Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getPrefix <em>Prefix</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getIEDNameType()
 * @model extendedMetaData="name='IEDName_._type' kind='simple'"
 * @extends SCLObject
 * @generated
 */
public interface IEDNameType extends SCLObject {
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getIEDNameType_Value()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TIEDName"
   *        extendedMetaData="name=':0' kind='simple'"
   * @generated
   */
  String getValue();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(String value);

  /**
   * Returns the value of the '<em><b>Ap Ref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ap Ref</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ap Ref</em>' attribute.
   * @see #setApRef(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getIEDNameType_ApRef()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TAccessPointName"
   *        extendedMetaData="kind='attribute' name='apRef'"
   * @generated
   */
  String getApRef();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getApRef <em>Ap Ref</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ap Ref</em>' attribute.
   * @see #getApRef()
   * @generated
   */
  void setApRef(String value);

  /**
   * Returns the value of the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ld Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ld Inst</em>' attribute.
   * @see #setLdInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getIEDNameType_LdInst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLDInst"
   *        extendedMetaData="kind='attribute' name='ldInst'"
   * @generated
   */
  String getLdInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getLdInst <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ld Inst</em>' attribute.
   * @see #getLdInst()
   * @generated
   */
  void setLdInst(String value);

  /**
   * Returns the value of the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ln Class</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ln Class</em>' attribute.
   * @see #setLnClass(Object)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getIEDNameType_LnClass()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLNClassEnum"
   *        extendedMetaData="kind='attribute' name='lnClass'"
   * @generated
   */
  Object getLnClass();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getLnClass <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ln Class</em>' attribute.
   * @see #getLnClass()
   * @generated
   */
  void setLnClass(Object value);

  /**
   * Returns the value of the '<em><b>Ln Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ln Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ln Inst</em>' attribute.
   * @see #setLnInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getIEDNameType_LnInst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLNInst"
   *        extendedMetaData="kind='attribute' name='lnInst'"
   * @generated
   */
  String getLnInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getLnInst <em>Ln Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ln Inst</em>' attribute.
   * @see #getLnInst()
   * @generated
   */
  void setLnInst(String value);

  /**
   * Returns the value of the '<em><b>Prefix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prefix</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prefix</em>' attribute.
   * @see #setPrefix(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getIEDNameType_Prefix()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TPrefix"
   *        extendedMetaData="kind='attribute' name='prefix'"
   * @generated
   */
  String getPrefix();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getPrefix <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prefix</em>' attribute.
   * @see #getPrefix()
   * @generated
   */
  void setPrefix(String value);

} // IEDNameType
