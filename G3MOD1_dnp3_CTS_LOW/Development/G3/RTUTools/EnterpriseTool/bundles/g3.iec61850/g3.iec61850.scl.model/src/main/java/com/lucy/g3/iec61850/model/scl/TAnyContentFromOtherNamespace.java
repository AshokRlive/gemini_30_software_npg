/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TAny Content From Other Namespace</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace#getMixed <em>Mixed</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace#getGroup <em>Group</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace#getAny <em>Any</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace#getAnyAttribute <em>Any Attribute</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAnyContentFromOtherNamespace()
 * @model abstract="true"
 *        extendedMetaData="name='tAnyContentFromOtherNamespace' kind='mixed'"
 * @extends SCLObject
 * @generated
 */
public interface TAnyContentFromOtherNamespace extends SCLObject {
  /**
   * Returns the value of the '<em><b>Mixed</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mixed</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mixed</em>' attribute list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAnyContentFromOtherNamespace_Mixed()
   * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='elementWildcard' name=':mixed'"
   * @generated
   */
  FeatureMap getMixed();

  /**
   * Returns the value of the '<em><b>Group</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group</em>' attribute list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAnyContentFromOtherNamespace_Group()
   * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true" transient="true" volatile="true" derived="true"
   *        extendedMetaData="kind='group' name='group:1'"
   * @generated
   */
  FeatureMap getGroup();

  /**
   * Returns the value of the '<em><b>Any</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Any</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Any</em>' attribute list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAnyContentFromOtherNamespace_Any()
   * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true" transient="true" volatile="true" derived="true"
   *        extendedMetaData="kind='elementWildcard' wildcards='##other' name=':2' processing='lax' group='#group:1'"
   * @generated
   */
  FeatureMap getAny();

  /**
   * Returns the value of the '<em><b>Any Attribute</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Any Attribute</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Any Attribute</em>' attribute list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAnyContentFromOtherNamespace_AnyAttribute()
   * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='attributeWildcard' wildcards='##other' name=':3' processing='lax'"
   * @generated
   */
  FeatureMap getAnyAttribute();

} // TAnyContentFromOtherNamespace
