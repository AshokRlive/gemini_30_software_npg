/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TP Port</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPPort()
 * @model abstract="true"
 *        extendedMetaData="name='tP_Port' kind='simple'"
 * @generated
 */
public interface TPPort extends TP {
} // TPPort
