/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.internal.SCLObjectImpl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TEnumVal;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TEnum Val</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TEnumValImpl#getValue <em>Value</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TEnumValImpl#getDesc <em>Desc</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TEnumValImpl#getOrd <em>Ord</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TEnumValImpl extends SCLObjectImpl implements TEnumVal {
  /**
   * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected static final String VALUE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected String value = VALUE_EDEFAULT;

  /**
   * The default value of the '{@link #getDesc() <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDesc()
   * @generated
   * @ordered
   */
  protected static final String DESC_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getDesc() <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDesc()
   * @generated
   * @ordered
   */
  protected String desc = DESC_EDEFAULT;

  /**
   * This is true if the Desc attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean descESet;

  /**
   * The default value of the '{@link #getOrd() <em>Ord</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOrd()
   * @generated
   * @ordered
   */
  protected static final int ORD_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getOrd() <em>Ord</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOrd()
   * @generated
   * @ordered
   */
  protected int ord = ORD_EDEFAULT;

  /**
   * This is true if the Ord attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean ordESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TEnumValImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTEnumVal();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getValue() {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValue(String newValue) {
    String oldValue = value;
    value = newValue;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TENUM_VAL__VALUE, oldValue, value));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDesc() {
    return desc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDesc(String newDesc) {
    String oldDesc = desc;
    desc = newDesc;
    boolean oldDescESet = descESet;
    descESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TENUM_VAL__DESC, oldDesc, desc, !oldDescESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetDesc() {
    String oldDesc = desc;
    boolean oldDescESet = descESet;
    desc = DESC_EDEFAULT;
    descESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TENUM_VAL__DESC, oldDesc, DESC_EDEFAULT, oldDescESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetDesc() {
    return descESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getOrd() {
    return ord;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOrd(int newOrd) {
    int oldOrd = ord;
    ord = newOrd;
    boolean oldOrdESet = ordESet;
    ordESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TENUM_VAL__ORD, oldOrd, ord, !oldOrdESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetOrd() {
    int oldOrd = ord;
    boolean oldOrdESet = ordESet;
    ord = ORD_EDEFAULT;
    ordESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TENUM_VAL__ORD, oldOrd, ORD_EDEFAULT, oldOrdESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetOrd() {
    return ordESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TENUM_VAL__VALUE:
        return getValue();
      case SCLPackage.TENUM_VAL__DESC:
        return getDesc();
      case SCLPackage.TENUM_VAL__ORD:
        return getOrd();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TENUM_VAL__VALUE:
        setValue((String)newValue);
        return;
      case SCLPackage.TENUM_VAL__DESC:
        setDesc((String)newValue);
        return;
      case SCLPackage.TENUM_VAL__ORD:
        setOrd((Integer)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TENUM_VAL__VALUE:
        setValue(VALUE_EDEFAULT);
        return;
      case SCLPackage.TENUM_VAL__DESC:
        unsetDesc();
        return;
      case SCLPackage.TENUM_VAL__ORD:
        unsetOrd();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TENUM_VAL__VALUE:
        return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
      case SCLPackage.TENUM_VAL__DESC:
        return isSetDesc();
      case SCLPackage.TENUM_VAL__ORD:
        return isSetOrd();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (value: ");
    result.append(value);
    result.append(", desc: ");
    if (descESet) result.append(desc); else result.append("<unset>");
    result.append(", ord: ");
    if (ordESet) result.append(ord); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TEnumValImpl
