/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPOSITSEL</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPOSITSEL()
 * @model extendedMetaData="name='tP_OSI-TSEL' kind='simple'"
 * @generated
 */
public interface TPOSITSEL extends TP {
} // TPOSITSEL
