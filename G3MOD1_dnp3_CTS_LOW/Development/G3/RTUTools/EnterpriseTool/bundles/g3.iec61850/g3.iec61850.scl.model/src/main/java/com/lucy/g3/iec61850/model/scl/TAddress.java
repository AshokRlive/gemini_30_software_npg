/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TAddress</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAddress#getP <em>P</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAddress()
 * @model extendedMetaData="name='tAddress' kind='elementOnly'"
 * @extends SCLObject
 * @generated
 */
public interface TAddress extends SCLObject {
  /**
   * Returns the value of the '<em><b>P</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TP}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>P</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>P</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAddress_P()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='P' namespace='##targetNamespace'"
   * @generated
   */
  EList<TP> getP();

} // TAddress
