/**
 */
package com.lucy.g3.iec61850.model.systemcorp;

import com.lucy.g3.iec61850.model.internal.SCLObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TGeneric Private Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField1 <em>Field1</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField2 <em>Field2</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField3 <em>Field3</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField4 <em>Field4</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField5 <em>Field5</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage#getTGenericPrivateObject()
 * @model extendedMetaData="name='tGenericPrivateObject' kind='empty'"
 * @extends SCLObject
 * @generated
 */
public interface TGenericPrivateObject extends SCLObject {
  /**
   * Returns the value of the '<em><b>Field1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field1</em>' attribute.
   * @see #isSetField1()
   * @see #unsetField1()
   * @see #setField1(long)
   * @see com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage#getTGenericPrivateObject_Field1()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
   *        extendedMetaData="kind='attribute' name='Field1'"
   * @generated
   */
  long getField1();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField1 <em>Field1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field1</em>' attribute.
   * @see #isSetField1()
   * @see #unsetField1()
   * @see #getField1()
   * @generated
   */
  void setField1(long value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField1 <em>Field1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetField1()
   * @see #getField1()
   * @see #setField1(long)
   * @generated
   */
  void unsetField1();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField1 <em>Field1</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Field1</em>' attribute is set.
   * @see #unsetField1()
   * @see #getField1()
   * @see #setField1(long)
   * @generated
   */
  boolean isSetField1();

  /**
   * Returns the value of the '<em><b>Field2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field2</em>' attribute.
   * @see #isSetField2()
   * @see #unsetField2()
   * @see #setField2(long)
   * @see com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage#getTGenericPrivateObject_Field2()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
   *        extendedMetaData="kind='attribute' name='Field2'"
   * @generated
   */
  long getField2();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField2 <em>Field2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field2</em>' attribute.
   * @see #isSetField2()
   * @see #unsetField2()
   * @see #getField2()
   * @generated
   */
  void setField2(long value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField2 <em>Field2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetField2()
   * @see #getField2()
   * @see #setField2(long)
   * @generated
   */
  void unsetField2();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField2 <em>Field2</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Field2</em>' attribute is set.
   * @see #unsetField2()
   * @see #getField2()
   * @see #setField2(long)
   * @generated
   */
  boolean isSetField2();

  /**
   * Returns the value of the '<em><b>Field3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field3</em>' attribute.
   * @see #isSetField3()
   * @see #unsetField3()
   * @see #setField3(long)
   * @see com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage#getTGenericPrivateObject_Field3()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
   *        extendedMetaData="kind='attribute' name='Field3'"
   * @generated
   */
  long getField3();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField3 <em>Field3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field3</em>' attribute.
   * @see #isSetField3()
   * @see #unsetField3()
   * @see #getField3()
   * @generated
   */
  void setField3(long value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField3 <em>Field3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetField3()
   * @see #getField3()
   * @see #setField3(long)
   * @generated
   */
  void unsetField3();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField3 <em>Field3</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Field3</em>' attribute is set.
   * @see #unsetField3()
   * @see #getField3()
   * @see #setField3(long)
   * @generated
   */
  boolean isSetField3();

  /**
   * Returns the value of the '<em><b>Field4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field4</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field4</em>' attribute.
   * @see #isSetField4()
   * @see #unsetField4()
   * @see #setField4(long)
   * @see com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage#getTGenericPrivateObject_Field4()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
   *        extendedMetaData="kind='attribute' name='Field4'"
   * @generated
   */
  long getField4();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField4 <em>Field4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field4</em>' attribute.
   * @see #isSetField4()
   * @see #unsetField4()
   * @see #getField4()
   * @generated
   */
  void setField4(long value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField4 <em>Field4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetField4()
   * @see #getField4()
   * @see #setField4(long)
   * @generated
   */
  void unsetField4();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField4 <em>Field4</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Field4</em>' attribute is set.
   * @see #unsetField4()
   * @see #getField4()
   * @see #setField4(long)
   * @generated
   */
  boolean isSetField4();

  /**
   * Returns the value of the '<em><b>Field5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field5</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field5</em>' attribute.
   * @see #isSetField5()
   * @see #unsetField5()
   * @see #setField5(long)
   * @see com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage#getTGenericPrivateObject_Field5()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
   *        extendedMetaData="kind='attribute' name='Field5'"
   * @generated
   */
  long getField5();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField5 <em>Field5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field5</em>' attribute.
   * @see #isSetField5()
   * @see #unsetField5()
   * @see #getField5()
   * @generated
   */
  void setField5(long value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField5 <em>Field5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetField5()
   * @see #getField5()
   * @see #setField5(long)
   * @generated
   */
  void unsetField5();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField5 <em>Field5</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Field5</em>' attribute is set.
   * @see #unsetField5()
   * @see #getField5()
   * @see #setField5(long)
   * @generated
   */
  boolean isSetField5();

} // TGenericPrivateObject
