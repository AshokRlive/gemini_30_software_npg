/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TGSE Control</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TGSEControl#getAppID <em>App ID</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TGSEControl#isFixedOffs <em>Fixed Offs</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TGSEControl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTGSEControl()
 * @model extendedMetaData="name='tGSEControl' kind='elementOnly'"
 * @generated
 */
public interface TGSEControl extends TControlWithIEDName {
  /**
   * Returns the value of the '<em><b>App ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>App ID</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>App ID</em>' attribute.
   * @see #setAppID(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTGSEControl_AppID()
   * @model dataType="com.lucy.g3.iec61850.model.scl.AppIDType" required="true"
   *        extendedMetaData="kind='attribute' name='appID'"
   * @generated
   */
  String getAppID();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TGSEControl#getAppID <em>App ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>App ID</em>' attribute.
   * @see #getAppID()
   * @generated
   */
  void setAppID(String value);

  /**
   * Returns the value of the '<em><b>Fixed Offs</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fixed Offs</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fixed Offs</em>' attribute.
   * @see #isSetFixedOffs()
   * @see #unsetFixedOffs()
   * @see #setFixedOffs(boolean)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTGSEControl_FixedOffs()
   * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   *        extendedMetaData="kind='attribute' name='fixedOffs'"
   * @generated
   */
  boolean isFixedOffs();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TGSEControl#isFixedOffs <em>Fixed Offs</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fixed Offs</em>' attribute.
   * @see #isSetFixedOffs()
   * @see #unsetFixedOffs()
   * @see #isFixedOffs()
   * @generated
   */
  void setFixedOffs(boolean value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TGSEControl#isFixedOffs <em>Fixed Offs</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetFixedOffs()
   * @see #isFixedOffs()
   * @see #setFixedOffs(boolean)
   * @generated
   */
  void unsetFixedOffs();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TGSEControl#isFixedOffs <em>Fixed Offs</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Fixed Offs</em>' attribute is set.
   * @see #unsetFixedOffs()
   * @see #isFixedOffs()
   * @see #setFixedOffs(boolean)
   * @generated
   */
  boolean isSetFixedOffs();

  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * The default value is <code>"GOOSE"</code>.
   * The literals are from the enumeration {@link com.lucy.g3.iec61850.model.scl.TGSEControlTypeEnum}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TGSEControlTypeEnum
   * @see #isSetType()
   * @see #unsetType()
   * @see #setType(TGSEControlTypeEnum)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTGSEControl_Type()
   * @model default="GOOSE" unsettable="true"
   *        extendedMetaData="kind='attribute' name='type'"
   * @generated
   */
  TGSEControlTypeEnum getType();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TGSEControl#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TGSEControlTypeEnum
   * @see #isSetType()
   * @see #unsetType()
   * @see #getType()
   * @generated
   */
  void setType(TGSEControlTypeEnum value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TGSEControl#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetType()
   * @see #getType()
   * @see #setType(TGSEControlTypeEnum)
   * @generated
   */
  void unsetType();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TGSEControl#getType <em>Type</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Type</em>' attribute is set.
   * @see #unsetType()
   * @see #getType()
   * @see #setType(TGSEControlTypeEnum)
   * @generated
   */
  boolean isSetType();

} // TGSEControl
