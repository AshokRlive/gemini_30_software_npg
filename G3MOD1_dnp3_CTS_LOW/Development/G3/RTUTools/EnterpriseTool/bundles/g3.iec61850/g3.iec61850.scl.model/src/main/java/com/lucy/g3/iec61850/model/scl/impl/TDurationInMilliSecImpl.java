/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.internal.SCLObjectImpl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TDurationInMilliSec;
import com.lucy.g3.iec61850.model.scl.TSIUnitEnum;
import com.lucy.g3.iec61850.model.scl.TUnitMultiplierEnum;

import java.math.BigDecimal;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TDuration In Milli Sec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TDurationInMilliSecImpl#getValue <em>Value</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TDurationInMilliSecImpl#getMultiplier <em>Multiplier</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TDurationInMilliSecImpl#getUnit <em>Unit</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TDurationInMilliSecImpl extends SCLObjectImpl implements TDurationInMilliSec {
  /**
   * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected static final BigDecimal VALUE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected BigDecimal value = VALUE_EDEFAULT;

  /**
   * The default value of the '{@link #getMultiplier() <em>Multiplier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMultiplier()
   * @generated
   * @ordered
   */
  protected static final TUnitMultiplierEnum MULTIPLIER_EDEFAULT = TUnitMultiplierEnum.M;

  /**
   * The cached value of the '{@link #getMultiplier() <em>Multiplier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMultiplier()
   * @generated
   * @ordered
   */
  protected TUnitMultiplierEnum multiplier = MULTIPLIER_EDEFAULT;

  /**
   * This is true if the Multiplier attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean multiplierESet;

  /**
   * The default value of the '{@link #getUnit() <em>Unit</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnit()
   * @generated
   * @ordered
   */
  protected static final TSIUnitEnum UNIT_EDEFAULT = TSIUnitEnum.S;

  /**
   * The cached value of the '{@link #getUnit() <em>Unit</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnit()
   * @generated
   * @ordered
   */
  protected TSIUnitEnum unit = UNIT_EDEFAULT;

  /**
   * This is true if the Unit attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean unitESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TDurationInMilliSecImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTDurationInMilliSec();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigDecimal getValue() {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValue(BigDecimal newValue) {
    BigDecimal oldValue = value;
    value = newValue;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TDURATION_IN_MILLI_SEC__VALUE, oldValue, value));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TUnitMultiplierEnum getMultiplier() {
    return multiplier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMultiplier(TUnitMultiplierEnum newMultiplier) {
    TUnitMultiplierEnum oldMultiplier = multiplier;
    multiplier = newMultiplier == null ? MULTIPLIER_EDEFAULT : newMultiplier;
    boolean oldMultiplierESet = multiplierESet;
    multiplierESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TDURATION_IN_MILLI_SEC__MULTIPLIER, oldMultiplier, multiplier, !oldMultiplierESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetMultiplier() {
    TUnitMultiplierEnum oldMultiplier = multiplier;
    boolean oldMultiplierESet = multiplierESet;
    multiplier = MULTIPLIER_EDEFAULT;
    multiplierESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TDURATION_IN_MILLI_SEC__MULTIPLIER, oldMultiplier, MULTIPLIER_EDEFAULT, oldMultiplierESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetMultiplier() {
    return multiplierESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSIUnitEnum getUnit() {
    return unit;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUnit(TSIUnitEnum newUnit) {
    TSIUnitEnum oldUnit = unit;
    unit = newUnit == null ? UNIT_EDEFAULT : newUnit;
    boolean oldUnitESet = unitESet;
    unitESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TDURATION_IN_MILLI_SEC__UNIT, oldUnit, unit, !oldUnitESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetUnit() {
    TSIUnitEnum oldUnit = unit;
    boolean oldUnitESet = unitESet;
    unit = UNIT_EDEFAULT;
    unitESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TDURATION_IN_MILLI_SEC__UNIT, oldUnit, UNIT_EDEFAULT, oldUnitESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetUnit() {
    return unitESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TDURATION_IN_MILLI_SEC__VALUE:
        return getValue();
      case SCLPackage.TDURATION_IN_MILLI_SEC__MULTIPLIER:
        return getMultiplier();
      case SCLPackage.TDURATION_IN_MILLI_SEC__UNIT:
        return getUnit();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TDURATION_IN_MILLI_SEC__VALUE:
        setValue((BigDecimal)newValue);
        return;
      case SCLPackage.TDURATION_IN_MILLI_SEC__MULTIPLIER:
        setMultiplier((TUnitMultiplierEnum)newValue);
        return;
      case SCLPackage.TDURATION_IN_MILLI_SEC__UNIT:
        setUnit((TSIUnitEnum)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TDURATION_IN_MILLI_SEC__VALUE:
        setValue(VALUE_EDEFAULT);
        return;
      case SCLPackage.TDURATION_IN_MILLI_SEC__MULTIPLIER:
        unsetMultiplier();
        return;
      case SCLPackage.TDURATION_IN_MILLI_SEC__UNIT:
        unsetUnit();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TDURATION_IN_MILLI_SEC__VALUE:
        return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
      case SCLPackage.TDURATION_IN_MILLI_SEC__MULTIPLIER:
        return isSetMultiplier();
      case SCLPackage.TDURATION_IN_MILLI_SEC__UNIT:
        return isSetUnit();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (value: ");
    result.append(value);
    result.append(", multiplier: ");
    if (multiplierESet) result.append(multiplier); else result.append("<unset>");
    result.append(", unit: ");
    if (unitESet) result.append(unit); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TDurationInMilliSecImpl
