/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.internal.SCLObjectImpl;

import com.lucy.g3.iec61850.model.scl.IEDNameType;
import com.lucy.g3.iec61850.model.scl.SCLPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IED Name Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.IEDNameTypeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.IEDNameTypeImpl#getApRef <em>Ap Ref</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.IEDNameTypeImpl#getLdInst <em>Ld Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.IEDNameTypeImpl#getLnClass <em>Ln Class</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.IEDNameTypeImpl#getLnInst <em>Ln Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.IEDNameTypeImpl#getPrefix <em>Prefix</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IEDNameTypeImpl extends SCLObjectImpl implements IEDNameType {
  /**
   * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected static final String VALUE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected String value = VALUE_EDEFAULT;

  /**
   * The default value of the '{@link #getApRef() <em>Ap Ref</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getApRef()
   * @generated
   * @ordered
   */
  protected static final String AP_REF_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getApRef() <em>Ap Ref</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getApRef()
   * @generated
   * @ordered
   */
  protected String apRef = AP_REF_EDEFAULT;

  /**
   * The default value of the '{@link #getLdInst() <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLdInst()
   * @generated
   * @ordered
   */
  protected static final String LD_INST_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLdInst() <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLdInst()
   * @generated
   * @ordered
   */
  protected String ldInst = LD_INST_EDEFAULT;

  /**
   * The default value of the '{@link #getLnClass() <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnClass()
   * @generated
   * @ordered
   */
  protected static final Object LN_CLASS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLnClass() <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnClass()
   * @generated
   * @ordered
   */
  protected Object lnClass = LN_CLASS_EDEFAULT;

  /**
   * The default value of the '{@link #getLnInst() <em>Ln Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnInst()
   * @generated
   * @ordered
   */
  protected static final String LN_INST_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLnInst() <em>Ln Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnInst()
   * @generated
   * @ordered
   */
  protected String lnInst = LN_INST_EDEFAULT;

  /**
   * The default value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrefix()
   * @generated
   * @ordered
   */
  protected static final String PREFIX_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrefix()
   * @generated
   * @ordered
   */
  protected String prefix = PREFIX_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IEDNameTypeImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getIEDNameType();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getValue() {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValue(String newValue) {
    String oldValue = value;
    value = newValue;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.IED_NAME_TYPE__VALUE, oldValue, value));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getApRef() {
    return apRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setApRef(String newApRef) {
    String oldApRef = apRef;
    apRef = newApRef;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.IED_NAME_TYPE__AP_REF, oldApRef, apRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLdInst() {
    return ldInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLdInst(String newLdInst) {
    String oldLdInst = ldInst;
    ldInst = newLdInst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.IED_NAME_TYPE__LD_INST, oldLdInst, ldInst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object getLnClass() {
    return lnClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLnClass(Object newLnClass) {
    Object oldLnClass = lnClass;
    lnClass = newLnClass;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.IED_NAME_TYPE__LN_CLASS, oldLnClass, lnClass));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLnInst() {
    return lnInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLnInst(String newLnInst) {
    String oldLnInst = lnInst;
    lnInst = newLnInst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.IED_NAME_TYPE__LN_INST, oldLnInst, lnInst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPrefix() {
    return prefix;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPrefix(String newPrefix) {
    String oldPrefix = prefix;
    prefix = newPrefix;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.IED_NAME_TYPE__PREFIX, oldPrefix, prefix));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.IED_NAME_TYPE__VALUE:
        return getValue();
      case SCLPackage.IED_NAME_TYPE__AP_REF:
        return getApRef();
      case SCLPackage.IED_NAME_TYPE__LD_INST:
        return getLdInst();
      case SCLPackage.IED_NAME_TYPE__LN_CLASS:
        return getLnClass();
      case SCLPackage.IED_NAME_TYPE__LN_INST:
        return getLnInst();
      case SCLPackage.IED_NAME_TYPE__PREFIX:
        return getPrefix();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.IED_NAME_TYPE__VALUE:
        setValue((String)newValue);
        return;
      case SCLPackage.IED_NAME_TYPE__AP_REF:
        setApRef((String)newValue);
        return;
      case SCLPackage.IED_NAME_TYPE__LD_INST:
        setLdInst((String)newValue);
        return;
      case SCLPackage.IED_NAME_TYPE__LN_CLASS:
        setLnClass(newValue);
        return;
      case SCLPackage.IED_NAME_TYPE__LN_INST:
        setLnInst((String)newValue);
        return;
      case SCLPackage.IED_NAME_TYPE__PREFIX:
        setPrefix((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.IED_NAME_TYPE__VALUE:
        setValue(VALUE_EDEFAULT);
        return;
      case SCLPackage.IED_NAME_TYPE__AP_REF:
        setApRef(AP_REF_EDEFAULT);
        return;
      case SCLPackage.IED_NAME_TYPE__LD_INST:
        setLdInst(LD_INST_EDEFAULT);
        return;
      case SCLPackage.IED_NAME_TYPE__LN_CLASS:
        setLnClass(LN_CLASS_EDEFAULT);
        return;
      case SCLPackage.IED_NAME_TYPE__LN_INST:
        setLnInst(LN_INST_EDEFAULT);
        return;
      case SCLPackage.IED_NAME_TYPE__PREFIX:
        setPrefix(PREFIX_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.IED_NAME_TYPE__VALUE:
        return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
      case SCLPackage.IED_NAME_TYPE__AP_REF:
        return AP_REF_EDEFAULT == null ? apRef != null : !AP_REF_EDEFAULT.equals(apRef);
      case SCLPackage.IED_NAME_TYPE__LD_INST:
        return LD_INST_EDEFAULT == null ? ldInst != null : !LD_INST_EDEFAULT.equals(ldInst);
      case SCLPackage.IED_NAME_TYPE__LN_CLASS:
        return LN_CLASS_EDEFAULT == null ? lnClass != null : !LN_CLASS_EDEFAULT.equals(lnClass);
      case SCLPackage.IED_NAME_TYPE__LN_INST:
        return LN_INST_EDEFAULT == null ? lnInst != null : !LN_INST_EDEFAULT.equals(lnInst);
      case SCLPackage.IED_NAME_TYPE__PREFIX:
        return PREFIX_EDEFAULT == null ? prefix != null : !PREFIX_EDEFAULT.equals(prefix);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (value: ");
    result.append(value);
    result.append(", apRef: ");
    result.append(apRef);
    result.append(", ldInst: ");
    result.append(ldInst);
    result.append(", lnClass: ");
    result.append(lnClass);
    result.append(", lnInst: ");
    result.append(lnInst);
    result.append(", prefix: ");
    result.append(prefix);
    result.append(')');
    return result.toString();
  }

} //IEDNameTypeImpl
