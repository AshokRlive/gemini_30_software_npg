/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TCertificate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TCertificate#getSubject <em>Subject</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TCertificate#getIssuerName <em>Issuer Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TCertificate#getSerialNumber <em>Serial Number</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TCertificate#getXferNumber <em>Xfer Number</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTCertificate()
 * @model extendedMetaData="name='tCertificate' kind='elementOnly'"
 * @generated
 */
public interface TCertificate extends TNaming {
  /**
   * Returns the value of the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subject</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subject</em>' containment reference.
   * @see #setSubject(TCert)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTCertificate_Subject()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='Subject' namespace='##targetNamespace'"
   * @generated
   */
  TCert getSubject();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TCertificate#getSubject <em>Subject</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Subject</em>' containment reference.
   * @see #getSubject()
   * @generated
   */
  void setSubject(TCert value);

  /**
   * Returns the value of the '<em><b>Issuer Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Issuer Name</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Issuer Name</em>' containment reference.
   * @see #setIssuerName(TCert)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTCertificate_IssuerName()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='IssuerName' namespace='##targetNamespace'"
   * @generated
   */
  TCert getIssuerName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TCertificate#getIssuerName <em>Issuer Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Issuer Name</em>' containment reference.
   * @see #getIssuerName()
   * @generated
   */
  void setIssuerName(TCert value);

  /**
   * Returns the value of the '<em><b>Serial Number</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Serial Number</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Serial Number</em>' attribute.
   * @see #setSerialNumber(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTCertificate_SerialNumber()
   * @model dataType="com.lucy.g3.iec61850.model.scl.SerialNumberType" required="true"
   *        extendedMetaData="kind='attribute' name='serialNumber'"
   * @generated
   */
  String getSerialNumber();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TCertificate#getSerialNumber <em>Serial Number</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Serial Number</em>' attribute.
   * @see #getSerialNumber()
   * @generated
   */
  void setSerialNumber(String value);

  /**
   * Returns the value of the '<em><b>Xfer Number</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Xfer Number</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Xfer Number</em>' attribute.
   * @see #isSetXferNumber()
   * @see #unsetXferNumber()
   * @see #setXferNumber(long)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTCertificate_XferNumber()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
   *        extendedMetaData="kind='attribute' name='xferNumber'"
   * @generated
   */
  long getXferNumber();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TCertificate#getXferNumber <em>Xfer Number</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Xfer Number</em>' attribute.
   * @see #isSetXferNumber()
   * @see #unsetXferNumber()
   * @see #getXferNumber()
   * @generated
   */
  void setXferNumber(long value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TCertificate#getXferNumber <em>Xfer Number</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetXferNumber()
   * @see #getXferNumber()
   * @see #setXferNumber(long)
   * @generated
   */
  void unsetXferNumber();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TCertificate#getXferNumber <em>Xfer Number</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Xfer Number</em>' attribute is set.
   * @see #unsetXferNumber()
   * @see #getXferNumber()
   * @see #setXferNumber(long)
   * @generated
   */
  boolean isSetXferNumber();

} // TCertificate
