/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.LN0Type;
import com.lucy.g3.iec61850.model.scl.SCLPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LN0 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LN0TypeImpl extends TLN0Impl implements LN0Type {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LN0TypeImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getLN0Type();
  }

} //LN0TypeImpl
