/**
 */
package com.lucy.g3.iec61850.model.scl.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61850.model.scl.util.SCLResourceFactoryImpl
 * @generated
 */
public class SCLResourceImpl extends XMLResourceImpl {
  /**
   * Creates an instance of the resource.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param uri the URI of the new resource.
   * @generated
   */
  public SCLResourceImpl(URI uri) {
    super(uri);
  }

} //SCLResourceImpl
