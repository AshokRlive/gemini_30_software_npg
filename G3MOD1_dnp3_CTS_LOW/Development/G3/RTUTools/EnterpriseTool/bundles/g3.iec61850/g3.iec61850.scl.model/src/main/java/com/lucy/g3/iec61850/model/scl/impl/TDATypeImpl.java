/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.ProtNsType;
import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TBDA;
import com.lucy.g3.iec61850.model.scl.TDAType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TDA Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TDATypeImpl#getBDA <em>BDA</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TDATypeImpl#getProtNs <em>Prot Ns</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TDATypeImpl#getIedType <em>Ied Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TDATypeImpl extends TIDNamingImpl implements TDAType {
  /**
   * The cached value of the '{@link #getBDA() <em>BDA</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBDA()
   * @generated
   * @ordered
   */
  protected EList<TBDA> bDA;

  /**
   * The cached value of the '{@link #getProtNs() <em>Prot Ns</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProtNs()
   * @generated
   * @ordered
   */
  protected EList<ProtNsType> protNs;

  /**
   * The default value of the '{@link #getIedType() <em>Ied Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIedType()
   * @generated
   * @ordered
   */
  protected static final String IED_TYPE_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getIedType() <em>Ied Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIedType()
   * @generated
   * @ordered
   */
  protected String iedType = IED_TYPE_EDEFAULT;

  /**
   * This is true if the Ied Type attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean iedTypeESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TDATypeImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTDAType();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TBDA> getBDA() {
    if (bDA == null) {
      bDA = new EObjectContainmentEList<TBDA>(TBDA.class, this, SCLPackage.TDA_TYPE__BDA);
    }
    return bDA;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ProtNsType> getProtNs() {
    if (protNs == null) {
      protNs = new EObjectContainmentEList<ProtNsType>(ProtNsType.class, this, SCLPackage.TDA_TYPE__PROT_NS);
    }
    return protNs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIedType() {
    return iedType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIedType(String newIedType) {
    String oldIedType = iedType;
    iedType = newIedType;
    boolean oldIedTypeESet = iedTypeESet;
    iedTypeESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TDA_TYPE__IED_TYPE, oldIedType, iedType, !oldIedTypeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetIedType() {
    String oldIedType = iedType;
    boolean oldIedTypeESet = iedTypeESet;
    iedType = IED_TYPE_EDEFAULT;
    iedTypeESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TDA_TYPE__IED_TYPE, oldIedType, IED_TYPE_EDEFAULT, oldIedTypeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetIedType() {
    return iedTypeESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case SCLPackage.TDA_TYPE__BDA:
        return ((InternalEList<?>)getBDA()).basicRemove(otherEnd, msgs);
      case SCLPackage.TDA_TYPE__PROT_NS:
        return ((InternalEList<?>)getProtNs()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TDA_TYPE__BDA:
        return getBDA();
      case SCLPackage.TDA_TYPE__PROT_NS:
        return getProtNs();
      case SCLPackage.TDA_TYPE__IED_TYPE:
        return getIedType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TDA_TYPE__BDA:
        getBDA().clear();
        getBDA().addAll((Collection<? extends TBDA>)newValue);
        return;
      case SCLPackage.TDA_TYPE__PROT_NS:
        getProtNs().clear();
        getProtNs().addAll((Collection<? extends ProtNsType>)newValue);
        return;
      case SCLPackage.TDA_TYPE__IED_TYPE:
        setIedType((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TDA_TYPE__BDA:
        getBDA().clear();
        return;
      case SCLPackage.TDA_TYPE__PROT_NS:
        getProtNs().clear();
        return;
      case SCLPackage.TDA_TYPE__IED_TYPE:
        unsetIedType();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TDA_TYPE__BDA:
        return bDA != null && !bDA.isEmpty();
      case SCLPackage.TDA_TYPE__PROT_NS:
        return protNs != null && !protNs.isEmpty();
      case SCLPackage.TDA_TYPE__IED_TYPE:
        return isSetIedType();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (iedType: ");
    if (iedTypeESet) result.append(iedType); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TDATypeImpl
