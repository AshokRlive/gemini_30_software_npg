/**
 */
package com.lucy.g3.iec61850.model.systemcorp.impl;

import com.lucy.g3.iec61850.model.internal.SCLObjectImpl;

import com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage;
import com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TGeneric Private Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.systemcorp.impl.TGenericPrivateObjectImpl#getField1 <em>Field1</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.systemcorp.impl.TGenericPrivateObjectImpl#getField2 <em>Field2</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.systemcorp.impl.TGenericPrivateObjectImpl#getField3 <em>Field3</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.systemcorp.impl.TGenericPrivateObjectImpl#getField4 <em>Field4</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.systemcorp.impl.TGenericPrivateObjectImpl#getField5 <em>Field5</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TGenericPrivateObjectImpl extends SCLObjectImpl implements TGenericPrivateObject {
  /**
   * The default value of the '{@link #getField1() <em>Field1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField1()
   * @generated
   * @ordered
   */
  protected static final long FIELD1_EDEFAULT = 0L;

  /**
   * The cached value of the '{@link #getField1() <em>Field1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField1()
   * @generated
   * @ordered
   */
  protected long field1 = FIELD1_EDEFAULT;

  /**
   * This is true if the Field1 attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean field1ESet;

  /**
   * The default value of the '{@link #getField2() <em>Field2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField2()
   * @generated
   * @ordered
   */
  protected static final long FIELD2_EDEFAULT = 0L;

  /**
   * The cached value of the '{@link #getField2() <em>Field2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField2()
   * @generated
   * @ordered
   */
  protected long field2 = FIELD2_EDEFAULT;

  /**
   * This is true if the Field2 attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean field2ESet;

  /**
   * The default value of the '{@link #getField3() <em>Field3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField3()
   * @generated
   * @ordered
   */
  protected static final long FIELD3_EDEFAULT = 0L;

  /**
   * The cached value of the '{@link #getField3() <em>Field3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField3()
   * @generated
   * @ordered
   */
  protected long field3 = FIELD3_EDEFAULT;

  /**
   * This is true if the Field3 attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean field3ESet;

  /**
   * The default value of the '{@link #getField4() <em>Field4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField4()
   * @generated
   * @ordered
   */
  protected static final long FIELD4_EDEFAULT = 0L;

  /**
   * The cached value of the '{@link #getField4() <em>Field4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField4()
   * @generated
   * @ordered
   */
  protected long field4 = FIELD4_EDEFAULT;

  /**
   * This is true if the Field4 attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean field4ESet;

  /**
   * The default value of the '{@link #getField5() <em>Field5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField5()
   * @generated
   * @ordered
   */
  protected static final long FIELD5_EDEFAULT = 0L;

  /**
   * The cached value of the '{@link #getField5() <em>Field5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField5()
   * @generated
   * @ordered
   */
  protected long field5 = FIELD5_EDEFAULT;

  /**
   * This is true if the Field5 attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean field5ESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TGenericPrivateObjectImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SystemCorpPackage.Literals.TGENERIC_PRIVATE_OBJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public long getField1() {
    return field1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField1(long newField1) {
    long oldField1 = field1;
    field1 = newField1;
    boolean oldField1ESet = field1ESet;
    field1ESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD1, oldField1, field1, !oldField1ESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetField1() {
    long oldField1 = field1;
    boolean oldField1ESet = field1ESet;
    field1 = FIELD1_EDEFAULT;
    field1ESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD1, oldField1, FIELD1_EDEFAULT, oldField1ESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetField1() {
    return field1ESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public long getField2() {
    return field2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField2(long newField2) {
    long oldField2 = field2;
    field2 = newField2;
    boolean oldField2ESet = field2ESet;
    field2ESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD2, oldField2, field2, !oldField2ESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetField2() {
    long oldField2 = field2;
    boolean oldField2ESet = field2ESet;
    field2 = FIELD2_EDEFAULT;
    field2ESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD2, oldField2, FIELD2_EDEFAULT, oldField2ESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetField2() {
    return field2ESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public long getField3() {
    return field3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField3(long newField3) {
    long oldField3 = field3;
    field3 = newField3;
    boolean oldField3ESet = field3ESet;
    field3ESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD3, oldField3, field3, !oldField3ESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetField3() {
    long oldField3 = field3;
    boolean oldField3ESet = field3ESet;
    field3 = FIELD3_EDEFAULT;
    field3ESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD3, oldField3, FIELD3_EDEFAULT, oldField3ESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetField3() {
    return field3ESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public long getField4() {
    return field4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField4(long newField4) {
    long oldField4 = field4;
    field4 = newField4;
    boolean oldField4ESet = field4ESet;
    field4ESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD4, oldField4, field4, !oldField4ESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetField4() {
    long oldField4 = field4;
    boolean oldField4ESet = field4ESet;
    field4 = FIELD4_EDEFAULT;
    field4ESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD4, oldField4, FIELD4_EDEFAULT, oldField4ESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetField4() {
    return field4ESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public long getField5() {
    return field5;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField5(long newField5) {
    long oldField5 = field5;
    field5 = newField5;
    boolean oldField5ESet = field5ESet;
    field5ESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD5, oldField5, field5, !oldField5ESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetField5() {
    long oldField5 = field5;
    boolean oldField5ESet = field5ESet;
    field5 = FIELD5_EDEFAULT;
    field5ESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD5, oldField5, FIELD5_EDEFAULT, oldField5ESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetField5() {
    return field5ESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD1:
        return getField1();
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD2:
        return getField2();
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD3:
        return getField3();
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD4:
        return getField4();
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD5:
        return getField5();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD1:
        setField1((Long)newValue);
        return;
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD2:
        setField2((Long)newValue);
        return;
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD3:
        setField3((Long)newValue);
        return;
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD4:
        setField4((Long)newValue);
        return;
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD5:
        setField5((Long)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD1:
        unsetField1();
        return;
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD2:
        unsetField2();
        return;
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD3:
        unsetField3();
        return;
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD4:
        unsetField4();
        return;
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD5:
        unsetField5();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD1:
        return isSetField1();
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD2:
        return isSetField2();
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD3:
        return isSetField3();
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD4:
        return isSetField4();
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT__FIELD5:
        return isSetField5();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (field1: ");
    if (field1ESet) result.append(field1); else result.append("<unset>");
    result.append(", field2: ");
    if (field2ESet) result.append(field2); else result.append("<unset>");
    result.append(", field3: ");
    if (field3ESet) result.append(field3); else result.append("<unset>");
    result.append(", field4: ");
    if (field4ESet) result.append(field4); else result.append("<unset>");
    result.append(", field5: ");
    if (field5ESet) result.append(field5); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TGenericPrivateObjectImpl
