/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TEnum Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TEnumType#getEnumVal <em>Enum Val</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTEnumType()
 * @model extendedMetaData="name='tEnumType' kind='elementOnly'"
 * @generated
 */
public interface TEnumType extends TIDNaming {
  /**
   * Returns the value of the '<em><b>Enum Val</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TEnumVal}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Enum Val</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Enum Val</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTEnumType_EnumVal()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='EnumVal' namespace='##targetNamespace'"
   * @generated
   */
  EList<TEnumVal> getEnumVal();

} // TEnumType
