/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TFCDA</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TFCDA#getDaName <em>Da Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TFCDA#getDoName <em>Do Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TFCDA#getFc <em>Fc</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TFCDA#getIx <em>Ix</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TFCDA#getLdInst <em>Ld Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TFCDA#getLnClass <em>Ln Class</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TFCDA#getLnInst <em>Ln Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TFCDA#getPrefix <em>Prefix</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTFCDA()
 * @model extendedMetaData="name='tFCDA' kind='empty'"
 * @extends SCLObject
 * @generated
 */
public interface TFCDA extends SCLObject {
  /**
   * Returns the value of the '<em><b>Da Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Da Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Da Name</em>' attribute.
   * @see #setDaName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTFCDA_DaName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TFullAttributeName"
   *        extendedMetaData="kind='attribute' name='daName'"
   * @generated
   */
  String getDaName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getDaName <em>Da Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Da Name</em>' attribute.
   * @see #getDaName()
   * @generated
   */
  void setDaName(String value);

  /**
   * Returns the value of the '<em><b>Do Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Do Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Do Name</em>' attribute.
   * @see #setDoName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTFCDA_DoName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TFullDOName"
   *        extendedMetaData="kind='attribute' name='doName'"
   * @generated
   */
  String getDoName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getDoName <em>Do Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Do Name</em>' attribute.
   * @see #getDoName()
   * @generated
   */
  void setDoName(String value);

  /**
   * Returns the value of the '<em><b>Fc</b></em>' attribute.
   * The literals are from the enumeration {@link com.lucy.g3.iec61850.model.scl.TFCEnum}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fc</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TFCEnum
   * @see #isSetFc()
   * @see #unsetFc()
   * @see #setFc(TFCEnum)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTFCDA_Fc()
   * @model unsettable="true" required="true"
   *        extendedMetaData="kind='attribute' name='fc'"
   * @generated
   */
  TFCEnum getFc();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getFc <em>Fc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fc</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TFCEnum
   * @see #isSetFc()
   * @see #unsetFc()
   * @see #getFc()
   * @generated
   */
  void setFc(TFCEnum value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getFc <em>Fc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetFc()
   * @see #getFc()
   * @see #setFc(TFCEnum)
   * @generated
   */
  void unsetFc();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getFc <em>Fc</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Fc</em>' attribute is set.
   * @see #unsetFc()
   * @see #getFc()
   * @see #setFc(TFCEnum)
   * @generated
   */
  boolean isSetFc();

  /**
   * Returns the value of the '<em><b>Ix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ix</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ix</em>' attribute.
   * @see #isSetIx()
   * @see #unsetIx()
   * @see #setIx(long)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTFCDA_Ix()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
   *        extendedMetaData="kind='attribute' name='ix'"
   * @generated
   */
  long getIx();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getIx <em>Ix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ix</em>' attribute.
   * @see #isSetIx()
   * @see #unsetIx()
   * @see #getIx()
   * @generated
   */
  void setIx(long value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getIx <em>Ix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetIx()
   * @see #getIx()
   * @see #setIx(long)
   * @generated
   */
  void unsetIx();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getIx <em>Ix</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Ix</em>' attribute is set.
   * @see #unsetIx()
   * @see #getIx()
   * @see #setIx(long)
   * @generated
   */
  boolean isSetIx();

  /**
   * Returns the value of the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ld Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ld Inst</em>' attribute.
   * @see #setLdInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTFCDA_LdInst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLDInst"
   *        extendedMetaData="kind='attribute' name='ldInst'"
   * @generated
   */
  String getLdInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getLdInst <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ld Inst</em>' attribute.
   * @see #getLdInst()
   * @generated
   */
  void setLdInst(String value);

  /**
   * Returns the value of the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ln Class</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ln Class</em>' attribute.
   * @see #setLnClass(Object)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTFCDA_LnClass()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLNClassEnum"
   *        extendedMetaData="kind='attribute' name='lnClass'"
   * @generated
   */
  Object getLnClass();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getLnClass <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ln Class</em>' attribute.
   * @see #getLnClass()
   * @generated
   */
  void setLnClass(Object value);

  /**
   * Returns the value of the '<em><b>Ln Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ln Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ln Inst</em>' attribute.
   * @see #setLnInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTFCDA_LnInst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLNInst"
   *        extendedMetaData="kind='attribute' name='lnInst'"
   * @generated
   */
  String getLnInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getLnInst <em>Ln Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ln Inst</em>' attribute.
   * @see #getLnInst()
   * @generated
   */
  void setLnInst(String value);

  /**
   * Returns the value of the '<em><b>Prefix</b></em>' attribute.
   * The default value is <code>"Lucy_"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prefix</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prefix</em>' attribute.
   * @see #isSetPrefix()
   * @see #unsetPrefix()
   * @see #setPrefix(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTFCDA_Prefix()
   * @model default="Lucy_" unsettable="true" dataType="com.lucy.g3.iec61850.model.scl.TPrefix"
   *        extendedMetaData="kind='attribute' name='prefix'"
   * @generated
   */
  String getPrefix();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getPrefix <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prefix</em>' attribute.
   * @see #isSetPrefix()
   * @see #unsetPrefix()
   * @see #getPrefix()
   * @generated
   */
  void setPrefix(String value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getPrefix <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetPrefix()
   * @see #getPrefix()
   * @see #setPrefix(String)
   * @generated
   */
  void unsetPrefix();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getPrefix <em>Prefix</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Prefix</em>' attribute is set.
   * @see #unsetPrefix()
   * @see #getPrefix()
   * @see #setPrefix(String)
   * @generated
   */
  boolean isSetPrefix();

} // TFCDA
