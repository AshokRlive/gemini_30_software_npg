/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TEnum Val</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TEnumVal#getValue <em>Value</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TEnumVal#getDesc <em>Desc</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TEnumVal#getOrd <em>Ord</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTEnumVal()
 * @model extendedMetaData="name='tEnumVal' kind='simple'"
 * @extends SCLObject
 * @generated
 */
public interface TEnumVal extends SCLObject {
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTEnumVal_Value()
   * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
   *        extendedMetaData="name=':0' kind='simple'"
   * @generated
   */
  String getValue();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TEnumVal#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(String value);

  /**
   * Returns the value of the '<em><b>Desc</b></em>' attribute.
   * The default value is <code>""</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Desc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Desc</em>' attribute.
   * @see #isSetDesc()
   * @see #unsetDesc()
   * @see #setDesc(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTEnumVal_Desc()
   * @model default="" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
   *        extendedMetaData="kind='attribute' name='desc'"
   * @generated
   */
  String getDesc();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TEnumVal#getDesc <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Desc</em>' attribute.
   * @see #isSetDesc()
   * @see #unsetDesc()
   * @see #getDesc()
   * @generated
   */
  void setDesc(String value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TEnumVal#getDesc <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetDesc()
   * @see #getDesc()
   * @see #setDesc(String)
   * @generated
   */
  void unsetDesc();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TEnumVal#getDesc <em>Desc</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Desc</em>' attribute is set.
   * @see #unsetDesc()
   * @see #getDesc()
   * @see #setDesc(String)
   * @generated
   */
  boolean isSetDesc();

  /**
   * Returns the value of the '<em><b>Ord</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ord</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ord</em>' attribute.
   * @see #isSetOrd()
   * @see #unsetOrd()
   * @see #setOrd(int)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTEnumVal_Ord()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int" required="true"
   *        extendedMetaData="kind='attribute' name='ord'"
   * @generated
   */
  int getOrd();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TEnumVal#getOrd <em>Ord</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ord</em>' attribute.
   * @see #isSetOrd()
   * @see #unsetOrd()
   * @see #getOrd()
   * @generated
   */
  void setOrd(int value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TEnumVal#getOrd <em>Ord</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetOrd()
   * @see #getOrd()
   * @see #setOrd(int)
   * @generated
   */
  void unsetOrd();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TEnumVal#getOrd <em>Ord</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Ord</em>' attribute is set.
   * @see #unsetOrd()
   * @see #getOrd()
   * @see #setOrd(int)
   * @generated
   */
  boolean isSetOrd();

} // TEnumVal
