/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPIP</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPIP()
 * @model extendedMetaData="name='tP_IP' kind='simple'"
 * @generated
 */
public interface TPIP extends TP {
} // TPIP
