/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping;

import com.lucy.g3.iec61850.model.internal.SCLObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mapping Root T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getEntry <em>Entry</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getSchemaVersionMajor <em>Schema Version Major</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getSchemaVersionMinor <em>Schema Version Minor</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getMappingRootT()
 * @model extendedMetaData="name='MappingRootT' kind='elementOnly'"
 * @extends SCLObject
 * @generated
 */
public interface MappingRootT extends SCLObject {
  /**
   * Returns the value of the '<em><b>Entry</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Entry</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Entry</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getMappingRootT_Entry()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='entry' namespace='##targetNamespace'"
   * @generated
   */
  EList<EntryT> getEntry();

  /**
   * Returns the value of the '<em><b>Schema Version Major</b></em>' attribute.
   * The default value is <code>"1"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Schema Version Major</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Schema Version Major</em>' attribute.
   * @see #isSetSchemaVersionMajor()
   * @see #unsetSchemaVersionMajor()
   * @see #setSchemaVersionMajor(short)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getMappingRootT_SchemaVersionMajor()
   * @model default="1" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedByte" required="true"
   *        extendedMetaData="kind='attribute' name='schemaVersionMajor' namespace='##targetNamespace'"
   * @generated
   */
  short getSchemaVersionMajor();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getSchemaVersionMajor <em>Schema Version Major</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Schema Version Major</em>' attribute.
   * @see #isSetSchemaVersionMajor()
   * @see #unsetSchemaVersionMajor()
   * @see #getSchemaVersionMajor()
   * @generated
   */
  void setSchemaVersionMajor(short value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getSchemaVersionMajor <em>Schema Version Major</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetSchemaVersionMajor()
   * @see #getSchemaVersionMajor()
   * @see #setSchemaVersionMajor(short)
   * @generated
   */
  void unsetSchemaVersionMajor();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getSchemaVersionMajor <em>Schema Version Major</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Schema Version Major</em>' attribute is set.
   * @see #unsetSchemaVersionMajor()
   * @see #getSchemaVersionMajor()
   * @see #setSchemaVersionMajor(short)
   * @generated
   */
  boolean isSetSchemaVersionMajor();

  /**
   * Returns the value of the '<em><b>Schema Version Minor</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Schema Version Minor</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Schema Version Minor</em>' attribute.
   * @see #isSetSchemaVersionMinor()
   * @see #unsetSchemaVersionMinor()
   * @see #setSchemaVersionMinor(short)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getMappingRootT_SchemaVersionMinor()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedByte" required="true"
   *        extendedMetaData="kind='attribute' name='schemaVersionMinor' namespace='##targetNamespace'"
   * @generated
   */
  short getSchemaVersionMinor();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getSchemaVersionMinor <em>Schema Version Minor</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Schema Version Minor</em>' attribute.
   * @see #isSetSchemaVersionMinor()
   * @see #unsetSchemaVersionMinor()
   * @see #getSchemaVersionMinor()
   * @generated
   */
  void setSchemaVersionMinor(short value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getSchemaVersionMinor <em>Schema Version Minor</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetSchemaVersionMinor()
   * @see #getSchemaVersionMinor()
   * @see #setSchemaVersionMinor(short)
   * @generated
   */
  void unsetSchemaVersionMinor();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getSchemaVersionMinor <em>Schema Version Minor</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Schema Version Minor</em>' attribute is set.
   * @see #unsetSchemaVersionMinor()
   * @see #getSchemaVersionMinor()
   * @see #setSchemaVersionMinor(short)
   * @generated
   */
  boolean isSetSchemaVersionMinor();

} // MappingRootT
