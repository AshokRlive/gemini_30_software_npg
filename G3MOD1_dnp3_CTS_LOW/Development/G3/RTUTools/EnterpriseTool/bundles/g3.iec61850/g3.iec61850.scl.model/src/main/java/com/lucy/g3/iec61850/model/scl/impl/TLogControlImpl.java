/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLFactory;
import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TLogControl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TLog Control</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TLogControlImpl#getBufTime <em>Buf Time</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TLogControlImpl#getLdInst <em>Ld Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TLogControlImpl#getLnClass <em>Ln Class</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TLogControlImpl#getLnInst <em>Ln Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TLogControlImpl#isLogEna <em>Log Ena</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TLogControlImpl#getLogName <em>Log Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TLogControlImpl#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TLogControlImpl#isReasonCode <em>Reason Code</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TLogControlImpl extends TControlWithTriggerOptImpl implements TLogControl {
  /**
   * The default value of the '{@link #getBufTime() <em>Buf Time</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBufTime()
   * @generated
   * @ordered
   */
  protected static final long BUF_TIME_EDEFAULT = 0L;

  /**
   * The cached value of the '{@link #getBufTime() <em>Buf Time</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBufTime()
   * @generated
   * @ordered
   */
  protected long bufTime = BUF_TIME_EDEFAULT;

  /**
   * This is true if the Buf Time attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean bufTimeESet;

  /**
   * The default value of the '{@link #getLdInst() <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLdInst()
   * @generated
   * @ordered
   */
  protected static final String LD_INST_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLdInst() <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLdInst()
   * @generated
   * @ordered
   */
  protected String ldInst = LD_INST_EDEFAULT;

  /**
   * The default value of the '{@link #getLnClass() <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnClass()
   * @generated
   * @ordered
   */
  protected static final Object LN_CLASS_EDEFAULT = SCLFactory.eINSTANCE.createFromString(SCLPackage.eINSTANCE.getTLNClassEnum(), "LLN0");

  /**
   * The cached value of the '{@link #getLnClass() <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnClass()
   * @generated
   * @ordered
   */
  protected Object lnClass = LN_CLASS_EDEFAULT;

  /**
   * This is true if the Ln Class attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean lnClassESet;

  /**
   * The default value of the '{@link #getLnInst() <em>Ln Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnInst()
   * @generated
   * @ordered
   */
  protected static final String LN_INST_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLnInst() <em>Ln Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnInst()
   * @generated
   * @ordered
   */
  protected String lnInst = LN_INST_EDEFAULT;

  /**
   * The default value of the '{@link #isLogEna() <em>Log Ena</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isLogEna()
   * @generated
   * @ordered
   */
  protected static final boolean LOG_ENA_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isLogEna() <em>Log Ena</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isLogEna()
   * @generated
   * @ordered
   */
  protected boolean logEna = LOG_ENA_EDEFAULT;

  /**
   * This is true if the Log Ena attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean logEnaESet;

  /**
   * The default value of the '{@link #getLogName() <em>Log Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLogName()
   * @generated
   * @ordered
   */
  protected static final String LOG_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLogName() <em>Log Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLogName()
   * @generated
   * @ordered
   */
  protected String logName = LOG_NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrefix()
   * @generated
   * @ordered
   */
  protected static final String PREFIX_EDEFAULT = "Lucy_";

  /**
   * The cached value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrefix()
   * @generated
   * @ordered
   */
  protected String prefix = PREFIX_EDEFAULT;

  /**
   * This is true if the Prefix attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean prefixESet;

  /**
   * The default value of the '{@link #isReasonCode() <em>Reason Code</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReasonCode()
   * @generated
   * @ordered
   */
  protected static final boolean REASON_CODE_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isReasonCode() <em>Reason Code</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReasonCode()
   * @generated
   * @ordered
   */
  protected boolean reasonCode = REASON_CODE_EDEFAULT;

  /**
   * This is true if the Reason Code attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean reasonCodeESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TLogControlImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTLogControl();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public long getBufTime() {
    return bufTime;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBufTime(long newBufTime) {
    long oldBufTime = bufTime;
    bufTime = newBufTime;
    boolean oldBufTimeESet = bufTimeESet;
    bufTimeESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TLOG_CONTROL__BUF_TIME, oldBufTime, bufTime, !oldBufTimeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetBufTime() {
    long oldBufTime = bufTime;
    boolean oldBufTimeESet = bufTimeESet;
    bufTime = BUF_TIME_EDEFAULT;
    bufTimeESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TLOG_CONTROL__BUF_TIME, oldBufTime, BUF_TIME_EDEFAULT, oldBufTimeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetBufTime() {
    return bufTimeESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLdInst() {
    return ldInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLdInst(String newLdInst) {
    String oldLdInst = ldInst;
    ldInst = newLdInst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TLOG_CONTROL__LD_INST, oldLdInst, ldInst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object getLnClass() {
    return lnClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLnClass(Object newLnClass) {
    Object oldLnClass = lnClass;
    lnClass = newLnClass;
    boolean oldLnClassESet = lnClassESet;
    lnClassESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TLOG_CONTROL__LN_CLASS, oldLnClass, lnClass, !oldLnClassESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetLnClass() {
    Object oldLnClass = lnClass;
    boolean oldLnClassESet = lnClassESet;
    lnClass = LN_CLASS_EDEFAULT;
    lnClassESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TLOG_CONTROL__LN_CLASS, oldLnClass, LN_CLASS_EDEFAULT, oldLnClassESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetLnClass() {
    return lnClassESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLnInst() {
    return lnInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLnInst(String newLnInst) {
    String oldLnInst = lnInst;
    lnInst = newLnInst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TLOG_CONTROL__LN_INST, oldLnInst, lnInst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isLogEna() {
    return logEna;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLogEna(boolean newLogEna) {
    boolean oldLogEna = logEna;
    logEna = newLogEna;
    boolean oldLogEnaESet = logEnaESet;
    logEnaESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TLOG_CONTROL__LOG_ENA, oldLogEna, logEna, !oldLogEnaESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetLogEna() {
    boolean oldLogEna = logEna;
    boolean oldLogEnaESet = logEnaESet;
    logEna = LOG_ENA_EDEFAULT;
    logEnaESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TLOG_CONTROL__LOG_ENA, oldLogEna, LOG_ENA_EDEFAULT, oldLogEnaESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetLogEna() {
    return logEnaESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLogName() {
    return logName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLogName(String newLogName) {
    String oldLogName = logName;
    logName = newLogName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TLOG_CONTROL__LOG_NAME, oldLogName, logName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPrefix() {
    return prefix;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPrefix(String newPrefix) {
    String oldPrefix = prefix;
    prefix = newPrefix;
    boolean oldPrefixESet = prefixESet;
    prefixESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TLOG_CONTROL__PREFIX, oldPrefix, prefix, !oldPrefixESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetPrefix() {
    String oldPrefix = prefix;
    boolean oldPrefixESet = prefixESet;
    prefix = PREFIX_EDEFAULT;
    prefixESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TLOG_CONTROL__PREFIX, oldPrefix, PREFIX_EDEFAULT, oldPrefixESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetPrefix() {
    return prefixESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isReasonCode() {
    return reasonCode;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReasonCode(boolean newReasonCode) {
    boolean oldReasonCode = reasonCode;
    reasonCode = newReasonCode;
    boolean oldReasonCodeESet = reasonCodeESet;
    reasonCodeESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TLOG_CONTROL__REASON_CODE, oldReasonCode, reasonCode, !oldReasonCodeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetReasonCode() {
    boolean oldReasonCode = reasonCode;
    boolean oldReasonCodeESet = reasonCodeESet;
    reasonCode = REASON_CODE_EDEFAULT;
    reasonCodeESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TLOG_CONTROL__REASON_CODE, oldReasonCode, REASON_CODE_EDEFAULT, oldReasonCodeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetReasonCode() {
    return reasonCodeESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TLOG_CONTROL__BUF_TIME:
        return getBufTime();
      case SCLPackage.TLOG_CONTROL__LD_INST:
        return getLdInst();
      case SCLPackage.TLOG_CONTROL__LN_CLASS:
        return getLnClass();
      case SCLPackage.TLOG_CONTROL__LN_INST:
        return getLnInst();
      case SCLPackage.TLOG_CONTROL__LOG_ENA:
        return isLogEna();
      case SCLPackage.TLOG_CONTROL__LOG_NAME:
        return getLogName();
      case SCLPackage.TLOG_CONTROL__PREFIX:
        return getPrefix();
      case SCLPackage.TLOG_CONTROL__REASON_CODE:
        return isReasonCode();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TLOG_CONTROL__BUF_TIME:
        setBufTime((Long)newValue);
        return;
      case SCLPackage.TLOG_CONTROL__LD_INST:
        setLdInst((String)newValue);
        return;
      case SCLPackage.TLOG_CONTROL__LN_CLASS:
        setLnClass(newValue);
        return;
      case SCLPackage.TLOG_CONTROL__LN_INST:
        setLnInst((String)newValue);
        return;
      case SCLPackage.TLOG_CONTROL__LOG_ENA:
        setLogEna((Boolean)newValue);
        return;
      case SCLPackage.TLOG_CONTROL__LOG_NAME:
        setLogName((String)newValue);
        return;
      case SCLPackage.TLOG_CONTROL__PREFIX:
        setPrefix((String)newValue);
        return;
      case SCLPackage.TLOG_CONTROL__REASON_CODE:
        setReasonCode((Boolean)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TLOG_CONTROL__BUF_TIME:
        unsetBufTime();
        return;
      case SCLPackage.TLOG_CONTROL__LD_INST:
        setLdInst(LD_INST_EDEFAULT);
        return;
      case SCLPackage.TLOG_CONTROL__LN_CLASS:
        unsetLnClass();
        return;
      case SCLPackage.TLOG_CONTROL__LN_INST:
        setLnInst(LN_INST_EDEFAULT);
        return;
      case SCLPackage.TLOG_CONTROL__LOG_ENA:
        unsetLogEna();
        return;
      case SCLPackage.TLOG_CONTROL__LOG_NAME:
        setLogName(LOG_NAME_EDEFAULT);
        return;
      case SCLPackage.TLOG_CONTROL__PREFIX:
        unsetPrefix();
        return;
      case SCLPackage.TLOG_CONTROL__REASON_CODE:
        unsetReasonCode();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TLOG_CONTROL__BUF_TIME:
        return isSetBufTime();
      case SCLPackage.TLOG_CONTROL__LD_INST:
        return LD_INST_EDEFAULT == null ? ldInst != null : !LD_INST_EDEFAULT.equals(ldInst);
      case SCLPackage.TLOG_CONTROL__LN_CLASS:
        return isSetLnClass();
      case SCLPackage.TLOG_CONTROL__LN_INST:
        return LN_INST_EDEFAULT == null ? lnInst != null : !LN_INST_EDEFAULT.equals(lnInst);
      case SCLPackage.TLOG_CONTROL__LOG_ENA:
        return isSetLogEna();
      case SCLPackage.TLOG_CONTROL__LOG_NAME:
        return LOG_NAME_EDEFAULT == null ? logName != null : !LOG_NAME_EDEFAULT.equals(logName);
      case SCLPackage.TLOG_CONTROL__PREFIX:
        return isSetPrefix();
      case SCLPackage.TLOG_CONTROL__REASON_CODE:
        return isSetReasonCode();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (bufTime: ");
    if (bufTimeESet) result.append(bufTime); else result.append("<unset>");
    result.append(", ldInst: ");
    result.append(ldInst);
    result.append(", lnClass: ");
    if (lnClassESet) result.append(lnClass); else result.append("<unset>");
    result.append(", lnInst: ");
    result.append(lnInst);
    result.append(", logEna: ");
    if (logEnaESet) result.append(logEna); else result.append("<unset>");
    result.append(", logName: ");
    result.append(logName);
    result.append(", prefix: ");
    if (prefixESet) result.append(prefix); else result.append("<unset>");
    result.append(", reasonCode: ");
    if (reasonCodeESet) result.append(reasonCode); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TLogControlImpl
