/**
 *
 * $Id$
 */
package com.lucy.g3.iec61850.model.scl.validation;

import com.lucy.g3.iec61850.model.scl.TDAType;
import com.lucy.g3.iec61850.model.scl.TDOType;
import com.lucy.g3.iec61850.model.scl.TEnumType;
import com.lucy.g3.iec61850.model.scl.TLNodeType;

import org.eclipse.emf.common.util.EList;

/**
 * A sample validator interface for {@link com.lucy.g3.iec61850.model.scl.TDataTypeTemplates}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface TDataTypeTemplatesValidator {
  boolean validate();

  boolean validateLNodeType(EList<TLNodeType> value);
  boolean validateDOType(EList<TDOType> value);
  boolean validateDAType(EList<TDAType> value);
  boolean validateEnumType(EList<TEnumType> value);
}
