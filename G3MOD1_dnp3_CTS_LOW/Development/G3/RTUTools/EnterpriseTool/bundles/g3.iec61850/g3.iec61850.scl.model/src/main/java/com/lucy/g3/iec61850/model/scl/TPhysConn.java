/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPhys Conn</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TPhysConn#getP <em>P</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TPhysConn#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPhysConn()
 * @model extendedMetaData="name='tPhysConn' kind='elementOnly'"
 * @generated
 */
public interface TPhysConn extends TUnNaming {
  /**
   * Returns the value of the '<em><b>P</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TPPhysConn}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>P</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>P</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPhysConn_P()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='P' namespace='##targetNamespace'"
   * @generated
   */
  EList<TPPhysConn> getP();

  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(Object)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPhysConn_Type()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TPhysConnTypeEnum" required="true"
   *        extendedMetaData="kind='attribute' name='type'"
   * @generated
   */
  Object getType();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TPhysConn#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(Object value);

} // TPhysConn
