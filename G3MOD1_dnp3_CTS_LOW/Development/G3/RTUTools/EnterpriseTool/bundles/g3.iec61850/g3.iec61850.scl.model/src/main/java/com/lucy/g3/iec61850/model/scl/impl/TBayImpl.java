/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TBay;
import com.lucy.g3.iec61850.model.scl.TConductingEquipment;
import com.lucy.g3.iec61850.model.scl.TConnectivityNode;
import com.lucy.g3.iec61850.model.scl.TFunction;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TBay</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TBayImpl#getConductingEquipment <em>Conducting Equipment</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TBayImpl#getConnectivityNode <em>Connectivity Node</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TBayImpl#getFunction <em>Function</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TBayImpl extends TEquipmentContainerImpl implements TBay {
  /**
   * The cached value of the '{@link #getConductingEquipment() <em>Conducting Equipment</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConductingEquipment()
   * @generated
   * @ordered
   */
  protected EList<TConductingEquipment> conductingEquipment;

  /**
   * The cached value of the '{@link #getConnectivityNode() <em>Connectivity Node</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConnectivityNode()
   * @generated
   * @ordered
   */
  protected EList<TConnectivityNode> connectivityNode;

  /**
   * The cached value of the '{@link #getFunction() <em>Function</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunction()
   * @generated
   * @ordered
   */
  protected EList<TFunction> function;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TBayImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTBay();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TConductingEquipment> getConductingEquipment() {
    if (conductingEquipment == null) {
      conductingEquipment = new EObjectContainmentEList<TConductingEquipment>(TConductingEquipment.class, this, SCLPackage.TBAY__CONDUCTING_EQUIPMENT);
    }
    return conductingEquipment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TConnectivityNode> getConnectivityNode() {
    if (connectivityNode == null) {
      connectivityNode = new EObjectContainmentEList<TConnectivityNode>(TConnectivityNode.class, this, SCLPackage.TBAY__CONNECTIVITY_NODE);
    }
    return connectivityNode;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TFunction> getFunction() {
    if (function == null) {
      function = new EObjectContainmentEList<TFunction>(TFunction.class, this, SCLPackage.TBAY__FUNCTION);
    }
    return function;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case SCLPackage.TBAY__CONDUCTING_EQUIPMENT:
        return ((InternalEList<?>)getConductingEquipment()).basicRemove(otherEnd, msgs);
      case SCLPackage.TBAY__CONNECTIVITY_NODE:
        return ((InternalEList<?>)getConnectivityNode()).basicRemove(otherEnd, msgs);
      case SCLPackage.TBAY__FUNCTION:
        return ((InternalEList<?>)getFunction()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TBAY__CONDUCTING_EQUIPMENT:
        return getConductingEquipment();
      case SCLPackage.TBAY__CONNECTIVITY_NODE:
        return getConnectivityNode();
      case SCLPackage.TBAY__FUNCTION:
        return getFunction();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TBAY__CONDUCTING_EQUIPMENT:
        getConductingEquipment().clear();
        getConductingEquipment().addAll((Collection<? extends TConductingEquipment>)newValue);
        return;
      case SCLPackage.TBAY__CONNECTIVITY_NODE:
        getConnectivityNode().clear();
        getConnectivityNode().addAll((Collection<? extends TConnectivityNode>)newValue);
        return;
      case SCLPackage.TBAY__FUNCTION:
        getFunction().clear();
        getFunction().addAll((Collection<? extends TFunction>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TBAY__CONDUCTING_EQUIPMENT:
        getConductingEquipment().clear();
        return;
      case SCLPackage.TBAY__CONNECTIVITY_NODE:
        getConnectivityNode().clear();
        return;
      case SCLPackage.TBAY__FUNCTION:
        getFunction().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TBAY__CONDUCTING_EQUIPMENT:
        return conductingEquipment != null && !conductingEquipment.isEmpty();
      case SCLPackage.TBAY__CONNECTIVITY_NODE:
        return connectivityNode != null && !connectivityNode.isEmpty();
      case SCLPackage.TBAY__FUNCTION:
        return function != null && !function.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //TBayImpl
