/**
 *
 * $Id$
 */
package com.lucy.g3.iec61850.model.scl.validation;


/**
 * A sample validator interface for {@link com.lucy.g3.iec61850.model.scl.AuthenticationType}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface AuthenticationTypeValidator {
  boolean validate();

  boolean validateCertificate(boolean value);
  boolean validateNone(boolean value);
  boolean validatePassword(boolean value);
  boolean validateStrong(boolean value);
  boolean validateWeak(boolean value);
}
