/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TBay;
import com.lucy.g3.iec61850.model.scl.TFunction;
import com.lucy.g3.iec61850.model.scl.TVoltage;
import com.lucy.g3.iec61850.model.scl.TVoltageLevel;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TVoltage Level</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TVoltageLevelImpl#getVoltage <em>Voltage</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TVoltageLevelImpl#getBay <em>Bay</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TVoltageLevelImpl#getFunction <em>Function</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TVoltageLevelImpl extends TEquipmentContainerImpl implements TVoltageLevel {
  /**
   * The cached value of the '{@link #getVoltage() <em>Voltage</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVoltage()
   * @generated
   * @ordered
   */
  protected TVoltage voltage;

  /**
   * The cached value of the '{@link #getBay() <em>Bay</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBay()
   * @generated
   * @ordered
   */
  protected EList<TBay> bay;

  /**
   * The cached value of the '{@link #getFunction() <em>Function</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunction()
   * @generated
   * @ordered
   */
  protected EList<TFunction> function;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TVoltageLevelImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTVoltageLevel();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TVoltage getVoltage() {
    return voltage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVoltage(TVoltage newVoltage, NotificationChain msgs) {
    TVoltage oldVoltage = voltage;
    voltage = newVoltage;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TVOLTAGE_LEVEL__VOLTAGE, oldVoltage, newVoltage);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVoltage(TVoltage newVoltage) {
    if (newVoltage != voltage) {
      NotificationChain msgs = null;
      if (voltage != null)
        msgs = ((InternalEObject)voltage).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TVOLTAGE_LEVEL__VOLTAGE, null, msgs);
      if (newVoltage != null)
        msgs = ((InternalEObject)newVoltage).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TVOLTAGE_LEVEL__VOLTAGE, null, msgs);
      msgs = basicSetVoltage(newVoltage, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TVOLTAGE_LEVEL__VOLTAGE, newVoltage, newVoltage));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TBay> getBay() {
    if (bay == null) {
      bay = new EObjectContainmentEList<TBay>(TBay.class, this, SCLPackage.TVOLTAGE_LEVEL__BAY);
    }
    return bay;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TFunction> getFunction() {
    if (function == null) {
      function = new EObjectContainmentEList<TFunction>(TFunction.class, this, SCLPackage.TVOLTAGE_LEVEL__FUNCTION);
    }
    return function;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case SCLPackage.TVOLTAGE_LEVEL__VOLTAGE:
        return basicSetVoltage(null, msgs);
      case SCLPackage.TVOLTAGE_LEVEL__BAY:
        return ((InternalEList<?>)getBay()).basicRemove(otherEnd, msgs);
      case SCLPackage.TVOLTAGE_LEVEL__FUNCTION:
        return ((InternalEList<?>)getFunction()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TVOLTAGE_LEVEL__VOLTAGE:
        return getVoltage();
      case SCLPackage.TVOLTAGE_LEVEL__BAY:
        return getBay();
      case SCLPackage.TVOLTAGE_LEVEL__FUNCTION:
        return getFunction();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TVOLTAGE_LEVEL__VOLTAGE:
        setVoltage((TVoltage)newValue);
        return;
      case SCLPackage.TVOLTAGE_LEVEL__BAY:
        getBay().clear();
        getBay().addAll((Collection<? extends TBay>)newValue);
        return;
      case SCLPackage.TVOLTAGE_LEVEL__FUNCTION:
        getFunction().clear();
        getFunction().addAll((Collection<? extends TFunction>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TVOLTAGE_LEVEL__VOLTAGE:
        setVoltage((TVoltage)null);
        return;
      case SCLPackage.TVOLTAGE_LEVEL__BAY:
        getBay().clear();
        return;
      case SCLPackage.TVOLTAGE_LEVEL__FUNCTION:
        getFunction().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TVOLTAGE_LEVEL__VOLTAGE:
        return voltage != null;
      case SCLPackage.TVOLTAGE_LEVEL__BAY:
        return bay != null && !bay.isEmpty();
      case SCLPackage.TVOLTAGE_LEVEL__FUNCTION:
        return function != null && !function.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //TVoltageLevelImpl
