/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61850.model.IEC61850Mapping.util.IEC61850MappingResourceFactoryImpl
 * @generated
 */
public class IEC61850MappingResourceImpl extends XMLResourceImpl {
  /**
   * Creates an instance of the resource.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param uri the URI of the new resource.
   * @generated
   */
  public IEC61850MappingResourceImpl(URI uri) {
    super(uri);
  }

} //IEC61850MappingResourceImpl
