/**
 */
package com.lucy.g3.iec61850.model.systemcorp.util;

import com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SystemCorpXMLProcessor extends XMLProcessor {

  /**
   * Public constructor to instantiate the helper.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SystemCorpXMLProcessor() {
    super((EPackage.Registry.INSTANCE));
    SystemCorpPackage.eINSTANCE.eClass();
  }
  
  /**
   * Register for "*" and "xml" file extensions the SystemCorpResourceFactoryImpl factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected Map<String, Resource.Factory> getRegistrations() {
    if (registrations == null) {
      super.getRegistrations();
      registrations.put(XML_EXTENSION, new SystemCorpResourceFactoryImpl());
      registrations.put(STAR_EXTENSION, new SystemCorpResourceFactoryImpl());
    }
    return registrations;
  }

} //SystemCorpXMLProcessor
