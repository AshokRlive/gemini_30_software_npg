/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TControl With IED Name</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TControlWithIEDName#getIEDName <em>IED Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TControlWithIEDName#getConfRev <em>Conf Rev</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTControlWithIEDName()
 * @model extendedMetaData="name='tControlWithIEDName' kind='elementOnly'"
 * @generated
 */
public interface TControlWithIEDName extends TControl {
  /**
   * Returns the value of the '<em><b>IED Name</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.IEDNameType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>IED Name</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>IED Name</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTControlWithIEDName_IEDName()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='IEDName' namespace='##targetNamespace'"
   * @generated
   */
  EList<IEDNameType> getIEDName();

  /**
   * Returns the value of the '<em><b>Conf Rev</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Conf Rev</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Conf Rev</em>' attribute.
   * @see #isSetConfRev()
   * @see #unsetConfRev()
   * @see #setConfRev(long)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTControlWithIEDName_ConfRev()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
   *        extendedMetaData="kind='attribute' name='confRev'"
   * @generated
   */
  long getConfRev();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TControlWithIEDName#getConfRev <em>Conf Rev</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Conf Rev</em>' attribute.
   * @see #isSetConfRev()
   * @see #unsetConfRev()
   * @see #getConfRev()
   * @generated
   */
  void setConfRev(long value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TControlWithIEDName#getConfRev <em>Conf Rev</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetConfRev()
   * @see #getConfRev()
   * @see #setConfRev(long)
   * @generated
   */
  void unsetConfRev();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TControlWithIEDName#getConfRev <em>Conf Rev</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Conf Rev</em>' attribute is set.
   * @see #unsetConfRev()
   * @see #getConfRev()
   * @see #setConfRev(long)
   * @generated
   */
  boolean isSetConfRev();

} // TControlWithIEDName
