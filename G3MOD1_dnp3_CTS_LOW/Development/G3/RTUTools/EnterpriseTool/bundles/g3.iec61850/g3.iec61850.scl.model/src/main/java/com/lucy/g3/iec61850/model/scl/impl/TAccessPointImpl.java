/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TAccessPoint;
import com.lucy.g3.iec61850.model.scl.TCertificate;
import com.lucy.g3.iec61850.model.scl.TLN;
import com.lucy.g3.iec61850.model.scl.TServer;
import com.lucy.g3.iec61850.model.scl.TServerAt;
import com.lucy.g3.iec61850.model.scl.TServices;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TAccess Point</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAccessPointImpl#getServer <em>Server</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAccessPointImpl#getLN <em>LN</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAccessPointImpl#getServerAt <em>Server At</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAccessPointImpl#getServices <em>Services</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAccessPointImpl#getGOOSESecurity <em>GOOSE Security</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAccessPointImpl#getSMVSecurity <em>SMV Security</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAccessPointImpl#isClock <em>Clock</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAccessPointImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAccessPointImpl#isRouter <em>Router</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TAccessPointImpl extends TUnNamingImpl implements TAccessPoint {
  /**
   * The cached value of the '{@link #getServer() <em>Server</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getServer()
   * @generated
   * @ordered
   */
  protected TServer server;

  /**
   * The cached value of the '{@link #getLN() <em>LN</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLN()
   * @generated
   * @ordered
   */
  protected EList<TLN> lN;

  /**
   * The cached value of the '{@link #getServerAt() <em>Server At</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getServerAt()
   * @generated
   * @ordered
   */
  protected TServerAt serverAt;

  /**
   * The cached value of the '{@link #getServices() <em>Services</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getServices()
   * @generated
   * @ordered
   */
  protected TServices services;

  /**
   * The cached value of the '{@link #getGOOSESecurity() <em>GOOSE Security</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGOOSESecurity()
   * @generated
   * @ordered
   */
  protected EList<TCertificate> gOOSESecurity;

  /**
   * The cached value of the '{@link #getSMVSecurity() <em>SMV Security</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSMVSecurity()
   * @generated
   * @ordered
   */
  protected EList<TCertificate> sMVSecurity;

  /**
   * The default value of the '{@link #isClock() <em>Clock</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isClock()
   * @generated
   * @ordered
   */
  protected static final boolean CLOCK_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isClock() <em>Clock</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isClock()
   * @generated
   * @ordered
   */
  protected boolean clock = CLOCK_EDEFAULT;

  /**
   * This is true if the Clock attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean clockESet;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #isRouter() <em>Router</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isRouter()
   * @generated
   * @ordered
   */
  protected static final boolean ROUTER_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isRouter() <em>Router</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isRouter()
   * @generated
   * @ordered
   */
  protected boolean router = ROUTER_EDEFAULT;

  /**
   * This is true if the Router attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean routerESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TAccessPointImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTAccessPoint();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServer getServer() {
    return server;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetServer(TServer newServer, NotificationChain msgs) {
    TServer oldServer = server;
    server = newServer;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TACCESS_POINT__SERVER, oldServer, newServer);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setServer(TServer newServer) {
    if (newServer != server) {
      NotificationChain msgs = null;
      if (server != null)
        msgs = ((InternalEObject)server).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TACCESS_POINT__SERVER, null, msgs);
      if (newServer != null)
        msgs = ((InternalEObject)newServer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TACCESS_POINT__SERVER, null, msgs);
      msgs = basicSetServer(newServer, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TACCESS_POINT__SERVER, newServer, newServer));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TLN> getLN() {
    if (lN == null) {
      lN = new EObjectContainmentEList<TLN>(TLN.class, this, SCLPackage.TACCESS_POINT__LN);
    }
    return lN;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServerAt getServerAt() {
    return serverAt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetServerAt(TServerAt newServerAt, NotificationChain msgs) {
    TServerAt oldServerAt = serverAt;
    serverAt = newServerAt;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TACCESS_POINT__SERVER_AT, oldServerAt, newServerAt);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setServerAt(TServerAt newServerAt) {
    if (newServerAt != serverAt) {
      NotificationChain msgs = null;
      if (serverAt != null)
        msgs = ((InternalEObject)serverAt).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TACCESS_POINT__SERVER_AT, null, msgs);
      if (newServerAt != null)
        msgs = ((InternalEObject)newServerAt).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TACCESS_POINT__SERVER_AT, null, msgs);
      msgs = basicSetServerAt(newServerAt, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TACCESS_POINT__SERVER_AT, newServerAt, newServerAt));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServices getServices() {
    return services;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetServices(TServices newServices, NotificationChain msgs) {
    TServices oldServices = services;
    services = newServices;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TACCESS_POINT__SERVICES, oldServices, newServices);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setServices(TServices newServices) {
    if (newServices != services) {
      NotificationChain msgs = null;
      if (services != null)
        msgs = ((InternalEObject)services).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TACCESS_POINT__SERVICES, null, msgs);
      if (newServices != null)
        msgs = ((InternalEObject)newServices).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TACCESS_POINT__SERVICES, null, msgs);
      msgs = basicSetServices(newServices, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TACCESS_POINT__SERVICES, newServices, newServices));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TCertificate> getGOOSESecurity() {
    if (gOOSESecurity == null) {
      gOOSESecurity = new EObjectContainmentEList<TCertificate>(TCertificate.class, this, SCLPackage.TACCESS_POINT__GOOSE_SECURITY);
    }
    return gOOSESecurity;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TCertificate> getSMVSecurity() {
    if (sMVSecurity == null) {
      sMVSecurity = new EObjectContainmentEList<TCertificate>(TCertificate.class, this, SCLPackage.TACCESS_POINT__SMV_SECURITY);
    }
    return sMVSecurity;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isClock() {
    return clock;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setClock(boolean newClock) {
    boolean oldClock = clock;
    clock = newClock;
    boolean oldClockESet = clockESet;
    clockESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TACCESS_POINT__CLOCK, oldClock, clock, !oldClockESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetClock() {
    boolean oldClock = clock;
    boolean oldClockESet = clockESet;
    clock = CLOCK_EDEFAULT;
    clockESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TACCESS_POINT__CLOCK, oldClock, CLOCK_EDEFAULT, oldClockESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetClock() {
    return clockESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName) {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TACCESS_POINT__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isRouter() {
    return router;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRouter(boolean newRouter) {
    boolean oldRouter = router;
    router = newRouter;
    boolean oldRouterESet = routerESet;
    routerESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TACCESS_POINT__ROUTER, oldRouter, router, !oldRouterESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetRouter() {
    boolean oldRouter = router;
    boolean oldRouterESet = routerESet;
    router = ROUTER_EDEFAULT;
    routerESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TACCESS_POINT__ROUTER, oldRouter, ROUTER_EDEFAULT, oldRouterESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetRouter() {
    return routerESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case SCLPackage.TACCESS_POINT__SERVER:
        return basicSetServer(null, msgs);
      case SCLPackage.TACCESS_POINT__LN:
        return ((InternalEList<?>)getLN()).basicRemove(otherEnd, msgs);
      case SCLPackage.TACCESS_POINT__SERVER_AT:
        return basicSetServerAt(null, msgs);
      case SCLPackage.TACCESS_POINT__SERVICES:
        return basicSetServices(null, msgs);
      case SCLPackage.TACCESS_POINT__GOOSE_SECURITY:
        return ((InternalEList<?>)getGOOSESecurity()).basicRemove(otherEnd, msgs);
      case SCLPackage.TACCESS_POINT__SMV_SECURITY:
        return ((InternalEList<?>)getSMVSecurity()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TACCESS_POINT__SERVER:
        return getServer();
      case SCLPackage.TACCESS_POINT__LN:
        return getLN();
      case SCLPackage.TACCESS_POINT__SERVER_AT:
        return getServerAt();
      case SCLPackage.TACCESS_POINT__SERVICES:
        return getServices();
      case SCLPackage.TACCESS_POINT__GOOSE_SECURITY:
        return getGOOSESecurity();
      case SCLPackage.TACCESS_POINT__SMV_SECURITY:
        return getSMVSecurity();
      case SCLPackage.TACCESS_POINT__CLOCK:
        return isClock();
      case SCLPackage.TACCESS_POINT__NAME:
        return getName();
      case SCLPackage.TACCESS_POINT__ROUTER:
        return isRouter();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TACCESS_POINT__SERVER:
        setServer((TServer)newValue);
        return;
      case SCLPackage.TACCESS_POINT__LN:
        getLN().clear();
        getLN().addAll((Collection<? extends TLN>)newValue);
        return;
      case SCLPackage.TACCESS_POINT__SERVER_AT:
        setServerAt((TServerAt)newValue);
        return;
      case SCLPackage.TACCESS_POINT__SERVICES:
        setServices((TServices)newValue);
        return;
      case SCLPackage.TACCESS_POINT__GOOSE_SECURITY:
        getGOOSESecurity().clear();
        getGOOSESecurity().addAll((Collection<? extends TCertificate>)newValue);
        return;
      case SCLPackage.TACCESS_POINT__SMV_SECURITY:
        getSMVSecurity().clear();
        getSMVSecurity().addAll((Collection<? extends TCertificate>)newValue);
        return;
      case SCLPackage.TACCESS_POINT__CLOCK:
        setClock((Boolean)newValue);
        return;
      case SCLPackage.TACCESS_POINT__NAME:
        setName((String)newValue);
        return;
      case SCLPackage.TACCESS_POINT__ROUTER:
        setRouter((Boolean)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TACCESS_POINT__SERVER:
        setServer((TServer)null);
        return;
      case SCLPackage.TACCESS_POINT__LN:
        getLN().clear();
        return;
      case SCLPackage.TACCESS_POINT__SERVER_AT:
        setServerAt((TServerAt)null);
        return;
      case SCLPackage.TACCESS_POINT__SERVICES:
        setServices((TServices)null);
        return;
      case SCLPackage.TACCESS_POINT__GOOSE_SECURITY:
        getGOOSESecurity().clear();
        return;
      case SCLPackage.TACCESS_POINT__SMV_SECURITY:
        getSMVSecurity().clear();
        return;
      case SCLPackage.TACCESS_POINT__CLOCK:
        unsetClock();
        return;
      case SCLPackage.TACCESS_POINT__NAME:
        setName(NAME_EDEFAULT);
        return;
      case SCLPackage.TACCESS_POINT__ROUTER:
        unsetRouter();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TACCESS_POINT__SERVER:
        return server != null;
      case SCLPackage.TACCESS_POINT__LN:
        return lN != null && !lN.isEmpty();
      case SCLPackage.TACCESS_POINT__SERVER_AT:
        return serverAt != null;
      case SCLPackage.TACCESS_POINT__SERVICES:
        return services != null;
      case SCLPackage.TACCESS_POINT__GOOSE_SECURITY:
        return gOOSESecurity != null && !gOOSESecurity.isEmpty();
      case SCLPackage.TACCESS_POINT__SMV_SECURITY:
        return sMVSecurity != null && !sMVSecurity.isEmpty();
      case SCLPackage.TACCESS_POINT__CLOCK:
        return isSetClock();
      case SCLPackage.TACCESS_POINT__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case SCLPackage.TACCESS_POINT__ROUTER:
        return isSetRouter();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (clock: ");
    if (clockESet) result.append(clock); else result.append("<unset>");
    result.append(", name: ");
    result.append(name);
    result.append(", router: ");
    if (routerESet) result.append(router); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TAccessPointImpl
