/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping;

import com.lucy.g3.iec61850.model.internal.SCLObject;

import java.math.BigInteger;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entry T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getG3Ref <em>G3 Ref</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getDataAttributeId <em>Data Attribute Id</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getAttributeRef <em>Attribute Ref</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getUuid <em>Uuid</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getDataType <em>Data Type</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getEntryT()
 * @model extendedMetaData="name='EntryT' kind='elementOnly'"
 * @extends SCLObject
 * @generated
 */
public interface EntryT extends SCLObject {
  /**
   * Returns the value of the '<em><b>G3 Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>G3 Ref</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>G3 Ref</em>' containment reference.
   * @see #setG3Ref(G3RefT)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getEntryT_G3Ref()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='g3Ref' namespace='##targetNamespace'"
   * @generated
   */
  G3RefT getG3Ref();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getG3Ref <em>G3 Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>G3 Ref</em>' containment reference.
   * @see #getG3Ref()
   * @generated
   */
  void setG3Ref(G3RefT value);

  /**
   * Returns the value of the '<em><b>Data Attribute Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Data Attribute Id</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Data Attribute Id</em>' containment reference.
   * @see #setDataAttributeId(DataAttributeIdT)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getEntryT_DataAttributeId()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='dataAttributeId' namespace='##targetNamespace'"
   * @generated
   */
  DataAttributeIdT getDataAttributeId();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getDataAttributeId <em>Data Attribute Id</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Data Attribute Id</em>' containment reference.
   * @see #getDataAttributeId()
   * @generated
   */
  void setDataAttributeId(DataAttributeIdT value);

  /**
   * Returns the value of the '<em><b>Attribute Ref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Attribute Ref</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Attribute Ref</em>' attribute.
   * @see #setAttributeRef(String)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getEntryT_AttributeRef()
   * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   *        extendedMetaData="kind='attribute' name='attributeRef' namespace='##targetNamespace'"
   * @generated
   */
  String getAttributeRef();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getAttributeRef <em>Attribute Ref</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Attribute Ref</em>' attribute.
   * @see #getAttributeRef()
   * @generated
   */
  void setAttributeRef(String value);

  /**
   * Returns the value of the '<em><b>Uuid</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uuid</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uuid</em>' attribute.
   * @see #setUuid(String)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getEntryT_Uuid()
   * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   *        extendedMetaData="kind='attribute' name='attributeRef' namespace='##targetNamespace'"
   * @generated
   */
  String getUuid();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getUuid <em>Uuid</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uuid</em>' attribute.
   * @see #getUuid()
   * @generated
   */
  void setUuid(String value);

  /**
   * Returns the value of the '<em><b>Data Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Data Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Data Type</em>' attribute.
   * @see #setDataType(BigInteger)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getEntryT_DataType()
   * @model dataType="org.eclipse.emf.ecore.xml.type.Integer" required="true"
   *        extendedMetaData="kind='attribute' name='dataType' namespace='##targetNamespace'"
   * @generated
   */
  BigInteger getDataType();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getDataType <em>Data Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Data Type</em>' attribute.
   * @see #getDataType()
   * @generated
   */
  void setDataType(BigInteger value);

} // EntryT
