/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TServiceForConfDataSet;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TService For Conf Data Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServiceForConfDataSetImpl#isModify <em>Modify</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TServiceForConfDataSetImpl extends TServiceWithMaxAndMaxAttributesImpl implements TServiceForConfDataSet {
  /**
   * The default value of the '{@link #isModify() <em>Modify</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isModify()
   * @generated
   * @ordered
   */
  protected static final boolean MODIFY_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isModify() <em>Modify</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isModify()
   * @generated
   * @ordered
   */
  protected boolean modify = MODIFY_EDEFAULT;

  /**
   * This is true if the Modify attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean modifyESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TServiceForConfDataSetImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTServiceForConfDataSet();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isModify() {
    return modify;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModify(boolean newModify) {
    boolean oldModify = modify;
    modify = newModify;
    boolean oldModifyESet = modifyESet;
    modifyESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICE_FOR_CONF_DATA_SET__MODIFY, oldModify, modify, !oldModifyESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetModify() {
    boolean oldModify = modify;
    boolean oldModifyESet = modifyESet;
    modify = MODIFY_EDEFAULT;
    modifyESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TSERVICE_FOR_CONF_DATA_SET__MODIFY, oldModify, MODIFY_EDEFAULT, oldModifyESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetModify() {
    return modifyESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TSERVICE_FOR_CONF_DATA_SET__MODIFY:
        return isModify();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TSERVICE_FOR_CONF_DATA_SET__MODIFY:
        setModify((Boolean)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TSERVICE_FOR_CONF_DATA_SET__MODIFY:
        unsetModify();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TSERVICE_FOR_CONF_DATA_SET__MODIFY:
        return isSetModify();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (modify: ");
    if (modifyESet) result.append(modify); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TServiceForConfDataSetImpl
