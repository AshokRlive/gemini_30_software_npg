/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TDO;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TDO</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TDOImpl#getAccessControl <em>Access Control</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TDOImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TDOImpl#isTransient <em>Transient</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TDOImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TDOImpl extends TUnNamingImpl implements TDO {
  /**
   * The default value of the '{@link #getAccessControl() <em>Access Control</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAccessControl()
   * @generated
   * @ordered
   */
  protected static final String ACCESS_CONTROL_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAccessControl() <em>Access Control</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAccessControl()
   * @generated
   * @ordered
   */
  protected String accessControl = ACCESS_CONTROL_EDEFAULT;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #isTransient() <em>Transient</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isTransient()
   * @generated
   * @ordered
   */
  protected static final boolean TRANSIENT_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isTransient() <em>Transient</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isTransient()
   * @generated
   * @ordered
   */
  protected boolean transient_ = TRANSIENT_EDEFAULT;

  /**
   * This is true if the Transient attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean transientESet;

  /**
   * The default value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected static final String TYPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected String type = TYPE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TDOImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTDO();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAccessControl() {
    return accessControl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAccessControl(String newAccessControl) {
    String oldAccessControl = accessControl;
    accessControl = newAccessControl;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TDO__ACCESS_CONTROL, oldAccessControl, accessControl));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName) {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TDO__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isTransient() {
    return transient_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTransient(boolean newTransient) {
    boolean oldTransient = transient_;
    transient_ = newTransient;
    boolean oldTransientESet = transientESet;
    transientESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TDO__TRANSIENT, oldTransient, transient_, !oldTransientESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetTransient() {
    boolean oldTransient = transient_;
    boolean oldTransientESet = transientESet;
    transient_ = TRANSIENT_EDEFAULT;
    transientESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TDO__TRANSIENT, oldTransient, TRANSIENT_EDEFAULT, oldTransientESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetTransient() {
    return transientESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getType() {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(String newType) {
    String oldType = type;
    type = newType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TDO__TYPE, oldType, type));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TDO__ACCESS_CONTROL:
        return getAccessControl();
      case SCLPackage.TDO__NAME:
        return getName();
      case SCLPackage.TDO__TRANSIENT:
        return isTransient();
      case SCLPackage.TDO__TYPE:
        return getType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TDO__ACCESS_CONTROL:
        setAccessControl((String)newValue);
        return;
      case SCLPackage.TDO__NAME:
        setName((String)newValue);
        return;
      case SCLPackage.TDO__TRANSIENT:
        setTransient((Boolean)newValue);
        return;
      case SCLPackage.TDO__TYPE:
        setType((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TDO__ACCESS_CONTROL:
        setAccessControl(ACCESS_CONTROL_EDEFAULT);
        return;
      case SCLPackage.TDO__NAME:
        setName(NAME_EDEFAULT);
        return;
      case SCLPackage.TDO__TRANSIENT:
        unsetTransient();
        return;
      case SCLPackage.TDO__TYPE:
        setType(TYPE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TDO__ACCESS_CONTROL:
        return ACCESS_CONTROL_EDEFAULT == null ? accessControl != null : !ACCESS_CONTROL_EDEFAULT.equals(accessControl);
      case SCLPackage.TDO__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case SCLPackage.TDO__TRANSIENT:
        return isSetTransient();
      case SCLPackage.TDO__TYPE:
        return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (accessControl: ");
    result.append(accessControl);
    result.append(", name: ");
    result.append(name);
    result.append(", transient: ");
    if (transientESet) result.append(transient_); else result.append("<unset>");
    result.append(", type: ");
    result.append(type);
    result.append(')');
    return result.toString();
  }

} //TDOImpl
