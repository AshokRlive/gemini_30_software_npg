/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TGSEControl;
import com.lucy.g3.iec61850.model.scl.TGSEControlTypeEnum;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TGSE Control</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TGSEControlImpl#getAppID <em>App ID</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TGSEControlImpl#isFixedOffs <em>Fixed Offs</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TGSEControlImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TGSEControlImpl extends TControlWithIEDNameImpl implements TGSEControl {
  /**
   * The default value of the '{@link #getAppID() <em>App ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAppID()
   * @generated
   * @ordered
   */
  protected static final String APP_ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAppID() <em>App ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAppID()
   * @generated
   * @ordered
   */
  protected String appID = APP_ID_EDEFAULT;

  /**
   * The default value of the '{@link #isFixedOffs() <em>Fixed Offs</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isFixedOffs()
   * @generated
   * @ordered
   */
  protected static final boolean FIXED_OFFS_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isFixedOffs() <em>Fixed Offs</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isFixedOffs()
   * @generated
   * @ordered
   */
  protected boolean fixedOffs = FIXED_OFFS_EDEFAULT;

  /**
   * This is true if the Fixed Offs attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean fixedOffsESet;

  /**
   * The default value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected static final TGSEControlTypeEnum TYPE_EDEFAULT = TGSEControlTypeEnum.GOOSE;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected TGSEControlTypeEnum type = TYPE_EDEFAULT;

  /**
   * This is true if the Type attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean typeESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TGSEControlImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTGSEControl();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAppID() {
    return appID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAppID(String newAppID) {
    String oldAppID = appID;
    appID = newAppID;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TGSE_CONTROL__APP_ID, oldAppID, appID));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isFixedOffs() {
    return fixedOffs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFixedOffs(boolean newFixedOffs) {
    boolean oldFixedOffs = fixedOffs;
    fixedOffs = newFixedOffs;
    boolean oldFixedOffsESet = fixedOffsESet;
    fixedOffsESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TGSE_CONTROL__FIXED_OFFS, oldFixedOffs, fixedOffs, !oldFixedOffsESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetFixedOffs() {
    boolean oldFixedOffs = fixedOffs;
    boolean oldFixedOffsESet = fixedOffsESet;
    fixedOffs = FIXED_OFFS_EDEFAULT;
    fixedOffsESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TGSE_CONTROL__FIXED_OFFS, oldFixedOffs, FIXED_OFFS_EDEFAULT, oldFixedOffsESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetFixedOffs() {
    return fixedOffsESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TGSEControlTypeEnum getType() {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(TGSEControlTypeEnum newType) {
    TGSEControlTypeEnum oldType = type;
    type = newType == null ? TYPE_EDEFAULT : newType;
    boolean oldTypeESet = typeESet;
    typeESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TGSE_CONTROL__TYPE, oldType, type, !oldTypeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetType() {
    TGSEControlTypeEnum oldType = type;
    boolean oldTypeESet = typeESet;
    type = TYPE_EDEFAULT;
    typeESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TGSE_CONTROL__TYPE, oldType, TYPE_EDEFAULT, oldTypeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetType() {
    return typeESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TGSE_CONTROL__APP_ID:
        return getAppID();
      case SCLPackage.TGSE_CONTROL__FIXED_OFFS:
        return isFixedOffs();
      case SCLPackage.TGSE_CONTROL__TYPE:
        return getType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TGSE_CONTROL__APP_ID:
        setAppID((String)newValue);
        return;
      case SCLPackage.TGSE_CONTROL__FIXED_OFFS:
        setFixedOffs((Boolean)newValue);
        return;
      case SCLPackage.TGSE_CONTROL__TYPE:
        setType((TGSEControlTypeEnum)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TGSE_CONTROL__APP_ID:
        setAppID(APP_ID_EDEFAULT);
        return;
      case SCLPackage.TGSE_CONTROL__FIXED_OFFS:
        unsetFixedOffs();
        return;
      case SCLPackage.TGSE_CONTROL__TYPE:
        unsetType();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TGSE_CONTROL__APP_ID:
        return APP_ID_EDEFAULT == null ? appID != null : !APP_ID_EDEFAULT.equals(appID);
      case SCLPackage.TGSE_CONTROL__FIXED_OFFS:
        return isSetFixedOffs();
      case SCLPackage.TGSE_CONTROL__TYPE:
        return isSetType();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (appID: ");
    result.append(appID);
    result.append(", fixedOffs: ");
    if (fixedOffsESet) result.append(fixedOffs); else result.append("<unset>");
    result.append(", type: ");
    if (typeESet) result.append(type); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TGSEControlImpl
