/**
 */
package com.lucy.g3.iec61850.model.systemcorp.impl;

import com.lucy.g3.iec61850.model.systemcorp.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SystemCorpFactoryImpl extends EFactoryImpl implements SystemCorpFactory {
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static SystemCorpFactory init() {
    try {
      SystemCorpFactory theSystemCorpFactory = (SystemCorpFactory)EPackage.Registry.INSTANCE.getEFactory(SystemCorpPackage.eNS_URI);
      if (theSystemCorpFactory != null) {
        return theSystemCorpFactory;
      }
    }
    catch (Exception exception) {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new SystemCorpFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SystemCorpFactoryImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass) {
    switch (eClass.getClassifierID()) {
      case SystemCorpPackage.DOCUMENT_ROOT: return (EObject)createDocumentRoot();
      case SystemCorpPackage.TGENERIC_PRIVATE_OBJECT: return (EObject)createTGenericPrivateObject();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DocumentRoot createDocumentRoot() {
    DocumentRootImpl documentRoot = new DocumentRootImpl();
    return documentRoot;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TGenericPrivateObject createTGenericPrivateObject() {
    TGenericPrivateObjectImpl tGenericPrivateObject = new TGenericPrivateObjectImpl();
    return tGenericPrivateObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SystemCorpPackage getSystemCorpPackage() {
    return (SystemCorpPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static SystemCorpPackage getPackage() {
    return SystemCorpPackage.eINSTANCE;
  }

} //SystemCorpFactoryImpl
