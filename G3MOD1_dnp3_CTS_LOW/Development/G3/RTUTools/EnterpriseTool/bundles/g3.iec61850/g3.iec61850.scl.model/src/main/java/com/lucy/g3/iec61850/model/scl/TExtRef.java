/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TExt Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getDaName <em>Da Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getDesc <em>Desc</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getDoName <em>Do Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getIedName <em>Ied Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getIntAddr <em>Int Addr</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getLdInst <em>Ld Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getLnClass <em>Ln Class</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getLnInst <em>Ln Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getServiceType <em>Service Type</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcCBName <em>Src CB Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcLDInst <em>Src LD Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcLNClass <em>Src LN Class</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcLNInst <em>Src LN Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcPrefix <em>Src Prefix</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef()
 * @model extendedMetaData="name='tExtRef' kind='empty'"
 * @extends SCLObject
 * @generated
 */
public interface TExtRef extends SCLObject {
  /**
   * Returns the value of the '<em><b>Da Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Da Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Da Name</em>' attribute.
   * @see #setDaName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_DaName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TFullAttributeName"
   *        extendedMetaData="kind='attribute' name='daName'"
   * @generated
   */
  String getDaName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getDaName <em>Da Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Da Name</em>' attribute.
   * @see #getDaName()
   * @generated
   */
  void setDaName(String value);

  /**
   * Returns the value of the '<em><b>Desc</b></em>' attribute.
   * The default value is <code>""</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Desc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Desc</em>' attribute.
   * @see #isSetDesc()
   * @see #unsetDesc()
   * @see #setDesc(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_Desc()
   * @model default="" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
   *        extendedMetaData="kind='attribute' name='desc'"
   * @generated
   */
  String getDesc();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getDesc <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Desc</em>' attribute.
   * @see #isSetDesc()
   * @see #unsetDesc()
   * @see #getDesc()
   * @generated
   */
  void setDesc(String value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getDesc <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetDesc()
   * @see #getDesc()
   * @see #setDesc(String)
   * @generated
   */
  void unsetDesc();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getDesc <em>Desc</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Desc</em>' attribute is set.
   * @see #unsetDesc()
   * @see #getDesc()
   * @see #setDesc(String)
   * @generated
   */
  boolean isSetDesc();

  /**
   * Returns the value of the '<em><b>Do Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Do Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Do Name</em>' attribute.
   * @see #setDoName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_DoName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TFullDOName"
   *        extendedMetaData="kind='attribute' name='doName'"
   * @generated
   */
  String getDoName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getDoName <em>Do Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Do Name</em>' attribute.
   * @see #getDoName()
   * @generated
   */
  void setDoName(String value);

  /**
   * Returns the value of the '<em><b>Ied Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ied Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ied Name</em>' attribute.
   * @see #setIedName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_IedName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TIEDName"
   *        extendedMetaData="kind='attribute' name='iedName'"
   * @generated
   */
  String getIedName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getIedName <em>Ied Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ied Name</em>' attribute.
   * @see #getIedName()
   * @generated
   */
  void setIedName(String value);

  /**
   * Returns the value of the '<em><b>Int Addr</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Int Addr</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Int Addr</em>' attribute.
   * @see #setIntAddr(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_IntAddr()
   * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
   *        extendedMetaData="kind='attribute' name='intAddr'"
   * @generated
   */
  String getIntAddr();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getIntAddr <em>Int Addr</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Int Addr</em>' attribute.
   * @see #getIntAddr()
   * @generated
   */
  void setIntAddr(String value);

  /**
   * Returns the value of the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ld Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ld Inst</em>' attribute.
   * @see #setLdInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_LdInst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLDInst"
   *        extendedMetaData="kind='attribute' name='ldInst'"
   * @generated
   */
  String getLdInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getLdInst <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ld Inst</em>' attribute.
   * @see #getLdInst()
   * @generated
   */
  void setLdInst(String value);

  /**
   * Returns the value of the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ln Class</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ln Class</em>' attribute.
   * @see #setLnClass(Object)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_LnClass()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLNClassEnum"
   *        extendedMetaData="kind='attribute' name='lnClass'"
   * @generated
   */
  Object getLnClass();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getLnClass <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ln Class</em>' attribute.
   * @see #getLnClass()
   * @generated
   */
  void setLnClass(Object value);

  /**
   * Returns the value of the '<em><b>Ln Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ln Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ln Inst</em>' attribute.
   * @see #setLnInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_LnInst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLNInst"
   *        extendedMetaData="kind='attribute' name='lnInst'"
   * @generated
   */
  String getLnInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getLnInst <em>Ln Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ln Inst</em>' attribute.
   * @see #getLnInst()
   * @generated
   */
  void setLnInst(String value);

  /**
   * Returns the value of the '<em><b>Prefix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prefix</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prefix</em>' attribute.
   * @see #setPrefix(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_Prefix()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TPrefix"
   *        extendedMetaData="kind='attribute' name='prefix'"
   * @generated
   */
  String getPrefix();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getPrefix <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prefix</em>' attribute.
   * @see #getPrefix()
   * @generated
   */
  void setPrefix(String value);

  /**
   * Returns the value of the '<em><b>Service Type</b></em>' attribute.
   * The literals are from the enumeration {@link com.lucy.g3.iec61850.model.scl.TServiceType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Service Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Service Type</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TServiceType
   * @see #isSetServiceType()
   * @see #unsetServiceType()
   * @see #setServiceType(TServiceType)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_ServiceType()
   * @model unsettable="true"
   *        extendedMetaData="kind='attribute' name='serviceType'"
   * @generated
   */
  TServiceType getServiceType();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getServiceType <em>Service Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Service Type</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TServiceType
   * @see #isSetServiceType()
   * @see #unsetServiceType()
   * @see #getServiceType()
   * @generated
   */
  void setServiceType(TServiceType value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getServiceType <em>Service Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetServiceType()
   * @see #getServiceType()
   * @see #setServiceType(TServiceType)
   * @generated
   */
  void unsetServiceType();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getServiceType <em>Service Type</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Service Type</em>' attribute is set.
   * @see #unsetServiceType()
   * @see #getServiceType()
   * @see #setServiceType(TServiceType)
   * @generated
   */
  boolean isSetServiceType();

  /**
   * Returns the value of the '<em><b>Src CB Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Src CB Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Src CB Name</em>' attribute.
   * @see #setSrcCBName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_SrcCBName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TCBName"
   *        extendedMetaData="kind='attribute' name='srcCBName'"
   * @generated
   */
  String getSrcCBName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcCBName <em>Src CB Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Src CB Name</em>' attribute.
   * @see #getSrcCBName()
   * @generated
   */
  void setSrcCBName(String value);

  /**
   * Returns the value of the '<em><b>Src LD Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Src LD Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Src LD Inst</em>' attribute.
   * @see #setSrcLDInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_SrcLDInst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLDInst"
   *        extendedMetaData="kind='attribute' name='srcLDInst'"
   * @generated
   */
  String getSrcLDInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcLDInst <em>Src LD Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Src LD Inst</em>' attribute.
   * @see #getSrcLDInst()
   * @generated
   */
  void setSrcLDInst(String value);

  /**
   * Returns the value of the '<em><b>Src LN Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Src LN Class</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Src LN Class</em>' attribute.
   * @see #setSrcLNClass(Object)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_SrcLNClass()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLNClassEnum"
   *        extendedMetaData="kind='attribute' name='srcLNClass'"
   * @generated
   */
  Object getSrcLNClass();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcLNClass <em>Src LN Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Src LN Class</em>' attribute.
   * @see #getSrcLNClass()
   * @generated
   */
  void setSrcLNClass(Object value);

  /**
   * Returns the value of the '<em><b>Src LN Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Src LN Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Src LN Inst</em>' attribute.
   * @see #setSrcLNInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_SrcLNInst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLNInst"
   *        extendedMetaData="kind='attribute' name='srcLNInst'"
   * @generated
   */
  String getSrcLNInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcLNInst <em>Src LN Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Src LN Inst</em>' attribute.
   * @see #getSrcLNInst()
   * @generated
   */
  void setSrcLNInst(String value);

  /**
   * Returns the value of the '<em><b>Src Prefix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Src Prefix</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Src Prefix</em>' attribute.
   * @see #setSrcPrefix(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTExtRef_SrcPrefix()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TPrefix"
   *        extendedMetaData="kind='attribute' name='srcPrefix'"
   * @generated
   */
  String getSrcPrefix();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcPrefix <em>Src Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Src Prefix</em>' attribute.
   * @see #getSrcPrefix()
   * @generated
   */
  void setSrcPrefix(String value);

} // TExtRef
