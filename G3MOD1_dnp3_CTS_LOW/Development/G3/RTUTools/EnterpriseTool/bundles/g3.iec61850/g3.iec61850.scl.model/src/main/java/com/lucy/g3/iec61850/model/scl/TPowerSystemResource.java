/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPower System Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPowerSystemResource()
 * @model abstract="true"
 *        extendedMetaData="name='tPowerSystemResource' kind='elementOnly'"
 * @generated
 */
public interface TPowerSystemResource extends TLNodeContainer {
} // TPowerSystemResource
