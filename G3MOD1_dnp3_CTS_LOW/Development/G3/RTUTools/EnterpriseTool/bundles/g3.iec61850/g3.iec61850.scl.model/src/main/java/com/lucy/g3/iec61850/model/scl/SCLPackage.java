/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Revised SCL normative schema. Version 3.0. (SCL language version "2007"). Release 2009/03/16.
 * Revised SCL normative schema. Version 3.0. (SCL language version "2007"). Release 2009/03/16.
 * Revised SCL normative schema. Version 3.0. (SCL language version "2007"). Release 2009/03/16.
 * Revised SCL normative schema. Version 3.0. (SCL language version "2007"). Release 2009/03/19.
 * <!-- end-model-doc -->
 * @see com.lucy.g3.iec61850.model.scl.SCLFactory
 * @model kind="package"
 * @generated
 */
public interface SCLPackage extends EPackage {
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "scl";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.iec.ch/61850/2003/SCL";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "scl";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  SCLPackage eINSTANCE = com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl.init();

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.AuthenticationTypeImpl <em>Authentication Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.AuthenticationTypeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getAuthenticationType()
   * @generated
   */
  int AUTHENTICATION_TYPE = 0;

  /**
   * The feature id for the '<em><b>Certificate</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AUTHENTICATION_TYPE__CERTIFICATE = 0;

  /**
   * The feature id for the '<em><b>None</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AUTHENTICATION_TYPE__NONE = 1;

  /**
   * The feature id for the '<em><b>Password</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AUTHENTICATION_TYPE__PASSWORD = 2;

  /**
   * The feature id for the '<em><b>Strong</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AUTHENTICATION_TYPE__STRONG = 3;

  /**
   * The feature id for the '<em><b>Weak</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AUTHENTICATION_TYPE__WEAK = 4;

  /**
   * The number of structural features of the '<em>Authentication Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AUTHENTICATION_TYPE_FEATURE_COUNT = 5;

  /**
   * The number of operations of the '<em>Authentication Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AUTHENTICATION_TYPE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.DocumentRootImpl <em>Document Root</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.DocumentRootImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getDocumentRoot()
   * @generated
   */
  int DOCUMENT_ROOT = 1;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__MIXED = 0;

  /**
   * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

  /**
   * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

  /**
   * The feature id for the '<em><b>Communication</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__COMMUNICATION = 3;

  /**
   * The feature id for the '<em><b>Data Type Templates</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__DATA_TYPE_TEMPLATES = 4;

  /**
   * The feature id for the '<em><b>IED</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IED = 5;

  /**
   * The feature id for the '<em><b>LN</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__LN = 6;

  /**
   * The feature id for the '<em><b>LN0</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__LN0 = 7;

  /**
   * The feature id for the '<em><b>SCL</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__SCL = 8;

  /**
   * The feature id for the '<em><b>Substation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__SUBSTATION = 9;

  /**
   * The number of structural features of the '<em>Document Root</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT_FEATURE_COUNT = 10;

  /**
   * The number of operations of the '<em>Document Root</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.HistoryTypeImpl <em>History Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.HistoryTypeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getHistoryType()
   * @generated
   */
  int HISTORY_TYPE = 2;

  /**
   * The feature id for the '<em><b>Hitem</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HISTORY_TYPE__HITEM = 0;

  /**
   * The number of structural features of the '<em>History Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HISTORY_TYPE_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>History Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HISTORY_TYPE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.IEDNameTypeImpl <em>IED Name Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.IEDNameTypeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getIEDNameType()
   * @generated
   */
  int IED_NAME_TYPE = 3;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IED_NAME_TYPE__VALUE = 0;

  /**
   * The feature id for the '<em><b>Ap Ref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IED_NAME_TYPE__AP_REF = 1;

  /**
   * The feature id for the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IED_NAME_TYPE__LD_INST = 2;

  /**
   * The feature id for the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IED_NAME_TYPE__LN_CLASS = 3;

  /**
   * The feature id for the '<em><b>Ln Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IED_NAME_TYPE__LN_INST = 4;

  /**
   * The feature id for the '<em><b>Prefix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IED_NAME_TYPE__PREFIX = 5;

  /**
   * The number of structural features of the '<em>IED Name Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IED_NAME_TYPE_FEATURE_COUNT = 6;

  /**
   * The number of operations of the '<em>IED Name Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IED_NAME_TYPE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TBaseElementImpl <em>TBase Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TBaseElementImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTBaseElement()
   * @generated
   */
  int TBASE_ELEMENT = 18;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBASE_ELEMENT__ANY = 0;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBASE_ELEMENT__TEXT = 1;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBASE_ELEMENT__PRIVATE = 2;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBASE_ELEMENT__ANY_ATTRIBUTE = 3;

  /**
   * The number of structural features of the '<em>TBase Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBASE_ELEMENT_FEATURE_COUNT = 4;

  /**
   * The number of operations of the '<em>TBase Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBASE_ELEMENT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TUnNamingImpl <em>TUn Naming</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TUnNamingImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTUnNaming()
   * @generated
   */
  int TUN_NAMING = 124;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TUN_NAMING__ANY = TBASE_ELEMENT__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TUN_NAMING__TEXT = TBASE_ELEMENT__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TUN_NAMING__PRIVATE = TBASE_ELEMENT__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TUN_NAMING__ANY_ATTRIBUTE = TBASE_ELEMENT__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TUN_NAMING__DESC = TBASE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TUn Naming</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TUN_NAMING_FEATURE_COUNT = TBASE_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TUn Naming</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TUN_NAMING_OPERATION_COUNT = TBASE_ELEMENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TAnyLNImpl <em>TAny LN</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TAnyLNImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAnyLN()
   * @generated
   */
  int TANY_LN = 16;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Data Set</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN__DATA_SET = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Report Control</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN__REPORT_CONTROL = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Log Control</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN__LOG_CONTROL = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>DOI</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN__DOI = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Inputs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN__INPUTS = TUN_NAMING_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Log</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN__LOG = TUN_NAMING_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Ln Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN__LN_TYPE = TUN_NAMING_FEATURE_COUNT + 6;

  /**
   * The number of structural features of the '<em>TAny LN</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 7;

  /**
   * The number of operations of the '<em>TAny LN</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_LN_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TLN0Impl <em>TLN0</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TLN0Impl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLN0()
   * @generated
   */
  int TLN0 = 63;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__ANY = TANY_LN__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__TEXT = TANY_LN__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__PRIVATE = TANY_LN__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__ANY_ATTRIBUTE = TANY_LN__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__DESC = TANY_LN__DESC;

  /**
   * The feature id for the '<em><b>Data Set</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__DATA_SET = TANY_LN__DATA_SET;

  /**
   * The feature id for the '<em><b>Report Control</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__REPORT_CONTROL = TANY_LN__REPORT_CONTROL;

  /**
   * The feature id for the '<em><b>Log Control</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__LOG_CONTROL = TANY_LN__LOG_CONTROL;

  /**
   * The feature id for the '<em><b>DOI</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__DOI = TANY_LN__DOI;

  /**
   * The feature id for the '<em><b>Inputs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__INPUTS = TANY_LN__INPUTS;

  /**
   * The feature id for the '<em><b>Log</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__LOG = TANY_LN__LOG;

  /**
   * The feature id for the '<em><b>Ln Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__LN_TYPE = TANY_LN__LN_TYPE;

  /**
   * The feature id for the '<em><b>GSE Control</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__GSE_CONTROL = TANY_LN_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Sampled Value Control</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__SAMPLED_VALUE_CONTROL = TANY_LN_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Setting Control</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__SETTING_CONTROL = TANY_LN_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__INST = TANY_LN_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0__LN_CLASS = TANY_LN_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>TLN0</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0_FEATURE_COUNT = TANY_LN_FEATURE_COUNT + 5;

  /**
   * The number of operations of the '<em>TLN0</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN0_OPERATION_COUNT = TANY_LN_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.LN0TypeImpl <em>LN0 Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.LN0TypeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getLN0Type()
   * @generated
   */
  int LN0_TYPE = 4;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__ANY = TLN0__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__TEXT = TLN0__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__PRIVATE = TLN0__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__ANY_ATTRIBUTE = TLN0__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__DESC = TLN0__DESC;

  /**
   * The feature id for the '<em><b>Data Set</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__DATA_SET = TLN0__DATA_SET;

  /**
   * The feature id for the '<em><b>Report Control</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__REPORT_CONTROL = TLN0__REPORT_CONTROL;

  /**
   * The feature id for the '<em><b>Log Control</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__LOG_CONTROL = TLN0__LOG_CONTROL;

  /**
   * The feature id for the '<em><b>DOI</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__DOI = TLN0__DOI;

  /**
   * The feature id for the '<em><b>Inputs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__INPUTS = TLN0__INPUTS;

  /**
   * The feature id for the '<em><b>Log</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__LOG = TLN0__LOG;

  /**
   * The feature id for the '<em><b>Ln Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__LN_TYPE = TLN0__LN_TYPE;

  /**
   * The feature id for the '<em><b>GSE Control</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__GSE_CONTROL = TLN0__GSE_CONTROL;

  /**
   * The feature id for the '<em><b>Sampled Value Control</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__SAMPLED_VALUE_CONTROL = TLN0__SAMPLED_VALUE_CONTROL;

  /**
   * The feature id for the '<em><b>Setting Control</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__SETTING_CONTROL = TLN0__SETTING_CONTROL;

  /**
   * The feature id for the '<em><b>Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__INST = TLN0__INST;

  /**
   * The feature id for the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE__LN_CLASS = TLN0__LN_CLASS;

  /**
   * The number of structural features of the '<em>LN0 Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE_FEATURE_COUNT = TLN0_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>LN0 Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LN0_TYPE_OPERATION_COUNT = TLN0_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.OptFieldsTypeImpl <em>Opt Fields Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.OptFieldsTypeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getOptFieldsType()
   * @generated
   */
  int OPT_FIELDS_TYPE = 5;

  /**
   * The feature id for the '<em><b>Buf Ovfl</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPT_FIELDS_TYPE__BUF_OVFL = 0;

  /**
   * The feature id for the '<em><b>Config Ref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPT_FIELDS_TYPE__CONFIG_REF = 1;

  /**
   * The feature id for the '<em><b>Data Ref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPT_FIELDS_TYPE__DATA_REF = 2;

  /**
   * The feature id for the '<em><b>Data Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPT_FIELDS_TYPE__DATA_SET = 3;

  /**
   * The feature id for the '<em><b>Entry ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPT_FIELDS_TYPE__ENTRY_ID = 4;

  /**
   * The feature id for the '<em><b>Reason Code</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPT_FIELDS_TYPE__REASON_CODE = 5;

  /**
   * The feature id for the '<em><b>Seq Num</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPT_FIELDS_TYPE__SEQ_NUM = 6;

  /**
   * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPT_FIELDS_TYPE__TIME_STAMP = 7;

  /**
   * The number of structural features of the '<em>Opt Fields Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPT_FIELDS_TYPE_FEATURE_COUNT = 8;

  /**
   * The number of operations of the '<em>Opt Fields Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPT_FIELDS_TYPE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.ProtNsTypeImpl <em>Prot Ns Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.ProtNsTypeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getProtNsType()
   * @generated
   */
  int PROT_NS_TYPE = 6;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROT_NS_TYPE__VALUE = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROT_NS_TYPE__TYPE = 1;

  /**
   * The number of structural features of the '<em>Prot Ns Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROT_NS_TYPE_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>Prot Ns Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROT_NS_TYPE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.SCLTypeImpl <em>Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLTypeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getSCLType()
   * @generated
   */
  int SCL_TYPE = 7;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCL_TYPE__ANY = TBASE_ELEMENT__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCL_TYPE__TEXT = TBASE_ELEMENT__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCL_TYPE__PRIVATE = TBASE_ELEMENT__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCL_TYPE__ANY_ATTRIBUTE = TBASE_ELEMENT__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Header</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCL_TYPE__HEADER = TBASE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Substation</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCL_TYPE__SUBSTATION = TBASE_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Communication</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCL_TYPE__COMMUNICATION = TBASE_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>IED</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCL_TYPE__IED = TBASE_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Data Type Templates</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCL_TYPE__DATA_TYPE_TEMPLATES = TBASE_ELEMENT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Revision</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCL_TYPE__REVISION = TBASE_ELEMENT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Version</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCL_TYPE__VERSION = TBASE_ELEMENT_FEATURE_COUNT + 6;

  /**
   * The number of structural features of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCL_TYPE_FEATURE_COUNT = TBASE_ELEMENT_FEATURE_COUNT + 7;

  /**
   * The number of operations of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCL_TYPE_OPERATION_COUNT = TBASE_ELEMENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.SettingGroupsTypeImpl <em>Setting Groups Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.SettingGroupsTypeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getSettingGroupsType()
   * @generated
   */
  int SETTING_GROUPS_TYPE = 8;

  /**
   * The feature id for the '<em><b>SG Edit</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SETTING_GROUPS_TYPE__SG_EDIT = 0;

  /**
   * The feature id for the '<em><b>Conf SG</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SETTING_GROUPS_TYPE__CONF_SG = 1;

  /**
   * The number of structural features of the '<em>Setting Groups Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SETTING_GROUPS_TYPE_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>Setting Groups Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SETTING_GROUPS_TYPE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.SmvOptsTypeImpl <em>Smv Opts Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.SmvOptsTypeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getSmvOptsType()
   * @generated
   */
  int SMV_OPTS_TYPE = 9;

  /**
   * The feature id for the '<em><b>Data Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SMV_OPTS_TYPE__DATA_SET = 0;

  /**
   * The feature id for the '<em><b>Refresh Time</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SMV_OPTS_TYPE__REFRESH_TIME = 1;

  /**
   * The feature id for the '<em><b>Sample Rate</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SMV_OPTS_TYPE__SAMPLE_RATE = 2;

  /**
   * The feature id for the '<em><b>Sample Synchronized</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SMV_OPTS_TYPE__SAMPLE_SYNCHRONIZED = 3;

  /**
   * The feature id for the '<em><b>Security</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SMV_OPTS_TYPE__SECURITY = 4;

  /**
   * The number of structural features of the '<em>Smv Opts Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SMV_OPTS_TYPE_FEATURE_COUNT = 5;

  /**
   * The number of operations of the '<em>Smv Opts Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SMV_OPTS_TYPE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TNamingImpl <em>TNaming</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TNamingImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTNaming()
   * @generated
   */
  int TNAMING = 70;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TNAMING__ANY = TBASE_ELEMENT__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TNAMING__TEXT = TBASE_ELEMENT__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TNAMING__PRIVATE = TBASE_ELEMENT__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TNAMING__ANY_ATTRIBUTE = TBASE_ELEMENT__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TNAMING__DESC = TBASE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TNAMING__NAME = TBASE_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TNaming</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TNAMING_FEATURE_COUNT = TBASE_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TNaming</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TNAMING_OPERATION_COUNT = TBASE_ELEMENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TLNodeContainerImpl <em>TL Node Container</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TLNodeContainerImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLNodeContainer()
   * @generated
   */
  int TL_NODE_CONTAINER = 65;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_CONTAINER__ANY = TNAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_CONTAINER__TEXT = TNAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_CONTAINER__PRIVATE = TNAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_CONTAINER__ANY_ATTRIBUTE = TNAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_CONTAINER__DESC = TNAMING__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_CONTAINER__NAME = TNAMING__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_CONTAINER__LNODE = TNAMING_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TL Node Container</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_CONTAINER_FEATURE_COUNT = TNAMING_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TL Node Container</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_CONTAINER_OPERATION_COUNT = TNAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPowerSystemResourceImpl <em>TPower System Resource</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPowerSystemResourceImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPowerSystemResource()
   * @generated
   */
  int TPOWER_SYSTEM_RESOURCE = 87;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_SYSTEM_RESOURCE__ANY = TL_NODE_CONTAINER__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_SYSTEM_RESOURCE__TEXT = TL_NODE_CONTAINER__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_SYSTEM_RESOURCE__PRIVATE = TL_NODE_CONTAINER__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_SYSTEM_RESOURCE__ANY_ATTRIBUTE = TL_NODE_CONTAINER__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_SYSTEM_RESOURCE__DESC = TL_NODE_CONTAINER__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_SYSTEM_RESOURCE__NAME = TL_NODE_CONTAINER__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_SYSTEM_RESOURCE__LNODE = TL_NODE_CONTAINER__LNODE;

  /**
   * The number of structural features of the '<em>TPower System Resource</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT = TL_NODE_CONTAINER_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPower System Resource</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_SYSTEM_RESOURCE_OPERATION_COUNT = TL_NODE_CONTAINER_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TEquipmentImpl <em>TEquipment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TEquipmentImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTEquipment()
   * @generated
   */
  int TEQUIPMENT = 47;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT__ANY = TPOWER_SYSTEM_RESOURCE__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT__TEXT = TPOWER_SYSTEM_RESOURCE__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT__PRIVATE = TPOWER_SYSTEM_RESOURCE__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT__ANY_ATTRIBUTE = TPOWER_SYSTEM_RESOURCE__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT__DESC = TPOWER_SYSTEM_RESOURCE__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT__NAME = TPOWER_SYSTEM_RESOURCE__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT__LNODE = TPOWER_SYSTEM_RESOURCE__LNODE;

  /**
   * The feature id for the '<em><b>Virtual</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT__VIRTUAL = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TEquipment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT_FEATURE_COUNT = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TEquipment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT_OPERATION_COUNT = TPOWER_SYSTEM_RESOURCE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TAbstractConductingEquipmentImpl <em>TAbstract Conducting Equipment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TAbstractConductingEquipmentImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAbstractConductingEquipment()
   * @generated
   */
  int TABSTRACT_CONDUCTING_EQUIPMENT = 10;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_CONDUCTING_EQUIPMENT__ANY = TEQUIPMENT__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_CONDUCTING_EQUIPMENT__TEXT = TEQUIPMENT__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_CONDUCTING_EQUIPMENT__PRIVATE = TEQUIPMENT__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_CONDUCTING_EQUIPMENT__ANY_ATTRIBUTE = TEQUIPMENT__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_CONDUCTING_EQUIPMENT__DESC = TEQUIPMENT__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_CONDUCTING_EQUIPMENT__NAME = TEQUIPMENT__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_CONDUCTING_EQUIPMENT__LNODE = TEQUIPMENT__LNODE;

  /**
   * The feature id for the '<em><b>Virtual</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_CONDUCTING_EQUIPMENT__VIRTUAL = TEQUIPMENT__VIRTUAL;

  /**
   * The feature id for the '<em><b>Terminal</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_CONDUCTING_EQUIPMENT__TERMINAL = TEQUIPMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Sub Equipment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_CONDUCTING_EQUIPMENT__SUB_EQUIPMENT = TEQUIPMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TAbstract Conducting Equipment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_CONDUCTING_EQUIPMENT_FEATURE_COUNT = TEQUIPMENT_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TAbstract Conducting Equipment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_CONDUCTING_EQUIPMENT_OPERATION_COUNT = TEQUIPMENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TAbstractDataAttributeImpl <em>TAbstract Data Attribute</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TAbstractDataAttributeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAbstractDataAttribute()
   * @generated
   */
  int TABSTRACT_DATA_ATTRIBUTE = 11;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Val</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE__VAL = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>BType</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE__BTYPE = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Count</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE__COUNT = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE__NAME = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>SAddr</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE__SADDR = TUN_NAMING_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE__TYPE = TUN_NAMING_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Val Kind</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE__VAL_KIND = TUN_NAMING_FEATURE_COUNT + 6;

  /**
   * The number of structural features of the '<em>TAbstract Data Attribute</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 7;

  /**
   * The number of operations of the '<em>TAbstract Data Attribute</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TABSTRACT_DATA_ATTRIBUTE_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TAnyContentFromOtherNamespaceImpl <em>TAny Content From Other Namespace</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TAnyContentFromOtherNamespaceImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAnyContentFromOtherNamespace()
   * @generated
   */
  int TANY_CONTENT_FROM_OTHER_NAMESPACE = 15;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_CONTENT_FROM_OTHER_NAMESPACE__MIXED = 0;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_CONTENT_FROM_OTHER_NAMESPACE__GROUP = 1;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_CONTENT_FROM_OTHER_NAMESPACE__ANY = 2;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_CONTENT_FROM_OTHER_NAMESPACE__ANY_ATTRIBUTE = 3;

  /**
   * The number of structural features of the '<em>TAny Content From Other Namespace</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT = 4;

  /**
   * The number of operations of the '<em>TAny Content From Other Namespace</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TANY_CONTENT_FROM_OTHER_NAMESPACE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TAccessControlImpl <em>TAccess Control</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TAccessControlImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAccessControl()
   * @generated
   */
  int TACCESS_CONTROL = 12;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_CONTROL__MIXED = TANY_CONTENT_FROM_OTHER_NAMESPACE__MIXED;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_CONTROL__GROUP = TANY_CONTENT_FROM_OTHER_NAMESPACE__GROUP;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_CONTROL__ANY = TANY_CONTENT_FROM_OTHER_NAMESPACE__ANY;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_CONTROL__ANY_ATTRIBUTE = TANY_CONTENT_FROM_OTHER_NAMESPACE__ANY_ATTRIBUTE;

  /**
   * The number of structural features of the '<em>TAccess Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_CONTROL_FEATURE_COUNT = TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TAccess Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_CONTROL_OPERATION_COUNT = TANY_CONTENT_FROM_OTHER_NAMESPACE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TAccessPointImpl <em>TAccess Point</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TAccessPointImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAccessPoint()
   * @generated
   */
  int TACCESS_POINT = 13;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Server</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__SERVER = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>LN</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__LN = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Server At</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__SERVER_AT = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Services</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__SERVICES = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>GOOSE Security</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__GOOSE_SECURITY = TUN_NAMING_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>SMV Security</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__SMV_SECURITY = TUN_NAMING_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Clock</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__CLOCK = TUN_NAMING_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__NAME = TUN_NAMING_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Router</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT__ROUTER = TUN_NAMING_FEATURE_COUNT + 8;

  /**
   * The number of structural features of the '<em>TAccess Point</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 9;

  /**
   * The number of operations of the '<em>TAccess Point</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TACCESS_POINT_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TAddressImpl <em>TAddress</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TAddressImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAddress()
   * @generated
   */
  int TADDRESS = 14;

  /**
   * The feature id for the '<em><b>P</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TADDRESS__P = 0;

  /**
   * The number of structural features of the '<em>TAddress</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TADDRESS_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>TAddress</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TADDRESS_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TAssociationImpl <em>TAssociation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TAssociationImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAssociation()
   * @generated
   */
  int TASSOCIATION = 17;

  /**
   * The feature id for the '<em><b>Association ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASSOCIATION__ASSOCIATION_ID = 0;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASSOCIATION__DESC = 1;

  /**
   * The feature id for the '<em><b>Ied Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASSOCIATION__IED_NAME = 2;

  /**
   * The feature id for the '<em><b>Kind</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASSOCIATION__KIND = 3;

  /**
   * The feature id for the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASSOCIATION__LD_INST = 4;

  /**
   * The feature id for the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASSOCIATION__LN_CLASS = 5;

  /**
   * The feature id for the '<em><b>Ln Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASSOCIATION__LN_INST = 6;

  /**
   * The feature id for the '<em><b>Prefix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASSOCIATION__PREFIX = 7;

  /**
   * The number of structural features of the '<em>TAssociation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASSOCIATION_FEATURE_COUNT = 8;

  /**
   * The number of operations of the '<em>TAssociation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASSOCIATION_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TEquipmentContainerImpl <em>TEquipment Container</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TEquipmentContainerImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTEquipmentContainer()
   * @generated
   */
  int TEQUIPMENT_CONTAINER = 48;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT_CONTAINER__ANY = TPOWER_SYSTEM_RESOURCE__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT_CONTAINER__TEXT = TPOWER_SYSTEM_RESOURCE__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT_CONTAINER__PRIVATE = TPOWER_SYSTEM_RESOURCE__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT_CONTAINER__ANY_ATTRIBUTE = TPOWER_SYSTEM_RESOURCE__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT_CONTAINER__DESC = TPOWER_SYSTEM_RESOURCE__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT_CONTAINER__NAME = TPOWER_SYSTEM_RESOURCE__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT_CONTAINER__LNODE = TPOWER_SYSTEM_RESOURCE__LNODE;

  /**
   * The feature id for the '<em><b>Power Transformer</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT_CONTAINER__POWER_TRANSFORMER = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>General Equipment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT_CONTAINER__GENERAL_EQUIPMENT = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TEquipment Container</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT_CONTAINER_FEATURE_COUNT = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TEquipment Container</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEQUIPMENT_CONTAINER_OPERATION_COUNT = TPOWER_SYSTEM_RESOURCE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TBayImpl <em>TBay</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TBayImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTBay()
   * @generated
   */
  int TBAY = 19;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY__ANY = TEQUIPMENT_CONTAINER__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY__TEXT = TEQUIPMENT_CONTAINER__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY__PRIVATE = TEQUIPMENT_CONTAINER__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY__ANY_ATTRIBUTE = TEQUIPMENT_CONTAINER__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY__DESC = TEQUIPMENT_CONTAINER__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY__NAME = TEQUIPMENT_CONTAINER__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY__LNODE = TEQUIPMENT_CONTAINER__LNODE;

  /**
   * The feature id for the '<em><b>Power Transformer</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY__POWER_TRANSFORMER = TEQUIPMENT_CONTAINER__POWER_TRANSFORMER;

  /**
   * The feature id for the '<em><b>General Equipment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY__GENERAL_EQUIPMENT = TEQUIPMENT_CONTAINER__GENERAL_EQUIPMENT;

  /**
   * The feature id for the '<em><b>Conducting Equipment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY__CONDUCTING_EQUIPMENT = TEQUIPMENT_CONTAINER_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Connectivity Node</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY__CONNECTIVITY_NODE = TEQUIPMENT_CONTAINER_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Function</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY__FUNCTION = TEQUIPMENT_CONTAINER_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>TBay</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY_FEATURE_COUNT = TEQUIPMENT_CONTAINER_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>TBay</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBAY_OPERATION_COUNT = TEQUIPMENT_CONTAINER_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TBDAImpl <em>TBDA</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TBDAImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTBDA()
   * @generated
   */
  int TBDA = 20;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA__ANY = TABSTRACT_DATA_ATTRIBUTE__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA__TEXT = TABSTRACT_DATA_ATTRIBUTE__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA__PRIVATE = TABSTRACT_DATA_ATTRIBUTE__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA__ANY_ATTRIBUTE = TABSTRACT_DATA_ATTRIBUTE__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA__DESC = TABSTRACT_DATA_ATTRIBUTE__DESC;

  /**
   * The feature id for the '<em><b>Val</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA__VAL = TABSTRACT_DATA_ATTRIBUTE__VAL;

  /**
   * The feature id for the '<em><b>BType</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA__BTYPE = TABSTRACT_DATA_ATTRIBUTE__BTYPE;

  /**
   * The feature id for the '<em><b>Count</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA__COUNT = TABSTRACT_DATA_ATTRIBUTE__COUNT;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA__NAME = TABSTRACT_DATA_ATTRIBUTE__NAME;

  /**
   * The feature id for the '<em><b>SAddr</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA__SADDR = TABSTRACT_DATA_ATTRIBUTE__SADDR;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA__TYPE = TABSTRACT_DATA_ATTRIBUTE__TYPE;

  /**
   * The feature id for the '<em><b>Val Kind</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA__VAL_KIND = TABSTRACT_DATA_ATTRIBUTE__VAL_KIND;

  /**
   * The number of structural features of the '<em>TBDA</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA_FEATURE_COUNT = TABSTRACT_DATA_ATTRIBUTE_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TBDA</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBDA_OPERATION_COUNT = TABSTRACT_DATA_ATTRIBUTE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TBitRateInMbPerSecImpl <em>TBit Rate In Mb Per Sec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TBitRateInMbPerSecImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTBitRateInMbPerSec()
   * @generated
   */
  int TBIT_RATE_IN_MB_PER_SEC = 21;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBIT_RATE_IN_MB_PER_SEC__VALUE = 0;

  /**
   * The feature id for the '<em><b>Multiplier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBIT_RATE_IN_MB_PER_SEC__MULTIPLIER = 1;

  /**
   * The feature id for the '<em><b>Unit</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBIT_RATE_IN_MB_PER_SEC__UNIT = 2;

  /**
   * The number of structural features of the '<em>TBit Rate In Mb Per Sec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBIT_RATE_IN_MB_PER_SEC_FEATURE_COUNT = 3;

  /**
   * The number of operations of the '<em>TBit Rate In Mb Per Sec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TBIT_RATE_IN_MB_PER_SEC_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TCertImpl <em>TCert</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TCertImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTCert()
   * @generated
   */
  int TCERT = 22;

  /**
   * The feature id for the '<em><b>Common Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERT__COMMON_NAME = 0;

  /**
   * The feature id for the '<em><b>Id Hierarchy</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERT__ID_HIERARCHY = 1;

  /**
   * The number of structural features of the '<em>TCert</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERT_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>TCert</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TCertificateImpl <em>TCertificate</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TCertificateImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTCertificate()
   * @generated
   */
  int TCERTIFICATE = 23;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERTIFICATE__ANY = TNAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERTIFICATE__TEXT = TNAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERTIFICATE__PRIVATE = TNAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERTIFICATE__ANY_ATTRIBUTE = TNAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERTIFICATE__DESC = TNAMING__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERTIFICATE__NAME = TNAMING__NAME;

  /**
   * The feature id for the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERTIFICATE__SUBJECT = TNAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Issuer Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERTIFICATE__ISSUER_NAME = TNAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Serial Number</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERTIFICATE__SERIAL_NUMBER = TNAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Xfer Number</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERTIFICATE__XFER_NUMBER = TNAMING_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>TCertificate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERTIFICATE_FEATURE_COUNT = TNAMING_FEATURE_COUNT + 4;

  /**
   * The number of operations of the '<em>TCertificate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCERTIFICATE_OPERATION_COUNT = TNAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TClientLNImpl <em>TClient LN</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TClientLNImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTClientLN()
   * @generated
   */
  int TCLIENT_LN = 24;

  /**
   * The feature id for the '<em><b>Ap Ref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_LN__AP_REF = 0;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_LN__DESC = 1;

  /**
   * The feature id for the '<em><b>Ied Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_LN__IED_NAME = 2;

  /**
   * The feature id for the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_LN__LD_INST = 3;

  /**
   * The feature id for the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_LN__LN_CLASS = 4;

  /**
   * The feature id for the '<em><b>Ln Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_LN__LN_INST = 5;

  /**
   * The feature id for the '<em><b>Prefix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_LN__PREFIX = 6;

  /**
   * The number of structural features of the '<em>TClient LN</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_LN_FEATURE_COUNT = 7;

  /**
   * The number of operations of the '<em>TClient LN</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_LN_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TClientServicesImpl <em>TClient Services</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TClientServicesImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTClientServices()
   * @generated
   */
  int TCLIENT_SERVICES = 25;

  /**
   * The feature id for the '<em><b>Buf Report</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_SERVICES__BUF_REPORT = 0;

  /**
   * The feature id for the '<em><b>Goose</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_SERVICES__GOOSE = 1;

  /**
   * The feature id for the '<em><b>Gsse</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_SERVICES__GSSE = 2;

  /**
   * The feature id for the '<em><b>Read Log</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_SERVICES__READ_LOG = 3;

  /**
   * The feature id for the '<em><b>Sv</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_SERVICES__SV = 4;

  /**
   * The feature id for the '<em><b>Unbuf Report</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_SERVICES__UNBUF_REPORT = 5;

  /**
   * The number of structural features of the '<em>TClient Services</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_SERVICES_FEATURE_COUNT = 6;

  /**
   * The number of operations of the '<em>TClient Services</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCLIENT_SERVICES_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TCommunicationImpl <em>TCommunication</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TCommunicationImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTCommunication()
   * @generated
   */
  int TCOMMUNICATION = 26;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCOMMUNICATION__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCOMMUNICATION__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCOMMUNICATION__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCOMMUNICATION__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCOMMUNICATION__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Sub Network</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCOMMUNICATION__SUB_NETWORK = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TCommunication</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCOMMUNICATION_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TCommunication</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCOMMUNICATION_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TConductingEquipmentImpl <em>TConducting Equipment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TConductingEquipmentImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTConductingEquipment()
   * @generated
   */
  int TCONDUCTING_EQUIPMENT = 27;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONDUCTING_EQUIPMENT__ANY = TABSTRACT_CONDUCTING_EQUIPMENT__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONDUCTING_EQUIPMENT__TEXT = TABSTRACT_CONDUCTING_EQUIPMENT__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONDUCTING_EQUIPMENT__PRIVATE = TABSTRACT_CONDUCTING_EQUIPMENT__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONDUCTING_EQUIPMENT__ANY_ATTRIBUTE = TABSTRACT_CONDUCTING_EQUIPMENT__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONDUCTING_EQUIPMENT__DESC = TABSTRACT_CONDUCTING_EQUIPMENT__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONDUCTING_EQUIPMENT__NAME = TABSTRACT_CONDUCTING_EQUIPMENT__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONDUCTING_EQUIPMENT__LNODE = TABSTRACT_CONDUCTING_EQUIPMENT__LNODE;

  /**
   * The feature id for the '<em><b>Virtual</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONDUCTING_EQUIPMENT__VIRTUAL = TABSTRACT_CONDUCTING_EQUIPMENT__VIRTUAL;

  /**
   * The feature id for the '<em><b>Terminal</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONDUCTING_EQUIPMENT__TERMINAL = TABSTRACT_CONDUCTING_EQUIPMENT__TERMINAL;

  /**
   * The feature id for the '<em><b>Sub Equipment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONDUCTING_EQUIPMENT__SUB_EQUIPMENT = TABSTRACT_CONDUCTING_EQUIPMENT__SUB_EQUIPMENT;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONDUCTING_EQUIPMENT__TYPE = TABSTRACT_CONDUCTING_EQUIPMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TConducting Equipment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONDUCTING_EQUIPMENT_FEATURE_COUNT = TABSTRACT_CONDUCTING_EQUIPMENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TConducting Equipment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONDUCTING_EQUIPMENT_OPERATION_COUNT = TABSTRACT_CONDUCTING_EQUIPMENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TConfLNsImpl <em>TConf LNs</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TConfLNsImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTConfLNs()
   * @generated
   */
  int TCONF_LNS = 28;

  /**
   * The feature id for the '<em><b>Fix Ln Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONF_LNS__FIX_LN_INST = 0;

  /**
   * The feature id for the '<em><b>Fix Prefix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONF_LNS__FIX_PREFIX = 1;

  /**
   * The number of structural features of the '<em>TConf LNs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONF_LNS_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>TConf LNs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONF_LNS_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TConnectedAPImpl <em>TConnected AP</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TConnectedAPImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTConnectedAP()
   * @generated
   */
  int TCONNECTED_AP = 29;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTED_AP__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTED_AP__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTED_AP__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTED_AP__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTED_AP__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Address</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTED_AP__ADDRESS = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>GSE</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTED_AP__GSE = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>SMV</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTED_AP__SMV = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Phys Conn</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTED_AP__PHYS_CONN = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Ap Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTED_AP__AP_NAME = TUN_NAMING_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Ied Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTED_AP__IED_NAME = TUN_NAMING_FEATURE_COUNT + 5;

  /**
   * The number of structural features of the '<em>TConnected AP</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTED_AP_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 6;

  /**
   * The number of operations of the '<em>TConnected AP</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTED_AP_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TConnectivityNodeImpl <em>TConnectivity Node</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TConnectivityNodeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTConnectivityNode()
   * @generated
   */
  int TCONNECTIVITY_NODE = 30;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTIVITY_NODE__ANY = TL_NODE_CONTAINER__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTIVITY_NODE__TEXT = TL_NODE_CONTAINER__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTIVITY_NODE__PRIVATE = TL_NODE_CONTAINER__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTIVITY_NODE__ANY_ATTRIBUTE = TL_NODE_CONTAINER__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTIVITY_NODE__DESC = TL_NODE_CONTAINER__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTIVITY_NODE__NAME = TL_NODE_CONTAINER__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTIVITY_NODE__LNODE = TL_NODE_CONTAINER__LNODE;

  /**
   * The feature id for the '<em><b>Path Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTIVITY_NODE__PATH_NAME = TL_NODE_CONTAINER_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TConnectivity Node</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTIVITY_NODE_FEATURE_COUNT = TL_NODE_CONTAINER_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TConnectivity Node</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONNECTIVITY_NODE_OPERATION_COUNT = TL_NODE_CONTAINER_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TControlImpl <em>TControl</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TControlImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTControl()
   * @generated
   */
  int TCONTROL = 31;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Dat Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL__DAT_SET = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL__NAME = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TControl</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TControl</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TControlBlockImpl <em>TControl Block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TControlBlockImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTControlBlock()
   * @generated
   */
  int TCONTROL_BLOCK = 32;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_BLOCK__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_BLOCK__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_BLOCK__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_BLOCK__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_BLOCK__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Address</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_BLOCK__ADDRESS = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Cb Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_BLOCK__CB_NAME = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_BLOCK__LD_INST = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>TControl Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_BLOCK_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>TControl Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_BLOCK_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TControlWithIEDNameImpl <em>TControl With IED Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TControlWithIEDNameImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTControlWithIEDName()
   * @generated
   */
  int TCONTROL_WITH_IED_NAME = 33;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_IED_NAME__ANY = TCONTROL__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_IED_NAME__TEXT = TCONTROL__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_IED_NAME__PRIVATE = TCONTROL__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_IED_NAME__ANY_ATTRIBUTE = TCONTROL__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_IED_NAME__DESC = TCONTROL__DESC;

  /**
   * The feature id for the '<em><b>Dat Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_IED_NAME__DAT_SET = TCONTROL__DAT_SET;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_IED_NAME__NAME = TCONTROL__NAME;

  /**
   * The feature id for the '<em><b>IED Name</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_IED_NAME__IED_NAME = TCONTROL_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Conf Rev</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_IED_NAME__CONF_REV = TCONTROL_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TControl With IED Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_IED_NAME_FEATURE_COUNT = TCONTROL_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TControl With IED Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_IED_NAME_OPERATION_COUNT = TCONTROL_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TControlWithTriggerOptImpl <em>TControl With Trigger Opt</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TControlWithTriggerOptImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTControlWithTriggerOpt()
   * @generated
   */
  int TCONTROL_WITH_TRIGGER_OPT = 34;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_TRIGGER_OPT__ANY = TCONTROL__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_TRIGGER_OPT__TEXT = TCONTROL__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_TRIGGER_OPT__PRIVATE = TCONTROL__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_TRIGGER_OPT__ANY_ATTRIBUTE = TCONTROL__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_TRIGGER_OPT__DESC = TCONTROL__DESC;

  /**
   * The feature id for the '<em><b>Dat Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_TRIGGER_OPT__DAT_SET = TCONTROL__DAT_SET;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_TRIGGER_OPT__NAME = TCONTROL__NAME;

  /**
   * The feature id for the '<em><b>Trg Ops</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_TRIGGER_OPT__TRG_OPS = TCONTROL_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Intg Pd</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_TRIGGER_OPT__INTG_PD = TCONTROL_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TControl With Trigger Opt</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT = TCONTROL_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TControl With Trigger Opt</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TCONTROL_WITH_TRIGGER_OPT_OPERATION_COUNT = TCONTROL_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TDAImpl <em>TDA</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TDAImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDA()
   * @generated
   */
  int TDA = 35;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__ANY = TABSTRACT_DATA_ATTRIBUTE__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__TEXT = TABSTRACT_DATA_ATTRIBUTE__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__PRIVATE = TABSTRACT_DATA_ATTRIBUTE__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__ANY_ATTRIBUTE = TABSTRACT_DATA_ATTRIBUTE__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__DESC = TABSTRACT_DATA_ATTRIBUTE__DESC;

  /**
   * The feature id for the '<em><b>Val</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__VAL = TABSTRACT_DATA_ATTRIBUTE__VAL;

  /**
   * The feature id for the '<em><b>BType</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__BTYPE = TABSTRACT_DATA_ATTRIBUTE__BTYPE;

  /**
   * The feature id for the '<em><b>Count</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__COUNT = TABSTRACT_DATA_ATTRIBUTE__COUNT;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__NAME = TABSTRACT_DATA_ATTRIBUTE__NAME;

  /**
   * The feature id for the '<em><b>SAddr</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__SADDR = TABSTRACT_DATA_ATTRIBUTE__SADDR;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__TYPE = TABSTRACT_DATA_ATTRIBUTE__TYPE;

  /**
   * The feature id for the '<em><b>Val Kind</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__VAL_KIND = TABSTRACT_DATA_ATTRIBUTE__VAL_KIND;

  /**
   * The feature id for the '<em><b>Dchg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__DCHG = TABSTRACT_DATA_ATTRIBUTE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Dupd</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__DUPD = TABSTRACT_DATA_ATTRIBUTE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Fc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__FC = TABSTRACT_DATA_ATTRIBUTE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Qchg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA__QCHG = TABSTRACT_DATA_ATTRIBUTE_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>TDA</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA_FEATURE_COUNT = TABSTRACT_DATA_ATTRIBUTE_FEATURE_COUNT + 4;

  /**
   * The number of operations of the '<em>TDA</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA_OPERATION_COUNT = TABSTRACT_DATA_ATTRIBUTE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TDAIImpl <em>TDAI</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TDAIImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDAI()
   * @generated
   */
  int TDAI = 36;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDAI__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDAI__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDAI__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDAI__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDAI__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Val</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDAI__VAL = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Ix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDAI__IX = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDAI__NAME = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>SAddr</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDAI__SADDR = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Val Kind</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDAI__VAL_KIND = TUN_NAMING_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>TDAI</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDAI_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 5;

  /**
   * The number of operations of the '<em>TDAI</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDAI_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TDataSetImpl <em>TData Set</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TDataSetImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDataSet()
   * @generated
   */
  int TDATA_SET = 37;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_SET__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_SET__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_SET__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_SET__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_SET__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_SET__GROUP = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>FCDA</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_SET__FCDA = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_SET__NAME = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>TData Set</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_SET_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>TData Set</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_SET_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TDataTypeTemplatesImpl <em>TData Type Templates</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TDataTypeTemplatesImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDataTypeTemplates()
   * @generated
   */
  int TDATA_TYPE_TEMPLATES = 38;

  /**
   * The feature id for the '<em><b>LNode Type</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_TYPE_TEMPLATES__LNODE_TYPE = 0;

  /**
   * The feature id for the '<em><b>DO Type</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_TYPE_TEMPLATES__DO_TYPE = 1;

  /**
   * The feature id for the '<em><b>DA Type</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_TYPE_TEMPLATES__DA_TYPE = 2;

  /**
   * The feature id for the '<em><b>Enum Type</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_TYPE_TEMPLATES__ENUM_TYPE = 3;

  /**
   * The number of structural features of the '<em>TData Type Templates</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_TYPE_TEMPLATES_FEATURE_COUNT = 4;

  /**
   * The number of operations of the '<em>TData Type Templates</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDATA_TYPE_TEMPLATES_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TIDNamingImpl <em>TID Naming</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TIDNamingImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTIDNaming()
   * @generated
   */
  int TID_NAMING = 58;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TID_NAMING__ANY = TBASE_ELEMENT__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TID_NAMING__TEXT = TBASE_ELEMENT__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TID_NAMING__PRIVATE = TBASE_ELEMENT__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TID_NAMING__ANY_ATTRIBUTE = TBASE_ELEMENT__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TID_NAMING__DESC = TBASE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TID_NAMING__ID = TBASE_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TID Naming</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TID_NAMING_FEATURE_COUNT = TBASE_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TID Naming</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TID_NAMING_OPERATION_COUNT = TBASE_ELEMENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TDATypeImpl <em>TDA Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TDATypeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDAType()
   * @generated
   */
  int TDA_TYPE = 39;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA_TYPE__ANY = TID_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA_TYPE__TEXT = TID_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA_TYPE__PRIVATE = TID_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA_TYPE__ANY_ATTRIBUTE = TID_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA_TYPE__DESC = TID_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA_TYPE__ID = TID_NAMING__ID;

  /**
   * The feature id for the '<em><b>BDA</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA_TYPE__BDA = TID_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Prot Ns</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA_TYPE__PROT_NS = TID_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Ied Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA_TYPE__IED_TYPE = TID_NAMING_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>TDA Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA_TYPE_FEATURE_COUNT = TID_NAMING_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>TDA Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDA_TYPE_OPERATION_COUNT = TID_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TDOImpl <em>TDO</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TDOImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDO()
   * @generated
   */
  int TDO = 40;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Access Control</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO__ACCESS_CONTROL = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO__NAME = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Transient</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO__TRANSIENT = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO__TYPE = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>TDO</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 4;

  /**
   * The number of operations of the '<em>TDO</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TDOIImpl <em>TDOI</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TDOIImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDOI()
   * @generated
   */
  int TDOI = 41;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDOI__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDOI__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDOI__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDOI__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDOI__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDOI__GROUP = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>SDI</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDOI__SDI = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>DAI</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDOI__DAI = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Access Control</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDOI__ACCESS_CONTROL = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Ix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDOI__IX = TUN_NAMING_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDOI__NAME = TUN_NAMING_FEATURE_COUNT + 5;

  /**
   * The number of structural features of the '<em>TDOI</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDOI_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 6;

  /**
   * The number of operations of the '<em>TDOI</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDOI_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TDOTypeImpl <em>TDO Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TDOTypeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDOType()
   * @generated
   */
  int TDO_TYPE = 42;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_TYPE__ANY = TID_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_TYPE__TEXT = TID_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_TYPE__PRIVATE = TID_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_TYPE__ANY_ATTRIBUTE = TID_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_TYPE__DESC = TID_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_TYPE__ID = TID_NAMING__ID;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_TYPE__GROUP = TID_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>SDO</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_TYPE__SDO = TID_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>DA</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_TYPE__DA = TID_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Cdc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_TYPE__CDC = TID_NAMING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Ied Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_TYPE__IED_TYPE = TID_NAMING_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>TDO Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_TYPE_FEATURE_COUNT = TID_NAMING_FEATURE_COUNT + 5;

  /**
   * The number of operations of the '<em>TDO Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDO_TYPE_OPERATION_COUNT = TID_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TDurationInMilliSecImpl <em>TDuration In Milli Sec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TDurationInMilliSecImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDurationInMilliSec()
   * @generated
   */
  int TDURATION_IN_MILLI_SEC = 43;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDURATION_IN_MILLI_SEC__VALUE = 0;

  /**
   * The feature id for the '<em><b>Multiplier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDURATION_IN_MILLI_SEC__MULTIPLIER = 1;

  /**
   * The feature id for the '<em><b>Unit</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDURATION_IN_MILLI_SEC__UNIT = 2;

  /**
   * The number of structural features of the '<em>TDuration In Milli Sec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDURATION_IN_MILLI_SEC_FEATURE_COUNT = 3;

  /**
   * The number of operations of the '<em>TDuration In Milli Sec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDURATION_IN_MILLI_SEC_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TValueWithUnitImpl <em>TValue With Unit</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TValueWithUnitImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTValueWithUnit()
   * @generated
   */
  int TVALUE_WITH_UNIT = 126;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVALUE_WITH_UNIT__VALUE = 0;

  /**
   * The feature id for the '<em><b>Multiplier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVALUE_WITH_UNIT__MULTIPLIER = 1;

  /**
   * The feature id for the '<em><b>Unit</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVALUE_WITH_UNIT__UNIT = 2;

  /**
   * The number of structural features of the '<em>TValue With Unit</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVALUE_WITH_UNIT_FEATURE_COUNT = 3;

  /**
   * The number of operations of the '<em>TValue With Unit</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVALUE_WITH_UNIT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TDurationInSecImpl <em>TDuration In Sec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TDurationInSecImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDurationInSec()
   * @generated
   */
  int TDURATION_IN_SEC = 44;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDURATION_IN_SEC__VALUE = TVALUE_WITH_UNIT__VALUE;

  /**
   * The feature id for the '<em><b>Multiplier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDURATION_IN_SEC__MULTIPLIER = TVALUE_WITH_UNIT__MULTIPLIER;

  /**
   * The feature id for the '<em><b>Unit</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDURATION_IN_SEC__UNIT = TVALUE_WITH_UNIT__UNIT;

  /**
   * The number of structural features of the '<em>TDuration In Sec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDURATION_IN_SEC_FEATURE_COUNT = TVALUE_WITH_UNIT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TDuration In Sec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TDURATION_IN_SEC_OPERATION_COUNT = TVALUE_WITH_UNIT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TEnumTypeImpl <em>TEnum Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TEnumTypeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTEnumType()
   * @generated
   */
  int TENUM_TYPE = 45;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_TYPE__ANY = TID_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_TYPE__TEXT = TID_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_TYPE__PRIVATE = TID_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_TYPE__ANY_ATTRIBUTE = TID_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_TYPE__DESC = TID_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_TYPE__ID = TID_NAMING__ID;

  /**
   * The feature id for the '<em><b>Enum Val</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_TYPE__ENUM_VAL = TID_NAMING_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TEnum Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_TYPE_FEATURE_COUNT = TID_NAMING_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TEnum Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_TYPE_OPERATION_COUNT = TID_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TEnumValImpl <em>TEnum Val</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TEnumValImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTEnumVal()
   * @generated
   */
  int TENUM_VAL = 46;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_VAL__VALUE = 0;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_VAL__DESC = 1;

  /**
   * The feature id for the '<em><b>Ord</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_VAL__ORD = 2;

  /**
   * The number of structural features of the '<em>TEnum Val</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_VAL_FEATURE_COUNT = 3;

  /**
   * The number of operations of the '<em>TEnum Val</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TENUM_VAL_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl <em>TExt Ref</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTExtRef()
   * @generated
   */
  int TEXT_REF = 49;

  /**
   * The feature id for the '<em><b>Da Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__DA_NAME = 0;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__DESC = 1;

  /**
   * The feature id for the '<em><b>Do Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__DO_NAME = 2;

  /**
   * The feature id for the '<em><b>Ied Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__IED_NAME = 3;

  /**
   * The feature id for the '<em><b>Int Addr</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__INT_ADDR = 4;

  /**
   * The feature id for the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__LD_INST = 5;

  /**
   * The feature id for the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__LN_CLASS = 6;

  /**
   * The feature id for the '<em><b>Ln Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__LN_INST = 7;

  /**
   * The feature id for the '<em><b>Prefix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__PREFIX = 8;

  /**
   * The feature id for the '<em><b>Service Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__SERVICE_TYPE = 9;

  /**
   * The feature id for the '<em><b>Src CB Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__SRC_CB_NAME = 10;

  /**
   * The feature id for the '<em><b>Src LD Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__SRC_LD_INST = 11;

  /**
   * The feature id for the '<em><b>Src LN Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__SRC_LN_CLASS = 12;

  /**
   * The feature id for the '<em><b>Src LN Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__SRC_LN_INST = 13;

  /**
   * The feature id for the '<em><b>Src Prefix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF__SRC_PREFIX = 14;

  /**
   * The number of structural features of the '<em>TExt Ref</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF_FEATURE_COUNT = 15;

  /**
   * The number of operations of the '<em>TExt Ref</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_REF_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TFCDAImpl <em>TFCDA</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TFCDAImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTFCDA()
   * @generated
   */
  int TFCDA = 50;

  /**
   * The feature id for the '<em><b>Da Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFCDA__DA_NAME = 0;

  /**
   * The feature id for the '<em><b>Do Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFCDA__DO_NAME = 1;

  /**
   * The feature id for the '<em><b>Fc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFCDA__FC = 2;

  /**
   * The feature id for the '<em><b>Ix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFCDA__IX = 3;

  /**
   * The feature id for the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFCDA__LD_INST = 4;

  /**
   * The feature id for the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFCDA__LN_CLASS = 5;

  /**
   * The feature id for the '<em><b>Ln Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFCDA__LN_INST = 6;

  /**
   * The feature id for the '<em><b>Prefix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFCDA__PREFIX = 7;

  /**
   * The number of structural features of the '<em>TFCDA</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFCDA_FEATURE_COUNT = 8;

  /**
   * The number of operations of the '<em>TFCDA</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFCDA_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TFunctionImpl <em>TFunction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TFunctionImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTFunction()
   * @generated
   */
  int TFUNCTION = 51;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFUNCTION__ANY = TPOWER_SYSTEM_RESOURCE__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFUNCTION__TEXT = TPOWER_SYSTEM_RESOURCE__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFUNCTION__PRIVATE = TPOWER_SYSTEM_RESOURCE__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFUNCTION__ANY_ATTRIBUTE = TPOWER_SYSTEM_RESOURCE__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFUNCTION__DESC = TPOWER_SYSTEM_RESOURCE__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFUNCTION__NAME = TPOWER_SYSTEM_RESOURCE__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFUNCTION__LNODE = TPOWER_SYSTEM_RESOURCE__LNODE;

  /**
   * The feature id for the '<em><b>Sub Function</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFUNCTION__SUB_FUNCTION = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>General Equipment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFUNCTION__GENERAL_EQUIPMENT = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Conducting Equipment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFUNCTION__CONDUCTING_EQUIPMENT = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFUNCTION__TYPE = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>TFunction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFUNCTION_FEATURE_COUNT = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 4;

  /**
   * The number of operations of the '<em>TFunction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TFUNCTION_OPERATION_COUNT = TPOWER_SYSTEM_RESOURCE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TGeneralEquipmentImpl <em>TGeneral Equipment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TGeneralEquipmentImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTGeneralEquipment()
   * @generated
   */
  int TGENERAL_EQUIPMENT = 52;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERAL_EQUIPMENT__ANY = TEQUIPMENT__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERAL_EQUIPMENT__TEXT = TEQUIPMENT__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERAL_EQUIPMENT__PRIVATE = TEQUIPMENT__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERAL_EQUIPMENT__ANY_ATTRIBUTE = TEQUIPMENT__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERAL_EQUIPMENT__DESC = TEQUIPMENT__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERAL_EQUIPMENT__NAME = TEQUIPMENT__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERAL_EQUIPMENT__LNODE = TEQUIPMENT__LNODE;

  /**
   * The feature id for the '<em><b>Virtual</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERAL_EQUIPMENT__VIRTUAL = TEQUIPMENT__VIRTUAL;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERAL_EQUIPMENT__TYPE = TEQUIPMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TGeneral Equipment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERAL_EQUIPMENT_FEATURE_COUNT = TEQUIPMENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TGeneral Equipment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERAL_EQUIPMENT_OPERATION_COUNT = TEQUIPMENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TGSEImpl <em>TGSE</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TGSEImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTGSE()
   * @generated
   */
  int TGSE = 53;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE__ANY = TCONTROL_BLOCK__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE__TEXT = TCONTROL_BLOCK__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE__PRIVATE = TCONTROL_BLOCK__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE__ANY_ATTRIBUTE = TCONTROL_BLOCK__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE__DESC = TCONTROL_BLOCK__DESC;

  /**
   * The feature id for the '<em><b>Address</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE__ADDRESS = TCONTROL_BLOCK__ADDRESS;

  /**
   * The feature id for the '<em><b>Cb Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE__CB_NAME = TCONTROL_BLOCK__CB_NAME;

  /**
   * The feature id for the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE__LD_INST = TCONTROL_BLOCK__LD_INST;

  /**
   * The feature id for the '<em><b>Min Time</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE__MIN_TIME = TCONTROL_BLOCK_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Max Time</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE__MAX_TIME = TCONTROL_BLOCK_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TGSE</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_FEATURE_COUNT = TCONTROL_BLOCK_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TGSE</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_OPERATION_COUNT = TCONTROL_BLOCK_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TGSEControlImpl <em>TGSE Control</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TGSEControlImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTGSEControl()
   * @generated
   */
  int TGSE_CONTROL = 54;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL__ANY = TCONTROL_WITH_IED_NAME__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL__TEXT = TCONTROL_WITH_IED_NAME__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL__PRIVATE = TCONTROL_WITH_IED_NAME__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL__ANY_ATTRIBUTE = TCONTROL_WITH_IED_NAME__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL__DESC = TCONTROL_WITH_IED_NAME__DESC;

  /**
   * The feature id for the '<em><b>Dat Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL__DAT_SET = TCONTROL_WITH_IED_NAME__DAT_SET;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL__NAME = TCONTROL_WITH_IED_NAME__NAME;

  /**
   * The feature id for the '<em><b>IED Name</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL__IED_NAME = TCONTROL_WITH_IED_NAME__IED_NAME;

  /**
   * The feature id for the '<em><b>Conf Rev</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL__CONF_REV = TCONTROL_WITH_IED_NAME__CONF_REV;

  /**
   * The feature id for the '<em><b>App ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL__APP_ID = TCONTROL_WITH_IED_NAME_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Fixed Offs</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL__FIXED_OFFS = TCONTROL_WITH_IED_NAME_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL__TYPE = TCONTROL_WITH_IED_NAME_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>TGSE Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL_FEATURE_COUNT = TCONTROL_WITH_IED_NAME_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>TGSE Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_CONTROL_OPERATION_COUNT = TCONTROL_WITH_IED_NAME_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TServiceSettingsImpl <em>TService Settings</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TServiceSettingsImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServiceSettings()
   * @generated
   */
  int TSERVICE_SETTINGS = 106;

  /**
   * The feature id for the '<em><b>Cb Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_SETTINGS__CB_NAME = 0;

  /**
   * The feature id for the '<em><b>Dat Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_SETTINGS__DAT_SET = 1;

  /**
   * The number of structural features of the '<em>TService Settings</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_SETTINGS_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>TService Settings</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_SETTINGS_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TGSESettingsImpl <em>TGSE Settings</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TGSESettingsImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTGSESettings()
   * @generated
   */
  int TGSE_SETTINGS = 55;

  /**
   * The feature id for the '<em><b>Cb Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_SETTINGS__CB_NAME = TSERVICE_SETTINGS__CB_NAME;

  /**
   * The feature id for the '<em><b>Dat Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_SETTINGS__DAT_SET = TSERVICE_SETTINGS__DAT_SET;

  /**
   * The feature id for the '<em><b>App ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_SETTINGS__APP_ID = TSERVICE_SETTINGS_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Data Label</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_SETTINGS__DATA_LABEL = TSERVICE_SETTINGS_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TGSE Settings</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_SETTINGS_FEATURE_COUNT = TSERVICE_SETTINGS_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TGSE Settings</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGSE_SETTINGS_OPERATION_COUNT = TSERVICE_SETTINGS_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.THeaderImpl <em>THeader</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.THeaderImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTHeader()
   * @generated
   */
  int THEADER = 56;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THEADER__TEXT = 0;

  /**
   * The feature id for the '<em><b>History</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THEADER__HISTORY = 1;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THEADER__ID = 2;

  /**
   * The feature id for the '<em><b>Name Structure</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THEADER__NAME_STRUCTURE = 3;

  /**
   * The feature id for the '<em><b>Revision</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THEADER__REVISION = 4;

  /**
   * The feature id for the '<em><b>Tool ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THEADER__TOOL_ID = 5;

  /**
   * The feature id for the '<em><b>Version</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THEADER__VERSION = 6;

  /**
   * The number of structural features of the '<em>THeader</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THEADER_FEATURE_COUNT = 7;

  /**
   * The number of operations of the '<em>THeader</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THEADER_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.THitemImpl <em>THitem</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.THitemImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTHitem()
   * @generated
   */
  int THITEM = 57;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THITEM__MIXED = TANY_CONTENT_FROM_OTHER_NAMESPACE__MIXED;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THITEM__GROUP = TANY_CONTENT_FROM_OTHER_NAMESPACE__GROUP;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THITEM__ANY = TANY_CONTENT_FROM_OTHER_NAMESPACE__ANY;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THITEM__ANY_ATTRIBUTE = TANY_CONTENT_FROM_OTHER_NAMESPACE__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Revision</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THITEM__REVISION = TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Version</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THITEM__VERSION = TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>What</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THITEM__WHAT = TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>When</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THITEM__WHEN = TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Who</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THITEM__WHO = TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Why</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THITEM__WHY = TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT + 5;

  /**
   * The number of structural features of the '<em>THitem</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THITEM_FEATURE_COUNT = TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT + 6;

  /**
   * The number of operations of the '<em>THitem</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THITEM_OPERATION_COUNT = TANY_CONTENT_FROM_OTHER_NAMESPACE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TIEDImpl <em>TIED</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TIEDImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTIED()
   * @generated
   */
  int TIED = 59;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Services</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__SERVICES = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Access Point</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__ACCESS_POINT = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Config Version</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__CONFIG_VERSION = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Eng Right</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__ENG_RIGHT = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Manufacturer</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__MANUFACTURER = TUN_NAMING_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__NAME = TUN_NAMING_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Original Scl Revision</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__ORIGINAL_SCL_REVISION = TUN_NAMING_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Original Scl Version</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__ORIGINAL_SCL_VERSION = TUN_NAMING_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Owner</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__OWNER = TUN_NAMING_FEATURE_COUNT + 8;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED__TYPE = TUN_NAMING_FEATURE_COUNT + 9;

  /**
   * The number of structural features of the '<em>TIED</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 10;

  /**
   * The number of operations of the '<em>TIED</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIED_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TInputsImpl <em>TInputs</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TInputsImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTInputs()
   * @generated
   */
  int TINPUTS = 60;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TINPUTS__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TINPUTS__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TINPUTS__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TINPUTS__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TINPUTS__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Ext Ref</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TINPUTS__EXT_REF = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TInputs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TINPUTS_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TInputs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TINPUTS_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TLDeviceImpl <em>TL Device</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TLDeviceImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLDevice()
   * @generated
   */
  int TL_DEVICE = 61;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_DEVICE__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_DEVICE__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_DEVICE__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_DEVICE__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_DEVICE__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>LN0</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_DEVICE__LN0 = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>LN</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_DEVICE__LN = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Access Control</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_DEVICE__ACCESS_CONTROL = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_DEVICE__INST = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Ld Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_DEVICE__LD_NAME = TUN_NAMING_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>TL Device</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_DEVICE_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 5;

  /**
   * The number of operations of the '<em>TL Device</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_DEVICE_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TLNImpl <em>TLN</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TLNImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLN()
   * @generated
   */
  int TLN = 62;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__ANY = TANY_LN__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__TEXT = TANY_LN__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__PRIVATE = TANY_LN__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__ANY_ATTRIBUTE = TANY_LN__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__DESC = TANY_LN__DESC;

  /**
   * The feature id for the '<em><b>Data Set</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__DATA_SET = TANY_LN__DATA_SET;

  /**
   * The feature id for the '<em><b>Report Control</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__REPORT_CONTROL = TANY_LN__REPORT_CONTROL;

  /**
   * The feature id for the '<em><b>Log Control</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__LOG_CONTROL = TANY_LN__LOG_CONTROL;

  /**
   * The feature id for the '<em><b>DOI</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__DOI = TANY_LN__DOI;

  /**
   * The feature id for the '<em><b>Inputs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__INPUTS = TANY_LN__INPUTS;

  /**
   * The feature id for the '<em><b>Log</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__LOG = TANY_LN__LOG;

  /**
   * The feature id for the '<em><b>Ln Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__LN_TYPE = TANY_LN__LN_TYPE;

  /**
   * The feature id for the '<em><b>Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__INST = TANY_LN_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__LN_CLASS = TANY_LN_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Prefix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN__PREFIX = TANY_LN_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>TLN</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN_FEATURE_COUNT = TANY_LN_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>TLN</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLN_OPERATION_COUNT = TANY_LN_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TLNodeImpl <em>TL Node</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TLNodeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLNode()
   * @generated
   */
  int TL_NODE = 64;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Ied Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE__IED_NAME = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE__LD_INST = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE__LN_CLASS = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Ln Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE__LN_INST = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Ln Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE__LN_TYPE = TUN_NAMING_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Prefix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE__PREFIX = TUN_NAMING_FEATURE_COUNT + 5;

  /**
   * The number of structural features of the '<em>TL Node</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 6;

  /**
   * The number of operations of the '<em>TL Node</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TLNodeTypeImpl <em>TL Node Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TLNodeTypeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLNodeType()
   * @generated
   */
  int TL_NODE_TYPE = 66;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_TYPE__ANY = TID_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_TYPE__TEXT = TID_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_TYPE__PRIVATE = TID_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_TYPE__ANY_ATTRIBUTE = TID_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_TYPE__DESC = TID_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_TYPE__ID = TID_NAMING__ID;

  /**
   * The feature id for the '<em><b>DO</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_TYPE__DO = TID_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Ied Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_TYPE__IED_TYPE = TID_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_TYPE__LN_CLASS = TID_NAMING_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>TL Node Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_TYPE_FEATURE_COUNT = TID_NAMING_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>TL Node Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TL_NODE_TYPE_OPERATION_COUNT = TID_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TLogImpl <em>TLog</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TLogImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLog()
   * @generated
   */
  int TLOG = 67;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG__NAME = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TLog</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TLog</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TLogControlImpl <em>TLog Control</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TLogControlImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLogControl()
   * @generated
   */
  int TLOG_CONTROL = 68;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__ANY = TCONTROL_WITH_TRIGGER_OPT__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__TEXT = TCONTROL_WITH_TRIGGER_OPT__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__PRIVATE = TCONTROL_WITH_TRIGGER_OPT__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__ANY_ATTRIBUTE = TCONTROL_WITH_TRIGGER_OPT__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__DESC = TCONTROL_WITH_TRIGGER_OPT__DESC;

  /**
   * The feature id for the '<em><b>Dat Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__DAT_SET = TCONTROL_WITH_TRIGGER_OPT__DAT_SET;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__NAME = TCONTROL_WITH_TRIGGER_OPT__NAME;

  /**
   * The feature id for the '<em><b>Trg Ops</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__TRG_OPS = TCONTROL_WITH_TRIGGER_OPT__TRG_OPS;

  /**
   * The feature id for the '<em><b>Intg Pd</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__INTG_PD = TCONTROL_WITH_TRIGGER_OPT__INTG_PD;

  /**
   * The feature id for the '<em><b>Buf Time</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__BUF_TIME = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__LD_INST = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Ln Class</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__LN_CLASS = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Ln Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__LN_INST = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Log Ena</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__LOG_ENA = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Log Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__LOG_NAME = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Prefix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__PREFIX = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Reason Code</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL__REASON_CODE = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 7;

  /**
   * The number of structural features of the '<em>TLog Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL_FEATURE_COUNT = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 8;

  /**
   * The number of operations of the '<em>TLog Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_CONTROL_OPERATION_COUNT = TCONTROL_WITH_TRIGGER_OPT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TLogSettingsImpl <em>TLog Settings</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TLogSettingsImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLogSettings()
   * @generated
   */
  int TLOG_SETTINGS = 69;

  /**
   * The feature id for the '<em><b>Cb Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_SETTINGS__CB_NAME = TSERVICE_SETTINGS__CB_NAME;

  /**
   * The feature id for the '<em><b>Dat Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_SETTINGS__DAT_SET = TSERVICE_SETTINGS__DAT_SET;

  /**
   * The feature id for the '<em><b>Intg Pd</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_SETTINGS__INTG_PD = TSERVICE_SETTINGS_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Log Ena</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_SETTINGS__LOG_ENA = TSERVICE_SETTINGS_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Trg Ops</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_SETTINGS__TRG_OPS = TSERVICE_SETTINGS_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>TLog Settings</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_SETTINGS_FEATURE_COUNT = TSERVICE_SETTINGS_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>TLog Settings</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TLOG_SETTINGS_OPERATION_COUNT = TSERVICE_SETTINGS_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPImpl <em>TP</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTP()
   * @generated
   */
  int TP = 71;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TP__VALUE = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TP__TYPE = 1;

  /**
   * The number of structural features of the '<em>TP</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TP_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>TP</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TP_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPAPPIDImpl <em>TPAPPID</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPAPPIDImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPAPPID()
   * @generated
   */
  int TPAPPID = 72;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPAPPID__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPAPPID__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPAPPID</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPAPPID_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPAPPID</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPAPPID_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPhysConnImpl <em>TPhys Conn</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPhysConnImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPhysConn()
   * @generated
   */
  int TPHYS_CONN = 73;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPHYS_CONN__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPHYS_CONN__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPHYS_CONN__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPHYS_CONN__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPHYS_CONN__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>P</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPHYS_CONN__P = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPHYS_CONN__TYPE = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TPhys Conn</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPHYS_CONN_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TPhys Conn</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPHYS_CONN_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPIPImpl <em>TPIP</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPIPImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPIP()
   * @generated
   */
  int TPIP = 74;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPIP__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPIP__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPIP</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPIP_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPIP</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPIP_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPIPGATEWAYImpl <em>TPIPGATEWAY</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPIPGATEWAYImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPIPGATEWAY()
   * @generated
   */
  int TPIPGATEWAY = 75;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPIPGATEWAY__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPIPGATEWAY__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPIPGATEWAY</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPIPGATEWAY_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPIPGATEWAY</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPIPGATEWAY_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPIPSUBNETImpl <em>TPIPSUBNET</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPIPSUBNETImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPIPSUBNET()
   * @generated
   */
  int TPIPSUBNET = 76;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPIPSUBNET__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPIPSUBNET__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPIPSUBNET</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPIPSUBNET_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPIPSUBNET</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPIPSUBNET_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPMACAddressImpl <em>TPMAC Address</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPMACAddressImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPMACAddress()
   * @generated
   */
  int TPMAC_ADDRESS = 77;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPMAC_ADDRESS__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPMAC_ADDRESS__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPMAC Address</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPMAC_ADDRESS_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPMAC Address</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPMAC_ADDRESS_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPPortImpl <em>TP Port</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPPortImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPPort()
   * @generated
   */
  int TP_PORT = 90;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TP_PORT__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TP_PORT__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TP Port</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TP_PORT_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TP Port</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TP_PORT_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPMMSPortImpl <em>TPMMS Port</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPMMSPortImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPMMSPort()
   * @generated
   */
  int TPMMS_PORT = 78;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPMMS_PORT__VALUE = TP_PORT__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPMMS_PORT__TYPE = TP_PORT__TYPE;

  /**
   * The number of structural features of the '<em>TPMMS Port</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPMMS_PORT_FEATURE_COUNT = TP_PORT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPMMS Port</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPMMS_PORT_OPERATION_COUNT = TP_PORT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPOSIAEInvokeImpl <em>TPOSIAE Invoke</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPOSIAEInvokeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPOSIAEInvoke()
   * @generated
   */
  int TPOSIAE_INVOKE = 79;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAE_INVOKE__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAE_INVOKE__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPOSIAE Invoke</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAE_INVOKE_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPOSIAE Invoke</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAE_INVOKE_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPOSIAEQualifierImpl <em>TPOSIAE Qualifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPOSIAEQualifierImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPOSIAEQualifier()
   * @generated
   */
  int TPOSIAE_QUALIFIER = 80;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAE_QUALIFIER__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAE_QUALIFIER__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPOSIAE Qualifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAE_QUALIFIER_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPOSIAE Qualifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAE_QUALIFIER_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPOSIAPInvokeImpl <em>TPOSIAP Invoke</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPOSIAPInvokeImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPOSIAPInvoke()
   * @generated
   */
  int TPOSIAP_INVOKE = 81;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAP_INVOKE__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAP_INVOKE__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPOSIAP Invoke</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAP_INVOKE_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPOSIAP Invoke</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAP_INVOKE_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPOSIAPTitleImpl <em>TPOSIAP Title</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPOSIAPTitleImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPOSIAPTitle()
   * @generated
   */
  int TPOSIAP_TITLE = 82;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAP_TITLE__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAP_TITLE__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPOSIAP Title</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAP_TITLE_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPOSIAP Title</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIAP_TITLE_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPOSINSAPImpl <em>TPOSINSAP</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPOSINSAPImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPOSINSAP()
   * @generated
   */
  int TPOSINSAP = 83;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSINSAP__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSINSAP__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPOSINSAP</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSINSAP_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPOSINSAP</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSINSAP_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPOSIPSELImpl <em>TPOSIPSEL</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPOSIPSELImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPOSIPSEL()
   * @generated
   */
  int TPOSIPSEL = 84;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIPSEL__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIPSEL__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPOSIPSEL</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIPSEL_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPOSIPSEL</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSIPSEL_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPOSISSELImpl <em>TPOSISSEL</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPOSISSELImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPOSISSEL()
   * @generated
   */
  int TPOSISSEL = 85;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSISSEL__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSISSEL__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPOSISSEL</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSISSEL_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPOSISSEL</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSISSEL_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPOSITSELImpl <em>TPOSITSEL</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPOSITSELImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPOSITSEL()
   * @generated
   */
  int TPOSITSEL = 86;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSITSEL__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSITSEL__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPOSITSEL</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSITSEL_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPOSITSEL</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOSITSEL_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPowerTransformerImpl <em>TPower Transformer</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPowerTransformerImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPowerTransformer()
   * @generated
   */
  int TPOWER_TRANSFORMER = 88;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_TRANSFORMER__ANY = TEQUIPMENT__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_TRANSFORMER__TEXT = TEQUIPMENT__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_TRANSFORMER__PRIVATE = TEQUIPMENT__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_TRANSFORMER__ANY_ATTRIBUTE = TEQUIPMENT__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_TRANSFORMER__DESC = TEQUIPMENT__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_TRANSFORMER__NAME = TEQUIPMENT__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_TRANSFORMER__LNODE = TEQUIPMENT__LNODE;

  /**
   * The feature id for the '<em><b>Virtual</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_TRANSFORMER__VIRTUAL = TEQUIPMENT__VIRTUAL;

  /**
   * The feature id for the '<em><b>Transformer Winding</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_TRANSFORMER__TRANSFORMER_WINDING = TEQUIPMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_TRANSFORMER__TYPE = TEQUIPMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TPower Transformer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_TRANSFORMER_FEATURE_COUNT = TEQUIPMENT_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TPower Transformer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPOWER_TRANSFORMER_OPERATION_COUNT = TEQUIPMENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPPhysConnImpl <em>TP Phys Conn</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPPhysConnImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPPhysConn()
   * @generated
   */
  int TP_PHYS_CONN = 89;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TP_PHYS_CONN__VALUE = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TP_PHYS_CONN__TYPE = 1;

  /**
   * The number of structural features of the '<em>TP Phys Conn</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TP_PHYS_CONN_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>TP Phys Conn</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TP_PHYS_CONN_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPrivateImpl <em>TPrivate</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPrivateImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPrivate()
   * @generated
   */
  int TPRIVATE = 91;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPRIVATE__MIXED = TANY_CONTENT_FROM_OTHER_NAMESPACE__MIXED;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPRIVATE__GROUP = TANY_CONTENT_FROM_OTHER_NAMESPACE__GROUP;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPRIVATE__ANY = TANY_CONTENT_FROM_OTHER_NAMESPACE__ANY;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPRIVATE__ANY_ATTRIBUTE = TANY_CONTENT_FROM_OTHER_NAMESPACE__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Source</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPRIVATE__SOURCE = TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPRIVATE__TYPE = TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TPrivate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPRIVATE_FEATURE_COUNT = TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TPrivate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPRIVATE_OPERATION_COUNT = TANY_CONTENT_FROM_OTHER_NAMESPACE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPSNTPPortImpl <em>TPSNTP Port</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPSNTPPortImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPSNTPPort()
   * @generated
   */
  int TPSNTP_PORT = 92;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPSNTP_PORT__VALUE = TP_PORT__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPSNTP_PORT__TYPE = TP_PORT__TYPE;

  /**
   * The number of structural features of the '<em>TPSNTP Port</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPSNTP_PORT_FEATURE_COUNT = TP_PORT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPSNTP Port</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPSNTP_PORT_OPERATION_COUNT = TP_PORT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPVLANIDImpl <em>TPVLANID</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPVLANIDImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPVLANID()
   * @generated
   */
  int TPVLANID = 93;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPVLANID__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPVLANID__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPVLANID</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPVLANID_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPVLANID</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPVLANID_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TPVLANPRIORITYImpl <em>TPVLANPRIORITY</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TPVLANPRIORITYImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPVLANPRIORITY()
   * @generated
   */
  int TPVLANPRIORITY = 94;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPVLANPRIORITY__VALUE = TP__VALUE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPVLANPRIORITY__TYPE = TP__TYPE;

  /**
   * The number of structural features of the '<em>TPVLANPRIORITY</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPVLANPRIORITY_FEATURE_COUNT = TP_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TPVLANPRIORITY</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TPVLANPRIORITY_OPERATION_COUNT = TP_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TReportControlImpl <em>TReport Control</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TReportControlImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTReportControl()
   * @generated
   */
  int TREPORT_CONTROL = 95;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__ANY = TCONTROL_WITH_TRIGGER_OPT__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__TEXT = TCONTROL_WITH_TRIGGER_OPT__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__PRIVATE = TCONTROL_WITH_TRIGGER_OPT__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__ANY_ATTRIBUTE = TCONTROL_WITH_TRIGGER_OPT__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__DESC = TCONTROL_WITH_TRIGGER_OPT__DESC;

  /**
   * The feature id for the '<em><b>Dat Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__DAT_SET = TCONTROL_WITH_TRIGGER_OPT__DAT_SET;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__NAME = TCONTROL_WITH_TRIGGER_OPT__NAME;

  /**
   * The feature id for the '<em><b>Trg Ops</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__TRG_OPS = TCONTROL_WITH_TRIGGER_OPT__TRG_OPS;

  /**
   * The feature id for the '<em><b>Intg Pd</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__INTG_PD = TCONTROL_WITH_TRIGGER_OPT__INTG_PD;

  /**
   * The feature id for the '<em><b>Opt Fields</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__OPT_FIELDS = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Rpt Enabled</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__RPT_ENABLED = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Buffered</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__BUFFERED = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Buf Time</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__BUF_TIME = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Conf Rev</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__CONF_REV = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Indexed</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__INDEXED = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Rpt ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL__RPT_ID = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 6;

  /**
   * The number of structural features of the '<em>TReport Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL_FEATURE_COUNT = TCONTROL_WITH_TRIGGER_OPT_FEATURE_COUNT + 7;

  /**
   * The number of operations of the '<em>TReport Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_CONTROL_OPERATION_COUNT = TCONTROL_WITH_TRIGGER_OPT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TReportSettingsImpl <em>TReport Settings</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TReportSettingsImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTReportSettings()
   * @generated
   */
  int TREPORT_SETTINGS = 96;

  /**
   * The feature id for the '<em><b>Cb Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_SETTINGS__CB_NAME = TSERVICE_SETTINGS__CB_NAME;

  /**
   * The feature id for the '<em><b>Dat Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_SETTINGS__DAT_SET = TSERVICE_SETTINGS__DAT_SET;

  /**
   * The feature id for the '<em><b>Buf Time</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_SETTINGS__BUF_TIME = TSERVICE_SETTINGS_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Intg Pd</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_SETTINGS__INTG_PD = TSERVICE_SETTINGS_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Opt Fields</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_SETTINGS__OPT_FIELDS = TSERVICE_SETTINGS_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Resv Tms</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_SETTINGS__RESV_TMS = TSERVICE_SETTINGS_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Rpt ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_SETTINGS__RPT_ID = TSERVICE_SETTINGS_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Trg Ops</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_SETTINGS__TRG_OPS = TSERVICE_SETTINGS_FEATURE_COUNT + 5;

  /**
   * The number of structural features of the '<em>TReport Settings</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_SETTINGS_FEATURE_COUNT = TSERVICE_SETTINGS_FEATURE_COUNT + 6;

  /**
   * The number of operations of the '<em>TReport Settings</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TREPORT_SETTINGS_OPERATION_COUNT = TSERVICE_SETTINGS_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TRptEnabledImpl <em>TRpt Enabled</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TRptEnabledImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTRptEnabled()
   * @generated
   */
  int TRPT_ENABLED = 97;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRPT_ENABLED__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRPT_ENABLED__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRPT_ENABLED__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRPT_ENABLED__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRPT_ENABLED__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Client LN</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRPT_ENABLED__CLIENT_LN = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Max</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRPT_ENABLED__MAX = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TRpt Enabled</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRPT_ENABLED_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TRpt Enabled</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRPT_ENABLED_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TSampledValueControlImpl <em>TSampled Value Control</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TSampledValueControlImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSampledValueControl()
   * @generated
   */
  int TSAMPLED_VALUE_CONTROL = 98;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__ANY = TCONTROL_WITH_IED_NAME__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__TEXT = TCONTROL_WITH_IED_NAME__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__PRIVATE = TCONTROL_WITH_IED_NAME__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__ANY_ATTRIBUTE = TCONTROL_WITH_IED_NAME__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__DESC = TCONTROL_WITH_IED_NAME__DESC;

  /**
   * The feature id for the '<em><b>Dat Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__DAT_SET = TCONTROL_WITH_IED_NAME__DAT_SET;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__NAME = TCONTROL_WITH_IED_NAME__NAME;

  /**
   * The feature id for the '<em><b>IED Name</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__IED_NAME = TCONTROL_WITH_IED_NAME__IED_NAME;

  /**
   * The feature id for the '<em><b>Conf Rev</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__CONF_REV = TCONTROL_WITH_IED_NAME__CONF_REV;

  /**
   * The feature id for the '<em><b>Smv Opts</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__SMV_OPTS = TCONTROL_WITH_IED_NAME_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Multicast</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__MULTICAST = TCONTROL_WITH_IED_NAME_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Nof ASDU</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__NOF_ASDU = TCONTROL_WITH_IED_NAME_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Smp Mod</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__SMP_MOD = TCONTROL_WITH_IED_NAME_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Smp Rate</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__SMP_RATE = TCONTROL_WITH_IED_NAME_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Smv ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL__SMV_ID = TCONTROL_WITH_IED_NAME_FEATURE_COUNT + 5;

  /**
   * The number of structural features of the '<em>TSampled Value Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL_FEATURE_COUNT = TCONTROL_WITH_IED_NAME_FEATURE_COUNT + 6;

  /**
   * The number of operations of the '<em>TSampled Value Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSAMPLED_VALUE_CONTROL_OPERATION_COUNT = TCONTROL_WITH_IED_NAME_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TSDIImpl <em>TSDI</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TSDIImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSDI()
   * @generated
   */
  int TSDI = 99;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDI__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDI__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDI__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDI__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDI__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDI__GROUP = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>SDI</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDI__SDI = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>DAI</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDI__DAI = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Ix</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDI__IX = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDI__NAME = TUN_NAMING_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>TSDI</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDI_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 5;

  /**
   * The number of operations of the '<em>TSDI</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDI_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TSDOImpl <em>TSDO</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TSDOImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSDO()
   * @generated
   */
  int TSDO = 100;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDO__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDO__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDO__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDO__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDO__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Count</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDO__COUNT = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDO__NAME = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDO__TYPE = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>TSDO</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDO_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>TSDO</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSDO_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TServerImpl <em>TServer</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TServerImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServer()
   * @generated
   */
  int TSERVER = 101;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Authentication</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER__AUTHENTICATION = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>LDevice</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER__LDEVICE = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Association</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER__ASSOCIATION = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Timeout</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER__TIMEOUT = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>TServer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 4;

  /**
   * The number of operations of the '<em>TServer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TServerAtImpl <em>TServer At</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TServerAtImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServerAt()
   * @generated
   */
  int TSERVER_AT = 102;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER_AT__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER_AT__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER_AT__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER_AT__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER_AT__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Ap Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER_AT__AP_NAME = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TServer At</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER_AT_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TServer At</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVER_AT_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TServiceWithMaxImpl <em>TService With Max</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TServiceWithMaxImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServiceWithMax()
   * @generated
   */
  int TSERVICE_WITH_MAX = 107;

  /**
   * The feature id for the '<em><b>Max</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_MAX__MAX = 0;

  /**
   * The number of structural features of the '<em>TService With Max</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_MAX_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>TService With Max</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_MAX_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TServiceConfReportControlImpl <em>TService Conf Report Control</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TServiceConfReportControlImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServiceConfReportControl()
   * @generated
   */
  int TSERVICE_CONF_REPORT_CONTROL = 103;

  /**
   * The feature id for the '<em><b>Max</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_CONF_REPORT_CONTROL__MAX = TSERVICE_WITH_MAX__MAX;

  /**
   * The feature id for the '<em><b>Buf Conf</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_CONF_REPORT_CONTROL__BUF_CONF = TSERVICE_WITH_MAX_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Buf Mode</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_CONF_REPORT_CONTROL__BUF_MODE = TSERVICE_WITH_MAX_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TService Conf Report Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_CONF_REPORT_CONTROL_FEATURE_COUNT = TSERVICE_WITH_MAX_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TService Conf Report Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_CONF_REPORT_CONTROL_OPERATION_COUNT = TSERVICE_WITH_MAX_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TServiceWithMaxAndMaxAttributesImpl <em>TService With Max And Max Attributes</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TServiceWithMaxAndMaxAttributesImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServiceWithMaxAndMaxAttributes()
   * @generated
   */
  int TSERVICE_WITH_MAX_AND_MAX_ATTRIBUTES = 108;

  /**
   * The feature id for the '<em><b>Max</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_MAX_AND_MAX_ATTRIBUTES__MAX = TSERVICE_WITH_MAX__MAX;

  /**
   * The feature id for the '<em><b>Max Attributes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_MAX_AND_MAX_ATTRIBUTES__MAX_ATTRIBUTES = TSERVICE_WITH_MAX_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TService With Max And Max Attributes</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_MAX_AND_MAX_ATTRIBUTES_FEATURE_COUNT = TSERVICE_WITH_MAX_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TService With Max And Max Attributes</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_MAX_AND_MAX_ATTRIBUTES_OPERATION_COUNT = TSERVICE_WITH_MAX_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TServiceForConfDataSetImpl <em>TService For Conf Data Set</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TServiceForConfDataSetImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServiceForConfDataSet()
   * @generated
   */
  int TSERVICE_FOR_CONF_DATA_SET = 104;

  /**
   * The feature id for the '<em><b>Max</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_FOR_CONF_DATA_SET__MAX = TSERVICE_WITH_MAX_AND_MAX_ATTRIBUTES__MAX;

  /**
   * The feature id for the '<em><b>Max Attributes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_FOR_CONF_DATA_SET__MAX_ATTRIBUTES = TSERVICE_WITH_MAX_AND_MAX_ATTRIBUTES__MAX_ATTRIBUTES;

  /**
   * The feature id for the '<em><b>Modify</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_FOR_CONF_DATA_SET__MODIFY = TSERVICE_WITH_MAX_AND_MAX_ATTRIBUTES_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TService For Conf Data Set</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_FOR_CONF_DATA_SET_FEATURE_COUNT = TSERVICE_WITH_MAX_AND_MAX_ATTRIBUTES_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TService For Conf Data Set</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_FOR_CONF_DATA_SET_OPERATION_COUNT = TSERVICE_WITH_MAX_AND_MAX_ATTRIBUTES_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl <em>TServices</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TServicesImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServices()
   * @generated
   */
  int TSERVICES = 105;

  /**
   * The feature id for the '<em><b>Dyn Association</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__DYN_ASSOCIATION = 0;

  /**
   * The feature id for the '<em><b>Setting Groups</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__SETTING_GROUPS = 1;

  /**
   * The feature id for the '<em><b>Get Directory</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__GET_DIRECTORY = 2;

  /**
   * The feature id for the '<em><b>Get Data Object Definition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__GET_DATA_OBJECT_DEFINITION = 3;

  /**
   * The feature id for the '<em><b>Data Object Directory</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__DATA_OBJECT_DIRECTORY = 4;

  /**
   * The feature id for the '<em><b>Get Data Set Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__GET_DATA_SET_VALUE = 5;

  /**
   * The feature id for the '<em><b>Set Data Set Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__SET_DATA_SET_VALUE = 6;

  /**
   * The feature id for the '<em><b>Data Set Directory</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__DATA_SET_DIRECTORY = 7;

  /**
   * The feature id for the '<em><b>Conf Data Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__CONF_DATA_SET = 8;

  /**
   * The feature id for the '<em><b>Dyn Data Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__DYN_DATA_SET = 9;

  /**
   * The feature id for the '<em><b>Read Write</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__READ_WRITE = 10;

  /**
   * The feature id for the '<em><b>Timer Activated Control</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__TIMER_ACTIVATED_CONTROL = 11;

  /**
   * The feature id for the '<em><b>Conf Report Control</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__CONF_REPORT_CONTROL = 12;

  /**
   * The feature id for the '<em><b>Get CB Values</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__GET_CB_VALUES = 13;

  /**
   * The feature id for the '<em><b>Conf Log Control</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__CONF_LOG_CONTROL = 14;

  /**
   * The feature id for the '<em><b>Report Settings</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__REPORT_SETTINGS = 15;

  /**
   * The feature id for the '<em><b>Log Settings</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__LOG_SETTINGS = 16;

  /**
   * The feature id for the '<em><b>GSE Settings</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__GSE_SETTINGS = 17;

  /**
   * The feature id for the '<em><b>SMV Settings</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__SMV_SETTINGS = 18;

  /**
   * The feature id for the '<em><b>GSE Dir</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__GSE_DIR = 19;

  /**
   * The feature id for the '<em><b>GOOSE</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__GOOSE = 20;

  /**
   * The feature id for the '<em><b>GSSE</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__GSSE = 21;

  /**
   * The feature id for the '<em><b>SM Vsc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__SM_VSC = 22;

  /**
   * The feature id for the '<em><b>File Handling</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__FILE_HANDLING = 23;

  /**
   * The feature id for the '<em><b>Conf LNs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__CONF_LNS = 24;

  /**
   * The feature id for the '<em><b>Client Services</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__CLIENT_SERVICES = 25;

  /**
   * The feature id for the '<em><b>Conf Ld Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__CONF_LD_NAME = 26;

  /**
   * The feature id for the '<em><b>Sup Subscription</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__SUP_SUBSCRIPTION = 27;

  /**
   * The feature id for the '<em><b>Conf Sig Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__CONF_SIG_REF = 28;

  /**
   * The feature id for the '<em><b>Name Length</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES__NAME_LENGTH = 29;

  /**
   * The number of structural features of the '<em>TServices</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES_FEATURE_COUNT = 30;

  /**
   * The number of operations of the '<em>TServices</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICES_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TServiceWithMaxAndModifyImpl <em>TService With Max And Modify</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TServiceWithMaxAndModifyImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServiceWithMaxAndModify()
   * @generated
   */
  int TSERVICE_WITH_MAX_AND_MODIFY = 109;

  /**
   * The feature id for the '<em><b>Max</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_MAX_AND_MODIFY__MAX = TSERVICE_WITH_MAX__MAX;

  /**
   * The feature id for the '<em><b>Modify</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_MAX_AND_MODIFY__MODIFY = TSERVICE_WITH_MAX_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TService With Max And Modify</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_MAX_AND_MODIFY_FEATURE_COUNT = TSERVICE_WITH_MAX_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TService With Max And Modify</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_MAX_AND_MODIFY_OPERATION_COUNT = TSERVICE_WITH_MAX_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TServiceWithOptionalMaxImpl <em>TService With Optional Max</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TServiceWithOptionalMaxImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServiceWithOptionalMax()
   * @generated
   */
  int TSERVICE_WITH_OPTIONAL_MAX = 110;

  /**
   * The feature id for the '<em><b>Max</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_OPTIONAL_MAX__MAX = 0;

  /**
   * The number of structural features of the '<em>TService With Optional Max</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_OPTIONAL_MAX_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>TService With Optional Max</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_WITH_OPTIONAL_MAX_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TServiceYesNoImpl <em>TService Yes No</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TServiceYesNoImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServiceYesNo()
   * @generated
   */
  int TSERVICE_YES_NO = 111;

  /**
   * The number of structural features of the '<em>TService Yes No</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_YES_NO_FEATURE_COUNT = 0;

  /**
   * The number of operations of the '<em>TService Yes No</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSERVICE_YES_NO_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TSettingControlImpl <em>TSetting Control</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TSettingControlImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSettingControl()
   * @generated
   */
  int TSETTING_CONTROL = 112;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSETTING_CONTROL__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSETTING_CONTROL__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSETTING_CONTROL__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSETTING_CONTROL__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSETTING_CONTROL__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Act SG</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSETTING_CONTROL__ACT_SG = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Num Of SGs</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSETTING_CONTROL__NUM_OF_SGS = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TSetting Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSETTING_CONTROL_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TSetting Control</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSETTING_CONTROL_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TSMVImpl <em>TSMV</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TSMVImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSMV()
   * @generated
   */
  int TSMV = 113;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV__ANY = TCONTROL_BLOCK__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV__TEXT = TCONTROL_BLOCK__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV__PRIVATE = TCONTROL_BLOCK__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV__ANY_ATTRIBUTE = TCONTROL_BLOCK__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV__DESC = TCONTROL_BLOCK__DESC;

  /**
   * The feature id for the '<em><b>Address</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV__ADDRESS = TCONTROL_BLOCK__ADDRESS;

  /**
   * The feature id for the '<em><b>Cb Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV__CB_NAME = TCONTROL_BLOCK__CB_NAME;

  /**
   * The feature id for the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV__LD_INST = TCONTROL_BLOCK__LD_INST;

  /**
   * The number of structural features of the '<em>TSMV</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_FEATURE_COUNT = TCONTROL_BLOCK_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TSMV</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_OPERATION_COUNT = TCONTROL_BLOCK_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TSMVSettingsImpl <em>TSMV Settings</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TSMVSettingsImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSMVSettings()
   * @generated
   */
  int TSMV_SETTINGS = 114;

  /**
   * The feature id for the '<em><b>Cb Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_SETTINGS__CB_NAME = TSERVICE_SETTINGS__CB_NAME;

  /**
   * The feature id for the '<em><b>Dat Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_SETTINGS__DAT_SET = TSERVICE_SETTINGS__DAT_SET;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_SETTINGS__GROUP = TSERVICE_SETTINGS_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Smp Rate</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_SETTINGS__SMP_RATE = TSERVICE_SETTINGS_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Samples Per Sec</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_SETTINGS__SAMPLES_PER_SEC = TSERVICE_SETTINGS_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Sec Per Samples</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_SETTINGS__SEC_PER_SAMPLES = TSERVICE_SETTINGS_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Opt Fields</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_SETTINGS__OPT_FIELDS = TSERVICE_SETTINGS_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Samples Per Sec1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_SETTINGS__SAMPLES_PER_SEC1 = TSERVICE_SETTINGS_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Smp Rate1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_SETTINGS__SMP_RATE1 = TSERVICE_SETTINGS_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Sv ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_SETTINGS__SV_ID = TSERVICE_SETTINGS_FEATURE_COUNT + 7;

  /**
   * The number of structural features of the '<em>TSMV Settings</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_SETTINGS_FEATURE_COUNT = TSERVICE_SETTINGS_FEATURE_COUNT + 8;

  /**
   * The number of operations of the '<em>TSMV Settings</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSMV_SETTINGS_OPERATION_COUNT = TSERVICE_SETTINGS_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TSubEquipmentImpl <em>TSub Equipment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TSubEquipmentImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSubEquipment()
   * @generated
   */
  int TSUB_EQUIPMENT = 115;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_EQUIPMENT__ANY = TPOWER_SYSTEM_RESOURCE__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_EQUIPMENT__TEXT = TPOWER_SYSTEM_RESOURCE__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_EQUIPMENT__PRIVATE = TPOWER_SYSTEM_RESOURCE__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_EQUIPMENT__ANY_ATTRIBUTE = TPOWER_SYSTEM_RESOURCE__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_EQUIPMENT__DESC = TPOWER_SYSTEM_RESOURCE__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_EQUIPMENT__NAME = TPOWER_SYSTEM_RESOURCE__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_EQUIPMENT__LNODE = TPOWER_SYSTEM_RESOURCE__LNODE;

  /**
   * The feature id for the '<em><b>Phase</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_EQUIPMENT__PHASE = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Virtual</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_EQUIPMENT__VIRTUAL = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TSub Equipment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_EQUIPMENT_FEATURE_COUNT = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TSub Equipment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_EQUIPMENT_OPERATION_COUNT = TPOWER_SYSTEM_RESOURCE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TSubFunctionImpl <em>TSub Function</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TSubFunctionImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSubFunction()
   * @generated
   */
  int TSUB_FUNCTION = 116;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_FUNCTION__ANY = TPOWER_SYSTEM_RESOURCE__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_FUNCTION__TEXT = TPOWER_SYSTEM_RESOURCE__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_FUNCTION__PRIVATE = TPOWER_SYSTEM_RESOURCE__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_FUNCTION__ANY_ATTRIBUTE = TPOWER_SYSTEM_RESOURCE__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_FUNCTION__DESC = TPOWER_SYSTEM_RESOURCE__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_FUNCTION__NAME = TPOWER_SYSTEM_RESOURCE__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_FUNCTION__LNODE = TPOWER_SYSTEM_RESOURCE__LNODE;

  /**
   * The feature id for the '<em><b>General Equipment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_FUNCTION__GENERAL_EQUIPMENT = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Conducting Equipment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_FUNCTION__CONDUCTING_EQUIPMENT = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_FUNCTION__TYPE = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>TSub Function</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_FUNCTION_FEATURE_COUNT = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>TSub Function</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_FUNCTION_OPERATION_COUNT = TPOWER_SYSTEM_RESOURCE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TSubNetworkImpl <em>TSub Network</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TSubNetworkImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSubNetwork()
   * @generated
   */
  int TSUB_NETWORK = 117;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_NETWORK__ANY = TNAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_NETWORK__TEXT = TNAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_NETWORK__PRIVATE = TNAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_NETWORK__ANY_ATTRIBUTE = TNAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_NETWORK__DESC = TNAMING__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_NETWORK__NAME = TNAMING__NAME;

  /**
   * The feature id for the '<em><b>Bit Rate</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_NETWORK__BIT_RATE = TNAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Connected AP</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_NETWORK__CONNECTED_AP = TNAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_NETWORK__TYPE = TNAMING_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>TSub Network</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_NETWORK_FEATURE_COUNT = TNAMING_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>TSub Network</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUB_NETWORK_OPERATION_COUNT = TNAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TSubstationImpl <em>TSubstation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TSubstationImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSubstation()
   * @generated
   */
  int TSUBSTATION = 118;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUBSTATION__ANY = TEQUIPMENT_CONTAINER__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUBSTATION__TEXT = TEQUIPMENT_CONTAINER__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUBSTATION__PRIVATE = TEQUIPMENT_CONTAINER__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUBSTATION__ANY_ATTRIBUTE = TEQUIPMENT_CONTAINER__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUBSTATION__DESC = TEQUIPMENT_CONTAINER__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUBSTATION__NAME = TEQUIPMENT_CONTAINER__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUBSTATION__LNODE = TEQUIPMENT_CONTAINER__LNODE;

  /**
   * The feature id for the '<em><b>Power Transformer</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUBSTATION__POWER_TRANSFORMER = TEQUIPMENT_CONTAINER__POWER_TRANSFORMER;

  /**
   * The feature id for the '<em><b>General Equipment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUBSTATION__GENERAL_EQUIPMENT = TEQUIPMENT_CONTAINER__GENERAL_EQUIPMENT;

  /**
   * The feature id for the '<em><b>Voltage Level</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUBSTATION__VOLTAGE_LEVEL = TEQUIPMENT_CONTAINER_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Function</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUBSTATION__FUNCTION = TEQUIPMENT_CONTAINER_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TSubstation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUBSTATION_FEATURE_COUNT = TEQUIPMENT_CONTAINER_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TSubstation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TSUBSTATION_OPERATION_COUNT = TEQUIPMENT_CONTAINER_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TTapChangerImpl <em>TTap Changer</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TTapChangerImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTTapChanger()
   * @generated
   */
  int TTAP_CHANGER = 119;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTAP_CHANGER__ANY = TPOWER_SYSTEM_RESOURCE__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTAP_CHANGER__TEXT = TPOWER_SYSTEM_RESOURCE__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTAP_CHANGER__PRIVATE = TPOWER_SYSTEM_RESOURCE__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTAP_CHANGER__ANY_ATTRIBUTE = TPOWER_SYSTEM_RESOURCE__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTAP_CHANGER__DESC = TPOWER_SYSTEM_RESOURCE__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTAP_CHANGER__NAME = TPOWER_SYSTEM_RESOURCE__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTAP_CHANGER__LNODE = TPOWER_SYSTEM_RESOURCE__LNODE;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTAP_CHANGER__TYPE = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Virtual</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTAP_CHANGER__VIRTUAL = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TTap Changer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTAP_CHANGER_FEATURE_COUNT = TPOWER_SYSTEM_RESOURCE_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TTap Changer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTAP_CHANGER_OPERATION_COUNT = TPOWER_SYSTEM_RESOURCE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TTerminalImpl <em>TTerminal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TTerminalImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTTerminal()
   * @generated
   */
  int TTERMINAL = 120;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL__ANY = TUN_NAMING__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL__TEXT = TUN_NAMING__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL__PRIVATE = TUN_NAMING__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL__ANY_ATTRIBUTE = TUN_NAMING__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL__DESC = TUN_NAMING__DESC;

  /**
   * The feature id for the '<em><b>Bay Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL__BAY_NAME = TUN_NAMING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>CNode Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL__CNODE_NAME = TUN_NAMING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Connectivity Node</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL__CONNECTIVITY_NODE = TUN_NAMING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL__NAME = TUN_NAMING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Neutral Point</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL__NEUTRAL_POINT = TUN_NAMING_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Substation Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL__SUBSTATION_NAME = TUN_NAMING_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Voltage Level Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL__VOLTAGE_LEVEL_NAME = TUN_NAMING_FEATURE_COUNT + 6;

  /**
   * The number of structural features of the '<em>TTerminal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL_FEATURE_COUNT = TUN_NAMING_FEATURE_COUNT + 7;

  /**
   * The number of operations of the '<em>TTerminal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTERMINAL_OPERATION_COUNT = TUN_NAMING_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TTextImpl <em>TText</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TTextImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTText()
   * @generated
   */
  int TTEXT = 121;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTEXT__MIXED = TANY_CONTENT_FROM_OTHER_NAMESPACE__MIXED;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTEXT__GROUP = TANY_CONTENT_FROM_OTHER_NAMESPACE__GROUP;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTEXT__ANY = TANY_CONTENT_FROM_OTHER_NAMESPACE__ANY;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTEXT__ANY_ATTRIBUTE = TANY_CONTENT_FROM_OTHER_NAMESPACE__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Source</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTEXT__SOURCE = TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TText</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTEXT_FEATURE_COUNT = TANY_CONTENT_FROM_OTHER_NAMESPACE_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>TText</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTEXT_OPERATION_COUNT = TANY_CONTENT_FROM_OTHER_NAMESPACE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TTransformerWindingImpl <em>TTransformer Winding</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TTransformerWindingImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTTransformerWinding()
   * @generated
   */
  int TTRANSFORMER_WINDING = 122;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING__ANY = TABSTRACT_CONDUCTING_EQUIPMENT__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING__TEXT = TABSTRACT_CONDUCTING_EQUIPMENT__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING__PRIVATE = TABSTRACT_CONDUCTING_EQUIPMENT__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING__ANY_ATTRIBUTE = TABSTRACT_CONDUCTING_EQUIPMENT__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING__DESC = TABSTRACT_CONDUCTING_EQUIPMENT__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING__NAME = TABSTRACT_CONDUCTING_EQUIPMENT__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING__LNODE = TABSTRACT_CONDUCTING_EQUIPMENT__LNODE;

  /**
   * The feature id for the '<em><b>Virtual</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING__VIRTUAL = TABSTRACT_CONDUCTING_EQUIPMENT__VIRTUAL;

  /**
   * The feature id for the '<em><b>Terminal</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING__TERMINAL = TABSTRACT_CONDUCTING_EQUIPMENT__TERMINAL;

  /**
   * The feature id for the '<em><b>Sub Equipment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING__SUB_EQUIPMENT = TABSTRACT_CONDUCTING_EQUIPMENT__SUB_EQUIPMENT;

  /**
   * The feature id for the '<em><b>Tap Changer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING__TAP_CHANGER = TABSTRACT_CONDUCTING_EQUIPMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING__TYPE = TABSTRACT_CONDUCTING_EQUIPMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>TTransformer Winding</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING_FEATURE_COUNT = TABSTRACT_CONDUCTING_EQUIPMENT_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>TTransformer Winding</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRANSFORMER_WINDING_OPERATION_COUNT = TABSTRACT_CONDUCTING_EQUIPMENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TTrgOpsImpl <em>TTrg Ops</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TTrgOpsImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTTrgOps()
   * @generated
   */
  int TTRG_OPS = 123;

  /**
   * The feature id for the '<em><b>Dchg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRG_OPS__DCHG = 0;

  /**
   * The feature id for the '<em><b>Dupd</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRG_OPS__DUPD = 1;

  /**
   * The feature id for the '<em><b>Gi</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRG_OPS__GI = 2;

  /**
   * The feature id for the '<em><b>Period</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRG_OPS__PERIOD = 3;

  /**
   * The feature id for the '<em><b>Qchg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRG_OPS__QCHG = 4;

  /**
   * The number of structural features of the '<em>TTrg Ops</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRG_OPS_FEATURE_COUNT = 5;

  /**
   * The number of operations of the '<em>TTrg Ops</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TTRG_OPS_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TValImpl <em>TVal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TValImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTVal()
   * @generated
   */
  int TVAL = 125;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVAL__VALUE = 0;

  /**
   * The feature id for the '<em><b>SGroup</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVAL__SGROUP = 1;

  /**
   * The number of structural features of the '<em>TVal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVAL_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>TVal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVAL_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TVoltageImpl <em>TVoltage</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TVoltageImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTVoltage()
   * @generated
   */
  int TVOLTAGE = 127;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE__VALUE = TVALUE_WITH_UNIT__VALUE;

  /**
   * The feature id for the '<em><b>Multiplier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE__MULTIPLIER = TVALUE_WITH_UNIT__MULTIPLIER;

  /**
   * The feature id for the '<em><b>Unit</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE__UNIT = TVALUE_WITH_UNIT__UNIT;

  /**
   * The number of structural features of the '<em>TVoltage</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_FEATURE_COUNT = TVALUE_WITH_UNIT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>TVoltage</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_OPERATION_COUNT = TVALUE_WITH_UNIT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.impl.TVoltageLevelImpl <em>TVoltage Level</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.TVoltageLevelImpl
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTVoltageLevel()
   * @generated
   */
  int TVOLTAGE_LEVEL = 128;

  /**
   * The feature id for the '<em><b>Any</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL__ANY = TEQUIPMENT_CONTAINER__ANY;

  /**
   * The feature id for the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL__TEXT = TEQUIPMENT_CONTAINER__TEXT;

  /**
   * The feature id for the '<em><b>Private</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL__PRIVATE = TEQUIPMENT_CONTAINER__PRIVATE;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL__ANY_ATTRIBUTE = TEQUIPMENT_CONTAINER__ANY_ATTRIBUTE;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL__DESC = TEQUIPMENT_CONTAINER__DESC;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL__NAME = TEQUIPMENT_CONTAINER__NAME;

  /**
   * The feature id for the '<em><b>LNode</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL__LNODE = TEQUIPMENT_CONTAINER__LNODE;

  /**
   * The feature id for the '<em><b>Power Transformer</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL__POWER_TRANSFORMER = TEQUIPMENT_CONTAINER__POWER_TRANSFORMER;

  /**
   * The feature id for the '<em><b>General Equipment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL__GENERAL_EQUIPMENT = TEQUIPMENT_CONTAINER__GENERAL_EQUIPMENT;

  /**
   * The feature id for the '<em><b>Voltage</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL__VOLTAGE = TEQUIPMENT_CONTAINER_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Bay</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL__BAY = TEQUIPMENT_CONTAINER_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Function</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL__FUNCTION = TEQUIPMENT_CONTAINER_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>TVoltage Level</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL_FEATURE_COUNT = TEQUIPMENT_CONTAINER_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>TVoltage Level</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TVOLTAGE_LEVEL_OPERATION_COUNT = TEQUIPMENT_CONTAINER_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.BufModeType <em>Buf Mode Type</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.BufModeType
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getBufModeType()
   * @generated
   */
  int BUF_MODE_TYPE = 129;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.NameStructureType <em>Name Structure Type</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.NameStructureType
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getNameStructureType()
   * @generated
   */
  int NAME_STRUCTURE_TYPE = 130;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TAssociationKindEnum <em>TAssociation Kind Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TAssociationKindEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAssociationKindEnum()
   * @generated
   */
  int TASSOCIATION_KIND_ENUM = 131;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TAuthenticationEnum <em>TAuthentication Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TAuthenticationEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAuthenticationEnum()
   * @generated
   */
  int TAUTHENTICATION_ENUM = 132;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupAEnum <em>TDomain LN Group AEnum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupAEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupAEnum()
   * @generated
   */
  int TDOMAIN_LN_GROUP_AENUM = 133;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupCEnum <em>TDomain LN Group CEnum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupCEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupCEnum()
   * @generated
   */
  int TDOMAIN_LN_GROUP_CENUM = 134;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupGEnum <em>TDomain LN Group GEnum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupGEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupGEnum()
   * @generated
   */
  int TDOMAIN_LN_GROUP_GENUM = 135;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupIEnum <em>TDomain LN Group IEnum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupIEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupIEnum()
   * @generated
   */
  int TDOMAIN_LN_GROUP_IENUM = 136;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupMEnum <em>TDomain LN Group MEnum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupMEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupMEnum()
   * @generated
   */
  int TDOMAIN_LN_GROUP_MENUM = 137;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupPEnum <em>TDomain LN Group PEnum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupPEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupPEnum()
   * @generated
   */
  int TDOMAIN_LN_GROUP_PENUM = 138;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupREnum <em>TDomain LN Group REnum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupREnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupREnum()
   * @generated
   */
  int TDOMAIN_LN_GROUP_RENUM = 139;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupSEnum <em>TDomain LN Group SEnum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupSEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupSEnum()
   * @generated
   */
  int TDOMAIN_LN_GROUP_SENUM = 140;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupTEnum <em>TDomain LN Group TEnum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupTEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupTEnum()
   * @generated
   */
  int TDOMAIN_LN_GROUP_TENUM = 141;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupXEnum <em>TDomain LN Group XEnum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupXEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupXEnum()
   * @generated
   */
  int TDOMAIN_LN_GROUP_XENUM = 142;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupYEnum <em>TDomain LN Group YEnum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupYEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupYEnum()
   * @generated
   */
  int TDOMAIN_LN_GROUP_YENUM = 143;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupZEnum <em>TDomain LN Group ZEnum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupZEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupZEnum()
   * @generated
   */
  int TDOMAIN_LN_GROUP_ZENUM = 144;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TFCEnum <em>TFC Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TFCEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTFCEnum()
   * @generated
   */
  int TFC_ENUM = 145;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TGSEControlTypeEnum <em>TGSE Control Type Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TGSEControlTypeEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTGSEControlTypeEnum()
   * @generated
   */
  int TGSE_CONTROL_TYPE_ENUM = 146;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TLLN0Enum <em>TLLN0 Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TLLN0Enum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLLN0Enum()
   * @generated
   */
  int TLLN0_ENUM = 147;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TLPHDEnum <em>TLPHD Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TLPHDEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLPHDEnum()
   * @generated
   */
  int TLPHD_ENUM = 148;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TPhaseEnum <em>TPhase Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPhaseEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPhaseEnum()
   * @generated
   */
  int TPHASE_ENUM = 149;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TPowerTransformerEnum <em>TPower Transformer Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPowerTransformerEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPowerTransformerEnum()
   * @generated
   */
  int TPOWER_TRANSFORMER_ENUM = 150;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TPredefinedAttributeNameEnum <em>TPredefined Attribute Name Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedAttributeNameEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedAttributeNameEnum()
   * @generated
   */
  int TPREDEFINED_ATTRIBUTE_NAME_ENUM = 151;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TPredefinedBasicTypeEnum <em>TPredefined Basic Type Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedBasicTypeEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedBasicTypeEnum()
   * @generated
   */
  int TPREDEFINED_BASIC_TYPE_ENUM = 152;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TPredefinedCDCEnum <em>TPredefined CDC Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedCDCEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedCDCEnum()
   * @generated
   */
  int TPREDEFINED_CDC_ENUM = 153;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TPredefinedCommonConductingEquipmentEnum <em>TPredefined Common Conducting Equipment Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedCommonConductingEquipmentEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedCommonConductingEquipmentEnum()
   * @generated
   */
  int TPREDEFINED_COMMON_CONDUCTING_EQUIPMENT_ENUM = 154;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TPredefinedGeneralEquipmentEnum <em>TPredefined General Equipment Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedGeneralEquipmentEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedGeneralEquipmentEnum()
   * @generated
   */
  int TPREDEFINED_GENERAL_EQUIPMENT_ENUM = 155;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TPredefinedPhysConnTypeEnum <em>TPredefined Phys Conn Type Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedPhysConnTypeEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedPhysConnTypeEnum()
   * @generated
   */
  int TPREDEFINED_PHYS_CONN_TYPE_ENUM = 156;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TPredefinedPTypeEnum <em>TPredefined PType Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedPTypeEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedPTypeEnum()
   * @generated
   */
  int TPREDEFINED_PTYPE_ENUM = 157;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TPredefinedPTypePhysConnEnum <em>TPredefined PType Phys Conn Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedPTypePhysConnEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedPTypePhysConnEnum()
   * @generated
   */
  int TPREDEFINED_PTYPE_PHYS_CONN_ENUM = 158;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TRightEnum <em>TRight Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TRightEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTRightEnum()
   * @generated
   */
  int TRIGHT_ENUM = 159;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum <em>TService Settings Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServiceSettingsEnum()
   * @generated
   */
  int TSERVICE_SETTINGS_ENUM = 160;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TServiceType <em>TService Type</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TServiceType
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServiceType()
   * @generated
   */
  int TSERVICE_TYPE = 161;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TSIUnitEnum <em>TSI Unit Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TSIUnitEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSIUnitEnum()
   * @generated
   */
  int TSI_UNIT_ENUM = 162;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TSmpMod <em>TSmp Mod</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TSmpMod
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSmpMod()
   * @generated
   */
  int TSMP_MOD = 163;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TTransformerWindingEnum <em>TTransformer Winding Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TTransformerWindingEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTTransformerWindingEnum()
   * @generated
   */
  int TTRANSFORMER_WINDING_ENUM = 164;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TUnitMultiplierEnum <em>TUnit Multiplier Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TUnitMultiplierEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTUnitMultiplierEnum()
   * @generated
   */
  int TUNIT_MULTIPLIER_ENUM = 165;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.scl.TValKindEnum <em>TVal Kind Enum</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TValKindEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTValKindEnum()
   * @generated
   */
  int TVAL_KIND_ENUM = 166;

  /**
   * The meta object id for the '<em>Act SG Type</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getActSGType()
   * @generated
   */
  int ACT_SG_TYPE = 167;

  /**
   * The meta object id for the '<em>Act SG Type Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Long
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getActSGTypeObject()
   * @generated
   */
  int ACT_SG_TYPE_OBJECT = 168;

  /**
   * The meta object id for the '<em>App ID Type</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getAppIDType()
   * @generated
   */
  int APP_ID_TYPE = 169;

  /**
   * The meta object id for the '<em>Buf Mode Type Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.BufModeType
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getBufModeTypeObject()
   * @generated
   */
  int BUF_MODE_TYPE_OBJECT = 170;

  /**
   * The meta object id for the '<em>Common Name Type</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getCommonNameType()
   * @generated
   */
  int COMMON_NAME_TYPE = 171;

  /**
   * The meta object id for the '<em>Id Hierarchy Type</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getIdHierarchyType()
   * @generated
   */
  int ID_HIERARCHY_TYPE = 172;

  /**
   * The meta object id for the '<em>Name Length Type</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getNameLengthType()
   * @generated
   */
  int NAME_LENGTH_TYPE = 173;

  /**
   * The meta object id for the '<em>Name Length Type Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Long
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getNameLengthTypeObject()
   * @generated
   */
  int NAME_LENGTH_TYPE_OBJECT = 174;

  /**
   * The meta object id for the '<em>Name Structure Type Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.NameStructureType
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getNameStructureTypeObject()
   * @generated
   */
  int NAME_STRUCTURE_TYPE_OBJECT = 175;

  /**
   * The meta object id for the '<em>Num Of SGs Type</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getNumOfSGsType()
   * @generated
   */
  int NUM_OF_SGS_TYPE = 176;

  /**
   * The meta object id for the '<em>Num Of SGs Type Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Long
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getNumOfSGsTypeObject()
   * @generated
   */
  int NUM_OF_SGS_TYPE_OBJECT = 177;

  /**
   * The meta object id for the '<em>Samples Per Sec Type</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getSamplesPerSecType()
   * @generated
   */
  int SAMPLES_PER_SEC_TYPE = 178;

  /**
   * The meta object id for the '<em>Samples Per Sec Type Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Long
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getSamplesPerSecTypeObject()
   * @generated
   */
  int SAMPLES_PER_SEC_TYPE_OBJECT = 179;

  /**
   * The meta object id for the '<em>Sec Per Samples Type</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getSecPerSamplesType()
   * @generated
   */
  int SEC_PER_SAMPLES_TYPE = 180;

  /**
   * The meta object id for the '<em>Sec Per Samples Type Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Long
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getSecPerSamplesTypeObject()
   * @generated
   */
  int SEC_PER_SAMPLES_TYPE_OBJECT = 181;

  /**
   * The meta object id for the '<em>Serial Number Type</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getSerialNumberType()
   * @generated
   */
  int SERIAL_NUMBER_TYPE = 182;

  /**
   * The meta object id for the '<em>Smp Rate Type</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getSmpRateType()
   * @generated
   */
  int SMP_RATE_TYPE = 183;

  /**
   * The meta object id for the '<em>Smp Rate Type Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Long
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getSmpRateTypeObject()
   * @generated
   */
  int SMP_RATE_TYPE_OBJECT = 184;

  /**
   * The meta object id for the '<em>Smv ID Type</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getSmvIDType()
   * @generated
   */
  int SMV_ID_TYPE = 185;

  /**
   * The meta object id for the '<em>TAccess Point Name</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAccessPointName()
   * @generated
   */
  int TACCESS_POINT_NAME = 186;

  /**
   * The meta object id for the '<em>TAcsi Name</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAcsiName()
   * @generated
   */
  int TACSI_NAME = 187;

  /**
   * The meta object id for the '<em>TAny Name</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAnyName()
   * @generated
   */
  int TANY_NAME = 188;

  /**
   * The meta object id for the '<em>TAssociation ID</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAssociationID()
   * @generated
   */
  int TASSOCIATION_ID = 189;

  /**
   * The meta object id for the '<em>TAssociation Kind Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TAssociationKindEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAssociationKindEnumObject()
   * @generated
   */
  int TASSOCIATION_KIND_ENUM_OBJECT = 190;

  /**
   * The meta object id for the '<em>TAttribute Name Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Object
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAttributeNameEnum()
   * @generated
   */
  int TATTRIBUTE_NAME_ENUM = 191;

  /**
   * The meta object id for the '<em>TAuthentication Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TAuthenticationEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTAuthenticationEnumObject()
   * @generated
   */
  int TAUTHENTICATION_ENUM_OBJECT = 192;

  /**
   * The meta object id for the '<em>TBasic Type Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedBasicTypeEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTBasicTypeEnum()
   * @generated
   */
  int TBASIC_TYPE_ENUM = 193;

  /**
   * The meta object id for the '<em>TCB Name</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTCBName()
   * @generated
   */
  int TCB_NAME = 194;

  /**
   * The meta object id for the '<em>TCDC Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedCDCEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTCDCEnum()
   * @generated
   */
  int TCDC_ENUM = 195;

  /**
   * The meta object id for the '<em>TCommon Conducting Equipment Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Object
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTCommonConductingEquipmentEnum()
   * @generated
   */
  int TCOMMON_CONDUCTING_EQUIPMENT_ENUM = 196;

  /**
   * The meta object id for the '<em>TDA Count</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Object
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDACount()
   * @generated
   */
  int TDA_COUNT = 197;

  /**
   * The meta object id for the '<em>TData Name</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDataName()
   * @generated
   */
  int TDATA_NAME = 198;

  /**
   * The meta object id for the '<em>TData Set Name</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDataSetName()
   * @generated
   */
  int TDATA_SET_NAME = 199;

  /**
   * The meta object id for the '<em>TDomain LN Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.common.util.Enumerator
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNEnum()
   * @generated
   */
  int TDOMAIN_LN_ENUM = 200;

  /**
   * The meta object id for the '<em>TDomain LN Group AEnum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupAEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupAEnumObject()
   * @generated
   */
  int TDOMAIN_LN_GROUP_AENUM_OBJECT = 201;

  /**
   * The meta object id for the '<em>TDomain LN Group CEnum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupCEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupCEnumObject()
   * @generated
   */
  int TDOMAIN_LN_GROUP_CENUM_OBJECT = 202;

  /**
   * The meta object id for the '<em>TDomain LN Group GEnum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupGEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupGEnumObject()
   * @generated
   */
  int TDOMAIN_LN_GROUP_GENUM_OBJECT = 203;

  /**
   * The meta object id for the '<em>TDomain LN Group IEnum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupIEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupIEnumObject()
   * @generated
   */
  int TDOMAIN_LN_GROUP_IENUM_OBJECT = 204;

  /**
   * The meta object id for the '<em>TDomain LN Group MEnum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupMEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupMEnumObject()
   * @generated
   */
  int TDOMAIN_LN_GROUP_MENUM_OBJECT = 205;

  /**
   * The meta object id for the '<em>TDomain LN Group PEnum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupPEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupPEnumObject()
   * @generated
   */
  int TDOMAIN_LN_GROUP_PENUM_OBJECT = 206;

  /**
   * The meta object id for the '<em>TDomain LN Group REnum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupREnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupREnumObject()
   * @generated
   */
  int TDOMAIN_LN_GROUP_RENUM_OBJECT = 207;

  /**
   * The meta object id for the '<em>TDomain LN Group SEnum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupSEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupSEnumObject()
   * @generated
   */
  int TDOMAIN_LN_GROUP_SENUM_OBJECT = 208;

  /**
   * The meta object id for the '<em>TDomain LN Group TEnum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupTEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupTEnumObject()
   * @generated
   */
  int TDOMAIN_LN_GROUP_TENUM_OBJECT = 209;

  /**
   * The meta object id for the '<em>TDomain LN Group XEnum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupXEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupXEnumObject()
   * @generated
   */
  int TDOMAIN_LN_GROUP_XENUM_OBJECT = 210;

  /**
   * The meta object id for the '<em>TDomain LN Group YEnum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupYEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupYEnumObject()
   * @generated
   */
  int TDOMAIN_LN_GROUP_YENUM_OBJECT = 211;

  /**
   * The meta object id for the '<em>TDomain LN Group ZEnum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupZEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTDomainLNGroupZEnumObject()
   * @generated
   */
  int TDOMAIN_LN_GROUP_ZENUM_OBJECT = 212;

  /**
   * The meta object id for the '<em>TEmpty</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTEmpty()
   * @generated
   */
  int TEMPTY = 213;

  /**
   * The meta object id for the '<em>TExtension Attribute Name Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTExtensionAttributeNameEnum()
   * @generated
   */
  int TEXTENSION_ATTRIBUTE_NAME_ENUM = 214;

  /**
   * The meta object id for the '<em>TExtension Equipment Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTExtensionEquipmentEnum()
   * @generated
   */
  int TEXTENSION_EQUIPMENT_ENUM = 215;

  /**
   * The meta object id for the '<em>TExtension General Equipment Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTExtensionGeneralEquipmentEnum()
   * @generated
   */
  int TEXTENSION_GENERAL_EQUIPMENT_ENUM = 216;

  /**
   * The meta object id for the '<em>TExtension LN Class Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTExtensionLNClassEnum()
   * @generated
   */
  int TEXTENSION_LN_CLASS_ENUM = 217;

  /**
   * The meta object id for the '<em>TExtension Phys Conn Type Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTExtensionPhysConnTypeEnum()
   * @generated
   */
  int TEXTENSION_PHYS_CONN_TYPE_ENUM = 218;

  /**
   * The meta object id for the '<em>TExtension PType Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTExtensionPTypeEnum()
   * @generated
   */
  int TEXTENSION_PTYPE_ENUM = 219;

  /**
   * The meta object id for the '<em>TFC Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TFCEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTFCEnumObject()
   * @generated
   */
  int TFC_ENUM_OBJECT = 220;

  /**
   * The meta object id for the '<em>TFull Attribute Name</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTFullAttributeName()
   * @generated
   */
  int TFULL_ATTRIBUTE_NAME = 221;

  /**
   * The meta object id for the '<em>TFull DO Name</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTFullDOName()
   * @generated
   */
  int TFULL_DO_NAME = 222;

  /**
   * The meta object id for the '<em>TGeneral Equipment Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Object
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTGeneralEquipmentEnum()
   * @generated
   */
  int TGENERAL_EQUIPMENT_ENUM = 223;

  /**
   * The meta object id for the '<em>TGSE Control Type Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TGSEControlTypeEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTGSEControlTypeEnumObject()
   * @generated
   */
  int TGSE_CONTROL_TYPE_ENUM_OBJECT = 224;

  /**
   * The meta object id for the '<em>TIED Name</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTIEDName()
   * @generated
   */
  int TIED_NAME = 225;

  /**
   * The meta object id for the '<em>TLD Inst</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLDInst()
   * @generated
   */
  int TLD_INST = 226;

  /**
   * The meta object id for the '<em>TLD Inst Or Empty</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLDInstOrEmpty()
   * @generated
   */
  int TLD_INST_OR_EMPTY = 227;

  /**
   * The meta object id for the '<em>TLD Name</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLDName()
   * @generated
   */
  int TLD_NAME = 228;

  /**
   * The meta object id for the '<em>TLLN0 Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TLLN0Enum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLLN0EnumObject()
   * @generated
   */
  int TLLN0_ENUM_OBJECT = 229;

  /**
   * The meta object id for the '<em>TLN Class Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Object
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLNClassEnum()
   * @generated
   */
  int TLN_CLASS_ENUM = 230;

  /**
   * The meta object id for the '<em>TLN Inst</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLNInst()
   * @generated
   */
  int TLN_INST = 231;

  /**
   * The meta object id for the '<em>TLN Inst Or Empty</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLNInstOrEmpty()
   * @generated
   */
  int TLN_INST_OR_EMPTY = 232;

  /**
   * The meta object id for the '<em>TLog Name</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLogName()
   * @generated
   */
  int TLOG_NAME = 233;

  /**
   * The meta object id for the '<em>TLPHD Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TLPHDEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTLPHDEnumObject()
   * @generated
   */
  int TLPHD_ENUM_OBJECT = 234;

  /**
   * The meta object id for the '<em>TName</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTName()
   * @generated
   */
  int TNAME = 235;

  /**
   * The meta object id for the '<em>TP Addr</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPAddr()
   * @generated
   */
  int TP_ADDR = 236;

  /**
   * The meta object id for the '<em>TPhase Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPhaseEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPhaseEnumObject()
   * @generated
   */
  int TPHASE_ENUM_OBJECT = 237;

  /**
   * The meta object id for the '<em>TPhys Conn Type Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Object
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPhysConnTypeEnum()
   * @generated
   */
  int TPHYS_CONN_TYPE_ENUM = 238;

  /**
   * The meta object id for the '<em>TPower Transformer Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPowerTransformerEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPowerTransformerEnumObject()
   * @generated
   */
  int TPOWER_TRANSFORMER_ENUM_OBJECT = 239;

  /**
   * The meta object id for the '<em>TPredefined Attribute Name Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedAttributeNameEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedAttributeNameEnumObject()
   * @generated
   */
  int TPREDEFINED_ATTRIBUTE_NAME_ENUM_OBJECT = 240;

  /**
   * The meta object id for the '<em>TPredefined Basic Type Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedBasicTypeEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedBasicTypeEnumObject()
   * @generated
   */
  int TPREDEFINED_BASIC_TYPE_ENUM_OBJECT = 241;

  /**
   * The meta object id for the '<em>TPredefined CDC Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedCDCEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedCDCEnumObject()
   * @generated
   */
  int TPREDEFINED_CDC_ENUM_OBJECT = 242;

  /**
   * The meta object id for the '<em>TPredefined Common Conducting Equipment Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedCommonConductingEquipmentEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedCommonConductingEquipmentEnumObject()
   * @generated
   */
  int TPREDEFINED_COMMON_CONDUCTING_EQUIPMENT_ENUM_OBJECT = 243;

  /**
   * The meta object id for the '<em>TPredefined General Equipment Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedGeneralEquipmentEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedGeneralEquipmentEnumObject()
   * @generated
   */
  int TPREDEFINED_GENERAL_EQUIPMENT_ENUM_OBJECT = 244;

  /**
   * The meta object id for the '<em>TPredefined LN Class Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.common.util.Enumerator
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedLNClassEnum()
   * @generated
   */
  int TPREDEFINED_LN_CLASS_ENUM = 245;

  /**
   * The meta object id for the '<em>TPredefined Phys Conn Type Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedPhysConnTypeEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedPhysConnTypeEnumObject()
   * @generated
   */
  int TPREDEFINED_PHYS_CONN_TYPE_ENUM_OBJECT = 246;

  /**
   * The meta object id for the '<em>TPredefined PType Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedPTypeEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedPTypeEnumObject()
   * @generated
   */
  int TPREDEFINED_PTYPE_ENUM_OBJECT = 247;

  /**
   * The meta object id for the '<em>TPredefined PType Phys Conn Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedPTypePhysConnEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPredefinedPTypePhysConnEnumObject()
   * @generated
   */
  int TPREDEFINED_PTYPE_PHYS_CONN_ENUM_OBJECT = 248;

  /**
   * The meta object id for the '<em>TPrefix</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPrefix()
   * @generated
   */
  int TPREFIX = 249;

  /**
   * The meta object id for the '<em>TP Type Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Object
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPTypeEnum()
   * @generated
   */
  int TP_TYPE_ENUM = 250;

  /**
   * The meta object id for the '<em>TP Type Phys Conn Enum</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Object
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTPTypePhysConnEnum()
   * @generated
   */
  int TP_TYPE_PHYS_CONN_ENUM = 251;

  /**
   * The meta object id for the '<em>TRef</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTRef()
   * @generated
   */
  int TREF = 252;

  /**
   * The meta object id for the '<em>TRestr Name1st L</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTRestrName1stL()
   * @generated
   */
  int TRESTR_NAME1ST_L = 253;

  /**
   * The meta object id for the '<em>TRestr Name1st U</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTRestrName1stU()
   * @generated
   */
  int TRESTR_NAME1ST_U = 254;

  /**
   * The meta object id for the '<em>TRight Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TRightEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTRightEnumObject()
   * @generated
   */
  int TRIGHT_ENUM_OBJECT = 255;

  /**
   * The meta object id for the '<em>TRpt ID</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTRptID()
   * @generated
   */
  int TRPT_ID = 256;

  /**
   * The meta object id for the '<em>TScl Revision</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSclRevision()
   * @generated
   */
  int TSCL_REVISION = 257;

  /**
   * The meta object id for the '<em>TScl Version</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSclVersion()
   * @generated
   */
  int TSCL_VERSION = 258;

  /**
   * The meta object id for the '<em>TSDO Count</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.Object
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSDOCount()
   * @generated
   */
  int TSDO_COUNT = 259;

  /**
   * The meta object id for the '<em>TService Settings Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServiceSettingsEnumObject()
   * @generated
   */
  int TSERVICE_SETTINGS_ENUM_OBJECT = 260;

  /**
   * The meta object id for the '<em>TService Type Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TServiceType
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTServiceTypeObject()
   * @generated
   */
  int TSERVICE_TYPE_OBJECT = 261;

  /**
   * The meta object id for the '<em>TSI Unit Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TSIUnitEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSIUnitEnumObject()
   * @generated
   */
  int TSI_UNIT_ENUM_OBJECT = 262;

  /**
   * The meta object id for the '<em>TSmp Mod Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TSmpMod
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTSmpModObject()
   * @generated
   */
  int TSMP_MOD_OBJECT = 263;

  /**
   * The meta object id for the '<em>TTransformer Winding Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TTransformerWindingEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTTransformerWindingEnumObject()
   * @generated
   */
  int TTRANSFORMER_WINDING_ENUM_OBJECT = 264;

  /**
   * The meta object id for the '<em>TUnit Multiplier Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TUnitMultiplierEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTUnitMultiplierEnumObject()
   * @generated
   */
  int TUNIT_MULTIPLIER_ENUM_OBJECT = 265;

  /**
   * The meta object id for the '<em>TVal Kind Enum Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.scl.TValKindEnum
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTValKindEnumObject()
   * @generated
   */
  int TVAL_KIND_ENUM_OBJECT = 266;

  /**
   * The meta object id for the '<em>Type Type</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.lang.String
   * @see com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl#getTypeType()
   * @generated
   */
  int TYPE_TYPE = 267;


  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.AuthenticationType <em>Authentication Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Authentication Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.AuthenticationType
   * @generated
   */
  EClass getAuthenticationType();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.AuthenticationType#isCertificate <em>Certificate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Certificate</em>'.
   * @see com.lucy.g3.iec61850.model.scl.AuthenticationType#isCertificate()
   * @see #getAuthenticationType()
   * @generated
   */
  EAttribute getAuthenticationType_Certificate();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.AuthenticationType#isNone <em>None</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>None</em>'.
   * @see com.lucy.g3.iec61850.model.scl.AuthenticationType#isNone()
   * @see #getAuthenticationType()
   * @generated
   */
  EAttribute getAuthenticationType_None();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.AuthenticationType#isPassword <em>Password</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Password</em>'.
   * @see com.lucy.g3.iec61850.model.scl.AuthenticationType#isPassword()
   * @see #getAuthenticationType()
   * @generated
   */
  EAttribute getAuthenticationType_Password();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.AuthenticationType#isStrong <em>Strong</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Strong</em>'.
   * @see com.lucy.g3.iec61850.model.scl.AuthenticationType#isStrong()
   * @see #getAuthenticationType()
   * @generated
   */
  EAttribute getAuthenticationType_Strong();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.AuthenticationType#isWeak <em>Weak</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Weak</em>'.
   * @see com.lucy.g3.iec61850.model.scl.AuthenticationType#isWeak()
   * @see #getAuthenticationType()
   * @generated
   */
  EAttribute getAuthenticationType_Weak();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.DocumentRoot <em>Document Root</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Document Root</em>'.
   * @see com.lucy.g3.iec61850.model.scl.DocumentRoot
   * @generated
   */
  EClass getDocumentRoot();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.DocumentRoot#getMixed <em>Mixed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Mixed</em>'.
   * @see com.lucy.g3.iec61850.model.scl.DocumentRoot#getMixed()
   * @see #getDocumentRoot()
   * @generated
   */
  EAttribute getDocumentRoot_Mixed();

  /**
   * Returns the meta object for the map '{@link com.lucy.g3.iec61850.model.scl.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
   * @see com.lucy.g3.iec61850.model.scl.DocumentRoot#getXMLNSPrefixMap()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XMLNSPrefixMap();

  /**
   * Returns the meta object for the map '{@link com.lucy.g3.iec61850.model.scl.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XSI Schema Location</em>'.
   * @see com.lucy.g3.iec61850.model.scl.DocumentRoot#getXSISchemaLocation()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XSISchemaLocation();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.DocumentRoot#getCommunication <em>Communication</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Communication</em>'.
   * @see com.lucy.g3.iec61850.model.scl.DocumentRoot#getCommunication()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_Communication();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.DocumentRoot#getDataTypeTemplates <em>Data Type Templates</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Data Type Templates</em>'.
   * @see com.lucy.g3.iec61850.model.scl.DocumentRoot#getDataTypeTemplates()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_DataTypeTemplates();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.DocumentRoot#getIED <em>IED</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>IED</em>'.
   * @see com.lucy.g3.iec61850.model.scl.DocumentRoot#getIED()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_IED();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.DocumentRoot#getLN <em>LN</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>LN</em>'.
   * @see com.lucy.g3.iec61850.model.scl.DocumentRoot#getLN()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_LN();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.DocumentRoot#getLN0 <em>LN0</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>LN0</em>'.
   * @see com.lucy.g3.iec61850.model.scl.DocumentRoot#getLN0()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_LN0();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.DocumentRoot#getSCL <em>SCL</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>SCL</em>'.
   * @see com.lucy.g3.iec61850.model.scl.DocumentRoot#getSCL()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_SCL();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.DocumentRoot#getSubstation <em>Substation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Substation</em>'.
   * @see com.lucy.g3.iec61850.model.scl.DocumentRoot#getSubstation()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_Substation();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.HistoryType <em>History Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>History Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.HistoryType
   * @generated
   */
  EClass getHistoryType();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.HistoryType#getHitem <em>Hitem</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Hitem</em>'.
   * @see com.lucy.g3.iec61850.model.scl.HistoryType#getHitem()
   * @see #getHistoryType()
   * @generated
   */
  EReference getHistoryType_Hitem();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.IEDNameType <em>IED Name Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>IED Name Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.IEDNameType
   * @generated
   */
  EClass getIEDNameType();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61850.model.scl.IEDNameType#getValue()
   * @see #getIEDNameType()
   * @generated
   */
  EAttribute getIEDNameType_Value();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getApRef <em>Ap Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ap Ref</em>'.
   * @see com.lucy.g3.iec61850.model.scl.IEDNameType#getApRef()
   * @see #getIEDNameType()
   * @generated
   */
  EAttribute getIEDNameType_ApRef();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getLdInst <em>Ld Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ld Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.IEDNameType#getLdInst()
   * @see #getIEDNameType()
   * @generated
   */
  EAttribute getIEDNameType_LdInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getLnClass <em>Ln Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Class</em>'.
   * @see com.lucy.g3.iec61850.model.scl.IEDNameType#getLnClass()
   * @see #getIEDNameType()
   * @generated
   */
  EAttribute getIEDNameType_LnClass();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getLnInst <em>Ln Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.IEDNameType#getLnInst()
   * @see #getIEDNameType()
   * @generated
   */
  EAttribute getIEDNameType_LnInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.IEDNameType#getPrefix <em>Prefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Prefix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.IEDNameType#getPrefix()
   * @see #getIEDNameType()
   * @generated
   */
  EAttribute getIEDNameType_Prefix();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.LN0Type <em>LN0 Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LN0 Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.LN0Type
   * @generated
   */
  EClass getLN0Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.OptFieldsType <em>Opt Fields Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Opt Fields Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.OptFieldsType
   * @generated
   */
  EClass getOptFieldsType();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.OptFieldsType#isBufOvfl <em>Buf Ovfl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Buf Ovfl</em>'.
   * @see com.lucy.g3.iec61850.model.scl.OptFieldsType#isBufOvfl()
   * @see #getOptFieldsType()
   * @generated
   */
  EAttribute getOptFieldsType_BufOvfl();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.OptFieldsType#isConfigRef <em>Config Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Config Ref</em>'.
   * @see com.lucy.g3.iec61850.model.scl.OptFieldsType#isConfigRef()
   * @see #getOptFieldsType()
   * @generated
   */
  EAttribute getOptFieldsType_ConfigRef();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.OptFieldsType#isDataRef <em>Data Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Data Ref</em>'.
   * @see com.lucy.g3.iec61850.model.scl.OptFieldsType#isDataRef()
   * @see #getOptFieldsType()
   * @generated
   */
  EAttribute getOptFieldsType_DataRef();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.OptFieldsType#isDataSet <em>Data Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Data Set</em>'.
   * @see com.lucy.g3.iec61850.model.scl.OptFieldsType#isDataSet()
   * @see #getOptFieldsType()
   * @generated
   */
  EAttribute getOptFieldsType_DataSet();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.OptFieldsType#isEntryID <em>Entry ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Entry ID</em>'.
   * @see com.lucy.g3.iec61850.model.scl.OptFieldsType#isEntryID()
   * @see #getOptFieldsType()
   * @generated
   */
  EAttribute getOptFieldsType_EntryID();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.OptFieldsType#isReasonCode <em>Reason Code</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Reason Code</em>'.
   * @see com.lucy.g3.iec61850.model.scl.OptFieldsType#isReasonCode()
   * @see #getOptFieldsType()
   * @generated
   */
  EAttribute getOptFieldsType_ReasonCode();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.OptFieldsType#isSeqNum <em>Seq Num</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Seq Num</em>'.
   * @see com.lucy.g3.iec61850.model.scl.OptFieldsType#isSeqNum()
   * @see #getOptFieldsType()
   * @generated
   */
  EAttribute getOptFieldsType_SeqNum();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.OptFieldsType#isTimeStamp <em>Time Stamp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Time Stamp</em>'.
   * @see com.lucy.g3.iec61850.model.scl.OptFieldsType#isTimeStamp()
   * @see #getOptFieldsType()
   * @generated
   */
  EAttribute getOptFieldsType_TimeStamp();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.ProtNsType <em>Prot Ns Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Prot Ns Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.ProtNsType
   * @generated
   */
  EClass getProtNsType();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.ProtNsType#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61850.model.scl.ProtNsType#getValue()
   * @see #getProtNsType()
   * @generated
   */
  EAttribute getProtNsType_Value();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.ProtNsType#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.ProtNsType#getType()
   * @see #getProtNsType()
   * @generated
   */
  EAttribute getProtNsType_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.SCLType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SCLType
   * @generated
   */
  EClass getSCLType();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.SCLType#getHeader <em>Header</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Header</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SCLType#getHeader()
   * @see #getSCLType()
   * @generated
   */
  EReference getSCLType_Header();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.SCLType#getSubstation <em>Substation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Substation</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SCLType#getSubstation()
   * @see #getSCLType()
   * @generated
   */
  EReference getSCLType_Substation();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.SCLType#getCommunication <em>Communication</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Communication</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SCLType#getCommunication()
   * @see #getSCLType()
   * @generated
   */
  EReference getSCLType_Communication();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.SCLType#getIED <em>IED</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>IED</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SCLType#getIED()
   * @see #getSCLType()
   * @generated
   */
  EReference getSCLType_IED();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.SCLType#getDataTypeTemplates <em>Data Type Templates</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Data Type Templates</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SCLType#getDataTypeTemplates()
   * @see #getSCLType()
   * @generated
   */
  EReference getSCLType_DataTypeTemplates();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.SCLType#getRevision <em>Revision</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Revision</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SCLType#getRevision()
   * @see #getSCLType()
   * @generated
   */
  EAttribute getSCLType_Revision();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.SCLType#getVersion <em>Version</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Version</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SCLType#getVersion()
   * @see #getSCLType()
   * @generated
   */
  EAttribute getSCLType_Version();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.SettingGroupsType <em>Setting Groups Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Setting Groups Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SettingGroupsType
   * @generated
   */
  EClass getSettingGroupsType();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.SettingGroupsType#getSGEdit <em>SG Edit</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>SG Edit</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SettingGroupsType#getSGEdit()
   * @see #getSettingGroupsType()
   * @generated
   */
  EReference getSettingGroupsType_SGEdit();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.SettingGroupsType#getConfSG <em>Conf SG</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Conf SG</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SettingGroupsType#getConfSG()
   * @see #getSettingGroupsType()
   * @generated
   */
  EReference getSettingGroupsType_ConfSG();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.SmvOptsType <em>Smv Opts Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Smv Opts Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SmvOptsType
   * @generated
   */
  EClass getSmvOptsType();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.SmvOptsType#isDataSet <em>Data Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Data Set</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SmvOptsType#isDataSet()
   * @see #getSmvOptsType()
   * @generated
   */
  EAttribute getSmvOptsType_DataSet();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.SmvOptsType#isRefreshTime <em>Refresh Time</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Refresh Time</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SmvOptsType#isRefreshTime()
   * @see #getSmvOptsType()
   * @generated
   */
  EAttribute getSmvOptsType_RefreshTime();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.SmvOptsType#isSampleRate <em>Sample Rate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Sample Rate</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SmvOptsType#isSampleRate()
   * @see #getSmvOptsType()
   * @generated
   */
  EAttribute getSmvOptsType_SampleRate();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.SmvOptsType#isSampleSynchronized <em>Sample Synchronized</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Sample Synchronized</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SmvOptsType#isSampleSynchronized()
   * @see #getSmvOptsType()
   * @generated
   */
  EAttribute getSmvOptsType_SampleSynchronized();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.SmvOptsType#isSecurity <em>Security</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Security</em>'.
   * @see com.lucy.g3.iec61850.model.scl.SmvOptsType#isSecurity()
   * @see #getSmvOptsType()
   * @generated
   */
  EAttribute getSmvOptsType_Security();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TAbstractConductingEquipment <em>TAbstract Conducting Equipment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TAbstract Conducting Equipment</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAbstractConductingEquipment
   * @generated
   */
  EClass getTAbstractConductingEquipment();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TAbstractConductingEquipment#getTerminal <em>Terminal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Terminal</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAbstractConductingEquipment#getTerminal()
   * @see #getTAbstractConductingEquipment()
   * @generated
   */
  EReference getTAbstractConductingEquipment_Terminal();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TAbstractConductingEquipment#getSubEquipment <em>Sub Equipment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sub Equipment</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAbstractConductingEquipment#getSubEquipment()
   * @see #getTAbstractConductingEquipment()
   * @generated
   */
  EReference getTAbstractConductingEquipment_SubEquipment();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute <em>TAbstract Data Attribute</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TAbstract Data Attribute</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute
   * @generated
   */
  EClass getTAbstractDataAttribute();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getVal <em>Val</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Val</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getVal()
   * @see #getTAbstractDataAttribute()
   * @generated
   */
  EReference getTAbstractDataAttribute_Val();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getBType <em>BType</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>BType</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getBType()
   * @see #getTAbstractDataAttribute()
   * @generated
   */
  EAttribute getTAbstractDataAttribute_BType();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getCount <em>Count</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Count</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getCount()
   * @see #getTAbstractDataAttribute()
   * @generated
   */
  EAttribute getTAbstractDataAttribute_Count();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getName()
   * @see #getTAbstractDataAttribute()
   * @generated
   */
  EAttribute getTAbstractDataAttribute_Name();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getSAddr <em>SAddr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>SAddr</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getSAddr()
   * @see #getTAbstractDataAttribute()
   * @generated
   */
  EAttribute getTAbstractDataAttribute_SAddr();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getType()
   * @see #getTAbstractDataAttribute()
   * @generated
   */
  EAttribute getTAbstractDataAttribute_Type();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getValKind <em>Val Kind</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Val Kind</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute#getValKind()
   * @see #getTAbstractDataAttribute()
   * @generated
   */
  EAttribute getTAbstractDataAttribute_ValKind();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TAccessControl <em>TAccess Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TAccess Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAccessControl
   * @generated
   */
  EClass getTAccessControl();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint <em>TAccess Point</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TAccess Point</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAccessPoint
   * @generated
   */
  EClass getTAccessPoint();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getServer <em>Server</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Server</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAccessPoint#getServer()
   * @see #getTAccessPoint()
   * @generated
   */
  EReference getTAccessPoint_Server();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getLN <em>LN</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>LN</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAccessPoint#getLN()
   * @see #getTAccessPoint()
   * @generated
   */
  EReference getTAccessPoint_LN();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getServerAt <em>Server At</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Server At</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAccessPoint#getServerAt()
   * @see #getTAccessPoint()
   * @generated
   */
  EReference getTAccessPoint_ServerAt();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getServices <em>Services</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Services</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAccessPoint#getServices()
   * @see #getTAccessPoint()
   * @generated
   */
  EReference getTAccessPoint_Services();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getGOOSESecurity <em>GOOSE Security</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>GOOSE Security</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAccessPoint#getGOOSESecurity()
   * @see #getTAccessPoint()
   * @generated
   */
  EReference getTAccessPoint_GOOSESecurity();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getSMVSecurity <em>SMV Security</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>SMV Security</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAccessPoint#getSMVSecurity()
   * @see #getTAccessPoint()
   * @generated
   */
  EReference getTAccessPoint_SMVSecurity();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#isClock <em>Clock</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Clock</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAccessPoint#isClock()
   * @see #getTAccessPoint()
   * @generated
   */
  EAttribute getTAccessPoint_Clock();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAccessPoint#getName()
   * @see #getTAccessPoint()
   * @generated
   */
  EAttribute getTAccessPoint_Name();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#isRouter <em>Router</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Router</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAccessPoint#isRouter()
   * @see #getTAccessPoint()
   * @generated
   */
  EAttribute getTAccessPoint_Router();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TAddress <em>TAddress</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TAddress</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAddress
   * @generated
   */
  EClass getTAddress();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TAddress#getP <em>P</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>P</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAddress#getP()
   * @see #getTAddress()
   * @generated
   */
  EReference getTAddress_P();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace <em>TAny Content From Other Namespace</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TAny Content From Other Namespace</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace
   * @generated
   */
  EClass getTAnyContentFromOtherNamespace();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace#getMixed <em>Mixed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Mixed</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace#getMixed()
   * @see #getTAnyContentFromOtherNamespace()
   * @generated
   */
  EAttribute getTAnyContentFromOtherNamespace_Mixed();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Group</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace#getGroup()
   * @see #getTAnyContentFromOtherNamespace()
   * @generated
   */
  EAttribute getTAnyContentFromOtherNamespace_Group();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace#getAny <em>Any</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Any</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace#getAny()
   * @see #getTAnyContentFromOtherNamespace()
   * @generated
   */
  EAttribute getTAnyContentFromOtherNamespace_Any();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace#getAnyAttribute <em>Any Attribute</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Any Attribute</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace#getAnyAttribute()
   * @see #getTAnyContentFromOtherNamespace()
   * @generated
   */
  EAttribute getTAnyContentFromOtherNamespace_AnyAttribute();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TAnyLN <em>TAny LN</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TAny LN</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAnyLN
   * @generated
   */
  EClass getTAnyLN();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TAnyLN#getDataSet <em>Data Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Data Set</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAnyLN#getDataSet()
   * @see #getTAnyLN()
   * @generated
   */
  EReference getTAnyLN_DataSet();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TAnyLN#getReportControl <em>Report Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Report Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAnyLN#getReportControl()
   * @see #getTAnyLN()
   * @generated
   */
  EReference getTAnyLN_ReportControl();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TAnyLN#getLogControl <em>Log Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Log Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAnyLN#getLogControl()
   * @see #getTAnyLN()
   * @generated
   */
  EReference getTAnyLN_LogControl();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TAnyLN#getDOI <em>DOI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>DOI</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAnyLN#getDOI()
   * @see #getTAnyLN()
   * @generated
   */
  EReference getTAnyLN_DOI();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TAnyLN#getInputs <em>Inputs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Inputs</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAnyLN#getInputs()
   * @see #getTAnyLN()
   * @generated
   */
  EReference getTAnyLN_Inputs();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TAnyLN#getLog <em>Log</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Log</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAnyLN#getLog()
   * @see #getTAnyLN()
   * @generated
   */
  EReference getTAnyLN_Log();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAnyLN#getLnType <em>Ln Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAnyLN#getLnType()
   * @see #getTAnyLN()
   * @generated
   */
  EAttribute getTAnyLN_LnType();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TAssociation <em>TAssociation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TAssociation</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAssociation
   * @generated
   */
  EClass getTAssociation();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getAssociationID <em>Association ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Association ID</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAssociation#getAssociationID()
   * @see #getTAssociation()
   * @generated
   */
  EAttribute getTAssociation_AssociationID();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getDesc <em>Desc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Desc</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAssociation#getDesc()
   * @see #getTAssociation()
   * @generated
   */
  EAttribute getTAssociation_Desc();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getIedName <em>Ied Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ied Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAssociation#getIedName()
   * @see #getTAssociation()
   * @generated
   */
  EAttribute getTAssociation_IedName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getKind <em>Kind</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Kind</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAssociation#getKind()
   * @see #getTAssociation()
   * @generated
   */
  EAttribute getTAssociation_Kind();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getLdInst <em>Ld Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ld Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAssociation#getLdInst()
   * @see #getTAssociation()
   * @generated
   */
  EAttribute getTAssociation_LdInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getLnClass <em>Ln Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Class</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAssociation#getLnClass()
   * @see #getTAssociation()
   * @generated
   */
  EAttribute getTAssociation_LnClass();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getLnInst <em>Ln Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAssociation#getLnInst()
   * @see #getTAssociation()
   * @generated
   */
  EAttribute getTAssociation_LnInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TAssociation#getPrefix <em>Prefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Prefix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAssociation#getPrefix()
   * @see #getTAssociation()
   * @generated
   */
  EAttribute getTAssociation_Prefix();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TBaseElement <em>TBase Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TBase Element</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBaseElement
   * @generated
   */
  EClass getTBaseElement();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TBaseElement#getAny <em>Any</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Any</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBaseElement#getAny()
   * @see #getTBaseElement()
   * @generated
   */
  EAttribute getTBaseElement_Any();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TBaseElement#getText <em>Text</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Text</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBaseElement#getText()
   * @see #getTBaseElement()
   * @generated
   */
  EReference getTBaseElement_Text();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TBaseElement#getPrivate <em>Private</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Private</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBaseElement#getPrivate()
   * @see #getTBaseElement()
   * @generated
   */
  EReference getTBaseElement_Private();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TBaseElement#getAnyAttribute <em>Any Attribute</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Any Attribute</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBaseElement#getAnyAttribute()
   * @see #getTBaseElement()
   * @generated
   */
  EAttribute getTBaseElement_AnyAttribute();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TBay <em>TBay</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TBay</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBay
   * @generated
   */
  EClass getTBay();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TBay#getConductingEquipment <em>Conducting Equipment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Conducting Equipment</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBay#getConductingEquipment()
   * @see #getTBay()
   * @generated
   */
  EReference getTBay_ConductingEquipment();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TBay#getConnectivityNode <em>Connectivity Node</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Connectivity Node</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBay#getConnectivityNode()
   * @see #getTBay()
   * @generated
   */
  EReference getTBay_ConnectivityNode();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TBay#getFunction <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Function</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBay#getFunction()
   * @see #getTBay()
   * @generated
   */
  EReference getTBay_Function();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TBDA <em>TBDA</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TBDA</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBDA
   * @generated
   */
  EClass getTBDA();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec <em>TBit Rate In Mb Per Sec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TBit Rate In Mb Per Sec</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec
   * @generated
   */
  EClass getTBitRateInMbPerSec();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getValue()
   * @see #getTBitRateInMbPerSec()
   * @generated
   */
  EAttribute getTBitRateInMbPerSec_Value();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getMultiplier <em>Multiplier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Multiplier</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getMultiplier()
   * @see #getTBitRateInMbPerSec()
   * @generated
   */
  EAttribute getTBitRateInMbPerSec_Multiplier();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getUnit <em>Unit</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Unit</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getUnit()
   * @see #getTBitRateInMbPerSec()
   * @generated
   */
  EAttribute getTBitRateInMbPerSec_Unit();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TCert <em>TCert</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TCert</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TCert
   * @generated
   */
  EClass getTCert();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TCert#getCommonName <em>Common Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Common Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TCert#getCommonName()
   * @see #getTCert()
   * @generated
   */
  EAttribute getTCert_CommonName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TCert#getIdHierarchy <em>Id Hierarchy</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id Hierarchy</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TCert#getIdHierarchy()
   * @see #getTCert()
   * @generated
   */
  EAttribute getTCert_IdHierarchy();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TCertificate <em>TCertificate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TCertificate</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TCertificate
   * @generated
   */
  EClass getTCertificate();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TCertificate#getSubject <em>Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Subject</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TCertificate#getSubject()
   * @see #getTCertificate()
   * @generated
   */
  EReference getTCertificate_Subject();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TCertificate#getIssuerName <em>Issuer Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Issuer Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TCertificate#getIssuerName()
   * @see #getTCertificate()
   * @generated
   */
  EReference getTCertificate_IssuerName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TCertificate#getSerialNumber <em>Serial Number</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Serial Number</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TCertificate#getSerialNumber()
   * @see #getTCertificate()
   * @generated
   */
  EAttribute getTCertificate_SerialNumber();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TCertificate#getXferNumber <em>Xfer Number</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Xfer Number</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TCertificate#getXferNumber()
   * @see #getTCertificate()
   * @generated
   */
  EAttribute getTCertificate_XferNumber();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TClientLN <em>TClient LN</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TClient LN</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientLN
   * @generated
   */
  EClass getTClientLN();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TClientLN#getApRef <em>Ap Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ap Ref</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientLN#getApRef()
   * @see #getTClientLN()
   * @generated
   */
  EAttribute getTClientLN_ApRef();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TClientLN#getDesc <em>Desc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Desc</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientLN#getDesc()
   * @see #getTClientLN()
   * @generated
   */
  EAttribute getTClientLN_Desc();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TClientLN#getIedName <em>Ied Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ied Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientLN#getIedName()
   * @see #getTClientLN()
   * @generated
   */
  EAttribute getTClientLN_IedName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TClientLN#getLdInst <em>Ld Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ld Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientLN#getLdInst()
   * @see #getTClientLN()
   * @generated
   */
  EAttribute getTClientLN_LdInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TClientLN#getLnClass <em>Ln Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Class</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientLN#getLnClass()
   * @see #getTClientLN()
   * @generated
   */
  EAttribute getTClientLN_LnClass();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TClientLN#getLnInst <em>Ln Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientLN#getLnInst()
   * @see #getTClientLN()
   * @generated
   */
  EAttribute getTClientLN_LnInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TClientLN#getPrefix <em>Prefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Prefix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientLN#getPrefix()
   * @see #getTClientLN()
   * @generated
   */
  EAttribute getTClientLN_Prefix();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TClientServices <em>TClient Services</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TClient Services</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientServices
   * @generated
   */
  EClass getTClientServices();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TClientServices#isBufReport <em>Buf Report</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Buf Report</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientServices#isBufReport()
   * @see #getTClientServices()
   * @generated
   */
  EAttribute getTClientServices_BufReport();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TClientServices#isGoose <em>Goose</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Goose</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientServices#isGoose()
   * @see #getTClientServices()
   * @generated
   */
  EAttribute getTClientServices_Goose();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TClientServices#isGsse <em>Gsse</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Gsse</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientServices#isGsse()
   * @see #getTClientServices()
   * @generated
   */
  EAttribute getTClientServices_Gsse();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TClientServices#isReadLog <em>Read Log</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Read Log</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientServices#isReadLog()
   * @see #getTClientServices()
   * @generated
   */
  EAttribute getTClientServices_ReadLog();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TClientServices#isSv <em>Sv</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Sv</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientServices#isSv()
   * @see #getTClientServices()
   * @generated
   */
  EAttribute getTClientServices_Sv();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TClientServices#isUnbufReport <em>Unbuf Report</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Unbuf Report</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TClientServices#isUnbufReport()
   * @see #getTClientServices()
   * @generated
   */
  EAttribute getTClientServices_UnbufReport();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TCommunication <em>TCommunication</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TCommunication</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TCommunication
   * @generated
   */
  EClass getTCommunication();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TCommunication#getSubNetwork <em>Sub Network</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sub Network</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TCommunication#getSubNetwork()
   * @see #getTCommunication()
   * @generated
   */
  EReference getTCommunication_SubNetwork();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TConductingEquipment <em>TConducting Equipment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TConducting Equipment</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConductingEquipment
   * @generated
   */
  EClass getTConductingEquipment();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TConductingEquipment#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConductingEquipment#getType()
   * @see #getTConductingEquipment()
   * @generated
   */
  EAttribute getTConductingEquipment_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TConfLNs <em>TConf LNs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TConf LNs</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConfLNs
   * @generated
   */
  EClass getTConfLNs();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TConfLNs#isFixLnInst <em>Fix Ln Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Fix Ln Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConfLNs#isFixLnInst()
   * @see #getTConfLNs()
   * @generated
   */
  EAttribute getTConfLNs_FixLnInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TConfLNs#isFixPrefix <em>Fix Prefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Fix Prefix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConfLNs#isFixPrefix()
   * @see #getTConfLNs()
   * @generated
   */
  EAttribute getTConfLNs_FixPrefix();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TConnectedAP <em>TConnected AP</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TConnected AP</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConnectedAP
   * @generated
   */
  EClass getTConnectedAP();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TConnectedAP#getAddress <em>Address</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Address</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConnectedAP#getAddress()
   * @see #getTConnectedAP()
   * @generated
   */
  EReference getTConnectedAP_Address();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TConnectedAP#getGSE <em>GSE</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>GSE</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConnectedAP#getGSE()
   * @see #getTConnectedAP()
   * @generated
   */
  EReference getTConnectedAP_GSE();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TConnectedAP#getSMV <em>SMV</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>SMV</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConnectedAP#getSMV()
   * @see #getTConnectedAP()
   * @generated
   */
  EReference getTConnectedAP_SMV();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TConnectedAP#getPhysConn <em>Phys Conn</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Phys Conn</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConnectedAP#getPhysConn()
   * @see #getTConnectedAP()
   * @generated
   */
  EReference getTConnectedAP_PhysConn();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TConnectedAP#getApName <em>Ap Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ap Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConnectedAP#getApName()
   * @see #getTConnectedAP()
   * @generated
   */
  EAttribute getTConnectedAP_ApName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TConnectedAP#getIedName <em>Ied Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ied Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConnectedAP#getIedName()
   * @see #getTConnectedAP()
   * @generated
   */
  EAttribute getTConnectedAP_IedName();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TConnectivityNode <em>TConnectivity Node</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TConnectivity Node</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConnectivityNode
   * @generated
   */
  EClass getTConnectivityNode();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TConnectivityNode#getPathName <em>Path Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Path Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TConnectivityNode#getPathName()
   * @see #getTConnectivityNode()
   * @generated
   */
  EAttribute getTConnectivityNode_PathName();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TControl <em>TControl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TControl</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TControl
   * @generated
   */
  EClass getTControl();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TControl#getDatSet <em>Dat Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dat Set</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TControl#getDatSet()
   * @see #getTControl()
   * @generated
   */
  EAttribute getTControl_DatSet();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TControl#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TControl#getName()
   * @see #getTControl()
   * @generated
   */
  EAttribute getTControl_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TControlBlock <em>TControl Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TControl Block</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TControlBlock
   * @generated
   */
  EClass getTControlBlock();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TControlBlock#getAddress <em>Address</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Address</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TControlBlock#getAddress()
   * @see #getTControlBlock()
   * @generated
   */
  EReference getTControlBlock_Address();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TControlBlock#getCbName <em>Cb Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Cb Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TControlBlock#getCbName()
   * @see #getTControlBlock()
   * @generated
   */
  EAttribute getTControlBlock_CbName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TControlBlock#getLdInst <em>Ld Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ld Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TControlBlock#getLdInst()
   * @see #getTControlBlock()
   * @generated
   */
  EAttribute getTControlBlock_LdInst();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TControlWithIEDName <em>TControl With IED Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TControl With IED Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TControlWithIEDName
   * @generated
   */
  EClass getTControlWithIEDName();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TControlWithIEDName#getIEDName <em>IED Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>IED Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TControlWithIEDName#getIEDName()
   * @see #getTControlWithIEDName()
   * @generated
   */
  EReference getTControlWithIEDName_IEDName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TControlWithIEDName#getConfRev <em>Conf Rev</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Conf Rev</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TControlWithIEDName#getConfRev()
   * @see #getTControlWithIEDName()
   * @generated
   */
  EAttribute getTControlWithIEDName_ConfRev();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TControlWithTriggerOpt <em>TControl With Trigger Opt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TControl With Trigger Opt</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TControlWithTriggerOpt
   * @generated
   */
  EClass getTControlWithTriggerOpt();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TControlWithTriggerOpt#getTrgOps <em>Trg Ops</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Trg Ops</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TControlWithTriggerOpt#getTrgOps()
   * @see #getTControlWithTriggerOpt()
   * @generated
   */
  EReference getTControlWithTriggerOpt_TrgOps();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TControlWithTriggerOpt#getIntgPd <em>Intg Pd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Intg Pd</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TControlWithTriggerOpt#getIntgPd()
   * @see #getTControlWithTriggerOpt()
   * @generated
   */
  EAttribute getTControlWithTriggerOpt_IntgPd();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TDA <em>TDA</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TDA</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDA
   * @generated
   */
  EClass getTDA();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDA#isDchg <em>Dchg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dchg</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDA#isDchg()
   * @see #getTDA()
   * @generated
   */
  EAttribute getTDA_Dchg();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDA#isDupd <em>Dupd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dupd</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDA#isDupd()
   * @see #getTDA()
   * @generated
   */
  EAttribute getTDA_Dupd();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDA#getFc <em>Fc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Fc</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDA#getFc()
   * @see #getTDA()
   * @generated
   */
  EAttribute getTDA_Fc();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDA#isQchg <em>Qchg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Qchg</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDA#isQchg()
   * @see #getTDA()
   * @generated
   */
  EAttribute getTDA_Qchg();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TDAI <em>TDAI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TDAI</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDAI
   * @generated
   */
  EClass getTDAI();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TDAI#getVal <em>Val</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Val</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDAI#getVal()
   * @see #getTDAI()
   * @generated
   */
  EReference getTDAI_Val();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDAI#getIx <em>Ix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDAI#getIx()
   * @see #getTDAI()
   * @generated
   */
  EAttribute getTDAI_Ix();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDAI#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDAI#getName()
   * @see #getTDAI()
   * @generated
   */
  EAttribute getTDAI_Name();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDAI#getSAddr <em>SAddr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>SAddr</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDAI#getSAddr()
   * @see #getTDAI()
   * @generated
   */
  EAttribute getTDAI_SAddr();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDAI#getValKind <em>Val Kind</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Val Kind</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDAI#getValKind()
   * @see #getTDAI()
   * @generated
   */
  EAttribute getTDAI_ValKind();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TDataSet <em>TData Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TData Set</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDataSet
   * @generated
   */
  EClass getTDataSet();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TDataSet#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Group</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDataSet#getGroup()
   * @see #getTDataSet()
   * @generated
   */
  EAttribute getTDataSet_Group();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TDataSet#getFCDA <em>FCDA</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>FCDA</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDataSet#getFCDA()
   * @see #getTDataSet()
   * @generated
   */
  EReference getTDataSet_FCDA();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDataSet#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDataSet#getName()
   * @see #getTDataSet()
   * @generated
   */
  EAttribute getTDataSet_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TDataTypeTemplates <em>TData Type Templates</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TData Type Templates</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDataTypeTemplates
   * @generated
   */
  EClass getTDataTypeTemplates();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TDataTypeTemplates#getLNodeType <em>LNode Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>LNode Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDataTypeTemplates#getLNodeType()
   * @see #getTDataTypeTemplates()
   * @generated
   */
  EReference getTDataTypeTemplates_LNodeType();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TDataTypeTemplates#getDOType <em>DO Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>DO Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDataTypeTemplates#getDOType()
   * @see #getTDataTypeTemplates()
   * @generated
   */
  EReference getTDataTypeTemplates_DOType();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TDataTypeTemplates#getDAType <em>DA Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>DA Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDataTypeTemplates#getDAType()
   * @see #getTDataTypeTemplates()
   * @generated
   */
  EReference getTDataTypeTemplates_DAType();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TDataTypeTemplates#getEnumType <em>Enum Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Enum Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDataTypeTemplates#getEnumType()
   * @see #getTDataTypeTemplates()
   * @generated
   */
  EReference getTDataTypeTemplates_EnumType();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TDAType <em>TDA Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TDA Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDAType
   * @generated
   */
  EClass getTDAType();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TDAType#getBDA <em>BDA</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>BDA</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDAType#getBDA()
   * @see #getTDAType()
   * @generated
   */
  EReference getTDAType_BDA();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TDAType#getProtNs <em>Prot Ns</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Prot Ns</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDAType#getProtNs()
   * @see #getTDAType()
   * @generated
   */
  EReference getTDAType_ProtNs();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDAType#getIedType <em>Ied Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ied Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDAType#getIedType()
   * @see #getTDAType()
   * @generated
   */
  EAttribute getTDAType_IedType();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TDO <em>TDO</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TDO</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDO
   * @generated
   */
  EClass getTDO();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDO#getAccessControl <em>Access Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Access Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDO#getAccessControl()
   * @see #getTDO()
   * @generated
   */
  EAttribute getTDO_AccessControl();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDO#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDO#getName()
   * @see #getTDO()
   * @generated
   */
  EAttribute getTDO_Name();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDO#isTransient <em>Transient</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Transient</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDO#isTransient()
   * @see #getTDO()
   * @generated
   */
  EAttribute getTDO_Transient();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDO#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDO#getType()
   * @see #getTDO()
   * @generated
   */
  EAttribute getTDO_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TDOI <em>TDOI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TDOI</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDOI
   * @generated
   */
  EClass getTDOI();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TDOI#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Group</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDOI#getGroup()
   * @see #getTDOI()
   * @generated
   */
  EAttribute getTDOI_Group();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TDOI#getSDI <em>SDI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>SDI</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDOI#getSDI()
   * @see #getTDOI()
   * @generated
   */
  EReference getTDOI_SDI();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TDOI#getDAI <em>DAI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>DAI</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDOI#getDAI()
   * @see #getTDOI()
   * @generated
   */
  EReference getTDOI_DAI();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDOI#getAccessControl <em>Access Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Access Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDOI#getAccessControl()
   * @see #getTDOI()
   * @generated
   */
  EAttribute getTDOI_AccessControl();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDOI#getIx <em>Ix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDOI#getIx()
   * @see #getTDOI()
   * @generated
   */
  EAttribute getTDOI_Ix();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDOI#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDOI#getName()
   * @see #getTDOI()
   * @generated
   */
  EAttribute getTDOI_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TDOType <em>TDO Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TDO Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDOType
   * @generated
   */
  EClass getTDOType();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TDOType#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Group</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDOType#getGroup()
   * @see #getTDOType()
   * @generated
   */
  EAttribute getTDOType_Group();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TDOType#getSDO <em>SDO</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>SDO</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDOType#getSDO()
   * @see #getTDOType()
   * @generated
   */
  EReference getTDOType_SDO();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TDOType#getDA <em>DA</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>DA</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDOType#getDA()
   * @see #getTDOType()
   * @generated
   */
  EReference getTDOType_DA();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDOType#getCdc <em>Cdc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Cdc</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDOType#getCdc()
   * @see #getTDOType()
   * @generated
   */
  EAttribute getTDOType_Cdc();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDOType#getIedType <em>Ied Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ied Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDOType#getIedType()
   * @see #getTDOType()
   * @generated
   */
  EAttribute getTDOType_IedType();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TDurationInMilliSec <em>TDuration In Milli Sec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TDuration In Milli Sec</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDurationInMilliSec
   * @generated
   */
  EClass getTDurationInMilliSec();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDurationInMilliSec#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDurationInMilliSec#getValue()
   * @see #getTDurationInMilliSec()
   * @generated
   */
  EAttribute getTDurationInMilliSec_Value();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDurationInMilliSec#getMultiplier <em>Multiplier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Multiplier</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDurationInMilliSec#getMultiplier()
   * @see #getTDurationInMilliSec()
   * @generated
   */
  EAttribute getTDurationInMilliSec_Multiplier();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TDurationInMilliSec#getUnit <em>Unit</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Unit</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDurationInMilliSec#getUnit()
   * @see #getTDurationInMilliSec()
   * @generated
   */
  EAttribute getTDurationInMilliSec_Unit();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TDurationInSec <em>TDuration In Sec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TDuration In Sec</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDurationInSec
   * @generated
   */
  EClass getTDurationInSec();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TEnumType <em>TEnum Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TEnum Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TEnumType
   * @generated
   */
  EClass getTEnumType();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TEnumType#getEnumVal <em>Enum Val</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Enum Val</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TEnumType#getEnumVal()
   * @see #getTEnumType()
   * @generated
   */
  EReference getTEnumType_EnumVal();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TEnumVal <em>TEnum Val</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TEnum Val</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TEnumVal
   * @generated
   */
  EClass getTEnumVal();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TEnumVal#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TEnumVal#getValue()
   * @see #getTEnumVal()
   * @generated
   */
  EAttribute getTEnumVal_Value();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TEnumVal#getDesc <em>Desc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Desc</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TEnumVal#getDesc()
   * @see #getTEnumVal()
   * @generated
   */
  EAttribute getTEnumVal_Desc();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TEnumVal#getOrd <em>Ord</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ord</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TEnumVal#getOrd()
   * @see #getTEnumVal()
   * @generated
   */
  EAttribute getTEnumVal_Ord();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TEquipment <em>TEquipment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TEquipment</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TEquipment
   * @generated
   */
  EClass getTEquipment();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TEquipment#isVirtual <em>Virtual</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Virtual</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TEquipment#isVirtual()
   * @see #getTEquipment()
   * @generated
   */
  EAttribute getTEquipment_Virtual();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TEquipmentContainer <em>TEquipment Container</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TEquipment Container</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TEquipmentContainer
   * @generated
   */
  EClass getTEquipmentContainer();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TEquipmentContainer#getPowerTransformer <em>Power Transformer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Power Transformer</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TEquipmentContainer#getPowerTransformer()
   * @see #getTEquipmentContainer()
   * @generated
   */
  EReference getTEquipmentContainer_PowerTransformer();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TEquipmentContainer#getGeneralEquipment <em>General Equipment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>General Equipment</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TEquipmentContainer#getGeneralEquipment()
   * @see #getTEquipmentContainer()
   * @generated
   */
  EReference getTEquipmentContainer_GeneralEquipment();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TExtRef <em>TExt Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TExt Ref</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef
   * @generated
   */
  EClass getTExtRef();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getDaName <em>Da Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Da Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getDaName()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_DaName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getDesc <em>Desc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Desc</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getDesc()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_Desc();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getDoName <em>Do Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Do Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getDoName()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_DoName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getIedName <em>Ied Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ied Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getIedName()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_IedName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getIntAddr <em>Int Addr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Int Addr</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getIntAddr()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_IntAddr();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getLdInst <em>Ld Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ld Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getLdInst()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_LdInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getLnClass <em>Ln Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Class</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getLnClass()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_LnClass();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getLnInst <em>Ln Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getLnInst()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_LnInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getPrefix <em>Prefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Prefix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getPrefix()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_Prefix();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getServiceType <em>Service Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Service Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getServiceType()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_ServiceType();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcCBName <em>Src CB Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Src CB Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getSrcCBName()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_SrcCBName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcLDInst <em>Src LD Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Src LD Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getSrcLDInst()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_SrcLDInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcLNClass <em>Src LN Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Src LN Class</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getSrcLNClass()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_SrcLNClass();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcLNInst <em>Src LN Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Src LN Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getSrcLNInst()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_SrcLNInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TExtRef#getSrcPrefix <em>Src Prefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Src Prefix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef#getSrcPrefix()
   * @see #getTExtRef()
   * @generated
   */
  EAttribute getTExtRef_SrcPrefix();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TFCDA <em>TFCDA</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TFCDA</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFCDA
   * @generated
   */
  EClass getTFCDA();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getDaName <em>Da Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Da Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFCDA#getDaName()
   * @see #getTFCDA()
   * @generated
   */
  EAttribute getTFCDA_DaName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getDoName <em>Do Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Do Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFCDA#getDoName()
   * @see #getTFCDA()
   * @generated
   */
  EAttribute getTFCDA_DoName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getFc <em>Fc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Fc</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFCDA#getFc()
   * @see #getTFCDA()
   * @generated
   */
  EAttribute getTFCDA_Fc();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getIx <em>Ix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFCDA#getIx()
   * @see #getTFCDA()
   * @generated
   */
  EAttribute getTFCDA_Ix();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getLdInst <em>Ld Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ld Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFCDA#getLdInst()
   * @see #getTFCDA()
   * @generated
   */
  EAttribute getTFCDA_LdInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getLnClass <em>Ln Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Class</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFCDA#getLnClass()
   * @see #getTFCDA()
   * @generated
   */
  EAttribute getTFCDA_LnClass();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getLnInst <em>Ln Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFCDA#getLnInst()
   * @see #getTFCDA()
   * @generated
   */
  EAttribute getTFCDA_LnInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TFCDA#getPrefix <em>Prefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Prefix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFCDA#getPrefix()
   * @see #getTFCDA()
   * @generated
   */
  EAttribute getTFCDA_Prefix();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TFunction <em>TFunction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TFunction</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFunction
   * @generated
   */
  EClass getTFunction();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TFunction#getSubFunction <em>Sub Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sub Function</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFunction#getSubFunction()
   * @see #getTFunction()
   * @generated
   */
  EReference getTFunction_SubFunction();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TFunction#getGeneralEquipment <em>General Equipment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>General Equipment</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFunction#getGeneralEquipment()
   * @see #getTFunction()
   * @generated
   */
  EReference getTFunction_GeneralEquipment();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TFunction#getConductingEquipment <em>Conducting Equipment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Conducting Equipment</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFunction#getConductingEquipment()
   * @see #getTFunction()
   * @generated
   */
  EReference getTFunction_ConductingEquipment();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TFunction#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFunction#getType()
   * @see #getTFunction()
   * @generated
   */
  EAttribute getTFunction_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TGeneralEquipment <em>TGeneral Equipment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TGeneral Equipment</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGeneralEquipment
   * @generated
   */
  EClass getTGeneralEquipment();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TGeneralEquipment#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGeneralEquipment#getType()
   * @see #getTGeneralEquipment()
   * @generated
   */
  EAttribute getTGeneralEquipment_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TGSE <em>TGSE</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TGSE</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGSE
   * @generated
   */
  EClass getTGSE();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TGSE#getMinTime <em>Min Time</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Min Time</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGSE#getMinTime()
   * @see #getTGSE()
   * @generated
   */
  EReference getTGSE_MinTime();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TGSE#getMaxTime <em>Max Time</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Max Time</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGSE#getMaxTime()
   * @see #getTGSE()
   * @generated
   */
  EReference getTGSE_MaxTime();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TGSEControl <em>TGSE Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TGSE Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGSEControl
   * @generated
   */
  EClass getTGSEControl();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TGSEControl#getAppID <em>App ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>App ID</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGSEControl#getAppID()
   * @see #getTGSEControl()
   * @generated
   */
  EAttribute getTGSEControl_AppID();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TGSEControl#isFixedOffs <em>Fixed Offs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Fixed Offs</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGSEControl#isFixedOffs()
   * @see #getTGSEControl()
   * @generated
   */
  EAttribute getTGSEControl_FixedOffs();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TGSEControl#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGSEControl#getType()
   * @see #getTGSEControl()
   * @generated
   */
  EAttribute getTGSEControl_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TGSESettings <em>TGSE Settings</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TGSE Settings</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGSESettings
   * @generated
   */
  EClass getTGSESettings();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TGSESettings#getAppID <em>App ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>App ID</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGSESettings#getAppID()
   * @see #getTGSESettings()
   * @generated
   */
  EAttribute getTGSESettings_AppID();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TGSESettings#getDataLabel <em>Data Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Data Label</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGSESettings#getDataLabel()
   * @see #getTGSESettings()
   * @generated
   */
  EAttribute getTGSESettings_DataLabel();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.THeader <em>THeader</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>THeader</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THeader
   * @generated
   */
  EClass getTHeader();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.THeader#getText <em>Text</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Text</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THeader#getText()
   * @see #getTHeader()
   * @generated
   */
  EReference getTHeader_Text();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.THeader#getHistory <em>History</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>History</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THeader#getHistory()
   * @see #getTHeader()
   * @generated
   */
  EReference getTHeader_History();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.THeader#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THeader#getId()
   * @see #getTHeader()
   * @generated
   */
  EAttribute getTHeader_Id();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.THeader#getNameStructure <em>Name Structure</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name Structure</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THeader#getNameStructure()
   * @see #getTHeader()
   * @generated
   */
  EAttribute getTHeader_NameStructure();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.THeader#getRevision <em>Revision</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Revision</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THeader#getRevision()
   * @see #getTHeader()
   * @generated
   */
  EAttribute getTHeader_Revision();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.THeader#getToolID <em>Tool ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Tool ID</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THeader#getToolID()
   * @see #getTHeader()
   * @generated
   */
  EAttribute getTHeader_ToolID();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.THeader#getVersion <em>Version</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Version</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THeader#getVersion()
   * @see #getTHeader()
   * @generated
   */
  EAttribute getTHeader_Version();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.THitem <em>THitem</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>THitem</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THitem
   * @generated
   */
  EClass getTHitem();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.THitem#getRevision <em>Revision</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Revision</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THitem#getRevision()
   * @see #getTHitem()
   * @generated
   */
  EAttribute getTHitem_Revision();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.THitem#getVersion <em>Version</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Version</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THitem#getVersion()
   * @see #getTHitem()
   * @generated
   */
  EAttribute getTHitem_Version();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.THitem#getWhat <em>What</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>What</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THitem#getWhat()
   * @see #getTHitem()
   * @generated
   */
  EAttribute getTHitem_What();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.THitem#getWhen <em>When</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>When</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THitem#getWhen()
   * @see #getTHitem()
   * @generated
   */
  EAttribute getTHitem_When();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.THitem#getWho <em>Who</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Who</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THitem#getWho()
   * @see #getTHitem()
   * @generated
   */
  EAttribute getTHitem_Who();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.THitem#getWhy <em>Why</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Why</em>'.
   * @see com.lucy.g3.iec61850.model.scl.THitem#getWhy()
   * @see #getTHitem()
   * @generated
   */
  EAttribute getTHitem_Why();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TIDNaming <em>TID Naming</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TID Naming</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIDNaming
   * @generated
   */
  EClass getTIDNaming();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TIDNaming#getDesc <em>Desc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Desc</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIDNaming#getDesc()
   * @see #getTIDNaming()
   * @generated
   */
  EAttribute getTIDNaming_Desc();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TIDNaming#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIDNaming#getId()
   * @see #getTIDNaming()
   * @generated
   */
  EAttribute getTIDNaming_Id();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TIED <em>TIED</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TIED</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIED
   * @generated
   */
  EClass getTIED();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TIED#getServices <em>Services</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Services</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIED#getServices()
   * @see #getTIED()
   * @generated
   */
  EReference getTIED_Services();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TIED#getAccessPoint <em>Access Point</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Access Point</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIED#getAccessPoint()
   * @see #getTIED()
   * @generated
   */
  EReference getTIED_AccessPoint();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TIED#getConfigVersion <em>Config Version</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Config Version</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIED#getConfigVersion()
   * @see #getTIED()
   * @generated
   */
  EAttribute getTIED_ConfigVersion();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TIED#getEngRight <em>Eng Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Eng Right</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIED#getEngRight()
   * @see #getTIED()
   * @generated
   */
  EAttribute getTIED_EngRight();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TIED#getManufacturer <em>Manufacturer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Manufacturer</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIED#getManufacturer()
   * @see #getTIED()
   * @generated
   */
  EAttribute getTIED_Manufacturer();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TIED#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIED#getName()
   * @see #getTIED()
   * @generated
   */
  EAttribute getTIED_Name();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TIED#getOriginalSclRevision <em>Original Scl Revision</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Original Scl Revision</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIED#getOriginalSclRevision()
   * @see #getTIED()
   * @generated
   */
  EAttribute getTIED_OriginalSclRevision();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TIED#getOriginalSclVersion <em>Original Scl Version</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Original Scl Version</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIED#getOriginalSclVersion()
   * @see #getTIED()
   * @generated
   */
  EAttribute getTIED_OriginalSclVersion();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TIED#getOwner <em>Owner</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Owner</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIED#getOwner()
   * @see #getTIED()
   * @generated
   */
  EAttribute getTIED_Owner();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TIED#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TIED#getType()
   * @see #getTIED()
   * @generated
   */
  EAttribute getTIED_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TInputs <em>TInputs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TInputs</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TInputs
   * @generated
   */
  EClass getTInputs();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TInputs#getExtRef <em>Ext Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ext Ref</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TInputs#getExtRef()
   * @see #getTInputs()
   * @generated
   */
  EReference getTInputs_ExtRef();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TLDevice <em>TL Device</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TL Device</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLDevice
   * @generated
   */
  EClass getTLDevice();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TLDevice#getLN0 <em>LN0</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>LN0</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLDevice#getLN0()
   * @see #getTLDevice()
   * @generated
   */
  EReference getTLDevice_LN0();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TLDevice#getLN <em>LN</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>LN</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLDevice#getLN()
   * @see #getTLDevice()
   * @generated
   */
  EReference getTLDevice_LN();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TLDevice#getAccessControl <em>Access Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Access Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLDevice#getAccessControl()
   * @see #getTLDevice()
   * @generated
   */
  EReference getTLDevice_AccessControl();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLDevice#getInst <em>Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLDevice#getInst()
   * @see #getTLDevice()
   * @generated
   */
  EAttribute getTLDevice_Inst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLDevice#getLdName <em>Ld Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ld Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLDevice#getLdName()
   * @see #getTLDevice()
   * @generated
   */
  EAttribute getTLDevice_LdName();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TLN <em>TLN</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TLN</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLN
   * @generated
   */
  EClass getTLN();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLN#getInst <em>Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLN#getInst()
   * @see #getTLN()
   * @generated
   */
  EAttribute getTLN_Inst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLN#getLnClass <em>Ln Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Class</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLN#getLnClass()
   * @see #getTLN()
   * @generated
   */
  EAttribute getTLN_LnClass();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLN#getPrefix <em>Prefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Prefix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLN#getPrefix()
   * @see #getTLN()
   * @generated
   */
  EAttribute getTLN_Prefix();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TLN0 <em>TLN0</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TLN0</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLN0
   * @generated
   */
  EClass getTLN0();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TLN0#getGSEControl <em>GSE Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>GSE Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLN0#getGSEControl()
   * @see #getTLN0()
   * @generated
   */
  EReference getTLN0_GSEControl();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TLN0#getSampledValueControl <em>Sampled Value Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sampled Value Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLN0#getSampledValueControl()
   * @see #getTLN0()
   * @generated
   */
  EReference getTLN0_SampledValueControl();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TLN0#getSettingControl <em>Setting Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Setting Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLN0#getSettingControl()
   * @see #getTLN0()
   * @generated
   */
  EReference getTLN0_SettingControl();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLN0#getInst <em>Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLN0#getInst()
   * @see #getTLN0()
   * @generated
   */
  EAttribute getTLN0_Inst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLN0#getLnClass <em>Ln Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Class</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLN0#getLnClass()
   * @see #getTLN0()
   * @generated
   */
  EAttribute getTLN0_LnClass();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TLNode <em>TL Node</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TL Node</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLNode
   * @generated
   */
  EClass getTLNode();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLNode#getIedName <em>Ied Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ied Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLNode#getIedName()
   * @see #getTLNode()
   * @generated
   */
  EAttribute getTLNode_IedName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLNode#getLdInst <em>Ld Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ld Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLNode#getLdInst()
   * @see #getTLNode()
   * @generated
   */
  EAttribute getTLNode_LdInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLNode#getLnClass <em>Ln Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Class</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLNode#getLnClass()
   * @see #getTLNode()
   * @generated
   */
  EAttribute getTLNode_LnClass();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLNode#getLnInst <em>Ln Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLNode#getLnInst()
   * @see #getTLNode()
   * @generated
   */
  EAttribute getTLNode_LnInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLNode#getLnType <em>Ln Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLNode#getLnType()
   * @see #getTLNode()
   * @generated
   */
  EAttribute getTLNode_LnType();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLNode#getPrefix <em>Prefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Prefix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLNode#getPrefix()
   * @see #getTLNode()
   * @generated
   */
  EAttribute getTLNode_Prefix();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TLNodeContainer <em>TL Node Container</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TL Node Container</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLNodeContainer
   * @generated
   */
  EClass getTLNodeContainer();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TLNodeContainer#getLNode <em>LNode</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>LNode</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLNodeContainer#getLNode()
   * @see #getTLNodeContainer()
   * @generated
   */
  EReference getTLNodeContainer_LNode();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TLNodeType <em>TL Node Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TL Node Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLNodeType
   * @generated
   */
  EClass getTLNodeType();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TLNodeType#getDO <em>DO</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>DO</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLNodeType#getDO()
   * @see #getTLNodeType()
   * @generated
   */
  EReference getTLNodeType_DO();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLNodeType#getIedType <em>Ied Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ied Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLNodeType#getIedType()
   * @see #getTLNodeType()
   * @generated
   */
  EAttribute getTLNodeType_IedType();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLNodeType#getLnClass <em>Ln Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Class</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLNodeType#getLnClass()
   * @see #getTLNodeType()
   * @generated
   */
  EAttribute getTLNodeType_LnClass();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TLog <em>TLog</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TLog</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLog
   * @generated
   */
  EClass getTLog();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLog#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLog#getName()
   * @see #getTLog()
   * @generated
   */
  EAttribute getTLog_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TLogControl <em>TLog Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TLog Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLogControl
   * @generated
   */
  EClass getTLogControl();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getBufTime <em>Buf Time</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Buf Time</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLogControl#getBufTime()
   * @see #getTLogControl()
   * @generated
   */
  EAttribute getTLogControl_BufTime();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLdInst <em>Ld Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ld Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLogControl#getLdInst()
   * @see #getTLogControl()
   * @generated
   */
  EAttribute getTLogControl_LdInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLnClass <em>Ln Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Class</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLogControl#getLnClass()
   * @see #getTLogControl()
   * @generated
   */
  EAttribute getTLogControl_LnClass();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLnInst <em>Ln Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ln Inst</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLogControl#getLnInst()
   * @see #getTLogControl()
   * @generated
   */
  EAttribute getTLogControl_LnInst();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLogControl#isLogEna <em>Log Ena</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Log Ena</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLogControl#isLogEna()
   * @see #getTLogControl()
   * @generated
   */
  EAttribute getTLogControl_LogEna();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLogName <em>Log Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Log Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLogControl#getLogName()
   * @see #getTLogControl()
   * @generated
   */
  EAttribute getTLogControl_LogName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getPrefix <em>Prefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Prefix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLogControl#getPrefix()
   * @see #getTLogControl()
   * @generated
   */
  EAttribute getTLogControl_Prefix();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLogControl#isReasonCode <em>Reason Code</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Reason Code</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLogControl#isReasonCode()
   * @see #getTLogControl()
   * @generated
   */
  EAttribute getTLogControl_ReasonCode();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TLogSettings <em>TLog Settings</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TLog Settings</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLogSettings
   * @generated
   */
  EClass getTLogSettings();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLogSettings#getIntgPd <em>Intg Pd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Intg Pd</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLogSettings#getIntgPd()
   * @see #getTLogSettings()
   * @generated
   */
  EAttribute getTLogSettings_IntgPd();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLogSettings#getLogEna <em>Log Ena</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Log Ena</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLogSettings#getLogEna()
   * @see #getTLogSettings()
   * @generated
   */
  EAttribute getTLogSettings_LogEna();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TLogSettings#getTrgOps <em>Trg Ops</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Trg Ops</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLogSettings#getTrgOps()
   * @see #getTLogSettings()
   * @generated
   */
  EAttribute getTLogSettings_TrgOps();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TNaming <em>TNaming</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TNaming</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TNaming
   * @generated
   */
  EClass getTNaming();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TNaming#getDesc <em>Desc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Desc</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TNaming#getDesc()
   * @see #getTNaming()
   * @generated
   */
  EAttribute getTNaming_Desc();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TNaming#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TNaming#getName()
   * @see #getTNaming()
   * @generated
   */
  EAttribute getTNaming_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TP <em>TP</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TP</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TP
   * @generated
   */
  EClass getTP();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TP#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TP#getValue()
   * @see #getTP()
   * @generated
   */
  EAttribute getTP_Value();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TP#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TP#getType()
   * @see #getTP()
   * @generated
   */
  EAttribute getTP_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPAPPID <em>TPAPPID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPAPPID</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPAPPID
   * @generated
   */
  EClass getTPAPPID();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPhysConn <em>TPhys Conn</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPhys Conn</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPhysConn
   * @generated
   */
  EClass getTPhysConn();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TPhysConn#getP <em>P</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>P</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPhysConn#getP()
   * @see #getTPhysConn()
   * @generated
   */
  EReference getTPhysConn_P();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TPhysConn#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPhysConn#getType()
   * @see #getTPhysConn()
   * @generated
   */
  EAttribute getTPhysConn_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPIP <em>TPIP</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPIP</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPIP
   * @generated
   */
  EClass getTPIP();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPIPGATEWAY <em>TPIPGATEWAY</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPIPGATEWAY</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPIPGATEWAY
   * @generated
   */
  EClass getTPIPGATEWAY();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPIPSUBNET <em>TPIPSUBNET</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPIPSUBNET</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPIPSUBNET
   * @generated
   */
  EClass getTPIPSUBNET();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPMACAddress <em>TPMAC Address</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPMAC Address</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPMACAddress
   * @generated
   */
  EClass getTPMACAddress();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPMMSPort <em>TPMMS Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPMMS Port</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPMMSPort
   * @generated
   */
  EClass getTPMMSPort();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPOSIAEInvoke <em>TPOSIAE Invoke</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPOSIAE Invoke</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPOSIAEInvoke
   * @generated
   */
  EClass getTPOSIAEInvoke();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPOSIAEQualifier <em>TPOSIAE Qualifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPOSIAE Qualifier</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPOSIAEQualifier
   * @generated
   */
  EClass getTPOSIAEQualifier();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPOSIAPInvoke <em>TPOSIAP Invoke</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPOSIAP Invoke</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPOSIAPInvoke
   * @generated
   */
  EClass getTPOSIAPInvoke();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPOSIAPTitle <em>TPOSIAP Title</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPOSIAP Title</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPOSIAPTitle
   * @generated
   */
  EClass getTPOSIAPTitle();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPOSINSAP <em>TPOSINSAP</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPOSINSAP</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPOSINSAP
   * @generated
   */
  EClass getTPOSINSAP();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPOSIPSEL <em>TPOSIPSEL</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPOSIPSEL</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPOSIPSEL
   * @generated
   */
  EClass getTPOSIPSEL();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPOSISSEL <em>TPOSISSEL</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPOSISSEL</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPOSISSEL
   * @generated
   */
  EClass getTPOSISSEL();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPOSITSEL <em>TPOSITSEL</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPOSITSEL</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPOSITSEL
   * @generated
   */
  EClass getTPOSITSEL();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPowerSystemResource <em>TPower System Resource</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPower System Resource</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPowerSystemResource
   * @generated
   */
  EClass getTPowerSystemResource();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPowerTransformer <em>TPower Transformer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPower Transformer</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPowerTransformer
   * @generated
   */
  EClass getTPowerTransformer();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TPowerTransformer#getTransformerWinding <em>Transformer Winding</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Transformer Winding</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPowerTransformer#getTransformerWinding()
   * @see #getTPowerTransformer()
   * @generated
   */
  EReference getTPowerTransformer_TransformerWinding();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TPowerTransformer#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPowerTransformer#getType()
   * @see #getTPowerTransformer()
   * @generated
   */
  EAttribute getTPowerTransformer_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPPhysConn <em>TP Phys Conn</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TP Phys Conn</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPPhysConn
   * @generated
   */
  EClass getTPPhysConn();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TPPhysConn#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPPhysConn#getValue()
   * @see #getTPPhysConn()
   * @generated
   */
  EAttribute getTPPhysConn_Value();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TPPhysConn#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPPhysConn#getType()
   * @see #getTPPhysConn()
   * @generated
   */
  EAttribute getTPPhysConn_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPPort <em>TP Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TP Port</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPPort
   * @generated
   */
  EClass getTPPort();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPrivate <em>TPrivate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPrivate</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPrivate
   * @generated
   */
  EClass getTPrivate();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TPrivate#getSource <em>Source</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Source</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPrivate#getSource()
   * @see #getTPrivate()
   * @generated
   */
  EAttribute getTPrivate_Source();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TPrivate#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPrivate#getType()
   * @see #getTPrivate()
   * @generated
   */
  EAttribute getTPrivate_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPSNTPPort <em>TPSNTP Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPSNTP Port</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPSNTPPort
   * @generated
   */
  EClass getTPSNTPPort();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPVLANID <em>TPVLANID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPVLANID</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPVLANID
   * @generated
   */
  EClass getTPVLANID();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TPVLANPRIORITY <em>TPVLANPRIORITY</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TPVLANPRIORITY</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPVLANPRIORITY
   * @generated
   */
  EClass getTPVLANPRIORITY();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TReportControl <em>TReport Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TReport Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportControl
   * @generated
   */
  EClass getTReportControl();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TReportControl#getOptFields <em>Opt Fields</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Opt Fields</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportControl#getOptFields()
   * @see #getTReportControl()
   * @generated
   */
  EReference getTReportControl_OptFields();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TReportControl#getRptEnabled <em>Rpt Enabled</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Rpt Enabled</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportControl#getRptEnabled()
   * @see #getTReportControl()
   * @generated
   */
  EReference getTReportControl_RptEnabled();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TReportControl#isBuffered <em>Buffered</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Buffered</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportControl#isBuffered()
   * @see #getTReportControl()
   * @generated
   */
  EAttribute getTReportControl_Buffered();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TReportControl#getBufTime <em>Buf Time</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Buf Time</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportControl#getBufTime()
   * @see #getTReportControl()
   * @generated
   */
  EAttribute getTReportControl_BufTime();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TReportControl#getConfRev <em>Conf Rev</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Conf Rev</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportControl#getConfRev()
   * @see #getTReportControl()
   * @generated
   */
  EAttribute getTReportControl_ConfRev();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TReportControl#isIndexed <em>Indexed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Indexed</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportControl#isIndexed()
   * @see #getTReportControl()
   * @generated
   */
  EAttribute getTReportControl_Indexed();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TReportControl#getRptID <em>Rpt ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Rpt ID</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportControl#getRptID()
   * @see #getTReportControl()
   * @generated
   */
  EAttribute getTReportControl_RptID();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TReportSettings <em>TReport Settings</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TReport Settings</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportSettings
   * @generated
   */
  EClass getTReportSettings();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TReportSettings#getBufTime <em>Buf Time</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Buf Time</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportSettings#getBufTime()
   * @see #getTReportSettings()
   * @generated
   */
  EAttribute getTReportSettings_BufTime();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TReportSettings#getIntgPd <em>Intg Pd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Intg Pd</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportSettings#getIntgPd()
   * @see #getTReportSettings()
   * @generated
   */
  EAttribute getTReportSettings_IntgPd();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TReportSettings#getOptFields <em>Opt Fields</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Opt Fields</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportSettings#getOptFields()
   * @see #getTReportSettings()
   * @generated
   */
  EAttribute getTReportSettings_OptFields();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TReportSettings#isResvTms <em>Resv Tms</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Resv Tms</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportSettings#isResvTms()
   * @see #getTReportSettings()
   * @generated
   */
  EAttribute getTReportSettings_ResvTms();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TReportSettings#getRptID <em>Rpt ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Rpt ID</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportSettings#getRptID()
   * @see #getTReportSettings()
   * @generated
   */
  EAttribute getTReportSettings_RptID();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TReportSettings#getTrgOps <em>Trg Ops</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Trg Ops</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TReportSettings#getTrgOps()
   * @see #getTReportSettings()
   * @generated
   */
  EAttribute getTReportSettings_TrgOps();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TRptEnabled <em>TRpt Enabled</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TRpt Enabled</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TRptEnabled
   * @generated
   */
  EClass getTRptEnabled();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TRptEnabled#getClientLN <em>Client LN</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Client LN</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TRptEnabled#getClientLN()
   * @see #getTRptEnabled()
   * @generated
   */
  EReference getTRptEnabled_ClientLN();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TRptEnabled#getMax <em>Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Max</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TRptEnabled#getMax()
   * @see #getTRptEnabled()
   * @generated
   */
  EAttribute getTRptEnabled_Max();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TSampledValueControl <em>TSampled Value Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TSampled Value Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSampledValueControl
   * @generated
   */
  EClass getTSampledValueControl();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TSampledValueControl#getSmvOpts <em>Smv Opts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Smv Opts</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSampledValueControl#getSmvOpts()
   * @see #getTSampledValueControl()
   * @generated
   */
  EReference getTSampledValueControl_SmvOpts();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSampledValueControl#isMulticast <em>Multicast</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Multicast</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSampledValueControl#isMulticast()
   * @see #getTSampledValueControl()
   * @generated
   */
  EAttribute getTSampledValueControl_Multicast();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSampledValueControl#getNofASDU <em>Nof ASDU</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nof ASDU</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSampledValueControl#getNofASDU()
   * @see #getTSampledValueControl()
   * @generated
   */
  EAttribute getTSampledValueControl_NofASDU();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSampledValueControl#getSmpMod <em>Smp Mod</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Smp Mod</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSampledValueControl#getSmpMod()
   * @see #getTSampledValueControl()
   * @generated
   */
  EAttribute getTSampledValueControl_SmpMod();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSampledValueControl#getSmpRate <em>Smp Rate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Smp Rate</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSampledValueControl#getSmpRate()
   * @see #getTSampledValueControl()
   * @generated
   */
  EAttribute getTSampledValueControl_SmpRate();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSampledValueControl#getSmvID <em>Smv ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Smv ID</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSampledValueControl#getSmvID()
   * @see #getTSampledValueControl()
   * @generated
   */
  EAttribute getTSampledValueControl_SmvID();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TSDI <em>TSDI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TSDI</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSDI
   * @generated
   */
  EClass getTSDI();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TSDI#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Group</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSDI#getGroup()
   * @see #getTSDI()
   * @generated
   */
  EAttribute getTSDI_Group();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TSDI#getSDI <em>SDI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>SDI</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSDI#getSDI()
   * @see #getTSDI()
   * @generated
   */
  EReference getTSDI_SDI();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TSDI#getDAI <em>DAI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>DAI</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSDI#getDAI()
   * @see #getTSDI()
   * @generated
   */
  EReference getTSDI_DAI();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSDI#getIx <em>Ix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ix</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSDI#getIx()
   * @see #getTSDI()
   * @generated
   */
  EAttribute getTSDI_Ix();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSDI#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSDI#getName()
   * @see #getTSDI()
   * @generated
   */
  EAttribute getTSDI_Name();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TSDO <em>TSDO</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TSDO</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSDO
   * @generated
   */
  EClass getTSDO();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSDO#getCount <em>Count</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Count</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSDO#getCount()
   * @see #getTSDO()
   * @generated
   */
  EAttribute getTSDO_Count();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSDO#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSDO#getName()
   * @see #getTSDO()
   * @generated
   */
  EAttribute getTSDO_Name();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSDO#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSDO#getType()
   * @see #getTSDO()
   * @generated
   */
  EAttribute getTSDO_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TServer <em>TServer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TServer</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServer
   * @generated
   */
  EClass getTServer();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServer#getAuthentication <em>Authentication</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Authentication</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServer#getAuthentication()
   * @see #getTServer()
   * @generated
   */
  EReference getTServer_Authentication();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TServer#getLDevice <em>LDevice</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>LDevice</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServer#getLDevice()
   * @see #getTServer()
   * @generated
   */
  EReference getTServer_LDevice();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TServer#getAssociation <em>Association</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Association</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServer#getAssociation()
   * @see #getTServer()
   * @generated
   */
  EReference getTServer_Association();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TServer#getTimeout <em>Timeout</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Timeout</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServer#getTimeout()
   * @see #getTServer()
   * @generated
   */
  EAttribute getTServer_Timeout();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TServerAt <em>TServer At</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TServer At</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServerAt
   * @generated
   */
  EClass getTServerAt();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TServerAt#getApName <em>Ap Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ap Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServerAt#getApName()
   * @see #getTServerAt()
   * @generated
   */
  EAttribute getTServerAt_ApName();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TServiceConfReportControl <em>TService Conf Report Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TService Conf Report Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceConfReportControl
   * @generated
   */
  EClass getTServiceConfReportControl();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TServiceConfReportControl#isBufConf <em>Buf Conf</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Buf Conf</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceConfReportControl#isBufConf()
   * @see #getTServiceConfReportControl()
   * @generated
   */
  EAttribute getTServiceConfReportControl_BufConf();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TServiceConfReportControl#getBufMode <em>Buf Mode</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Buf Mode</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceConfReportControl#getBufMode()
   * @see #getTServiceConfReportControl()
   * @generated
   */
  EAttribute getTServiceConfReportControl_BufMode();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TServiceForConfDataSet <em>TService For Conf Data Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TService For Conf Data Set</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceForConfDataSet
   * @generated
   */
  EClass getTServiceForConfDataSet();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TServiceForConfDataSet#isModify <em>Modify</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Modify</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceForConfDataSet#isModify()
   * @see #getTServiceForConfDataSet()
   * @generated
   */
  EAttribute getTServiceForConfDataSet_Modify();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TServices <em>TServices</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TServices</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices
   * @generated
   */
  EClass getTServices();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getDynAssociation <em>Dyn Association</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dyn Association</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getDynAssociation()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_DynAssociation();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getSettingGroups <em>Setting Groups</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Setting Groups</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getSettingGroups()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_SettingGroups();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getGetDirectory <em>Get Directory</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Get Directory</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getGetDirectory()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_GetDirectory();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getGetDataObjectDefinition <em>Get Data Object Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Get Data Object Definition</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getGetDataObjectDefinition()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_GetDataObjectDefinition();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getDataObjectDirectory <em>Data Object Directory</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Data Object Directory</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getDataObjectDirectory()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_DataObjectDirectory();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getGetDataSetValue <em>Get Data Set Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Get Data Set Value</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getGetDataSetValue()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_GetDataSetValue();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getSetDataSetValue <em>Set Data Set Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Set Data Set Value</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getSetDataSetValue()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_SetDataSetValue();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getDataSetDirectory <em>Data Set Directory</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Data Set Directory</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getDataSetDirectory()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_DataSetDirectory();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getConfDataSet <em>Conf Data Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Conf Data Set</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getConfDataSet()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_ConfDataSet();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getDynDataSet <em>Dyn Data Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dyn Data Set</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getDynDataSet()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_DynDataSet();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getReadWrite <em>Read Write</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Read Write</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getReadWrite()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_ReadWrite();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getTimerActivatedControl <em>Timer Activated Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Timer Activated Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getTimerActivatedControl()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_TimerActivatedControl();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getConfReportControl <em>Conf Report Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Conf Report Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getConfReportControl()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_ConfReportControl();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getGetCBValues <em>Get CB Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Get CB Values</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getGetCBValues()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_GetCBValues();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getConfLogControl <em>Conf Log Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Conf Log Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getConfLogControl()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_ConfLogControl();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getReportSettings <em>Report Settings</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Report Settings</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getReportSettings()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_ReportSettings();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getLogSettings <em>Log Settings</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Log Settings</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getLogSettings()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_LogSettings();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getGSESettings <em>GSE Settings</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>GSE Settings</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getGSESettings()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_GSESettings();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getSMVSettings <em>SMV Settings</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>SMV Settings</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getSMVSettings()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_SMVSettings();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getGSEDir <em>GSE Dir</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>GSE Dir</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getGSEDir()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_GSEDir();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getGOOSE <em>GOOSE</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>GOOSE</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getGOOSE()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_GOOSE();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getGSSE <em>GSSE</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>GSSE</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getGSSE()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_GSSE();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getSMVsc <em>SM Vsc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>SM Vsc</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getSMVsc()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_SMVsc();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getFileHandling <em>File Handling</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>File Handling</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getFileHandling()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_FileHandling();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getConfLNs <em>Conf LNs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Conf LNs</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getConfLNs()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_ConfLNs();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getClientServices <em>Client Services</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Client Services</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getClientServices()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_ClientServices();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getConfLdName <em>Conf Ld Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Conf Ld Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getConfLdName()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_ConfLdName();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getSupSubscription <em>Sup Subscription</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sup Subscription</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getSupSubscription()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_SupSubscription();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TServices#getConfSigRef <em>Conf Sig Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Conf Sig Ref</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getConfSigRef()
   * @see #getTServices()
   * @generated
   */
  EReference getTServices_ConfSigRef();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TServices#getNameLength <em>Name Length</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name Length</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServices#getNameLength()
   * @see #getTServices()
   * @generated
   */
  EAttribute getTServices_NameLength();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TServiceSettings <em>TService Settings</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TService Settings</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettings
   * @generated
   */
  EClass getTServiceSettings();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TServiceSettings#getCbName <em>Cb Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Cb Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettings#getCbName()
   * @see #getTServiceSettings()
   * @generated
   */
  EAttribute getTServiceSettings_CbName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TServiceSettings#getDatSet <em>Dat Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dat Set</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettings#getDatSet()
   * @see #getTServiceSettings()
   * @generated
   */
  EAttribute getTServiceSettings_DatSet();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TServiceWithMax <em>TService With Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TService With Max</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceWithMax
   * @generated
   */
  EClass getTServiceWithMax();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TServiceWithMax#getMax <em>Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Max</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceWithMax#getMax()
   * @see #getTServiceWithMax()
   * @generated
   */
  EAttribute getTServiceWithMax_Max();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TServiceWithMaxAndMaxAttributes <em>TService With Max And Max Attributes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TService With Max And Max Attributes</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceWithMaxAndMaxAttributes
   * @generated
   */
  EClass getTServiceWithMaxAndMaxAttributes();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TServiceWithMaxAndMaxAttributes#getMaxAttributes <em>Max Attributes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Max Attributes</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceWithMaxAndMaxAttributes#getMaxAttributes()
   * @see #getTServiceWithMaxAndMaxAttributes()
   * @generated
   */
  EAttribute getTServiceWithMaxAndMaxAttributes_MaxAttributes();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TServiceWithMaxAndModify <em>TService With Max And Modify</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TService With Max And Modify</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceWithMaxAndModify
   * @generated
   */
  EClass getTServiceWithMaxAndModify();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TServiceWithMaxAndModify#isModify <em>Modify</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Modify</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceWithMaxAndModify#isModify()
   * @see #getTServiceWithMaxAndModify()
   * @generated
   */
  EAttribute getTServiceWithMaxAndModify_Modify();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TServiceWithOptionalMax <em>TService With Optional Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TService With Optional Max</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceWithOptionalMax
   * @generated
   */
  EClass getTServiceWithOptionalMax();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TServiceWithOptionalMax#getMax <em>Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Max</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceWithOptionalMax#getMax()
   * @see #getTServiceWithOptionalMax()
   * @generated
   */
  EAttribute getTServiceWithOptionalMax_Max();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TServiceYesNo <em>TService Yes No</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TService Yes No</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceYesNo
   * @generated
   */
  EClass getTServiceYesNo();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TSettingControl <em>TSetting Control</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TSetting Control</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSettingControl
   * @generated
   */
  EClass getTSettingControl();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSettingControl#getActSG <em>Act SG</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Act SG</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSettingControl#getActSG()
   * @see #getTSettingControl()
   * @generated
   */
  EAttribute getTSettingControl_ActSG();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSettingControl#getNumOfSGs <em>Num Of SGs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Num Of SGs</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSettingControl#getNumOfSGs()
   * @see #getTSettingControl()
   * @generated
   */
  EAttribute getTSettingControl_NumOfSGs();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TSMV <em>TSMV</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TSMV</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSMV
   * @generated
   */
  EClass getTSMV();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings <em>TSMV Settings</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TSMV Settings</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSMVSettings
   * @generated
   */
  EClass getTSMVSettings();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Group</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSMVSettings#getGroup()
   * @see #getTSMVSettings()
   * @generated
   */
  EAttribute getTSMVSettings_Group();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSmpRate <em>Smp Rate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Smp Rate</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSMVSettings#getSmpRate()
   * @see #getTSMVSettings()
   * @generated
   */
  EAttribute getTSMVSettings_SmpRate();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSamplesPerSec <em>Samples Per Sec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Samples Per Sec</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSMVSettings#getSamplesPerSec()
   * @see #getTSMVSettings()
   * @generated
   */
  EAttribute getTSMVSettings_SamplesPerSec();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSecPerSamples <em>Sec Per Samples</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Sec Per Samples</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSMVSettings#getSecPerSamples()
   * @see #getTSMVSettings()
   * @generated
   */
  EAttribute getTSMVSettings_SecPerSamples();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getOptFields <em>Opt Fields</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Opt Fields</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSMVSettings#getOptFields()
   * @see #getTSMVSettings()
   * @generated
   */
  EAttribute getTSMVSettings_OptFields();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#isSamplesPerSec1 <em>Samples Per Sec1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Samples Per Sec1</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSMVSettings#isSamplesPerSec1()
   * @see #getTSMVSettings()
   * @generated
   */
  EAttribute getTSMVSettings_SamplesPerSec1();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSmpRate1 <em>Smp Rate1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Smp Rate1</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSMVSettings#getSmpRate1()
   * @see #getTSMVSettings()
   * @generated
   */
  EAttribute getTSMVSettings_SmpRate1();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSvID <em>Sv ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Sv ID</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSMVSettings#getSvID()
   * @see #getTSMVSettings()
   * @generated
   */
  EAttribute getTSMVSettings_SvID();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TSubEquipment <em>TSub Equipment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TSub Equipment</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubEquipment
   * @generated
   */
  EClass getTSubEquipment();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSubEquipment#getPhase <em>Phase</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Phase</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubEquipment#getPhase()
   * @see #getTSubEquipment()
   * @generated
   */
  EAttribute getTSubEquipment_Phase();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSubEquipment#isVirtual <em>Virtual</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Virtual</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubEquipment#isVirtual()
   * @see #getTSubEquipment()
   * @generated
   */
  EAttribute getTSubEquipment_Virtual();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TSubFunction <em>TSub Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TSub Function</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubFunction
   * @generated
   */
  EClass getTSubFunction();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TSubFunction#getGeneralEquipment <em>General Equipment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>General Equipment</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubFunction#getGeneralEquipment()
   * @see #getTSubFunction()
   * @generated
   */
  EReference getTSubFunction_GeneralEquipment();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TSubFunction#getConductingEquipment <em>Conducting Equipment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Conducting Equipment</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubFunction#getConductingEquipment()
   * @see #getTSubFunction()
   * @generated
   */
  EReference getTSubFunction_ConductingEquipment();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSubFunction#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubFunction#getType()
   * @see #getTSubFunction()
   * @generated
   */
  EAttribute getTSubFunction_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TSubNetwork <em>TSub Network</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TSub Network</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubNetwork
   * @generated
   */
  EClass getTSubNetwork();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TSubNetwork#getBitRate <em>Bit Rate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bit Rate</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubNetwork#getBitRate()
   * @see #getTSubNetwork()
   * @generated
   */
  EReference getTSubNetwork_BitRate();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TSubNetwork#getConnectedAP <em>Connected AP</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Connected AP</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubNetwork#getConnectedAP()
   * @see #getTSubNetwork()
   * @generated
   */
  EReference getTSubNetwork_ConnectedAP();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TSubNetwork#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubNetwork#getType()
   * @see #getTSubNetwork()
   * @generated
   */
  EAttribute getTSubNetwork_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TSubstation <em>TSubstation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TSubstation</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubstation
   * @generated
   */
  EClass getTSubstation();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TSubstation#getVoltageLevel <em>Voltage Level</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Voltage Level</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubstation#getVoltageLevel()
   * @see #getTSubstation()
   * @generated
   */
  EReference getTSubstation_VoltageLevel();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TSubstation#getFunction <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Function</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSubstation#getFunction()
   * @see #getTSubstation()
   * @generated
   */
  EReference getTSubstation_Function();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TTapChanger <em>TTap Changer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TTap Changer</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTapChanger
   * @generated
   */
  EClass getTTapChanger();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTapChanger#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTapChanger#getType()
   * @see #getTTapChanger()
   * @generated
   */
  EAttribute getTTapChanger_Type();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTapChanger#isVirtual <em>Virtual</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Virtual</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTapChanger#isVirtual()
   * @see #getTTapChanger()
   * @generated
   */
  EAttribute getTTapChanger_Virtual();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TTerminal <em>TTerminal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TTerminal</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTerminal
   * @generated
   */
  EClass getTTerminal();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getBayName <em>Bay Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bay Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTerminal#getBayName()
   * @see #getTTerminal()
   * @generated
   */
  EAttribute getTTerminal_BayName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getCNodeName <em>CNode Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>CNode Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTerminal#getCNodeName()
   * @see #getTTerminal()
   * @generated
   */
  EAttribute getTTerminal_CNodeName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getConnectivityNode <em>Connectivity Node</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Connectivity Node</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTerminal#getConnectivityNode()
   * @see #getTTerminal()
   * @generated
   */
  EAttribute getTTerminal_ConnectivityNode();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTerminal#getName()
   * @see #getTTerminal()
   * @generated
   */
  EAttribute getTTerminal_Name();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTerminal#isNeutralPoint <em>Neutral Point</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Neutral Point</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTerminal#isNeutralPoint()
   * @see #getTTerminal()
   * @generated
   */
  EAttribute getTTerminal_NeutralPoint();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getSubstationName <em>Substation Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Substation Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTerminal#getSubstationName()
   * @see #getTTerminal()
   * @generated
   */
  EAttribute getTTerminal_SubstationName();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getVoltageLevelName <em>Voltage Level Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Voltage Level Name</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTerminal#getVoltageLevelName()
   * @see #getTTerminal()
   * @generated
   */
  EAttribute getTTerminal_VoltageLevelName();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TText <em>TText</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TText</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TText
   * @generated
   */
  EClass getTText();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TText#getSource <em>Source</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Source</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TText#getSource()
   * @see #getTText()
   * @generated
   */
  EAttribute getTText_Source();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TTransformerWinding <em>TTransformer Winding</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TTransformer Winding</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTransformerWinding
   * @generated
   */
  EClass getTTransformerWinding();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TTransformerWinding#getTapChanger <em>Tap Changer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Tap Changer</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTransformerWinding#getTapChanger()
   * @see #getTTransformerWinding()
   * @generated
   */
  EReference getTTransformerWinding_TapChanger();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTransformerWinding#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTransformerWinding#getType()
   * @see #getTTransformerWinding()
   * @generated
   */
  EAttribute getTTransformerWinding_Type();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TTrgOps <em>TTrg Ops</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TTrg Ops</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTrgOps
   * @generated
   */
  EClass getTTrgOps();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isDchg <em>Dchg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dchg</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTrgOps#isDchg()
   * @see #getTTrgOps()
   * @generated
   */
  EAttribute getTTrgOps_Dchg();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isDupd <em>Dupd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dupd</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTrgOps#isDupd()
   * @see #getTTrgOps()
   * @generated
   */
  EAttribute getTTrgOps_Dupd();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isGi <em>Gi</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Gi</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTrgOps#isGi()
   * @see #getTTrgOps()
   * @generated
   */
  EAttribute getTTrgOps_Gi();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isPeriod <em>Period</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Period</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTrgOps#isPeriod()
   * @see #getTTrgOps()
   * @generated
   */
  EAttribute getTTrgOps_Period();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isQchg <em>Qchg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Qchg</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTrgOps#isQchg()
   * @see #getTTrgOps()
   * @generated
   */
  EAttribute getTTrgOps_Qchg();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TUnNaming <em>TUn Naming</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TUn Naming</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TUnNaming
   * @generated
   */
  EClass getTUnNaming();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TUnNaming#getDesc <em>Desc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Desc</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TUnNaming#getDesc()
   * @see #getTUnNaming()
   * @generated
   */
  EAttribute getTUnNaming_Desc();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TVal <em>TVal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TVal</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TVal
   * @generated
   */
  EClass getTVal();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TVal#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TVal#getValue()
   * @see #getTVal()
   * @generated
   */
  EAttribute getTVal_Value();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TVal#getSGroup <em>SGroup</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>SGroup</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TVal#getSGroup()
   * @see #getTVal()
   * @generated
   */
  EAttribute getTVal_SGroup();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TValueWithUnit <em>TValue With Unit</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TValue With Unit</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TValueWithUnit
   * @generated
   */
  EClass getTValueWithUnit();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TValueWithUnit#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TValueWithUnit#getValue()
   * @see #getTValueWithUnit()
   * @generated
   */
  EAttribute getTValueWithUnit_Value();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TValueWithUnit#getMultiplier <em>Multiplier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Multiplier</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TValueWithUnit#getMultiplier()
   * @see #getTValueWithUnit()
   * @generated
   */
  EAttribute getTValueWithUnit_Multiplier();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.scl.TValueWithUnit#getUnit <em>Unit</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Unit</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TValueWithUnit#getUnit()
   * @see #getTValueWithUnit()
   * @generated
   */
  EAttribute getTValueWithUnit_Unit();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TVoltage <em>TVoltage</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TVoltage</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TVoltage
   * @generated
   */
  EClass getTVoltage();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.scl.TVoltageLevel <em>TVoltage Level</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TVoltage Level</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TVoltageLevel
   * @generated
   */
  EClass getTVoltageLevel();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.scl.TVoltageLevel#getVoltage <em>Voltage</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Voltage</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TVoltageLevel#getVoltage()
   * @see #getTVoltageLevel()
   * @generated
   */
  EReference getTVoltageLevel_Voltage();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TVoltageLevel#getBay <em>Bay</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Bay</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TVoltageLevel#getBay()
   * @see #getTVoltageLevel()
   * @generated
   */
  EReference getTVoltageLevel_Bay();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.scl.TVoltageLevel#getFunction <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Function</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TVoltageLevel#getFunction()
   * @see #getTVoltageLevel()
   * @generated
   */
  EReference getTVoltageLevel_Function();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.BufModeType <em>Buf Mode Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Buf Mode Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.BufModeType
   * @generated
   */
  EEnum getBufModeType();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.NameStructureType <em>Name Structure Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Name Structure Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.NameStructureType
   * @generated
   */
  EEnum getNameStructureType();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TAssociationKindEnum <em>TAssociation Kind Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TAssociation Kind Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAssociationKindEnum
   * @generated
   */
  EEnum getTAssociationKindEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TAuthenticationEnum <em>TAuthentication Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TAuthentication Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAuthenticationEnum
   * @generated
   */
  EEnum getTAuthenticationEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupAEnum <em>TDomain LN Group AEnum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TDomain LN Group AEnum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupAEnum
   * @generated
   */
  EEnum getTDomainLNGroupAEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupCEnum <em>TDomain LN Group CEnum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TDomain LN Group CEnum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupCEnum
   * @generated
   */
  EEnum getTDomainLNGroupCEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupGEnum <em>TDomain LN Group GEnum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TDomain LN Group GEnum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupGEnum
   * @generated
   */
  EEnum getTDomainLNGroupGEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupIEnum <em>TDomain LN Group IEnum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TDomain LN Group IEnum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupIEnum
   * @generated
   */
  EEnum getTDomainLNGroupIEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupMEnum <em>TDomain LN Group MEnum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TDomain LN Group MEnum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupMEnum
   * @generated
   */
  EEnum getTDomainLNGroupMEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupPEnum <em>TDomain LN Group PEnum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TDomain LN Group PEnum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupPEnum
   * @generated
   */
  EEnum getTDomainLNGroupPEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupREnum <em>TDomain LN Group REnum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TDomain LN Group REnum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupREnum
   * @generated
   */
  EEnum getTDomainLNGroupREnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupSEnum <em>TDomain LN Group SEnum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TDomain LN Group SEnum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupSEnum
   * @generated
   */
  EEnum getTDomainLNGroupSEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupTEnum <em>TDomain LN Group TEnum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TDomain LN Group TEnum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupTEnum
   * @generated
   */
  EEnum getTDomainLNGroupTEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupXEnum <em>TDomain LN Group XEnum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TDomain LN Group XEnum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupXEnum
   * @generated
   */
  EEnum getTDomainLNGroupXEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupYEnum <em>TDomain LN Group YEnum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TDomain LN Group YEnum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupYEnum
   * @generated
   */
  EEnum getTDomainLNGroupYEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupZEnum <em>TDomain LN Group ZEnum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TDomain LN Group ZEnum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupZEnum
   * @generated
   */
  EEnum getTDomainLNGroupZEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TFCEnum <em>TFC Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TFC Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFCEnum
   * @generated
   */
  EEnum getTFCEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TGSEControlTypeEnum <em>TGSE Control Type Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TGSE Control Type Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGSEControlTypeEnum
   * @generated
   */
  EEnum getTGSEControlTypeEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TLLN0Enum <em>TLLN0 Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TLLN0 Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLLN0Enum
   * @generated
   */
  EEnum getTLLN0Enum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TLPHDEnum <em>TLPHD Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TLPHD Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLPHDEnum
   * @generated
   */
  EEnum getTLPHDEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TPhaseEnum <em>TPhase Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TPhase Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPhaseEnum
   * @generated
   */
  EEnum getTPhaseEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TPowerTransformerEnum <em>TPower Transformer Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TPower Transformer Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPowerTransformerEnum
   * @generated
   */
  EEnum getTPowerTransformerEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TPredefinedAttributeNameEnum <em>TPredefined Attribute Name Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TPredefined Attribute Name Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedAttributeNameEnum
   * @generated
   */
  EEnum getTPredefinedAttributeNameEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TPredefinedBasicTypeEnum <em>TPredefined Basic Type Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TPredefined Basic Type Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedBasicTypeEnum
   * @generated
   */
  EEnum getTPredefinedBasicTypeEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TPredefinedCDCEnum <em>TPredefined CDC Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TPredefined CDC Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedCDCEnum
   * @generated
   */
  EEnum getTPredefinedCDCEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TPredefinedCommonConductingEquipmentEnum <em>TPredefined Common Conducting Equipment Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TPredefined Common Conducting Equipment Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedCommonConductingEquipmentEnum
   * @generated
   */
  EEnum getTPredefinedCommonConductingEquipmentEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TPredefinedGeneralEquipmentEnum <em>TPredefined General Equipment Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TPredefined General Equipment Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedGeneralEquipmentEnum
   * @generated
   */
  EEnum getTPredefinedGeneralEquipmentEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TPredefinedPhysConnTypeEnum <em>TPredefined Phys Conn Type Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TPredefined Phys Conn Type Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedPhysConnTypeEnum
   * @generated
   */
  EEnum getTPredefinedPhysConnTypeEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TPredefinedPTypeEnum <em>TPredefined PType Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TPredefined PType Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedPTypeEnum
   * @generated
   */
  EEnum getTPredefinedPTypeEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TPredefinedPTypePhysConnEnum <em>TPredefined PType Phys Conn Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TPredefined PType Phys Conn Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedPTypePhysConnEnum
   * @generated
   */
  EEnum getTPredefinedPTypePhysConnEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TRightEnum <em>TRight Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TRight Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TRightEnum
   * @generated
   */
  EEnum getTRightEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum <em>TService Settings Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TService Settings Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum
   * @generated
   */
  EEnum getTServiceSettingsEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TServiceType <em>TService Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TService Type</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceType
   * @generated
   */
  EEnum getTServiceType();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TSIUnitEnum <em>TSI Unit Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TSI Unit Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSIUnitEnum
   * @generated
   */
  EEnum getTSIUnitEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TSmpMod <em>TSmp Mod</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TSmp Mod</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSmpMod
   * @generated
   */
  EEnum getTSmpMod();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TTransformerWindingEnum <em>TTransformer Winding Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TTransformer Winding Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTransformerWindingEnum
   * @generated
   */
  EEnum getTTransformerWindingEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TUnitMultiplierEnum <em>TUnit Multiplier Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TUnit Multiplier Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TUnitMultiplierEnum
   * @generated
   */
  EEnum getTUnitMultiplierEnum();

  /**
   * Returns the meta object for enum '{@link com.lucy.g3.iec61850.model.scl.TValKindEnum <em>TVal Kind Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>TVal Kind Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TValKindEnum
   * @generated
   */
  EEnum getTValKindEnum();

  /**
   * Returns the meta object for data type '<em>Act SG Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Act SG Type</em>'.
   * @model instanceClass="long"
   *        extendedMetaData="name='actSG_._type' baseType='http://www.eclipse.org/emf/2003/XMLType#unsignedInt' minInclusive='1'"
   * @generated
   */
  EDataType getActSGType();

  /**
   * Returns the meta object for data type '{@link java.lang.Long <em>Act SG Type Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Act SG Type Object</em>'.
   * @see java.lang.Long
   * @model instanceClass="java.lang.Long"
   *        extendedMetaData="name='actSG_._type:Object' baseType='actSG_._type'"
   * @generated
   */
  EDataType getActSGTypeObject();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>App ID Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>App ID Type</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='appID_._type' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' maxLength='128' pattern='\\p{IsBasicLatin}*'"
   * @generated
   */
  EDataType getAppIDType();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.BufModeType <em>Buf Mode Type Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Buf Mode Type Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.BufModeType
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.BufModeType"
   *        extendedMetaData="name='bufMode_._type:Object' baseType='bufMode_._type'"
   * @generated
   */
  EDataType getBufModeTypeObject();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>Common Name Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Common Name Type</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='commonName_._type' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' minLength='4' pattern='none CN=.+'"
   * @generated
   */
  EDataType getCommonNameType();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>Id Hierarchy Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Id Hierarchy Type</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='idHierarchy_._type' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' minLength='1'"
   * @generated
   */
  EDataType getIdHierarchyType();

  /**
   * Returns the meta object for data type '<em>Name Length Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Name Length Type</em>'.
   * @model instanceClass="long"
   *        extendedMetaData="name='nameLength_._type' baseType='http://www.eclipse.org/emf/2003/XMLType#unsignedInt' minExclusive='0'"
   * @generated
   */
  EDataType getNameLengthType();

  /**
   * Returns the meta object for data type '{@link java.lang.Long <em>Name Length Type Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Name Length Type Object</em>'.
   * @see java.lang.Long
   * @model instanceClass="java.lang.Long"
   *        extendedMetaData="name='nameLength_._type:Object' baseType='nameLength_._type'"
   * @generated
   */
  EDataType getNameLengthTypeObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.NameStructureType <em>Name Structure Type Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Name Structure Type Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.NameStructureType
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.NameStructureType"
   *        extendedMetaData="name='nameStructure_._type:Object' baseType='nameStructure_._type'"
   * @generated
   */
  EDataType getNameStructureTypeObject();

  /**
   * Returns the meta object for data type '<em>Num Of SGs Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Num Of SGs Type</em>'.
   * @model instanceClass="long"
   *        extendedMetaData="name='numOfSGs_._type' baseType='http://www.eclipse.org/emf/2003/XMLType#unsignedInt' minInclusive='1'"
   * @generated
   */
  EDataType getNumOfSGsType();

  /**
   * Returns the meta object for data type '{@link java.lang.Long <em>Num Of SGs Type Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Num Of SGs Type Object</em>'.
   * @see java.lang.Long
   * @model instanceClass="java.lang.Long"
   *        extendedMetaData="name='numOfSGs_._type:Object' baseType='numOfSGs_._type'"
   * @generated
   */
  EDataType getNumOfSGsTypeObject();

  /**
   * Returns the meta object for data type '<em>Samples Per Sec Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Samples Per Sec Type</em>'.
   * @model instanceClass="long"
   *        extendedMetaData="name='SamplesPerSec_._type' baseType='http://www.eclipse.org/emf/2003/XMLType#unsignedInt' minExclusive='0'"
   * @generated
   */
  EDataType getSamplesPerSecType();

  /**
   * Returns the meta object for data type '{@link java.lang.Long <em>Samples Per Sec Type Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Samples Per Sec Type Object</em>'.
   * @see java.lang.Long
   * @model instanceClass="java.lang.Long"
   *        extendedMetaData="name='SamplesPerSec_._type:Object' baseType='SamplesPerSec_._type'"
   * @generated
   */
  EDataType getSamplesPerSecTypeObject();

  /**
   * Returns the meta object for data type '<em>Sec Per Samples Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Sec Per Samples Type</em>'.
   * @model instanceClass="long"
   *        extendedMetaData="name='SecPerSamples_._type' baseType='http://www.eclipse.org/emf/2003/XMLType#unsignedInt' minExclusive='0'"
   * @generated
   */
  EDataType getSecPerSamplesType();

  /**
   * Returns the meta object for data type '{@link java.lang.Long <em>Sec Per Samples Type Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Sec Per Samples Type Object</em>'.
   * @see java.lang.Long
   * @model instanceClass="java.lang.Long"
   *        extendedMetaData="name='SecPerSamples_._type:Object' baseType='SecPerSamples_._type'"
   * @generated
   */
  EDataType getSecPerSamplesTypeObject();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>Serial Number Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Serial Number Type</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='serialNumber_._type' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' minLength='1' pattern='[0-9]+'"
   * @generated
   */
  EDataType getSerialNumberType();

  /**
   * Returns the meta object for data type '<em>Smp Rate Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Smp Rate Type</em>'.
   * @model instanceClass="long"
   *        extendedMetaData="name='SmpRate_._type' baseType='http://www.eclipse.org/emf/2003/XMLType#unsignedInt' minExclusive='0'"
   * @generated
   */
  EDataType getSmpRateType();

  /**
   * Returns the meta object for data type '{@link java.lang.Long <em>Smp Rate Type Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Smp Rate Type Object</em>'.
   * @see java.lang.Long
   * @model instanceClass="java.lang.Long"
   *        extendedMetaData="name='SmpRate_._type:Object' baseType='SmpRate_._type'"
   * @generated
   */
  EDataType getSmpRateTypeObject();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>Smv ID Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Smv ID Type</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='smvID_._type' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' maxLength='128' pattern='\\p{IsBasicLatin}*'"
   * @generated
   */
  EDataType getSmvIDType();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TAccess Point Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TAccess Point Name</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tAccessPointName' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' pattern='[A-Z,a-z,0-9][0-9,A-Z,a-z,_]*'"
   * @generated
   */
  EDataType getTAccessPointName();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TAcsi Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TAcsi Name</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tAcsiName' baseType='http://www.eclipse.org/emf/2003/XMLType#Name' pattern='[A-Z,a-z][0-9,A-Z,a-z,_]*'"
   * @generated
   */
  EDataType getTAcsiName();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TAny Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TAny Name</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tAnyName' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString'"
   * @generated
   */
  EDataType getTAnyName();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TAssociation ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TAssociation ID</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tAssociationID' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' minLength='1' pattern='[0-9,A-Z,a-z]+'"
   * @generated
   */
  EDataType getTAssociationID();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TAssociationKindEnum <em>TAssociation Kind Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TAssociation Kind Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAssociationKindEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TAssociationKindEnum"
   *        extendedMetaData="name='tAssociationKindEnum:Object' baseType='tAssociationKindEnum'"
   * @generated
   */
  EDataType getTAssociationKindEnumObject();

  /**
   * Returns the meta object for data type '{@link java.lang.Object <em>TAttribute Name Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TAttribute Name Enum</em>'.
   * @see java.lang.Object
   * @model instanceClass="java.lang.Object"
   *        extendedMetaData="name='tAttributeNameEnum' memberTypes='tPredefinedAttributeNameEnum tExtensionAttributeNameEnum'"
   * @generated
   */
  EDataType getTAttributeNameEnum();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TAuthenticationEnum <em>TAuthentication Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TAuthentication Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TAuthenticationEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TAuthenticationEnum"
   *        extendedMetaData="name='tAuthenticationEnum:Object' baseType='tAuthenticationEnum'"
   * @generated
   */
  EDataType getTAuthenticationEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TPredefinedBasicTypeEnum <em>TBasic Type Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TBasic Type Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedBasicTypeEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TPredefinedBasicTypeEnum"
   *        extendedMetaData="name='tBasicTypeEnum' baseType='tPredefinedBasicTypeEnum'"
   * @generated
   */
  EDataType getTBasicTypeEnum();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TCB Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TCB Name</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tCBName' baseType='tAcsiName' maxLength='32'"
   * @generated
   */
  EDataType getTCBName();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TPredefinedCDCEnum <em>TCDC Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TCDC Enum</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedCDCEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TPredefinedCDCEnum"
   *        extendedMetaData="name='tCDCEnum' baseType='tPredefinedCDCEnum'"
   * @generated
   */
  EDataType getTCDCEnum();

  /**
   * Returns the meta object for data type '{@link java.lang.Object <em>TCommon Conducting Equipment Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TCommon Conducting Equipment Enum</em>'.
   * @see java.lang.Object
   * @model instanceClass="java.lang.Object"
   *        extendedMetaData="name='tCommonConductingEquipmentEnum' memberTypes='tPredefinedCommonConductingEquipmentEnum tExtensionEquipmentEnum'"
   * @generated
   */
  EDataType getTCommonConductingEquipmentEnum();

  /**
   * Returns the meta object for data type '{@link java.lang.Object <em>TDA Count</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDA Count</em>'.
   * @see java.lang.Object
   * @model instanceClass="java.lang.Object"
   *        extendedMetaData="name='tDACount' memberTypes='http://www.eclipse.org/emf/2003/XMLType#unsignedInt tAttributeNameEnum'"
   * @generated
   */
  EDataType getTDACount();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TData Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TData Name</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tDataName' baseType='tRestrName1stU' maxLength='12'"
   * @generated
   */
  EDataType getTDataName();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TData Set Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TData Set Name</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tDataSetName' baseType='tAcsiName' maxLength='32'"
   * @generated
   */
  EDataType getTDataSetName();

  /**
   * Returns the meta object for data type '{@link org.eclipse.emf.common.util.Enumerator <em>TDomain LN Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDomain LN Enum</em>'.
   * @see org.eclipse.emf.common.util.Enumerator
   * @model instanceClass="org.eclipse.emf.common.util.Enumerator"
   *        extendedMetaData="name='tDomainLNEnum' memberTypes='tDomainLNGroupAEnum tDomainLNGroupCEnum tDomainLNGroupGEnum tDomainLNGroupIEnum tDomainLNGroupMEnum tDomainLNGroupPEnum tDomainLNGroupREnum tDomainLNGroupSEnum tDomainLNGroupTEnum tDomainLNGroupXEnum tDomainLNGroupYEnum tDomainLNGroupZEnum'"
   * @generated
   */
  EDataType getTDomainLNEnum();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupAEnum <em>TDomain LN Group AEnum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDomain LN Group AEnum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupAEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TDomainLNGroupAEnum"
   *        extendedMetaData="name='tDomainLNGroupAEnum:Object' baseType='tDomainLNGroupAEnum'"
   * @generated
   */
  EDataType getTDomainLNGroupAEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupCEnum <em>TDomain LN Group CEnum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDomain LN Group CEnum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupCEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TDomainLNGroupCEnum"
   *        extendedMetaData="name='tDomainLNGroupCEnum:Object' baseType='tDomainLNGroupCEnum'"
   * @generated
   */
  EDataType getTDomainLNGroupCEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupGEnum <em>TDomain LN Group GEnum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDomain LN Group GEnum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupGEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TDomainLNGroupGEnum"
   *        extendedMetaData="name='tDomainLNGroupGEnum:Object' baseType='tDomainLNGroupGEnum'"
   * @generated
   */
  EDataType getTDomainLNGroupGEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupIEnum <em>TDomain LN Group IEnum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDomain LN Group IEnum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupIEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TDomainLNGroupIEnum"
   *        extendedMetaData="name='tDomainLNGroupIEnum:Object' baseType='tDomainLNGroupIEnum'"
   * @generated
   */
  EDataType getTDomainLNGroupIEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupMEnum <em>TDomain LN Group MEnum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDomain LN Group MEnum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupMEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TDomainLNGroupMEnum"
   *        extendedMetaData="name='tDomainLNGroupMEnum:Object' baseType='tDomainLNGroupMEnum'"
   * @generated
   */
  EDataType getTDomainLNGroupMEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupPEnum <em>TDomain LN Group PEnum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDomain LN Group PEnum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupPEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TDomainLNGroupPEnum"
   *        extendedMetaData="name='tDomainLNGroupPEnum:Object' baseType='tDomainLNGroupPEnum'"
   * @generated
   */
  EDataType getTDomainLNGroupPEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupREnum <em>TDomain LN Group REnum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDomain LN Group REnum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupREnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TDomainLNGroupREnum"
   *        extendedMetaData="name='tDomainLNGroupREnum:Object' baseType='tDomainLNGroupREnum'"
   * @generated
   */
  EDataType getTDomainLNGroupREnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupSEnum <em>TDomain LN Group SEnum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDomain LN Group SEnum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupSEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TDomainLNGroupSEnum"
   *        extendedMetaData="name='tDomainLNGroupSEnum:Object' baseType='tDomainLNGroupSEnum'"
   * @generated
   */
  EDataType getTDomainLNGroupSEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupTEnum <em>TDomain LN Group TEnum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDomain LN Group TEnum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupTEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TDomainLNGroupTEnum"
   *        extendedMetaData="name='tDomainLNGroupTEnum:Object' baseType='tDomainLNGroupTEnum'"
   * @generated
   */
  EDataType getTDomainLNGroupTEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupXEnum <em>TDomain LN Group XEnum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDomain LN Group XEnum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupXEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TDomainLNGroupXEnum"
   *        extendedMetaData="name='tDomainLNGroupXEnum:Object' baseType='tDomainLNGroupXEnum'"
   * @generated
   */
  EDataType getTDomainLNGroupXEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupYEnum <em>TDomain LN Group YEnum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDomain LN Group YEnum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupYEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TDomainLNGroupYEnum"
   *        extendedMetaData="name='tDomainLNGroupYEnum:Object' baseType='tDomainLNGroupYEnum'"
   * @generated
   */
  EDataType getTDomainLNGroupYEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TDomainLNGroupZEnum <em>TDomain LN Group ZEnum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TDomain LN Group ZEnum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TDomainLNGroupZEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TDomainLNGroupZEnum"
   *        extendedMetaData="name='tDomainLNGroupZEnum:Object' baseType='tDomainLNGroupZEnum'"
   * @generated
   */
  EDataType getTDomainLNGroupZEnumObject();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TEmpty</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TEmpty</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tEmpty' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' maxLength='0'"
   * @generated
   */
  EDataType getTEmpty();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TExtension Attribute Name Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TExtension Attribute Name Enum</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tExtensionAttributeNameEnum' baseType='tRestrName1stL'"
   * @generated
   */
  EDataType getTExtensionAttributeNameEnum();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TExtension Equipment Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TExtension Equipment Enum</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tExtensionEquipmentEnum' baseType='http://www.eclipse.org/emf/2003/XMLType#Name' pattern='E[A-Z]*'"
   * @generated
   */
  EDataType getTExtensionEquipmentEnum();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TExtension General Equipment Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TExtension General Equipment Enum</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tExtensionGeneralEquipmentEnum' baseType='http://www.eclipse.org/emf/2003/XMLType#Name' pattern='E[A-Z]*'"
   * @generated
   */
  EDataType getTExtensionGeneralEquipmentEnum();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TExtension LN Class Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TExtension LN Class Enum</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tExtensionLNClassEnum' baseType='http://www.eclipse.org/emf/2003/XMLType#Name' length='4' pattern='[A-Z]+'"
   * @generated
   */
  EDataType getTExtensionLNClassEnum();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TExtension Phys Conn Type Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TExtension Phys Conn Type Enum</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tExtensionPhysConnTypeEnum' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' pattern='[A-Z][0-9,A-Z,a-z,\\-]*'"
   * @generated
   */
  EDataType getTExtensionPhysConnTypeEnum();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TExtension PType Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TExtension PType Enum</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tExtensionPTypeEnum' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' pattern='[A-Z][0-9,A-Z,a-z,\\-]*'"
   * @generated
   */
  EDataType getTExtensionPTypeEnum();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TFCEnum <em>TFC Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TFC Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TFCEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TFCEnum"
   *        extendedMetaData="name='tFCEnum:Object' baseType='tFCEnum'"
   * @generated
   */
  EDataType getTFCEnumObject();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TFull Attribute Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TFull Attribute Name</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tFullAttributeName' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' pattern='[a-z,A-Z][a-z,A-Z,0-9]*(\\([0-9]+\\))?(\\.[a-z,A-Z][a-z,A-Z,0-9]*(\\([0-9]+\\))?)*'"
   * @generated
   */
  EDataType getTFullAttributeName();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TFull DO Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TFull DO Name</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tFullDOName' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' pattern='[A-Z][0-9,A-Z,a-z]{0,11}(\\.[a-z][0-9,A-Z,a-z]*(\\([0-9]+\\))?)?'"
   * @generated
   */
  EDataType getTFullDOName();

  /**
   * Returns the meta object for data type '{@link java.lang.Object <em>TGeneral Equipment Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TGeneral Equipment Enum</em>'.
   * @see java.lang.Object
   * @model instanceClass="java.lang.Object"
   *        extendedMetaData="name='tGeneralEquipmentEnum' memberTypes='tPredefinedGeneralEquipmentEnum tExtensionGeneralEquipmentEnum'"
   * @generated
   */
  EDataType getTGeneralEquipmentEnum();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TGSEControlTypeEnum <em>TGSE Control Type Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TGSE Control Type Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TGSEControlTypeEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TGSEControlTypeEnum"
   *        extendedMetaData="name='tGSEControlTypeEnum:Object' baseType='tGSEControlTypeEnum'"
   * @generated
   */
  EDataType getTGSEControlTypeEnumObject();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TIED Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TIED Name</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tIEDName' baseType='tAcsiName' maxLength='64'"
   * @generated
   */
  EDataType getTIEDName();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TLD Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TLD Inst</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tLDInst' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' maxLength='64' pattern='[A-Z,a-z,0-9][0-9,A-Z,a-z,_]*'"
   * @generated
   */
  EDataType getTLDInst();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TLD Inst Or Empty</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TLD Inst Or Empty</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tLDInstOrEmpty' memberTypes='tLDInst tEmpty'"
   * @generated
   */
  EDataType getTLDInstOrEmpty();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TLD Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TLD Name</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tLDName' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' maxLength='64' pattern='[A-Z,a-z][0-9,A-Z,a-z,_]*'"
   * @generated
   */
  EDataType getTLDName();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TLLN0Enum <em>TLLN0 Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TLLN0 Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLLN0Enum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TLLN0Enum"
   *        extendedMetaData="name='tLLN0Enum:Object' baseType='tLLN0Enum'"
   * @generated
   */
  EDataType getTLLN0EnumObject();

  /**
   * Returns the meta object for data type '{@link java.lang.Object <em>TLN Class Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TLN Class Enum</em>'.
   * @see java.lang.Object
   * @model instanceClass="java.lang.Object"
   *        extendedMetaData="name='tLNClassEnum' memberTypes='tPredefinedLNClassEnum tExtensionLNClassEnum'"
   * @generated
   */
  EDataType getTLNClassEnum();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TLN Inst</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TLN Inst</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tLNInst' baseType='tLNInstOrEmpty' minLength='1'"
   * @generated
   */
  EDataType getTLNInst();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TLN Inst Or Empty</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TLN Inst Or Empty</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tLNInstOrEmpty' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' maxLength='12' pattern='[0-9]*'"
   * @generated
   */
  EDataType getTLNInstOrEmpty();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TLog Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TLog Name</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tLogName' baseType='tAcsiName' maxLength='64'"
   * @generated
   */
  EDataType getTLogName();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TLPHDEnum <em>TLPHD Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TLPHD Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TLPHDEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TLPHDEnum"
   *        extendedMetaData="name='tLPHDEnum:Object' baseType='tLPHDEnum'"
   * @generated
   */
  EDataType getTLPHDEnumObject();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TName</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TName</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tName' baseType='tAnyName' minLength='1'"
   * @generated
   */
  EDataType getTName();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TP Addr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TP Addr</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tPAddr' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' minLength='1'"
   * @generated
   */
  EDataType getTPAddr();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TPhaseEnum <em>TPhase Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TPhase Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPhaseEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TPhaseEnum"
   *        extendedMetaData="name='tPhaseEnum:Object' baseType='tPhaseEnum'"
   * @generated
   */
  EDataType getTPhaseEnumObject();

  /**
   * Returns the meta object for data type '{@link java.lang.Object <em>TPhys Conn Type Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TPhys Conn Type Enum</em>'.
   * @see java.lang.Object
   * @model instanceClass="java.lang.Object"
   *        extendedMetaData="name='tPhysConnTypeEnum' memberTypes='tPredefinedPhysConnTypeEnum tExtensionPhysConnTypeEnum'"
   * @generated
   */
  EDataType getTPhysConnTypeEnum();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TPowerTransformerEnum <em>TPower Transformer Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TPower Transformer Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPowerTransformerEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TPowerTransformerEnum"
   *        extendedMetaData="name='tPowerTransformerEnum:Object' baseType='tPowerTransformerEnum'"
   * @generated
   */
  EDataType getTPowerTransformerEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TPredefinedAttributeNameEnum <em>TPredefined Attribute Name Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TPredefined Attribute Name Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedAttributeNameEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TPredefinedAttributeNameEnum"
   *        extendedMetaData="name='tPredefinedAttributeNameEnum:Object' baseType='tPredefinedAttributeNameEnum'"
   * @generated
   */
  EDataType getTPredefinedAttributeNameEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TPredefinedBasicTypeEnum <em>TPredefined Basic Type Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TPredefined Basic Type Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedBasicTypeEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TPredefinedBasicTypeEnum"
   *        extendedMetaData="name='tPredefinedBasicTypeEnum:Object' baseType='tPredefinedBasicTypeEnum'"
   * @generated
   */
  EDataType getTPredefinedBasicTypeEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TPredefinedCDCEnum <em>TPredefined CDC Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TPredefined CDC Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedCDCEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TPredefinedCDCEnum"
   *        extendedMetaData="name='tPredefinedCDCEnum:Object' baseType='tPredefinedCDCEnum'"
   * @generated
   */
  EDataType getTPredefinedCDCEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TPredefinedCommonConductingEquipmentEnum <em>TPredefined Common Conducting Equipment Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TPredefined Common Conducting Equipment Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedCommonConductingEquipmentEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TPredefinedCommonConductingEquipmentEnum"
   *        extendedMetaData="name='tPredefinedCommonConductingEquipmentEnum:Object' baseType='tPredefinedCommonConductingEquipmentEnum'"
   * @generated
   */
  EDataType getTPredefinedCommonConductingEquipmentEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TPredefinedGeneralEquipmentEnum <em>TPredefined General Equipment Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TPredefined General Equipment Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedGeneralEquipmentEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TPredefinedGeneralEquipmentEnum"
   *        extendedMetaData="name='tPredefinedGeneralEquipmentEnum:Object' baseType='tPredefinedGeneralEquipmentEnum'"
   * @generated
   */
  EDataType getTPredefinedGeneralEquipmentEnumObject();

  /**
   * Returns the meta object for data type '{@link org.eclipse.emf.common.util.Enumerator <em>TPredefined LN Class Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TPredefined LN Class Enum</em>'.
   * @see org.eclipse.emf.common.util.Enumerator
   * @model instanceClass="org.eclipse.emf.common.util.Enumerator"
   *        extendedMetaData="name='tPredefinedLNClassEnum' memberTypes='tLPHDEnum tLLN0Enum tDomainLNEnum'"
   * @generated
   */
  EDataType getTPredefinedLNClassEnum();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TPredefinedPhysConnTypeEnum <em>TPredefined Phys Conn Type Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TPredefined Phys Conn Type Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedPhysConnTypeEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TPredefinedPhysConnTypeEnum"
   *        extendedMetaData="name='tPredefinedPhysConnTypeEnum:Object' baseType='tPredefinedPhysConnTypeEnum'"
   * @generated
   */
  EDataType getTPredefinedPhysConnTypeEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TPredefinedPTypeEnum <em>TPredefined PType Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TPredefined PType Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedPTypeEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TPredefinedPTypeEnum"
   *        extendedMetaData="name='tPredefinedPTypeEnum:Object' baseType='tPredefinedPTypeEnum'"
   * @generated
   */
  EDataType getTPredefinedPTypeEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TPredefinedPTypePhysConnEnum <em>TPredefined PType Phys Conn Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TPredefined PType Phys Conn Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TPredefinedPTypePhysConnEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TPredefinedPTypePhysConnEnum"
   *        extendedMetaData="name='tPredefinedPTypePhysConnEnum:Object' baseType='tPredefinedPTypePhysConnEnum'"
   * @generated
   */
  EDataType getTPredefinedPTypePhysConnEnumObject();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TPrefix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TPrefix</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tPrefix' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' maxLength='11' pattern='[A-Z,a-z][0-9,A-Z,a-z,_]* '"
   * @generated
   */
  EDataType getTPrefix();

  /**
   * Returns the meta object for data type '{@link java.lang.Object <em>TP Type Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TP Type Enum</em>'.
   * @see java.lang.Object
   * @model instanceClass="java.lang.Object"
   *        extendedMetaData="name='tPTypeEnum' memberTypes='tPredefinedPTypeEnum tExtensionPTypeEnum'"
   * @generated
   */
  EDataType getTPTypeEnum();

  /**
   * Returns the meta object for data type '{@link java.lang.Object <em>TP Type Phys Conn Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TP Type Phys Conn Enum</em>'.
   * @see java.lang.Object
   * @model instanceClass="java.lang.Object"
   *        extendedMetaData="name='tPTypePhysConnEnum' memberTypes='tPredefinedPTypePhysConnEnum tExtensionPTypeEnum'"
   * @generated
   */
  EDataType getTPTypePhysConnEnum();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TRef</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TRef</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tRef' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' pattern='.+/.+/.+/.+'"
   * @generated
   */
  EDataType getTRef();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TRestr Name1st L</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TRestr Name1st L</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tRestrName1stL' baseType='http://www.eclipse.org/emf/2003/XMLType#Name' pattern='[a-z][0-9,A-Z,a-z]*'"
   * @generated
   */
  EDataType getTRestrName1stL();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TRestr Name1st U</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TRestr Name1st U</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tRestrName1stU' baseType='http://www.eclipse.org/emf/2003/XMLType#Name' pattern='[A-Z][0-9,A-Z,a-z]*'"
   * @generated
   */
  EDataType getTRestrName1stU();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TRightEnum <em>TRight Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TRight Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TRightEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TRightEnum"
   *        extendedMetaData="name='tRightEnum:Object' baseType='tRightEnum'"
   * @generated
   */
  EDataType getTRightEnumObject();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TRpt ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TRpt ID</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tRptID' baseType='tName' pattern='\\p{IsBasicLatin}+'"
   * @generated
   */
  EDataType getTRptID();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TScl Revision</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TScl Revision</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tSclRevision' baseType='http://www.eclipse.org/emf/2003/XMLType#Name' pattern='[A-Z]'"
   * @generated
   */
  EDataType getTSclRevision();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>TScl Version</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TScl Version</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='tSclVersion' baseType='tName' pattern='20[0-9]{2}'"
   * @generated
   */
  EDataType getTSclVersion();

  /**
   * Returns the meta object for data type '{@link java.lang.Object <em>TSDO Count</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TSDO Count</em>'.
   * @see java.lang.Object
   * @model instanceClass="java.lang.Object"
   *        extendedMetaData="name='tSDOCount' memberTypes='http://www.eclipse.org/emf/2003/XMLType#unsignedInt tRestrName1stL'"
   * @generated
   */
  EDataType getTSDOCount();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum <em>TService Settings Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TService Settings Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum"
   *        extendedMetaData="name='tServiceSettingsEnum:Object' baseType='tServiceSettingsEnum'"
   * @generated
   */
  EDataType getTServiceSettingsEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TServiceType <em>TService Type Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TService Type Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TServiceType
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TServiceType"
   *        extendedMetaData="name='tServiceType:Object' baseType='tServiceType'"
   * @generated
   */
  EDataType getTServiceTypeObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TSIUnitEnum <em>TSI Unit Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TSI Unit Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSIUnitEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TSIUnitEnum"
   *        extendedMetaData="name='tSIUnitEnum:Object' baseType='tSIUnitEnum'"
   * @generated
   */
  EDataType getTSIUnitEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TSmpMod <em>TSmp Mod Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TSmp Mod Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TSmpMod
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TSmpMod"
   *        extendedMetaData="name='tSmpMod:Object' baseType='tSmpMod'"
   * @generated
   */
  EDataType getTSmpModObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TTransformerWindingEnum <em>TTransformer Winding Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TTransformer Winding Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TTransformerWindingEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TTransformerWindingEnum"
   *        extendedMetaData="name='tTransformerWindingEnum:Object' baseType='tTransformerWindingEnum'"
   * @generated
   */
  EDataType getTTransformerWindingEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TUnitMultiplierEnum <em>TUnit Multiplier Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TUnit Multiplier Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TUnitMultiplierEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TUnitMultiplierEnum"
   *        extendedMetaData="name='tUnitMultiplierEnum:Object' baseType='tUnitMultiplierEnum'"
   * @generated
   */
  EDataType getTUnitMultiplierEnumObject();

  /**
   * Returns the meta object for data type '{@link com.lucy.g3.iec61850.model.scl.TValKindEnum <em>TVal Kind Enum Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>TVal Kind Enum Object</em>'.
   * @see com.lucy.g3.iec61850.model.scl.TValKindEnum
   * @model instanceClass="com.lucy.g3.iec61850.model.scl.TValKindEnum"
   *        extendedMetaData="name='tValKindEnum:Object' baseType='tValKindEnum'"
   * @generated
   */
  EDataType getTValKindEnumObject();

  /**
   * Returns the meta object for data type '{@link java.lang.String <em>Type Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Type Type</em>'.
   * @see java.lang.String
   * @model instanceClass="java.lang.String"
   *        extendedMetaData="name='type_._type' baseType='http://www.eclipse.org/emf/2003/XMLType#normalizedString' minLength='1'"
   * @generated
   */
  EDataType getTypeType();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  SCLFactory getSCLFactory();

} //SCLPackage
