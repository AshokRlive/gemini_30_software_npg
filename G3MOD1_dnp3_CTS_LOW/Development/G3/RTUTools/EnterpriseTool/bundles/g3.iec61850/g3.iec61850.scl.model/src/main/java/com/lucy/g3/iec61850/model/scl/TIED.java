/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TIED</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TIED#getServices <em>Services</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TIED#getAccessPoint <em>Access Point</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TIED#getConfigVersion <em>Config Version</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TIED#getEngRight <em>Eng Right</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TIED#getManufacturer <em>Manufacturer</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TIED#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TIED#getOriginalSclRevision <em>Original Scl Revision</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TIED#getOriginalSclVersion <em>Original Scl Version</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TIED#getOwner <em>Owner</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TIED#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIED()
 * @model extendedMetaData="name='tIED' kind='elementOnly'"
 * @generated
 */
public interface TIED extends TUnNaming {
  /**
   * Returns the value of the '<em><b>Services</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Services</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Services</em>' containment reference.
   * @see #setServices(TServices)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIED_Services()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='Services' namespace='##targetNamespace'"
   * @generated
   */
  TServices getServices();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TIED#getServices <em>Services</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Services</em>' containment reference.
   * @see #getServices()
   * @generated
   */
  void setServices(TServices value);

  /**
   * Returns the value of the '<em><b>Access Point</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TAccessPoint}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Access Point</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Access Point</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIED_AccessPoint()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='AccessPoint' namespace='##targetNamespace'"
   * @generated
   */
  EList<TAccessPoint> getAccessPoint();

  /**
   * Returns the value of the '<em><b>Config Version</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Config Version</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Config Version</em>' attribute.
   * @see #setConfigVersion(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIED_ConfigVersion()
   * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
   *        extendedMetaData="kind='attribute' name='configVersion'"
   * @generated
   */
  String getConfigVersion();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TIED#getConfigVersion <em>Config Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Config Version</em>' attribute.
   * @see #getConfigVersion()
   * @generated
   */
  void setConfigVersion(String value);

  /**
   * Returns the value of the '<em><b>Eng Right</b></em>' attribute.
   * The default value is <code>"full"</code>.
   * The literals are from the enumeration {@link com.lucy.g3.iec61850.model.scl.TRightEnum}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Eng Right</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Eng Right</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TRightEnum
   * @see #isSetEngRight()
   * @see #unsetEngRight()
   * @see #setEngRight(TRightEnum)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIED_EngRight()
   * @model default="full" unsettable="true"
   *        extendedMetaData="kind='attribute' name='engRight'"
   * @generated
   */
  TRightEnum getEngRight();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TIED#getEngRight <em>Eng Right</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Eng Right</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TRightEnum
   * @see #isSetEngRight()
   * @see #unsetEngRight()
   * @see #getEngRight()
   * @generated
   */
  void setEngRight(TRightEnum value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TIED#getEngRight <em>Eng Right</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetEngRight()
   * @see #getEngRight()
   * @see #setEngRight(TRightEnum)
   * @generated
   */
  void unsetEngRight();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TIED#getEngRight <em>Eng Right</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Eng Right</em>' attribute is set.
   * @see #unsetEngRight()
   * @see #getEngRight()
   * @see #setEngRight(TRightEnum)
   * @generated
   */
  boolean isSetEngRight();

  /**
   * Returns the value of the '<em><b>Manufacturer</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Manufacturer</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Manufacturer</em>' attribute.
   * @see #setManufacturer(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIED_Manufacturer()
   * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
   *        extendedMetaData="kind='attribute' name='manufacturer'"
   * @generated
   */
  String getManufacturer();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TIED#getManufacturer <em>Manufacturer</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Manufacturer</em>' attribute.
   * @see #getManufacturer()
   * @generated
   */
  void setManufacturer(String value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIED_Name()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TIEDName" required="true"
   *        extendedMetaData="kind='attribute' name='name'"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TIED#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Original Scl Revision</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Original Scl Revision</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Original Scl Revision</em>' attribute.
   * @see #setOriginalSclRevision(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIED_OriginalSclRevision()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TSclRevision"
   *        extendedMetaData="kind='attribute' name='originalSclRevision'"
   * @generated
   */
  String getOriginalSclRevision();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TIED#getOriginalSclRevision <em>Original Scl Revision</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Original Scl Revision</em>' attribute.
   * @see #getOriginalSclRevision()
   * @generated
   */
  void setOriginalSclRevision(String value);

  /**
   * Returns the value of the '<em><b>Original Scl Version</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Original Scl Version</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Original Scl Version</em>' attribute.
   * @see #setOriginalSclVersion(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIED_OriginalSclVersion()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TSclVersion"
   *        extendedMetaData="kind='attribute' name='originalSclVersion'"
   * @generated
   */
  String getOriginalSclVersion();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TIED#getOriginalSclVersion <em>Original Scl Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Original Scl Version</em>' attribute.
   * @see #getOriginalSclVersion()
   * @generated
   */
  void setOriginalSclVersion(String value);

  /**
   * Returns the value of the '<em><b>Owner</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Owner</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Owner</em>' attribute.
   * @see #setOwner(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIED_Owner()
   * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
   *        extendedMetaData="kind='attribute' name='owner'"
   * @generated
   */
  String getOwner();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TIED#getOwner <em>Owner</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Owner</em>' attribute.
   * @see #getOwner()
   * @generated
   */
  void setOwner(String value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIED_Type()
   * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
   *        extendedMetaData="kind='attribute' name='type'"
   * @generated
   */
  String getType();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TIED#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(String value);

} // TIED
