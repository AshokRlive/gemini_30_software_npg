/**
 */
package com.lucy.g3.iec61850.model.scl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>TPredefined CDC Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPredefinedCDCEnum()
 * @model extendedMetaData="name='tPredefinedCDCEnum'"
 * @generated
 */
public enum TPredefinedCDCEnum implements Enumerator {
  /**
   * The '<em><b>SPS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SPS_VALUE
   * @generated
   * @ordered
   */
  SPS(0, "SPS", "SPS"),

  /**
   * The '<em><b>DPS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DPS_VALUE
   * @generated
   * @ordered
   */
  DPS(1, "DPS", "DPS"),

  /**
   * The '<em><b>INS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #INS_VALUE
   * @generated
   * @ordered
   */
  INS(2, "INS", "INS"),

  /**
   * The '<em><b>ACT</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ACT_VALUE
   * @generated
   * @ordered
   */
  ACT(3, "ACT", "ACT"),

  /**
   * The '<em><b>ACD</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ACD_VALUE
   * @generated
   * @ordered
   */
  ACD(4, "ACD", "ACD"),

  /**
   * The '<em><b>SEC</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SEC_VALUE
   * @generated
   * @ordered
   */
  SEC(5, "SEC", "SEC"),

  /**
   * The '<em><b>BCR</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #BCR_VALUE
   * @generated
   * @ordered
   */
  BCR(6, "BCR", "BCR"),

  /**
   * The '<em><b>MV</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #MV_VALUE
   * @generated
   * @ordered
   */
  MV(7, "MV", "MV"),

  /**
   * The '<em><b>CMV</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CMV_VALUE
   * @generated
   * @ordered
   */
  CMV(8, "CMV", "CMV"),

  /**
   * The '<em><b>SAV</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SAV_VALUE
   * @generated
   * @ordered
   */
  SAV(9, "SAV", "SAV"),

  /**
   * The '<em><b>WYE</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #WYE_VALUE
   * @generated
   * @ordered
   */
  WYE(10, "WYE", "WYE"),

  /**
   * The '<em><b>DEL</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DEL_VALUE
   * @generated
   * @ordered
   */
  DEL(11, "DEL", "DEL"),

  /**
   * The '<em><b>SEQ</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SEQ_VALUE
   * @generated
   * @ordered
   */
  SEQ(12, "SEQ", "SEQ"),

  /**
   * The '<em><b>HMV</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #HMV_VALUE
   * @generated
   * @ordered
   */
  HMV(13, "HMV", "HMV"),

  /**
   * The '<em><b>HWYE</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #HWYE_VALUE
   * @generated
   * @ordered
   */
  HWYE(14, "HWYE", "HWYE"),

  /**
   * The '<em><b>HDEL</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #HDEL_VALUE
   * @generated
   * @ordered
   */
  HDEL(15, "HDEL", "HDEL"),

  /**
   * The '<em><b>SPC</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SPC_VALUE
   * @generated
   * @ordered
   */
  SPC(16, "SPC", "SPC"),

  /**
   * The '<em><b>DPC</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DPC_VALUE
   * @generated
   * @ordered
   */
  DPC(17, "DPC", "DPC"),

  /**
   * The '<em><b>INC</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #INC_VALUE
   * @generated
   * @ordered
   */
  INC(18, "INC", "INC"),

  /**
   * The '<em><b>BSC</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #BSC_VALUE
   * @generated
   * @ordered
   */
  BSC(19, "BSC", "BSC"),

  /**
   * The '<em><b>ISC</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ISC_VALUE
   * @generated
   * @ordered
   */
  ISC(20, "ISC", "ISC"),

  /**
   * The '<em><b>APC</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #APC_VALUE
   * @generated
   * @ordered
   */
  APC(21, "APC", "APC"),

  /**
   * The '<em><b>SPG</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SPG_VALUE
   * @generated
   * @ordered
   */
  SPG(22, "SPG", "SPG"),

  /**
   * The '<em><b>ING</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ING_VALUE
   * @generated
   * @ordered
   */
  ING(23, "ING", "ING"),

  /**
   * The '<em><b>ASG</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ASG_VALUE
   * @generated
   * @ordered
   */
  ASG(24, "ASG", "ASG"),

  /**
   * The '<em><b>CURVE</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CURVE_VALUE
   * @generated
   * @ordered
   */
  CURVE(25, "CURVE", "CURVE"),

  /**
   * The '<em><b>DPL</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DPL_VALUE
   * @generated
   * @ordered
   */
  DPL(26, "DPL", "DPL"),

  /**
   * The '<em><b>LPL</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #LPL_VALUE
   * @generated
   * @ordered
   */
  LPL(27, "LPL", "LPL"),

  /**
   * The '<em><b>CSD</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CSD_VALUE
   * @generated
   * @ordered
   */
  CSD(28, "CSD", "CSD"),

  /**
   * The '<em><b>ENS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ENS_VALUE
   * @generated
   * @ordered
   */
  ENS(29, "ENS", "ENS"),

  /**
   * The '<em><b>ENC</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ENC_VALUE
   * @generated
   * @ordered
   */
  ENC(30, "ENC", "ENC"),

  /**
   * The '<em><b>ENG</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ENG_VALUE
   * @generated
   * @ordered
   */
  ENG(31, "ENG", "ENG"),

  /**
   * The '<em><b>CTS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CTS_VALUE
   * @generated
   * @ordered
   */
  CTS(32, "CTS", "CTS"),

  /**
   * The '<em><b>UTS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #UTS_VALUE
   * @generated
   * @ordered
   */
  UTS(33, "UTS", "UTS"),

  /**
   * The '<em><b>BTS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #BTS_VALUE
   * @generated
   * @ordered
   */
  BTS(34, "BTS", "BTS"),

  /**
   * The '<em><b>LTS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #LTS_VALUE
   * @generated
   * @ordered
   */
  LTS(35, "LTS", "LTS"),

  /**
   * The '<em><b>OTS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #OTS_VALUE
   * @generated
   * @ordered
   */
  OTS(36, "OTS", "OTS"),

  /**
   * The '<em><b>GTS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #GTS_VALUE
   * @generated
   * @ordered
   */
  GTS(37, "GTS", "GTS"),

  /**
   * The '<em><b>MTS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #MTS_VALUE
   * @generated
   * @ordered
   */
  MTS(38, "MTS", "MTS"),

  /**
   * The '<em><b>NTS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #NTS_VALUE
   * @generated
   * @ordered
   */
  NTS(39, "NTS", "NTS"),

  /**
   * The '<em><b>STS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #STS_VALUE
   * @generated
   * @ordered
   */
  STS(40, "STS", "STS"),

  /**
   * The '<em><b>BAC</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #BAC_VALUE
   * @generated
   * @ordered
   */
  BAC(41, "BAC", "BAC"),

  /**
   * The '<em><b>HST</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #HST_VALUE
   * @generated
   * @ordered
   */
  HST(42, "HST", "HST"),

  /**
   * The '<em><b>ORG</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ORG_VALUE
   * @generated
   * @ordered
   */
  ORG(43, "ORG", "ORG"),

  /**
   * The '<em><b>TSG</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #TSG_VALUE
   * @generated
   * @ordered
   */
  TSG(44, "TSG", "TSG"),

  /**
   * The '<em><b>CUG</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CUG_VALUE
   * @generated
   * @ordered
   */
  CUG(45, "CUG", "CUG"),

  /**
   * The '<em><b>CSG</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CSG_VALUE
   * @generated
   * @ordered
   */
  CSG(47, "CSG", "CSG"),

  /**
   * The '<em><b>VSS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VSS_VALUE
   * @generated
   * @ordered
   */
  VSS(48, "VSS", "VSS"),

  /**
   * The '<em><b>VSG</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VSG_VALUE
   * @generated
   * @ordered
   */
  VSG(49, "VSG", "VSG"),

  /**
   * The '<em><b>CMB</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CMB_VALUE
   * @generated
   * @ordered
   */
  CMB(50, "CMB", "CMB"),

  /**
   * The '<em><b>CMMD</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CMMD_VALUE
   * @generated
   * @ordered
   */
  CMMD(51, "CMMD", "CMMD"),

  /**
   * The '<em><b>CMSV</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CMSV_VALUE
   * @generated
   * @ordered
   */
  CMSV(52, "CMSV", "CMSV"),

  /**
   * The '<em><b>SVA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SVA_VALUE
   * @generated
   * @ordered
   */
  SVA(53, "SVA", "SVA"),

  /**
   * The '<em><b>CMSVA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CMSVA_VALUE
   * @generated
   * @ordered
   */
  CMSVA(54, "CMSVA", "CMSVA"),

  /**
   * The '<em><b>CMVV</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CMVV_VALUE
   * @generated
   * @ordered
   */
  CMVV(55, "CMVV", "CMVV"),

  /**
   * The '<em><b>SCA</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SCA_VALUE
   * @generated
   * @ordered
   */
  SCA(56, "SCA", "SCA");

  /**
   * The '<em><b>SPS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SPS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SPS
   * @model
   * @generated
   * @ordered
   */
  public static final int SPS_VALUE = 0;

  /**
   * The '<em><b>DPS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DPS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DPS
   * @model
   * @generated
   * @ordered
   */
  public static final int DPS_VALUE = 1;

  /**
   * The '<em><b>INS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>INS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #INS
   * @model
   * @generated
   * @ordered
   */
  public static final int INS_VALUE = 2;

  /**
   * The '<em><b>ACT</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ACT</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ACT
   * @model
   * @generated
   * @ordered
   */
  public static final int ACT_VALUE = 3;

  /**
   * The '<em><b>ACD</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ACD</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ACD
   * @model
   * @generated
   * @ordered
   */
  public static final int ACD_VALUE = 4;

  /**
   * The '<em><b>SEC</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SEC</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SEC
   * @model
   * @generated
   * @ordered
   */
  public static final int SEC_VALUE = 5;

  /**
   * The '<em><b>BCR</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>BCR</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #BCR
   * @model
   * @generated
   * @ordered
   */
  public static final int BCR_VALUE = 6;

  /**
   * The '<em><b>MV</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>MV</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #MV
   * @model
   * @generated
   * @ordered
   */
  public static final int MV_VALUE = 7;

  /**
   * The '<em><b>CMV</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>CMV</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CMV
   * @model
   * @generated
   * @ordered
   */
  public static final int CMV_VALUE = 8;

  /**
   * The '<em><b>SAV</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SAV</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SAV
   * @model
   * @generated
   * @ordered
   */
  public static final int SAV_VALUE = 9;

  /**
   * The '<em><b>WYE</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>WYE</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #WYE
   * @model
   * @generated
   * @ordered
   */
  public static final int WYE_VALUE = 10;

  /**
   * The '<em><b>DEL</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DEL</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DEL
   * @model
   * @generated
   * @ordered
   */
  public static final int DEL_VALUE = 11;

  /**
   * The '<em><b>SEQ</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SEQ</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SEQ
   * @model
   * @generated
   * @ordered
   */
  public static final int SEQ_VALUE = 12;

  /**
   * The '<em><b>HMV</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>HMV</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #HMV
   * @model
   * @generated
   * @ordered
   */
  public static final int HMV_VALUE = 13;

  /**
   * The '<em><b>HWYE</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>HWYE</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #HWYE
   * @model
   * @generated
   * @ordered
   */
  public static final int HWYE_VALUE = 14;

  /**
   * The '<em><b>HDEL</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>HDEL</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #HDEL
   * @model
   * @generated
   * @ordered
   */
  public static final int HDEL_VALUE = 15;

  /**
   * The '<em><b>SPC</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SPC</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SPC
   * @model
   * @generated
   * @ordered
   */
  public static final int SPC_VALUE = 16;

  /**
   * The '<em><b>DPC</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DPC</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DPC
   * @model
   * @generated
   * @ordered
   */
  public static final int DPC_VALUE = 17;

  /**
   * The '<em><b>INC</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>INC</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #INC
   * @model
   * @generated
   * @ordered
   */
  public static final int INC_VALUE = 18;

  /**
   * The '<em><b>BSC</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>BSC</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #BSC
   * @model
   * @generated
   * @ordered
   */
  public static final int BSC_VALUE = 19;

  /**
   * The '<em><b>ISC</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ISC</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ISC
   * @model
   * @generated
   * @ordered
   */
  public static final int ISC_VALUE = 20;

  /**
   * The '<em><b>APC</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>APC</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #APC
   * @model
   * @generated
   * @ordered
   */
  public static final int APC_VALUE = 21;

  /**
   * The '<em><b>SPG</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SPG</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SPG
   * @model
   * @generated
   * @ordered
   */
  public static final int SPG_VALUE = 22;

  /**
   * The '<em><b>ING</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ING</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ING
   * @model
   * @generated
   * @ordered
   */
  public static final int ING_VALUE = 23;

  /**
   * The '<em><b>ASG</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ASG</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ASG
   * @model
   * @generated
   * @ordered
   */
  public static final int ASG_VALUE = 24;

  /**
   * The '<em><b>CURVE</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>CURVE</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CURVE
   * @model
   * @generated
   * @ordered
   */
  public static final int CURVE_VALUE = 25;

  /**
   * The '<em><b>DPL</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DPL</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DPL
   * @model
   * @generated
   * @ordered
   */
  public static final int DPL_VALUE = 26;

  /**
   * The '<em><b>LPL</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>LPL</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #LPL
   * @model
   * @generated
   * @ordered
   */
  public static final int LPL_VALUE = 27;

  /**
   * The '<em><b>CSD</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>CSD</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CSD
   * @model
   * @generated
   * @ordered
   */
  public static final int CSD_VALUE = 28;

  /**
   * The '<em><b>ENS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ENS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ENS
   * @model
   * @generated
   * @ordered
   */
  public static final int ENS_VALUE = 29;

  /**
   * The '<em><b>ENC</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ENC</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ENC
   * @model
   * @generated
   * @ordered
   */
  public static final int ENC_VALUE = 30;

  /**
   * The '<em><b>ENG</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ENG</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ENG
   * @model
   * @generated
   * @ordered
   */
  public static final int ENG_VALUE = 31;

  /**
   * The '<em><b>CTS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>CTS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CTS
   * @model
   * @generated
   * @ordered
   */
  public static final int CTS_VALUE = 32;

  /**
   * The '<em><b>UTS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>UTS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #UTS
   * @model
   * @generated
   * @ordered
   */
  public static final int UTS_VALUE = 33;

  /**
   * The '<em><b>BTS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>BTS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #BTS
   * @model
   * @generated
   * @ordered
   */
  public static final int BTS_VALUE = 34;

  /**
   * The '<em><b>LTS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>LTS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #LTS
   * @model
   * @generated
   * @ordered
   */
  public static final int LTS_VALUE = 35;

  /**
   * The '<em><b>OTS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>OTS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #OTS
   * @model
   * @generated
   * @ordered
   */
  public static final int OTS_VALUE = 36;

  /**
   * The '<em><b>GTS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>GTS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #GTS
   * @model
   * @generated
   * @ordered
   */
  public static final int GTS_VALUE = 37;

  /**
   * The '<em><b>MTS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>MTS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #MTS
   * @model
   * @generated
   * @ordered
   */
  public static final int MTS_VALUE = 38;

  /**
   * The '<em><b>NTS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>NTS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #NTS
   * @model
   * @generated
   * @ordered
   */
  public static final int NTS_VALUE = 39;

  /**
   * The '<em><b>STS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>STS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #STS
   * @model
   * @generated
   * @ordered
   */
  public static final int STS_VALUE = 40;

  /**
   * The '<em><b>BAC</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>BAC</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #BAC
   * @model
   * @generated
   * @ordered
   */
  public static final int BAC_VALUE = 41;

  /**
   * The '<em><b>HST</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>HST</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #HST
   * @model
   * @generated
   * @ordered
   */
  public static final int HST_VALUE = 42;

  /**
   * The '<em><b>ORG</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ORG</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ORG
   * @model
   * @generated
   * @ordered
   */
  public static final int ORG_VALUE = 43;

  /**
   * The '<em><b>TSG</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>TSG</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #TSG
   * @model
   * @generated
   * @ordered
   */
  public static final int TSG_VALUE = 44;

  /**
   * The '<em><b>CUG</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>CUG</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CUG
   * @model
   * @generated
   * @ordered
   */
  public static final int CUG_VALUE = 45;

  /**
   * The '<em><b>CSG</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>CSG</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CSG
   * @model
   * @generated
   * @ordered
   */
  public static final int CSG_VALUE = 47;

  /**
   * The '<em><b>VSS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VSS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VSS
   * @model
   * @generated
   * @ordered
   */
  public static final int VSS_VALUE = 48;

  /**
   * The '<em><b>VSG</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VSG</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VSG
   * @model
   * @generated
   * @ordered
   */
  public static final int VSG_VALUE = 49;

  /**
   * The '<em><b>CMB</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>CMB</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CMB
   * @model
   * @generated
   * @ordered
   */
  public static final int CMB_VALUE = 50;

  /**
   * The '<em><b>CMMD</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>CMMD</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CMMD
   * @model
   * @generated
   * @ordered
   */
  public static final int CMMD_VALUE = 51;

  /**
   * The '<em><b>CMSV</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>CMSV</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CMSV
   * @model
   * @generated
   * @ordered
   */
  public static final int CMSV_VALUE = 52;

  /**
   * The '<em><b>SVA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SVA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SVA
   * @model
   * @generated
   * @ordered
   */
  public static final int SVA_VALUE = 53;

  /**
   * The '<em><b>CMSVA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>CMSVA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CMSVA
   * @model
   * @generated
   * @ordered
   */
  public static final int CMSVA_VALUE = 54;

  /**
   * The '<em><b>CMVV</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>CMVV</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CMVV
   * @model
   * @generated
   * @ordered
   */
  public static final int CMVV_VALUE = 55;

  /**
   * The '<em><b>SCA</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SCA</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SCA
   * @model
   * @generated
   * @ordered
   */
  public static final int SCA_VALUE = 56;

  /**
   * An array of all the '<em><b>TPredefined CDC Enum</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final TPredefinedCDCEnum[] VALUES_ARRAY =
    new TPredefinedCDCEnum[] {
      SPS,
      DPS,
      INS,
      ACT,
      ACD,
      SEC,
      BCR,
      MV,
      CMV,
      SAV,
      WYE,
      DEL,
      SEQ,
      HMV,
      HWYE,
      HDEL,
      SPC,
      DPC,
      INC,
      BSC,
      ISC,
      APC,
      SPG,
      ING,
      ASG,
      CURVE,
      DPL,
      LPL,
      CSD,
      ENS,
      ENC,
      ENG,
      CTS,
      UTS,
      BTS,
      LTS,
      OTS,
      GTS,
      MTS,
      NTS,
      STS,
      BAC,
      HST,
      ORG,
      TSG,
      CUG,
      CSG,
      VSS,
      VSG,
      CMB,
      CMMD,
      CMSV,
      SVA,
      CMSVA,
      CMVV,
      SCA,
    };

  /**
   * A public read-only list of all the '<em><b>TPredefined CDC Enum</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<TPredefinedCDCEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>TPredefined CDC Enum</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param literal the literal.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static TPredefinedCDCEnum get(String literal) {
    for (int i = 0; i < VALUES_ARRAY.length; ++i) {
      TPredefinedCDCEnum result = VALUES_ARRAY[i];
      if (result.toString().equals(literal)) {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>TPredefined CDC Enum</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param name the name.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static TPredefinedCDCEnum getByName(String name) {
    for (int i = 0; i < VALUES_ARRAY.length; ++i) {
      TPredefinedCDCEnum result = VALUES_ARRAY[i];
      if (result.getName().equals(name)) {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>TPredefined CDC Enum</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the integer value.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static TPredefinedCDCEnum get(int value) {
    switch (value) {
      case SPS_VALUE: return SPS;
      case DPS_VALUE: return DPS;
      case INS_VALUE: return INS;
      case ACT_VALUE: return ACT;
      case ACD_VALUE: return ACD;
      case SEC_VALUE: return SEC;
      case BCR_VALUE: return BCR;
      case MV_VALUE: return MV;
      case CMV_VALUE: return CMV;
      case SAV_VALUE: return SAV;
      case WYE_VALUE: return WYE;
      case DEL_VALUE: return DEL;
      case SEQ_VALUE: return SEQ;
      case HMV_VALUE: return HMV;
      case HWYE_VALUE: return HWYE;
      case HDEL_VALUE: return HDEL;
      case SPC_VALUE: return SPC;
      case DPC_VALUE: return DPC;
      case INC_VALUE: return INC;
      case BSC_VALUE: return BSC;
      case ISC_VALUE: return ISC;
      case APC_VALUE: return APC;
      case SPG_VALUE: return SPG;
      case ING_VALUE: return ING;
      case ASG_VALUE: return ASG;
      case CURVE_VALUE: return CURVE;
      case DPL_VALUE: return DPL;
      case LPL_VALUE: return LPL;
      case CSD_VALUE: return CSD;
      case ENS_VALUE: return ENS;
      case ENC_VALUE: return ENC;
      case ENG_VALUE: return ENG;
      case CTS_VALUE: return CTS;
      case UTS_VALUE: return UTS;
      case BTS_VALUE: return BTS;
      case LTS_VALUE: return LTS;
      case OTS_VALUE: return OTS;
      case GTS_VALUE: return GTS;
      case MTS_VALUE: return MTS;
      case NTS_VALUE: return NTS;
      case STS_VALUE: return STS;
      case BAC_VALUE: return BAC;
      case HST_VALUE: return HST;
      case ORG_VALUE: return ORG;
      case TSG_VALUE: return TSG;
      case CUG_VALUE: return CUG;
      case CSG_VALUE: return CSG;
      case VSS_VALUE: return VSS;
      case VSG_VALUE: return VSG;
      case CMB_VALUE: return CMB;
      case CMMD_VALUE: return CMMD;
      case CMSV_VALUE: return CMSV;
      case SVA_VALUE: return SVA;
      case CMSVA_VALUE: return CMSVA;
      case CMVV_VALUE: return CMVV;
      case SCA_VALUE: return SCA;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private TPredefinedCDCEnum(int value, String name, String literal) {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue() {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral() {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    return literal;
  }
  
} //TPredefinedCDCEnum
