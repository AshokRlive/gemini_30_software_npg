/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPSNTP Port</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPSNTPPort()
 * @model extendedMetaData="name='tP_SNTP-Port' kind='simple'"
 * @generated
 */
public interface TPSNTPPort extends TPPort {
} // TPSNTPPort
