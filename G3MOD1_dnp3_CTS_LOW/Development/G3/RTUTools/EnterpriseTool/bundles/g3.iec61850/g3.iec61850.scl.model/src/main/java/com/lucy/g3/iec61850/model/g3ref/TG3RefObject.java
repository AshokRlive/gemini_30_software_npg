/**
 */
package com.lucy.g3.iec61850.model.g3ref;

import com.lucy.g3.iec61850.model.internal.SCLObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TG3 Ref Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.g3ref.TG3RefObject#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.g3ref.TG3RefObject#getUuid <em>Uuid</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.g3ref.G3RefPackage#getTG3RefObject()
 * @model extendedMetaData="name='tG3RefObject' kind='empty'"
 * @extends SCLObject
 * @generated
 */
public interface TG3RefObject extends SCLObject {
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(Object)
   * @see com.lucy.g3.iec61850.model.g3ref.G3RefPackage#getTG3RefObject_Name()
   * @model dataType="org.eclipse.emf.ecore.xml.type.AnySimpleType" required="true"
   *        extendedMetaData="kind='attribute' name='name'"
   * @generated
   */
  Object getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.g3ref.TG3RefObject#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(Object value);

  /**
   * Returns the value of the '<em><b>Uuid</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uuid</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uuid</em>' attribute.
   * @see #setUuid(String)
   * @see com.lucy.g3.iec61850.model.g3ref.G3RefPackage#getTG3RefObject_Uuid()
   * @model dataType="org.eclipse.emf.ecore.xml.type.String"
   *        extendedMetaData="kind='attribute' name='uuid'"
   * @generated
   */
  String getUuid();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.g3ref.TG3RefObject#getUuid <em>Uuid</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uuid</em>' attribute.
   * @see #getUuid()
   * @generated
   */
  void setUuid(String value);

} // TG3RefObject
