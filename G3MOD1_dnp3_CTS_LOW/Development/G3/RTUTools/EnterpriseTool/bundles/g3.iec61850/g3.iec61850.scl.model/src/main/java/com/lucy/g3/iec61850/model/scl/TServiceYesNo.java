/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TService Yes No</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTServiceYesNo()
 * @model extendedMetaData="name='tServiceYesNo' kind='empty'"
 * @extends SCLObject
 * @generated
 */
public interface TServiceYesNo extends SCLObject {
} // TServiceYesNo
