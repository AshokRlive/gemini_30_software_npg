/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPAPPID</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPAPPID()
 * @model extendedMetaData="name='tP_APPID' kind='simple'"
 * @generated
 */
public interface TPAPPID extends TP {
} // TPAPPID
