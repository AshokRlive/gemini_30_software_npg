/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPIPGATEWAY</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPIPGATEWAY()
 * @model extendedMetaData="name='tP_IP-GATEWAY' kind='simple'"
 * @generated
 */
public interface TPIPGATEWAY extends TP {
} // TPIPGATEWAY
