/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TServer At</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TServerAt#getApName <em>Ap Name</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTServerAt()
 * @model extendedMetaData="name='tServerAt' kind='elementOnly'"
 * @generated
 */
public interface TServerAt extends TUnNaming {
  /**
   * Returns the value of the '<em><b>Ap Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ap Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ap Name</em>' attribute.
   * @see #setApName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTServerAt_ApName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TAccessPointName" required="true"
   *        extendedMetaData="kind='attribute' name='apName'"
   * @generated
   */
  String getApName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TServerAt#getApName <em>Ap Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ap Name</em>' attribute.
   * @see #getApName()
   * @generated
   */
  void setApName(String value);

} // TServerAt
