/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TLog</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TLog#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLog()
 * @model extendedMetaData="name='tLog' kind='elementOnly'"
 * @generated
 */
public interface TLog extends TUnNaming {
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLog_Name()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLogName"
   *        extendedMetaData="kind='attribute' name='name'"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLog#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

} // TLog
