/**
 */
package com.lucy.g3.iec61850.model.g3ref.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61850.model.g3ref.util.G3RefResourceFactoryImpl
 * @generated
 */
public class G3RefResourceImpl extends XMLResourceImpl {
  /**
   * Creates an instance of the resource.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param uri the URI of the new resource.
   * @generated
   */
  public G3RefResourceImpl(URI uri) {
    super(uri);
  }

} //G3RefResourceImpl
