/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TConnectivity Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TConnectivityNode#getPathName <em>Path Name</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTConnectivityNode()
 * @model extendedMetaData="name='tConnectivityNode' kind='elementOnly'"
 * @generated
 */
public interface TConnectivityNode extends TLNodeContainer {
  /**
   * Returns the value of the '<em><b>Path Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Path Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Path Name</em>' attribute.
   * @see #setPathName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTConnectivityNode_PathName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TRef" required="true"
   *        extendedMetaData="kind='attribute' name='pathName'"
   * @generated
   */
  String getPathName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TConnectivityNode#getPathName <em>Path Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Path Name</em>' attribute.
   * @see #getPathName()
   * @generated
   */
  void setPathName(String value);

} // TConnectivityNode
