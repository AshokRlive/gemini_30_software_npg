/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TConductingEquipment;
import com.lucy.g3.iec61850.model.scl.TFunction;
import com.lucy.g3.iec61850.model.scl.TGeneralEquipment;
import com.lucy.g3.iec61850.model.scl.TSubFunction;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TFunction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TFunctionImpl#getSubFunction <em>Sub Function</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TFunctionImpl#getGeneralEquipment <em>General Equipment</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TFunctionImpl#getConductingEquipment <em>Conducting Equipment</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TFunctionImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TFunctionImpl extends TPowerSystemResourceImpl implements TFunction {
  /**
   * The cached value of the '{@link #getSubFunction() <em>Sub Function</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubFunction()
   * @generated
   * @ordered
   */
  protected EList<TSubFunction> subFunction;

  /**
   * The cached value of the '{@link #getGeneralEquipment() <em>General Equipment</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGeneralEquipment()
   * @generated
   * @ordered
   */
  protected EList<TGeneralEquipment> generalEquipment;

  /**
   * The cached value of the '{@link #getConductingEquipment() <em>Conducting Equipment</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConductingEquipment()
   * @generated
   * @ordered
   */
  protected EList<TConductingEquipment> conductingEquipment;

  /**
   * The default value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected static final String TYPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected String type = TYPE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TFunctionImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTFunction();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TSubFunction> getSubFunction() {
    if (subFunction == null) {
      subFunction = new EObjectContainmentEList<TSubFunction>(TSubFunction.class, this, SCLPackage.TFUNCTION__SUB_FUNCTION);
    }
    return subFunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TGeneralEquipment> getGeneralEquipment() {
    if (generalEquipment == null) {
      generalEquipment = new EObjectContainmentEList<TGeneralEquipment>(TGeneralEquipment.class, this, SCLPackage.TFUNCTION__GENERAL_EQUIPMENT);
    }
    return generalEquipment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TConductingEquipment> getConductingEquipment() {
    if (conductingEquipment == null) {
      conductingEquipment = new EObjectContainmentEList<TConductingEquipment>(TConductingEquipment.class, this, SCLPackage.TFUNCTION__CONDUCTING_EQUIPMENT);
    }
    return conductingEquipment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getType() {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(String newType) {
    String oldType = type;
    type = newType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TFUNCTION__TYPE, oldType, type));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case SCLPackage.TFUNCTION__SUB_FUNCTION:
        return ((InternalEList<?>)getSubFunction()).basicRemove(otherEnd, msgs);
      case SCLPackage.TFUNCTION__GENERAL_EQUIPMENT:
        return ((InternalEList<?>)getGeneralEquipment()).basicRemove(otherEnd, msgs);
      case SCLPackage.TFUNCTION__CONDUCTING_EQUIPMENT:
        return ((InternalEList<?>)getConductingEquipment()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TFUNCTION__SUB_FUNCTION:
        return getSubFunction();
      case SCLPackage.TFUNCTION__GENERAL_EQUIPMENT:
        return getGeneralEquipment();
      case SCLPackage.TFUNCTION__CONDUCTING_EQUIPMENT:
        return getConductingEquipment();
      case SCLPackage.TFUNCTION__TYPE:
        return getType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TFUNCTION__SUB_FUNCTION:
        getSubFunction().clear();
        getSubFunction().addAll((Collection<? extends TSubFunction>)newValue);
        return;
      case SCLPackage.TFUNCTION__GENERAL_EQUIPMENT:
        getGeneralEquipment().clear();
        getGeneralEquipment().addAll((Collection<? extends TGeneralEquipment>)newValue);
        return;
      case SCLPackage.TFUNCTION__CONDUCTING_EQUIPMENT:
        getConductingEquipment().clear();
        getConductingEquipment().addAll((Collection<? extends TConductingEquipment>)newValue);
        return;
      case SCLPackage.TFUNCTION__TYPE:
        setType((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TFUNCTION__SUB_FUNCTION:
        getSubFunction().clear();
        return;
      case SCLPackage.TFUNCTION__GENERAL_EQUIPMENT:
        getGeneralEquipment().clear();
        return;
      case SCLPackage.TFUNCTION__CONDUCTING_EQUIPMENT:
        getConductingEquipment().clear();
        return;
      case SCLPackage.TFUNCTION__TYPE:
        setType(TYPE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TFUNCTION__SUB_FUNCTION:
        return subFunction != null && !subFunction.isEmpty();
      case SCLPackage.TFUNCTION__GENERAL_EQUIPMENT:
        return generalEquipment != null && !generalEquipment.isEmpty();
      case SCLPackage.TFUNCTION__CONDUCTING_EQUIPMENT:
        return conductingEquipment != null && !conductingEquipment.isEmpty();
      case SCLPackage.TFUNCTION__TYPE:
        return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (type: ");
    result.append(type);
    result.append(')');
    return result.toString();
  }

} //TFunctionImpl
