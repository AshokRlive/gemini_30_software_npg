/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping.util;

import com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageRegistryImpl;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class IEC61850MappingXMLProcessor extends XMLProcessor {

  /**
   * Public constructor to instantiate the helper.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IEC61850MappingXMLProcessor() {
    super(new EPackageRegistryImpl(EPackage.Registry.INSTANCE));
    extendedMetaData.putPackage(null, IEC61850MappingPackage.eINSTANCE);
  }
  
  /**
   * Register for "*" and "xml" file extensions the IEC61850MappingResourceFactoryImpl factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected Map<String, Resource.Factory> getRegistrations() {
    if (registrations == null) {
      super.getRegistrations();
      registrations.put(XML_EXTENSION, new IEC61850MappingResourceFactoryImpl());
      registrations.put(STAR_EXTENSION, new IEC61850MappingResourceFactoryImpl());
    }
    return registrations;
  }

} //IEC61850MappingXMLProcessor
