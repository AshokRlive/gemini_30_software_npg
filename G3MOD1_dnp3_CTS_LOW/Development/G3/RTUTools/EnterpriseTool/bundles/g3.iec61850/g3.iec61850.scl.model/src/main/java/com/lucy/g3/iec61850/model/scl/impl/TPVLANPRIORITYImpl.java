/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TPVLANPRIORITY;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TPVLANPRIORITY</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TPVLANPRIORITYImpl extends TPImpl implements TPVLANPRIORITY {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TPVLANPRIORITYImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTPVLANPRIORITY();
  }

} //TPVLANPRIORITYImpl
