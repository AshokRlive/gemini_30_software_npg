/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TTerminal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TTerminal#getBayName <em>Bay Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TTerminal#getCNodeName <em>CNode Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TTerminal#getConnectivityNode <em>Connectivity Node</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TTerminal#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TTerminal#isNeutralPoint <em>Neutral Point</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TTerminal#getSubstationName <em>Substation Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TTerminal#getVoltageLevelName <em>Voltage Level Name</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTerminal()
 * @model extendedMetaData="name='tTerminal' kind='elementOnly'"
 * @generated
 */
public interface TTerminal extends TUnNaming {
  /**
   * Returns the value of the '<em><b>Bay Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bay Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bay Name</em>' attribute.
   * @see #setBayName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTerminal_BayName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TName" required="true"
   *        extendedMetaData="kind='attribute' name='bayName'"
   * @generated
   */
  String getBayName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getBayName <em>Bay Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bay Name</em>' attribute.
   * @see #getBayName()
   * @generated
   */
  void setBayName(String value);

  /**
   * Returns the value of the '<em><b>CNode Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>CNode Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>CNode Name</em>' attribute.
   * @see #setCNodeName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTerminal_CNodeName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TName" required="true"
   *        extendedMetaData="kind='attribute' name='cNodeName'"
   * @generated
   */
  String getCNodeName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getCNodeName <em>CNode Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>CNode Name</em>' attribute.
   * @see #getCNodeName()
   * @generated
   */
  void setCNodeName(String value);

  /**
   * Returns the value of the '<em><b>Connectivity Node</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Connectivity Node</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Connectivity Node</em>' attribute.
   * @see #setConnectivityNode(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTerminal_ConnectivityNode()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TRef" required="true"
   *        extendedMetaData="kind='attribute' name='connectivityNode'"
   * @generated
   */
  String getConnectivityNode();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getConnectivityNode <em>Connectivity Node</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Connectivity Node</em>' attribute.
   * @see #getConnectivityNode()
   * @generated
   */
  void setConnectivityNode(String value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * The default value is <code>""</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #isSetName()
   * @see #unsetName()
   * @see #setName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTerminal_Name()
   * @model default="" unsettable="true" dataType="com.lucy.g3.iec61850.model.scl.TAnyName"
   *        extendedMetaData="kind='attribute' name='name'"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #isSetName()
   * @see #unsetName()
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetName()
   * @see #getName()
   * @see #setName(String)
   * @generated
   */
  void unsetName();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getName <em>Name</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Name</em>' attribute is set.
   * @see #unsetName()
   * @see #getName()
   * @see #setName(String)
   * @generated
   */
  boolean isSetName();

  /**
   * Returns the value of the '<em><b>Neutral Point</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Neutral Point</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Neutral Point</em>' attribute.
   * @see #isSetNeutralPoint()
   * @see #unsetNeutralPoint()
   * @see #setNeutralPoint(boolean)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTerminal_NeutralPoint()
   * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   *        extendedMetaData="kind='attribute' name='neutralPoint'"
   * @generated
   */
  boolean isNeutralPoint();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTerminal#isNeutralPoint <em>Neutral Point</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Neutral Point</em>' attribute.
   * @see #isSetNeutralPoint()
   * @see #unsetNeutralPoint()
   * @see #isNeutralPoint()
   * @generated
   */
  void setNeutralPoint(boolean value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTerminal#isNeutralPoint <em>Neutral Point</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetNeutralPoint()
   * @see #isNeutralPoint()
   * @see #setNeutralPoint(boolean)
   * @generated
   */
  void unsetNeutralPoint();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TTerminal#isNeutralPoint <em>Neutral Point</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Neutral Point</em>' attribute is set.
   * @see #unsetNeutralPoint()
   * @see #isNeutralPoint()
   * @see #setNeutralPoint(boolean)
   * @generated
   */
  boolean isSetNeutralPoint();

  /**
   * Returns the value of the '<em><b>Substation Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Substation Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Substation Name</em>' attribute.
   * @see #setSubstationName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTerminal_SubstationName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TName" required="true"
   *        extendedMetaData="kind='attribute' name='substationName'"
   * @generated
   */
  String getSubstationName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getSubstationName <em>Substation Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Substation Name</em>' attribute.
   * @see #getSubstationName()
   * @generated
   */
  void setSubstationName(String value);

  /**
   * Returns the value of the '<em><b>Voltage Level Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Voltage Level Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Voltage Level Name</em>' attribute.
   * @see #setVoltageLevelName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTerminal_VoltageLevelName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TName" required="true"
   *        extendedMetaData="kind='attribute' name='voltageLevelName'"
   * @generated
   */
  String getVoltageLevelName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTerminal#getVoltageLevelName <em>Voltage Level Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Voltage Level Name</em>' attribute.
   * @see #getVoltageLevelName()
   * @generated
   */
  void setVoltageLevelName(String value);

} // TTerminal
