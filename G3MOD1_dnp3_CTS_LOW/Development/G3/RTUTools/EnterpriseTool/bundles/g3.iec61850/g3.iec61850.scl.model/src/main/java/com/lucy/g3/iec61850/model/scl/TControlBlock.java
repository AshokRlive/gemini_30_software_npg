/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TControl Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TControlBlock#getAddress <em>Address</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TControlBlock#getCbName <em>Cb Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TControlBlock#getLdInst <em>Ld Inst</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTControlBlock()
 * @model abstract="true"
 *        extendedMetaData="name='tControlBlock' kind='elementOnly'"
 * @generated
 */
public interface TControlBlock extends TUnNaming {
  /**
   * Returns the value of the '<em><b>Address</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Address</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Address</em>' containment reference.
   * @see #setAddress(TAddress)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTControlBlock_Address()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='Address' namespace='##targetNamespace'"
   * @generated
   */
  TAddress getAddress();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TControlBlock#getAddress <em>Address</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Address</em>' containment reference.
   * @see #getAddress()
   * @generated
   */
  void setAddress(TAddress value);

  /**
   * Returns the value of the '<em><b>Cb Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cb Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cb Name</em>' attribute.
   * @see #setCbName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTControlBlock_CbName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TCBName" required="true"
   *        extendedMetaData="kind='attribute' name='cbName'"
   * @generated
   */
  String getCbName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TControlBlock#getCbName <em>Cb Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Cb Name</em>' attribute.
   * @see #getCbName()
   * @generated
   */
  void setCbName(String value);

  /**
   * Returns the value of the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ld Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ld Inst</em>' attribute.
   * @see #setLdInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTControlBlock_LdInst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLDInst" required="true"
   *        extendedMetaData="kind='attribute' name='ldInst'"
   * @generated
   */
  String getLdInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TControlBlock#getLdInst <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ld Inst</em>' attribute.
   * @see #getLdInst()
   * @generated
   */
  void setLdInst(String value);

} // TControlBlock
