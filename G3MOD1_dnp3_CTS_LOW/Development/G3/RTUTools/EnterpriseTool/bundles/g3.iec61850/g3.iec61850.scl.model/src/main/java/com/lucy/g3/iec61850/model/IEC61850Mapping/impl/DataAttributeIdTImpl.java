/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping.impl;

import com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage;

import com.lucy.g3.iec61850.model.internal.SCLObjectImpl;

import java.math.BigInteger;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Attribute Id T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DataAttributeIdTImpl#getField0 <em>Field0</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DataAttributeIdTImpl#getField1 <em>Field1</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DataAttributeIdTImpl#getField2 <em>Field2</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DataAttributeIdTImpl#getField3 <em>Field3</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DataAttributeIdTImpl#getField4 <em>Field4</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DataAttributeIdTImpl#getField5 <em>Field5</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAttributeIdTImpl extends SCLObjectImpl implements DataAttributeIdT {
  /**
   * The default value of the '{@link #getField0() <em>Field0</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField0()
   * @generated
   * @ordered
   */
  protected static final BigInteger FIELD0_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getField0() <em>Field0</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField0()
   * @generated
   * @ordered
   */
  protected BigInteger field0 = FIELD0_EDEFAULT;

  /**
   * The default value of the '{@link #getField1() <em>Field1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField1()
   * @generated
   * @ordered
   */
  protected static final BigInteger FIELD1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getField1() <em>Field1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField1()
   * @generated
   * @ordered
   */
  protected BigInteger field1 = FIELD1_EDEFAULT;

  /**
   * The default value of the '{@link #getField2() <em>Field2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField2()
   * @generated
   * @ordered
   */
  protected static final BigInteger FIELD2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getField2() <em>Field2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField2()
   * @generated
   * @ordered
   */
  protected BigInteger field2 = FIELD2_EDEFAULT;

  /**
   * The default value of the '{@link #getField3() <em>Field3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField3()
   * @generated
   * @ordered
   */
  protected static final BigInteger FIELD3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getField3() <em>Field3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField3()
   * @generated
   * @ordered
   */
  protected BigInteger field3 = FIELD3_EDEFAULT;

  /**
   * The default value of the '{@link #getField4() <em>Field4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField4()
   * @generated
   * @ordered
   */
  protected static final BigInteger FIELD4_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getField4() <em>Field4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField4()
   * @generated
   * @ordered
   */
  protected BigInteger field4 = FIELD4_EDEFAULT;

  /**
   * The default value of the '{@link #getField5() <em>Field5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField5()
   * @generated
   * @ordered
   */
  protected static final BigInteger FIELD5_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getField5() <em>Field5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField5()
   * @generated
   * @ordered
   */
  protected BigInteger field5 = FIELD5_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DataAttributeIdTImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return IEC61850MappingPackage.Literals.DATA_ATTRIBUTE_ID_T;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getField0() {
    return field0;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField0(BigInteger newField0) {
    BigInteger oldField0 = field0;
    field0 = newField0;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD0, oldField0, field0));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getField1() {
    return field1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField1(BigInteger newField1) {
    BigInteger oldField1 = field1;
    field1 = newField1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD1, oldField1, field1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getField2() {
    return field2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField2(BigInteger newField2) {
    BigInteger oldField2 = field2;
    field2 = newField2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD2, oldField2, field2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getField3() {
    return field3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField3(BigInteger newField3) {
    BigInteger oldField3 = field3;
    field3 = newField3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD3, oldField3, field3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getField4() {
    return field4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField4(BigInteger newField4) {
    BigInteger oldField4 = field4;
    field4 = newField4;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD4, oldField4, field4));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getField5() {
    return field5;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setField5(BigInteger newField5) {
    BigInteger oldField5 = field5;
    field5 = newField5;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD5, oldField5, field5));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD0:
        return getField0();
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD1:
        return getField1();
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD2:
        return getField2();
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD3:
        return getField3();
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD4:
        return getField4();
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD5:
        return getField5();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD0:
        setField0((BigInteger)newValue);
        return;
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD1:
        setField1((BigInteger)newValue);
        return;
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD2:
        setField2((BigInteger)newValue);
        return;
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD3:
        setField3((BigInteger)newValue);
        return;
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD4:
        setField4((BigInteger)newValue);
        return;
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD5:
        setField5((BigInteger)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD0:
        setField0(FIELD0_EDEFAULT);
        return;
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD1:
        setField1(FIELD1_EDEFAULT);
        return;
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD2:
        setField2(FIELD2_EDEFAULT);
        return;
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD3:
        setField3(FIELD3_EDEFAULT);
        return;
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD4:
        setField4(FIELD4_EDEFAULT);
        return;
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD5:
        setField5(FIELD5_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD0:
        return FIELD0_EDEFAULT == null ? field0 != null : !FIELD0_EDEFAULT.equals(field0);
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD1:
        return FIELD1_EDEFAULT == null ? field1 != null : !FIELD1_EDEFAULT.equals(field1);
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD2:
        return FIELD2_EDEFAULT == null ? field2 != null : !FIELD2_EDEFAULT.equals(field2);
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD3:
        return FIELD3_EDEFAULT == null ? field3 != null : !FIELD3_EDEFAULT.equals(field3);
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD4:
        return FIELD4_EDEFAULT == null ? field4 != null : !FIELD4_EDEFAULT.equals(field4);
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD5:
        return FIELD5_EDEFAULT == null ? field5 != null : !FIELD5_EDEFAULT.equals(field5);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (field0: ");
    result.append(field0);
    result.append(", field1: ");
    result.append(field1);
    result.append(", field2: ");
    result.append(field2);
    result.append(", field3: ");
    result.append(field3);
    result.append(", field4: ");
    result.append(field4);
    result.append(", field5: ");
    result.append(field5);
    result.append(')');
    return result.toString();
  }

} //DataAttributeIdTImpl
