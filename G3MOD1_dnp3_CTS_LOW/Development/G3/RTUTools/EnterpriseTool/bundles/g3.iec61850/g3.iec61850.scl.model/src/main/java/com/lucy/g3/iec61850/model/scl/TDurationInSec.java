/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TDuration In Sec</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTDurationInSec()
 * @model extendedMetaData="name='tDurationInSec' kind='simple'"
 * @generated
 */
public interface TDurationInSec extends TValueWithUnit {
} // TDurationInSec
