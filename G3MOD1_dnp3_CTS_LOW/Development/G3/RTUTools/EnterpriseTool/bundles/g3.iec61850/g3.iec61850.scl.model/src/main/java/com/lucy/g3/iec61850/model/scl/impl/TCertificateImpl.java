/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TCert;
import com.lucy.g3.iec61850.model.scl.TCertificate;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TCertificate</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TCertificateImpl#getSubject <em>Subject</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TCertificateImpl#getIssuerName <em>Issuer Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TCertificateImpl#getSerialNumber <em>Serial Number</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TCertificateImpl#getXferNumber <em>Xfer Number</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TCertificateImpl extends TNamingImpl implements TCertificate {
  /**
   * The cached value of the '{@link #getSubject() <em>Subject</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubject()
   * @generated
   * @ordered
   */
  protected TCert subject;

  /**
   * The cached value of the '{@link #getIssuerName() <em>Issuer Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIssuerName()
   * @generated
   * @ordered
   */
  protected TCert issuerName;

  /**
   * The default value of the '{@link #getSerialNumber() <em>Serial Number</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSerialNumber()
   * @generated
   * @ordered
   */
  protected static final String SERIAL_NUMBER_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSerialNumber() <em>Serial Number</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSerialNumber()
   * @generated
   * @ordered
   */
  protected String serialNumber = SERIAL_NUMBER_EDEFAULT;

  /**
   * The default value of the '{@link #getXferNumber() <em>Xfer Number</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getXferNumber()
   * @generated
   * @ordered
   */
  protected static final long XFER_NUMBER_EDEFAULT = 0L;

  /**
   * The cached value of the '{@link #getXferNumber() <em>Xfer Number</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getXferNumber()
   * @generated
   * @ordered
   */
  protected long xferNumber = XFER_NUMBER_EDEFAULT;

  /**
   * This is true if the Xfer Number attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean xferNumberESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TCertificateImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTCertificate();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TCert getSubject() {
    return subject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSubject(TCert newSubject, NotificationChain msgs) {
    TCert oldSubject = subject;
    subject = newSubject;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TCERTIFICATE__SUBJECT, oldSubject, newSubject);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSubject(TCert newSubject) {
    if (newSubject != subject) {
      NotificationChain msgs = null;
      if (subject != null)
        msgs = ((InternalEObject)subject).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TCERTIFICATE__SUBJECT, null, msgs);
      if (newSubject != null)
        msgs = ((InternalEObject)newSubject).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TCERTIFICATE__SUBJECT, null, msgs);
      msgs = basicSetSubject(newSubject, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TCERTIFICATE__SUBJECT, newSubject, newSubject));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TCert getIssuerName() {
    return issuerName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIssuerName(TCert newIssuerName, NotificationChain msgs) {
    TCert oldIssuerName = issuerName;
    issuerName = newIssuerName;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TCERTIFICATE__ISSUER_NAME, oldIssuerName, newIssuerName);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIssuerName(TCert newIssuerName) {
    if (newIssuerName != issuerName) {
      NotificationChain msgs = null;
      if (issuerName != null)
        msgs = ((InternalEObject)issuerName).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TCERTIFICATE__ISSUER_NAME, null, msgs);
      if (newIssuerName != null)
        msgs = ((InternalEObject)newIssuerName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TCERTIFICATE__ISSUER_NAME, null, msgs);
      msgs = basicSetIssuerName(newIssuerName, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TCERTIFICATE__ISSUER_NAME, newIssuerName, newIssuerName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSerialNumber() {
    return serialNumber;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSerialNumber(String newSerialNumber) {
    String oldSerialNumber = serialNumber;
    serialNumber = newSerialNumber;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TCERTIFICATE__SERIAL_NUMBER, oldSerialNumber, serialNumber));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public long getXferNumber() {
    return xferNumber;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setXferNumber(long newXferNumber) {
    long oldXferNumber = xferNumber;
    xferNumber = newXferNumber;
    boolean oldXferNumberESet = xferNumberESet;
    xferNumberESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TCERTIFICATE__XFER_NUMBER, oldXferNumber, xferNumber, !oldXferNumberESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetXferNumber() {
    long oldXferNumber = xferNumber;
    boolean oldXferNumberESet = xferNumberESet;
    xferNumber = XFER_NUMBER_EDEFAULT;
    xferNumberESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TCERTIFICATE__XFER_NUMBER, oldXferNumber, XFER_NUMBER_EDEFAULT, oldXferNumberESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetXferNumber() {
    return xferNumberESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case SCLPackage.TCERTIFICATE__SUBJECT:
        return basicSetSubject(null, msgs);
      case SCLPackage.TCERTIFICATE__ISSUER_NAME:
        return basicSetIssuerName(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TCERTIFICATE__SUBJECT:
        return getSubject();
      case SCLPackage.TCERTIFICATE__ISSUER_NAME:
        return getIssuerName();
      case SCLPackage.TCERTIFICATE__SERIAL_NUMBER:
        return getSerialNumber();
      case SCLPackage.TCERTIFICATE__XFER_NUMBER:
        return getXferNumber();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TCERTIFICATE__SUBJECT:
        setSubject((TCert)newValue);
        return;
      case SCLPackage.TCERTIFICATE__ISSUER_NAME:
        setIssuerName((TCert)newValue);
        return;
      case SCLPackage.TCERTIFICATE__SERIAL_NUMBER:
        setSerialNumber((String)newValue);
        return;
      case SCLPackage.TCERTIFICATE__XFER_NUMBER:
        setXferNumber((Long)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TCERTIFICATE__SUBJECT:
        setSubject((TCert)null);
        return;
      case SCLPackage.TCERTIFICATE__ISSUER_NAME:
        setIssuerName((TCert)null);
        return;
      case SCLPackage.TCERTIFICATE__SERIAL_NUMBER:
        setSerialNumber(SERIAL_NUMBER_EDEFAULT);
        return;
      case SCLPackage.TCERTIFICATE__XFER_NUMBER:
        unsetXferNumber();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TCERTIFICATE__SUBJECT:
        return subject != null;
      case SCLPackage.TCERTIFICATE__ISSUER_NAME:
        return issuerName != null;
      case SCLPackage.TCERTIFICATE__SERIAL_NUMBER:
        return SERIAL_NUMBER_EDEFAULT == null ? serialNumber != null : !SERIAL_NUMBER_EDEFAULT.equals(serialNumber);
      case SCLPackage.TCERTIFICATE__XFER_NUMBER:
        return isSetXferNumber();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (serialNumber: ");
    result.append(serialNumber);
    result.append(", xferNumber: ");
    if (xferNumberESet) result.append(xferNumber); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TCertificateImpl
