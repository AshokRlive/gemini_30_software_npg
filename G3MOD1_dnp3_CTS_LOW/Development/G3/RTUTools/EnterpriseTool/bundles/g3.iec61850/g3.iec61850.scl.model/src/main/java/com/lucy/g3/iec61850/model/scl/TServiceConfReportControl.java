/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TService Conf Report Control</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TServiceConfReportControl#isBufConf <em>Buf Conf</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TServiceConfReportControl#getBufMode <em>Buf Mode</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTServiceConfReportControl()
 * @model extendedMetaData="name='tServiceConfReportControl' kind='empty'"
 * @generated
 */
public interface TServiceConfReportControl extends TServiceWithMax {
  /**
   * Returns the value of the '<em><b>Buf Conf</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Buf Conf</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Buf Conf</em>' attribute.
   * @see #isSetBufConf()
   * @see #unsetBufConf()
   * @see #setBufConf(boolean)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTServiceConfReportControl_BufConf()
   * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   *        extendedMetaData="kind='attribute' name='bufConf'"
   * @generated
   */
  boolean isBufConf();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TServiceConfReportControl#isBufConf <em>Buf Conf</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Buf Conf</em>' attribute.
   * @see #isSetBufConf()
   * @see #unsetBufConf()
   * @see #isBufConf()
   * @generated
   */
  void setBufConf(boolean value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TServiceConfReportControl#isBufConf <em>Buf Conf</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetBufConf()
   * @see #isBufConf()
   * @see #setBufConf(boolean)
   * @generated
   */
  void unsetBufConf();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TServiceConfReportControl#isBufConf <em>Buf Conf</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Buf Conf</em>' attribute is set.
   * @see #unsetBufConf()
   * @see #isBufConf()
   * @see #setBufConf(boolean)
   * @generated
   */
  boolean isSetBufConf();

  /**
   * Returns the value of the '<em><b>Buf Mode</b></em>' attribute.
   * The literals are from the enumeration {@link com.lucy.g3.iec61850.model.scl.BufModeType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Buf Mode</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Buf Mode</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.BufModeType
   * @see #isSetBufMode()
   * @see #unsetBufMode()
   * @see #setBufMode(BufModeType)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTServiceConfReportControl_BufMode()
   * @model unsettable="true"
   *        extendedMetaData="kind='attribute' name='bufMode'"
   * @generated
   */
  BufModeType getBufMode();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TServiceConfReportControl#getBufMode <em>Buf Mode</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Buf Mode</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.BufModeType
   * @see #isSetBufMode()
   * @see #unsetBufMode()
   * @see #getBufMode()
   * @generated
   */
  void setBufMode(BufModeType value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TServiceConfReportControl#getBufMode <em>Buf Mode</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetBufMode()
   * @see #getBufMode()
   * @see #setBufMode(BufModeType)
   * @generated
   */
  void unsetBufMode();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TServiceConfReportControl#getBufMode <em>Buf Mode</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Buf Mode</em>' attribute is set.
   * @see #unsetBufMode()
   * @see #getBufMode()
   * @see #setBufMode(BufModeType)
   * @generated
   */
  boolean isSetBufMode();

} // TServiceConfReportControl
