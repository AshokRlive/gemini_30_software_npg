/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping.impl;

import com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage;
import com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT;

import com.lucy.g3.iec61850.model.internal.SCLObjectImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mapping Root T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.MappingRootTImpl#getEntry <em>Entry</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.MappingRootTImpl#getSchemaVersionMajor <em>Schema Version Major</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.MappingRootTImpl#getSchemaVersionMinor <em>Schema Version Minor</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MappingRootTImpl extends SCLObjectImpl implements MappingRootT {
  /**
   * The cached value of the '{@link #getEntry() <em>Entry</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEntry()
   * @generated
   * @ordered
   */
  protected EList<EntryT> entry;

  /**
   * The default value of the '{@link #getSchemaVersionMajor() <em>Schema Version Major</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSchemaVersionMajor()
   * @generated
   * @ordered
   */
  protected static final short SCHEMA_VERSION_MAJOR_EDEFAULT = 1;

  /**
   * The cached value of the '{@link #getSchemaVersionMajor() <em>Schema Version Major</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSchemaVersionMajor()
   * @generated
   * @ordered
   */
  protected short schemaVersionMajor = SCHEMA_VERSION_MAJOR_EDEFAULT;

  /**
   * This is true if the Schema Version Major attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean schemaVersionMajorESet;

  /**
   * The default value of the '{@link #getSchemaVersionMinor() <em>Schema Version Minor</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSchemaVersionMinor()
   * @generated
   * @ordered
   */
  protected static final short SCHEMA_VERSION_MINOR_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getSchemaVersionMinor() <em>Schema Version Minor</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSchemaVersionMinor()
   * @generated
   * @ordered
   */
  protected short schemaVersionMinor = SCHEMA_VERSION_MINOR_EDEFAULT;

  /**
   * This is true if the Schema Version Minor attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean schemaVersionMinorESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MappingRootTImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return IEC61850MappingPackage.Literals.MAPPING_ROOT_T;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<EntryT> getEntry() {
    if (entry == null) {
      entry = new EObjectContainmentEList<EntryT>(EntryT.class, this, IEC61850MappingPackage.MAPPING_ROOT_T__ENTRY);
    }
    return entry;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public short getSchemaVersionMajor() {
    return schemaVersionMajor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSchemaVersionMajor(short newSchemaVersionMajor) {
    short oldSchemaVersionMajor = schemaVersionMajor;
    schemaVersionMajor = newSchemaVersionMajor;
    boolean oldSchemaVersionMajorESet = schemaVersionMajorESet;
    schemaVersionMajorESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.MAPPING_ROOT_T__SCHEMA_VERSION_MAJOR, oldSchemaVersionMajor, schemaVersionMajor, !oldSchemaVersionMajorESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetSchemaVersionMajor() {
    short oldSchemaVersionMajor = schemaVersionMajor;
    boolean oldSchemaVersionMajorESet = schemaVersionMajorESet;
    schemaVersionMajor = SCHEMA_VERSION_MAJOR_EDEFAULT;
    schemaVersionMajorESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, IEC61850MappingPackage.MAPPING_ROOT_T__SCHEMA_VERSION_MAJOR, oldSchemaVersionMajor, SCHEMA_VERSION_MAJOR_EDEFAULT, oldSchemaVersionMajorESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetSchemaVersionMajor() {
    return schemaVersionMajorESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public short getSchemaVersionMinor() {
    return schemaVersionMinor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSchemaVersionMinor(short newSchemaVersionMinor) {
    short oldSchemaVersionMinor = schemaVersionMinor;
    schemaVersionMinor = newSchemaVersionMinor;
    boolean oldSchemaVersionMinorESet = schemaVersionMinorESet;
    schemaVersionMinorESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.MAPPING_ROOT_T__SCHEMA_VERSION_MINOR, oldSchemaVersionMinor, schemaVersionMinor, !oldSchemaVersionMinorESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetSchemaVersionMinor() {
    short oldSchemaVersionMinor = schemaVersionMinor;
    boolean oldSchemaVersionMinorESet = schemaVersionMinorESet;
    schemaVersionMinor = SCHEMA_VERSION_MINOR_EDEFAULT;
    schemaVersionMinorESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, IEC61850MappingPackage.MAPPING_ROOT_T__SCHEMA_VERSION_MINOR, oldSchemaVersionMinor, SCHEMA_VERSION_MINOR_EDEFAULT, oldSchemaVersionMinorESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetSchemaVersionMinor() {
    return schemaVersionMinorESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case IEC61850MappingPackage.MAPPING_ROOT_T__ENTRY:
        return ((InternalEList<?>)getEntry()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case IEC61850MappingPackage.MAPPING_ROOT_T__ENTRY:
        return getEntry();
      case IEC61850MappingPackage.MAPPING_ROOT_T__SCHEMA_VERSION_MAJOR:
        return getSchemaVersionMajor();
      case IEC61850MappingPackage.MAPPING_ROOT_T__SCHEMA_VERSION_MINOR:
        return getSchemaVersionMinor();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case IEC61850MappingPackage.MAPPING_ROOT_T__ENTRY:
        getEntry().clear();
        getEntry().addAll((Collection<? extends EntryT>)newValue);
        return;
      case IEC61850MappingPackage.MAPPING_ROOT_T__SCHEMA_VERSION_MAJOR:
        setSchemaVersionMajor((Short)newValue);
        return;
      case IEC61850MappingPackage.MAPPING_ROOT_T__SCHEMA_VERSION_MINOR:
        setSchemaVersionMinor((Short)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case IEC61850MappingPackage.MAPPING_ROOT_T__ENTRY:
        getEntry().clear();
        return;
      case IEC61850MappingPackage.MAPPING_ROOT_T__SCHEMA_VERSION_MAJOR:
        unsetSchemaVersionMajor();
        return;
      case IEC61850MappingPackage.MAPPING_ROOT_T__SCHEMA_VERSION_MINOR:
        unsetSchemaVersionMinor();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case IEC61850MappingPackage.MAPPING_ROOT_T__ENTRY:
        return entry != null && !entry.isEmpty();
      case IEC61850MappingPackage.MAPPING_ROOT_T__SCHEMA_VERSION_MAJOR:
        return isSetSchemaVersionMajor();
      case IEC61850MappingPackage.MAPPING_ROOT_T__SCHEMA_VERSION_MINOR:
        return isSetSchemaVersionMinor();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (schemaVersionMajor: ");
    if (schemaVersionMajorESet) result.append(schemaVersionMajor); else result.append("<unset>");
    result.append(", schemaVersionMinor: ");
    if (schemaVersionMinorESet) result.append(schemaVersionMinor); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //MappingRootTImpl
