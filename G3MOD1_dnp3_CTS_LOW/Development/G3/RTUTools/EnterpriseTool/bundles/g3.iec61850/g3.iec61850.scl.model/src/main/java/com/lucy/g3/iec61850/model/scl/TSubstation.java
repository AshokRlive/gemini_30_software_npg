/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TSubstation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSubstation#getVoltageLevel <em>Voltage Level</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSubstation#getFunction <em>Function</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSubstation()
 * @model extendedMetaData="name='tSubstation' kind='elementOnly'"
 * @generated
 */
public interface TSubstation extends TEquipmentContainer {
  /**
   * Returns the value of the '<em><b>Voltage Level</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TVoltageLevel}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Voltage Level</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Voltage Level</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSubstation_VoltageLevel()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='VoltageLevel' namespace='##targetNamespace'"
   * @generated
   */
  EList<TVoltageLevel> getVoltageLevel();

  /**
   * Returns the value of the '<em><b>Function</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TFunction}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Function</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Function</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSubstation_Function()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='Function' namespace='##targetNamespace'"
   * @generated
   */
  EList<TFunction> getFunction();

} // TSubstation
