/**
 *
 * $Id$
 */
package com.lucy.g3.iec61850.model.g3ref.validation;

import com.lucy.g3.iec61850.model.g3ref.TG3RefObject;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * A sample validator interface for {@link com.lucy.g3.iec61850.model.g3ref.DocumentRoot}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface DocumentRootValidator {
  boolean validate();

  boolean validateMixed(FeatureMap value);
  boolean validateXMLNSPrefixMap(EMap<String, String> value);
  boolean validateXSISchemaLocation(EMap<String, String> value);
  boolean validateG3RefObject(TG3RefObject value);
}
