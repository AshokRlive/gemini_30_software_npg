/**
 */
package com.lucy.g3.iec61850.model.systemcorp;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * 
 * 			COPYRIGHT SystemCORP Pty Ltd, 2009. Version 1.0.Release
 * 		
 * <!-- end-model-doc -->
 * @see com.lucy.g3.iec61850.model.systemcorp.SystemCorpFactory
 * @model kind="package"
 * @generated
 */
public interface SystemCorpPackage extends EPackage {
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "systemcorp";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.systemcorp.com.au/61850/SCL/Generic";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "SystemCorp_Generic";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  SystemCorpPackage eINSTANCE = com.lucy.g3.iec61850.model.systemcorp.impl.SystemCorpPackageImpl.init();

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.systemcorp.impl.DocumentRootImpl <em>Document Root</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.systemcorp.impl.DocumentRootImpl
   * @see com.lucy.g3.iec61850.model.systemcorp.impl.SystemCorpPackageImpl#getDocumentRoot()
   * @generated
   */
  int DOCUMENT_ROOT = 0;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__MIXED = 0;

  /**
   * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

  /**
   * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

  /**
   * The feature id for the '<em><b>Generic Private Object</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__GENERIC_PRIVATE_OBJECT = 3;

  /**
   * The number of structural features of the '<em>Document Root</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT_FEATURE_COUNT = 4;

  /**
   * The number of operations of the '<em>Document Root</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.systemcorp.impl.TGenericPrivateObjectImpl <em>TGeneric Private Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.systemcorp.impl.TGenericPrivateObjectImpl
   * @see com.lucy.g3.iec61850.model.systemcorp.impl.SystemCorpPackageImpl#getTGenericPrivateObject()
   * @generated
   */
  int TGENERIC_PRIVATE_OBJECT = 1;

  /**
   * The feature id for the '<em><b>Field1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERIC_PRIVATE_OBJECT__FIELD1 = 0;

  /**
   * The feature id for the '<em><b>Field2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERIC_PRIVATE_OBJECT__FIELD2 = 1;

  /**
   * The feature id for the '<em><b>Field3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERIC_PRIVATE_OBJECT__FIELD3 = 2;

  /**
   * The feature id for the '<em><b>Field4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERIC_PRIVATE_OBJECT__FIELD4 = 3;

  /**
   * The feature id for the '<em><b>Field5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERIC_PRIVATE_OBJECT__FIELD5 = 4;

  /**
   * The number of structural features of the '<em>TGeneric Private Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERIC_PRIVATE_OBJECT_FEATURE_COUNT = 5;

  /**
   * The number of operations of the '<em>TGeneric Private Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TGENERIC_PRIVATE_OBJECT_OPERATION_COUNT = 0;


  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.systemcorp.DocumentRoot <em>Document Root</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Document Root</em>'.
   * @see com.lucy.g3.iec61850.model.systemcorp.DocumentRoot
   * @generated
   */
  EClass getDocumentRoot();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.systemcorp.DocumentRoot#getMixed <em>Mixed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Mixed</em>'.
   * @see com.lucy.g3.iec61850.model.systemcorp.DocumentRoot#getMixed()
   * @see #getDocumentRoot()
   * @generated
   */
  EAttribute getDocumentRoot_Mixed();

  /**
   * Returns the meta object for the map '{@link com.lucy.g3.iec61850.model.systemcorp.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
   * @see com.lucy.g3.iec61850.model.systemcorp.DocumentRoot#getXMLNSPrefixMap()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XMLNSPrefixMap();

  /**
   * Returns the meta object for the map '{@link com.lucy.g3.iec61850.model.systemcorp.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XSI Schema Location</em>'.
   * @see com.lucy.g3.iec61850.model.systemcorp.DocumentRoot#getXSISchemaLocation()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XSISchemaLocation();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.systemcorp.DocumentRoot#getGenericPrivateObject <em>Generic Private Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Generic Private Object</em>'.
   * @see com.lucy.g3.iec61850.model.systemcorp.DocumentRoot#getGenericPrivateObject()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_GenericPrivateObject();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject <em>TGeneric Private Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TGeneric Private Object</em>'.
   * @see com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject
   * @generated
   */
  EClass getTGenericPrivateObject();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField1 <em>Field1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Field1</em>'.
   * @see com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField1()
   * @see #getTGenericPrivateObject()
   * @generated
   */
  EAttribute getTGenericPrivateObject_Field1();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField2 <em>Field2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Field2</em>'.
   * @see com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField2()
   * @see #getTGenericPrivateObject()
   * @generated
   */
  EAttribute getTGenericPrivateObject_Field2();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField3 <em>Field3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Field3</em>'.
   * @see com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField3()
   * @see #getTGenericPrivateObject()
   * @generated
   */
  EAttribute getTGenericPrivateObject_Field3();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField4 <em>Field4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Field4</em>'.
   * @see com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField4()
   * @see #getTGenericPrivateObject()
   * @generated
   */
  EAttribute getTGenericPrivateObject_Field4();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField5 <em>Field5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Field5</em>'.
   * @see com.lucy.g3.iec61850.model.systemcorp.TGenericPrivateObject#getField5()
   * @see #getTGenericPrivateObject()
   * @generated
   */
  EAttribute getTGenericPrivateObject_Field5();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  SystemCorpFactory getSystemCorpFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each operation of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals {
    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61850.model.systemcorp.impl.DocumentRootImpl <em>Document Root</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61850.model.systemcorp.impl.DocumentRootImpl
     * @see com.lucy.g3.iec61850.model.systemcorp.impl.SystemCorpPackageImpl#getDocumentRoot()
     * @generated
     */
    EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

    /**
     * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

    /**
     * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

    /**
     * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

    /**
     * The meta object literal for the '<em><b>Generic Private Object</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__GENERIC_PRIVATE_OBJECT = eINSTANCE.getDocumentRoot_GenericPrivateObject();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61850.model.systemcorp.impl.TGenericPrivateObjectImpl <em>TGeneric Private Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61850.model.systemcorp.impl.TGenericPrivateObjectImpl
     * @see com.lucy.g3.iec61850.model.systemcorp.impl.SystemCorpPackageImpl#getTGenericPrivateObject()
     * @generated
     */
    EClass TGENERIC_PRIVATE_OBJECT = eINSTANCE.getTGenericPrivateObject();

    /**
     * The meta object literal for the '<em><b>Field1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TGENERIC_PRIVATE_OBJECT__FIELD1 = eINSTANCE.getTGenericPrivateObject_Field1();

    /**
     * The meta object literal for the '<em><b>Field2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TGENERIC_PRIVATE_OBJECT__FIELD2 = eINSTANCE.getTGenericPrivateObject_Field2();

    /**
     * The meta object literal for the '<em><b>Field3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TGENERIC_PRIVATE_OBJECT__FIELD3 = eINSTANCE.getTGenericPrivateObject_Field3();

    /**
     * The meta object literal for the '<em><b>Field4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TGENERIC_PRIVATE_OBJECT__FIELD4 = eINSTANCE.getTGenericPrivateObject_Field4();

    /**
     * The meta object literal for the '<em><b>Field5</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TGENERIC_PRIVATE_OBJECT__FIELD5 = eINSTANCE.getTGenericPrivateObject_Field5();

  }

} //SystemCorpPackage
