/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TAccess Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getServer <em>Server</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getLN <em>LN</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getServerAt <em>Server At</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getServices <em>Services</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getGOOSESecurity <em>GOOSE Security</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getSMVSecurity <em>SMV Security</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#isClock <em>Clock</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#isRouter <em>Router</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAccessPoint()
 * @model extendedMetaData="name='tAccessPoint' kind='elementOnly'"
 * @generated
 */
public interface TAccessPoint extends TUnNaming {
  /**
   * Returns the value of the '<em><b>Server</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Server</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Server</em>' containment reference.
   * @see #setServer(TServer)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAccessPoint_Server()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='Server' namespace='##targetNamespace'"
   * @generated
   */
  TServer getServer();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getServer <em>Server</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Server</em>' containment reference.
   * @see #getServer()
   * @generated
   */
  void setServer(TServer value);

  /**
   * Returns the value of the '<em><b>LN</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TLN}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>LN</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>LN</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAccessPoint_LN()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='LN' namespace='##targetNamespace'"
   * @generated
   */
  EList<TLN> getLN();

  /**
   * Returns the value of the '<em><b>Server At</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Server At</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Server At</em>' containment reference.
   * @see #setServerAt(TServerAt)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAccessPoint_ServerAt()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='ServerAt' namespace='##targetNamespace'"
   * @generated
   */
  TServerAt getServerAt();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getServerAt <em>Server At</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Server At</em>' containment reference.
   * @see #getServerAt()
   * @generated
   */
  void setServerAt(TServerAt value);

  /**
   * Returns the value of the '<em><b>Services</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Services</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Services</em>' containment reference.
   * @see #setServices(TServices)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAccessPoint_Services()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='Services' namespace='##targetNamespace'"
   * @generated
   */
  TServices getServices();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getServices <em>Services</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Services</em>' containment reference.
   * @see #getServices()
   * @generated
   */
  void setServices(TServices value);

  /**
   * Returns the value of the '<em><b>GOOSE Security</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TCertificate}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>GOOSE Security</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>GOOSE Security</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAccessPoint_GOOSESecurity()
   * @model containment="true" upper="7"
   *        extendedMetaData="kind='element' name='GOOSESecurity' namespace='##targetNamespace'"
   * @generated
   */
  EList<TCertificate> getGOOSESecurity();

  /**
   * Returns the value of the '<em><b>SMV Security</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TCertificate}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>SMV Security</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>SMV Security</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAccessPoint_SMVSecurity()
   * @model containment="true" upper="7"
   *        extendedMetaData="kind='element' name='SMVSecurity' namespace='##targetNamespace'"
   * @generated
   */
  EList<TCertificate> getSMVSecurity();

  /**
   * Returns the value of the '<em><b>Clock</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Clock</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Clock</em>' attribute.
   * @see #isSetClock()
   * @see #unsetClock()
   * @see #setClock(boolean)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAccessPoint_Clock()
   * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   *        extendedMetaData="kind='attribute' name='clock'"
   * @generated
   */
  boolean isClock();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#isClock <em>Clock</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Clock</em>' attribute.
   * @see #isSetClock()
   * @see #unsetClock()
   * @see #isClock()
   * @generated
   */
  void setClock(boolean value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#isClock <em>Clock</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetClock()
   * @see #isClock()
   * @see #setClock(boolean)
   * @generated
   */
  void unsetClock();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#isClock <em>Clock</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Clock</em>' attribute is set.
   * @see #unsetClock()
   * @see #isClock()
   * @see #setClock(boolean)
   * @generated
   */
  boolean isSetClock();

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAccessPoint_Name()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TAccessPointName" required="true"
   *        extendedMetaData="kind='attribute' name='name'"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Router</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Router</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Router</em>' attribute.
   * @see #isSetRouter()
   * @see #unsetRouter()
   * @see #setRouter(boolean)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAccessPoint_Router()
   * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   *        extendedMetaData="kind='attribute' name='router'"
   * @generated
   */
  boolean isRouter();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#isRouter <em>Router</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Router</em>' attribute.
   * @see #isSetRouter()
   * @see #unsetRouter()
   * @see #isRouter()
   * @generated
   */
  void setRouter(boolean value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#isRouter <em>Router</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetRouter()
   * @see #isRouter()
   * @see #setRouter(boolean)
   * @generated
   */
  void unsetRouter();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint#isRouter <em>Router</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Router</em>' attribute is set.
   * @see #unsetRouter()
   * @see #isRouter()
   * @see #setRouter(boolean)
   * @generated
   */
  boolean isSetRouter();

} // TAccessPoint
