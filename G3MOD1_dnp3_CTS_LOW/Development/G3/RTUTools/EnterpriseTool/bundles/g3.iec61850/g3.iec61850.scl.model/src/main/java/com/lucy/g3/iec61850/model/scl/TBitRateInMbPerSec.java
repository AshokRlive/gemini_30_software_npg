/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

import java.math.BigDecimal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TBit Rate In Mb Per Sec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getValue <em>Value</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getMultiplier <em>Multiplier</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getUnit <em>Unit</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBitRateInMbPerSec()
 * @model extendedMetaData="name='tBitRateInMbPerSec' kind='simple'"
 * @extends SCLObject
 * @generated
 */
public interface TBitRateInMbPerSec extends SCLObject {
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(BigDecimal)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBitRateInMbPerSec_Value()
   * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
   *        extendedMetaData="name=':0' kind='simple'"
   * @generated
   */
  BigDecimal getValue();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(BigDecimal value);

  /**
   * Returns the value of the '<em><b>Multiplier</b></em>' attribute.
   * The default value is <code>"M"</code>.
   * The literals are from the enumeration {@link com.lucy.g3.iec61850.model.scl.TUnitMultiplierEnum}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Multiplier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Multiplier</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TUnitMultiplierEnum
   * @see #isSetMultiplier()
   * @see #unsetMultiplier()
   * @see #setMultiplier(TUnitMultiplierEnum)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBitRateInMbPerSec_Multiplier()
   * @model default="M" unsettable="true"
   *        extendedMetaData="kind='attribute' name='multiplier'"
   * @generated
   */
  TUnitMultiplierEnum getMultiplier();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getMultiplier <em>Multiplier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Multiplier</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TUnitMultiplierEnum
   * @see #isSetMultiplier()
   * @see #unsetMultiplier()
   * @see #getMultiplier()
   * @generated
   */
  void setMultiplier(TUnitMultiplierEnum value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getMultiplier <em>Multiplier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetMultiplier()
   * @see #getMultiplier()
   * @see #setMultiplier(TUnitMultiplierEnum)
   * @generated
   */
  void unsetMultiplier();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getMultiplier <em>Multiplier</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Multiplier</em>' attribute is set.
   * @see #unsetMultiplier()
   * @see #getMultiplier()
   * @see #setMultiplier(TUnitMultiplierEnum)
   * @generated
   */
  boolean isSetMultiplier();

  /**
   * Returns the value of the '<em><b>Unit</b></em>' attribute.
   * The default value is <code>"b/s"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Unit</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Unit</em>' attribute.
   * @see #isSetUnit()
   * @see #unsetUnit()
   * @see #setUnit(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBitRateInMbPerSec_Unit()
   * @model default="b/s" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
   *        extendedMetaData="kind='attribute' name='unit'"
   * @generated
   */
  String getUnit();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getUnit <em>Unit</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Unit</em>' attribute.
   * @see #isSetUnit()
   * @see #unsetUnit()
   * @see #getUnit()
   * @generated
   */
  void setUnit(String value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getUnit <em>Unit</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetUnit()
   * @see #getUnit()
   * @see #setUnit(String)
   * @generated
   */
  void unsetUnit();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec#getUnit <em>Unit</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Unit</em>' attribute is set.
   * @see #unsetUnit()
   * @see #getUnit()
   * @see #setUnit(String)
   * @generated
   */
  boolean isSetUnit();

} // TBitRateInMbPerSec
