/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.BufModeType;
import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TServiceConfReportControl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TService Conf Report Control</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServiceConfReportControlImpl#isBufConf <em>Buf Conf</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServiceConfReportControlImpl#getBufMode <em>Buf Mode</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TServiceConfReportControlImpl extends TServiceWithMaxImpl implements TServiceConfReportControl {
  /**
   * The default value of the '{@link #isBufConf() <em>Buf Conf</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isBufConf()
   * @generated
   * @ordered
   */
  protected static final boolean BUF_CONF_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isBufConf() <em>Buf Conf</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isBufConf()
   * @generated
   * @ordered
   */
  protected boolean bufConf = BUF_CONF_EDEFAULT;

  /**
   * This is true if the Buf Conf attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean bufConfESet;

  /**
   * The default value of the '{@link #getBufMode() <em>Buf Mode</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBufMode()
   * @generated
   * @ordered
   */
  protected static final BufModeType BUF_MODE_EDEFAULT = BufModeType.UNBUFFERED;

  /**
   * The cached value of the '{@link #getBufMode() <em>Buf Mode</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBufMode()
   * @generated
   * @ordered
   */
  protected BufModeType bufMode = BUF_MODE_EDEFAULT;

  /**
   * This is true if the Buf Mode attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean bufModeESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TServiceConfReportControlImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTServiceConfReportControl();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isBufConf() {
    return bufConf;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBufConf(boolean newBufConf) {
    boolean oldBufConf = bufConf;
    bufConf = newBufConf;
    boolean oldBufConfESet = bufConfESet;
    bufConfESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICE_CONF_REPORT_CONTROL__BUF_CONF, oldBufConf, bufConf, !oldBufConfESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetBufConf() {
    boolean oldBufConf = bufConf;
    boolean oldBufConfESet = bufConfESet;
    bufConf = BUF_CONF_EDEFAULT;
    bufConfESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TSERVICE_CONF_REPORT_CONTROL__BUF_CONF, oldBufConf, BUF_CONF_EDEFAULT, oldBufConfESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetBufConf() {
    return bufConfESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BufModeType getBufMode() {
    return bufMode;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBufMode(BufModeType newBufMode) {
    BufModeType oldBufMode = bufMode;
    bufMode = newBufMode == null ? BUF_MODE_EDEFAULT : newBufMode;
    boolean oldBufModeESet = bufModeESet;
    bufModeESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICE_CONF_REPORT_CONTROL__BUF_MODE, oldBufMode, bufMode, !oldBufModeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetBufMode() {
    BufModeType oldBufMode = bufMode;
    boolean oldBufModeESet = bufModeESet;
    bufMode = BUF_MODE_EDEFAULT;
    bufModeESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TSERVICE_CONF_REPORT_CONTROL__BUF_MODE, oldBufMode, BUF_MODE_EDEFAULT, oldBufModeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetBufMode() {
    return bufModeESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TSERVICE_CONF_REPORT_CONTROL__BUF_CONF:
        return isBufConf();
      case SCLPackage.TSERVICE_CONF_REPORT_CONTROL__BUF_MODE:
        return getBufMode();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TSERVICE_CONF_REPORT_CONTROL__BUF_CONF:
        setBufConf((Boolean)newValue);
        return;
      case SCLPackage.TSERVICE_CONF_REPORT_CONTROL__BUF_MODE:
        setBufMode((BufModeType)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TSERVICE_CONF_REPORT_CONTROL__BUF_CONF:
        unsetBufConf();
        return;
      case SCLPackage.TSERVICE_CONF_REPORT_CONTROL__BUF_MODE:
        unsetBufMode();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TSERVICE_CONF_REPORT_CONTROL__BUF_CONF:
        return isSetBufConf();
      case SCLPackage.TSERVICE_CONF_REPORT_CONTROL__BUF_MODE:
        return isSetBufMode();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (bufConf: ");
    if (bufConfESet) result.append(bufConf); else result.append("<unset>");
    result.append(", bufMode: ");
    if (bufModeESet) result.append(bufMode); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TServiceConfReportControlImpl
