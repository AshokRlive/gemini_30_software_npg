/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingFactory
 * @model kind="package"
 *        extendedMetaData="qualified='false'"
 * @generated
 */
public interface IEC61850MappingPackage extends EPackage {
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "IEC61850Mapping";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "platform:/resource/g3.iec61850.scl.model/model/Edition2/schema/IEC61850Mapping.xsd";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "IEC61850Mapping";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  IEC61850MappingPackage eINSTANCE = com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl.init();

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DataAttributeIdTImpl <em>Data Attribute Id T</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DataAttributeIdTImpl
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl#getDataAttributeIdT()
   * @generated
   */
  int DATA_ATTRIBUTE_ID_T = 0;

  /**
   * The feature id for the '<em><b>Field0</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_ATTRIBUTE_ID_T__FIELD0 = 0;

  /**
   * The feature id for the '<em><b>Field1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_ATTRIBUTE_ID_T__FIELD1 = 1;

  /**
   * The feature id for the '<em><b>Field2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_ATTRIBUTE_ID_T__FIELD2 = 2;

  /**
   * The feature id for the '<em><b>Field3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_ATTRIBUTE_ID_T__FIELD3 = 3;

  /**
   * The feature id for the '<em><b>Field4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_ATTRIBUTE_ID_T__FIELD4 = 4;

  /**
   * The feature id for the '<em><b>Field5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_ATTRIBUTE_ID_T__FIELD5 = 5;

  /**
   * The number of structural features of the '<em>Data Attribute Id T</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_ATTRIBUTE_ID_T_FEATURE_COUNT = 6;

  /**
   * The number of operations of the '<em>Data Attribute Id T</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_ATTRIBUTE_ID_T_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DocumentRootImpl <em>Document Root</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DocumentRootImpl
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl#getDocumentRoot()
   * @generated
   */
  int DOCUMENT_ROOT = 1;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__MIXED = 0;

  /**
   * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

  /**
   * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

  /**
   * The feature id for the '<em><b>Mapping Root</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__MAPPING_ROOT = 3;

  /**
   * The number of structural features of the '<em>Document Root</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT_FEATURE_COUNT = 4;

  /**
   * The number of operations of the '<em>Document Root</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.EntryTImpl <em>Entry T</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.EntryTImpl
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl#getEntryT()
   * @generated
   */
  int ENTRY_T = 2;

  /**
   * The feature id for the '<em><b>G3 Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENTRY_T__G3_REF = 0;

  /**
   * The feature id for the '<em><b>Data Attribute Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENTRY_T__DATA_ATTRIBUTE_ID = 1;

  /**
   * The feature id for the '<em><b>Attribute Ref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENTRY_T__ATTRIBUTE_REF = 2;

  /**
   * The feature id for the '<em><b>Uuid</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENTRY_T__UUID = 3;

  /**
   * The feature id for the '<em><b>Data Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENTRY_T__DATA_TYPE = 4;

  /**
   * The number of structural features of the '<em>Entry T</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENTRY_T_FEATURE_COUNT = 5;

  /**
   * The number of operations of the '<em>Entry T</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENTRY_T_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.G3RefTImpl <em>G3 Ref T</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.G3RefTImpl
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl#getG3RefT()
   * @generated
   */
  int G3_REF_T = 3;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int G3_REF_T__GROUP = 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int G3_REF_T__ID = 1;

  /**
   * The feature id for the '<em><b>Uuid</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int G3_REF_T__UUID = 2;

  /**
   * The number of structural features of the '<em>G3 Ref T</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int G3_REF_T_FEATURE_COUNT = 3;

  /**
   * The number of operations of the '<em>G3 Ref T</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int G3_REF_T_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.MappingRootTImpl <em>Mapping Root T</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.MappingRootTImpl
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl#getMappingRootT()
   * @generated
   */
  int MAPPING_ROOT_T = 4;

  /**
   * The feature id for the '<em><b>Entry</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING_ROOT_T__ENTRY = 0;

  /**
   * The feature id for the '<em><b>Schema Version Major</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING_ROOT_T__SCHEMA_VERSION_MAJOR = 1;

  /**
   * The feature id for the '<em><b>Schema Version Minor</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING_ROOT_T__SCHEMA_VERSION_MINOR = 2;

  /**
   * The number of structural features of the '<em>Mapping Root T</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING_ROOT_T_FEATURE_COUNT = 3;

  /**
   * The number of operations of the '<em>Mapping Root T</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING_ROOT_T_OPERATION_COUNT = 0;


  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT <em>Data Attribute Id T</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Data Attribute Id T</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT
   * @generated
   */
  EClass getDataAttributeIdT();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField0 <em>Field0</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Field0</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField0()
   * @see #getDataAttributeIdT()
   * @generated
   */
  EAttribute getDataAttributeIdT_Field0();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField1 <em>Field1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Field1</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField1()
   * @see #getDataAttributeIdT()
   * @generated
   */
  EAttribute getDataAttributeIdT_Field1();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField2 <em>Field2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Field2</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField2()
   * @see #getDataAttributeIdT()
   * @generated
   */
  EAttribute getDataAttributeIdT_Field2();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField3 <em>Field3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Field3</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField3()
   * @see #getDataAttributeIdT()
   * @generated
   */
  EAttribute getDataAttributeIdT_Field3();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField4 <em>Field4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Field4</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField4()
   * @see #getDataAttributeIdT()
   * @generated
   */
  EAttribute getDataAttributeIdT_Field4();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField5 <em>Field5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Field5</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField5()
   * @see #getDataAttributeIdT()
   * @generated
   */
  EAttribute getDataAttributeIdT_Field5();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DocumentRoot <em>Document Root</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Document Root</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DocumentRoot
   * @generated
   */
  EClass getDocumentRoot();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DocumentRoot#getMixed <em>Mixed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Mixed</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DocumentRoot#getMixed()
   * @see #getDocumentRoot()
   * @generated
   */
  EAttribute getDocumentRoot_Mixed();

  /**
   * Returns the meta object for the map '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DocumentRoot#getXMLNSPrefixMap()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XMLNSPrefixMap();

  /**
   * Returns the meta object for the map '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XSI Schema Location</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DocumentRoot#getXSISchemaLocation()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XSISchemaLocation();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DocumentRoot#getMappingRoot <em>Mapping Root</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Mapping Root</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DocumentRoot#getMappingRoot()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_MappingRoot();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT <em>Entry T</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Entry T</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT
   * @generated
   */
  EClass getEntryT();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getG3Ref <em>G3 Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>G3 Ref</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getG3Ref()
   * @see #getEntryT()
   * @generated
   */
  EReference getEntryT_G3Ref();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getDataAttributeId <em>Data Attribute Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Data Attribute Id</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getDataAttributeId()
   * @see #getEntryT()
   * @generated
   */
  EReference getEntryT_DataAttributeId();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getAttributeRef <em>Attribute Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Attribute Ref</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getAttributeRef()
   * @see #getEntryT()
   * @generated
   */
  EAttribute getEntryT_AttributeRef();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getUuid <em>Uuid</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uuid</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getUuid()
   * @see #getEntryT()
   * @generated
   */
  EAttribute getEntryT_Uuid();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getDataType <em>Data Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Data Type</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT#getDataType()
   * @see #getEntryT()
   * @generated
   */
  EAttribute getEntryT_DataType();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT <em>G3 Ref T</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>G3 Ref T</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT
   * @generated
   */
  EClass getG3RefT();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Group</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT#getGroup()
   * @see #getG3RefT()
   * @generated
   */
  EAttribute getG3RefT_Group();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT#getId()
   * @see #getG3RefT()
   * @generated
   */
  EAttribute getG3RefT_Id();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT#getUuid <em>Uuid</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uuid</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT#getUuid()
   * @see #getG3RefT()
   * @generated
   */
  EAttribute getG3RefT_Uuid();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT <em>Mapping Root T</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mapping Root T</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT
   * @generated
   */
  EClass getMappingRootT();

  /**
   * Returns the meta object for the containment reference list '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getEntry <em>Entry</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Entry</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getEntry()
   * @see #getMappingRootT()
   * @generated
   */
  EReference getMappingRootT_Entry();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getSchemaVersionMajor <em>Schema Version Major</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Schema Version Major</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getSchemaVersionMajor()
   * @see #getMappingRootT()
   * @generated
   */
  EAttribute getMappingRootT_SchemaVersionMajor();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getSchemaVersionMinor <em>Schema Version Minor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Schema Version Minor</em>'.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT#getSchemaVersionMinor()
   * @see #getMappingRootT()
   * @generated
   */
  EAttribute getMappingRootT_SchemaVersionMinor();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  IEC61850MappingFactory getIEC61850MappingFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each operation of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals {
    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DataAttributeIdTImpl <em>Data Attribute Id T</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DataAttributeIdTImpl
     * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl#getDataAttributeIdT()
     * @generated
     */
    EClass DATA_ATTRIBUTE_ID_T = eINSTANCE.getDataAttributeIdT();

    /**
     * The meta object literal for the '<em><b>Field0</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATA_ATTRIBUTE_ID_T__FIELD0 = eINSTANCE.getDataAttributeIdT_Field0();

    /**
     * The meta object literal for the '<em><b>Field1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATA_ATTRIBUTE_ID_T__FIELD1 = eINSTANCE.getDataAttributeIdT_Field1();

    /**
     * The meta object literal for the '<em><b>Field2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATA_ATTRIBUTE_ID_T__FIELD2 = eINSTANCE.getDataAttributeIdT_Field2();

    /**
     * The meta object literal for the '<em><b>Field3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATA_ATTRIBUTE_ID_T__FIELD3 = eINSTANCE.getDataAttributeIdT_Field3();

    /**
     * The meta object literal for the '<em><b>Field4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATA_ATTRIBUTE_ID_T__FIELD4 = eINSTANCE.getDataAttributeIdT_Field4();

    /**
     * The meta object literal for the '<em><b>Field5</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATA_ATTRIBUTE_ID_T__FIELD5 = eINSTANCE.getDataAttributeIdT_Field5();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DocumentRootImpl <em>Document Root</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DocumentRootImpl
     * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl#getDocumentRoot()
     * @generated
     */
    EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

    /**
     * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

    /**
     * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

    /**
     * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

    /**
     * The meta object literal for the '<em><b>Mapping Root</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__MAPPING_ROOT = eINSTANCE.getDocumentRoot_MappingRoot();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.EntryTImpl <em>Entry T</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.EntryTImpl
     * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl#getEntryT()
     * @generated
     */
    EClass ENTRY_T = eINSTANCE.getEntryT();

    /**
     * The meta object literal for the '<em><b>G3 Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ENTRY_T__G3_REF = eINSTANCE.getEntryT_G3Ref();

    /**
     * The meta object literal for the '<em><b>Data Attribute Id</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ENTRY_T__DATA_ATTRIBUTE_ID = eINSTANCE.getEntryT_DataAttributeId();

    /**
     * The meta object literal for the '<em><b>Attribute Ref</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ENTRY_T__ATTRIBUTE_REF = eINSTANCE.getEntryT_AttributeRef();

    /**
     * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ENTRY_T__UUID = eINSTANCE.getEntryT_Uuid();

    /**
     * The meta object literal for the '<em><b>Data Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ENTRY_T__DATA_TYPE = eINSTANCE.getEntryT_DataType();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.G3RefTImpl <em>G3 Ref T</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.G3RefTImpl
     * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl#getG3RefT()
     * @generated
     */
    EClass G3_REF_T = eINSTANCE.getG3RefT();

    /**
     * The meta object literal for the '<em><b>Group</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute G3_REF_T__GROUP = eINSTANCE.getG3RefT_Group();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute G3_REF_T__ID = eINSTANCE.getG3RefT_Id();

    /**
     * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute G3_REF_T__UUID = eINSTANCE.getG3RefT_Uuid();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.MappingRootTImpl <em>Mapping Root T</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.MappingRootTImpl
     * @see com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl#getMappingRootT()
     * @generated
     */
    EClass MAPPING_ROOT_T = eINSTANCE.getMappingRootT();

    /**
     * The meta object literal for the '<em><b>Entry</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAPPING_ROOT_T__ENTRY = eINSTANCE.getMappingRootT_Entry();

    /**
     * The meta object literal for the '<em><b>Schema Version Major</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MAPPING_ROOT_T__SCHEMA_VERSION_MAJOR = eINSTANCE.getMappingRootT_SchemaVersionMajor();

    /**
     * The meta object literal for the '<em><b>Schema Version Minor</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MAPPING_ROOT_T__SCHEMA_VERSION_MINOR = eINSTANCE.getMappingRootT_SchemaVersionMinor();

  }

} //IEC61850MappingPackage
