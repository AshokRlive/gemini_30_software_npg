/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TSMV Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getGroup <em>Group</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSmpRate <em>Smp Rate</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSamplesPerSec <em>Samples Per Sec</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSecPerSamples <em>Sec Per Samples</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getOptFields <em>Opt Fields</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#isSamplesPerSec1 <em>Samples Per Sec1</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSmpRate1 <em>Smp Rate1</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSvID <em>Sv ID</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSMVSettings()
 * @model extendedMetaData="name='tSMVSettings' kind='elementOnly'"
 * @generated
 */
public interface TSMVSettings extends TServiceSettings {
  /**
   * Returns the value of the '<em><b>Group</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group</em>' attribute list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSMVSettings_Group()
   * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='group' name='group:2'"
   * @generated
   */
  FeatureMap getGroup();

  /**
   * Returns the value of the '<em><b>Smp Rate</b></em>' attribute list.
   * The list contents are of type {@link java.lang.Long}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Smp Rate</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Smp Rate</em>' attribute list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSMVSettings_SmpRate()
   * @model unique="false" dataType="com.lucy.g3.iec61850.model.scl.SmpRateType" transient="true" volatile="true" derived="true"
   *        extendedMetaData="kind='element' name='SmpRate' namespace='##targetNamespace' group='#group:2'"
   * @generated
   */
  EList<Long> getSmpRate();

  /**
   * Returns the value of the '<em><b>Samples Per Sec</b></em>' attribute list.
   * The list contents are of type {@link java.lang.Long}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Samples Per Sec</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Samples Per Sec</em>' attribute list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSMVSettings_SamplesPerSec()
   * @model unique="false" dataType="com.lucy.g3.iec61850.model.scl.SamplesPerSecType" transient="true" volatile="true" derived="true"
   *        extendedMetaData="kind='element' name='SamplesPerSec' namespace='##targetNamespace' group='#group:2'"
   * @generated
   */
  EList<Long> getSamplesPerSec();

  /**
   * Returns the value of the '<em><b>Sec Per Samples</b></em>' attribute list.
   * The list contents are of type {@link java.lang.Long}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sec Per Samples</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sec Per Samples</em>' attribute list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSMVSettings_SecPerSamples()
   * @model unique="false" dataType="com.lucy.g3.iec61850.model.scl.SecPerSamplesType" transient="true" volatile="true" derived="true"
   *        extendedMetaData="kind='element' name='SecPerSamples' namespace='##targetNamespace' group='#group:2'"
   * @generated
   */
  EList<Long> getSecPerSamples();

  /**
   * Returns the value of the '<em><b>Opt Fields</b></em>' attribute.
   * The default value is <code>"Fix"</code>.
   * The literals are from the enumeration {@link com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Opt Fields</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Opt Fields</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum
   * @see #isSetOptFields()
   * @see #unsetOptFields()
   * @see #setOptFields(TServiceSettingsEnum)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSMVSettings_OptFields()
   * @model default="Fix" unsettable="true"
   *        extendedMetaData="kind='attribute' name='optFields'"
   * @generated
   */
  TServiceSettingsEnum getOptFields();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getOptFields <em>Opt Fields</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Opt Fields</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum
   * @see #isSetOptFields()
   * @see #unsetOptFields()
   * @see #getOptFields()
   * @generated
   */
  void setOptFields(TServiceSettingsEnum value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getOptFields <em>Opt Fields</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetOptFields()
   * @see #getOptFields()
   * @see #setOptFields(TServiceSettingsEnum)
   * @generated
   */
  void unsetOptFields();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getOptFields <em>Opt Fields</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Opt Fields</em>' attribute is set.
   * @see #unsetOptFields()
   * @see #getOptFields()
   * @see #setOptFields(TServiceSettingsEnum)
   * @generated
   */
  boolean isSetOptFields();

  /**
   * Returns the value of the '<em><b>Samples Per Sec1</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Samples Per Sec1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Samples Per Sec1</em>' attribute.
   * @see #isSetSamplesPerSec1()
   * @see #unsetSamplesPerSec1()
   * @see #setSamplesPerSec1(boolean)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSMVSettings_SamplesPerSec1()
   * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   *        extendedMetaData="kind='attribute' name='samplesPerSec'"
   * @generated
   */
  boolean isSamplesPerSec1();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#isSamplesPerSec1 <em>Samples Per Sec1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Samples Per Sec1</em>' attribute.
   * @see #isSetSamplesPerSec1()
   * @see #unsetSamplesPerSec1()
   * @see #isSamplesPerSec1()
   * @generated
   */
  void setSamplesPerSec1(boolean value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#isSamplesPerSec1 <em>Samples Per Sec1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetSamplesPerSec1()
   * @see #isSamplesPerSec1()
   * @see #setSamplesPerSec1(boolean)
   * @generated
   */
  void unsetSamplesPerSec1();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#isSamplesPerSec1 <em>Samples Per Sec1</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Samples Per Sec1</em>' attribute is set.
   * @see #unsetSamplesPerSec1()
   * @see #isSamplesPerSec1()
   * @see #setSamplesPerSec1(boolean)
   * @generated
   */
  boolean isSetSamplesPerSec1();

  /**
   * Returns the value of the '<em><b>Smp Rate1</b></em>' attribute.
   * The default value is <code>"Fix"</code>.
   * The literals are from the enumeration {@link com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Smp Rate1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Smp Rate1</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum
   * @see #isSetSmpRate1()
   * @see #unsetSmpRate1()
   * @see #setSmpRate1(TServiceSettingsEnum)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSMVSettings_SmpRate1()
   * @model default="Fix" unsettable="true"
   *        extendedMetaData="kind='attribute' name='smpRate'"
   * @generated
   */
  TServiceSettingsEnum getSmpRate1();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSmpRate1 <em>Smp Rate1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Smp Rate1</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum
   * @see #isSetSmpRate1()
   * @see #unsetSmpRate1()
   * @see #getSmpRate1()
   * @generated
   */
  void setSmpRate1(TServiceSettingsEnum value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSmpRate1 <em>Smp Rate1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetSmpRate1()
   * @see #getSmpRate1()
   * @see #setSmpRate1(TServiceSettingsEnum)
   * @generated
   */
  void unsetSmpRate1();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSmpRate1 <em>Smp Rate1</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Smp Rate1</em>' attribute is set.
   * @see #unsetSmpRate1()
   * @see #getSmpRate1()
   * @see #setSmpRate1(TServiceSettingsEnum)
   * @generated
   */
  boolean isSetSmpRate1();

  /**
   * Returns the value of the '<em><b>Sv ID</b></em>' attribute.
   * The default value is <code>"Fix"</code>.
   * The literals are from the enumeration {@link com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sv ID</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sv ID</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum
   * @see #isSetSvID()
   * @see #unsetSvID()
   * @see #setSvID(TServiceSettingsEnum)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSMVSettings_SvID()
   * @model default="Fix" unsettable="true"
   *        extendedMetaData="kind='attribute' name='svID'"
   * @generated
   */
  TServiceSettingsEnum getSvID();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSvID <em>Sv ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sv ID</em>' attribute.
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum
   * @see #isSetSvID()
   * @see #unsetSvID()
   * @see #getSvID()
   * @generated
   */
  void setSvID(TServiceSettingsEnum value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSvID <em>Sv ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetSvID()
   * @see #getSvID()
   * @see #setSvID(TServiceSettingsEnum)
   * @generated
   */
  void unsetSvID();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings#getSvID <em>Sv ID</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Sv ID</em>' attribute is set.
   * @see #unsetSvID()
   * @see #getSvID()
   * @see #setSvID(TServiceSettingsEnum)
   * @generated
   */
  boolean isSetSvID();

} // TSMVSettings
