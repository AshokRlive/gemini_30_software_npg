/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TBase Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TBaseElement#getAny <em>Any</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TBaseElement#getText <em>Text</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TBaseElement#getPrivate <em>Private</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TBaseElement#getAnyAttribute <em>Any Attribute</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBaseElement()
 * @model abstract="true"
 *        extendedMetaData="name='tBaseElement' kind='elementOnly'"
 * @extends SCLObject
 * @generated
 */
public interface TBaseElement extends SCLObject {
  /**
   * Returns the value of the '<em><b>Any</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Any</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Any</em>' attribute list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBaseElement_Any()
   * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='elementWildcard' wildcards='##other' name=':0' processing='lax'"
   * @generated
   */
  FeatureMap getAny();

  /**
   * Returns the value of the '<em><b>Text</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Text</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Text</em>' containment reference.
   * @see #setText(TText)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBaseElement_Text()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='Text' namespace='##targetNamespace'"
   * @generated
   */
  TText getText();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TBaseElement#getText <em>Text</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Text</em>' containment reference.
   * @see #getText()
   * @generated
   */
  void setText(TText value);

  /**
   * Returns the value of the '<em><b>Private</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TPrivate}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Private</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Private</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBaseElement_Private()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='Private' namespace='##targetNamespace'"
   * @generated
   */
  EList<TPrivate> getPrivate();

  /**
   * Returns the value of the '<em><b>Any Attribute</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Any Attribute</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Any Attribute</em>' attribute list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBaseElement_AnyAttribute()
   * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='attributeWildcard' wildcards='##other' name=':3' processing='lax'"
   * @generated
   */
  FeatureMap getAnyAttribute();

} // TBaseElement
