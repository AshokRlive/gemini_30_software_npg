/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPVLANID</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPVLANID()
 * @model extendedMetaData="name='tP_VLAN-ID' kind='simple'"
 * @generated
 */
public interface TPVLANID extends TP {
} // TPVLANID
