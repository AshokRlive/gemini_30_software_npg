/**
 *
 * $Id$
 */
package com.lucy.g3.iec61850.model.scl.validation;


/**
 * A sample validator interface for {@link com.lucy.g3.iec61850.model.scl.TClientLN}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface TClientLNValidator {
  boolean validate();

  boolean validateApRef(String value);
  boolean validateDesc(String value);
  boolean validateIedName(String value);
  boolean validateLdInst(String value);
  boolean validateLnClass(Object value);
  boolean validateLnInst(String value);
  boolean validatePrefix(String value);
}
