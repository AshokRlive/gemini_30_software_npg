/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage;

import com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl;

import com.lucy.g3.iec61850.model.g3ref.G3RefPackage;

import com.lucy.g3.iec61850.model.g3ref.impl.G3RefPackageImpl;

import com.lucy.g3.iec61850.model.scl.SCLFactory;
import com.lucy.g3.iec61850.model.scl.SCLPackage;

import com.lucy.g3.iec61850.model.scl.util.SCLValidator;

import com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage;

import com.lucy.g3.iec61850.model.systemcorp.impl.SystemCorpPackageImpl;

import java.io.IOException;

import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SCLPackageImpl extends EPackageImpl implements SCLPackage {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected String packageFilename = "scl.ecore";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass authenticationTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass documentRootEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass historyTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass iedNameTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ln0TypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass optFieldsTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass protNsTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sclTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass settingGroupsTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass smvOptsTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tAbstractConductingEquipmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tAbstractDataAttributeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tAccessControlEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tAccessPointEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tAddressEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tAnyContentFromOtherNamespaceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tAnyLNEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tAssociationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tBaseElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tBayEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tbdaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tBitRateInMbPerSecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tCertEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tCertificateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tClientLNEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tClientServicesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tCommunicationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tConductingEquipmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tConfLNsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tConnectedAPEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tConnectivityNodeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tControlEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tControlBlockEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tControlWithIEDNameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tControlWithTriggerOptEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tdaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tdaiEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tDataSetEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tDataTypeTemplatesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tdaTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tdoEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tdoiEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tdoTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tDurationInMilliSecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tDurationInSecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tEnumTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tEnumValEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tEquipmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tEquipmentContainerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tExtRefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tfcdaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tFunctionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tGeneralEquipmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tgseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tgseControlEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tgseSettingsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tHeaderEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tHitemEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tidNamingEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tiedEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tInputsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tlDeviceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tlnEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tln0EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tlNodeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tlNodeContainerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tlNodeTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tLogEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tLogControlEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tLogSettingsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tNamingEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tpappidEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tPhysConnEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tpipEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tpipgatewayEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tpipsubnetEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tpmacAddressEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tpmmsPortEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tposiaeInvokeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tposiaeQualifierEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tposiapInvokeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tposiapTitleEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tposinsapEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tposipselEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tposisselEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tpositselEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tPowerSystemResourceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tPowerTransformerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tpPhysConnEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tpPortEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tPrivateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tpsntpPortEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tpvlanidEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tpvlanpriorityEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tReportControlEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tReportSettingsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tRptEnabledEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tSampledValueControlEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tsdiEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tsdoEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tServerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tServerAtEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tServiceConfReportControlEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tServiceForConfDataSetEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tServicesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tServiceSettingsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tServiceWithMaxEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tServiceWithMaxAndMaxAttributesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tServiceWithMaxAndModifyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tServiceWithOptionalMaxEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tServiceYesNoEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tSettingControlEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tsmvEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tsmvSettingsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tSubEquipmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tSubFunctionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tSubNetworkEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tSubstationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tTapChangerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tTerminalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tTextEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tTransformerWindingEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tTrgOpsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tUnNamingEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tValEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tValueWithUnitEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tVoltageEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tVoltageLevelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum bufModeTypeEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum nameStructureTypeEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tAssociationKindEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tAuthenticationEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tDomainLNGroupAEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tDomainLNGroupCEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tDomainLNGroupGEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tDomainLNGroupIEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tDomainLNGroupMEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tDomainLNGroupPEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tDomainLNGroupREnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tDomainLNGroupSEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tDomainLNGroupTEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tDomainLNGroupXEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tDomainLNGroupYEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tDomainLNGroupZEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tfcEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tgseControlTypeEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tlln0EnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tlphdEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tPhaseEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tPowerTransformerEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tPredefinedAttributeNameEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tPredefinedBasicTypeEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tPredefinedCDCEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tPredefinedCommonConductingEquipmentEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tPredefinedGeneralEquipmentEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tPredefinedPhysConnTypeEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tPredefinedPTypeEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tPredefinedPTypePhysConnEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tRightEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tServiceSettingsEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tServiceTypeEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tsiUnitEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tSmpModEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tTransformerWindingEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tUnitMultiplierEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum tValKindEnumEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType actSGTypeEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType actSGTypeObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType appIDTypeEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType bufModeTypeObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType commonNameTypeEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType idHierarchyTypeEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType nameLengthTypeEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType nameLengthTypeObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType nameStructureTypeObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType numOfSGsTypeEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType numOfSGsTypeObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType samplesPerSecTypeEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType samplesPerSecTypeObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType secPerSamplesTypeEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType secPerSamplesTypeObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType serialNumberTypeEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType smpRateTypeEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType smpRateTypeObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType smvIDTypeEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tAccessPointNameEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tAcsiNameEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tAnyNameEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tAssociationIDEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tAssociationKindEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tAttributeNameEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tAuthenticationEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tBasicTypeEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tcbNameEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tcdcEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tCommonConductingEquipmentEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tdaCountEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDataNameEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDataSetNameEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDomainLNEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDomainLNGroupAEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDomainLNGroupCEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDomainLNGroupGEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDomainLNGroupIEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDomainLNGroupMEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDomainLNGroupPEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDomainLNGroupREnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDomainLNGroupSEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDomainLNGroupTEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDomainLNGroupXEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDomainLNGroupYEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tDomainLNGroupZEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tEmptyEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tExtensionAttributeNameEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tExtensionEquipmentEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tExtensionGeneralEquipmentEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tExtensionLNClassEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tExtensionPhysConnTypeEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tExtensionPTypeEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tfcEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tFullAttributeNameEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tFullDONameEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tGeneralEquipmentEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tgseControlTypeEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tiedNameEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tldInstEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tldInstOrEmptyEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tldNameEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tlln0EnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tlnClassEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tlnInstEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tlnInstOrEmptyEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tLogNameEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tlphdEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tNameEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tpAddrEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tPhaseEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tPhysConnTypeEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tPowerTransformerEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tPredefinedAttributeNameEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tPredefinedBasicTypeEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tPredefinedCDCEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tPredefinedCommonConductingEquipmentEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tPredefinedGeneralEquipmentEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tPredefinedLNClassEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tPredefinedPhysConnTypeEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tPredefinedPTypeEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tPredefinedPTypePhysConnEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tPrefixEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tpTypeEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tpTypePhysConnEnumEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tRefEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tRestrName1stLEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tRestrName1stUEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tRightEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tRptIDEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tSclRevisionEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tSclVersionEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tsdoCountEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tServiceSettingsEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tServiceTypeObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tsiUnitEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tSmpModObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tTransformerWindingEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tUnitMultiplierEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType tValKindEnumObjectEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType typeTypeEDataType = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private SCLPackageImpl() {
    super(eNS_URI, SCLFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link SCLPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @generated
   */
  public static SCLPackage init() {
    if (isInited) return (SCLPackage)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI);

    // Obtain or create and register package
    SCLPackageImpl theSCLPackage = (SCLPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SCLPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SCLPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    XMLTypePackage.eINSTANCE.eClass();

    // Obtain or create and register interdependencies
    G3RefPackageImpl theG3RefPackage = (G3RefPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(G3RefPackage.eNS_URI) instanceof G3RefPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(G3RefPackage.eNS_URI) : G3RefPackage.eINSTANCE);
    IEC61850MappingPackageImpl theIEC61850MappingPackage = (IEC61850MappingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IEC61850MappingPackage.eNS_URI) instanceof IEC61850MappingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IEC61850MappingPackage.eNS_URI) : IEC61850MappingPackage.eINSTANCE);
    SystemCorpPackageImpl theSystemCorpPackage = (SystemCorpPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SystemCorpPackage.eNS_URI) instanceof SystemCorpPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SystemCorpPackage.eNS_URI) : SystemCorpPackage.eINSTANCE);

    // Load packages
    theSCLPackage.loadPackage();

    // Create package meta-data objects
    theG3RefPackage.createPackageContents();
    theIEC61850MappingPackage.createPackageContents();
    theSystemCorpPackage.createPackageContents();

    // Initialize created meta-data
    theG3RefPackage.initializePackageContents();
    theIEC61850MappingPackage.initializePackageContents();
    theSystemCorpPackage.initializePackageContents();

    // Fix loaded packages
    theSCLPackage.fixPackageContents();

    // Register package validator
    EValidator.Registry.INSTANCE.put
      (theSCLPackage, 
       new EValidator.Descriptor() {
         public EValidator getEValidator() {
           return SCLValidator.INSTANCE;
         }
       });

    // Mark meta-data to indicate it can't be changed
    theSCLPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(SCLPackage.eNS_URI, theSCLPackage);
    return theSCLPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAuthenticationType() {
    if (authenticationTypeEClass == null) {
      authenticationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(3);
    }
    return authenticationTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAuthenticationType_Certificate() {
        return (EAttribute)getAuthenticationType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAuthenticationType_None() {
        return (EAttribute)getAuthenticationType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAuthenticationType_Password() {
        return (EAttribute)getAuthenticationType().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAuthenticationType_Strong() {
        return (EAttribute)getAuthenticationType().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAuthenticationType_Weak() {
        return (EAttribute)getAuthenticationType().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDocumentRoot() {
    if (documentRootEClass == null) {
      documentRootEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(7);
    }
    return documentRootEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDocumentRoot_Mixed() {
        return (EAttribute)getDocumentRoot().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XMLNSPrefixMap() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XSISchemaLocation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_Communication() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_DataTypeTemplates() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_IED() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_LN() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_LN0() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_SCL() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_Substation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHistoryType() {
    if (historyTypeEClass == null) {
      historyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(8);
    }
    return historyTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHistoryType_Hitem() {
        return (EReference)getHistoryType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIEDNameType() {
    if (iedNameTypeEClass == null) {
      iedNameTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(10);
    }
    return iedNameTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIEDNameType_Value() {
        return (EAttribute)getIEDNameType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIEDNameType_ApRef() {
        return (EAttribute)getIEDNameType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIEDNameType_LdInst() {
        return (EAttribute)getIEDNameType().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIEDNameType_LnClass() {
        return (EAttribute)getIEDNameType().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIEDNameType_LnInst() {
        return (EAttribute)getIEDNameType().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIEDNameType_Prefix() {
        return (EAttribute)getIEDNameType().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLN0Type() {
    if (ln0TypeEClass == null) {
      ln0TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(11);
    }
    return ln0TypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOptFieldsType() {
    if (optFieldsTypeEClass == null) {
      optFieldsTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(18);
    }
    return optFieldsTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOptFieldsType_BufOvfl() {
        return (EAttribute)getOptFieldsType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOptFieldsType_ConfigRef() {
        return (EAttribute)getOptFieldsType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOptFieldsType_DataRef() {
        return (EAttribute)getOptFieldsType().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOptFieldsType_DataSet() {
        return (EAttribute)getOptFieldsType().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOptFieldsType_EntryID() {
        return (EAttribute)getOptFieldsType().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOptFieldsType_ReasonCode() {
        return (EAttribute)getOptFieldsType().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOptFieldsType_SeqNum() {
        return (EAttribute)getOptFieldsType().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOptFieldsType_TimeStamp() {
        return (EAttribute)getOptFieldsType().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getProtNsType() {
    if (protNsTypeEClass == null) {
      protNsTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(19);
    }
    return protNsTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getProtNsType_Value() {
        return (EAttribute)getProtNsType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getProtNsType_Type() {
        return (EAttribute)getProtNsType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSCLType() {
    if (sclTypeEClass == null) {
      sclTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(22);
    }
    return sclTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSCLType_Header() {
        return (EReference)getSCLType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSCLType_Substation() {
        return (EReference)getSCLType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSCLType_Communication() {
        return (EReference)getSCLType().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSCLType_IED() {
        return (EReference)getSCLType().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSCLType_DataTypeTemplates() {
        return (EReference)getSCLType().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSCLType_Revision() {
        return (EAttribute)getSCLType().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSCLType_Version() {
        return (EAttribute)getSCLType().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSettingGroupsType() {
    if (settingGroupsTypeEClass == null) {
      settingGroupsTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(26);
    }
    return settingGroupsTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSettingGroupsType_SGEdit() {
        return (EReference)getSettingGroupsType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSettingGroupsType_ConfSG() {
        return (EReference)getSettingGroupsType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSmvOptsType() {
    if (smvOptsTypeEClass == null) {
      smvOptsTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(30);
    }
    return smvOptsTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSmvOptsType_DataSet() {
        return (EAttribute)getSmvOptsType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSmvOptsType_RefreshTime() {
        return (EAttribute)getSmvOptsType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSmvOptsType_SampleRate() {
        return (EAttribute)getSmvOptsType().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSmvOptsType_SampleSynchronized() {
        return (EAttribute)getSmvOptsType().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSmvOptsType_Security() {
        return (EAttribute)getSmvOptsType().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTAbstractConductingEquipment() {
    if (tAbstractConductingEquipmentEClass == null) {
      tAbstractConductingEquipmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(31);
    }
    return tAbstractConductingEquipmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAbstractConductingEquipment_Terminal() {
        return (EReference)getTAbstractConductingEquipment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAbstractConductingEquipment_SubEquipment() {
        return (EReference)getTAbstractConductingEquipment().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTAbstractDataAttribute() {
    if (tAbstractDataAttributeEClass == null) {
      tAbstractDataAttributeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(32);
    }
    return tAbstractDataAttributeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAbstractDataAttribute_Val() {
        return (EReference)getTAbstractDataAttribute().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAbstractDataAttribute_BType() {
        return (EAttribute)getTAbstractDataAttribute().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAbstractDataAttribute_Count() {
        return (EAttribute)getTAbstractDataAttribute().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAbstractDataAttribute_Name() {
        return (EAttribute)getTAbstractDataAttribute().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAbstractDataAttribute_SAddr() {
        return (EAttribute)getTAbstractDataAttribute().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAbstractDataAttribute_Type() {
        return (EAttribute)getTAbstractDataAttribute().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAbstractDataAttribute_ValKind() {
        return (EAttribute)getTAbstractDataAttribute().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTAccessControl() {
    if (tAccessControlEClass == null) {
      tAccessControlEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(33);
    }
    return tAccessControlEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTAccessPoint() {
    if (tAccessPointEClass == null) {
      tAccessPointEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(34);
    }
    return tAccessPointEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAccessPoint_Server() {
        return (EReference)getTAccessPoint().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAccessPoint_LN() {
        return (EReference)getTAccessPoint().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAccessPoint_ServerAt() {
        return (EReference)getTAccessPoint().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAccessPoint_Services() {
        return (EReference)getTAccessPoint().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAccessPoint_GOOSESecurity() {
        return (EReference)getTAccessPoint().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAccessPoint_SMVSecurity() {
        return (EReference)getTAccessPoint().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAccessPoint_Clock() {
        return (EAttribute)getTAccessPoint().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAccessPoint_Name() {
        return (EAttribute)getTAccessPoint().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAccessPoint_Router() {
        return (EAttribute)getTAccessPoint().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTAddress() {
    if (tAddressEClass == null) {
      tAddressEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(37);
    }
    return tAddressEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAddress_P() {
        return (EReference)getTAddress().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTAnyContentFromOtherNamespace() {
    if (tAnyContentFromOtherNamespaceEClass == null) {
      tAnyContentFromOtherNamespaceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(38);
    }
    return tAnyContentFromOtherNamespaceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAnyContentFromOtherNamespace_Mixed() {
        return (EAttribute)getTAnyContentFromOtherNamespace().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAnyContentFromOtherNamespace_Group() {
        return (EAttribute)getTAnyContentFromOtherNamespace().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAnyContentFromOtherNamespace_Any() {
        return (EAttribute)getTAnyContentFromOtherNamespace().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAnyContentFromOtherNamespace_AnyAttribute() {
        return (EAttribute)getTAnyContentFromOtherNamespace().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTAnyLN() {
    if (tAnyLNEClass == null) {
      tAnyLNEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(39);
    }
    return tAnyLNEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAnyLN_DataSet() {
        return (EReference)getTAnyLN().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAnyLN_ReportControl() {
        return (EReference)getTAnyLN().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAnyLN_LogControl() {
        return (EReference)getTAnyLN().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAnyLN_DOI() {
        return (EReference)getTAnyLN().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAnyLN_Inputs() {
        return (EReference)getTAnyLN().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTAnyLN_Log() {
        return (EReference)getTAnyLN().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAnyLN_LnType() {
        return (EAttribute)getTAnyLN().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTAssociation() {
    if (tAssociationEClass == null) {
      tAssociationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(41);
    }
    return tAssociationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAssociation_AssociationID() {
        return (EAttribute)getTAssociation().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAssociation_Desc() {
        return (EAttribute)getTAssociation().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAssociation_IedName() {
        return (EAttribute)getTAssociation().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAssociation_Kind() {
        return (EAttribute)getTAssociation().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAssociation_LdInst() {
        return (EAttribute)getTAssociation().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAssociation_LnClass() {
        return (EAttribute)getTAssociation().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAssociation_LnInst() {
        return (EAttribute)getTAssociation().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTAssociation_Prefix() {
        return (EAttribute)getTAssociation().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTBaseElement() {
    if (tBaseElementEClass == null) {
      tBaseElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(48);
    }
    return tBaseElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTBaseElement_Any() {
        return (EAttribute)getTBaseElement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTBaseElement_Text() {
        return (EReference)getTBaseElement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTBaseElement_Private() {
        return (EReference)getTBaseElement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTBaseElement_AnyAttribute() {
        return (EAttribute)getTBaseElement().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTBay() {
    if (tBayEClass == null) {
      tBayEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(50);
    }
    return tBayEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTBay_ConductingEquipment() {
        return (EReference)getTBay().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTBay_ConnectivityNode() {
        return (EReference)getTBay().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTBay_Function() {
        return (EReference)getTBay().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTBDA() {
    if (tbdaEClass == null) {
      tbdaEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(51);
    }
    return tbdaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTBitRateInMbPerSec() {
    if (tBitRateInMbPerSecEClass == null) {
      tBitRateInMbPerSecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(52);
    }
    return tBitRateInMbPerSecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTBitRateInMbPerSec_Value() {
        return (EAttribute)getTBitRateInMbPerSec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTBitRateInMbPerSec_Multiplier() {
        return (EAttribute)getTBitRateInMbPerSec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTBitRateInMbPerSec_Unit() {
        return (EAttribute)getTBitRateInMbPerSec().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTCert() {
    if (tCertEClass == null) {
      tCertEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(55);
    }
    return tCertEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTCert_CommonName() {
        return (EAttribute)getTCert().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTCert_IdHierarchy() {
        return (EAttribute)getTCert().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTCertificate() {
    if (tCertificateEClass == null) {
      tCertificateEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(56);
    }
    return tCertificateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTCertificate_Subject() {
        return (EReference)getTCertificate().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTCertificate_IssuerName() {
        return (EReference)getTCertificate().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTCertificate_SerialNumber() {
        return (EAttribute)getTCertificate().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTCertificate_XferNumber() {
        return (EAttribute)getTCertificate().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTClientLN() {
    if (tClientLNEClass == null) {
      tClientLNEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(57);
    }
    return tClientLNEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTClientLN_ApRef() {
        return (EAttribute)getTClientLN().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTClientLN_Desc() {
        return (EAttribute)getTClientLN().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTClientLN_IedName() {
        return (EAttribute)getTClientLN().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTClientLN_LdInst() {
        return (EAttribute)getTClientLN().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTClientLN_LnClass() {
        return (EAttribute)getTClientLN().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTClientLN_LnInst() {
        return (EAttribute)getTClientLN().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTClientLN_Prefix() {
        return (EAttribute)getTClientLN().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTClientServices() {
    if (tClientServicesEClass == null) {
      tClientServicesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(58);
    }
    return tClientServicesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTClientServices_BufReport() {
        return (EAttribute)getTClientServices().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTClientServices_Goose() {
        return (EAttribute)getTClientServices().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTClientServices_Gsse() {
        return (EAttribute)getTClientServices().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTClientServices_ReadLog() {
        return (EAttribute)getTClientServices().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTClientServices_Sv() {
        return (EAttribute)getTClientServices().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTClientServices_UnbufReport() {
        return (EAttribute)getTClientServices().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTCommunication() {
    if (tCommunicationEClass == null) {
      tCommunicationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(60);
    }
    return tCommunicationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTCommunication_SubNetwork() {
        return (EReference)getTCommunication().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTConductingEquipment() {
    if (tConductingEquipmentEClass == null) {
      tConductingEquipmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(61);
    }
    return tConductingEquipmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTConductingEquipment_Type() {
        return (EAttribute)getTConductingEquipment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTConfLNs() {
    if (tConfLNsEClass == null) {
      tConfLNsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(62);
    }
    return tConfLNsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTConfLNs_FixLnInst() {
        return (EAttribute)getTConfLNs().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTConfLNs_FixPrefix() {
        return (EAttribute)getTConfLNs().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTConnectedAP() {
    if (tConnectedAPEClass == null) {
      tConnectedAPEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(63);
    }
    return tConnectedAPEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTConnectedAP_Address() {
        return (EReference)getTConnectedAP().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTConnectedAP_GSE() {
        return (EReference)getTConnectedAP().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTConnectedAP_SMV() {
        return (EReference)getTConnectedAP().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTConnectedAP_PhysConn() {
        return (EReference)getTConnectedAP().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTConnectedAP_ApName() {
        return (EAttribute)getTConnectedAP().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTConnectedAP_IedName() {
        return (EAttribute)getTConnectedAP().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTConnectivityNode() {
    if (tConnectivityNodeEClass == null) {
      tConnectivityNodeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(64);
    }
    return tConnectivityNodeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTConnectivityNode_PathName() {
        return (EAttribute)getTConnectivityNode().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTControl() {
    if (tControlEClass == null) {
      tControlEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(65);
    }
    return tControlEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTControl_DatSet() {
        return (EAttribute)getTControl().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTControl_Name() {
        return (EAttribute)getTControl().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTControlBlock() {
    if (tControlBlockEClass == null) {
      tControlBlockEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(66);
    }
    return tControlBlockEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTControlBlock_Address() {
        return (EReference)getTControlBlock().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTControlBlock_CbName() {
        return (EAttribute)getTControlBlock().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTControlBlock_LdInst() {
        return (EAttribute)getTControlBlock().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTControlWithIEDName() {
    if (tControlWithIEDNameEClass == null) {
      tControlWithIEDNameEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(67);
    }
    return tControlWithIEDNameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTControlWithIEDName_IEDName() {
        return (EReference)getTControlWithIEDName().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTControlWithIEDName_ConfRev() {
        return (EAttribute)getTControlWithIEDName().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTControlWithTriggerOpt() {
    if (tControlWithTriggerOptEClass == null) {
      tControlWithTriggerOptEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(68);
    }
    return tControlWithTriggerOptEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTControlWithTriggerOpt_TrgOps() {
        return (EReference)getTControlWithTriggerOpt().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTControlWithTriggerOpt_IntgPd() {
        return (EAttribute)getTControlWithTriggerOpt().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTDA() {
    if (tdaEClass == null) {
      tdaEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(69);
    }
    return tdaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDA_Dchg() {
        return (EAttribute)getTDA().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDA_Dupd() {
        return (EAttribute)getTDA().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDA_Fc() {
        return (EAttribute)getTDA().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDA_Qchg() {
        return (EAttribute)getTDA().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTDAI() {
    if (tdaiEClass == null) {
      tdaiEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(71);
    }
    return tdaiEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTDAI_Val() {
        return (EReference)getTDAI().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDAI_Ix() {
        return (EAttribute)getTDAI().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDAI_Name() {
        return (EAttribute)getTDAI().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDAI_SAddr() {
        return (EAttribute)getTDAI().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDAI_ValKind() {
        return (EAttribute)getTDAI().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTDataSet() {
    if (tDataSetEClass == null) {
      tDataSetEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(73);
    }
    return tDataSetEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDataSet_Group() {
        return (EAttribute)getTDataSet().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTDataSet_FCDA() {
        return (EReference)getTDataSet().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDataSet_Name() {
        return (EAttribute)getTDataSet().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTDataTypeTemplates() {
    if (tDataTypeTemplatesEClass == null) {
      tDataTypeTemplatesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(75);
    }
    return tDataTypeTemplatesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTDataTypeTemplates_LNodeType() {
        return (EReference)getTDataTypeTemplates().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTDataTypeTemplates_DOType() {
        return (EReference)getTDataTypeTemplates().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTDataTypeTemplates_DAType() {
        return (EReference)getTDataTypeTemplates().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTDataTypeTemplates_EnumType() {
        return (EReference)getTDataTypeTemplates().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTDAType() {
    if (tdaTypeEClass == null) {
      tdaTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(76);
    }
    return tdaTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTDAType_BDA() {
        return (EReference)getTDAType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTDAType_ProtNs() {
        return (EReference)getTDAType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDAType_IedType() {
        return (EAttribute)getTDAType().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTDO() {
    if (tdoEClass == null) {
      tdoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(77);
    }
    return tdoEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDO_AccessControl() {
        return (EAttribute)getTDO().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDO_Name() {
        return (EAttribute)getTDO().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDO_Transient() {
        return (EAttribute)getTDO().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDO_Type() {
        return (EAttribute)getTDO().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTDOI() {
    if (tdoiEClass == null) {
      tdoiEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(78);
    }
    return tdoiEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDOI_Group() {
        return (EAttribute)getTDOI().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTDOI_SDI() {
        return (EReference)getTDOI().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTDOI_DAI() {
        return (EReference)getTDOI().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDOI_AccessControl() {
        return (EAttribute)getTDOI().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDOI_Ix() {
        return (EAttribute)getTDOI().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDOI_Name() {
        return (EAttribute)getTDOI().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTDOType() {
    if (tdoTypeEClass == null) {
      tdoTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(104);
    }
    return tdoTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDOType_Group() {
        return (EAttribute)getTDOType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTDOType_SDO() {
        return (EReference)getTDOType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTDOType_DA() {
        return (EReference)getTDOType().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDOType_Cdc() {
        return (EAttribute)getTDOType().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDOType_IedType() {
        return (EAttribute)getTDOType().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTDurationInMilliSec() {
    if (tDurationInMilliSecEClass == null) {
      tDurationInMilliSecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(105);
    }
    return tDurationInMilliSecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDurationInMilliSec_Value() {
        return (EAttribute)getTDurationInMilliSec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDurationInMilliSec_Multiplier() {
        return (EAttribute)getTDurationInMilliSec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTDurationInMilliSec_Unit() {
        return (EAttribute)getTDurationInMilliSec().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTDurationInSec() {
    if (tDurationInSecEClass == null) {
      tDurationInSecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(106);
    }
    return tDurationInSecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTEnumType() {
    if (tEnumTypeEClass == null) {
      tEnumTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(108);
    }
    return tEnumTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTEnumType_EnumVal() {
        return (EReference)getTEnumType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTEnumVal() {
    if (tEnumValEClass == null) {
      tEnumValEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(109);
    }
    return tEnumValEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTEnumVal_Value() {
        return (EAttribute)getTEnumVal().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTEnumVal_Desc() {
        return (EAttribute)getTEnumVal().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTEnumVal_Ord() {
        return (EAttribute)getTEnumVal().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTEquipment() {
    if (tEquipmentEClass == null) {
      tEquipmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(110);
    }
    return tEquipmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTEquipment_Virtual() {
        return (EAttribute)getTEquipment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTEquipmentContainer() {
    if (tEquipmentContainerEClass == null) {
      tEquipmentContainerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(111);
    }
    return tEquipmentContainerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTEquipmentContainer_PowerTransformer() {
        return (EReference)getTEquipmentContainer().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTEquipmentContainer_GeneralEquipment() {
        return (EReference)getTEquipmentContainer().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTExtRef() {
    if (tExtRefEClass == null) {
      tExtRefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(118);
    }
    return tExtRefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_DaName() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_Desc() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_DoName() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_IedName() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_IntAddr() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_LdInst() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_LnClass() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_LnInst() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_Prefix() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_ServiceType() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_SrcCBName() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(10);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_SrcLDInst() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(11);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_SrcLNClass() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(12);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_SrcLNInst() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(13);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTExtRef_SrcPrefix() {
        return (EAttribute)getTExtRef().getEStructuralFeatures().get(14);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTFCDA() {
    if (tfcdaEClass == null) {
      tfcdaEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(119);
    }
    return tfcdaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTFCDA_DaName() {
        return (EAttribute)getTFCDA().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTFCDA_DoName() {
        return (EAttribute)getTFCDA().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTFCDA_Fc() {
        return (EAttribute)getTFCDA().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTFCDA_Ix() {
        return (EAttribute)getTFCDA().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTFCDA_LdInst() {
        return (EAttribute)getTFCDA().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTFCDA_LnClass() {
        return (EAttribute)getTFCDA().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTFCDA_LnInst() {
        return (EAttribute)getTFCDA().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTFCDA_Prefix() {
        return (EAttribute)getTFCDA().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTFunction() {
    if (tFunctionEClass == null) {
      tFunctionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(124);
    }
    return tFunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTFunction_SubFunction() {
        return (EReference)getTFunction().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTFunction_GeneralEquipment() {
        return (EReference)getTFunction().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTFunction_ConductingEquipment() {
        return (EReference)getTFunction().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTFunction_Type() {
        return (EAttribute)getTFunction().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTGeneralEquipment() {
    if (tGeneralEquipmentEClass == null) {
      tGeneralEquipmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(125);
    }
    return tGeneralEquipmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTGeneralEquipment_Type() {
        return (EAttribute)getTGeneralEquipment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTGSE() {
    if (tgseEClass == null) {
      tgseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(127);
    }
    return tgseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTGSE_MinTime() {
        return (EReference)getTGSE().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTGSE_MaxTime() {
        return (EReference)getTGSE().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTGSEControl() {
    if (tgseControlEClass == null) {
      tgseControlEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(128);
    }
    return tgseControlEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTGSEControl_AppID() {
        return (EAttribute)getTGSEControl().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTGSEControl_FixedOffs() {
        return (EAttribute)getTGSEControl().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTGSEControl_Type() {
        return (EAttribute)getTGSEControl().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTGSESettings() {
    if (tgseSettingsEClass == null) {
      tgseSettingsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(131);
    }
    return tgseSettingsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTGSESettings_AppID() {
        return (EAttribute)getTGSESettings().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTGSESettings_DataLabel() {
        return (EAttribute)getTGSESettings().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTHeader() {
    if (tHeaderEClass == null) {
      tHeaderEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(132);
    }
    return tHeaderEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTHeader_Text() {
        return (EReference)getTHeader().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTHeader_History() {
        return (EReference)getTHeader().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTHeader_Id() {
        return (EAttribute)getTHeader().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTHeader_NameStructure() {
        return (EAttribute)getTHeader().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTHeader_Revision() {
        return (EAttribute)getTHeader().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTHeader_ToolID() {
        return (EAttribute)getTHeader().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTHeader_Version() {
        return (EAttribute)getTHeader().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTHitem() {
    if (tHitemEClass == null) {
      tHitemEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(133);
    }
    return tHitemEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTHitem_Revision() {
        return (EAttribute)getTHitem().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTHitem_Version() {
        return (EAttribute)getTHitem().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTHitem_What() {
        return (EAttribute)getTHitem().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTHitem_When() {
        return (EAttribute)getTHitem().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTHitem_Who() {
        return (EAttribute)getTHitem().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTHitem_Why() {
        return (EAttribute)getTHitem().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTIDNaming() {
    if (tidNamingEClass == null) {
      tidNamingEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(134);
    }
    return tidNamingEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTIDNaming_Desc() {
        return (EAttribute)getTIDNaming().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTIDNaming_Id() {
        return (EAttribute)getTIDNaming().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTIED() {
    if (tiedEClass == null) {
      tiedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(135);
    }
    return tiedEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTIED_Services() {
        return (EReference)getTIED().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTIED_AccessPoint() {
        return (EReference)getTIED().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTIED_ConfigVersion() {
        return (EAttribute)getTIED().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTIED_EngRight() {
        return (EAttribute)getTIED().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTIED_Manufacturer() {
        return (EAttribute)getTIED().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTIED_Name() {
        return (EAttribute)getTIED().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTIED_OriginalSclRevision() {
        return (EAttribute)getTIED().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTIED_OriginalSclVersion() {
        return (EAttribute)getTIED().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTIED_Owner() {
        return (EAttribute)getTIED().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTIED_Type() {
        return (EAttribute)getTIED().getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTInputs() {
    if (tInputsEClass == null) {
      tInputsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(137);
    }
    return tInputsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTInputs_ExtRef() {
        return (EReference)getTInputs().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTLDevice() {
    if (tlDeviceEClass == null) {
      tlDeviceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(138);
    }
    return tlDeviceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTLDevice_LN0() {
        return (EReference)getTLDevice().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTLDevice_LN() {
        return (EReference)getTLDevice().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTLDevice_AccessControl() {
        return (EReference)getTLDevice().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLDevice_Inst() {
        return (EAttribute)getTLDevice().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLDevice_LdName() {
        return (EAttribute)getTLDevice().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTLN() {
    if (tlnEClass == null) {
      tlnEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(144);
    }
    return tlnEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLN_Inst() {
        return (EAttribute)getTLN().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLN_LnClass() {
        return (EAttribute)getTLN().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLN_Prefix() {
        return (EAttribute)getTLN().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTLN0() {
    if (tln0EClass == null) {
      tln0EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(145);
    }
    return tln0EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTLN0_GSEControl() {
        return (EReference)getTLN0().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTLN0_SampledValueControl() {
        return (EReference)getTLN0().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTLN0_SettingControl() {
        return (EReference)getTLN0().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLN0_Inst() {
        return (EAttribute)getTLN0().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLN0_LnClass() {
        return (EAttribute)getTLN0().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTLNode() {
    if (tlNodeEClass == null) {
      tlNodeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(149);
    }
    return tlNodeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLNode_IedName() {
        return (EAttribute)getTLNode().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLNode_LdInst() {
        return (EAttribute)getTLNode().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLNode_LnClass() {
        return (EAttribute)getTLNode().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLNode_LnInst() {
        return (EAttribute)getTLNode().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLNode_LnType() {
        return (EAttribute)getTLNode().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLNode_Prefix() {
        return (EAttribute)getTLNode().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTLNodeContainer() {
    if (tlNodeContainerEClass == null) {
      tlNodeContainerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(150);
    }
    return tlNodeContainerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTLNodeContainer_LNode() {
        return (EReference)getTLNodeContainer().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTLNodeType() {
    if (tlNodeTypeEClass == null) {
      tlNodeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(151);
    }
    return tlNodeTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTLNodeType_DO() {
        return (EReference)getTLNodeType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLNodeType_IedType() {
        return (EAttribute)getTLNodeType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLNodeType_LnClass() {
        return (EAttribute)getTLNodeType().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTLog() {
    if (tLogEClass == null) {
      tLogEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(152);
    }
    return tLogEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLog_Name() {
        return (EAttribute)getTLog().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTLogControl() {
    if (tLogControlEClass == null) {
      tLogControlEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(153);
    }
    return tLogControlEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLogControl_BufTime() {
        return (EAttribute)getTLogControl().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLogControl_LdInst() {
        return (EAttribute)getTLogControl().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLogControl_LnClass() {
        return (EAttribute)getTLogControl().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLogControl_LnInst() {
        return (EAttribute)getTLogControl().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLogControl_LogEna() {
        return (EAttribute)getTLogControl().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLogControl_LogName() {
        return (EAttribute)getTLogControl().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLogControl_Prefix() {
        return (EAttribute)getTLogControl().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLogControl_ReasonCode() {
        return (EAttribute)getTLogControl().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTLogSettings() {
    if (tLogSettingsEClass == null) {
      tLogSettingsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(155);
    }
    return tLogSettingsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLogSettings_IntgPd() {
        return (EAttribute)getTLogSettings().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLogSettings_LogEna() {
        return (EAttribute)getTLogSettings().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTLogSettings_TrgOps() {
        return (EAttribute)getTLogSettings().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTNaming() {
    if (tNamingEClass == null) {
      tNamingEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(159);
    }
    return tNamingEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTNaming_Desc() {
        return (EAttribute)getTNaming().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTNaming_Name() {
        return (EAttribute)getTNaming().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTP() {
    if (tpEClass == null) {
      tpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(160);
    }
    return tpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTP_Value() {
        return (EAttribute)getTP().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTP_Type() {
        return (EAttribute)getTP().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPAPPID() {
    if (tpappidEClass == null) {
      tpappidEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(162);
    }
    return tpappidEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPhysConn() {
    if (tPhysConnEClass == null) {
      tPhysConnEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(165);
    }
    return tPhysConnEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTPhysConn_P() {
        return (EReference)getTPhysConn().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTPhysConn_Type() {
        return (EAttribute)getTPhysConn().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPIP() {
    if (tpipEClass == null) {
      tpipEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(167);
    }
    return tpipEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPIPGATEWAY() {
    if (tpipgatewayEClass == null) {
      tpipgatewayEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(168);
    }
    return tpipgatewayEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPIPSUBNET() {
    if (tpipsubnetEClass == null) {
      tpipsubnetEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(169);
    }
    return tpipsubnetEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPMACAddress() {
    if (tpmacAddressEClass == null) {
      tpmacAddressEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(170);
    }
    return tpmacAddressEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPMMSPort() {
    if (tpmmsPortEClass == null) {
      tpmmsPortEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(171);
    }
    return tpmmsPortEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPOSIAEInvoke() {
    if (tposiaeInvokeEClass == null) {
      tposiaeInvokeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(172);
    }
    return tposiaeInvokeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPOSIAEQualifier() {
    if (tposiaeQualifierEClass == null) {
      tposiaeQualifierEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(173);
    }
    return tposiaeQualifierEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPOSIAPInvoke() {
    if (tposiapInvokeEClass == null) {
      tposiapInvokeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(174);
    }
    return tposiapInvokeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPOSIAPTitle() {
    if (tposiapTitleEClass == null) {
      tposiapTitleEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(175);
    }
    return tposiapTitleEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPOSINSAP() {
    if (tposinsapEClass == null) {
      tposinsapEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(176);
    }
    return tposinsapEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPOSIPSEL() {
    if (tposipselEClass == null) {
      tposipselEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(177);
    }
    return tposipselEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPOSISSEL() {
    if (tposisselEClass == null) {
      tposisselEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(178);
    }
    return tposisselEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPOSITSEL() {
    if (tpositselEClass == null) {
      tpositselEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(179);
    }
    return tpositselEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPowerSystemResource() {
    if (tPowerSystemResourceEClass == null) {
      tPowerSystemResourceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(180);
    }
    return tPowerSystemResourceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPowerTransformer() {
    if (tPowerTransformerEClass == null) {
      tPowerTransformerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(181);
    }
    return tPowerTransformerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTPowerTransformer_TransformerWinding() {
        return (EReference)getTPowerTransformer().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTPowerTransformer_Type() {
        return (EAttribute)getTPowerTransformer().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPPhysConn() {
    if (tpPhysConnEClass == null) {
      tpPhysConnEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(184);
    }
    return tpPhysConnEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTPPhysConn_Value() {
        return (EAttribute)getTPPhysConn().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTPPhysConn_Type() {
        return (EAttribute)getTPPhysConn().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPPort() {
    if (tpPortEClass == null) {
      tpPortEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(185);
    }
    return tpPortEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPrivate() {
    if (tPrivateEClass == null) {
      tPrivateEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(204);
    }
    return tPrivateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTPrivate_Source() {
        return (EAttribute)getTPrivate().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTPrivate_Type() {
        return (EAttribute)getTPrivate().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPSNTPPort() {
    if (tpsntpPortEClass == null) {
      tpsntpPortEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(205);
    }
    return tpsntpPortEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPVLANID() {
    if (tpvlanidEClass == null) {
      tpvlanidEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(208);
    }
    return tpvlanidEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTPVLANPRIORITY() {
    if (tpvlanpriorityEClass == null) {
      tpvlanpriorityEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(209);
    }
    return tpvlanpriorityEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTReportControl() {
    if (tReportControlEClass == null) {
      tReportControlEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(211);
    }
    return tReportControlEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTReportControl_OptFields() {
        return (EReference)getTReportControl().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTReportControl_RptEnabled() {
        return (EReference)getTReportControl().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTReportControl_Buffered() {
        return (EAttribute)getTReportControl().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTReportControl_BufTime() {
        return (EAttribute)getTReportControl().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTReportControl_ConfRev() {
        return (EAttribute)getTReportControl().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTReportControl_Indexed() {
        return (EAttribute)getTReportControl().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTReportControl_RptID() {
        return (EAttribute)getTReportControl().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTReportSettings() {
    if (tReportSettingsEClass == null) {
      tReportSettingsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(212);
    }
    return tReportSettingsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTReportSettings_BufTime() {
        return (EAttribute)getTReportSettings().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTReportSettings_IntgPd() {
        return (EAttribute)getTReportSettings().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTReportSettings_OptFields() {
        return (EAttribute)getTReportSettings().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTReportSettings_ResvTms() {
        return (EAttribute)getTReportSettings().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTReportSettings_RptID() {
        return (EAttribute)getTReportSettings().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTReportSettings_TrgOps() {
        return (EAttribute)getTReportSettings().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTRptEnabled() {
    if (tRptEnabledEClass == null) {
      tRptEnabledEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(217);
    }
    return tRptEnabledEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTRptEnabled_ClientLN() {
        return (EReference)getTRptEnabled().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTRptEnabled_Max() {
        return (EAttribute)getTRptEnabled().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTSampledValueControl() {
    if (tSampledValueControlEClass == null) {
      tSampledValueControlEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(219);
    }
    return tSampledValueControlEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTSampledValueControl_SmvOpts() {
        return (EReference)getTSampledValueControl().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSampledValueControl_Multicast() {
        return (EAttribute)getTSampledValueControl().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSampledValueControl_NofASDU() {
        return (EAttribute)getTSampledValueControl().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSampledValueControl_SmpMod() {
        return (EAttribute)getTSampledValueControl().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSampledValueControl_SmpRate() {
        return (EAttribute)getTSampledValueControl().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSampledValueControl_SmvID() {
        return (EAttribute)getTSampledValueControl().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTSDI() {
    if (tsdiEClass == null) {
      tsdiEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(222);
    }
    return tsdiEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSDI_Group() {
        return (EAttribute)getTSDI().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTSDI_SDI() {
        return (EReference)getTSDI().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTSDI_DAI() {
        return (EReference)getTSDI().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSDI_Ix() {
        return (EAttribute)getTSDI().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSDI_Name() {
        return (EAttribute)getTSDI().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTSDO() {
    if (tsdoEClass == null) {
      tsdoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(223);
    }
    return tsdoEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSDO_Count() {
        return (EAttribute)getTSDO().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSDO_Name() {
        return (EAttribute)getTSDO().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSDO_Type() {
        return (EAttribute)getTSDO().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTServer() {
    if (tServerEClass == null) {
      tServerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(225);
    }
    return tServerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServer_Authentication() {
        return (EReference)getTServer().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServer_LDevice() {
        return (EReference)getTServer().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServer_Association() {
        return (EReference)getTServer().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTServer_Timeout() {
        return (EAttribute)getTServer().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTServerAt() {
    if (tServerAtEClass == null) {
      tServerAtEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(226);
    }
    return tServerAtEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTServerAt_ApName() {
        return (EAttribute)getTServerAt().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTServiceConfReportControl() {
    if (tServiceConfReportControlEClass == null) {
      tServiceConfReportControlEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(227);
    }
    return tServiceConfReportControlEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTServiceConfReportControl_BufConf() {
        return (EAttribute)getTServiceConfReportControl().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTServiceConfReportControl_BufMode() {
        return (EAttribute)getTServiceConfReportControl().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTServiceForConfDataSet() {
    if (tServiceForConfDataSetEClass == null) {
      tServiceForConfDataSetEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(228);
    }
    return tServiceForConfDataSetEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTServiceForConfDataSet_Modify() {
        return (EAttribute)getTServiceForConfDataSet().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTServices() {
    if (tServicesEClass == null) {
      tServicesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(229);
    }
    return tServicesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_DynAssociation() {
        return (EReference)getTServices().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_SettingGroups() {
        return (EReference)getTServices().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_GetDirectory() {
        return (EReference)getTServices().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_GetDataObjectDefinition() {
        return (EReference)getTServices().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_DataObjectDirectory() {
        return (EReference)getTServices().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_GetDataSetValue() {
        return (EReference)getTServices().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_SetDataSetValue() {
        return (EReference)getTServices().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_DataSetDirectory() {
        return (EReference)getTServices().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_ConfDataSet() {
        return (EReference)getTServices().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_DynDataSet() {
        return (EReference)getTServices().getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_ReadWrite() {
        return (EReference)getTServices().getEStructuralFeatures().get(10);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_TimerActivatedControl() {
        return (EReference)getTServices().getEStructuralFeatures().get(11);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_ConfReportControl() {
        return (EReference)getTServices().getEStructuralFeatures().get(12);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_GetCBValues() {
        return (EReference)getTServices().getEStructuralFeatures().get(13);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_ConfLogControl() {
        return (EReference)getTServices().getEStructuralFeatures().get(14);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_ReportSettings() {
        return (EReference)getTServices().getEStructuralFeatures().get(15);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_LogSettings() {
        return (EReference)getTServices().getEStructuralFeatures().get(16);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_GSESettings() {
        return (EReference)getTServices().getEStructuralFeatures().get(17);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_SMVSettings() {
        return (EReference)getTServices().getEStructuralFeatures().get(18);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_GSEDir() {
        return (EReference)getTServices().getEStructuralFeatures().get(19);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_GOOSE() {
        return (EReference)getTServices().getEStructuralFeatures().get(20);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_GSSE() {
        return (EReference)getTServices().getEStructuralFeatures().get(21);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_SMVsc() {
        return (EReference)getTServices().getEStructuralFeatures().get(22);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_FileHandling() {
        return (EReference)getTServices().getEStructuralFeatures().get(23);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_ConfLNs() {
        return (EReference)getTServices().getEStructuralFeatures().get(24);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_ClientServices() {
        return (EReference)getTServices().getEStructuralFeatures().get(25);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_ConfLdName() {
        return (EReference)getTServices().getEStructuralFeatures().get(26);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_SupSubscription() {
        return (EReference)getTServices().getEStructuralFeatures().get(27);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTServices_ConfSigRef() {
        return (EReference)getTServices().getEStructuralFeatures().get(28);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTServices_NameLength() {
        return (EAttribute)getTServices().getEStructuralFeatures().get(29);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTServiceSettings() {
    if (tServiceSettingsEClass == null) {
      tServiceSettingsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(230);
    }
    return tServiceSettingsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTServiceSettings_CbName() {
        return (EAttribute)getTServiceSettings().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTServiceSettings_DatSet() {
        return (EAttribute)getTServiceSettings().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTServiceWithMax() {
    if (tServiceWithMaxEClass == null) {
      tServiceWithMaxEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(235);
    }
    return tServiceWithMaxEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTServiceWithMax_Max() {
        return (EAttribute)getTServiceWithMax().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTServiceWithMaxAndMaxAttributes() {
    if (tServiceWithMaxAndMaxAttributesEClass == null) {
      tServiceWithMaxAndMaxAttributesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(236);
    }
    return tServiceWithMaxAndMaxAttributesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTServiceWithMaxAndMaxAttributes_MaxAttributes() {
        return (EAttribute)getTServiceWithMaxAndMaxAttributes().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTServiceWithMaxAndModify() {
    if (tServiceWithMaxAndModifyEClass == null) {
      tServiceWithMaxAndModifyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(237);
    }
    return tServiceWithMaxAndModifyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTServiceWithMaxAndModify_Modify() {
        return (EAttribute)getTServiceWithMaxAndModify().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTServiceWithOptionalMax() {
    if (tServiceWithOptionalMaxEClass == null) {
      tServiceWithOptionalMaxEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(238);
    }
    return tServiceWithOptionalMaxEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTServiceWithOptionalMax_Max() {
        return (EAttribute)getTServiceWithOptionalMax().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTServiceYesNo() {
    if (tServiceYesNoEClass == null) {
      tServiceYesNoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(239);
    }
    return tServiceYesNoEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTSettingControl() {
    if (tSettingControlEClass == null) {
      tSettingControlEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(240);
    }
    return tSettingControlEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSettingControl_ActSG() {
        return (EAttribute)getTSettingControl().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSettingControl_NumOfSGs() {
        return (EAttribute)getTSettingControl().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTSMV() {
    if (tsmvEClass == null) {
      tsmvEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(245);
    }
    return tsmvEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTSMVSettings() {
    if (tsmvSettingsEClass == null) {
      tsmvSettingsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(246);
    }
    return tsmvSettingsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSMVSettings_Group() {
        return (EAttribute)getTSMVSettings().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSMVSettings_SmpRate() {
        return (EAttribute)getTSMVSettings().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSMVSettings_SamplesPerSec() {
        return (EAttribute)getTSMVSettings().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSMVSettings_SecPerSamples() {
        return (EAttribute)getTSMVSettings().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSMVSettings_OptFields() {
        return (EAttribute)getTSMVSettings().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSMVSettings_SamplesPerSec1() {
        return (EAttribute)getTSMVSettings().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSMVSettings_SmpRate1() {
        return (EAttribute)getTSMVSettings().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSMVSettings_SvID() {
        return (EAttribute)getTSMVSettings().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTSubEquipment() {
    if (tSubEquipmentEClass == null) {
      tSubEquipmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(247);
    }
    return tSubEquipmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSubEquipment_Phase() {
        return (EAttribute)getTSubEquipment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSubEquipment_Virtual() {
        return (EAttribute)getTSubEquipment().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTSubFunction() {
    if (tSubFunctionEClass == null) {
      tSubFunctionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(248);
    }
    return tSubFunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTSubFunction_GeneralEquipment() {
        return (EReference)getTSubFunction().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTSubFunction_ConductingEquipment() {
        return (EReference)getTSubFunction().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSubFunction_Type() {
        return (EAttribute)getTSubFunction().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTSubNetwork() {
    if (tSubNetworkEClass == null) {
      tSubNetworkEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(249);
    }
    return tSubNetworkEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTSubNetwork_BitRate() {
        return (EReference)getTSubNetwork().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTSubNetwork_ConnectedAP() {
        return (EReference)getTSubNetwork().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTSubNetwork_Type() {
        return (EAttribute)getTSubNetwork().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTSubstation() {
    if (tSubstationEClass == null) {
      tSubstationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(250);
    }
    return tSubstationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTSubstation_VoltageLevel() {
        return (EReference)getTSubstation().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTSubstation_Function() {
        return (EReference)getTSubstation().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTTapChanger() {
    if (tTapChangerEClass == null) {
      tTapChangerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(251);
    }
    return tTapChangerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTapChanger_Type() {
        return (EAttribute)getTTapChanger().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTapChanger_Virtual() {
        return (EAttribute)getTTapChanger().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTTerminal() {
    if (tTerminalEClass == null) {
      tTerminalEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(252);
    }
    return tTerminalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTerminal_BayName() {
        return (EAttribute)getTTerminal().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTerminal_CNodeName() {
        return (EAttribute)getTTerminal().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTerminal_ConnectivityNode() {
        return (EAttribute)getTTerminal().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTerminal_Name() {
        return (EAttribute)getTTerminal().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTerminal_NeutralPoint() {
        return (EAttribute)getTTerminal().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTerminal_SubstationName() {
        return (EAttribute)getTTerminal().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTerminal_VoltageLevelName() {
        return (EAttribute)getTTerminal().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTText() {
    if (tTextEClass == null) {
      tTextEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(253);
    }
    return tTextEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTText_Source() {
        return (EAttribute)getTText().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTTransformerWinding() {
    if (tTransformerWindingEClass == null) {
      tTransformerWindingEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(254);
    }
    return tTransformerWindingEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTTransformerWinding_TapChanger() {
        return (EReference)getTTransformerWinding().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTransformerWinding_Type() {
        return (EAttribute)getTTransformerWinding().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTTrgOps() {
    if (tTrgOpsEClass == null) {
      tTrgOpsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(257);
    }
    return tTrgOpsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTrgOps_Dchg() {
        return (EAttribute)getTTrgOps().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTrgOps_Dupd() {
        return (EAttribute)getTTrgOps().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTrgOps_Gi() {
        return (EAttribute)getTTrgOps().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTrgOps_Period() {
        return (EAttribute)getTTrgOps().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTrgOps_Qchg() {
        return (EAttribute)getTTrgOps().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTUnNaming() {
    if (tUnNamingEClass == null) {
      tUnNamingEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(260);
    }
    return tUnNamingEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTUnNaming_Desc() {
        return (EAttribute)getTUnNaming().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTVal() {
    if (tValEClass == null) {
      tValEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(261);
    }
    return tValEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTVal_Value() {
        return (EAttribute)getTVal().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTVal_SGroup() {
        return (EAttribute)getTVal().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTValueWithUnit() {
    if (tValueWithUnitEClass == null) {
      tValueWithUnitEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(264);
    }
    return tValueWithUnitEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTValueWithUnit_Value() {
        return (EAttribute)getTValueWithUnit().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTValueWithUnit_Multiplier() {
        return (EAttribute)getTValueWithUnit().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTValueWithUnit_Unit() {
        return (EAttribute)getTValueWithUnit().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTVoltage() {
    if (tVoltageEClass == null) {
      tVoltageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(265);
    }
    return tVoltageEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTVoltageLevel() {
    if (tVoltageLevelEClass == null) {
      tVoltageLevelEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(266);
    }
    return tVoltageLevelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTVoltageLevel_Voltage() {
        return (EReference)getTVoltageLevel().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTVoltageLevel_Bay() {
        return (EReference)getTVoltageLevel().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTVoltageLevel_Function() {
        return (EReference)getTVoltageLevel().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getBufModeType() {
    if (bufModeTypeEEnum == null) {
      bufModeTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(4);
    }
    return bufModeTypeEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getNameStructureType() {
    if (nameStructureTypeEEnum == null) {
      nameStructureTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(14);
    }
    return nameStructureTypeEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTAssociationKindEnum() {
    if (tAssociationKindEnumEEnum == null) {
      tAssociationKindEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(43);
    }
    return tAssociationKindEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTAuthenticationEnum() {
    if (tAuthenticationEnumEEnum == null) {
      tAuthenticationEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(46);
    }
    return tAuthenticationEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTDomainLNGroupAEnum() {
    if (tDomainLNGroupAEnumEEnum == null) {
      tDomainLNGroupAEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(80);
    }
    return tDomainLNGroupAEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTDomainLNGroupCEnum() {
    if (tDomainLNGroupCEnumEEnum == null) {
      tDomainLNGroupCEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(82);
    }
    return tDomainLNGroupCEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTDomainLNGroupGEnum() {
    if (tDomainLNGroupGEnumEEnum == null) {
      tDomainLNGroupGEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(84);
    }
    return tDomainLNGroupGEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTDomainLNGroupIEnum() {
    if (tDomainLNGroupIEnumEEnum == null) {
      tDomainLNGroupIEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(86);
    }
    return tDomainLNGroupIEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTDomainLNGroupMEnum() {
    if (tDomainLNGroupMEnumEEnum == null) {
      tDomainLNGroupMEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(88);
    }
    return tDomainLNGroupMEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTDomainLNGroupPEnum() {
    if (tDomainLNGroupPEnumEEnum == null) {
      tDomainLNGroupPEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(90);
    }
    return tDomainLNGroupPEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTDomainLNGroupREnum() {
    if (tDomainLNGroupREnumEEnum == null) {
      tDomainLNGroupREnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(92);
    }
    return tDomainLNGroupREnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTDomainLNGroupSEnum() {
    if (tDomainLNGroupSEnumEEnum == null) {
      tDomainLNGroupSEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(94);
    }
    return tDomainLNGroupSEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTDomainLNGroupTEnum() {
    if (tDomainLNGroupTEnumEEnum == null) {
      tDomainLNGroupTEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(96);
    }
    return tDomainLNGroupTEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTDomainLNGroupXEnum() {
    if (tDomainLNGroupXEnumEEnum == null) {
      tDomainLNGroupXEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(98);
    }
    return tDomainLNGroupXEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTDomainLNGroupYEnum() {
    if (tDomainLNGroupYEnumEEnum == null) {
      tDomainLNGroupYEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(100);
    }
    return tDomainLNGroupYEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTDomainLNGroupZEnum() {
    if (tDomainLNGroupZEnumEEnum == null) {
      tDomainLNGroupZEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(102);
    }
    return tDomainLNGroupZEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTFCEnum() {
    if (tfcEnumEEnum == null) {
      tfcEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(120);
    }
    return tfcEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTGSEControlTypeEnum() {
    if (tgseControlTypeEnumEEnum == null) {
      tgseControlTypeEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(129);
    }
    return tgseControlTypeEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTLLN0Enum() {
    if (tlln0EnumEEnum == null) {
      tlln0EnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(142);
    }
    return tlln0EnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTLPHDEnum() {
    if (tlphdEnumEEnum == null) {
      tlphdEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(156);
    }
    return tlphdEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTPhaseEnum() {
    if (tPhaseEnumEEnum == null) {
      tPhaseEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(163);
    }
    return tPhaseEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTPowerTransformerEnum() {
    if (tPowerTransformerEnumEEnum == null) {
      tPowerTransformerEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(182);
    }
    return tPowerTransformerEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTPredefinedAttributeNameEnum() {
    if (tPredefinedAttributeNameEnumEEnum == null) {
      tPredefinedAttributeNameEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(186);
    }
    return tPredefinedAttributeNameEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTPredefinedBasicTypeEnum() {
    if (tPredefinedBasicTypeEnumEEnum == null) {
      tPredefinedBasicTypeEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(188);
    }
    return tPredefinedBasicTypeEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTPredefinedCDCEnum() {
    if (tPredefinedCDCEnumEEnum == null) {
      tPredefinedCDCEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(190);
    }
    return tPredefinedCDCEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTPredefinedCommonConductingEquipmentEnum() {
    if (tPredefinedCommonConductingEquipmentEnumEEnum == null) {
      tPredefinedCommonConductingEquipmentEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(192);
    }
    return tPredefinedCommonConductingEquipmentEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTPredefinedGeneralEquipmentEnum() {
    if (tPredefinedGeneralEquipmentEnumEEnum == null) {
      tPredefinedGeneralEquipmentEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(194);
    }
    return tPredefinedGeneralEquipmentEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTPredefinedPhysConnTypeEnum() {
    if (tPredefinedPhysConnTypeEnumEEnum == null) {
      tPredefinedPhysConnTypeEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(197);
    }
    return tPredefinedPhysConnTypeEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTPredefinedPTypeEnum() {
    if (tPredefinedPTypeEnumEEnum == null) {
      tPredefinedPTypeEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(199);
    }
    return tPredefinedPTypeEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTPredefinedPTypePhysConnEnum() {
    if (tPredefinedPTypePhysConnEnumEEnum == null) {
      tPredefinedPTypePhysConnEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(201);
    }
    return tPredefinedPTypePhysConnEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTRightEnum() {
    if (tRightEnumEEnum == null) {
      tRightEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(215);
    }
    return tRightEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTServiceSettingsEnum() {
    if (tServiceSettingsEnumEEnum == null) {
      tServiceSettingsEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(231);
    }
    return tServiceSettingsEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTServiceType() {
    if (tServiceTypeEEnum == null) {
      tServiceTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(233);
    }
    return tServiceTypeEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTSIUnitEnum() {
    if (tsiUnitEnumEEnum == null) {
      tsiUnitEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(241);
    }
    return tsiUnitEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTSmpMod() {
    if (tSmpModEEnum == null) {
      tSmpModEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(243);
    }
    return tSmpModEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTTransformerWindingEnum() {
    if (tTransformerWindingEnumEEnum == null) {
      tTransformerWindingEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(255);
    }
    return tTransformerWindingEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTUnitMultiplierEnum() {
    if (tUnitMultiplierEnumEEnum == null) {
      tUnitMultiplierEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(258);
    }
    return tUnitMultiplierEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTValKindEnum() {
    if (tValKindEnumEEnum == null) {
      tValKindEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(262);
    }
    return tValKindEnumEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getActSGType() {
    if (actSGTypeEDataType == null) {
      actSGTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(0);
    }
    return actSGTypeEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getActSGTypeObject() {
    if (actSGTypeObjectEDataType == null) {
      actSGTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(1);
    }
    return actSGTypeObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getAppIDType() {
    if (appIDTypeEDataType == null) {
      appIDTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(2);
    }
    return appIDTypeEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getBufModeTypeObject() {
    if (bufModeTypeObjectEDataType == null) {
      bufModeTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(5);
    }
    return bufModeTypeObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getCommonNameType() {
    if (commonNameTypeEDataType == null) {
      commonNameTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(6);
    }
    return commonNameTypeEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getIdHierarchyType() {
    if (idHierarchyTypeEDataType == null) {
      idHierarchyTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(9);
    }
    return idHierarchyTypeEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getNameLengthType() {
    if (nameLengthTypeEDataType == null) {
      nameLengthTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(12);
    }
    return nameLengthTypeEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getNameLengthTypeObject() {
    if (nameLengthTypeObjectEDataType == null) {
      nameLengthTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(13);
    }
    return nameLengthTypeObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getNameStructureTypeObject() {
    if (nameStructureTypeObjectEDataType == null) {
      nameStructureTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(15);
    }
    return nameStructureTypeObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getNumOfSGsType() {
    if (numOfSGsTypeEDataType == null) {
      numOfSGsTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(16);
    }
    return numOfSGsTypeEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getNumOfSGsTypeObject() {
    if (numOfSGsTypeObjectEDataType == null) {
      numOfSGsTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(17);
    }
    return numOfSGsTypeObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getSamplesPerSecType() {
    if (samplesPerSecTypeEDataType == null) {
      samplesPerSecTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(20);
    }
    return samplesPerSecTypeEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getSamplesPerSecTypeObject() {
    if (samplesPerSecTypeObjectEDataType == null) {
      samplesPerSecTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(21);
    }
    return samplesPerSecTypeObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getSecPerSamplesType() {
    if (secPerSamplesTypeEDataType == null) {
      secPerSamplesTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(23);
    }
    return secPerSamplesTypeEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getSecPerSamplesTypeObject() {
    if (secPerSamplesTypeObjectEDataType == null) {
      secPerSamplesTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(24);
    }
    return secPerSamplesTypeObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getSerialNumberType() {
    if (serialNumberTypeEDataType == null) {
      serialNumberTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(25);
    }
    return serialNumberTypeEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getSmpRateType() {
    if (smpRateTypeEDataType == null) {
      smpRateTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(27);
    }
    return smpRateTypeEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getSmpRateTypeObject() {
    if (smpRateTypeObjectEDataType == null) {
      smpRateTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(28);
    }
    return smpRateTypeObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getSmvIDType() {
    if (smvIDTypeEDataType == null) {
      smvIDTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(29);
    }
    return smvIDTypeEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTAccessPointName() {
    if (tAccessPointNameEDataType == null) {
      tAccessPointNameEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(35);
    }
    return tAccessPointNameEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTAcsiName() {
    if (tAcsiNameEDataType == null) {
      tAcsiNameEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(36);
    }
    return tAcsiNameEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTAnyName() {
    if (tAnyNameEDataType == null) {
      tAnyNameEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(40);
    }
    return tAnyNameEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTAssociationID() {
    if (tAssociationIDEDataType == null) {
      tAssociationIDEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(42);
    }
    return tAssociationIDEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTAssociationKindEnumObject() {
    if (tAssociationKindEnumObjectEDataType == null) {
      tAssociationKindEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(44);
    }
    return tAssociationKindEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTAttributeNameEnum() {
    if (tAttributeNameEnumEDataType == null) {
      tAttributeNameEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(45);
    }
    return tAttributeNameEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTAuthenticationEnumObject() {
    if (tAuthenticationEnumObjectEDataType == null) {
      tAuthenticationEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(47);
    }
    return tAuthenticationEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTBasicTypeEnum() {
    if (tBasicTypeEnumEDataType == null) {
      tBasicTypeEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(49);
    }
    return tBasicTypeEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTCBName() {
    if (tcbNameEDataType == null) {
      tcbNameEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(53);
    }
    return tcbNameEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTCDCEnum() {
    if (tcdcEnumEDataType == null) {
      tcdcEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(54);
    }
    return tcdcEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTCommonConductingEquipmentEnum() {
    if (tCommonConductingEquipmentEnumEDataType == null) {
      tCommonConductingEquipmentEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(59);
    }
    return tCommonConductingEquipmentEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDACount() {
    if (tdaCountEDataType == null) {
      tdaCountEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(70);
    }
    return tdaCountEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDataName() {
    if (tDataNameEDataType == null) {
      tDataNameEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(72);
    }
    return tDataNameEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDataSetName() {
    if (tDataSetNameEDataType == null) {
      tDataSetNameEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(74);
    }
    return tDataSetNameEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDomainLNEnum() {
    if (tDomainLNEnumEDataType == null) {
      tDomainLNEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(79);
    }
    return tDomainLNEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDomainLNGroupAEnumObject() {
    if (tDomainLNGroupAEnumObjectEDataType == null) {
      tDomainLNGroupAEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(81);
    }
    return tDomainLNGroupAEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDomainLNGroupCEnumObject() {
    if (tDomainLNGroupCEnumObjectEDataType == null) {
      tDomainLNGroupCEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(83);
    }
    return tDomainLNGroupCEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDomainLNGroupGEnumObject() {
    if (tDomainLNGroupGEnumObjectEDataType == null) {
      tDomainLNGroupGEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(85);
    }
    return tDomainLNGroupGEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDomainLNGroupIEnumObject() {
    if (tDomainLNGroupIEnumObjectEDataType == null) {
      tDomainLNGroupIEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(87);
    }
    return tDomainLNGroupIEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDomainLNGroupMEnumObject() {
    if (tDomainLNGroupMEnumObjectEDataType == null) {
      tDomainLNGroupMEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(89);
    }
    return tDomainLNGroupMEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDomainLNGroupPEnumObject() {
    if (tDomainLNGroupPEnumObjectEDataType == null) {
      tDomainLNGroupPEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(91);
    }
    return tDomainLNGroupPEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDomainLNGroupREnumObject() {
    if (tDomainLNGroupREnumObjectEDataType == null) {
      tDomainLNGroupREnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(93);
    }
    return tDomainLNGroupREnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDomainLNGroupSEnumObject() {
    if (tDomainLNGroupSEnumObjectEDataType == null) {
      tDomainLNGroupSEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(95);
    }
    return tDomainLNGroupSEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDomainLNGroupTEnumObject() {
    if (tDomainLNGroupTEnumObjectEDataType == null) {
      tDomainLNGroupTEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(97);
    }
    return tDomainLNGroupTEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDomainLNGroupXEnumObject() {
    if (tDomainLNGroupXEnumObjectEDataType == null) {
      tDomainLNGroupXEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(99);
    }
    return tDomainLNGroupXEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDomainLNGroupYEnumObject() {
    if (tDomainLNGroupYEnumObjectEDataType == null) {
      tDomainLNGroupYEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(101);
    }
    return tDomainLNGroupYEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTDomainLNGroupZEnumObject() {
    if (tDomainLNGroupZEnumObjectEDataType == null) {
      tDomainLNGroupZEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(103);
    }
    return tDomainLNGroupZEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTEmpty() {
    if (tEmptyEDataType == null) {
      tEmptyEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(107);
    }
    return tEmptyEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTExtensionAttributeNameEnum() {
    if (tExtensionAttributeNameEnumEDataType == null) {
      tExtensionAttributeNameEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(112);
    }
    return tExtensionAttributeNameEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTExtensionEquipmentEnum() {
    if (tExtensionEquipmentEnumEDataType == null) {
      tExtensionEquipmentEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(113);
    }
    return tExtensionEquipmentEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTExtensionGeneralEquipmentEnum() {
    if (tExtensionGeneralEquipmentEnumEDataType == null) {
      tExtensionGeneralEquipmentEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(114);
    }
    return tExtensionGeneralEquipmentEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTExtensionLNClassEnum() {
    if (tExtensionLNClassEnumEDataType == null) {
      tExtensionLNClassEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(115);
    }
    return tExtensionLNClassEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTExtensionPhysConnTypeEnum() {
    if (tExtensionPhysConnTypeEnumEDataType == null) {
      tExtensionPhysConnTypeEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(116);
    }
    return tExtensionPhysConnTypeEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTExtensionPTypeEnum() {
    if (tExtensionPTypeEnumEDataType == null) {
      tExtensionPTypeEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(117);
    }
    return tExtensionPTypeEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTFCEnumObject() {
    if (tfcEnumObjectEDataType == null) {
      tfcEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(121);
    }
    return tfcEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTFullAttributeName() {
    if (tFullAttributeNameEDataType == null) {
      tFullAttributeNameEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(122);
    }
    return tFullAttributeNameEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTFullDOName() {
    if (tFullDONameEDataType == null) {
      tFullDONameEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(123);
    }
    return tFullDONameEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTGeneralEquipmentEnum() {
    if (tGeneralEquipmentEnumEDataType == null) {
      tGeneralEquipmentEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(126);
    }
    return tGeneralEquipmentEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTGSEControlTypeEnumObject() {
    if (tgseControlTypeEnumObjectEDataType == null) {
      tgseControlTypeEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(130);
    }
    return tgseControlTypeEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTIEDName() {
    if (tiedNameEDataType == null) {
      tiedNameEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(136);
    }
    return tiedNameEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTLDInst() {
    if (tldInstEDataType == null) {
      tldInstEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(139);
    }
    return tldInstEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTLDInstOrEmpty() {
    if (tldInstOrEmptyEDataType == null) {
      tldInstOrEmptyEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(140);
    }
    return tldInstOrEmptyEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTLDName() {
    if (tldNameEDataType == null) {
      tldNameEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(141);
    }
    return tldNameEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTLLN0EnumObject() {
    if (tlln0EnumObjectEDataType == null) {
      tlln0EnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(143);
    }
    return tlln0EnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTLNClassEnum() {
    if (tlnClassEnumEDataType == null) {
      tlnClassEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(146);
    }
    return tlnClassEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTLNInst() {
    if (tlnInstEDataType == null) {
      tlnInstEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(147);
    }
    return tlnInstEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTLNInstOrEmpty() {
    if (tlnInstOrEmptyEDataType == null) {
      tlnInstOrEmptyEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(148);
    }
    return tlnInstOrEmptyEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTLogName() {
    if (tLogNameEDataType == null) {
      tLogNameEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(154);
    }
    return tLogNameEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTLPHDEnumObject() {
    if (tlphdEnumObjectEDataType == null) {
      tlphdEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(157);
    }
    return tlphdEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTName() {
    if (tNameEDataType == null) {
      tNameEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(158);
    }
    return tNameEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPAddr() {
    if (tpAddrEDataType == null) {
      tpAddrEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(161);
    }
    return tpAddrEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPhaseEnumObject() {
    if (tPhaseEnumObjectEDataType == null) {
      tPhaseEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(164);
    }
    return tPhaseEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPhysConnTypeEnum() {
    if (tPhysConnTypeEnumEDataType == null) {
      tPhysConnTypeEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(166);
    }
    return tPhysConnTypeEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPowerTransformerEnumObject() {
    if (tPowerTransformerEnumObjectEDataType == null) {
      tPowerTransformerEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(183);
    }
    return tPowerTransformerEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPredefinedAttributeNameEnumObject() {
    if (tPredefinedAttributeNameEnumObjectEDataType == null) {
      tPredefinedAttributeNameEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(187);
    }
    return tPredefinedAttributeNameEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPredefinedBasicTypeEnumObject() {
    if (tPredefinedBasicTypeEnumObjectEDataType == null) {
      tPredefinedBasicTypeEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(189);
    }
    return tPredefinedBasicTypeEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPredefinedCDCEnumObject() {
    if (tPredefinedCDCEnumObjectEDataType == null) {
      tPredefinedCDCEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(191);
    }
    return tPredefinedCDCEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPredefinedCommonConductingEquipmentEnumObject() {
    if (tPredefinedCommonConductingEquipmentEnumObjectEDataType == null) {
      tPredefinedCommonConductingEquipmentEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(193);
    }
    return tPredefinedCommonConductingEquipmentEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPredefinedGeneralEquipmentEnumObject() {
    if (tPredefinedGeneralEquipmentEnumObjectEDataType == null) {
      tPredefinedGeneralEquipmentEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(195);
    }
    return tPredefinedGeneralEquipmentEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPredefinedLNClassEnum() {
    if (tPredefinedLNClassEnumEDataType == null) {
      tPredefinedLNClassEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(196);
    }
    return tPredefinedLNClassEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPredefinedPhysConnTypeEnumObject() {
    if (tPredefinedPhysConnTypeEnumObjectEDataType == null) {
      tPredefinedPhysConnTypeEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(198);
    }
    return tPredefinedPhysConnTypeEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPredefinedPTypeEnumObject() {
    if (tPredefinedPTypeEnumObjectEDataType == null) {
      tPredefinedPTypeEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(200);
    }
    return tPredefinedPTypeEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPredefinedPTypePhysConnEnumObject() {
    if (tPredefinedPTypePhysConnEnumObjectEDataType == null) {
      tPredefinedPTypePhysConnEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(202);
    }
    return tPredefinedPTypePhysConnEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPrefix() {
    if (tPrefixEDataType == null) {
      tPrefixEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(203);
    }
    return tPrefixEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPTypeEnum() {
    if (tpTypeEnumEDataType == null) {
      tpTypeEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(206);
    }
    return tpTypeEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTPTypePhysConnEnum() {
    if (tpTypePhysConnEnumEDataType == null) {
      tpTypePhysConnEnumEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(207);
    }
    return tpTypePhysConnEnumEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTRef() {
    if (tRefEDataType == null) {
      tRefEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(210);
    }
    return tRefEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTRestrName1stL() {
    if (tRestrName1stLEDataType == null) {
      tRestrName1stLEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(213);
    }
    return tRestrName1stLEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTRestrName1stU() {
    if (tRestrName1stUEDataType == null) {
      tRestrName1stUEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(214);
    }
    return tRestrName1stUEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTRightEnumObject() {
    if (tRightEnumObjectEDataType == null) {
      tRightEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(216);
    }
    return tRightEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTRptID() {
    if (tRptIDEDataType == null) {
      tRptIDEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(218);
    }
    return tRptIDEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTSclRevision() {
    if (tSclRevisionEDataType == null) {
      tSclRevisionEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(220);
    }
    return tSclRevisionEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTSclVersion() {
    if (tSclVersionEDataType == null) {
      tSclVersionEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(221);
    }
    return tSclVersionEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTSDOCount() {
    if (tsdoCountEDataType == null) {
      tsdoCountEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(224);
    }
    return tsdoCountEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTServiceSettingsEnumObject() {
    if (tServiceSettingsEnumObjectEDataType == null) {
      tServiceSettingsEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(232);
    }
    return tServiceSettingsEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTServiceTypeObject() {
    if (tServiceTypeObjectEDataType == null) {
      tServiceTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(234);
    }
    return tServiceTypeObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTSIUnitEnumObject() {
    if (tsiUnitEnumObjectEDataType == null) {
      tsiUnitEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(242);
    }
    return tsiUnitEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTSmpModObject() {
    if (tSmpModObjectEDataType == null) {
      tSmpModObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(244);
    }
    return tSmpModObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTTransformerWindingEnumObject() {
    if (tTransformerWindingEnumObjectEDataType == null) {
      tTransformerWindingEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(256);
    }
    return tTransformerWindingEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTUnitMultiplierEnumObject() {
    if (tUnitMultiplierEnumObjectEDataType == null) {
      tUnitMultiplierEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(259);
    }
    return tUnitMultiplierEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTValKindEnumObject() {
    if (tValKindEnumObjectEDataType == null) {
      tValKindEnumObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(263);
    }
    return tValKindEnumObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getTypeType() {
    if (typeTypeEDataType == null) {
      typeTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI).getEClassifiers().get(267);
    }
    return typeTypeEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SCLFactory getSCLFactory() {
    return (SCLFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isLoaded = false;

  /**
   * Laods the package and any sub-packages from their serialized form.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void loadPackage() {
    if (isLoaded) return;
    isLoaded = true;

    URL url = getClass().getResource(packageFilename);
    if (url == null) {
      throw new RuntimeException("Missing serialized package: " + packageFilename);
    }
    URI uri = URI.createURI(url.toString());
    Resource resource = new EcoreResourceFactoryImpl().createResource(uri);
    try {
      resource.load(null);
    }
    catch (IOException exception) {
      throw new WrappedException(exception);
    }
    initializeFromLoadedEPackage(this, (EPackage)resource.getContents().get(0));
    createResource(eNS_URI);
  }


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isFixed = false;

  /**
   * Fixes up the loaded package, to make it appear as if it had been programmatically built.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void fixPackageContents() {
    if (isFixed) return;
    isFixed = true;
    fixEClassifiers();
  }

  /**
   * Sets the instance class on the given classifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void fixInstanceClass(EClassifier eClassifier) {
    if (eClassifier.getInstanceClassName() == null) {
      eClassifier.setInstanceClassName("com.lucy.g3.iec61850.model.scl." + eClassifier.getName());
      setGeneratedClassName(eClassifier);
    }
  }

} //SCLPackageImpl
