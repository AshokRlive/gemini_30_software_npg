/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.SCLType;
import com.lucy.g3.iec61850.model.scl.TCommunication;
import com.lucy.g3.iec61850.model.scl.TDataTypeTemplates;
import com.lucy.g3.iec61850.model.scl.THeader;
import com.lucy.g3.iec61850.model.scl.TIED;
import com.lucy.g3.iec61850.model.scl.TSubstation;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.SCLTypeImpl#getHeader <em>Header</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.SCLTypeImpl#getSubstation <em>Substation</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.SCLTypeImpl#getCommunication <em>Communication</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.SCLTypeImpl#getIED <em>IED</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.SCLTypeImpl#getDataTypeTemplates <em>Data Type Templates</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.SCLTypeImpl#getRevision <em>Revision</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.SCLTypeImpl#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SCLTypeImpl extends TBaseElementImpl implements SCLType {
  /**
   * The cached value of the '{@link #getHeader() <em>Header</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHeader()
   * @generated
   * @ordered
   */
  protected THeader header;

  /**
   * The cached value of the '{@link #getSubstation() <em>Substation</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubstation()
   * @generated
   * @ordered
   */
  protected EList<TSubstation> substation;

  /**
   * The cached value of the '{@link #getCommunication() <em>Communication</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCommunication()
   * @generated
   * @ordered
   */
  protected TCommunication communication;

  /**
   * The cached value of the '{@link #getIED() <em>IED</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIED()
   * @generated
   * @ordered
   */
  protected EList<TIED> iED;

  /**
   * The cached value of the '{@link #getDataTypeTemplates() <em>Data Type Templates</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDataTypeTemplates()
   * @generated
   * @ordered
   */
  protected TDataTypeTemplates dataTypeTemplates;

  /**
   * The default value of the '{@link #getRevision() <em>Revision</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRevision()
   * @generated
   * @ordered
   */
  protected static final String REVISION_EDEFAULT = "A";

  /**
   * The cached value of the '{@link #getRevision() <em>Revision</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRevision()
   * @generated
   * @ordered
   */
  protected String revision = REVISION_EDEFAULT;

  /**
   * This is true if the Revision attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean revisionESet;

  /**
   * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVersion()
   * @generated
   * @ordered
   */
  protected static final String VERSION_EDEFAULT = "2007";

  /**
   * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVersion()
   * @generated
   * @ordered
   */
  protected String version = VERSION_EDEFAULT;

  /**
   * This is true if the Version attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean versionESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SCLTypeImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getSCLType();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public THeader getHeader() {
    return header;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetHeader(THeader newHeader, NotificationChain msgs) {
    THeader oldHeader = header;
    header = newHeader;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.SCL_TYPE__HEADER, oldHeader, newHeader);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setHeader(THeader newHeader) {
    if (newHeader != header) {
      NotificationChain msgs = null;
      if (header != null)
        msgs = ((InternalEObject)header).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.SCL_TYPE__HEADER, null, msgs);
      if (newHeader != null)
        msgs = ((InternalEObject)newHeader).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.SCL_TYPE__HEADER, null, msgs);
      msgs = basicSetHeader(newHeader, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.SCL_TYPE__HEADER, newHeader, newHeader));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TSubstation> getSubstation() {
    if (substation == null) {
      substation = new EObjectContainmentEList<TSubstation>(TSubstation.class, this, SCLPackage.SCL_TYPE__SUBSTATION);
    }
    return substation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TCommunication getCommunication() {
    return communication;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCommunication(TCommunication newCommunication, NotificationChain msgs) {
    TCommunication oldCommunication = communication;
    communication = newCommunication;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.SCL_TYPE__COMMUNICATION, oldCommunication, newCommunication);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCommunication(TCommunication newCommunication) {
    if (newCommunication != communication) {
      NotificationChain msgs = null;
      if (communication != null)
        msgs = ((InternalEObject)communication).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.SCL_TYPE__COMMUNICATION, null, msgs);
      if (newCommunication != null)
        msgs = ((InternalEObject)newCommunication).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.SCL_TYPE__COMMUNICATION, null, msgs);
      msgs = basicSetCommunication(newCommunication, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.SCL_TYPE__COMMUNICATION, newCommunication, newCommunication));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TIED> getIED() {
    if (iED == null) {
      iED = new EObjectContainmentEList<TIED>(TIED.class, this, SCLPackage.SCL_TYPE__IED);
    }
    return iED;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TDataTypeTemplates getDataTypeTemplates() {
    return dataTypeTemplates;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDataTypeTemplates(TDataTypeTemplates newDataTypeTemplates, NotificationChain msgs) {
    TDataTypeTemplates oldDataTypeTemplates = dataTypeTemplates;
    dataTypeTemplates = newDataTypeTemplates;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.SCL_TYPE__DATA_TYPE_TEMPLATES, oldDataTypeTemplates, newDataTypeTemplates);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDataTypeTemplates(TDataTypeTemplates newDataTypeTemplates) {
    if (newDataTypeTemplates != dataTypeTemplates) {
      NotificationChain msgs = null;
      if (dataTypeTemplates != null)
        msgs = ((InternalEObject)dataTypeTemplates).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.SCL_TYPE__DATA_TYPE_TEMPLATES, null, msgs);
      if (newDataTypeTemplates != null)
        msgs = ((InternalEObject)newDataTypeTemplates).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.SCL_TYPE__DATA_TYPE_TEMPLATES, null, msgs);
      msgs = basicSetDataTypeTemplates(newDataTypeTemplates, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.SCL_TYPE__DATA_TYPE_TEMPLATES, newDataTypeTemplates, newDataTypeTemplates));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getRevision() {
    return revision;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRevision(String newRevision) {
    String oldRevision = revision;
    revision = newRevision;
    boolean oldRevisionESet = revisionESet;
    revisionESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.SCL_TYPE__REVISION, oldRevision, revision, !oldRevisionESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetRevision() {
    String oldRevision = revision;
    boolean oldRevisionESet = revisionESet;
    revision = REVISION_EDEFAULT;
    revisionESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.SCL_TYPE__REVISION, oldRevision, REVISION_EDEFAULT, oldRevisionESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetRevision() {
    return revisionESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVersion() {
    return version;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVersion(String newVersion) {
    String oldVersion = version;
    version = newVersion;
    boolean oldVersionESet = versionESet;
    versionESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.SCL_TYPE__VERSION, oldVersion, version, !oldVersionESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetVersion() {
    String oldVersion = version;
    boolean oldVersionESet = versionESet;
    version = VERSION_EDEFAULT;
    versionESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.SCL_TYPE__VERSION, oldVersion, VERSION_EDEFAULT, oldVersionESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetVersion() {
    return versionESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case SCLPackage.SCL_TYPE__HEADER:
        return basicSetHeader(null, msgs);
      case SCLPackage.SCL_TYPE__SUBSTATION:
        return ((InternalEList<?>)getSubstation()).basicRemove(otherEnd, msgs);
      case SCLPackage.SCL_TYPE__COMMUNICATION:
        return basicSetCommunication(null, msgs);
      case SCLPackage.SCL_TYPE__IED:
        return ((InternalEList<?>)getIED()).basicRemove(otherEnd, msgs);
      case SCLPackage.SCL_TYPE__DATA_TYPE_TEMPLATES:
        return basicSetDataTypeTemplates(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.SCL_TYPE__HEADER:
        return getHeader();
      case SCLPackage.SCL_TYPE__SUBSTATION:
        return getSubstation();
      case SCLPackage.SCL_TYPE__COMMUNICATION:
        return getCommunication();
      case SCLPackage.SCL_TYPE__IED:
        return getIED();
      case SCLPackage.SCL_TYPE__DATA_TYPE_TEMPLATES:
        return getDataTypeTemplates();
      case SCLPackage.SCL_TYPE__REVISION:
        return getRevision();
      case SCLPackage.SCL_TYPE__VERSION:
        return getVersion();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.SCL_TYPE__HEADER:
        setHeader((THeader)newValue);
        return;
      case SCLPackage.SCL_TYPE__SUBSTATION:
        getSubstation().clear();
        getSubstation().addAll((Collection<? extends TSubstation>)newValue);
        return;
      case SCLPackage.SCL_TYPE__COMMUNICATION:
        setCommunication((TCommunication)newValue);
        return;
      case SCLPackage.SCL_TYPE__IED:
        getIED().clear();
        getIED().addAll((Collection<? extends TIED>)newValue);
        return;
      case SCLPackage.SCL_TYPE__DATA_TYPE_TEMPLATES:
        setDataTypeTemplates((TDataTypeTemplates)newValue);
        return;
      case SCLPackage.SCL_TYPE__REVISION:
        setRevision((String)newValue);
        return;
      case SCLPackage.SCL_TYPE__VERSION:
        setVersion((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.SCL_TYPE__HEADER:
        setHeader((THeader)null);
        return;
      case SCLPackage.SCL_TYPE__SUBSTATION:
        getSubstation().clear();
        return;
      case SCLPackage.SCL_TYPE__COMMUNICATION:
        setCommunication((TCommunication)null);
        return;
      case SCLPackage.SCL_TYPE__IED:
        getIED().clear();
        return;
      case SCLPackage.SCL_TYPE__DATA_TYPE_TEMPLATES:
        setDataTypeTemplates((TDataTypeTemplates)null);
        return;
      case SCLPackage.SCL_TYPE__REVISION:
        unsetRevision();
        return;
      case SCLPackage.SCL_TYPE__VERSION:
        unsetVersion();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.SCL_TYPE__HEADER:
        return header != null;
      case SCLPackage.SCL_TYPE__SUBSTATION:
        return substation != null && !substation.isEmpty();
      case SCLPackage.SCL_TYPE__COMMUNICATION:
        return communication != null;
      case SCLPackage.SCL_TYPE__IED:
        return iED != null && !iED.isEmpty();
      case SCLPackage.SCL_TYPE__DATA_TYPE_TEMPLATES:
        return dataTypeTemplates != null;
      case SCLPackage.SCL_TYPE__REVISION:
        return isSetRevision();
      case SCLPackage.SCL_TYPE__VERSION:
        return isSetVersion();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (revision: ");
    if (revisionESet) result.append(revision); else result.append("<unset>");
    result.append(", version: ");
    if (versionESet) result.append(version); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //SCLTypeImpl
