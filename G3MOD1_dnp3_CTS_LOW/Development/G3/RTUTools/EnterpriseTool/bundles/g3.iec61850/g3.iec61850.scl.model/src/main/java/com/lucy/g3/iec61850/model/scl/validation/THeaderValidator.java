/**
 *
 * $Id$
 */
package com.lucy.g3.iec61850.model.scl.validation;

import com.lucy.g3.iec61850.model.scl.HistoryType;
import com.lucy.g3.iec61850.model.scl.NameStructureType;
import com.lucy.g3.iec61850.model.scl.TText;

/**
 * A sample validator interface for {@link com.lucy.g3.iec61850.model.scl.THeader}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface THeaderValidator {
  boolean validate();

  boolean validateText(TText value);
  boolean validateHistory(HistoryType value);
  boolean validateId(String value);
  boolean validateNameStructure(NameStructureType value);
  boolean validateRevision(String value);
  boolean validateToolID(String value);
  boolean validateVersion(String value);
}
