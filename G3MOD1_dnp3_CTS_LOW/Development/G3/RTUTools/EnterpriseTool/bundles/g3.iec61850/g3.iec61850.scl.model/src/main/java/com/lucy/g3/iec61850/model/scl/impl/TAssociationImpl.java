/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.internal.SCLObjectImpl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TAssociation;
import com.lucy.g3.iec61850.model.scl.TAssociationKindEnum;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TAssociation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAssociationImpl#getAssociationID <em>Association ID</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAssociationImpl#getDesc <em>Desc</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAssociationImpl#getIedName <em>Ied Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAssociationImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAssociationImpl#getLdInst <em>Ld Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAssociationImpl#getLnClass <em>Ln Class</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAssociationImpl#getLnInst <em>Ln Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TAssociationImpl#getPrefix <em>Prefix</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TAssociationImpl extends SCLObjectImpl implements TAssociation {
  /**
   * The default value of the '{@link #getAssociationID() <em>Association ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAssociationID()
   * @generated
   * @ordered
   */
  protected static final String ASSOCIATION_ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAssociationID() <em>Association ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAssociationID()
   * @generated
   * @ordered
   */
  protected String associationID = ASSOCIATION_ID_EDEFAULT;

  /**
   * The default value of the '{@link #getDesc() <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDesc()
   * @generated
   * @ordered
   */
  protected static final String DESC_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getDesc() <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDesc()
   * @generated
   * @ordered
   */
  protected String desc = DESC_EDEFAULT;

  /**
   * This is true if the Desc attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean descESet;

  /**
   * The default value of the '{@link #getIedName() <em>Ied Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIedName()
   * @generated
   * @ordered
   */
  protected static final String IED_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getIedName() <em>Ied Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIedName()
   * @generated
   * @ordered
   */
  protected String iedName = IED_NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKind()
   * @generated
   * @ordered
   */
  protected static final TAssociationKindEnum KIND_EDEFAULT = TAssociationKindEnum.PRE_ESTABLISHED;

  /**
   * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKind()
   * @generated
   * @ordered
   */
  protected TAssociationKindEnum kind = KIND_EDEFAULT;

  /**
   * This is true if the Kind attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean kindESet;

  /**
   * The default value of the '{@link #getLdInst() <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLdInst()
   * @generated
   * @ordered
   */
  protected static final String LD_INST_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLdInst() <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLdInst()
   * @generated
   * @ordered
   */
  protected String ldInst = LD_INST_EDEFAULT;

  /**
   * The default value of the '{@link #getLnClass() <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnClass()
   * @generated
   * @ordered
   */
  protected static final Object LN_CLASS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLnClass() <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnClass()
   * @generated
   * @ordered
   */
  protected Object lnClass = LN_CLASS_EDEFAULT;

  /**
   * The default value of the '{@link #getLnInst() <em>Ln Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnInst()
   * @generated
   * @ordered
   */
  protected static final String LN_INST_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLnInst() <em>Ln Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnInst()
   * @generated
   * @ordered
   */
  protected String lnInst = LN_INST_EDEFAULT;

  /**
   * The default value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrefix()
   * @generated
   * @ordered
   */
  protected static final String PREFIX_EDEFAULT = "Lucy_";

  /**
   * The cached value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrefix()
   * @generated
   * @ordered
   */
  protected String prefix = PREFIX_EDEFAULT;

  /**
   * This is true if the Prefix attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean prefixESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TAssociationImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTAssociation();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAssociationID() {
    return associationID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAssociationID(String newAssociationID) {
    String oldAssociationID = associationID;
    associationID = newAssociationID;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TASSOCIATION__ASSOCIATION_ID, oldAssociationID, associationID));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDesc() {
    return desc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDesc(String newDesc) {
    String oldDesc = desc;
    desc = newDesc;
    boolean oldDescESet = descESet;
    descESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TASSOCIATION__DESC, oldDesc, desc, !oldDescESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetDesc() {
    String oldDesc = desc;
    boolean oldDescESet = descESet;
    desc = DESC_EDEFAULT;
    descESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TASSOCIATION__DESC, oldDesc, DESC_EDEFAULT, oldDescESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetDesc() {
    return descESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIedName() {
    return iedName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIedName(String newIedName) {
    String oldIedName = iedName;
    iedName = newIedName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TASSOCIATION__IED_NAME, oldIedName, iedName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TAssociationKindEnum getKind() {
    return kind;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setKind(TAssociationKindEnum newKind) {
    TAssociationKindEnum oldKind = kind;
    kind = newKind == null ? KIND_EDEFAULT : newKind;
    boolean oldKindESet = kindESet;
    kindESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TASSOCIATION__KIND, oldKind, kind, !oldKindESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetKind() {
    TAssociationKindEnum oldKind = kind;
    boolean oldKindESet = kindESet;
    kind = KIND_EDEFAULT;
    kindESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TASSOCIATION__KIND, oldKind, KIND_EDEFAULT, oldKindESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetKind() {
    return kindESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLdInst() {
    return ldInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLdInst(String newLdInst) {
    String oldLdInst = ldInst;
    ldInst = newLdInst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TASSOCIATION__LD_INST, oldLdInst, ldInst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object getLnClass() {
    return lnClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLnClass(Object newLnClass) {
    Object oldLnClass = lnClass;
    lnClass = newLnClass;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TASSOCIATION__LN_CLASS, oldLnClass, lnClass));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLnInst() {
    return lnInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLnInst(String newLnInst) {
    String oldLnInst = lnInst;
    lnInst = newLnInst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TASSOCIATION__LN_INST, oldLnInst, lnInst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPrefix() {
    return prefix;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPrefix(String newPrefix) {
    String oldPrefix = prefix;
    prefix = newPrefix;
    boolean oldPrefixESet = prefixESet;
    prefixESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TASSOCIATION__PREFIX, oldPrefix, prefix, !oldPrefixESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetPrefix() {
    String oldPrefix = prefix;
    boolean oldPrefixESet = prefixESet;
    prefix = PREFIX_EDEFAULT;
    prefixESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TASSOCIATION__PREFIX, oldPrefix, PREFIX_EDEFAULT, oldPrefixESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetPrefix() {
    return prefixESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TASSOCIATION__ASSOCIATION_ID:
        return getAssociationID();
      case SCLPackage.TASSOCIATION__DESC:
        return getDesc();
      case SCLPackage.TASSOCIATION__IED_NAME:
        return getIedName();
      case SCLPackage.TASSOCIATION__KIND:
        return getKind();
      case SCLPackage.TASSOCIATION__LD_INST:
        return getLdInst();
      case SCLPackage.TASSOCIATION__LN_CLASS:
        return getLnClass();
      case SCLPackage.TASSOCIATION__LN_INST:
        return getLnInst();
      case SCLPackage.TASSOCIATION__PREFIX:
        return getPrefix();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TASSOCIATION__ASSOCIATION_ID:
        setAssociationID((String)newValue);
        return;
      case SCLPackage.TASSOCIATION__DESC:
        setDesc((String)newValue);
        return;
      case SCLPackage.TASSOCIATION__IED_NAME:
        setIedName((String)newValue);
        return;
      case SCLPackage.TASSOCIATION__KIND:
        setKind((TAssociationKindEnum)newValue);
        return;
      case SCLPackage.TASSOCIATION__LD_INST:
        setLdInst((String)newValue);
        return;
      case SCLPackage.TASSOCIATION__LN_CLASS:
        setLnClass(newValue);
        return;
      case SCLPackage.TASSOCIATION__LN_INST:
        setLnInst((String)newValue);
        return;
      case SCLPackage.TASSOCIATION__PREFIX:
        setPrefix((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TASSOCIATION__ASSOCIATION_ID:
        setAssociationID(ASSOCIATION_ID_EDEFAULT);
        return;
      case SCLPackage.TASSOCIATION__DESC:
        unsetDesc();
        return;
      case SCLPackage.TASSOCIATION__IED_NAME:
        setIedName(IED_NAME_EDEFAULT);
        return;
      case SCLPackage.TASSOCIATION__KIND:
        unsetKind();
        return;
      case SCLPackage.TASSOCIATION__LD_INST:
        setLdInst(LD_INST_EDEFAULT);
        return;
      case SCLPackage.TASSOCIATION__LN_CLASS:
        setLnClass(LN_CLASS_EDEFAULT);
        return;
      case SCLPackage.TASSOCIATION__LN_INST:
        setLnInst(LN_INST_EDEFAULT);
        return;
      case SCLPackage.TASSOCIATION__PREFIX:
        unsetPrefix();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TASSOCIATION__ASSOCIATION_ID:
        return ASSOCIATION_ID_EDEFAULT == null ? associationID != null : !ASSOCIATION_ID_EDEFAULT.equals(associationID);
      case SCLPackage.TASSOCIATION__DESC:
        return isSetDesc();
      case SCLPackage.TASSOCIATION__IED_NAME:
        return IED_NAME_EDEFAULT == null ? iedName != null : !IED_NAME_EDEFAULT.equals(iedName);
      case SCLPackage.TASSOCIATION__KIND:
        return isSetKind();
      case SCLPackage.TASSOCIATION__LD_INST:
        return LD_INST_EDEFAULT == null ? ldInst != null : !LD_INST_EDEFAULT.equals(ldInst);
      case SCLPackage.TASSOCIATION__LN_CLASS:
        return LN_CLASS_EDEFAULT == null ? lnClass != null : !LN_CLASS_EDEFAULT.equals(lnClass);
      case SCLPackage.TASSOCIATION__LN_INST:
        return LN_INST_EDEFAULT == null ? lnInst != null : !LN_INST_EDEFAULT.equals(lnInst);
      case SCLPackage.TASSOCIATION__PREFIX:
        return isSetPrefix();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (associationID: ");
    result.append(associationID);
    result.append(", desc: ");
    if (descESet) result.append(desc); else result.append("<unset>");
    result.append(", iedName: ");
    result.append(iedName);
    result.append(", kind: ");
    if (kindESet) result.append(kind); else result.append("<unset>");
    result.append(", ldInst: ");
    result.append(ldInst);
    result.append(", lnClass: ");
    result.append(lnClass);
    result.append(", lnInst: ");
    result.append(lnInst);
    result.append(", prefix: ");
    if (prefixESet) result.append(prefix); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TAssociationImpl
