/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TCommunication;
import com.lucy.g3.iec61850.model.scl.TSubNetwork;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TCommunication</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TCommunicationImpl#getSubNetwork <em>Sub Network</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TCommunicationImpl extends TUnNamingImpl implements TCommunication {
  /**
   * The cached value of the '{@link #getSubNetwork() <em>Sub Network</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubNetwork()
   * @generated
   * @ordered
   */
  protected EList<TSubNetwork> subNetwork;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TCommunicationImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTCommunication();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TSubNetwork> getSubNetwork() {
    if (subNetwork == null) {
      subNetwork = new EObjectContainmentEList<TSubNetwork>(TSubNetwork.class, this, SCLPackage.TCOMMUNICATION__SUB_NETWORK);
    }
    return subNetwork;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case SCLPackage.TCOMMUNICATION__SUB_NETWORK:
        return ((InternalEList<?>)getSubNetwork()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TCOMMUNICATION__SUB_NETWORK:
        return getSubNetwork();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TCOMMUNICATION__SUB_NETWORK:
        getSubNetwork().clear();
        getSubNetwork().addAll((Collection<? extends TSubNetwork>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TCOMMUNICATION__SUB_NETWORK:
        getSubNetwork().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TCOMMUNICATION__SUB_NETWORK:
        return subNetwork != null && !subNetwork.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //TCommunicationImpl
