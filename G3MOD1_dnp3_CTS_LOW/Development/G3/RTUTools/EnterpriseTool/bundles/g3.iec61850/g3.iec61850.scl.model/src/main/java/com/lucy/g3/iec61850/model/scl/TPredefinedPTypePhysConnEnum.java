/**
 */
package com.lucy.g3.iec61850.model.scl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>TPredefined PType Phys Conn Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPredefinedPTypePhysConnEnum()
 * @model extendedMetaData="name='tPredefinedPTypePhysConnEnum'"
 * @generated
 */
public enum TPredefinedPTypePhysConnEnum implements Enumerator {
  /**
   * The '<em><b>Type</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #TYPE_VALUE
   * @generated
   * @ordered
   */
  TYPE(0, "Type", "Type"),

  /**
   * The '<em><b>Plug</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #PLUG_VALUE
   * @generated
   * @ordered
   */
  PLUG(1, "Plug", "Plug"),

  /**
   * The '<em><b>Cable</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CABLE_VALUE
   * @generated
   * @ordered
   */
  CABLE(2, "Cable", "Cable"),

  /**
   * The '<em><b>Port</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #PORT_VALUE
   * @generated
   * @ordered
   */
  PORT(3, "Port", "Port");

  /**
   * The '<em><b>Type</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Type</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #TYPE
   * @model name="Type"
   * @generated
   * @ordered
   */
  public static final int TYPE_VALUE = 0;

  /**
   * The '<em><b>Plug</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Plug</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #PLUG
   * @model name="Plug"
   * @generated
   * @ordered
   */
  public static final int PLUG_VALUE = 1;

  /**
   * The '<em><b>Cable</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Cable</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CABLE
   * @model name="Cable"
   * @generated
   * @ordered
   */
  public static final int CABLE_VALUE = 2;

  /**
   * The '<em><b>Port</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Port</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #PORT
   * @model name="Port"
   * @generated
   * @ordered
   */
  public static final int PORT_VALUE = 3;

  /**
   * An array of all the '<em><b>TPredefined PType Phys Conn Enum</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final TPredefinedPTypePhysConnEnum[] VALUES_ARRAY =
    new TPredefinedPTypePhysConnEnum[] {
      TYPE,
      PLUG,
      CABLE,
      PORT,
    };

  /**
   * A public read-only list of all the '<em><b>TPredefined PType Phys Conn Enum</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<TPredefinedPTypePhysConnEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>TPredefined PType Phys Conn Enum</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param literal the literal.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static TPredefinedPTypePhysConnEnum get(String literal) {
    for (int i = 0; i < VALUES_ARRAY.length; ++i) {
      TPredefinedPTypePhysConnEnum result = VALUES_ARRAY[i];
      if (result.toString().equals(literal)) {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>TPredefined PType Phys Conn Enum</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param name the name.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static TPredefinedPTypePhysConnEnum getByName(String name) {
    for (int i = 0; i < VALUES_ARRAY.length; ++i) {
      TPredefinedPTypePhysConnEnum result = VALUES_ARRAY[i];
      if (result.getName().equals(name)) {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>TPredefined PType Phys Conn Enum</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the integer value.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static TPredefinedPTypePhysConnEnum get(int value) {
    switch (value) {
      case TYPE_VALUE: return TYPE;
      case PLUG_VALUE: return PLUG;
      case CABLE_VALUE: return CABLE;
      case PORT_VALUE: return PORT;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private TPredefinedPTypePhysConnEnum(int value, String name, String literal) {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue() {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral() {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    return literal;
  }
  
} //TPredefinedPTypePhysConnEnum
