/**
 */
package com.lucy.g3.iec61850.model.scl.util;

import com.lucy.g3.iec61850.model.scl.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage
 * @generated
 */
public class SCLAdapterFactory extends AdapterFactoryImpl {
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static SCLPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SCLAdapterFactory() {
    if (modelPackage == null) {
      modelPackage = SCLPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object) {
    if (object == modelPackage) {
      return true;
    }
    if (object instanceof EObject) {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SCLSwitch<Adapter> modelSwitch =
    new SCLSwitch<Adapter>() {
      @Override
      public Adapter caseAuthenticationType(AuthenticationType object) {
        return createAuthenticationTypeAdapter();
      }
      @Override
      public Adapter caseDocumentRoot(DocumentRoot object) {
        return createDocumentRootAdapter();
      }
      @Override
      public Adapter caseHistoryType(HistoryType object) {
        return createHistoryTypeAdapter();
      }
      @Override
      public Adapter caseIEDNameType(IEDNameType object) {
        return createIEDNameTypeAdapter();
      }
      @Override
      public Adapter caseLN0Type(LN0Type object) {
        return createLN0TypeAdapter();
      }
      @Override
      public Adapter caseOptFieldsType(OptFieldsType object) {
        return createOptFieldsTypeAdapter();
      }
      @Override
      public Adapter caseProtNsType(ProtNsType object) {
        return createProtNsTypeAdapter();
      }
      @Override
      public Adapter caseSCLType(SCLType object) {
        return createSCLTypeAdapter();
      }
      @Override
      public Adapter caseSettingGroupsType(SettingGroupsType object) {
        return createSettingGroupsTypeAdapter();
      }
      @Override
      public Adapter caseSmvOptsType(SmvOptsType object) {
        return createSmvOptsTypeAdapter();
      }
      @Override
      public Adapter caseTAbstractConductingEquipment(TAbstractConductingEquipment object) {
        return createTAbstractConductingEquipmentAdapter();
      }
      @Override
      public Adapter caseTAbstractDataAttribute(TAbstractDataAttribute object) {
        return createTAbstractDataAttributeAdapter();
      }
      @Override
      public Adapter caseTAccessControl(TAccessControl object) {
        return createTAccessControlAdapter();
      }
      @Override
      public Adapter caseTAccessPoint(TAccessPoint object) {
        return createTAccessPointAdapter();
      }
      @Override
      public Adapter caseTAddress(TAddress object) {
        return createTAddressAdapter();
      }
      @Override
      public Adapter caseTAnyContentFromOtherNamespace(TAnyContentFromOtherNamespace object) {
        return createTAnyContentFromOtherNamespaceAdapter();
      }
      @Override
      public Adapter caseTAnyLN(TAnyLN object) {
        return createTAnyLNAdapter();
      }
      @Override
      public Adapter caseTAssociation(TAssociation object) {
        return createTAssociationAdapter();
      }
      @Override
      public Adapter caseTBaseElement(TBaseElement object) {
        return createTBaseElementAdapter();
      }
      @Override
      public Adapter caseTBay(TBay object) {
        return createTBayAdapter();
      }
      @Override
      public Adapter caseTBDA(TBDA object) {
        return createTBDAAdapter();
      }
      @Override
      public Adapter caseTBitRateInMbPerSec(TBitRateInMbPerSec object) {
        return createTBitRateInMbPerSecAdapter();
      }
      @Override
      public Adapter caseTCert(TCert object) {
        return createTCertAdapter();
      }
      @Override
      public Adapter caseTCertificate(TCertificate object) {
        return createTCertificateAdapter();
      }
      @Override
      public Adapter caseTClientLN(TClientLN object) {
        return createTClientLNAdapter();
      }
      @Override
      public Adapter caseTClientServices(TClientServices object) {
        return createTClientServicesAdapter();
      }
      @Override
      public Adapter caseTCommunication(TCommunication object) {
        return createTCommunicationAdapter();
      }
      @Override
      public Adapter caseTConductingEquipment(TConductingEquipment object) {
        return createTConductingEquipmentAdapter();
      }
      @Override
      public Adapter caseTConfLNs(TConfLNs object) {
        return createTConfLNsAdapter();
      }
      @Override
      public Adapter caseTConnectedAP(TConnectedAP object) {
        return createTConnectedAPAdapter();
      }
      @Override
      public Adapter caseTConnectivityNode(TConnectivityNode object) {
        return createTConnectivityNodeAdapter();
      }
      @Override
      public Adapter caseTControl(TControl object) {
        return createTControlAdapter();
      }
      @Override
      public Adapter caseTControlBlock(TControlBlock object) {
        return createTControlBlockAdapter();
      }
      @Override
      public Adapter caseTControlWithIEDName(TControlWithIEDName object) {
        return createTControlWithIEDNameAdapter();
      }
      @Override
      public Adapter caseTControlWithTriggerOpt(TControlWithTriggerOpt object) {
        return createTControlWithTriggerOptAdapter();
      }
      @Override
      public Adapter caseTDA(TDA object) {
        return createTDAAdapter();
      }
      @Override
      public Adapter caseTDAI(TDAI object) {
        return createTDAIAdapter();
      }
      @Override
      public Adapter caseTDataSet(TDataSet object) {
        return createTDataSetAdapter();
      }
      @Override
      public Adapter caseTDataTypeTemplates(TDataTypeTemplates object) {
        return createTDataTypeTemplatesAdapter();
      }
      @Override
      public Adapter caseTDAType(TDAType object) {
        return createTDATypeAdapter();
      }
      @Override
      public Adapter caseTDO(TDO object) {
        return createTDOAdapter();
      }
      @Override
      public Adapter caseTDOI(TDOI object) {
        return createTDOIAdapter();
      }
      @Override
      public Adapter caseTDOType(TDOType object) {
        return createTDOTypeAdapter();
      }
      @Override
      public Adapter caseTDurationInMilliSec(TDurationInMilliSec object) {
        return createTDurationInMilliSecAdapter();
      }
      @Override
      public Adapter caseTDurationInSec(TDurationInSec object) {
        return createTDurationInSecAdapter();
      }
      @Override
      public Adapter caseTEnumType(TEnumType object) {
        return createTEnumTypeAdapter();
      }
      @Override
      public Adapter caseTEnumVal(TEnumVal object) {
        return createTEnumValAdapter();
      }
      @Override
      public Adapter caseTEquipment(TEquipment object) {
        return createTEquipmentAdapter();
      }
      @Override
      public Adapter caseTEquipmentContainer(TEquipmentContainer object) {
        return createTEquipmentContainerAdapter();
      }
      @Override
      public Adapter caseTExtRef(TExtRef object) {
        return createTExtRefAdapter();
      }
      @Override
      public Adapter caseTFCDA(TFCDA object) {
        return createTFCDAAdapter();
      }
      @Override
      public Adapter caseTFunction(TFunction object) {
        return createTFunctionAdapter();
      }
      @Override
      public Adapter caseTGeneralEquipment(TGeneralEquipment object) {
        return createTGeneralEquipmentAdapter();
      }
      @Override
      public Adapter caseTGSE(TGSE object) {
        return createTGSEAdapter();
      }
      @Override
      public Adapter caseTGSEControl(TGSEControl object) {
        return createTGSEControlAdapter();
      }
      @Override
      public Adapter caseTGSESettings(TGSESettings object) {
        return createTGSESettingsAdapter();
      }
      @Override
      public Adapter caseTHeader(THeader object) {
        return createTHeaderAdapter();
      }
      @Override
      public Adapter caseTHitem(THitem object) {
        return createTHitemAdapter();
      }
      @Override
      public Adapter caseTIDNaming(TIDNaming object) {
        return createTIDNamingAdapter();
      }
      @Override
      public Adapter caseTIED(TIED object) {
        return createTIEDAdapter();
      }
      @Override
      public Adapter caseTInputs(TInputs object) {
        return createTInputsAdapter();
      }
      @Override
      public Adapter caseTLDevice(TLDevice object) {
        return createTLDeviceAdapter();
      }
      @Override
      public Adapter caseTLN(TLN object) {
        return createTLNAdapter();
      }
      @Override
      public Adapter caseTLN0(TLN0 object) {
        return createTLN0Adapter();
      }
      @Override
      public Adapter caseTLNode(TLNode object) {
        return createTLNodeAdapter();
      }
      @Override
      public Adapter caseTLNodeContainer(TLNodeContainer object) {
        return createTLNodeContainerAdapter();
      }
      @Override
      public Adapter caseTLNodeType(TLNodeType object) {
        return createTLNodeTypeAdapter();
      }
      @Override
      public Adapter caseTLog(TLog object) {
        return createTLogAdapter();
      }
      @Override
      public Adapter caseTLogControl(TLogControl object) {
        return createTLogControlAdapter();
      }
      @Override
      public Adapter caseTLogSettings(TLogSettings object) {
        return createTLogSettingsAdapter();
      }
      @Override
      public Adapter caseTNaming(TNaming object) {
        return createTNamingAdapter();
      }
      @Override
      public Adapter caseTP(TP object) {
        return createTPAdapter();
      }
      @Override
      public Adapter caseTPAPPID(TPAPPID object) {
        return createTPAPPIDAdapter();
      }
      @Override
      public Adapter caseTPhysConn(TPhysConn object) {
        return createTPhysConnAdapter();
      }
      @Override
      public Adapter caseTPIP(TPIP object) {
        return createTPIPAdapter();
      }
      @Override
      public Adapter caseTPIPGATEWAY(TPIPGATEWAY object) {
        return createTPIPGATEWAYAdapter();
      }
      @Override
      public Adapter caseTPIPSUBNET(TPIPSUBNET object) {
        return createTPIPSUBNETAdapter();
      }
      @Override
      public Adapter caseTPMACAddress(TPMACAddress object) {
        return createTPMACAddressAdapter();
      }
      @Override
      public Adapter caseTPMMSPort(TPMMSPort object) {
        return createTPMMSPortAdapter();
      }
      @Override
      public Adapter caseTPOSIAEInvoke(TPOSIAEInvoke object) {
        return createTPOSIAEInvokeAdapter();
      }
      @Override
      public Adapter caseTPOSIAEQualifier(TPOSIAEQualifier object) {
        return createTPOSIAEQualifierAdapter();
      }
      @Override
      public Adapter caseTPOSIAPInvoke(TPOSIAPInvoke object) {
        return createTPOSIAPInvokeAdapter();
      }
      @Override
      public Adapter caseTPOSIAPTitle(TPOSIAPTitle object) {
        return createTPOSIAPTitleAdapter();
      }
      @Override
      public Adapter caseTPOSINSAP(TPOSINSAP object) {
        return createTPOSINSAPAdapter();
      }
      @Override
      public Adapter caseTPOSIPSEL(TPOSIPSEL object) {
        return createTPOSIPSELAdapter();
      }
      @Override
      public Adapter caseTPOSISSEL(TPOSISSEL object) {
        return createTPOSISSELAdapter();
      }
      @Override
      public Adapter caseTPOSITSEL(TPOSITSEL object) {
        return createTPOSITSELAdapter();
      }
      @Override
      public Adapter caseTPowerSystemResource(TPowerSystemResource object) {
        return createTPowerSystemResourceAdapter();
      }
      @Override
      public Adapter caseTPowerTransformer(TPowerTransformer object) {
        return createTPowerTransformerAdapter();
      }
      @Override
      public Adapter caseTPPhysConn(TPPhysConn object) {
        return createTPPhysConnAdapter();
      }
      @Override
      public Adapter caseTPPort(TPPort object) {
        return createTPPortAdapter();
      }
      @Override
      public Adapter caseTPrivate(TPrivate object) {
        return createTPrivateAdapter();
      }
      @Override
      public Adapter caseTPSNTPPort(TPSNTPPort object) {
        return createTPSNTPPortAdapter();
      }
      @Override
      public Adapter caseTPVLANID(TPVLANID object) {
        return createTPVLANIDAdapter();
      }
      @Override
      public Adapter caseTPVLANPRIORITY(TPVLANPRIORITY object) {
        return createTPVLANPRIORITYAdapter();
      }
      @Override
      public Adapter caseTReportControl(TReportControl object) {
        return createTReportControlAdapter();
      }
      @Override
      public Adapter caseTReportSettings(TReportSettings object) {
        return createTReportSettingsAdapter();
      }
      @Override
      public Adapter caseTRptEnabled(TRptEnabled object) {
        return createTRptEnabledAdapter();
      }
      @Override
      public Adapter caseTSampledValueControl(TSampledValueControl object) {
        return createTSampledValueControlAdapter();
      }
      @Override
      public Adapter caseTSDI(TSDI object) {
        return createTSDIAdapter();
      }
      @Override
      public Adapter caseTSDO(TSDO object) {
        return createTSDOAdapter();
      }
      @Override
      public Adapter caseTServer(TServer object) {
        return createTServerAdapter();
      }
      @Override
      public Adapter caseTServerAt(TServerAt object) {
        return createTServerAtAdapter();
      }
      @Override
      public Adapter caseTServiceConfReportControl(TServiceConfReportControl object) {
        return createTServiceConfReportControlAdapter();
      }
      @Override
      public Adapter caseTServiceForConfDataSet(TServiceForConfDataSet object) {
        return createTServiceForConfDataSetAdapter();
      }
      @Override
      public Adapter caseTServices(TServices object) {
        return createTServicesAdapter();
      }
      @Override
      public Adapter caseTServiceSettings(TServiceSettings object) {
        return createTServiceSettingsAdapter();
      }
      @Override
      public Adapter caseTServiceWithMax(TServiceWithMax object) {
        return createTServiceWithMaxAdapter();
      }
      @Override
      public Adapter caseTServiceWithMaxAndMaxAttributes(TServiceWithMaxAndMaxAttributes object) {
        return createTServiceWithMaxAndMaxAttributesAdapter();
      }
      @Override
      public Adapter caseTServiceWithMaxAndModify(TServiceWithMaxAndModify object) {
        return createTServiceWithMaxAndModifyAdapter();
      }
      @Override
      public Adapter caseTServiceWithOptionalMax(TServiceWithOptionalMax object) {
        return createTServiceWithOptionalMaxAdapter();
      }
      @Override
      public Adapter caseTServiceYesNo(TServiceYesNo object) {
        return createTServiceYesNoAdapter();
      }
      @Override
      public Adapter caseTSettingControl(TSettingControl object) {
        return createTSettingControlAdapter();
      }
      @Override
      public Adapter caseTSMV(TSMV object) {
        return createTSMVAdapter();
      }
      @Override
      public Adapter caseTSMVSettings(TSMVSettings object) {
        return createTSMVSettingsAdapter();
      }
      @Override
      public Adapter caseTSubEquipment(TSubEquipment object) {
        return createTSubEquipmentAdapter();
      }
      @Override
      public Adapter caseTSubFunction(TSubFunction object) {
        return createTSubFunctionAdapter();
      }
      @Override
      public Adapter caseTSubNetwork(TSubNetwork object) {
        return createTSubNetworkAdapter();
      }
      @Override
      public Adapter caseTSubstation(TSubstation object) {
        return createTSubstationAdapter();
      }
      @Override
      public Adapter caseTTapChanger(TTapChanger object) {
        return createTTapChangerAdapter();
      }
      @Override
      public Adapter caseTTerminal(TTerminal object) {
        return createTTerminalAdapter();
      }
      @Override
      public Adapter caseTText(TText object) {
        return createTTextAdapter();
      }
      @Override
      public Adapter caseTTransformerWinding(TTransformerWinding object) {
        return createTTransformerWindingAdapter();
      }
      @Override
      public Adapter caseTTrgOps(TTrgOps object) {
        return createTTrgOpsAdapter();
      }
      @Override
      public Adapter caseTUnNaming(TUnNaming object) {
        return createTUnNamingAdapter();
      }
      @Override
      public Adapter caseTVal(TVal object) {
        return createTValAdapter();
      }
      @Override
      public Adapter caseTValueWithUnit(TValueWithUnit object) {
        return createTValueWithUnitAdapter();
      }
      @Override
      public Adapter caseTVoltage(TVoltage object) {
        return createTVoltageAdapter();
      }
      @Override
      public Adapter caseTVoltageLevel(TVoltageLevel object) {
        return createTVoltageLevelAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object) {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target) {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.AuthenticationType <em>Authentication Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.AuthenticationType
   * @generated
   */
  public Adapter createAuthenticationTypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.DocumentRoot <em>Document Root</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.DocumentRoot
   * @generated
   */
  public Adapter createDocumentRootAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.HistoryType <em>History Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.HistoryType
   * @generated
   */
  public Adapter createHistoryTypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.IEDNameType <em>IED Name Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.IEDNameType
   * @generated
   */
  public Adapter createIEDNameTypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.LN0Type <em>LN0 Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.LN0Type
   * @generated
   */
  public Adapter createLN0TypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.OptFieldsType <em>Opt Fields Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.OptFieldsType
   * @generated
   */
  public Adapter createOptFieldsTypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.ProtNsType <em>Prot Ns Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.ProtNsType
   * @generated
   */
  public Adapter createProtNsTypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.SCLType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.SCLType
   * @generated
   */
  public Adapter createSCLTypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.SettingGroupsType <em>Setting Groups Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.SettingGroupsType
   * @generated
   */
  public Adapter createSettingGroupsTypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.SmvOptsType <em>Smv Opts Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.SmvOptsType
   * @generated
   */
  public Adapter createSmvOptsTypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TAbstractConductingEquipment <em>TAbstract Conducting Equipment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TAbstractConductingEquipment
   * @generated
   */
  public Adapter createTAbstractConductingEquipmentAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute <em>TAbstract Data Attribute</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute
   * @generated
   */
  public Adapter createTAbstractDataAttributeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TAccessControl <em>TAccess Control</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TAccessControl
   * @generated
   */
  public Adapter createTAccessControlAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TAccessPoint <em>TAccess Point</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TAccessPoint
   * @generated
   */
  public Adapter createTAccessPointAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TAddress <em>TAddress</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TAddress
   * @generated
   */
  public Adapter createTAddressAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace <em>TAny Content From Other Namespace</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace
   * @generated
   */
  public Adapter createTAnyContentFromOtherNamespaceAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TAnyLN <em>TAny LN</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TAnyLN
   * @generated
   */
  public Adapter createTAnyLNAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TAssociation <em>TAssociation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TAssociation
   * @generated
   */
  public Adapter createTAssociationAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TBaseElement <em>TBase Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TBaseElement
   * @generated
   */
  public Adapter createTBaseElementAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TBay <em>TBay</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TBay
   * @generated
   */
  public Adapter createTBayAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TBDA <em>TBDA</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TBDA
   * @generated
   */
  public Adapter createTBDAAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec <em>TBit Rate In Mb Per Sec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TBitRateInMbPerSec
   * @generated
   */
  public Adapter createTBitRateInMbPerSecAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TCert <em>TCert</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TCert
   * @generated
   */
  public Adapter createTCertAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TCertificate <em>TCertificate</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TCertificate
   * @generated
   */
  public Adapter createTCertificateAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TClientLN <em>TClient LN</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TClientLN
   * @generated
   */
  public Adapter createTClientLNAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TClientServices <em>TClient Services</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TClientServices
   * @generated
   */
  public Adapter createTClientServicesAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TCommunication <em>TCommunication</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TCommunication
   * @generated
   */
  public Adapter createTCommunicationAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TConductingEquipment <em>TConducting Equipment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TConductingEquipment
   * @generated
   */
  public Adapter createTConductingEquipmentAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TConfLNs <em>TConf LNs</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TConfLNs
   * @generated
   */
  public Adapter createTConfLNsAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TConnectedAP <em>TConnected AP</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TConnectedAP
   * @generated
   */
  public Adapter createTConnectedAPAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TConnectivityNode <em>TConnectivity Node</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TConnectivityNode
   * @generated
   */
  public Adapter createTConnectivityNodeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TControl <em>TControl</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TControl
   * @generated
   */
  public Adapter createTControlAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TControlBlock <em>TControl Block</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TControlBlock
   * @generated
   */
  public Adapter createTControlBlockAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TControlWithIEDName <em>TControl With IED Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TControlWithIEDName
   * @generated
   */
  public Adapter createTControlWithIEDNameAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TControlWithTriggerOpt <em>TControl With Trigger Opt</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TControlWithTriggerOpt
   * @generated
   */
  public Adapter createTControlWithTriggerOptAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TDA <em>TDA</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TDA
   * @generated
   */
  public Adapter createTDAAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TDAI <em>TDAI</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TDAI
   * @generated
   */
  public Adapter createTDAIAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TDataSet <em>TData Set</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TDataSet
   * @generated
   */
  public Adapter createTDataSetAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TDataTypeTemplates <em>TData Type Templates</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TDataTypeTemplates
   * @generated
   */
  public Adapter createTDataTypeTemplatesAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TDAType <em>TDA Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TDAType
   * @generated
   */
  public Adapter createTDATypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TDO <em>TDO</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TDO
   * @generated
   */
  public Adapter createTDOAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TDOI <em>TDOI</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TDOI
   * @generated
   */
  public Adapter createTDOIAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TDOType <em>TDO Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TDOType
   * @generated
   */
  public Adapter createTDOTypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TDurationInMilliSec <em>TDuration In Milli Sec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TDurationInMilliSec
   * @generated
   */
  public Adapter createTDurationInMilliSecAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TDurationInSec <em>TDuration In Sec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TDurationInSec
   * @generated
   */
  public Adapter createTDurationInSecAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TEnumType <em>TEnum Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TEnumType
   * @generated
   */
  public Adapter createTEnumTypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TEnumVal <em>TEnum Val</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TEnumVal
   * @generated
   */
  public Adapter createTEnumValAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TEquipment <em>TEquipment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TEquipment
   * @generated
   */
  public Adapter createTEquipmentAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TEquipmentContainer <em>TEquipment Container</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TEquipmentContainer
   * @generated
   */
  public Adapter createTEquipmentContainerAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TExtRef <em>TExt Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TExtRef
   * @generated
   */
  public Adapter createTExtRefAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TFCDA <em>TFCDA</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TFCDA
   * @generated
   */
  public Adapter createTFCDAAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TFunction <em>TFunction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TFunction
   * @generated
   */
  public Adapter createTFunctionAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TGeneralEquipment <em>TGeneral Equipment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TGeneralEquipment
   * @generated
   */
  public Adapter createTGeneralEquipmentAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TGSE <em>TGSE</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TGSE
   * @generated
   */
  public Adapter createTGSEAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TGSEControl <em>TGSE Control</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TGSEControl
   * @generated
   */
  public Adapter createTGSEControlAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TGSESettings <em>TGSE Settings</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TGSESettings
   * @generated
   */
  public Adapter createTGSESettingsAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.THeader <em>THeader</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.THeader
   * @generated
   */
  public Adapter createTHeaderAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.THitem <em>THitem</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.THitem
   * @generated
   */
  public Adapter createTHitemAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TIDNaming <em>TID Naming</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TIDNaming
   * @generated
   */
  public Adapter createTIDNamingAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TIED <em>TIED</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TIED
   * @generated
   */
  public Adapter createTIEDAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TInputs <em>TInputs</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TInputs
   * @generated
   */
  public Adapter createTInputsAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TLDevice <em>TL Device</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TLDevice
   * @generated
   */
  public Adapter createTLDeviceAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TLN <em>TLN</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TLN
   * @generated
   */
  public Adapter createTLNAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TLN0 <em>TLN0</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TLN0
   * @generated
   */
  public Adapter createTLN0Adapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TLNode <em>TL Node</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TLNode
   * @generated
   */
  public Adapter createTLNodeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TLNodeContainer <em>TL Node Container</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TLNodeContainer
   * @generated
   */
  public Adapter createTLNodeContainerAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TLNodeType <em>TL Node Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TLNodeType
   * @generated
   */
  public Adapter createTLNodeTypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TLog <em>TLog</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TLog
   * @generated
   */
  public Adapter createTLogAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TLogControl <em>TLog Control</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TLogControl
   * @generated
   */
  public Adapter createTLogControlAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TLogSettings <em>TLog Settings</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TLogSettings
   * @generated
   */
  public Adapter createTLogSettingsAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TNaming <em>TNaming</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TNaming
   * @generated
   */
  public Adapter createTNamingAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TP <em>TP</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TP
   * @generated
   */
  public Adapter createTPAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPAPPID <em>TPAPPID</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPAPPID
   * @generated
   */
  public Adapter createTPAPPIDAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPhysConn <em>TPhys Conn</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPhysConn
   * @generated
   */
  public Adapter createTPhysConnAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPIP <em>TPIP</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPIP
   * @generated
   */
  public Adapter createTPIPAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPIPGATEWAY <em>TPIPGATEWAY</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPIPGATEWAY
   * @generated
   */
  public Adapter createTPIPGATEWAYAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPIPSUBNET <em>TPIPSUBNET</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPIPSUBNET
   * @generated
   */
  public Adapter createTPIPSUBNETAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPMACAddress <em>TPMAC Address</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPMACAddress
   * @generated
   */
  public Adapter createTPMACAddressAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPMMSPort <em>TPMMS Port</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPMMSPort
   * @generated
   */
  public Adapter createTPMMSPortAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPOSIAEInvoke <em>TPOSIAE Invoke</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPOSIAEInvoke
   * @generated
   */
  public Adapter createTPOSIAEInvokeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPOSIAEQualifier <em>TPOSIAE Qualifier</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPOSIAEQualifier
   * @generated
   */
  public Adapter createTPOSIAEQualifierAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPOSIAPInvoke <em>TPOSIAP Invoke</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPOSIAPInvoke
   * @generated
   */
  public Adapter createTPOSIAPInvokeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPOSIAPTitle <em>TPOSIAP Title</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPOSIAPTitle
   * @generated
   */
  public Adapter createTPOSIAPTitleAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPOSINSAP <em>TPOSINSAP</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPOSINSAP
   * @generated
   */
  public Adapter createTPOSINSAPAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPOSIPSEL <em>TPOSIPSEL</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPOSIPSEL
   * @generated
   */
  public Adapter createTPOSIPSELAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPOSISSEL <em>TPOSISSEL</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPOSISSEL
   * @generated
   */
  public Adapter createTPOSISSELAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPOSITSEL <em>TPOSITSEL</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPOSITSEL
   * @generated
   */
  public Adapter createTPOSITSELAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPowerSystemResource <em>TPower System Resource</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPowerSystemResource
   * @generated
   */
  public Adapter createTPowerSystemResourceAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPowerTransformer <em>TPower Transformer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPowerTransformer
   * @generated
   */
  public Adapter createTPowerTransformerAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPPhysConn <em>TP Phys Conn</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPPhysConn
   * @generated
   */
  public Adapter createTPPhysConnAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPPort <em>TP Port</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPPort
   * @generated
   */
  public Adapter createTPPortAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPrivate <em>TPrivate</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPrivate
   * @generated
   */
  public Adapter createTPrivateAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPSNTPPort <em>TPSNTP Port</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPSNTPPort
   * @generated
   */
  public Adapter createTPSNTPPortAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPVLANID <em>TPVLANID</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPVLANID
   * @generated
   */
  public Adapter createTPVLANIDAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TPVLANPRIORITY <em>TPVLANPRIORITY</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TPVLANPRIORITY
   * @generated
   */
  public Adapter createTPVLANPRIORITYAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TReportControl <em>TReport Control</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TReportControl
   * @generated
   */
  public Adapter createTReportControlAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TReportSettings <em>TReport Settings</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TReportSettings
   * @generated
   */
  public Adapter createTReportSettingsAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TRptEnabled <em>TRpt Enabled</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TRptEnabled
   * @generated
   */
  public Adapter createTRptEnabledAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TSampledValueControl <em>TSampled Value Control</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TSampledValueControl
   * @generated
   */
  public Adapter createTSampledValueControlAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TSDI <em>TSDI</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TSDI
   * @generated
   */
  public Adapter createTSDIAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TSDO <em>TSDO</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TSDO
   * @generated
   */
  public Adapter createTSDOAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TServer <em>TServer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TServer
   * @generated
   */
  public Adapter createTServerAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TServerAt <em>TServer At</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TServerAt
   * @generated
   */
  public Adapter createTServerAtAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TServiceConfReportControl <em>TService Conf Report Control</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TServiceConfReportControl
   * @generated
   */
  public Adapter createTServiceConfReportControlAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TServiceForConfDataSet <em>TService For Conf Data Set</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TServiceForConfDataSet
   * @generated
   */
  public Adapter createTServiceForConfDataSetAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TServices <em>TServices</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TServices
   * @generated
   */
  public Adapter createTServicesAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TServiceSettings <em>TService Settings</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TServiceSettings
   * @generated
   */
  public Adapter createTServiceSettingsAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TServiceWithMax <em>TService With Max</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TServiceWithMax
   * @generated
   */
  public Adapter createTServiceWithMaxAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TServiceWithMaxAndMaxAttributes <em>TService With Max And Max Attributes</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TServiceWithMaxAndMaxAttributes
   * @generated
   */
  public Adapter createTServiceWithMaxAndMaxAttributesAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TServiceWithMaxAndModify <em>TService With Max And Modify</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TServiceWithMaxAndModify
   * @generated
   */
  public Adapter createTServiceWithMaxAndModifyAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TServiceWithOptionalMax <em>TService With Optional Max</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TServiceWithOptionalMax
   * @generated
   */
  public Adapter createTServiceWithOptionalMaxAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TServiceYesNo <em>TService Yes No</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TServiceYesNo
   * @generated
   */
  public Adapter createTServiceYesNoAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TSettingControl <em>TSetting Control</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TSettingControl
   * @generated
   */
  public Adapter createTSettingControlAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TSMV <em>TSMV</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TSMV
   * @generated
   */
  public Adapter createTSMVAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TSMVSettings <em>TSMV Settings</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TSMVSettings
   * @generated
   */
  public Adapter createTSMVSettingsAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TSubEquipment <em>TSub Equipment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TSubEquipment
   * @generated
   */
  public Adapter createTSubEquipmentAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TSubFunction <em>TSub Function</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TSubFunction
   * @generated
   */
  public Adapter createTSubFunctionAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TSubNetwork <em>TSub Network</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TSubNetwork
   * @generated
   */
  public Adapter createTSubNetworkAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TSubstation <em>TSubstation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TSubstation
   * @generated
   */
  public Adapter createTSubstationAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TTapChanger <em>TTap Changer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TTapChanger
   * @generated
   */
  public Adapter createTTapChangerAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TTerminal <em>TTerminal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TTerminal
   * @generated
   */
  public Adapter createTTerminalAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TText <em>TText</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TText
   * @generated
   */
  public Adapter createTTextAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TTransformerWinding <em>TTransformer Winding</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TTransformerWinding
   * @generated
   */
  public Adapter createTTransformerWindingAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TTrgOps <em>TTrg Ops</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TTrgOps
   * @generated
   */
  public Adapter createTTrgOpsAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TUnNaming <em>TUn Naming</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TUnNaming
   * @generated
   */
  public Adapter createTUnNamingAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TVal <em>TVal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TVal
   * @generated
   */
  public Adapter createTValAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TValueWithUnit <em>TValue With Unit</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TValueWithUnit
   * @generated
   */
  public Adapter createTValueWithUnitAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TVoltage <em>TVoltage</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TVoltage
   * @generated
   */
  public Adapter createTVoltageAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.scl.TVoltageLevel <em>TVoltage Level</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.scl.TVoltageLevel
   * @generated
   */
  public Adapter createTVoltageLevelAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter() {
    return null;
  }

} //SCLAdapterFactory
