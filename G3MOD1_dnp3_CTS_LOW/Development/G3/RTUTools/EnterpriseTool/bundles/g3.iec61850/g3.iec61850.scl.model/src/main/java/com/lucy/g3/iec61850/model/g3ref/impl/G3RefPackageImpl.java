/**
 */
package com.lucy.g3.iec61850.model.g3ref.impl;

import com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage;

import com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingPackageImpl;

import com.lucy.g3.iec61850.model.g3ref.DocumentRoot;
import com.lucy.g3.iec61850.model.g3ref.G3RefFactory;
import com.lucy.g3.iec61850.model.g3ref.G3RefPackage;
import com.lucy.g3.iec61850.model.g3ref.TG3RefObject;

import com.lucy.g3.iec61850.model.scl.SCLPackage;

import com.lucy.g3.iec61850.model.scl.impl.SCLPackageImpl;

import com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage;

import com.lucy.g3.iec61850.model.systemcorp.impl.SystemCorpPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class G3RefPackageImpl extends EPackageImpl implements G3RefPackage {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass documentRootEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tg3RefObjectEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see com.lucy.g3.iec61850.model.g3ref.G3RefPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private G3RefPackageImpl() {
    super(eNS_URI, G3RefFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link G3RefPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static G3RefPackage init() {
    if (isInited) return (G3RefPackage)EPackage.Registry.INSTANCE.getEPackage(G3RefPackage.eNS_URI);

    // Obtain or create and register package
    G3RefPackageImpl theG3RefPackage = (G3RefPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof G3RefPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new G3RefPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    XMLTypePackage.eINSTANCE.eClass();

    // Obtain or create and register interdependencies
    IEC61850MappingPackageImpl theIEC61850MappingPackage = (IEC61850MappingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IEC61850MappingPackage.eNS_URI) instanceof IEC61850MappingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IEC61850MappingPackage.eNS_URI) : IEC61850MappingPackage.eINSTANCE);
    SCLPackageImpl theSCLPackage = (SCLPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI) instanceof SCLPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SCLPackage.eNS_URI) : SCLPackage.eINSTANCE);
    SystemCorpPackageImpl theSystemCorpPackage = (SystemCorpPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SystemCorpPackage.eNS_URI) instanceof SystemCorpPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SystemCorpPackage.eNS_URI) : SystemCorpPackage.eINSTANCE);

    // Load packages
    theSCLPackage.loadPackage();

    // Create package meta-data objects
    theG3RefPackage.createPackageContents();
    theIEC61850MappingPackage.createPackageContents();
    theSystemCorpPackage.createPackageContents();

    // Initialize created meta-data
    theG3RefPackage.initializePackageContents();
    theIEC61850MappingPackage.initializePackageContents();
    theSystemCorpPackage.initializePackageContents();

    // Fix loaded packages
    theSCLPackage.fixPackageContents();

    // Mark meta-data to indicate it can't be changed
    theG3RefPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(G3RefPackage.eNS_URI, theG3RefPackage);
    return theG3RefPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDocumentRoot() {
    return documentRootEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDocumentRoot_Mixed() {
    return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XMLNSPrefixMap() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XSISchemaLocation() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_G3RefObject() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTG3RefObject() {
    return tg3RefObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTG3RefObject_Name() {
    return (EAttribute)tg3RefObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTG3RefObject_Uuid() {
    return (EAttribute)tg3RefObjectEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public G3RefFactory getG3RefFactory() {
    return (G3RefFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents() {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    documentRootEClass = createEClass(DOCUMENT_ROOT);
    createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
    createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
    createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
    createEReference(documentRootEClass, DOCUMENT_ROOT__G3_REF_OBJECT);

    tg3RefObjectEClass = createEClass(TG3_REF_OBJECT);
    createEAttribute(tg3RefObjectEClass, TG3_REF_OBJECT__NAME);
    createEAttribute(tg3RefObjectEClass, TG3_REF_OBJECT__UUID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents() {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes

    // Initialize classes, features, and operations; add parameters
    initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_G3RefObject(), this.getTG3RefObject(), null, "g3RefObject", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

    initEClass(tg3RefObjectEClass, TG3RefObject.class, "TG3RefObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getTG3RefObject_Name(), theXMLTypePackage.getAnySimpleType(), "name", null, 1, 1, TG3RefObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTG3RefObject_Uuid(), theXMLTypePackage.getString(), "uuid", null, 0, 1, TG3RefObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);

    // Create annotations
    // http:///org/eclipse/emf/ecore/util/ExtendedMetaData
    createExtendedMetaDataAnnotations();
  }

  /**
   * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createExtendedMetaDataAnnotations() {
    String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";	
    addAnnotation
      (documentRootEClass, 
       source, 
       new String[] {
       "name", "",
       "kind", "mixed"
       });	
    addAnnotation
      (getDocumentRoot_Mixed(), 
       source, 
       new String[] {
       "kind", "elementWildcard",
       "name", ":mixed"
       });	
    addAnnotation
      (getDocumentRoot_XMLNSPrefixMap(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "xmlns:prefix"
       });	
    addAnnotation
      (getDocumentRoot_XSISchemaLocation(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "xsi:schemaLocation"
       });	
    addAnnotation
      (getDocumentRoot_G3RefObject(), 
       source, 
       new String[] {
       "kind", "element",
       "name", "G3RefObject",
       "namespace", "##targetNamespace"
       });	
    addAnnotation
      (tg3RefObjectEClass, 
       source, 
       new String[] {
       "name", "tG3RefObject",
       "kind", "empty"
       });	
    addAnnotation
      (getTG3RefObject_Name(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "name"
       });	
    addAnnotation
      (getTG3RefObject_Uuid(), 
       source, 
       new String[] {
       "kind", "attribute",
       "name", "uuid"
       });
  }

} //G3RefPackageImpl
