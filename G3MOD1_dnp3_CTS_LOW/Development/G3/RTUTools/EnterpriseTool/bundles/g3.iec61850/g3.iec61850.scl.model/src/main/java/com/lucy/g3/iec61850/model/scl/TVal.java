/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TVal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TVal#getValue <em>Value</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TVal#getSGroup <em>SGroup</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTVal()
 * @model extendedMetaData="name='tVal' kind='simple'"
 * @extends SCLObject
 * @generated
 */
public interface TVal extends SCLObject {
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTVal_Value()
   * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
   *        extendedMetaData="name=':0' kind='simple'"
   * @generated
   */
  String getValue();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TVal#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(String value);

  /**
   * Returns the value of the '<em><b>SGroup</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>SGroup</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>SGroup</em>' attribute.
   * @see #isSetSGroup()
   * @see #unsetSGroup()
   * @see #setSGroup(long)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTVal_SGroup()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
   *        extendedMetaData="kind='attribute' name='sGroup'"
   * @generated
   */
  long getSGroup();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TVal#getSGroup <em>SGroup</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>SGroup</em>' attribute.
   * @see #isSetSGroup()
   * @see #unsetSGroup()
   * @see #getSGroup()
   * @generated
   */
  void setSGroup(long value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TVal#getSGroup <em>SGroup</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetSGroup()
   * @see #getSGroup()
   * @see #setSGroup(long)
   * @generated
   */
  void unsetSGroup();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TVal#getSGroup <em>SGroup</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>SGroup</em>' attribute is set.
   * @see #unsetSGroup()
   * @see #getSGroup()
   * @see #setSGroup(long)
   * @generated
   */
  boolean isSetSGroup();

} // TVal
