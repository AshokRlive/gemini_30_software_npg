/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TSDO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSDO#getCount <em>Count</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSDO#getName <em>Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSDO#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSDO()
 * @model extendedMetaData="name='tSDO' kind='elementOnly'"
 * @generated
 */
public interface TSDO extends TUnNaming {
  /**
   * Returns the value of the '<em><b>Count</b></em>' attribute.
   * The default value is <code>"0"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Count</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Count</em>' attribute.
   * @see #isSetCount()
   * @see #unsetCount()
   * @see #setCount(Object)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSDO_Count()
   * @model default="0" unsettable="true" dataType="com.lucy.g3.iec61850.model.scl.TSDOCount"
   *        extendedMetaData="kind='attribute' name='count'"
   * @generated
   */
  Object getCount();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TSDO#getCount <em>Count</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Count</em>' attribute.
   * @see #isSetCount()
   * @see #unsetCount()
   * @see #getCount()
   * @generated
   */
  void setCount(Object value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TSDO#getCount <em>Count</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetCount()
   * @see #getCount()
   * @see #setCount(Object)
   * @generated
   */
  void unsetCount();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TSDO#getCount <em>Count</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Count</em>' attribute is set.
   * @see #unsetCount()
   * @see #getCount()
   * @see #setCount(Object)
   * @generated
   */
  boolean isSetCount();

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSDO_Name()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TRestrName1stL" required="true"
   *        extendedMetaData="kind='attribute' name='name'"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TSDO#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSDO_Type()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TName" required="true"
   *        extendedMetaData="kind='attribute' name='type'"
   * @generated
   */
  String getType();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TSDO#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(String value);

} // TSDO
