/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping;

import com.lucy.g3.iec61850.model.internal.SCLObject;

import java.math.BigInteger;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Attribute Id T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField0 <em>Field0</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField1 <em>Field1</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField2 <em>Field2</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField3 <em>Field3</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField4 <em>Field4</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField5 <em>Field5</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getDataAttributeIdT()
 * @model extendedMetaData="name='DataAttributeIdT' kind='empty'"
 * @extends SCLObject
 * @generated
 */
public interface DataAttributeIdT extends SCLObject {
  /**
   * Returns the value of the '<em><b>Field0</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field0</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field0</em>' attribute.
   * @see #setField0(BigInteger)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getDataAttributeIdT_Field0()
   * @model dataType="org.eclipse.emf.ecore.xml.type.Integer" required="true"
   *        extendedMetaData="kind='attribute' name='field0' namespace='##targetNamespace'"
   * @generated
   */
  BigInteger getField0();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField0 <em>Field0</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field0</em>' attribute.
   * @see #getField0()
   * @generated
   */
  void setField0(BigInteger value);

  /**
   * Returns the value of the '<em><b>Field1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field1</em>' attribute.
   * @see #setField1(BigInteger)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getDataAttributeIdT_Field1()
   * @model dataType="org.eclipse.emf.ecore.xml.type.Integer" required="true"
   *        extendedMetaData="kind='attribute' name='field1' namespace='##targetNamespace'"
   * @generated
   */
  BigInteger getField1();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField1 <em>Field1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field1</em>' attribute.
   * @see #getField1()
   * @generated
   */
  void setField1(BigInteger value);

  /**
   * Returns the value of the '<em><b>Field2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field2</em>' attribute.
   * @see #setField2(BigInteger)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getDataAttributeIdT_Field2()
   * @model dataType="org.eclipse.emf.ecore.xml.type.Integer" required="true"
   *        extendedMetaData="kind='attribute' name='field2' namespace='##targetNamespace'"
   * @generated
   */
  BigInteger getField2();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField2 <em>Field2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field2</em>' attribute.
   * @see #getField2()
   * @generated
   */
  void setField2(BigInteger value);

  /**
   * Returns the value of the '<em><b>Field3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field3</em>' attribute.
   * @see #setField3(BigInteger)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getDataAttributeIdT_Field3()
   * @model dataType="org.eclipse.emf.ecore.xml.type.Integer" required="true"
   *        extendedMetaData="kind='attribute' name='field3' namespace='##targetNamespace'"
   * @generated
   */
  BigInteger getField3();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField3 <em>Field3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field3</em>' attribute.
   * @see #getField3()
   * @generated
   */
  void setField3(BigInteger value);

  /**
   * Returns the value of the '<em><b>Field4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field4</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field4</em>' attribute.
   * @see #setField4(BigInteger)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getDataAttributeIdT_Field4()
   * @model dataType="org.eclipse.emf.ecore.xml.type.Integer" required="true"
   *        extendedMetaData="kind='attribute' name='field4' namespace='##targetNamespace'"
   * @generated
   */
  BigInteger getField4();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField4 <em>Field4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field4</em>' attribute.
   * @see #getField4()
   * @generated
   */
  void setField4(BigInteger value);

  /**
   * Returns the value of the '<em><b>Field5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field5</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field5</em>' attribute.
   * @see #setField5(BigInteger)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getDataAttributeIdT_Field5()
   * @model dataType="org.eclipse.emf.ecore.xml.type.Integer" required="true"
   *        extendedMetaData="kind='attribute' name='field5' namespace='##targetNamespace'"
   * @generated
   */
  BigInteger getField5();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT#getField5 <em>Field5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field5</em>' attribute.
   * @see #getField5()
   * @generated
   */
  void setField5(BigInteger value);

} // DataAttributeIdT
