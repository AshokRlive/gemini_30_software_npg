/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage
 * @generated
 */
public interface IEC61850MappingFactory extends EFactory {
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  IEC61850MappingFactory eINSTANCE = com.lucy.g3.iec61850.model.IEC61850Mapping.impl.IEC61850MappingFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Data Attribute Id T</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Data Attribute Id T</em>'.
   * @generated
   */
  DataAttributeIdT createDataAttributeIdT();

  /**
   * Returns a new object of class '<em>Document Root</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Document Root</em>'.
   * @generated
   */
  DocumentRoot createDocumentRoot();

  /**
   * Returns a new object of class '<em>Entry T</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Entry T</em>'.
   * @generated
   */
  EntryT createEntryT();

  /**
   * Returns a new object of class '<em>G3 Ref T</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>G3 Ref T</em>'.
   * @generated
   */
  G3RefT createG3RefT();

  /**
   * Returns a new object of class '<em>Mapping Root T</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Mapping Root T</em>'.
   * @generated
   */
  MappingRootT createMappingRootT();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  IEC61850MappingPackage getIEC61850MappingPackage();

} //IEC61850MappingFactory
