/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TCommunication</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TCommunication#getSubNetwork <em>Sub Network</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTCommunication()
 * @model extendedMetaData="name='tCommunication' kind='elementOnly'"
 * @generated
 */
public interface TCommunication extends TUnNaming {
  /**
   * Returns the value of the '<em><b>Sub Network</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TSubNetwork}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sub Network</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sub Network</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTCommunication_SubNetwork()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='SubNetwork' namespace='##targetNamespace'"
   * @generated
   */
  EList<TSubNetwork> getSubNetwork();

} // TCommunication
