/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TInputs</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TInputs#getExtRef <em>Ext Ref</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTInputs()
 * @model extendedMetaData="name='tInputs' kind='elementOnly'"
 * @generated
 */
public interface TInputs extends TUnNaming {
  /**
   * Returns the value of the '<em><b>Ext Ref</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TExtRef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ext Ref</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ext Ref</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTInputs_ExtRef()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='ExtRef' namespace='##targetNamespace'"
   * @generated
   */
  EList<TExtRef> getExtRef();

} // TInputs
