/**
 */
package com.lucy.g3.iec61850.model.scl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>TAuthentication Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAuthenticationEnum()
 * @model extendedMetaData="name='tAuthenticationEnum'"
 * @generated
 */
public enum TAuthenticationEnum implements Enumerator {
  /**
   * The '<em><b>None</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #NONE_VALUE
   * @generated
   * @ordered
   */
  NONE(0, "none", "none"),

  /**
   * The '<em><b>Password</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #PASSWORD_VALUE
   * @generated
   * @ordered
   */
  PASSWORD(1, "password", "password"),

  /**
   * The '<em><b>Weak</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #WEAK_VALUE
   * @generated
   * @ordered
   */
  WEAK(2, "weak", "weak"),

  /**
   * The '<em><b>Strong</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #STRONG_VALUE
   * @generated
   * @ordered
   */
  STRONG(3, "strong", "strong"),

  /**
   * The '<em><b>Certificate</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CERTIFICATE_VALUE
   * @generated
   * @ordered
   */
  CERTIFICATE(4, "certificate", "certificate");

  /**
   * The '<em><b>None</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>None</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #NONE
   * @model name="none"
   * @generated
   * @ordered
   */
  public static final int NONE_VALUE = 0;

  /**
   * The '<em><b>Password</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Password</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #PASSWORD
   * @model name="password"
   * @generated
   * @ordered
   */
  public static final int PASSWORD_VALUE = 1;

  /**
   * The '<em><b>Weak</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Weak</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #WEAK
   * @model name="weak"
   * @generated
   * @ordered
   */
  public static final int WEAK_VALUE = 2;

  /**
   * The '<em><b>Strong</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Strong</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #STRONG
   * @model name="strong"
   * @generated
   * @ordered
   */
  public static final int STRONG_VALUE = 3;

  /**
   * The '<em><b>Certificate</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Certificate</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CERTIFICATE
   * @model name="certificate"
   * @generated
   * @ordered
   */
  public static final int CERTIFICATE_VALUE = 4;

  /**
   * An array of all the '<em><b>TAuthentication Enum</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final TAuthenticationEnum[] VALUES_ARRAY =
    new TAuthenticationEnum[] {
      NONE,
      PASSWORD,
      WEAK,
      STRONG,
      CERTIFICATE,
    };

  /**
   * A public read-only list of all the '<em><b>TAuthentication Enum</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<TAuthenticationEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>TAuthentication Enum</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param literal the literal.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static TAuthenticationEnum get(String literal) {
    for (int i = 0; i < VALUES_ARRAY.length; ++i) {
      TAuthenticationEnum result = VALUES_ARRAY[i];
      if (result.toString().equals(literal)) {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>TAuthentication Enum</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param name the name.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static TAuthenticationEnum getByName(String name) {
    for (int i = 0; i < VALUES_ARRAY.length; ++i) {
      TAuthenticationEnum result = VALUES_ARRAY[i];
      if (result.getName().equals(name)) {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>TAuthentication Enum</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the integer value.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static TAuthenticationEnum get(int value) {
    switch (value) {
      case NONE_VALUE: return NONE;
      case PASSWORD_VALUE: return PASSWORD;
      case WEAK_VALUE: return WEAK;
      case STRONG_VALUE: return STRONG;
      case CERTIFICATE_VALUE: return CERTIFICATE;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private TAuthenticationEnum(int value, String name, String literal) {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue() {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral() {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    return literal;
  }
  
} //TAuthenticationEnum
