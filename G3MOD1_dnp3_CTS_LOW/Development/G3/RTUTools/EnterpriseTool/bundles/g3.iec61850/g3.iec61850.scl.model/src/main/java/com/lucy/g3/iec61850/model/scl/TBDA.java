/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TBDA</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTBDA()
 * @model extendedMetaData="name='tBDA' kind='elementOnly'"
 * @generated
 */
public interface TBDA extends TAbstractDataAttribute {
} // TBDA
