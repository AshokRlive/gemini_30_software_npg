/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPOSIAE Qualifier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPOSIAEQualifier()
 * @model extendedMetaData="name='tP_OSI-AE-Qualifier' kind='simple'"
 * @generated
 */
public interface TPOSIAEQualifier extends TP {
} // TPOSIAEQualifier
