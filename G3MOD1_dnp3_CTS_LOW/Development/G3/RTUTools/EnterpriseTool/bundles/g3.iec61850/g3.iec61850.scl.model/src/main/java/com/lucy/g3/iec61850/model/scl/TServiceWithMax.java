/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TService With Max</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TServiceWithMax#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTServiceWithMax()
 * @model extendedMetaData="name='tServiceWithMax' kind='empty'"
 * @extends SCLObject
 * @generated
 */
public interface TServiceWithMax extends SCLObject {
  /**
   * Returns the value of the '<em><b>Max</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Max</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Max</em>' attribute.
   * @see #isSetMax()
   * @see #unsetMax()
   * @see #setMax(long)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTServiceWithMax_Max()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
   *        extendedMetaData="kind='attribute' name='max'"
   * @generated
   */
  long getMax();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TServiceWithMax#getMax <em>Max</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Max</em>' attribute.
   * @see #isSetMax()
   * @see #unsetMax()
   * @see #getMax()
   * @generated
   */
  void setMax(long value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TServiceWithMax#getMax <em>Max</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetMax()
   * @see #getMax()
   * @see #setMax(long)
   * @generated
   */
  void unsetMax();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TServiceWithMax#getMax <em>Max</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Max</em>' attribute is set.
   * @see #unsetMax()
   * @see #getMax()
   * @see #setMax(long)
   * @generated
   */
  boolean isSetMax();

} // TServiceWithMax
