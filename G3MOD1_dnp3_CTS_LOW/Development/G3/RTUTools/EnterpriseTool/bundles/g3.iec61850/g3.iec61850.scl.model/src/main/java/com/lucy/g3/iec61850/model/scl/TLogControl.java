/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TLog Control</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TLogControl#getBufTime <em>Buf Time</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLdInst <em>Ld Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLnClass <em>Ln Class</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLnInst <em>Ln Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TLogControl#isLogEna <em>Log Ena</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLogName <em>Log Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TLogControl#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TLogControl#isReasonCode <em>Reason Code</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLogControl()
 * @model extendedMetaData="name='tLogControl' kind='elementOnly'"
 * @generated
 */
public interface TLogControl extends TControlWithTriggerOpt {
  /**
   * Returns the value of the '<em><b>Buf Time</b></em>' attribute.
   * The default value is <code>"0"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Buf Time</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Buf Time</em>' attribute.
   * @see #isSetBufTime()
   * @see #unsetBufTime()
   * @see #setBufTime(long)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLogControl_BufTime()
   * @model default="0" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
   *        extendedMetaData="kind='attribute' name='bufTime'"
   * @generated
   */
  long getBufTime();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getBufTime <em>Buf Time</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Buf Time</em>' attribute.
   * @see #isSetBufTime()
   * @see #unsetBufTime()
   * @see #getBufTime()
   * @generated
   */
  void setBufTime(long value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getBufTime <em>Buf Time</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetBufTime()
   * @see #getBufTime()
   * @see #setBufTime(long)
   * @generated
   */
  void unsetBufTime();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getBufTime <em>Buf Time</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Buf Time</em>' attribute is set.
   * @see #unsetBufTime()
   * @see #getBufTime()
   * @see #setBufTime(long)
   * @generated
   */
  boolean isSetBufTime();

  /**
   * Returns the value of the '<em><b>Ld Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ld Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ld Inst</em>' attribute.
   * @see #setLdInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLogControl_LdInst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLDInst"
   *        extendedMetaData="kind='attribute' name='ldInst'"
   * @generated
   */
  String getLdInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLdInst <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ld Inst</em>' attribute.
   * @see #getLdInst()
   * @generated
   */
  void setLdInst(String value);

  /**
   * Returns the value of the '<em><b>Ln Class</b></em>' attribute.
   * The default value is <code>"LLN0"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ln Class</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ln Class</em>' attribute.
   * @see #isSetLnClass()
   * @see #unsetLnClass()
   * @see #setLnClass(Object)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLogControl_LnClass()
   * @model default="LLN0" unsettable="true" dataType="com.lucy.g3.iec61850.model.scl.TLNClassEnum"
   *        extendedMetaData="kind='attribute' name='lnClass'"
   * @generated
   */
  Object getLnClass();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLnClass <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ln Class</em>' attribute.
   * @see #isSetLnClass()
   * @see #unsetLnClass()
   * @see #getLnClass()
   * @generated
   */
  void setLnClass(Object value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLnClass <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetLnClass()
   * @see #getLnClass()
   * @see #setLnClass(Object)
   * @generated
   */
  void unsetLnClass();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLnClass <em>Ln Class</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Ln Class</em>' attribute is set.
   * @see #unsetLnClass()
   * @see #getLnClass()
   * @see #setLnClass(Object)
   * @generated
   */
  boolean isSetLnClass();

  /**
   * Returns the value of the '<em><b>Ln Inst</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ln Inst</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ln Inst</em>' attribute.
   * @see #setLnInst(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLogControl_LnInst()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLNInst"
   *        extendedMetaData="kind='attribute' name='lnInst'"
   * @generated
   */
  String getLnInst();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLnInst <em>Ln Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ln Inst</em>' attribute.
   * @see #getLnInst()
   * @generated
   */
  void setLnInst(String value);

  /**
   * Returns the value of the '<em><b>Log Ena</b></em>' attribute.
   * The default value is <code>"true"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Log Ena</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Log Ena</em>' attribute.
   * @see #isSetLogEna()
   * @see #unsetLogEna()
   * @see #setLogEna(boolean)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLogControl_LogEna()
   * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   *        extendedMetaData="kind='attribute' name='logEna'"
   * @generated
   */
  boolean isLogEna();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#isLogEna <em>Log Ena</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Log Ena</em>' attribute.
   * @see #isSetLogEna()
   * @see #unsetLogEna()
   * @see #isLogEna()
   * @generated
   */
  void setLogEna(boolean value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#isLogEna <em>Log Ena</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetLogEna()
   * @see #isLogEna()
   * @see #setLogEna(boolean)
   * @generated
   */
  void unsetLogEna();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#isLogEna <em>Log Ena</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Log Ena</em>' attribute is set.
   * @see #unsetLogEna()
   * @see #isLogEna()
   * @see #setLogEna(boolean)
   * @generated
   */
  boolean isSetLogEna();

  /**
   * Returns the value of the '<em><b>Log Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Log Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Log Name</em>' attribute.
   * @see #setLogName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLogControl_LogName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TLogName" required="true"
   *        extendedMetaData="kind='attribute' name='logName'"
   * @generated
   */
  String getLogName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getLogName <em>Log Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Log Name</em>' attribute.
   * @see #getLogName()
   * @generated
   */
  void setLogName(String value);

  /**
   * Returns the value of the '<em><b>Prefix</b></em>' attribute.
   * The default value is <code>"Lucy_"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prefix</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prefix</em>' attribute.
   * @see #isSetPrefix()
   * @see #unsetPrefix()
   * @see #setPrefix(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLogControl_Prefix()
   * @model default="Lucy_" unsettable="true" dataType="com.lucy.g3.iec61850.model.scl.TPrefix"
   *        extendedMetaData="kind='attribute' name='prefix'"
   * @generated
   */
  String getPrefix();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getPrefix <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prefix</em>' attribute.
   * @see #isSetPrefix()
   * @see #unsetPrefix()
   * @see #getPrefix()
   * @generated
   */
  void setPrefix(String value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getPrefix <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetPrefix()
   * @see #getPrefix()
   * @see #setPrefix(String)
   * @generated
   */
  void unsetPrefix();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#getPrefix <em>Prefix</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Prefix</em>' attribute is set.
   * @see #unsetPrefix()
   * @see #getPrefix()
   * @see #setPrefix(String)
   * @generated
   */
  boolean isSetPrefix();

  /**
   * Returns the value of the '<em><b>Reason Code</b></em>' attribute.
   * The default value is <code>"true"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Reason Code</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reason Code</em>' attribute.
   * @see #isSetReasonCode()
   * @see #unsetReasonCode()
   * @see #setReasonCode(boolean)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTLogControl_ReasonCode()
   * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   *        extendedMetaData="kind='attribute' name='reasonCode'"
   * @generated
   */
  boolean isReasonCode();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#isReasonCode <em>Reason Code</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reason Code</em>' attribute.
   * @see #isSetReasonCode()
   * @see #unsetReasonCode()
   * @see #isReasonCode()
   * @generated
   */
  void setReasonCode(boolean value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#isReasonCode <em>Reason Code</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetReasonCode()
   * @see #isReasonCode()
   * @see #setReasonCode(boolean)
   * @generated
   */
  void unsetReasonCode();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TLogControl#isReasonCode <em>Reason Code</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Reason Code</em>' attribute is set.
   * @see #unsetReasonCode()
   * @see #isReasonCode()
   * @see #setReasonCode(boolean)
   * @generated
   */
  boolean isSetReasonCode();

} // TLogControl
