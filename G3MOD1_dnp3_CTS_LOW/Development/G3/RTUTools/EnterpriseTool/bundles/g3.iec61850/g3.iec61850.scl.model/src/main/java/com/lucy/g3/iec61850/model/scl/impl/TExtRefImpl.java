/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.internal.SCLObjectImpl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TExtRef;
import com.lucy.g3.iec61850.model.scl.TServiceType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TExt Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getDaName <em>Da Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getDesc <em>Desc</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getDoName <em>Do Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getIedName <em>Ied Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getIntAddr <em>Int Addr</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getLdInst <em>Ld Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getLnClass <em>Ln Class</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getLnInst <em>Ln Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getServiceType <em>Service Type</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getSrcCBName <em>Src CB Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getSrcLDInst <em>Src LD Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getSrcLNClass <em>Src LN Class</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getSrcLNInst <em>Src LN Inst</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TExtRefImpl#getSrcPrefix <em>Src Prefix</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TExtRefImpl extends SCLObjectImpl implements TExtRef {
  /**
   * The default value of the '{@link #getDaName() <em>Da Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDaName()
   * @generated
   * @ordered
   */
  protected static final String DA_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDaName() <em>Da Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDaName()
   * @generated
   * @ordered
   */
  protected String daName = DA_NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getDesc() <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDesc()
   * @generated
   * @ordered
   */
  protected static final String DESC_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getDesc() <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDesc()
   * @generated
   * @ordered
   */
  protected String desc = DESC_EDEFAULT;

  /**
   * This is true if the Desc attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean descESet;

  /**
   * The default value of the '{@link #getDoName() <em>Do Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDoName()
   * @generated
   * @ordered
   */
  protected static final String DO_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDoName() <em>Do Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDoName()
   * @generated
   * @ordered
   */
  protected String doName = DO_NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getIedName() <em>Ied Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIedName()
   * @generated
   * @ordered
   */
  protected static final String IED_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getIedName() <em>Ied Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIedName()
   * @generated
   * @ordered
   */
  protected String iedName = IED_NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getIntAddr() <em>Int Addr</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIntAddr()
   * @generated
   * @ordered
   */
  protected static final String INT_ADDR_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getIntAddr() <em>Int Addr</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIntAddr()
   * @generated
   * @ordered
   */
  protected String intAddr = INT_ADDR_EDEFAULT;

  /**
   * The default value of the '{@link #getLdInst() <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLdInst()
   * @generated
   * @ordered
   */
  protected static final String LD_INST_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLdInst() <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLdInst()
   * @generated
   * @ordered
   */
  protected String ldInst = LD_INST_EDEFAULT;

  /**
   * The default value of the '{@link #getLnClass() <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnClass()
   * @generated
   * @ordered
   */
  protected static final Object LN_CLASS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLnClass() <em>Ln Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnClass()
   * @generated
   * @ordered
   */
  protected Object lnClass = LN_CLASS_EDEFAULT;

  /**
   * The default value of the '{@link #getLnInst() <em>Ln Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnInst()
   * @generated
   * @ordered
   */
  protected static final String LN_INST_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLnInst() <em>Ln Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLnInst()
   * @generated
   * @ordered
   */
  protected String lnInst = LN_INST_EDEFAULT;

  /**
   * The default value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrefix()
   * @generated
   * @ordered
   */
  protected static final String PREFIX_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrefix()
   * @generated
   * @ordered
   */
  protected String prefix = PREFIX_EDEFAULT;

  /**
   * The default value of the '{@link #getServiceType() <em>Service Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getServiceType()
   * @generated
   * @ordered
   */
  protected static final TServiceType SERVICE_TYPE_EDEFAULT = TServiceType.POLL;

  /**
   * The cached value of the '{@link #getServiceType() <em>Service Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getServiceType()
   * @generated
   * @ordered
   */
  protected TServiceType serviceType = SERVICE_TYPE_EDEFAULT;

  /**
   * This is true if the Service Type attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean serviceTypeESet;

  /**
   * The default value of the '{@link #getSrcCBName() <em>Src CB Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSrcCBName()
   * @generated
   * @ordered
   */
  protected static final String SRC_CB_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSrcCBName() <em>Src CB Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSrcCBName()
   * @generated
   * @ordered
   */
  protected String srcCBName = SRC_CB_NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getSrcLDInst() <em>Src LD Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSrcLDInst()
   * @generated
   * @ordered
   */
  protected static final String SRC_LD_INST_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSrcLDInst() <em>Src LD Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSrcLDInst()
   * @generated
   * @ordered
   */
  protected String srcLDInst = SRC_LD_INST_EDEFAULT;

  /**
   * The default value of the '{@link #getSrcLNClass() <em>Src LN Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSrcLNClass()
   * @generated
   * @ordered
   */
  protected static final Object SRC_LN_CLASS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSrcLNClass() <em>Src LN Class</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSrcLNClass()
   * @generated
   * @ordered
   */
  protected Object srcLNClass = SRC_LN_CLASS_EDEFAULT;

  /**
   * The default value of the '{@link #getSrcLNInst() <em>Src LN Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSrcLNInst()
   * @generated
   * @ordered
   */
  protected static final String SRC_LN_INST_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSrcLNInst() <em>Src LN Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSrcLNInst()
   * @generated
   * @ordered
   */
  protected String srcLNInst = SRC_LN_INST_EDEFAULT;

  /**
   * The default value of the '{@link #getSrcPrefix() <em>Src Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSrcPrefix()
   * @generated
   * @ordered
   */
  protected static final String SRC_PREFIX_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSrcPrefix() <em>Src Prefix</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSrcPrefix()
   * @generated
   * @ordered
   */
  protected String srcPrefix = SRC_PREFIX_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TExtRefImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTExtRef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDaName() {
    return daName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDaName(String newDaName) {
    String oldDaName = daName;
    daName = newDaName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__DA_NAME, oldDaName, daName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDesc() {
    return desc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDesc(String newDesc) {
    String oldDesc = desc;
    desc = newDesc;
    boolean oldDescESet = descESet;
    descESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__DESC, oldDesc, desc, !oldDescESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetDesc() {
    String oldDesc = desc;
    boolean oldDescESet = descESet;
    desc = DESC_EDEFAULT;
    descESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TEXT_REF__DESC, oldDesc, DESC_EDEFAULT, oldDescESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetDesc() {
    return descESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDoName() {
    return doName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDoName(String newDoName) {
    String oldDoName = doName;
    doName = newDoName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__DO_NAME, oldDoName, doName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIedName() {
    return iedName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIedName(String newIedName) {
    String oldIedName = iedName;
    iedName = newIedName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__IED_NAME, oldIedName, iedName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIntAddr() {
    return intAddr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIntAddr(String newIntAddr) {
    String oldIntAddr = intAddr;
    intAddr = newIntAddr;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__INT_ADDR, oldIntAddr, intAddr));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLdInst() {
    return ldInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLdInst(String newLdInst) {
    String oldLdInst = ldInst;
    ldInst = newLdInst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__LD_INST, oldLdInst, ldInst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object getLnClass() {
    return lnClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLnClass(Object newLnClass) {
    Object oldLnClass = lnClass;
    lnClass = newLnClass;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__LN_CLASS, oldLnClass, lnClass));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLnInst() {
    return lnInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLnInst(String newLnInst) {
    String oldLnInst = lnInst;
    lnInst = newLnInst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__LN_INST, oldLnInst, lnInst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPrefix() {
    return prefix;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPrefix(String newPrefix) {
    String oldPrefix = prefix;
    prefix = newPrefix;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__PREFIX, oldPrefix, prefix));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceType getServiceType() {
    return serviceType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setServiceType(TServiceType newServiceType) {
    TServiceType oldServiceType = serviceType;
    serviceType = newServiceType == null ? SERVICE_TYPE_EDEFAULT : newServiceType;
    boolean oldServiceTypeESet = serviceTypeESet;
    serviceTypeESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__SERVICE_TYPE, oldServiceType, serviceType, !oldServiceTypeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetServiceType() {
    TServiceType oldServiceType = serviceType;
    boolean oldServiceTypeESet = serviceTypeESet;
    serviceType = SERVICE_TYPE_EDEFAULT;
    serviceTypeESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TEXT_REF__SERVICE_TYPE, oldServiceType, SERVICE_TYPE_EDEFAULT, oldServiceTypeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetServiceType() {
    return serviceTypeESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSrcCBName() {
    return srcCBName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSrcCBName(String newSrcCBName) {
    String oldSrcCBName = srcCBName;
    srcCBName = newSrcCBName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__SRC_CB_NAME, oldSrcCBName, srcCBName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSrcLDInst() {
    return srcLDInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSrcLDInst(String newSrcLDInst) {
    String oldSrcLDInst = srcLDInst;
    srcLDInst = newSrcLDInst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__SRC_LD_INST, oldSrcLDInst, srcLDInst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object getSrcLNClass() {
    return srcLNClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSrcLNClass(Object newSrcLNClass) {
    Object oldSrcLNClass = srcLNClass;
    srcLNClass = newSrcLNClass;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__SRC_LN_CLASS, oldSrcLNClass, srcLNClass));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSrcLNInst() {
    return srcLNInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSrcLNInst(String newSrcLNInst) {
    String oldSrcLNInst = srcLNInst;
    srcLNInst = newSrcLNInst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__SRC_LN_INST, oldSrcLNInst, srcLNInst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSrcPrefix() {
    return srcPrefix;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSrcPrefix(String newSrcPrefix) {
    String oldSrcPrefix = srcPrefix;
    srcPrefix = newSrcPrefix;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TEXT_REF__SRC_PREFIX, oldSrcPrefix, srcPrefix));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TEXT_REF__DA_NAME:
        return getDaName();
      case SCLPackage.TEXT_REF__DESC:
        return getDesc();
      case SCLPackage.TEXT_REF__DO_NAME:
        return getDoName();
      case SCLPackage.TEXT_REF__IED_NAME:
        return getIedName();
      case SCLPackage.TEXT_REF__INT_ADDR:
        return getIntAddr();
      case SCLPackage.TEXT_REF__LD_INST:
        return getLdInst();
      case SCLPackage.TEXT_REF__LN_CLASS:
        return getLnClass();
      case SCLPackage.TEXT_REF__LN_INST:
        return getLnInst();
      case SCLPackage.TEXT_REF__PREFIX:
        return getPrefix();
      case SCLPackage.TEXT_REF__SERVICE_TYPE:
        return getServiceType();
      case SCLPackage.TEXT_REF__SRC_CB_NAME:
        return getSrcCBName();
      case SCLPackage.TEXT_REF__SRC_LD_INST:
        return getSrcLDInst();
      case SCLPackage.TEXT_REF__SRC_LN_CLASS:
        return getSrcLNClass();
      case SCLPackage.TEXT_REF__SRC_LN_INST:
        return getSrcLNInst();
      case SCLPackage.TEXT_REF__SRC_PREFIX:
        return getSrcPrefix();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TEXT_REF__DA_NAME:
        setDaName((String)newValue);
        return;
      case SCLPackage.TEXT_REF__DESC:
        setDesc((String)newValue);
        return;
      case SCLPackage.TEXT_REF__DO_NAME:
        setDoName((String)newValue);
        return;
      case SCLPackage.TEXT_REF__IED_NAME:
        setIedName((String)newValue);
        return;
      case SCLPackage.TEXT_REF__INT_ADDR:
        setIntAddr((String)newValue);
        return;
      case SCLPackage.TEXT_REF__LD_INST:
        setLdInst((String)newValue);
        return;
      case SCLPackage.TEXT_REF__LN_CLASS:
        setLnClass(newValue);
        return;
      case SCLPackage.TEXT_REF__LN_INST:
        setLnInst((String)newValue);
        return;
      case SCLPackage.TEXT_REF__PREFIX:
        setPrefix((String)newValue);
        return;
      case SCLPackage.TEXT_REF__SERVICE_TYPE:
        setServiceType((TServiceType)newValue);
        return;
      case SCLPackage.TEXT_REF__SRC_CB_NAME:
        setSrcCBName((String)newValue);
        return;
      case SCLPackage.TEXT_REF__SRC_LD_INST:
        setSrcLDInst((String)newValue);
        return;
      case SCLPackage.TEXT_REF__SRC_LN_CLASS:
        setSrcLNClass(newValue);
        return;
      case SCLPackage.TEXT_REF__SRC_LN_INST:
        setSrcLNInst((String)newValue);
        return;
      case SCLPackage.TEXT_REF__SRC_PREFIX:
        setSrcPrefix((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TEXT_REF__DA_NAME:
        setDaName(DA_NAME_EDEFAULT);
        return;
      case SCLPackage.TEXT_REF__DESC:
        unsetDesc();
        return;
      case SCLPackage.TEXT_REF__DO_NAME:
        setDoName(DO_NAME_EDEFAULT);
        return;
      case SCLPackage.TEXT_REF__IED_NAME:
        setIedName(IED_NAME_EDEFAULT);
        return;
      case SCLPackage.TEXT_REF__INT_ADDR:
        setIntAddr(INT_ADDR_EDEFAULT);
        return;
      case SCLPackage.TEXT_REF__LD_INST:
        setLdInst(LD_INST_EDEFAULT);
        return;
      case SCLPackage.TEXT_REF__LN_CLASS:
        setLnClass(LN_CLASS_EDEFAULT);
        return;
      case SCLPackage.TEXT_REF__LN_INST:
        setLnInst(LN_INST_EDEFAULT);
        return;
      case SCLPackage.TEXT_REF__PREFIX:
        setPrefix(PREFIX_EDEFAULT);
        return;
      case SCLPackage.TEXT_REF__SERVICE_TYPE:
        unsetServiceType();
        return;
      case SCLPackage.TEXT_REF__SRC_CB_NAME:
        setSrcCBName(SRC_CB_NAME_EDEFAULT);
        return;
      case SCLPackage.TEXT_REF__SRC_LD_INST:
        setSrcLDInst(SRC_LD_INST_EDEFAULT);
        return;
      case SCLPackage.TEXT_REF__SRC_LN_CLASS:
        setSrcLNClass(SRC_LN_CLASS_EDEFAULT);
        return;
      case SCLPackage.TEXT_REF__SRC_LN_INST:
        setSrcLNInst(SRC_LN_INST_EDEFAULT);
        return;
      case SCLPackage.TEXT_REF__SRC_PREFIX:
        setSrcPrefix(SRC_PREFIX_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TEXT_REF__DA_NAME:
        return DA_NAME_EDEFAULT == null ? daName != null : !DA_NAME_EDEFAULT.equals(daName);
      case SCLPackage.TEXT_REF__DESC:
        return isSetDesc();
      case SCLPackage.TEXT_REF__DO_NAME:
        return DO_NAME_EDEFAULT == null ? doName != null : !DO_NAME_EDEFAULT.equals(doName);
      case SCLPackage.TEXT_REF__IED_NAME:
        return IED_NAME_EDEFAULT == null ? iedName != null : !IED_NAME_EDEFAULT.equals(iedName);
      case SCLPackage.TEXT_REF__INT_ADDR:
        return INT_ADDR_EDEFAULT == null ? intAddr != null : !INT_ADDR_EDEFAULT.equals(intAddr);
      case SCLPackage.TEXT_REF__LD_INST:
        return LD_INST_EDEFAULT == null ? ldInst != null : !LD_INST_EDEFAULT.equals(ldInst);
      case SCLPackage.TEXT_REF__LN_CLASS:
        return LN_CLASS_EDEFAULT == null ? lnClass != null : !LN_CLASS_EDEFAULT.equals(lnClass);
      case SCLPackage.TEXT_REF__LN_INST:
        return LN_INST_EDEFAULT == null ? lnInst != null : !LN_INST_EDEFAULT.equals(lnInst);
      case SCLPackage.TEXT_REF__PREFIX:
        return PREFIX_EDEFAULT == null ? prefix != null : !PREFIX_EDEFAULT.equals(prefix);
      case SCLPackage.TEXT_REF__SERVICE_TYPE:
        return isSetServiceType();
      case SCLPackage.TEXT_REF__SRC_CB_NAME:
        return SRC_CB_NAME_EDEFAULT == null ? srcCBName != null : !SRC_CB_NAME_EDEFAULT.equals(srcCBName);
      case SCLPackage.TEXT_REF__SRC_LD_INST:
        return SRC_LD_INST_EDEFAULT == null ? srcLDInst != null : !SRC_LD_INST_EDEFAULT.equals(srcLDInst);
      case SCLPackage.TEXT_REF__SRC_LN_CLASS:
        return SRC_LN_CLASS_EDEFAULT == null ? srcLNClass != null : !SRC_LN_CLASS_EDEFAULT.equals(srcLNClass);
      case SCLPackage.TEXT_REF__SRC_LN_INST:
        return SRC_LN_INST_EDEFAULT == null ? srcLNInst != null : !SRC_LN_INST_EDEFAULT.equals(srcLNInst);
      case SCLPackage.TEXT_REF__SRC_PREFIX:
        return SRC_PREFIX_EDEFAULT == null ? srcPrefix != null : !SRC_PREFIX_EDEFAULT.equals(srcPrefix);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (daName: ");
    result.append(daName);
    result.append(", desc: ");
    if (descESet) result.append(desc); else result.append("<unset>");
    result.append(", doName: ");
    result.append(doName);
    result.append(", iedName: ");
    result.append(iedName);
    result.append(", intAddr: ");
    result.append(intAddr);
    result.append(", ldInst: ");
    result.append(ldInst);
    result.append(", lnClass: ");
    result.append(lnClass);
    result.append(", lnInst: ");
    result.append(lnInst);
    result.append(", prefix: ");
    result.append(prefix);
    result.append(", serviceType: ");
    if (serviceTypeESet) result.append(serviceType); else result.append("<unset>");
    result.append(", srcCBName: ");
    result.append(srcCBName);
    result.append(", srcLDInst: ");
    result.append(srcLDInst);
    result.append(", srcLNClass: ");
    result.append(srcLNClass);
    result.append(", srcLNInst: ");
    result.append(srcLNInst);
    result.append(", srcPrefix: ");
    result.append(srcPrefix);
    result.append(')');
    return result.toString();
  }

} //TExtRefImpl
