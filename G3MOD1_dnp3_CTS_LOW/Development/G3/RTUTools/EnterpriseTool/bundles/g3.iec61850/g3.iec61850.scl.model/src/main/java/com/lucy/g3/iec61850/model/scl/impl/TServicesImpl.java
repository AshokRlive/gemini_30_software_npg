/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.internal.SCLObjectImpl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.SettingGroupsType;
import com.lucy.g3.iec61850.model.scl.TClientServices;
import com.lucy.g3.iec61850.model.scl.TConfLNs;
import com.lucy.g3.iec61850.model.scl.TGSESettings;
import com.lucy.g3.iec61850.model.scl.TLogSettings;
import com.lucy.g3.iec61850.model.scl.TReportSettings;
import com.lucy.g3.iec61850.model.scl.TSMVSettings;
import com.lucy.g3.iec61850.model.scl.TServiceConfReportControl;
import com.lucy.g3.iec61850.model.scl.TServiceForConfDataSet;
import com.lucy.g3.iec61850.model.scl.TServiceWithMax;
import com.lucy.g3.iec61850.model.scl.TServiceWithMaxAndMaxAttributes;
import com.lucy.g3.iec61850.model.scl.TServiceWithOptionalMax;
import com.lucy.g3.iec61850.model.scl.TServiceYesNo;
import com.lucy.g3.iec61850.model.scl.TServices;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TServices</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getDynAssociation <em>Dyn Association</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getSettingGroups <em>Setting Groups</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getGetDirectory <em>Get Directory</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getGetDataObjectDefinition <em>Get Data Object Definition</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getDataObjectDirectory <em>Data Object Directory</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getGetDataSetValue <em>Get Data Set Value</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getSetDataSetValue <em>Set Data Set Value</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getDataSetDirectory <em>Data Set Directory</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getConfDataSet <em>Conf Data Set</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getDynDataSet <em>Dyn Data Set</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getReadWrite <em>Read Write</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getTimerActivatedControl <em>Timer Activated Control</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getConfReportControl <em>Conf Report Control</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getGetCBValues <em>Get CB Values</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getConfLogControl <em>Conf Log Control</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getReportSettings <em>Report Settings</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getLogSettings <em>Log Settings</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getGSESettings <em>GSE Settings</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getSMVSettings <em>SMV Settings</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getGSEDir <em>GSE Dir</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getGOOSE <em>GOOSE</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getGSSE <em>GSSE</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getSMVsc <em>SM Vsc</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getFileHandling <em>File Handling</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getConfLNs <em>Conf LNs</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getClientServices <em>Client Services</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getConfLdName <em>Conf Ld Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getSupSubscription <em>Sup Subscription</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getConfSigRef <em>Conf Sig Ref</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServicesImpl#getNameLength <em>Name Length</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TServicesImpl extends SCLObjectImpl implements TServices {
  /**
   * The cached value of the '{@link #getDynAssociation() <em>Dyn Association</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDynAssociation()
   * @generated
   * @ordered
   */
  protected TServiceWithOptionalMax dynAssociation;

  /**
   * The cached value of the '{@link #getSettingGroups() <em>Setting Groups</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSettingGroups()
   * @generated
   * @ordered
   */
  protected SettingGroupsType settingGroups;

  /**
   * The cached value of the '{@link #getGetDirectory() <em>Get Directory</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGetDirectory()
   * @generated
   * @ordered
   */
  protected TServiceYesNo getDirectory;

  /**
   * The cached value of the '{@link #getGetDataObjectDefinition() <em>Get Data Object Definition</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGetDataObjectDefinition()
   * @generated
   * @ordered
   */
  protected TServiceYesNo getDataObjectDefinition;

  /**
   * The cached value of the '{@link #getDataObjectDirectory() <em>Data Object Directory</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDataObjectDirectory()
   * @generated
   * @ordered
   */
  protected TServiceYesNo dataObjectDirectory;

  /**
   * The cached value of the '{@link #getGetDataSetValue() <em>Get Data Set Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGetDataSetValue()
   * @generated
   * @ordered
   */
  protected TServiceYesNo getDataSetValue;

  /**
   * The cached value of the '{@link #getSetDataSetValue() <em>Set Data Set Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSetDataSetValue()
   * @generated
   * @ordered
   */
  protected TServiceYesNo setDataSetValue;

  /**
   * The cached value of the '{@link #getDataSetDirectory() <em>Data Set Directory</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDataSetDirectory()
   * @generated
   * @ordered
   */
  protected TServiceYesNo dataSetDirectory;

  /**
   * The cached value of the '{@link #getConfDataSet() <em>Conf Data Set</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConfDataSet()
   * @generated
   * @ordered
   */
  protected TServiceForConfDataSet confDataSet;

  /**
   * The cached value of the '{@link #getDynDataSet() <em>Dyn Data Set</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDynDataSet()
   * @generated
   * @ordered
   */
  protected TServiceWithMaxAndMaxAttributes dynDataSet;

  /**
   * The cached value of the '{@link #getReadWrite() <em>Read Write</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReadWrite()
   * @generated
   * @ordered
   */
  protected TServiceYesNo readWrite;

  /**
   * The cached value of the '{@link #getTimerActivatedControl() <em>Timer Activated Control</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTimerActivatedControl()
   * @generated
   * @ordered
   */
  protected TServiceYesNo timerActivatedControl;

  /**
   * The cached value of the '{@link #getConfReportControl() <em>Conf Report Control</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConfReportControl()
   * @generated
   * @ordered
   */
  protected TServiceConfReportControl confReportControl;

  /**
   * The cached value of the '{@link #getGetCBValues() <em>Get CB Values</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGetCBValues()
   * @generated
   * @ordered
   */
  protected TServiceYesNo getCBValues;

  /**
   * The cached value of the '{@link #getConfLogControl() <em>Conf Log Control</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConfLogControl()
   * @generated
   * @ordered
   */
  protected TServiceWithMax confLogControl;

  /**
   * The cached value of the '{@link #getReportSettings() <em>Report Settings</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReportSettings()
   * @generated
   * @ordered
   */
  protected TReportSettings reportSettings;

  /**
   * The cached value of the '{@link #getLogSettings() <em>Log Settings</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLogSettings()
   * @generated
   * @ordered
   */
  protected TLogSettings logSettings;

  /**
   * The cached value of the '{@link #getGSESettings() <em>GSE Settings</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGSESettings()
   * @generated
   * @ordered
   */
  protected TGSESettings gSESettings;

  /**
   * The cached value of the '{@link #getSMVSettings() <em>SMV Settings</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSMVSettings()
   * @generated
   * @ordered
   */
  protected TSMVSettings sMVSettings;

  /**
   * The cached value of the '{@link #getGSEDir() <em>GSE Dir</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGSEDir()
   * @generated
   * @ordered
   */
  protected TServiceYesNo gSEDir;

  /**
   * The cached value of the '{@link #getGOOSE() <em>GOOSE</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGOOSE()
   * @generated
   * @ordered
   */
  protected TServiceWithMax gOOSE;

  /**
   * The cached value of the '{@link #getGSSE() <em>GSSE</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGSSE()
   * @generated
   * @ordered
   */
  protected TServiceWithMax gSSE;

  /**
   * The cached value of the '{@link #getSMVsc() <em>SM Vsc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSMVsc()
   * @generated
   * @ordered
   */
  protected TServiceWithMax sMVsc;

  /**
   * The cached value of the '{@link #getFileHandling() <em>File Handling</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFileHandling()
   * @generated
   * @ordered
   */
  protected TServiceYesNo fileHandling;

  /**
   * The cached value of the '{@link #getConfLNs() <em>Conf LNs</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConfLNs()
   * @generated
   * @ordered
   */
  protected TConfLNs confLNs;

  /**
   * The cached value of the '{@link #getClientServices() <em>Client Services</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClientServices()
   * @generated
   * @ordered
   */
  protected TClientServices clientServices;

  /**
   * The cached value of the '{@link #getConfLdName() <em>Conf Ld Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConfLdName()
   * @generated
   * @ordered
   */
  protected TServiceYesNo confLdName;

  /**
   * The cached value of the '{@link #getSupSubscription() <em>Sup Subscription</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSupSubscription()
   * @generated
   * @ordered
   */
  protected TServiceWithMax supSubscription;

  /**
   * The cached value of the '{@link #getConfSigRef() <em>Conf Sig Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConfSigRef()
   * @generated
   * @ordered
   */
  protected TServiceWithMax confSigRef;

  /**
   * The default value of the '{@link #getNameLength() <em>Name Length</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNameLength()
   * @generated
   * @ordered
   */
  protected static final long NAME_LENGTH_EDEFAULT = 32L;

  /**
   * The cached value of the '{@link #getNameLength() <em>Name Length</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNameLength()
   * @generated
   * @ordered
   */
  protected long nameLength = NAME_LENGTH_EDEFAULT;

  /**
   * This is true if the Name Length attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean nameLengthESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TServicesImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTServices();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceWithOptionalMax getDynAssociation() {
    return dynAssociation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDynAssociation(TServiceWithOptionalMax newDynAssociation, NotificationChain msgs) {
    TServiceWithOptionalMax oldDynAssociation = dynAssociation;
    dynAssociation = newDynAssociation;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__DYN_ASSOCIATION, oldDynAssociation, newDynAssociation);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDynAssociation(TServiceWithOptionalMax newDynAssociation) {
    if (newDynAssociation != dynAssociation) {
      NotificationChain msgs = null;
      if (dynAssociation != null)
        msgs = ((InternalEObject)dynAssociation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__DYN_ASSOCIATION, null, msgs);
      if (newDynAssociation != null)
        msgs = ((InternalEObject)newDynAssociation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__DYN_ASSOCIATION, null, msgs);
      msgs = basicSetDynAssociation(newDynAssociation, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__DYN_ASSOCIATION, newDynAssociation, newDynAssociation));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SettingGroupsType getSettingGroups() {
    return settingGroups;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSettingGroups(SettingGroupsType newSettingGroups, NotificationChain msgs) {
    SettingGroupsType oldSettingGroups = settingGroups;
    settingGroups = newSettingGroups;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__SETTING_GROUPS, oldSettingGroups, newSettingGroups);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSettingGroups(SettingGroupsType newSettingGroups) {
    if (newSettingGroups != settingGroups) {
      NotificationChain msgs = null;
      if (settingGroups != null)
        msgs = ((InternalEObject)settingGroups).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__SETTING_GROUPS, null, msgs);
      if (newSettingGroups != null)
        msgs = ((InternalEObject)newSettingGroups).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__SETTING_GROUPS, null, msgs);
      msgs = basicSetSettingGroups(newSettingGroups, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__SETTING_GROUPS, newSettingGroups, newSettingGroups));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceYesNo getGetDirectory() {
    return getDirectory;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGetDirectory(TServiceYesNo newGetDirectory, NotificationChain msgs) {
    TServiceYesNo oldGetDirectory = getDirectory;
    getDirectory = newGetDirectory;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GET_DIRECTORY, oldGetDirectory, newGetDirectory);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGetDirectory(TServiceYesNo newGetDirectory) {
    if (newGetDirectory != getDirectory) {
      NotificationChain msgs = null;
      if (getDirectory != null)
        msgs = ((InternalEObject)getDirectory).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GET_DIRECTORY, null, msgs);
      if (newGetDirectory != null)
        msgs = ((InternalEObject)newGetDirectory).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GET_DIRECTORY, null, msgs);
      msgs = basicSetGetDirectory(newGetDirectory, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GET_DIRECTORY, newGetDirectory, newGetDirectory));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceYesNo getGetDataObjectDefinition() {
    return getDataObjectDefinition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGetDataObjectDefinition(TServiceYesNo newGetDataObjectDefinition, NotificationChain msgs) {
    TServiceYesNo oldGetDataObjectDefinition = getDataObjectDefinition;
    getDataObjectDefinition = newGetDataObjectDefinition;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GET_DATA_OBJECT_DEFINITION, oldGetDataObjectDefinition, newGetDataObjectDefinition);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGetDataObjectDefinition(TServiceYesNo newGetDataObjectDefinition) {
    if (newGetDataObjectDefinition != getDataObjectDefinition) {
      NotificationChain msgs = null;
      if (getDataObjectDefinition != null)
        msgs = ((InternalEObject)getDataObjectDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GET_DATA_OBJECT_DEFINITION, null, msgs);
      if (newGetDataObjectDefinition != null)
        msgs = ((InternalEObject)newGetDataObjectDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GET_DATA_OBJECT_DEFINITION, null, msgs);
      msgs = basicSetGetDataObjectDefinition(newGetDataObjectDefinition, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GET_DATA_OBJECT_DEFINITION, newGetDataObjectDefinition, newGetDataObjectDefinition));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceYesNo getDataObjectDirectory() {
    return dataObjectDirectory;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDataObjectDirectory(TServiceYesNo newDataObjectDirectory, NotificationChain msgs) {
    TServiceYesNo oldDataObjectDirectory = dataObjectDirectory;
    dataObjectDirectory = newDataObjectDirectory;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__DATA_OBJECT_DIRECTORY, oldDataObjectDirectory, newDataObjectDirectory);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDataObjectDirectory(TServiceYesNo newDataObjectDirectory) {
    if (newDataObjectDirectory != dataObjectDirectory) {
      NotificationChain msgs = null;
      if (dataObjectDirectory != null)
        msgs = ((InternalEObject)dataObjectDirectory).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__DATA_OBJECT_DIRECTORY, null, msgs);
      if (newDataObjectDirectory != null)
        msgs = ((InternalEObject)newDataObjectDirectory).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__DATA_OBJECT_DIRECTORY, null, msgs);
      msgs = basicSetDataObjectDirectory(newDataObjectDirectory, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__DATA_OBJECT_DIRECTORY, newDataObjectDirectory, newDataObjectDirectory));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceYesNo getGetDataSetValue() {
    return getDataSetValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGetDataSetValue(TServiceYesNo newGetDataSetValue, NotificationChain msgs) {
    TServiceYesNo oldGetDataSetValue = getDataSetValue;
    getDataSetValue = newGetDataSetValue;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GET_DATA_SET_VALUE, oldGetDataSetValue, newGetDataSetValue);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGetDataSetValue(TServiceYesNo newGetDataSetValue) {
    if (newGetDataSetValue != getDataSetValue) {
      NotificationChain msgs = null;
      if (getDataSetValue != null)
        msgs = ((InternalEObject)getDataSetValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GET_DATA_SET_VALUE, null, msgs);
      if (newGetDataSetValue != null)
        msgs = ((InternalEObject)newGetDataSetValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GET_DATA_SET_VALUE, null, msgs);
      msgs = basicSetGetDataSetValue(newGetDataSetValue, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GET_DATA_SET_VALUE, newGetDataSetValue, newGetDataSetValue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceYesNo getSetDataSetValue() {
    return setDataSetValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSetDataSetValue(TServiceYesNo newSetDataSetValue, NotificationChain msgs) {
    TServiceYesNo oldSetDataSetValue = setDataSetValue;
    setDataSetValue = newSetDataSetValue;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__SET_DATA_SET_VALUE, oldSetDataSetValue, newSetDataSetValue);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSetDataSetValue(TServiceYesNo newSetDataSetValue) {
    if (newSetDataSetValue != setDataSetValue) {
      NotificationChain msgs = null;
      if (setDataSetValue != null)
        msgs = ((InternalEObject)setDataSetValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__SET_DATA_SET_VALUE, null, msgs);
      if (newSetDataSetValue != null)
        msgs = ((InternalEObject)newSetDataSetValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__SET_DATA_SET_VALUE, null, msgs);
      msgs = basicSetSetDataSetValue(newSetDataSetValue, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__SET_DATA_SET_VALUE, newSetDataSetValue, newSetDataSetValue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceYesNo getDataSetDirectory() {
    return dataSetDirectory;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDataSetDirectory(TServiceYesNo newDataSetDirectory, NotificationChain msgs) {
    TServiceYesNo oldDataSetDirectory = dataSetDirectory;
    dataSetDirectory = newDataSetDirectory;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__DATA_SET_DIRECTORY, oldDataSetDirectory, newDataSetDirectory);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDataSetDirectory(TServiceYesNo newDataSetDirectory) {
    if (newDataSetDirectory != dataSetDirectory) {
      NotificationChain msgs = null;
      if (dataSetDirectory != null)
        msgs = ((InternalEObject)dataSetDirectory).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__DATA_SET_DIRECTORY, null, msgs);
      if (newDataSetDirectory != null)
        msgs = ((InternalEObject)newDataSetDirectory).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__DATA_SET_DIRECTORY, null, msgs);
      msgs = basicSetDataSetDirectory(newDataSetDirectory, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__DATA_SET_DIRECTORY, newDataSetDirectory, newDataSetDirectory));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceForConfDataSet getConfDataSet() {
    return confDataSet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConfDataSet(TServiceForConfDataSet newConfDataSet, NotificationChain msgs) {
    TServiceForConfDataSet oldConfDataSet = confDataSet;
    confDataSet = newConfDataSet;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CONF_DATA_SET, oldConfDataSet, newConfDataSet);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConfDataSet(TServiceForConfDataSet newConfDataSet) {
    if (newConfDataSet != confDataSet) {
      NotificationChain msgs = null;
      if (confDataSet != null)
        msgs = ((InternalEObject)confDataSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CONF_DATA_SET, null, msgs);
      if (newConfDataSet != null)
        msgs = ((InternalEObject)newConfDataSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CONF_DATA_SET, null, msgs);
      msgs = basicSetConfDataSet(newConfDataSet, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CONF_DATA_SET, newConfDataSet, newConfDataSet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceWithMaxAndMaxAttributes getDynDataSet() {
    return dynDataSet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDynDataSet(TServiceWithMaxAndMaxAttributes newDynDataSet, NotificationChain msgs) {
    TServiceWithMaxAndMaxAttributes oldDynDataSet = dynDataSet;
    dynDataSet = newDynDataSet;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__DYN_DATA_SET, oldDynDataSet, newDynDataSet);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDynDataSet(TServiceWithMaxAndMaxAttributes newDynDataSet) {
    if (newDynDataSet != dynDataSet) {
      NotificationChain msgs = null;
      if (dynDataSet != null)
        msgs = ((InternalEObject)dynDataSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__DYN_DATA_SET, null, msgs);
      if (newDynDataSet != null)
        msgs = ((InternalEObject)newDynDataSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__DYN_DATA_SET, null, msgs);
      msgs = basicSetDynDataSet(newDynDataSet, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__DYN_DATA_SET, newDynDataSet, newDynDataSet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceYesNo getReadWrite() {
    return readWrite;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReadWrite(TServiceYesNo newReadWrite, NotificationChain msgs) {
    TServiceYesNo oldReadWrite = readWrite;
    readWrite = newReadWrite;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__READ_WRITE, oldReadWrite, newReadWrite);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReadWrite(TServiceYesNo newReadWrite) {
    if (newReadWrite != readWrite) {
      NotificationChain msgs = null;
      if (readWrite != null)
        msgs = ((InternalEObject)readWrite).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__READ_WRITE, null, msgs);
      if (newReadWrite != null)
        msgs = ((InternalEObject)newReadWrite).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__READ_WRITE, null, msgs);
      msgs = basicSetReadWrite(newReadWrite, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__READ_WRITE, newReadWrite, newReadWrite));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceYesNo getTimerActivatedControl() {
    return timerActivatedControl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTimerActivatedControl(TServiceYesNo newTimerActivatedControl, NotificationChain msgs) {
    TServiceYesNo oldTimerActivatedControl = timerActivatedControl;
    timerActivatedControl = newTimerActivatedControl;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__TIMER_ACTIVATED_CONTROL, oldTimerActivatedControl, newTimerActivatedControl);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTimerActivatedControl(TServiceYesNo newTimerActivatedControl) {
    if (newTimerActivatedControl != timerActivatedControl) {
      NotificationChain msgs = null;
      if (timerActivatedControl != null)
        msgs = ((InternalEObject)timerActivatedControl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__TIMER_ACTIVATED_CONTROL, null, msgs);
      if (newTimerActivatedControl != null)
        msgs = ((InternalEObject)newTimerActivatedControl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__TIMER_ACTIVATED_CONTROL, null, msgs);
      msgs = basicSetTimerActivatedControl(newTimerActivatedControl, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__TIMER_ACTIVATED_CONTROL, newTimerActivatedControl, newTimerActivatedControl));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceConfReportControl getConfReportControl() {
    return confReportControl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConfReportControl(TServiceConfReportControl newConfReportControl, NotificationChain msgs) {
    TServiceConfReportControl oldConfReportControl = confReportControl;
    confReportControl = newConfReportControl;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CONF_REPORT_CONTROL, oldConfReportControl, newConfReportControl);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConfReportControl(TServiceConfReportControl newConfReportControl) {
    if (newConfReportControl != confReportControl) {
      NotificationChain msgs = null;
      if (confReportControl != null)
        msgs = ((InternalEObject)confReportControl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CONF_REPORT_CONTROL, null, msgs);
      if (newConfReportControl != null)
        msgs = ((InternalEObject)newConfReportControl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CONF_REPORT_CONTROL, null, msgs);
      msgs = basicSetConfReportControl(newConfReportControl, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CONF_REPORT_CONTROL, newConfReportControl, newConfReportControl));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceYesNo getGetCBValues() {
    return getCBValues;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGetCBValues(TServiceYesNo newGetCBValues, NotificationChain msgs) {
    TServiceYesNo oldGetCBValues = getCBValues;
    getCBValues = newGetCBValues;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GET_CB_VALUES, oldGetCBValues, newGetCBValues);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGetCBValues(TServiceYesNo newGetCBValues) {
    if (newGetCBValues != getCBValues) {
      NotificationChain msgs = null;
      if (getCBValues != null)
        msgs = ((InternalEObject)getCBValues).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GET_CB_VALUES, null, msgs);
      if (newGetCBValues != null)
        msgs = ((InternalEObject)newGetCBValues).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GET_CB_VALUES, null, msgs);
      msgs = basicSetGetCBValues(newGetCBValues, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GET_CB_VALUES, newGetCBValues, newGetCBValues));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceWithMax getConfLogControl() {
    return confLogControl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConfLogControl(TServiceWithMax newConfLogControl, NotificationChain msgs) {
    TServiceWithMax oldConfLogControl = confLogControl;
    confLogControl = newConfLogControl;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CONF_LOG_CONTROL, oldConfLogControl, newConfLogControl);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConfLogControl(TServiceWithMax newConfLogControl) {
    if (newConfLogControl != confLogControl) {
      NotificationChain msgs = null;
      if (confLogControl != null)
        msgs = ((InternalEObject)confLogControl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CONF_LOG_CONTROL, null, msgs);
      if (newConfLogControl != null)
        msgs = ((InternalEObject)newConfLogControl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CONF_LOG_CONTROL, null, msgs);
      msgs = basicSetConfLogControl(newConfLogControl, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CONF_LOG_CONTROL, newConfLogControl, newConfLogControl));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TReportSettings getReportSettings() {
    return reportSettings;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReportSettings(TReportSettings newReportSettings, NotificationChain msgs) {
    TReportSettings oldReportSettings = reportSettings;
    reportSettings = newReportSettings;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__REPORT_SETTINGS, oldReportSettings, newReportSettings);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReportSettings(TReportSettings newReportSettings) {
    if (newReportSettings != reportSettings) {
      NotificationChain msgs = null;
      if (reportSettings != null)
        msgs = ((InternalEObject)reportSettings).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__REPORT_SETTINGS, null, msgs);
      if (newReportSettings != null)
        msgs = ((InternalEObject)newReportSettings).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__REPORT_SETTINGS, null, msgs);
      msgs = basicSetReportSettings(newReportSettings, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__REPORT_SETTINGS, newReportSettings, newReportSettings));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TLogSettings getLogSettings() {
    return logSettings;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLogSettings(TLogSettings newLogSettings, NotificationChain msgs) {
    TLogSettings oldLogSettings = logSettings;
    logSettings = newLogSettings;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__LOG_SETTINGS, oldLogSettings, newLogSettings);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLogSettings(TLogSettings newLogSettings) {
    if (newLogSettings != logSettings) {
      NotificationChain msgs = null;
      if (logSettings != null)
        msgs = ((InternalEObject)logSettings).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__LOG_SETTINGS, null, msgs);
      if (newLogSettings != null)
        msgs = ((InternalEObject)newLogSettings).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__LOG_SETTINGS, null, msgs);
      msgs = basicSetLogSettings(newLogSettings, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__LOG_SETTINGS, newLogSettings, newLogSettings));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TGSESettings getGSESettings() {
    return gSESettings;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGSESettings(TGSESettings newGSESettings, NotificationChain msgs) {
    TGSESettings oldGSESettings = gSESettings;
    gSESettings = newGSESettings;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GSE_SETTINGS, oldGSESettings, newGSESettings);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGSESettings(TGSESettings newGSESettings) {
    if (newGSESettings != gSESettings) {
      NotificationChain msgs = null;
      if (gSESettings != null)
        msgs = ((InternalEObject)gSESettings).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GSE_SETTINGS, null, msgs);
      if (newGSESettings != null)
        msgs = ((InternalEObject)newGSESettings).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GSE_SETTINGS, null, msgs);
      msgs = basicSetGSESettings(newGSESettings, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GSE_SETTINGS, newGSESettings, newGSESettings));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSMVSettings getSMVSettings() {
    return sMVSettings;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSMVSettings(TSMVSettings newSMVSettings, NotificationChain msgs) {
    TSMVSettings oldSMVSettings = sMVSettings;
    sMVSettings = newSMVSettings;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__SMV_SETTINGS, oldSMVSettings, newSMVSettings);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSMVSettings(TSMVSettings newSMVSettings) {
    if (newSMVSettings != sMVSettings) {
      NotificationChain msgs = null;
      if (sMVSettings != null)
        msgs = ((InternalEObject)sMVSettings).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__SMV_SETTINGS, null, msgs);
      if (newSMVSettings != null)
        msgs = ((InternalEObject)newSMVSettings).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__SMV_SETTINGS, null, msgs);
      msgs = basicSetSMVSettings(newSMVSettings, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__SMV_SETTINGS, newSMVSettings, newSMVSettings));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceYesNo getGSEDir() {
    return gSEDir;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGSEDir(TServiceYesNo newGSEDir, NotificationChain msgs) {
    TServiceYesNo oldGSEDir = gSEDir;
    gSEDir = newGSEDir;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GSE_DIR, oldGSEDir, newGSEDir);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGSEDir(TServiceYesNo newGSEDir) {
    if (newGSEDir != gSEDir) {
      NotificationChain msgs = null;
      if (gSEDir != null)
        msgs = ((InternalEObject)gSEDir).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GSE_DIR, null, msgs);
      if (newGSEDir != null)
        msgs = ((InternalEObject)newGSEDir).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GSE_DIR, null, msgs);
      msgs = basicSetGSEDir(newGSEDir, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GSE_DIR, newGSEDir, newGSEDir));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceWithMax getGOOSE() {
    return gOOSE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGOOSE(TServiceWithMax newGOOSE, NotificationChain msgs) {
    TServiceWithMax oldGOOSE = gOOSE;
    gOOSE = newGOOSE;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GOOSE, oldGOOSE, newGOOSE);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGOOSE(TServiceWithMax newGOOSE) {
    if (newGOOSE != gOOSE) {
      NotificationChain msgs = null;
      if (gOOSE != null)
        msgs = ((InternalEObject)gOOSE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GOOSE, null, msgs);
      if (newGOOSE != null)
        msgs = ((InternalEObject)newGOOSE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GOOSE, null, msgs);
      msgs = basicSetGOOSE(newGOOSE, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GOOSE, newGOOSE, newGOOSE));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceWithMax getGSSE() {
    return gSSE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGSSE(TServiceWithMax newGSSE, NotificationChain msgs) {
    TServiceWithMax oldGSSE = gSSE;
    gSSE = newGSSE;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GSSE, oldGSSE, newGSSE);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGSSE(TServiceWithMax newGSSE) {
    if (newGSSE != gSSE) {
      NotificationChain msgs = null;
      if (gSSE != null)
        msgs = ((InternalEObject)gSSE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GSSE, null, msgs);
      if (newGSSE != null)
        msgs = ((InternalEObject)newGSSE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__GSSE, null, msgs);
      msgs = basicSetGSSE(newGSSE, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__GSSE, newGSSE, newGSSE));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceWithMax getSMVsc() {
    return sMVsc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSMVsc(TServiceWithMax newSMVsc, NotificationChain msgs) {
    TServiceWithMax oldSMVsc = sMVsc;
    sMVsc = newSMVsc;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__SM_VSC, oldSMVsc, newSMVsc);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSMVsc(TServiceWithMax newSMVsc) {
    if (newSMVsc != sMVsc) {
      NotificationChain msgs = null;
      if (sMVsc != null)
        msgs = ((InternalEObject)sMVsc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__SM_VSC, null, msgs);
      if (newSMVsc != null)
        msgs = ((InternalEObject)newSMVsc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__SM_VSC, null, msgs);
      msgs = basicSetSMVsc(newSMVsc, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__SM_VSC, newSMVsc, newSMVsc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceYesNo getFileHandling() {
    return fileHandling;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFileHandling(TServiceYesNo newFileHandling, NotificationChain msgs) {
    TServiceYesNo oldFileHandling = fileHandling;
    fileHandling = newFileHandling;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__FILE_HANDLING, oldFileHandling, newFileHandling);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFileHandling(TServiceYesNo newFileHandling) {
    if (newFileHandling != fileHandling) {
      NotificationChain msgs = null;
      if (fileHandling != null)
        msgs = ((InternalEObject)fileHandling).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__FILE_HANDLING, null, msgs);
      if (newFileHandling != null)
        msgs = ((InternalEObject)newFileHandling).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__FILE_HANDLING, null, msgs);
      msgs = basicSetFileHandling(newFileHandling, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__FILE_HANDLING, newFileHandling, newFileHandling));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TConfLNs getConfLNs() {
    return confLNs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConfLNs(TConfLNs newConfLNs, NotificationChain msgs) {
    TConfLNs oldConfLNs = confLNs;
    confLNs = newConfLNs;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CONF_LNS, oldConfLNs, newConfLNs);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConfLNs(TConfLNs newConfLNs) {
    if (newConfLNs != confLNs) {
      NotificationChain msgs = null;
      if (confLNs != null)
        msgs = ((InternalEObject)confLNs).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CONF_LNS, null, msgs);
      if (newConfLNs != null)
        msgs = ((InternalEObject)newConfLNs).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CONF_LNS, null, msgs);
      msgs = basicSetConfLNs(newConfLNs, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CONF_LNS, newConfLNs, newConfLNs));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TClientServices getClientServices() {
    return clientServices;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetClientServices(TClientServices newClientServices, NotificationChain msgs) {
    TClientServices oldClientServices = clientServices;
    clientServices = newClientServices;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CLIENT_SERVICES, oldClientServices, newClientServices);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setClientServices(TClientServices newClientServices) {
    if (newClientServices != clientServices) {
      NotificationChain msgs = null;
      if (clientServices != null)
        msgs = ((InternalEObject)clientServices).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CLIENT_SERVICES, null, msgs);
      if (newClientServices != null)
        msgs = ((InternalEObject)newClientServices).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CLIENT_SERVICES, null, msgs);
      msgs = basicSetClientServices(newClientServices, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CLIENT_SERVICES, newClientServices, newClientServices));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceYesNo getConfLdName() {
    return confLdName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConfLdName(TServiceYesNo newConfLdName, NotificationChain msgs) {
    TServiceYesNo oldConfLdName = confLdName;
    confLdName = newConfLdName;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CONF_LD_NAME, oldConfLdName, newConfLdName);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConfLdName(TServiceYesNo newConfLdName) {
    if (newConfLdName != confLdName) {
      NotificationChain msgs = null;
      if (confLdName != null)
        msgs = ((InternalEObject)confLdName).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CONF_LD_NAME, null, msgs);
      if (newConfLdName != null)
        msgs = ((InternalEObject)newConfLdName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CONF_LD_NAME, null, msgs);
      msgs = basicSetConfLdName(newConfLdName, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CONF_LD_NAME, newConfLdName, newConfLdName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceWithMax getSupSubscription() {
    return supSubscription;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSupSubscription(TServiceWithMax newSupSubscription, NotificationChain msgs) {
    TServiceWithMax oldSupSubscription = supSubscription;
    supSubscription = newSupSubscription;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__SUP_SUBSCRIPTION, oldSupSubscription, newSupSubscription);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSupSubscription(TServiceWithMax newSupSubscription) {
    if (newSupSubscription != supSubscription) {
      NotificationChain msgs = null;
      if (supSubscription != null)
        msgs = ((InternalEObject)supSubscription).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__SUP_SUBSCRIPTION, null, msgs);
      if (newSupSubscription != null)
        msgs = ((InternalEObject)newSupSubscription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__SUP_SUBSCRIPTION, null, msgs);
      msgs = basicSetSupSubscription(newSupSubscription, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__SUP_SUBSCRIPTION, newSupSubscription, newSupSubscription));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServiceWithMax getConfSigRef() {
    return confSigRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConfSigRef(TServiceWithMax newConfSigRef, NotificationChain msgs) {
    TServiceWithMax oldConfSigRef = confSigRef;
    confSigRef = newConfSigRef;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CONF_SIG_REF, oldConfSigRef, newConfSigRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConfSigRef(TServiceWithMax newConfSigRef) {
    if (newConfSigRef != confSigRef) {
      NotificationChain msgs = null;
      if (confSigRef != null)
        msgs = ((InternalEObject)confSigRef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CONF_SIG_REF, null, msgs);
      if (newConfSigRef != null)
        msgs = ((InternalEObject)newConfSigRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVICES__CONF_SIG_REF, null, msgs);
      msgs = basicSetConfSigRef(newConfSigRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__CONF_SIG_REF, newConfSigRef, newConfSigRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public long getNameLength() {
    return nameLength;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNameLength(long newNameLength) {
    long oldNameLength = nameLength;
    nameLength = newNameLength;
    boolean oldNameLengthESet = nameLengthESet;
    nameLengthESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVICES__NAME_LENGTH, oldNameLength, nameLength, !oldNameLengthESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetNameLength() {
    long oldNameLength = nameLength;
    boolean oldNameLengthESet = nameLengthESet;
    nameLength = NAME_LENGTH_EDEFAULT;
    nameLengthESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TSERVICES__NAME_LENGTH, oldNameLength, NAME_LENGTH_EDEFAULT, oldNameLengthESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetNameLength() {
    return nameLengthESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case SCLPackage.TSERVICES__DYN_ASSOCIATION:
        return basicSetDynAssociation(null, msgs);
      case SCLPackage.TSERVICES__SETTING_GROUPS:
        return basicSetSettingGroups(null, msgs);
      case SCLPackage.TSERVICES__GET_DIRECTORY:
        return basicSetGetDirectory(null, msgs);
      case SCLPackage.TSERVICES__GET_DATA_OBJECT_DEFINITION:
        return basicSetGetDataObjectDefinition(null, msgs);
      case SCLPackage.TSERVICES__DATA_OBJECT_DIRECTORY:
        return basicSetDataObjectDirectory(null, msgs);
      case SCLPackage.TSERVICES__GET_DATA_SET_VALUE:
        return basicSetGetDataSetValue(null, msgs);
      case SCLPackage.TSERVICES__SET_DATA_SET_VALUE:
        return basicSetSetDataSetValue(null, msgs);
      case SCLPackage.TSERVICES__DATA_SET_DIRECTORY:
        return basicSetDataSetDirectory(null, msgs);
      case SCLPackage.TSERVICES__CONF_DATA_SET:
        return basicSetConfDataSet(null, msgs);
      case SCLPackage.TSERVICES__DYN_DATA_SET:
        return basicSetDynDataSet(null, msgs);
      case SCLPackage.TSERVICES__READ_WRITE:
        return basicSetReadWrite(null, msgs);
      case SCLPackage.TSERVICES__TIMER_ACTIVATED_CONTROL:
        return basicSetTimerActivatedControl(null, msgs);
      case SCLPackage.TSERVICES__CONF_REPORT_CONTROL:
        return basicSetConfReportControl(null, msgs);
      case SCLPackage.TSERVICES__GET_CB_VALUES:
        return basicSetGetCBValues(null, msgs);
      case SCLPackage.TSERVICES__CONF_LOG_CONTROL:
        return basicSetConfLogControl(null, msgs);
      case SCLPackage.TSERVICES__REPORT_SETTINGS:
        return basicSetReportSettings(null, msgs);
      case SCLPackage.TSERVICES__LOG_SETTINGS:
        return basicSetLogSettings(null, msgs);
      case SCLPackage.TSERVICES__GSE_SETTINGS:
        return basicSetGSESettings(null, msgs);
      case SCLPackage.TSERVICES__SMV_SETTINGS:
        return basicSetSMVSettings(null, msgs);
      case SCLPackage.TSERVICES__GSE_DIR:
        return basicSetGSEDir(null, msgs);
      case SCLPackage.TSERVICES__GOOSE:
        return basicSetGOOSE(null, msgs);
      case SCLPackage.TSERVICES__GSSE:
        return basicSetGSSE(null, msgs);
      case SCLPackage.TSERVICES__SM_VSC:
        return basicSetSMVsc(null, msgs);
      case SCLPackage.TSERVICES__FILE_HANDLING:
        return basicSetFileHandling(null, msgs);
      case SCLPackage.TSERVICES__CONF_LNS:
        return basicSetConfLNs(null, msgs);
      case SCLPackage.TSERVICES__CLIENT_SERVICES:
        return basicSetClientServices(null, msgs);
      case SCLPackage.TSERVICES__CONF_LD_NAME:
        return basicSetConfLdName(null, msgs);
      case SCLPackage.TSERVICES__SUP_SUBSCRIPTION:
        return basicSetSupSubscription(null, msgs);
      case SCLPackage.TSERVICES__CONF_SIG_REF:
        return basicSetConfSigRef(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TSERVICES__DYN_ASSOCIATION:
        return getDynAssociation();
      case SCLPackage.TSERVICES__SETTING_GROUPS:
        return getSettingGroups();
      case SCLPackage.TSERVICES__GET_DIRECTORY:
        return getGetDirectory();
      case SCLPackage.TSERVICES__GET_DATA_OBJECT_DEFINITION:
        return getGetDataObjectDefinition();
      case SCLPackage.TSERVICES__DATA_OBJECT_DIRECTORY:
        return getDataObjectDirectory();
      case SCLPackage.TSERVICES__GET_DATA_SET_VALUE:
        return getGetDataSetValue();
      case SCLPackage.TSERVICES__SET_DATA_SET_VALUE:
        return getSetDataSetValue();
      case SCLPackage.TSERVICES__DATA_SET_DIRECTORY:
        return getDataSetDirectory();
      case SCLPackage.TSERVICES__CONF_DATA_SET:
        return getConfDataSet();
      case SCLPackage.TSERVICES__DYN_DATA_SET:
        return getDynDataSet();
      case SCLPackage.TSERVICES__READ_WRITE:
        return getReadWrite();
      case SCLPackage.TSERVICES__TIMER_ACTIVATED_CONTROL:
        return getTimerActivatedControl();
      case SCLPackage.TSERVICES__CONF_REPORT_CONTROL:
        return getConfReportControl();
      case SCLPackage.TSERVICES__GET_CB_VALUES:
        return getGetCBValues();
      case SCLPackage.TSERVICES__CONF_LOG_CONTROL:
        return getConfLogControl();
      case SCLPackage.TSERVICES__REPORT_SETTINGS:
        return getReportSettings();
      case SCLPackage.TSERVICES__LOG_SETTINGS:
        return getLogSettings();
      case SCLPackage.TSERVICES__GSE_SETTINGS:
        return getGSESettings();
      case SCLPackage.TSERVICES__SMV_SETTINGS:
        return getSMVSettings();
      case SCLPackage.TSERVICES__GSE_DIR:
        return getGSEDir();
      case SCLPackage.TSERVICES__GOOSE:
        return getGOOSE();
      case SCLPackage.TSERVICES__GSSE:
        return getGSSE();
      case SCLPackage.TSERVICES__SM_VSC:
        return getSMVsc();
      case SCLPackage.TSERVICES__FILE_HANDLING:
        return getFileHandling();
      case SCLPackage.TSERVICES__CONF_LNS:
        return getConfLNs();
      case SCLPackage.TSERVICES__CLIENT_SERVICES:
        return getClientServices();
      case SCLPackage.TSERVICES__CONF_LD_NAME:
        return getConfLdName();
      case SCLPackage.TSERVICES__SUP_SUBSCRIPTION:
        return getSupSubscription();
      case SCLPackage.TSERVICES__CONF_SIG_REF:
        return getConfSigRef();
      case SCLPackage.TSERVICES__NAME_LENGTH:
        return getNameLength();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TSERVICES__DYN_ASSOCIATION:
        setDynAssociation((TServiceWithOptionalMax)newValue);
        return;
      case SCLPackage.TSERVICES__SETTING_GROUPS:
        setSettingGroups((SettingGroupsType)newValue);
        return;
      case SCLPackage.TSERVICES__GET_DIRECTORY:
        setGetDirectory((TServiceYesNo)newValue);
        return;
      case SCLPackage.TSERVICES__GET_DATA_OBJECT_DEFINITION:
        setGetDataObjectDefinition((TServiceYesNo)newValue);
        return;
      case SCLPackage.TSERVICES__DATA_OBJECT_DIRECTORY:
        setDataObjectDirectory((TServiceYesNo)newValue);
        return;
      case SCLPackage.TSERVICES__GET_DATA_SET_VALUE:
        setGetDataSetValue((TServiceYesNo)newValue);
        return;
      case SCLPackage.TSERVICES__SET_DATA_SET_VALUE:
        setSetDataSetValue((TServiceYesNo)newValue);
        return;
      case SCLPackage.TSERVICES__DATA_SET_DIRECTORY:
        setDataSetDirectory((TServiceYesNo)newValue);
        return;
      case SCLPackage.TSERVICES__CONF_DATA_SET:
        setConfDataSet((TServiceForConfDataSet)newValue);
        return;
      case SCLPackage.TSERVICES__DYN_DATA_SET:
        setDynDataSet((TServiceWithMaxAndMaxAttributes)newValue);
        return;
      case SCLPackage.TSERVICES__READ_WRITE:
        setReadWrite((TServiceYesNo)newValue);
        return;
      case SCLPackage.TSERVICES__TIMER_ACTIVATED_CONTROL:
        setTimerActivatedControl((TServiceYesNo)newValue);
        return;
      case SCLPackage.TSERVICES__CONF_REPORT_CONTROL:
        setConfReportControl((TServiceConfReportControl)newValue);
        return;
      case SCLPackage.TSERVICES__GET_CB_VALUES:
        setGetCBValues((TServiceYesNo)newValue);
        return;
      case SCLPackage.TSERVICES__CONF_LOG_CONTROL:
        setConfLogControl((TServiceWithMax)newValue);
        return;
      case SCLPackage.TSERVICES__REPORT_SETTINGS:
        setReportSettings((TReportSettings)newValue);
        return;
      case SCLPackage.TSERVICES__LOG_SETTINGS:
        setLogSettings((TLogSettings)newValue);
        return;
      case SCLPackage.TSERVICES__GSE_SETTINGS:
        setGSESettings((TGSESettings)newValue);
        return;
      case SCLPackage.TSERVICES__SMV_SETTINGS:
        setSMVSettings((TSMVSettings)newValue);
        return;
      case SCLPackage.TSERVICES__GSE_DIR:
        setGSEDir((TServiceYesNo)newValue);
        return;
      case SCLPackage.TSERVICES__GOOSE:
        setGOOSE((TServiceWithMax)newValue);
        return;
      case SCLPackage.TSERVICES__GSSE:
        setGSSE((TServiceWithMax)newValue);
        return;
      case SCLPackage.TSERVICES__SM_VSC:
        setSMVsc((TServiceWithMax)newValue);
        return;
      case SCLPackage.TSERVICES__FILE_HANDLING:
        setFileHandling((TServiceYesNo)newValue);
        return;
      case SCLPackage.TSERVICES__CONF_LNS:
        setConfLNs((TConfLNs)newValue);
        return;
      case SCLPackage.TSERVICES__CLIENT_SERVICES:
        setClientServices((TClientServices)newValue);
        return;
      case SCLPackage.TSERVICES__CONF_LD_NAME:
        setConfLdName((TServiceYesNo)newValue);
        return;
      case SCLPackage.TSERVICES__SUP_SUBSCRIPTION:
        setSupSubscription((TServiceWithMax)newValue);
        return;
      case SCLPackage.TSERVICES__CONF_SIG_REF:
        setConfSigRef((TServiceWithMax)newValue);
        return;
      case SCLPackage.TSERVICES__NAME_LENGTH:
        setNameLength((Long)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TSERVICES__DYN_ASSOCIATION:
        setDynAssociation((TServiceWithOptionalMax)null);
        return;
      case SCLPackage.TSERVICES__SETTING_GROUPS:
        setSettingGroups((SettingGroupsType)null);
        return;
      case SCLPackage.TSERVICES__GET_DIRECTORY:
        setGetDirectory((TServiceYesNo)null);
        return;
      case SCLPackage.TSERVICES__GET_DATA_OBJECT_DEFINITION:
        setGetDataObjectDefinition((TServiceYesNo)null);
        return;
      case SCLPackage.TSERVICES__DATA_OBJECT_DIRECTORY:
        setDataObjectDirectory((TServiceYesNo)null);
        return;
      case SCLPackage.TSERVICES__GET_DATA_SET_VALUE:
        setGetDataSetValue((TServiceYesNo)null);
        return;
      case SCLPackage.TSERVICES__SET_DATA_SET_VALUE:
        setSetDataSetValue((TServiceYesNo)null);
        return;
      case SCLPackage.TSERVICES__DATA_SET_DIRECTORY:
        setDataSetDirectory((TServiceYesNo)null);
        return;
      case SCLPackage.TSERVICES__CONF_DATA_SET:
        setConfDataSet((TServiceForConfDataSet)null);
        return;
      case SCLPackage.TSERVICES__DYN_DATA_SET:
        setDynDataSet((TServiceWithMaxAndMaxAttributes)null);
        return;
      case SCLPackage.TSERVICES__READ_WRITE:
        setReadWrite((TServiceYesNo)null);
        return;
      case SCLPackage.TSERVICES__TIMER_ACTIVATED_CONTROL:
        setTimerActivatedControl((TServiceYesNo)null);
        return;
      case SCLPackage.TSERVICES__CONF_REPORT_CONTROL:
        setConfReportControl((TServiceConfReportControl)null);
        return;
      case SCLPackage.TSERVICES__GET_CB_VALUES:
        setGetCBValues((TServiceYesNo)null);
        return;
      case SCLPackage.TSERVICES__CONF_LOG_CONTROL:
        setConfLogControl((TServiceWithMax)null);
        return;
      case SCLPackage.TSERVICES__REPORT_SETTINGS:
        setReportSettings((TReportSettings)null);
        return;
      case SCLPackage.TSERVICES__LOG_SETTINGS:
        setLogSettings((TLogSettings)null);
        return;
      case SCLPackage.TSERVICES__GSE_SETTINGS:
        setGSESettings((TGSESettings)null);
        return;
      case SCLPackage.TSERVICES__SMV_SETTINGS:
        setSMVSettings((TSMVSettings)null);
        return;
      case SCLPackage.TSERVICES__GSE_DIR:
        setGSEDir((TServiceYesNo)null);
        return;
      case SCLPackage.TSERVICES__GOOSE:
        setGOOSE((TServiceWithMax)null);
        return;
      case SCLPackage.TSERVICES__GSSE:
        setGSSE((TServiceWithMax)null);
        return;
      case SCLPackage.TSERVICES__SM_VSC:
        setSMVsc((TServiceWithMax)null);
        return;
      case SCLPackage.TSERVICES__FILE_HANDLING:
        setFileHandling((TServiceYesNo)null);
        return;
      case SCLPackage.TSERVICES__CONF_LNS:
        setConfLNs((TConfLNs)null);
        return;
      case SCLPackage.TSERVICES__CLIENT_SERVICES:
        setClientServices((TClientServices)null);
        return;
      case SCLPackage.TSERVICES__CONF_LD_NAME:
        setConfLdName((TServiceYesNo)null);
        return;
      case SCLPackage.TSERVICES__SUP_SUBSCRIPTION:
        setSupSubscription((TServiceWithMax)null);
        return;
      case SCLPackage.TSERVICES__CONF_SIG_REF:
        setConfSigRef((TServiceWithMax)null);
        return;
      case SCLPackage.TSERVICES__NAME_LENGTH:
        unsetNameLength();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TSERVICES__DYN_ASSOCIATION:
        return dynAssociation != null;
      case SCLPackage.TSERVICES__SETTING_GROUPS:
        return settingGroups != null;
      case SCLPackage.TSERVICES__GET_DIRECTORY:
        return getDirectory != null;
      case SCLPackage.TSERVICES__GET_DATA_OBJECT_DEFINITION:
        return getDataObjectDefinition != null;
      case SCLPackage.TSERVICES__DATA_OBJECT_DIRECTORY:
        return dataObjectDirectory != null;
      case SCLPackage.TSERVICES__GET_DATA_SET_VALUE:
        return getDataSetValue != null;
      case SCLPackage.TSERVICES__SET_DATA_SET_VALUE:
        return setDataSetValue != null;
      case SCLPackage.TSERVICES__DATA_SET_DIRECTORY:
        return dataSetDirectory != null;
      case SCLPackage.TSERVICES__CONF_DATA_SET:
        return confDataSet != null;
      case SCLPackage.TSERVICES__DYN_DATA_SET:
        return dynDataSet != null;
      case SCLPackage.TSERVICES__READ_WRITE:
        return readWrite != null;
      case SCLPackage.TSERVICES__TIMER_ACTIVATED_CONTROL:
        return timerActivatedControl != null;
      case SCLPackage.TSERVICES__CONF_REPORT_CONTROL:
        return confReportControl != null;
      case SCLPackage.TSERVICES__GET_CB_VALUES:
        return getCBValues != null;
      case SCLPackage.TSERVICES__CONF_LOG_CONTROL:
        return confLogControl != null;
      case SCLPackage.TSERVICES__REPORT_SETTINGS:
        return reportSettings != null;
      case SCLPackage.TSERVICES__LOG_SETTINGS:
        return logSettings != null;
      case SCLPackage.TSERVICES__GSE_SETTINGS:
        return gSESettings != null;
      case SCLPackage.TSERVICES__SMV_SETTINGS:
        return sMVSettings != null;
      case SCLPackage.TSERVICES__GSE_DIR:
        return gSEDir != null;
      case SCLPackage.TSERVICES__GOOSE:
        return gOOSE != null;
      case SCLPackage.TSERVICES__GSSE:
        return gSSE != null;
      case SCLPackage.TSERVICES__SM_VSC:
        return sMVsc != null;
      case SCLPackage.TSERVICES__FILE_HANDLING:
        return fileHandling != null;
      case SCLPackage.TSERVICES__CONF_LNS:
        return confLNs != null;
      case SCLPackage.TSERVICES__CLIENT_SERVICES:
        return clientServices != null;
      case SCLPackage.TSERVICES__CONF_LD_NAME:
        return confLdName != null;
      case SCLPackage.TSERVICES__SUP_SUBSCRIPTION:
        return supSubscription != null;
      case SCLPackage.TSERVICES__CONF_SIG_REF:
        return confSigRef != null;
      case SCLPackage.TSERVICES__NAME_LENGTH:
        return isSetNameLength();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (nameLength: ");
    if (nameLengthESet) result.append(nameLength); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TServicesImpl
