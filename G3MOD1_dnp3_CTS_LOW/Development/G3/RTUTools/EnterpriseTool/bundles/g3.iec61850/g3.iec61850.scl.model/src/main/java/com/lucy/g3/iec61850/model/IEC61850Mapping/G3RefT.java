/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping;

import com.lucy.g3.iec61850.model.internal.SCLObject;

import java.math.BigInteger;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>G3 Ref T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT#getGroup <em>Group</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT#getId <em>Id</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT#getUuid <em>Uuid</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getG3RefT()
 * @model extendedMetaData="name='G3RefT' kind='empty'"
 * @extends SCLObject
 * @generated
 */
public interface G3RefT extends SCLObject {
  /**
   * Returns the value of the '<em><b>Group</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group</em>' attribute.
   * @see #setGroup(BigInteger)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getG3RefT_Group()
   * @model dataType="org.eclipse.emf.ecore.xml.type.Integer" required="true"
   *        extendedMetaData="kind='attribute' name='group' namespace='##targetNamespace'"
   * @generated
   */
  BigInteger getGroup();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT#getGroup <em>Group</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Group</em>' attribute.
   * @see #getGroup()
   * @generated
   */
  void setGroup(BigInteger value);

  /**
   * Returns the value of the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' attribute.
   * @see #setId(BigInteger)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getG3RefT_Id()
   * @model dataType="org.eclipse.emf.ecore.xml.type.Integer"
   *        extendedMetaData="kind='attribute' name='id' namespace='##targetNamespace'"
   * @generated
   */
  BigInteger getId();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT#getId <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' attribute.
   * @see #getId()
   * @generated
   */
  void setId(BigInteger value);

  /**
   * Returns the value of the '<em><b>Uuid</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uuid</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uuid</em>' attribute.
   * @see #setUuid(String)
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage#getG3RefT_Uuid()
   * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   *        extendedMetaData="kind='attribute' name='uuid' namespace='##targetNamespace'"
   * @generated
   */
  String getUuid();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT#getUuid <em>Uuid</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uuid</em>' attribute.
   * @see #getUuid()
   * @generated
   */
  void setUuid(String value);

} // G3RefT
