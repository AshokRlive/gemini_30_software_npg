/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TAddress;
import com.lucy.g3.iec61850.model.scl.TControlBlock;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TControl Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TControlBlockImpl#getAddress <em>Address</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TControlBlockImpl#getCbName <em>Cb Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TControlBlockImpl#getLdInst <em>Ld Inst</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class TControlBlockImpl extends TUnNamingImpl implements TControlBlock {
  /**
   * The cached value of the '{@link #getAddress() <em>Address</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAddress()
   * @generated
   * @ordered
   */
  protected TAddress address;

  /**
   * The default value of the '{@link #getCbName() <em>Cb Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCbName()
   * @generated
   * @ordered
   */
  protected static final String CB_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCbName() <em>Cb Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCbName()
   * @generated
   * @ordered
   */
  protected String cbName = CB_NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getLdInst() <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLdInst()
   * @generated
   * @ordered
   */
  protected static final String LD_INST_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLdInst() <em>Ld Inst</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLdInst()
   * @generated
   * @ordered
   */
  protected String ldInst = LD_INST_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TControlBlockImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTControlBlock();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TAddress getAddress() {
    return address;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAddress(TAddress newAddress, NotificationChain msgs) {
    TAddress oldAddress = address;
    address = newAddress;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TCONTROL_BLOCK__ADDRESS, oldAddress, newAddress);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAddress(TAddress newAddress) {
    if (newAddress != address) {
      NotificationChain msgs = null;
      if (address != null)
        msgs = ((InternalEObject)address).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TCONTROL_BLOCK__ADDRESS, null, msgs);
      if (newAddress != null)
        msgs = ((InternalEObject)newAddress).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TCONTROL_BLOCK__ADDRESS, null, msgs);
      msgs = basicSetAddress(newAddress, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TCONTROL_BLOCK__ADDRESS, newAddress, newAddress));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCbName() {
    return cbName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCbName(String newCbName) {
    String oldCbName = cbName;
    cbName = newCbName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TCONTROL_BLOCK__CB_NAME, oldCbName, cbName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLdInst() {
    return ldInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLdInst(String newLdInst) {
    String oldLdInst = ldInst;
    ldInst = newLdInst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TCONTROL_BLOCK__LD_INST, oldLdInst, ldInst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case SCLPackage.TCONTROL_BLOCK__ADDRESS:
        return basicSetAddress(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TCONTROL_BLOCK__ADDRESS:
        return getAddress();
      case SCLPackage.TCONTROL_BLOCK__CB_NAME:
        return getCbName();
      case SCLPackage.TCONTROL_BLOCK__LD_INST:
        return getLdInst();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TCONTROL_BLOCK__ADDRESS:
        setAddress((TAddress)newValue);
        return;
      case SCLPackage.TCONTROL_BLOCK__CB_NAME:
        setCbName((String)newValue);
        return;
      case SCLPackage.TCONTROL_BLOCK__LD_INST:
        setLdInst((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TCONTROL_BLOCK__ADDRESS:
        setAddress((TAddress)null);
        return;
      case SCLPackage.TCONTROL_BLOCK__CB_NAME:
        setCbName(CB_NAME_EDEFAULT);
        return;
      case SCLPackage.TCONTROL_BLOCK__LD_INST:
        setLdInst(LD_INST_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TCONTROL_BLOCK__ADDRESS:
        return address != null;
      case SCLPackage.TCONTROL_BLOCK__CB_NAME:
        return CB_NAME_EDEFAULT == null ? cbName != null : !CB_NAME_EDEFAULT.equals(cbName);
      case SCLPackage.TCONTROL_BLOCK__LD_INST:
        return LD_INST_EDEFAULT == null ? ldInst != null : !LD_INST_EDEFAULT.equals(ldInst);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (cbName: ");
    result.append(cbName);
    result.append(", ldInst: ");
    result.append(ldInst);
    result.append(')');
    return result.toString();
  }

} //TControlBlockImpl
