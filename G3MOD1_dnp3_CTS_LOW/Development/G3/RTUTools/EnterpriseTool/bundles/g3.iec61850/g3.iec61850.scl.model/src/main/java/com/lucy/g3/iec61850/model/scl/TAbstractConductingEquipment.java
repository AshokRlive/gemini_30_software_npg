/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TAbstract Conducting Equipment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAbstractConductingEquipment#getTerminal <em>Terminal</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TAbstractConductingEquipment#getSubEquipment <em>Sub Equipment</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAbstractConductingEquipment()
 * @model abstract="true"
 *        extendedMetaData="name='tAbstractConductingEquipment' kind='elementOnly'"
 * @generated
 */
public interface TAbstractConductingEquipment extends TEquipment {
  /**
   * Returns the value of the '<em><b>Terminal</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TTerminal}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Terminal</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Terminal</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAbstractConductingEquipment_Terminal()
   * @model containment="true" upper="2"
   *        extendedMetaData="kind='element' name='Terminal' namespace='##targetNamespace'"
   * @generated
   */
  EList<TTerminal> getTerminal();

  /**
   * Returns the value of the '<em><b>Sub Equipment</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TSubEquipment}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sub Equipment</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sub Equipment</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTAbstractConductingEquipment_SubEquipment()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='SubEquipment' namespace='##targetNamespace'"
   * @generated
   */
  EList<TSubEquipment> getSubEquipment();

} // TAbstractConductingEquipment
