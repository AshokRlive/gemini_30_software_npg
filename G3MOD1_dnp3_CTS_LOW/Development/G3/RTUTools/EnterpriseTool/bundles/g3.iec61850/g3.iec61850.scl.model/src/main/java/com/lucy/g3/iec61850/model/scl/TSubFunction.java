/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TSub Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSubFunction#getGeneralEquipment <em>General Equipment</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSubFunction#getConductingEquipment <em>Conducting Equipment</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TSubFunction#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSubFunction()
 * @model extendedMetaData="name='tSubFunction' kind='elementOnly'"
 * @generated
 */
public interface TSubFunction extends TPowerSystemResource {
  /**
   * Returns the value of the '<em><b>General Equipment</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TGeneralEquipment}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>General Equipment</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>General Equipment</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSubFunction_GeneralEquipment()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='GeneralEquipment' namespace='##targetNamespace'"
   * @generated
   */
  EList<TGeneralEquipment> getGeneralEquipment();

  /**
   * Returns the value of the '<em><b>Conducting Equipment</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TConductingEquipment}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Conducting Equipment</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Conducting Equipment</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSubFunction_ConductingEquipment()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='ConductingEquipment' namespace='##targetNamespace'"
   * @generated
   */
  EList<TConductingEquipment> getConductingEquipment();

  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSubFunction_Type()
   * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
   *        extendedMetaData="kind='attribute' name='type'"
   * @generated
   */
  String getType();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TSubFunction#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(String value);

} // TSubFunction
