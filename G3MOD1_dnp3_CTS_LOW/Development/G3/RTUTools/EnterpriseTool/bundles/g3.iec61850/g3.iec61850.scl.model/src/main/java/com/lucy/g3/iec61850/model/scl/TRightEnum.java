/**
 */
package com.lucy.g3.iec61850.model.scl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>TRight Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTRightEnum()
 * @model extendedMetaData="name='tRightEnum'"
 * @generated
 */
public enum TRightEnum implements Enumerator {
  /**
   * The '<em><b>Full</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FULL_VALUE
   * @generated
   * @ordered
   */
  FULL(0, "full", "full"),

  /**
   * The '<em><b>Fix</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FIX_VALUE
   * @generated
   * @ordered
   */
  FIX(1, "fix", "fix"),

  /**
   * The '<em><b>Dataflow</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DATAFLOW_VALUE
   * @generated
   * @ordered
   */
  DATAFLOW(2, "dataflow", "dataflow");

  /**
   * The '<em><b>Full</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Full</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FULL
   * @model name="full"
   * @generated
   * @ordered
   */
  public static final int FULL_VALUE = 0;

  /**
   * The '<em><b>Fix</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Fix</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FIX
   * @model name="fix"
   * @generated
   * @ordered
   */
  public static final int FIX_VALUE = 1;

  /**
   * The '<em><b>Dataflow</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Dataflow</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DATAFLOW
   * @model name="dataflow"
   * @generated
   * @ordered
   */
  public static final int DATAFLOW_VALUE = 2;

  /**
   * An array of all the '<em><b>TRight Enum</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final TRightEnum[] VALUES_ARRAY =
    new TRightEnum[] {
      FULL,
      FIX,
      DATAFLOW,
    };

  /**
   * A public read-only list of all the '<em><b>TRight Enum</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<TRightEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>TRight Enum</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param literal the literal.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static TRightEnum get(String literal) {
    for (int i = 0; i < VALUES_ARRAY.length; ++i) {
      TRightEnum result = VALUES_ARRAY[i];
      if (result.toString().equals(literal)) {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>TRight Enum</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param name the name.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static TRightEnum getByName(String name) {
    for (int i = 0; i < VALUES_ARRAY.length; ++i) {
      TRightEnum result = VALUES_ARRAY[i];
      if (result.getName().equals(name)) {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>TRight Enum</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the integer value.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static TRightEnum get(int value) {
    switch (value) {
      case FULL_VALUE: return FULL;
      case FIX_VALUE: return FIX;
      case DATAFLOW_VALUE: return DATAFLOW;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private TRightEnum(int value, String name, String literal) {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue() {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral() {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    return literal;
  }
  
} //TRightEnum
