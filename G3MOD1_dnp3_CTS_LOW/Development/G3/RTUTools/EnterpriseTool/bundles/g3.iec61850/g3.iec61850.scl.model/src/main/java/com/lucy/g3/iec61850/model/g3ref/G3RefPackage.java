/**
 */
package com.lucy.g3.iec61850.model.g3ref;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * 
 *  COPYRIGHT Lucy Electric Version 1.0.Release
 *  
 * <!-- end-model-doc -->
 * @see com.lucy.g3.iec61850.model.g3ref.G3RefFactory
 * @model kind="package"
 * @generated
 */
public interface G3RefPackage extends EPackage {
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "g3ref";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://ww.lucy.com/61850/SCL/Generic";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "g3ref";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  G3RefPackage eINSTANCE = com.lucy.g3.iec61850.model.g3ref.impl.G3RefPackageImpl.init();

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.g3ref.impl.DocumentRootImpl <em>Document Root</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.g3ref.impl.DocumentRootImpl
   * @see com.lucy.g3.iec61850.model.g3ref.impl.G3RefPackageImpl#getDocumentRoot()
   * @generated
   */
  int DOCUMENT_ROOT = 0;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__MIXED = 0;

  /**
   * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

  /**
   * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

  /**
   * The feature id for the '<em><b>G3 Ref Object</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__G3_REF_OBJECT = 3;

  /**
   * The number of structural features of the '<em>Document Root</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT_FEATURE_COUNT = 4;

  /**
   * The number of operations of the '<em>Document Root</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.lucy.g3.iec61850.model.g3ref.impl.TG3RefObjectImpl <em>TG3 Ref Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.lucy.g3.iec61850.model.g3ref.impl.TG3RefObjectImpl
   * @see com.lucy.g3.iec61850.model.g3ref.impl.G3RefPackageImpl#getTG3RefObject()
   * @generated
   */
  int TG3_REF_OBJECT = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TG3_REF_OBJECT__NAME = 0;

  /**
   * The feature id for the '<em><b>Uuid</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TG3_REF_OBJECT__UUID = 1;

  /**
   * The number of structural features of the '<em>TG3 Ref Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TG3_REF_OBJECT_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>TG3 Ref Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TG3_REF_OBJECT_OPERATION_COUNT = 0;


  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.g3ref.DocumentRoot <em>Document Root</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Document Root</em>'.
   * @see com.lucy.g3.iec61850.model.g3ref.DocumentRoot
   * @generated
   */
  EClass getDocumentRoot();

  /**
   * Returns the meta object for the attribute list '{@link com.lucy.g3.iec61850.model.g3ref.DocumentRoot#getMixed <em>Mixed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Mixed</em>'.
   * @see com.lucy.g3.iec61850.model.g3ref.DocumentRoot#getMixed()
   * @see #getDocumentRoot()
   * @generated
   */
  EAttribute getDocumentRoot_Mixed();

  /**
   * Returns the meta object for the map '{@link com.lucy.g3.iec61850.model.g3ref.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
   * @see com.lucy.g3.iec61850.model.g3ref.DocumentRoot#getXMLNSPrefixMap()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XMLNSPrefixMap();

  /**
   * Returns the meta object for the map '{@link com.lucy.g3.iec61850.model.g3ref.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XSI Schema Location</em>'.
   * @see com.lucy.g3.iec61850.model.g3ref.DocumentRoot#getXSISchemaLocation()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XSISchemaLocation();

  /**
   * Returns the meta object for the containment reference '{@link com.lucy.g3.iec61850.model.g3ref.DocumentRoot#getG3RefObject <em>G3 Ref Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>G3 Ref Object</em>'.
   * @see com.lucy.g3.iec61850.model.g3ref.DocumentRoot#getG3RefObject()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_G3RefObject();

  /**
   * Returns the meta object for class '{@link com.lucy.g3.iec61850.model.g3ref.TG3RefObject <em>TG3 Ref Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TG3 Ref Object</em>'.
   * @see com.lucy.g3.iec61850.model.g3ref.TG3RefObject
   * @generated
   */
  EClass getTG3RefObject();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.g3ref.TG3RefObject#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.lucy.g3.iec61850.model.g3ref.TG3RefObject#getName()
   * @see #getTG3RefObject()
   * @generated
   */
  EAttribute getTG3RefObject_Name();

  /**
   * Returns the meta object for the attribute '{@link com.lucy.g3.iec61850.model.g3ref.TG3RefObject#getUuid <em>Uuid</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uuid</em>'.
   * @see com.lucy.g3.iec61850.model.g3ref.TG3RefObject#getUuid()
   * @see #getTG3RefObject()
   * @generated
   */
  EAttribute getTG3RefObject_Uuid();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  G3RefFactory getG3RefFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each operation of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals {
    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61850.model.g3ref.impl.DocumentRootImpl <em>Document Root</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61850.model.g3ref.impl.DocumentRootImpl
     * @see com.lucy.g3.iec61850.model.g3ref.impl.G3RefPackageImpl#getDocumentRoot()
     * @generated
     */
    EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

    /**
     * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

    /**
     * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

    /**
     * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

    /**
     * The meta object literal for the '<em><b>G3 Ref Object</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__G3_REF_OBJECT = eINSTANCE.getDocumentRoot_G3RefObject();

    /**
     * The meta object literal for the '{@link com.lucy.g3.iec61850.model.g3ref.impl.TG3RefObjectImpl <em>TG3 Ref Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.lucy.g3.iec61850.model.g3ref.impl.TG3RefObjectImpl
     * @see com.lucy.g3.iec61850.model.g3ref.impl.G3RefPackageImpl#getTG3RefObject()
     * @generated
     */
    EClass TG3_REF_OBJECT = eINSTANCE.getTG3RefObject();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TG3_REF_OBJECT__NAME = eINSTANCE.getTG3RefObject_Name();

    /**
     * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TG3_REF_OBJECT__UUID = eINSTANCE.getTG3RefObject_Uuid();

  }

} //G3RefPackage
