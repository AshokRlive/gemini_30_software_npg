/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping.impl;

import com.lucy.g3.iec61850.model.IEC61850Mapping.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class IEC61850MappingFactoryImpl extends EFactoryImpl implements IEC61850MappingFactory {
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static IEC61850MappingFactory init() {
    try {
      IEC61850MappingFactory theIEC61850MappingFactory = (IEC61850MappingFactory)EPackage.Registry.INSTANCE.getEFactory(IEC61850MappingPackage.eNS_URI);
      if (theIEC61850MappingFactory != null) {
        return theIEC61850MappingFactory;
      }
    }
    catch (Exception exception) {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new IEC61850MappingFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IEC61850MappingFactoryImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass) {
    switch (eClass.getClassifierID()) {
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T: return (EObject)createDataAttributeIdT();
      case IEC61850MappingPackage.DOCUMENT_ROOT: return (EObject)createDocumentRoot();
      case IEC61850MappingPackage.ENTRY_T: return (EObject)createEntryT();
      case IEC61850MappingPackage.G3_REF_T: return (EObject)createG3RefT();
      case IEC61850MappingPackage.MAPPING_ROOT_T: return (EObject)createMappingRootT();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DataAttributeIdT createDataAttributeIdT() {
    DataAttributeIdTImpl dataAttributeIdT = new DataAttributeIdTImpl();
    return dataAttributeIdT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DocumentRoot createDocumentRoot() {
    DocumentRootImpl documentRoot = new DocumentRootImpl();
    return documentRoot;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EntryT createEntryT() {
    EntryTImpl entryT = new EntryTImpl();
    return entryT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public G3RefT createG3RefT() {
    G3RefTImpl g3RefT = new G3RefTImpl();
    return g3RefT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MappingRootT createMappingRootT() {
    MappingRootTImpl mappingRootT = new MappingRootTImpl();
    return mappingRootT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IEC61850MappingPackage getIEC61850MappingPackage() {
    return (IEC61850MappingPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static IEC61850MappingPackage getPackage() {
    return IEC61850MappingPackage.eINSTANCE;
  }

} //IEC61850MappingFactoryImpl
