/**
 */
package com.lucy.g3.iec61850.model.g3ref.util;

import com.lucy.g3.iec61850.model.g3ref.G3RefPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class G3RefXMLProcessor extends XMLProcessor {

  /**
   * Public constructor to instantiate the helper.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public G3RefXMLProcessor() {
    super((EPackage.Registry.INSTANCE));
    G3RefPackage.eINSTANCE.eClass();
  }
  
  /**
   * Register for "*" and "xml" file extensions the G3RefResourceFactoryImpl factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected Map<String, Resource.Factory> getRegistrations() {
    if (registrations == null) {
      super.getRegistrations();
      registrations.put(XML_EXTENSION, new G3RefResourceFactoryImpl());
      registrations.put(STAR_EXTENSION, new G3RefResourceFactoryImpl());
    }
    return registrations;
  }

} //G3RefXMLProcessor
