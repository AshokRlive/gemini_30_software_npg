/**
 */
package com.lucy.g3.iec61850.model.scl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TData Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TDataSet#getGroup <em>Group</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TDataSet#getFCDA <em>FCDA</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TDataSet#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTDataSet()
 * @model extendedMetaData="name='tDataSet' kind='elementOnly'"
 * @generated
 */
public interface TDataSet extends TUnNaming {
  /**
   * Returns the value of the '<em><b>Group</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group</em>' attribute list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTDataSet_Group()
   * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='group' name='group:5'"
   * @generated
   */
  FeatureMap getGroup();

  /**
   * Returns the value of the '<em><b>FCDA</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.TFCDA}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>FCDA</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>FCDA</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTDataSet_FCDA()
   * @model containment="true" transient="true" volatile="true" derived="true"
   *        extendedMetaData="kind='element' name='FCDA' namespace='##targetNamespace' group='#group:5'"
   * @generated
   */
  EList<TFCDA> getFCDA();

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTDataSet_Name()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TDataSetName" required="true"
   *        extendedMetaData="kind='attribute' name='name'"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TDataSet#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

} // TDataSet
