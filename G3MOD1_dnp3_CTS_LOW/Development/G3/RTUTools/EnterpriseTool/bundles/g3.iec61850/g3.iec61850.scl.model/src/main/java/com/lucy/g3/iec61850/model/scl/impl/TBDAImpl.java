/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TBDA;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TBDA</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TBDAImpl extends TAbstractDataAttributeImpl implements TBDA {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TBDAImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTBDA();
  }

} //TBDAImpl
