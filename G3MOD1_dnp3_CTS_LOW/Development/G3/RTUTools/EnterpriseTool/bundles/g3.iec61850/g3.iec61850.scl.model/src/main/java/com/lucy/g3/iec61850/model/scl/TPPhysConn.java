/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TP Phys Conn</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TPPhysConn#getValue <em>Value</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TPPhysConn#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPPhysConn()
 * @model extendedMetaData="name='tP_PhysConn' kind='simple'"
 * @extends SCLObject
 * @generated
 */
public interface TPPhysConn extends SCLObject {
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPPhysConn_Value()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TPAddr"
   *        extendedMetaData="name=':0' kind='simple'"
   * @generated
   */
  String getValue();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TPPhysConn#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(String value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(Object)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPPhysConn_Type()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TPTypePhysConnEnum" required="true"
   *        extendedMetaData="kind='attribute' name='type'"
   * @generated
   */
  Object getType();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TPPhysConn#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(Object value);

} // TPPhysConn
