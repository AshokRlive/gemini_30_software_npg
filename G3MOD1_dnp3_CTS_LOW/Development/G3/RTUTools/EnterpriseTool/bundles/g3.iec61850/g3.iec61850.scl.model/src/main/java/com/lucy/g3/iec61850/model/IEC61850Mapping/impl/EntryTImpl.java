/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping.impl;

import com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage;

import com.lucy.g3.iec61850.model.internal.SCLObjectImpl;

import java.math.BigInteger;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Entry T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.EntryTImpl#getG3Ref <em>G3 Ref</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.EntryTImpl#getDataAttributeId <em>Data Attribute Id</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.EntryTImpl#getAttributeRef <em>Attribute Ref</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.EntryTImpl#getUuid <em>Uuid</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.IEC61850Mapping.impl.EntryTImpl#getDataType <em>Data Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EntryTImpl extends SCLObjectImpl implements EntryT {
  /**
   * The cached value of the '{@link #getG3Ref() <em>G3 Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getG3Ref()
   * @generated
   * @ordered
   */
  protected G3RefT g3Ref;

  /**
   * The cached value of the '{@link #getDataAttributeId() <em>Data Attribute Id</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDataAttributeId()
   * @generated
   * @ordered
   */
  protected DataAttributeIdT dataAttributeId;

  /**
   * The default value of the '{@link #getAttributeRef() <em>Attribute Ref</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAttributeRef()
   * @generated
   * @ordered
   */
  protected static final String ATTRIBUTE_REF_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAttributeRef() <em>Attribute Ref</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAttributeRef()
   * @generated
   * @ordered
   */
  protected String attributeRef = ATTRIBUTE_REF_EDEFAULT;

  /**
   * The default value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUuid()
   * @generated
   * @ordered
   */
  protected static final String UUID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUuid()
   * @generated
   * @ordered
   */
  protected String uuid = UUID_EDEFAULT;

  /**
   * The default value of the '{@link #getDataType() <em>Data Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDataType()
   * @generated
   * @ordered
   */
  protected static final BigInteger DATA_TYPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDataType() <em>Data Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDataType()
   * @generated
   * @ordered
   */
  protected BigInteger dataType = DATA_TYPE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EntryTImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return IEC61850MappingPackage.Literals.ENTRY_T;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public G3RefT getG3Ref() {
    return g3Ref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetG3Ref(G3RefT newG3Ref, NotificationChain msgs) {
    G3RefT oldG3Ref = g3Ref;
    g3Ref = newG3Ref;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.ENTRY_T__G3_REF, oldG3Ref, newG3Ref);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setG3Ref(G3RefT newG3Ref) {
    if (newG3Ref != g3Ref) {
      NotificationChain msgs = null;
      if (g3Ref != null)
        msgs = ((InternalEObject)g3Ref).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - IEC61850MappingPackage.ENTRY_T__G3_REF, null, msgs);
      if (newG3Ref != null)
        msgs = ((InternalEObject)newG3Ref).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - IEC61850MappingPackage.ENTRY_T__G3_REF, null, msgs);
      msgs = basicSetG3Ref(newG3Ref, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.ENTRY_T__G3_REF, newG3Ref, newG3Ref));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DataAttributeIdT getDataAttributeId() {
    return dataAttributeId;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDataAttributeId(DataAttributeIdT newDataAttributeId, NotificationChain msgs) {
    DataAttributeIdT oldDataAttributeId = dataAttributeId;
    dataAttributeId = newDataAttributeId;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.ENTRY_T__DATA_ATTRIBUTE_ID, oldDataAttributeId, newDataAttributeId);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDataAttributeId(DataAttributeIdT newDataAttributeId) {
    if (newDataAttributeId != dataAttributeId) {
      NotificationChain msgs = null;
      if (dataAttributeId != null)
        msgs = ((InternalEObject)dataAttributeId).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - IEC61850MappingPackage.ENTRY_T__DATA_ATTRIBUTE_ID, null, msgs);
      if (newDataAttributeId != null)
        msgs = ((InternalEObject)newDataAttributeId).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - IEC61850MappingPackage.ENTRY_T__DATA_ATTRIBUTE_ID, null, msgs);
      msgs = basicSetDataAttributeId(newDataAttributeId, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.ENTRY_T__DATA_ATTRIBUTE_ID, newDataAttributeId, newDataAttributeId));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAttributeRef() {
    return attributeRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAttributeRef(String newAttributeRef) {
    String oldAttributeRef = attributeRef;
    attributeRef = newAttributeRef;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.ENTRY_T__ATTRIBUTE_REF, oldAttributeRef, attributeRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUuid() {
    return uuid;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUuid(String newUuid) {
    String oldUuid = uuid;
    uuid = newUuid;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.ENTRY_T__UUID, oldUuid, uuid));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getDataType() {
    return dataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDataType(BigInteger newDataType) {
    BigInteger oldDataType = dataType;
    dataType = newDataType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, IEC61850MappingPackage.ENTRY_T__DATA_TYPE, oldDataType, dataType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case IEC61850MappingPackage.ENTRY_T__G3_REF:
        return basicSetG3Ref(null, msgs);
      case IEC61850MappingPackage.ENTRY_T__DATA_ATTRIBUTE_ID:
        return basicSetDataAttributeId(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case IEC61850MappingPackage.ENTRY_T__G3_REF:
        return getG3Ref();
      case IEC61850MappingPackage.ENTRY_T__DATA_ATTRIBUTE_ID:
        return getDataAttributeId();
      case IEC61850MappingPackage.ENTRY_T__ATTRIBUTE_REF:
        return getAttributeRef();
      case IEC61850MappingPackage.ENTRY_T__UUID:
        return getUuid();
      case IEC61850MappingPackage.ENTRY_T__DATA_TYPE:
        return getDataType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case IEC61850MappingPackage.ENTRY_T__G3_REF:
        setG3Ref((G3RefT)newValue);
        return;
      case IEC61850MappingPackage.ENTRY_T__DATA_ATTRIBUTE_ID:
        setDataAttributeId((DataAttributeIdT)newValue);
        return;
      case IEC61850MappingPackage.ENTRY_T__ATTRIBUTE_REF:
        setAttributeRef((String)newValue);
        return;
      case IEC61850MappingPackage.ENTRY_T__UUID:
        setUuid((String)newValue);
        return;
      case IEC61850MappingPackage.ENTRY_T__DATA_TYPE:
        setDataType((BigInteger)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case IEC61850MappingPackage.ENTRY_T__G3_REF:
        setG3Ref((G3RefT)null);
        return;
      case IEC61850MappingPackage.ENTRY_T__DATA_ATTRIBUTE_ID:
        setDataAttributeId((DataAttributeIdT)null);
        return;
      case IEC61850MappingPackage.ENTRY_T__ATTRIBUTE_REF:
        setAttributeRef(ATTRIBUTE_REF_EDEFAULT);
        return;
      case IEC61850MappingPackage.ENTRY_T__UUID:
        setUuid(UUID_EDEFAULT);
        return;
      case IEC61850MappingPackage.ENTRY_T__DATA_TYPE:
        setDataType(DATA_TYPE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case IEC61850MappingPackage.ENTRY_T__G3_REF:
        return g3Ref != null;
      case IEC61850MappingPackage.ENTRY_T__DATA_ATTRIBUTE_ID:
        return dataAttributeId != null;
      case IEC61850MappingPackage.ENTRY_T__ATTRIBUTE_REF:
        return ATTRIBUTE_REF_EDEFAULT == null ? attributeRef != null : !ATTRIBUTE_REF_EDEFAULT.equals(attributeRef);
      case IEC61850MappingPackage.ENTRY_T__UUID:
        return UUID_EDEFAULT == null ? uuid != null : !UUID_EDEFAULT.equals(uuid);
      case IEC61850MappingPackage.ENTRY_T__DATA_TYPE:
        return DATA_TYPE_EDEFAULT == null ? dataType != null : !DATA_TYPE_EDEFAULT.equals(dataType);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (attributeRef: ");
    result.append(attributeRef);
    result.append(", uuid: ");
    result.append(uuid);
    result.append(", dataType: ");
    result.append(dataType);
    result.append(')');
    return result.toString();
  }

} //EntryTImpl
