/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping.util;

import com.lucy.g3.iec61850.model.IEC61850Mapping.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage
 * @generated
 */
public class IEC61850MappingAdapterFactory extends AdapterFactoryImpl {
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static IEC61850MappingPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IEC61850MappingAdapterFactory() {
    if (modelPackage == null) {
      modelPackage = IEC61850MappingPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object) {
    if (object == modelPackage) {
      return true;
    }
    if (object instanceof EObject) {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IEC61850MappingSwitch<Adapter> modelSwitch =
    new IEC61850MappingSwitch<Adapter>() {
      @Override
      public Adapter caseDataAttributeIdT(DataAttributeIdT object) {
        return createDataAttributeIdTAdapter();
      }
      @Override
      public Adapter caseDocumentRoot(DocumentRoot object) {
        return createDocumentRootAdapter();
      }
      @Override
      public Adapter caseEntryT(EntryT object) {
        return createEntryTAdapter();
      }
      @Override
      public Adapter caseG3RefT(G3RefT object) {
        return createG3RefTAdapter();
      }
      @Override
      public Adapter caseMappingRootT(MappingRootT object) {
        return createMappingRootTAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object) {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target) {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT <em>Data Attribute Id T</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT
   * @generated
   */
  public Adapter createDataAttributeIdTAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.DocumentRoot <em>Document Root</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.DocumentRoot
   * @generated
   */
  public Adapter createDocumentRootAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT <em>Entry T</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT
   * @generated
   */
  public Adapter createEntryTAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT <em>G3 Ref T</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT
   * @generated
   */
  public Adapter createG3RefTAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT <em>Mapping Root T</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT
   * @generated
   */
  public Adapter createMappingRootTAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter() {
    return null;
  }

} //IEC61850MappingAdapterFactory
