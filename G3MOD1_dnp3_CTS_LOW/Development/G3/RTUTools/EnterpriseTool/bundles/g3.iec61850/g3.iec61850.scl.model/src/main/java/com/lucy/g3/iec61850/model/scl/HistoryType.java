/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>History Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.HistoryType#getHitem <em>Hitem</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getHistoryType()
 * @model extendedMetaData="name='History_._type' kind='elementOnly'"
 * @extends SCLObject
 * @generated
 */
public interface HistoryType extends SCLObject {
  /**
   * Returns the value of the '<em><b>Hitem</b></em>' containment reference list.
   * The list contents are of type {@link com.lucy.g3.iec61850.model.scl.THitem}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Hitem</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Hitem</em>' containment reference list.
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getHistoryType_Hitem()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='Hitem' namespace='##targetNamespace'"
   * @generated
   */
  EList<THitem> getHitem();

} // HistoryType
