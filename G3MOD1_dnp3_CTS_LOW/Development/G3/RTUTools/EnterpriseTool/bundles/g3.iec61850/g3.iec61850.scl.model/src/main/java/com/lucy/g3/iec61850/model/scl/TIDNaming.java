/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TID Naming</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TIDNaming#getDesc <em>Desc</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TIDNaming#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIDNaming()
 * @model abstract="true"
 *        extendedMetaData="name='tIDNaming' kind='elementOnly'"
 * @generated
 */
public interface TIDNaming extends TBaseElement {
  /**
   * Returns the value of the '<em><b>Desc</b></em>' attribute.
   * The default value is <code>""</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Desc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Desc</em>' attribute.
   * @see #isSetDesc()
   * @see #unsetDesc()
   * @see #setDesc(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIDNaming_Desc()
   * @model default="" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
   *        extendedMetaData="kind='attribute' name='desc'"
   * @generated
   */
  String getDesc();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TIDNaming#getDesc <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Desc</em>' attribute.
   * @see #isSetDesc()
   * @see #unsetDesc()
   * @see #getDesc()
   * @generated
   */
  void setDesc(String value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TIDNaming#getDesc <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetDesc()
   * @see #getDesc()
   * @see #setDesc(String)
   * @generated
   */
  void unsetDesc();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TIDNaming#getDesc <em>Desc</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Desc</em>' attribute is set.
   * @see #unsetDesc()
   * @see #getDesc()
   * @see #setDesc(String)
   * @generated
   */
  boolean isSetDesc();

  /**
   * Returns the value of the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' attribute.
   * @see #setId(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTIDNaming_Id()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TName" required="true"
   *        extendedMetaData="kind='attribute' name='id'"
   * @generated
   */
  String getId();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TIDNaming#getId <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' attribute.
   * @see #getId()
   * @generated
   */
  void setId(String value);

} // TIDNaming
