/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.scl.AuthenticationType;
import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TAssociation;
import com.lucy.g3.iec61850.model.scl.TLDevice;
import com.lucy.g3.iec61850.model.scl.TServer;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TServer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServerImpl#getAuthentication <em>Authentication</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServerImpl#getLDevice <em>LDevice</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServerImpl#getAssociation <em>Association</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.TServerImpl#getTimeout <em>Timeout</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TServerImpl extends TUnNamingImpl implements TServer {
  /**
   * The cached value of the '{@link #getAuthentication() <em>Authentication</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAuthentication()
   * @generated
   * @ordered
   */
  protected AuthenticationType authentication;

  /**
   * The cached value of the '{@link #getLDevice() <em>LDevice</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLDevice()
   * @generated
   * @ordered
   */
  protected EList<TLDevice> lDevice;

  /**
   * The cached value of the '{@link #getAssociation() <em>Association</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAssociation()
   * @generated
   * @ordered
   */
  protected EList<TAssociation> association;

  /**
   * The default value of the '{@link #getTimeout() <em>Timeout</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTimeout()
   * @generated
   * @ordered
   */
  protected static final long TIMEOUT_EDEFAULT = 30L;

  /**
   * The cached value of the '{@link #getTimeout() <em>Timeout</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTimeout()
   * @generated
   * @ordered
   */
  protected long timeout = TIMEOUT_EDEFAULT;

  /**
   * This is true if the Timeout attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean timeoutESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TServerImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTServer();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AuthenticationType getAuthentication() {
    return authentication;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAuthentication(AuthenticationType newAuthentication, NotificationChain msgs) {
    AuthenticationType oldAuthentication = authentication;
    authentication = newAuthentication;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVER__AUTHENTICATION, oldAuthentication, newAuthentication);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAuthentication(AuthenticationType newAuthentication) {
    if (newAuthentication != authentication) {
      NotificationChain msgs = null;
      if (authentication != null)
        msgs = ((InternalEObject)authentication).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVER__AUTHENTICATION, null, msgs);
      if (newAuthentication != null)
        msgs = ((InternalEObject)newAuthentication).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.TSERVER__AUTHENTICATION, null, msgs);
      msgs = basicSetAuthentication(newAuthentication, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVER__AUTHENTICATION, newAuthentication, newAuthentication));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TLDevice> getLDevice() {
    if (lDevice == null) {
      lDevice = new EObjectContainmentEList<TLDevice>(TLDevice.class, this, SCLPackage.TSERVER__LDEVICE);
    }
    return lDevice;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TAssociation> getAssociation() {
    if (association == null) {
      association = new EObjectContainmentEList<TAssociation>(TAssociation.class, this, SCLPackage.TSERVER__ASSOCIATION);
    }
    return association;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public long getTimeout() {
    return timeout;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTimeout(long newTimeout) {
    long oldTimeout = timeout;
    timeout = newTimeout;
    boolean oldTimeoutESet = timeoutESet;
    timeoutESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.TSERVER__TIMEOUT, oldTimeout, timeout, !oldTimeoutESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetTimeout() {
    long oldTimeout = timeout;
    boolean oldTimeoutESet = timeoutESet;
    timeout = TIMEOUT_EDEFAULT;
    timeoutESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.TSERVER__TIMEOUT, oldTimeout, TIMEOUT_EDEFAULT, oldTimeoutESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetTimeout() {
    return timeoutESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case SCLPackage.TSERVER__AUTHENTICATION:
        return basicSetAuthentication(null, msgs);
      case SCLPackage.TSERVER__LDEVICE:
        return ((InternalEList<?>)getLDevice()).basicRemove(otherEnd, msgs);
      case SCLPackage.TSERVER__ASSOCIATION:
        return ((InternalEList<?>)getAssociation()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.TSERVER__AUTHENTICATION:
        return getAuthentication();
      case SCLPackage.TSERVER__LDEVICE:
        return getLDevice();
      case SCLPackage.TSERVER__ASSOCIATION:
        return getAssociation();
      case SCLPackage.TSERVER__TIMEOUT:
        return getTimeout();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.TSERVER__AUTHENTICATION:
        setAuthentication((AuthenticationType)newValue);
        return;
      case SCLPackage.TSERVER__LDEVICE:
        getLDevice().clear();
        getLDevice().addAll((Collection<? extends TLDevice>)newValue);
        return;
      case SCLPackage.TSERVER__ASSOCIATION:
        getAssociation().clear();
        getAssociation().addAll((Collection<? extends TAssociation>)newValue);
        return;
      case SCLPackage.TSERVER__TIMEOUT:
        setTimeout((Long)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.TSERVER__AUTHENTICATION:
        setAuthentication((AuthenticationType)null);
        return;
      case SCLPackage.TSERVER__LDEVICE:
        getLDevice().clear();
        return;
      case SCLPackage.TSERVER__ASSOCIATION:
        getAssociation().clear();
        return;
      case SCLPackage.TSERVER__TIMEOUT:
        unsetTimeout();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.TSERVER__AUTHENTICATION:
        return authentication != null;
      case SCLPackage.TSERVER__LDEVICE:
        return lDevice != null && !lDevice.isEmpty();
      case SCLPackage.TSERVER__ASSOCIATION:
        return association != null && !association.isEmpty();
      case SCLPackage.TSERVER__TIMEOUT:
        return isSetTimeout();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (timeout: ");
    if (timeoutESet) result.append(timeout); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TServerImpl
