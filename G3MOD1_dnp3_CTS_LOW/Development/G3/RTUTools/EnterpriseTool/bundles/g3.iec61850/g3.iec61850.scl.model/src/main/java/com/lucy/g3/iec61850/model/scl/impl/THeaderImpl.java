/**
 */
package com.lucy.g3.iec61850.model.scl.impl;

import com.lucy.g3.iec61850.model.internal.SCLObjectImpl;

import com.lucy.g3.iec61850.model.scl.HistoryType;
import com.lucy.g3.iec61850.model.scl.NameStructureType;
import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.THeader;
import com.lucy.g3.iec61850.model.scl.TText;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>THeader</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.THeaderImpl#getText <em>Text</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.THeaderImpl#getHistory <em>History</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.THeaderImpl#getId <em>Id</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.THeaderImpl#getNameStructure <em>Name Structure</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.THeaderImpl#getRevision <em>Revision</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.THeaderImpl#getToolID <em>Tool ID</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.impl.THeaderImpl#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @generated
 */
public class THeaderImpl extends SCLObjectImpl implements THeader {
  /**
   * The cached value of the '{@link #getText() <em>Text</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getText()
   * @generated
   * @ordered
   */
  protected TText text;

  /**
   * The cached value of the '{@link #getHistory() <em>History</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHistory()
   * @generated
   * @ordered
   */
  protected HistoryType history;

  /**
   * The default value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected static final String ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected String id = ID_EDEFAULT;

  /**
   * The default value of the '{@link #getNameStructure() <em>Name Structure</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNameStructure()
   * @generated
   * @ordered
   */
  protected static final NameStructureType NAME_STRUCTURE_EDEFAULT = NameStructureType.IED_NAME;

  /**
   * The cached value of the '{@link #getNameStructure() <em>Name Structure</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNameStructure()
   * @generated
   * @ordered
   */
  protected NameStructureType nameStructure = NAME_STRUCTURE_EDEFAULT;

  /**
   * This is true if the Name Structure attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean nameStructureESet;

  /**
   * The default value of the '{@link #getRevision() <em>Revision</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRevision()
   * @generated
   * @ordered
   */
  protected static final String REVISION_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getRevision() <em>Revision</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRevision()
   * @generated
   * @ordered
   */
  protected String revision = REVISION_EDEFAULT;

  /**
   * This is true if the Revision attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean revisionESet;

  /**
   * The default value of the '{@link #getToolID() <em>Tool ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getToolID()
   * @generated
   * @ordered
   */
  protected static final String TOOL_ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getToolID() <em>Tool ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getToolID()
   * @generated
   * @ordered
   */
  protected String toolID = TOOL_ID_EDEFAULT;

  /**
   * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVersion()
   * @generated
   * @ordered
   */
  protected static final String VERSION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVersion()
   * @generated
   * @ordered
   */
  protected String version = VERSION_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected THeaderImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return SCLPackage.eINSTANCE.getTHeader();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TText getText() {
    return text;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetText(TText newText, NotificationChain msgs) {
    TText oldText = text;
    text = newText;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.THEADER__TEXT, oldText, newText);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setText(TText newText) {
    if (newText != text) {
      NotificationChain msgs = null;
      if (text != null)
        msgs = ((InternalEObject)text).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.THEADER__TEXT, null, msgs);
      if (newText != null)
        msgs = ((InternalEObject)newText).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.THEADER__TEXT, null, msgs);
      msgs = basicSetText(newText, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.THEADER__TEXT, newText, newText));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HistoryType getHistory() {
    return history;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetHistory(HistoryType newHistory, NotificationChain msgs) {
    HistoryType oldHistory = history;
    history = newHistory;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SCLPackage.THEADER__HISTORY, oldHistory, newHistory);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setHistory(HistoryType newHistory) {
    if (newHistory != history) {
      NotificationChain msgs = null;
      if (history != null)
        msgs = ((InternalEObject)history).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SCLPackage.THEADER__HISTORY, null, msgs);
      if (newHistory != null)
        msgs = ((InternalEObject)newHistory).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SCLPackage.THEADER__HISTORY, null, msgs);
      msgs = basicSetHistory(newHistory, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.THEADER__HISTORY, newHistory, newHistory));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getId() {
    return id;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setId(String newId) {
    String oldId = id;
    id = newId;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.THEADER__ID, oldId, id));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NameStructureType getNameStructure() {
    return nameStructure;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNameStructure(NameStructureType newNameStructure) {
    NameStructureType oldNameStructure = nameStructure;
    nameStructure = newNameStructure == null ? NAME_STRUCTURE_EDEFAULT : newNameStructure;
    boolean oldNameStructureESet = nameStructureESet;
    nameStructureESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.THEADER__NAME_STRUCTURE, oldNameStructure, nameStructure, !oldNameStructureESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetNameStructure() {
    NameStructureType oldNameStructure = nameStructure;
    boolean oldNameStructureESet = nameStructureESet;
    nameStructure = NAME_STRUCTURE_EDEFAULT;
    nameStructureESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.THEADER__NAME_STRUCTURE, oldNameStructure, NAME_STRUCTURE_EDEFAULT, oldNameStructureESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetNameStructure() {
    return nameStructureESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getRevision() {
    return revision;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRevision(String newRevision) {
    String oldRevision = revision;
    revision = newRevision;
    boolean oldRevisionESet = revisionESet;
    revisionESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.THEADER__REVISION, oldRevision, revision, !oldRevisionESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetRevision() {
    String oldRevision = revision;
    boolean oldRevisionESet = revisionESet;
    revision = REVISION_EDEFAULT;
    revisionESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SCLPackage.THEADER__REVISION, oldRevision, REVISION_EDEFAULT, oldRevisionESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetRevision() {
    return revisionESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getToolID() {
    return toolID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setToolID(String newToolID) {
    String oldToolID = toolID;
    toolID = newToolID;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.THEADER__TOOL_ID, oldToolID, toolID));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVersion() {
    return version;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVersion(String newVersion) {
    String oldVersion = version;
    version = newVersion;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SCLPackage.THEADER__VERSION, oldVersion, version));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case SCLPackage.THEADER__TEXT:
        return basicSetText(null, msgs);
      case SCLPackage.THEADER__HISTORY:
        return basicSetHistory(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case SCLPackage.THEADER__TEXT:
        return getText();
      case SCLPackage.THEADER__HISTORY:
        return getHistory();
      case SCLPackage.THEADER__ID:
        return getId();
      case SCLPackage.THEADER__NAME_STRUCTURE:
        return getNameStructure();
      case SCLPackage.THEADER__REVISION:
        return getRevision();
      case SCLPackage.THEADER__TOOL_ID:
        return getToolID();
      case SCLPackage.THEADER__VERSION:
        return getVersion();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case SCLPackage.THEADER__TEXT:
        setText((TText)newValue);
        return;
      case SCLPackage.THEADER__HISTORY:
        setHistory((HistoryType)newValue);
        return;
      case SCLPackage.THEADER__ID:
        setId((String)newValue);
        return;
      case SCLPackage.THEADER__NAME_STRUCTURE:
        setNameStructure((NameStructureType)newValue);
        return;
      case SCLPackage.THEADER__REVISION:
        setRevision((String)newValue);
        return;
      case SCLPackage.THEADER__TOOL_ID:
        setToolID((String)newValue);
        return;
      case SCLPackage.THEADER__VERSION:
        setVersion((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case SCLPackage.THEADER__TEXT:
        setText((TText)null);
        return;
      case SCLPackage.THEADER__HISTORY:
        setHistory((HistoryType)null);
        return;
      case SCLPackage.THEADER__ID:
        setId(ID_EDEFAULT);
        return;
      case SCLPackage.THEADER__NAME_STRUCTURE:
        unsetNameStructure();
        return;
      case SCLPackage.THEADER__REVISION:
        unsetRevision();
        return;
      case SCLPackage.THEADER__TOOL_ID:
        setToolID(TOOL_ID_EDEFAULT);
        return;
      case SCLPackage.THEADER__VERSION:
        setVersion(VERSION_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case SCLPackage.THEADER__TEXT:
        return text != null;
      case SCLPackage.THEADER__HISTORY:
        return history != null;
      case SCLPackage.THEADER__ID:
        return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
      case SCLPackage.THEADER__NAME_STRUCTURE:
        return isSetNameStructure();
      case SCLPackage.THEADER__REVISION:
        return isSetRevision();
      case SCLPackage.THEADER__TOOL_ID:
        return TOOL_ID_EDEFAULT == null ? toolID != null : !TOOL_ID_EDEFAULT.equals(toolID);
      case SCLPackage.THEADER__VERSION:
        return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (id: ");
    result.append(id);
    result.append(", nameStructure: ");
    if (nameStructureESet) result.append(nameStructure); else result.append("<unset>");
    result.append(", revision: ");
    if (revisionESet) result.append(revision); else result.append("<unset>");
    result.append(", toolID: ");
    result.append(toolID);
    result.append(", version: ");
    result.append(version);
    result.append(')');
    return result.toString();
  }

} //THeaderImpl
