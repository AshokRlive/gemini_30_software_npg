/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TSMV</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTSMV()
 * @model extendedMetaData="name='tSMV' kind='elementOnly'"
 * @generated
 */
public interface TSMV extends TControlBlock {
} // TSMV
