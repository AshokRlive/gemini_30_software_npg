/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TControl</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TControl#getDatSet <em>Dat Set</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TControl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTControl()
 * @model abstract="true"
 *        extendedMetaData="name='tControl' kind='elementOnly'"
 * @generated
 */
public interface TControl extends TUnNaming {
  /**
   * Returns the value of the '<em><b>Dat Set</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dat Set</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dat Set</em>' attribute.
   * @see #setDatSet(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTControl_DatSet()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TDataSetName"
   *        extendedMetaData="kind='attribute' name='datSet'"
   * @generated
   */
  String getDatSet();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TControl#getDatSet <em>Dat Set</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dat Set</em>' attribute.
   * @see #getDatSet()
   * @generated
   */
  void setDatSet(String value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTControl_Name()
   * @model dataType="com.lucy.g3.iec61850.model.scl.TCBName" required="true"
   *        extendedMetaData="kind='attribute' name='name'"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TControl#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

} // TControl
