/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TTrg Ops</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isDchg <em>Dchg</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isDupd <em>Dupd</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isGi <em>Gi</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isPeriod <em>Period</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isQchg <em>Qchg</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTrgOps()
 * @model extendedMetaData="name='tTrgOps' kind='empty'"
 * @extends SCLObject
 * @generated
 */
public interface TTrgOps extends SCLObject {
  /**
   * Returns the value of the '<em><b>Dchg</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dchg</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dchg</em>' attribute.
   * @see #isSetDchg()
   * @see #unsetDchg()
   * @see #setDchg(boolean)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTrgOps_Dchg()
   * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   *        extendedMetaData="kind='attribute' name='dchg'"
   * @generated
   */
  boolean isDchg();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isDchg <em>Dchg</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dchg</em>' attribute.
   * @see #isSetDchg()
   * @see #unsetDchg()
   * @see #isDchg()
   * @generated
   */
  void setDchg(boolean value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isDchg <em>Dchg</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetDchg()
   * @see #isDchg()
   * @see #setDchg(boolean)
   * @generated
   */
  void unsetDchg();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isDchg <em>Dchg</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Dchg</em>' attribute is set.
   * @see #unsetDchg()
   * @see #isDchg()
   * @see #setDchg(boolean)
   * @generated
   */
  boolean isSetDchg();

  /**
   * Returns the value of the '<em><b>Dupd</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dupd</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dupd</em>' attribute.
   * @see #isSetDupd()
   * @see #unsetDupd()
   * @see #setDupd(boolean)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTrgOps_Dupd()
   * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   *        extendedMetaData="kind='attribute' name='dupd'"
   * @generated
   */
  boolean isDupd();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isDupd <em>Dupd</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dupd</em>' attribute.
   * @see #isSetDupd()
   * @see #unsetDupd()
   * @see #isDupd()
   * @generated
   */
  void setDupd(boolean value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isDupd <em>Dupd</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetDupd()
   * @see #isDupd()
   * @see #setDupd(boolean)
   * @generated
   */
  void unsetDupd();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isDupd <em>Dupd</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Dupd</em>' attribute is set.
   * @see #unsetDupd()
   * @see #isDupd()
   * @see #setDupd(boolean)
   * @generated
   */
  boolean isSetDupd();

  /**
   * Returns the value of the '<em><b>Gi</b></em>' attribute.
   * The default value is <code>"true"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Gi</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Gi</em>' attribute.
   * @see #isSetGi()
   * @see #unsetGi()
   * @see #setGi(boolean)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTrgOps_Gi()
   * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   *        extendedMetaData="kind='attribute' name='gi'"
   * @generated
   */
  boolean isGi();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isGi <em>Gi</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Gi</em>' attribute.
   * @see #isSetGi()
   * @see #unsetGi()
   * @see #isGi()
   * @generated
   */
  void setGi(boolean value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isGi <em>Gi</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetGi()
   * @see #isGi()
   * @see #setGi(boolean)
   * @generated
   */
  void unsetGi();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isGi <em>Gi</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Gi</em>' attribute is set.
   * @see #unsetGi()
   * @see #isGi()
   * @see #setGi(boolean)
   * @generated
   */
  boolean isSetGi();

  /**
   * Returns the value of the '<em><b>Period</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Period</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Period</em>' attribute.
   * @see #isSetPeriod()
   * @see #unsetPeriod()
   * @see #setPeriod(boolean)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTrgOps_Period()
   * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   *        extendedMetaData="kind='attribute' name='period'"
   * @generated
   */
  boolean isPeriod();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isPeriod <em>Period</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Period</em>' attribute.
   * @see #isSetPeriod()
   * @see #unsetPeriod()
   * @see #isPeriod()
   * @generated
   */
  void setPeriod(boolean value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isPeriod <em>Period</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetPeriod()
   * @see #isPeriod()
   * @see #setPeriod(boolean)
   * @generated
   */
  void unsetPeriod();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isPeriod <em>Period</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Period</em>' attribute is set.
   * @see #unsetPeriod()
   * @see #isPeriod()
   * @see #setPeriod(boolean)
   * @generated
   */
  boolean isSetPeriod();

  /**
   * Returns the value of the '<em><b>Qchg</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Qchg</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Qchg</em>' attribute.
   * @see #isSetQchg()
   * @see #unsetQchg()
   * @see #setQchg(boolean)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTTrgOps_Qchg()
   * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   *        extendedMetaData="kind='attribute' name='qchg'"
   * @generated
   */
  boolean isQchg();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isQchg <em>Qchg</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Qchg</em>' attribute.
   * @see #isSetQchg()
   * @see #unsetQchg()
   * @see #isQchg()
   * @generated
   */
  void setQchg(boolean value);

  /**
   * Unsets the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isQchg <em>Qchg</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetQchg()
   * @see #isQchg()
   * @see #setQchg(boolean)
   * @generated
   */
  void unsetQchg();

  /**
   * Returns whether the value of the '{@link com.lucy.g3.iec61850.model.scl.TTrgOps#isQchg <em>Qchg</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Qchg</em>' attribute is set.
   * @see #unsetQchg()
   * @see #isQchg()
   * @see #setQchg(boolean)
   * @generated
   */
  boolean isSetQchg();

} // TTrgOps
