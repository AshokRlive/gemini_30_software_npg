/**
 */
package com.lucy.g3.iec61850.model.scl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TPIPSUBNET</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTPIPSUBNET()
 * @model extendedMetaData="name='tP_IP-SUBNET' kind='simple'"
 * @generated
 */
public interface TPIPSUBNET extends TP {
} // TPIPSUBNET
