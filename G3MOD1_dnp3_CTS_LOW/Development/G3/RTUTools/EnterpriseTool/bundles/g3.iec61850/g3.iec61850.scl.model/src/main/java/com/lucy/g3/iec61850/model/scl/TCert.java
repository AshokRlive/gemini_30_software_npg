/**
 */
package com.lucy.g3.iec61850.model.scl;

import com.lucy.g3.iec61850.model.internal.SCLObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TCert</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TCert#getCommonName <em>Common Name</em>}</li>
 *   <li>{@link com.lucy.g3.iec61850.model.scl.TCert#getIdHierarchy <em>Id Hierarchy</em>}</li>
 * </ul>
 *
 * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTCert()
 * @model extendedMetaData="name='tCert' kind='empty'"
 * @extends SCLObject
 * @generated
 */
public interface TCert extends SCLObject {
  /**
   * Returns the value of the '<em><b>Common Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Common Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Common Name</em>' attribute.
   * @see #setCommonName(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTCert_CommonName()
   * @model dataType="com.lucy.g3.iec61850.model.scl.CommonNameType" required="true"
   *        extendedMetaData="kind='attribute' name='commonName'"
   * @generated
   */
  String getCommonName();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TCert#getCommonName <em>Common Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Common Name</em>' attribute.
   * @see #getCommonName()
   * @generated
   */
  void setCommonName(String value);

  /**
   * Returns the value of the '<em><b>Id Hierarchy</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id Hierarchy</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id Hierarchy</em>' attribute.
   * @see #setIdHierarchy(String)
   * @see com.lucy.g3.iec61850.model.scl.SCLPackage#getTCert_IdHierarchy()
   * @model dataType="com.lucy.g3.iec61850.model.scl.IdHierarchyType" required="true"
   *        extendedMetaData="kind='attribute' name='idHierarchy'"
   * @generated
   */
  String getIdHierarchy();

  /**
   * Sets the value of the '{@link com.lucy.g3.iec61850.model.scl.TCert#getIdHierarchy <em>Id Hierarchy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id Hierarchy</em>' attribute.
   * @see #getIdHierarchy()
   * @generated
   */
  void setIdHierarchy(String value);

} // TCert
