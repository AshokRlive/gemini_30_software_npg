
package com.lucy.g3.iec61850.gui.gemini3rtu.handlers;

import java.io.File;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import com.lucy.g3.iec61850.gui.gemini3rtu.parts.G3ConfigPart;
import com.lucy.g3.iec61850.gui.gemini3rtu.xml.G3Configuration;


public class LoadG3Config {
  @Execute
  public void executeHandler(EPartService partService, Shell shell) {
    FileDialog dlg = new FileDialog(shell,  SWT.OPEN );
    dlg.setText("Open");
    String path = dlg.open();
    if(path == null) return;
    
        
    File g3file = new File(path);
    
    MPart part = partService.findPart(G3ConfigPart.ID);
    partService.activate(part, false);
    G3ConfigPart g3config = (G3ConfigPart)part.getObject();
    
    if (g3config == null) return;
    
    
    g3config.config.load(g3file);    
    g3config.viewer.setInput(G3Configuration.getRootList());
  }
  
  @CanExecute
  public boolean canExecute() {
//    System.out.println("Testing 010");
    return true;
  }
}

