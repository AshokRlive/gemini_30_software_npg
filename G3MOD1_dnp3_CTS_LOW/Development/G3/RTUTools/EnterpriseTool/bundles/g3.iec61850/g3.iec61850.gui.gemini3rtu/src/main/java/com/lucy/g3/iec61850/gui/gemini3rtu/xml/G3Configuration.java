
package com.lucy.g3.iec61850.gui.gemini3rtu.xml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.lucy.g3.iec61850.gui.internal.G3Point;
import com.lucy.g3.iec61850.model.scl.SCLFactory;
import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TAddress;
import com.lucy.g3.iec61850.model.scl.TConnectedAP;
import com.lucy.g3.iec61850.model.scl.TP;
import com.lucy.g3.iec61850.model.scl.TPOSIAPTitle;

public class G3Configuration {
  
//  private static String G3CONFIG_FILE = "C:/repobox/G3Software/Development/G3/RTUTools/bundles/g3.iec61850/g3.iec61850.gui/g3config.xml";
  
  public Document doc;

  public G3Configuration() {        
  }
  
  public static void main(String[] args) {
    G3Configuration xml  = new G3Configuration();
    
//    xml.load(new File(G3CONFIG_FILE));
    
    System.out.println("Root element :" + xml.doc.getDocumentElement().getNodeName());
    Element root = xml.doc.getDocumentElement();

    getRootList();

    xml.getComms();

  }
  
  public static ArrayList getRootList() {
    ArrayList<String> types = new ArrayList<String>();
    types.add("Virtual Points");
    types.add("Control Logic");
    return types;    
  }
  
  public ArrayList getVPTypeList() {
    ArrayList<String> types = new ArrayList<String>();
    Node parentVP = getVirtualPoints();
    if (parentVP != null) {
      NodeList nList = parentVP.getChildNodes();    
      for (int i = 0; i < nList.getLength(); i++) {
        Node nNode = nList.item(i);
       
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
  //        System.out.println("\nCurrent Element :" + nNode.getNodeName());
          types.add(nNode.getNodeName());
        }
      }
    }
    return types;  
  }
  
  public ArrayList<G3Point> getControlLogicList() {
    ArrayList<G3Point> clogic = new ArrayList<G3Point>();
    Node parentCL = getControlLogic();
    
    if (parentCL != null) {
      NodeList nList = parentCL.getChildNodes();    
      for (int i = 0; i < nList.getLength(); i++) {
        Node nNode = nList.item(i);
       
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          Node name = nNode.getAttributes().getNamedItem("name");
          Node uuid = nNode.getAttributes().getNamedItem("uuid");
          Node grp = nNode.getAttributes().getNamedItem("group");
          clogic.add(G3Point.createG3PointByData(grp.getNodeValue(), "0", uuid.getNodeName(), name.getNodeValue()));
        }
      }
    }
    return clogic;    
  }
  

  
  public void load(File file) {
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

    dbf.setNamespaceAware(true);
    dbf.setValidating(false);
    
    
    try {
      DocumentBuilder db = dbf.newDocumentBuilder();
      doc = db.parse(file);
      doc.getDocumentElement().normalize();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
  }
  
  
  private String getElementNodeName(Node nNode) {
    if (nNode.getNodeType() == Node.ELEMENT_NODE)
      return nNode.getNodeName();
    return null;
  }
  
  
  private Node getVirtualPoints() {

    try {
    
//      System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
      NodeList nList = doc.getElementsByTagName("virtualPoints");
      
      //TODO if it doesn't exist ?!
      Node virtualPoints = nList.item(0);
      return virtualPoints;
    } catch(NullPointerException ex) {
      System.err.println("G3 Configuration not loaded");
    }
    return null;
    
  }
  
  private Node getControlLogic() {
    
    try {
//      System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
      NodeList nList = doc.getElementsByTagName("controlLogic");
      
      //TODO if it doesn't exist ?!    
      Node controlLogics = nList.item(0);
      return controlLogics;
    } catch(NullPointerException ex) {
      System.err.println("G3 Configuration not loaded");
    }
    return null;
    
  }
  
  public boolean allowChild(String parent) {
    if (getRootList().contains(parent.toString()))
      return true;
    
    if (parent.matches("\\[\\d+\\].*?"))
      return true;
    
    NodeList nList = doc.getElementsByTagName(parent);
    
    if(nList.getLength() > 0)
      return true;
    else return false;
  }
  
  public ArrayList<String> getChildrenByCL(String cl) {
    
    ArrayList<String> pts = new ArrayList<String>();
    
    String grp = cl.substring(0, cl.indexOf("]")).replace("[", "");
    
    NodeList nList = doc.getElementsByTagName("clogic");
    for (int i = 0; i < nList.getLength(); i++) {
      Node nNode = nList.item(i);
      if (nNode.getNodeType() == Node.ELEMENT_NODE) {
        Node group = nNode.getAttributes().getNamedItem("group");
        if (group.getNodeValue().toString().contains(grp)) {
          NodeList children = nNode.getChildNodes();
          for(int j = 0; j < children.getLength(); j++) {
            Node n = children.item(j);
            if (n.getNodeType() == Node.ELEMENT_NODE) {
              Node desc = null;
              if (n.getNodeName().startsWith("inputs") || n.getNodeName().startsWith("outputs"))
                desc = n.getAttributes().getNamedItem("name");
              else
                desc = n.getAttributes().getNamedItem("description");
              if (desc != null)
                pts.add(desc.getNodeValue());
              else 
                pts.add(n.getNodeName());
            }
          }
          return pts;
        }
      }
    }
    return null;
    
  }
  
  public ArrayList<G3Point> getChildrenByVPType(String type) {
    NodeList nList = doc.getElementsByTagName(type);
    nList = nList.item(0).getChildNodes();
    
    ArrayList<G3Point> vps = new ArrayList<G3Point>();
    for (int i = 0; i < nList.getLength(); i++) {
      Node nNode = nList.item(i);
      if (nNode.getNodeType() == Node.ELEMENT_NODE) {
        Node grp = nNode.getAttributes().getNamedItem("group");
        Node id = nNode.getAttributes().getNamedItem("id");
        Node uuid = nNode.getAttributes().getNamedItem("uuid");
        Node desc = nNode.getAttributes().getNamedItem("description");
        
        String uuidStr = UUID.randomUUID().toString();
        if (uuid != null)
          uuidStr = uuid.getNodeValue();
        
        if (id != null || desc != null)
          vps.add(G3Point.createG3PointByData(grp.getNodeValue(), id.getNodeValue(), uuidStr, desc.getNodeValue()));
      }
    }
    
    return vps;
  }
  
  public ArrayList getComms(){
    EClass clazz = SCLPackage.eINSTANCE.getTConnectedAP();
    
    NodeList nList = doc.getElementsByTagName("ethernet");
    for (int i = 0; i < nList.getLength(); i++) {
      Node eth = nList.item(i);
      if (eth.getNodeType() != Node.ELEMENT_NODE)
        continue;
      Node name = eth.getAttributes().getNamedItem("portName");
      
      NodeList ipsetting = eth.getChildNodes();
      clazz = SCLPackage.eINSTANCE.getTConnectedAP();
      TConnectedAP cap = (TConnectedAP) EcoreUtil.create(clazz);
      cap.setApName(name.getNodeValue());
      
      for(int j = 0; j < ipsetting.getLength(); j++){
        clazz = SCLPackage.eINSTANCE.getTAddress();
        TAddress addr = (TAddress) EcoreUtil.create(clazz);
        
        cap.setAddress(addr);
        clazz = SCLPackage.eINSTANCE.getTP();
        TP tp = (TP) EcoreUtil.create(clazz);
        
        TPOSIAPTitle title = SCLFactory.eINSTANCE.createTPOSIAPTitle();
        title.setValue("1,1,9999,1");
        
//        addr.getP().
        Node ipv4 = ipsetting.item(j);
        if (ipv4.getNodeType() != Node.ELEMENT_NODE)
          continue;
        if (ipv4.getNodeName().equals("ipv4Setting")) {
          NodeList stIPchld = ipv4.getChildNodes();
          for(int k = 0; k < stIPchld.getLength(); k++){
            Node stIP = stIPchld.item(k);
            if (stIP.getNodeType() != Node.ELEMENT_NODE)
              continue;
            Node ip = stIP.getAttributes().getNamedItem("ipaddress");
            Node gateway = stIP.getAttributes().getNamedItem("gateway");
            Node submask = stIP.getAttributes().getNamedItem("submask");
            
            
          }
        }
        ipv4.getNextSibling();
      }
      
    }
    return null;
  }
  
  
}




