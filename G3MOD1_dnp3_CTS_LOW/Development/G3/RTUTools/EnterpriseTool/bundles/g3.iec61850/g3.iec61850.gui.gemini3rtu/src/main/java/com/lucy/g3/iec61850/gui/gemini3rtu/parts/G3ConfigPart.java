 
package com.lucy.g3.iec61850.gui.gemini3rtu.parts;

import javax.inject.Inject;

import java.net.URL;
import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import com.lucy.g3.iec61850.gui.gemini3rtu.xml.G3Configuration;
import com.lucy.g3.iec61850.gui.internal.G3Point;



public class G3ConfigPart {
  public TreeViewer viewer;
  
  public G3Configuration config;
  
  public static String ID = "g3.iec61850.gui.explorer.part.gemini3";
  
  @Inject
  private ESelectionService selectionService;
  
//  @Inject
  public G3ConfigPart(){
    config = new G3Configuration();
  }

  @PostConstruct
  public void createControls(Composite parent) {
    
    

    viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
    
    viewer.setContentProvider(new G3ConfigContentProvider(config));
    viewer.setLabelProvider(new DelegatingStyledCellLabelProvider(new ViewLabelProvider(createImageDescriptor())));
    viewer.setInput(G3Configuration.getRootList());
    
    viewer.addSelectionChangedListener(new ISelectionChangedListener() {
      
      @Override
      public void selectionChanged(SelectionChangedEvent event) {
        IStructuredSelection sel = (IStructuredSelection) viewer.getSelection();
        
        if (sel.getFirstElement() instanceof G3Point){
          G3Point p = (G3Point) sel.getFirstElement();
          selectionService.setSelection(p.ref);
        }
        
      }
    });
    
//    TreeViewerSWTFactory.createTreeViewer(parent, null);

    
  }

  public static ImageDescriptor createImageDescriptor() {
    Bundle bundle = FrameworkUtil.getBundle(ViewLabelProvider.class);
    URL url = FileLocator.find(bundle, new Path("icons/save_edit.png"), null);
    return ImageDescriptor.createFromURL(url);
  }

  static class G3ConfigContentProvider implements ITreeContentProvider {
    
    G3Configuration config;
    
    public G3ConfigContentProvider(G3Configuration config){
      this.config = config;
    }
    
    public void inputChanged(Viewer v, Object oldInput, Object newInput) {
    }

    @Override
    public void dispose() {
    }

    @Override
    public Object[] getElements(Object inputElement) {
      return ((ArrayList) inputElement).toArray();
    }

    @Override
    public Object[] getChildren(Object parentElement) {
      String parent = parentElement.toString();
      Object[] out = null;
      
      if (parent.equals("Virtual Points")) {
        return config.getVPTypeList().toArray();
      }
      if (parent.equals("Control Logic")) {
        return config.getControlLogicList().toArray();
      }
      
      if (config.getVPTypeList().contains(parent)){
        out = config.getChildrenByVPType(parent).toArray();
      }
      
      if (config.getControlLogicList().contains(parent)){
        out = config.getChildrenByCL(parent).toArray();
      }
      
      
      return out;
    }

    @Override
    public Object getParent(Object element) {

      return null;
    }

    @Override
    public boolean hasChildren(Object element) {
      return config.allowChild(element.toString());
    }

  }

  static class ViewLabelProvider extends LabelProvider implements IStyledLabelProvider {
    
    private ImageDescriptor directoryImage;
    private ResourceManager resourceManager;

    public ViewLabelProvider(ImageDescriptor directoryImage) {
      this.directoryImage = directoryImage;
    }

    @Override
    public StyledString getStyledText(Object element) {
      
      if (element instanceof G3Point){
        G3Point p = (G3Point) element;
        String desc = (p.description.isEmpty()) ? p.ref.getUuid() : p.description;
        return new StyledString("[" +p.ref.getGroup() + "," + p.ref.getId() + "] " + desc);
      }
        
      
      return new StyledString(element.toString());
    }
    
    @Override
    public Image getImage(Object element) {

      
      return super.getImage(element);
    }
    
    @Override
    public void dispose() {
      // garbage collect system resources
      if(resourceManager != null) {
        resourceManager.dispose();
        resourceManager = null;
      }
    }
    
    protected ResourceManager getResourceManager() {
      if(resourceManager == null) {
        resourceManager = new LocalResourceManager(JFaceResources.getResources());
      }
      return resourceManager;
    }


  }
  
  @Focus
  public void setFocus() {
    viewer.getControl().setFocus();
  }
  

}
