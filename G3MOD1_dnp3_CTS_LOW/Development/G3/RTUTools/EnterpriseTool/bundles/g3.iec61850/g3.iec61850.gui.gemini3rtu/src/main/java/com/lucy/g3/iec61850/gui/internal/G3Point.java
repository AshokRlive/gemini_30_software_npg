
package com.lucy.g3.iec61850.gui.internal;

import java.math.BigInteger;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage;

public class G3Point {
  public G3RefT ref;
  public String description;
  
  public G3Point(G3RefT ref, String desc) {
    this.ref = ref;
    this.description = desc;
  }
  
  public static G3Point createG3PointByData(String group, String id, String uuid, String desc) {
    EClass eClass = IEC61850MappingPackage.eINSTANCE.getG3RefT();
    G3RefT ref = (G3RefT) EcoreUtil.create(eClass);
    ref.setGroup(new BigInteger(group));
    ref.setId(new BigInteger(id));
    ref.setUuid(uuid);
    return new G3Point(ref, desc);
  }
}
