
1. Open installed product from \RTUTools\products\g3.iec61850.gui.product\target\products\g3.iec61850.gui.product\win32\win32\x86_64\Lucy_IEC61850.exe
	- use mvn clean package at \RTUTools\releng\g3.releng.conf.iec61850\
2. File > Load G3 Configuration 
	- sample file under \RTUTools\bundles\g3.iec61850\sample files
3. File > New - create a new ICD file

4. Add mapping on Data Attributes (DA) by selecting each Data Object for Logical Node using TreeView
	- Logical Nodes are under IED / Access Point / Device / 

4.1 Once all the mandatory DA are mapped, 
	you can Generate the ICD (IED Capability Description) and Mapping file
5. File > Generate ICD - Dialog to Confirm integrity and location

-- Load SCL file with mapping - CID File
6. File > Import 
6.1 	Mapping file must have the same name as the SCL file and it should be resided in the same directory 