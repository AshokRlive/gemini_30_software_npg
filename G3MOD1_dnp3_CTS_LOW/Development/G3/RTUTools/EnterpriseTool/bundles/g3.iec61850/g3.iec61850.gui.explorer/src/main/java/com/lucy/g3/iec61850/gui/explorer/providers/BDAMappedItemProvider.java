
package com.lucy.g3.iec61850.gui.explorer.providers;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ITableItemLabelProvider;
import com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute;
import com.lucy.g3.iec61850.model.scl.provider.TBDAItemProvider;

public class BDAMappedItemProvider extends TBDAItemProvider implements ITableItemLabelProvider {
  public BDAMappedItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
    
  }
   
  @Override
  public String getColumnText(Object ob, int index){
    if (ob instanceof TAbstractDataAttribute) {
      TAbstractDataAttribute sel = (TAbstractDataAttribute) ob;
//      if (sel.getObject() instanceof sel)
        return DAMappedItemProvider.getColumnTextForDA(sel, index);
    }
    return super.getColumnText(ob, index);
  }
  

}
