
package com.lucy.g3.iec61850.gui.explorer.adaptor;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ITableItemColorProvider;
import org.eclipse.emf.edit.provider.ITableItemLabelProvider;
import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.ui.views.properties.IPropertySource;
import org.osgi.framework.FrameworkUtil;

import com.lucy.g3.iec61850.gui.explorer.internal.SelectEObject;
import com.lucy.g3.iec61850.gui.explorer.providers.BDAMappedItemProvider;
import com.lucy.g3.iec61850.gui.explorer.providers.DAMappedItemProvider;
import com.lucy.g3.iec61850.gui.explorer.providers.DOItemProvider;
import com.lucy.g3.iec61850.gui.explorer.providers.LNItemProvider;
import com.lucy.g3.iec61850.gui.explorer.services.IMappingService;
import com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute;
import com.lucy.g3.iec61850.model.scl.TDA;
import com.lucy.g3.iec61850.model.scl.provider.SCLItemProviderAdapterFactory;
import com.lucy.g3.iec61850.model.scl.provider.TBDAItemProvider;
import com.lucy.g3.iec61850.model.scl.provider.TDAItemProvider;
import com.lucy.g3.iec61850.model.scl.provider.TDOItemProvider;
import com.lucy.g3.iec61850.model.scl.provider.TLNItemProvider;


public class SCLProviderAdapterFactory extends SCLItemProviderAdapterFactory {
  

  
  public SCLProviderAdapterFactory(){
    supportedTypes.add(ITableItemLabelProvider.class);
    supportedTypes.add(ITableItemColorProvider.class);
//    supportedTypes.add(IPropertySource.class);
    
  }
  
  @Override
  public Adapter createTBDAAdapter() {
    if (tbdaItemProvider == null) {
      tbdaItemProvider = new BDAMappedItemProvider(this);
    }

    return tbdaItemProvider;
  }
  
  @Override
  public Adapter createTDAAdapter() {
    if (tdaItemProvider == null) {
      tdaItemProvider = new DAMappedItemProvider(this);
    }    

    return tdaItemProvider;
  }
  
  @Override
  public Adapter createTLNAdapter() {
    if (tlnItemProvider == null) {
      tlnItemProvider =  new LNItemProvider(this);
    }

    return tlnItemProvider;
  }
  
  @Override
  public Adapter createTDOAdapter() {
    if (tdoItemProvider == null) {
      tdoItemProvider = new DOItemProvider(this);
    }

    return tdoItemProvider;
  }
  
  
  

  
}


