
package com.lucy.g3.iec61850.gui.explorer.handlers;


import java.io.File;
import java.io.IOException;

import javax.inject.Inject;
import javax.swing.JFileChooser;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import com.lucy.g3.iec61850.gui.explorer.parts.TreeViewPart;
import com.lucy.g3.iec61850.gui.explorer.services.IMappingService;
import com.lucy.g3.iec61850.gui.explorer.services.LookUpDataTemplate;
import com.lucy.g3.iec61850.model.scl.SCLType;
import com.lucy.g3.iec61850.model.scl.impl.DocumentRootImpl;




public class ImportSCDFile  {

  private static final String[] FILTER_EXTS = { "*.scd", "*.icd", "*.cid", "*.*"};
  
  @Inject
  IMappingService mappingService;
  
  @Execute
  public void executeHandler(EPartService partService, Shell shell) {
    // TODO Auto-generated method stub
    
    File scd = new File("C:/repobox/G3Software/Development/G3/RTUTools/bundles/g3.iec61850/g3.iec61850.gui/newcid.xml");
    
    FileDialog dlg = new FileDialog(shell,  SWT.OPEN );
    dlg.setText("Open");
    String path = dlg.open();
    if(path == null) return;
    
    //TODO add file filter - for scd, cid, iid (icd)
    
        
    scd = new File(path);
    
    MPart part = partService.findPart(TreeViewPart.TreeViewPart_ID);
    TreeViewPart scl = (TreeViewPart)part.getObject();
    scl.init();
    Resource rs = TreeViewPart.editingDomain.getResourceSet().getResource(URI.createFileURI(scd.getAbsolutePath()), true);

    
    
    scl.viewer.setInput(rs.getContents().get(0));
    
    scl.viewer.reveal(SCLType.class);
    
    mappingService.load(scd.getAbsolutePath());
    
    
  }
  
  
  @CanExecute
  public boolean canExecute(final IEclipseContext ictx) {
    
      
    
    return true;
  }
  

}

