
package com.lucy.g3.iec61850.gui.explorer.adaptor;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.ui.views.properties.IPropertySource;

public class SCLPropertyAdapter implements IAdapterFactory   {
  
  ComposedAdapterFactory emfGlobalFactory;
  
  public Object getAdapter(Object adaptableObject, Class adapterType) {
    System.out.println("--" + adaptableObject.getClass().getName());
    if (adapterType == IPropertySource.class && adaptableObject instanceof EObject) {
      emfGlobalFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
      emfGlobalFactory.addAdapterFactory(new SCLProviderAdapterFactory());
      
      EObject eo = (EObject) adaptableObject;
      
      
      return new AdapterFactoryContentProvider(emfGlobalFactory).getPropertySource((EObject)eo);
    }
    return null;
  }

  @Override
  public Class<?>[] getAdapterList() {
    // TODO Auto-generated method stub
//    return null;
    return new Class[] { IPropertySource.class };
  }

  
}

