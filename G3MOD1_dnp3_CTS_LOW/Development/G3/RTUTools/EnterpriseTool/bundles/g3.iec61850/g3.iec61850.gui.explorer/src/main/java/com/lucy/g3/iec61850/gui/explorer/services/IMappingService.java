
package com.lucy.g3.iec61850.gui.explorer.services;


import java.io.File;
import java.io.IOException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;

import com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT;

public interface IMappingService {
  void addEntry(String path, G3RefT virualPoint, Object dataType);
  G3RefT getG3Ref(Object dataAttribute);
  
  IStatus validate(EObject root) throws IllegalStateException;
  void save(File file) throws IOException;
  void load(String filename);
  void create();
}

