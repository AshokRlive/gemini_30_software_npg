
package com.lucy.g3.iec61850.gui.explorer.properties;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.ui.celleditor.ExtendedDialogCellEditor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.ui.celleditor.FeatureEditorDialog;
import org.eclipse.emf.edit.ui.provider.PropertyDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.ICellEditorListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

import com.lucy.g3.iec61850.gui.explorer.parts.TreeViewPart;
import com.lucy.g3.iec61850.gui.explorer.services.LookUpDataTemplate;
import com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute;
import com.lucy.g3.iec61850.model.scl.TDA;
import com.lucy.g3.iec61850.model.scl.TDAType;
import com.lucy.g3.iec61850.model.scl.TDO;
import com.lucy.g3.iec61850.model.scl.TDOType;
import com.lucy.g3.iec61850.model.scl.TDataTypeTemplates;
import com.lucy.g3.iec61850.model.scl.TEnumType;
import com.lucy.g3.iec61850.model.scl.TLNodeType;
import com.lucy.g3.iec61850.model.scl.impl.TLNImpl;


public class LNPropertyDescriptor extends PropertyDescriptor  {

  
  private AdapterFactory adapterFactory;
  /**
  * @param object
  * @param itemPropertyDescriptor
  */
  public LNPropertyDescriptor(AdapterFactory adapterFactory, Object 
  object, IItemPropertyDescriptor itemPropertyDescriptor) {
    super(object, itemPropertyDescriptor);
    this.adapterFactory = adapterFactory;
//    System.out.println(itemPropertyDescriptor);
//    System.out.println(itemPropertyDescriptor.getFeature(object));
//    System.out.println(itemPropertyDescriptor.getLabelProvider(object));
  }

  @Override
  public CellEditor createPropertyEditor(Composite parent) {
    
    final EStructuralFeature feature = (EStructuralFeature)itemPropertyDescriptor.getFeature(object);
    final ILabelProvider editLabelProvider = getEditLabelProvider();
    CellEditor c =  new ExtendedDialogCellEditor(parent, editLabelProvider)
    {
      @Override
      protected Object openDialogBox(Control cellEditorWindow)
      {
        List<Object> v = new ArrayList<Object>();
        v.add(object);
        FeatureEditorDialog dialog = new FeatureEditorDialog(
          cellEditorWindow.getShell(),
          editLabelProvider,
          object,
          feature.getEType(),
          v,
          getDisplayName(),
          null,
          itemPropertyDescriptor.isMultiLine(object),
          false,
          feature.isUnique());
        dialog.open();
        return dialog.getResult();
      }
    };
    
    return t(parent);
    
  }
  
  private CellEditor t(Composite parent){
    CellEditor ced = super.createPropertyEditor(parent);
    
    if (!(this.object instanceof TLNImpl))
      return ced;
    
    //EAttribute is LnClass
    ItemPropertyDescriptor dsr = (ItemPropertyDescriptor) itemPropertyDescriptor;
    EStructuralFeature feature = (EStructuralFeature) itemPropertyDescriptor.getFeature(object);
    if (feature instanceof EAttribute){
      EAttribute at = (EAttribute) feature;
      if(!at.getName().equals("lnClass")){
        return ced;
      }
    } else {
      return ced;
    }
    
    final TLNImpl ob = (TLNImpl) this.object;
    
    if (ced instanceof ComboBoxCellEditor) {
      final ComboBoxCellEditor combo = (ComboBoxCellEditor) ced;
      
      
      
      combo.addListener(new ICellEditorListener() {
        
        @Override
        public void editorValueChanged(boolean oldValidState, boolean newValidState) {
          // TODO Auto-generated method stub
          System.out.println("editorValueChanged");
        }
        
        @Override
        public void cancelEditor() {
          // TODO Auto-generated method stub
          System.out.println("cancelEditor");
        }
        
        @Override
        public void applyEditorValue() {
          // TODO add Logical Node from Lookup
          System.out.println("Action: add Logical Node from Lookup");
          if(combo.getValue() == null)
            return;

          String curValue = combo.getValue().toString();
          
          
          LookUpDataTemplate lookup = LookUpDataTemplate.getInstance();
          
          
          EObject root = EcoreUtil.getRootContainer((EObject)object);
          TDataTypeTemplates dt = (TDataTypeTemplates) EcoreUtil.getEObject(root, "@sCL/@dataTypeTemplates");
          
          
          TLNodeType node = (TLNodeType) lookup.getLNByClass(curValue);
          
          int count = 0;
          for(TLNodeType lnt : dt.getLNodeType()) {
            if(lnt.getLnClass().getClass().isEnum()) {
              Enumerator enm = (Enumerator) lnt.getLnClass();
              if (enm.getLiteral().equals(curValue))
                count++;
            }
          }
          
          node.setId(node.getId() + "_" + count);
          
          if (node != null){
            for(TDO dob : node.getDO()){
              
                TDOType dtype = (TDOType) lookup.getDOType(dob.getType());
                dtype.setId(dtype.getId() + "_" + node.getId());
//                for(TAbstractDataAttribute da : dtype.getDA())
                dob.setType(dtype.getId());
                addDataType(dtype.getId(), dtype.getDA(), dt);
                
                dt.getDOType().add(dtype);              
            }
            ob.setLnType(node.getId());
            ob.setInst(count+"");
            ob.setDesc(node.getDesc());
            
            //TODO do not add new LN if it already exists - or delete them on save
            dt.getLNodeType().add( node);
          }
          
          
          
        }
        
        private void addDataType(String doID, EList<?> list, TDataTypeTemplates dt) {
          LookUpDataTemplate l = LookUpDataTemplate.getInstance();
          for(Object ob : list){
            TAbstractDataAttribute da = (TAbstractDataAttribute) ob;
            if (da.getBType().getLiteral().equals("Struct")){
              
              
              TDAType ndtype = (TDAType) l.getDAType(da.getType());
              ndtype.setId(ndtype.getId()+"_"+doID);
              da.setType(ndtype.getId());
              dt.getDAType().add(ndtype);
              addDataType(ndtype.getId(), ndtype.getBDA(), dt);
                                
            }
            
            if (da.getBType().getLiteral().equals("Enum")){
              
              TEnumType ndtype = (TEnumType) l.getEnumType(da.getType());
              ndtype.setId(ndtype.getId()+"_"+doID);
              da.setType(ndtype.getId());
              dt.getEnumType().add(ndtype);
                              
            }
          }
        }
      });
      ced = combo;
    }
    return ced;
  }
  
  


}

