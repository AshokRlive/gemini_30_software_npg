
package com.lucy.g3.iec61850.gui.explorer.properties;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.ui.provider.PropertyDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.ICellEditorListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

import com.lucy.g3.iec61850.gui.explorer.parts.TreeViewPart;
import com.lucy.g3.iec61850.gui.explorer.services.LookUpDataTemplate;
import com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute;
import com.lucy.g3.iec61850.model.scl.TDA;
import com.lucy.g3.iec61850.model.scl.TDAType;
import com.lucy.g3.iec61850.model.scl.TDO;
import com.lucy.g3.iec61850.model.scl.TDOType;
import com.lucy.g3.iec61850.model.scl.TDataTypeTemplates;
import com.lucy.g3.iec61850.model.scl.TEnumType;
import com.lucy.g3.iec61850.model.scl.TLNodeType;
import com.lucy.g3.iec61850.model.scl.impl.TLNImpl;


public class CustomPropertyDescriptor extends PropertyDescriptor  {

  
  private AdapterFactory adapterFactory;
  /**
  * @param object
  * @param itemPropertyDescriptor
  */
  public CustomPropertyDescriptor(AdapterFactory adapterFactory, Object 
  object, IItemPropertyDescriptor itemPropertyDescriptor) {
    super(object, itemPropertyDescriptor);
    this.adapterFactory = adapterFactory;
  }


}

