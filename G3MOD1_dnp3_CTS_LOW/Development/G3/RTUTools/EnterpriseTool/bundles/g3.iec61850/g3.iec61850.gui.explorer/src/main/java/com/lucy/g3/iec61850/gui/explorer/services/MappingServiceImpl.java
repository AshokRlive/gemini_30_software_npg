
package com.lucy.g3.iec61850.gui.explorer.services;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.lucy.g3.iec61850.gui.explorer.internal.TemplateFunction;
import com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.EntryT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage;
import com.lucy.g3.iec61850.model.IEC61850Mapping.MappingRootT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.impl.DocumentRootImpl;
import com.lucy.g3.iec61850.model.IEC61850Mapping.util.IEC61850MappingResourceFactoryImpl;
import com.lucy.g3.iec61850.model.scl.TDO;
import com.lucy.g3.iec61850.model.scl.TSDO;

@Creatable
@Singleton
public class MappingServiceImpl implements IMappingService{

  
  static List<EntryT> lstMapping = new ArrayList<EntryT>();
  
  private static String FILE_MAPPING = "C:/repobox/G3Software/Development/G3/RTUTools/bundles/g3.iec61850/g3.iec61850.gui/mapping.xml";
  
  protected static Resource resource;
  
  ResourceSet resourceSet;
  
  @Inject
  private EPartService partService;
  
  @Inject ESelectionService selectionService;
  
  static MappingRootT root;
  
  public MappingServiceImpl(){
    resourceSet = new ResourceSetImpl();
    resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION,
        new IEC61850MappingResourceFactoryImpl());
    resourceSet.getPackageRegistry().put(IEC61850MappingPackage.eNS_URI, IEC61850MappingPackage.eINSTANCE);
    
  
    
  }
  
  public void save() {
    try {
      resource.save(Collections.EMPTY_MAP);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  @Override
  public void save(File file) throws IOException{
    URI uri = URI.createFileURI(file.getAbsolutePath());
    Resource rs = resource.getResourceSet().createResource(uri);
    rs.getContents().addAll(resource.getContents());
    rs.save(Collections.EMPTY_MAP);
  }

  
  @Override
  public void load(String filename){    
    
    File map = new File(FilenameUtils.getFullPath(filename) + FilenameUtils.getBaseName(filename) + ".map" );
    
    if(map.exists()) {
      URI uri = URI.createFileURI(map.getAbsolutePath());
      resource = resourceSet.getResource(uri, true);
      DocumentRootImpl doc;
      
      if(resource.getContents().isEmpty()) {
        System.err.println("Resource is empty; Creating DocumentRoot...");
        EClass eClass = IEC61850MappingPackage.eINSTANCE.getDocumentRoot();
        resource.getContents().add(doc = (DocumentRootImpl) EcoreUtil.create(eClass));
        eClass = IEC61850MappingPackage.eINSTANCE.getMappingRootT();
        doc.setMappingRoot((MappingRootT) EcoreUtil.create(eClass));
      }
      
      doc = (DocumentRootImpl) resource.getContents().get(0);
      root = doc.getMappingRoot();
    } else {
      create();
    }
    
  }
  
  public void create() {
    URI uri = URI.createURI("temp.map");
    create(uri);
  }
  
  public void creates(File file) {
    if (!file.exists()) {
      try {
        file.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    
    URI uri = URI.createFileURI(file.getAbsolutePath()); 
    create(uri);
  }
  
  public void create(URI uri) {
    
       
    resource = resourceSet.createResource(uri);
    
    EClass eClass = IEC61850MappingPackage.eINSTANCE.getDocumentRoot();
    resource.getContents().add(EcoreUtil.create(eClass));
    
    
    DocumentRootImpl doc = (DocumentRootImpl) resource.getContents().get(0);
    
    eClass = IEC61850MappingPackage.eINSTANCE.getMappingRootT();
    
    root = (MappingRootT) EcoreUtil.create(eClass);
    
    doc.setMappingRoot(root);
    
    root.setSchemaVersionMajor((short) 1);
    root.setSchemaVersionMinor((short) 0);
  }
  
    
  @Override
  public void addEntry(String path, G3RefT g3ref, Object dateType) {
    if (g3ref == null || path == null || path.length() <= 0)
      return;
    
    EClass eClass = IEC61850MappingPackage.eINSTANCE.getEntryT();
    EntryT en = (EntryT) EcoreUtil.create(eClass);
    en.setAttributeRef(path.toString());
    
    
    en.setG3Ref(g3ref);
    
        
    eClass = IEC61850MappingPackage.eINSTANCE.getDataAttributeIdT();
    DataAttributeIdT attr = (DataAttributeIdT) EcoreUtil.create(eClass);
    en.setDataAttributeId(attr);
    
    // default values
    attr.setField0(new BigInteger("0"));
    attr.setField1(new BigInteger("0"));
    attr.setField2(new BigInteger("0"));
    attr.setField3(new BigInteger("0"));
    attr.setField4(new BigInteger("0"));
    attr.setField5(new BigInteger("0"));


    en.setDataType(new BigInteger("0"));
    if (dateType.getClass().isEnum()) {
    	Enumerator enm = (Enumerator) dateType;
    	en.setDataType(new BigInteger(enm.getValue() + ""));
    }
   
    int pos = findEntryByAttribute(path.toString());
    if (pos < 0)
      root.getEntry().add(en);
    else {
      root.getEntry().set(pos, en);
    }
  }
  
  private int findEntryByAttribute(String attr) {
    if (attr == null || root == null || root.getEntry().size() <= 0) return -1;
    for (EntryT en : root.getEntry()) {
      if(en.getAttributeRef().equals(attr))
        return root.getEntry().indexOf(en);
    }
    return -1;
  }
  

  

  
  
  
  public G3RefT getG3Ref(Object dataAttribute) {
    
    int pos = findEntryByAttribute(dataAttribute.toString());
    if (pos >= 0) {
      
      return root.getEntry().get(pos).getG3Ref();
    }
       
    return null;
  }
  
  @Override
  public MultiStatus validate(EObject root){
    

    HashMap<String, EObject> list = TemplateFunction.getListWithAttr(root);

    MultiStatus ms = new MultiStatus("g3.iec61850.gui", IStatus.ERROR, 
    					"Missing references on mandatory fields.", 
    					new Throwable("References not found on"));
    
    for (Map.Entry<String, EObject> entry : list.entrySet()) {
      // find DO which are mandatory
      if(entry.getValue() instanceof TDO || entry.getValue() instanceof TSDO) {
        if(ValidationMandatory.checkMandatory(entry.getValue())){  
          // iterate again to find the DA
          Iterator<String> itr = list.keySet().iterator();
          while (itr.hasNext()) {
            String attrval = itr.next();
            // check key with DO name
            if(attrval.startsWith(entry.getKey()+".")) {
              // check if its mandatory field and whether it is mapped
              if (ValidationMandatory.checkMandatory(list.get(attrval))){
                if (findEntryByAttribute(attrval) < 0){
                  // in the event of mapping not found - add to status report
                  Status status = new Status(IStatus.ERROR, "g3.iec61850.gui", "Path: " +attrval);
                  ms.add(status);
                }
              }
            }
          }
        }        
      }
    }
    
    System.err.println("Error length:" + ms.getChildren().length);
    return ms;
    
  }
  

  


  

  


}

