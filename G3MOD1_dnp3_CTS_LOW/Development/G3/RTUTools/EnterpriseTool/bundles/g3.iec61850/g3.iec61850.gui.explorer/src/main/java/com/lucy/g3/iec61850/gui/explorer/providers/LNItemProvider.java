
package com.lucy.g3.iec61850.gui.explorer.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ITableItemLabelProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.services.IServiceLocator;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;

import com.lucy.g3.iec61850.gui.explorer.parts.TreeViewPart;
import com.lucy.g3.iec61850.gui.explorer.services.LookUpDataTemplate;
import com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT;
import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute;
import com.lucy.g3.iec61850.model.scl.TDO;
import com.lucy.g3.iec61850.model.scl.TDataTypeTemplates;
import com.lucy.g3.iec61850.model.scl.TLN;
import com.lucy.g3.iec61850.model.scl.TLNodeType;
import com.lucy.g3.iec61850.model.scl.provider.TLNItemProvider;

public class LNItemProvider extends TLNItemProvider implements ITableItemLabelProvider {

  public LNItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }
  
  @Override
  public Object getBackground(Object object, int columnIndex){
    return new Color(Display.getDefault(), 0xFF, 0xFF, 0xDD);
  }
  
  @Override
  public String getText(Object object) {
    TLN ln = ((TLN)object);
    if (ln.getLnClass() == null &&  ln.getDesc() == null)
      return "New Logical Node";
    String inst = (ln.getInst() == null) ? "Inst" : ln.getInst();
    String label = "[" + (ln.getLnClass() != null ? ln.getLnClass().toString() : "LnClass?") + "-" + inst +  "]" ;
    return label + " " + ln.getDesc();
  }
  
  @Override
  protected void addLnClassPropertyDescriptor(Object object) {
    ItemPropertyDescriptor pd = new LNClassPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TLN_lnClass_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TLN_lnClass_feature", "_UI_TLN_type"),
         SCLPackage.eINSTANCE.getTLN_LnClass(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null);
    
    
    itemPropertyDescriptors.add(pd);
    
  }
  
  @Override
  protected void addLnTypePropertyDescriptor(Object object) {
    ItemPropertyDescriptor pd = new LNTypeIDPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TAnyLN_lnType_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TAnyLN_lnType_feature", "_UI_TAnyLN_type"),
         SCLPackage.eINSTANCE.getTAnyLN_LnType(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null);
    
    
    itemPropertyDescriptors.add(pd);
    
  }

  class LNClassPropertyDescriptor extends ItemPropertyDescriptor {

    public LNClassPropertyDescriptor(AdapterFactory adapterFactory, ResourceLocator resourceLocator, String displayName,
        String description, EStructuralFeature feature, boolean isSettable, boolean multiLine, boolean sortChoices,
        Object staticImage, String category, String[] filterFlags) {
      super(adapterFactory, resourceLocator, displayName, description, feature, isSettable, multiLine, sortChoices,
          staticImage, category, filterFlags);
    }
    
    @Override 
    public Collection<?> getChoiceOfValues(Object object)
    {
      LookUpDataTemplate ldt = LookUpDataTemplate.getInstance();
      List<String> lnclass =  ldt.getLNClasses();
      
      Collection<?> parent = super.getChoiceOfValues(object);
      
      // adding user defined classes?
      
      return lnclass;
    }
    
  }
  class LNTypeIDPropertyDescriptor extends ItemPropertyDescriptor {

    public LNTypeIDPropertyDescriptor(AdapterFactory adapterFactory, ResourceLocator resourceLocator, String displayName,
        String description, EStructuralFeature feature, boolean isSettable, boolean multiLine, boolean sortChoices,
        Object staticImage, String category, String[] filterFlags) {
      super(adapterFactory, resourceLocator, displayName, description, feature, isSettable, multiLine, sortChoices,
          staticImage, category, filterFlags);
    }
    
    @Override 
    public Collection<?> getChoiceOfValues(Object object)
    {
      List<String> lnlist = new ArrayList<String>();
      try {
        EObject root = EcoreUtil.getRootContainer((EObject)object);
        
        TDataTypeTemplates dt = (TDataTypeTemplates) EcoreUtil.getEObject(root, "@sCL/@dataTypeTemplates");
        
        for(TLNodeType nt : dt.getLNodeType())
          lnlist.add(nt.getId());
        return lnlist;
      } catch (Exception ex) {
        System.err.println("LNTypeID: autofill fail");
        return super.getChoiceOfValues(object);
      }

    }
  
  }
    

}

