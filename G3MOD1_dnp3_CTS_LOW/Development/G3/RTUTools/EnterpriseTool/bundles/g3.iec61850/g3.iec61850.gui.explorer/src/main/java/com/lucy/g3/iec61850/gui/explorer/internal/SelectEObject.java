
package com.lucy.g3.iec61850.gui.explorer.internal;

import org.eclipse.emf.ecore.EObject;

public class SelectEObject {
  
  EObject object;
  String path;
  
  public SelectEObject(EObject ob, String path){
    object = ob;
    this.path = path;
  }
  
  public EObject getObject() {
    return object;
  }
  
  public void setObject(EObject object) {
    Object oldValue = this.object;
    this.object = object;
  }
  
  public String getPath() {
    return path;
  }
  
  public void setPath(String path) {
    Object oldValue = this.path;
    this.path = path;
  }
  
  @Override
  public String toString(){
    return path + " - " + object.getClass().getSimpleName();
  }
  
  public static SelectEObject _new(EObject object, String path){
    return new SelectEObject(object, path);
  }

  
}

