
package com.lucy.g3.iec61850.gui.explorer.handlers;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.lucy.g3.iec61850.gui.explorer.dialogs.GenerateDialog;
import com.lucy.g3.iec61850.gui.explorer.parts.TreeViewPart;
import com.lucy.g3.iec61850.gui.explorer.services.IMappingService;

public class SaveFile {
  
  @Inject
  private IMappingService mappingService;
  
  @Execute
  public void execute(EPartService partService, Shell shell) {
    // TODO Auto-generated method stub
    

    
    
    File file = new File("C:/repobox/G3Software/Development/G3/RTUTools/bundles/g3.iec61850/g3.iec61850.gui/test.xml");
    MPart part = partService.findPart(TreeViewPart.TreeViewPart_ID);
    
    TreeViewPart scl = (TreeViewPart)part.getObject();
    
    ResourceSet set = TreeViewPart.editingDomain.getResourceSet();
    
    
    
//    scl.init();
    
    
    EObject root = (EObject)scl.viewer.getInput();
    
    
    
    GenerateDialog diag = GenerateDialog.open(shell);
    
    if (diag.open() == Dialog.OK) {
    	IStatus status = null;
      try {
    	  status = mappingService.validate(root);
        if(!status.isOK()) {
        	throw new IllegalStateException("Validation failed", status.getException());
        }
        
        Resource rs = set.createResource(URI.createFileURI(diag.getResourceFile().getAbsolutePath()));
        rs.getContents().add((EObject)scl.viewer.getInput());
        
        // Conformance Statement I114 (utf-8 encoding)
        Map<String, String> options = new HashMap<String, String>();
        options.put(XMLResource.OPTION_ENCODING, "UTF-8");
        
        rs.save(options);
        mappingService.save(diag.getMappingFile());
        
        MessageDialog.openInformation(shell, "Done", "ICD file has been generated successfully");
        
      } catch (IllegalStateException e) {
    	  if (status == null)
    	    status = mappingService.validate(root);
        ErrorDialog.openError(shell, "Generation Failed", e.getMessage(), status);
        
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    
    
    try {
/*      Map<Object, Object> saveOptions = new HashMap<Object, Object>();
      saveOptions.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);
      EList<Resource> list =set.getResources(); 
      
      list.get(0).getContents().clear();
      list.get(0).getContents().add((EObject)scl.viewer.getInput());
      
      list.get(0).save(saveOptions);*/
      
//      set.get
      
//      Map<Object, Object> saveOptions = new HashMap<Object, Object>();
//      saveOptions.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);
//
//      rs.save(saveOptions);
      
      
      
      
      
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
//    scl.viewer.setInput(rs.getContents().get(0));
    
    
  }
  
  
  @CanExecute
  public boolean canExecute(final IEclipseContext ictx) {
    
      
    
    return true;
  }
}

