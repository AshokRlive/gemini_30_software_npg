
package com.lucy.g3.iec61850.gui.explorer.parts;

import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.internal.E4PartWrapper;

import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.PropertySource;
import org.eclipse.emf.edit.ui.view.ExtendedPropertySheetPage;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.IPropertySourceProvider;
import org.eclipse.ui.views.properties.PropertySheet;
import org.eclipse.ui.views.properties.PropertySheetPage;

import com.lucy.g3.iec61850.gui.explorer.internal.SelectEObject;
import com.lucy.g3.iec61850.gui.explorer.properties.CustomPropertySource;
import com.lucy.g3.iec61850.gui.explorer.properties.CustomPropertySourceProvider;

public class CustomPropertiesView {

  ExtendedPropertySheetPage propertySheetPage;

  private EObject selectedObject;
  
  public static String ID = "g3.iec61850.gui.explorer.part.properties";

  @Inject
  MApplication app;
  
  @Inject  EPartService partService;

  private Composite parent;


  public CustomPropertiesView() {
    // super();
  }

  public void recreateUI() {

    parent.redraw();


  }

  @PostConstruct
  public void createComposite(Composite parent) {
    this.parent = parent;

  }

  public IPropertySheetPage getPropertySheetPage(AdapterFactory adapterFactory) {
    
    propertySheetPage = new ExtendedPropertySheetPage(TreeViewPart.editingDomain) {

          @Override
          public void setSelectionToViewer(List<?> selection) {
            // this.setSelectionToViewer(selection);
            final Collection<?> theSelection = selection;
            // Make sure it's okay.
            //
            if (theSelection != null && !theSelection.isEmpty()) {
              System.out.println("SelectionToViewer" + selection.size());
              // getSite().getShell().getDisplay().asyncExec(runnable);
            }
            this.setFocus();
          }

          @Override
          public void setActionBars(IActionBars actionBars) {
            super.setActionBars(actionBars);
            // getActionBarContributor().shareGlobalActions(this, actionBars);
          }
        };
    propertySheetPage.setPropertySourceProvider(new CustomPropertySourceProvider(adapterFactory, selectedObject));
    // propertySheetPages.add(propertySheetPage);
    for (Control control : parent.getChildren()) {
      control.dispose();
    }
    propertySheetPage.createControl(parent);

    parent.layout();
    

    return propertySheetPage;
  }

  @Inject
  public void setsObject(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) Object ob) {
    if (ob != null) {
      
      if (ob instanceof EObject)
        selectedObject = (EObject) ob;
      
      if (ob instanceof SelectEObject)
        selectedObject = ((SelectEObject)ob).getObject();
      
      MPart p = partService.findPart(CustomPropertiesView.ID);
      E4PartWrapper e = E4PartWrapper.getE4PartWrapper(p);
      
      if (propertySheetPage != null)
        propertySheetPage.selectionChanged(e.getSite().getPart(), new StructuredSelection(ob));

    }
  }
  


}
