
package com.lucy.g3.iec61850.gui.explorer.providers;

import java.util.ArrayList;

import org.apache.commons.lang3.ArrayUtils;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.ui.views.properties.IPropertySource;

import com.lucy.g3.iec61850.gui.explorer.parts.DetailPart;
import com.lucy.g3.iec61850.gui.explorer.properties.CustomPropertySource;
import com.lucy.g3.iec61850.model.scl.impl.DocumentRootImpl;

public class ExplorerContentProvider extends AdapterFactoryContentProvider{
  public ExplorerContentProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }
  

  
  public Object getParent(Object object, boolean templates){
    
    return null;
  }
  
  @Override
  public Object[] getElements(Object object) {
    
    
    if (object instanceof DocumentRootImpl) {
      DocumentRootImpl doc = (DocumentRootImpl) object;
      ArrayList<Object> list = new ArrayList<Object>();
      list.add(doc.getCommunication());
      list.add(doc.getIED());
      list.add(doc.getDataTypeTemplates());
      return super.getElements(doc.getSCL());
      
    }
    
    
    return super.getElements(object);      
  }
  
  @Override
  public Object[] getChildren(Object object){
    
    ITreeItemContentProvider treeItemContentProvider = 
        (ITreeItemContentProvider)adapterFactory.adapt(object, ITreeItemContentProvider.class);
    
    Object[] children = super.getChildren(object);
    
    
    Object[] tempinputs = DetailPart.checkTemplates(object);
    
    
    return ArrayUtils.addAll(children, tempinputs);
  
  }
  
  
  public boolean hasChildren(Object object)
  {
    // Get the adapter from the factory.
    //
    ITreeItemContentProvider treeItemContentProvider = 
      (ITreeItemContentProvider)adapterFactory.adapt(object, ITreeItemContentProvider.class);
    
    
    if (treeItemContentProvider != null) {
      boolean hc = treeItemContentProvider.hasChildren(object);
      
      if (!hc) {
        if (DetailPart.checkTemplates(object).length > 0){
          hc = true;
          System.out.println(object.getClass().getName());
        }
          
      }   
      return hc;
      
    }
    
    return false;
  }
  
  protected IPropertySource createPropertySource(Object object, IItemPropertySource itemPropertySource) {
    return new CustomPropertySource(adapterFactory, object, itemPropertySource);
  }
}

