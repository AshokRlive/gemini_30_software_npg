
package com.lucy.g3.iec61850.gui.explorer.services;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.HashMap;
import java.util.Scanner;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EObject;
import org.osgi.framework.Bundle;

import com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute;
import com.lucy.g3.iec61850.model.scl.TDO;
import com.lucy.g3.iec61850.model.scl.TDOType;
import com.lucy.g3.iec61850.model.scl.TLNodeType;
import com.lucy.g3.iec61850.model.scl.impl.TDOImpl;

public class ValidationMandatory {
  
  private static String FILE_MANDATORY = "MandatoryValidation";
  
  private static ValidationMandatory instance;
  
  HashMap<String, String> reqFieldIndex = new HashMap<String, String>();
  
  protected ValidationMandatory(){
    load();
  }
  
  public boolean checkMandatory(String key){
    return reqFieldIndex.get(key) != null ? reqFieldIndex.get(key).equals("M") : false;
  }
  
  private void load() {
    Bundle bundle = Platform.getBundle("g3.iec61850.gui");
    
    if (!reqFieldIndex.isEmpty())
      return;
    
    URL url = bundle.getEntry("/src/main/resources/" + FILE_MANDATORY);
    URL fileURL;
    try {
      fileURL = FileLocator.toFileURL(url);
      File file = new File(fileURL.getFile());
      Scanner scanner = new Scanner(file);
      while (scanner.hasNext()) {
        String line = scanner.next();
        
        int last = line.lastIndexOf("$")+1;
        String id = line.substring(0, last);
        String val = line.substring(last);
        reqFieldIndex.put(id, val);
        
      }
      scanner.close();
      
      
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public static boolean checkMandatory(EObject ob) {
    
    ValidationMandatory vm = ValidationMandatory.getInstance();
    
    String find = parseObjectPath(ob, "");
      
    return vm.checkMandatory(find);
    
//    return "";
    
  }
  
  public static ValidationMandatory getInstance(){
    if (instance == null)
      instance = new ValidationMandatory();
    return instance;
  }
  
  protected static String parseObjectPath(EObject eo, String str)  {
    
    if(eo instanceof TAbstractDataAttribute){
      TAbstractDataAttribute da = (TAbstractDataAttribute) eo;
      str = "DA$" + da.getName() + "$" + str;
    }
    
    if(eo instanceof TDO) {
      TDO dob = (TDO) eo;
      str = "DO$" + dob.getName() + "$" + str;
    }
    
    if(eo instanceof TDOType) {
      TDOType dt = (TDOType) eo;
      str = "DOType$" + dt.getCdc() + "$" + str;
    }
    
    if (eo instanceof TLNodeType){
      TLNodeType nt = (TLNodeType) eo;
      str = "LNodeType$" + nt.getLnClass() + "$" + str;
    }
    
    EObject parent = eo.eContainer();
    if (parent != null) {
        str = parseObjectPath(parent, str) ;
    }
    return str;
  }
}

