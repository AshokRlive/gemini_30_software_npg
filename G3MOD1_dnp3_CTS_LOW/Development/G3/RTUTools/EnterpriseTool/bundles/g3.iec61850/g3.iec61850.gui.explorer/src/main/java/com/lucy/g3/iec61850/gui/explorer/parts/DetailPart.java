
package com.lucy.g3.iec61850.gui.explorer.parts;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.ArrayUtils;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxViewerCellEditor;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.internal.E4PartWrapper;
import org.eclipse.ui.part.WorkbenchPart;

import com.lucy.g3.iec61850.gui.explorer.adaptor.SCLProviderAdapterFactory;
import com.lucy.g3.iec61850.gui.explorer.internal.G3ReferenceCellEditor;
import com.lucy.g3.iec61850.gui.explorer.internal.SelectEObject;
import com.lucy.g3.iec61850.gui.explorer.providers.DetailedLabelProvider;
import com.lucy.g3.iec61850.gui.explorer.services.IMappingService;
import com.lucy.g3.iec61850.gui.explorer.services.LookUpDataTemplate;
import com.lucy.g3.iec61850.gui.gemini3rtu.parts.G3ConfigPart;
import com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage;
import com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute;
import com.lucy.g3.iec61850.model.scl.TAnyLN;
import com.lucy.g3.iec61850.model.scl.TDA;
import com.lucy.g3.iec61850.model.scl.TDAType;
import com.lucy.g3.iec61850.model.scl.TDO;
import com.lucy.g3.iec61850.model.scl.TDOType;
import com.lucy.g3.iec61850.model.scl.TDataTypeTemplates;
import com.lucy.g3.iec61850.model.scl.TLNodeType;
import com.lucy.g3.iec61850.model.scl.TSDO;

public class DetailPart  {
  private TreeViewer viewer;
  
  @Inject
  private IMappingService service;
  
  @Inject
  private EModelService modelService;
  
  @Inject ESelectionService detailSelection;
  
  @Inject EPartService partService;
  
  
  @Inject private MApplication app;
  
  private SelectEObject selectedEObject;
  
  public static String DETAIL_PART_ID = "g3.iec61850.gui.explorer.part.detailed";
  
  protected ComposedAdapterFactory adapterFactory;
  
  protected TreeViewerColumn refColumn;
  
  
  @PostConstruct
  public void createComposite(Composite parent) {
    viewer = new TreeViewer(parent, SWT.FULL_SELECTION | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
    
    init();
    
    
    
    Tree tree = viewer.getTree();
    tree.setLayoutData(new FillLayout());
    tree.setHeaderVisible(true);
    tree.setLinesVisible(true);

    TreeViewerColumn objectColumn = new TreeViewerColumn(viewer, SWT.NONE);
    objectColumn.getColumn().setText("Name");
    objectColumn.getColumn().setResizable(true);
    objectColumn.getColumn().setWidth(100);


    TreeViewerColumn selfColumn = new TreeViewerColumn(viewer, SWT.NONE);
    selfColumn.getColumn().setText("Desc");
    selfColumn.getColumn().setResizable(true);
    selfColumn.getColumn().setWidth(100);
    
    
    refColumn = new TreeViewerColumn(viewer, SWT.NONE);
    refColumn.getColumn().setText("G3 Reference");
    refColumn.getColumn().setResizable(false);
    refColumn.getColumn().setWidth(0);
    
    selfColumn = new TreeViewerColumn(viewer, SWT.NONE);
    selfColumn.getColumn().setText("Type");
    selfColumn.getColumn().setResizable(true);
    selfColumn.getColumn().setWidth(100);    
    
    selfColumn = new TreeViewerColumn(viewer, SWT.NONE);
    selfColumn.getColumn().setText("Optional");
    selfColumn.getColumn().setResizable(true);
    selfColumn.getColumn().setWidth(100);    
  
    viewer.setColumnProperties(new String [] {"n", "d", "vp", "t", "opt"});
    
//    viewer.setCellEditors(new CellEditor[]{null, null, null, null, new TextCellEditor(viewer.getTree()) });
    
    
    
    viewer.setContentProvider(new DetailedContentProvider(adapterFactory));
    viewer.setLabelProvider(new DetailedLabelProvider(adapterFactory));
    
    refColumn.setEditingSupport(new EditingSupport(viewer) {

      @Override
      protected void setValue(Object element, Object value) {
        
        if (element instanceof TAbstractDataAttribute)
        {
          if (value.toString().length() <= 0)
            return;
          TAbstractDataAttribute data = (TAbstractDataAttribute)element;
          
          EClass eclass = IEC61850MappingPackage.eINSTANCE.getG3RefT();
          
          G3RefT ref = (G3RefT) EcoreUtil.create(eclass);
          ref.setId(new BigInteger("0"));
          ref.setUuid(UUID.randomUUID().toString());
          // right now parse string
          // it could be dialog or dnd
          String str = value.toString().trim();          
          String[] parts = str.split(",");
          switch (parts.length) {
          case 1:
            try {
              Integer.parseInt(str);
              ref.setGroup(new BigInteger(str));
              
            } catch (NumberFormatException e) {
              System.err.println("Cannot parse integer from '" + str + "'");
              return;
            }
            break;
          case 2:
            try {
              Integer.parseInt(parts[0].trim());
              Integer.parseInt(parts[1].trim());
              ref.setGroup(new BigInteger(parts[0].trim()));
              ref.setId(new BigInteger(parts[1].trim()));
            } catch (NumberFormatException e) {
              System.err.println("Cannot parse integers from array '" + parts + "'");
              return;
            }
            break;
          default:   
            
            
          }
          String attr = selectedEObject.getPath() +"."+data.getName() ;
                    
          service.addEntry(attr, ref, data.getBType());
//          data.setVirtualPoint(value.toString());
          
//          viewer.update(data, null);
//          return;
          
//          data.value = value.toString();
        }
        viewer.update(element, null);
      }
      
      @Override
      protected String getValue(Object element) {
        if (element instanceof TAbstractDataAttribute){
          TAbstractDataAttribute da = (TAbstractDataAttribute) element;
          
          String attr = selectedEObject.getPath() +"."+da.getName();
                    
          G3RefT ref = service.getG3Ref(attr);
          
          if (ref != null)
            return ref.getGroup().toString() + " , " + ref.getId().intValue();
        }
        
        return "";
      }
      
      @Override
      protected CellEditor getCellEditor(Object element) {
        G3ReferenceCellEditor diag = new G3ReferenceCellEditor(viewer.getTree());

        
        return new TextCellEditor(viewer.getTree());
      }
      
      @Override
      protected boolean canEdit(Object element) {
        if (element instanceof TAbstractDataAttribute)
          return true;
        return false;
      }
    });
    
    viewer.addSelectionChangedListener(new ISelectionChangedListener() {

      @Override
      public void selectionChanged(SelectionChangedEvent event) {
        // TreeViewPart.this.showView("org.eclipse.ui.views.PropertySheet");
        // System.out.println(o.getClass().getName());
        IStructuredSelection selection = viewer.getStructuredSelection();
        
//        CustomPropertiesView cp = app.getContext().get(CustomPropertiesView.class);
        
//        cp.propertySheetPage.setPropertySourceProvider(new AdapterFactoryContentProvider(adapterFactory));
        
        
//        SelObject ob = new SelObject("", selection.getFirstElement());
        

        
        
        MUIElement elem = modelService.find(CustomPropertiesView.ID, app);
        
        MPart pp = (MPart) elem;
            
        CustomPropertiesView cp = (CustomPropertiesView)pp.getObject();
        cp.getPropertySheetPage(adapterFactory);
        pp.setObject(cp);
        String attr = "";
        if (selection.getFirstElement() instanceof TAbstractDataAttribute){
          TAbstractDataAttribute da = (TAbstractDataAttribute) selection.getFirstElement();
          attr = selectedEObject.getPath()+"."+da.getName();
//          da.setPath(attr);
          detailSelection.setSelection(SelectEObject._new((EObject)da, attr));
        }
               
//        


      }
    });
    
    

    
    
    
    
  }
  
  
  private TreePath getSelectedTreePath() {
    MPart part = partService.findPart(TreeViewPart.TreeViewPart_ID);
    TreeViewPart exp = (TreeViewPart) part.getObject();
    return (exp != null) ? exp.getCurrentTreePath() : null;
  }

  private void init(){

    initDomain();
  }
  
  
  public void initDomain() {
    adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
    adapterFactory.addAdapterFactory(new SCLProviderAdapterFactory());
    

  }
  



  
  @Inject
  public void setObject(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) Object ob,
      @Optional @Named(IServiceConstants.ACTIVE_PART) MPart part) {
    if (part == null || part.getObject() instanceof DetailPart || viewer == null)
      return;
    
    
    if (part.getObject() instanceof TreeViewPart && ob != null) {
      if (!(ob instanceof SelectEObject))
        return;
      selectedEObject = (SelectEObject)ob;
      viewer.setInput(selectedEObject.getObject());
      
      if (selectedEObject.getObject() instanceof TDO || selectedEObject.getObject() instanceof TSDO) {
        refColumn.getColumn().setResizable(true);
        refColumn.getColumn().setWidth(100);
      } else {
        refColumn.getColumn().setResizable(false);
        refColumn.getColumn().setWidth(0);
      }
      
//      viewer.getContentProvider().inputChanged(viewer, viewer.getInput(), ob);
      
    }
    
    
    
    if (part.getElementId().equals(G3ConfigPart.ID) && ob != null) {

      
      IStructuredSelection sel = (IStructuredSelection) viewer.getSelection();
      Object pob = sel.getFirstElement();
      
      if (pob instanceof TAbstractDataAttribute && ob instanceof G3RefT) {
        TAbstractDataAttribute da = (TAbstractDataAttribute) pob;
        
        String attr = selectedEObject.getPath()+"."+da.getName();
        
        service.addEntry(attr, EcoreUtil.copy((G3RefT)ob), da.getBType());
        
      }
      
      viewer.refresh();
      
    }
    
  }
  

  
  
  class Detailed2LabelProvider extends AdapterFactoryLabelProvider implements ITableLabelProvider {

    public Detailed2LabelProvider(AdapterFactory adapterFactory) {
      super(adapterFactory);
    }
    
    @Override
    public String getColumnText(Object ob, int index){
      System.out.println("DetailedLabelProvider: "+index+ob.getClass().getName());
      switch (index) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        break;
      case 3:
        return "";
      case 4:
        return "";        
      }
      return super.getColumnText(ob, index);
    }
    
  }

  class DetailedContentProvider extends AdapterFactoryContentProvider {

    public DetailedContentProvider(AdapterFactory adapterFactory) {
      super(adapterFactory);
    }
    

    
    @Override
    public boolean hasChildren(Object object) {
      
      ITreeItemContentProvider treeItemContentProvider = 
          (ITreeItemContentProvider)adapterFactory.adapt(object, ITreeItemContentProvider.class);
      
//      System.out.println(this.getClass().getName() + ": " + object.getClass().getName());
      
        if (treeItemContentProvider != null) {
          boolean hc = treeItemContentProvider.hasChildren(object);
          
          if (!hc) {
            if (checkTemplates(object).length > 0){
              hc = true;
            }
              
          }   
          return hc;
          
        }
        
        return false;
    }
    
    @Override
    public Object[] getElements(Object object)
    {
      // Get the adapter from the factory.
      //
      ITreeItemContentProvider treeItemContentProvider = 
          (ITreeItemContentProvider)adapterFactory.adapt(object, ITreeItemContentProvider.class);
      
      Object[] children = super.getElements(object);
      
      
      Object[] tempinputs = checkTemplates(object);

      // Either delegate the call or return nothing.
      //
      return ArrayUtils.addAll(children, tempinputs);
    }
    
    @Override
    public Object[] getChildren(Object object){
      
      ITreeItemContentProvider treeItemContentProvider = 
          (ITreeItemContentProvider)adapterFactory.adapt(object, ITreeItemContentProvider.class);
      
      Object[] children = super.getChildren(object);
      
      
      Object[] tempinputs = checkTemplates(object);
      
      
      return ArrayUtils.addAll(children, tempinputs);
    }
    
    protected Object[] tableContent(Object[] objects){
             
      return null;
    }
    
   
  }
  
  public static Object[] checkTemplates(Object object) {
    ResourceSet set = TreeViewPart.editingDomain.getResourceSet();
    
    Resource rs = set.getResources().get(0);
    
    TDataTypeTemplates temp = (TDataTypeTemplates) rs.getEObject("//@sCL/@dataTypeTemplates");
    
    if (temp == null)
      return new Object[] {};

    
    if (object instanceof TAnyLN){
     
      TAnyLN ln = (TAnyLN) object;
      
      if (temp.getLNodeType() == null || ln.getLnType() == null)
        return new Object[] {};
      
      for (TLNodeType nt : temp.getLNodeType()){

        if (nt.getId().contains(ln.getLnType())){
          
          return nt.getDO().toArray();
        }
      }
    }
    
    if (object instanceof TDO){
      
      TDO dob = (TDO) object;
      if (temp.getDOType() == null || dob.getType() == null)
        return new Object[] {};
      for (TDOType nt : temp.getDOType()){

        if (nt.getId().contains(dob.getType())){
          Object[] sdo = nt.getSDO().toArray();
          Object[] da = ArrayUtils.addAll(sdo, nt.getDA().toArray());
          Object[] prv = ArrayUtils.addAll(da, nt.getPrivate().toArray());
          
          return prv;
        }
      }
    }
    
    if (object instanceof TAbstractDataAttribute){
      
      TAbstractDataAttribute da = (TAbstractDataAttribute) object;
      
      if (da.getType() == null)
        return new Object[] {};
      
      for (TDAType nt : temp.getDAType()){
        
        if(!da.getBType().getLiteral().equals("Struct"))
          continue;
        
        if (nt.getId().contains(da.getType())){
          Object[] bda = nt.getBDA().toArray();
          
          Object[] prv = ArrayUtils.addAll(bda, nt.getPrivate().toArray());
          
          return prv;
        }
      }
      
    }
    
    


    return new Object[] {};
  }
  
  
  
}

