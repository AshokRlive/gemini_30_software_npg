
package com.lucy.g3.iec61850.gui.explorer.services;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.osgi.framework.Bundle;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TDAType;
import com.lucy.g3.iec61850.model.scl.TDO;
import com.lucy.g3.iec61850.model.scl.TDOType;
import com.lucy.g3.iec61850.model.scl.TEnumType;
import com.lucy.g3.iec61850.model.scl.TLNodeType;
import com.lucy.g3.iec61850.model.scl.util.SCLResourceFactoryImpl;
import com.lucy.g3.iec61850.scl.internal.ResourceUtils;

public class LookUpDataTemplate {

  private static String FILE_IEC61850Ed2_Template = "IEC61850_LookupTemplate.xml";
  
  public Document doc;
  
  static LookUpDataTemplate inst;
  
  XPath xpath;
  
  private static EObject root;
  
  
  protected LookUpDataTemplate(){
    // load();
    resourceLoad();
  }
  
  //singleton approach
  public static LookUpDataTemplate getInstance(){
    if (inst == null)
      inst = new LookUpDataTemplate();
    return inst;
  }
  
  
  private void resourceLoad(){
    try {

      Bundle bundle = Platform.getBundle("g3.iec61850.gui");
            
      URL url = bundle.getEntry("/src/main/resources/" + FILE_IEC61850Ed2_Template);
      URL fileURL = FileLocator.toFileURL(url);
      

      File file = new File(fileURL.getFile());
      System.out.println(file);
      root = ResourceUtils.loadObject(file);
      
      
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  private void load(File file) {
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    
    XPathFactory xPathfactory = XPathFactory.newInstance();
    xpath = xPathfactory.newXPath();
    

    dbf.setNamespaceAware(true);
    dbf.setValidating(false);
    
    try {
      DocumentBuilder db = dbf.newDocumentBuilder();
      doc = db.parse(file);
      doc.getDocumentElement().normalize();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
  }
  
  
  public ArrayList<String> getLNClasses() {
    ArrayList<String> cls = new ArrayList<String>();
    
    TreeIterator<EObject> it = root.eAllContents();
    
    while(it.hasNext()) {
      EObject ths = it.next();
      
      if (ths instanceof TLNodeType) {
        cls.add(((TLNodeType)ths).getId());
      }
    }
    return cls;
  }
  
  public NodeList getRdc(String type){
    
    List<Node> ls = new ArrayList<Node>();
    
    try {
      XPathExpression expr = xpath.compile("//*[@id='"+type+"']");
      
      NodeList list = (NodeList) expr.evaluate(doc,XPathConstants.NODESET);
      
      for(int i = 0; i < list.getLength(); i++){
        Node n = list.item(i);
        
        System.out.println(n);
      }
      System.out.println(list.getClass());
      
      
    } catch (XPathExpressionException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    return null;
    
  }
  
  public void ab (Node elem) {
    
    NodeList ls = elem.getChildNodes();
    
    if (ls.getLength() <= 0)
      return;
    
    for(int i = 0; i < ls.getLength(); i++){
      Node dotype = ls.item(i);
      
      if (dotype.getNodeType() != Node.ELEMENT_NODE)
        continue;
      Node type = dotype.getAttributes().getNamedItem("type");
      
      if (type == null)
        continue;
      
      Element e = getNodeWithAttribute(doc, "id",type.getNodeValue());
      
      e.getNodeType();
      
      
      // find dotype by type
//      for (int j = 0; j < 0; j++){
//        
//      }
    }
  }
  
  public EObject getDOType(String type){
    TreeIterator<EObject> it = root.eAllContents();
    TDOType node = null;
    
    while(it.hasNext()) {
      EObject ths = it.next();
      
      if (ths instanceof TDOType) {
        TDOType temp = (TDOType) ths;
        if (temp.getId().equals(type)){
          node = EcoreUtil.copy(temp);
          break;
        }          
      }
    }
    return node;
  }
  
  public EObject getLNByClass(String lnClass) {
    TreeIterator<EObject> it = root.eAllContents();
    TLNodeType node = null;
    
    while(it.hasNext()) {
      EObject ths = it.next();
      
      if (ths instanceof TLNodeType) {
        TLNodeType temp = (TLNodeType) ths;
        if (temp.getId().equals(lnClass)){
          node = EcoreUtil.copy(temp);
          break;
        }          
      }
    }

    return node;
  }
  
  public EObject getDAType(String type) {
    TreeIterator<EObject> it = root.eAllContents();
    TDAType node = null;
    
    while(it.hasNext()) {
      EObject ths = it.next();
      
      if (ths instanceof TDAType) {
        TDAType temp = (TDAType) ths;
        if (temp.getId().equals(type)){
          node = EcoreUtil.copy(temp);
          break;
        }          
      }
    }

    return node;
  }
  
  public EObject getEnumType(String enm) {
    TreeIterator<EObject> it = root.eAllContents();
    TEnumType node = null;
    
    while(it.hasNext()) {
      EObject ths = it.next();
      
      if (ths instanceof TEnumType) {
        TEnumType temp = (TEnumType) ths;
        if (temp.getId().equals(enm)){
          node = EcoreUtil.copy(temp);
          break;
        }          
      }
    }

    return node;
  }
  
  public static Element getNodeWithAttribute(Node root, String attrName, String attrValue)
  {
      NodeList nl = root.getChildNodes();
      for (int i = 0; i < nl.getLength(); i++) {
          Node n = nl.item(i);
          if (n instanceof Element) {
              Element el = (Element) n;
              if (el.getAttribute(attrName).equals(attrValue)) {
                  return el;
              }else{
         el =  getNodeWithAttribute(n, attrName, attrValue); //search recursively
         if(el != null){
          return el;
         }
      }
          }
      }
      return null;
  }
  

  
  public static void main(String[] args) {
    initialiseResource();
    LookUpDataTemplate lk = LookUpDataTemplate.getInstance();
    
    lk.load(new File("C:/repobox/G3Software/Development/G3/RTUTools/bundles/g3.iec61850/g3.iec61850.gui/61850DTTEd2.xml"));
    
    NodeList nl = lk.doc.getElementsByTagName("DAType");
    
    for(int i = 0; i < nl.getLength(); i++){
      NodeList sub = nl.item(i).getChildNodes();
      if (nl.item(i).getNodeType() != Node.ELEMENT_NODE)
        continue;
      Node idNode = nl.item(i).getAttributes().getNamedItem("id");
      
      for (int j = 0; j < sub.getLength(); j++) {
        Node m = sub.item(j);
        if (m.getNodeType() != Node.ELEMENT_NODE)
          continue;
        Node name = m.getAttributes().getNamedItem("name");
        Node pre = m.getAttributes().getNamedItem("pre");
        if (pre == null || pre.getNodeValue().equals("O"))
          continue;
        System.out.println("DAType$" + idNode.getNodeValue() + "$" + m.getNodeName() +  "$" + name.getNodeValue() + "=" + pre.getNodeValue());
      }
    }
    
    
    
  }
  
  public static void initialiseResource() {
    ResourceSet resourceSet = new ResourceSetImpl();
    resourceSet
        .getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION,
            new SCLResourceFactoryImpl());
    resourceSet
        .getPackageRegistry().put(SCLPackage.eNS_URI,
            SCLPackage.eINSTANCE);
    
    
    ResourceUtils.setResourceManager(resourceSet);

  }
  
}

