
package com.lucy.g3.iec61850.gui.explorer.providers;

import javax.inject.Inject;

import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import com.lucy.g3.iec61850.gui.explorer.internal.SelectEObject;
import com.lucy.g3.iec61850.gui.explorer.parts.TreeViewPart;
import com.lucy.g3.iec61850.gui.explorer.services.IMappingService;
import com.lucy.g3.iec61850.gui.explorer.services.ValidationMandatory;
import com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT;
import com.lucy.g3.iec61850.model.g3ref.util.G3RefAdapterFactory;
import com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute;

public class DetailedLabelProvider extends AdapterFactoryLabelProvider implements ITableColorProvider{
  
  
  public DetailedLabelProvider(AdapterFactory adapterFactory)  {
    super(adapterFactory);
  }
  
  private boolean getG3Ref(Object object){
    TAbstractDataAttribute da = null;
    if(object instanceof TAbstractDataAttribute)
      da = (TAbstractDataAttribute)object;
    else
      return false;
    
    String ref = DAMappedItemProvider.getColumnTextForDA(da, 2);
    
    return ref == null || ref.isEmpty() ? false : true ;
  }
  
  
  @Override
  public Color getBackground(Object object, int columnIndex) {
    if (!(object instanceof EObject))
      return null;
    if (!ValidationMandatory.checkMandatory((EObject)object))
      return null;
    boolean ref = getG3Ref(object);
    if (columnIndex == 2) {
      if (ref)
        return Display.getCurrent().getSystemColor(SWT.COLOR_GRAY);
      else
        return Display.getCurrent().getSystemColor(SWT.COLOR_YELLOW);
    }
    return null;
  }

}

