
package com.lucy.g3.iec61850.gui.explorer.handlers;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.inject.Inject;
import javax.swing.JOptionPane;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.osgi.framework.Bundle;

import com.lucy.g3.iec61850.gui.explorer.parts.TreeViewPart;
import com.lucy.g3.iec61850.gui.explorer.services.IMappingService;
import com.lucy.g3.iec61850.model.scl.SCLType;

public class NewFile {
  
  private static String FILE_NewTemplate = "NewTemplate.xml";
  
  @Inject
  IMappingService mappingService;
  
  @Execute
  public void executeHandler(EPartService partService, Shell shell) {
    // Load From Template after user selects a folder
    Bundle bundle = Platform.getBundle("g3.iec61850.gui");    
    URL url = bundle.getEntry("/src/main/resources/" + FILE_NewTemplate);
    
    
    
    
    try {
      URL fileURL = FileLocator.toFileURL(url);
      File template = new File(fileURL.getFile());
      MPart part = partService.findPart(TreeViewPart.TreeViewPart_ID);
      TreeViewPart scl = (TreeViewPart)part.getObject();
      
      if(scl.viewer.getInput() != null){
        if(!MessageDialog.openConfirm(shell, "Save?", "Are you sure you want to discard the current file?"))
          return;
      }
      scl.init();
      Resource rs = TreeViewPart.editingDomain.getResourceSet().getResource(URI.createFileURI(template.getAbsolutePath()), true);

      scl.viewer.setInput(rs.getContents().get(0));      
      scl.viewer.reveal(SCLType.class);
      
      mappingService.create();
      
    } catch (IOException e) {
      
      
      e.printStackTrace();
    }
  }
  
  @CanExecute
  public boolean canExecute(final IEclipseContext ictx) {
    return true;
  }
}

