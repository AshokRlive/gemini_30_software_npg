
package com.lucy.g3.iec61850.gui.explorer.services;

import org.eclipse.e4.core.contexts.ContextFunction;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

public class MappingServiceContextFunction extends ContextFunction {

  @Override
  public Object compute(IEclipseContext context, String contextKey) {
    IMappingService service = ContextInjectionFactory.make(MappingServiceImpl.class, context);

    MApplication app = context.get(MApplication.class);
    IEclipseContext appCtx = app.getContext();
    appCtx.set(IMappingService.class, service);
    
 // in case the ITodoService is also needed in the OSGi layer, e.g.
    // by other OSGi services, register the instance also in the OSGi service layer
    Bundle bundle = FrameworkUtil.getBundle(this.getClass());
    BundleContext bundleContext = bundle.getBundleContext();
    bundleContext.registerService(IMappingService.class, service, null);

    return service;
  }
}



