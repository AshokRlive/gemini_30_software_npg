
package com.lucy.g3.iec61850.gui.explorer.internal;

import java.util.HashMap;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.lucy.g3.iec61850.model.scl.TAnyLN;
import com.lucy.g3.iec61850.model.scl.TBDA;
import com.lucy.g3.iec61850.model.scl.TDA;
import com.lucy.g3.iec61850.model.scl.TDAType;
import com.lucy.g3.iec61850.model.scl.TDO;
import com.lucy.g3.iec61850.model.scl.TDOType;
import com.lucy.g3.iec61850.model.scl.TDataTypeTemplates;
import com.lucy.g3.iec61850.model.scl.TLDevice;
import com.lucy.g3.iec61850.model.scl.TLN;
import com.lucy.g3.iec61850.model.scl.TLNodeType;
import com.lucy.g3.iec61850.model.scl.TSDO;

public class TemplateFunction {
  
  
  public static Object[] getListbyObject(Object object){
    
    EObject root = EcoreUtil.getRootContainer((EObject)object);
    TDataTypeTemplates templates = (TDataTypeTemplates) EcoreUtil.getEObject(root, "@sCL/@dataTypeTemplates");
    HashMap<String, EObject> list = new HashMap<String, EObject>();
    
    if (object instanceof TAnyLN) {
      TAnyLN ln = (TAnyLN) object;
      list = getTLNfromTemplate(root, ln.getLnType(), "");
    }
    
    if (object instanceof TDO){
      TDO dob = (TDO) object;
      list = getTDOfromTemplate(templates, dob.getType(), "");
    }
    
    if (object instanceof TDA){
      TDA da = (TDA) object;
      list = getTDAfromTemplate(templates, da.getType(), "");
    }
    
    return list.values().toArray(new Object[]{});
  }
  
  
  public static HashMap<String, EObject> getListWithAttr(EObject root) {
    HashMap<String, EObject> list = new HashMap<String, EObject>();
    TreeIterator<EObject> iter = EcoreUtil.getAllContents(root, true);
    
    // Find devices 
    while(iter.hasNext()) {
      EObject dev = iter.next();
      if(dev instanceof TLDevice) {
        list.putAll(getListByDevice(root, (TLDevice)dev));
      }
    }
    
    
    
    return list;
  }
  
  public static HashMap<String, EObject> getListByDevice(EObject root, TLDevice device) {
    HashMap<String, EObject> list = new HashMap<String, EObject>();
    String device_name = device.getLdName() + device.getInst() + "/";
    
    list.putAll(getTLNfromTemplate(root, device.getLN0().getLnType(), device_name+device.getLN0().getLnClass()));
    
    for(TLN ln : device.getLN()) {
      //find LNode Types
      String pre = device_name + ln.getLnClass() + ln.getInst();
      list.putAll(getTLNfromTemplate(root, ln.getLnType(), pre));    
      
    }
    
    return list;
    
  }

  
  public static HashMap<String, EObject> getTLNfromTemplate(EObject root, String lnType, String prefix){
    TDataTypeTemplates templates = (TDataTypeTemplates) EcoreUtil.getEObject(root, "@sCL/@dataTypeTemplates");
    HashMap<String, EObject> lnmap = new HashMap<String, EObject>();
    
    for(TLNodeType ntype :templates.getLNodeType()){
      if (ntype.getId().equals(lnType)){
        
        for(TDO dob : ntype.getDO()) {
          String pre = prefix + "." +dob.getName();
          lnmap.put(pre, dob);
          lnmap.putAll(getTDOfromTemplate(templates, dob.getType(), pre));
        }
      }      
    }   
    return lnmap;
  }
  
  public static HashMap<String, EObject> getTDOfromTemplate(TDataTypeTemplates templates, String id, String prefix){
    HashMap<String, EObject> domap = new HashMap<String, EObject>();
    
    for(TDOType dotype : templates.getDOType()) {
      if(dotype.getId().equals(id)) {
        for(TDA da : dotype.getDA()) {
          String pre = prefix +  "." +da.getName();
          if (da.getBType().equals("Struct")){
            domap.putAll(getTDAfromTemplate(templates, da.getType(), pre));
          } else {
            domap.put(pre, da);
          }
        }
        
        for(TSDO sdo : dotype.getSDO()) {
          String pre = prefix +  "." +sdo.getName();
          domap.put(pre, sdo);
          domap.putAll(getTDOfromTemplate(templates, sdo.getType(), pre));
        }
        
      }
    }

    return domap;
  }
  
  public static HashMap<String, EObject> getTDAfromTemplate(TDataTypeTemplates templates, String id, String prefix){
    HashMap<String, EObject> damap = new HashMap<String, EObject>();
    for(TDAType datype : templates.getDAType()){
      if(datype.getId().equals(id)) {
        for(TBDA bda : datype.getBDA()) {
          String pre = prefix+"." +bda.getName();
          if (bda.getBType().equals("Struct")){
            damap.putAll(getTDAfromTemplate(templates, bda.getType(), pre));
          } else {
            damap.put(pre, bda);
          }
          
        }
      }
    }
    return damap;
  }
}

