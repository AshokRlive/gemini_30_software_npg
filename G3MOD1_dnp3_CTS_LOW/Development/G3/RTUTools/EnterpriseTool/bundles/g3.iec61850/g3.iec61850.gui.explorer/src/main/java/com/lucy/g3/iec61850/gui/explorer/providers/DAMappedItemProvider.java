
package com.lucy.g3.iec61850.gui.explorer.providers;


import javax.inject.Inject;

import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ITableItemColorProvider;
import org.eclipse.emf.edit.provider.ITableItemLabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.services.IServiceLocator;

import com.lucy.g3.iec61850.gui.explorer.internal.SelectEObject;
import com.lucy.g3.iec61850.gui.explorer.parts.TreeViewPart;
import com.lucy.g3.iec61850.gui.explorer.services.IMappingService;
import com.lucy.g3.iec61850.gui.explorer.services.ValidationMandatory;
import com.lucy.g3.iec61850.model.IEC61850Mapping.G3RefT;
import com.lucy.g3.iec61850.model.scl.TAbstractDataAttribute;
import com.lucy.g3.iec61850.model.scl.provider.TDAItemProvider;

public class DAMappedItemProvider extends TDAItemProvider implements ITableItemLabelProvider, ITableItemColorProvider {
  
  SelectEObject selectedObject;
  
  @Inject
  ESelectionService selectionService;
  
  public DAMappedItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);   
  }
  
  
  @Override
  public String getColumnText(Object ob, int index){
    
    
    if (ob instanceof TAbstractDataAttribute) {
      TAbstractDataAttribute sel = (TAbstractDataAttribute) ob;
//      if (sel.getObject() instanceof TAbstractDataAttribute)
      return getColumnTextForDA(sel, index);
    }
    
    return super.getColumnText(ob, index);
  }
  
  
  public static String getColumnTextForDA(TAbstractDataAttribute object, int index) {
    IServiceLocator serviceLocator = PlatformUI.getWorkbench();
    IMappingService mappingService = serviceLocator.getService(IMappingService.class);
    ESelectionService selService = serviceLocator.getService(ESelectionService.class);
    
    SelectEObject sel = null;
    if (selService.getSelection(TreeViewPart.TreeViewPart_ID) instanceof SelectEObject) {
      sel = (SelectEObject) selService.getSelection(TreeViewPart.TreeViewPart_ID);
    }
    
    G3RefT ref = null;
    if (mappingService != null) {
//      ref = mappingService.getG3Ref(object.getPath());
      if (sel != null) {  
        ref = mappingService.getG3Ref(sel.getPath()+ "." +object.getName());
        if (ref == null)
          ref = mappingService.getG3Ref(sel.getPath());          
      }
    }
    
    
    switch (index) {
      case 0:
        return object.getName().toString();
      case 1:
        return object.getDesc();
      case 2:
        if (ref != null)
          return ref.getGroup().toString() + " , " + ref.getId().intValue();
        return "";        
      case 3:
        return object.getBType().toString();
      case 4:
        return ValidationMandatory.checkMandatory(object) ? "Mandatory" : "";

      default:
        return "";
    }
  }
  
  
}
