
package com.lucy.g3.iec61850.gui.explorer.parts;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventObject;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.ArrayUtils;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.advanced.MPlaceholder;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.impl.PartImpl;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.command.CommandStackListener;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.E4PartWrapper;
import org.eclipse.ui.internal.e4.compatibility.CompatibilityView;
import org.eclipse.ui.services.IServiceLocator;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.PropertySheet;
import org.eclipse.ui.views.properties.PropertySheetPage;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import com.lucy.g3.iec61850.gui.explorer.adaptor.SCLProviderAdapterFactory;
import com.lucy.g3.iec61850.gui.explorer.internal.SelectEObject;
import com.lucy.g3.iec61850.gui.explorer.properties.CustomPropertySource;
import com.lucy.g3.iec61850.gui.explorer.providers.ExplorerContentProvider;
import com.lucy.g3.iec61850.gui.explorer.services.IMappingService;
import com.lucy.g3.iec61850.model.g3ref.G3RefPackage;
import com.lucy.g3.iec61850.model.g3ref.TG3RefObject;
import com.lucy.g3.iec61850.model.internal.SCLObject;
import com.lucy.g3.iec61850.model.scl.SCLFactory;
import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TAnyLN;
import com.lucy.g3.iec61850.model.scl.TDO;
import com.lucy.g3.iec61850.model.scl.TLDevice;
import com.lucy.g3.iec61850.model.scl.TLN;
import com.lucy.g3.iec61850.model.scl.TLN0;
import com.lucy.g3.iec61850.model.scl.TPrivate;
import com.lucy.g3.iec61850.model.scl.impl.DocumentRootImpl;
import com.lucy.g3.iec61850.model.scl.util.SCLResourceFactoryImpl;
import com.lucy.g3.iec61850.scl.internal.ResourceUtils;

public class TreeViewPart {
  
  public static final String TreeViewPart_ID = "g3.iec61850.gui.explorer.part.sclexplorer";
  
  public TreeViewer viewer;

  
  static ResourceSet resourceSet;
  public static AdapterFactoryEditingDomain editingDomain;
  protected ComposedAdapterFactory adapterFactory;
  
  protected List<PropertySheetPage> propertySheetPages = new ArrayList<PropertySheetPage>();
  
  private static final Class<?>[] adapterList = { IPropertySheetPage.class };
  
  static boolean setLegacySelProvider = false;
  
//  protected Collection<ISelectionChangedListener> selectionChangedListeners = new ArrayList<ISelectionChangedListener>();
  
  @Inject ESelectionService selectionService;
  
  @Inject IMappingService mappingService;
  
  @Inject
  MApplication app;

  @Inject
  private EPartService partService;
  
  @Inject
  private EModelService modelService;
  
  Resource resource;
  
  public TreeViewPart(){
    super();
    
  }
  
  public void init(){
    initialiseResource();
    initDomain();
  }
  
  public void initDomain() {
    adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
    adapterFactory.addAdapterFactory(new SCLProviderAdapterFactory());
    
    BasicCommandStack commandStack = new BasicCommandStack();

    // Add a listener to set the most recent command's affected objects to be the selection of the viewer with focus.
    //
    commandStack.addCommandStackListener
      (new CommandStackListener() {
         public void commandStackChanged(final EventObject event) {
/*           getSite().getShell().getDisplay().asyncExec
             (new Runnable() {
                public void run() {
                  firePropertyChange(IEditorPart.PROP_DIRTY);

                  // Try to select the affected objects.
                  //
                  Command mostRecentCommand = ((CommandStack)event.getSource()).getMostRecentCommand();
                  if (mostRecentCommand != null) {
                    setSelectionToViewer(mostRecentCommand.getAffectedObjects());
                  }
//                  for (Iterator<PropertySheetPage> i = propertySheetPages.iterator(); i.hasNext(); ) {
//                    PropertySheetPage propertySheetPage = i.next();
//                    if (propertySheetPage.getControl().isDisposed()) {
//                      i.remove();
//                    }
//                    else {
//                      propertySheetPage.refresh();
//                    }
//                  }
                }
              });*/
         }
       });
    
    

    // Create the editing domain with a special command stack.
    //
    editingDomain = new AdapterFactoryEditingDomain(adapterFactory, commandStack, resourceSet);
  }
  
  public void setSelectionView(Collection<?> collection) {
    final Collection<?> theSelection = collection;
    // Make sure it's okay.
    //
    if (theSelection != null && !theSelection.isEmpty()) {
      Runnable runnable =
        new Runnable() {
          public void run() {
            // Try to select the items in the current content viewer of the editor.
            //
            if (viewer != null) {
              viewer.setSelection(new StructuredSelection(theSelection.toArray()), true);
            }
          }
        };
//      getSite().getShell().getDisplay().asyncExec(runnable);
    }
  }
  
  
  public TreePath getCurrentTreePath(){
    TreePath[] p = viewer.getStructuredSelection().getPaths();
    if (p.length >= 0)
      return p[0];
    return null;
  }


  @PostConstruct
  public void createComposite(Composite parent) {
    // viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
//    config = new G3Configuration();

    init();
    
//    Platform.getAdapterManager().registerAdapters(this, IPropertySheetPage.class);

//    Adapters.adapt(sourceObject, adapter);

//    File scd = new File("C:/repobox/G3Software/Development/G3/RTUTools/bundles/g3.iec61850/g3.iec61850.gui/newcid.xml");
//    System.out.println(scd.getAbsolutePath());

    EObject dummyObject = getDummyEObject();
    try {
//      dummyObject = ResourceUtils.loadObject(scd);
      
//      resource = editingDomain.getResourceSet().getResource(URI.createFileURI(scd.getAbsolutePath()), true);
      
//      dummyObject = resource.getContents().get(0);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
    // viewer = TreeViewerSWTFactory.createTreeViewer(parent, dummyObject);


    viewer.setContentProvider(new ExplorerContentProvider(adapterFactory));

    viewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
    
//    viewer.setContentProvider(new AdapterFactoryContentProvider(ad));
//    viewer.setLabelProvider(new AdapterFactoryLabelProvider(ad));
    
//    viewer.setInput(dummyObject);
//    viewer.setSelection(new StructuredSelection(rs, true);


    createContextMenu(viewer);
    

    // viewer.setSelection(ISelection);

    viewer.addSelectionChangedListener(new ISelectionChangedListener() {

          @Override
          public void selectionChanged(SelectionChangedEvent event) {
            // TreeViewPart.this.showView("org.eclipse.ui.views.PropertySheet");
            // System.out.println(o.getClass().getName());
            IStructuredSelection selection = viewer.getStructuredSelection();
            
//            CustomPropertiesView cp = app.getContext().get(CustomPropertiesView.class);
            
//            cp.propertySheetPage.setPropertySourceProvider(new AdapterFactoryContentProvider(adapterFactory));
            
            if(viewer.getStructuredSelection().getPaths().length <= 0)
              return;
            
//            SelObject ob = new SelObject(path, selection.getFirstElement());
            
            
            MUIElement elem = modelService.find(CustomPropertiesView.ID, app);
            
            MPart pp = (MPart) elem;
            CustomPropertiesView cp = (CustomPropertiesView)pp.getObject();
            cp.getPropertySheetPage(adapterFactory);
            pp.setObject(cp);

            
            
//            ISelectionProvider selp = v.getSelectionProvider();
            
//            selp.setSelection(event.getSelection());
//            ps.dispose();

            TreePath p = getCurrentTreePath();
            
            String path = getObjectName(p);
            SCLObject ob = (SCLObject) selection.getFirstElement();
//            ob.setPath(path);
//            v.getClass();
//            wrp.getSite().getSelectionProvider().setSelection(selection);;
            selectionService.setSelection(SelectEObject._new(ob, path));
            
            cp.recreateUI();

          }
        });
    
 // after the viewer is instantiated
//    viewer.addPostSelectionChangedListener(new ISelectionChangedListener() {
//      @Override
//      public void selectionChanged(SelectionChangedEvent event) {
//        IStructuredSelection selection =
//        (IStructuredSelection) viewer.getSelection();
//        // selection.getFirstElement() returns a Todo object
//        selectionService.setPostSelection(selection.getFirstElement());
//      }
//    });
    

    MPart pr = partService.findPart("g3.iec61850.gui.part.properties");
    

    
    parent.layout();
    
//    createPartControl(parent);
  }
  
  public static String getObjectName_test() {
    IServiceLocator serviceLocator = PlatformUI.getWorkbench();
    EPartService partService = serviceLocator.getService(EPartService.class);
    MPart part = partService.findPart(TreeViewPart.TreeViewPart_ID);
    TreeViewPart exp = (TreeViewPart) part.getObject();
    return getObjectName(exp.getCurrentTreePath());
  }
  
  public static void getTreePathFromList(TreePath[] paths) {
    if (paths == null || paths.length <= 0)
      return;
    
    for(int i = 0; i < paths.length; i++) {
      for(int j = 0; j < paths[i].getSegmentCount(); j++){
        Object ob = paths[i].getSegment(j);
        System.out.println(ob.getClass().getSimpleName());
      }
    }
  }
  
  //TODO - need work TBDA and TSDO
  public static String getObjectName(TreePath treePath) {
    String path ="";
    for (int i = 0; i < treePath.getSegmentCount(); i++) {
      Object ob = treePath.getSegment(i) ;
      if (ob instanceof TLDevice){
        TLDevice dev = (TLDevice) ob;
        path += dev.getLdName() + dev.getInst() + "/";
      }
      if (ob instanceof TAnyLN){
    	if (ob instanceof TLN0)
    	  path += ((TLN0)ob).getLnClass();
        if (ob instanceof TLN)
          path += ((TLN)ob).getLnClass() + ((TLN)ob).getInst();
        path += ".";
      }
      if (ob instanceof TDO){
        path += ((TDO)ob).getName();
      }      
    }
    return path;
  }

  public static void initialiseResource() {
    resourceSet = new ResourceSetImpl();
    resourceSet
        .getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION,
            new SCLResourceFactoryImpl());
    resourceSet
        .getPackageRegistry().put(SCLPackage.eNS_URI,
            SCLPackage.eINSTANCE);
    
    
    ResourceUtils.setResourceManager(resourceSet);

  }

  /**
   * Create new sample object
   * @return
   */
  private EObject getDummyEObject() {
    // Replace this with your own model EClass to test the application with a
    // custom model
    final EClass eClass = SCLPackage.eINSTANCE.getDocumentRoot();
    return EcoreUtil.create(eClass);
  }
  


  private ImageDescriptor createImageDescriptor() {
    Bundle bundle = FrameworkUtil
        .getBundle(AdapterFactoryLabelProvider.class);
    URL url = FileLocator
        .find(bundle, new Path("icons/save_edit.png"), null);
    return ImageDescriptor
        .createFromURL(url);
  }
  
  @Focus
  public void setFocus() {
     viewer.getControl().setFocus();
  }
  
  // to be dynamic using editing domains tools
  protected void createContextMenu(Viewer viewer) {
    MenuManager contextMenu = new MenuManager("#ViewerMenu"); //$NON-NLS-1$
    contextMenu.setRemoveAllWhenShown(true);
    contextMenu.addMenuListener(new IMenuListener() {
        @Override
        public void menuAboutToShow(IMenuManager mgr) {
            fillContextMenu(mgr);
        }
    });

    Menu menu = contextMenu.createContextMenu(viewer.getControl());
    viewer.getControl().setMenu(menu);
}

/**
 * Fill dynamic context menu
 *
 * @param contextMenu
 */
protected void fillContextMenu(IMenuManager contextMenu) {
    contextMenu.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));

    IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
    Object selObj = selection.getFirstElement();
    if (selObj instanceof TLDevice) {
      final TLDevice logDev = (TLDevice)selObj;
      
      contextMenu.add(new Action("Add Logical Node") {
        @Override
        public void run() {
          
//          SCLPackage.eINSTANCE.getTLN();
			editingDomain.getNewChildDescriptors(logDev, null);
	
			EClass eClass = SCLPackage.eINSTANCE.getTLN();
			TLN ln = (TLN) EcoreUtil.create(eClass);
	
			eClass = G3RefPackage.eINSTANCE.getTG3RefObject();
			TG3RefObject g3ref = (TG3RefObject) EcoreUtil.create(eClass);
			g3ref.setUuid(UUID.randomUUID().toString());
	
			TPrivate newPrivate = SCLFactory.eINSTANCE.createTPrivate();
	
			newPrivate.setType(G3RefPackage.eNS_PREFIX);
	
			FeatureMap privateMixed = newPrivate.getMixed();
	
			BasicExtendedMetaData metaData = new BasicExtendedMetaData(resourceSet.getPackageRegistry());
			resourceSet.getLoadOptions().put(XMLResource.OPTION_EXTENDED_META_DATA, metaData);
			EStructuralFeature sysFeature = metaData.demandFeature(G3RefPackage.eNS_URI, "G3RefObject", true);
			// FeatureMapUtil.addText(privateMixed, "n ");
			privateMixed.add(sysFeature, g3ref);
	
			ln.getPrivate().add(newPrivate);
			logDev.getLN().add(ln);
	
			viewer.refresh();
        }
      });
    }
    
    if (selObj instanceof TAnyLN) {
      final TAnyLN logDev = (TAnyLN)selObj;
      
      contextMenu.add(new Action("Add Data") {
        @Override
        public void run() {
          
//          SCLPackage.eINSTANCE.getTLN();
          editingDomain.getNewChildDescriptors(logDev, null);
          
          final EClass eClass = SCLPackage.eINSTANCE.getTLN();
          EcoreUtil.create(eClass);
          
          logDev.getLnType();
          
          logDev.getDOI();

//          logDev.getLN().add((TLN) EcoreUtil.create(eClass));
          
          viewer.refresh();
        }
      });
    }
    
    
    contextMenu.add(new Action("Validate") {
        @Override
        public void run() {
        	
          IStatus status = mappingService.validate((EObject)viewer.getInput());
          if (!status.isOK()) {
        	  ErrorDialog.openError(null, "Validation Failed", "SCL Validation failed.", status);
          } else {
        	  MessageDialog.openInformation(null, "Success", "Validation Successfull");
          }        

        }
    });
    

    
    contextMenu.add(new Action("Refresh") {
        @Override
        public void run() {
          viewer.refresh();
        }
    });
    contextMenu.add(new Action("Delete") {
        @Override
        public void run() {
            IStructuredSelection sel = (IStructuredSelection) viewer.getSelection();
            if (sel.getFirstElement() instanceof EObject)
              EcoreUtil.delete((EObject)sel.getFirstElement(), true);
        }
    });
}




//  @Override
//  public void addSelectionChangedListener(ISelectionChangedListener listener) {
//    // TODO Auto-generated method stub
//    selectionChangedListeners.add(listener);
//  }

//  @Override
//  public ISelection getSelection() {
//    // TODO Auto-generated method stub
//    return viewer.getSelection();
//  }
//
//  @Override
//  public void removeSelectionChangedListener(ISelectionChangedListener listener) {
//    // TODO Auto-generated method stub
//    selectionChangedListeners.remove(listener);
//  }
//
//  @Override
//  public void setSelection(ISelection selection) {
//    for (ISelectionChangedListener listener : selectionChangedListeners) {
//      listener.selectionChanged(new SelectionChangedEvent(this, selection));
//    }
//
//  }

  public void createPartControl(Composite parent) {
    // TODO Auto-generated method stub
//    getSite(). setSelectionProvider(viewer);
    MUIElement elem = modelService.find("g3.iec61850.gui.part.properties", app);
    
    MPlaceholder placeholder = (MPlaceholder) elem;
    
    PartImpl o = (PartImpl) placeholder.getRef();
    
    E4PartWrapper wrp = E4PartWrapper.getE4PartWrapper(o);
//    p.dispose();
//    E4PartWrapper
    wrp.getSite();
    
    EList<?> list = o.eAdapters();
    
    CompatibilityView cm = (CompatibilityView)o.getObject();
    PropertySheet ps = (PropertySheet) cm.getPart();
    
    IWorkbenchPartSite v = ps.getSite();
      if (!setLegacySelProvider){
      v.setSelectionProvider(viewer);
      setLegacySelProvider = true;
    }
    System.out.println("test");
  }

}
