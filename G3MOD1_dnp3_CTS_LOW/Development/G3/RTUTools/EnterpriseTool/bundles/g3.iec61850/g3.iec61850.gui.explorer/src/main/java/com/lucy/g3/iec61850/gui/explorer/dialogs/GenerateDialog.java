
package com.lucy.g3.iec61850.gui.explorer.dialogs;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Text;

import com.lucy.g3.iec61850.gui.explorer.services.IMappingService;


public class GenerateDialog extends Dialog {
  
  @Inject
  private IMappingService mappingService;
  private Text txtMap;
  private Text txtFile;
  
  private File fResource;
  private File fMap;
  
  protected GenerateDialog(Shell parentShell) {
    super(parentShell);
  }
  
  public static GenerateDialog open(Shell shell) {
    GenerateDialog d = new GenerateDialog(shell);
    return d;
  }
  
  public File getMappingFile(){
    return fMap;
  }
  
  public File getResourceFile(){
    return fResource;
  }
  
  private static String fileDialog(Shell shell){
    FileDialog dlg = new FileDialog(shell,  SWT.OPEN );
    dlg.setText("Save");
    String path = dlg.open();
    if(path == null) return path;
    
    File file = new File(path);
    if (file.exists()) {
      // The file already exists; asks for confirmation
      MessageBox mb = new MessageBox(dlg.getParent(), SWT.ICON_WARNING | SWT.YES | SWT.NO);
      mb.setMessage(path + " already exists. Do you want to replace it?");
      if (mb.open() != SWT.YES)
        path = null;
    }
    
    return path;
  }
  

  @Override
  protected Control createDialogArea(final Composite parent) {
          Composite container = (Composite) super.createDialogArea(parent);
          GridLayout gridLayout = (GridLayout) container.getLayout();
          gridLayout.numColumns = 2;
          
          Label lblIcdFile = new Label(container, SWT.NONE);
          lblIcdFile.setText("ICD File Location");
          new Label(container, SWT.NONE);
          
          txtFile = new Text(container, SWT.BORDER);
          txtFile.setText("C:/repobox/G3Software/Development/G3/RTUTools/bundles/g3.iec61850/g3.iec61850.gui/test.icd");
          txtFile.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
          txtFile.setEditable(false);
          
          Button btnFile = new Button(container, SWT.NONE);
          btnFile.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
              String path = fileDialog(parent.getShell());
              if (path != null)
                txtFile.setText(path);
            }
          });
          btnFile.setText("...");
          
          Label lblNewLabel = new Label(container, SWT.NONE);
          lblNewLabel.setText("Gemini 3 Configuration");
          new Label(container, SWT.NONE);
          
          txtMap = new Text(container, SWT.BORDER);
          txtMap.setText("C:/repobox/G3Software/Development/G3/RTUTools/bundles/g3.iec61850/g3.iec61850.gui/test.map");
          txtMap.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
          txtMap.setEditable(false);
          Button btnMap = new Button(container, SWT.PUSH);
          btnMap.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false,
                          false));
          btnMap.setText("...");
          btnMap.addSelectionListener(new SelectionAdapter() {
                  @Override
                  public void widgetSelected(SelectionEvent e) {
                    String path = fileDialog(parent.getShell());
                    if (path != null)
                      txtMap.setText(path);
                  }
          });

          return container;
  }

  // overriding this methods allows you to set the
  // title of the custom dialog
  @Override
  protected void configureShell(Shell newShell) {
          super.configureShell(newShell);
          newShell.setText("Generate ICD");
  }

  @Override
  protected Point getInitialSize() {
          return new Point(665, 268);
  }
  
  @Override
  protected void okPressed() {
      fMap = new File(txtMap.getText());
      fResource = new File(txtFile.getText());
      super.okPressed();
  }
}

