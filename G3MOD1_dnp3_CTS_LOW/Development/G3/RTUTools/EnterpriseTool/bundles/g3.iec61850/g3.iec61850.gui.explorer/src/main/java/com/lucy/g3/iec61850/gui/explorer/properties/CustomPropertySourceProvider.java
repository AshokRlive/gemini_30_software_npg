
package com.lucy.g3.iec61850.gui.explorer.properties;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.IPropertySourceProvider;

import com.lucy.g3.iec61850.gui.explorer.internal.SelectEObject;
import com.lucy.g3.iec61850.model.scl.TLN;

public class CustomPropertySourceProvider implements IPropertySourceProvider {
  ComposedAdapterFactory caf;
  public CustomPropertySourceProvider(AdapterFactory adapterFactory, EObject selectedObject) {
    caf = (ComposedAdapterFactory) adapterFactory;
  }

  @Override
  public IPropertySource getPropertySource(Object object) {
//    System.out.println(object);
    if (object instanceof EObject)
      object = ((EObject) object);
    if (object instanceof SelectEObject)
      object = ((SelectEObject)object).getObject();
    IItemPropertySource ips = (IItemPropertySource) caf.adapt(object, IItemPropertySource.class);
    
    if (ips != null){
      if (object instanceof TLN) {
        IPropertySource ps = new LNPropertySource(caf, object, ips);
        return ps;
      }
      IPropertySource ps = new CustomPropertySource(caf, object, ips);      
      return ps;
    }
    return null;
       
  }
  
}
