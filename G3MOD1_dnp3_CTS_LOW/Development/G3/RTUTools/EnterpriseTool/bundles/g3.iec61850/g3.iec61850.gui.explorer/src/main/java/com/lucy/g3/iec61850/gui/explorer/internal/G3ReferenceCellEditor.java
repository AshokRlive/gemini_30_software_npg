
package com.lucy.g3.iec61850.gui.explorer.internal;


import org.eclipse.jface.viewers.ColorCellEditor;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Shell;


public class G3ReferenceCellEditor extends DialogCellEditor {

  public G3ReferenceCellEditor(Composite parent){
    
  }
  
  @Override
  protected Object openDialogBox(Control cellEditorWindow) {
    // TODO Auto-generated method stub
    
    ReferenceDialog dialog = new ReferenceDialog(cellEditorWindow.getShell());
    
    return dialog.getData();
  }
  
  private class ReferenceDialog extends Dialog {

    public ReferenceDialog(Shell parent) {
      super(parent);
    }

    public Object getData() {
      // TODO Auto-generated method stub
      return "45";
    }
    
  }

}

