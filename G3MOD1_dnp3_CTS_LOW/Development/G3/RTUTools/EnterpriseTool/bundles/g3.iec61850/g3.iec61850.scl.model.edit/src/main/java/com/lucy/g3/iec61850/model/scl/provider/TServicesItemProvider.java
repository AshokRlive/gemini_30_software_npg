/**
 */
package com.lucy.g3.iec61850.model.scl.provider;


import com.lucy.g3.iec61850.model.g3ref.provider.SCLEditPlugin;

import com.lucy.g3.iec61850.model.scl.SCLFactory;
import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TServices;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.lucy.g3.iec61850.model.scl.TServices} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TServicesItemProvider 
  extends ItemProviderAdapter
  implements
    IEditingDomainItemProvider,
    IStructuredItemContentProvider,
    ITreeItemContentProvider,
    IItemLabelProvider,
    IItemPropertySource {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TServicesItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

      addNameLengthPropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Name Length feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addNameLengthPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TServices_nameLength_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TServices_nameLength_feature", "_UI_TServices_type"),
         SCLPackage.eINSTANCE.getTServices_NameLength(),
         true,
         false,
         false,
         ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
    if (childrenFeatures == null) {
      super.getChildrenFeatures(object);
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_DynAssociation());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_SettingGroups());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_GetDirectory());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_GetDataObjectDefinition());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_DataObjectDirectory());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_GetDataSetValue());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_SetDataSetValue());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_DataSetDirectory());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_ConfDataSet());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_DynDataSet());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_ReadWrite());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_TimerActivatedControl());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_ConfReportControl());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_GetCBValues());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_ConfLogControl());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_ReportSettings());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_LogSettings());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_GSESettings());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_SMVSettings());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_GSEDir());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_GOOSE());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_GSSE());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_SMVsc());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_FileHandling());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_ConfLNs());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_ClientServices());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_ConfLdName());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_SupSubscription());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTServices_ConfSigRef());
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child) {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns TServices.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/TServices"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    TServices tServices = (TServices)object;
    return getString("_UI_TServices_type") + " " + tServices.getNameLength();
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(TServices.class)) {
      case SCLPackage.TSERVICES__NAME_LENGTH:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
      case SCLPackage.TSERVICES__DYN_ASSOCIATION:
      case SCLPackage.TSERVICES__SETTING_GROUPS:
      case SCLPackage.TSERVICES__GET_DIRECTORY:
      case SCLPackage.TSERVICES__GET_DATA_OBJECT_DEFINITION:
      case SCLPackage.TSERVICES__DATA_OBJECT_DIRECTORY:
      case SCLPackage.TSERVICES__GET_DATA_SET_VALUE:
      case SCLPackage.TSERVICES__SET_DATA_SET_VALUE:
      case SCLPackage.TSERVICES__DATA_SET_DIRECTORY:
      case SCLPackage.TSERVICES__CONF_DATA_SET:
      case SCLPackage.TSERVICES__DYN_DATA_SET:
      case SCLPackage.TSERVICES__READ_WRITE:
      case SCLPackage.TSERVICES__TIMER_ACTIVATED_CONTROL:
      case SCLPackage.TSERVICES__CONF_REPORT_CONTROL:
      case SCLPackage.TSERVICES__GET_CB_VALUES:
      case SCLPackage.TSERVICES__CONF_LOG_CONTROL:
      case SCLPackage.TSERVICES__REPORT_SETTINGS:
      case SCLPackage.TSERVICES__LOG_SETTINGS:
      case SCLPackage.TSERVICES__GSE_SETTINGS:
      case SCLPackage.TSERVICES__SMV_SETTINGS:
      case SCLPackage.TSERVICES__GSE_DIR:
      case SCLPackage.TSERVICES__GOOSE:
      case SCLPackage.TSERVICES__GSSE:
      case SCLPackage.TSERVICES__SM_VSC:
      case SCLPackage.TSERVICES__FILE_HANDLING:
      case SCLPackage.TSERVICES__CONF_LNS:
      case SCLPackage.TSERVICES__CLIENT_SERVICES:
      case SCLPackage.TSERVICES__CONF_LD_NAME:
      case SCLPackage.TSERVICES__SUP_SUBSCRIPTION:
      case SCLPackage.TSERVICES__CONF_SIG_REF:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_DynAssociation(),
         SCLFactory.eINSTANCE.createTServiceWithOptionalMax()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_SettingGroups(),
         SCLFactory.eINSTANCE.createSettingGroupsType()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GetDirectory(),
         SCLFactory.eINSTANCE.createTServiceYesNo()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GetDataObjectDefinition(),
         SCLFactory.eINSTANCE.createTServiceYesNo()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_DataObjectDirectory(),
         SCLFactory.eINSTANCE.createTServiceYesNo()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GetDataSetValue(),
         SCLFactory.eINSTANCE.createTServiceYesNo()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_SetDataSetValue(),
         SCLFactory.eINSTANCE.createTServiceYesNo()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_DataSetDirectory(),
         SCLFactory.eINSTANCE.createTServiceYesNo()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfDataSet(),
         SCLFactory.eINSTANCE.createTServiceForConfDataSet()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_DynDataSet(),
         SCLFactory.eINSTANCE.createTServiceWithMaxAndMaxAttributes()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_DynDataSet(),
         SCLFactory.eINSTANCE.createTServiceForConfDataSet()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ReadWrite(),
         SCLFactory.eINSTANCE.createTServiceYesNo()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_TimerActivatedControl(),
         SCLFactory.eINSTANCE.createTServiceYesNo()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfReportControl(),
         SCLFactory.eINSTANCE.createTServiceConfReportControl()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GetCBValues(),
         SCLFactory.eINSTANCE.createTServiceYesNo()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfLogControl(),
         SCLFactory.eINSTANCE.createTServiceWithMax()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfLogControl(),
         SCLFactory.eINSTANCE.createTServiceConfReportControl()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfLogControl(),
         SCLFactory.eINSTANCE.createTServiceWithMaxAndMaxAttributes()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfLogControl(),
         SCLFactory.eINSTANCE.createTServiceForConfDataSet()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfLogControl(),
         SCLFactory.eINSTANCE.createTServiceWithMaxAndModify()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ReportSettings(),
         SCLFactory.eINSTANCE.createTReportSettings()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_LogSettings(),
         SCLFactory.eINSTANCE.createTLogSettings()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GSESettings(),
         SCLFactory.eINSTANCE.createTGSESettings()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_SMVSettings(),
         SCLFactory.eINSTANCE.createTSMVSettings()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GSEDir(),
         SCLFactory.eINSTANCE.createTServiceYesNo()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GOOSE(),
         SCLFactory.eINSTANCE.createTServiceWithMax()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GOOSE(),
         SCLFactory.eINSTANCE.createTServiceConfReportControl()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GOOSE(),
         SCLFactory.eINSTANCE.createTServiceWithMaxAndMaxAttributes()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GOOSE(),
         SCLFactory.eINSTANCE.createTServiceForConfDataSet()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GOOSE(),
         SCLFactory.eINSTANCE.createTServiceWithMaxAndModify()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GSSE(),
         SCLFactory.eINSTANCE.createTServiceWithMax()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GSSE(),
         SCLFactory.eINSTANCE.createTServiceConfReportControl()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GSSE(),
         SCLFactory.eINSTANCE.createTServiceWithMaxAndMaxAttributes()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GSSE(),
         SCLFactory.eINSTANCE.createTServiceForConfDataSet()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_GSSE(),
         SCLFactory.eINSTANCE.createTServiceWithMaxAndModify()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_SMVsc(),
         SCLFactory.eINSTANCE.createTServiceWithMax()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_SMVsc(),
         SCLFactory.eINSTANCE.createTServiceConfReportControl()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_SMVsc(),
         SCLFactory.eINSTANCE.createTServiceWithMaxAndMaxAttributes()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_SMVsc(),
         SCLFactory.eINSTANCE.createTServiceForConfDataSet()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_SMVsc(),
         SCLFactory.eINSTANCE.createTServiceWithMaxAndModify()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_FileHandling(),
         SCLFactory.eINSTANCE.createTServiceYesNo()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfLNs(),
         SCLFactory.eINSTANCE.createTConfLNs()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ClientServices(),
         SCLFactory.eINSTANCE.createTClientServices()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfLdName(),
         SCLFactory.eINSTANCE.createTServiceYesNo()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_SupSubscription(),
         SCLFactory.eINSTANCE.createTServiceWithMax()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_SupSubscription(),
         SCLFactory.eINSTANCE.createTServiceConfReportControl()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_SupSubscription(),
         SCLFactory.eINSTANCE.createTServiceWithMaxAndMaxAttributes()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_SupSubscription(),
         SCLFactory.eINSTANCE.createTServiceForConfDataSet()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_SupSubscription(),
         SCLFactory.eINSTANCE.createTServiceWithMaxAndModify()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfSigRef(),
         SCLFactory.eINSTANCE.createTServiceWithMax()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfSigRef(),
         SCLFactory.eINSTANCE.createTServiceConfReportControl()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfSigRef(),
         SCLFactory.eINSTANCE.createTServiceWithMaxAndMaxAttributes()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfSigRef(),
         SCLFactory.eINSTANCE.createTServiceForConfDataSet()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTServices_ConfSigRef(),
         SCLFactory.eINSTANCE.createTServiceWithMaxAndModify()));
  }

  /**
   * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
    Object childFeature = feature;
    Object childObject = child;

    boolean qualify =
      childFeature == SCLPackage.eINSTANCE.getTServices_GetDirectory() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_GetDataObjectDefinition() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_DataObjectDirectory() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_GetDataSetValue() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_SetDataSetValue() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_DataSetDirectory() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_ReadWrite() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_TimerActivatedControl() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_GetCBValues() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_GSEDir() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_FileHandling() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_ConfLdName() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_ConfDataSet() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_DynDataSet() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_ConfLogControl() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_GOOSE() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_GSSE() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_SMVsc() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_SupSubscription() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_ConfSigRef() ||
      childFeature == SCLPackage.eINSTANCE.getTServices_ConfReportControl();

    if (qualify) {
      return getString
        ("_UI_CreateChild_text2",
         new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
    }
    return super.getCreateChildText(owner, feature, child, selection);
  }

  /**
   * Return the resource locator for this item provider's resources.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ResourceLocator getResourceLocator() {
    return SCLEditPlugin.INSTANCE;
  }

}
