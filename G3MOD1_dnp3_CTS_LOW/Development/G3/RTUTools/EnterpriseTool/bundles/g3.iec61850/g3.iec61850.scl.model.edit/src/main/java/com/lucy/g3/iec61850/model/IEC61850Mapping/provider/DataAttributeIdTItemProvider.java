/**
 */
package com.lucy.g3.iec61850.model.IEC61850Mapping.provider;


import com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT;
import com.lucy.g3.iec61850.model.IEC61850Mapping.IEC61850MappingPackage;

import com.lucy.g3.iec61850.model.g3ref.provider.SCLEditPlugin;

import java.math.BigInteger;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.lucy.g3.iec61850.model.IEC61850Mapping.DataAttributeIdT} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DataAttributeIdTItemProvider 
  extends ItemProviderAdapter
  implements
    IEditingDomainItemProvider,
    IStructuredItemContentProvider,
    ITreeItemContentProvider,
    IItemLabelProvider,
    IItemPropertySource {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DataAttributeIdTItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

      addField0PropertyDescriptor(object);
      addField1PropertyDescriptor(object);
      addField2PropertyDescriptor(object);
      addField3PropertyDescriptor(object);
      addField4PropertyDescriptor(object);
      addField5PropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Field0 feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addField0PropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_DataAttributeIdT_field0_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_DataAttributeIdT_field0_feature", "_UI_DataAttributeIdT_type"),
         IEC61850MappingPackage.Literals.DATA_ATTRIBUTE_ID_T__FIELD0,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Field1 feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addField1PropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_DataAttributeIdT_field1_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_DataAttributeIdT_field1_feature", "_UI_DataAttributeIdT_type"),
         IEC61850MappingPackage.Literals.DATA_ATTRIBUTE_ID_T__FIELD1,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Field2 feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addField2PropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_DataAttributeIdT_field2_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_DataAttributeIdT_field2_feature", "_UI_DataAttributeIdT_type"),
         IEC61850MappingPackage.Literals.DATA_ATTRIBUTE_ID_T__FIELD2,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Field3 feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addField3PropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_DataAttributeIdT_field3_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_DataAttributeIdT_field3_feature", "_UI_DataAttributeIdT_type"),
         IEC61850MappingPackage.Literals.DATA_ATTRIBUTE_ID_T__FIELD3,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Field4 feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addField4PropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_DataAttributeIdT_field4_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_DataAttributeIdT_field4_feature", "_UI_DataAttributeIdT_type"),
         IEC61850MappingPackage.Literals.DATA_ATTRIBUTE_ID_T__FIELD4,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Field5 feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addField5PropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_DataAttributeIdT_field5_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_DataAttributeIdT_field5_feature", "_UI_DataAttributeIdT_type"),
         IEC61850MappingPackage.Literals.DATA_ATTRIBUTE_ID_T__FIELD5,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This returns DataAttributeIdT.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/DataAttributeIdT"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    BigInteger labelValue = ((DataAttributeIdT)object).getField0();
    String label = labelValue == null ? null : labelValue.toString();
    return label == null || label.length() == 0 ?
      getString("_UI_DataAttributeIdT_type") :
      getString("_UI_DataAttributeIdT_type") + " " + label;
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(DataAttributeIdT.class)) {
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD0:
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD1:
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD2:
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD3:
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD4:
      case IEC61850MappingPackage.DATA_ATTRIBUTE_ID_T__FIELD5:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);
  }

  /**
   * Return the resource locator for this item provider's resources.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ResourceLocator getResourceLocator() {
    return SCLEditPlugin.INSTANCE;
  }

}
