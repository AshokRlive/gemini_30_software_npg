/**
 */
package com.lucy.g3.iec61850.model.scl.provider;


import com.lucy.g3.iec61850.model.g3ref.provider.SCLEditPlugin;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TExtRef;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.lucy.g3.iec61850.model.scl.TExtRef} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TExtRefItemProvider 
  extends ItemProviderAdapter
  implements
    IEditingDomainItemProvider,
    IStructuredItemContentProvider,
    ITreeItemContentProvider,
    IItemLabelProvider,
    IItemPropertySource {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TExtRefItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

      addDaNamePropertyDescriptor(object);
      addDescPropertyDescriptor(object);
      addDoNamePropertyDescriptor(object);
      addIedNamePropertyDescriptor(object);
      addIntAddrPropertyDescriptor(object);
      addLdInstPropertyDescriptor(object);
      addLnClassPropertyDescriptor(object);
      addLnInstPropertyDescriptor(object);
      addPrefixPropertyDescriptor(object);
      addServiceTypePropertyDescriptor(object);
      addSrcCBNamePropertyDescriptor(object);
      addSrcLDInstPropertyDescriptor(object);
      addSrcLNClassPropertyDescriptor(object);
      addSrcLNInstPropertyDescriptor(object);
      addSrcPrefixPropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Da Name feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addDaNamePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_daName_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_daName_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_DaName(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Desc feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addDescPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_desc_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_desc_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_Desc(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Do Name feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addDoNamePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_doName_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_doName_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_DoName(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Ied Name feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addIedNamePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_iedName_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_iedName_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_IedName(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Int Addr feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addIntAddrPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_intAddr_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_intAddr_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_IntAddr(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Ld Inst feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addLdInstPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_ldInst_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_ldInst_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_LdInst(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Ln Class feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addLnClassPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_lnClass_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_lnClass_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_LnClass(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Ln Inst feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addLnInstPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_lnInst_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_lnInst_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_LnInst(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Prefix feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addPrefixPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_prefix_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_prefix_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_Prefix(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Service Type feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addServiceTypePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_serviceType_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_serviceType_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_ServiceType(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Src CB Name feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSrcCBNamePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_srcCBName_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_srcCBName_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_SrcCBName(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Src LD Inst feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSrcLDInstPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_srcLDInst_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_srcLDInst_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_SrcLDInst(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Src LN Class feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSrcLNClassPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_srcLNClass_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_srcLNClass_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_SrcLNClass(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Src LN Inst feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSrcLNInstPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_srcLNInst_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_srcLNInst_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_SrcLNInst(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Src Prefix feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSrcPrefixPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TExtRef_srcPrefix_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TExtRef_srcPrefix_feature", "_UI_TExtRef_type"),
         SCLPackage.eINSTANCE.getTExtRef_SrcPrefix(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This returns TExtRef.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/TExtRef"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    String label = ((TExtRef)object).getDaName();
    return label == null || label.length() == 0 ?
      getString("_UI_TExtRef_type") :
      getString("_UI_TExtRef_type") + " " + label;
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(TExtRef.class)) {
      case SCLPackage.TEXT_REF__DA_NAME:
      case SCLPackage.TEXT_REF__DESC:
      case SCLPackage.TEXT_REF__DO_NAME:
      case SCLPackage.TEXT_REF__IED_NAME:
      case SCLPackage.TEXT_REF__INT_ADDR:
      case SCLPackage.TEXT_REF__LD_INST:
      case SCLPackage.TEXT_REF__LN_CLASS:
      case SCLPackage.TEXT_REF__LN_INST:
      case SCLPackage.TEXT_REF__PREFIX:
      case SCLPackage.TEXT_REF__SERVICE_TYPE:
      case SCLPackage.TEXT_REF__SRC_CB_NAME:
      case SCLPackage.TEXT_REF__SRC_LD_INST:
      case SCLPackage.TEXT_REF__SRC_LN_CLASS:
      case SCLPackage.TEXT_REF__SRC_LN_INST:
      case SCLPackage.TEXT_REF__SRC_PREFIX:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);
  }

  /**
   * Return the resource locator for this item provider's resources.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ResourceLocator getResourceLocator() {
    return SCLEditPlugin.INSTANCE;
  }

}
