/**
 */
package com.lucy.g3.iec61850.model.scl.provider;


import com.lucy.g3.iec61850.model.g3ref.G3RefFactory;
import com.lucy.g3.iec61850.model.g3ref.G3RefPackage;

import com.lucy.g3.iec61850.model.g3ref.provider.SCLEditPlugin;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace;

import com.lucy.g3.iec61850.model.systemcorp.SystemCorpFactory;
import com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.ecore.xml.type.XMLTypeFactory;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.lucy.g3.iec61850.model.scl.TAnyContentFromOtherNamespace} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TAnyContentFromOtherNamespaceItemProvider 
  extends ItemProviderAdapter
  implements
    IEditingDomainItemProvider,
    IStructuredItemContentProvider,
    ITreeItemContentProvider,
    IItemLabelProvider,
    IItemPropertySource {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TAnyContentFromOtherNamespaceItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

    }
    return itemPropertyDescriptors;
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
    if (childrenFeatures == null) {
      super.getChildrenFeatures(object);
      childrenFeatures.add(SCLPackage.eINSTANCE.getTAnyContentFromOtherNamespace_Mixed());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTAnyContentFromOtherNamespace_AnyAttribute());
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child) {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    return getString("_UI_TAnyContentFromOtherNamespace_type");
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(TAnyContentFromOtherNamespace.class)) {
      case SCLPackage.TANY_CONTENT_FROM_OTHER_NAMESPACE__MIXED:
      case SCLPackage.TANY_CONTENT_FROM_OTHER_NAMESPACE__ANY_ATTRIBUTE:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAnyContentFromOtherNamespace_Mixed(),
         FeatureMapUtil.createEntry
          (XMLTypePackage.Literals.XML_TYPE_DOCUMENT_ROOT__COMMENT,
           "")));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAnyContentFromOtherNamespace_Mixed(),
         FeatureMapUtil.createEntry
          (XMLTypePackage.Literals.XML_TYPE_DOCUMENT_ROOT__TEXT,
           "")));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAnyContentFromOtherNamespace_Mixed(),
         FeatureMapUtil.createEntry
          (XMLTypePackage.Literals.XML_TYPE_DOCUMENT_ROOT__PROCESSING_INSTRUCTION,
           XMLTypeFactory.eINSTANCE.createProcessingInstruction())));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAnyContentFromOtherNamespace_Mixed(),
         FeatureMapUtil.createEntry
          (XMLTypePackage.Literals.XML_TYPE_DOCUMENT_ROOT__CDATA,
           "")));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAnyContentFromOtherNamespace_Mixed(),
         FeatureMapUtil.createEntry
          (G3RefPackage.Literals.DOCUMENT_ROOT__G3_REF_OBJECT,
           G3RefFactory.eINSTANCE.createTG3RefObject())));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAnyContentFromOtherNamespace_Mixed(),
         FeatureMapUtil.createEntry
          (SystemCorpPackage.Literals.DOCUMENT_ROOT__GENERIC_PRIVATE_OBJECT,
           SystemCorpFactory.eINSTANCE.createTGenericPrivateObject())));
  }

  /**
   * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
    Object childFeature = feature;
    Object childObject = child;

    if (childFeature instanceof EStructuralFeature && FeatureMapUtil.isFeatureMap((EStructuralFeature)childFeature)) {
      FeatureMap.Entry entry = (FeatureMap.Entry)childObject;
      childFeature = entry.getEStructuralFeature();
      childObject = entry.getValue();
    }

    boolean qualify =
      childFeature == G3RefPackage.Literals.DOCUMENT_ROOT__G3_REF_OBJECT ||
      childFeature == SystemCorpPackage.Literals.DOCUMENT_ROOT__GENERIC_PRIVATE_OBJECT;

    if (qualify) {
      return getString
        ("_UI_CreateChild_text2",
         new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
    }
    return super.getCreateChildText(owner, feature, child, selection);
  }

  /**
   * Return the resource locator for this item provider's resources.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ResourceLocator getResourceLocator() {
    return SCLEditPlugin.INSTANCE;
  }

}
