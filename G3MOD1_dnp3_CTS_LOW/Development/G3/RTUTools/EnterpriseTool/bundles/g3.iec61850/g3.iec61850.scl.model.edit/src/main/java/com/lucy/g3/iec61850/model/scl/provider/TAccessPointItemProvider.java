/**
 */
package com.lucy.g3.iec61850.model.scl.provider;


import com.lucy.g3.iec61850.model.scl.SCLFactory;
import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TAccessPoint;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.lucy.g3.iec61850.model.scl.TAccessPoint} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TAccessPointItemProvider extends TUnNamingItemProvider {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TAccessPointItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

      addClockPropertyDescriptor(object);
      addNamePropertyDescriptor(object);
      addRouterPropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Clock feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addClockPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TAccessPoint_clock_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TAccessPoint_clock_feature", "_UI_TAccessPoint_type"),
         SCLPackage.eINSTANCE.getTAccessPoint_Clock(),
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Name feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addNamePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TAccessPoint_name_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TAccessPoint_name_feature", "_UI_TAccessPoint_type"),
         SCLPackage.eINSTANCE.getTAccessPoint_Name(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Router feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addRouterPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TAccessPoint_router_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TAccessPoint_router_feature", "_UI_TAccessPoint_type"),
         SCLPackage.eINSTANCE.getTAccessPoint_Router(),
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
    if (childrenFeatures == null) {
      super.getChildrenFeatures(object);
      childrenFeatures.add(SCLPackage.eINSTANCE.getTAccessPoint_Server());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTAccessPoint_LN());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTAccessPoint_ServerAt());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTAccessPoint_Services());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTAccessPoint_GOOSESecurity());
      childrenFeatures.add(SCLPackage.eINSTANCE.getTAccessPoint_SMVSecurity());
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child) {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns TAccessPoint.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/TAccessPoint"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    String label = ((TAccessPoint)object).getName();
    return label == null || label.length() == 0 ?
      getString("_UI_TAccessPoint_type") :
      getString("_UI_TAccessPoint_type") + " " + label;
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(TAccessPoint.class)) {
      case SCLPackage.TACCESS_POINT__CLOCK:
      case SCLPackage.TACCESS_POINT__NAME:
      case SCLPackage.TACCESS_POINT__ROUTER:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
      case SCLPackage.TACCESS_POINT__SERVER:
      case SCLPackage.TACCESS_POINT__LN:
      case SCLPackage.TACCESS_POINT__SERVER_AT:
      case SCLPackage.TACCESS_POINT__SERVICES:
      case SCLPackage.TACCESS_POINT__GOOSE_SECURITY:
      case SCLPackage.TACCESS_POINT__SMV_SECURITY:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAccessPoint_Server(),
         SCLFactory.eINSTANCE.createTServer()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAccessPoint_LN(),
         SCLFactory.eINSTANCE.createTLN()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAccessPoint_ServerAt(),
         SCLFactory.eINSTANCE.createTServerAt()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAccessPoint_Services(),
         SCLFactory.eINSTANCE.createTServices()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAccessPoint_GOOSESecurity(),
         SCLFactory.eINSTANCE.createTCertificate()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAccessPoint_SMVSecurity(),
         SCLFactory.eINSTANCE.createTCertificate()));
  }

  /**
   * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
    Object childFeature = feature;
    Object childObject = child;

    if (childFeature instanceof EStructuralFeature && FeatureMapUtil.isFeatureMap((EStructuralFeature)childFeature)) {
      FeatureMap.Entry entry = (FeatureMap.Entry)childObject;
      childFeature = entry.getEStructuralFeature();
      childObject = entry.getValue();
    }

    boolean qualify =
      childFeature == SCLPackage.eINSTANCE.getTAccessPoint_GOOSESecurity() ||
      childFeature == SCLPackage.eINSTANCE.getTAccessPoint_SMVSecurity();

    if (qualify) {
      return getString
        ("_UI_CreateChild_text2",
         new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
    }
    return super.getCreateChildText(owner, feature, child, selection);
  }

}
