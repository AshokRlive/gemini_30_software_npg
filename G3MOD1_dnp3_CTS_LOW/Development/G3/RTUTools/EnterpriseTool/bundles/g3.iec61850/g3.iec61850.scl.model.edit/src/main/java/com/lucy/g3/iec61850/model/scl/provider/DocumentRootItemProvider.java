/**
 */
package com.lucy.g3.iec61850.model.scl.provider;


import com.lucy.g3.iec61850.model.g3ref.provider.SCLEditPlugin;

import com.lucy.g3.iec61850.model.scl.DocumentRoot;
import com.lucy.g3.iec61850.model.scl.SCLFactory;
import com.lucy.g3.iec61850.model.scl.SCLPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.lucy.g3.iec61850.model.scl.DocumentRoot} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DocumentRootItemProvider 
  extends ItemProviderAdapter
  implements
    IEditingDomainItemProvider,
    IStructuredItemContentProvider,
    ITreeItemContentProvider,
    IItemLabelProvider,
    IItemPropertySource {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DocumentRootItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

    }
    return itemPropertyDescriptors;
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
    if (childrenFeatures == null) {
      super.getChildrenFeatures(object);
      childrenFeatures.add(SCLPackage.eINSTANCE.getDocumentRoot_Communication());
      childrenFeatures.add(SCLPackage.eINSTANCE.getDocumentRoot_DataTypeTemplates());
      childrenFeatures.add(SCLPackage.eINSTANCE.getDocumentRoot_IED());
      childrenFeatures.add(SCLPackage.eINSTANCE.getDocumentRoot_LN());
      childrenFeatures.add(SCLPackage.eINSTANCE.getDocumentRoot_LN0());
      childrenFeatures.add(SCLPackage.eINSTANCE.getDocumentRoot_SCL());
      childrenFeatures.add(SCLPackage.eINSTANCE.getDocumentRoot_Substation());
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child) {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns DocumentRoot.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/DocumentRoot"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    return getString("_UI_DocumentRoot_type");
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(DocumentRoot.class)) {
      case SCLPackage.DOCUMENT_ROOT__COMMUNICATION:
      case SCLPackage.DOCUMENT_ROOT__DATA_TYPE_TEMPLATES:
      case SCLPackage.DOCUMENT_ROOT__IED:
      case SCLPackage.DOCUMENT_ROOT__LN:
      case SCLPackage.DOCUMENT_ROOT__LN0:
      case SCLPackage.DOCUMENT_ROOT__SCL:
      case SCLPackage.DOCUMENT_ROOT__SUBSTATION:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getDocumentRoot_Communication(),
         SCLFactory.eINSTANCE.createTCommunication()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getDocumentRoot_DataTypeTemplates(),
         SCLFactory.eINSTANCE.createTDataTypeTemplates()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getDocumentRoot_IED(),
         SCLFactory.eINSTANCE.createTIED()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getDocumentRoot_LN(),
         SCLFactory.eINSTANCE.createTLN()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getDocumentRoot_LN0(),
         SCLFactory.eINSTANCE.createLN0Type()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getDocumentRoot_SCL(),
         SCLFactory.eINSTANCE.createSCLType()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getDocumentRoot_Substation(),
         SCLFactory.eINSTANCE.createTSubstation()));
  }

  /**
   * Return the resource locator for this item provider's resources.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ResourceLocator getResourceLocator() {
    return SCLEditPlugin.INSTANCE;
  }

}
