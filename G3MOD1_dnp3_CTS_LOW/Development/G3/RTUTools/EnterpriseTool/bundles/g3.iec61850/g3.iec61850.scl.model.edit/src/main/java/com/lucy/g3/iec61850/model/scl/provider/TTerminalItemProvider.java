/**
 */
package com.lucy.g3.iec61850.model.scl.provider;


import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TTerminal;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.lucy.g3.iec61850.model.scl.TTerminal} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TTerminalItemProvider extends TUnNamingItemProvider {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTerminalItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

      addBayNamePropertyDescriptor(object);
      addCNodeNamePropertyDescriptor(object);
      addConnectivityNodePropertyDescriptor(object);
      addNamePropertyDescriptor(object);
      addNeutralPointPropertyDescriptor(object);
      addSubstationNamePropertyDescriptor(object);
      addVoltageLevelNamePropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Bay Name feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addBayNamePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TTerminal_bayName_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TTerminal_bayName_feature", "_UI_TTerminal_type"),
         SCLPackage.eINSTANCE.getTTerminal_BayName(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the CNode Name feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addCNodeNamePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TTerminal_cNodeName_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TTerminal_cNodeName_feature", "_UI_TTerminal_type"),
         SCLPackage.eINSTANCE.getTTerminal_CNodeName(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Connectivity Node feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addConnectivityNodePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TTerminal_connectivityNode_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TTerminal_connectivityNode_feature", "_UI_TTerminal_type"),
         SCLPackage.eINSTANCE.getTTerminal_ConnectivityNode(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Name feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addNamePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TTerminal_name_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TTerminal_name_feature", "_UI_TTerminal_type"),
         SCLPackage.eINSTANCE.getTTerminal_Name(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Neutral Point feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addNeutralPointPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TTerminal_neutralPoint_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TTerminal_neutralPoint_feature", "_UI_TTerminal_type"),
         SCLPackage.eINSTANCE.getTTerminal_NeutralPoint(),
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Substation Name feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSubstationNamePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TTerminal_substationName_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TTerminal_substationName_feature", "_UI_TTerminal_type"),
         SCLPackage.eINSTANCE.getTTerminal_SubstationName(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Voltage Level Name feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addVoltageLevelNamePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TTerminal_voltageLevelName_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TTerminal_voltageLevelName_feature", "_UI_TTerminal_type"),
         SCLPackage.eINSTANCE.getTTerminal_VoltageLevelName(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This returns TTerminal.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/TTerminal"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    String label = ((TTerminal)object).getName();
    return label == null || label.length() == 0 ?
      getString("_UI_TTerminal_type") :
      getString("_UI_TTerminal_type") + " " + label;
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(TTerminal.class)) {
      case SCLPackage.TTERMINAL__BAY_NAME:
      case SCLPackage.TTERMINAL__CNODE_NAME:
      case SCLPackage.TTERMINAL__CONNECTIVITY_NODE:
      case SCLPackage.TTERMINAL__NAME:
      case SCLPackage.TTERMINAL__NEUTRAL_POINT:
      case SCLPackage.TTERMINAL__SUBSTATION_NAME:
      case SCLPackage.TTERMINAL__VOLTAGE_LEVEL_NAME:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);
  }

}
