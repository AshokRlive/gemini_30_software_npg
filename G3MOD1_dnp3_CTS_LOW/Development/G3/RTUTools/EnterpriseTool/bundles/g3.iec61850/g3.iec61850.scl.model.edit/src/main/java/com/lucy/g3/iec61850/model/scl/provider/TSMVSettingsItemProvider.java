/**
 */
package com.lucy.g3.iec61850.model.scl.provider;


import com.lucy.g3.iec61850.model.scl.SCLFactory;
import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TSMVSettings;
import com.lucy.g3.iec61850.model.scl.TServiceSettingsEnum;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.lucy.g3.iec61850.model.scl.TSMVSettings} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TSMVSettingsItemProvider extends TServiceSettingsItemProvider {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TSMVSettingsItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

      addSmpRatePropertyDescriptor(object);
      addSamplesPerSecPropertyDescriptor(object);
      addSecPerSamplesPropertyDescriptor(object);
      addOptFieldsPropertyDescriptor(object);
      addSamplesPerSec1PropertyDescriptor(object);
      addSmpRate1PropertyDescriptor(object);
      addSvIDPropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Smp Rate feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSmpRatePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TSMVSettings_smpRate_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TSMVSettings_smpRate_feature", "_UI_TSMVSettings_type"),
         SCLPackage.eINSTANCE.getTSMVSettings_SmpRate(),
         true,
         false,
         false,
         ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Samples Per Sec feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSamplesPerSecPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TSMVSettings_samplesPerSec_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TSMVSettings_samplesPerSec_feature", "_UI_TSMVSettings_type"),
         SCLPackage.eINSTANCE.getTSMVSettings_SamplesPerSec(),
         true,
         false,
         false,
         ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Sec Per Samples feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSecPerSamplesPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TSMVSettings_secPerSamples_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TSMVSettings_secPerSamples_feature", "_UI_TSMVSettings_type"),
         SCLPackage.eINSTANCE.getTSMVSettings_SecPerSamples(),
         true,
         false,
         false,
         ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Opt Fields feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addOptFieldsPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TSMVSettings_optFields_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TSMVSettings_optFields_feature", "_UI_TSMVSettings_type"),
         SCLPackage.eINSTANCE.getTSMVSettings_OptFields(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Samples Per Sec1 feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSamplesPerSec1PropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TSMVSettings_samplesPerSec1_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TSMVSettings_samplesPerSec1_feature", "_UI_TSMVSettings_type"),
         SCLPackage.eINSTANCE.getTSMVSettings_SamplesPerSec1(),
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Smp Rate1 feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSmpRate1PropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TSMVSettings_smpRate1_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TSMVSettings_smpRate1_feature", "_UI_TSMVSettings_type"),
         SCLPackage.eINSTANCE.getTSMVSettings_SmpRate1(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Sv ID feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSvIDPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_TSMVSettings_svID_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_TSMVSettings_svID_feature", "_UI_TSMVSettings_type"),
         SCLPackage.eINSTANCE.getTSMVSettings_SvID(),
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
    if (childrenFeatures == null) {
      super.getChildrenFeatures(object);
      childrenFeatures.add(SCLPackage.eINSTANCE.getTSMVSettings_Group());
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child) {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns TSMVSettings.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/TSMVSettings"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    TServiceSettingsEnum labelValue = ((TSMVSettings)object).getCbName();
    String label = labelValue == null ? null : labelValue.toString();
    return label == null || label.length() == 0 ?
      getString("_UI_TSMVSettings_type") :
      getString("_UI_TSMVSettings_type") + " " + label;
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(TSMVSettings.class)) {
      case SCLPackage.TSMV_SETTINGS__SMP_RATE:
      case SCLPackage.TSMV_SETTINGS__SAMPLES_PER_SEC:
      case SCLPackage.TSMV_SETTINGS__SEC_PER_SAMPLES:
      case SCLPackage.TSMV_SETTINGS__OPT_FIELDS:
      case SCLPackage.TSMV_SETTINGS__SAMPLES_PER_SEC1:
      case SCLPackage.TSMV_SETTINGS__SMP_RATE1:
      case SCLPackage.TSMV_SETTINGS__SV_ID:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
      case SCLPackage.TSMV_SETTINGS__GROUP:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTSMVSettings_Group(),
         FeatureMapUtil.createEntry
          (SCLPackage.eINSTANCE.getTSMVSettings_SmpRate(),
           SCLFactory.eINSTANCE.createFromString(SCLPackage.eINSTANCE.getSmpRateType(), "0"))));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTSMVSettings_Group(),
         FeatureMapUtil.createEntry
          (SCLPackage.eINSTANCE.getTSMVSettings_SamplesPerSec(),
           SCLFactory.eINSTANCE.createFromString(SCLPackage.eINSTANCE.getSamplesPerSecType(), "0"))));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTSMVSettings_Group(),
         FeatureMapUtil.createEntry
          (SCLPackage.eINSTANCE.getTSMVSettings_SecPerSamples(),
           SCLFactory.eINSTANCE.createFromString(SCLPackage.eINSTANCE.getSecPerSamplesType(), "0"))));
  }

}
