/**
 */
package com.lucy.g3.iec61850.model.scl.provider;


import com.lucy.g3.iec61850.model.g3ref.provider.SCLEditPlugin;

import com.lucy.g3.iec61850.model.scl.SCLFactory;
import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TAddress;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.lucy.g3.iec61850.model.scl.TAddress} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TAddressItemProvider 
  extends ItemProviderAdapter
  implements
    IEditingDomainItemProvider,
    IStructuredItemContentProvider,
    ITreeItemContentProvider,
    IItemLabelProvider,
    IItemPropertySource {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TAddressItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

    }
    return itemPropertyDescriptors;
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
    if (childrenFeatures == null) {
      super.getChildrenFeatures(object);
      childrenFeatures.add(SCLPackage.eINSTANCE.getTAddress_P());
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child) {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns TAddress.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/TAddress"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    return getString("_UI_TAddress_type");
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(TAddress.class)) {
      case SCLPackage.TADDRESS__P:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTP()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPAPPID()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPIP()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPIPGATEWAY()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPIPSUBNET()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPMACAddress()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPMMSPort()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPOSIAEInvoke()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPOSIAEQualifier()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPOSIAPInvoke()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPOSIAPTitle()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPOSINSAP()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPOSIPSEL()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPOSISSEL()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPOSITSEL()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPSNTPPort()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPVLANID()));

    newChildDescriptors.add
      (createChildParameter
        (SCLPackage.eINSTANCE.getTAddress_P(),
         SCLFactory.eINSTANCE.createTPVLANPRIORITY()));
  }

  /**
   * Return the resource locator for this item provider's resources.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ResourceLocator getResourceLocator() {
    return SCLEditPlugin.INSTANCE;
  }

}
