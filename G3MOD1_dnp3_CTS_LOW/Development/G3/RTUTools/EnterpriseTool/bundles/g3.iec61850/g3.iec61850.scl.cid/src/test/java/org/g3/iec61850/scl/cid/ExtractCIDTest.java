package org.g3.iec61850.scl.cid;

import java.net.URL;

import com.lucy.g3.iec61850.scl.ResourceRegister;
import com.lucy.g3.iec61850.scl.cid.ExtractCID;

import junit.framework.Assert;
import junit.framework.TestCase;

public class ExtractCIDTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
		ResourceRegister.init();
	}
	
	public void testExtract() {
		String out = "target/testOutput.xml";
		
		try {
			
			URL url = getClass().getResource("/example1.scd");
					
			
			ExtractCID.generate(url.getFile(), out);
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

}
