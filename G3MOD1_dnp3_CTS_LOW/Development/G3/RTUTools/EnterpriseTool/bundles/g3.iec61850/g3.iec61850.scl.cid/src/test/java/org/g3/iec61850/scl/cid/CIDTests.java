package org.g3.iec61850.scl.cid;

import junit.framework.Test;
import junit.framework.TestSuite;

public class CIDTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(CIDTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTestSuite(ExtractCIDTest.class);
		//$JUnit-END$
		return suite;
	}

}
