package com.lucy.g3.iec61850.scl.cid;


import java.io.IOException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.lucy.g3.iec61850.scl.internal.ResourceUtils;
import com.lucy.g3.iec61850.model.scl.DocumentRoot;
import com.lucy.g3.iec61850.model.scl.SCLFactory;
import com.lucy.g3.iec61850.model.scl.SCLPackage;

public class ExtractCID {
	
	private EObject root;
	
	protected ExtractCID() {
		init();
	}
	

	
	protected void init() {
		ResourceUtils.check();
	}
	
	private EObject loadFile(String filePath) throws Exception {
		return root = ResourceUtils.loadObject(filePath);
	}
	
	protected void setRoot(EObject root) {
		this.root = root;
	}
	
	private EObject extractCID (EObject ob) {
		setRoot(ob);
		return extractCID();
	}
	
	private EObject extractCID(String filePath) throws Exception {
		loadFile(filePath);
		return extractCID();
	}
	
	/**
	 * 
	 * @return EObject
	 */
	private EObject extractCID () {
		
		DocumentRoot root = (DocumentRoot) EcoreUtil.copy(this.root);
		
		// currently these elements are included in the CID file
		root.getSCL();		
		root.getSCL().getHeader();
		root.getSCL().getCommunication();
		root.getSCL().getDataTypeTemplates();
		root.getSCL().getIED().get(0);
		
		
		DocumentRoot doc = SCLFactory.eINSTANCE.createDocumentRoot();
		doc.getXMLNSPrefixMap().put("", SCLPackage.eNS_URI);
				
		doc.setSCL(SCLFactory.eINSTANCE.createSCLType());
		doc.getSCL().setHeader(root.getSCL().getHeader());
		doc.getSCL().setCommunication(root.getSCL().getCommunication());
		doc.getSCL().setDataTypeTemplates(root.getSCL().getDataTypeTemplates());
		doc.getSCL().getIED().add(root.getSCL().getIED().get(0));
		
		return doc;
	}
	


	/**
	 * 
	 * @param inputFile
	 * @param outputFile
	 * @throws Exception
	 */
	public static void generate(String inputFile, String outputFile) throws Exception {
				
		EObject doc = new ExtractCID().extractCID(inputFile);
		ResourceUtils.saveObject(doc, outputFile);	
	}
	
	
	public static void generate(EObject root, String outputFile) throws IOException {
		EObject doc = new ExtractCID().extractCID(root);
		ResourceUtils.saveObject(doc, outputFile);	
	}
}
