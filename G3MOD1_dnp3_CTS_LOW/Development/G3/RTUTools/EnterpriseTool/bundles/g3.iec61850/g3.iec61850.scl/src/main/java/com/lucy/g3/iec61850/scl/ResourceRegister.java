package com.lucy.g3.iec61850.scl;

import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.util.SCLResourceFactoryImpl;
import com.lucy.g3.iec61850.scl.internal.ResourceUtils;

public class ResourceRegister {
	private static Logger log = Logger.getLogger(ResourceRegister.class);
	
	public static void init() {
		ResourceSet resourceSet = new ResourceSetImpl();
    	log.debug("Register Factory...");
    	resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
			(Resource.Factory.Registry.DEFAULT_EXTENSION, 
					new SCLResourceFactoryImpl());
    	log.debug("Register package...");
		resourceSet.getPackageRegistry().put
			(SCLPackage.eNS_URI, 
					SCLPackage.eINSTANCE);
		
		ResourceUtils.setResourceManager(resourceSet);
	}
}
