package com.lucy.g3.iec61850.scl.internal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.ParseException;
import java.util.HashMap;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

public class ResourceUtils {
	
	private static ResourceSet resourceSet = new ResourceSetImpl();
	
	public static void setResourceManager(final ResourceSet rs) {
		resourceSet = rs;
	}
	
	public static void check() {
		if (resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().size() < 1)
			throw new IllegalStateException("Ecore Initialising Error: Cannot find any ResourceFactory in register");
		if (resourceSet.getPackageRegistry().size() < 1)
			throw new IllegalStateException("Ecore Initialising Error: Cannot find any Package in register");
	}
	
	public static ExtendedMetaData getMetaData() {
		ExtendedMetaData metaData =
	       	     new BasicExtendedMetaData(resourceSet.getPackageRegistry());
		resourceSet.getLoadOptions().put
	       	    		(XMLResource.OPTION_EXTENDED_META_DATA, metaData);
		return metaData;
	}

	/**
	 * Serialize the EObject in XMI and return as String.
	 * 
	 * @param object
	 *            the object to serialize
	 * @return the String representation of serialized EObject
	 * @throws IOException
	 */
	public static String serialize(EObject object) throws IOException {
		// Store resource
		StringWriter out = new StringWriter();
		serialize(object, out);
		return out.toString();
	}

	/**
	 * Serialize the EObject in the given Writer.
	 * @param object
	 * @param out
	 * @throws IOException
	 */
	public static void serialize(EObject object, Writer out) throws IOException {
		XMLResource resource = null;
		if (object.eResource() != null) {
			resource = (XMLResource) object.eResource();
		} else {
			resource = new XMIResourceImpl();
			resource.getContents().add(object);
		}
		// Store resource
		saveResource(resource, out);
	}

	/**
	 * Load a serialized EObject
	 * @param file
	 * @return
	 * @throws Exception 
	 */
	public static EObject loadObject(String file) throws Exception {
		return loadObject(new File(file));
	}

	/**
	 * Load a serialized EObject
	 * @param file
	 * @return
	 * @throws Exception 
	 */
	public static EObject loadObject(File file) throws Exception {
		if (!file.exists())
			throw new FileNotFoundException();
		URI uri = URI.createFileURI(file.getAbsolutePath());
		return loadObject(uri);
	}

	/**
	 * Load a serialized EObject
	 * @param file
	 * @return
	 * @throws Exception 
	 */
	public static EObject loadObject(URI uri) throws Exception {
		Resource resource = resourceSet.getResource(uri, true);
		validate(resource);
		return resource.getContents().get(0);
	}
	
	/**
	 * 
	 * @param resource
	 * @throws Exception
	 */
	public static void validate(Resource resource) throws Exception {

		if (resource == null || !resource.isLoaded())
			throw new ParseException("XML Resource not loaded properly", 0);

		// sample work
		for (EObject eObject : resource.getContents()) {
			Diagnostic diagnostic = Diagnostician.INSTANCE.validate(eObject);
			if (diagnostic.getSeverity() != Diagnostic.OK) {
				System.out.println(diagnostic.getMessage());
				for (Diagnostic child : diagnostic.getChildren()) {
					System.out.println("  " + child.getMessage());
				}
			}
		}

	}

	/**
	 * Save an EMF resource
	 * @param resource
	 * @throws IOException
	 */
	public static void saveResource(Resource resource) throws IOException {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put(XMLResource.OPTION_SKIP_ESCAPE_URI, Boolean.FALSE);
		map.put(XMLResource.OPTION_ENCODING, "UTF-8");
		resource.save(map);

	}

	/**
	 * Save an EMF resource
	 * @param resource
	 * @param out
	 * @throws IOException
	 */
	public static void saveResource(XMLResource resource, Writer out)
			throws IOException {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put(XMLResource.OPTION_SKIP_ESCAPE_URI, Boolean.FALSE);
		map.put(XMLResource.OPTION_ENCODING, "UTF-8");
		resource.save(out, map);
	}
	
    

	/**
	 * Save an EMF resource
	 * @param object
	 * @param file
	 * @throws IOException
	 */
	public static void saveObject(EObject object, String file)
			throws IOException {
		saveObject(object, new File(file));
	}

	/**
	 * Save an EObject in a file
	 * @param object
	 * @param file
	 * @throws IOException
	 */
	public static void saveObject(EObject object, File file) throws IOException {
		URI uri = URI.createFileURI(file.getAbsolutePath());
		Resource resource = resourceSet.createResource(uri);
		resource.getContents().add(object);
		saveResource(resource);
	}
	

}
