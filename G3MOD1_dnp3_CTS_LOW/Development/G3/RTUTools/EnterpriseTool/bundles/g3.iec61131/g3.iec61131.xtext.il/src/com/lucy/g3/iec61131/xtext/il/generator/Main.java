
package com.lucy.g3.iec61131.xtext.il.generator;

import java.io.IOException;

public class Main {

  public static void main(String[] args) {
    if (args.length == 0) {
      System.err
          .println("Aborting: no path to EMF resource provided!");
      System.exit(-1);
    }
    
    try {
      if (args.length == 1) {
        // argument input file
        Generator.generate(args[0]);
      } else if (args.length > 1) {
        // argument input file and destination path
        Generator.generate(args[0], args[1]);
      }
      System.exit(0);
    } catch (Exception e) {
      System.err.println(e.getMessage() == null ? e.getClass().getName() : e.getMessage());
      System.exit(-1);
    }
    
  }

}
