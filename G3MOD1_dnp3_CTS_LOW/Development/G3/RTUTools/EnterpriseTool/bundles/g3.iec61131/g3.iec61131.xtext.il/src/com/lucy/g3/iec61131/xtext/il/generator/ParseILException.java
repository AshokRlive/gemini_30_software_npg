
package com.lucy.g3.iec61131.xtext.il.generator;

import org.eclipse.emf.common.util.URI;
import org.eclipse.xtext.diagnostics.Severity;
import org.eclipse.xtext.validation.CheckType;
import org.eclipse.xtext.validation.Issue;
import org.eclipse.xtext.validation.Issue.IssueImpl;

/**
 * 
 * 
 * Exception to handle syntax errors from XText IL parsing.
 * Reuses {@link Issue} from xtext validation
 * 
 * @author george_j
 * @see Issue
 */
public class ParseILException extends Exception implements Issue  {

  private static final long serialVersionUID = 1L;
  private Issue _this = null;
  
  public ParseILException(Issue impl) {
    super(impl.toString());
    _this = impl;
  }
  
  
  private ParseILException() {
    _this = new IssueImpl();
  }

  @Override
  public Severity getSeverity() {
    return _this.getSeverity();
  }

  @Override
  public String getCode() {
    return _this.getCode();
  }

  @Override
  public CheckType getType() {
    return _this.getType();
  }

  @Override
  public URI getUriToProblem() {
    return _this.getUriToProblem();
  }

  @Override
  public Integer getLineNumber() {
    return _this.getLineNumber();
  }

  @Override
  public Integer getColumn() {
    return _this.getColumn();
  }

  @Override
  public Integer getOffset() {
    return _this.getOffset();
  }

  @Override
  public Integer getLength() {
    return _this.getLength();
  }

  @Override
  public boolean isSyntaxError() {
    return _this.isSyntaxError();
  }

  @Override
  public String[] getData() {
    return _this.getData();
  }
  
  
}

