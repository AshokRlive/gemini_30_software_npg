/*
 * generated by Xtext
 */
package com.lucy.g3.iec61131.xtext.il;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class RuleEngineStandaloneSetup extends RuleEngineStandaloneSetupGenerated{

	public static void doSetup() {
		new RuleEngineStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

