package com.lucy.g3.iec61131.il.preferences;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.di.extensions.Preference;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

public class ILPreferences {

	private boolean modified = false;

	private String location = "I am here";

	public String getLocation() {
		return location;
	}

	@Inject
	public void setLocation(@Preference("password") String location) {
		this.location = location;
		this.modified = true;
		System.out.println(location);
	}

	@PostConstruct
	public void init() {
		if (location != null) {

		}
		modified = false;
	}

	@Focus
	void onFocus(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		if (modified) {
			if (MessageDialog.openQuestion(shell, "AccountInfos Modified",
					"The account informations have been modified would you like to reconnect with them?")) {
				init();
				if (location == null) {
					MessageDialog.openWarning(shell, "Connection failed",
							"Opening a connecting to the mail server failed.");
				}
			}
		}
	}
}
