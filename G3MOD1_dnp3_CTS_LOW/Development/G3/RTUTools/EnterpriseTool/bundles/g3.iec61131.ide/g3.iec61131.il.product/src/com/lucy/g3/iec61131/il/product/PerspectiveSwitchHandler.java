package com.lucy.g3.iec61131.il.product;

import java.util.List;

import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.advanced.impl.PerspectiveImpl;
import org.eclipse.e4.ui.model.application.ui.advanced.impl.PerspectiveStackImpl;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.model.application.ui.basic.MWindowElement;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

// switcher perspectives based on a command parameter 
public class PerspectiveSwitchHandler {

	@Execute
	public void switchPerspective(MWindow window, EPartService partService, EModelService modelService,
			@Named("perspective_parameter") String perspectiveId) {

		List<MWindowElement> children = window.getChildren();
		MWindowElement mWindowElement = children.get(0);
		if (mWindowElement instanceof PerspectiveStackImpl) {
			List<MPerspective> children2 = ((PerspectiveStackImpl) mWindowElement).getChildren();
			for (MPerspective perspective : children2) {
				if (perspective.getElementId().equals(perspectiveId)) {
					partService.switchPerspective(perspective);

				} else if (perspective.getElementId().contains(".<")) {
					int indexOf = perspective.getElementId().indexOf(".<");
					if (perspective.getElementId().substring(0, indexOf).equals(perspectiveId)) {
						partService.switchPerspective(perspective);
					}
				}
			}
		} else if (mWindowElement instanceof PerspectiveImpl) {
			for (MWindowElement perspective : children) {
				if (perspective.getElementId().equals(perspectiveId)) {
					partService.switchPerspective((MPerspective) perspective);
				}
			}
		}

	}
}
