package com.lucy.g3.iec61131.il.preferences;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.e4.core.di.extensions.Preference;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.osgi.service.prefs.BackingStoreException;

public class PreferenceDialog extends TitleAreaDialog {

	@Inject
	@Preference("location")
	private String location;

	private Text locationField;

	private Shell shell;
	
	@Inject
	public PreferenceDialog(@Named(IServiceConstants.ACTIVE_SHELL) Shell parentShell) {
		super(parentShell);
		this.shell = parentShell;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		getShell().setText("Preferences");
		setTitle("Preferences");
		setMessage("Configure preferences here");
		Composite container = new Composite(area, SWT.NONE);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));
		container.setLayout(new GridLayout(3, false));
		Label l = new Label(container, SWT.NONE);
		l.setText("location");
		locationField = new Text(container, SWT.BORDER);
		locationField.setText(location == null ? "" : location);
		locationField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Button btnBrowse = new Button(container, SWT.NONE);
		btnBrowse.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(shell, SWT.OPEN);
				dialog.setFilterExtensions(new String[] { "*.project" });
				dialog.setFilterPath("c:\\");
				String result = dialog.open();
				locationField.setText(result);
			}
		});
		btnBrowse.setText("Browse");
		return area;
	}

	@Override
	protected void okPressed() {
		IEclipsePreferences prefs = new InstanceScope().getNode("com.lucy.g3.iec61131.il.product");
		prefs.put("location", locationField.getText());
		try {
			prefs.flush();
			super.okPressed();
		} catch (BackingStoreException e) {
			ErrorDialog.openError(getShell(), "Error", "Error while storing preferences",
					new Status(IStatus.ERROR, "com.lucy.g3.iec61131.il.product", e.getMessage(), e));
		}
	}
}
