package com.lucy.g3.iec61131.debugger;

import java.nio.ByteBuffer;

import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DBG_ERR;
import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DBG_REPLY;
import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DebugState;
import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DebugVar;
import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DebugVarRef;

/**
 *  A reply message received from 61131 debug server.
 */
public class DBGReply {

  private int schemeID = -1;
  private DBG_REPLY id = null;
  private DebugState state = null;
  private DebugVar[] varList = null;
  private DBG_ERR err = null;
  private String libFileName = null;

  private DBGReply () {}
  
  public DebugState getState() {
    if(id != DBG_REPLY.STATE)
      throw new RuntimeException("Reply state unavailable");
    return state; 
  }

  @Override
  public String toString() {
    return String.format("%s",id == null ? "Invalid" : id.name());
  }
  
  public static DBGReply parseBytes(byte[] bytes) throws DBGMsgParserException{
    validate(bytes);
    
    ByteBuffer buf = ByteBuffer.wrap(bytes);
    buf.order(IDebuggerProtocol.RTU_BYTE_ORDER);
    
    DBGReply reply = new DBGReply();
    int replyID = buf.getInt();
    reply.id = DBG_REPLY.forValue(replyID );
    if(reply.id == null)
      throw new DBGMsgParserException("Unrecognised reply id:"+replyID);
    
    boolean validPayload = false;
    final int payloadSize = buf.remaining();
    switch (reply.id) {
    case SCHEMEID:
      if(payloadSize >= 4) {
        reply.schemeID = buf.getInt();
        validPayload = true;
      }
      break;
    
    case STATE:
      if(payloadSize >= DebugState.size()) {
        reply.state = new DebugState(
            buf.getInt(), 
            buf.getInt(), 
            buf.getInt());
        validPayload = true;
      }
      break;
      
    case VAR:
      if(payloadSize % DebugVar.size() == 0) {
        reply.varList= parseDebugVarReply(buf);
        validPayload = true;
      }
      break;
      
    case ACK:
      validPayload = true;
      break;
      
    case LIB_FILE_NAME:
      if(payloadSize > 0) {
        byte[] payloadBytes = new byte[payloadSize];
        buf.get(payloadBytes);
        reply.libFileName = new String(payloadBytes);
        validPayload = true;
      }
      break;
      
    case NACK:
      if(payloadSize >= 4) {
        reply.err = DBG_ERR.forValue(buf.getInt());
        validPayload = true;
      }
      break;
      
    default:
      break;
    }
    if(!validPayload)
      throw new DBGMsgParserException("Invalid Payload!");
    return reply;
  }
  
  private static DebugVar[] parseDebugVarReply(ByteBuffer buf) {
    int size = buf.remaining()/DebugVar.size();
    DebugVar[] varList = new DebugVar[size];
    for (int i = 0; i < varList.length; i++) {
      varList[i] = new DebugVar(new DebugVarRef(buf.get(), buf.get()),buf.getInt());
    }
    return varList ;
  }

  private static void validate(byte[] bytes) throws DBGMsgParserException {
    if (bytes == null || bytes.length == 0) {
      throw new DBGMsgParserException("No reply!");
      
    } else if(bytes.length < IDebuggerProtocol.DBG_MESSAGE_HEADER_SIZE) {
      throw new DBGMsgParserException("Invalid reply size:"+bytes.length);
    }
  }

  public int getSchemeID() {
    if(id != DBG_REPLY.SCHEMEID)
      throw new RuntimeException("Scheme ID unavailable");
    return schemeID;
  }
  
  public DBG_ERR getError() {
    return err;
  }

  public boolean isNack() {
    return id == DBG_REPLY.NACK;
  }
  
  public DebugVar[] getVarList() {
    return varList;
  }
  
  public String getLibFileName() {
    return libFileName;
  }
  
}
