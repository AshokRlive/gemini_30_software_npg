
package com.lucy.g3.iec61131.debugger;

import java.nio.ByteOrder;

/**
 * <p>The debug protocol for communicating with 61131 Automation Debug server.</p>
 * <strong>Note:</strong> this file derives from IDebuggerProtoco.h. They must be synched if there is any change.
 */
public interface IDebuggerProtocol {

  int DEBUG_PORT = 30023;
  
  int DBG_MESSAGE_HEADER_SIZE = 4;
  
  ByteOrder RTU_BYTE_ORDER = ByteOrder.LITTLE_ENDIAN;

  enum DBG_CMD {
    STEP,
    PAUSE,
    RESUME,
    SET_BREAKPOINT,
    DEL_BREAKPOINT,
    GET_STATE,
    GET_SCHEMEID,
    GET_VAR,
    GET_LIB_FILE_NAME;
    
    public int getValue() {
      return ordinal();
    }
  };

  enum DBG_ERR {
    DBG_ERR_NONE,
    DBG_ERR_BP_ALREADY_EXIST,
    DBG_ERR_BP_NOT_EXIST,
    DBG_ERR_BP_REACH_MAX,
    DBG_ERR_STEP_FAIL,
    DBG_ERR_UNSUPPORTED,
    DBG_ERR_INVALID_PAYLOAD;
    
    public int getValue() {
      return ordinal();
    }
    
    public static DBG_ERR forValue(int value) {
      DBG_ERR [] values = DBG_ERR.values();
      
      if(value >=0 && value <= values.length) {
        return values[value];
      }
      return null;
    }
  }
  
  enum DBG_REPLY {
    ACK,
    NACK,
    STATE,
    SCHEMEID,
    VAR,
    LIB_FILE_NAME; // String
    
    public static DBG_REPLY forValue(int value) {
      DBG_REPLY [] values = DBG_REPLY.values();
      
      if(value >=0 && value <= values.length) {
        return values[value];
      }
      
      return null;
    }
  };
  
  
  enum  DBG_STATE{
    NOT_RUNNING,
    RUNNING,
    SUSPENDED,
    STOPPED;
    
    public static DBG_STATE forValue(int value) {
      DBG_STATE [] values = DBG_STATE.values();
      
      if(value >=0 && value <= values.length) {
        return values[value];
      }
      
      return null;
    }
    
    public int getValue() {
      return ordinal();
    }
    
    @Override
    public String toString() {
      return name();
    }
  };
  
  enum DBG_VAR 
  {
      DBG_VAR_INPUT("INPUT"),
      DBG_VAR_CONSTANT("CONSTANT"),
      DBG_VAR_OUTPUT("OUTPUT");
      
      DBG_VAR(String description) {
        this.description = description;
      }
      
      @Override
      public String toString() {
        return description;
      }
      
      final String description;
  };

  
  class DebugVarRef 
  {
    public final byte  type;    /*DBG_VAR */
    public final byte  index;
    
    public DebugVarRef(DBG_VAR type, byte index) {
      this((byte) type.ordinal(), index);
    }
    
    public DebugVarRef(byte type, byte index) {
      super();
      this.type = type;
      this.index = index;
    }
    
    @Override
    public String toString() {
      return "DebugVarRef [type=" + DBG_VAR.values()[type] + ", index=" + index + "]";
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + index;
      result = prime * result + type;
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      DebugVarRef other = (DebugVarRef) obj;
      if (index != other.index)
        return false;
      if (type != other.type)
        return false;
      return true;
    }

    public static int size() {
      return 2;
    }
  };

  /* The struct of debug variable.*/
  class DebugVar
  {

    public final DebugVarRef ref;
    public final int value;

    public DebugVar(DebugVarRef ref, int value) {
      super();
      this.ref = ref;
      this.value = value;
    }


    public static int size() {
      return 6;
    }
  };
  
  class DebugState{
     public final int curState;   /*DBG_STATE*/
     
     /**
      * Line number start from 0.
      */
     private final int curLine;    /*lu_uint32_t*/ 
     public final int cr;         /*lu_int32_t */
     
    public DebugState(int curState, int curLine, int cr) {
      this.curState = curState;
      this.curLine = curLine;
      this.cr = cr;
    }
    
    /**
     * @return line number start from one.
     */
    public int curLineFromOne() {
      return curLine + 1 ;
    }
    
    public DBG_STATE getState() {
      return DBG_STATE.forValue(curState);
    }
    
    public static int size() {
      return 4 * 3; // bytes
    }
  }
}
