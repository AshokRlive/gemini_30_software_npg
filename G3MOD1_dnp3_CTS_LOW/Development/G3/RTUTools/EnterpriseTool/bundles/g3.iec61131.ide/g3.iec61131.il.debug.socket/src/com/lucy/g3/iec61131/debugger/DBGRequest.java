/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.debugger;

import java.nio.ByteBuffer;

import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DBG_CMD;

/**
 * A request message to 61131 debug server.
 */
public class DBGRequest {

  private final ByteBuffer buf;
  private DBG_CMD cmd;


  public DBGRequest(DBG_CMD cmd) {
    if (cmd == null)
      throw new NullPointerException("cmd must not be null");
    this.cmd = cmd;
    buf = ByteBuffer.allocate(1024);
    buf.order(IDebuggerProtocol.RTU_BYTE_ORDER);
    buf.putInt(cmd.getValue());// Request ID
  }

  public void putPayloadInt(int value) {
    buf.putInt(value);
  }

  public void putPayloadBytes(byte[] value) {
    buf.put(value);
  }

  ByteBuffer getBuf() {
    return buf;
  }
  
  public byte[] toBytes() {
    byte[] bytes = new byte[buf.position()];
    buf.position(0);
    buf.get(bytes);
    return bytes;
  }

  @Override
  public String toString() {
    return String.format("%s", cmd);
  }

}
