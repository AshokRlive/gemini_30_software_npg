/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.debugger;

import java.io.IOException;


/**
 *
 */
public class DBGMsgParserException extends IOException {

  public DBGMsgParserException() {
    super();
  }

  public DBGMsgParserException(String message, Throwable cause) {
    super(message, cause);
  }

  public DBGMsgParserException(String message) {
    super(message);
  }

  public DBGMsgParserException(Throwable cause) {
    super(cause);
  }

}

