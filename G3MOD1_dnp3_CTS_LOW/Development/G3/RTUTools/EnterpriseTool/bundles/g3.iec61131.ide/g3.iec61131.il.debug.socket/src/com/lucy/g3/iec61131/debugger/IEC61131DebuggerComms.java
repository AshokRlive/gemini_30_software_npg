/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.debugger;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;

/**
 * This class is for handling the communication to IEC61131 debugger server.
 */
public class IEC61131DebuggerComms implements IIEC61131DebuggerComms {

  private final static int CONNECT_TIMEOUT_MS = 1000;
  
  private static final String SERVER_REPLY_ERROR = "Sorry";
  private String ip = "";
  private int port = IDebuggerProtocol.DEBUG_PORT;
  
  private Socket socket;
  
  public IEC61131DebuggerComms(String hostIP){
    this.ip = hostIP;
  }
  
  public IEC61131DebuggerComms(){
    this("");
  }
  
  @Override
  public boolean isConnected() {
    return socket != null && !socket.isClosed();
  }
  
  @Override
  public String connect() throws IOException {
    if (!isConnected()) {
      try {
        /* Connect socket */
        socket = new Socket();
        socket.connect(new InetSocketAddress(ip, port), CONNECT_TIMEOUT_MS);

        /* Server accepted, now read reply from server */
        String reply = new String(readSocket());
        if (reply.contains(SERVER_REPLY_ERROR)) {
          throw new IOException(reply);
        }else{
          return reply;
        }

      } catch (IOException e) {
        
        /* Close socket always when error occurs*/
        if (socket != null) {
          try {
            socket.close();
          } catch (IOException e1) {
          }
          socket = null;
          throw e;
        }
      }
    } else {
      throw new IOException("Already connected");
    }
    
    return null;
  }

  @Override
  public void disconnect() {
    if (socket != null) {
      Socket toBeClosed = socket;
      socket = null;
      try {
        toBeClosed.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void setHost(String ip) throws IOException{
    setHost(ip, IDebuggerProtocol.DEBUG_PORT);
  }
  
  @Override
  public void setHost(String ip, int port) throws IOException{
    if(ip == null)
      return;
    
    String oldIP = this.ip;
    int oldPort = this.port;
    this.ip = ip;
    this.port = port;
    
    // Update connected socket with the new address
    if(socket != null
        && (!oldIP.equals(ip) || oldPort != port)) {
        socket.connect(new InetSocketAddress(ip, port), CONNECT_TIMEOUT_MS);
    }
  }

  @Override
  public String getIp() {
    return ip;
  }

  public int getPort() {
    return port;
  }

  @Override
  public DBGReply sendRequest(DBG_CMD cmd, int... payloadData) throws IOException {
    DBGRequest request = new DBGRequest(cmd);
    for (int i = 0; i < payloadData.length; i++) {
      request.putPayloadInt(payloadData[i]);
    }
    return sendRequest(request);
  }
  
  @Override
  public DBGReply sendRequest(DBG_CMD cmd, byte... payloadData) throws IOException {
    DBGRequest request = new DBGRequest(cmd);
    request.putPayloadBytes(payloadData);
    
    return sendRequest(request);
  }
  
  @Override
  public DBGReply sendRequest(DBGRequest request) throws IOException {
    if (socket == null) {
      throw new IOException("No conneciton!");
    }

    byte[] rcv = sendRawData(request.toBytes());

    DBGReply reply = DBGReply.parseBytes(rcv);
    
    if(reply.isNack())
      throw new ProtocolException(String.format("NACK reply[%s] for the request:%s", reply.getError(), request.toString()));
    
    return reply;
  }

  private byte[] sendRawData(byte[] data) throws UnknownHostException, IOException {
    writeSocket(data);
    return readSocket();
  }

  private void writeSocket(byte[] data) throws IOException {
    OutputStream out = socket.getOutputStream();
    out.write(data);
  }

  private byte[] readSocket() throws IOException {
    BufferedInputStream in =
        new BufferedInputStream(socket.getInputStream());
    byte[] buffer = new byte[1024];
    int nbytes = in.read(buffer, 0, buffer.length);

    if (nbytes > 0)
      return Arrays.copyOf(buffer, nbytes);
    return new byte[0];
  }


  @Override
  public void pause() throws IOException {
    sendRequest(DBG_CMD.PAUSE);
  }

  @Override
  public void resume()throws IOException {
    sendRequest(DBG_CMD.RESUME);
  }

  @Override
  public void stepover()throws IOException {
    sendRequest(DBG_CMD.STEP);
  }
  
  @Override
  public void setBreakpoint(int breakpointLine)throws IOException {
    sendRequest(DBG_CMD.SET_BREAKPOINT, breakpointLine);
  }
  
  @Override
  public void delBreakpoint(int breakpointLine)throws IOException {
    sendRequest(DBG_CMD.DEL_BREAKPOINT, breakpointLine);
  }
  
  @Override
  public DebugState getCurrentState()throws IOException {
    DBGReply reply = sendRequest(DBG_CMD.GET_STATE);
    return reply == null ? null : reply.getState();
  }
  
  public int getSchemeID()throws IOException {
    DBGReply reply = sendRequest(DBG_CMD.GET_SCHEMEID);
    return reply == null ? -1: reply.getSchemeID();
  }

  @Override
  public DebugVar[] getVariable(DebugVarRef[] varList) throws IOException {
    DBGRequest request = new DBGRequest(DBG_CMD.GET_VAR);
    for (int i = 0; varList != null && i < varList.length; i++) {
      request.putPayloadBytes(new byte[]{varList[i].type, varList[i].index});
    }
    
    DBGReply reply = sendRequest(request);
    return reply.getVarList();
  }

  @Override
  public String getLibraryFileName() throws IOException {
    DBGRequest request = new DBGRequest(DBG_CMD.GET_LIB_FILE_NAME);
    DBGReply reply = sendRequest(request);
    return reply.getLibFileName();
  }
}
