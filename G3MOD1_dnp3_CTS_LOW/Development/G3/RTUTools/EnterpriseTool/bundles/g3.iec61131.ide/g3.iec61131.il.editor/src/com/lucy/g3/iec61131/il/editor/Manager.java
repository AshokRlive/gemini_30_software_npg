package com.lucy.g3.iec61131.il.editor;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IPerspectiveListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

public class Manager {
	@Inject
	UISynchronize sync;
	IProject project;
	private IWorkspaceRoot root;
	private String ilLocation = "";

	private void createPerspectiveListener() {
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().addPerspectiveListener(new IPerspectiveListener() {

			@Override
			public void perspectiveChanged(IWorkbenchPage page, IPerspectiveDescriptor perspective, String changeId) {
			}

			@Override
			public void perspectiveActivated(IWorkbenchPage page, IPerspectiveDescriptor perspective) {
				if (perspective.getLabel().toString().equals("<IL Editor>")) {
					IEclipsePreferences prefs = new InstanceScope().getNode("org.lucy.g3.iec61131.il.product");
					ilLocation = prefs.get("location", "");
					if (!ilLocation.equals("")) {
						checkExistsAndImport("automationSchemes");
					}
				}
			}
		});
	}

	@PostConstruct
	void postContextCreate(IApplicationContext appContext, Display display) {
		Job job = new Job("Editor Perspective Listener") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						root = ResourcesPlugin.getWorkspace().getRoot();
						createPerspectiveListener();
					}
				});
				return Status.OK_STATUS;
			}
		};

		job.schedule();

	}

	public void checkExistsAndImport(String name) {
		if (!root.getProject(name).exists()) {
			try {
				importExisitingProject();
			} catch (Throwable e) {

			}
		}
	}

	public void importExisitingProject() throws CoreException {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IPath projectpath = new Path(ilLocation);
		final IProjectDescription description = workspace.loadProjectDescription(projectpath);
		final IProject project = root.getProject(description.getName());
		if (!project.exists()) {
			IWorkspaceRunnable runnable = new IWorkspaceRunnable() {
				@Override
				public void run(IProgressMonitor monitor) throws CoreException {
					project.create(description, monitor);
					project.open(IResource.NONE, monitor);
				}
			};
			workspace.run(runnable, workspace.getRuleFactory().modifyRule(workspace.getRoot()), IResource.NONE, null);
		}
	}
}
