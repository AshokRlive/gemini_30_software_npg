 
package com.lucy.g3.iec61131.il.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class Intro {
	@Inject
	public Intro(Composite parent) {
		Composite outerContainer = new Composite(parent, SWT.NONE);
        GridLayout gridLayout = new GridLayout();
        outerContainer.setLayout(gridLayout);
        outerContainer.setBackground(outerContainer.getDisplay().getSystemColor(
             SWT.COLOR_TITLE_BACKGROUND_GRADIENT));
        Label label = new Label(outerContainer, SWT.CENTER);
        label.setText("G3 IEC61131-3 IL GENERATOR TOOL");
        GridData gd = new GridData(GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL);
        gd.horizontalAlignment = GridData.CENTER;
        gd.verticalAlignment = GridData.CENTER;
        label.setLayoutData(gd);
        label.setBackground(outerContainer.getDisplay().getSystemColor(
             SWT.COLOR_TITLE_BACKGROUND_GRADIENT));
	}
	
	@PostConstruct
	public void postConstruct(Composite parent) {
		
	}
	
	
	
	
}