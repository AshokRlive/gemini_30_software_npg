/*****************************************************************
 * Copyright (c) 2009, 2013 Texas Instruments and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Patrick Chuong (Texas Instruments) - Initial API and implementation (Bug 238956)
 *     IBM Corporation - ongoing enhancements and bug fixing
 *****************************************************************/
package com.lucy.g3.iec61131.il.debug.parts;

import org.eclipse.debug.internal.ui.views.breakpoints.BreakpointsView;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Composite;

/**
 * This class implements the breakpoints view.
 */
public class ILBreakpointsView extends BreakpointsView {

	public ILBreakpointsView() {

	}

	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
	}

	@Override
	public void setFocus() {
		super.setFocus();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.debug.internal.ui.views.variables.VariablesView#createViewer(
	 * org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public Viewer createViewer(Composite parent) {
		Viewer viewer = super.createViewer(parent);
		return viewer;
	}

	protected boolean isDetailPaneVisible() {
		return false;
	}
	
	@Override
	protected void createActions() {
		super.createActions();
	}

}