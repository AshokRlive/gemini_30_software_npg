package com.lucy.g3.iec61131.il.debug;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.LineBreakpoint;


public class TextLineBreakpoint extends LineBreakpoint {

	/**
	 * Default constructor. Used by the debug framework to restore stored
	 * breakpoints.
	 */
	public TextLineBreakpoint() {
		
	}

	/**
	 * Constructs a line breakpoint on the given resource at the given line
	 * number. The line number is 1-based (i.e. the first line of a file is line
	 * number 1).
	 * 
	 * @param resource
	 *            resource on which to set the breakpoint
	 * @param lineNumber
	 *            1-based line number of the breakpoint
	 * @throws CoreException
	 *             if unable to create the breakpoint
	 */
	public TextLineBreakpoint(final IResource resource, final int lineNumber) throws CoreException {
		this(resource, lineNumber, true);
	}

	protected TextLineBreakpoint(final IResource resource, final int lineNumber, final boolean persistent) throws CoreException {
		IWorkspaceRunnable runnable = new IWorkspaceRunnable() {
			@Override
			public void run(IProgressMonitor monitor) throws CoreException {
				IMarker marker = resource.createMarker("com.lucy.g3.iec61131.il.debug.textLineBreakpointMarker");
				setMarker(marker);
				marker.setAttribute(IBreakpoint.ENABLED, true);
				marker.setAttribute(IBreakpoint.PERSISTED, persistent);
				marker.setAttribute(IMarker.LINE_NUMBER, lineNumber);
				marker.setAttribute(IBreakpoint.ID, getModelIdentifier());
				marker.setAttribute(IMarker.MESSAGE, "Line Breakpoint: " + resource.getName() + " [line: " + lineNumber + "]");
			}
		};
		run(getMarkerRule(resource), runnable);
	}

	@Override
	public String getModelIdentifier() {
		return TextDebugModelPresentation.ID;
	}
}
