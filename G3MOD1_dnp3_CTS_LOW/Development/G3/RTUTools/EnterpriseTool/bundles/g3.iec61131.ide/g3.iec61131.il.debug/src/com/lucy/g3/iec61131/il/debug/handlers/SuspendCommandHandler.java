
package com.lucy.g3.iec61131.il.debug.handlers;

import java.io.IOException;

import javax.inject.Inject;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;

import com.lucy.g3.iec61131.debugger.IIEC61131DebuggerComms;
import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DBG_STATE;
import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DebugState;

public class SuspendCommandHandler {

	@Inject
	private IIEC61131DebuggerComms comms;

	@Execute
	public void execute() {
		try {
      comms.pause();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

	}
	
  @CanExecute
  public boolean canExecute() {
      try {
          DebugState state = comms.getCurrentState();
          if (DBG_STATE.forValue(state.curState) == DBG_STATE.RUNNING)
            return true;
          return false;
        } catch (IOException e) {
          // System.err.println("No connection found");
          return false;
        }
  }

}
