
package com.lucy.g3.iec61131.il.debug.handlers;

import javax.inject.Inject;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.swt.widgets.Display;

import com.lucy.g3.iec61131.debugger.IIEC61131DebuggerComms;
import com.lucy.g3.iec61131.il.debug.DebugConstants;

public class DisconnectCommandHandler {

	@Inject
	private IIEC61131DebuggerComms comms;

	@Execute
	public void execute() {
		// TODO Auto-generated method stub
	  DebugConstants.CURRENT_POLL_RATE = -1;
	  comms.disconnect();
		System.out.println("Disconnected");
		
		

	}

}
