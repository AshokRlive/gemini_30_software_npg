
package com.lucy.g3.iec61131.il.debug.parts;


import javax.annotation.PostConstruct;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import com.lucy.g3.iec61131.il.debug.DebugConstants;

public class IPAddressToolItem {
  @PostConstruct
  public void createControls(Composite parent) {
    final Composite comp = new Composite(parent, SWT.NONE);
    comp.setLayout(new GridLayout());
    final Text text = new Text(comp, SWT.SEARCH | SWT.CANCEL
        | SWT.BORDER);
    text.setMessage("IP Address");
    text.setText(DebugConstants.DEFAULT_IP_ADDRESS);
    
    text.addModifyListener(new ModifyListener() {
      @Override
      public void modifyText(ModifyEvent e) {
        DebugConstants.DEFAULT_IP_ADDRESS = text.getText();
      }
    });
    
    GridDataFactory.fillDefaults().hint(130, SWT.DEFAULT).applyTo(text);

  }
} 

