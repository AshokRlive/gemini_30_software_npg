import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.lucy.g3.iec61131.il.debug.perspective.WSPerspectiveAdapter;

public class Activator extends AbstractUIPlugin {

	public Activator() {
		// TODO Auto-generated constructor stub
	}
	
	public void start(BundleContext context) throws Exception {
	  super.start(context);
	  
  	// Add a listener to the workbench to listen for perspective change and
  	// perform custom actions
  	// Add the listener once the workbenchh is fully started
  	Display.getDefault().asyncExec(new Runnable() {
  	    @Override
  	    public void run() {
  	        // wait until the workbench has been initialized
  	        // if the workbench is still starting, reschedule the execution
  	        // of this runnable
  	        if (PlatformUI.getWorkbench().isStarting()) {
  	            Display.getDefault().timerExec(1000, this);
  	        } else { 
  	            // the workbench finished the initialization process 
  	            IWorkbenchWindow workbenchWindow = PlatformUI
  	                    .getWorkbench().getActiveWorkbenchWindow();
  	            WSPerspectiveAdapter wsPerspectiveListener = new WSPerspectiveAdapter();
//  	            workbenchWindow
//  	                    .addPerspectiveListener(wsPerspectiveListener);
  	            
  	            System.out.println(PlatformUI.getWorkbench()
                .getActiveWorkbenchWindow().getActivePage()
                .getPerspective().getId());
  	            
  	            
  	            // update the PerspectiveListener with the current perspective
  	            // we need to perform this update by hand as no event is
  	            // sent by the platform at
  	            // platform initialization and we need to have the current
  	            // perspective set
//  	            wsPerspectiveListener.perspectiveActivated(PlatformUI
//  	                    .getWorkbench().getActiveWorkbenchWindow()
//  	                    .getActivePage(), PlatformUI.getWorkbench()
//  	                    .getActiveWorkbenchWindow().getActivePage()
//  	                    .getPerspective());
  	        }
  	    }// else - isStarting()
  	});
	  
	}

}
