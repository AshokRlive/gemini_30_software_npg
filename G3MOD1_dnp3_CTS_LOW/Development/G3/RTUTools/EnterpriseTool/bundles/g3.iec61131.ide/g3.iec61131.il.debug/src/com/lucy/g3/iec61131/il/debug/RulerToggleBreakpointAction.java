package com.lucy.g3.iec61131.il.debug;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.ILineBreakpoint;
import org.eclipse.debug.ui.actions.IToggleBreakpointsTarget;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.text.source.IVerticalRulerInfo;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.texteditor.AbstractRulerActionDelegate;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

import com.lucy.g3.iec61131.debugger.IIEC61131DebuggerComms;

public class RulerToggleBreakpointAction extends AbstractRulerActionDelegate implements IEditorActionDelegate {
	private ITextEditor mEditor = null;
	private IVerticalRulerInfo mRulerInfo = null;

	private IIEC61131DebuggerComms comms;
	
	
	public RulerToggleBreakpointAction() {

	}

	@Override
	public void mouseDoubleClick(MouseEvent e) {
		breakpointAction();
	}

	public void setSocket(IIEC61131DebuggerComms socket) {
		this.comms = (IIEC61131DebuggerComms) socket;
	}

	public void breakpointAction() {
		IDocumentProvider provider = mEditor.getDocumentProvider();
		ITextSelection selection = null;
		int line = mRulerInfo.getLineOfLastMouseButtonActivity();
		try {
			provider.connect(this);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		IDocument document = provider.getDocument(mEditor.getEditorInput());
		IRegion region = null;
		try {
			if (line >= 0) {
				region = document.getLineInformation(line);
			}
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (region != null) {
			selection = new TextSelection(document, region.getOffset(), region.getLength());
		}
		if (selection != null) {
			IToggleBreakpointsTarget target = (IToggleBreakpointsTarget) mEditor
					.getAdapter(IToggleBreakpointsTarget.class);
			try {
				if ((target != null) && (target.canToggleLineBreakpoints(mEditor, selection)))
					target.toggleLineBreakpoints(mEditor, selection);
				IResource resource = (IResource) mEditor.getEditorInput().getAdapter(IResource.class);
				ITextSelection textSelection = (ITextSelection) selection;
				int lineNumber = textSelection.getStartLine();
				IBreakpoint[] breakpoints = DebugPlugin.getDefault().getBreakpointManager()
						.getBreakpoints(TextDebugModelPresentation.ID);

				boolean exists = false;

				for (int i = 0; i < breakpoints.length; i++) {
					IBreakpoint breakpoint = breakpoints[i];
					if (resource.equals(breakpoint.getMarker().getResource())) {
						if (((ILineBreakpoint) breakpoint).getLineNumber() == (lineNumber + 1)) {
							breakpoint.delete();
							comms.delBreakpoint(lineNumber + 1);
							exists = true;
							// return;
						}
					}
				}

				// update breakpoints in automation scheme engine

				if (!exists) {
					TextLineBreakpoint lineBreakpoint = new TextLineBreakpoint(resource, lineNumber + 1);
					DebugPlugin.getDefault().getBreakpointManager().addBreakpoint(lineBreakpoint);
					comms.setBreakpoint(lineNumber + 1);
				}
				
				breakpoints = DebugPlugin.getDefault().getBreakpointManager()
						.getBreakpoints(TextDebugModelPresentation.ID);


			} catch (Exception e3) {
			}
		}
	}


	private class ToggleBreakpointAction extends Action {

		public ToggleBreakpointAction(ITextEditor editor, IVerticalRulerInfo rulerInfo) {
			super("Toggle line breakpoint");
			mEditor = editor;
			mRulerInfo = rulerInfo;
		}

		@Override
		public void run() {
			breakpointAction();
		}
	}

	@Override
	protected IAction createAction(ITextEditor editor, IVerticalRulerInfo rulerInfo) {
		return new ToggleBreakpointAction(editor, rulerInfo);
	}
}
