
package com.lucy.g3.iec61131.il.debug.handlers;


import java.io.IOException;
import java.util.Map.Entry;

import javax.inject.Inject;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.internal.contexts.EclipseContext;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

import com.lucy.g3.iec61131.il.debug.DebugConstants;

import com.lucy.g3.iec61131.debugger.IIEC61131DebuggerComms;
import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DBG_CMD;
import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DBG_STATE;
import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DebugState;



public class ResumeCommandHandler  {

  @Inject
  private IIEC61131DebuggerComms comms;
  @Inject
	IEventBroker broker;
  
  @Inject
	private EPartService partService;
  
  private Runnable playRunnable;
  private boolean running = false;
  
  @Execute
  public void execute() {
    try {
      comms.resume();
      broker.send(DBG_CMD.RESUME.toString(), null);
    } catch (IOException e) {
      e.printStackTrace();
    }
    final ITextEditor t = (ITextEditor) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
    
    DebugConstants.CURRENT_POLL_RATE = DebugConstants.DEFAULT_POLL_RATE;
    
	final Display display = Display.getCurrent();
	running = true;
	playRunnable = new Runnable() {
		public void run() {
			if (running) {
			  try {
				  DebugState state = comms.getCurrentState();
					highlightLine(t, state.curLineFromOne());
					// System.out.println("line: "+ state.curLineFromOne() + " cr=" + state.cr);
					
			  } catch(IOException e) {
			    // e.printStackTrace();
			  }
				

				display.timerExec(DebugConstants.CURRENT_POLL_RATE, this);
			}
		}
	};
	
	display.timerExec(DebugConstants.CURRENT_POLL_RATE, playRunnable);
    
    
  }
  
  private void highlightLine(ITextEditor currentTextEditor, int line) {
		IDocumentProvider provider = currentTextEditor.getDocumentProvider();
		try {
			provider.connect(this);
			IDocument document = provider.getDocument(currentTextEditor.getEditorInput());
			IRegion region = document.getLineInformation(line);
			currentTextEditor.selectAndReveal(region.getOffset(), region.getLength());
		} catch (CoreException e1) {
		} catch (BadLocationException e2) {
		} finally {
			// provider.disconnect(this);
		}
	}
  
  
  @CanExecute
  public boolean canExecute() {
      try {
          DebugState state = comms.getCurrentState();
          System.out.println(DBG_STATE.forValue(state.curState));
          return true;
        } catch (IOException e) {
      	  // System.err.println("No connection found");
          return false;
        }
  }


}

