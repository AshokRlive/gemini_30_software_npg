
package com.lucy.g3.iec61131.il.debug.handlers;


import java.io.IOException;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.dialogs.MessageDialog;

import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DebugState;
import com.lucy.g3.iec61131.debugger.IIEC61131DebuggerComms;
import com.lucy.g3.iec61131.il.debug.DebugConstants;



public class ConnectCommandHandler  {

  @Inject
  private IIEC61131DebuggerComms comms;
  
  @Execute
  public void execute() {
    // TODO Auto-generated method stub
    try {
      comms.setHost(DebugConstants.DEFAULT_IP_ADDRESS);
      comms.connect();
    } catch (IOException e) {
      MessageDialog.openError(null, "Error Occured", "Caused by: " + e.getMessage());
    }
    
    
  }
  
  
  @CanExecute
  public boolean canExecute(final IEclipseContext ictx) {
    
      try {
        DebugState state = comms.getCurrentState();
        if (state != null)
        	return false;
      } catch (IOException e) {
        return true;
      }
    
    return true;
  }
  

}

