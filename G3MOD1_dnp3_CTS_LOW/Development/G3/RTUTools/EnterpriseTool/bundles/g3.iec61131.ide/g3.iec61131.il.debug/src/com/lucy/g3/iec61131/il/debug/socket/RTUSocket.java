package com.lucy.g3.iec61131.il.debug.socket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class RTUSocket {
	
	Socket echoSocket;
	PrintWriter out;
	BufferedReader in;
	InputStreamReader isr;
	int portNumber = 22336;
	
	public Socket getSocket(){
		return echoSocket;
	}
	
	public void createSocket(String hostName) {
		try {
			echoSocket = new Socket(hostName, portNumber);
			//echoSocket.setSoTimeout(250);
			out = new PrintWriter(echoSocket.getOutputStream(), true);
			isr = new InputStreamReader(echoSocket.getInputStream());
			in = new BufferedReader(isr);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void closeSocket() {
		out.write("disconnect");
		out.flush();
		out.close();
		try {
			echoSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String readLine(){
		try {
			return in.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void connect(){
		out.write("connect");
		out.flush();
	}
	
	public void start(){
		out.write("start");
		out.flush();
	}
	
	public void step(){
		out.write("step");
		out.flush();
	}
	
	public void resume(){
		out.write("resume");
		out.flush();
	}
	
	public void pause(){
		out.write("pause");
		out.flush();
	}
	
	public void stop(){
		out.write("stop");
		out.flush();
	}
	
	public void send(String buffer){
		out.write(buffer);
		out.flush();
	}
	
	public boolean isClosed(){
		if(echoSocket!=null){
			return echoSocket.isClosed();
		}
		else{
			return false;
		}
	}
	
}
