
package com.lucy.g3.iec61131.il.debug;


import org.eclipse.e4.core.contexts.ContextFunction;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IContextFunction;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;

import com.lucy.g3.iec61131.debugger.IIEC61131DebuggerComms;
import com.lucy.g3.iec61131.debugger.IEC61131DebuggerComms;


public class AutoDebugProtocolContextFunction extends ContextFunction {
  @Override
  public Object compute(IEclipseContext context, String contextKey) {
	  IIEC61131DebuggerComms comms = 
        ContextInjectionFactory.make(IEC61131DebuggerComms.class, context);

    MApplication app = context.get(MApplication.class);
    IEclipseContext appCtx = app.getContext();
    appCtx.set(IIEC61131DebuggerComms.class, comms);

    return comms;
  }
}
