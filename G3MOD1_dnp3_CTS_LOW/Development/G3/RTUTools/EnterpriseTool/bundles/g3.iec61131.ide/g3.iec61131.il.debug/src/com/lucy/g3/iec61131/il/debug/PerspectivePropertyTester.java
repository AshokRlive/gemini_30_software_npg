
package com.lucy.g3.iec61131.il.debug;

import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.core.expressions.PropertyTester;

/**
 * Property tester that checks the <code>elementId</code> of the currently active perspective
 */
public class PerspectivePropertyTester extends PropertyTester {

    /**
     * @param receiver the currently active {@link MPerspective}
     * @param property the property to test, in this case 'elementId'
     * @param args additional arguments, in this case an empty array
     * @param expectedValue the expected value of {@link MPerspective#getElementId()}
     */
    @Override
    public boolean test(final Object receiver, final String property, final Object[] args, final Object expectedValue) {
        final MPerspective perspective = (MPerspective) receiver;
        System.out.println(perspective.getElementId());
        return perspective.getElementId().equals(expectedValue);
    }
}