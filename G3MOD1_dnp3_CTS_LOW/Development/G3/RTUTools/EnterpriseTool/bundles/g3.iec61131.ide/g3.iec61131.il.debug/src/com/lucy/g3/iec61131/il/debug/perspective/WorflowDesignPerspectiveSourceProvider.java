
package com.lucy.g3.iec61131.il.debug.perspective;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.ui.AbstractSourceProvider;
import org.eclipse.ui.ISources;

public class WorflowDesignPerspectiveSourceProvider extends AbstractSourceProvider {
  
  public final static String wsPerspectiveKey = "com.lucy.g3.iec61131.il.debug.handlers.ShowActions";
  private final static String debugPerspective = "showActions";
  private final static String otherPerspective = "hideActions";

  /**
   * true if the current perspective is the Resource Manager Perspective
   */
  private boolean isWSPerspective = true;

  @Override
  public void dispose() {
      // TODO Auto-generated method stub

  }

  @Override
  public Map getCurrentState() {
      Map<String, String> currentState = new HashMap<String, String>(1);
      String currentState1 = isWSPerspective ? debugPerspective
              : otherPerspective;
      currentState.put(wsPerspectiveKey, currentState1);
      return currentState;
  }

  @Override
  public String[] getProvidedSourceNames() {
      return new String[] { wsPerspectiveKey };
  }

  public void perspectiveChanged(boolean _isWSPerspective) {
      if (this.isWSPerspective == _isWSPerspective)
          return; // no change

      this.isWSPerspective = _isWSPerspective;
      String currentState = isWSPerspective ? debugPerspective
              : otherPerspective;
      fireSourceChanged(ISources.WORKBENCH, wsPerspectiveKey, currentState);
  }

}