package com.lucy.g3.iec61131.il.debug.parts;

import java.io.IOException;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.wb.swt.ResourceManager;

import com.lucy.g3.iec61131.debugger.IDebuggerProtocol;
import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DBG_CMD;
import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DBG_STATE;
import com.lucy.g3.iec61131.debugger.IDebuggerProtocol.DebugState;
import com.lucy.g3.iec61131.debugger.IIEC61131DebuggerComms;
import com.lucy.g3.iec61131.debugger.IEC61131DebuggerComms;
import com.lucy.g3.iec61131.il.debug.RulerToggleBreakpointAction;

public class Control {

	final String DEFAULT_IP_ADDRESS = "10.11.11.200";
	
	private static int MIN_POLING_RATE = 50;

	private Text textIpAddress;
	private Label lblCR;
	
	private IIEC61131DebuggerComms comms;
	
	private Button btnResume;
	private boolean running = false;
	private boolean connectionMade = false;
	private MPart foundPart;
	private Combo comboAutomationScheme;
	private ArrayList<IEditorPart> activeEditors;
	private ITextEditor currentTextEditor;
	private IWorkspaceRoot root;
	private RulerToggleBreakpointAction rulerToggleBreakpointAction;
	private Runnable playRunnable;
	
	@Inject
	IEventBroker broker;

	@Inject
	private EPartService partService;

	@Inject
	public Control() {
		comms = new IEC61131DebuggerComms();
	}

	private void suspend() throws IOException {

		comms.pause();
		running = false;
		btnResume.setImage(ResourceManager.getPluginImage("org.eclipse.debug.ui", "/icons/full/elcl16/resume_co.gif"));
	}

	// private static final String UPDATE_LINE = "updateLine";
	private static final String UPDATE_CR = "updateCR";
	private static final String SUSPEND = "suspend";

	private void resume() throws IOException {
		running = true;
		btnResume.setImage(ResourceManager.getPluginImage("org.eclipse.debug.ui", "/icons/full/elcl16/suspend_co.gif"));
		comms.resume();
		final Display display = Display.getCurrent();
		
		playRunnable = new Runnable() {
			public void run() {
				if (running) {
				  try {
  				  DebugState state = comms.getCurrentState();
  					highlightLine(state.curLineFromOne());
  					
  					if (DBG_STATE.forValue(state.curState) == DBG_STATE.SUSPENDED) {
  					  broker.send(SUSPEND, 0);
  					} else {
  					  broker.send(UPDATE_CR, state.cr);
  					}
					
				  } catch(IOException e) {
				    e.printStackTrace();
				  }
					

					display.timerExec(MIN_POLING_RATE, this);
				}
			}
		};
		
		display.timerExec(MIN_POLING_RATE, playRunnable);
	}

	@Inject
	@Optional
	public void getCREvent(@UIEventTopic(UPDATE_CR) int cr) {
		lblCR.setText(String.valueOf(cr));
	}

	@Inject
	@Optional
	public void suspend(@UIEventTopic(SUSPEND) int cr) throws IOException {
		suspend();
	}

	public static IViewPart getView(String id) {
		IViewReference viewReferences[] = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
				.getViewReferences();
		for (int i = 0; i < viewReferences.length; i++) {
			if (id.equals(viewReferences[i].getId())) {
				return viewReferences[i].getView(false);
			}
		}
		return null;
	}

	private void waitForEditor() {
		final Display display = Display.getCurrent();
		activeEditors = new ArrayList<IEditorPart>();
		display.timerExec(MIN_POLING_RATE, new Runnable() {
			public void run() {
				foundPart = partService.findPart("org.eclipse.e4.ui.compatibility.editor");
				if (foundPart != null) {
					IWorkbench workbench = PlatformUI.getWorkbench();
					IWorkbenchWindow activeWorkbenchWindow = workbench.getActiveWorkbenchWindow();
					if (activeWorkbenchWindow != null) {
						IWorkbenchPage activePage = activeWorkbenchWindow.getActivePage();
						if (activePage != null) {
							IEditorPart[] editors = activePage.getEditors();
							for (IEditorPart e : editors) {
								if (!activeEditors.contains(e)) {
									activeEditors.add(e);
									rulerToggleBreakpointAction = new RulerToggleBreakpointAction();
									rulerToggleBreakpointAction.setActiveEditor(null, e);
									rulerToggleBreakpointAction.breakpointAction();
								}
							}
						}
					}
				}
				display.timerExec(MIN_POLING_RATE, this);
			}
		});
	}

	private void highlightLine(int line) {
		IDocumentProvider provider = currentTextEditor.getDocumentProvider();
		try {
			provider.connect(this);
			IDocument document = provider.getDocument(currentTextEditor.getEditorInput());
			IRegion region = document.getLineInformation(line);
			System.out.println(line);
			currentTextEditor.selectAndReveal(region.getOffset(), region.getLength());
		} catch (CoreException e1) {
		} catch (BadLocationException e2) {
		} finally {
			// provider.disconnect(this);
		}
	}

	@PostConstruct
	public void createControls(Composite parent, IIEC61131DebuggerComms comm) {
		parent.setLayout(new GridLayout(1, false));

		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));
		
		try {
      comm.setHost("10.11.11.200");
//      comm.connect();
//      System.out.println(comm.getCurrentState().curState);
    } catch (IOException e2) {
      // TODO Auto-generated catch block
      e2.printStackTrace();
    }
   

		// wait for il code editor to be opened
		waitForEditor();

		btnResume = new Button(composite, SWT.NONE);
		btnResume.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				running = !running;
				try {
  				if (running) {  					
              resume();            
  				} else {
  					suspend();
  				}
				} catch (IOException e1) {
          e1.printStackTrace();
        }
			}
		});
		btnResume.setImage(ResourceManager.getPluginImage("org.eclipse.debug.ui", "/icons/full/elcl16/resume_co.gif"));

		Button btnStepOver = new Button(composite, SWT.NONE);
		btnStepOver.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				try {
          comms.stepover();
          DebugState state = comms.getCurrentState();
          highlightLine(state.curLineFromOne());
          updateCR();
        } catch (IOException e1) {
          e1.printStackTrace();
        }

			}
		});
		btnStepOver
				.setImage(ResourceManager.getPluginImage("org.eclipse.debug.ui", "/icons/full/elcl16/stepover_co.gif"));

		Composite composite_1 = new Composite(parent, SWT.NONE);
		composite_1.setLayout(new GridLayout(3, false));

		Label lblNewLabel = new Label(composite_1, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel.setText("RTU IP Address");

		textIpAddress = new Text(composite_1, SWT.BORDER);
		textIpAddress.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		textIpAddress.setText(DEFAULT_IP_ADDRESS);

		final Button btnConnect = new Button(composite_1, SWT.NONE);
		GridData gd_btnConnect = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnConnect.widthHint = 72;
		btnConnect.setLayoutData(gd_btnConnect);
		btnConnect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			  try {

          if ( btnConnect.getText() == "Disconnect") {
            comms.disconnect();
            btnConnect.setText("Connect");
          } else {
  					String ipAddress = textIpAddress.getText();
  					if (ipAddress != null) {
              comms.setHost(ipAddress);
              comms.connect();
              
  						rulerToggleBreakpointAction.setSocket(comms);
  						connectionMade = true;
  					}

  				  
  					try {
  						Thread.sleep(50);
  					} catch (InterruptedException e1) {
  						e1.printStackTrace();
  					}
  					
  					btnConnect.setText("Disconnect");
  					
          }

				} catch (IOException e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }
			}
		});
		btnConnect.setText("Connect");

		Composite compositeCurrentRegister = new Composite(parent, SWT.NONE);
		compositeCurrentRegister.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		compositeCurrentRegister.setLayout(new GridLayout(2, false));

		Label lblCurrentRegisterValue = new Label(compositeCurrentRegister, SWT.NONE);
		lblCurrentRegisterValue.setText("Current Register Value:");

		lblCR = new Label(compositeCurrentRegister, SWT.NONE);
		lblCR.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblCR.setText("1");

		Composite compositeAutomationScheme = new Composite(parent, SWT.NONE);
		compositeAutomationScheme.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		compositeAutomationScheme.setLayout(new GridLayout(2, false));

		Label lblAutomationScheme = new Label(compositeAutomationScheme, SWT.NONE);
		lblAutomationScheme.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAutomationScheme.setText("Automation Scheme:");

		comboAutomationScheme = new Combo(compositeAutomationScheme, SWT.NONE);

		comboAutomationScheme.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void focusGained(FocusEvent e) {
				updateAutomationSchemeCombo();
			}
		});

		comboAutomationScheme.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String source = e.getSource().toString();
				int start = source.indexOf("{") + 1;
				int end = source.length() - 1;
				final String substring = source.toString().substring(start, end);
				IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				IEditorPart[] editors = activePage.getEditors();
				for (final IEditorPart editor : editors) {
					String title = editor.getTitle();
					if (title.equals(substring)) {
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().bringToTop(editor);
						currentTextEditor = (ITextEditor) editor;
						return;
					}
					final IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					root = ResourcesPlugin.getWorkspace().getRoot();
					try {
						root.accept(new IResourceVisitor() {
							public boolean visit(IResource resource) {
								if (resource.getType() != IResource.FILE) {
									return true;
								}
								if (resource.getName().endsWith(".il")) {
									if (resource.getName().equals(substring)) {
										try {
											currentTextEditor = (ITextEditor) editor;
											IDE.openEditor(page, (IFile) resource);
										} catch (PartInitException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
									}
								}
								return true;
							}
						});
					} catch (CoreException e3) {
						// TODO Auto-generated catch block
						e3.printStackTrace();
					}
				}
			}
		});
		comboAutomationScheme.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		updateAutomationSchemeCombo();
	}

	private void updateAutomationSchemeCombo() {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		final ArrayList<IResource> ilFiles = new ArrayList<IResource>();
		try {
			root.accept(new IResourceVisitor() {
				public boolean visit(IResource resource) {
					if (resource.getType() != IResource.FILE) {
						return true;
					}
					if (resource.getName().endsWith(".il")) {
						ilFiles.add(resource);
						comboAutomationScheme.add(resource.getName());
					}
					return true;
				}
			});
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void updateCR() throws IOException {
		DebugState state = comms.getCurrentState();
		lblCR.setText(String.valueOf(state.cr));
	}

	@PreDestroy
	public void dispose() {
		if (connectionMade) {
			comms.disconnect();
		}
	}

}