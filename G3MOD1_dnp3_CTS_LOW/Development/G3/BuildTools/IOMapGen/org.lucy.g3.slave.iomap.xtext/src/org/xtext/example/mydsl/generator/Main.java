package org.xtext.example.mydsl.generator;

import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.generator.JavaIoFileSystemAccess;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.Issue;
import org.xtext.example.mydsl.myDsl.Model;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;

public class Main {

	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.println("Aborting: no path to EMF resource provided!");
			return;
		}
		Injector injector = new org.xtext.example.mydsl.MyDslStandaloneSetup().createInjectorAndDoEMFRegistration();
		Main main = injector.getInstance(Main.class);
		if (args.length == 2) {
			main.runGenerator(args[0], args[1], "", "");
		}

		else if (args.length == 3) {
			main.runGenerator(args[0], args[1], args[2], "");
		}

		else if (args.length == 4) {
			main.runGenerator(args[0], args[1], args[2], args[3]);
		}

	}

	@Inject
	private Provider<ResourceSet> resourceSetProvider;

	@Inject
	private IGenerator generator;
	
	@Inject
  private IResourceValidator validator;

	@Inject
	private JavaIoFileSystemAccess fileAccess;

	protected void runGenerator(String input, String output, String type1, String type2) {

		// load the resource
		ResourceSet set = resourceSetProvider.get();
		Resource resource = set.getResource(URI.createURI(input), true);
		
		// configure and start the generator
		fileAccess.setOutputPath(output);

		boolean genType1 = false;
		boolean genType2 = false;

		if (type1.equals("t1") || type2.equals("t1")) {
			genType1 = true;
		} else {
			genType1 = false;
		}

		if (type1.equals("t2") || type2.equals("t2")) {
			genType2 = true;
		} else {
			genType2 = false;
		}

		// generator.doGenerate(resource, fileAccess);
		
		 // validate the resource
    List<Issue> list = validator.validate(resource, CheckMode.ALL, CancelIndicator.NullImpl);
    if (!list.isEmpty()) {
      for (Issue issue : list) {
        System.err.println(issue);
      }
       return;
    }

		MyDslGenerator myDslGenerator = new MyDslGenerator();

		Model model = (Model) resource.getContents().get(0);
		List<String> segmentsList = resource.getURI().segmentsList();

		if (genType1) {
			fileAccess.generateFile(segmentsList.get(segmentsList.size() - 1).replace(".map", "") + "1.c",
					myDslGenerator.toC(model));
		}
		if (genType2) {
			fileAccess.generateFile(segmentsList.get(segmentsList.size() - 1).replace(".map", "") + "2.c",
					myDslGenerator.toC2(model));
		}
		
		fileAccess.generateFile(segmentsList.get(segmentsList.size() - 1).replace(".map", "") + ".xml",
        myDslGenerator.toXML(model));

		System.out.println("Code generation finished.");
	}
}
