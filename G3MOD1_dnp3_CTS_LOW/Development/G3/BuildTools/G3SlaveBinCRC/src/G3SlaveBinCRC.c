/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
//#include <memory.h>
//#include <windows.h>
//#include <conio.h>
#include <string.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "SlaveBinImageDef.h"
#include "crc32.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


SlaveBinImageHeaderStr binImageHeader;


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

char *getBinArchStr(SLAVE_BIN_ARCH typ)
{
	switch (typ)
	{
	case SLAVE_BIN_ARCH_ARM_NXP_LPC1765:
		return "NXP LPC1765";
		break;

	case SLAVE_BIN_ARCH_ARM_NXP_LPC1768:
		return "NXP LPC1768";
		break;

	default:
		return "Unknown";
		break;
	}
}

char *getImageTypeStr(SLAVE_IMAGE_TYPE typ)
{
	switch (typ)
	{
	case SLAVE_IMAGE_TYPE_BOOT_LOADER:
		return "BootLoader";
		break;

	case SLAVE_IMAGE_TYPE_APPLICATION:
		return "Application";
		break;

	case SLAVE_IMAGE_TYPE_APP_BOOT_PROGRAM:
		return "Application - Bootloader Programmer";
		break;

	default:
		return "Unknown";
		break;
	}
}

char *getRelTypeStr(VERSION_TYPE typ)
{
	switch (typ)
	{
	case VERSION_TYPE_RELEASE:
		return "Release";
		break;

	case VERSION_TYPE_DEBUG:
		return "Debug";
		break;

	case VERSION_TYPE_BETA:
			return "Beta";
			break;

	case VERSION_TYPE_TEST:
			return "Test";
			break;

	default:
		return "Unknown";
		break;
	}
}

/*!
******************************************************************************
*   \brief Brief description
*
*   Detailed description
*
*   \param parameterName Description
*
*
*   \return
*
******************************************************************************
*/
int main(int argc, char* argv[])
{
	lu_int8_t sourceFileName[400];
	FILE      *filePtr;
	lu_uint32_t fileSize, byteCount;
	lu_uint32_t i;
	lu_uint32_t count;
	lu_uint32_t crc32;
	lu_uint32_t postImageSize;
	lu_uint8_t  data;
	lu_uint8_t  *dataPtr;


	if (argc < 2)
	{
		printf("no args\n\n");
	}

	for(i = 1; i < argc; i++)
	{
		if (sscanf(argv[i], "-FP=%s", sourceFileName) == 1)
		{
			/* Open file - then Patch CRC */
			filePtr = fopen(sourceFileName,"rb+");

			if (filePtr != NULL)
			{
				/* Get file size */
				fseek(filePtr , 1 , SEEK_END);
				fileSize = ftell(filePtr);
				fileSize--;
				rewind (filePtr);

				/* Read bin header */
				fseek(filePtr , SLAVE_IMAGE_BASE_OFFSET , SEEK_SET);
				byteCount = fread(&binImageHeader, 1, sizeof(SlaveBinImageHeaderStr), filePtr);
				if (byteCount != sizeof(SlaveBinImageHeaderStr))
				{
					printf("Error reading header??\n\n");
					exit(-1);
				}

				binImageHeader.imageSize = fileSize;

				if (binImageHeader.magicSyncCode != SLAVE_IMAGE_MAGIC_SYNC)
				{
					printf("magicSyncCode ??\n\n");
					//exit(-1);
				}

				/* (1) Calculate preHeader CRC */
				rewind (filePtr);
				crc32_init(&crc32);
				for (count = 0; count < SLAVE_IMAGE_BASE_OFFSET; count++)
				{
					byteCount = fread(&data, 1, 1, filePtr);
					if (byteCount == 1)
					{
						crc32_byte(data, &crc32);
					}
				}
				crc32 ^= 0XFFFFFFFFL;

				binImageHeader.preHeaderCRC32 = crc32;

				/* (2) Calculate post Header crc */
				postImageSize  = fileSize;
				postImageSize -= SLAVE_IMAGE_BASE_OFFSET;
				postImageSize -= SLAVE_IMAGE_HEAD_SIZE;

				fseek(filePtr , (SLAVE_IMAGE_BASE_OFFSET + SLAVE_IMAGE_HEAD_SIZE), SEEK_SET);
				crc32_init(&crc32);
				for (count = 0; count < postImageSize; count++)
				{
					byteCount = fread(&data, 1, 1, filePtr);
					if (byteCount == 1)
					{
						crc32_byte(data, &crc32);
					}
				}
				crc32 ^= 0XFFFFFFFFL;

				binImageHeader.postHeaderCRC32 = crc32;

				/* (3) Calculate the image header crc32 */
				fseek(filePtr , (SLAVE_IMAGE_BASE_OFFSET + sizeof(binImageHeader.headerCRC32)) , SEEK_SET);

				crc32_init(&crc32);
				dataPtr  = &binImageHeader;
				dataPtr += sizeof(binImageHeader.headerCRC32);

				for (count = 0; count < (sizeof(SlaveBinImageHeaderStr) - sizeof(binImageHeader.headerCRC32)) ; count++)
				{
					crc32_byte(*dataPtr, &crc32);
					dataPtr++;
				}
				crc32 ^= 0XFFFFFFFFL;

				binImageHeader.headerCRC32 = crc32;


				printf("Filename        = %s\n\n", sourceFileName);

				printf("Image offset to Header = 0x%08x\n\n", binImageHeader.appImageAddress);
				printf("imageAddress    = 0x%08x\n", binImageHeader.appImageAddress);
				printf("imageSize       = 0x%x (%ld)\n", binImageHeader.imageSize, binImageHeader.imageSize);

				printf("processorArch   = %s\n", getBinArchStr(binImageHeader.processorArch));
				printf("ImageType       = %s\n", getImageTypeStr(binImageHeader.imageType));

				printf("svnRevsion      = %ld\n", binImageHeader.svnRevsion);
				printf("systemAPI       = V%d.%d\n", binImageHeader.systemAPI.major, 
				                                     binImageHeader.systemAPI.minor);


				printf("softwareVersion = %s V%d.%dP%d\n",  getRelTypeStr(binImageHeader.softwareVersion.relType),
						                             binImageHeader.softwareVersion.version.major,
					                                 binImageHeader.softwareVersion.version.minor,
													 binImageHeader.softwareVersion.patch);

				printf("magicSyncCode   = 0x%08x\n", binImageHeader.magicSyncCode);
				printf("preHeaderCRC32  = 0x%08x\n", binImageHeader.preHeaderCRC32);
				printf("postHeaderCRC32 = 0x%08x\n", binImageHeader.postHeaderCRC32);
				printf("headerCRC32     = 0x%08x\n", binImageHeader.headerCRC32);

				/* Now patch the file */
				fseek(filePtr , SLAVE_IMAGE_BASE_OFFSET , SEEK_SET);
				byteCount = fwrite(&binImageHeader, 1, sizeof(SlaveBinImageHeaderStr), filePtr);

				if (byteCount != sizeof(SlaveBinImageHeaderStr))
				{
					printf("Failed to patch exebin header!!\n\n");
				}
			}
			else
			{
				printf("Can't open file\n\n");
			}

			fclose(filePtr);
		}
		else if (sscanf(argv[i], "-FV=%s", sourceFileName) == 1)
		{
			/* Open file - then Patch CRC */
			filePtr = fopen(sourceFileName,"rb");

			if (filePtr != NULL)
			{
				/* Get file size */
				fseek(filePtr , 1 , SEEK_END);
				fileSize = ftell(filePtr);
				fileSize--;
				rewind (filePtr);

				/* Read bin header */
				fseek(filePtr , SLAVE_IMAGE_BASE_OFFSET , SEEK_SET);
				byteCount = fread(&binImageHeader, 1, sizeof(SlaveBinImageHeaderStr), filePtr);
				if (byteCount != sizeof(SlaveBinImageHeaderStr))
				{
					printf("Error reading header??\n\n");
					exit(-1);
				}

				printf("Filename        = %s\n\n", sourceFileName);

				printf("Image offset to Header = 0x%08x\n\n", binImageHeader.appImageAddress);
				printf("imageAddress    = 0x%08x\n", binImageHeader.appImageAddress);

				if (binImageHeader.imageSize != fileSize)
				{
					printf("imageSize       = 0x%x (%d) (Bad, file size = %d)\n", binImageHeader.imageSize, binImageHeader.imageSize, fileSize);
				}
				else
				{
					printf("imageSize       = 0x%x (%d)\n", binImageHeader.imageSize, binImageHeader.imageSize);
				}

				printf("processorArch   = %s\n", getBinArchStr(binImageHeader.processorArch));
				printf("ImageType       = %s\n", getImageTypeStr(binImageHeader.imageType));

				printf("svnRevsion      = %ld\n", binImageHeader.svnRevsion);
				printf("systemAPI       = V%d.%d\n", binImageHeader.systemAPI.major,
													 binImageHeader.systemAPI.minor);
				printf("softwareVersion = %s V%d.%dP%d\n",  getRelTypeStr(binImageHeader.softwareVersion.relType),
						                             binImageHeader.softwareVersion.version.major,
													 binImageHeader.softwareVersion.version.minor,
													 binImageHeader.softwareVersion.patch);

				printf("magicSyncCode   = 0x%08x\n", binImageHeader.magicSyncCode);

				if (binImageHeader.magicSyncCode != SLAVE_IMAGE_MAGIC_SYNC)
				{
					printf("magicSyncCode ??\n\n");
					//exit(-1);
				}

				/* (1) Calculate preHeader CRC */
				rewind (filePtr);
				crc32_init(&crc32);
				for (count = 0; count < SLAVE_IMAGE_BASE_OFFSET; count++)
				{
					byteCount = fread(&data, 1, 1, filePtr);
					if (byteCount == 1)
					{
						crc32_byte(data, &crc32);
					}
				}
				crc32 ^= 0XFFFFFFFFL;

				if (binImageHeader.preHeaderCRC32 != crc32)
				{
					printf("preHeaderCRC32  = 0x%08x  (Bad CRC calculated = 0x%08x\n", binImageHeader.preHeaderCRC32, crc32);
				}
				else
				{
					printf("preHeaderCRC32  = 0x%08x\n", binImageHeader.preHeaderCRC32);
				}

				/* (2) Calculate post Header crc */
				postImageSize  = fileSize;
				postImageSize -= SLAVE_IMAGE_BASE_OFFSET;
				postImageSize -= SLAVE_IMAGE_HEAD_SIZE;

				fseek(filePtr , (SLAVE_IMAGE_BASE_OFFSET + SLAVE_IMAGE_HEAD_SIZE), SEEK_SET);

				crc32_init(&crc32);
				for (count = 0; count < postImageSize; count++)
				{
					byteCount = fread(&data, 1, 1, filePtr);
					if (byteCount == 1)
					{
						crc32_byte(data, &crc32);
					}
				}
				crc32 ^= 0XFFFFFFFFL;

				if (binImageHeader.postHeaderCRC32 != crc32)
				{
					printf("postHeaderCRC32 = 0x%08x (Bad, calculated = 0x%08x)\n", binImageHeader.postHeaderCRC32, crc32);
				}
				else
				{
					printf("postHeaderCRC32 = 0x%08x\n", binImageHeader.postHeaderCRC32);
				}

				/* (3) Calculate the image header crc32 */
				fseek(filePtr , (SLAVE_IMAGE_BASE_OFFSET + sizeof(binImageHeader.headerCRC32)) , SEEK_SET);

				crc32_init(&crc32);
				for (count = 0; count < (sizeof(SlaveBinImageHeaderStr) - sizeof(binImageHeader.headerCRC32)); count++)
				{
					byteCount = fread(&data, 1, 1, filePtr);
					if (byteCount == 1)
					{
						crc32_byte(data, &crc32);
					}
				}
				crc32 ^= 0XFFFFFFFFL;

				if (binImageHeader.headerCRC32 != crc32)
				{
					printf("headerCRC32     = 0x%08x (Bad, calculated = 0x%0x)\n", binImageHeader.headerCRC32, crc32);
				}
				else
				{
					printf("headerCRC32     = 0x%08x\n", binImageHeader.headerCRC32);
				}

			}
			else
			{
				printf("Can't open file\n\n");
			}

			fclose(filePtr);
		}
	}

	return 0;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
