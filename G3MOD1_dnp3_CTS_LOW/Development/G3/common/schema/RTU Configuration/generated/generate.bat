@echo off
REM This batch file is for generating schema components from XML sources in common directory using XSLT1.0
REM Make sure all schema components are regenerated after related XML sources are updated.

xsltproc ../../../xslt/xml2xsd.xsl ../../../xml/ModuleProtocolEnum.xml >ModuleProtocolEnum.xsd
xsltproc ../../../xslt/xml2xsd.xsl ../../../xml/MCMConfigEnum.xml >MCMConfigEnum.xsd
xsltproc ../../../xslt/xml2xsd.xsl ../../../xml/DNP3Enum.xml >DNP3Enum.xsd
xsltproc ../../../xslt/xml2xsd.xsl ../../../xml/IEC870Enum.xml >IEC870Enum.xsd
xsltproc ../../../xslt/xml2xsd.xsl ../../../xml/ProtocolStackCommonEnum.xml >ProtocolStackCommonEnum.xsd
xsltproc ../../../xslt/xml2xsd.xsl ../../../xml/ModBusEnum.xml >ModBusEnum.xsd
echo Done!
pause