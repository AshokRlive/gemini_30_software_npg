#include <embUnit/embUnit.h>

static void setUp(void)
{
    //TODO setup test environments
}

static void tearDown(void)
{
    //TODO clean up test environments
}

/* Test Case*/
static void testYOUR_FUNCTION_NAME(void)
{
    //TODO Your test case implementation
}



/* Test Suite*/
TestRef YOUR_CLASS_NAMETest_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures) {
        /* Test Cases*/
        new_TestFixture("testYOUR_FUNCTION_NAME",testYOUR_FUNCTION_NAME),
        // ... more test cases
    };
    EMB_UNIT_TESTCALLER(YOUR_CLASS_NAMeTest_tests,"YOUR_CLASS_NAMETest_tests",setUp,tearDown,fixtures);

    return (TestRef)&YOUR_CLASS_NAMeTest_tests;;
}
