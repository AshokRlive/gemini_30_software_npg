message("Generating SVN Header...")

# Check if the required variables have been defined. 
if(NOT DEFINED SVN_INPUT)
    message(SEND_ERROR "SVN_INPUT not defined")
else()
    #message("SVN_INPUT:${SVN_INPUT}")
endif()

if(NOT DEFINED SVN_DIR)
    message(SEND_ERROR "SVN_INPUT not defined")
else()
    #message("SVN_DIR:${SVN_DIR}")
endif()

if(NOT DEFINED SVN_OUTPUT)
    message(SEND_ERROR "SVN_OUTPUT not defined")
else()
    message("SVN_OUTPUT:${SVN_OUTPUT}")
endif()

#set_directory_properties(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES  ${SVN_OUTPUT})

# Run svnversion to extract SVN revision number that could be in any of the 
# formats: "4123:41168", "4168M", "4123S", "4123P", "4123:4168MS"
# Note: Subversion_WC_INFO does not always report the latest SVN commit revision. 
EXECUTE_PROCESS(COMMAND svnversion ${SVN_DIR}
                OUTPUT_VARIABLE svnVersionComplete)
                
if(svnVersionComplete MATCHES ":([0-9]*)|^([0-9]*)([A-Z])*")
    message("Full SVN revision:${svnVersionComplete}")
else()
    message(SEND_ERROR "Unexpected svn revision format: ${svnVersionComplete}")
endif()


# Extract latest SVN revision number
# If a STRING REGEX parameter error is reported by CMake, is due to an unexpected format of SVN.
STRING(REGEX MATCH ":([0-9]*)|^([0-9]*)([A-Z])*" svnVersion2 ${svnVersionComplete}) 
STRING(REGEX REPLACE ":" "" svnVersion3 ${svnVersion2})  
STRING(REGEX REPLACE "[A-Z]" "" svnVersion ${svnVersion3}) 

#message for debug only
MESSAGE("SVN revision: ${svnVersion}")

# please note that configure_file() only regenerates the file when the content changes
CONFIGURE_FILE(${SVN_INPUT} ${SVN_OUTPUT})
