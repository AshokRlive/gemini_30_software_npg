# A cmake script for generating source files from XML using XSLT.
# To include the directories of the generated code, use variable 
# CMAKE_G3_GENERATED_HEADER_INCLUDE in the parent CMakeLists.txt.
# E.g. include_directories(${CMAKE_G3_GENERATED_HEADER_INCLUDE})

FIND_PROGRAM(XSLTPROC xsltproc PATHS /usr/bin /usr/local/bin c:/xsltproc)

# Check if the required variables are defined. 
if(NOT DEFINED CMAKE_G3_PATH_ROOT)
    message(SEND_ERROR "!!!Please set the CMAKE_G3_PATH_ROOT in your project CMakeLists.txt")
endif()

if(NOT DEFINED CMAKE_G3_GENERATED_HEADER_BUILD_PATH)
    message(SEND_ERROR "!!!Please set the path:CMAKE_G3_GENERATED_HEADER_BUILD_PATH in your project CMakeLists.txt")
endif()

if(NOT DEFINED CMAKE_G3_GENERATED_HEADER_LIST)
    message(SEND_ERROR "!!!Please set the CMAKE_G3_GENERATED_HEADER_LIST in your project CMakeLists.txt")
endif()

# Add generate header directory to include path
include_directories(SYSTEM ${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/)

# Specify XML folder
set(XML_DIR         "${CMAKE_G3_PATH_ROOT}/common/xml")

# Specify XSLT for XML transformation.
set(XMLENUM2CH      "${CMAKE_G3_PATH_ROOT}/common/xslt/xml2C_header.xsl")
set(XML2MCMVersion  "${CMAKE_G3_PATH_ROOT}/common/xslt/MCMVersion.xsl")
set(XSD2CH          "${CMAKE_G3_PATH_ROOT}/common/xslt/xsd2C_header.xsl")


# ++++++++++ GENERATE HEADER FILES +++++++++++
SET(listHeader)

# Create version properties file for MCM main application
if(PROJECT_NAME STREQUAL "MCMBoard")
ADD_CUSTOM_COMMAND(
        OUTPUT ${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/version.properties
        COMMAND ${XSLTPROC} ${XML2MCMVersion} ${XML_DIR}/versions.xml > ${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/version.properties
                  )
list(APPEND listHeader "${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/version.properties")
endif()

# Process XML files to create *.h files
foreach(fileCouple ${CMAKE_G3_GENERATED_HEADER_LIST})
    # Get each part of the couple
    list(GET fileCouple 0 fileXML)
    list(GET fileCouple 1 fileHeader)
    # Trim spaces
    string(STRIP ${fileXML} fileXML)
    string(STRIP ${fileHeader} fileHeader)
    # Generate files
    ADD_CUSTOM_COMMAND(
        OUTPUT ${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/${fileHeader}.h
        COMMAND ${XSLTPROC} --stringparam inputXML ${fileXML}.xml ${XMLENUM2CH} ${XML_DIR}/${fileXML}.xml > ${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/${fileHeader}.h
        DEPENDS ${XML_DIR}/${fileXML}.xml 
                  )
    list(APPEND listHeader ${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/${fileHeader}.h)                  
endforeach(fileCouple)

#Generate enums from XSD to create *.h file(s)
if(DEFINED CMAKE_G3_GENERATED_XSD_HEADER_LIST)
    foreach(fileCouple ${CMAKE_G3_GENERATED_XSD_HEADER_LIST})
        # Get each part of the couple
        list(GET fileCouple 0 fileXSD)
        list(GET fileCouple 1 fileHeader)
        # Trim spaces
        string(STRIP ${fileXSD} fileXSD)
        string(STRIP ${fileHeader} fileHeader)
        ADD_CUSTOM_COMMAND(
            OUTPUT ${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/${fileHeader}.h
            COMMAND ${XSLTPROC} --stringparam inputXSD ${fileXSD}.xsd ${XSD2CH} ${XML_DIR}/${fileXSD}.xsd > ${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/${fileHeader}.h
            DEPENDS ${XML_DIR}/${fileXSD}.xsd
                      )
        list(APPEND listHeader ${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/${fileHeader}.h)
    endforeach(fileCouple)
endif()                  


# Add all the header list to the global dependency 
ADD_CUSTOM_TARGET(globalHeaders DEPENDS ${listHeader} )

# Set default output sub-directories required by xml transformation 
if(NOT DEFINED OUTPUT_SUB_DIRECTORIES)
    SET(OUTPUT_SUB_DIRECTORIES ModuleProtocol SysAlarm NVRAMDef)
endif()

# Create default output sub-directories .
foreach(SUB_DIR ${OUTPUT_SUB_DIRECTORIES})
    SET(TMP_TARGET  createDir${SUB_DIR})
    ADD_CUSTOM_TARGET(${TMP_TARGET} ALL
    COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/${SUB_DIR}/)

    ADD_DEPENDENCIES(globalHeaders ${TMP_TARGET})

    # Clean sub-directory
    set_property(DIRECTORY APPEND PROPERTY ADDITIONAL_MAKE_CLEAN_FILES ${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/${SUB_DIR})
    
    # Add sub-directory to include
    include_directories(SYSTEM ${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/${SUB_DIR})
endforeach(SUB_DIR)

# Set include directories variable
get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
set(CMAKE_G3_GENERATED_HEADER_INCLUDE ${dirs} PARENT_SCOPE)
