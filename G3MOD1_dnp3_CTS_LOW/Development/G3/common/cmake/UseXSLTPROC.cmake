#
# Find the xsltproc utility to Apply XSLT stylesheet to XML
#
# This module defines
# XSLTPROC_PATH, where to find xsltproc
# XSLTPROC_FOUND, If false, do not try to use xsltproc.

#  also defined, but not for general use are
#

SET(XSLTPROC_FOUND "NO")

FIND_PATH(XSLTPROC_PATH xsltproc /usr/local/bin  /usr/bin)

# Message ("msg: ${XSLTPROC_PATH}")


IF(XSLTPROC_PATH)
  SET(XSLTPROC_FOUND "YES")
ELSE (XSLTPROC_PATH)
  MESSAGE(SEND_ERROR "Could not find xsltproc ?")
ENDIF(XSLTPROC_PATH)

# runXSLTPROC XML xslt transformation
macro (runXSLTPROC xmlPath xmlFile xslPathFile outFile)
	
	MESSAGE(XSLTPROC cmd:  ${XSLTPROC_PATH}/xsltproc ${xslPathFile} ${xmlPath}/${xmlFile} >${xmlPath}/${outFile}  )
	
	ADD_CUSTOM_COMMAND(
	    OUTPUT ${xmlPath}/${outFile}
	    COMMAND cd ${xmlPath} &&  ${XSLTPROC_PATH}/xsltproc ${xmlPath}/${xmlFile} >${xmlPath}/${outFile}
	    DEPENDS ${xmlPath}/${xmlFile}
	    )
	
	set_directory_properties(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES
		${xmlPath}/${outFile})
	
endmacro (runXSLTPROC)


