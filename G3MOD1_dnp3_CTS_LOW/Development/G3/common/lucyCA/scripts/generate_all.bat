@echo off
call generate_root_keystore.bat
if errorlevel 1 exit /B %errorlevel%

call extract_private_root_key.bat
if errorlevel 1 exit /B %errorlevel%

call extract_public_root_key.bat
if errorlevel 1 exit /B %errorlevel%




