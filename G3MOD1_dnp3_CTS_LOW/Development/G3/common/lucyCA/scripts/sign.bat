:: A script for signing a csr with lucy CA.
@echo off
if "%1%"=="" goto missingArgs

echo Signing csr...
set rootcert="..\root\private\lucyroot.crt"
set rootkey="..\root\private\lucyroot.key"
set csr=%1%
set crt=%csr%.crt
set serial=1234
set days=3650

::Sign request with root CA and private key.
openssl x509 -req -CA %rootcert% -CAkey %rootkey% -in %csr% -out %crt% -days %days% -set_serial %serial%
if errorlevel 1 exit /b %errorlevel%
echo Certificate was generated: "%crt%"
echo Done
exit /B 0

:missingArgs
echo Usage: sign_csr.bat  [Input CSR file] 
exit /B -1
