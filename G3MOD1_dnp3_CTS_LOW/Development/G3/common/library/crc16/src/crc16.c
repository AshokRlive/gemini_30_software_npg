/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28/09/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "crc16.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

lu_uint16_t crc16;



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void crc16_init(void)
{
	crc16 = 0xffff;
}

void crc16_byte(lu_uint8_t data)
{
	crc16  = (lu_uint8_t)(crc16 >> 8) | (crc16 << 8);
	crc16 ^= data;
	crc16 ^= (lu_uint8_t)(crc16 & 0xff) >> 4;
	crc16 ^= (crc16 << 8) << 4;
	crc16 ^= ((crc16 & 0xff) << 4) << 1;
}


void crc16_calc16(lu_uint8_t *data_p, lu_uint16_t len)
{
	crc16_init();

	while (len--)
	{
	  crc16_byte(*data_p);
	  data_p ++;
	}
}

lu_uint16_t crc16_modbus_calc16(lu_uint8_t *data_p, lu_uint8_t len)
{
	lu_uint16_t crc = 0xFFFF;
	lu_uint8_t idx;

	while(len--)
	{
		crc ^= * data_p++;

		for(idx = 0; idx < 8 ; idx++)
		{
			if(crc & 0x01)
			{
				crc >>= 1;
				crc ^= 0xA001;
			}
			else
			{
				crc >>= 1;
			}
		}
	}
	return crc;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
