/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Linear interpolation unit test cases module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   02/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <embUnit/embUnit.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "LinearInterpolation.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MAX_1DU16U32_TEST		5

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

//void lnInterp1DU16U32Test_setUp(void);
//void lnInterp1DU16U32Test_tearDown(void);

//void lnInterp1DU16U32Test_lnInterp1DU16U16(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


LnInterpTable1DU16U32Str testTable1dU16U32[MAX_1DU16U32_TEST] = {
		/*
		 * Input value ,       Outputvalue
		 */
		{5,                    1200},
		{20,                   1300},
		{20000,                1800},
		{300000,               3200},
		{500000,               65530}
};




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

static void lnInterp1DU16U32Test_setUp(void)
{

}

static void lnInterp1DU16U32Test_tearDown(void)
{

}

static void lnInterp1DU16U32Test_limits(void)
{
	TEST_ASSERT_EQUAL_INT(1200,
			              lnInterp1DU16U32(&testTable1dU16U32[0],
			            		           MAX_1DU16U32_TEST,
			            		           0
			            		          )
			             );

	TEST_ASSERT_EQUAL_INT(65530,
				              lnInterp1DU16U32(&testTable1dU16U32[0],
				            		           MAX_1DU16U32_TEST,
				            		           16777215
				            		          )
				             );

	TEST_ASSERT_EQUAL_INT(1200,
			              lnInterp1DU16U32(&testTable1dU16U32[0],
				            		           MAX_1DU16U32_TEST,
				            		           5
				            		          )
				             );

	TEST_ASSERT_EQUAL_INT(3123,
					              lnInterp1DU16U32(&testTable1dU16U32[0],
					            		           MAX_1DU16U32_TEST,
					            		           285530
					            		          )
					             );

	TEST_ASSERT_EQUAL_INT(65530,
				              lnInterp1DU16U32(&testTable1dU16U32[0],
				            		           MAX_1DU16U32_TEST,
				            		           500000
				            		          )
				             );
}

static void lnInterp1DU16U32Test_interp(void)
{
	TEST_ASSERT_EQUAL_INT(1233,
				              lnInterp1DU16U32(&testTable1dU16U32[0],
				            		           MAX_1DU16U32_TEST,
				            		           10
				            		          )
				             );
}

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

TestRef lnInterp1DU16U32Test_tests(void)
{
	EMB_UNIT_TESTFIXTURES(fixtures) {
		new_TestFixture("lnInterp_test1DU16U32_limits",lnInterp1DU16U32Test_limits),
		new_TestFixture("lnInterp_test1DU16U32_interp",lnInterp1DU16U32Test_interp),


	};
	EMB_UNIT_TESTCALLER( LinearInterpTest,
			             "LinearInterpolationTest",
			             lnInterp1DU16U32Test_setUp,
			             lnInterp1DU16U32Test_tearDown,
			             fixtures
			            );

	return (TestRef)&LinearInterpTest;
}



/*
 *********************** End of file ******************************************
 */
