/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Linear interpolation public library header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   02/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _C_LINEARINTERPOLATION_INCLUDED
#define _C_LINEARINTERPOLATION_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*!
 * Linear Interpolation table for 1D from (unsigned int 16 bit) to
 * (signed int 16 bit)
 */
typedef struct LnInterpTable1DU16S16Def
{
    lu_uint16_t distributionX;        // Input value
    lu_int16_t interpolateToX;       // Output interpolate to
}LnInterpTable1DU16S16Str;


/*!
 * Linear Interpolation table for 1D from (unsigned int 16 bit) to
 * (unsigned int 16 bit)
 */
typedef struct LnInterpTable1DU16U16Def
{
    lu_uint16_t distributionX;        // Input value
    lu_uint16_t interpolateToX;       // Output interpolate to
}LnInterpTable1DU16U16Str;

/*!
 * Linear Interpolation table for 1D from (unsigned int 32 bit) to
 * (unsigned int 16 bit)
 */
typedef struct LnInterpTable1DU16U32Def
{
    lu_uint16_t distributionX;        // Input value
    lu_uint32_t interpolateToX;       // Output interpolate to
}LnInterpTable1DU16U32Str;

/*!
 * Linear Interpolation table for 1D from (unsigned int 32 bit) to
 * (unsigned int 32 bit)
 */
typedef struct LnInterpTable1DU32U32Def
{
    lu_uint32_t distributionX;        // Input value
    lu_uint32_t interpolateToX;       // Output interpolate to
}LnInterpTable1DU32U32Str;

/*!
 * Linear Interpolation table for 1D from (int 32 bit) to
 * (unsigned int 32 bit)
 */
typedef struct LnInterpTable1DS32U32Def
{
    lu_int32_t distributionX;        // Input value
    lu_uint32_t interpolateToX;       // Output interpolate to
}LnInterpTable1DS32U32Str;

/*!
 * Linear Interpolation table for 1D from (unsigned int 32 bit) to
 * (unsigned int 32 bit)
 */
typedef struct LnInterpTable1DU16F32Def
{
	lu_uint16_t distributionX;        // Input value
	lu_float32_t interpolateToX;       // Output interpolate to
}LnInterpTable1DU16F32Str;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Perform a 1D linear interpolation (16 bit signed)
 *
 *   \param interpTable_p Pointer to interpolation table map of points.
 *   \param tableSize Size of the table (no. of points)
 *   \param x Input value to be interpolated via the table map.
 *
 *   \return Result Value from interpolation (16 bit signed)
 *
 ******************************************************************************
 */
 extern lu_int16_t lnInterp1DU16S16( LnInterpTable1DU16S16Str *interpTable_p    ,
                                      lu_uint8_t               tableSize         ,
                                      lu_uint16_t              x
                                    );


/*!
 ******************************************************************************
 *   \brief Perform a 1D linear interpolation (16 bit unsigned)
 *
 *   \param interpTable_p Pointer to interpolation table map of points.
 *   \param tableSize Size of the table (no. of points)
 *   \param x Input value to be interpolated via the table map.
 *
 *   \return Result Value from interpolation (16 bit unsigned)
 *
 ******************************************************************************
 */
 extern lu_uint16_t lnInterp1DU16U16( LnInterpTable1DU16U16Str *interpTable_p    ,
                                      lu_uint8_t               tableSize         ,
                                      lu_uint16_t              x
                                    );

 /*!
  ******************************************************************************
  *   \brief Perform a 1D linear interpolation (32 bit unsigned)
  *
  *   \param interpTable_p Pointer to interpolation table map of points.
  *   \param tableSize Size of the table (no. of points)
  *   \param x Input value to be interpolated via the table map.
  *
  *   \return Result Value from interpolation (16 bit unsigned)
  *
  ******************************************************************************
  */
 extern lu_uint16_t lnInterp1DU16U32( LnInterpTable1DU16U32Str *interpTable_p    ,
                                      lu_uint8_t               tableSize         ,
                                      lu_uint32_t              x
                                    );

 /*!
   ******************************************************************************
   *   \brief Perform a 1D linear interpolation (32 bit signed)
   *
   *   \param interpTable_p Pointer to interpolation table map of points.
   *   \param tableSize Size of the table (no. of points)
   *   \param x Input value to be interpolated via the table map.
   *
   *   \return Result Value from interpolation (32 bit unsigned)
   *
   ******************************************************************************
   */
 extern lu_uint32_t lnInterp1DS32U32( LnInterpTable1DS32U32Str *interpTable_p    ,
                                      lu_uint8_t               tableSize         ,
                                      lu_int32_t              x
                                    );

/*!
  ******************************************************************************
  *   \brief Perform a 1D linear interpolation (32 bit unsigned)
  *
  *   \param interpTable_p Pointer to interpolation table map of points.
  *   \param tableSize Size of the table (no. of points)
  *   \param x Input value to be interpolated via the table map.
  *
  *   \return Result Value from interpolation (32 bit unsigned)
  *
  ******************************************************************************
  */
extern lu_uint32_t lnInterp1DU32U32( LnInterpTable1DU32U32Str *interpTable_p    ,
                                     lu_uint8_t               tableSize         ,
                                     lu_uint32_t              x
                                   );

extern lu_float32_t lnInterp1DU16F32( LnInterpTable1DU16F32Str *interpTable_p    ,
                                      lu_uint8_t               tableSize         ,
                                      lu_uint32_t              x
                                  	);


#endif /* _C_LINEARINTERPOLATION_INCLUDED */

/*
 *********************** End of file ******************************************
 */
