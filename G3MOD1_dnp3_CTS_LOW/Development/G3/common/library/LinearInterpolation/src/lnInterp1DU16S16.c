/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Linear Interpolation module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   02/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "LinearInterpolation.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define DV_SCALE_FACTOR	16

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void lnInterp1DS16Search( LnInterpTable1DU16S16Str *interpTable_p     ,
                          lu_uint8_t               tableSize          ,
                          lu_uint8_t               *index_p           , 
                          lu_uint64_t              *scaleFactor_p     ,
                          lu_uint16_t              x
                        );

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_int16_t lnInterp1DU16S16( LnInterpTable1DU16S16Str *interpTable_p    ,
                             lu_uint8_t               tableSize         ,
                             lu_uint16_t              x
                           )
{
    lu_uint8_t   index;
    lu_uint64_t  dv;
    lu_uint64_t  scaleFactor;
    
    scaleFactor = 0;
    
    /* Begin by searching the 1D table
     */
    lnInterp1DS16Search(interpTable_p, tableSize, &index, &scaleFactor, x);
    
    /* See if we need to interpolate ?
     */
    if (scaleFactor != 0)
    {
        /* OK, do the interpolation between point found and workout the deviation...
         */
        dv = (interpTable_p[(index + 1)].interpolateToX - interpTable_p[index].interpolateToX);
        
        /* Re-scale by power of 2n DV_SCALE_FACTOR
         */
        return (interpTable_p[index].interpolateToX + ((dv * scaleFactor) >> DV_SCALE_FACTOR));
    }
    else
    {
        /* This is an exact match with points in the map table,
         * so no need to interpolate, just return the value.
         */
        return interpTable_p[index].interpolateToX;
    }
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Linear interpolation binary search table map of points
 *
 *   \param interpTable_p Pointer to interpolation table map of points.
 *   \param tableSize Size of the table (no. of points)
 *   \param index set to LU_TRUE if this is a fragmented message
 *   \param scaleFactor Result scale factor up scaled by 2n DV_SCALE_FACTOR.
 *   \param x Input value to be intperolated via the table map.
 *
 *   \return None
 *
 ******************************************************************************
 */
void lnInterp1DS16Search( LnInterpTable1DU16S16Str *interpTable_p     ,
                          lu_uint8_t               tableSize          ,
                          lu_uint8_t               *index_p           , 
                          lu_uint64_t              *scaleFactor_p     ,
                          lu_uint16_t              x
                        )
{
	lu_uint64_t   delta;
	lu_uint64_t   divX;
    lu_uint8_t    max;
    lu_uint8_t    min;
    
    /* Ensure limits are trimmed within outer limits of the table point map
     */
     
    if (x < interpTable_p[0].distributionX)
    {
        *index_p = 0;
        return;
    }
    
    if (x > interpTable_p[(tableSize - 1)].distributionX)
    {
        *index_p = (tableSize - 1);
        return;
    }
   
    /* Now do the binary search...
     */
     
    min = 0;
    max = (tableSize - 1);
    
    /* Start in the middle
     */
    *index_p = (max / 2); 
    
    while (*index_p > min)
    {
        if (x < interpTable_p[*index_p].distributionX)
        {
            max = *index_p;
        }
        else
        {
            min = *index_p;
        }
        /* Get the middle point
         */
        *index_p = ((min + max) / 2);
    }

    /* Calculate between the two points..
     */
    delta = (x - interpTable_p[*index_p].distributionX);
    
    if (delta != 0)
    {
        divX = (interpTable_p[(*index_p + 1)].distributionX - interpTable_p[*index_p].distributionX);
        
        /* Scale by 2n DV_SCALE_FACTOR to prevent loss of resolution
         */
        *scaleFactor_p = ((delta << DV_SCALE_FACTOR) / divX);
    }
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */

