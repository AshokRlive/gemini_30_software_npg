/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: crc32.h 2238 2013-01-15 12:51:31Z pueyos_a $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/common/library/crc32/include/crc32.h $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28/09/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _CRC32_INCLUDED
#define _CRC32_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Init crc32 to 0xffffffff
 *
 *   Call to init crc before starting the calculation
 *
 *   \param none
 *
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void crc32_init(lu_uint32_t *crc32Ptr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *    (set crc32 to 0xffffffff - crc32_init() before you begin the calculation)
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern void crc32_byte(lu_uint8_t data, lu_uint32_t *crc32Ptr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *   (set crc32 to 0xffff - crc32_init() before you begin the calculation)
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern void crc32_calc32(lu_uint8_t *data_p, lu_uint32_t len, lu_uint32_t *crc32Ptr);

#ifdef __cplusplus
}
#endif


#endif /* _CRC32_INCLUDED */

/*
 *********************** End of file ******************************************
 */
