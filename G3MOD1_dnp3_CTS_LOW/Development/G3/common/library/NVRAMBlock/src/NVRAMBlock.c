/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: crc32.c 1729 2012-09-11 20:41:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/library/crc32/src/crc32.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28/09/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "NVRAMBlock.h"
#include "crc32.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_bool_t NVRAMBlockCalcCRC(lu_uint8_t *dataBlkRamPtr, NVRAM_TYPE nvramType, lu_uint8_t nvramBlkId, lu_uint32_t *crcPtr)
{
	lu_uint32_t crc32;
	lu_uint8_t *nvramBlkPtr;
	lu_uint8_t *nvramBlkDataPtr;
	lu_uint16_t dataSize;
	lu_uint16_t offset;
	NVRAMBlkHeadStr *nvramBlkHeaderPtr;

	/* Set pointer to base memory for NVRAM copy */
	nvramBlkPtr = dataBlkRamPtr;

	if (getNVRAMBlockOffsetAndSize(nvramType, nvramBlkId, &offset, &dataSize))
	{
		/* Add offset for block type */
		nvramBlkPtr += offset;

		/* Set pointer to block header */
		nvramBlkHeaderPtr = (NVRAMBlkHeadStr *)nvramBlkPtr;

		nvramBlkDataPtr  = nvramBlkPtr;
		nvramBlkDataPtr += sizeof(NVRAMBlkHeadStr);

		/* Check data block integrity crc ?? */
		crc32_calc32((lu_uint8_t * )nvramBlkDataPtr, dataSize, &crc32);

		/* return the crc32 */
		*crcPtr = crc32;

		return LU_TRUE;
	}
	else
	{
		return LU_FALSE;
	}
}

lu_bool_t NVRAMBlockCheckGoodCRC(lu_uint8_t *dataBlkRamPtr, NVRAM_TYPE nvramType, lu_uint8_t nvramBlkId)
{
	lu_uint32_t crc32 = 0xFFFFFFFF;
	lu_uint8_t *nvramBlkPtr;
	lu_uint16_t dataSize;
	lu_uint16_t offset;
	NVRAMBlkHeadStr *nvramBlkHeaderPtr;


	if (dataBlkRamPtr == 0)
	{
	    return LU_FALSE;
	}

	/* Set pointer to base memory for NVRAM copy */
	nvramBlkPtr = dataBlkRamPtr;

	if (getNVRAMBlockOffsetAndSize(nvramType, nvramBlkId, &offset, &dataSize))
	{
		/* Add offset for block type */
		nvramBlkPtr += offset;

		/* Set pointer to block header */
		nvramBlkHeaderPtr  = (NVRAMBlkHeadStr *)nvramBlkPtr;
		nvramBlkPtr       += sizeof(NVRAMBlkHeadStr);

		/* Check data block integrity crc */
		crc32_calc32((lu_uint8_t * )nvramBlkPtr, nvramBlkHeaderPtr->dataSize, &crc32);

		if (crc32 == nvramBlkHeaderPtr->dataCrc32)
		{
			return LU_TRUE;
		}
	}

	return LU_FALSE;
}

lu_bool_t NVRAMBlockGetPointerAndSize(lu_uint8_t *nvramPtr, NVRAM_TYPE nvramType, lu_uint8_t nvramBlkId, lu_uint8_t **hedPtr, lu_uint8_t **dataPtr, lu_uint16_t *sizePtr, lu_uint16_t *blkOffsetPtr)
{
	lu_uint8_t *nvramDataBlkPtr;
	lu_uint8_t *nvramHedBlkPtr;
	lu_uint16_t blkSize;
	lu_uint16_t blkOffset;
	NVRAMBlkHeadStr *nvramBlkHeaderPtr;

	if (getNVRAMBlockOffsetAndSize(nvramType, nvramBlkId, &blkOffset, &blkSize))
	{
		/* Set pointer to block header */
		nvramHedBlkPtr  = nvramPtr;
		nvramHedBlkPtr += blkOffset;
		*hedPtr         = nvramHedBlkPtr;

		/* Calculate offset to data within block */
		nvramDataBlkPtr  = nvramPtr;
		nvramDataBlkPtr += blkOffset;
		nvramDataBlkPtr += sizeof(NVRAMBlkHeadStr);
		*dataPtr = nvramDataBlkPtr;

		/* Set block offset */
		*blkOffsetPtr = blkOffset;

		/* Adjust size to max data size available within a block */
		*sizePtr = (blkSize - sizeof(NVRAMBlkHeadStr));

		return LU_TRUE;
	}

	return LU_FALSE;
}

lu_bool_t NVRAMBlockCheckParamsOK(NVRAM_TYPE nvramType, lu_uint8_t nvramBlkId)
{
	lu_uint8_t *nvramDataBlkPtr;
	lu_uint16_t blkSize;
	lu_uint16_t blkOffset;
	NVRAMBlkHeadStr *nvramBlkHeaderPtr;

	if (getNVRAMBlockOffsetAndSize(nvramType, nvramBlkId, &blkOffset, &blkSize))
	{
		return LU_TRUE;
	}

	return LU_FALSE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
lu_bool_t getNVRAMBlockOffsetAndSize(NVRAM_TYPE nvramType, lu_uint8_t nvramBlkId, lu_uint16_t *offsetPtr, lu_uint16_t *sizePtr)
{
    if ((offsetPtr == 0) || (sizePtr == 0))
    {
        return LU_FALSE;
    }

	switch (nvramType)
	{
	case NVRAM_TYPE_IDENTITY:
		switch (nvramBlkId)
		{
		case NVRAM_ID_BLK_INFO:
			*offsetPtr        = NVRAM_ID_BLK_INFO_OFFSET;
			*sizePtr          = NVRAM_ID_BLK_INFO_SIZE;
			break;

		case NVRAM_ID_BLK_OPTS:
			*offsetPtr        = NVRAM_ID_BLK_OPTS_OFFSET;
			*sizePtr          = NVRAM_ID_BLK_OPTS_SIZE;
			break;

		case NVRAM_ID_BLK_TEST:
			*offsetPtr        = NVRAM_ID_BLK_TEST_OFFSET;
			*sizePtr          = NVRAM_ID_BLK_TEST_SIZE;
			break;

		case NVRAM_ID_BLK_CAL:
			*offsetPtr        = NVRAM_ID_BLK_CAL_OFFSET;
			*sizePtr          = NVRAM_ID_BLK_CAL_SIZE;
			break;

		default:
			return LU_FALSE;
			break;
		}
		break;

	case NVRAM_TYPE_APPLICATION:
		switch (nvramBlkId)
		{
		case NVRAM_APP_BLK_INFO:
			*offsetPtr        = NVRAM_APP_BLK_INFO_OFFSET;
			*sizePtr          = NVRAM_APP_BLK_INFO_SIZE;
			break;

		case NVRAM_APP_BLK_USER_A:
			*offsetPtr        = NVRAM_APP_BLK_USER_A_OFFSET;
			*sizePtr          = NVRAM_APP_BLK_USER_A_SIZE;
			break;

		case NVRAM_APP_BLK_REG:
			*offsetPtr        = NVRAM_APP_BLK_REG_OFFSET;
			*sizePtr          = NVRAM_APP_BLK_REG_SIZE;
			break;

		case NVRAM_ID_BLK_CAL:
			*offsetPtr       = NVRAM_APP_BLK_CAL_OFFSET;
			*sizePtr          = NVRAM_APP_BLK_CAL_SIZE;
			break;

		default:
			return LU_FALSE;
			break;
		}
		break;

	case NVRAM_TYPE_BATTERY_ID:
		switch (nvramBlkId)
		{
		case NVRAM_BATT_ID_BLK_INFO:
			*offsetPtr        = NVRAM_BATT_ID_BLK_INFO_OFFSET;
			*sizePtr          = NVRAM_BATT_ID_BLK_INFO_SIZE;
			break;

		case NVRAM_BATT_ID_BLK_TEST:
			*offsetPtr        = NVRAM_BATT_ID_BLK_TEST_OFFSET;
			*sizePtr          = NVRAM_BATT_ID_BLK_TEST_SIZE;
			break;

		case NVRAM_BATT_ID_BLK_CAL:
			*offsetPtr        = NVRAM_BATT_ID_BLK_CAL_OFFSET;
			*sizePtr          = NVRAM_BATT_ID_BLK_CAL_SIZE;
			break;

		default:
			return LU_FALSE;
			break;
		}
		break;

	case NVRAM_TYPE_BATTERY_DATA:
		switch (nvramBlkId)
		{
		case NVRAM_BATT_DATA_BLK_INFO:
			*offsetPtr        = NVRAM_BATT_DATA_BLK_INFO_OFFSET;
			*sizePtr          = NVRAM_BATT_DATA_BLK_INFO_SIZE;
			break;

		case NVRAM_BATT_DATA_BLK_USER_A:
			*offsetPtr        = NVRAM_BATT_DATA_BLK_USER_A_OFFSET;
			*sizePtr          = NVRAM_BATT_DATA_BLK_USER_A_SIZE;
			break;

		case NVRAM_BATT_DATA_BLK_USER_B:
			*offsetPtr        = NVRAM_BATT_DATA_BLK_USER_B_OFFSET;
			*sizePtr          = NVRAM_BATT_DATA_BLK_USER_B_SIZE;
			break;

		default:
			return LU_FALSE;
			break;
		}
		break;

	default:
		return LU_FALSE;
		break;
	}

	return LU_TRUE;
}

/*
 *********************** End of file ******************************************
 */
