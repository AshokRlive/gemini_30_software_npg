/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: crc32.h 1729 2012-09-11 20:41:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/library/crc32/include/crc32.h $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28/09/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _NVRAMBLOCK_INCLUDED
#define _NVRAMBLOCK_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "NVRAMDef.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Calculate NVRAM block CRC
 *
 *   Calculate the NVRAM block CRC
 *
 *   \param dataBlkRamPtr - pointer to base memory copy of NVRAM for chip
 *   \param nvramType - NVRAM_TYPE_* (The NVRAM chip to use)
 *   \param nvramBlkId - NVRAM_ID_BLK_*, NVRAM_APP_BLK_*, etc (Block to use)
 *   \param crcPtr - pointer to return the calculated crc for the block
 *
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t NVRAMBlockCalcCRC(lu_uint8_t *dataBlkRamPtr, NVRAM_TYPE nvramType, lu_uint8_t nvramBlkId, lu_uint32_t *crcPtr);

/*!
 ******************************************************************************
 *   \brief Check NVRAM block CRC
 *
 *   Calculate the NVRAM block CRC
 *
 *   \param dataBlkRamPtr - pointer to base memory copy of NVRAM for chip
 *   \param nvramType - NVRAM_TYPE_* (The NVRAM chip to use)
 *   \param nvramBlkId - NVRAM_ID_BLK_*, NVRAM_APP_BLK_*, etc (Block to use)
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t NVRAMBlockCheckGoodCRC(lu_uint8_t *dataBlkRamPtr, NVRAM_TYPE nvramType, lu_uint8_t nvramBlkId);

/*!
 ******************************************************************************
 *   \brief Calculate NVRAM block CRC
 *
 *   Calculate the NVRAM block CRC
 *
 *   \param none
 *
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t NVRAMBlockGetPointerAndSize(lu_uint8_t *nvramPtr, NVRAM_TYPE nvramType, lu_uint8_t nvramBlkId, lu_uint8_t **hedPtr, lu_uint8_t **dataPtr, lu_uint16_t *sizePtr, lu_uint16_t *blkOffsetPtr);

/*!
 ******************************************************************************
 *   \brief Validate NVRAMType / Blk number is OK
 *
 *   Check parameter OK
 *
 *   \param none
 *
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t NVRAMBlockCheckParamsOK(NVRAM_TYPE nvramType, lu_uint8_t nvramBlkId);

/*!
 ******************************************************************************
 *   \brief Return offset and size of block
 *
 *   \param nvramType
 *   \param nvramBlkId
 *   \param offsetPtr
 *   \param sizePtr
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
extern lu_bool_t getNVRAMBlockOffsetAndSize(NVRAM_TYPE nvramType, lu_uint8_t nvramBlkId, lu_uint16_t *offsetPtr, lu_uint16_t *sizePtr);

#ifdef __cplusplus
}
#endif

#endif /* _NVRAMBLOCK_INCLUDED */

/*
 *********************** End of file ******************************************
 */
