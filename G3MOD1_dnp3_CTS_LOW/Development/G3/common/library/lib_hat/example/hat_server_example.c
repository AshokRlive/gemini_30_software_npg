
#include "hat_server.h"
#include "hat_utils.h"
#include "../src/hat_dbg.h"

static HAT_ERR hat_handler_impl(const HatMessage* request, HatMessage* reply)
{
    DBG_INFO("hat_handler_impl() called id:%d", request->header.id);

    reply->header.id   = request->header.id + 1;

    switch (request->header.id)
    {
        case HAT_MSG_ID_HATVERSION_C:
        {
            HatProtocolVersion versionHAT;
            versionHAT.version.major = HAT_PROTOCOL_VERSION_MAJOR;
            versionHAT.version.minor = HAT_PROTOCOL_VERSION_MINOR;

            memcpy(reply->payload, &versionHAT, sizeof(versionHAT));
            reply->payloadSize = sizeof(versionHAT);
        }
        break;

        case HAT_MSG_ID_VERSION_C:
        {
            HatVersion version;
            strcpy(version.versionAPI, "API  1.0");
            strcpy(version.versionSDP, "SDPI 2.0");
            strcpy(version.versionSVN, "SVN  3.0" );

            memcpy(reply->payload, &version, sizeof(version));
            reply->payloadSize = sizeof(version);
        }
        break;

        default:
            DBG_ERR("HAT server Example - unsupported message, id:%d", request->header.id);
            return HAT_ERR_UNSUPPORTED;
    }

    return HAT_ERR_NONE;
}


lu_int32_t main(lu_int32_t argc, lu_char_t **argv) {
    if(hat_server_init(5000, &hat_handler_impl) == HAT_ERR_NONE)
    {
        hat_server_run();
    }
    else
    {
        DBG_ERR("Failed to initialise server!");
    }
}
