#include "hat_client.h"
#include "hat_utils.h"
#include "../src/hat_dbg.h"
#include <sys/timeb.h>
#include <stdlib.h>
#include <stdio.h>

#define NUMTESTS 8  /* Number of AUTOMATIC tests including test 0 */

/* CAN-specific data */
typedef struct CANHeaderDef
{
    /*! Set to 1 if this is a fragment */
    lu_uint32_t fragment    : 1;
    lu_uint32_t deviceIDSrc : 3;
    lu_uint32_t deviceSrc   : 5;
    lu_uint32_t deviceIDDst : 3;
    lu_uint32_t deviceDst   : 5;
    lu_uint32_t messageID   : 6;
    lu_uint32_t messageType : 6;
}CANHeaderStr;

/* Serial-specific data */
typedef enum
{
    SERIAL_MODE_8N1 = 1,    /* 8N1 */
    SERIAL_MODE_7E1,
    SERIAL_MODE_7O1,
    SERIAL_MODE_8E2,
    SERIAL_MODE_LAST
} SERIAL_MODE;
typedef struct SerialOptionDef
{
    lu_uint32_t portNum;
    lu_uint32_t baud;
    SERIAL_MODE mode;   /* Enumerated mode: 8N1, 7E1, etc */
} SerialOptionStr;
typedef struct SerialTypeDef
{
    lu_uint32_t portNum;
    HAT_SERIAL_PORT_ID portID;
    lu_char_t* name;
} SerialTypeStr;
static const SerialTypeStr serialTypeTable[] =
{
    /* Number   portID                      Name on nameplate */
    {   1,      HAT_SERIAL_PORT_ID_232A,    "RS232-A" },
    {   2,      HAT_SERIAL_PORT_ID_232B,    "RS232-B" },
    {   3,      HAT_SERIAL_PORT_ID_485,     "RS485" }
};
#define SERIALTYPETABLE_SIZE LU_ARRAY_LENGTH(serialTypeTable)
#define SERIALMESSAGE "Testing Serial Communications"
#define SERIAL_TIMEOUT_MS 3000
static const HatSerialCfg defaultSerialCfg = {  HAT_SERIAL_PORT_ID_232A,
                                                HAT_SERIAL_BAUD_RATE_115200,
                                                HAT_SERIAL_DATA_BITS_8,
                                                HAT_SERIAL_STOP_BITS_1,
                                                HAT_SERIAL_PARITY_NONE,
                                                HAT_SERIAL_MODE_NONE
                                            };
static const SerialOptionStr defaultSerialOpt = {1, HAT_SERIAL_BAUD_RATE_115200, SERIAL_MODE_8N1};

/* Global options set by user */
static SerialOptionStr serialOptions;   /* Serial-specific */


#pragma pack(push)
#pragma pack(1)
/**
 * \brief Generic Version basic structure
 */
typedef struct BasicVersionDef
{
    lu_uint32_t major;
    lu_uint32_t minor;
} BasicVersionStr;
#pragma pack(pop)

#define FAIL(name, err) DBG_ERR("%s: FAILED! err:%d",name,err);exit(1)

void printCANHeader(lu_char_t* name, CANHeaderStr* headerPtr)
{
    DBG_INFO("%s: type:%d, id:%d, src:%d, srcID:%d, dst:%d, dstID:%d",
                    name,
                    headerPtr->messageType,
                    headerPtr->messageID,
                    headerPtr->deviceSrc,
                    headerPtr->deviceIDSrc,
                    headerPtr->deviceDst,
                    headerPtr->deviceIDDst
                    );

}

void testGetVersion()
{
    HatProtocolVersion ret;
    HAT_ERR err;

    DBG_INFO("testGetVersion start");

    if ((err = hat_get_protocol_version(&ret)) == HAT_ERR_NONE)
    {
        DBG_INFO("testGetVersion: SUCCEEDED!");
        DBG_INFO("=====HAT Version=====\n\t%u.%u\n", ret.version.major, ret.version.minor);
    }
    else
    {
        FAIL("testGetVersion",err);
    }
    printf("\n");
}

void testGetAPIVersion()
{
    HatVersion ret;
    HAT_ERR err;

    DBG_INFO("testGetAPIVersion start");

    if ((err = hat_get_version(&ret)) == HAT_ERR_NONE)
    {
        DBG_INFO("testGetAPIVersion: SUCCEEDED!");
        DBG_INFO("=====API Version=====\n\t%s\n\t%s\n\t%s\n", ret.versionAPI,
                        ret.versionSDP, ret.versionSVN);
    }
    else
    {
        FAIL("testGetAPIVersion",err);
    }
    printf("\n");
}

void SlaveBoardSendCAN()
{
    lu_int16_t canBus =0;
    lu_int32_t timeoutMs = 2000;
    lu_uint8_t payload[2] = {0x00, 0x02};
/*    lu_uint8_t payload[19] = {2, 0, 0, 16, 1, 14, 1, 15, 1, 16, 0, 23, 0, 24, 1, 4, 1, 8, 1}; */
    lu_uint8_t sendBuf[sizeof(payload) + sizeof(CANHeaderStr)];
    lu_int16_t sendSize = sizeof(sendBuf);
    lu_uint8_t rcvBuf[HAT_CAN_MAX_SIZE];
    lu_int16_t rcvSize  =0;
    HAT_ERR res;

    CANHeaderStr header;
    header.fragment    = 0;
    header.deviceIDSrc = 0;
    header.deviceSrc   = 1;
    header.deviceIDDst = 0;
    header.deviceDst   = 0;
    header.messageType = 0x33;  /* Bootloader Test */
    header.messageID   = 0x0c;  /* Read ADC */

    DBG_INFO("TestSlaveBoardSendCAN");
 
    memcpy(sendBuf, &header, sizeof(header));
    memcpy(sendBuf+sizeof(header), payload, sizeof(payload));

    dump_data("Can Send:",sendBuf,sendSize);

    res = hat_send_can2(canBus, timeoutMs, (char*)sendBuf, sendSize, &rcvBuf[0], &rcvSize);
    if (res == HAT_ERR_NONE)
   {
       dump_data("Can Received:",rcvBuf,rcvSize);
       DBG_INFO("SlaveBoardSendCAN: SUCCEEDED!\nRcv Size:%d",rcvSize);
   }
   else
   {
       FAIL("SlaveBoardSendCAN!", res);
   }

   printf("\n");
}

void testSendCAN2()
{
    lu_int16_t canBus =0;
    lu_int32_t timeoutMs = 200;
    lu_uint8_t sendBuf[HAT_CAN_MAX_SIZE];
    lu_int16_t sendSize =0;
    lu_uint8_t* rcvBuf = NULL;  /* Note that we reuse sendBuf as incoming data as well */
    lu_int16_t rcvSize  =0;
    HAT_ERR res;

    CANHeaderStr header;
    header.fragment    = 0;
    header.deviceIDSrc = 0;
    header.deviceSrc   = 1;
    header.deviceIDDst = 0;
    header.deviceDst   = 0;
    header.messageType = 0x3F;
    header.messageID   = 0x02; /* module info */
    memcpy(&sendBuf, &header, sizeof(header));
    sendSize = sizeof header;

    rcvBuf = &(sendBuf[0]);
    dump_data("Can Send:",sendBuf,sendSize);

    res = hat_send_can2(canBus, timeoutMs, &(sendBuf[0]), sendSize, rcvBuf, &rcvSize);
    if (res == HAT_ERR_NONE)
    {
        dump_data("Can Received:",rcvBuf,rcvSize);
        DBG_INFO("Received Data Length: %d", rcvSize);
    }
    else
    {
        FAIL("testSendCAN2!", res);
    }
    printf("\n");
}

void testSendCAN()
{
    
    HatCANSend send;
    HatCANReceive receive;
    HAT_ERR err;
    const lu_int32_t expectedSize = 61 + sizeof(CANHeaderStr);
    CANHeaderStr* header = (CANHeaderStr*)send.sendBuf;

    memset(&send,    0, sizeof send);
    memset(&receive, 0, sizeof receive);

    send.canBus = 0;
    send.sendSize = sizeof(CANHeaderStr);
    send.timeoutMs = 11;

    
    header->fragment    = 0;
    header->deviceIDSrc = 0;
    header->deviceSrc   = 1;
    header->deviceIDDst = 0;
    header->deviceDst   = 0;
    header->messageType = 0x3F;
    header->messageID   = 0x02;

    printCANHeader("CANSend",header);

    err = hat_send_can(&send, &receive);
    if( (err == HAT_ERR_NONE) && (receive.rcvSize == expectedSize) )
    {
        CANHeaderStr* replyHeader = (CANHeaderStr* )receive.rcvBuf;
        printCANHeader("CANReply",replyHeader);
        DBG_INFO("testSendCAN: SUCCEEDED!\nRcv Data: %d", receive.rcvSize);
    }
    else
    {
        FAIL("testSendCAN",err);
    }
    printf("\n");
}

void repeat_test_CAN()
{
    struct timeb start, end;
    int diff;
    int var;

    for (var = 0; var < 200; ++var)
    {
        ftime(&start);

        testSendCAN();

        ftime(&end);
        diff = (int)((1000.0 * (end.time - start.time)) + (end.millitm - start.millitm));

        if(diff > 200)
        {
            printf("\n =========== Operation took %u milliseconds. index:%d=========== \n", diff,var);
        }
    }
    printf("\n");
}


void prepareTestSerial(HatSerialCfg* hatSerialCfg)
{
    HAT_ERR ret;

    /* Check serial-specific options */
    if( (serialOptions.portNum == 0) || (serialOptions.portNum > HAT_SERIAL_PORT_ID_LAST) )
    {
        printf("Invalid parameter value: -sp=%u\n", serialOptions.portNum);
        exit(1);
    }

    *hatSerialCfg = defaultSerialCfg;
    hatSerialCfg->portID = serialTypeTable[serialOptions.portNum - 1].portID;
    hatSerialCfg->baudrate = serialOptions.baud;
    switch (serialOptions.mode)
    {
        case SERIAL_MODE_7E1:
            hatSerialCfg->dataBits = HAT_SERIAL_DATA_BITS_7;
            hatSerialCfg->stopBits = HAT_SERIAL_STOP_BITS_1;
            hatSerialCfg->parity = HAT_SERIAL_PARITY_EVEN;
            break;
        case SERIAL_MODE_7O1:
            hatSerialCfg->dataBits = HAT_SERIAL_DATA_BITS_7;
            hatSerialCfg->stopBits = HAT_SERIAL_STOP_BITS_1;
            hatSerialCfg->parity = HAT_SERIAL_PARITY_ODD;
            break;
        case SERIAL_MODE_8E2:
            hatSerialCfg->dataBits = HAT_SERIAL_DATA_BITS_8;
            hatSerialCfg->stopBits = HAT_SERIAL_STOP_BITS_2;
            hatSerialCfg->parity = HAT_SERIAL_PARITY_EVEN;
            break;
        case SERIAL_MODE_8N1:
        default:
            break;
    }
    printf("Testing serial port%i %s, @%u %u%c%u\n",
            hatSerialCfg->portID,
            serialTypeTable[serialOptions.portNum - 1].name,
            hatSerialCfg->baudrate,
            hatSerialCfg->dataBits,
            (hatSerialCfg->parity == HAT_SERIAL_PARITY_NONE)? 'N' :
                (hatSerialCfg->parity == HAT_SERIAL_PARITY_EVEN)? 'E' : 'O',
            hatSerialCfg->stopBits
            );
    ret = hat_serial_cfg(hatSerialCfg->portID,
                         hatSerialCfg->baudrate,
                         hatSerialCfg->dataBits,
                         hatSerialCfg->stopBits,
                         hatSerialCfg->parity,
                         hatSerialCfg->portMode);
    if(ret != HAT_ERR_NONE)
    {
        FAIL("Failed to configure Serial Port!!!\n", ret);
    }
}


void testSerialAsync()
{
    HAT_ERR ret;
    HatSerialCfg hatSerialCfg;
    lu_char_t serialMsg[] = SERIALMESSAGE;
    lu_uint8_t rcvBuf[HAT_SERIAL_MAX_RCV];
    lu_uint32_t rcvSize = HAT_SERIAL_MAX_RCV;


    prepareTestSerial(&hatSerialCfg);
    printf("Sending %u bytes, using %u ms timeout\n", strlen(SERIALMESSAGE)+1, SERIAL_TIMEOUT_MS);
    ret = hat_serial_send(hatSerialCfg.portID, &(serialMsg[0]), strlen(SERIALMESSAGE)+1);
    if(ret != HAT_ERR_NONE)
    {
        FAIL("Failed to send data to Serial Port!!!\n", ret);
    }
    printf("Data sent successfully\n");

    ret = hat_serial_rcv(hatSerialCfg.portID, SERIAL_TIMEOUT_MS, rcvBuf, &rcvSize);
    if(ret != HAT_ERR_NONE)
    {
        FAIL("Failed to receive data from Serial Port!!!\n", ret);
    }
    rcvBuf[LU_MIN(HAT_SERIAL_MAX_RCV - 1, rcvSize + 1)] = '\0'; /* Add terminator for safer printing */
    printf("--SUCCESS! Received %u bytes from serial port%i %s: '%s'\n",
            rcvSize,
            hatSerialCfg.portID,
            serialTypeTable[serialOptions.portNum - 1].name,
            (lu_char_t*)rcvBuf
            );
}


void testSerialSync()
{
    HAT_ERR ret;
    HatSerialCfg hatSerialCfg;
    lu_char_t serialMsg[] = SERIALMESSAGE;
    lu_uint8_t rcvBuf[HAT_SERIAL_MAX_RCV];
    lu_uint32_t rcvSize = HAT_SERIAL_MAX_RCV;


    prepareTestSerial(&hatSerialCfg);
    printf("Sending %u bytes, using %u ms timeout\n", strlen(SERIALMESSAGE)+1, SERIAL_TIMEOUT_MS);
    ret = hat_serial_sendRcv(hatSerialCfg.portID,
                            SERIAL_TIMEOUT_MS,
                            &(serialMsg[0]),
                            strlen(SERIALMESSAGE)+1,
                            rcvBuf,
                            &rcvSize);
    if(ret != HAT_ERR_NONE)
    {
        FAIL("Failed to send/receive data from Serial Port!!!\n", ret);
    }
    rcvBuf[LU_MIN(HAT_SERIAL_MAX_RCV - 1, rcvSize + 1)] = '\0'; /* Add terminator for safer printing */
    printf("--SUCCESS! Received %u bytes from serial port%i %s: '%s'\n",
            rcvSize,
            hatSerialCfg.portID,
            serialTypeTable[serialOptions.portNum - 1].name,
            (lu_char_t*)rcvBuf
            );
}


void start_test(const lu_uint32_t value)
{
    lu_uint32_t i;
    if(value == 0)
    {
        printf("***** Starting ALL TESTS *****\n");
    }
    else
    {
        printf("***** Starting test num %d *****\n", value);
    }
    switch (value)
    {
        case 0:
            /* Run all the tests */
            for(i=1; i<=NUMTESTS; i++)
            {
                start_test(i);
            }
            break;
        case 1:
            testGetVersion();
            break;
        case 2:
            testGetAPIVersion();
            break;
        case 3:
            testSendCAN();
            break;
        case 4:
            testSendCAN2();
            break;
        case 5:
            SlaveBoardSendCAN();
            break;
        case 6:
            testSerialAsync();
            break;
        case 7:
            testSerialSync();
            break;
        case 33:
            repeat_test_CAN();
            break;
        case NUMTESTS:  /* Prevent NUMTESTS and cases mismatch */
        default:
            printf("Error: Unrecognised test number\n");
            break;
    }
}

lu_int32_t main(lu_int32_t argc, lu_int8_t **argv)
{
    lu_int32_t i;
	lu_int16_t canBus =0;
    lu_int32_t timeoutMs = 100;
    lu_uint8_t payload[32];
    lu_uint8_t sendBuf[sizeof(CANHeaderStr) + sizeof(payload)];
    lu_int16_t sendSize = 0;
    lu_uint8_t rcvBuf[HAT_CAN_MAX_SIZE];
    lu_int16_t rcvSize  =0;
	lu_uint32_t autotest;
	lu_uint32_t value;
	lu_uint32_t data;
    CANHeaderStr header;

    header.messageType = 0;
    header.messageID = 0;
    header.deviceDst = 0x01;  /* MCM */
    header.deviceIDDst = 0;
    header.deviceSrc = 0x01;  /* MCM */
    header.deviceIDSrc = 0;
    header.fragment = 0;

    if (argc <= 2)
    {
        /* Print help */
        printf("\nRun Tests for Gemini 3's HAT protocol.\n"
                "Usage 1:\n"
                "--------\n"
                "  %s <hostIP> [-mt=<message_type>] [-mid=<message_ID>] "
                "[-dt=<destination>] [-did=<dest_addr>] [DATA]\n"
                "  Being:\n"
                "\t hostIP: IP address\n"
                "\t message_type: number type of CAN message (MODULE_MSG_TYPE_*)\n"
                "\t message_ID: number ID of CAN message (MODULE_MSG_ID_*)\n"
                "\t destination: number type of CAN module\n"
                "\t dest_addr: CAN ID of CAN module (module num-1)\n"
                "\t DATA: payload to send; hex values eparated by spaces, up to 32 bytes\n"
                "    Example: Send MODULE_MSG_ID_BLTST_ADC_READ_C to PSM1, channel 0, 2KHz\n"
                "\t\t%s 10.11.23.45 -mt=33 -dt=0 -did=0 -mid=0c 00 02\n"
                "  OR:\n"
                "Usage 2:\n"
                "--------\n"
                "  %s <hostIP> -t=<test_num> [-sp=<serial_port>] "
                "[-sb=<serial_baud>] [-sm=<serial_mode>]\n"
                "  Being <test_num> one of these AUTOMATIC tests:\n"
                "\t 0: All tests (once-only)\n"
                "\t 1: get protocol version\n"
                "\t 2: get Device's API version\n"
                "\t 3: Send test1 CAN message using generic message\n"
                "\t 4: Send test2 CAN message using parameterized message\n"
                "\t 5: Send PSM Slave Board Bootloader test CAN message\n"
                "\t 6: Test serial port echo asynchronously (send/receive separately)\n"
                "\t 7: Test serial port echo synchronously (send+receive in one go -- suitable for serial loopback)\n"
            /* Please note you need to modify NUMTESTS when adding more AUTO tests */
                "\t 33: repeat CAN test1\n"
                "  And <serial_port> one of these values:\n"
                "\t 1: RS232-A (default)\n"
                "\t 2: RS232-B\n"
                "\t 3: RS485\n"
                "  And <serial_baud> the serial speed in bauds (default 115200 bauds)\n"
                "  And <serial_mode> the serial data/parity/stop bits combo:\n"
                "\t 1: 8N1 (default)\n"
                "\t 2: 7E1\n"
                "\t 3: 7O1\n"
                "\t 4: 8E2\n"
                "\n", argv[0], argv[0]);
        return 0;
    }
	if(hat_client_init(argv[1], 5000) != HAT_ERR_NONE)
    {
        DBG_ERR("HAT client init error!");
        exit(1);
    }

	if (sscanf(argv[2], "-t=%x", &autotest) == 1)
    {
	    if( (autotest == 0) || (autotest == 6) || (autotest == 7) )
	    {
	        serialOptions = defaultSerialOpt;   /* copy default values */
	        /* Read serial-specific parameters */
	        for(i = 2; i < argc; i++)
            {
                /* Get message type */
                if(sscanf(argv[i], "-sp=%u", &value) == 1)
                {
                    serialOptions.portNum = value;
                }
                else if(sscanf(argv[i], "-sb=%u", &value) == 1)
                {
                    serialOptions.baud = value;
                }
                else if(sscanf(argv[i], "-sm=%u", &value) == 1)
                {
                    if( (value == 0) || (value >= SERIAL_MODE_LAST) )
                    {
                        printf("Invalid parameter value: -sm=%u", value);
                        exit(1);
                    }
                    serialOptions.mode = (SERIAL_MODE)value;
                }
            }
	    }
	    start_test(autotest);
        return 0;   /* Finished AUTOMATIC tests */
    }

	/* CAN Data test */
	header.fragment    = 0;
    header.deviceSrc   = 0x01;  /* MCM */
    header.deviceIDSrc = 0;

	for(i = 2; i < argc; i++)
	{
	    /* Get message type */
	    if (sscanf(argv[i], "-mt=%x", &value) == 1)
		{ 
			header.messageType = value; 
		}

		/* Get message ID */
		else if (sscanf(argv[i], "-mid=%x", &value) == 1)
		{
			header.messageID = value;
		}

		/* Get Destination (FDM/SCM/HMI/BRD/PSM/etc) */
		else if (sscanf(argv[i], "-dt=%x", &value) == 1)
		{
			header.deviceDst = value;
		}

		/* Get Destination Address */
		else if (sscanf(argv[i], "-did=%x", &value) == 1)
		{
			header.deviceIDDst = value;
		}

		else if (sscanf(argv[i], "%x", &data) == 1)
		{
			payload[sendSize] = (lu_uint8_t)(data & 0xFF);
			sendSize++;
		}
	}

/*	DBG_INFO("TestSlaveBoardSendCAN"); */

    memcpy(sendBuf, &header, sizeof(header));
    memcpy(sendBuf+sizeof(header), payload, sizeof(payload));

	sendSize = sendSize + sizeof(CANHeaderStr);

    /*dump_data("Can Send:",sendBuf,sendSize);*/

	HAT_ERR result;
	result = hat_send_can2(canBus, timeoutMs, (char*)sendBuf, sendSize,&rcvBuf[0], &rcvSize);
    if (result == HAT_ERR_NONE)
	{
       memcpy(&header, &rcvBuf[0], sizeof(CANHeaderStr));
	   
	   printf("\ndt=%x did=%x mt=%x mid=%x data=", header.deviceSrc, header.deviceIDSrc, header.messageType, header.messageID);

	   for(i = 0; i < (rcvSize - sizeof(CANHeaderStr)); i++)
	   {
		   printf(" %02x", rcvBuf[sizeof(CANHeaderStr) + i]);
	   }
	   
	   /*dump_data("Can Received:",rcvBuf,rcvSize);                   */
       /*DBG_INFO("SlaveBoardSendCAN: SUCCEEDED!Rcv Size:%d",rcvSize);*/
	}
	else
	{
       FAIL("Specific CAN message send test", result);
	}

    printf("\n");
}
