
set (EXAMPLE_DIR ${HAT_SOURCE_DIR}/example/)

include_directories(
	${HAT_SOURCE_DIR}/include
	${HAT_SOURCE_DIR}/src/
)
 

########################### CLIENT EXAMPLE ###################
SET (LINK_LIBRARIES 
	hat_client
	)
	
if (WIN32)
SET (LINK_LIBRARIES ${LINK_LIBRARIES}
	ws2_32
	)
endif()

set(EXE_NAME hat_client_example)

add_executable(${EXE_NAME} ${EXAMPLE_DIR}/hat_client_example.c)
add_dependencies(${EXE_NAME} hat_client)
add_dependencies(${EXE_NAME} hat_server)


target_link_libraries (${EXE_NAME} ${LINK_LIBRARIES})



########################### INSTALL BIN FILES ###################
install(TARGETS ${EXE_NAME} DESTINATION ${LIBRARY_INSTALL_DIR})



