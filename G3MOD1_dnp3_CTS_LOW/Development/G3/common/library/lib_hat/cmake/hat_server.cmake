if(NOT DEFINED HAT_SOURCE_DIR)
message(SEND_ERROR "!!!Please set the HAT_SOURCE_DIR in your project CMakeLists.txt")
endif()

set(SRCS
	${HAT_SOURCE_DIR}/src/hat_server.c
	${HAT_SOURCE_DIR}/src/hat_utils.c
	${HAT_SOURCE_DIR}/src/hat_socket.c
	${HAT_SOURCE_DIR}/src/hat_serialization.c
)

include_directories(
  ${HAT_SOURCE_DIR}/include  
  ${HAT_SOURCE_DIR}/src)
  
set(LIB_NAME hat_server)

add_library(${LIB_NAME} ${SRCS})

install(TARGETS ${LIB_NAME} DESTINATION ${LIBRARY_INSTALL_DIR})


