set output_dir=vs2010

del /f /q /s %output_dir%
rmdir /s /q %output_dir%
 
mkdir %output_dir%

cd %output_dir%
cmake -G "Visual Studio 10 2010" ..\..\cmake
cd ..
