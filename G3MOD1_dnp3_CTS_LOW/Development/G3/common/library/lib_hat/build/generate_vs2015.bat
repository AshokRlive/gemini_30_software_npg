set output_dir=vs2015

del /f /q /s %output_dir%
rmdir /s /q %output_dir%
 
mkdir %output_dir%

cd %output_dir%
cmake -G "Visual Studio 14 2015" ..\..\cmake
cd ..
