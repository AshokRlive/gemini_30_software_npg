set output_dir=mingw32

del /f /q /s %output_dir%
rmdir /s /q %output_dir%
 
mkdir %output_dir%

cd %output_dir%
cmake -G "MinGW Makefiles" ..\..\cmake
cd ..
