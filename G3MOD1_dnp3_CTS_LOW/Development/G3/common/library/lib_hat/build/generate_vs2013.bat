set output_dir=vs2013

del /f /q /s %output_dir%
rmdir /s /q %output_dir%
 
mkdir %output_dir%

cd %output_dir%
cmake -G "Visual Studio 12 2013" ..\..\cmake
cd ..
