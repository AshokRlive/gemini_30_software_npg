/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:hat_client.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 Sep 2015     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef HAT_CLIENT_H_
#define HAT_CLIENT_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "hat_protocol.h"

#ifdef __cplusplus
extern "C" {
#endif
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Initialises HAT client
 *
 * This function has to be called once before any other HAT operation takes place
 *
 * \param rtuIpAddress NULL-terminated C string specifying the HAT server IP address
 * \param port Ethernet port to use for communication, typically 5000
 *
 * \return Error code
 */
DLL_EXPORT HAT_ERR hat_client_init(lu_uint8_t* rtuIpAddress, lu_int32_t port);

/**
 * Close client socket.
 */
DLL_EXPORT HAT_ERR hat_client_close();

/**
 * \brief Sends a generic HAT message to the HAT server
 *
 * \param request Request message to send
 * \param reply Where to store the received data
 *
 * \return Error code
 */
DLL_EXPORT HAT_ERR hat_send_msg(const HatMessage* request, HatMessage* reply);

/**
 * \brief Sends a HAT message to retrieve the HAT protocol version
 *
 * \param receive Where to store the received data
 *
 * \return Error code
 */
DLL_EXPORT HAT_ERR hat_get_protocol_version(HatProtocolVersion* receive);

/**
 * \brief Sends a HAT message to retrieve the API version numbers
 *
 * \param receive Where to store the received data
 *
 * \return Error code
 */
DLL_EXPORT HAT_ERR hat_get_version(HatVersion* receive);

/**
 * \brief Sends a CAN message and waits for a reply, using structures.
 *
 * \param send CAN data to send.
 * \param receive CAN reply received
 *
 * \return error code
 */
DLL_EXPORT HAT_ERR hat_send_can(const HatCANSend* send, HatCANReceive* receive);

/**
 * \brief Sends a CAN message and waits for a reply, using parameters.
 *
 * \param canbus CAN bus ID (0 for can0, 1 for can1).
 * \param timeoutMs Time out for the expected reply, in milliseconds.
 * \param sendBuf CAN message to be sent.
 * \param sendSize Size of the CAN message to be sent.
 * \param rcvBuf Buffer to contain the received CAN reply. Note that has to be
 *              big enough for a CAN message, typically is:
 *              rcvBuf = (lu_uint8_t*)malloc(HAT_CAN_MAX_SIZE);
 * \param rcvSize Where to store the size of the received CAN reply.
 *
 * \return error code
 */
DLL_EXPORT HAT_ERR hat_send_can2( lu_int16_t canbus,
								  lu_int32_t timeoutMs,
								  lu_uint8_t* sendBuf,
								  lu_int16_t sendSize,
								  lu_uint8_t* rcvBuf,
								  lu_int16_t* rcvSize
								);

/**
 * \brief Sends HAT message for configuring a serial port
 *
 * \param portID ID number of the serial port (uses HAT_SERIAL_PORT_ID_*).
 * \param baudrate Serial port's speed     (uses HAT_SERIAL_BAUD_RATE_*).
 * \param dataBits Serial port's data Bits (uses HAT_SERIAL_DATA_BITS_*).
 * \param stopBits Serial port's stop Bits (uses HAT_SERIAL_STOP_BITS_*).
 * \param parity   Serial port's parity    (uses HAT_SERIAL_PARITY_*).
 * \param portMode Serial port's mode      (uses HAT_SERIAL_MODE_*).
 *
 * \return Error code
 */
DLL_EXPORT HAT_ERR hat_serial_cfg(const lu_uint32_t portID,
                                    const lu_uint32_t baudrate,
                                    const lu_uint8_t dataBits ,
                                    const lu_uint8_t stopBits ,
                                    const lu_uint8_t parity   ,
                                    const lu_uint8_t portMode);

/**
 * \brief Sends HAT message for sending data to a serial port
 *
 * \param portID ID number of the serial port (uses HAT_SERIAL_PORT_ID_*).
 * \param sendBuf Data to send throughout the serial port.
 * \param sendSize Size of data to be send.
 *
 * \return Error code
 */
DLL_EXPORT HAT_ERR hat_serial_send(const lu_uint32_t portID,
                                    lu_uint8_t* const sendBuf,
                                    const lu_uint8_t sendSize);

/**
 * \brief Sends HAT message for waiting for incoming data from a serial port
 *
 *  Please note that rcvSize must state the max size of the provided buffer,
 *  and later it is modified to reflect the size of the incoming data.
 *
 * \param portID ID number of the serial port (uses HAT_SERIAL_PORT_ID_*).
 * \param timeoutMs Max time to wait for incoming data, in milliseconds.
 * \param rcvBuf Where to store the received data.
 * \param rcvSize Max size of the data -- then overwritten with new size.
 *
 * \return Error code
 */
DLL_EXPORT HAT_ERR hat_serial_rcv(const lu_uint32_t portID,
                                    const lu_uint32_t timeoutMs,
                                    lu_uint8_t* const rcvBuf,
                                    lu_uint32_t* const rcvSize);

/**
 * \brief Sends data to a serial port and waits for incoming data
 *
 *  Please note that rcvSize must state the max size of the provided buffer,
 *  and later it is modified to reflect the size of the incoming data.
 *
 * \param portID ID number of the serial port (uses HAT_SERIAL_PORT_ID_*).
 * \param timeoutMs Max time to wait for incoming data, in milliseconds.
 * \param sendBuf Data to send throughout the serial port.
 * \param sendSize Size of data to be send.
 * \param rcvBuf Where to store the received data.
 * \param rcvSize Max size of the data -- then overwritten with new size.
 *
 * \return Error code
 */
DLL_EXPORT HAT_ERR hat_serial_sendRcv(const lu_uint32_t portID,
                                    const lu_uint32_t timeoutMs,
                                    lu_uint8_t* const sendBuf,
                                    const lu_uint8_t sendSize,
                                    lu_uint8_t* const rcvBuf,
                                    lu_uint32_t* const rcvSize);

#ifdef __cplusplus
}
#endif


#endif /* HAT_CLIENT_H_ */

/*
 *********************** End of file ******************************************
 */
