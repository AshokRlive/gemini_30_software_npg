/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: hat_protocol.h 12 Oct 2015 wang_p $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/library/lib_hat/include/hat_protocol.h $
 *
 *    FILE TYPE: C header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Hardware Access Test (HAT) Protocol definition.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: wang_p $: (Author of last commit)
 *       \date   $Date: 12 Oct 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12 Oct 2015   wang_p    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef _HAT_PROTOCOL_INCLUDED
#define _HAT_PROTOCOL_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stddef.h>    /* needed for NULL symbol usage */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "../../../include/lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions
 ******************************************************************************
 */

/* Hardware Access Test protocol version
 * NOTE: defined here in order to support different versions of the protocol.
 */
#define HAT_PROTOCOL_VERSION_MAJOR  0
#define HAT_PROTOCOL_VERSION_MINOR  2

#define HAT_TCP_PORT 5000

#ifdef WIN32
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT
#endif

#define HAT_MSG_MAX_SIZE 1024
#define HAT_MSG_HEADER_SIZE sizeof(HatHeader)
#define HAT_MSG_MAX_PAYLOAD_SIZE (HAT_MSG_MAX_SIZE - HAT_MSG_HEADER_SIZE)
#define HAT_VERSION_SIZE    52      /* Note it has to be multiple of 4 for alignment reasons */
#define HAT_CAN_MAX_SIZE    440
#define HAT_SERIAL_MAX_SEND (HAT_MSG_MAX_PAYLOAD_SIZE - 12)
#define HAT_SERIAL_MAX_RCV  (HAT_MSG_MAX_PAYLOAD_SIZE - 12)

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Enums
 ******************************************************************************
 */

/**
 * \brief HAT message IDs
 */
typedef enum
{
    HAT_MSG_ID_NACK                     = 0x00,
    HAT_MSG_ID_HATVERSION_C             ,
    HAT_MSG_ID_HATVERSION_R             ,       /*! HATProtocolVersion */
    HAT_MSG_ID_VERSION_C                ,
    HAT_MSG_ID_VERSION_R                ,       /*! HATVersion */

    /* CAN */
    HAT_MSG_ID_CAN_PROXY_C              ,       /*! HatCANSend */
    HAT_MSG_ID_CAN_PROXY_R              ,       /*! HatCANReceive */

    /* MCM IO */
    HAT_MSG_ID_MCM_IO_READ_C            ,       /*! HatPinIOReadReq */
    HAT_MSG_ID_MCM_IO_READ_R            ,       /*! HatPinIOData */
    HAT_MSG_ID_MCM_IO_WRITE_C           ,       /*! HatPinIOData */
    HAT_MSG_ID_MCM_IO_WRITE_R           ,       /*! HatResult */
    HAT_MSG_ID_MCM_IO_READ_DI_C         ,       /*! HatPinIOReadReq */
    HAT_MSG_ID_MCM_IO_READ_DI_R         ,       /*! HatPinIOData */
    HAT_MSG_ID_MCM_IO_WRITE_DO_C        ,       /*! HatPinIOData */
    HAT_MSG_ID_MCM_IO_WRITE_DO_R        ,       /*! HatResult */
    HAT_MSG_ID_MCM_IO_READ_AI_C         ,       /*! HatPinIOReadReq */
    HAT_MSG_ID_MCM_IO_READ_AI_R         ,       /*! HatPinIOData */

    /* Serial Port */
    HAT_MSG_ID_MCM_SERIAL_CONF_C        ,       /*! HatSerialCfg */
    HAT_MSG_ID_MCM_SERIAL_CONF_R        ,       /*! HatResult */
    HAT_MSG_ID_MCM_SERIAL_READ_C        ,       /*! HatSerialRcvRq */
    HAT_MSG_ID_MCM_SERIAL_READ_R        ,       /*! HatSerialRcv */
    HAT_MSG_ID_MCM_SERIAL_WRITE_C       ,       /*! HatSerialSend */
    HAT_MSG_ID_MCM_SERIAL_WRITE_R       ,       /*! HatResult */
    HAT_MSG_ID_MCM_SERIAL_READWRITE_C   ,       /*! HatSerialRcvRq */
    HAT_MSG_ID_MCM_SERIAL_READWRITE_R   ,       /*! HatSerialRcv */
    HAT_MSG_ID_LAST
} HAT_MSG_ID;

/**
 * \brief HAT error codes
 */
typedef enum
{
  HAT_ERR_NONE                          = 0X00,
  HAT_ERR_INIT                          = 0X01,
  HAT_ERR_NO_REPLY                      = 0X02,
  HAT_ERR_UNEXPECTED_REPLY              = 0X03,

  HAT_ERR_INVALID_MSG_SIZE              = 0X04,
  HAT_ERR_INVALID_PAYLOAD_SIZE          = 0X05,

  HAT_ERR_CAN_SEND_FAILURE              = 0X06,
  HAT_ERR_CAN_RECV_SIZE_INCORRECT       = 0X07,

  HAT_ERR_SOCKET_INIT                   = 0X08,
  HAT_ERR_SOCKET_CLOSED                 = 0X09,
  HAT_ERR_SOCKET_READ_ERR               = 0X0A,
  HAT_ERR_SOCKET_WRITE_ERR              = 0X0B,

  HAT_ERR_BADPARAMS                     = 0X0C,
  HAT_ERR_BADVERSION                    = 0X0D,

  HAT_ERR_UNSUPPORTED
} HAT_ERR;

/**
 * \brief GPIO type
 */
typedef enum
{
    HAT_GPIO_TYPE_DIGITAL_INPUT     = 0x00,
    HAT_GPIO_TYPE_DIGITAL_OUTPUT    ,
    HAT_GPIO_TYPE_ANALOGUE_INPUT    ,
    HAT_GPIO_TYPE_ANALOGUE_OUTPUT   ,

    HAT_GPIO_TYPE_LAST
} HAT_GPIO_TYPE;

/**
 * \brief RTU's serial port ID
 */
typedef enum
{
    /* Value 0 reserved for debug port */
    HAT_SERIAL_PORT_ID_232A = 1,
    HAT_SERIAL_PORT_ID_232B,
    HAT_SERIAL_PORT_ID_485,
    HAT_SERIAL_PORT_ID_LAST
} HAT_SERIAL_PORT_ID;

/**
 * \brief RTU serial port's parity
 */
typedef enum
{
  HAT_SERIAL_PARITY_NONE = 0,
  HAT_SERIAL_PARITY_EVEN,
  HAT_SERIAL_PARITY_ODD
} HAT_SERIAL_PARITY;

/**
 * \brief RTU serial port's amount of stop bits
 */
typedef enum
{
  HAT_SERIAL_STOP_BITS_1 = 1,
  HAT_SERIAL_STOP_BITS_2
} HAT_SERIAL_STOP_BITS;

/**
 * \brief RTU serial port's amount of data bits
 */
typedef enum
{
  HAT_SERIAL_DATA_BITS_7 = 7,
  HAT_SERIAL_DATA_BITS_8 = 8
} HAT_SERIAL_DATA_BITS;

/**
 * \brief RTU serial port's mode (handshake)
 */
typedef enum
{
  HAT_SERIAL_MODE_NONE,
  HAT_SERIAL_MODE_HARDWARE  /* Microsoft's Handshake enum value = RequestToSend */
} HAT_SERIAL_MODE;

/**
 * \brief RTU serial port's speed (in bauds)
 */
typedef enum
{
  HAT_SERIAL_BAUD_RATE_110     = 110,
  HAT_SERIAL_BAUD_RATE_300     = 300,
  HAT_SERIAL_BAUD_RATE_600     = 600,
  HAT_SERIAL_BAUD_RATE_1200    = 1200,
  HAT_SERIAL_BAUD_RATE_2400    = 2400,
  HAT_SERIAL_BAUD_RATE_4800    = 4800,
  HAT_SERIAL_BAUD_RATE_9600    = 9600,
  HAT_SERIAL_BAUD_RATE_19200   = 19200,
  HAT_SERIAL_BAUD_RATE_38400   = 38400,
  HAT_SERIAL_BAUD_RATE_57600   = 57600,
  HAT_SERIAL_BAUD_RATE_115200  = 115200,
  HAT_SERIAL_BAUD_RATE_230400  = 230400,
  HAT_SERIAL_BAUD_RATE_576000  = 576000,
  HAT_SERIAL_BAUD_RATE_921600  = 921600,
  HAT_SERIAL_BAUD_RATE_1152000 = 1152000
} HAT_SERIAL_BAUD_RATE;

/*
 ******************************************************************************
 * EXPORTED - Structures
 ******************************************************************************
 */
#pragma pack(push)
#pragma pack(1)

/**
 * \brief hardware I/O pin reference
 */
typedef struct HatIOPinIdStr
{
    lu_uint8_t  bank;
    lu_uint8_t  id;
} HatIOPinIDStr;

/**
 * \brief Generic Version basic structure
 */
typedef struct BasicVersion
{
    lu_uint32_t major;
    lu_uint32_t minor;
} BasicVersion;

/**
 * \brief Common HAT message header -- All HAT messages should have this header
 */
typedef struct HatHeader
{
    /* VERY IMPORTANT: Please keep the header 4-byte aligned - see note on
     * HatMessage struct.
     */
    lu_uint32_t id;         /* Uses HAT_MSG_ID */
} HatHeader;

/**
 * \brief Generic HAT message structure
 */
typedef struct HatMessage
{
    /* VERY IMPORTANT: Please keep the header 4-byte aligned so the payload
     * could point to a valid address. Operations over the payload member, such
     * as memcopy, would be wrong otherwise. This problem does not happen on x86
     * architectures but it is a problem on ARM or some other architectures.
     */
    HatHeader      header;
    lu_uint32_t    payloadSize;
    lu_uint8_t     payload[HAT_MSG_MAX_PAYLOAD_SIZE];
} HatMessage;

/**
 * \brief HAT protocol version
 */
typedef struct HatProtocolVersion
{
    BasicVersion version;   /* HAT protocol version info */
} HatProtocolVersion;

/**
 * \brief API version info
 */
typedef struct HatVersion
{
    lu_char_t versionAPI[HAT_VERSION_SIZE];     /* System (CAN) API version string */
    lu_char_t versionSDP[HAT_VERSION_SIZE];     /* Current SDP (Distrib. Package) version string */
    lu_char_t versionSVN[HAT_VERSION_SIZE];     /* Current SVN (Subversion) version string */
} HatVersion;

/**
 * \brief Synchronous CAN message sending request.
 */
typedef struct HatCANSend
{
    lu_uint16_t     canBus;                     /* CAN bus where to send the message */
    lu_uint16_t     sendSize;                   /* Size (Bytes) of the message to send */
    lu_uint32_t     timeoutMs;                  /* Timeout after sending the message */
    lu_uint8_t      sendBuf[HAT_CAN_MAX_SIZE];  /* CAN message to send */
} HatCANSend;

/**
 * \brief Synchronous CAN message receiving response.
 */
typedef struct HatCANReceive
{
    lu_uint16_t     canBus;                     /* CAN bus where the message was received */
    lu_uint16_t     rcvSize;                    /* Size (Bytes) of the message */
    lu_uint32_t     status;                     /*! Uses HAT_ERR */
    lu_uint8_t      rcvBuf[HAT_CAN_MAX_SIZE];   /* Received CAN message */
} HatCANReceive;

/**
 * \brief Device/Module information request
 */
typedef struct HatDeviceInfoReq
{
    lu_uint16_t     deviceType;         /*! Uses MODULE */
    lu_uint16_t     deviceID;           /*! Uses MODULE_ID */
} HatDeviceInfoReq;

/**
 * \brief I/O pin read value request
 */
typedef struct HatPinIOReadReq
{
    HatIOPinIDStr   gpio;
} HatPinIOReadReq;

/**
 * \brief I/O pin value data (req or rep)
 */
typedef struct HatPinIOData
{
    HatIOPinIDStr   gpio;           /* Pin ID */
    lu_uint16_t     type;           /*! Uses HAT_GPIO_TYPE */
    BasicVersion    featureRev;     /* Hardware feature revision */
    lu_uint32_t     status;         /*! Uses HAT_ERR */
    union ValueIOData
    {
        lu_uint8_t      data[4];    /* Byte array value */
        lu_float32_t    aValue;     /* Analogue value   */
        lu_int32_t      iValue;     /* Integer value    */
        lu_uint32_t     dValue;     /* Digital value    */
    } value;
} HatPinIOData;


/**
 * \brief Serial port configuration request
 */
typedef struct HatSerialCfg
{
    lu_uint32_t portID;         /*! Uses HAT_SERIAL_PORT_ID */
    lu_uint32_t baudrate;       /*! Uses HAT_SERIAL_BAUDRATE */
    lu_uint8_t dataBits;        /*! Uses HAT_SERIAL_DATA_BITS */
    lu_uint8_t stopBits;        /*! Uses HAT_SERIAL_STOP_BITS */
    lu_uint8_t parity;          /*! Uses HAT_SERIAL_PARITY */
    lu_uint8_t portMode;        /*! Uses HAT_SERIAL_MODE */
} HatSerialCfg;

/**
 * \brief Serial port data sending request
 */
typedef struct HatSerialSend
{
    lu_uint32_t     portID;                     /*! Uses HAT_SERIAL_PORT_ID */
    lu_uint32_t     timeoutMs;                  /* Timeout after sending */
    lu_uint32_t     sendSize;                   /* Size (Bytes) of the message to send */
    lu_uint8_t      sendBuf[HAT_SERIAL_MAX_SEND];  /* data to send */
} HatSerialSend;

/**
 * \brief Serial port data receiving request
 */
typedef struct HatSerialRcvRq
{
    lu_uint32_t     portID;                     /*! Uses HAT_SERIAL_PORT_ID */
    lu_uint32_t     timeoutMs;                  /* Time window for receiving data */
} HatSerialRcvRq;

/**
 * \brief Serial port data receiving reply
 */
typedef struct HatSerialRcv
{
    lu_uint32_t     portID;                     /*! Uses HAT_SERIAL_PORT_ID */
    lu_uint32_t     status;                     /*! Uses HAT_ERR */
    lu_uint32_t     rcvSize;                    /* Size (Bytes) of the message received */
    lu_uint8_t      rcvBuf[HAT_SERIAL_MAX_RCV];   /* data received */
} HatSerialRcv;


/**
 * \brief Generic operation result reply.
 */
typedef struct HatResult
{
    lu_uint32_t     status;     /*! Uses HAT_ERR */
} HatResult;

#pragma pack(pop)


/*
 ******************************************************************************
 * EXPORTED - Typedefs
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

#ifdef __cplusplus
}
#endif


#endif /* _HAT_PROTOCOLINCLUDED */

/*
 *********************** End of file ******************************************
 */
