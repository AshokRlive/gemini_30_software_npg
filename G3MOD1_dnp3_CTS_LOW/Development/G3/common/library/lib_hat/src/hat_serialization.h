/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:hat_serialization.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Oct 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef SRC_HAT_SERIALIZATION_H_
#define SRC_HAT_SERIALIZATION_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "hat_protocol.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/**
 * \brief Packs a native message to raw packets that can be sent over network.
 *
 * \param source HAT protocol message source to pack.
 * \param destBuf Buffer to contain packed message. Must have size >= HAT_MSG_HEADER_SIZE + source.payloadSize
 *
 * \return Size of the packed data.
 */
lu_uint32_t hat_pack(const HatMessage* source, lu_uint8_t destBuf[HAT_MSG_HEADER_SIZE]);

/**
 * \brief Unpacks raw packets received from network to a native message.
 *
 * \param destBuf Buffer that contains packed message.
 * \param size Size of the packed message.
 * \param result Where to extract the message.
 *
 * \return Error code.
 */
HAT_ERR hat_unpack(const lu_uint8_t* buf, const lu_uint32_t size, HatMessage* result);



#endif /* SRC_HAT_SERIALIZATION_H_ */

/*
 *********************** End of file ******************************************
 */
