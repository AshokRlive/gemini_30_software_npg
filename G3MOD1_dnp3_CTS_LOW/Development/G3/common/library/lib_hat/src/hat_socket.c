/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:hat_socket.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Oct 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <errno.h>
#include <stddef.h>

#ifndef WIN32
#include <unistd.h>     //Needed for POSIX's close()
#else
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x0501
#include <winsock2.h>  // getaddrinfo, freeaddrinfo
#include <ws2tcpip.h>
#endif

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "hat_utils.h"
#include "hat_socket.h"
#include "hat_dbg.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define DEBUG_SOCKET_DATA 0

#define SOCK_RW_BUF_SIZE 1024
#define BACKLOG 10  // how many pending connections queue will hold

#ifdef WIN32
#define MSG_NOSIGNAL 0
#endif

#define TITLE "socket: "

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void compose_error_reply(lu_uint8_t *replyPtr, lu_int32_t* replySize);
static void build_select_list(hatsocket_server_context_t* ctxt);
static void read_socks(hatsocket_server_context_t* ctxt);
static void handle_new_connection(hatsocket_server_context_t* ctxt);
static void deal_with_data(hatsocket_server_context_t* ctxt, const lu_int32_t listnum);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

#ifdef WIN32
 // Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
#endif

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
#ifdef WIN32
void hatsocket_initWinsock()
{
    WSADATA wsaData;
    lu_int32_t iResult = 0;
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0)
    {
       printf("WSAStartup failed with error: %d\n", iResult);
    }
}
#endif

void hatsocket_set_nonblock(lu_int32_t sockfd)
{
#ifdef WIN32
    lu_int32_t NonBlock = 1;

    if (ioctlsocket(sockfd, FIONBIO, &NonBlock) == SOCKET_ERROR)
    {
        printf("ioctlsocket() failed with error %d\n", WSAGetLastError());
    }
    else
    {
        printf("ioctlsocket() is OK!\n");
    }
#else
    lu_int32_t flags;

    /* Set socket to non-blocking */

    if ((flags = fcntl(sockfd, F_GETFL, 0)) < 0)
    {
        DBG_ERR("Failed to set nonblock");
    }


    if (fcntl(sockfd, F_SETFL, flags | O_NONBLOCK) < 0)
    {
        DBG_ERR("Failed to set nonblock");
    }
#endif
}


lu_int32_t hatsocket_make_client_socket(lu_uint8_t* serverIPAddr, lu_int32_t port)
{
    struct addrinfo hints;
    struct addrinfo* res = NULL;
    lu_char_t portStr[20];
    lu_int32_t sockfd = -1;

    // converts to port string.
    sprintf(portStr,"%i",port);

    // Load up address structs with getaddrinfo()
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    if(getaddrinfo((lu_char_t*)serverIPAddr, portStr, &hints, &res) != 0)
    {
        perror("failed to get address info");
    }
    else
    {
        // Make a socket
        DBG_INFO("opening sys socket...");
        sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if (sockfd < 0)
        {
            perror("failed to create socket");
        }
        else
        {
            // Connect socket!
            DBG_INFO("connecting sys socket...");
            if (connect(sockfd, res->ai_addr, res->ai_addrlen) < 0)
            {
                perror("failed to connect");
                hatsocket_close(sockfd);
                sockfd = -1;
            }
        }
    }

    DBG_INFO("make_client_socket() %s", (sockfd < 0)? "failed!" : "completed!");

    freeaddrinfo(res);

    return sockfd;
}


void hatsocket_init_context(hatsocket_server_context_t* ctxt, lu_int32_t listenSock, hatsocket_handler handler)
{
    lu_uint32_t i;
    /* Since we start with only one socket, the listening socket,
           it is the highest socket so far. */
    ctxt->highsock = listenSock;
    ctxt->listenSock = listenSock;
    ctxt->handler = handler;
    //Init all connections on the list to an invalid socket value
    for (i = 0; i < MAX_CONNECTION_NUM; ++i)
    {
        ctxt->connectlist[i] = -1;
    }
}


lu_int32_t hatsocket_make_server_socket(lu_int32_t port)
{
    struct addrinfo hints;
    struct addrinfo* servinfo;
    lu_int32_t rv;
    lu_int32_t listenSock = -1;
    lu_char_t portStr[20];
    lu_int32_t yes = 1;

    sprintf(portStr,"%i",port);// converts to port string.

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;// AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE; // use my IP

    if ((rv = getaddrinfo(NULL, portStr, &hints, &servinfo)) != 0)
    {
        DBG_ERR(TITLE"getaddrinfo: %s", gai_strerror(rv));
    }
    else
    {
        // Create socket
        listenSock = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
        if(listenSock < 0)
        {
            perror("failed to create socket");
        }
        else
        {
            // Set socket reusable
            if (setsockopt(listenSock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(lu_int32_t)) == -1)
            {
                hatsocket_close(listenSock);
                listenSock = -1;
                perror("failed to setsockopt");
            }
            else
            {
                // Set socket to non-blocking
                hatsocket_set_nonblock(listenSock);

                // Bind socket
                if(bind(listenSock, servinfo->ai_addr, servinfo->ai_addrlen) != 0)
                {
                    hatsocket_close(listenSock);
                    listenSock = -1;
                    perror("failed to bind");
                }
            }
        }
        freeaddrinfo(servinfo); // all done with this structure

        // listen to socket
        if(listenSock >= 0)
        {
            if(listen(listenSock, BACKLOG) != 0)
            {
                hatsocket_close(listenSock);
                listenSock = -1;
                perror("failed to listen");
            }
        }
    }

    return listenSock;
}


lu_int32_t hatsocket_recv_data(lu_int32_t socket, lu_uint8_t* recvBuffer, lu_int32_t bufSize)
{
    lu_int32_t result = 0;
    DBG_INFO(TITLE"receiving data from sock:%d ",socket);

    do
    {
        /* Receive a reply from the server*/
        result = recv(socket, (char*)recvBuffer, bufSize, 0);
    }
    while( (result == -1) && (errno == EINTR) );


#if DEBUG_SOCKET_DATA
    if (result > 0)
    {
        dump_data("received hat message", recvBuffer, result);
    }
    else
    {
        DBG_ERR("receive error");
    }
#endif

    return result;
}


lu_int32_t hatsocket_send_data(lu_int32_t socket, lu_uint8_t* sendBuffer, lu_int32_t sendSize)
{

    lu_int32_t result;      //Partial result
    lu_int32_t sent = 0;    //Final result of the operation

    DBG_INFO(TITLE"sending data");

    do
    {
        result = send(socket, sendBuffer + sent, sendSize - sent, MSG_NOSIGNAL);
        if (result > 0)
        {
            sent += result;
        }
        else if( (result < 0) && (errno != EINTR) )
        {
            break;// failure
        }
    }
    while (sendSize > sent);

#if DEBUG_SOCKET_DATA
    if(sendSize == sent)
    {
        dump_data("sent hat message", sendBuffer, sendSize);
    }
#endif

    return sent;
}


void hatsocket_select(hatsocket_server_context_t* ctxt)
{
    lu_int32_t result;
	lu_uint16_t listnum;

//    struct timeval timeout;  /* Timeout for select */
//    timeout.tv_sec = 3;
//    timeout.tv_usec = 0;


    build_select_list(ctxt);

    result = select(ctxt->highsock+1, &(ctxt->socks), (fd_set *) 0, (fd_set *) 0, /*&timeout*/ NULL);

    if (result == 0)
    {
        /* Select timeout*/
        DBG_INFO(TITLE"select() timed out!");
    }
    else if( (result < 0) && (errno != EINTR) )
    {
        /* Select error*/
        DBG_INFO(TITLE"error in select(): %s", strerror(errno));

        /* Close related sockets*/
        for (listnum = 0; listnum < MAX_CONNECTION_NUM; listnum++)
        {
            if(ctxt->connectlist[listnum] >= 0)
            {
                if (FD_ISSET(ctxt->connectlist[listnum], &(ctxt->socks)))
                {
                    DBG_INFO(TITLE"closed socket:%d", ctxt->connectlist[listnum]);
                    hatsocket_close(ctxt->connectlist[listnum]);
                    ctxt->connectlist[listnum] = -1;
                }
            }
        }

    }
    else if (result > 0)
    {
        DBG_INFO(TITLE"selected socket. num:%d", result);
        read_socks(ctxt);
    }
}


lu_int32_t hatsocket_close(lu_int32_t sock)
{
#ifdef WIN32
    return closesocket(sock);
#else
    return close(sock);
#endif
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
static void compose_error_reply(lu_uint8_t *replyPtr, lu_int32_t* replySize)
{
    const lu_char_t* err = "internal error occurred";
    
    DBG_TRACK(TITLE"compose_error_reply() called");
    strcpy((lu_char_t*)replyPtr, err);
    *replySize = strlen(err);
}


static void build_select_list(hatsocket_server_context_t* ctxt)
{
    lu_int32_t listnum;         /* Current item in connectlist for for loops */

    FD_ZERO(&(ctxt->socks));
    FD_SET(ctxt->listenSock, &(ctxt->socks));

    /* Loops through all the possible connections and adds
        those sockets to the fd_set */
    for (listnum = 0; listnum < MAX_CONNECTION_NUM; listnum++)
    {
        if (ctxt->connectlist[listnum] >= 0)
        {
            //Add socket to the connection list
            FD_SET(ctxt->connectlist[listnum], &(ctxt->socks));
            if (ctxt->connectlist[listnum] > ctxt->highsock)
            {
                ctxt->highsock = ctxt->connectlist[listnum];
            }
        }
    }
}


static void handle_new_connection(hatsocket_server_context_t* ctxt)
{
    lu_int32_t listnum;        /* Current item in connectlist for for loops */
    lu_int32_t connection;     /* Socket file descriptor for incoming connections */
    const lu_char_t* errTxt = "Sorry, this server is too busy. Try again later!\r\n";

    /* We have a new connection coming in!  We'll try to find a spot for it in connectlist. */
    connection = accept(ctxt->listenSock, NULL, NULL);
    if (connection < 0)
    {
        perror("failed to accept");
        return;
    }

    hatsocket_set_nonblock(connection);

    /* Add new connection to the queue. */
    for (listnum = 0; (listnum < MAX_CONNECTION_NUM) && (connection != -1); listnum++)
    {
        if (ctxt->connectlist[listnum] < 0)
        {
            DBG_INFO(TITLE"new connection accepted:   FD=%d; Slot=%d", connection, listnum);
            ctxt->connectlist[listnum] = connection;
            connection = -1;    //Break loop
        }
    }

    /* Failed to add new connections to queue. */
    if (connection > 0)
    {
        //A connection was accepted but not added to the queue
        DBG_ERR(TITLE"no room left for new connection");
        hatsocket_send_data(connection, (lu_uint8_t*)errTxt, strlen(errTxt) );
        hatsocket_close(connection);
    }
}


static void deal_with_data(hatsocket_server_context_t* ctxt, const lu_int32_t listnum)
{
    static lu_uint8_t readBuf [SOCK_RW_BUF_SIZE];    /* Buffer for socket reads */
    static lu_uint8_t writeBuf[SOCK_RW_BUF_SIZE];    /* Buffer for socket reads */
    lu_int32_t writeSize = 0;
    lu_int32_t readSize = 0;

    readSize = hatsocket_recv_data(ctxt->connectlist[listnum],readBuf,SOCK_RW_BUF_SIZE);

    //DBG_INFO("deal_with_data() listnum:%d",listnum);

    if (readSize < 0)
    {
        /* Connection closed, close this end and free up entry in connectlist */
        DBG_ERR(TITLE"connection lost: FD=%d;  Slot=%d", ctxt->connectlist[listnum],listnum);
        hatsocket_close(ctxt->connectlist[listnum]);
        ctxt->connectlist[listnum] = -1;
    }
    else if(readSize > 0)
    {
        DBG_INFO(TITLE" received data size: %d. start processing request...",readSize);
        if ((*(ctxt->handler))(readBuf, readSize, writeBuf, &writeSize) != 0)
        {
            compose_error_reply(writeBuf, &writeSize);
        }

        DBG_INFO(TITLE"finished processing request, sending reply now.");
        hatsocket_send_data(ctxt->connectlist[listnum], writeBuf, writeSize);
    }
    else
    {
        DBG_WARN(TITLE"peer is shutdown. closing connection index:%d.",listnum);
        hatsocket_close(ctxt->connectlist[listnum]);
        ctxt->connectlist[listnum] = -1;
    }
}

static void read_socks(hatsocket_server_context_t* ctxt)
{
    lu_int32_t listnum;         /* Current item in connectlist for for loops */

    /* Deal with listenSock */
    if(ctxt->listenSock >= 0)
    {
        if (FD_ISSET(ctxt->listenSock, &(ctxt->socks)))
        {
            handle_new_connection(ctxt);
        }
    }
    /* Deal with connection sockets */
    for (listnum = 0; listnum < MAX_CONNECTION_NUM; listnum++)
    {
        if(ctxt->connectlist[listnum] >= 0)
        {
            if (FD_ISSET(ctxt->connectlist[listnum], &(ctxt->socks)))
            {
                deal_with_data(ctxt, listnum);
            }
        }
    }
}

/*
 *********************** End of file ******************************************
 */
