/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:hat_serialization.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Oct 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <assert.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "hat_serialization.h"
#include "hat_utils.h"
#include "hat_dbg.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
/*! Filter table structure */
typedef struct HatValidationEntry
{
    HAT_MSG_ID  msgId  ;
    lu_uint32_t payloadSize;
}HatPayloadSize;


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/**
 * \brief Payload size of each HAT protocol message
 */
static const HatPayloadSize payloadSizeTable[] =
{
    /* messageType                        payloadSize */
    {  HAT_MSG_ID_NACK                  , 0                           },
    {  HAT_MSG_ID_HATVERSION_C          , 0                           },
    {  HAT_MSG_ID_HATVERSION_R          , sizeof(HatProtocolVersion)  },
    {  HAT_MSG_ID_VERSION_C             , 0                           },
    {  HAT_MSG_ID_VERSION_R             , sizeof(HatVersion)          },

    {  HAT_MSG_ID_CAN_PROXY_C           , sizeof(HatCANSend)          },
    {  HAT_MSG_ID_CAN_PROXY_R           , sizeof(HatCANReceive)       },

    {  HAT_MSG_ID_MCM_IO_READ_C         , sizeof(HatPinIOReadReq)     },
    {  HAT_MSG_ID_MCM_IO_READ_R         , sizeof(HatPinIOData   )     },
    {  HAT_MSG_ID_MCM_IO_WRITE_C        , sizeof(HatPinIOData   )     },
    {  HAT_MSG_ID_MCM_IO_WRITE_R        , sizeof(HatResult      )     },
    {  HAT_MSG_ID_MCM_IO_READ_DI_C      , sizeof(HatPinIOReadReq)     },
    {  HAT_MSG_ID_MCM_IO_READ_DI_R      , sizeof(HatPinIOData   )     },
    {  HAT_MSG_ID_MCM_IO_WRITE_DO_C     , sizeof(HatPinIOData   )     },
    {  HAT_MSG_ID_MCM_IO_WRITE_DO_R     , sizeof(HatResult      )     },
    {  HAT_MSG_ID_MCM_IO_READ_AI_C      , sizeof(HatPinIOReadReq)     },
    {  HAT_MSG_ID_MCM_IO_READ_AI_R      , sizeof(HatPinIOData   )     },

    {  HAT_MSG_ID_MCM_SERIAL_CONF_C     , sizeof(HatSerialCfg)        },
    {  HAT_MSG_ID_MCM_SERIAL_CONF_R     , sizeof(HatResult)           },
    {  HAT_MSG_ID_MCM_SERIAL_READ_C     , sizeof(HatSerialRcvRq)      },
    {  HAT_MSG_ID_MCM_SERIAL_READ_R     , sizeof(HatSerialRcv)        },
    {  HAT_MSG_ID_MCM_SERIAL_WRITE_R    , sizeof(HatSerialSend)       },
    {  HAT_MSG_ID_MCM_SERIAL_WRITE_C    , sizeof(HatResult)           },
    {  HAT_MSG_ID_MCM_SERIAL_READWRITE_C, sizeof(HatSerialSend)       },
    {  HAT_MSG_ID_MCM_SERIAL_READWRITE_R, sizeof(HatSerialRcv)        }
};
static const size_t payloadSizeTable_size = LU_ARRAY_LENGTH(payloadSizeTable);

/* Check (partial) validity of the payload size table with static assertion */
#define PAYLOADSIZETABLE_SIZE LU_ARRAY_LENGTH(payloadSizeTable)
LU_STATIC_ASSERT( (PAYLOADSIZETABLE_SIZE == HAT_MSG_ID_LAST), HAT_PAYLOAD_SIZE_MISMATCH);

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_uint32_t hat_pack(const HatMessage* source, lu_uint8_t* destBuf)
{
    DBG_TRACK("packing...");

    /* Ensure payloadSizeTable has been properly updated */
    assert(payloadSizeTable_size == HAT_MSG_ID_LAST);

    memcpy(destBuf, &(source->header), HAT_MSG_HEADER_SIZE);
    memcpy(destBuf+HAT_MSG_HEADER_SIZE, source->payload, source->payloadSize);

    return HAT_MSG_HEADER_SIZE + source->payloadSize;
}


HAT_ERR hat_unpack(const lu_uint8_t *buf, const lu_uint32_t size, HatMessage* result)
{
    lu_int32_t payloadSize;

	DBG_TRACK("unpacking ...");

    /* Ensure payloadSizeTable has been properly updated */
    assert(payloadSizeTable_size == HAT_MSG_ID_LAST);


    if ( (size < HAT_MSG_HEADER_SIZE) || (size > HAT_MSG_MAX_SIZE) )
    {
        return HAT_ERR_INVALID_MSG_SIZE;
    }

    payloadSize = size - HAT_MSG_HEADER_SIZE;
    memcpy(&(result->header), buf, HAT_MSG_HEADER_SIZE);
    memcpy(result->payload, buf + HAT_MSG_HEADER_SIZE, payloadSize);
    result->payloadSize = payloadSize;

    // validate id
    if (result->header.id >= HAT_MSG_ID_LAST)
    {
        return HAT_ERR_UNEXPECTED_REPLY;
    }

    // validate payload size
    if(payloadSizeTable[result->header.id].payloadSize != result->payloadSize)
    {
        return HAT_ERR_INVALID_PAYLOAD_SIZE;
    }
    return HAT_ERR_NONE;
}



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
