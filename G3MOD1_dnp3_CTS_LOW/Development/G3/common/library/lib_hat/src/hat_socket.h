/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:hat_socket.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Oct 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef SRC_HAT_SOCKET_H_
#define SRC_HAT_SOCKET_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <netdb.h>
#include <fcntl.h>
#include <sys/select.h>
#endif

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */



/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define MAX_CONNECTION_NUM  10

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef lu_int32_t (*hatsocket_handler)(
                                    const lu_uint8_t* requestPtr,
                                    const lu_int32_t requestSize,
                                    lu_uint8_t *replyPtr,
                                    lu_int32_t* replySize);


typedef struct hatsocket_server_context_t
{
    /* The socket file descriptor for our "listening" socket*/
    lu_int32_t listenSock;

    /* Array of connected sockets so we know who we are talking to*/
    lu_int32_t connectlist[MAX_CONNECTION_NUM];

    /* Socket file descriptors we want to wake up for, using select()*/
    fd_set socks;

    /* Highest file descriptor, needed for select()*/
    lu_int32_t highsock;

    /* For handling request from client*/
    hatsocket_handler handler;

} hatsocket_server_context_t;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
#ifdef WIN32
void hatsocket_initWinsock();
#endif

/* TODO: pueyos_a - rename to hat_* */

lu_int32_t hatsocket_make_server_socket(lu_int32_t port);

lu_int32_t hatsocket_make_client_socket(lu_uint8_t* serverIPAddr, lu_int32_t port);

lu_int32_t hatsocket_close(lu_int32_t sock);

void hatsocket_init_context(hatsocket_server_context_t* ctxt, lu_int32_t listenSock, hatsocket_handler handler);

void hatsocket_select(hatsocket_server_context_t* ctxt);

lu_int32_t hatsocket_send_data(lu_int32_t socket, lu_uint8_t* sendBuffer, lu_int32_t sendSize);

lu_int32_t hatsocket_recv_data(lu_int32_t socket, lu_uint8_t* recvBuffer, lu_int32_t bufSize);

void hatsocket_set_nonblock(lu_int32_t sockfd);

#endif /* SRC_HAT_SOCKET_H_ */

/*
 *********************** End of file ******************************************
 */
