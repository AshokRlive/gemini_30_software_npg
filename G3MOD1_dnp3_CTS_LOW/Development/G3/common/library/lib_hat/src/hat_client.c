/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:hat_client.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Sep 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <assert.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "hat_client.h"
#include "hat_socket.h"
#include "hat_utils.h"
#include "hat_serialization.h"
#include "hat_dbg.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/**
 * \brief Initialise a request/reply pair of messages
 *
 * \param request HAT request message to initialise
 * \param reply HAT reply message to initialise
 * \param id Message ID for the request message
 */
static void init_msg(HatMessage* request, HatMessage* reply, const HAT_MSG_ID id);

/**
 * \brief Send a generic HAT message to the HAT socket
 *
 * \param sockfd HAT socket file descriptor
 * \param requestMsg HAT request message to send
 * \param replyMsg Where to put the returning HAT reply message
 *
 * \return Error code
 */
static HAT_ERR send_msg(lu_int32_t sockfd, const HatMessage* requestMsg, HatMessage* replyMsg);

/**
 * \brief Create and initialise a HAT socket
 *
 * \return New HAT Socket file descriptor
 */
static lu_int32_t make_socket();

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static lu_uint8_t* m_rtuIpAddress = NULL;   /* IP address of the RTU */
static lu_int32_t m_port = 0;               /* HAT client port */
static lu_int32_t m_sockfd = -1;            /* Socket descriptor */

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
HAT_ERR hat_client_init(lu_uint8_t* rtuIpAddress, lu_int32_t port)
{
    #ifdef WIN32
    	hatsocket_initWinsock();
    #endif

    m_rtuIpAddress = rtuIpAddress;
    m_port = port;
    m_sockfd = make_socket();

    if(m_sockfd < 0)
    {
        return HAT_ERR_INIT;
    }
    return HAT_ERR_NONE;
}

HAT_ERR hat_client_close()
{
    if(m_sockfd < 0)
    {
        return HAT_ERR_INIT;
    }
    hatsocket_close(m_sockfd);
    m_sockfd = -1;

    return HAT_ERR_NONE;
}



HAT_ERR hat_send_msg(const HatMessage* request, HatMessage* reply)
{
    HAT_ERR err;

    if(m_sockfd < 0)
    {
        return HAT_ERR_SOCKET_INIT;
    }
    err = send_msg(m_sockfd, request, reply);
    return err;
}


HAT_ERR hat_get_protocol_version(HatProtocolVersion* receive)
{
    HatMessage request;
    HatMessage reply;
    HAT_ERR    err;

    init_msg(&request, &reply, HAT_MSG_ID_HATVERSION_C);

    err = hat_send_msg(&request, &reply);
    if(err == HAT_ERR_NONE)
    {
        assert (sizeof(*receive) == reply.payloadSize);
        memcpy(receive, reply.payload, reply.payloadSize);
    }
    return err;
}


HAT_ERR hat_get_version(HatVersion* receive)
{
    HatMessage request;
    HatMessage reply;
    HAT_ERR    err;

    init_msg(&request, &reply, HAT_MSG_ID_VERSION_C);

    err = hat_send_msg(&request, &reply);
    if(err == HAT_ERR_NONE)
    {
        assert (sizeof(*receive) == reply.payloadSize);
        memcpy(receive, reply.payload, reply.payloadSize);
    }
    return err;
}


HAT_ERR hat_send_can(const HatCANSend* send, HatCANReceive* receive)
{
    HatMessage request;
    HatMessage reply;
    HAT_ERR err = HAT_ERR_NONE;

    init_msg(&request, &reply, HAT_MSG_ID_CAN_PROXY_C);

    memcpy(request.payload, send, sizeof(*send));
    request.payloadSize = sizeof(*send);

    err = hat_send_msg(&request, &reply);
    if(err == HAT_ERR_NONE)
    {
        assert (sizeof(*receive) == reply.payloadSize);
        memcpy(receive, reply.payload,  reply.payloadSize);
    }
    return err;
}


HAT_ERR hat_send_can2( lu_int16_t canBus,
                       lu_int32_t timeoutMs,
                       lu_uint8_t* sendBuf,
                       lu_int16_t sendSize,
                       lu_uint8_t* rcvBuf,
                       lu_int16_t* rcvSize
                     )
{
    HatCANSend send;
    HatCANReceive receive;
    HAT_ERR err = HAT_ERR_NONE;

    memset(&send, 0, sizeof(send));
    memset(&receive, 0, sizeof(receive));

    send.canBus = canBus;
    send.sendSize = MIN(sendSize,HAT_CAN_MAX_SIZE);
    send.timeoutMs = timeoutMs;
    memcpy(send.sendBuf, sendBuf, send.sendSize);

    err = hat_send_can(&send, &receive);
    if (err != HAT_ERR_NONE)
    {
        return err;
    }

    if(rcvBuf != NULL)
    {
        /* Note that CAN messages never exceed HAT_CAN_MAX_SIZE */
        memcpy(&rcvBuf[0], receive.rcvBuf, receive.rcvSize);
    }

    if(rcvSize != NULL)
    {
        *rcvSize = receive.rcvSize;
    }
    return err;
}


HAT_ERR hat_serial_cfg( const lu_uint32_t portID,
                        const lu_uint32_t baudrate,
                        const lu_uint8_t dataBits ,
                        const lu_uint8_t stopBits ,
                        const lu_uint8_t parity   ,
                        const lu_uint8_t portMode)
{
    HAT_ERR err = HAT_ERR_NONE;
    HatMessage request;
    HatMessage reply;
    HatSerialCfg cfg;
    HatResult* resultPtr;

    memset(&cfg, 0, sizeof(cfg));
    cfg.portID = portID;
    cfg.baudrate = baudrate;
    cfg.dataBits = dataBits;
    cfg.stopBits = stopBits;
    cfg.parity   = parity  ;
    cfg.portMode = portMode;

    init_msg(&request, &reply, HAT_MSG_ID_MCM_SERIAL_CONF_C);

    memcpy(request.payload, &cfg, sizeof(cfg));
    request.payloadSize = sizeof(cfg);

    err = hat_send_msg(&request, &reply);
    if(err == HAT_ERR_NONE)
    {
        assert (sizeof(*resultPtr) == reply.payloadSize);
        resultPtr = (HatResult*)(reply.payload);
        err = resultPtr->status;
    }
    return err;
}


HAT_ERR hat_serial_send(const lu_uint32_t portID,
                        lu_uint8_t* const sendBuf,
                        const lu_uint8_t sendSize)
{
    HAT_ERR err = HAT_ERR_NONE;
    HatMessage request;
    HatMessage reply;
    HatSerialSend sSend;
    HatResult* resultPtr;

    memset(&sSend, 0, sizeof(sSend));
    sSend.portID = portID;
    sSend.timeoutMs = 0;    /* Unused for send-only */
    sSend.sendSize = LU_MIN(sendSize, HAT_SERIAL_MAX_SEND);
    memcpy(sSend.sendBuf, sendBuf, sSend.sendSize);

    init_msg(&request, &reply, HAT_MSG_ID_MCM_SERIAL_WRITE_C);
    memcpy(request.payload, &sSend, sizeof(sSend));
    request.payloadSize = sizeof(sSend);

    err = hat_send_msg(&request, &reply);
    if(err == HAT_ERR_NONE)
    {
        assert (sizeof(*resultPtr) == reply.payloadSize);
        resultPtr = (HatResult*)(reply.payload);
        err = resultPtr->status;
    }
    return err;
}


HAT_ERR hat_serial_rcv(const lu_uint32_t portID,
                        const lu_uint32_t timeoutMs,
                        lu_uint8_t* const rcvBuf,
                        lu_uint32_t* const rcvSize)
{
    HAT_ERR err = HAT_ERR_NONE;
    lu_uint32_t maxSize;
    HatMessage request;
    HatMessage reply;
    HatSerialRcvRq serialReq;
    HatSerialRcv* resultPtr;

    if( (rcvBuf == NULL) || (rcvSize == NULL) )
    {
        return HAT_ERR_BADPARAMS;
    }

    memset(&serialReq, 0, sizeof(serialReq));
    serialReq.portID = portID;
    serialReq.timeoutMs = timeoutMs;

    init_msg(&request, &reply, HAT_MSG_ID_MCM_SERIAL_READ_C);
    memcpy(request.payload, &serialReq, sizeof(serialReq));
    request.payloadSize = sizeof(serialReq);

    err = hat_send_msg(&request, &reply);
    if(err == HAT_ERR_NONE)
    {
        assert (sizeof(*resultPtr) == reply.payloadSize);
        resultPtr = (HatSerialRcv*)(reply.payload);
        err = (HAT_ERR)(resultPtr->status);
        maxSize = LU_MIN(*rcvSize, resultPtr->rcvSize);
        if(err == HAT_ERR_NONE)
        {
            memcpy(rcvBuf, resultPtr->rcvBuf, maxSize);
            *rcvSize = maxSize;
        }
    }
    return err;
}


HAT_ERR hat_serial_sendRcv(const lu_uint32_t portID,
                            const lu_uint32_t timeoutMs,
                            lu_uint8_t* const sendBuf,
                            const lu_uint8_t sendSize,
                            lu_uint8_t* const rcvBuf,
                            lu_uint32_t* const rcvSize)
{
    HAT_ERR err = HAT_ERR_NONE;
    lu_uint32_t maxSize;
    HatMessage request;
    HatMessage reply;
    HatSerialSend sSend;
    HatSerialRcv* resultPtr;

    if( (rcvBuf == NULL) || (rcvSize == NULL) )
    {
        return HAT_ERR_BADPARAMS;
    }

    memset(&sSend, 0, sizeof(sSend));
    sSend.portID = portID;
    sSend.timeoutMs = timeoutMs;
    sSend.sendSize = LU_MIN(sendSize, HAT_SERIAL_MAX_SEND);
    memcpy(sSend.sendBuf, sendBuf, sSend.sendSize);

    init_msg(&request, &reply, HAT_MSG_ID_MCM_SERIAL_READWRITE_C);
    memcpy(request.payload, &sSend, sizeof(sSend));
    request.payloadSize = sizeof(sSend);

    err = hat_send_msg(&request, &reply);
    if(err == HAT_ERR_NONE)
    {
        assert (sizeof(*resultPtr) == reply.payloadSize);
        resultPtr = (HatSerialRcv*)(reply.payload);
        err = (HAT_ERR)(resultPtr->status);
        maxSize = LU_MIN(*rcvSize, resultPtr->rcvSize);
        if(err == HAT_ERR_NONE)
        {
            memcpy(rcvBuf, resultPtr->rcvBuf, maxSize);
            *rcvSize = maxSize;
        }
    }
    return err;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
static void init_msg(HatMessage* request, HatMessage* reply, const HAT_MSG_ID id)
{
    memset(request, 0, sizeof(*request));
    memset(reply,   0, sizeof(*reply));

    request->header.id = id;
}


static HAT_ERR send_msg(lu_int32_t sockfd, const HatMessage* requestMsg, HatMessage* replyMsg)
{
    HAT_ERR err = HAT_ERR_NONE;
    /* NOTE: the message buffer (buf & size) are static in order to prevent
     *       frecuent allocations and deallocations in the stack since this
     *       function might be called frequently.
     *       The buffer is cleared and reused every time.
     */
    static lu_uint8_t buf[HAT_MSG_MAX_SIZE];    /* Message buffer */
    static lu_int32_t size;                     /* Size of the message buffer */

    DBG_INFO("sending HAT message. id:%d, payload:%d",
                    requestMsg->header.id,
                    requestMsg->payloadSize);

    /* Serialise */
    size = hat_pack(requestMsg, buf);

    /* Send */
    if(hatsocket_send_data(sockfd, buf, size) != size)
    {
        return HAT_ERR_SOCKET_WRITE_ERR;
    }

    /* Receive */
    size = hatsocket_recv_data(sockfd, buf, HAT_MSG_MAX_SIZE);

    if (size < 0)
    {
        return HAT_ERR_SOCKET_READ_ERR;
    }
    if (size == 0)
    {
        return HAT_ERR_SOCKET_CLOSED;
    }
    /* De-serialise */
    err = hat_unpack(buf, size, replyMsg);
    if(err != HAT_ERR_NONE)
    {
        DBG_ERR("failed to unpack, err:%d", err);
        return err;
    }

    /* Check reply msg id matched with request */
    if( replyMsg->header.id != (requestMsg->header.id + 1) )
    {
        err = HAT_ERR_UNEXPECTED_REPLY;
    }

    return err;
}


static lu_int32_t make_socket()
{
    lu_int32_t sockfd = hatsocket_make_client_socket(m_rtuIpAddress, m_port);
	DBG_INFO("opening client socket:%d", sockfd);
    return sockfd;
}


/*
 *********************** End of file ******************************************
 */
