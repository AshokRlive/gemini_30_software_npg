/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:hat_client.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Sep 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <netdb.h>
#endif

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "hat_server.h"
#include "hat_utils.h"
#include "hat_socket.h"
#include "hat_dbg.h"
#include "hat_serialization.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/* Implementation of call back function: hatsocket_handler */
static lu_int32_t socket_handler_impl(
                                    const lu_uint8_t* requestPtr,
                                    const lu_int32_t requestSize,
                                    lu_uint8_t *replyPtr,
                                    lu_int32_t* replySize);

static void print_info(lu_int32_t sock);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static lu_int32_t  m_srvSock =  -1;
static hat_handler m_handler = NULL;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

HAT_ERR hat_server_close(void)
{
#ifdef WIN_32
    WSACleanup();
#endif

    HAT_ERR result = HAT_ERR_SOCKET_CLOSED;
    // Close previous sock
    if (m_srvSock >= 0)
    {
        if(hatsocket_close(m_srvSock) == 0)
        {
            result = HAT_ERR_NONE;
        }
        m_srvSock = -1; //Invalidate sock after closing it
    }
    return result;
}


HAT_ERR hat_server_init(lu_int32_t port, hat_handler handler)
{
#ifdef WIN32
    hatsocket_initWinsock();
#endif

    m_handler = handler;
    m_srvSock = hatsocket_make_server_socket(port);

    if (m_srvSock < 0)
    {
        DBG_INFO("HAT server NOT initialised. port:%d", port);
    }
    else
    {
        print_info(m_srvSock);
    }

    return (m_srvSock >= 0) ? HAT_ERR_NONE : HAT_ERR_INIT;
}


HAT_ERR hat_server_run()
{
    lu_uint32_t counter = 0;	//Debug: Number of requests waited
	hatsocket_server_context_t ctxt;

	if (m_srvSock < 0)
    {
        DBG_ERR("invalid server socket:%i. server may not be initialised!", m_srvSock);
        return HAT_ERR_INIT;
    }
    
    hatsocket_init_context(&ctxt, m_srvSock, &socket_handler_impl);

    do
    {
        DBG_INFO("\n\n[%u]Waiting for request....",counter++);
        hatsocket_select(&ctxt);
    }
    while (1);

    return HAT_ERR_NONE;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
static void print_info(lu_int32_t sock)
{
	struct sockaddr_in sin;
	socklen_t len = sizeof(sin);

	if (getsockname(sock, (struct sockaddr *)&sin, &len) == -1)
	{
		perror("getsockname");
		DBG_ERR("failed to get sockname for sock:%d",sock);
	}
	else
	{
		DBG_INFO("sock number:%d port number %d", sock, ntohs(sin.sin_port));
	}

}


static lu_int32_t socket_handler_impl(
                                    const lu_uint8_t* requestPtr,
                                    const lu_int32_t requestSize,
                                    lu_uint8_t *replyPtr,
                                    lu_int32_t* replySize)
{
    static HatMessage replyMsg  ;
    static HatMessage requestMsg;
	lu_bool_t success = LU_FALSE;

	DBG_TRACK("socket_handler_impl() called");

    /* Init memory*/
    memset(&replyMsg,      0,   sizeof(replyMsg));
    memset(&requestMsg,    0,   sizeof(requestMsg));

    if(hat_unpack(requestPtr,requestSize, &requestMsg) == HAT_ERR_NONE)
    {
        if((*m_handler)(&requestMsg, &replyMsg) == HAT_ERR_NONE)
        {
            success = LU_TRUE;
        }
        else
        {
            DBG_ERR("failed to handle request!");
        }
    }
    else
    {
        DBG_ERR("failed to unpack request!");
    }

    if(success == LU_FALSE)
    {
        replyMsg.header.id = HAT_MSG_ID_NACK;
    }

    *replySize = hat_pack(&replyMsg, replyPtr);

    return (success == LU_TRUE)? 0 : -1;
}



/*
 *********************** End of file ******************************************
 */
