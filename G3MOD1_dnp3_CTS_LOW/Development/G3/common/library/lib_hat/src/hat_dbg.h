/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:hat_dbg.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13 Oct 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef SRC_HAT_DBG_H_
#define SRC_HAT_DBG_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

/* Specify subsystem name before including dbg.h*/
#define SUBSYSTEM_NAME "HAT"
#include "../../../../RTU/MCM/common/BaseReuse/include/dbg.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/* Redefine to get rid of compilation warnings */
#ifdef DBG_ERR
#undef DBG_ERR
#define DBG_ERR(M, ...) fprintf(stderr, "[INFO  ]%s%s" M "(%s)\n", SUBSYSTEM_NAME, DBG_SEPARATOR, ##__VA_ARGS__, __FILE__)
#endif

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */



#endif /* SRC_HAT_DBG_H_ */

/*
 *********************** End of file ******************************************
 */
