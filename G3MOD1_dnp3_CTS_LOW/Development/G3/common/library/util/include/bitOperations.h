/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: bitOperations.h 18 Feb 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/common/library/util/inc/bitOperations.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Generic Bit operation macros.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 18 Feb 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef BITOPERATIONS_H__INCLUDED
#define BITOPERATIONS_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Gets a bit value inside a bit field
 *
 *   This macro gets the value of a concrete bit inside a bit field.
 *   Example to get the 4th bit of x and store the result (0 or 1) in y:
 *     y = LU_GETBIT(x, 3);
 *
 *   \param bitField Bit field value to read from.
 *   \param bitShift Position of the bit in the bit field (from 0)
 *
 *   \return Resulting bit value (0 or 1).
 ******************************************************************************
 */
#define LU_GETBIT(bitField, bitShift)  (((bitField) & (0x01 << (bitShift)))? 1 : 0)

/*
#define LU_TESTBIT(bitField, bitShift, bitValue) \
    ((bitField) ^ ((-(bitValue) ^ (bitField)) & (1<<(bitShift))))
*/

/*!
  ******************************************************************************
  *   \brief Set/clear a bit value in a bit field
  *
  *   This macro sets/clears a concrete bit value inside a bit field.
  *   This algorithm was based in:
  *   http://www.graphics.stanford.edu/~seander/bithacks.html#ConditionalSetOrClearBitsWithoutBranching
  *     Example to set the 4th bit of x:
  *         LU_SETBITVALUE(x, 3, 1);
  *
  *   \param bitField Bit field value to alter: it will be modified
  *   \param bitShift Position of the bit in the bit field (from 0)
  *   \param bitValue Bit value (value 0 to clear bit)
  ******************************************************************************
  */
#define LU_SETBITVALUE(bitField, bitShift, bitValue) \
    (bitField) ^= ( (-(bitValue) ^ (bitField)) & (1<<(bitShift)) )

/*!
  ******************************************************************************
  *   \brief Set a bit value in a bit field
  *
  * This macro sets bit values inside a bit field, according to the given mask.
  *     Example to set the 4th bit of x and store the result in y:
  *         LU_SETBIT(x, 0x08);
  *
  *   \param bitField Bit field value to alter: it will be modified
  *   \param bitMask Mask of the affected bits in the bit field
  ******************************************************************************
  */
#define LU_SETBIT(bitField, bitMask)       ((bitField) |= (bitMask))

/*!
  ******************************************************************************
  *   \brief Clear a bit value in a bit field
  *
  * This macro clears bit values inside a bit field, according to the given mask.
  *     Example to clear the 2nd and 3rd bits of x:
  *         LU_CLEARBIT(x, 0x06);
  *
  *   \param bitField Bit field value to alter: it will be modified
  *   \param bitMask Mask of the affected bits in the bit field
  ******************************************************************************
  */
#define LU_CLEARBIT(bitField, bitMask)     ((bitField) &= (~(bitMask)))

/*!
  ******************************************************************************
  *   \brief Set a bit value in a bit field
  *
  *   This macro sets/clears a single bit value inside a bit field.
  *     Example to set the 4th bit of x:
  *         LU_SETBIT_BS(x, 3);
  *
  *   \param bitField Bit field value to alter: it will be modified
  *   \param bitShift Position of the bit in the bit field (from 0)
  ******************************************************************************
  */
#define LU_SETBIT_BS(bitField, bitShift)       ((bitField) |= (1<<(bitShift)))

/*!
  ******************************************************************************
  *   \brief Clear a bit value in a bit field
  *
  *   This macro clears a single bit value inside a bit field.
  *     Example to clear the 5th bit of x:
  *         LU_CLEARBIT_BS(x, 4);
  *
  *   \param bitField Bit field value to alter: it will be modified
  *   \param bitShift Position of the bit in the bit field (from 0)
  ******************************************************************************
  */
#define LU_CLEARBIT_BS(bitField, bitShift)     ((bitField) &= (~(1<<(bitShift))))

/*!
 ******************************************************************************
 *   \brief Set/clear bits values in a byte using a given mask
 *
 *   This macro sets/clears bit values inside a bit field by using a given mask.
 *   If the given value is 1, all the bits set in the mask are set in the bit
 *   field; if it is 0, all the bits set in the mask are cleared in the bit field.
 *   This algorithm was based in:
 *   http://www.graphics.stanford.edu/~seander/bithacks.html#ConditionalSetOrClearBitsWithoutBranching
 *      Example to set 1st and 8th bit of x:
 *          LU_SETALLMASKBITS(x, 0x81, 1);
 *
 *   \param bitField Bit field value to alter: it will be modified
 *   \param bitMask Mask of the affected bits in the bit field
 *   \param bitValue Bit value (value 0 to clear all the affected bits)
 ******************************************************************************
 */
#define LU_SETALLMASKBITS(bitField, bitMask, bitValue) \
    ((bitField) ^= ((-(bitValue) ^ (bitField)) & (bitMask)))

/*!
 ******************************************************************************
 *   \brief Set a value in a group of bits inside a bit field using a mask
 *
 *   This macro sets the value specified by all the bits set in a mask inside
 *   a bit field to the given value.
 *   This algorithm was based in:
 *   http://graphics.stanford.edu/~seander/bithacks.html#MaskedMerge
 *      Example to set 3rd to 6th bits of x to a 4-bit value given by z:
 *          LU_SETBITMASKVALUE(x, 0x3C, z, 2);
 *
 *   \param bitField Bit field value to alter: it will be modified
 *   \param bitMask Mask of the affected bit field
 *   \param bitValue New value for the group of bits
 *   \param bitShift Position (from 0) of the group of bits (no shift by default)
 ******************************************************************************
 */
#define LU_SETBITMASKVALUE(bitField, bitMask, bitValue, bitShift) \
    ((bitField) ^= (((bitField) ^ ((bitValue)<<(bitShift))) & (bitMask)))

/*!
 ******************************************************************************
 *   \brief Get a value in a group of bits inside a bit field using a mask
 *
 *   This macro gets the value contained in all the bits set in a mask inside
 *   a bit field.
 *      Example to get 3rd to 6th bits of x as a 4-bit value and store the
 *      result in y:
 *          y = LU_GETBITMASKVALUE(x, 0x3C, 2);
 *
 *   \param bitField Bit field value to read from.
 *   \param bitMask Mask of the affected bit field
 *   \param bitShift Position (from 0) of the group of bits (no shift by default)
 *
 *   \return Resulting bit field value.
 ******************************************************************************
 */
#define LU_GETBITMASKVALUE(bitField, bitMask, bitShift) \
    (((bitField) & (bitMask)) >> (bitShift))

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

#endif /* BITOPERATIONS_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
