/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: timeOperations.h 29 Jul 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/library/util/include/timeOperations.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Library for time-related common operations
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 29 Jul 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29/07/11      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef TIMEOPERATIONS_H__INCLUDED
#define TIMEOPERATIONS_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <sys/time.h>   //timeval definition
#include <time.h>       //timespec definition
#include <errno.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/**
 * \brief Handy default values
 */
static const struct timeval TIMEVAL_ZERO = {0,0};
static const struct timespec TIMESPEC_ZERO = {0,0};

/**
 * \brief conversion functions between SI prefixes
 */
#define SEC_TO_MSEC(sec)    ((sec) * 1000L)
#define SEC_TO_USEC(sec)    ((sec) * 1000000L)
#define SEC_TO_NSEC(sec)    ((sec) * 1000000000L)
#define MSEC_TO_USEC(msec)  ((msec) * 1000L)
#define MSEC_TO_NSEC(msec)  ((msec) * 1000000L)
#define MSEC_TO_SEC(msec)   ((msec) / 1000L)
#define USEC_TO_NSEC(usec)  ((usec) * 1000L)
#define USEC_TO_MSEC(usec)  ((usec) / 1000L)
#define USEC_TO_SEC(usec)   ((usec) / 1000000L)
#define NSEC_TO_MSEC(nsec)  ((nsec) / 1000000L)
#define NSEC_TO_USEC(nsec)  ((nsec) / 1000L)
#define NSEC_TO_SEC(nsec)   ((nsec) / 1000000000L)

/**
 * \brief Macro for converting a timeval struct to a timespec
 *
 * \param tv Pointer to timeval structure to convert
 * \param ts Pointer to timespec struct where to store the converted structure
 */
#ifndef TIMEVAL_TO_TIMESPEC
#define TIMEVAL_TO_TIMESPEC(ptv, pts)                                   \
        do                                                              \
        {                                                               \
            (pts)->tv_sec = (ptv)->tv_sec;                              \
            (pts)->tv_nsec = (ptv)->tv_usec * 1000;                     \
        } while(0)
#endif
/**
 * \brief Macro for converting a timespec struct to a timeval
 *
 * Note that the order of the parameters is the same as the one provided by the
 * GNU implementation in GNU's sys/time.h
 *
 * \param tv Pointer to timeval struct where to store the converted structure
 * \param ts Pointer to timespec structure to convert
 */
#ifndef TIMESPEC_TO_TIMEVAL
#define TIMESPEC_TO_TIMEVAL(ptv, pts)                                   \
        do                                                              \
        {                                                               \
            (ptv)->tv_sec = (pts)->tv_sec;                              \
            (ptv)->tv_usec = (pts)->tv_nsec / 1000;                     \
        } while(0)
#endif

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/**
 * \brief Compare two Timeval structures
 *
 * \param strTime1 Pointer to time structure
 * \param strTime2 Pointer to time structure
 *
 * \return LU_TRUE if both structures are equal
 */
static inline lu_bool_t timeval_compare(const struct timeval* strTime1,
                                        const struct timeval* strTime2
                                        )
{
    if( (strTime1 != NULL) && (strTime2 != NULL) )
    {
        return ( (strTime1->tv_sec == strTime2->tv_sec) &&
                (strTime1->tv_usec == strTime2->tv_usec) )?
                                LU_TRUE : LU_FALSE;
    }
    return LU_FALSE;
}


/**
 * \brief Compare two timespec structures
 *
 * \param strTime1 Pointer to time structure
 * \param strTime2 Pointer to time structure
 *
 * \return LU_TRUE if both structures are equal
 */
static inline lu_bool_t timespec_compare(const struct timespec* strTime1,
                                         const struct timespec* strTime2
                                        )
{
    if( (strTime1 != NULL) && (strTime2 != NULL) )
    {
        return ( (strTime1->tv_sec == strTime2->tv_sec) &&
                (strTime1->tv_nsec == strTime2->tv_nsec) )?
                                LU_TRUE : LU_FALSE;
    }
    return LU_FALSE;
}


/**
 * \brief Normalise a Timeval structure
 *
 * Checks a given timeval structure and re-adjust it if needed.
 * Values in tv_usec that are over 1 Million of magnitude are converted to a
 * smaller amount, updating the seconds accordingly. The tv_usec field will
 * only store microseconds and not seconds.
 * BE CAREFUL: for negative timevals, it is done by setting negative usec,
 * as -0.5 secs being {0,-500000}
 * This complies with the rule of: Total_usec = (tv_sec * 1000000) + tv_usec
 *
 * \param strTime Pointer to structure to normalise (content will be altered)
 */
static inline void timeval_normalise(struct timeval* strTime)
{
    if(strTime != NULL)
    {
        strTime->tv_sec += (strTime->tv_usec / 1000000L);
        strTime->tv_usec = (strTime->tv_usec % 1000000L);
    }
}


/**
 * \brief Normalise a Timespec structure
 *
 * Checks a given timespec structure and re-adjust it if needed.
 * Values in tv_nsec that are over 1 Billion of magnitude are converted to a
 * smaller amount, updating the seconds accordingly. The tv_nsec field will
 * only store nanoseconds and not seconds.
 * BE CAREFUL: for negative timevals, it is done by setting negative nsec,
 * as -0.5 secs being {0,-500000000}
 * timespec->nsec is only a LONG, and therefore will overflow with values above
 * the value 2147483647!!!
 * This complies with the rule of: Total_nsec = (tv_sec * 1000000000) + tv_nsec
 *
 * \param strTime Pointer to structure to normalise (content will be altered)
 */
//#define GET_NS strTime->tv_nsec / 1000000000L
static inline void timespec_normalise(struct timespec* strTime)
{
    if(strTime != NULL)
    {
        strTime->tv_sec += (strTime->tv_nsec / 1000000000L);
        strTime->tv_nsec = (strTime->tv_nsec % 1000000000L);
    }
}


/**
 * \brief Gets a timeval time stamp for use in elapsed time calculations
 *
 * Note: It also gives easier way to substitute CLOCK_MONOTONIC by
 * CLOCK_MONOTONIC_RAW, when available.
 *
 * \param timestamp Where to store the current time stamp.
 *
 * \return POSIX return code (0 for success, -1 for error).
 */
static inline lu_int32_t clock_gettimeval(struct timeval* timestamp)
{
    struct timespec time_ns;
    lu_int32_t ret;

    if(timestamp == NULL)
    {
        return -1;
    }
#ifdef CLOCK_MONOTONIC_RAW
    ret = clock_gettime(CLOCK_MONOTONIC_RAW, &time_ns); //get present time stamp
#else
    ret = clock_gettime(CLOCK_MONOTONIC, &time_ns); //get present time stamp
#endif
    TIMESPEC_TO_TIMEVAL(timestamp, &time_ns);       //return it over the parameter
    return ret;
}


/**
 * \brief Gets a timespec time stamp for use in elapsed time calculations
 *
 * Note: Not essential, but gives easier way to substitute CLOCK_MONOTONIC by
 * CLOCK_MONOTONIC_RAW, when available.
 *
 * \param timestamp Where to store the current time stamp.
 *
 * \return POSIX return code (0 for success, -1 for error).
 */
static inline lu_int32_t clock_gettimespec(struct timespec* timestamp)
{
    lu_int32_t ret;

    if(timestamp == NULL)
    {
        return -1;
    }
#ifdef CLOCK_MONOTONIC_RAW
    ret = clock_gettime(CLOCK_MONOTONIC_RAW, timestamp); //get present time stamp
#else
    ret = clock_gettime(CLOCK_MONOTONIC, timestamp); //get present time stamp
#endif
    return ret;
}


/**
 * \brief Add two timeval time stamps
 *
 * \param baseTime Pointer to time structure
 * \param sumTime Pointer to time structure
 * \param result Pointer to where to store the result
 */
static inline void timeval_add( const struct timeval* baseTime,
                                const struct timeval* sumTime,
                                struct timeval* result
                                )
{
    if( (baseTime != NULL) && (sumTime != NULL) && (result != NULL) )
    {
        result->tv_sec = (sumTime->tv_sec + baseTime->tv_sec);
        result->tv_usec = (sumTime->tv_usec + baseTime->tv_usec);
        timeval_normalise(result);  //offer the result normalised
    }
}


/**
 * \brief Add two timespec time stamps
 *
 * \param baseTime Pointer to oldest time structure
 * \param sumTime Pointer to newest time structure
 * \param result Pointer to where to store the result
 */
static inline void timespec_add(const struct timespec* baseTime,
                                    const struct timespec* sumTime,
                                    struct timespec* result
                                    )
{
    if( (baseTime != NULL) && (sumTime != NULL) && (result != NULL) )
    {
        result->tv_sec = (sumTime->tv_sec + baseTime->tv_sec);
        result->tv_nsec = (sumTime->tv_nsec + baseTime->tv_nsec);
        timespec_normalise(result);  //offer the result normalised
    }
}


/**
 * \brief Calculate elapsed time between timeval time stamps
 *
 * \param startTime Pointer to oldest time structure
 * \param finishTime Pointer to newest time structure
 * \param result Pointer to where to store the result
 */
static inline void timeval_elapsed( const struct timeval* startTime,
                                    const struct timeval* finishTime,
                                    struct timeval* result
                                    )
{
    if( (startTime != NULL) && (finishTime != NULL) && (result != NULL) )
    {
        result->tv_sec = (finishTime->tv_sec - startTime->tv_sec);
        result->tv_usec = (finishTime->tv_usec - startTime->tv_usec);
        timeval_normalise(result);  //offer the result normalised
    }
}


/**
 * \brief Calculate elapsed time between timespec time stamps
 *
 * \param startTime Pointer to oldest time structure
 * \param finishTime Pointer to newest time structure
 * \param result Pointer to where to store the result
 */
static inline void timespec_elapsed(const struct timespec* startTime,
                                    const struct timespec* finishTime,
                                    struct timespec* result
                                    )
{
    if( (startTime != NULL) && (finishTime != NULL) && (result != NULL) )
    {
        result->tv_sec = (finishTime->tv_sec - startTime->tv_sec);
        result->tv_nsec = (finishTime->tv_nsec - startTime->tv_nsec);
        timespec_normalise(result);  //offer the result normalised
    }
}


/**
 * \brief Convert a millisecond value to timeval
 *
 * From a given elapsed time in milliseconds, convert it to a timeval structure.
 * Elapsed time is considered positive.
 *
 * \param time_ms Elapsed time to convert, expressed in milliseconds.
 *
 * \return Time structure.
 */
static inline struct timeval ms_to_timeval(lu_uint64_t time_ms)
{
    struct timeval result;
    result.tv_sec = (time_ms / 1000);   //ms to sec
    result.tv_usec = MSEC_TO_USEC(time_ms - SEC_TO_MSEC(result.tv_sec)); //rest of ms to usec
    return result;
}


/**
 * \brief Convert a millisecond value to timespec
 *
 * From a given elapsed time in milliseconds, convert it to a timespec structure.
 * Elapsed time is considered positive.
 *
 * \param time_ms Elapsed time to convert, expressed in milliseconds.
 *
 * \return Time structure.
 */
static inline struct timespec ms_to_timespec(lu_uint64_t time_ms)
{
    struct timespec result;
    result.tv_sec = (time_ms / 1000);   //ms to sec
    result.tv_nsec = MSEC_TO_NSEC(time_ms - SEC_TO_MSEC(result.tv_sec)); //rest of ms to nsec
    return result;
}


/**
 * \brief Convert a microsecond value to timespec
 *
 * From a given elapsed time in microseconds, convert it to a timespec structure.
 * Elapsed time is considered positive.
 *
 * \param time_ms Elapsed time to convert, expressed in microseconds.
 *
 * \return Time structure.
 */
static inline struct timespec us_to_timespec(lu_uint64_t delayUs)
{
    struct timespec delay = { 0, 0 };
    lu_uint64_t     delayNs = 0;

    // Convert the milliseconds to a timespec
    delayNs = USEC_TO_NSEC(delayUs);

    // Normalise here, timespec_normalise() will overflow on anything
    // above 2147483647 nsecs!
    delay.tv_sec += (delayNs / 1000000000L);
    delay.tv_nsec = (delayNs % 1000000000L);

    return delay;
}


/**
 * \brief Convert a timeval structure to milliseconds
 *
 * From a given elapsed time in a timeval structure, convert it to an elapsed
 * time, expressed in milliseconds.
 *
 * \param time_us Time structure to convert.
 *
 * \return Elapsed Time, expressed in milliseconds.
 */
static inline lu_int64_t timeval_to_ms(const struct timeval* time_us)
{
    return ( (SEC_TO_USEC((lu_int64_t)(time_us->tv_sec)) + (lu_int64_t)(time_us->tv_usec)) / 1000 );
}


/**
 * \brief Convert a timespec structure to milliseconds
 *
 * From a given elapsed time in a timespec structure, convert it to an elapsed
 * time, expressed in milliseconds.
 *
 * \warning Time is TRUNCATED to the ms, NOT rounded to the nearest ms.
 *
 * \param time_ns Time structure to convert.
 *
 * \return Elapsed Time, expressed in milliseconds.
 */
static inline lu_int64_t timespec_to_ms(const struct timespec* time_ns)
{
    return ((lu_int64_t)(SEC_TO_MSEC((lu_int64_t)(time_ns->tv_sec))) +
            (lu_int64_t)(NSEC_TO_MSEC((lu_int64_t)(time_ns->tv_nsec)))
            );
}


/**
 * \brief Calculate elapsed (positive) time between timeval time stamps in msec
 *
 * Please note that this function always returns a positive value, even if the
 * finish time is earlier than the start time. microsecond precision is dropped;
 * that means if a difference is 42.9 ms, it returns 42 since a complete ms is
 * not reached.
 *
 * \param startTime Pointer to oldest time structure
 * \param finishTime Pointer to newest time structure
 *
 * \return Time difference in milliseconds, or 0 when negative difference.
 */
static inline lu_uint64_t timeval_elapsed_ms( const struct timeval* startTime,
                                              const struct timeval* finishTime
                                              )
{
    if( (startTime != NULL) && (finishTime != NULL) )
    {
        struct timeval result;
        result.tv_sec = (finishTime->tv_sec - startTime->tv_sec);
        result.tv_usec = (finishTime->tv_usec - startTime->tv_usec);
        lu_int64_t ret = timeval_to_ms(&result);
        return (ret < 0)? 0 : ret;
    }
    return 0;
}


/**
 * \brief Calculate elapsed time between timespec time stamps in msec
 *
 * Please note that this function always returns a positive value, even if the
 * finish time is earlier than the start time. nanosecond precision is dropped;
 * that means if a difference is 42.9 ms, it returns 42 since a complete ms is
 * not reached.
 *
 * \param startTime Pointer to oldest time structure
 * \param finishTime Pointer to newest time structure
 *
 * \return Time difference in milliseconds, or 0 when negative difference
 */
static inline lu_uint64_t timespec_elapsed_ms(const struct timespec* startTime,
                                              const struct timespec* finishTime
                                              )
{
    if( (startTime != NULL) && (finishTime != NULL) )
    {
        struct timespec result;
        result.tv_sec = (finishTime->tv_sec - startTime->tv_sec);
        result.tv_nsec = (finishTime->tv_nsec - startTime->tv_nsec);
        timespec_normalise(&result);
        lu_int64_t ret = timespec_to_ms(&result);
        return (ret < 0)? 0 : ret;
    }
    return 0;
}


/**
 * \brief Add milliseconds to a timespec time stamp.
 *
 * \param startTime Pointer to time structure to be updated.
 * \param time_ms Amount of milliseconds to be added to the time structure.
 */
static inline void timespec_add_ms(struct timespec* startTime,
                                    const lu_uint64_t time_ms
                                    )
{
    if (startTime != NULL)
    {
        startTime->tv_sec += (lu_uint32_t)(time_ms / 1000);
        startTime->tv_nsec += MSEC_TO_NSEC((time_ms % 1000));
        timespec_normalise(startTime);
    }
}


/**
 * \brief Subtract milliseconds from a timespec time stamp.
 *
 * \param startTime Pointer to time structure to be updated.
 * \param time_ms Amount of milliseconds to be subtracted from the time structure.
 */
static inline void timespec_sub_ms(struct timespec* startTime,
                                    const lu_uint64_t time_ms
                                    )
{
    if (startTime != NULL)
    {
        timespec_normalise(startTime);  //Ensure nsecs < 1 sec
        time_t secs = (time_t)MSEC_TO_SEC(time_ms);
        time_t nsecs = (time_t)(MSEC_TO_NSEC(time_ms) - SEC_TO_NSEC(secs));
        startTime->tv_nsec -= nsecs;
        if(startTime->tv_nsec < 0)
        {
            secs += 1;
            startTime->tv_nsec = SEC_TO_NSEC(1) - nsecs;
        }
        startTime->tv_sec -= secs;
        if (startTime->tv_sec < 0)
        {
            //Negative time: sets the time to 0
            startTime->tv_sec = 0;
            startTime->tv_nsec = 0;
        }
    }
}


/**
 * \brief Compare two timespec time stamps to check if it has timed out.
 *
 * \param timeRef Reference time structure (older).
 * \param timeCheck Time to be compared.
 *
 * \return LU_TRUE when timeCheck > timeRef
 */
static inline lu_bool_t checkTimeout(const struct timespec* timeCheck, const struct timespec* timeRef)
{
    if( (timeCheck == NULL) || (timeRef == NULL) )
    {
        return LU_FALSE;
    }
    struct timespec result;
    timespec_elapsed(timeRef, timeCheck, &result);
    if(result.tv_sec < 0)
        return LU_FALSE;
    if(result.tv_sec > 0)
        return LU_TRUE;
    return (result.tv_nsec >= 0)? LU_TRUE : LU_FALSE;
}

/**
 * \brief Non interrupting Nano Sleep
 *
 *  This function will not be interrupted upon receipt of
 *  a handled signal.  If an unhandled signal is received it will
 *  exit.
 *
 * \param delay timespec containing the desired delay
 *
 * \return 0 on successfully waiting for the desired delay, else -1
 */
static inline lu_int32_t lucy_nanosleep(struct timespec *delayPtr)
{
    lu_int32_t ret;

    while((ret = nanosleep(delayPtr, delayPtr)) != 0)
    {
        if (errno != EINTR)
        {
            break;
        }
    }

    return ret;
}

/**
 * \brief Non interrupting usleep
 *
 *  This function will not be interrupted upon receipt of
 *  a handled signal.  If an unhandled signal is received it will
 *  exit.
 *
 * \param delayUs Number of microseconds to sleep
 *
 * \return 0 on successfully waiting for the desired delay, else -1
 */
static inline lu_int32_t lucy_usleep(lu_uint64_t delayUs)
{
    struct timespec delay = { 0, 0 };

    // Convert to timespec
    delay = us_to_timespec(delayUs);

    return lucy_nanosleep(&delay);
}

#endif /* TIMEOPERATIONS_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
