/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief Slave Software Binary Image Header global definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/06/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _SLAVE_BIN_IMAGE_DEF_INCLUDED
#define _SLAVE_BIN_IMAGE_DEF_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif




/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "ModuleProtocol.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define SLAVE_IMAGE_MAGIC_SYNC				0x4ac0de32L

#define SLAVE_IMAGE_VERSION					0x00000001L

#define SLAVE_IMAGE_HEAD_SIZE       		0x100L

#define SLAVE_IMAGE_BASE_OFFSET				0xccL

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*! SLAVE Processor architecture / type */
typedef enum
{
	SLAVE_BIN_ARCH_ARM_NXP_LPC1765         = 0x00,
	SLAVE_BIN_ARCH_ARM_NXP_LPC1768         = 0x01
}SLAVE_BIN_ARCH;

/*! SLAVE Image type */
//typedef enum
//{
//	SLAVE_IMAGE_TYPE_BOOT_LOADER           = 0x00,
//	SLAVE_IMAGE_TYPE_APPLICATION           = 0x01,
//	SLAVE_IMAGE_TYPE_APP_BOOT_PROGRAM      = 0x02
//}SLAVE_IMAGE_TYPE;

#pragma pack(1)

/*! Fixed size slave binary image header - 0x100 bytes */
typedef struct SlaveBinImageHeaderDef
{
	lu_uint32_t					headerCRC32;  /* CRC32 of this structure only */

	lu_uint32_t					magicSyncCode; /* SLAVE_IMAGE_MAGIC_SYNC */

	lu_uint32_t					preHeaderCRC32; /* CRC32 of data before this header */
	lu_uint32_t					postHeaderCRC32; /* CRC of the binary image after this header */

	lu_uint16_t					headerVersion;
	lu_uint16_t       			moduleType;

	lu_uint32_t					imageSize; /* Total size of the entire binary image */

	lu_uint32_t                 appImageAddress; /* Absolute Start of Application Image */
	lu_uint16_t				    processorArch;   /* SLAVE_BIN_ARCH */
	lu_uint16_t                 imageType;       /* SLAVE_IMAGE_TYPE */

	ModuleVersionStr 			featureRevision;

	ModuleVersionStr 			systemAPI;

	ModuleSoftwareVersionStr 	softwareVersion;

	lu_uint32_t					svnRevsion; /* SVN workspace revision */

	lu_uint8_t                  Spare[194];
} SlaveBinImageHeaderStr;

#pragma pack()

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern char __rom_start;
extern char __rom_size;

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

#pragma pack()


#ifdef __cplusplus
}
#endif

#endif /* _SLAVE_BIN_IMAGE_DEF_INCLUDED */

/*
 *********************** End of file ******************************************
 */
