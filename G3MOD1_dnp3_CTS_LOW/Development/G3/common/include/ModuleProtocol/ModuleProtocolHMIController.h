/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/02/12      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULEPROTOCOLHMICONTROLLER_INCLUDED
#define _MODULEPROTOCOLHMICONTROLLER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*!
 * HMI Button bitmask enum
 */
typedef enum
{
	HMI_BUTTON_BIT_MASK_SOFT_LEFT_1		= 0x0001,
	HMI_BUTTON_BIT_MASK_SOFT_LEFT_2 	= 0x0002,
	HMI_BUTTON_BIT_MASK_SOFT_RIGHT_1	= 0x0004,
	HMI_BUTTON_BIT_MASK_SOFT_RIGHT_2	= 0x0008,
	HMI_BUTTON_BIT_MASK_PB_NAV_UP		= 0x0010,
	HMI_BUTTON_BIT_MASK_PB_NAV_DOWN		= 0x0020,
	HMI_BUTTON_BIT_MASK_PB_NAV_LEFT		= 0x0040,
	HMI_BUTTON_BIT_MASK_PB_NAV_RIGHT	= 0x0080,
	HMI_BUTTON_BIT_MASK_ID_PB_OK 		= 0x0100,
	HMI_BUTTON_BIT_MASK_PB_MENU 		= 0x0200,
	HMI_BUTTON_BIT_MASK_PB_ESC 			= 0x0400,
	HMI_BUTTON_BIT_MASK_PB_SW_ACTIVATE 	= 0x0800,

	HMI_BUTTON_BIT_MASK_SW_OPEN 		= 0x2000,
	HMI_BUTTON_BIT_MASK_PB_SW_CLOSE 	= 0x4000
}HMI_BUTTON_BIT_MASK;

/*!
 * HMI LED bitmask enum
 */
typedef enum
{
	HMI_LED_BIT_MASK_SOFT_LEFT_1		= 0x0001,
	HMI_LED_BIT_MASK_SOFT_LEFT_2 		= 0x0002,
	HMI_LED_BIT_MASK_SOFT_RIGHT_1		= 0x0004,
	HMI_LED_BIT_MASK_SOFT_RIGHT_2 		= 0x0008
}HMI_LED_BIT_MASK;

/*!
 * HMI Configure
 */
typedef struct HMIConfigDef
{
	lu_bool_t 	buttonClickEnable;	/* Enable click sound when button pressed */
	lu_bool_t 	reqOLREnable;		/* Enable control of OLR from the HMI */
	lu_uint16_t timeModeToggleLRMs; /* Time required on Mode button to toggle LR state */
	lu_uint16_t timeModeOffMs;		/* Time required on Mode button until off requested */
}HMIConfigStr;

/*!
 * OLR state enum
 */
typedef enum
{
	HMI_OLR_STATE_OFF		= 0x00,
	HMI_OLR_STATE_LOCAL 	= 0x01,
	HMI_OLR_STATE_REMOTE 	= 0x02,
	HMI_OLR_STATE_INVALID 	= 0x03
}HMI_OLR_STATE;

/*!
 * HMI Request LOR change event
 */
typedef struct HmiReqOLRChangeEventDef
{
	lu_int8_t	requestedHMIOLRState; /* Set to one of HMI_OLR_STATE enum */
}HmiReqOLRChangeEventStr;

typedef struct HmiButtonStateDef
{
	/*! Button state bitmask */
	lu_int32_t      buttonBitMask; /* bits set using HMI_BUTTON_BIT_MASK */
}HmiButtonStateStr;

/*!
 * HMI Button event
 */
typedef struct HmiButtonEventDef
{
    /*! Button state */
	HmiButtonStateStr     state;
}HmiButtonEventStr;



/*!
 * HMI Display OLR state
 */
typedef struct HmiDisplayOLRDef
{
	lu_int8_t	displayHMIOLRState;  /* Set to one of HMI_OLR_STATE enum */
}HmiDisplayOLRStr;

/*!
 * HMI Play Sound
 */
typedef struct HmiPlaySoundDef
{
	lu_int16_t	frequencyHz; 	/* Frequency of sound in HZ */
	lu_int16_t	durationMs;     /* Duration of sound in Ms */
}HmiPlaySoundStr;

/*!
 * HMI Display information
 */
typedef struct HmiDisplayInfoReplyDef
{
	lu_int8_t	numRows; 	/* Number of lines for the display */
	lu_int8_t	numColums;  /* Duration of sound in Ms */
}HmiDisplayInfoReplyStr;


/*!
 * HMI Button state
 */
typedef struct HmiButtonStateReplyDef
{
    /*! Button state */
	HmiButtonStateStr     state;
}HmiButtonStateReplyStr;


/*!
 * HMI LED Write
 */
typedef struct HmiLEDWriteDef
{
    /*! LED state */
	lu_uint32_t     ledBitMask;  /* Bitmask set using HMI_LED_BIT_MASK */
}HmiLEDWriteStr;

/*!
 * HMI Write Display
 */
typedef struct HmiDisplayWriteDef
{
    /*! Display write */
	lu_uint8_t     posRow;
	lu_uint8_t     posColumn;
	lu_uint8_t	   bufferLen;  /* Length of additional payload */
}HmiDisplayWriteStr;

/*!
 * HMI Write Display
 */
typedef struct HmiDisplayClearDef
{
	lu_uint8_t     posRow;
	lu_uint8_t     posColumn;
	lu_uint8_t	   numChars;  /* Number of chars to clear (0 = whole display) */
}HmiDisplayClearStr;

/*!
 * HMI Display Sync
 */
typedef struct HmiDisplaySyncDef
{
	lu_bool_t     forceFullUpdate; /* Force a physical re-write screen update */
}HmiDisplaySyncStr;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



#endif /* _MODULEPROTOCOLHMICONTROLLER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
