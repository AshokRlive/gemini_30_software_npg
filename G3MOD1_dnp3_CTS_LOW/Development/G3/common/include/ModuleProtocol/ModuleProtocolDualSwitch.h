/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Protocol Switch Controller library header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/06/14      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULEPROTOCOLDUALSWITCH_INCLUDED
#define _MODULEPROTOCOLDUALSWITCH_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef struct DualSwitchConfigDef
{
	/* Channel Number */
	lu_uint8_t		channel;

	/* Enabled indicator */
	lu_uint8_t		enable : 1;

	lu_uint8_t      unused : 7;

	/* Length of operating pulse in ms */
	lu_uint16_t 	pulseLengthMS;

	/* Length of overrun time in ms */
	lu_uint16_t 	overrunMS;

	/* Length of operation timeout in seconds */
	lu_uint16_t		opTimeoutS;

	/* drive motor past limit switch in ms */
	lu_uint16_t 	motorOverDriveMS;

	/* drive motor in reverse in ms */
	lu_uint16_t 	motorReverseDriveMS;

	/* Bit mask of inhibit inputs by channel number */
	lu_uint32_t 	inhibitBI;

}DualSwitchConfigStr;

typedef struct DualSwitchOperateDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Delayed operation duration in seconds */
	lu_uint8_t		SwitchDelay;

	/*! Pre-operation delay duration in milliseconds */
	lu_uint16_t		SwitchPreOpDelay;

	/*! Local or remote operation indicator */
	lu_bool_t		local;
}DualSwitchOperateStr;

typedef struct DualSwitchSelectDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Time out for operate in seconds */
	lu_uint8_t		SelectTimeout;

	/*! Local or remote operation indicator */
	lu_bool_t		local;
} DualSwitchSelectStr;

typedef struct DualSwitchCancelDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Local or remote operation indicator */
	lu_bool_t		local;
} DualSwitchCancelStr;

typedef struct DualSwitchSynchDef
{
    /*! Channel ID */
    lu_uint8_t      channel;
} DualSwitchSynchStr;


typedef enum
{
	DUAL_SWITCH_POLARITY_UP,
	DUAL_SWITCH_POLARITY_DOWN,
	DUAL_SWITCH_POLARITY_LINK,

	DUAL_SWITCH_POLARITY_DIRECT_DRIVE,

	DUAL_SWITCH_POLARITY_LAST
} DUAL_SWITCH_POLARITY;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */


#endif /* _MODULEPROTOCOLDUALSWITCH_INCLUDED */

/*
 *********************** End of file ******************************************
 */
