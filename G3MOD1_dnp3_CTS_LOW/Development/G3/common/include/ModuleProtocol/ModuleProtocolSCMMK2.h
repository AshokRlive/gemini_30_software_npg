/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULEPROTOCOLSCMMK2_INCLUDED
#define _MODULEPROTOCOLSCMMK2_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleProtocolEnumSCMMK2.h"

#include "ModuleProtocolAuxSwitch.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

LU_DISABLE_WARNING(-pedantic)

typedef struct SCMMK2ConfigDef
{
	/*! LED Polarity - 1 = Green for open ; 0 = Red for open */
	lu_uint8_t  	greenLEDPolarity : 1;

	/*! For Allowing forced operation */
	lu_uint8_t		doNotCheckSwitchPosition  : 1;

	/*! Hardware controlled motor shut off */
	lu_uint8_t		hardwareMotorLimitEnable  : 1;

	/*! Software controlled actuator mode */
	lu_uint8_t		actuatorLimitSwitchEnable : 1;

	lu_uint8_t      unused : 4;
} SCMMK2ConfigStr;

LU_ENABLE_WARNING(-pedantic)

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



#endif /* _MODULEPROTOCOLSCMMK2_INCLUDED */

/*
 *********************** End of file ******************************************
 */
