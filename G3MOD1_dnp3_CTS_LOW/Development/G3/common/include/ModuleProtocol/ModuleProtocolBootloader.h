/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18/07/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _C_MODULEPROTOCOLBOOTLOADER_INCLUDED
#define _C_MODULEPROTOCOLBOOTLOADER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


typedef struct BootloaderFlashParamsReplyDef
{
	lu_uint32_t  appFlashBaseAddress;
	lu_uint16_t  maxAppFlashAddress;
}BootloaderFlashParamsReplyStr;

typedef struct BootloaderFirmwareBlkDef
{
	lu_uint8_t  firmwareBlock256[256];
}BootloaderFirmwareBlkStr;

typedef struct BootloaderFirmwareDef
{
	lu_uint16_t blockOffset;  /* This is not an offset but an block number 0,1,2,3,etc */
	lu_uint8_t  firmwareBlock256[256];
}BootloaderFirmwareStr;

typedef struct BootloaderFirmwareReadDef
{
	lu_uint16_t blockOffset;  /* This is not an offset but an block number 0,1,2,3,etc */
}BootloaderFirmwareReadStr;

typedef struct StartOnboardApplicationRspDef
{
	lu_uint8_t errorCode;
} StartOnboardApplicationRspStr;

typedef struct BootloaderPICFirmwareDef
{
	lu_uint16_t blockOffset;       /* This is not an offset but an block number 0,1,2,3,etc */
	lu_uint8_t  uartChannel;        /* FPM: 2 = PIC1 ; 0 = PIC 2 */
	lu_uint8_t  firmwareBlock[64];
}BootloaderPICFirmwareStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */


#endif /* _C_MODULEPROTOCOLBOOTLOADER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
