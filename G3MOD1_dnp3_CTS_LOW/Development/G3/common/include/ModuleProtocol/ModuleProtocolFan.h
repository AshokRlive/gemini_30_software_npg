/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26/10/11      walde_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULEPROTOCOLFAN_INCLUDED
#define _MODULEPROTOCOLFAN_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleProtocol/ModuleProtocolEnumFanController.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


typedef struct FanTestDef
{
	/*! Channel ID */
	lu_uint8_t		channel;
	/*! Test duration (secs.) */
	lu_uint8_t		testDuration;
} FanTestStr;

LU_DISABLE_WARNING(-pedantic)

typedef struct FanConfigDef
{
	/*! Enabled indicator    */
	lu_uint8_t		fanFitted 			 : 1;
	lu_uint8_t		fanSpeedSensorFitted : 1;
	lu_uint8_t		fanDigitalOutput 	 : 1;
	lu_uint8_t		fanExternalSupply	 : 1;
	lu_uint8_t		unused               : 4;
	lu_int16_t      fanTempThreshold;
	lu_uint16_t     fanTempHysteresis;
	lu_uint16_t 	fanFaultHysteresisMs;
}FanConfigStr;

LU_ENABLE_WARNING(-pedantic)
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



#endif /* _MODULEPROTOCOLFAN_INCLUDED */

/*
 *********************** End of file ******************************************
 */
