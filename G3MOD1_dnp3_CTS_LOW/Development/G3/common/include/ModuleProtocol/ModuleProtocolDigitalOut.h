/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27/10/11      walde_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULEPROTOCOLDIGITALOUT_INCLUDED
#define _MODULEPROTOCOLDIGITALOUT_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

LU_DISABLE_WARNING(-pedantic)

typedef struct DigOutConfigDef
{
	/* Channel Number */
	lu_uint8_t		channel;

	/*! Enabled indicator    */
	lu_uint8_t		outputEnabled : 1;
	lu_uint8_t      unused : 7;

	lu_uint16_t 	pulseLengthMS;
}DigOutConfigStr;


typedef struct DigOutSelectDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Time out for operate in milliseconds */
	lu_uint16_t		SelectTimeoutMS;

	/*! Local or remote operation indicator */
	lu_uint8_t		local        : 1;
	/*! Set to 1 for request from automation sequence */
	lu_uint8_t		autoSequence : 1;
	lu_uint8_t		unused       : 6;
} DigOutSelectStr;

typedef struct DigOutOperateDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Local or remote operation indicator */
	lu_uint8_t		local        : 1;
	/*! Set to 1 for request from automation sequence */
	lu_uint8_t		autoSequence : 1;
	lu_uint8_t		unused       : 6;

	/*! On/Off control - 1 = On ; 0 = Off */
	lu_bool_t       onOff;
} DigOutOperateStr;

typedef struct DigOutCancelDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Local or remote operation indicator */
	lu_uint8_t		local        : 1;
	/*! Set to 1 for request from automation sequence */
	lu_uint8_t		autoSequence : 1;
	lu_uint8_t		unused       : 6;
} DigOutCancelStr;

LU_ENABLE_WARNING(-pedantic)

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



#endif /* _MODULEPROTOCOLDIGITALOUT_INCLUDED */

/*
 *********************** End of file ******************************************
 */
