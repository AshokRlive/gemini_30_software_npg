/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27/10/11      walde_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULEPROTOCOLBATTERY_INCLUDED
#define _MODULEPROTOCOLBATTERY_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef struct ChargerConfigDef
{
	lu_uint16_t  	batteryTestPeriodHours; // 0 = disabled

	/*! Duration of operation in seconds */
	lu_uint16_t		batteryTestDurationSecs;
} ChargerConfigStr;

typedef struct BatteryResetDef
{
	/* Current battery type 					*/
	lu_uint8_t  	batType;
} BatteryResetStr;

typedef struct BatteryOperateDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Duration of operation in seconds */
	lu_uint16_t		operationDuration;

	/*! Local or remote operation indicator */
	lu_bool_t		local;

	/* Unused: reserved for later - maybe the percentage */
	lu_uint8_t      unused;
} BatteryOperateStr;

typedef struct BatterySelectDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Time out for operate in seconds */
	lu_uint8_t		SelectTimeout;

	/*! Local or remote operation indicator */
	lu_bool_t		local;
} BatterySelectStr;

typedef struct BatteryCancelDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Local or remote operation indicator */
	lu_bool_t		local;
} BatteryCancelStr;

typedef struct BatTestInhibOperateDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Local or remote operation indicator */
	lu_bool_t		local;

} BatTestInhibOperateStr;

typedef struct BatTestInhibCancelDef
{
	/*! Channel ID */
	lu_uint8_t		channel;
	/*! Local or remote operation indicator */
	lu_bool_t		local;

} BatTestInhibCancelStr;






/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



#endif /* _MODULEPROTOCOLBATTERY_INCLUDED */

/*
 *********************** End of file ******************************************
 */
