/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26/10/11      walde_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULEPROTOCOLSUPPLY_INCLUDED
#define _MODULEPROTOCOLSUPPLY_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef struct SupplyConfigDef
{
	/* Indicators for supplies enabled */
	lu_bool_t		motorSupplyEnabled;
	lu_bool_t		auxComms2SupplyEnabled;
	lu_bool_t		comms1SupplyEnabled;
}SupplyConfigStr;


typedef struct SupplyOperateDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! operation Id */
	lu_uint16_t		operationID;

	/*! Supply time-out duration in seconds */
	lu_uint8_t		supplyDuration;

	/*! Current limit (used for motor supply only) */
	lu_uint32_t		currentLimit;

	/*! Current limit threshold time in ms. */
	lu_uint16_t		currentLimitDuration;

	/*! Local or remote operation indicator */
	lu_bool_t local;
} SupplyOperateStr;

typedef struct SupplySelectDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! operation Id */
	lu_uint16_t		operationID;

	/*! Time out for operate in seconds */
	lu_uint8_t		SelectTimeout;

	/*! Local or remote operation indicator */
	lu_bool_t		local;
} SupplySelectStr;

typedef struct SupplyCancelDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! operation Id */
	lu_uint16_t		operationID;

	/*! Local or remote operation indicator */
	lu_bool_t		local;
} SupplyCancelStr;


typedef struct SupplyCancelReplyDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! operation Id */
	lu_uint16_t		operationID;

	/* !Peak motor supply current during operation */
	lu_int32_t		peakCurrent;

	/*! Status - okay or error */
	lu_uint8_t		status;
}SupplyCancelReplyStr;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



#endif /* _MODULEPROTOCOLSUPPLY_INCLUDED */

/*
 *********************** End of file ******************************************
 */
