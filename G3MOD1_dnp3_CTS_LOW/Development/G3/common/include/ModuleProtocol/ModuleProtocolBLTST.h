/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Protocol Bootloader Test API library header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULEPROTOCOLBTST_INCLUDED
#define _MODULEPROTOCOLBTST_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

typedef enum
{
	ADE78xx_FPI_1,
	ADE78xx_FPI_2,
	ADE78xx_FPI_LAST
}ADE78xxChannel;


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/* GPIO */
typedef struct TestGPIOModeDef
{
	lu_uint8_t    port;
	lu_uint8_t    pin;
	lu_uint8_t    pinFunc;
	lu_uint8_t    pinMode;
	lu_uint8_t	  openDrain;
	lu_uint8_t    dir;
} TestGPIOModeStr;

typedef struct TestGPIOPinPortDef
{
	lu_uint8_t    port;
	lu_uint8_t    pin;
} TestGPIOPinPortStr;

typedef struct TestGPIOReadDef
{
	lu_uint8_t    value;
} TestGPIOReadStr;

typedef struct TestGPIOWriteDef
{
	lu_uint8_t    port;
	lu_uint8_t    pin;
	lu_uint8_t    value;
} TestGPIOWriteStr;

typedef struct TestGPIOPortReadDef
{
	lu_uint32_t    value;
} TestGPIOPortReadStr;

typedef struct TestGPIOPortDef
{
	lu_uint8_t    port;
} TestGPIOPortStr;

typedef struct TestGPIOPortWriteDef
{
	lu_uint8_t    port;
	lu_uint32_t   value;
} TestGPIOPortWriteStr;

/* ADC_READ */
typedef struct TestADCPinChanDef
{
	lu_uint8_t    chan;
	lu_uint8_t	  rateKhz;
} TestADCPinChanStr;

typedef struct TestADCReadDef
{
	lu_uint16_t    value;
} TestADCReadStr;

/* SPI DIGI POT  */
typedef struct TestSPIDigiPotAddrDef
{
	lu_uint8_t	  sspChan;
	lu_uint8_t	  csChan;
	lu_uint8_t	  potChan;
} TestSPIDigiPotAddrStr;

/* SPI DIGI POT - FactoryTestSPIDigiPotWrite() / FactoryTestSPIADDigitalPotWrite() */
typedef struct TestSPIDigiPotWriteDef
{
	TestSPIDigiPotAddrStr	addr;
	lu_uint16_t	  			potVal;
} TestSPIDigiPotWriteStr;

typedef struct TestSPIDigiPotWriteV2Def
{
	lu_uint8_t sspChan;
	lu_uint8_t csPort;
	lu_uint8_t csPin;
	lu_uint8_t potChan;
	lu_uint16_t	potVal;
}TestSPIDigiPotWriteV2Str;

typedef struct TestDACStr
{
	lu_uint16_t value;
}TestDACStr;

typedef struct TestSPIDigiPotWriteRspDef
{
	lu_uint8_t	retVal;
}TestSPIDigiPotWriteRspStr;

/* SPI DIGI POT - FactoryTestSPIDigiPotRead() */
typedef struct TestSPIDigiPotReadDef
{
	TestSPIDigiPotAddrStr	addr;
} TestSPIDigiPotReadStr;

typedef struct TestSPIDigiPotReadRspDef
{
	lu_uint16_t	potVal;
}TestSPIDigiPotReadRspStr;

/* I2C ADC MCP342x */
typedef struct TestI2CAdcMCP342XReadDef
{
	lu_uint8_t	  i2cChan;
	lu_uint8_t	  addr;
	lu_uint8_t	  adcChan;
}TestI2CAdcMCP342XReadStr;

typedef struct TestI2CAdcMCP342XReadRspDef
{
	lu_uint16_t	adcVal;
}TestI2CAdcMCP342XReadRspStr;

/* SPI ADC  */
typedef struct TestSpiAdcAddrDef
{
	lu_uint8_t	  spiChan;
	lu_uint8_t	  csPort;
	lu_uint8_t	  csPin;
	lu_uint8_t	  adcChan;
} TestSpiAdcAddrStr;

/* SPI ADC - FactoryTestSpiAdcRead() */
typedef struct TestSpiAdcReadDef
{
	TestSpiAdcAddrStr	addr;
} TestSpiAdcReadStr;

typedef struct TestSpiAdcReadRspDef
{
	lu_uint16_t	adcVal;
}TestSpiAdcReadRspStr;

/* I2C IO Expander */
typedef struct TestI2cExpAddrDef
{
	lu_uint8_t	  i2cChan;
	lu_uint8_t	  addr;
} TestI2cExpAddrStr;

/* I2C Expander write pin */
typedef struct TestI2cExpPinWriteDef
{
	TestI2cExpAddrStr addr;
	lu_uint8_t        pin;
	lu_uint8_t	      value;
} TestI2cExpPinWriteStr;

/* I2C Expander read pin */
typedef struct TestI2cExpPinReadDef
{
	TestI2cExpAddrStr addr;
	lu_uint8_t        pin;
} TestI2cExpPinReadStr;

typedef struct TestI2cExpPinReadRspDef
{
	lu_uint8_t		  value;
} TestI2cExpPinReadRspStr;

/* I2C Expander write port */
typedef struct TestI2cExpPortWriteDef
{
	TestI2cExpAddrStr addr;
	lu_uint16_t	      value;
} TestI2cExpPortWriteStr;

/* I2C Expander read port */
typedef struct TestI2cExpPortReadDef
{
	TestI2cExpAddrStr addr;
} TestI2cExpPortReadStr;

typedef struct TestI2cExpPortReadRspDef
{
	lu_uint16_t		  value;
} TestI2cExpPortReadRspStr;

/* I2C Expander write ddr */
typedef struct TestI2cExpDDRWriteDef
{
	TestI2cExpAddrStr addr;
	lu_uint16_t	      value;
} TestI2cExpDDRWriteStr;

/* I2C Expander read ddr */
typedef struct TestI2cExpDDRReadDef
{
	TestI2cExpAddrStr addr;
} TestI2cExpDDRReadStr;

typedef struct TestI2cExpDDRReadRspDef
{
	lu_uint16_t		  value;
} TestI2cExpDDRReadRspStr;

/* I2C NVRAM */
typedef struct TestNVRAMAddrDef
{
	lu_uint8_t	  i2cChan;
	lu_uint8_t	  addr;
	lu_uint16_t	  offset;
} TestNVRAMAddrStr;

/* I2C NVRAM - FactoryTestNVRAMRead() */
typedef struct TestNVRAMReadDef
{
	TestNVRAMAddrStr	addr;
} TestNVRAMReadStr;

/* I2C NVRAM - FactoryTestNVRAMReadBuffer() */
typedef struct TestNVRAMReadBuffDef
{
	TestNVRAMAddrStr	addr;
	lu_uint16_t         size;
} TestNVRAMReadBuffStr;

/* I2C NVRAM - FactoryTestNVRAMWrite() */
typedef struct TestNVRAMWriteDef
{
	TestNVRAMAddrStr	addr;
	lu_uint8_t	  		value;
} TestNVRAMWriteStr;

typedef struct TestNVRAMReadRspDef
{
	lu_uint8_t	retVal;
}TestNVRAMReadRspStr;


/* I2C Temperature sensor LM73 */
typedef struct TestI2cTempLM73ReadDef
{
	lu_uint8_t	  i2cChan;
	lu_uint8_t	  addr;
} TestI2cTempLM73ReadStr;

typedef struct TestI2cTempLM73ReadRspDef
{
	lu_int32_t		  value;
} TestI2cTempLM73ReadRspStr;

typedef struct TestI2cTempLM73ReadRegDef
{
	TestI2cTempLM73ReadStr params;
	lu_uint8_t             reg;
} TestI2cTempLM73ReadRegStr;

typedef struct TestI2cTempLM73WriteRegDef
{
	lu_uint8_t	  i2cChan;
	lu_uint8_t	  addr;
	lu_uint8_t	  reg;
	lu_uint8_t    value;
}TestI2cTempLM73WriteRegStr;


/* I2C Humidity sensor */
typedef struct TestI2cHumidityReadDef
{
	lu_uint8_t	  i2cChan;
} TestI2cHumidityReadStr;

typedef struct TestI2cTHumidityReadRspDef
{
	lu_int32_t		  temperature;
	lu_int32_t		  humidity;
	lu_uint8_t        status;
} TestI2cTHumidityReadRspStr;

typedef struct IOIDTableCountDef
{
	lu_uint16_t ioIDLast;
}IOIDTableCountStr;

typedef struct IOIDTableElementDef
{
	lu_uint16_t ioID;
	lu_uint8_t *ioIDName;
	lu_uint16_t ioIDClass;
}IOIDTableElementStr;

/* SSP DMA - Enable / Disable */
typedef struct TestSSPDMADef
{
	lu_uint8_t sspBus;
	lu_uint8_t enable;
}TestSSPDMAStr;

typedef struct TestFEPReadDef
{
	lu_uint8_t sspBus;
	lu_uint8_t adcChan;
}TestFEPReadStr;

typedef struct TestFEPDataDef
{
	lu_uint8_t	errorCode;
	lu_uint16_t value;
}TestFEPDataStr;


/******************** ADE78xx Tests ********************/

/* ADE78xx Pin map Structure */
typedef struct TestADE78xxPinMapDef
{
	TestGPIOPinPortStr pm0;
	TestGPIOPinPortStr pm1;
	TestGPIOPinPortStr reset;
	TestGPIOPinPortStr irq0;
	TestGPIOPinPortStr irq1;
	TestGPIOPinPortStr cf1;
	TestGPIOPinPortStr cf2;
}TestADE78xxPinMapStr;

/* I2C ADE78xx Enable and Initialise Command */
typedef struct TestI2cADE78xxInitDef
{
	lu_uint8_t 			 i2cChan;
	lu_uint8_t 			 sspBus;
	lu_uint8_t 			 csPort;
	lu_uint8_t 			 csPin;
	TestADE78xxPinMapStr pinMap;
	lu_uint8_t 			 enable;
} TestI2cADE78xxInitStr;

/* I2C ADE78xx Write Register Command */
typedef struct TestI2cADE78xxWriteRegDef
{
	lu_uint8_t		i2cChan;
	lu_uint16_t	    reg;
	lu_uint8_t		length;
	lu_uint32_t     value;
} TestI2cADE78xxWriteRegStr;

/* I2C ADE78xx Read Register Command */
typedef struct TestI2cADE78xxReadRegDef
{
	lu_uint8_t		i2cChan;
	lu_uint16_t	    reg;
	lu_uint8_t		length;
	lu_uint32_t     value;
} TestI2cADE78xxReadRegStr;

/* SPI ADE78xx  Enable and Initialise Command */
typedef struct TestSpiADE78xxInitDef
{
	lu_uint8_t 			 sspBus;
	lu_uint8_t 			 csPort;
	lu_uint8_t 			 csPin;
	TestADE78xxPinMapStr pinMap;
}TestSpiADE78xxInitStr;

/* SPI ADE78xx Write Register Command */
typedef struct TestSpiADE78xxWriteRegDef
{
	lu_uint8_t  sspBus;
	lu_uint8_t  csPort;
	lu_uint8_t  csPin;
	lu_uint16_t reg;
	lu_uint8_t  length;
	lu_uint32_t value;
}TestSpiADE78xxWriteRegStr;

/* SPI ADE78xx  Read Register Command*/
typedef struct TestSpiADE78xxReadRegDef
{
	lu_uint8_t  sspBus;
	lu_uint8_t  csPort;
	lu_uint8_t  csPin;
	lu_uint16_t reg;
	lu_uint8_t  length;
	lu_uint32_t	value;
}TestSpiADE78xxReadRegStr;

/* ADE78xx Read Register Response Response */
typedef struct TestI2cADE78xxReadRspDef
{
	lu_uint8_t		errorCode;
	lu_uint32_t		value;
} TestI2cADE78xxReadRspStr;

typedef struct TestI2cADE78xxReadPeakDef
{
	lu_uint8_t		fpiChan;
	lu_uint8_t		peakChan;
} TestI2cADE78xxReadPeakStr;

typedef struct TestI2cADE78xxReadPeakRespDef
{
	lu_uint8_t		fpiChan;
	lu_uint8_t		peakChan;
	lu_uint32_t     peakValue;
} TestI2cADE78xxReadPeakRespStr;

/*!
 * LCD Write Display
 */
typedef struct TestLcdWriteDef
{
    /*! Display write */
	lu_uint8_t     posRow;
	lu_uint8_t     posColumn;
	lu_uint8_t	   bufferLen;  /* Length of additional payload */
}TestLcdWriteStr;

/* General error code response */
typedef struct TestErrorRspDef
{
	lu_uint16_t	errorCode;
}TestErrorRspStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

#endif /* _MODULEPROTOCOLBTST_INCLUDED */

/*
 *********************** End of file ******************************************
 */
