/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Protocol Calibration Test API library header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/05/12      venkat     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULEPROTOCOLCALTST_INCLUDED
#define _MODULEPROTOCOLCALTST_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/* */
typedef struct CalTstEraseRspDef
{
	lu_uint8_t   status;
} CalTstEraseRspStr;

/* Calibration convert value */
typedef struct CalTstConvValueDef
{
	lu_uint8_t   calID;
	lu_int32_t   inputValue;
} CalTstConvValueStr;

/* Calibration convert value - Rsp */
typedef struct CalTstConvValueRspDef
{
	lu_int8_t  status;
	lu_int32_t calValue;
} CalTstConvValueRspStr;

/* Calibration Data format in memory */
typedef struct CalTstDataHeaderdef
{
	lu_uint8_t calId;
	lu_uint8_t type;
	lu_int16_t length;
} CalTstDataHeaderStr;

/* Calibration get data */
typedef struct CalTstGetDataDef
{
	lu_uint8_t   calID;
} CalTstCGetDataStr;

/* Calibration get data rsp */
typedef struct CalTstGetDataRspDef
{
	CalTstDataHeaderStr  calHeader;
} CalTstGetDataRspStr;

/* Calibration convert value - Rsp */
typedef struct CalTstAddElementRspDef
{
	lu_int8_t  calID;
	lu_int8_t status;
} CalTstAddElementRspStr;

typedef struct CalTstNvramSelDef
{
	lu_uint8_t 		nvramType; /* NVRAM_TYPE */
} CalTstNvramSelStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


#endif /* _MODULEPROTOCOLCALTST_INCLUDED */

/*
 *********************** End of file ******************************************
 */
