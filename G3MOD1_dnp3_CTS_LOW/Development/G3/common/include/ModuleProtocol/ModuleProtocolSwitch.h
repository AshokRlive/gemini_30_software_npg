/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Protocol Switch Controller library header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULEPROTOCOLSWITCH_INCLUDED
#define _MODULEPROTOCOLSWITCH_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef struct SwitchConfigDef
{
	/* Channel Number */
	lu_uint8_t		channel;

	/* Enabled indicator */
	lu_bool_t		SwitchEnabled;

	/* Up, Down or Link relay */
	lu_uint8_t 		polarity;

	/* Length of operating pulse in ms */
	lu_uint16_t 	pulseLengthMS;

	/* Length of overrun time in ms */
	lu_uint16_t 	overrunMS;

	/* Length of operation timeout in seconds */
	lu_uint16_t		opTimeoutS;

	/* Bit mask of inhibit inputs by channel number */
	lu_uint16_t 	inhibitBI;

}SwitchConfigStr;

typedef struct SwitchOperateDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Delayed operation duration in seconds */
	lu_uint8_t		SwitchDelay;

	/*! Pre-operation delay duration in milliseconds */
	lu_uint16_t		SwitchPreOpDelay;

	/*! Local or remote operation indicator */
	lu_bool_t		local;
} SwitchOperateStr;

typedef struct SwitchSelectDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Time out for operate in seconds */
	lu_uint8_t		SelectTimeout;

	/*! Local or remote operation indicator */
	lu_bool_t		local;
} SwitchSelectStr;

typedef struct SwitchCancelDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Local or remote operation indicator */
	lu_bool_t		local;
} SwitchCancelStr;

typedef struct SwitchSynchDef
{
    /*! Channel ID */
    lu_uint8_t      channel;
} SwitchSynchStr;


typedef enum
{
	SWITCH_POLARITY_UP,
	SWITCH_POLARITY_DOWN,
	SWITCH_POLARITY_LINK,

	SWITCH_POLARITY_LAST
}SWITCH_POLARITY;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


#endif /* _MODULEPROTOCOLSWITCH_INCLUDED */

/*
 *********************** End of file ******************************************
 */
