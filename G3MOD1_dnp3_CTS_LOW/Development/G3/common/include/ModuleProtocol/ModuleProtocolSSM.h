/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/14      venkat_s    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULEPROTOCOLSSM_INCLUDED
#define _MODULEPROTOCOLSSM_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleProtocolEnumDSM.h"

#include "ModuleProtocolDualSwitch.h"



/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

LU_DISABLE_WARNING(-pedantic)

typedef struct SSMConfigDef
{
	/*! LED Polarity - 1 = Green for open ; 0 = Red for open */
	lu_uint8_t  	greenLEDPolarity : 1;

	lu_uint8_t      polarity : 2;
	lu_uint8_t		doNotCheckSwitchPosition : 1;

	lu_uint8_t      unused : 4;
} SSMConfigStr;

LU_ENABLE_WARNING(-pedantic)

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



#endif /* _MODULEPROTOCOLSSM_INCLUDED */

/*
 *********************** End of file ******************************************
 */
