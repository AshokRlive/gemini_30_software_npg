/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULEPROTOCOLFPM_INCLUDED
#define _MODULEPROTOCOLFPM_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleProtocolEnumFPM.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

typedef enum
{
    FPI_CONFIG_CH_FPI1      = 0x00,
    FPI_CONFIG_CH_FPI2            ,

    FPI_CONFIG_CH_LAST
}FPI_CONFIG_CH;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef struct FPIOperateDef
{
	/*! Channel ID */
	lu_uint8_t		channel;
}FPIOperateStr;

typedef struct FPMEarthFaultDef
{
	lu_uint16_t     minFaultDurationMs;
	lu_uint32_t     timedFaultCurrent;  // x1 scaled units
	lu_uint32_t     InstantFaultCurrent; // x1 scaled units
}FPMEarthFaultStr;

typedef struct FPMPhaseFaultDef
{
	lu_uint16_t     minFaultDurationMs;
	lu_uint16_t  	minOverloadDurationMs;
	lu_uint32_t 	overloadCurrent;
	lu_uint32_t     timedFaultCurrent;  // x1 scaled units
	lu_uint32_t     InstantFaultCurrent; // x1 scaled units
}FPMPhaseFaultStr;

typedef struct FPMCTRatioDef
{
	lu_uint32_t     ctRatioDividend;  // x1 scaled units
	lu_uint32_t     ctRatioDivisor; // x1 scaled units
}FPMCTRatioStr;

typedef struct FPMAbsenceDef
{
	lu_uint32_t		timedPresenceCurrent;
	lu_uint32_t		minPresenceDurationMs;
	lu_uint32_t		timedAbsenceCurrent;
	lu_uint32_t		minAbsenceDurationMs;
}FPMAbsenceStr;


typedef struct FPMConfigDef
{
	/* Channel Number */
	lu_uint8_t   		fpiConfigChannel; // FPI_CONFIG_CH

	lu_bool_t	 		enabled;

	FPMPhaseFaultStr  	phaseFault;

	FPMEarthFaultStr  	earthFault;

    FPMEarthFaultStr  	sensitiveEarthFault;

    FPMCTRatioStr 		ctRatio;

    FPMAbsenceStr		absenceIndication;

    lu_uint32_t 		selfResetMs;
}FPMConfigStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



#endif /* _MODULEPROTOCOLFPM_INCLUDED */

/*
 *********************** End of file ******************************************
 */
