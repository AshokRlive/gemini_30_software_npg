/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       NVRAM stucture definitions header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/09/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _NVRAMDEF_INCLUDED
#define _NVRAMDEF_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(1)

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


#include "NVRAMDef/NVRAMDefApp.h"
#include "NVRAMDef/NVRAMDefID.h"
#include "NVRAMDef/NVRAMDefBattery.h"

#include "NVRAMDef/NVRAMDefInfo.h"
#include "NVRAMDef/NVRAMDefOptPSM.h"
#include "NVRAMDef/NVRAMDefOptHMI.h"
#include "NVRAMDef/NVRAMDefUserPSM.h"
#include "NVRAMDef/NVRAMDefUserDSM.h"
#include "NVRAMDef/NVRAMDefOptBat.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define NVRAM_EEPROM_SIZE				(1024)

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*!
 * NVRAM TYPE - the physical NVRAM chip to use
 */
typedef enum
{
	NVRAM_TYPE_IDENTITY		= 0,
	NVRAM_TYPE_APPLICATION     ,
	NVRAM_TYPE_BATTERY_ID      ,
	NVRAM_TYPE_BATTERY_DATA    ,

	NVRAM_TYPE_LAST
}NVRAM_TYPE;

#pragma pack(1)

LU_DISABLE_WARNING(-pedantic)
/*!
 *  NVRAM Block Version
 */
typedef struct NVRAMBlkHeadVersionDef
{
	//! ID test block Major no.
	lu_uint32_t versionMajor : 8;
	//! ID test block Minor no.
	lu_uint32_t versionMinor : 8;
	/*! Update counter (Increment each time write NVRAM) */
	lu_uint32_t	updateCounter : 16;
}NVRAMBlkHeadVersionStr;

LU_ENABLE_WARNING(-pedantic)

/*!
 *  NVRAM Block Header @ start of every block
 */
typedef struct NVRAMBlkHeadDef
{
	NVRAMBlkHeadVersionStr  version;
	/*! Size of data after this header */
	lu_uint16_t     		dataSize;
	/*! Size of data after this header */
	lu_uint32_t				dataCrc32;
}NVRAMBlkHeadStr;

#pragma pack()

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

#pragma pack()

#ifdef __cplusplus
}
#endif

#endif /* _NVRAMDEF_INCLUDED */

/*
 *********************** End of file ******************************************
 */
