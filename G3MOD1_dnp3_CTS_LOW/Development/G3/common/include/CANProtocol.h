/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol global definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



#ifndef _CAN_PROTOCOL_INCLUDED
#define _CAN_PROTOCOL_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define CAN_BUS_BAUDRATE             (500000)

#define CAN_MAX_PAYLOAD              (8)

/*! Return the variable size in bytes */
#define CAN_MESSAGE_SIZE(a)          (sizeof(a))

/*! CAN Extended Id (29-bit) */
#define CAN_EXT_ID_MASK              (0x1FFFFFFF)

/*! Maximum number of fragment in a fragmented message */
#define CAN_MAX_FRAGMENTS            (0x3F)


/*!
 * Masks values for CANHeaderDef bit fields
 *
 * They can be used for setting the CAN filter tables.
 */
typedef enum
{
    CANHEADERDEF_MASK_FRAGMENT     = 0x01,
    CANHEADERDEF_MASK_DEVICEID     = 0x07,
    CANHEADERDEF_MASK_DEVICE       = 0x1F,
    CANHEADERDEF_MASK_MESSAGEID    = 0x3F,
    CANHEADERDEF_MASK_MESSAGETYPE  = 0x3F
} CANHEADERDEF_MASK;
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

LU_DISABLE_WARNING(-pedantic)
/*!
 * CAN Header struct
 */
typedef struct CANHeaderDef
{
    /*! Set to 1 if this is a fragment */
    lu_uint32_t fragment    : 1;
    lu_uint32_t deviceIDSrc : 3;
    lu_uint32_t deviceSrc   : 5;
    lu_uint32_t deviceIDDst : 3;
    lu_uint32_t deviceDst   : 5;
    lu_uint32_t messageID   : 6;
    lu_uint32_t messageType : 6;
}CANHeaderStr;

/*!
 * CAN Fragment header.
 * First packet: start = 1, stop = 0
 * Intermediate packets: start = 1, stop = 1
 * Last packet: start = 0, stop = 1
 * Fragment ID is incremented for each packet
 */
typedef struct CANFragmentHeaderDef
{
    lu_uint8_t id    : 6;
    lu_uint8_t start : 1;
    lu_uint8_t stop  : 1;
}CANFragmentHeaderStr;
LU_ENABLE_WARNING(-pedantic)

typedef enum
{
    COMM_INTERFACE_CAN0 = 0,
    COMM_INTERFACE_CAN1    ,
    COMM_INTERFACE_UNKNOWN ,

    COMM_INTERFACE_LAST
}COMM_INTERFACE;
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */


#ifdef __cplusplus
}
#endif

#endif /* _CAN_PROTOCOL_INCLUDED */


/*
 *********************** End of file ******************************************
 */

