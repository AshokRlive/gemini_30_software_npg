/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       NVRAM stucture definitions header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/09/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _NVRAMDEFAPP_INCLUDED
#define _NVRAMDEFAPP_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define NVRAM_APP_BLK_INFO_OFFSET  		0x0
#define NVRAM_APP_BLK_INFO_SIZE  		0x80

#define NVRAM_APP_BLK_OPTS_OFFSET  		0x80
#define NVRAM_APP_BLK_OPTS_SIZE  		0x80

#define NVRAM_APP_BLK_USER_A_OFFSET  	0x100
#define NVRAM_APP_BLK_USER_A_SIZE  		0x40

#define NVRAM_APP_BLK_CAL_OFFSET		0x180
#define NVRAM_APP_BLK_CAL_SIZE			0x280

#define NVRAM_APP_BLK_REG_OFFSET  		0x140
#define NVRAM_APP_BLK_REG_SIZE  		0x40

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*!
 * NVRAM Application block ID
 */
typedef enum
{
	NVRAM_APP_BLK_INFO    = 0,
	NVRAM_APP_BLK_USER_A     ,
	NVRAM_APP_BLK_CAL        ,
	NVRAM_APP_BLK_OPTS       ,
	NVRAM_APP_BLK_REG        ,

    /*! End */
	NVRAM_APP_BLK_LAST
}NVRAM_APP_BLK;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


#endif /* _NVRAMDEFAPP_INCLUDED */

/*
 *********************** End of file ******************************************
 */
