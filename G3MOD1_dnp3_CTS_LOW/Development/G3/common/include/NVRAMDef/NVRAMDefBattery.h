/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _NVRAMDEFBATTERY_INCLUDED
#define _NVRAMDEFBATTERY_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(1)

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "NVRAMDef/NVRAMDefIDCommon.h"
#include "LinearInterpolation.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define NVRAM_BATT_ID_BLK_INFO_OFFSET  		0x0
#define NVRAM_BATT_ID_BLK_INFO_SIZE  		0x80

#define NVRAM_BATT_ID_BLK_OPTS_OFFSET  		0x80
#define NVRAM_BATT_ID_BLK_OPTS_SIZE  			0x80

#define NVRAM_BATT_ID_BLK_TEST_OFFSET  		0x180
#define NVRAM_BATT_ID_BLK_TEST_SIZE  		0x80

#define NVRAM_BATT_ID_BLK_CAL_OFFSET		0x200
#define NVRAM_BATT_ID_BLK_CAL_SIZE		    0x200


#define NVRAM_BATT_DATA_BLK_INFO_OFFSET  	0x0
#define NVRAM_BATT_DATA_BLK_INFO_SIZE  		0x80

#define NVRAM_BATT_DATA_BLK_OPTS_OFFSET  	0x80
#define NVRAM_BATT_DATA_BLK_OPTS_SIZE  		0x80

#define NVRAM_BATT_DATA_BLK_USER_A_OFFSET  	0x100
#define NVRAM_BATT_DATA_BLK_USER_A_SIZE		0x100

#define NVRAM_BATT_DATA_BLK_USER_B_OFFSET	0x200
#define NVRAM_BATT_DATA_BLK_USER_B_SIZE		0x100

/* Number of points to be used in the linear interpolation of battery capacity */
#define BATT_CAPACITY_MAX					8

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/* Battery Data block NVRAM Read/Write Enums */
typedef enum
{
	CHARGING = 0,
	DISCHARGING
}BAT_CHARGE;

typedef enum
{
	BAT_TEMP_RANGE_ALL = 0,
	BAT_TEMP_RANGE_BELOW_0,
	BAT_TEMP_RANGE_00_40,
	BAT_TEMP_RANGE_40_60,
	BAT_TEMP_RANGE_ABOVE_60,
	BAT_TEMP_RANGE_LAST
}BAT_TEMP_RANGE;


/* Battery Data block NVRAM Read/Write sub-structures */
typedef struct CurrentTotalDef
{
	lu_int32_t total100mAmpHours;		/* Records the total 0.1 amp hours								*/
	lu_int64_t mAmpSeconds;				/* Records additional mA seconds 								*/
} CurrentTotalStr;

typedef struct TimeTotalDef
{
	lu_int32_t totalMin;				/* Records the total minutes		 							*/
	lu_int32_t seconds;					/* Records additional seconds									*/
} TimeTotalStr;

typedef struct CurrentDataDef
{
	CurrentTotalStr	current;
	TimeTotalStr	time;
} CurrentDataStr;

typedef struct ChargingDef
{
	CurrentDataStr currentData[2];		/* Array of 2 for charging and discharging						*/
}ChargingStr;

LU_DISABLE_WARNING(-pedantic)

/* Battery NVRAM Read/Write Structure */
typedef struct BatteryDataNVRAMBlkDef
{
	/* Battery Data */
	lu_int32_t chargeCycles;			/* Records the number of charge cycles the battery has had		*/
	lu_int32_t estimatedCapacity;		/* Records the estimated charge in the battery pack 			*/

	ChargingStr	charging[BAT_TEMP_RANGE_LAST];

	lu_uint8_t batteryFault  : 1;
	lu_uint8_t unused             : 7;

} BatteryDataNVRAMBlkStr;

LU_ENABLE_WARNING(-pedantic)

/*!
 * NVRAM Battery ID block
 */
typedef enum
{
	NVRAM_BATT_ID_BLK_INFO    = 0,
	NVRAM_BATT_ID_BLK_TEST       ,
	NVRAM_BATT_ID_BLK_CAL        ,
	NVRAM_BATT_ID_BLK_OPTS		 ,

    /*! End */
	NVRAM_BATT_ID_BLK_LAST
}NVRAM_BATT_ID_BLK;

/*!
 * NVRAM Battery Data block
 */
typedef enum
{
	NVRAM_BATT_DATA_BLK_INFO    = 0,
	NVRAM_BATT_DATA_BLK_USER_A     ,
	NVRAM_BATT_DATA_BLK_USER_B     ,
	NVRAM_BATT_DATA_BLK_OPTS       ,

    /*! End */
	NVRAM_BATT_DATA_BLK_LAST
}NVRAM_BATT_DATA_BLK;



/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

#pragma pack()

#ifdef __cplusplus
}
#endif


#endif /* _NVRAMDEFBATTERY_INCLUDED */

/*
 *********************** End of file ******************************************
 */
