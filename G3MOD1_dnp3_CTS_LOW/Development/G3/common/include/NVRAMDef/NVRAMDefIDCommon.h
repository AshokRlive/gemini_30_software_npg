/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       NVRAM stucture definitions header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/09/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _NVRAMDEFIDCOMMON_INCLUDED
#define _NVRAMDEFIDCOMMON_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define	NVRAMID_ASSY_NO_MAX_SIZE		12
#define NVRAMID_ASSY_REV_MAX_SIZE		 8
#define NVRAMID_BATCH_MAX_SIZE	        12
#define NVRAMINFO_SUPPLY_ID_MAX_SIZE     4

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*!
 *  Date info
 */

typedef struct NVRAMIDDateDef
{
    lu_uint8_t	day;
    lu_uint8_t  month;
    lu_uint16_t year;
}NVRAMIDDateStr;

/*!
 *  Calibration info
 */

typedef struct NVRAMIDCalibrationInfoDef
{
	NVRAMIDDateStr  calibrationDate;
}NVRAMIDCalibrationInfoStr;

/*!
 *  Assembly info
 */

typedef struct NVRAMIDAssemblyInfoDef
{

    lu_uint8_t      assemblyNo[NVRAMID_ASSY_NO_MAX_SIZE];
    lu_uint8_t      assemblyRev[NVRAMID_ASSY_REV_MAX_SIZE];
    lu_uint32_t     serialNumber;

}NVRAMIDAssemblyInfoStr;

/*!
 *  Manufacturing info
 */

typedef struct NVRAMIDManufacturingInfoDef
{
    lu_uint8_t                batchNumber[NVRAMID_BATCH_MAX_SIZE];
    lu_uint8_t                supplierId[NVRAMINFO_SUPPLY_ID_MAX_SIZE];
    NVRAMIDAssemblyInfoStr    assembly;
    NVRAMIDAssemblyInfoStr    subAssembly;
    lu_uint8_t                subAssemblyBatchNumber[NVRAMID_BATCH_MAX_SIZE];
    NVRAMIDDateStr  buildDate;
}NVRAMIDManufacturingInfoStr;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


#endif /* _NVRAMDEFIDCOMMON_INCLUDED */

/*
 *********************** End of file ******************************************
 */
