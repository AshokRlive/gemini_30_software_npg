/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       NVRAM stucture definitions header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/09/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _NVRAMDEFID_INCLUDED
#define _NVRAMDEFID_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(1)
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "NVRAMDef/NVRAMDefIDCommon.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define NVRAM_ID_BLK_INFO_OFFSET  		0x0
#define NVRAM_ID_BLK_INFO_SIZE  		0x80

#define NVRAM_ID_BLK_OPTS_OFFSET  		0x80
#define NVRAM_ID_BLK_OPTS_SIZE  		0x80

#define NVRAM_ID_BLK_TEST_OFFSET  		0x100
#define NVRAM_ID_BLK_TEST_SIZE  		0x80

#define NVRAM_ID_BLK_CAL_OFFSET			0x180
#define NVRAM_ID_BLK_CAL_SIZE			0x280


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*!
 * NVRAM ID block
 */
typedef enum
{
	NVRAM_ID_BLK_INFO    = 0,
	NVRAM_ID_BLK_TEST       ,
	NVRAM_ID_BLK_CAL        ,
	NVRAM_ID_BLK_OPTS       ,

    /*! End */
	NVRAM_ID_BLK_LAST
}NVRAM_ID_BLK;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

#pragma pack()

#ifdef __cplusplus
}
#endif


#endif /* _NVRAMDEFID_INCLUDED */

/*
 *********************** End of file ******************************************
 */
