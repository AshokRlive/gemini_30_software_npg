/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.h 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.h $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _DS30LOADER_INCLUDED
#define _DS30LOADER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define	DS30_CMD_HELLO						0x31

#define DS30_REP_OK							0x4b // 'K'
#define DS30_REP_CHECKSUM_ERR				0x4e // 'N'
#define DS30_REP_VERIFY_FAIL				0x56 // 'V'
#define DS30_REP_BOOTL_PROTECTED			0x50 // 'P'
#define DS30_UKNOWN_COMMAND					0x55 // 'U'

#define DS30_HELLO_TIMEOUT_MS				2000
#define DS30_BL_TIMEOUT_MS					2000

#define DS30_BAUD_RATE						9600

#define DS30_CMD_ERASE						0x01
#define DS30_CMD_WRITE						0x02
#define DS30_CMD_WRITE_EEPROM				0x03

#define DS30_BOOTLOADER_PLACEMENT			256

#define DS30_PIC16F1788_MAX_FLASH			0x4000
#define DS30_PIC16F1788_PAGESIZEW			32
#define DS30_PIC16F1788_ROWSIZEW			32

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

#pragma pack(1)
typedef struct Ds30CmdHeaderDef
{
    lu_uint16_t address;
    lu_uint8_t  command;
    lu_uint8_t  length;
}Ds30CmdHeaderStr;
#pragma pack()

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


#endif /* _DS30LOADER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
