/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:lu_util.hpp
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief Utilities for C++ general purpose
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26 Jan 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef LU_UTIL_HPP_
#define LU_UTIL_HPP_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <limits>
#include <float.h>  //Float types' template specializations

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

/* Lucy-specific namespace. Defined in uppercase as an execption in order to
 * keep a similar look as global functions of type LU_XX() with LU::xx()
 */
namespace LU
{


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Get the type of a given entity.
 *
 * Especially useful for getting the type of a structure member when it can
 * change in the future.
 *
 * \param entity Variable or expression from where we want to infer the type
 *
 * \return Type of the entity.
 *
 * \example maxAddr = std::numeric_limits<LU_TYPEOF(info.startAddr)>::max();
 */
#if __cplusplus >= 201100L
    //C++11's decltype allows to get the type of a reference
    //such as decltype(int&) == int&
    #define LU_TYPEOF(entity)     decltype(entity)
#elif __GNUG__     //Check __GNUC__ && __cplusplus
    //GNU extension's typeof gets the type but drops references
    //such as typeof(int&) == int
    #define LU_TYPEOF(entity)     typeof(entity)
#elif __typeof__
    //More generic compiler extension
    #define LU_TYPEOF(entity)  __typeof__(entity)
#elif __typeof
    #define LU_TYPEOF(entity)  __typeof(entity)
#else
    //No proper alternative
#endif


/**
 * \brief Get the max/min value of an entity without needing to know the type.
 *
 * Note that lu_floatXX_t types gives the lowest possible value instead of the
 * minimum positive representable value given by std::numeric_limits<T>::min(),
 * so that's why there are template specializations.
 *
 * \param entity Variable.
 *
 * \return Max or min value.
 *
 * \example maxi = LU::maxValue(info.startAddress);
 */
template <typename T> T maxValue(const T& entity)
{
    LU_UNUSED(entity);
    return std::numeric_limits<T>::max();
}

template <typename T> T minValue(const T& entity)
{
    LU_UNUSED(entity);
    return std::numeric_limits<T>::min();
}

/* minValue's "function template specializations" for floats. Note that is
 * really a function overload of the template, since there is no such
 * "function's partial template specializations".
 * See http://www.gotw.ca/publications/mill17.htm for an article about it that
 * appeared in C/C++ Users Journal, 19(7), July 2001.
 *  */
template <typename T> lu_float32_t minValue(const lu_float32_t& entity)
{
    LU_UNUSED(entity);
    return -FLT_MAX;
}

template <typename t> lu_float64_t minValue(const lu_float64_t& entity)
{
    LU_UNUSED(entity);
    return -DBL_MAX;
}


}/*End namespace LU*/

#endif /* LU_UTIL_HPP_ */

/*
 *********************** End of file ******************************************
 */
