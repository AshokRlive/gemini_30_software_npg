/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief Module Protocol global definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULE_PROTOCOL_INCLUDED
#define _MODULE_PROTOCOL_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*! Include Auto-generated header file */
#include "versions.h"

#pragma pack(push)
#pragma pack(1) //Note: versions.h disables packing

#include "ModuleProtocolEnum.h"
#include "SysAlarmCodeEnum.h"
#include "ModuleProtocol/ModuleProtocolBootloader.h"
#include "ModuleProtocol/ModuleProtocolCALTST.h"
#include "ModuleProtocol/ModuleProtocolBLTST.h"
#include "ModuleProtocol/ModuleProtocolNVRAM.h"
#include "ModuleProtocol/ModuleProtocolMCM.h"
#include "ModuleProtocol/ModuleProtocolPSM.h"
#include "ModuleProtocol/ModuleProtocolSCM.h"
#include "ModuleProtocol/ModuleProtocolSCMMK2.h"
#include "ModuleProtocol/ModuleProtocolFDM.h"
#include "ModuleProtocol/ModuleProtocolHMI.h"
#include "ModuleProtocol/ModuleProtocolIOM.h"
#include "ModuleProtocol/ModuleProtocolDSM.h"
#include "ModuleProtocol/ModuleProtocolFPM.h"
#include "ModuleProtocol/ModuleProtocolSSM.h"




/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*! Maximum payload length in bytes */
#define MODULE_MESSAGE_LENGTH           (440)

/*! Return the variable size in bytes */
#define MODULE_MESSAGE_SIZE(a)          (sizeof(a))

/*! Time synch invalid time */
#define MODULE_TIME_INVALID             (0x3FFF)

/*! Time synch precision in us */
#define MODULE_TIME_RESOLUTION_US       (100)

/*! Maximum number of supported time slot for time synch */
#define MODULE_TIME_MAX_TIMESLOT        (4)

/*! Maximum delay between two Time synch message.
 * If a Time Synch message is not received within this time a slave enters the
 * Out-Of-Synch state
 */
#define MODULE_TIME_SYNCH_MAX_DELAY_US  (1500000)

/*! Maximum delay between two Heart Beat messages. */
#define MODULE_HBEAT_MAX_DELAY_MS       (3000)

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*!
 * MODULE message types.
 * The type value identifies the message priority.
 * 0 is the highest priority.
 */
typedef enum
{
    /*! Highest priority message */
    MODULE_MSG_TYPE_HPRIO      = 0x00,

    /*! IO Event type message */
    MODULE_MSG_TYPE_EVENT      = 0x01,

    /*! HMI Button press events */
    MODULE_MSG_TYPE_HMI_EVENT  = 0x10,

    /*! Module level command */
    MODULE_MSG_TYPE_MD_CMD     = 0x15,

    /*! Channel level command */
    MODULE_MSG_TYPE_CMD        = 0x20,
    MODULE_MSG_TYPE_CFG        = 0x21,

    /*! IO Manager type message */
    MODULE_MSG_TYPE_IOMAN      = 0x23,

    /*! System Alarm Log message type */
    MODULE_MSG_TYPE_SYSALARM   = 0x30,

    /*! Calibration message type */
    MODULE_MSG_TYPE_CALTST     = 0x31,

    /*! NVRAM message type */
    MODULE_MSG_TYPE_NVRAM      = 0x32,

    /*! Bootloader Test message type */
    MODULE_MSG_TYPE_BLTST      = 0x33,

    /*! Bootloader message type   */
    MODULE_MSG_TYPE_BL         = 0x34,

    /*! FPI Test Message Type     */
    MODULE_MSG_TYPE_BLTST_1    = 0x35,

	/*! Lowest priority message */
    MODULE_MSG_TYPE_LPRIO      = 0x3F
}MODULE_MSG_TYPE;

/*!
 * MODULE messages subtypes.
 * The type value identifies the message priority.
 * 0 is the highest priority.
 */
typedef enum
{
    /* MODULE_MSG_TYPE_HPRIO */
    MODULE_MSG_ID_HPRIO_RESERVED              = 0x00,
    MODULE_MSG_ID_HPRIO_TSYNCH                = 0x01, /*! ModuleTsynchStr */

    /* MODULE_MSG_TYPE_EVENT */
    MODULE_MSG_ID_EVENT_FPI                   = 0x00,
    MODULE_MSG_ID_EVENT_DIGITAL               = 0x01, /*! ModuleDEventStr */
    MODULE_MSG_ID_EVENT_ANALOGUE              = 0x02, /*! ModuleAEventStr */

    /* MODULE_MSG_TYPE_HMI_EVENT */
    MODULE_MSG_ID_HMI_EVENT_BUTTON			  = 0x00, /*! HmiButtonEventStr */
    MODULE_MSG_ID_HMI_EVENT_REQ_OLR_CHANGE	  = 0x01, /*! HmiReqOLRChangeEventStr */

    /* MODULE_MSG_TYPE_MD_CMD */
    /* Start module internal components */
    MODULE_MSG_ID_MD_CMD_START_MODULE_C       = 0x00, /*! MDCmdStartModuleStr */
    MODULE_MSG_ID_MD_CMD_START_MODULE_R       = 0x01, /*! ModReplyStr */
    MODULE_MSG_ID_MD_CMD_RESTART              = 0x02, /*! MDCmdRestartStr */
    MODULE_MSG_ID_MD_CMD_STOP_EVENTING		  = 0x03,
    MODULE_MSG_ID_MD_CMD_CAN_BUS_OFF		  = 0x04,

    /* MODULE_MSG_TYPE_CMD */
    /* WARNING - KEEP THE ID's IN SEQUENTIAL ORDER!!! */
    /* Battery Charger */
    MODULE_MSG_ID_CMD_BAT_CH_SELECT_C     	  = 0x00, /*! BatterySelectStr */
    MODULE_MSG_ID_CMD_BAT_CH_SELECT_R     	  = 0x01, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_BAT_CH_OPERATE_C        = 0x02, /*! BatteryOperateStr */
    MODULE_MSG_ID_CMD_BAT_CH_OPERATE_R        = 0x03, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_BAT_CH_CANCEL_C         = 0x04, /*! BatteryCancelStr */
    MODULE_MSG_ID_CMD_BAT_CH_CANCEL_R         = 0x05, /*! ModReplyStr */

    /* Power Supplies Controller */
    MODULE_MSG_ID_CMD_PSC_CH_SELECT_C         = 0x06, /*! SupplySelectStr */
    MODULE_MSG_ID_CMD_PSC_CH_SELECT_R         = 0x07, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_PSC_CH_OPERATE_C        = 0x08, /*! SupplyOperateStr */
    MODULE_MSG_ID_CMD_PSC_CH_OPERATE_R        = 0x09, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_PSC_CH_CANCEL_C         = 0x0a, /*! SupplyCancelStr */
    MODULE_MSG_ID_CMD_PSC_CH_CANCEL_R         = 0x0b, /*! SupplyCancelReplyStr */

    /* Fan Test */
    MODULE_MSG_ID_CMD_FAN_CH_TEST_C           = 0x0c, /*! FanTestStr */
    MODULE_MSG_ID_CMD_FAN_CH_TEST_R           = 0x0d, /*! ModReplyStr */

    /* Switch Controller */
    MODULE_MSG_ID_CMD_SWC_CH_SELECT_C         = 0x0e, /*! SwitchSelectStr */
    MODULE_MSG_ID_CMD_SWC_CH_SELECT_R         = 0x0f, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_SWC_CH_OPERATE_C        = 0x10, /*! SwitchOperateStr */
    MODULE_MSG_ID_CMD_SWC_CH_OPERATE_R        = 0x11, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_SWC_CH_CANCEL_C         = 0x12, /*! SwitchCancelStr */
    MODULE_MSG_ID_CMD_SWC_CH_CANCEL_R         = 0x13, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_SWC_CH_SYNCH_C          = 0x14, /*! SwitchSynchStr */
    MODULE_MSG_ID_CMD_SWC_CH_SYNCH_R          = 0x15, /*! ModReplyStr */

    /* Aux Switch Controller */
    MODULE_MSG_ID_CMD_ASC_CH_SELECT_C         = 0x16, /*! AuxSwitchSelectStr */
    MODULE_MSG_ID_CMD_ASC_CH_SELECT_R         = 0x17, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_ASC_CH_OPERATE_C        = 0x18, /*! AuxSwitchOperateStr */
    MODULE_MSG_ID_CMD_ASC_CH_OPERATE_R        = 0x19, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_ASC_CH_CANCEL_C         = 0x1a, /*! AuxSwitchCancelStr */
    MODULE_MSG_ID_CMD_ASC_CH_CANCEL_R         = 0x1b, /*! ModReplyStr */

    /* HMI Controller */
    MODULE_MSG_ID_CMD_HMC_DISPLAY_OLR_C		  = 0x1c, /*! HmiDisplayOLRStr */
    MODULE_MSG_ID_CMD_HMC_DISPLAY_OLR_R		  = 0x1d, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_HMC_PLAY_SOUND_C		  = 0x1e, /*! HmiPlaySoundStr */
    MODULE_MSG_ID_CMD_HMC_PLAY_SOUND_R		  = 0x1f, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_HMC_GET_DISPLAY_INFO_C  = 0x20,
    MODULE_MSG_ID_CMD_HMC_GET_DISPLAY_INFO_R  = 0x21, /*! HmiDisplayInfoReplyStr */
    MODULE_MSG_ID_CMD_HMC_GET_BUTTON_STATE_C  = 0x22,
    MODULE_MSG_ID_CMD_HMC_GET_BUTTON_STATE_R  = 0x23, /*! HmiButtonStateReplyStr */
    MODULE_MSG_ID_CMD_HMC_LED_WRITE_C         = 0x24, /*! HmiLEDWriteStr */
    MODULE_MSG_ID_CMD_HMC_LED_WRITE_R         = 0x25, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_HMC_DISPLAY_WRITE_C     = 0x26, /*! HmiDisplayWriteStr */
    MODULE_MSG_ID_CMD_HMC_DISPLAY_WRITE_R     = 0x27, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_HMC_DISPLAY_CLEAR_C     = 0x28, /*! HmiDisplayClearStr */
    MODULE_MSG_ID_CMD_HMC_DISPLAY_CLEAR_R     = 0x29, /*! ModReplyStr */
    MODULE_MSG_ID_CMD_HMC_DISPLAY_SYNC_C      = 0x2a, /*! HmiDisplaySyncStr */
    MODULE_MSG_ID_CMD_HMC_DISPLAY_SYNC_R      = 0x2b, /*! ModReplyStr */

    /* Digital Output Controller */
	MODULE_MSG_ID_CMD_DOC_CH_SELECT_C         = 0x2c, /*! DigOutSelectStr */
	MODULE_MSG_ID_CMD_DOC_CH_SELECT_R         = 0x2d, /*! ModReplyStr */
	MODULE_MSG_ID_CMD_DOC_CH_OPERATE_C        = 0x2e, /*! DigOutOperateStr */
	MODULE_MSG_ID_CMD_DOC_CH_OPERATE_R        = 0x2f, /*! ModReplyStr */
	MODULE_MSG_ID_CMD_DOC_CH_CANCEL_C         = 0x30, /*! DigOutCancelStr */
	MODULE_MSG_ID_CMD_DOC_CH_CANCEL_R         = 0x31, /*! ModReplyStr */

	/* Battery Statistics Reset Command */
    MODULE_MSG_ID_CMD_BAT_CH_RESET_C         = 0x32, /*! BatteryResetStr */
    MODULE_MSG_ID_CMD_BAT_CH_RESET_R         = 0x33, /*! ModReplyStr */

	/* FPI controller */
    MODULE_MSG_ID_CMD_FPI_CH_OPERATE_C       = 0x34, /*! FPIOperateStr - Test / Reset */
    MODULE_MSG_ID_CMD_FPI_CH_OPERATE_R       = 0x35, /*! ModReplyStr */

    /* MODULE_MSG_TYPE_CFG */
    MODULE_MSG_ID_CFG_PSM_C                   = 0x00, /*! PSMConfigStr??? */
    MODULE_MSG_ID_CFG_PSM_R                   = 0x01, /*! ModReplyStr */
    MODULE_MSG_ID_CFG_BATTERY_CHARGER_C       = 0x02, /*! ChargerConfigStr */
    MODULE_MSG_ID_CFG_BATTERY_CHARGER_R       = 0x03, /*! ReplyStr */
    MODULE_MSG_ID_CFG_POWER_SUPPLIES_C        = 0x04, /*! SupplyConfigStr */
    MODULE_MSG_ID_CFG_POWER_SUPPLIES_R        = 0x05, /*! ModReplyStr */
    MODULE_MSG_ID_CFG_FAN_CONTROLLER_C        = 0x06, /*! FanConfigStr */
    MODULE_MSG_ID_CFG_FAN_CONTROLLER_R        = 0x07, /*! ModReplyStr */
    MODULE_MSG_ID_CFG_SWITCH_C        		  = 0x08, /*! SwitchConfigStr */
    MODULE_MSG_ID_CFG_SWITCH_R        		  = 0x09, /*! ModReplyStr */
    MODULE_MSG_ID_CFG_AUXSWITCH_C        	  = 0x0a, /*! AuxSwitchConfigStr */
    MODULE_MSG_ID_CFG_AUXSWITCH_R        	  = 0x0b, /*! ModReplyStr */
    MODULE_MSG_ID_CFG_HMI_CONTROLLER_C		  = 0x0c, /*! HMIConfigStr */
    MODULE_MSG_ID_CFG_HMI_CONTROLLER_R		  = 0x0d, /*! ModReplyStr */
    MODULE_MSG_ID_CFG_DIGITAL_OUT_C  		  = 0x0e, /*! DigOutConfigStr */
    MODULE_MSG_ID_CFG_DIGITAL_OUT_R	    	  = 0x0f, /*! ModReplyStr */
    MODULE_MSG_ID_CFG_SCM_C  		          = 0x10, /*! SCMConfigStr */
    MODULE_MSG_ID_CFG_SCM_R	    	          = 0x11, /*! ModStatusReplyStr */
    MODULE_MSG_ID_CFG_DUAL_SWITCH_C		      = 0x12, /*! DualSwitchConfigStr */
    MODULE_MSG_ID_CFG_DUAL_SWITCH_R		      = 0x13, /*! ModReplyStr */
    MODULE_MSG_ID_CFG_DSM_C					  = 0x14, /*! DSMConfigStr */
    MODULE_MSG_ID_CFG_DSM_R					  = 0x15, /*! ModReplyStr */
    MODULE_MSG_ID_CFG_FPM_C					  = 0x14, /*! FPMConfigStr */
    MODULE_MSG_ID_CFG_FPM_R					  = 0x15, /*! ModReplyStr */
    MODULE_MSG_ID_CFG_SCM_MK2_C				  = 0x16, /*! SCMMK2ConfigStr */
    MODULE_MSG_ID_CFG_SCM_MK2_R				  = 0x17, /*! ModReplyStr */
    MODULE_MSG_ID_CFG_SSM_C					  = 0x18, /*! SSMConfigStr */
    MODULE_MSG_ID_CFG_SSM_R					  = 0x19, /*! ModReplyStr */

    /* MODULE_MSG_TYPE_IOMAN - IOManager API */
    MODULE_MSG_ID_IOMAN_CFG_IOCHAN_AI_C       = 0x00, /*! ModuleCfgIoChanAIStr */
    MODULE_MSG_ID_IOMAN_CFG_IOCHAN_AI_R       = 0x01, /*! ModReplyStr */
    MODULE_MSG_ID_IOMAN_CFG_IOCHAN_DI_C       = 0x02, /*! ModuleCfgIoChanDIStr */
    MODULE_MSG_ID_IOMAN_CFG_IOCHAN_DI_R       = 0x03, /*! ModReplyStr */

    /* IOManager - Poll ioChan  */
    MODULE_MSG_ID_IOMAN_CMD_POLL_AI_CHAN_C	  = 0x04, /*! ModulePollChanIdStr */
    MODULE_MSG_ID_IOMAN_CMD_POLL_AI_CHAN_R	  = 0x05, /*! ModuleAIPollRspStr */
    MODULE_MSG_ID_IOMAN_CMD_POLL_DI_CHAN_C	  = 0x06, /*! ModulePollChanIdStr */
    MODULE_MSG_ID_IOMAN_CMD_POLL_DI_CHAN_R	  = 0x07, /*! ModuleDIPollRspStr */

    /* IOManager - Diagnostic commands */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_READ_IOID_C  = 0x08, /*! ModuleReadIoIdStr */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_READ_IOID_R  = 0x09, /*! ModuleReadIoIdRspStr */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_WRITE_IOID_C = 0x0a, /*! ModuleWriteIoIdStr */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_WRITE_IOID_R = 0x0b,
    MODULE_MSG_ID_IOMAN_CMD_DIAG_PULSE_IOID_C = 0x0c, /*! ModulePulseIoIdStr */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_PULSE_IOID_R = 0x0d,
    MODULE_MSG_ID_IOMAN_CMD_DIAG_FLASH_IOID_C = 0x0e, /*! ModuleFlashIoIdStr */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_FLASH_IOID_R = 0x0f,

    /* IOManager - Simulated mode */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_SET_SIM_C    = 0x10, /*! ModuleSetSimStr */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_SET_SIM_R    = 0x11, /*! ModStatusReplyStr */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_IOID_C   = 0x12, /*! ModuleSimIoIdStr */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_IOID_R   = 0x13, /*! ModuleSimIoIdRspStr */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_AI_CHAN_C= 0x14, /*! ModuleSimAIChanIdStr */
	MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_AI_CHAN_R= 0x15, /*! ModuleSimAIChanRspStr */
	MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_DI_CHAN_C= 0x16, /*! ModuleSimDIChanIdStr */
	MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_DI_CHAN_R= 0x17, /*! ModuleSimDIChanRspStr */

    MODULE_MSG_ID_IOMAN_CMD_DIAG_W_RAW_IOID_C = 0x18, /*! ModuleWriteRawIoIdStr */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_W_RAW_IOID_R = 0x19, /*! ModuleWriteRawRspStr */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_R_RAW_IOID_C = 0x1a, /*! ModuleReadRawIoIdStr */
    MODULE_MSG_ID_IOMAN_CMD_DIAG_R_RAW_IOID_R = 0x1b, /*! ModuleReadRawRspStr */


    /* IOManger - Design Testing ADE7878 */
//    MODULE_MSG_ID_IOMAN_CMD_ADE7878_WRITE_REG_C = 0x30, /*! ADE7878WriteRegStr */
//   MODULE_MSG_ID_IOMAN_CMD_ADE7878_WRITE_REG_R = 0x31, /*! */
//    MODULE_MSG_ID_IOMAN_CMD_ADE7878_READ_REG_C  = 0x32, /*! ADE7878ReadRegStr  */
//    MODULE_MSG_ID_IOMAN_CMD_ADE7878_READ_REG_R  = 0x33, /*! ADE7878ReadRspStr  */

    /* MODULE_MSG_TYPE_SYSALARM - System Alarm Log */
    MODULE_MSG_ID_SYSALARM_CLEAR_EVENTS_C     = 0x00,
    MODULE_MSG_ID_SYSALARM_CLEAR_EVENTS_R     = 0x01,
    MODULE_MSG_ID_SYSALARM_GET_FIRST_EVENT_C  = 0x02, /*! SysAlSeverityStr */
    MODULE_MSG_ID_SYSALARM_GET_FIRST_EVENT_R  = 0x03, /*! SysAlEventStatStr */
    MODULE_MSG_ID_SYSALARM_GET_NEXT_EVENT_C   = 0x04, /*! SysAlSeverityStr */
    MODULE_MSG_ID_SYSALARM_GET_NEXT_EVENT_R   = 0x05, /*! SysAlEventStatStr */

    /* MODULE_MSG_TYPE_CALTST - Calibration API */
    MODULE_MSG_ID_CALTST_CONVERT_VALUE_C    = 0x00, /*! CalTstConvValueStr    */
    MODULE_MSG_ID_CALTST_CONVERT_VALUE_R    = 0x01, /*! CalTstConvValueRspStr */
    MODULE_MSG_ID_CALTST_ERASE_ELEMENTS_C   = 0x02,
    MODULE_MSG_ID_CALTST_ERASE_ELEMENTS_R   = 0x03, /*! CalTstEraseRspStr */
    MODULE_MSG_ID_CALTST_ADD_ELEMENT_C      = 0x04, /*! CalDataHeaderStr + data */
    MODULE_MSG_ID_CALTST_ADD_ELEMENT_R      = 0x05, /*! CalTstAddElementRspStr */
    MODULE_MSG_ID_CALTST_READ_ELEMENT_C     = 0x06, /*! CalTstCGetDataStr */
    MODULE_MSG_ID_CALTST_READ_ELEMENT_R     = 0x07, /*! CalTstGetDataRspStr + data(optional) */
    MODULE_MSG_ID_CALTST_NVRAM_SELECT_C     = 0x08, /*! CalTstNvramSelStr    */
    MODULE_MSG_ID_CALTST_NVRAM_SELECT_R     = 0x09, /*! ModStatusReplyStr */

    /* MODULE_MSG_TYPE_NVRAM */
    MODULE_MSG_ID_NVRAM_UPDATE_BLK_C        = 0x00, /*! NVRAMCmdStr */
    MODULE_MSG_ID_NVRAM_UPDATE_BLK_R        = 0x01, /*! ModStatusReplyStr */
    MODULE_MSG_ID_NVRAM_WRITE_RAM_BLK_C     = 0x02, /*! NVRAMCmdStr + Data */
    MODULE_MSG_ID_NVRAM_WRITE_RAM_BLK_R     = 0x03, /*! ModStatusReplyStr */
    MODULE_MSG_ID_NVRAM_READ_RAM_BLK_C      = 0x04, /*! NVRAMCmdStr */
    MODULE_MSG_ID_NVRAM_READ_RAM_BLK_R      = 0x05, /*! data(optional) */
    MODULE_MSG_ID_NVRAM_FORCE_BAD_CRC_BLK_C = 0x06, /*! NVRAMCmdStr */
    MODULE_MSG_ID_NVRAM_FORCE_BAD_CRC_BLK_R = 0x07, /*! ModStatusReplyStr */

    /* MODULE_MSG_TYPE_BLTST */
    MODULE_MSG_ID_BLTST_GPIO_GET_PIN_MODE_C     = 0x00, /*! TestGPIOPinPortStr */
    MODULE_MSG_ID_BLTST_GPIO_GET_PIN_MODE_R     = 0x01, /*! TestGPIOModeStr */
    MODULE_MSG_ID_BLTST_GPIO_SET_PIN_MODE_C     = 0x02, /*! TestGPIOModeStr */
    MODULE_MSG_ID_BLTST_GPIO_SET_PIN_MODE_R     = 0x03,
    MODULE_MSG_ID_BLTST_GPIO_READ_PIN_C         = 0x04, /*! TestGPIOPinPortStr */
    MODULE_MSG_ID_BLTST_GPIO_READ_PIN_R         = 0x05, /*! TestGPIOReadStr */
    MODULE_MSG_ID_BLTST_GPIO_WRITE_PIN_C        = 0x06, /*! TestGPIOWriteStr */
    MODULE_MSG_ID_BLTST_GPIO_WRITE_PIN_R        = 0x07,
    MODULE_MSG_ID_BLTST_GPIO_READ_PORT_C        = 0x08, /*! TestGPIOPortStr */
    MODULE_MSG_ID_BLTST_GPIO_READ_PORT_R        = 0x09, /*! TestGPIOPortReadStr */
    MODULE_MSG_ID_BLTST_GPIO_WRITE_PORT_C       = 0x0a, /*! TestGPIOPortWriteStr */
    MODULE_MSG_ID_BLTST_GPIO_WRITE_PORT_R       = 0x0b,
    MODULE_MSG_ID_BLTST_ADC_READ_C              = 0x0c, /*! TestADCPinChanStr */
    MODULE_MSG_ID_BLTST_ADC_READ_R              = 0x0d, /*! TestADCReadStr */
    MODULE_MSG_ID_BLTST_SPI_DIGIPOT_WRITE_C     = 0x0e, /*! TestSPIDigiPotWriteStr */
    MODULE_MSG_ID_BLTST_SPI_DIGIPOT_WRITE_R     = 0x0f, /*! TestSPIDigiPotWriteRspStr */
    MODULE_MSG_ID_BLTST_SPI_DIGIPOT_READ_C      = 0x10, /*! TestSPIDigiPotReadStr */
    MODULE_MSG_ID_BLTST_SPI_DIGIPOT_READ_R      = 0x11, /*! TestSPIDigiPotReadRspStr */
    MODULE_MSG_ID_BLTST_I2C_ADC_MCP342X_READ_C  = 0x12, /*! TestI2CAdcMCP342XReadStr */
    MODULE_MSG_ID_BLTST_I2C_ADC_MCP342X_READ_R  = 0x13, /*! TestI2CAdcMCP342XReadRspStr */
    MODULE_MSG_ID_BLTST_DAC_WRITE_C             = 0x14, /*! TestDACStr */
    MODULE_MSG_ID_BLTST_DAC_WRITE_R             = 0x15, /*! TestErrorRspStr */
    MODULE_MSG_ID_BLTST_NVRAM_READ_C            = 0x16, /*! TestNVRAMReadStr */
	MODULE_MSG_ID_BLTST_NVRAM_READ_R            = 0x17, /*! TestNVRAMReadRspStr */
	MODULE_MSG_ID_BLTST_NVRAM_WRITE_C           = 0x18, /*! TestNVRAMWriteStr */
	MODULE_MSG_ID_BLTST_NVRAM_WRITE_R           = 0x19, /*! TestErrorRspStr */
	MODULE_MSG_ID_BLTST_SPI_ADC_READ_C          = 0x1a, /*! TestSpiAdcReadStr */
	MODULE_MSG_ID_BLTST_SPI_ADC_READ_R          = 0x1b, /*! TestSpiAdcReadRspStr */
	MODULE_MSG_ID_BLTST_I2C_EXP_READ_DDR_C      = 0x1c, /*! TestI2cExpDDRReadStr */
	MODULE_MSG_ID_BLTST_I2C_EXP_READ_DDR_R      = 0x1d, /*! TestI2cExpDDRReadRspStr */
	MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_DDR_C     = 0x1e, /*! TestI2cExpDDRWriteStr */
	MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_DDR_R     = 0x1f,
	MODULE_MSG_ID_BLTST_I2C_EXP_READ_PIN_C      = 0x20, /*! TestI2cExpPinReadStr */
	MODULE_MSG_ID_BLTST_I2C_EXP_READ_PIN_R      = 0x21, /*! TestI2cExpPinReadRspStr */
	MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_PIN_C     = 0x22, /*! TestI2cExpPinWriteStr */
	MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_PIN_R     = 0x23,
	MODULE_MSG_ID_BLTST_I2C_EXP_READ_PORT_C     = 0x24, /*! TestI2cExpPortReadStr */
	MODULE_MSG_ID_BLTST_I2C_EXP_READ_PORT_R     = 0x25, /*! TestI2cExpPortReadRspStr */
	MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_PORT_C    = 0x26, /*! TestI2cExpPortWriteStr */
	MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_PORT_R    = 0x27,
	MODULE_MSG_ID_BLTST_I2C_ADE78XX_INIT_C      = 0x28, /*! TestI2cADE78xxInitStr */
	MODULE_MSG_ID_BLTST_I2C_ADE78XX_INIT_R      = 0x29, /*! TestErrorRspStr */
	MODULE_MSG_ID_BLTST_I2C_ADE78XX_WRITE_C     = 0x2a, /*! TestI2cADE78xxWriteRegStr */
	MODULE_MSG_ID_BLTST_I2C_ADE78XX_WRITE_R     = 0x2b, /*! TestErrorRspStr */
	MODULE_MSG_ID_BLTST_I2C_ADE78XX_READ_C      = 0x2c, /*! TestI2cADE78xxReadStr */
	MODULE_MSG_ID_BLTST_I2C_ADE78XX_READ_R      = 0x2d, /*! TestI2cADE78xxReadRspStr */
	MODULE_MSG_ID_BLTST_SPI_AD_DIGPOT_WRITE_C   = 0x2f, /*! TestSPIDigiPotWriteStr */
	MODULE_MSG_ID_BLTST_SPI_AD_DIGPOT_WRITE_R   = 0x30, /*! TestSPIDigiPotWriteRspStr */
	MODULE_MSG_ID_BLTST_I2C_TEMP_READ_DEG_C     = 0x31, /*! TestI2cTempLM73ReadStr */
	MODULE_MSG_ID_BLTST_I2C_TEMP_READ_DEG_R     = 0x32, /*! TestI2cTempLM73ReadRspStr */
	MODULE_MSG_ID_BLTST_I2C_TEMP_READ_REG_C     = 0x33, /*! TestI2cTempLM73ReadRegStr */
	MODULE_MSG_ID_BLTST_I2C_TEMP_READ_REG_R     = 0x34, /*! TestI2cTempLM73ReadRspStr */
	MODULE_MSG_ID_BLTST_SPI_AD_DIGPOT_WRITEV2_C = 0x35, /*! TestSPIDigiPotWriteV2Str  */
	MODULE_MSG_ID_BLTST_SPI_AD_DIGPOT_WRITEV2_R = 0x36, /*! TestSPIDigiPotWriteRspStr */
	MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_C     = 0x37, /*! TestNVRAMReadBuffStr */
	MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_R     = 0x38, /*! data... */
	MODULE_MSG_ID_BLTST_NVRAM_WRITE_BUFFER_C    = 0x39, /*! TestNVRAMReadStr + data */
	MODULE_MSG_ID_BLTST_NVRAM_WRITE_BUFFER_R    = 0x3a, /*! TestErrorRspStr */
	MODULE_MSG_ID_BLTST_I2C_TEMP_WRITE_REG_C    = 0x3b, /*! TestI2cTempLM73RegAddrStr */
	MODULE_MSG_ID_BLTST_I2C_TEMP_WRITE_REG_R    = 0x3c, /*! TestErrorRspStr */

	/* MODULE_MSG_TYPE_BL */
	MODULE_MSG_ID_BL_WRITE_FIRMWARE_C         = 0x00, /*! BootloaderFirmwareStr */
	MODULE_MSG_ID_BL_WRITE_FIRMWARE_R         = 0x01, /*! ModStatusReplyStr */
	MODULE_MSG_ID_BL_START_APP_C              = 0x02, /*! */
	MODULE_MSG_ID_BL_START_APP_R              = 0x03, /*! ModStatusReplyStr */
	MODULE_MSG_ID_BL_READ_FIRMWARE_C          = 0x04, /*! BootloaderFirmwareReadStr */
	MODULE_MSG_ID_BL_READ_FIRMWARE_R          = 0x05, /*! BootloaderFirmwareBlkStr */
	MODULE_MSG_ID_BL_GET_FLASH_PARAMS_C       = 0x06, /*! */
	MODULE_MSG_ID_BL_GET_FLASH_PARAMS_R       = 0x07, /*! BootloaderFlashParamsReplyStr */
	MODULE_MSG_ID_BL_ERASE_FIRMWARE_C         = 0x08, /*! */
	MODULE_MSG_ID_BL_ERASE_FIRMWARE_R         = 0x09, /*! ModStatusReplyStr */
	MODULE_MSG_ID_BL_WRITE_PIC_FIRMWARE_C     = 0x0a, /*! BootloaderPICFirmwareStr */
	MODULE_MSG_ID_BL_WRITE_PIC_FIRMWARE_R     = 0x0b, /*! ModStatusReplyStr */

	/* MODULE_MSG_TYPE_BLTST_1 */
	MODULE_MSG_ID_BLTST_1_SSP_DMA_FEP_INIT_C   		= 0x00, /*! TestSSPDMAStr */
	MODULE_MSG_ID_BLTST_1_SSP_DMA_FEP_INIT_R   		= 0x01, /*! TestErrorRspStr */
	MODULE_MSG_ID_BLTST_1_SSP_FEP_READ_C       		= 0x02, /*! TestFEPReadStr */
	MODULE_MSG_ID_BLTST_1_SSP_FEP_READ_R       		= 0x03, /*! data... */
	MODULE_MSG_ID_BLTST_1_ADE78xx_ENABLE_C     		= 0x04, /*! TestADE78xxSPIEnableStr*/
	MODULE_MSG_ID_BLTST_1_ADE78xx_ENABLE_R     		= 0x05, /*! TestErrorRspStr*/
	MODULE_MSG_ID_BLTST_1_ADE78xx_READ_REG_C   		= 0x06, /*! TestADE78xxSPIReadRegStr*/
	MODULE_MSG_ID_BLTST_1_ADE78xx_READ_REG_R   		= 0x07, /*! data...*/
	MODULE_MSG_ID_BLTST_1_ADE78xx_WRITE_REG_C  		= 0x08, /*! TestADE78xxSPIWriteRegStr */
	MODULE_MSG_ID_BLTST_1_ADE78xx_WRITE_REG_R  		= 0x09, /*! TestErrorRspStr*/
	MODULE_MSG_ID_BLTST_1_MEM_READ_C           		= 0x0a,
	MODULE_MSG_ID_BLTST_1_MEM_READ_R           		= 0x0b,
	MODULE_MSG_ID_BLTST_1_MEM_WRITE_C          		= 0x0c,
	MODULE_MSG_ID_BLTST_1_MEM_WRITE_R          		= 0x0d,
	MODULE_MSG_ID_BLTST_1_I2C_ADE78xx_READ_PEAK_C	= 0x0e, /*! TestI2cADE78xxReadPeakStr */
	MODULE_MSG_ID_BLTST_1_I2C_ADE78xx_READ_PEAK_R	= 0x0f, /*! TestI2cADE78xxReadPeakRespStr */
	MODULE_MSG_ID_BLTST_1_LCD_WRITE_C				= 0x10, /*! TestLcdWriteStr */
	MODULE_MSG_ID_BLTST_1_LCD_WRITE_R				= 0x11, /*! TestErrorRspStr */
	MODULE_MSG_ID_BLTST_1_LCD_CMD_C			   		= 0x12, /*! TestLcdCmdStr */
	MODULE_MSG_ID_BLTST_1_LCD_CMD_R					= 0x13, /*! TestErrorRspStr */
	MODULE_MSG_ID_BLTST_1_I2C_HUMIDITY_READ_C       = 0x14, /*! TestI2cHumidityReadStr */
	MODULE_MSG_ID_BLTST_1_I2C_HUMIDITY_READ_R       = 0x15, /*! TestI2cTHumidityReadRspStr */

	/* MODULE_MSG_TYPE_LPRIO */
    MODULE_MSG_ID_LPRIO_HBEAT_M               = 0x00, /*! ModuleHBeatMStr */
    MODULE_MSG_ID_LPRIO_HBEAT_S               = 0x01, /*! ModuleHBeatSStr */
    MODULE_MSG_ID_LPRIO_MINFO_C               = 0x02,
    MODULE_MSG_ID_LPRIO_MINFO_R               = 0x03, /*! ModuleInfoStr */
    MODULE_MSG_ID_LPRIO_CAN_STATS_C           = 0x04,
    MODULE_MSG_ID_LPRIO_CAN_STATS_R           = 0x05, /*! CanStatsStr */
    MODULE_MSG_ID_LPRIO_SERVICE_INFO_C        = 0x06,
    MODULE_MSG_ID_LPRIO_SERVICE_INFO_R        = 0x07, /*!  */
    MODULE_MSG_ID_LPRIO_TIME_C                = 0x3C,
    MODULE_MSG_ID_LPRIO_TIME_R                = 0x3D, /*! CANTime */
    MODULE_MSG_ID_LPRIO_PING_C                = 0x3E,
    MODULE_MSG_ID_LPRIO_PING_R                = 0x3F
}MODULE_MSG_ID;


LU_DISABLE_WARNING(-pedantic)
/*!
 * Time format
 */
typedef struct ModuleTimeDef
{
    /*! Time slot */
    lu_uint16_t slot : 2 ;
    /*! time slot relative time. Resolution 100 us */
    lu_uint16_t time : 14;
}ModuleTimeStr;
LU_ENABLE_WARNING(-pedantic)

LU_DISABLE_WARNING(-pedantic)
/**
 * Module Unique Identification definition
 */
typedef struct ModuleUIDDef
{
    lu_uint32_t serialNumber: 25;   //Serial number
    lu_uint32_t supplierId: 7;      //Supplier ID
} ModuleUIDStr;

static const ModuleUIDStr DEFAULT_MODULE_UID = {0, 0};
LU_ENABLE_WARNING(-pedantic)


/* ======= MAIN MESSAGES ========= */

/*!
 * Resynch all slave modules
 */
typedef struct ModuleTsynchDef
{
    /*! New time slot to use for the time.
     * Valid range 0-3
     */
    lu_uint8_t timeSlot;
}ModuleTsynchStr;

#define HBEAT_MAXTIMECOUNTER 8  //max value for ModuleHBeatMStr.timeCounter field

LU_DISABLE_WARNING(-pedantic)
typedef struct ModuleHBeatMDef
{
    /*! Notify slave boards if power saving required
     */
    lu_uint8_t powerSaveMode    : 1;    // If set then then turn off LED's on slave modules
    lu_uint8_t bootloaderMode   : 1;    // If set then stay in bootloader mode
    lu_uint8_t timeCounter      : 3;    // counter increased every HBeat message is emitted
    lu_uint8_t factoryMode      : 1;    // If set while in bootloader mode, then enter factory mode
                                        // (Alternative to using the factory pin)
    lu_uint8_t unused           : 2;
}ModuleHBeatMStr;
LU_ENABLE_WARNING(-pedantic)


typedef struct ModuleHBeatSDef
{
    /*! Slave module status. See MODULE_BOARD_STATUS
     * for valid values
     */
    lu_uint8_t status;
    /*! Slave modules error codes (bitmap). See MODULE_BOARD_ERROR
     * for valid values
     */
    lu_uint8_t error;

    /*! Module Serial Number, used for CAN module detection & registration
     */
    ModuleUIDStr moduleUID;
    /* WARNING: please NEVER make this field bigger than a CAN frame!!!
     * It will be fragmented and MCMApp's CANEventManager does mix up the
     * fragmented messages when coming from different (concurrent) sources.
     */
}ModuleHBeatSStr;


/*!
 * API and Version structure
 */
typedef struct ModuleVersionAPIDef
{
	ModuleVersionStr 		 systemAPI;
	ModuleSoftwareVersionStr software;
}ModuleVersionAPIStr;


/*!
 * Module Information
 *
 * NOTE: fields here should reflect information that does not change during the
 *       module's running time.
 */
typedef struct ModuleInfoDef
{
    lu_uint8_t             moduleType     ;
    ModuleUIDStr           moduleUID;
    lu_uint32_t            svnRevisionBoot;
    lu_uint32_t            svnRevisionApp ;
    lu_uint32_t            systemArch; // SLAVE_BIN_ARCH
    ModuleVersionStr       featureRevision;
    ModuleVersionAPIStr    bootloader     ;
    ModuleVersionAPIStr    application    ;
}ModuleInfoStr;

/*!
 * CAN statistics
 */
typedef struct CanStatsDef
{
	lu_uint32_t canError;
	lu_uint32_t	canBusError;
	lu_uint32_t canArbitrationError;
	lu_uint32_t canDataOverrun;
	lu_uint32_t canBusOff;
	lu_uint32_t canErrorPassive;
	lu_uint32_t canErrorBTR;    //deprecated
	lu_uint32_t canTxCount; // TXTotal
	lu_uint32_t canRxCount; // RXMessageTotal
	lu_uint32_t canRxLost; // RXMessageLost

}CanStatsStr;

/*!
 * Diag Read ioID request
 */
typedef struct ModuleReadIoIdDef
{
    /*! io ID */
    lu_uint16_t    ioID;
}ModuleReadIoIdStr;

/*!
 * Diag Read ioID request response
 */
typedef struct ModuleReadIoIdRspDef
{
    /*! ioID value */
    lu_int32_t     value;
    /*! ioID raw value */
    lu_int32_t     rawValue;
}ModuleReadIoIdRspStr;

/*!
 * Diag Write ioID request
 */
typedef struct ModuleWriteIoIdDef
{
    /*! io ID */
    lu_uint16_t    ioID;
    lu_int32_t     value;
}ModuleWriteIoIdStr;

/*!
 * Diag Pulse ioID request
 */
typedef struct ModulePulseIoIdDef
{
    /*! io ID */
    lu_uint16_t    ioID;
    lu_int16_t     timeMs;
}ModulePulseIoIdStr;

/*!
 * Diag Flash ioID request
 */
typedef struct ModuleFlashIoIdDef
{
    /*! io ID */
    lu_uint16_t    ioID;
    lu_int16_t     timeMs;
}ModuleFlashIoIdStr;


LU_DISABLE_WARNING(-pedantic)

/*!
 * Control IOManager simulation mode - to allow input ioID's to be controlled with simulated values.
 */
typedef struct ModuleSetSimDef
{
	lu_uint8_t	enableSimulationIO : 1;
	lu_uint8_t  unused : 7;
}ModuleSetSimStr;


/*!
 * Set/unset simulation mode for an ioID to allow override of input value.
 * (when simulated the last value is used and input is disconnected from physical input and becomes virtual)
 */
typedef struct ModuleSimIoIdDef
{
	lu_uint16_t    ioID;
	
	lu_uint8_t enableSimulation : 1;
	lu_uint8_t unused : 7;
}ModuleSimIoIdStr;


/*!
 * Set/unset simulation mode for an ioID to allow override of input value.
 * (when simulated the last value is used and input is disconnected from physical input and becomes virtual)
 */
typedef struct ModuleSimAIChanIdDef
{
	lu_uint16_t    ioChan;

	lu_uint8_t enableSimulation : 1;
	lu_uint8_t unused : 7;
}ModuleSimAIChanIdStr;

/*!
 * Set/unset simulation mode for an ioID to allow override of input value.
 * (when simulated the last value is used and input is disconnected from physical input and becomes virtual)
 */
typedef struct ModuleSimDIChanIdDef
{
	lu_uint16_t    ioChan;

	lu_uint8_t enableSimulation : 1;
	lu_uint8_t unused : 7;
}ModuleSimDIChanIdStr;

LU_ENABLE_WARNING(-pedantic)

/*!
 * 
 */
typedef struct ModuleSimIoIdRspDef
{
	lu_uint16_t    ioID;

	/*! Status - Uses REPLY_STATUS enum values */
	lu_uint8_t		status;
}ModuleSimIoIdRspStr;

/*!
 *
 */
typedef struct ModuleSimAIChanRspDef
{
	lu_uint16_t    ioChan;

	/*! Status - Uses REPLY_STATUS enum values */
	lu_uint8_t		status;
}ModuleSimAIChanRspStr;

/*!
 *
 */
typedef struct ModuleSimDIChanRspDef
{
	lu_uint16_t    ioChan;

	/*! Status - Uses REPLY_STATUS enum values */
	lu_uint8_t		status;
}ModuleSimDIChanRspStr;

/*!
 * Channel ID poll request
 */
typedef struct ModulePollChanIdDef
{
    /*! Channel ID */
    lu_uint8_t    channel;
}ModulePollChanIdStr;

/*!
 * Poll Digital
 */
typedef struct ModuleDIPollRspDef
{
    /*! Channel ID */
    lu_uint8_t    channel;
    /*! Channel value */
    lu_int8_t     value;
    /*! Channel raw value */
    lu_int8_t     rawValue;
    /*! isOnline - LU_TRUE or LU_FALSE */
	lu_uint8_t    isOnline : 1;
	lu_uint8_t    simulated : 1; // If LU_TRUE then values is forced simulation */
	lu_uint8_t    UNUSED : 6;
}ModuleDIPollRspStr;

/*!
 * poll Analogue
 */
typedef struct ModuleAIPollRspDef
{
    /*! Channel ID */
    lu_uint8_t    channel;
    /*! Channel value */
    lu_int32_t    value;
    /*! isOnline - LU_TRUE or LU_FALSE */
	lu_uint8_t    isOnline : 1;
	lu_uint8_t    simulated : 1; // If LU_TRUE then values is forced simulation */
	lu_uint8_t    UNUSED : 6;
}ModuleAIPollRspStr;


LU_DISABLE_WARNING(-pedantic)
/*!
 * Configure IOManager Analogue Channel
 */
typedef struct ModuleCfgIoChanAIDef
{
	/*! Channel ID */
	lu_uint8_t    channel;
	lu_uint8_t	  enable : 1;
	lu_uint8_t	  eventEnable :1;
	lu_uint16_t   eventMs;
}ModuleCfgIoChanAIStr;


/*!
 * Configure IOManager Digital Channel
 */
typedef struct ModuleCfgIoChanDIDef
{
	/*! Channel ID */
	lu_uint8_t    channel;
	lu_uint8_t	  enable : 1;
	lu_uint8_t	  extEquipInvert : 1;
	lu_uint8_t	  eventEnable : 1;
	lu_uint16_t   dbHigh2LowMs;
	lu_uint16_t   dbLow2HighMs;
}ModuleCfgIoChanDIStr;


/*!
 * Digital event
 */
typedef struct ModuleDEventDef
{
    /*! Channel ID */
    lu_uint8_t    channel;
    /*! isOnline - LU_TRUE or LU_FALSE */
	lu_uint8_t    isOnline : 1;
	lu_uint8_t    simulated : 1; // If LU_TRUE then values is forced simulation */
	lu_uint8_t    UNUSED : 6;
    /*! Channel value */
    lu_int8_t     value  ;
    /*! Event timestamp */
    ModuleTimeStr time   ;
}ModuleDEventStr;

/*!
 * Analogue event
 */
typedef struct ModuleAEventDef
{
    /*! Channel ID */
    lu_uint8_t    channel;
    /*! isOnline - LU_TRUE or LU_FALSE */
    lu_uint8_t    isOnline : 1;
    lu_uint8_t    simulated : 1; // If LU_TRUE then values is forced simulation */
    lu_uint8_t    UNUSED : 6;
    /*! Channel value */
    lu_int32_t    value  ;
    /*! Event timestamp */
    ModuleTimeStr time   ;
}ModuleAEventStr;
LU_ENABLE_WARNING(-pedantic)

/*!
 * Module restart parameters
 */
typedef struct MDCmdRestartDef
{
    /*! Use MD_RESTART enum */
    lu_uint8_t type;
}MDCmdRestartStr;

/*!
 * Module restart parameters
 */
typedef struct MDCmdStartModuleDef
{
    /*! LU_TRUE or LU_FALSE */
    lu_uint8_t watchdogEnable;
}MDCmdStartModuleStr;

/*!
 * Module reply
 */
typedef struct SysAlEventStatDef
{
	/*! Status - Uses REPLY_STATUS_OKAY or REPLY_STATUS_NO_EVENTS enum values */
	lu_uint8_t		status;

	lu_uint8_t		state;     /* Bit fields decode with SYS_ALARM_STATE */
	lu_uint8_t		subSystem; /* SYS_ALARM_SUBSYSTEM_* */
	lu_uint32_t		alarmCode; /* Subsystem specific alarm codes e.g. SYSALC_SYSTEM_* */
	lu_uint16_t		parameter; /* additional debug info */
} SysAlEventStatStr;

/*!
 * Module reply
 */
typedef struct SysAlSeverityDef
{
	lu_uint8_t severity; /* SYS_ALARM_SEVERITY_* */
} SysAlSeverityStr;

/*!
 * Module reply
 */
typedef struct ModReplyDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Status - Uses REPLY_STATUS enum values */
	lu_uint8_t		status;
}ModReplyStr;

typedef struct ModStatusReplyDef
{
	/*! Status - Uses REPLY_STATUS enum values */
	lu_uint8_t		status;
}ModStatusReplyStr;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

#pragma pack(pop)

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

#ifdef __cplusplus
}
#endif

#endif /* _MODULE_PROTOCOL_INCLUDED */

/*
 *********************** End of file ******************************************
 */
