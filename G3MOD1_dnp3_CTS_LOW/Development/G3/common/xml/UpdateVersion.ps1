# To run this script, run "Set-ExecutionPolicy RemoteSigned" in PowerShell first.
# Alternatively, you can run related batch file "UpdateVersion.bat"

$scriptName=$MyInvocation.MyCommand.Name

function setVersion($versionName) 
{
	$xml_path="/sourceFile/define[@name=""$versionName""]"
	
	echo "$versionName ==> ${version_type}-${version_major}.${version_minor}.${version_patch}"
	
	$xml_select= $doc.SelectNodes("${xml_path}/item[@name=""${versionName}_RELTYPE""]")
	$xml_select.Item(0).value="${version_type}";
		
	$xml_select= $doc.SelectNodes("${xml_path}/item[@name=""${versionName}_MAJOR""]")
	$xml_select.Item(0).value="${version_major}";
		
	$xml_select= $doc.SelectNodes("${xml_path}/item[@name=""${versionName}_MINOR""]")
	$xml_select.Item(0).value="${version_minor}";
	
	$xml_select= $doc.SelectNodes("${xml_path}/item[@name=""${versionName}_PATCH""]")
	$xml_select.Item(0).value="${version_patch}";
}

echo "Running [$scriptName]..."


# Initialise
$xmlFile="versions.xml"
$script:version_type=Read-Host 'What is the release type[RELEASE, BETA, DEBUG, TEST]?'
$script:version_type="VERSION_TYPE_${version_type}"
$script:version_major=Read-Host 'What is the major?'
$script:version_minor=Read-Host 'What is the minor?'
$script:version_patch=Read-Host 'What is the patch?'

# Read from file
[xml]$script:doc = Get-Content -Path $xmlFile

echo "Updating RTU software version..."
setVersion "MCM_SOFTWARE_VERSION"
setVersion "MCM_MONITOR_SOFTWARE_VERSION"
setVersion "MCM_UPDATER_SOFTWARE_VERSION"
setVersion "MCM_CONSOLE_SOFTWARE_VERSION"
setVersion "SCM_SOFTWARE_VERSION"
setVersion "FDM_SOFTWARE_VERSION"
setVersion "PSM_SOFTWARE_VERSION"
setVersion "IOM_SOFTWARE_VERSION"
setVersion "HMI_SOFTWARE_VERSION"
setVersion "DSM_SOFTWARE_VERSION"
setVersion "FPM_SOFTWARE_VERSION"
setVersion "SSM_SOFTWARE_VERSION"
setVersion "BOOTLOADER_SOFTWARE_VERSION"
setVersion "CONFIGTOOL_SOFTWARE_VERSION"

#echo "Updating ConfigTool software version..."
cd ..\..\RTUTools\G3ConfigTool\
#mvn --batch-mode release:update-versions -DdevelopmentVersion=${version_major}.${version_minor}.${version_patch}
#mvn release:clean release:prepare --batch-mode -DdevelopmentVersion=${version_major}.${version_minor}.${version_patch} -DdryRun=true
mvn versions:set "-DnewVersion=${version_major}.${version_minor}.${version_patch}"
mvn versions:commit
cd ..\..\common\xml

# Save to file
$doc.Save($xmlFile)
