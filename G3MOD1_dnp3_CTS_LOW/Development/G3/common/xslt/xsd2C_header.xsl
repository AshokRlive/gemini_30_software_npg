<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
  <xsl:param name="inputXSD" />
  <xsl:param name="inputXSD" />
  <xsl:strip-space elements="*"/>
  <xsl:output method="text" encoding="utf-8" media-type="text/plain" indent="no"/>

  <!-- =========== common functions ============ -->

  <!-- +++ CRLF adds a carriage return +++ -->
  <xsl:template name="CRLF">
<xsl:text>
</xsl:text>
  </xsl:template>

  
  <!-- +++ Indent adds 4 indentation spaces +++ -->
  <xsl:template name="Indent">
    <xsl:text>    </xsl:text>
  </xsl:template>


  <!-- +++ SectionText adds a section header text +++ -->
  <xsl:template name="SectionText">
    <xsl:param name="sectionName" />
<xsl:text>
/*
 ******************************************************************************
 * </xsl:text>
<xsl:value-of select="$sectionName" />
 <xsl:text>
 ******************************************************************************
 */

</xsl:text>
  </xsl:template>


  <!-- +++ CommentOut puts a content between C comment symbols +++ -->
  <xsl:template name="CommentOut">
    <xsl:param name="contentText" />
    <xsl:text>/* </xsl:text>
      <xsl:value-of select="$contentText"/>
    <xsl:text> */</xsl:text>
  </xsl:template>


  <!-- +++ BriefComment puts a content in a \brief style comment +++ -->
  <xsl:template name="BriefComment">
    <xsl:param name="contentText" />
    <xsl:text>/**</xsl:text><xsl:call-template name="CRLF"/>
    <xsl:text> * \brief </xsl:text>
    <xsl:value-of select="$contentText"/><xsl:call-template name="CRLF"/>
    <xsl:text> */</xsl:text>
    <xsl:call-template name="CRLF"/>
  </xsl:template>


  <!-- +++ GenSpaces it's a recursive template for generating strings made of N spaces +++ -->
  <xsl:template name="GenSpaces">
    <xsl:param name="amount" />
    <xsl:if test="$amount &gt; 0">
        <xsl:text> </xsl:text>
        <xsl:call-template name="GenSpaces">
            <xsl:with-param name="amount" select="$amount - 1" />
        </xsl:call-template>
    </xsl:if>
  </xsl:template>


  <!-- +++ WarnAutogenerated puts a comment with a warning +++ -->
  <xsl:template name="WarnAutogenerated">
    <xsl:text>/* ...Autogenerated from "</xsl:text>
    <xsl:value-of select="$inputXSD"/>
    <xsl:text>" using "xml2C_header.xsl" template -- DO NOT EDIT HERE!!! */</xsl:text>
    <xsl:call-template name="CRLF"/>
  </xsl:template>





  <!-- ============================== General parsing ================================ -->

  <xsl:template name="firstPass" match="*[local-name() = 'schema']">
  <xsl:text>
/* 
 * Autogenerated file from "</xsl:text>
    <xsl:value-of select="$inputXSD"/>
    <xsl:text>" using "xsd2C_header.xsl" as Template.</xsl:text> 
 * DO NOT EDIT THIS FILE!!!
 */
  <xsl:call-template name="CRLF"/>
  
  <!-- **************************** ADD GUARDS ************************* -->
  <xsl:variable name="guardName" select='translate(concat("_", $inputXSD, "_INCLUDED"),
                                         "abcdefghijklmnopqrstuvwxyz.", "ABCDEFGHIJKLMNOPQRSTUVWXYZ_"
                                         )'/>
  <xsl:text>#ifndef </xsl:text><xsl:value-of select="$guardName"/><xsl:call-template name="CRLF"/>
  <xsl:text>#define </xsl:text><xsl:value-of select="$guardName"/><xsl:call-template name="CRLF"/>
  <xsl:call-template name="CRLF"/>

  <!-- ********************** GENERATE LOGICPOINTS ********************* -->
  <xsl:call-template name="SectionText">
    <xsl:with-param name="sectionName" select="'EXPORTED - Enums'"/>
  </xsl:call-template>

  <xsl:for-each select="*[local-name() = 'simpleType']">
    <xsl:if test="@name='LogicPointType'">
      <xsl:variable name="enumName" select='concat("LOGICPOINT", "_TYPE")'/>
      <xsl:call-template name="WarnAutogenerated"/>
      <xsl:text>typedef enum</xsl:text><xsl:call-template name="CRLF"/>
      <xsl:text>{</xsl:text><xsl:call-template name="CRLF"/>

      <xsl:for-each select="*[local-name() = 'restriction']/*[local-name() = 'enumeration']">
        <xsl:variable name="cur" select='position()-1'/>

        <xsl:call-template name="Indent"/>
        <xsl:value-of select='translate(concat($enumName, "_", @value),
                                         "abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                         )'/>
        <xsl:text> = </xsl:text><xsl:value-of select="$cur"/><xsl:text>, </xsl:text>
        <xsl:call-template name="Indent"/>
        <xsl:call-template name="CommentOut">
          <xsl:with-param name="contentText" select="@value"/>
        </xsl:call-template>
        <xsl:if test ="position() != last()">
          <xsl:call-template name="CRLF"/>
        </xsl:if>
      </xsl:for-each>
      <xsl:call-template name="CRLF"/>
      <xsl:call-template name="Indent"/>
      <xsl:value-of select='translate(concat($enumName, "_LAST"), "abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ")' />
      <xsl:call-template name="CRLF"/>
      <xsl:text>} </xsl:text><xsl:value-of select="$enumName"/>;<xsl:call-template name="CRLF"/>
      <xsl:call-template name="CRLF"/>
    </xsl:if>
  </xsl:for-each>
#endif /* <xsl:value-of select="$guardName"/> */

/*
 *********************** End of file ******************************************
 */
</xsl:template>
</xsl:stylesheet> 
