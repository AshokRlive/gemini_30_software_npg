<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/XSL/Transform">
	<xsl:strip-space elements="*"/>
	<xsl:output method="xml" encoding="utf-8" media-type="text/plain" indent="yes"/>
	<xsl:template match="sourceFile">
		
		<xsl:comment> Autogenerated from XML file in g3/common/xml/ by "xml2xsd.xsl". - DO NOT MODIFY</xsl:comment>

		<!--Autogenerated from XML file. - DO NOT MODIFY-->
		<ns_xs:schema xmlns:ns_xs="http://www.w3.org/2001/XMLSchema"  elementFormDefault="unqualified" attributeFormDefault="unqualified" >			
			<xsl:attribute name="targetNamespace">generated.<xsl:value-of select="@classname"/></xsl:attribute>			
			<!--<xsl:attribute name="targetNamespace">generatedEnum</xsl:attribute>-->							
			<!--<xsl:namespace name="">generated.<xsl:value-of select="@classname"/></xsl:namespace>-->
			<xsl:for-each select="enum">
				<ns_xs:simpleType>
					<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
					<ns_xs:restriction base="ns_xs:string">
						<xsl:for-each select="item">
							<ns_xs:enumeration>
							<xsl:attribute name="value"><xsl:value-of select="@name"/></xsl:attribute>
							</ns_xs:enumeration>	
						</xsl:for-each>
					</ns_xs:restriction>
				</ns_xs:simpleType>
			</xsl:for-each>
		</ns_xs:schema>
	</xsl:template>
</xsl:stylesheet>
