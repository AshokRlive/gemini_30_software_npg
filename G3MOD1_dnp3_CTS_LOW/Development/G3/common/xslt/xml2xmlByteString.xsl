<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:strip-space elements="*"/>
  <xsl:output indent="yes"/>
  
  <!-- *** 
        This transformation takes all the structures in the input file, and 
        generates an XML file as an output with the format:
            <?xml version="1.0"?>
            <struct name="StructureNameStr">
              <Item type="T1" name="fieldName" value="defaultValue(IfDefined)"/>
              <Item isBitField="true" type="T2" name="bitField">
                <bitField name="rx" bit="2" value="DEFINITE_VALUE_B"/>
                <bitField name="unused" bit="5" length="3"/>
              </Item>
              <Item type="lu_uint32_t" name="patch"/>
            </struct>
        All the "default" attributes in the input file will be renamed to "value", 
        and the structure's "type" attribute (e.g.: XxxStr) will become the 
        structure's "name" attribute.
        Apart from these, the output format will only keep Item's "name", "type", 
        "dim" and "isBitField" attributes, and bitField's "name", "bit", "length" 
        attributes.
    *** -->

  <!-- first copy all nodes and later remove all uninteresting elements -->
  <xsl:template match="@*|node()" >
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>
  
  <!-- move interested node to root by removing "sourceFile" node -->
  <xsl:template match="sourceFile" >
    <xsl:apply-templates select="@*|node()" />    
  </xsl:template>

  <!-- remove all nodes that aren't a struct -->
  <xsl:template match="/sourceFile/*[not(self::struct)]" />
 
  <!-- rename "default" attribute to "value" -->
  <xsl:template match="@default" >
    <xsl:attribute name="value">
      <xsl:value-of select="." />
    </xsl:attribute>
  </xsl:template>

  <!-- rename "type" attribute to "name" in the struct only (not on Item or others) -->
  <xsl:template match="struct/@type" >
    <xsl:attribute name="name">
      <xsl:value-of select="." />
    </xsl:attribute>
  </xsl:template>
  
  <!-- remove unneeded attributes, nodes, and others -->
  <xsl:template match="@comment|@description|@packed|value" />
  <xsl:template match="text()|comment()|processing-instruction()" />
      
</xsl:stylesheet> 
