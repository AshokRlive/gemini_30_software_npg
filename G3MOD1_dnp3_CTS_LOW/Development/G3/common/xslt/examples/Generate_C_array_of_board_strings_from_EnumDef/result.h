
/* 
 * Autogenerated file by "xml2C_boardArray.xsl" from an XML source file!!!
 *
 * (DO NOT EDIT)
 *
 */
 
 /*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
 
 #include "lu_types.h"
 

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/* ...Autogenerated by "xml2C_boardArray.xsl" -- DO NOT EDIT!!! */
const lu_uint8_t *BoardIOID[] = 
{
    "IO_ID_CAN1_RX0",          // 0    
    "IO_ID_CAN1_TX",           // 1    
    /* UART Debug group */
    "IO_ID_UART_DEBUG_TX",     // 2        /* Debug TX */
    "IO_ID_UART_DEBUG_RX",     // 3    
    "IO_ID_LED_SCL",           // 4    
    "IO_ID_LED_SDA",           // 5    
    "IO_ID_I2C1_CLK",          // 6        /* Clock signal */
    "IO_ID_I2C1_SDA"           // 7    
}; 


/*
 *********************** End of file ******************************************
 */
