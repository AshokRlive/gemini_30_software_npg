/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _CMD_PARSER_INCLUDED
#define _CMD_PARSER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Main.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define ID_NVRAM_ADDRESS            0x14
#define APP_NVRAM_ADDRESS           0x15

#define BATT_ID_NVRAM_ADDRESS	    0x14
#define BATT_DATA_NVRAM_ADDRESS     0x15

#define BATT_ID_NVRAM_I2C_CHANNEL   0x00
#define BATT_DATA_NVRAM_I2C_CHANNEL 0x00

#define ID_NVRAM_I2C_CHANNEL        0x01
#define APP_NVRAM_I2C_CHANNEL       0x01

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef enum
{
	PARSER_FLAG_NONE                   = 0x00,
	PARSER_FLAG_FIRMWARE_DOWNLOAD      ,
	PARSER_FLAG_FIRMWARE_READ          ,
	PARSER_FLAG_FIRMWARE_VERIFY        ,
	PARSER_FLAG_FIRMWARE_ERASE         ,
	PARSER_FLAG_CAL_ADD_ELEMENT        ,
	PARSER_FLAG_CAL_READ_ELEMENT       ,
	PARSER_FLAG_SINGLE_COMMAND         ,
	PARSER_FLAG_NVRAM_READ_FACTORY     ,
	PARSER_FLAG_NVRAM_WRITE_FACTORY    ,
	PARSER_FLAG_NVRAM_READ_PSM_OPTS    ,
	PARSER_FLAG_NVRAM_WRITE_PSM_OPTS   ,
	PARSER_FLAG_NVRAM_READ_BATT_OPTS   ,
	PARSER_FLAG_NVRAM_WRITE_BATT_OPTS  ,
	PARSER_FLAG_SINGLE_COMMAND_LOOP    ,
	PARSER_FLAG_PIC_FIRMWARE_DOWNLOAD  ,
	PARSER_FLAG_CAL_BLOCK_WRITE        ,
	PARSER_FLAG_CAL_BLOCK_READ		   , 
	PARSER_FLAG_NVRAM_WRITE_VERIFY	   ,
	PARSER_FLAG_NVRAM_READ_HMI_OPTS    ,
	PARSER_FLAG_NVRAM_WRITE_HMI_OPTS   ,
	PARSER_FLAG_LAST
}PARSER_FLAG;

typedef enum
{
	COM_MODE_CAN = 0,
	COM_MODE_ETH = 1,
	COM_MODE_LAST
}COM_MODE;

typedef struct ParsingArgsDef
{
	lu_uint8_t  msgBuff[MODULE_MESSAGE_LENGTH];
	lu_uint32_t msgBuffLen;
	CANHeaderStr CANHeader;
	lu_uint8_t sourceFileName[800];
	lu_uint8_t destinationFileName[800];
	lu_int8_t CalElementFileName[800];
	lu_uint8_t calID;
	lu_uint8_t calType;
	lu_uint16_t calLength;
	lu_uint8_t nvramAddr;
	lu_uint8_t nvramI2CChannel;
	lu_uint16_t runInterval;
	lu_uint16_t numberOfRun;
	PARSER_FLAG parserFlag;
	COM_MODE    comMode;
}ParsingArgsStr;

typedef struct FileInfoDef
{
	FILE *filePtr;
	lu_uint32_t fileSize;
	lu_uint32_t numberOfBlocks;
	lu_uint32_t lastBlockSize;
}FileInfoStr;
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
ParsingArgsStr argsFromCommand;
ParsingArgsStr *flagDecoderPtr;

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern void CmdParserUsage(void);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern void CmdParser(int argc, char* argv[], ParsingArgsStr *argsFromCommand);
extern void CmdParserDll(	 MODULE module, 
							 MODULE_ID moduleId,
							 MODULE_MSG_TYPE msgType,
							 MODULE_MSG_ID msgId,
							 lu_uint8_t frag,
							 lu_uint8_t msgBuff[],
							 lu_uint16_t msgLen
						);

#endif /* _CMD_PARSER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
