/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   31/07/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "CmdParser.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define BLOCK_OFFSET_SIZE    sizeof(lu_int16_t)
#define BLOCK_256            0x100
#define FIRMWARE_BLOCK_SIZE  ( BLOCK_256 + BLOCK_OFFSET_SIZE )

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
FileInfoStr firmwareFile;
/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

SB_ERROR FirmwareDownloaderFilePrepare(ParsingArgsStr *argsFromCommand);
lu_bool_t FirmwareDownloaderBlockSend(FileInfoStr *firmwareFilePtr, ParsingArgsStr *argsFromCommand);
SB_ERROR LucyCANSendFirmwareDownloaderProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

/*
 *********************** End of file ******************************************
 */