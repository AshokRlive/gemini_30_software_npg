/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: NvramFactoryInfoWrite.h 3512 2013-07-08 11:35:05Z saravanan_v $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/test/protocol/CanKing/LucyCanSend/include/NvramFactoryInfoWrite.h $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/07/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "CmdParser.h"
#include "roxml.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define FACTORYINFO_FIELD_MAX       17
#define PSM_OPTIONS_FIELD_MAX        6
#define BATTERY_OPTIONS_FIELD_MAX   27
#define HMI_OPTIONS_FIELD_MAX	     4

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef struct factoryInfoBlkDef
{
	TestNVRAMReadBuffStr nvRamAddr;
	NVRAMBlkHeadStr factoryBlkHeader;
	NVRAMInfoStr factoryInfoData;
}factoryInfoBlkStr;

typedef struct psmOptionsBlkDef
{
	TestNVRAMReadBuffStr nvRamAddr;
	NVRAMBlkHeadStr factoryBlkHeader;
	NVRAMOptPSMStr psmOptions;
}psmOptionsBlkStr;

typedef struct hmiOptionsBlkDef
{
	TestNVRAMReadBuffStr nvRamAddr;
	NVRAMBlkHeadStr factoryBlkHeader;
	NVRAMOptHMIStr hmiOptions;
}hmiOptionsBlkStr;

typedef struct batteryOptionsBlkDef
{
	TestNVRAMReadBuffStr  nvRamAddr;
	NVRAMBlkHeadStr   factoryBlkHeader;
	NVRAMDefOptBatStr batteryOptions;
}batteryOptionsBlkStr;

typedef struct xmlInfoDef
{
	lu_int8_t   *xmlInfoType;
	lu_int8_t   *xmlInfoName;
	lu_int8_t   *xmlInfoDefault;
	lu_int8_t   *xmlInfoDim;
	lu_int8_t   *xmlInfoValue;
	lu_int8_t   *xmlInfoDescription;
}xmlInfoStr;

typedef struct strutureMapDef
{
	void *elementPtr;
	lu_uint8_t size;
}strutureMapStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
const xmlInfoStr factoryInfoMap[FACTORYINFO_FIELD_MAX];
const xmlInfoStr psmOptionsMap[PSM_OPTIONS_FIELD_MAX];
const xmlInfoStr hmiOptionsMap[HMI_OPTIONS_FIELD_MAX];
const xmlInfoStr batteryOptionsMap[BATTERY_OPTIONS_FIELD_MAX];


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

void ClearNVRamBuffer(lu_uint8_t buffer[], lu_uint8_t size);

/*!
 ******************************************************************************
 *   \brief Fetching data from Factory Information XML file
 *
 *   Fetching data from factory information from XML file and store into factoryinfo 
 *   structure to send to slave board's ID memory.
 *  
 *   \param  XMLfilename         File name of the XML file 
 *   \param  factoryInfoFromXML  Pointer to the structure of factoryInformation
 *
 *   \return SB_ERROR     
 *
 ******************************************************************************
 */
SB_ERROR FetchAndPackFactoryInfoFromXML( lu_uint8_t*            XMLfilename,
										 lu_uint8_t             numberOfTags,
										 const xmlInfoStr*            dataFromXML,
										 const strutureMapStr*        strTable
									   );


SB_ERROR BundleNVRAMDataToXMLFile( lu_uint8_t *readBuffer,
								   lu_int8_t  *fileName,
								   lu_uint8_t numberOfItemTags,
								   const strutureMapStr *elementMapStrPtr,
								   const xmlInfoStr *xmlTagMapStrPtr
								 );
/*!
 ******************************************************************************
 *   \brief To get values from single XML tag
 *
 *   To get value from XML tag and store it in correcsponding field of factory
 *   information structure
 *   
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR StripAndGetValueFromXMLTag( node_t* XMLTag, 
									 void *valueFromXMLTag, 
									 xmlInfoStr  xmlMapElement,
									 lu_uint8_t  mapIndex
								   );
/*
 *********************** End of file ******************************************
 */