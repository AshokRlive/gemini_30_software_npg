/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _CAN_CODEC_INCLUDED
#define _CAN_CODEC_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include <windows.h>
#include "lu_types.h"
#include "errorCodes.h"

#include "ModuleProtocol.h"
#include "CANProtocolFraming.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */



/*!
 * \brief CAN Protocol custom decode function.
 *
 * This function is called by the CANCDecode function.
 *
 * \param msgPtr CAN message
 * \param time Module relative time
 */
typedef SB_ERROR (*CANCCustomDecode)(CANFramingMsgStr *msgPtr, lu_uint32_t time);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

extern SB_ERROR CANCodecInit( lu_uint8_t        device       ,
                              lu_uint8_t        deviceID     ,
                              CANCCustomDecode  customDecoder
                            );


extern lu_uint32_t CANCodecInitDllVersion( lu_uint8_t  device,
										   lu_uint8_t        deviceID     ,
										   CANCCustomDecode  customDecoder
                                         );

/*!
 ******************************************************************************
 *  \brief Send a message to the MCM
 *
 *  \param messageType Message type
 *  \param messageID Message ID
 *  \param deviceDst Destination type
 *  \param deviceIDDst Destination ID
 *  \param msgLen Message payload length
 *  \param msgBufPtr Message payload pointer
 *
 *  \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CANCSendFromMCM( MODULE_MSG_TYPE  messageType,
                                 MODULE_MSG_ID    messageID  ,
						         lu_uint8_t       deviceDst  ,
						         lu_uint8_t       deviceIDDst,
                                 lu_uint32_t      msgLen     ,
                                 lu_uint8_t      *msgBufPtr
                               );

extern SB_ERROR CANCDecode(void);

extern SB_ERROR CANCDecodeDll(void);

extern SB_ERROR CANCodecDeInit(void);

extern SB_ERROR CANCodecDeInitDllVersion(lu_uint32_t handle);

#endif /* _CAN_CODEC_INCLUDED */

/*
 *********************** End of file ******************************************
 */