/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/07/15      saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "CmdParser.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
/*!
 *  Calibration data parameters
 */
typedef struct calDataParamDef
{
		lu_uint8_t   *calDataStartPtr;
		lu_uint8_t   *calDataNextPtr;
		lu_uint16_t   calDataSize;
		lu_uint16_t   calDataMaxSize;
		lu_int16_t    lastCalId;
} calDataParamStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
/*!
  ******************************************************************************
  *   \brief Initialise the calibration table and keep the address of next calibration
  *    element to be appended
  *
 *   Detailed description
 *
  *   \param calDataPtr ,size
 *
  *   \return Error code
 *
  ******************************************************************************
  */
extern SB_ERROR CalibrationBlockInitCalDataPtr(lu_uint8_t *calDataPtr, lu_uint16_t size, lu_uint16_t maxCalId);

/*!
 ******************************************************************************
 *   \brief Writing the calibration element structure to memory (Flash or NV Ram)
 *
 *	 Writing the calibration element structure to memory in the form of
 *	 ID,Type, Length (number of bytes), the actual data need for calibration.
 *	 Note: No address of data is stored.
 *
 *   After writing calibration elements in to memory, this function will update
 *	 the board specific calibration table as well.
 *
 *   \param calElement - Calibration Element in the form of structure CalElementStr
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CalibrationBlockAddElement( lu_uint8_t    calID,
								            lu_uint8_t    calType,
								            lu_uint16_t   calDatalength,
								            lu_uint8_t    *calDataPtr
								          );


/*!
 ******************************************************************************
 *   \brief To erase all calibration elements
 *
 *   Erasing of all calibration elements and data located in NV RAM
 *
 *   \param void
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CalibrationBlockEraseAllElements(void);

/*
 *********************** End of file ******************************************
 */