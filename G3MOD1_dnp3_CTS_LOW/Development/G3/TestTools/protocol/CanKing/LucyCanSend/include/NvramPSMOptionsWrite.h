/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: NvramFactoryInfoWrite.h 3512 2013-07-08 11:35:05Z saravanan_v $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/test/protocol/CanKing/LucyCanSend/include/NvramFactoryInfoWrite.h $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   31/07/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "CmdParser.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
FileInfoStr calElementFile;
/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

SB_ERROR NvramWritePSMOptionsPrepare(ParsingArgsStr *argsFromCommand);
lu_bool_t NvramWritePSMOptions(ParsingArgsStr *argsFromCommand);
SB_ERROR LucyCANSendNvramWritePSMOptionsProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

/*
 *********************** End of file ******************************************
 */