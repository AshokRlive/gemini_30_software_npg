/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   31/07/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "CmdParser.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

FileInfoStr srcFirmwareFile, dstFirmwareFile;

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

SB_ERROR FirmwareVerifierFilePrepare(ParsingArgsStr *argsFromCommand);

lu_bool_t FirmwareVerifierGetBlock  (FileInfoStr *srcFirmwareFile, 
	                                 FileInfoStr *dstFirmwareFile, 
									 ParsingArgsStr *argsFromCommand);

SB_ERROR FirmwareVerifierPutBlock(FileInfoStr *srcFirmwareFile, 
	                              FileInfoStr *dstFirmwareFile,
								  CANFramingMsgStr *msgPtr,
								  ParsingArgsStr *argsFromCommand
								 );

SB_ERROR FirmwareVerifierFileCompare(lu_int8_t *fileOriginal, lu_int8_t *fileFromBoard);

SB_ERROR LucyCANSendFirmwareVerifierProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

SB_ERROR FirmwareCompare(lu_int8_t *fileOriginal, lu_int8_t *fileFromBoard);

/*
 *********************** End of file ******************************************
 */
