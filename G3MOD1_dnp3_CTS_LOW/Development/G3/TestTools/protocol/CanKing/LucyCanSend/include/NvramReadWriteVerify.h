/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *				NvramReadWriteVerify.h
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/02/15      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _NVRAMREADWRITEVERIFY_INCLUDED
#define _NVRAMREADWRITEVERIFY_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "ModuleProtocol.h"
#include "CANProtocolFraming.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief NVRAM write and verify	
 *
 *   NVRAM write and verify
 *
 *   \moduleType	Type of module (PSM, DSM,...)
 *   \moudelId		Id of the module (0,1,2...)
 *	 \nvRamParams   NVRAM paramers for read (Write as well)
 *   \dataBuffer    Buffer for data needs to be written
 *   
 *   \return		SB_ERROR  
 *
 ******************************************************************************
 */
extern SB_ERROR NvramWriteAndVerify( MODULE moduleType,
									 MODULE_ID moduleId,
									 TestNVRAMReadBuffStr nvRamParam,
									 lu_uint8_t *dataBuffer
								   );

/*!
 ******************************************************************************
 *   \brief NVRAM read write verify protocol decoder
 *
 *   NVRAM read write verify protocol decoder
 *
 *   \msgPtr		Pointer to CAN reply
 *   \time		    Time stamp of CAN message
 *   
 *   \return		SB_ERROR  
 *
 ******************************************************************************
 */
extern SB_ERROR NVRAMReadWriteVerifyProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

#endif /* _NVRAMREADWRITEVERIFY_INCLUDED */

/*
 *********************** End of file ******************************************
 */