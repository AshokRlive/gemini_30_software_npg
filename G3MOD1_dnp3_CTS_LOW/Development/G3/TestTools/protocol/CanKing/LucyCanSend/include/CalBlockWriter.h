/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/07/15      saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "CmdParser.h"

#include "NVRAMDef.h"

#include "roxml.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
#pragma pack(1)

typedef struct CalBlkDef
{
	TestNVRAMReadBuffStr nvRamAddr;
	NVRAMBlkHeadStr      calBlkHeader;
	lu_uint8_t           calBlkData[NVRAM_ID_BLK_CAL_SIZE];
}CalBlkStr;

#pragma pack()

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \Adding calibration elements calibration block 
 *	   
 *     Adding calibration elements from CSV files into calibration block
 *     in bootloader mode. 
 *	
 *    \param      parameter name      Description
 *    \calFiles   Calibration Files   Calibration files in CSV format 
 *
 *    \return SB_ERROR 
 *
 ******************************************************************************
 */
extern SB_ERROR CalBlockWritePrepare( MODULE      module, 
									  MODULE_ID   moduleId,
							          lu_int8_t   *calFilesPath,
									  lu_int8_t   *calConfigFile,
									  lu_uint8_t  NvramAddress,
									  lu_uint8_t  NvramI2CChan,
									  lu_uint8_t  blkOffset
								    );

SB_ERROR CalibrationBlockWriterProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);
/*
 *********************** End of file ******************************************
 */