/*! \file
******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
******************************************************************************
*    PROJECT:
*       G3 [Module Name]
*
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*       \brief
*
*      
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*
*
*    CREATION
*
*   Date          Name        Details
*   --------------------------------------------------------------------------
*   09/08/12    venkat_s    Initial version / Original by Venkat.
*
******************************************************************************
*   COPYRIGHT
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
******************************************************************************
*/


/*
******************************************************************************
* INCLUDES - Standard Library Modules Used
******************************************************************************
*/

/*
******************************************************************************
* INCLUDES - Project Specific Modules Used
******************************************************************************
*/
#include "Main.h"
#include "SingleCmdSender.h"
#include "CmdParser.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"

/*
******************************************************************************
* LOCAL - Definitions And Macros
******************************************************************************
*/

/*
******************************************************************************
* LOCAL - Typedefs and Structures
******************************************************************************
*/


/*
*******************************************************************************
* LOCAL - Prototypes Of Local Functions
*******************************************************************************
*/


/*
******************************************************************************
* EXPORTED - Variables
******************************************************************************
*/


/*
******************************************************************************
* LOCAL - Global Variables
******************************************************************************
*/

/*
******************************************************************************
* Exported Functions
******************************************************************************
*/

SB_ERROR SendSingleCommandPrepare(ParsingArgsStr *argsFromCommand)
{
	return SB_ERROR_NONE;
}

SB_ERROR SendSingleCommandLoopPrepare(ParsingArgsStr *argsFromCommand)
{
	return SB_ERROR_NONE;
}

lu_bool_t SendSingleCommandLoop(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR ret;
	lu_bool_t finish;
	lu_uint32_t count, i;

	flagDecoderPtr = argsFromCommand;

	for(i = 0; i < argsFromCommand->numberOfRun; i++ )
	{
		ret = CANCSendFromMCM( argsFromCommand->CANHeader.messageType,
							   argsFromCommand->CANHeader.messageID,
							   argsFromCommand->CANHeader.deviceDst,
					           argsFromCommand->CANHeader.deviceIDDst,
						       argsFromCommand->msgBuffLen,
							   argsFromCommand->msgBuff
							);
		
		Sleep(argsFromCommand->runInterval);
	}

	finish = LU_FALSE;

	return finish; 
}

lu_bool_t SendSingleCommand(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR ret;
	lu_bool_t finish;

	flagDecoderPtr = argsFromCommand;

	ret = CANCSendFromMCM( argsFromCommand->CANHeader.messageType,
						   argsFromCommand->CANHeader.messageID,
						   argsFromCommand->CANHeader.deviceDst,
			               argsFromCommand->CANHeader.deviceIDDst,
			               argsFromCommand->msgBuffLen,
			               argsFromCommand->msgBuff
			             );
	finish = LU_FALSE;

	return finish; 
}

/*
******************************************************************************
* Local Functions
******************************************************************************
*/

/*!
******************************************************************************
*   \brief Brief description
*
*   Detailed description
*
*   \param parameterName Description 
*
*
*   \return 
*
******************************************************************************
*/

SB_ERROR LucyCANSendSingleCommandSendProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_NONE;
	lu_uint16_t idx; 
	lu_uint8_t value;

	if( (flagDecoderPtr->parserFlag == PARSER_FLAG_SINGLE_COMMAND      ) ||
		(flagDecoderPtr->parserFlag == PARSER_FLAG_SINGLE_COMMAND_LOOP )
	  )
	{
		if(msgPtr->msgLen)
		{
			for(idx = 0; idx < msgPtr->msgLen ;idx++)
			{
				msgPtr->msgBufPtr += idx;
				value = *msgPtr->msgBufPtr;
				printf("%02X ", value);
			}
		}
		else
		{
			printf("No Response for this command");
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}
	return retError;
}


/*
*********************** End of file ******************************************
*/

