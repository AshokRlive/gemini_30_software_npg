/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09/08/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "CANProtocolFraming.h"
#include "CmdParser.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define LM73_ADDR					0x48 // was 0x90

#define LM73_IDENT_NATIONAL			0x190 // Identification reg

#define LM73_REG_MASK				0x07

#define LM73_REG_TEMPERATURE		0x00
#define LM73_REG_CONFIGURATION		0x01
#define LM73_REG_T_HIGH				0x02
#define LM73_REG_T_LOW				0x03
#define LM73_REG_CONTROL_STATUS		0x04
#define LM73_REG_IDENTIFICATION		0x07

#define LM73_CFG_FULL_POWER_DOWN	0x80
#define LM73_CFG_ALERT_DISABLE		0x20
#define LM73_CFG_ALERT_ACTIVE_HIGH	0x10
#define LM73_CFG_ALERT_RESET		0x08
#define LM73_CFG_ONE_SHOT			0x04

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

extern SB_ERROR I2CTemperatureSensorTestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time);

extern SB_ERROR I2CTempLM73ReadDll( MODULE module, 
									MODULE_ID moduleId, 
									lu_uint8_t i2cChan,
									lu_uint8_t addr,
									lu_int32_t *temperature
								  );

extern SB_ERROR I2CTempLM73ReadRegDll( MODULE module, 
									   MODULE_ID moduleId, 
									   lu_uint8_t    i2cChan,
									   lu_uint8_t    addr,
									   lu_uint8_t    reg,
									   lu_uint16_t    *regValue_r
									 );

extern SB_ERROR I2CTempLM73WriteRegDll( MODULE module, 
								    MODULE_ID moduleId, 
								    lu_uint8_t    i2cChan,
								    lu_uint8_t    addr,
								    lu_uint8_t    reg,
									lu_uint8_t    value
						         );

/*
 *********************** End of file ******************************************
 */