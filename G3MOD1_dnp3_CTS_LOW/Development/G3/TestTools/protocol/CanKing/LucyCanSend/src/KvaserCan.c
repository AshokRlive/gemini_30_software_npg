/*
**                         Copyright 1996-98 by KVASER AB            
**                   P.O Box 4076 SE-51104 KINNAHULT, SWEDEN
**             E-mail: staff@kvaser.se   WWW: http://www.kvaser.se
**
** This software is furnished under a license and may be used and copied
** only in accordance with the terms of such license.
**
*/

#include <stdlib.h>
#include <stdio.h>
//#include <memory.h>
//#include <windows.h>
//#include <conio.h>
//#include <string.h>

// CANLIB requires canlib.h.
#include "canlib.h"
#include "KvaserCan.h"
#include "lu_types.h"

//
// Check a status code and issue an error message if the code
// isn't canOK.
//
void Check(char* id, canStatus stat)
{
    char buf[50];
    if (stat != canOK) 
	{
        buf[0] = '\0';
        canGetErrorText(stat, buf, sizeof(buf));
        printf("%s: failed, stat=%d (%s)\n", id, (int)stat, buf);
    }
}

//
// Setup a CAN controller.
//
int KvaserInitCtrl(int ctrl)
{
    int stat;
    int hnd;

	canInitializeLibrary();

    //
    // First, open a handle to the CAN circuit. Specifying
    // canOPEN_EXCLUSIVE ensures we get a circuit that noone else
    // is using.
    //
    //printf("\ncanOpenChannel, channel %d... ", ctrl);
    hnd = canOpenChannel(ctrl, 0);
    if (hnd < 0) 
	{
        Check("canOpenChannel", (canStatus)hnd);
        return -1;
    }
    //printf("OK.\n");

    //
    // Using our new shiny handle, we specify the baud rate
    // using one of the convenient BAUD_xxx constants.
    //
    // The bit layout is in depth discussed in most CAN
    // controller data sheets, and on the web at
    // http://www.kvaser.se.
    //
    //printf("Setting the bus speed...");
    stat = canSetBusParams(hnd, BAUD_500K, 0, 0, 1, 1, 0);

    if (stat < 0) 
	{
        printf("canSetBusParams failed, stat=%d\n", stat);
    }
    //printf("OK.\n");

    //
    // Then we start the ball rolling.
    // 
    //printf("Go bus-on...");
    stat = canBusOn(hnd);
    if (stat < 0) 
	{
        printf("canBusOn failed, stat=%d\n", stat);
    }
    //printf("OK.\n");

    // Return the handle; our caller will need it for
    // further exercising the CAN controller.
    return hnd;
}

void KvaserCanWrite(int handle, lu_uint32_t id, unsigned char *msgBuffer, lu_uint32_t len)
{
	canFlushReceiveQueue (handle);
	canWrite(handle, id, msgBuffer, len, canMSG_EXT);

	canWriteSync(handle, 10);
}

lu_uint8_t KvaserCanRead(int handle, lu_uint32_t *id, lu_uint8_t *msgBuffer)
{
	int             stat, i;
	unsigned int    dlc;
    unsigned int    flags;
    DWORD           time;

	canReadSync(handle, 200);
	stat = canRead(handle, id, msgBuffer, &dlc, &flags, &time);

	if (stat != 0)
	{
		return 0;
	}
	
	/* For Debugging purpose */
	/*for(i=0;i<7;i++)
	{
		printf("%0X ",msgBuffer[i]);
	}*/

	return dlc;
}

lu_uint32_t KvaserCanDeInit(const CanHandle handle)
{
    lu_uint32_t ret;
	ret = canClose(handle);
	return ret;
}

