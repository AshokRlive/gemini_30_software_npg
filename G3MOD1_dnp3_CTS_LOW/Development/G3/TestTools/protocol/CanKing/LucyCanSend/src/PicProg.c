/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [PIC program over CAN]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28/07/14      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stddef.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "PicProg.h"
#include "ds30loader.h"
#include "CmdParser.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

lu_uint8_t hex2BinNibble(lu_uint8_t *inChar);
lu_uint8_t hex2BinByte(lu_uint8_t *inChar);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


lu_uint8_t *bufferPtr = NULL;

static lu_uint32_t msgBlockCount;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


SB_ERROR PicProgDownloaderFilePrepare(ParsingArgsStr *argsFromCommand)
{
   	SB_ERROR retValue = SB_ERROR_NONE;
   	FILE *filePtr;
	lu_uint8_t line[200];
	lu_uint8_t dataLen;
	lu_uint16_t dataAddr;
	lu_uint8_t dataType;
	lu_uint8_t dataChk;
	lu_uint8_t calcChk;
	lu_uint8_t lineLen;
	lu_uint8_t idx;

	msgBlockCount = 0;

	filePtr = fopen(argsFromCommand->sourceFileName,"rb");

	if(filePtr == NULL)
	{
		printf(" Error in opening file ");
		return SB_ERROR_INITIALIZED;
	}

	bufferPtr = (lu_uint8_t *)malloc(0x10000);
	memset(bufferPtr, 0xff, 0x10000);

	printf("\n Parsing hex file");

	while (fgets(line, (sizeof(line) -1), filePtr) && !feof(filePtr))
	{
		if(line[0] != ':')
		{
			printf("\n Invalid hex file");
			exit(-1);
		}

		lineLen = strlen(line);

		dataLen   = hex2BinByte(&line[1]);

		dataAddr  = hex2BinByte(&line[3]) << 8;
		dataAddr |= hex2BinByte(&line[5]);

		dataType  = hex2BinByte(&line[7]);

		/* Check line Length */

		if(dataType == 0x00)
		{
			dataChk = hex2BinByte(&line[7 + (dataLen * 2) + 2]);

			/* Calculate Checksum */
			calcChk = 0;
			for(idx = 0; idx < (dataLen * 2) + 4 + 2 + 2 + 2; idx += 2)
			{
				calcChk += hex2BinByte(&line[1 + idx]);
			}

			/* Verify checksum */
			if(calcChk)
			{
				printf("\n Bad hex line checksum");
				exit(-1);
			}

			for(idx = 0; idx < (dataLen * 2); idx += 2)
			{
				bufferPtr[dataAddr + (idx / 2)] = hex2BinByte(&line[9 + idx]);
			}
		}
	}

	fclose(filePtr);

	/* Patch vector table */
	bufferPtr[((DS30_PIC16F1788_MAX_FLASH - DS30_BOOTLOADER_PLACEMENT) * 2) - 6] = bufferPtr[0];
	bufferPtr[((DS30_PIC16F1788_MAX_FLASH - DS30_BOOTLOADER_PLACEMENT) * 2) - 5] = bufferPtr[1];
	//bufferPtr[DS30_PIC16F1788_MAX_FLASH - DS30_BOOTLOADER_PLACEMENT - 4] = bufferPtr[2];
	//bufferPtr[DS30_PIC16F1788_MAX_FLASH - DS30_BOOTLOADER_PLACEMENT - 3] = bufferPtr[3];
	bufferPtr[((DS30_PIC16F1788_MAX_FLASH - DS30_BOOTLOADER_PLACEMENT) * 2) - 4] = 0x00;
	bufferPtr[((DS30_PIC16F1788_MAX_FLASH - DS30_BOOTLOADER_PLACEMENT) * 2) - 3] = 0x00;
	bufferPtr[((DS30_PIC16F1788_MAX_FLASH - DS30_BOOTLOADER_PLACEMENT) * 2) - 2] = bufferPtr[2];
	bufferPtr[((DS30_PIC16F1788_MAX_FLASH - DS30_BOOTLOADER_PLACEMENT) * 2) - 1] = bufferPtr[3];

	/* Repair bootloader vector table */
	bufferPtr[0] = 0xb8;
	bufferPtr[1] = 0x31;
	bufferPtr[2] = 0x00;
	bufferPtr[3] = 0x2f;

	return retValue;
}

lu_bool_t PicProgDownloaderBlockSend(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR retValue;
	lu_bool_t finish;
	lu_uint32_t msgByteCount;

	msgByteCount = 0;
	memset(argsFromCommand->msgBuff, 0xff, 64 + 3);

	argsFromCommand->msgBuff[0] = msgBlockCount & 0xFF;
	argsFromCommand->msgBuff[1] = msgBlockCount >> 8;

	argsFromCommand->msgBuff[2] = 2; // PIC 1 - UART2

	memcpy(&argsFromCommand->msgBuff[3], (bufferPtr + (msgBlockCount * 64)) , 64);

	msgBlockCount++;

	if(msgBlockCount == ((DS30_PIC16F1788_MAX_FLASH - DS30_BOOTLOADER_PLACEMENT) / 
		                  DS30_PIC16F1788_ROWSIZEW))
	{
		finish = LU_FALSE;
	}
	else
	{
		finish = LU_TRUE;
	}

	printf("%0X%02X ", argsFromCommand->msgBuff[1],argsFromCommand->msgBuff[0] );

	 /* Sending the firmware block via CAN bus */
	retValue = CANCSendFromMCM( MODULE_MSG_TYPE_BL,
								MODULE_MSG_ID_BL_WRITE_PIC_FIRMWARE_C,
								argsFromCommand->CANHeader.deviceDst,
								argsFromCommand->CANHeader.deviceIDDst,
								64 + 3,
								argsFromCommand->msgBuff
							  );
	//Sleep(10);

	return finish;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
lu_uint8_t hex2BinNibble(lu_uint8_t *inChar)
{
	lu_uint8_t ret;

	if(*inChar >= 'a')
	{
		ret = *inChar - 'a' + 10;
	}
	else if(*inChar >= 'A')
	{
		ret = *inChar - 'A' + 10;
	}
	else
	{
		ret = *inChar - '0';
	}

	return ret;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
lu_uint8_t hex2BinByte(lu_uint8_t *inChar)
{
	lu_uint8_t ret;

	ret = hex2BinNibble(inChar) << 4;

	ret |= hex2BinNibble(++inChar);

	return ret;
}

/*
 *********************** End of file ******************************************
 */
