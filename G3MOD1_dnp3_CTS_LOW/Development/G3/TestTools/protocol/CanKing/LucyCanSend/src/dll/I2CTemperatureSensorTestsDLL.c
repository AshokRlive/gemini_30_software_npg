/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *   22/11/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "I2CTemperatureSensorTestsDLL.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

TestI2cTempLM73ReadRspStr *tempReadRegValuePtr;
TestI2cTempLM73ReadRspStr *tempReadValuePtr;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR I2CTempLM73ReadDll( MODULE module, 
								MODULE_ID moduleId, 
								lu_uint8_t i2cChan,
								lu_uint8_t addr,
								lu_int32_t *temperature
							  )
{
	SB_ERROR retError;
	TestI2cTempLM73ReadStr I2CTempLM73Read;
	
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);
	
	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_I2C_TEMP_READ_DEG_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	I2CTempLM73Read.i2cChan = i2cChan;
	I2CTempLM73Read.addr    = addr;
	
	CANMsgFiller((lu_uint8_t*)&I2CTempLM73Read,sizeof(TestI2cTempLM73ReadStr));

	retError = LucyCANSendMainDll();

	*temperature = tempReadValuePtr->value;

	return retError;
	
}

SB_ERROR I2CTempLM73ReadRegDll( MODULE module, 
								   MODULE_ID moduleId, 
								   lu_uint8_t    i2cChan,
								   lu_uint8_t    addr,
								   lu_uint8_t    reg,
								   lu_uint16_t   *regValue_r
						         )
{
	SB_ERROR retError;
	TestI2cTempLM73ReadRegStr I2CTempLM73ReadReg;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_I2C_TEMP_READ_REG_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;
	
	I2CTempLM73ReadReg.params.i2cChan = i2cChan;
	I2CTempLM73ReadReg.params.addr	  = addr;
	I2CTempLM73ReadReg.reg			  = reg;

	CANMsgFiller ((lu_uint8_t*)&I2CTempLM73ReadReg,sizeof(TestI2cTempLM73ReadRegStr));
	
	retError = LucyCANSendMainDll();

	*regValue_r = tempReadRegValuePtr->value;

	return retError;
}

SB_ERROR I2CTempLM73WriteRegDll( MODULE module, 
								 MODULE_ID moduleId, 
								 lu_uint8_t    i2cChan,
								 lu_uint8_t    addr,
								 lu_uint8_t    reg,
								 lu_uint8_t    value
						       )
{
	SB_ERROR retError;
	TestI2cTempLM73WriteRegStr I2CTempLM73WriteReg;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_I2C_TEMP_WRITE_REG_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;
	
	I2CTempLM73WriteReg.i2cChan = i2cChan;
	I2CTempLM73WriteReg.addr    = addr;
	I2CTempLM73WriteReg.reg     = reg;
	I2CTempLM73WriteReg.value   = value;

	CANMsgFiller ((lu_uint8_t*)&I2CTempLM73WriteReg,sizeof(I2CTempLM73WriteReg));
	
	retError = LucyCANSendMainDll();

	return retError;
}

SB_ERROR I2CTemperatureSensorTestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_NONE;
		
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_SINGLE_COMMAND)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_BLTST:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_BLTST_I2C_TEMP_READ_DEG_R: 
						if(msgPtr->msgLen == sizeof(TestI2cTempLM73ReadRspStr))
						{
							tempReadValuePtr = (TestI2cTempLM73ReadRspStr*)msgPtr->msgBufPtr;
							retError = SB_ERROR_NONE;
						}
						break;

					case MODULE_MSG_ID_BLTST_I2C_TEMP_READ_REG_R: 
						if(msgPtr->msgLen == sizeof(TestI2cTempLM73ReadRspStr))
						{
							tempReadRegValuePtr = (TestI2cTempLM73ReadRspStr*)msgPtr->msgBufPtr;
							retError = SB_ERROR_NONE;
						}
						break;

					case MODULE_MSG_ID_BLTST_I2C_TEMP_WRITE_REG_R: 
						if(msgPtr->msgLen == sizeof(TestErrorRspStr))
						{
							retError = SB_ERROR_NONE;
						}
						break;
					
					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;
			default:
				printf("\n Error in Downloading ");
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */