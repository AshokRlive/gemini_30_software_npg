/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN protocol codec module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26/04/2012    fryers_j    Initial version / Original by Venkat.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "CANProtocolCodec.h"
#include "CANProtocolFraming.h"

#include "CmdParser.h"
#include "KvaserCan.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "hat_client.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*!
 * Data structure for the internal message queue
 */
typedef struct CANCodecDef
{
    /*! Device type */
    lu_uint8_t  device     ;
    /*! Device address */
    lu_uint8_t  deviceID   ;

	/*! Kvaser CAN handler */
	lu_uint32_t	handle;

//	CANFramingMsgStr msg;

    CANCCustomDecode customDecoder;
}CANCodecStr;

typedef struct HATReplyHeaderDef
{
	/* Buffer ID. Used by the CAN Framing library */
    lu_uint8_t  ID         ;
    /* Message type */
    lu_uint8_t  messageType;
    /* Message ID */
    lu_uint8_t  messageID  ;
    /* Source address */
    lu_uint8_t  deviceIDSrc;
    /* Source device type */
    lu_uint8_t  deviceSrc  ;
    /* Destination address */
    lu_uint8_t  deviceIDDst;
    /* Destination device type */
    lu_uint8_t  deviceDst  ;
}HATReplyHeaderStr;

typedef struct HATReplyMsgDef
{
	/* Message length */
    lu_uint32_t  hatMsgLen    ;
    /* Message buffer */
    lu_uint8_t hatMsgBuf[MODULE_MESSAGE_LENGTH];	
}HATReplyMsgStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void CANCNewMessage(CANFramingMsgStr *msgPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static CANCodecStr CANCodecData;
lu_uint8_t hatReceiveBuffer[440];
lu_int16_t hatReceiveSize;


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR CANCodecInit( lu_uint8_t        device       ,
                       lu_uint8_t        deviceID     ,
                       CANCCustomDecode  customDecoder
                     )
{
    SB_ERROR ret = SB_ERROR_INITIALIZED;

    if (customDecoder == NULL)
    {
        return SB_ERROR_PARAM;
    }

    /* Save custom decode function */
    CANCodecData.customDecoder = customDecoder;
	ret = SB_ERROR_NONE;

	/* Save device address */
	CANCodecData.device   = device;
	CANCodecData.deviceID = deviceID;

	CANCodecData.handle = KvaserInitCtrl(0);

	if(CANCodecData.handle > 0)
	{
	/* Initialize CAN Framing module */
		ret = CANFramingInit( device,
							deviceID,
							&CANCodecData.handle
						);
	}
	
	
    /* Set the filters for the message handled messages */
    return ret;
}

lu_uint32_t CANCodecInitDllVersion( lu_uint8_t        device       ,
									lu_uint8_t        deviceID     ,
									CANCCustomDecode  customDecoder
								  )
{
    SB_ERROR retValue = SB_ERROR_NONE;

    if (customDecoder == NULL)
    {
        return SB_ERROR_PARAM;
    }

    ///* Save custom decode function 
    CANCodecData.customDecoder = customDecoder;

    ///* Save device address 
    CANCodecData.device   = MODULE_MCM;
    CANCodecData.deviceID = MODULE_ID_0;

	CANCodecData.handle = 0;
	
	if(argsFromCommand.comMode == COM_MODE_CAN)
	{
		CANCodecData.handle = KvaserInitCtrl(0);
		if(CANCodecData.handle < 0)
		{
			retValue = SB_ERROR_INITIALIZED;
		}
	}
	
	///* Initialize CAN Framing module 
	retValue = CANFramingInit( device              ,
							   deviceID            ,
							   &CANCodecData.handle
				             );
	///* Set the filters for the message handled messages 
	return CANCodecData.handle;
}

SB_ERROR CANCSendFromMCM( MODULE_MSG_TYPE  messageType,
                          MODULE_MSG_ID    messageID  ,
						  lu_uint8_t       deviceDst  ,
						  lu_uint8_t       deviceIDDst,
                          lu_uint32_t      msgLen     ,
                          lu_uint8_t       *msgBufPtr
                        )
{
    CANFramingMsgStr CANTXMsg;
	SB_ERROR retError = SB_ERROR_NONE;
	lu_uint8_t ethPayLoad[440];
	lu_uint8_t ethSendBuffer[sizeof(CANHeaderStr)+sizeof(ethPayLoad)];
	CANHeaderStr canHeader;
	lu_int16_t payLoadSize;
	
	if(argsFromCommand.comMode == COM_MODE_ETH)
	{
		canHeader.fragment    = 0;
		canHeader.deviceSrc   = MODULE_MCM;
		canHeader.deviceIDSrc = MODULE_ID_0;
		canHeader.deviceDst   = deviceDst;
		canHeader.deviceIDDst = deviceIDDst;
		canHeader.messageType = messageType;
		canHeader.messageID   = messageID; 

		memcpy(&ethSendBuffer[0], &canHeader, sizeof(CANHeaderStr));
		memcpy(&ethSendBuffer[sizeof(CANHeaderStr)], msgBufPtr, msgLen);

		payLoadSize = sizeof(CANHeaderStr) + msgLen;

		hat_send_can2(0, 1000, (lu_uint8_t*)ethSendBuffer, payLoadSize, &hatReceiveBuffer[0], &hatReceiveSize);

		memcpy(&canHeader, &hatReceiveBuffer[0], sizeof(CANHeaderStr));
		//memcpy(testBuffer, hatReceiveBuffer, sizeof(testBuffer));
	}
	else
	{
		 /* Fill header */
	    CANTXMsg.ID = 0;
		CANTXMsg.messageType = messageType;
		CANTXMsg.messageID   = messageID;
		CANTXMsg.deviceDst   = deviceDst;
		CANTXMsg.deviceIDDst = deviceIDDst;
		CANTXMsg.deviceSrc   = MODULE_MCM;
		CANTXMsg.deviceIDSrc = MODULE_ID_0;
		CANTXMsg.msgLen      = msgLen;
		CANTXMsg.msgBufPtr   = msgBufPtr;
		/* Send message */
		retError = CANFramingSendMsg(&CANTXMsg);
	}

	return retError;
}

SB_ERROR CANCDecode(void)
{
    lu_uint32_t time;
    CANFramingMsgStr *msgPtr;
    SB_ERROR ret = SB_ERROR_NONE;

	/* Get a message */
	msgPtr = CANFramingRecvMsg();
	
	/* Get current time  */
    time = 0;

	if (msgPtr->msgLen == 0)
	{
		return SB_ERROR_CANC_NO_DATA;
	}

	/* Forward message */
	ret = CANCodecData.customDecoder(msgPtr, time);

    return ret;
}

SB_ERROR CANCDecodeDll(void)
{
    lu_uint32_t time;
    CANFramingMsgStr  *msgPtr; 
	CANFramingMsgStr  hatReply;
	CANHeaderStr *canHeader;
	volatile lu_uint8_t msgBuffer[440];

	SB_ERROR ret = SB_ERROR_NONE;

	/* Get current time  */
    time = 0;

	if(argsFromCommand.comMode == COM_MODE_CAN)
	{
		/* Get a message */
		msgPtr = CANFramingRecvMsg();
	}

	if(argsFromCommand.comMode == COM_MODE_ETH)
	{
		
		hatReply.ID          = 0;
    	
		canHeader = (CANHeaderStr*)&hatReceiveBuffer[0];

		hatReply.messageType = canHeader->messageType;
		hatReply.messageID   = canHeader->messageID;
		hatReply.deviceSrc   = canHeader->deviceSrc;
		hatReply.deviceIDSrc = canHeader->deviceIDSrc;
		hatReply.deviceDst   = canHeader->deviceDst;
		hatReply.deviceIDDst = canHeader->deviceIDDst;

		hatReply.msgLen      = hatReceiveSize - sizeof(CANHeaderStr);

		memcpy(&msgBuffer[0], &hatReceiveBuffer[sizeof(CANHeaderStr)],440);

		//hatReply.msgBufPtr   = &hatReceiveBuffer[sizeof(CANHeaderStr)];
		hatReply.msgBufPtr   = &msgBuffer[0];

		msgPtr = &hatReply;
	}
	
	
	/*if (msgPtr->msgLen == 0)
	{
		return SB_ERROR_CANC_NO_DATA;
	}
	*/

	/* Forward message */
	ret = CANCodecData.customDecoder(msgPtr, time);

    return ret;
}

SB_ERROR CANCodecDeInit()
{
	KvaserCanDeInit(CANCodecData.handle);
	return SB_ERROR_NONE;
}
 

SB_ERROR CANCodecDeInitDllVersion(lu_uint32_t handle)
{
	SB_ERROR retError = SB_ERROR_INITIALIZED;
	lu_uint32_t ret;

	ret = KvaserCanDeInit(handle);
	if(ret == LU_FALSE)
	{
		retError = SB_ERROR_NONE;
	}

	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
