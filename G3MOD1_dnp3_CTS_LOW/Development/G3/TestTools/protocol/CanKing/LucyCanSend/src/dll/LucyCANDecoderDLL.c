/*! \file
******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
******************************************************************************
*    PROJECT:
*       G3 [Module Name]
*
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*       \brief
*
*      
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*
*
*    CREATION
*
*   Date          Name        Details
*   --------------------------------------------------------------------------
*   22/11/12    venkat_s    Initial version / Original by Venkat.
*
******************************************************************************
*   COPYRIGHT
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
******************************************************************************
*/


/*
******************************************************************************
* INCLUDES - Standard Library Modules Used
******************************************************************************
*/

/*
******************************************************************************
* INCLUDES - Project Specific Modules Used
******************************************************************************
*/

//#include "HeartBeatMsg.h"
#include "GPIOTestsDLL.h"
#include "I2CExpIOTestsDLL.h"
#include "ADCTestsDLL.h"
#include "LucyCANDecoderDLL.h"
#include "I2CTemperatureSensorTestsDLL.h"
#include "I2CADCTestsDLL.h"
#include "SPIADDigiPotTestsDLL.h"
#include "SPIADCTestsDLL.h"
#include "I2CHumiditySensorTests.h"
#include "NvramFactoryInfoWriteDll.h"
#include "NvramFactoryInfoRead.h"
#include "NvramPSMOPtionsWriteDLL.h"
#include "NvramPSMOPtionsRead.h"
#include "NvramBatteryOptionsWriteDLL.h"
#include "NvramBatteryOptionsRead.h"
#include "CalElementAdderDll.h"
#include "CalBlockWriter.h"
#include "NvramReadWriteVerify.h"

//#ifdef FPMV2TestIOManager
#include "FPMV2Tests.h"
#include "ADE78xxSPIDLL.h"
#include "ADE78xxI2CDLL.h"
//#endif

#ifdef FPMTestIOManager
#include "FPITests.h"
#endif

#ifdef FDMTestIOManager
#include "FDMTests.h"
#endif

/*
******************************************************************************
* LOCAL - Definitions And Macros
******************************************************************************
*/

/*
******************************************************************************
* LOCAL - Typedefs and Structures
******************************************************************************
*/


/*
*******************************************************************************
* LOCAL - Prototypes Of Local Functions
*******************************************************************************
*/


/*
******************************************************************************
* EXPORTED - Variables
******************************************************************************
*/

/*
******************************************************************************
* LOCAL - Global Variables
******************************************************************************
*/


/*
******************************************************************************
* Exported Functions
******************************************************************************
*/
SB_ERROR LucyCANProtocolDecoderDll(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_PARAM;
	
	LU_UNUSED(time);

	/*
	retError = HeartBeatMsgSendCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}
	*/

	/* ********  Optimization is needed to improve efficiency in running time *********** */

//#ifdef FPMV2TestIOManager
	retError = FPMV2ADE78xxI2CTestsCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = FPMV2ADE78xxSPITestsCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}
//#endif

#ifdef FPMTestIOManager
	retError = FPITestsCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}
#endif
	
#ifdef FDMTestIOManager
	retError = FDMTestsCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}
#endif

	retError = GPIOTestsCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = ADCTestsCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = I2CExpIOTestsCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = I2CTemperatureSensorTestsCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = I2CADCTestsCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = SPIADDigiPotTestsCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = SPIADCTestsCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = I2CHumiditySensorTestsCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = CalElementAdderCANProtocolDll(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = NVRAMReadWriteVerifyProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = LucyCANSendNvramReadFactoryInfoProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = LucyCANSendNvramReadPSMOptionsProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = LucyCANSendNvramReadHMIOptionsProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = LucyCANSendNvramReadBatteryOptionsProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	/* ********  Optimization is needed to improve efficiency in running time *********** */
	return retError;
}

/*
******************************************************************************
* Local Functions
******************************************************************************
*/

/*!
******************************************************************************
*   \brief Brief description
*
*   Detailed description
*
*   \param parameterName Description 
*
*
*   \return 
*
******************************************************************************
*/


/*
*********************** End of file ******************************************
*/

