/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *  18/12/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "NvramBatteryOptionsWriteDLL.h"
#include "NvramBatteryOptionsReadDLL.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
TestErrorRspStr *nvramWriteResp;
 
/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR NvramBatteryOptionsWriteDll( MODULE module, 
								  MODULE_ID moduleId,
								  lu_uint8_t NvramAddress,
								  lu_int8_t *batteryOptionsFile
					   			 )
{
	SB_ERROR retError;
	lu_uint8_t fileNameLen;
    
	module = MODULE_PSM;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);
	
	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_NVRAM_WRITE_BUFFER_C;

	argsFromCommand.parserFlag = PARSER_FLAG_NVRAM_WRITE_BATT_OPTS;
	
	argsFromCommand.nvramAddr         = NvramAddress;
	fileNameLen = strlen(batteryOptionsFile);

	memset(argsFromCommand.sourceFileName, 0x00, sizeof(argsFromCommand.sourceFileName));
    memcpy(argsFromCommand.sourceFileName, batteryOptionsFile, fileNameLen);
		
	retError = LucyCANSendMainDll();
	
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */