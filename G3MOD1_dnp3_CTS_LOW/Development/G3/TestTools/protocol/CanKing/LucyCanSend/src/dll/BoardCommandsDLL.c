/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *  16/09/13      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "GPIOTestsDLL.h"
#include "HeartBeatMsg.h"
#include "BoardCommandsDLL.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
 
/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR GoToApplicationDll( MODULE module, 
						     MODULE_ID moduleId
					        )
{
	SB_ERROR retError;
		
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);
	
	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BL;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BL_START_APP_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	CANMsgFiller(0, 0);

	retError = LucyCANSendMainDll();

	return retError;
}

SB_ERROR GoToBootloaderDll( MODULE module, 
						    MODULE_ID moduleId
					      )
{
	SB_ERROR retError = SB_ERROR_PARAM;
	lu_uint32_t powerStatus = 0;
	lu_uint8_t readPinTest;
	lu_uint8_t boardReset = 0;
	lu_uint8_t moduleExist = 0;

	memset(&argsFromCommand, 0x00, sizeof(ParsingArgsStr));

	CANHeaderFiller( module, moduleId );

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_MD_CMD;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_MD_CMD_RESTART;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	CANMsgFiller(&boardReset, 1);

	retError = LucyCANSendMainDll();

	Sleep(100);

	HeartBeatMsgSend(); Sleep(100);HeartBeatMsgSend(); Sleep(100);
	HeartBeatMsgSend(); Sleep(100);HeartBeatMsgSend(); Sleep(100);
	HeartBeatMsgSend(); Sleep(100);HeartBeatMsgSend(); Sleep(100);
	HeartBeatMsgSend(); Sleep(100);HeartBeatMsgSend(); Sleep(100);
	HeartBeatMsgSend(); Sleep(100);HeartBeatMsgSend(); Sleep(100);
	HeartBeatMsgSend(); Sleep(100);HeartBeatMsgSend(); Sleep(100);
	HeartBeatMsgSend(); Sleep(100);HeartBeatMsgSend(); Sleep(100);
	HeartBeatMsgSend(); Sleep(100);
	HeartBeatMsgSend(); Sleep(100);
	HeartBeatMsgSend(); Sleep(100);

	Sleep(1000);
	
	retError = SB_ERROR_PARAM;
	retError = GPIOReadPinDll( module, 
							   moduleId,
							   0,
							   0,
							   &readPinTest
					    	 );
	
	return retError;
}


SB_ERROR CalibrationEraseAllDLL( MODULE module, 
						         MODULE_ID moduleId
						       )
{
	SB_ERROR retError;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);
	
	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_CALTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_CALTST_ERASE_ELEMENTS_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	CANMsgFiller(0, 0);

	retError = LucyCANSendMainDll();

	return retError;
}


SB_ERROR CalibrationNVRAMSelectDLL( MODULE module, 
						            MODULE_ID moduleId,
						            lu_uint8_t nvRamType
					              )
{
	SB_ERROR retError;

	CalTstNvramSelStr nvRamTypeSel;
		
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);
	
	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_CALTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_CALTST_NVRAM_SELECT_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	nvRamTypeSel.nvramType = nvRamType;

	CANMsgFiller((lu_uint8_t*)&nvRamTypeSel,sizeof(CalTstNvramSelStr));

	retError = LucyCANSendMainDll();

	return retError;
}

SB_ERROR CalibrationNVRAMUpdateDLL( MODULE module, 
						            MODULE_ID moduleId,
						            lu_uint8_t nvRamType,
									lu_uint8_t nvRamBlk
					              )
{
	SB_ERROR retError;

	NVRAMCmdStr	nvRamParam;
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);
	
	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_NVRAM;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_NVRAM_UPDATE_BLK_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	nvRamParam.nvramType = nvRamType;
	nvRamParam.nvramBlk  = nvRamBlk;

	CANMsgFiller((lu_uint8_t*)&nvRamParam,sizeof(NVRAMCmdStr));

	retError = LucyCANSendMainDll();

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
