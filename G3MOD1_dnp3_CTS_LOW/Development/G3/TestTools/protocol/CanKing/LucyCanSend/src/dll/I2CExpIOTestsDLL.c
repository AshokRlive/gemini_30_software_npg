/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *   28/11/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

TestI2cExpDDRReadRspStr *I2cExpDDRPtr;
TestI2cExpPinReadRspStr *I2cExpReadPinPtr;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR I2CExpIOReadDDRDll( MODULE module, 
							 MODULE_ID moduleId, 
							 lu_uint8_t i2cChan,
							 lu_uint8_t addr,
							 lu_uint16_t *value_r
						   )
{
	SB_ERROR retError;
	TestI2cExpDDRReadStr I2CExpIOReadDDR;
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_I2C_EXP_READ_DDR_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	I2CExpIOReadDDR.addr.i2cChan = i2cChan;
	I2CExpIOReadDDR.addr.addr    = addr;
	
	CANMsgFiller ((lu_uint8_t*)&I2CExpIOReadDDR,sizeof(TestI2cExpDDRReadStr));
	
	retError = LucyCANSendMainDll();

	*value_r = I2cExpDDRPtr->value;

	return retError;
}

SB_ERROR I2CExpIOWriteDDRDll( MODULE module, 
							  MODULE_ID moduleId, 
							  lu_uint8_t i2cChan,
							  lu_uint8_t addr,
							  lu_uint16_t value
							)
{
	SB_ERROR retError;
	TestI2cExpDDRWriteStr I2CExpIOWrite;
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);
	
	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_DDR_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	I2CExpIOWrite.addr.i2cChan = i2cChan;
	I2CExpIOWrite.addr.addr    = addr;
	I2CExpIOWrite.value        = value;

	CANMsgFiller ((lu_uint8_t*)&I2CExpIOWrite,sizeof(TestI2cExpDDRWriteStr));

	retError = LucyCANSendMainDll();

	return retError;
}

SB_ERROR I2CExpIOReadPinDll( MODULE module, 
							 MODULE_ID moduleId, 
							 lu_uint8_t i2cChan,
							 lu_uint8_t addr,
							 lu_uint8_t pin,
							 lu_uint8_t *value_r
				           )
{
	SB_ERROR retError;
	TestI2cExpPinReadStr I2CExpIORead;
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_I2C_EXP_READ_PIN_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	I2CExpIORead.addr.i2cChan = i2cChan;
	I2CExpIORead.addr.addr    = addr;
	I2CExpIORead.pin		  = pin;	

	CANMsgFiller ((lu_uint8_t*)&I2CExpIORead,sizeof(TestI2cExpPinReadStr));

	retError = LucyCANSendMainDll();

	*value_r = I2cExpReadPinPtr->value;

	return retError;
}

SB_ERROR I2CExpIOWritePinDll( MODULE module, 
							  MODULE_ID moduleId, 
							  lu_uint8_t i2cChan,
							  lu_uint8_t addr,
							  lu_uint8_t pin,
							  lu_uint8_t value
							)
{
	SB_ERROR retError;
	TestI2cExpPinWriteStr I2CExpIOWritePin;
	
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);
	
	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_PIN_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	I2CExpIOWritePin.addr.i2cChan = i2cChan;
	I2CExpIOWritePin.addr.addr    = addr;
	I2CExpIOWritePin.pin		  = pin;
	I2CExpIOWritePin.value		  = value;

	CANMsgFiller ((lu_uint8_t*)&I2CExpIOWritePin,sizeof(TestI2cExpPinWriteStr));

	retError = LucyCANSendMainDll();

	return retError;
}


SB_ERROR I2CExpIOTestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_NONE;
		
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_SINGLE_COMMAND)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_BLTST:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_BLTST_I2C_EXP_READ_DDR_R: 
						if(msgPtr->msgLen == sizeof(TestI2cExpDDRReadRspStr))
						{
							I2cExpDDRPtr = (TestI2cExpDDRReadRspStr*)msgPtr->msgBufPtr;
							retError = SB_ERROR_NONE;
						}
						break;
				
					case MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_DDR_R: 
						if(msgPtr->msgLen == 0)
						{
							retError = SB_ERROR_NONE;
						}
						break;
					
					case MODULE_MSG_ID_BLTST_I2C_EXP_READ_PIN_R: 
						if(msgPtr->msgLen == sizeof(TestI2cExpPinReadRspStr))
						{
							I2cExpReadPinPtr = (TestI2cExpPinReadRspStr*)msgPtr->msgBufPtr;
							retError = SB_ERROR_NONE;
						}
						break;

					case MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_PIN_R: 
						if(msgPtr->msgLen == 0)
						{
							retError = SB_ERROR_NONE;
						}
						break;

					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;
			default:
				printf("\n Error in Downloading ");
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */