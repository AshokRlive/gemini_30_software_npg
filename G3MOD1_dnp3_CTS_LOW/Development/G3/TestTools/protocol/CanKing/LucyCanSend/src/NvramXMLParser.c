/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: NvramFactoryInfoWrite.c 3512 2013-07-08 11:35:05Z saravanan_v $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 3512 $: (Revision of last commit)
 *               $Author: saravanan_v $: (Author of last commit)
 *       \date   $Date: 2013-07-08 12:35:05 +0100 (Mon, 08 Jul 2013) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/07/12     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ModuleProtocol.h"

#include "NVRAMDef.h"
#include "NVRAMDefOptPSM.h"
#include "NVRAMDefOptHMI.h"
#include "NVRAMDefOptBat.h"

#include "CmdParser.h"

#include "NvramXMLParser.h"

#include "crc32.h"
#include "roxml.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
const xmlInfoStr factoryInfoMap[FACTORYINFO_FIELD_MAX] = 
{   
	{"lu_uint8_t",  "InfoBlockVersionMajor",   "0",	"0",                              "0", "Info Block Version Major"   },
	{"lu_uint8_t",  "InfoBlockVersionMinor",   "1",	"0",							  "0", "Info Block Version Minor"   },
	{"lu_uint8_t",  "moduleType",              "0",	"0",							  "0", "Module Type"                },
	{"lu_uint32_t", "moduleFeatureMajor",      "0",	"0",							  "0", "Module Feature Major"       },
	{"lu_uint32_t", "moduleFeatureMinor",      "1",	"0",							  "0", "Module Feature Minor"       },
	{"lu_uint8_t",  "batchNumber",             "0",	"NVRAMINFO_BATCH_MAX_SIZE",       "0", "Batch Number"               },
	{"lu_uint8_t",  "supplierId",              "0",	"NVRAMINFO_SUPPLY_ID_MAX_SIZE",   "0", "Supplier ID"                },
	{"lu_uint8_t",  "assemblyNo",              "0",	"NVRAMINFO_ASSY_NO_MAX_SIZE",     "0", "Assembly Number"            },
	{"lu_uint8_t",  "assemblyRev",             "0",	"NVRAMINFO_ASSY_REV_MAX_SIZE",    "0", "Assembly Revision"          },
	{"lu_uint32_t", "serialNumber",            "0",	"0",							  "0", "Module Serial Number"       },
	{"lu_uint8_t",  "subAssemblyNo",           "0", "NVRAMINFO_ASSY_NO_MAX_SIZE",     "0", "Sub Assembly Number"        },
	{"lu_uint8_t",  "subAssemblyRev",          "0", "NVRAMINFO_ASSY_REV_MAX_SIZE",    "0", "Sub Assembly Revision"      },
	{"lu_uint32_t", "subAssemblySerialNumber", "0",	"0",							  "0", "Sub Assembly Serial Number" },
	{"lu_uint8_t",  "subAssemblyBatchNumber",  "0", "NVRAMINFO_BATCH_MAX_SIZE",       "0", "Sub Assembly Batch Number"  },
	{"lu_uint8_t",  "buildDay",                "0",	"0",							  "0", "Build Day"					},
	{"lu_uint8_t",  "buildMonth",              "0",	"0",							  "0", "Build Month"				},
	{"lu_uint16_t", "buildYear",               "0",	"0",							  "0", "Build Year"					}
};

const xmlInfoStr psmOptionsMap[PSM_OPTIONS_FIELD_MAX] = 
{   
	{"lu_uint8_t",  "optBlockVersionMajor",   "0",	"0",   "0", "Options Block Version Major" },
	{"lu_uint8_t",  "optBlockVersionMinor",   "1",	"0",   "0", "Options Block Version Minor" },
	{"lu_uint8_t",  "commsPrimaryFitted",     "0",	"0",   "0", "Comms Primary Fitted"		  },
	{"lu_uint8_t",  "commsSecondaryFitted",   "0",	"0",   "0", "Comms Secondary Fitted"      },
	{"lu_uint8_t",  "commsPrimaryVolts",      "0",	"0",   "0", "Comms Primary Volts"         },
	{"lu_uint8_t",  "commsSecondaryVolts",    "0",	"0",   "0", "Comms Secondary Volts"       }
};

const xmlInfoStr hmiOptionsMap[HMI_OPTIONS_FIELD_MAX] = 
{   
	{"lu_uint8_t",  "optBlockVersionMajor",   "0",	"0",   "0", "Options Block Version Major" },
	{"lu_uint8_t",  "optBlockVersionMinor",   "1",	"0",   "0", "Options Block Version Minor" },
	{"lu_uint8_t",  "displayType",            "0",	"0",   "0", "Display Type"				  },
	{"lu_uint8_t",  "contrastSetDigipot",     "0",	"0",   "0", "Contrast Set Digipot Value"  }
};

const xmlInfoStr batteryOptionsMap[BATTERY_OPTIONS_FIELD_MAX] = 
{   
	{"lu_uint8_t",  "BatteryChemistry",						 "0",	 "0",   "0", "Battery Chemistry"					 },
	{"lu_uint32_t", "BatteryCellNominalVolts",				 "0",	 "0",   "0", "Battery Cell Nominal Volts"			 },
	{"lu_int32_t",  "BatteryCellFullChargeVolts",			 "0",	 "0",   "0", "Battery Cell Full Charge Volts"		 },
	{"lu_int32_t",  "BatteryCellMaxCapacity",				 "0",	 "0",   "0", "Battery Cell Max Capacity"			 },
	{"lu_int32_t",  "BatteryCellChargingCurrent",			 "0",	 "0",   "0", "Battery Cell Charging Current"		 },
	{"lu_int32_t",  "BatteryCellChargingVolts",				 "0",	 "0",   "0", "Battery Cell Charging Volts"			 },
	{"lu_int32_t",  "BatteryCellLeakage",					 "0",	 "0",   "0", "Battery Cell Leakage"					 },
	{"lu_int32_t",  "BatteryCellFloatCharge",				 "0",	 "0",   "0", "Battery Cell Float Charge"			 },
	{"lu_int8_t",   "BatteryCellChargingMethod",			 "0",	 "0",   "0", "Battery Cell Charging Method"			 },
	{"lu_int8_t",   "BatteryPackNoOfCols",					 "0",	 "0",   "0", "Battery Pack No Of Cols"				 },
	{"lu_int8_t",   "BatteryPackNoOfRows",					 "0",	 "0",   "0", "Battery Pack No Of Rows"				 },
	{"lu_int32_t",  "BatteryPackMaxChargeTemp",				 "0", 	 "0",   "0", "Battery Pack Max Charge Temp"			 },
	{"lu_int32_t",  "BatteryPackMaxChargeTime",				 "0",	 "0",   "0", "Battery Pack Max Charge Time"			 },
	{"lu_int32_t",  "BatteryPackTimeBetweenCharge",			 "0",	 "0",   "0", "Battery Pack Time Between Charge"		 },
	{"lu_int32_t",  "BatteryPackOverideTime",				 "0",	 "0",   "0", "Battery Pack Overide Time"			 },
	{"lu_int32_t",  "BatteryPackChargeTrigger",				 "0",	 "0",   "0", "Battery Pack Charge Trigger"			 },
	{"lu_int32_t",  "BatteryPackThresholdVolts",			 "0",	 "0",   "0", "Battery Pack Threshold Volts"			 },
	{"lu_int32_t",  "BatteryPackLowLevel",					 "0",	 "0",   "0", "Battery Pack Low Level"				 },
	{"lu_uint8_t",  "BatteryTestTimeMinutes",		         "0",    "0",   "0", "Battery Test Time Minutes"	         },
	{"lu_uint16_t", "BatteryTestCurrentMinimum",		     "0",    "0",   "0", "Battery Test Current Minimum"	         },
	{"lu_uint8_t",  "BatteryTestSuspendTimeMinutes",		 "0",    "0",   "0", "Battery Test Suspend Time Minutes"     },
	{"lu_uint16_t", "BatteryTestMaximumDurationTimeMinutes", "0",    "0",   "0", "Battery Test Maximum Duration Time Minutes" },
	{"lu_uint8_t",  "BatteryTestDischargeCapacity",		     "0",    "0",   "0", "Battery Test Discharge Capacity"       },
	{"lu_uint16_t", "BatteryTestThresholdVolts",		     "0",    "0",   "0", "Battery Test Threshold Volts"          },
	{"lu_uint8_t",  "BatteryTestLoadType",		             "0",    "0",   "0", "Battery Test Load Type"                },
	{"lu_uint16_t", "BatteryPackShutdownLevelVolts",		 "0",    "0",   "0", "Battery Pack Shutdown Level Volts"     },
	{"lu_uint16_t", "BatteryPackDeepDischargeVolts",		 "0",    "0",   "0", "Battery Pack Deep Discharge Volts"     }
};
/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Fetching data from Factory Information XML file
 *
 *   Fetching data from factory information from XML file and store into factoryinfo 
 *   structure to send to slave board's ID memory.
 *  
 *   \param  XMLfilename         File name of the XML file 
 *   \param  factoryInfoFromXML  Pointer to the structure of factoryInformation
 *
 *   \return SB_ERROR     
 *
 ******************************************************************************
 */
SB_ERROR FetchAndPackFactoryInfoFromXML( lu_uint8_t*     XMLfilename,
										 lu_uint8_t      numberOfTags,
										 const xmlInfoStr     dataFromXML[],
										 const strutureMapStr strTable[]
									   )
{
	SB_ERROR retError = SB_ERROR_PARAM;
	int i;
	node_t *root, *sourceFile, *structDef, *item;	
	
	/* Opening an XML file using roxml  */
	root = roxml_load_doc((lu_int8_t*)XMLfilename);
	if(root == NULL)
	{
		retError = SB_ERROR_PARAM;
	}

	else
	{
		/* Traversing into XML child tags */
		sourceFile      = roxml_get_chld(root,       "SourceFile",  0);
		structDef       = roxml_get_chld(sourceFile, "StructDef",   0);
		item            = roxml_get_chld(structDef,  "Item",        0);

		for(i = 0; i < numberOfTags; i++)
		{
			if(i == 0)
			{
				retError = StripAndGetValueFromXMLTag(item, strTable[i].elementPtr, dataFromXML[i], i);
			}
			else
			{
				item = roxml_get_next_sibling(item);
				if(retError == SB_ERROR_NONE)
				{
					retError = StripAndGetValueFromXMLTag(item, strTable[i].elementPtr, dataFromXML[i], i);
				}
			}
		}
	}
	roxml_close(root);

	return retError;
}


SB_ERROR BundleNVRAMDataToXMLFile( lu_uint8_t *readBuffer,
								   lu_int8_t  *fileName,
								   lu_uint8_t numberOfItemTags,
								   const strutureMapStr *elementMapStrPtr,
								   const xmlInfoStr *xmlTagMapStrPtr
								 )
{
	SB_ERROR retError = SB_ERROR_NONE;
	lu_uint16_t i, j;
	FILE *filePtr;
	lu_int8_t valueString[16];

	filePtr = fopen(fileName,"wb");
	if(filePtr == NULL)
	{
		retError = SB_ERROR_PARAM;
	}

	fputs("<SourceFile>\n<StructDef>\n", filePtr);

	for(i = 0; i < numberOfItemTags; i++)
	{
		
		if(i != 0)
		{
			++xmlTagMapStrPtr;
			++elementMapStrPtr;
		}
		
		fputs("<Item", filePtr);

		fputs(" type=\"", filePtr);
		fputs(xmlTagMapStrPtr->xmlInfoType, filePtr);
		fputs("\"", filePtr);

		fputs(" name=\"", filePtr);
		fputs(xmlTagMapStrPtr->xmlInfoName, filePtr);
		fputs("\"", filePtr);

		fputs(" dim=\"", filePtr);
		fputs(xmlTagMapStrPtr->xmlInfoDim, filePtr);
		fputs("\"", filePtr);

		fputs(" value=\"", filePtr);

		if((strcmp(xmlTagMapStrPtr->xmlInfoType,"lu_uint32_t")) == 0)
		{
			itoa(*((lu_uint32_t*)elementMapStrPtr->elementPtr), valueString, 10);
			fputs(valueString, filePtr);
		}

		else if((strcmp(xmlTagMapStrPtr->xmlInfoType,"lu_uint16_t")) == 0)
		{
			itoa(*((lu_uint16_t*)elementMapStrPtr->elementPtr), valueString, 10);
			fputs(valueString, filePtr);
		}

		else if((strcmp(xmlTagMapStrPtr->xmlInfoType,"lu_int32_t" )) == 0)
		{
			itoa(*((lu_int32_t*)elementMapStrPtr->elementPtr), valueString, 10);
			fputs(valueString, filePtr);
		}

		else if((strcmp(xmlTagMapStrPtr->xmlInfoType,"lu_int16_t")) == 0)
		{
			itoa(*((lu_int16_t*)elementMapStrPtr->elementPtr), valueString, 10);
			fputs(valueString, filePtr);
		}

		else if((strcmp(xmlTagMapStrPtr->xmlInfoType, "lu_uint8_t") == 0) && 
			    (strcmp(xmlTagMapStrPtr->xmlInfoDim,  "0"         ) == 0)
			   )
		{
			itoa(*((lu_uint8_t*)elementMapStrPtr->elementPtr), valueString, 10);
			fputs(valueString, filePtr);
		}

		else if((strcmp(xmlTagMapStrPtr->xmlInfoType, "lu_int8_t") == 0) && 
			    (strcmp(xmlTagMapStrPtr->xmlInfoDim,  "0"        ) == 0)
			   )
		{
			itoa(*((lu_int8_t*)elementMapStrPtr->elementPtr), valueString, 10);
			fputs(valueString, filePtr);
		}
		
		else
		{
			j = 0;
			
			strncpy(valueString, (lu_int8_t*)elementMapStrPtr->elementPtr, elementMapStrPtr->size);

			while(j != elementMapStrPtr->size)
			{
				if((lu_int8_t*)valueString[j] == '\0' )
				{
					break;
				}
				j++;
			}
			
			//fwrite(elementMapStrPtr->elementPtr, elementMapStrPtr->size, 1, filePtr);
			fwrite(elementMapStrPtr->elementPtr, j, 1, filePtr);
		}

		fputs("\"", filePtr);

		fputs(" description=\"", filePtr);
		fputs(xmlTagMapStrPtr->xmlInfoDescription, filePtr);
		fputs("\"", filePtr);

		fputs("/>\n", filePtr);
	}
	fputs("</StructDef>\n</SourceFile>", filePtr);
	fclose(filePtr);

	return retError;
}

/*!
 ******************************************************************************
 *   \brief To get values from single XML tag
 *
 *   To get value from XML tag and store it in correcsponding field of factory
 *   information structure
 *   
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR StripAndGetValueFromXMLTag( node_t*     XMLTag, 
									 void*       valueFromXMLTag, 
									 xmlInfoStr  xmlMapElement,
									 lu_uint8_t  mapIndex
								   )
{
	SB_ERROR retError = SB_ERROR_PARAM;
	int len;
	lu_int8_t *type, *name, *defaultValue, *dim, *value, *description;
	node_t    *itemType, *itemName, *itemDefaultValue, *itemDim, *itemValue, *itemDescription; 
	
	/* Get description attribute */
	itemType         = roxml_get_attr(XMLTag, "type",          0);
	itemName         = roxml_get_attr(XMLTag, "name",          0);
	itemDefaultValue = roxml_get_attr(XMLTag, "default",       0);
	itemDim          = roxml_get_attr(XMLTag, "dim",           0);
	itemValue        = roxml_get_attr(XMLTag, "value",         0);
	itemDescription  = roxml_get_attr(XMLTag, "description",   0);
	
	/* Get content of description attribute */
	type         = roxml_get_content(itemType,         NULL, 0, &len);
	name         = roxml_get_content(itemName,         NULL, 0, &len);
	defaultValue = roxml_get_content(itemDefaultValue, NULL, 0, &len);
	dim          = roxml_get_content(itemDim,          NULL, 0, &len);
	value        = roxml_get_content(itemValue,        NULL, 0, &len);
	description  = roxml_get_content(itemDescription,  NULL, 0, &len);

	printf("\nName = %-24s, Value = %s\n", name, value);

	if ( (strcmp( type,         xmlMapElement.xmlInfoType         ) == 0) &&
		 (strcmp( name,         xmlMapElement.xmlInfoName         ) == 0) &&
	  // (strcmp( defaultValue, xmlMapElement.xmlInfoDefault      ) == 0) &&
	     (strcmp( dim,          xmlMapElement.xmlInfoDim          ) == 0) //&&
	  // (strcmp( description,  xmlMapElement.xmlInfoDescription  ) == 0) 
	   )
	{
		if( (strcmp(type, "lu_bool_t") == 0) && 
			(strcmp(dim,  "0"        ) == 0)
		  )
		{
			*(lu_uint8_t*)valueFromXMLTag = strtoul(value, NULL, 10);
			retError = SB_ERROR_NONE;

		}
		
		if( ((strcmp(type, "lu_uint8_t") == 0) || 
			 (strcmp(type, "lu_int8_t" ) == 0)) && 
			(strcmp(dim, "0"          ) == 0)
		  )
		{
			*(lu_uint8_t*)valueFromXMLTag = strtoul(value, NULL, 10);
			retError = SB_ERROR_NONE;

		}

		if( ((strcmp(type, "lu_uint16_t") == 0) || 
			 (strcmp(type, "lu_int16_t" ) == 0)) && 
		    (strcmp(dim,  "0"          ) == 0)
		  )
		{
			*(lu_uint16_t*)valueFromXMLTag = strtoul(value, NULL, 10);
			retError = SB_ERROR_NONE;
		}

		if( ((strcmp(type, "lu_uint32_t") == 0) || 
			 (strcmp(type, "lu_int32_t" ) == 0)) && 
			(strcmp(dim,  "0"          ) == 0)
		  )
		{
			*(lu_uint32_t*)valueFromXMLTag = strtoul(value, NULL, 10);
			retError = SB_ERROR_NONE;
		}

		if( ((strcmp(type, "lu_uint8_t")  == 0) || 
			 (strcmp(type, "lu_int8_t")   == 0)) && 
			(strcmp(dim,  "0"         )  == 0)
		  )
		{
			*(lu_int8_t*)valueFromXMLTag = strtoul(value, NULL, 10);
			retError = SB_ERROR_NONE;
		}
		    		    
		if( ((strcmp(type, "lu_uint8_t")  == 0) || 
			 (strcmp(type, "lu_int8_t")   == 0)) && 
			(strcmp(dim,  "0"         )  != 0)
		  )
		{
			strcpy((lu_int8_t*)valueFromXMLTag, value);
			retError = SB_ERROR_NONE;
		}
	}

	return retError;
}

void ClearNVRamBuffer(lu_uint8_t buffer[], lu_uint8_t size)
{
	lu_uint16_t i;

	for(i = 0; i < size; i++)
	{
		buffer[i] = 0xFF;
	}
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
*/