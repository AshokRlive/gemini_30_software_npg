/*! \file
******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
******************************************************************************
*    PROJECT:
*       G3 [Module Name]
*
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*       \brief
*
*       CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*
*
*    CREATION
*
*   Date          Name        Details
*   --------------------------------------------------------------------------
*   08/08/2012    venkat_s    Initial version / Original by Venkat.
*
******************************************************************************
*   COPYRIGHT
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
******************************************************************************
*/


/*
******************************************************************************
* INCLUDES - Standard Library Modules Used
******************************************************************************
*/

/*
******************************************************************************
* INCLUDES - Project Specific Modules Used
******************************************************************************
*/
#include "lu_types.h"

#include "Main.h"
#include "CmdParser.h"
#include "CmdExecutorDLL.h"

#include "SingleCmdSender.h"

#include "FirmwareDownloader.h"
#include "FirmwareVerifier.h"

#include "CalElementAdder.h"
#include "CalElementReader.h"

#include "NvramFactoryInfoWrite.h"
#include "NvramFactoryInfoRead.h"

#include "NvramPSMOptionsWrite.h"
#include "NvramPSMOptionsRead.h"

#include "NvramHMIOptionsWrite.h"

#include "NvramBatteryOptionsWrite.h"
#include "NvramBatteryOptionsRead.h"



/*
******************************************************************************
* LOCAL - Definitions And Macros
******************************************************************************
*/

/*
******************************************************************************
* LOCAL - Typedefs and Structures
******************************************************************************
*/


/*
*******************************************************************************
* LOCAL - Prototypes Of Local Functions
*******************************************************************************
*/


/*
******************************************************************************
* EXPORTED - Variables
******************************************************************************
*/


/*
******************************************************************************
* LOCAL - Global Variables
******************************************************************************
*/

/*
******************************************************************************
* Exported Functions
******************************************************************************
*/

SB_ERROR CommandExecutorInitialize(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR retError = SB_ERROR_NONE;

	switch(argsFromCommand->parserFlag)
	{
	case PARSER_FLAG_FIRMWARE_DOWNLOAD:
		{
			retError = FirmwareDownloaderFilePrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_FIRMWARE_VERIFY:
		{
			retError = FirmwareVerifierFilePrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_CAL_ADD_ELEMENT:
		{
			retError = CalElementAdderPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_CAL_READ_ELEMENT:
		{
			retError = CalElementReaderPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_FACTORY:
		{
			retError = NvramWriteFactoryInfoPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_READ_FACTORY:
		{
			retError = NvramReadFactoryInfoPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_PSM_OPTS:
		{
			retError = NvramWritePSMOptionsPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_HMI_OPTS:
		{
			retError = NvramWriteHMIOptionsPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_READ_HMI_OPTS:
		{
			retError = NvramReadHMIOptionsPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_READ_PSM_OPTS:
		{
			retError = NvramReadPSMOptionsPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_BATT_OPTS:
		{
			retError = NvramWriteBatteryOptionsPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_READ_BATT_OPTS:
		{
			retError = NvramReadBatteryOptionsPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_SINGLE_COMMAND:
		{
			retError = SendSingleCommandPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_CAL_BLOCK_WRITE:
		{
			retError = SB_ERROR_NONE;
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_VERIFY:
		{
			retError = SB_ERROR_NONE;
		}
		break;

	default:
		{
			printf("No Command has given. Please see Help for Details...\n\n");
			retError = SB_ERROR_PARAM;
		}
		break;
	}

	return retError;
}

lu_bool_t CommandExecutorRun(ParsingArgsStr *argsFromCommand)
{
    lu_bool_t retError = LU_FALSE;

	switch(argsFromCommand->parserFlag)
	{
	case PARSER_FLAG_FIRMWARE_DOWNLOAD:
		{
			retError = FirmwareDownloaderBlockSend(&firmwareFile, argsFromCommand);
		}
		break;

	case PARSER_FLAG_FIRMWARE_VERIFY:
		{
			retError = FirmwareVerifierGetBlock(&srcFirmwareFile, &dstFirmwareFile, argsFromCommand);
		}
		break;

	case PARSER_FLAG_CAL_ADD_ELEMENT:
		{
			retError = CalElementAdderSend(argsFromCommand);
		}
		break;

	case PARSER_FLAG_CAL_READ_ELEMENT:
		{
			retError = CalElementReaderSend(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_FACTORY:
		{
			retError = NvramWriteFactoryInfo(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_READ_FACTORY:
		{
			retError = NvramReadFactoryInfo(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_PSM_OPTS:
		{
			retError = NvramWritePSMOptions(argsFromCommand);
		}
		break;

	 case PARSER_FLAG_NVRAM_WRITE_HMI_OPTS:
		{
			retError = NvramWriteHMIOptions(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_READ_PSM_OPTS:
		{
			retError = NvramReadPSMOptions(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_READ_HMI_OPTS:
		{
			retError = NvramReadHMIOptions(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_BATT_OPTS:
		{
			retError = NvramWriteBatteryOptions(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_READ_BATT_OPTS:
		{
			retError = NvramReadBatteryOptions(argsFromCommand);
		}
		break;

	case PARSER_FLAG_SINGLE_COMMAND:
		{
			retError = SendSingleCommand(argsFromCommand);
		}
		break;
	default:
		{
			printf("Error in Command Execution");
		}
		break;
	}

	return retError;
}

/*
******************************************************************************
* Local Functions
******************************************************************************
*/


/*!
******************************************************************************
*   \brief Brief description
*
*   Detailed description
*
*   \param parameterName Description 
*
*
*   \return 
*
******************************************************************************
*/

/*
*********************** End of file ******************************************
*/

