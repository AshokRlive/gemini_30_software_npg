/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *  28/10/15      saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "mainDll.h"
#include "FileCompare.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define BASE_10	 10

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
 
/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
lu_uint16_t	 FileCompare( lu_int8_t   *fileOriginal, 
						  lu_int8_t   *fileFromBoard,
					      lu_int32_t  *errorLine,
					      lu_int32_t  *errorColumn
					    )
{
	FILE *fileOriginalfptr, *fileFromBoardfptr; 
	lu_int32_t sourceChar, destinationChar, errorByte = 0, line = 0;
	lu_uint16_t returnCode;

	fileFromBoardfptr = fopen(fileFromBoard, "rb");
	if(fileFromBoardfptr == NULL)
	{
		returnCode = FILE_2_PATH_INVALID;
		fclose(fileFromBoardfptr);
	}
	
	else
	{
		fileOriginalfptr = fopen(fileOriginal, "rb");
		if(fileOriginalfptr == NULL)
		{
			returnCode = FILE_1_PATH_INVALID;
			fclose(fileOriginalfptr);
		} 

		sourceChar       = fgetc(fileOriginalfptr);
		destinationChar  = fgetc(fileFromBoardfptr);

		while((sourceChar != EOF)			  && 
			  (destinationChar != EOF)		  && 
			  (sourceChar == destinationChar)
			 )
		{
			sourceChar       = fgetc(fileOriginalfptr);
			destinationChar  = fgetc(fileFromBoardfptr);

			errorByte++;
			if(sourceChar == '\r' || sourceChar == '\n')
			{
				line++;
				errorByte = 0;
			}
		}
	
		if (sourceChar == destinationChar )   
		{
				returnCode = FILE_CHECK_OK;   
				*errorLine   =  -1;
				*errorColumn =  -1;
		}
		else
		{
			returnCode = FILE_MISMATCH;
			*errorLine   =  line;
			*errorColumn =  errorByte;
			
		}

		fclose(fileOriginalfptr);
		fclose(fileFromBoardfptr);
	}
	
	return returnCode;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */