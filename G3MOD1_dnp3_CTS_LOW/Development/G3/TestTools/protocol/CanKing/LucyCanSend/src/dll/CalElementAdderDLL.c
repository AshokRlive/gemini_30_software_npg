/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *  18/12/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "CalElementAdderDll.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
TestErrorRspStr *nvramWriteResp;
 
/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR CalElementAdderDll( MODULE module, 
							 MODULE_ID moduleId,
							 lu_uint8_t calID,
							 lu_uint8_t calType,
							 lu_int8_t *calElementFile
					   		)
{
	SB_ERROR retError;
	lu_uint8_t fileNameLen;
		
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);
	
	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_CALTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_CALTST_ADD_ELEMENT_C;

	argsFromCommand.parserFlag = PARSER_FLAG_CAL_ADD_ELEMENT;
	
	argsFromCommand.calID   = calID;
	argsFromCommand.calType = calType;

	fileNameLen = strlen(calElementFile);

	memset(argsFromCommand.CalElementFileName, 0x00, sizeof(argsFromCommand.CalElementFileName));
	memcpy(argsFromCommand.CalElementFileName, calElementFile, fileNameLen);
		
	retError = LucyCANSendMainDll();

	return retError;
}

SB_ERROR CalElementAdderCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_CANC_NOT_HANDLED;
	CalTstAddElementRspStr *addCalElementResponse;
	
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_CAL_ADD_ELEMENT)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_CALTST:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_CALTST_ADD_ELEMENT_R: 
						
						addCalElementResponse = (CalTstAddElementRspStr*)msgPtr->msgBufPtr;

						if( addCalElementResponse->calID  == flagDecoderPtr->calID &&
						    addCalElementResponse->status == LU_TRUE	 
						  )
						{
							retError = SB_ERROR_NONE;
						}
						break;

					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;
			default:
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */