/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "Main.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "FirmwareDownloader.h"
#include "FirmwareVerifier.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void CmdParserUsage(void)
{
	printf("LucyCanSend [-mt=] [-mid=] [-dt=] [-did=] [data......]\n\n");
	printf(" -mt Message type\n");
	printf(" -mid Message ID\n");
	printf(" -dt Destination type (0=PSM; 2=SCM; 3=FDM; 4=HMI)\n");
	printf(" -did Destination ID (ID = 0 ,1 ,2,...)\n\n");
	printf(" (Data bytes in Hex)\n");
	printf("-loop=n Run this command for \'n\' times... Should be at end of the command");
	printf("\nExample (CFG_SWITCH to SCM ):- LucyCanSend -dt=2 -mt=3d -mid=8 0 0 0 0 0 0 0 0\n");
	

	printf("\n -FD Downloading firmware to the slave board");
	printf("\nExample: LucyCanSend -dt=2 -mt=3d -mid=8 -FD=filename.bin\n");

	printf("\n -FVS  Firmware Verifying Source");
	printf("\n -FVD  Firmware Verifying Destination");
	printf("\nExample:-dt=0 -mt=2a -mid=04 -FVS=templateBoardNXP.bin -FVD=ReadFirmware.bin\n");
	
	printf("\n -PD Downloading PIC firmware to the slave board");
	printf("\nExample: LucyCanSend -dt=7 -PD=filename.hex\n");

	printf("\n -calid        Calibration ID");
	printf("\n -caltype      Calibration Type");
	printf("\n -caladdfile   Calibration Element File");
	printf("\n -calreadfile  Calibration Element Reading File");
	
	printf("\nExample: -dt=0 -mt=2f -mid=04 -calid=09 -caltype=1 -caladdfile=CalibrationElement.csv\n");
	printf("\nExample: -dt=0 -mt=2f -mid=04 -calid=09 -caltype=1 -calreadfile=CalibrationElement.csv\n");

	printf("\n -factory=           ID or APP Factory(Primary) or  Application(Backup) NVRAM");
	printf("\n -factory=APP        Application NVRAM (Backup factory)");
	printf("\n -factoryreadfile=filename.csv");
	printf("\n -factorywritefile=filename.csv");
	printf("\nExample: -factory=APP -factorywritefile=factory.csv");
	printf("\nExample: -factory=ID -factorywritefile=factory.csv");
	printf("\nExample: -factory=ID -factoryreadfile=factory.csv");
}


void CmdParser(int argc, char* argv[], ParsingArgsStr *argsFromCommand)
{
	lu_uint32_t	value;
	lu_uint16_t msgLength   = 0;
	lu_uint32_t data;
	lu_bool_t   debugEnable;
	int i;
    lu_int8_t tempStr[800];
			
	debugEnable   = LU_FALSE;
		
	/* Set defaults for Can Header */
	argsFromCommand->CANHeader.deviceDst   = MODULE_PSM;
	argsFromCommand->CANHeader.deviceIDDst = MODULE_ID_0;
	argsFromCommand->CANHeader.deviceSrc   = MODULE_MCM;
	argsFromCommand->CANHeader.deviceIDSrc = MODULE_ID_0;
	argsFromCommand->CANHeader.fragment    = 0;
	argsFromCommand->CANHeader.messageType = 0;
	argsFromCommand->CANHeader.messageID   = 0;

	argsFromCommand->nvramAddr = 0x14; // Default for ID NVRAM

	if (argc < 1)
	{
		printf("no args");
		argsFromCommand->msgBuffLen = 0;
		return ;
	}

	for(i = 1; i < argc; i++)
	{
		if (sscanf_s(argv[i], "-debug") == 1)
		{
			debugEnable = LU_TRUE;
		}
	}

	for(i = 1; i < argc; i++)
	{
		
		/* Get message type */
		if (sscanf_s(argv[i], "-mt=%x", &value) == 1)
		{ 
			argsFromCommand->CANHeader.messageType = value; 
		}

		/* Get message ID */
		else if (sscanf_s(argv[i], "-mid=%x", &value) == 1)
		{
			argsFromCommand->CANHeader.messageID = value;
			argsFromCommand->parserFlag = PARSER_FLAG_SINGLE_COMMAND;
		}

		/* Get Destination (FDM/SCM/HMI/BRD/PSM/etc) */
		else if (sscanf_s(argv[i], "-dt=%x", &value) == 1)
		{
			argsFromCommand->CANHeader.deviceDst = value;
		}

		/* Get Destination Address */
		else if (sscanf_s(argv[i], "-did=%x", &value) == 1)
		{
			argsFromCommand->CANHeader.deviceIDDst = value;
		}

		/* To Send a file on CAN bus */
		else if (sscanf(argv[i], "-FD=%s",argsFromCommand->sourceFileName) == 1)
		{
			printf("\n  --- Firmware Downloader ---\n\n");
			printf(" Firmware File : %s",argsFromCommand->sourceFileName);
			argsFromCommand->parserFlag = PARSER_FLAG_FIRMWARE_DOWNLOAD;
			
		}

		/* To Receive a file on CAN bus */
		else if (sscanf(argv[i], "-FVS=%s",argsFromCommand->sourceFileName) == 1)
		{
			printf("\n --- Firmware Verifier ---\n ");
			printf("\n Original Firmware  = %s",argsFromCommand->sourceFileName);
			argsFromCommand->parserFlag = PARSER_FLAG_FIRMWARE_VERIFY;
			
		}

		else if (sscanf(argv[i], "-FVD=%s",argsFromCommand->destinationFileName) == 1)
		{
			printf("\n Board Firmware     = %s\n",argsFromCommand->destinationFileName);

		}

		else if (sscanf(argv[i], "-PD=%s",argsFromCommand->sourceFileName) == 1)
		{
			printf("\n --- PIC Firmware Download ---\n ");
			printf("\n Original Firmware  = %s",argsFromCommand->sourceFileName);
			argsFromCommand->parserFlag = PARSER_FLAG_PIC_FIRMWARE_DOWNLOAD;
		}

		/* To Receive Factory NVRAM (INFO block) as a file on CAN bus */
		else if (sscanf(argv[i], "-factoryreadfile=%s",argsFromCommand->destinationFileName) == 1)
		{
			printf("\n --- NVRAM Read -> Factory Information ---\n ");
			printf("\n Filename = %s",argsFromCommand->destinationFileName);
			argsFromCommand->parserFlag = PARSER_FLAG_NVRAM_READ_FACTORY;
		}

		/* To Receive Factory NVRAM (INFO block) as a file on CAN bus */
		else if (sscanf(argv[i], "-factorywritefile=%s",argsFromCommand->sourceFileName) == 1)
		{
			printf("\n --- NVRAM Write -> Factory Information ---\n ");
			printf("\n Filename = %s",argsFromCommand->sourceFileName);
			argsFromCommand->parserFlag = PARSER_FLAG_NVRAM_WRITE_FACTORY;
		}

		else if (sscanf(argv[i], "-psmoptreadfile=%s",argsFromCommand->destinationFileName) == 1)
		{
			printf("\n --- NVRAM Read -> PSM Options ---\n ");
			printf("\n Filename = %s",argsFromCommand->destinationFileName);
			argsFromCommand->parserFlag = PARSER_FLAG_NVRAM_READ_PSM_OPTS;
		}

		else if (sscanf(argv[i], "-psmoptwritefile=%s",argsFromCommand->sourceFileName) == 1)
		{
			printf("\n --- NVRAM Write -> PSM Options ---\n ");
			printf("\n Filename = %s",argsFromCommand->sourceFileName);
			argsFromCommand->parserFlag = PARSER_FLAG_NVRAM_WRITE_PSM_OPTS;
		}

		else if (sscanf(argv[i], "-battoptwritefile=%s",argsFromCommand->sourceFileName) == 1)
		{
			printf("\n --- NVRAM Write -> Battery Options ---\n ");
			printf("\n Filename = %s",argsFromCommand->sourceFileName);
			argsFromCommand->parserFlag = PARSER_FLAG_NVRAM_WRITE_BATT_OPTS;
		}

		else if (sscanf(argv[i], "-battoptreadfile=%s",argsFromCommand->destinationFileName) == 1)
		{
			printf("\n --- NVRAM Read -> Battery Options ---\n ");
			printf("\n Filename = %s",argsFromCommand->destinationFileName);
			argsFromCommand->parserFlag = PARSER_FLAG_NVRAM_READ_BATT_OPTS;
		}

		/* To Factory NVRAM Type */
		else if (strcmp(argv[i], "-factory=ID") == 0)
		{
			argsFromCommand->nvramAddr       = ID_NVRAM_ADDRESS; // Default for ID NVRAM
			argsFromCommand->nvramI2CChannel = ID_NVRAM_I2C_CHANNEL; 
		}

		else if (strcmp(argv[i], "-factory=APP") == 0)
		{
			argsFromCommand->nvramAddr       = APP_NVRAM_ADDRESS; 
			argsFromCommand->nvramI2CChannel = APP_NVRAM_I2C_CHANNEL; 
		}
		
		else if (strcmp(argv[i], "-factory=BATTID") == 0)
		{
			argsFromCommand->nvramAddr       = BATT_ID_NVRAM_ADDRESS; 
			argsFromCommand->nvramI2CChannel = BATT_ID_NVRAM_I2C_CHANNEL; 
		}

		else if (strcmp(argv[i], "-factory=BATTDATA") == 0)
		{
			argsFromCommand->nvramAddr       = BATT_DATA_NVRAM_ADDRESS; 
			argsFromCommand->nvramI2CChannel = BATT_DATA_NVRAM_I2C_CHANNEL; 
		}

		else if (sscanf(argv[i], "-calid=%x",&value) == 1)
		{
			argsFromCommand->calID = value;
		}

		else if (sscanf(argv[i], "-caltype=%x",&value) == 1)
		{
			argsFromCommand->calType = value;
		}

		else if (sscanf(argv[i], "-caladdfile=%s",argsFromCommand->CalElementFileName) == 1)
		{
			printf(" --- Calibration Element Adder ---\n ");
			printf("\n Calibration Element File = %s\n",argsFromCommand->CalElementFileName);
			argsFromCommand->parserFlag = PARSER_FLAG_CAL_ADD_ELEMENT;
		
		}

		else if (sscanf(argv[i], "-calreadfile=%s",argsFromCommand->destinationFileName) == 1)
		{
			printf(" --- Calibration Element Reader ---\n ");
			argsFromCommand->parserFlag = PARSER_FLAG_CAL_READ_ELEMENT;
			printf("\n Calibration Element ID = %x\n",argsFromCommand->calID);
		}

		else if(sscanf_s(argv[i], "-t=%x",&argsFromCommand->runInterval) == 1)
		{
			printf(" --- Single Command Loop Runner ---\n ");
			printf("  ---> With interval %d ms <---\n", argsFromCommand->runInterval);
		}
		
		else if(sscanf_s(argv[i], "-loop=%x",&argsFromCommand->numberOfRun) == 1)
		{
			argsFromCommand->parserFlag = PARSER_FLAG_SINGLE_COMMAND_LOOP;
		}

		/* Get decimal data payload */
		else if (sscanf_s(argv[i], "%x", &data) == 1)
		{
			argsFromCommand->parserFlag = PARSER_FLAG_SINGLE_COMMAND;
			strcpy(tempStr, argv[i]);
			if (debugEnable)
			{
				printf("%x", tempStr[0]);
			}

			argsFromCommand->msgBuff[msgLength] = (lu_uint8_t)(data & 0xFF);
			msgLength++;
		}

		/* Get hex data payload */
		else if (sscanf_s(argv[i], "h%x", &data) == 1)
		{
			argsFromCommand->parserFlag = PARSER_FLAG_SINGLE_COMMAND;
			argsFromCommand->msgBuff[msgLength] = (lu_uint8_t)(data & 0xFF);
			msgLength++;
		}

	} /* for(i = 1; i < argc; i++) */

	/* Return length of buffer read from cmd line */
	argsFromCommand->msgBuffLen = msgLength;
}

void CmdParserDll( MODULE module, 
				   MODULE_ID moduleId,
				   MODULE_MSG_TYPE msgType,
				   MODULE_MSG_ID msgId,
				   lu_uint8_t frag,
				   lu_uint8_t msgBuff[],
				   lu_uint16_t msgLen
				)
{
	lu_uint16_t i;
	/* Set defaults for Can Header */
	argsFromCommand.CANHeader.deviceDst   = module;
	argsFromCommand.CANHeader.deviceIDDst = moduleId;
	argsFromCommand.CANHeader.deviceSrc   = MODULE_MCM;
	argsFromCommand.CANHeader.deviceIDSrc = MODULE_ID_0;
	argsFromCommand.CANHeader.fragment    = frag;
	argsFromCommand.CANHeader.messageType = msgType;
	argsFromCommand.CANHeader.messageID   = msgId;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;
	for(i = 0; i < msgLen; i++)
	{
		argsFromCommand.msgBuff[i] = msgBuff[i];
	}
	
	argsFromCommand.msgBuffLen = msgLen;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
