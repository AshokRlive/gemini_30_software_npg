/*! \file
******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
******************************************************************************
*    PROJECT:
*       G3 [Module Name]
*
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*       \brief
*
*       Main Lucy CAN send module
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*
*
*    CREATION
*
*   Date          Name        Details
*   --------------------------------------------------------------------------
*   26/04/2012    fryers_j    Initial version / Original by Venkat.
*
******************************************************************************
*   COPYRIGHT
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
******************************************************************************
*/


/*
******************************************************************************
* INCLUDES - Standard Library Modules Used
******************************************************************************
*/

/*
******************************************************************************
* INCLUDES - Project Specific Modules Used
******************************************************************************
*/

#include "Main.h"
#include "CmdParser.h"
#include "CmdExecutor.h"
/*
******************************************************************************
* LOCAL - Definitions And Macros
******************************************************************************
*/

/*
******************************************************************************
* LOCAL - Typedefs and Structures
******************************************************************************
*/


/*
*******************************************************************************
* LOCAL - Prototypes Of Local Functions
*******************************************************************************
*/

/*
******************************************************************************
* EXPORTED - Variables
******************************************************************************
*/


/*
******************************************************************************
* LOCAL - Global Variables
******************************************************************************
*/

/*
******************************************************************************
* Exported Functions
******************************************************************************
*/

/*
******************************************************************************
* Local Functions
******************************************************************************
*/

/*!
******************************************************************************
*   \brief Brief description
*
*   Detailed description
*
*   \param parameterName Description 
*
*
*   \return 
*
******************************************************************************
*/

//VS For DLL test 
//lu_uint8_t msgBuff[10] = {1,2,3,4,5,6,7,8,9,10} ;

void main(int argc, char* argv[])
{
	SB_ERROR ret;
	lu_bool_t finish;
	
	/* Parse command line */
	CmdParser(argc, argv, &argsFromCommand);

	//VS For DLL test 
	/* CmdParserDll(  MODULE_MCM, 
				   MODULE_ID_0,
				   MODULE_MSG_TYPE_BLTST,
				   MODULE_MSG_ID_BLTST_GPIO_READ_PIN_C,
				   TRUE,
				   msgBuff,
				   sizeof(msgBuff)
				);
	*/
	CommandExecutorInitialize(&argsFromCommand);

	if (argsFromCommand.parserFlag != PARSER_FLAG_NONE)
	{
		// Initialise the CAN driver/ codec 
		ret = CANCodecInit( MODULE_MCM,
							MODULE_ID_0 ,
							LucyCANProtocolDecoder
						  );
		if(ret >= 0)
		{
			while (LU_TRUE)
			{
				finish = CommandExecutorRun(&argsFromCommand);
				ret = CANCDecode();
			
				if (finish == LU_FALSE || ret == SB_ERROR_CANC_NO_DATA )
				{
					break;
				}
			}
			// Close the CAN driver 
			ret = CANCodecDeInit();
		}
	}
	else
	{
		CmdParserUsage();
	}

	// set exit code for fail / pass
	exit(0);
}

/*
*********************** End of file ******************************************
*/

