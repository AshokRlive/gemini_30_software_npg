/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18/12/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "CANProtocolFraming.h"
#include "CmdParser.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

extern SB_ERROR GPIOTestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time);

extern SB_ERROR GPIOGetPinModeDll( MODULE module, 
							MODULE_ID moduleId, 
							lu_uint8_t port,
							lu_uint8_t pin,
							lu_uint8_t *port_r,
							lu_uint8_t *pin_r,
							lu_uint8_t *pinFunc_r,
							lu_uint8_t *pinMode_r,
							lu_uint8_t *openDrain_r,
							lu_uint8_t *dir_r
						 );

extern SB_ERROR GPIOSetPinModeDll( MODULE module,
						MODULE_ID moduleId, 
						lu_uint8_t    port,
						lu_uint8_t    pin,
						lu_uint8_t    pinFunc,
						lu_uint8_t    pinMode,
						lu_uint8_t	  openDrain,
						lu_uint8_t    dir
					  );

extern SB_ERROR GPIOReadPinDll( MODULE module, 
						MODULE_ID moduleId, 
						lu_uint8_t port,
						lu_uint8_t pin,
						lu_uint8_t *pinValue
					  );

extern SB_ERROR GPIOWritePinDll( MODULE module, 
						MODULE_ID moduleId, 
						lu_uint8_t port,
					    lu_uint8_t pin,
						lu_uint8_t value
					  );
/*
 *********************** End of file ******************************************
 */