/*! \file
 ******************************************************************************
 *       \author         Lucy Electric EMS Ltd: http://www.lucyelectric.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               NvramReadWrite.c
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *  --------------------------------------------------------------------------
 *   01/02/16      saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "NvramReadWriteVerify.h"
#include "CmdParser.h"
#include "CANProtocolCodec.h"
#include "dll/include/mainDll.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

lu_uint16_t recvSize;
lu_uint8_t nvramVerifyBuffer[440], *recvDataPtr = NULL;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR NvramWriteAndVerify( MODULE moduleType,
							  MODULE_ID moduleId,
							  TestNVRAMReadBuffStr nvRamParam,
							  lu_uint8_t *dataBuffer
						    )
{
	SB_ERROR returnError;
	PARSER_FLAG originalPaserflag = argsFromCommand.parserFlag;

	/* re-assigning paser flag for Nvram write and verify */
	argsFromCommand.parserFlag = PARSER_FLAG_NVRAM_WRITE_VERIFY;

	/* Assigning module type and ID to CAN command structure */
	argsFromCommand.CANHeader.deviceDst   = moduleType;
	argsFromCommand.CANHeader.deviceIDDst = moduleId;

	/* Assigning message type and ID to CAN command structure */
	argsFromCommand.CANHeader.messageType   = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID     = MODULE_MSG_ID_BLTST_NVRAM_WRITE_BUFFER_C;

	/* Copying only nvram write parameters from nvRamParams to CAN message buffer */
	memcpy(&argsFromCommand.msgBuff, &nvRamParam.addr, sizeof(nvRamParam.addr));

	/* Copying dataBuffer to CAN payload after NVRAM parameters*/
	memcpy(&argsFromCommand.msgBuff[sizeof(nvRamParam.addr)], dataBuffer, nvRamParam.size);

	/* Calculating CAN message length */
	argsFromCommand.msgBuffLen = sizeof(TestNVRAMReadBuffStr) + nvRamParam.size;

	/* Cpoying buffer size for verification */
	recvSize = nvRamParam.size;

	/* Copying data to reference buffer for verification */
	memcpy(&nvramVerifyBuffer, dataBuffer, nvRamParam.size);

	/* Sending CAN command to write NVRAM */
	returnError = CANCSendFromMCM( MODULE_MSG_TYPE_BLTST,
								   MODULE_MSG_ID_BLTST_NVRAM_WRITE_BUFFER_C,
								   moduleType,
								   moduleId,
								   sizeof(TestNVRAMAddrStr) + nvRamParam.size,
								   (lu_uint8_t*)&argsFromCommand.msgBuff
								 );
	
	/* Delay for NVRAM write operation */
	Sleep(500);

	/* Calling maindll for decoding the response from NVRAM */
	returnError = LucyCANSendMainDll();

	if(returnError == SB_ERROR_NONE)
	{
		/* Filling params for nvram read */
		memcpy(&argsFromCommand.msgBuff[0], &nvRamParam, sizeof(nvRamParam));

		/* Sending CAN command to Read NVRAM for verification */
		returnError = CANCSendFromMCM( MODULE_MSG_TYPE_BLTST,
									   MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_C,
									   moduleType,
									   moduleId,
									   sizeof(nvRamParam),
									   (lu_uint8_t*)&argsFromCommand.msgBuff
									 );
		/* Delay for NVRAM read operation */
		Sleep(2000);

		/* Calling maindll for decoding the response from NVRAM */
		returnError = LucyCANSendMainDll();

		if(returnError == SB_ERROR_NONE)
		{
			/* Non-NULL pointer check on "recvDataPtr" */
			if(recvDataPtr != NULL)
			{
				/* Comparing read data from NVRAM with original data */
				if(memcmp(recvDataPtr, &nvramVerifyBuffer, recvSize) != 0)
				{
					returnError = SB_ERROR_WRITE_FAIL;
				}
			}
			else
			{
				returnError = SB_ERROR_TIMEOUT;
			}
		}
	}

	return returnError;
}

SB_ERROR NVRAMReadWriteVerifyProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_CANC_NOT_HANDLED;
	lu_uint8_t writeResponse, readResponse;

	LU_UNUSED(time);

	if(argsFromCommand.parserFlag == PARSER_FLAG_NVRAM_WRITE_VERIFY)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_BLTST:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_BLTST_NVRAM_WRITE_BUFFER_R: 
						if(msgPtr->msgLen == sizeof(lu_uint8_t))
						{
							writeResponse = (lu_uint8_t)*msgPtr->msgBufPtr;
							retError = (SB_ERROR)writeResponse;
						}
						break;

					case MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_R: 
						if(msgPtr->msgLen == recvSize)
						{
							recvDataPtr = msgPtr->msgBufPtr;
							retError = SB_ERROR_NONE;
						}
						break;
						
					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;

			default:
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
