/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN protocol framing module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26/04/2012    fryers_j    Initial version / Original by Venkat.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "CANProtocolFraming.h"

#include "KvaserCan.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"



/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/* Increment and wrap. Optimised version: works ONLY with multiple of 2 values */
#define CAN_INCREMENT_AND_WRAP(a, mask) ((a) = (((a)+ 1) & mask))

/* Fragmentation Data */
#define CAN_FRAGMENT_HEADER_SIZE    (sizeof(CANFragmentHeaderStr))
#define CAN_FRAGMENT_PAYLOAD        (7)
#define CAN_FRAGMENT_ID_MASK        (0x3F) /* 6 bits */
#define CAN_FRAGMENT_ID             (0xFF)

/* Array size to track fragment numbers for duplicate rejection */
#define CAN_FRAGMENT_ID_MASK_BITS	32
#define CAN_FRAGMENT_ID_MASK_SIZE	((CAN_MAX_FRAGMENTS + CAN_FRAGMENT_ID_MASK_BITS) / CAN_FRAGMENT_ID_MASK_BITS)

/* Check if fragment id is set in mask */
#define CAN_FRAGMENT_ID_MASK_IS_NOT_SET(mask, id) 	( !(mask[(id >> 5) & CAN_FRAGMENT_ID_MASK] & (1 << (id & 0x1f)) ) )

/* Set fragment id in mask */
#define CAN_FRAGMENT_ID_MASK_SET(mask, id) 			( mask[(id >> 5) & CAN_FRAGMENT_ID_MASK] |= (1 << (id & 0x1f)) )

/* Get next fragment ID. Only one fragmented message at a time can be sent.
 * This doesn't need to be atomic
 */
#define CAN_FRAGMENT_NEXT_ID(a) (CAN_INCREMENT_AND_WRAP(a, CAN_FRAGMENT_ID_MASK))


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    CAN_FRAGMENTATION_STATUS_IDLE = 0,
    CAN_FRAGMENTATION_STATUS_START   ,
    CAN_FRAGMENTATION_STATUS_CONT    ,
    CAN_FRAGMENTATION_STATUS_END     ,

    CAN_FRAGMENTATION_STATUS_LAST
}CAN_FRAGMENTATION_STATUS;

/*!
 * TX Fragmentation State Machine Control Data
 */
typedef struct CANTXFragmentationDef
{
    /*! FSM state */
    CAN_FRAGMENTATION_STATUS state;
    /*! Fragment ID counter */
    lu_uint8_t counter;

    /*! Bytes already sent */
    lu_uint32_t sentData;

    /*! ObjectID (1-32) of the last sent packet */
    lu_int32_t objID;

    /* Kvaser CAN message Object */
    KVASER_CAN_MSG_TYPE CANMsgObject;

    /*! Total length of the buffer */
    lu_uint32_t bufferLen;
    /*! Local TX buffer */
    lu_uint8_t buffer[MODULE_MESSAGE_LENGTH];
}CANTXFragmentationStr;

/*!
 * RX Fragmentation State Machine Control Data
 */
typedef struct CANRXFragmentationDef
{
    /*! FSM state */
    CAN_FRAGMENTATION_STATUS state;
    /*! Expected fragment ID */
    lu_uint8_t fragmentID;
    /*! received data */
    lu_uint32_t recvData;
    /*! Expected CAN header */
    lu_uint32_t expectedHeader;

    /* Count total numb fragments received */
	lu_uint32_t fragmentCount;
	/* Track fragments received to reject duplicates */
	lu_uint32_t fragmentIDMask[CAN_FRAGMENT_ID_MASK_SIZE];

    /*! G3 CAN message structure */
    CANFramingMsgStr  CANFramingMsg;
    /*! Local buffer */
    lu_uint8_t buffer[MODULE_MESSAGE_LENGTH];
}CANRXFragmentationStr;

/*!
 * Library control data
 */
typedef struct CANFramingDef
{
    /*! Library status */
    lu_bool_t   initialzied;
    /*! Device type */
    lu_uint8_t  device     ;
    /*! Device address */
    lu_uint8_t  deviceID   ;

    /*! ID Mask */
    lu_uint32_t msgIDMask;

	/*! Kvaser CAN handler */
	lu_uint32_t	handle;

    /*! Receive Message */
    KVASER_CAN_MSG_TYPE canRxMsg;

    /*! Receive buffer ID */
    lu_uint32_t rxIdx;

    /*! Fragmentation Control data (TX) */
    CANTXFragmentationStr TXFragment;

    /*! Fragmentation Control data (RX) */
    CANRXFragmentationStr RXFragment;

}CANFramingStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void initHeaderMsgObject( CANFramingMsgStr *msgPtr,
                          KVASER_CAN_MSG_TYPE *CANMsgTxPtr,
                          lu_bool_t fragment
                        );

void initHeaderCANFMsg( CANFramingMsgStr *msgPtr,
                        CANHeaderStr *CANHeaderPtr
                      );

SB_ERROR CANFramingSendFragment(CANFramingMsgStr *msgPtr);

SB_ERROR CANFramingSendFragmentInt(lu_int32_t objID);

SB_ERROR CANFramingFillFragment(void);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static CANFramingStr CANFramingData;


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR CANFramingInit( lu_uint8_t        device    ,
		                 lu_uint8_t        deviceID  ,
						 lu_uint32_t       *handlePtr
		               )
{
    CANHeaderStr *CANHeaderPtr;

	if (CANFramingData.initialzied == LU_TRUE)
    {
        /* Module already initialized */
        return SB_ERROR_INITIALIZED;
    }

    /* Module initialized */
    CANFramingData.initialzied = LU_TRUE;

    /* Save source ID */
    CANFramingData.device   = device  ;
    CANFramingData.deviceID = deviceID;

    /* Mask everything apart the destination */
    CANFramingData.msgIDMask = 0;
    CANHeaderPtr = (CANHeaderStr*)(&CANFramingData.msgIDMask);
    CANHeaderPtr->deviceDst   = ((lu_uint8_t)(0x1F));
    CANHeaderPtr->deviceIDDst = ((lu_uint8_t)(0x07));

    /* Initialize Kvaser CAN */
	//*handlePtr = KvaserInitCtrl(0);
	//CANFramingData.handle = *handlePtr;

	/* Enable Hardware Acceptance Filter */

    return SB_ERROR_NONE;
}

SB_ERROR CANFramingSendMsg(CANFramingMsgStr *msgPtr)
{
    KVASER_CAN_MSG_TYPE CANMsgTx;
    lu_uint32_t msgLen;
    lu_uint32_t i;
	SB_ERROR ret;

    if (msgPtr == NULL)
    {
        return SB_ERROR_PARAM;
    }

    msgLen = msgPtr->msgLen;
    if (msgLen > MODULE_MESSAGE_LENGTH)
    {
        return SB_ERROR_CANF_MSG_TOO_LONG;
    }

    if (msgLen <= CAN_MAX_PAYLOAD)
    {
        /* Short message. Send immediately - fill in CAN ID*/
		initHeaderMsgObject(msgPtr, &CANMsgTx, LU_FALSE);

		/* Set CAN message length & copy data */
		CANMsgTx.len = (lu_uint8_t)msgPtr->msgLen;
        for(i = 0; i < msgPtr->msgLen; i++)
        {
			CANMsgTx.data[i] = msgPtr->msgBufPtr[i];
        }

		/* Send whole message in single CAN frame */
		KvaserCanWrite( CANFramingData.handle, 
			            CANMsgTx.id, 
						CANMsgTx.data, 
						CANMsgTx.len
					  );
        
        return SB_ERROR_NONE;
    }
    else
    {
        /* Long message. Start fragmentation */
		ret = CANFramingSendFragment(msgPtr);

		/* Send remaining fragments */
		do
		{
			/* Send each fragment */
			CANFramingSendFragmentInt(2);
		} while ( CANFramingData.TXFragment.state != 
			      CAN_FRAGMENTATION_STATUS_IDLE
				);
    }

    return SB_ERROR_CANF;
}

/*!
 ******************************************************************************
 *   \brief Get a new message from the receive message queue
 *
 *   \return Pointer to a new message. NULL if the queue is empty
 *
 ******************************************************************************
 */
CANFramingMsgStr* CANFramingRecvMsg(void)
{
	KVASER_CAN_MSG_TYPE canMsg;
	CANHeaderStr *CANHeaderPtr;
	SB_ERROR ret;

	/* Get a message */
	CANFramingData.canRxMsg.len = 
		KvaserCanRead(CANFramingData.handle, 
		              &CANFramingData.canRxMsg.id, 
				      &CANFramingData.canRxMsg.data[0]
	                 );

	if (CANFramingData.canRxMsg.len == 0)
	{
		CANFramingData.RXFragment.CANFramingMsg.msgLen    = 0;
		CANFramingData.RXFragment.CANFramingMsg.msgBufPtr = NULL;

//		return &CANFramingData.RXFragment.CANFramingMsg;
	}

	/* The message is for us - Decode Header*/
    CANHeaderPtr = (CANHeaderStr*)(&CANFramingData.canRxMsg.id);

	/* Copy CAN ID data fields */
	CANFramingData.RXFragment.CANFramingMsg.deviceDst    = CANHeaderPtr->deviceDst;
	CANFramingData.RXFragment.CANFramingMsg.deviceIDDst  = CANHeaderPtr->deviceIDDst;
	CANFramingData.RXFragment.CANFramingMsg.deviceSrc	 = CANHeaderPtr->deviceSrc;
	CANFramingData.RXFragment.CANFramingMsg.deviceIDSrc  = CANHeaderPtr->deviceIDSrc;
	CANFramingData.RXFragment.CANFramingMsg.messageID    = CANHeaderPtr->messageID;
	CANFramingData.RXFragment.CANFramingMsg.messageType  = CANHeaderPtr->messageType;
	
    if (CANHeaderPtr->fragment == LU_FALSE)
    {
		/* We have a single non-fragmented message */

		/* Set msg size / data pointer */
		CANFramingData.RXFragment.CANFramingMsg.msgLen    = 
			CANFramingData.canRxMsg.len;
		CANFramingData.RXFragment.CANFramingMsg.msgBufPtr = 
			CANFramingData.canRxMsg.data;
	}
	else
	{
		//printf("frag\n");
		/* Continue to read remaining fragments */
		do
		{
			/* Handle Fragmented message */
			ret = CANFramingRecvFragment(&CANFramingData.canRxMsg);

			/* Get next message */
			CANFramingData.canRxMsg.len = 
				KvaserCanRead(CANFramingData.handle, 
							  &CANFramingData.canRxMsg.id, 
				              &CANFramingData.canRxMsg.data[0]
	                         );
		} while (CANFramingData.canRxMsg.len > 0);

		/* To make sure that the CAN fragment state machine reset to receive next fragmented message*/
		CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;

		/* Set msg size / data pointer */
		CANFramingData.RXFragment.CANFramingMsg.msgLen    = 
			CANFramingData.RXFragment.recvData;
		CANFramingData.RXFragment.CANFramingMsg.msgBufPtr = 
			CANFramingData.RXFragment.buffer;
	}

	return &CANFramingData.RXFragment.CANFramingMsg;
}


SB_ERROR CANFramingRecvFragment(KVASER_CAN_MSG_TYPE *CANMsgPtr)
{
    lu_uint32_t msgLen;
    CANHeaderStr *CANHeaderPtr;
    CANFragmentHeaderStr *CANFragmentHeaderPtr;
    lu_uint8_t i;
    lu_uint32_t idx;
    SB_ERROR ret = SB_ERROR_NONE;

    /* Decode fragmentation header */
    CANFragmentHeaderPtr = (CANFragmentHeaderStr*)(&CANMsgPtr->data[0]);

    /* Save message length */
    msgLen = CANMsgPtr->len - CAN_FRAGMENT_HEADER_SIZE;

    switch(CANFramingData.RXFragment.state)
    {
        case CAN_FRAGMENTATION_STATUS_IDLE:
            /* Check start/stop bits */
            if ( (CANFragmentHeaderPtr->start == 1) &&
                 (CANFragmentHeaderPtr->stop  == 0) &&
                 (CANFragmentHeaderPtr->id == 0)
               )
            {
                /* Valid first fragment
                 * Initialize control data
                 */
                CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_CONT;
                CANFramingData.RXFragment.fragmentID = CANFragmentHeaderPtr->id;
                CANFramingData.RXFragment.expectedHeader = CANMsgPtr->id;

                /* Reset mask to detect repeated fragments */
				for (idx = 0; idx < CAN_FRAGMENT_ID_MASK_SIZE; idx++)
				{
					CANFramingData.RXFragment.fragmentIDMask[idx] = 0L;
				}

				/* First fragment detected */
				CANFramingData.RXFragment.fragmentCount      = 1;
				CAN_FRAGMENT_ID_MASK_SET(CANFramingData.RXFragment.fragmentIDMask, 0);

                /* Set the next expect fragment ID */
                CAN_FRAGMENT_NEXT_ID(CANFramingData.RXFragment.fragmentID);

                /* Save buffer.
                 * CANFramingData.RXFragment.buffer[0..6] = CANMsgPtr->data[1..7];
                 */
                for(i = 0; i < msgLen; i++)
                {
                    CANFramingData.RXFragment.buffer[i] = CANMsgPtr->data[i+1];
                }
                CANFramingData.RXFragment.recvData = msgLen;
            }
            else
            {
                /* Wrong header */
                ret = SB_ERROR_CANF_RX_FRAGMENT_ERROR;
            }
        break;

        case CAN_FRAGMENTATION_STATUS_CONT:
            /* Check start/stop bits */
            if (CANFragmentHeaderPtr->stop  == 1)
            {
                /* Valid header (continuation packet) */

            	/* Count total fragments received */
            	CANFramingData.RXFragment.fragmentCount++;

                /* Check msg header */
            	 if ( (CANFramingData.RXFragment.expectedHeader == CANMsgPtr->id) &&
					 (CAN_FRAGMENT_ID_MASK_IS_NOT_SET(CANFramingData.RXFragment.fragmentIDMask,
													  CANFragmentHeaderPtr->id)) &&
					 (CANFramingData.RXFragment.recvData + msgLen <= MODULE_MESSAGE_LENGTH )
				   )
				{
            		 CAN_FRAGMENT_ID_MASK_SET(CANFramingData.RXFragment.fragmentIDMask, CANFragmentHeaderPtr->id);

                    /* Fully validated header. Save buffer
                     * CANFramingData.RXFragment.buffer[recvData + 0..6] = CANMsgPtr->data[1..7];
                     */
                    for(i = 0; i < msgLen; i++)
                    {
                        CANFramingData.RXFragment.buffer[i+CANFramingData.RXFragment.recvData] = CANMsgPtr->data[i+1];
                    }
                    CANFramingData.RXFragment.recvData += msgLen;

                    /* Last packet ? */
                    if(CANFragmentHeaderPtr->start == 0)
                    {
                    	if ((CANFramingData.RXFragment.fragmentCount - 1) ==
                    	     CANFragmentHeaderPtr->id)
                    	{
							/* Yes */
							CANHeaderPtr = (CANHeaderStr*)(&CANFramingData.RXFragment.expectedHeader);
							initHeaderCANFMsg(&CANFramingData.RXFragment.CANFramingMsg, CANHeaderPtr);

							/* Prepare data */
							CANFramingData.RXFragment.CANFramingMsg.ID = CAN_FRAGMENT_ID;
							CANFramingData.RXFragment.CANFramingMsg.msgLen = CANFramingData.RXFragment.recvData;
							CANFramingData.RXFragment.CANFramingMsg.msgBufPtr = CANFramingData.RXFragment.buffer;

							/* Update state */
							CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_END;
                    	}
                    	else
                    	{
                    		/* Missing fragments - Reset State Machine */
							CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
							ret = SB_ERROR_CANF_RX_FRAGMENT_ERROR;
                    	}
                    }
                    else
                    {
                        /* Set the next expect fragment ID */
                        CAN_FRAGMENT_NEXT_ID(CANFramingData.RXFragment.fragmentID);
                    }
                }
                else
                {
                    /* Wrong header - Reset State Machine */
                    CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
                    ret = SB_ERROR_CANF_RX_FRAGMENT_ERROR;
                }
            }
            else
            {
                /* Wrong header - Reset State Machine */
                CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
                ret = SB_ERROR_CANF_RX_FRAGMENT_ERROR;
            }
        break;

        case CAN_FRAGMENTATION_STATUS_END:
            /* Waiting for release. Do nothing */
        case CAN_FRAGMENTATION_STATUS_START:
        case CAN_FRAGMENTATION_STATUS_LAST:
        break;
    }

    return ret;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialize CAN message using a G3 CAN Message
 *
 *   \param msgPtr G3 CAN Message used to initialize the CAN object
 *   \param CANMsgTxPtr CAN message to initialize
 *   \param fragment set to LU_TRUE if this is a fragmented message
 *
 *   \return None
 *
 ******************************************************************************
 */
void initHeaderMsgObject( CANFramingMsgStr *msgPtr,
                          KVASER_CAN_MSG_TYPE *CANMsgTxPtr,
                          lu_bool_t fragment
                        )
{
    CANHeaderStr *CANHeaderPtr;

    CANMsgTxPtr->id = 0;
    //CANMsgTxPtr->format = EXT_ID_FORMAT;
    //CANMsgTxPtr->type = DATA_FRAME;
    CANHeaderPtr = (CANHeaderStr*)(&CANMsgTxPtr->id);
    CANHeaderPtr->deviceSrc   = msgPtr->deviceSrc;
    CANHeaderPtr->deviceIDSrc = msgPtr->deviceIDSrc;
    CANHeaderPtr->deviceDst   = msgPtr->deviceDst;
    CANHeaderPtr->deviceIDDst = msgPtr->deviceIDDst;
    CANHeaderPtr->messageType = msgPtr->messageType;
    CANHeaderPtr->messageID   = msgPtr->messageID;
    if(fragment == LU_TRUE)
    {
        CANHeaderPtr->fragment = 1;
    }
}

/*!
 ******************************************************************************
 *   \brief Initialize G3 CAN Framing Message using a CAN object
 *
 *   \param msgPtr CAN object used to initialize the G3 CAN Message
 *   \param MsgObjectTxPtr G3 CAN Message to initialize
 *
 *   \return None
 *
 ******************************************************************************
 */
void initHeaderCANFMsg( CANFramingMsgStr *msgPtr,
                        CANHeaderStr *CANHeaderPtr
                      )
{
    msgPtr->messageType = CANHeaderPtr->messageType;
    msgPtr->messageID   = CANHeaderPtr->messageID;
    msgPtr->deviceDst   = CANHeaderPtr->deviceDst;
    msgPtr->deviceIDDst = CANHeaderPtr->deviceIDDst;
    msgPtr->deviceSrc   = CANHeaderPtr->deviceSrc;
    msgPtr->deviceIDSrc = CANHeaderPtr->deviceIDSrc;
}

/*!
 ******************************************************************************
 *   \brief Start the transmission of a "long" message
 *
 *   Only one long message at a time can be transmitted. The first fragment
 *   is sent immediately. The successive fragments are automatically sent
 *   in the ISR when the transmission of the previous message is terminated.
 *
 *   \param msgPtr Object to send
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingSendFragment(CANFramingMsgStr *msgPtr)
{
    SB_ERROR ret = SB_ERROR_NONE;

    switch(CANFramingData.TXFragment.state)
    {
        case CAN_FRAGMENTATION_STATUS_IDLE:

            /* Initialise control data */
            CANFramingData.TXFragment.objID = 0;
            CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_START;
            CANFramingData.TXFragment.sentData = 0;
            CANFramingData.TXFragment.counter = 0; // Reset fragment to zero for first fragment

            /* Save message buffer */
            CANFramingData.TXFragment.bufferLen = msgPtr->msgLen;
            memcpy( CANFramingData.TXFragment.buffer,
                    msgPtr->msgBufPtr,
                    msgPtr->msgLen
                  );

            /* Initialize common field of CAN MsgObj */
            initHeaderMsgObject(msgPtr, &CANFramingData.TXFragment.CANMsgObject, LU_TRUE);

            /* Initialize payload */
            ret = CANFramingFillFragment();

            if (ret == SB_ERROR_NONE)
            {
                /* Update state */
                CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_CONT;

				/* Send first fragment in CAN frame */
				KvaserCanWrite( CANFramingData.handle, 
				                CANFramingData.TXFragment.CANMsgObject.id, 
							    CANFramingData.TXFragment.CANMsgObject.data, 
							    CANFramingData.TXFragment.CANMsgObject.len
							  );
            }
            else
            {
                
                /* Reset FSM */
                CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
            }
        break;

        default:
            ret = SB_ERROR_BUSY;
        break;
    }


    return ret;
}

/*!
 ******************************************************************************
 *   \brief Continue the transmission of the "long" message
 *
 *   This function should be called directly from the ISR. A new fragment
 *   is sent only when a previous fragment has been successfully sent. The
 *   objID is used to identify the transmitted object
 *
 *   \param objID Id of the object that has just been transmitted (1-32)
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingSendFragmentInt(lu_int32_t objID)
{
    SB_ERROR ret = SB_ERROR_NONE;

    switch(CANFramingData.TXFragment.state)
    {
        case CAN_FRAGMENTATION_STATUS_IDLE:
        case CAN_FRAGMENTATION_STATUS_START:
            /* Do nothing */
        break;

        case CAN_FRAGMENTATION_STATUS_CONT:
            if (1)
            {
                /* this interrupt is for us
                 * Initialize payload
                 */
                if(CANFramingFillFragment() == SB_ERROR_NONE)
                {
                    /* Send message */
                    CANFramingData.TXFragment.objID = 2;

					/* Send Fragments */
					KvaserCanWrite( CANFramingData.handle, 
				                    CANFramingData.TXFragment.CANMsgObject.id, 
							        CANFramingData.TXFragment.CANMsgObject.data, 
							        CANFramingData.TXFragment.CANMsgObject.len
							      );
					
                    ret = SB_ERROR_CANF_TX_FRAGMENT;
                }
                else
                {
                    /* Reset FSM */
                    CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
                }
            }
        break;

        case CAN_FRAGMENTATION_STATUS_END:
            /* Finished. Reset state machine */
            CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
        break;

        default:
            /* Do nothing */
        break;
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Prepare a new fragment to send
 *
 *   The buffer of the fragment CAN message is filled with new data.
 *   If necessary the State Machine status is also updated
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingFillFragment(void)
{
    lu_uint32_t bytesLeft;
    CANFragmentHeaderStr *CANFragmentHeaderPtr;
    SB_ERROR ret = SB_ERROR_NONE;
    lu_uint32_t i;

    CANFragmentHeaderPtr =
        (CANFragmentHeaderStr *)&CANFramingData.TXFragment.CANMsgObject.data[0];

    /* Check left bytes and update state */
    bytesLeft = ( CANFramingData.TXFragment.bufferLen -
                  CANFramingData.TXFragment.sentData
				);

    /* Set Fragment ID */
    CANFragmentHeaderPtr->id = CANFramingData.TXFragment.counter;

    /* update Counter */
    CAN_FRAGMENT_NEXT_ID(CANFramingData.TXFragment.counter);

    /* Set start/stop flags */
    switch(CANFramingData.TXFragment.state) //MG Reset State machine after a timeout
    {
        case CAN_FRAGMENTATION_STATUS_START:
            /* Sanity check */
            if (bytesLeft > CAN_FRAGMENT_PAYLOAD)
            {
                CANFragmentHeaderPtr->start = 1;
                CANFragmentHeaderPtr->stop  = 0;
            }
            else
            {
                /* Error */
                ret = SB_ERROR_CANF_TX_FRAGMENT_ERROR;
            }
			break;

        case CAN_FRAGMENTATION_STATUS_CONT:
            if (bytesLeft > CAN_FRAGMENT_PAYLOAD)
            {
                CANFragmentHeaderPtr->start = 1;
                CANFragmentHeaderPtr->stop  = 1;
            }
            else
            {
                /* Last fragment */
                CANFragmentHeaderPtr->start = 0;
                CANFragmentHeaderPtr->stop  = 1;

                /* change state */
                CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_END;
            }
			break;

        case CAN_FRAGMENTATION_STATUS_END:
			break;

        default:
            ret = SB_ERROR_CANF_TX_FRAGMENT_ERROR;
			break;
    }

    if (ret == SB_ERROR_NONE)
    {
        /* Copy data to send into the buffer */
        bytesLeft = LU_MIN(bytesLeft, CAN_FRAGMENT_PAYLOAD);
        for(i = 0; i < bytesLeft; i++)
        {
			CANFramingData.TXFragment.CANMsgObject.data[i+1] =
				CANFramingData.TXFragment.buffer[CANFramingData.TXFragment.sentData+i];
        }

        /* Update sent bytes */
        CANFramingData.TXFragment.sentData += bytesLeft;

        /* Set payload length */
        CANFramingData.TXFragment.CANMsgObject.len = (bytesLeft + CAN_FRAGMENT_HEADER_SIZE);
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */



