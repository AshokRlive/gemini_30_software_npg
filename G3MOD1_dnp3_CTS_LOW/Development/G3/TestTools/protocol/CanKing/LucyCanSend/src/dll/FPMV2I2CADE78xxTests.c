/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1577 2012-07-23 20:18:25Z fryers_j $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1577 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-07-23 21:18:25 +0100 (Mon, 23 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/11/14     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "ADE78xxI2CDLL.h"
#include "ADE78xx.h"
#include "FPMV2Tests.h"
#include "FPMV2I2CADE78xxTests.h"



/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define ADE78XX_24ZP_SIGNUM     (0x08000000)
#define ADE78XX_24ZP_PAD        (0xFF000000)
#define ADE78XX_28ZP_SIGNUM     (0x00800000)
#define ADE78XX_28ZP_PAD        (0xF0000000)

/*!
 * \brief Convert a number from the 32bit standard format to the 28ZP format.
 *
 * The input value range is NOT checked
 */
#define ADE78XX_32_2_28ZP(_val_) (_val_ &= (~(ADE78XX_28ZP_PAD)))

//#define ADE78XX_IGAIN -8385324
//#define ADE78XX_VGAIN -8348885

#define ADE78XX_IGAIN					-8336000
#define ADE78XX_VGAIN					-8348885
#define ADE78XX_IRMSGAIN				-8310000

#define ADE78XX_HSDC_CONFIG				0x0A
#define I2C_LOCK						(1 << 1)
#define ADE78XX_CONFIG_HSDCEN			((1UL<<6)) 		 /* Enable HSDC serial port */

#define ADE78XX_CONFIG_A				0xE740
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
void CovertValueFrom32ZPSTo24Signed(lu_int32_t zp32Value, lu_uint32_t *actualValuePtr);
void CovertValueFrom32ZPSTo28Signed(lu_uint32_t zp32Value, lu_uint32_t *actualValue);
void CovertValueFrom32ZPSTo28Unsigned(lu_uint32_t zp32Value, lu_uint32_t *actualValuePtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
volatile lu_uint32_t ade78xxAIgainReg;
volatile lu_uint32_t ade78xxBIgainReg;
volatile lu_uint32_t ade78xxCIgainReg;

TestI2cADE78xxInitStr fpi_A_InitParams = {2,  0,  0,  16,  
										   {	
											  {1,  14},  // PM0
											  {1,  15},  // PM1
											  {1,  16},  // RST 
											  {0,  23},  // IRQ0
											  {0,  24},  // IRQ1
											  {1,  4},   // CF1
											  {1,  8}    // CF2
										   },
										  1
										 };

TestI2cADE78xxInitStr fpi_B_InitParams  = {1, 1, 0, 6,
											 {
												{2, 8 },  // PM0
												{2, 0 },  // PM1
												{2, 1 },  // RST
												{0, 25},  // IRQ0
												{0, 26},  // IRQ1
												{1, 24},  // CF1
												{0,  4}  // CF2
											  },
											  1
										     };

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR FPMInitialise( MODULE     module, 
						MODULE_ID  moduleId,
						lu_uint8_t fpiChan
					  )
{
	SB_ERROR retError;

	switch(fpiChan)
	{
		case 0:
			fpi_A_InitParams.enable = LU_TRUE;
			retError = FPMV2ADE78xxI2CEnable(module, moduleId, &fpi_A_InitParams);
			break;

		case 1:
			fpi_B_InitParams.enable = LU_TRUE;
			retError = FPMV2ADE78xxI2CEnable(module, moduleId, &fpi_B_InitParams);
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
	}

	return retError;
}

SB_ERROR FPMDeinitialise( MODULE     module, 
						  MODULE_ID  moduleId,
						  lu_uint8_t fpiChan
					    )
{
	SB_ERROR retError;

	switch(fpiChan)
	{
		case 0:
			{
				fpi_A_InitParams.enable = LU_FALSE;
				retError = FPMV2ADE78xxI2CEnable(module, moduleId, &fpi_A_InitParams);
		    }
			break;

		case 1:
			{
				fpi_B_InitParams.enable = LU_FALSE;
				retError = FPMV2ADE78xxI2CEnable(module, moduleId, &fpi_B_InitParams);
			}
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
	}

	return retError;
}

SB_ERROR FPMSetGain(  MODULE        module, 
					  MODULE_ID     moduleId,
					  lu_uint8_t    fpiChan, 
					  lu_uint32_t   gainRegAI,
					  lu_uint32_t   gainRegBI,
					  lu_uint32_t   gainRegCI,
					  lu_uint16_t   calPhaseA,
					  lu_uint16_t   calPhaseB,
					  lu_uint16_t   calPhaseC
				   )
{
	SB_ERROR    retError;
	volatile lu_uint32_t retValue;
	volatile lu_uint32_t retVal16U;
	lu_uint8_t i2cChan;

	switch(fpiChan)
	{
		case 0:
			i2cChan = fpi_A_InitParams.i2cChan;
			retError = SB_ERROR_NONE;
			break;

		case 1:
			i2cChan = fpi_B_InitParams.i2cChan;
			retError = SB_ERROR_NONE;
			break;

		default:
			i2cChan = fpi_A_InitParams.i2cChan;
			retError = SB_ERROR_PARAM;
			break;
	}
	
	if(retError == SB_ERROR_NONE)
	{
		/* Disable Memory Protection */
		//FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan, 0xE7FE, sizeof(lu_uint8_t), 0x000000AD);
		//FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan, 0xE7E3, sizeof(lu_uint8_t), 0x00000000);

		/* STOP DSP */
		FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan, ADE78XX_RUN, sizeof(lu_uint16_t), 0x00000000);
		Sleep(10);
	
		/* Reset all IRQs. Reset STATUS0 and STATUS1 registers*/
		retError = FPMV2ADE78xxI2CReadReg (module, moduleId, i2cChan,  ADE78XX_CONFIG2, sizeof(lu_uint8_t), &retValue);
		Sleep(10);
		retError = FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan,  ADE78XX_CONFIG2, sizeof(lu_uint8_t), retValue | I2C_LOCK );
		Sleep(10);
	
		retError = FPMV2ADE78xxI2CReadReg (module, moduleId, i2cChan,  ADE78XX_STATUS0, sizeof(lu_uint32_t), &retValue);
		Sleep(10);
		retError = FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan,  ADE78XX_STATUS0, sizeof(lu_uint32_t), retValue );
		Sleep(10);
									  
		retError = FPMV2ADE78xxI2CReadReg (module, moduleId, i2cChan, ADE78XX_STATUS1, sizeof(lu_uint32_t), &retValue);
		Sleep(10);
		retError = FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan, ADE78XX_STATUS1, sizeof(lu_uint32_t), retValue );
		Sleep(10);
	
		/* Current Gain register */
		ade78xxAIgainReg = gainRegAI;
		retError = FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan,	ADE78XX_AIGAIN, sizeof(lu_uint32_t), (lu_uint32_t)gainRegAI);
		Sleep(10);

		ade78xxBIgainReg    = gainRegBI;
		retError = FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan,	ADE78XX_BIGAIN, sizeof(lu_uint32_t), (lu_uint32_t)gainRegBI);
		Sleep(10);

		ade78xxCIgainReg    = gainRegCI;
		retError = FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan,	ADE78XX_CIGAIN, sizeof(lu_uint32_t), (lu_uint32_t)gainRegCI);
		Sleep(10);

		retError = FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan,	ADE78XX_APHCAL, sizeof(lu_uint16_t), calPhaseA);
		Sleep(10);

		retError = FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan,	ADE78XX_BPHCAL, sizeof(lu_uint16_t), calPhaseB);
		Sleep(10);

		retError = FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan,	ADE78XX_CPHCAL, sizeof(lu_uint16_t), calPhaseC);
		Sleep(10);
			
		retError = FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan,	ADE78XX_HSDC_CFG, sizeof(lu_uint8_t), ADE78XX_HSDC_CONFIG);
		Sleep(10);
	
		FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan, ADE78XX_CONFIG_A, sizeof(lu_uint8_t), (lu_uint32_t)0x01);
		Sleep(10);
	
		retError = FPMV2ADE78xxI2CReadReg(module, moduleId, i2cChan, ADE78XX_CONFIG, sizeof(lu_uint16_t), &retVal16U );
		Sleep(10);

		retVal16U = retVal16U | ADE78XX_CONFIG_HSDCEN;
	
		FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan, ADE78XX_CONFIG, sizeof(lu_uint16_t), retVal16U);
		Sleep(10);
	
		/* Write the last register 2 more times to flush the DSP internal pipeline */
		FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan, ADE78XX_CONFIG, sizeof(lu_uint16_t), (lu_uint32_t)retVal16U);
		Sleep(10);
	
		FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan, ADE78XX_CONFIG, sizeof(lu_uint16_t), (lu_uint32_t)retVal16U);
		Sleep(10);
	
		/* Enable memory protection */
		//FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan, 0xE7FE, sizeof(lu_uint8_t), 0x000000AD);
		//FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan, 0xE7E3, sizeof(lu_uint8_t), 0x00000080);

		/* RUN DSP */
		FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan, ADE78XX_RUN, sizeof(lu_uint16_t), 0x00000001);
		Sleep(10);
	}

	return retError;
}

SB_ERROR FPMStart( MODULE      module, 
				   MODULE_ID   moduleId,
				   lu_uint8_t  fpiChan,
				   lu_uint8_t  start 
				 )
{
	SB_ERROR retError;
	lu_uint8_t 	i2cChan;

	switch(fpiChan)
		{
			case 0:
				i2cChan = fpi_A_InitParams.i2cChan;
				retError = SB_ERROR_NONE;
				break;

			case 1:
				i2cChan = fpi_B_InitParams.i2cChan;
				retError = SB_ERROR_NONE;
				break;

			default:
				i2cChan = fpi_A_InitParams.i2cChan;
				retError = SB_ERROR_PARAM;
				break;
		}

		if(start == 1)
		{
			/* RUN DSP */
			retError = FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan, ADE78XX_RUN, sizeof(lu_uint16_t), 0x00000001);
		}
		if(start == 0)
		{
			/* STOP DSP */
			retError = FPMV2ADE78xxI2CWriteReg(module, moduleId, i2cChan, ADE78XX_RUN, sizeof(lu_uint16_t), 0x00000000);
		}
	return retError;
}

SB_ERROR FPMRead( MODULE       module, 
				  MODULE_ID    moduleId,
				  lu_uint8_t   fpiChan,
				  lu_uint8_t   peakChan,
				  lu_uint32_t  *peakValuePtr
				)
{
	return  FPMV2TestI2CADE78xxReadPeak( module, 
									     moduleId,
									     fpiChan,
									     peakChan,
										 peakValuePtr
								        );
}	

SB_ERROR FPMGetGain( lu_uint32_t iWavePeak,
					 lu_uint32_t zeroGainADC, 
					 lu_uint32_t *gainRegValue
					)
{
	long double gainRegLocal;

	gainRegLocal = iWavePeak * pow( 2.0, 23 );

	gainRegLocal = gainRegLocal / zeroGainADC;

	gainRegLocal = gainRegLocal - pow( 2.0, 23 );

	*gainRegValue = (lu_uint32_t)gainRegLocal;

	return SB_ERROR_NONE;
}

SB_ERROR FPMV2TestStartI2CADE78xx( MODULE     module, 
								   MODULE_ID  moduleId,
								   lu_uint8_t i2cChan,
								   lu_uint16_t run
							     )
{
	SB_ERROR retError = SB_ERROR_PARAM;

	retError = FPMV2ADE78xxI2CWriteReg( module,
										moduleId,
										i2cChan,
										ADE78XX_RUN,
										sizeof(lu_uint16_t),
										run
									  );

	return retError;
}

SB_ERROR FPMV2TestI2CReadIrms( MODULE      module, 
							   MODULE_ID   moduleId,
							   lu_uint8_t  i2cChan, 
							   lu_uint8_t  phase,
							   lu_uint32_t *IrmsPtr
						     )
{
	SB_ERROR retError = SB_ERROR_PARAM;
	volatile lu_uint16_t phaseReg;
	lu_uint32_t Irms32zp = 0; 

	switch(phase)
	{
		case 1:
			phaseReg = ADE78XX_AIRMS;
			retError = SB_ERROR_NONE;
			break;
		
		case 2:
			phaseReg = ADE78XX_BIRMS;
			retError = SB_ERROR_NONE;
			break;
		
		case 3:
			phaseReg = ADE78XX_CIRMS;
			retError = SB_ERROR_NONE;
			break;
		
		case 4:
			phaseReg = ADE78XX_NIRMS;
			retError = SB_ERROR_NONE;
			break;

		case 5:
			phaseReg = ADE78XX_ISUM;
			retError = SB_ERROR_NONE;
			break;
	
		default:
			retError = SB_ERROR_PARAM;
			break;
	}

	if(retError == SB_ERROR_NONE)
	{
		retError = FPMV2ADE78xxI2CReadReg(  module,
										    moduleId,
											i2cChan,
											phaseReg,
											sizeof(lu_uint32_t),
											&Irms32zp
										);
	}

	if(phaseReg == ADE78XX_ISUM)
	{
		CovertValueFrom32ZPSTo28Unsigned(Irms32zp, IrmsPtr);
	}
	else
	{
		//CovertValueFrom32ZPSTo24Signed(Irms32zp, IrmsPtr);
		*IrmsPtr = Irms32zp;
	}
	
	return retError;
}

void CovertValueFrom32ZPSBufferTo24SignedBuffer(lu_int32_t *zp32Value, lu_uint32_t *actualValuePtr, lu_uint16_t size)
{
	lu_uint16_t i;

	for(i=0; i < size; i++ )
	{
		CovertValueFrom32ZPSTo24Signed(zp32Value[i], &actualValuePtr[i]);
	}
}

void CovertValueFrom32ZPSTo24Signed(lu_int32_t zp32Value, lu_uint32_t *actualValuePtr)
{
	/*  24 bit value transmitted as a 32bit value
        with eight MSBs padded with 0s  */
	zp32Value &= 0xFFFFFFF;
	
	if(zp32Value & ADE78XX_24ZP_SIGNUM)
	{
		/* Signed value. Pad the 8 MSBs with 1s */
		zp32Value  = ~zp32Value;
		zp32Value &= 0xFFFFFF;
		if(zp32Value != 0)
		{
			zp32Value  = zp32Value - 1;
		}
	}
	
	*actualValuePtr = zp32Value;
}

void CovertValueFrom32ZPSTo28Unsigned(lu_uint32_t zp32Value, lu_uint32_t *actualValuePtr)
{
	/*  28 bit value transmitted as a 32bit value
        with eight MSBs padded with 0s  */
	zp32Value &= 0xFFFFFFF;

	if(zp32Value & ADE78XX_28ZP_SIGNUM)
	{
		/* Signed value. Pad the 8 MSBs with 1s */
		zp32Value  = ~zp32Value;
		zp32Value &= 0xFFFFFF;
		if(zp32Value != 0)
		{
			zp32Value  = zp32Value - 1;
		}
	}
	
	*actualValuePtr = zp32Value;
}

void CovertValueFrom32ZPSTo28Signed(lu_uint32_t zp32Value, lu_uint32_t *actualValuePtr)
{
	/*  24 bit value transmitted as a 32bit value
        with eight MSBs padded with 0s  */
	if(zp32Value & ADE78XX_28ZP_SIGNUM)
	{
		/* Signed value. Pad the 8 MSBs with 1s */
		zp32Value |= ADE78XX_28ZP_PAD;
	}
	
	*actualValuePtr = zp32Value;
}



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
*/
