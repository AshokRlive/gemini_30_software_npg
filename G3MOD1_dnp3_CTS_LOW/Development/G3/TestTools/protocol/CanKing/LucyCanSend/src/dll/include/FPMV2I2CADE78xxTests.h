/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [FPMV2 I2C ADE78xx Tests]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol global definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/05/15      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
/*!
 ******************************************************************************
 *   \brief Initialising ADE78xx Chip
 *
 *   Inializing I2C and SPI interfaces for ADE chip and selection of PM mode.
 *
 *   \module			Module Type
 *   \moduleId			Module ID
 *	 \fpiChan			SSP bus connected to ADE78xx
 *
 *   \return			SB_ERROR
 *
 ******************************************************************************
 */
extern SB_ERROR FPMInitialise( MODULE     module, 
						       MODULE_ID  moduleId,
						       lu_uint8_t fpiChan
					         );


/*!
 ******************************************************************************
 *   \brief Initialising ADE78xx Chip
 *
 *   Inializing I2C and SPI interfaces for ADE chip and selection of PM mode.
 *
 *   \module			Module Type
 *   \moduleId			Module ID
 *	 \fpiChan			SSP bus connected to ADE78xx
 *
 *   \return			SB_ERROR
 *
 ******************************************************************************
 */
extern SB_ERROR FPMDeinitialise( MODULE     module, 
						         MODULE_ID  moduleId,
						         lu_uint8_t fpiChan
					           );

/*!
 ******************************************************************************
 *   \brief Configuring ADE78xx Chip
 *
 *
 *   \module			Module Type
 *   \moduleId			Module ID
 *   \fpiChan			FPI Channel
 *   \gainRegAI         Gain value of AIGAIN reg
 *   \gainRegBI			Gain value of BIGAIN reg
 *	 \gainRegCI			Gain value of CIGAIN reg
 *
 *   \return			SB_ERROR
 *
 ******************************************************************************
 */
extern SB_ERROR FPMSetGain(  MODULE        module, 
							 MODULE_ID     moduleId,
						     lu_uint8_t    fpiChan, 
							 lu_uint32_t   gainRegAI,
							 lu_uint32_t   gainRegBI,
							 lu_uint32_t   gainRegCI,
							 lu_uint16_t   calPhaseA,
							 lu_uint16_t   calPhaseB,
							 lu_uint16_t   calPhaseC
						 );

/*!
 ******************************************************************************
 *   \brief Start / Stop ADE78xx Chip
 *
 *   Start / Stop ADE78xx Chip
 *
 *   \module			Module Type
 *   \moduleId			Module ID
  *	 \start             Start or Stop bit for ADE768xx
 * 
 *   \return			SB_ERROR
 *
 ******************************************************************************
 */
extern SB_ERROR FPMStart( MODULE      module, 
					      MODULE_ID   moduleId,
					      lu_uint8_t  fpiChan,
					      lu_uint8_t  start 
				        );

SB_ERROR FPMV2TestStartI2CADE78xx( MODULE       module, 
								   MODULE_ID    moduleId,
								   lu_uint8_t   i2cChan, 
								   lu_uint16_t  run
							     );

/*
*********************** End of file ******************************************
 */