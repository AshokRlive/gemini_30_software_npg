/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *   22/11/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
TestGPIOModeStr *GPIOModePtr;
TestGPIOReadStr *pinReadPtr;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR GPIOGetPinModeDll( MODULE module, 
							MODULE_ID moduleId, 
							lu_uint8_t port,
							lu_uint8_t pin,
							lu_uint8_t *port_r,
							lu_uint8_t *pin_r,
							lu_uint8_t *pinFunc_r,
							lu_uint8_t *pinMode_r,
							lu_uint8_t *openDrain_r,
							lu_uint8_t *dir_r
						 )
{
	SB_ERROR retError;
	TestGPIOPinPortStr GPIOGetPinMode;
	
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);
	
	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_GPIO_GET_PIN_MODE_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	GPIOGetPinMode.port = port;
	GPIOGetPinMode.pin  = pin;

	CANMsgFiller((lu_uint8_t*)&GPIOGetPinMode,sizeof(TestGPIOPinPortStr));

	retError = LucyCANSendMainDll();

	*port_r		 = GPIOModePtr->port;
	*pin_r       = GPIOModePtr->pin;
	*pinFunc_r   = GPIOModePtr->pinFunc;
	*pinMode_r   = GPIOModePtr->pinMode;
	*openDrain_r = GPIOModePtr->openDrain;
	*dir_r		 = GPIOModePtr->dir; 	

	return retError;
}

SB_ERROR GPIOSetPinModeDll( MODULE module, 
							MODULE_ID moduleId, 
						    lu_uint8_t    port,
							lu_uint8_t    pin,
							lu_uint8_t    pinFunc,
							lu_uint8_t    pinMode,
							lu_uint8_t	  openDrain,
							lu_uint8_t    dir
					      )
{
	SB_ERROR retError;
	TestGPIOModeStr GPIOSetPinMode;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_GPIO_SET_PIN_MODE_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;
	
	GPIOSetPinMode.port      = port;
	GPIOSetPinMode.pin       = pin;
	GPIOSetPinMode.pinFunc   = pinFunc;
	GPIOSetPinMode.pinMode   = pinMode;
	GPIOSetPinMode.openDrain = openDrain;
	GPIOSetPinMode.dir       = dir;

	CANMsgFiller ((lu_uint8_t*)&GPIOSetPinMode,sizeof(TestGPIOModeStr));
	
	retError = LucyCANSendMainDll();

	return retError;
}

SB_ERROR GPIOReadPinDll( MODULE module, 
					 MODULE_ID moduleId, 
					 lu_uint8_t port,
					 lu_uint8_t pin,
					 lu_uint8_t *pinValue
				   )
{
	SB_ERROR retError;
	TestGPIOPinPortStr GPIOReadPin;

	*pinValue = 0;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);
	
	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_GPIO_READ_PIN_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	GPIOReadPin.port = port;
	GPIOReadPin.pin  = pin;

	CANMsgFiller ((lu_uint8_t*)&GPIOReadPin,sizeof(TestGPIOPinPortStr));

	retError = LucyCANSendMainDll();

	if(retError == SB_ERROR_NONE)
	{
		*pinValue = pinReadPtr->value;
	}

	return retError;
}

SB_ERROR GPIOWritePinDll( MODULE module, 
						  MODULE_ID moduleId, 
					      lu_uint8_t port,
					      lu_uint8_t pin,
						  lu_uint8_t value
				        )
{
	SB_ERROR retError;
	TestGPIOWriteStr GPIOWritePin;
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_GPIO_WRITE_PIN_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;
	
	GPIOWritePin.port  = port;
	GPIOWritePin.pin   = pin;
	GPIOWritePin.value = value;

	CANMsgFiller ((lu_uint8_t*)(&GPIOWritePin),sizeof(TestGPIOWriteStr));

	retError = LucyCANSendMainDll();

	return retError;
}

SB_ERROR GPIOTestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_NONE;
		
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_SINGLE_COMMAND)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_BLTST:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_BLTST_GPIO_GET_PIN_MODE_R: 
						if(msgPtr->msgLen == sizeof(TestGPIOModeStr))
						{
							GPIOModePtr = (TestGPIOModeStr*)msgPtr->msgBufPtr;
							retError = SB_ERROR_NONE;
						}
						break;

					case MODULE_MSG_ID_BLTST_GPIO_SET_PIN_MODE_R: 
						if(msgPtr->msgLen == 0)
						{
							retError = SB_ERROR_NONE;
						}
						break;
					
					case MODULE_MSG_ID_BLTST_GPIO_READ_PIN_R: 
						if(msgPtr->msgLen == sizeof(TestGPIOReadStr))
						{
							pinReadPtr = (TestGPIOReadStr*)msgPtr->msgBufPtr;
							retError = SB_ERROR_NONE;
						}
						break;

					case MODULE_MSG_ID_BLTST_GPIO_WRITE_PIN_R: 
						if(msgPtr->msgLen == 0)
						{
							retError = SB_ERROR_NONE;
						}
						break;

					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;
			default:
				printf("\n Error in Downloading ");
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */