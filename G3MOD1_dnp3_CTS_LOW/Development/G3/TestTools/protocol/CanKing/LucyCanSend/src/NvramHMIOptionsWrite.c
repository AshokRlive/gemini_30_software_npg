/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: NvramFactoryInfoWrite.c 3512 2013-07-08 11:35:05Z saravanan_v $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 3512 $: (Revision of last commit)
 *               $Author: saravanan_v $: (Author of last commit)
 *       \date   $Date: 2013-07-08 12:35:05 +0100 (Mon, 08 Jul 2013) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   08/07/12     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "NvramHMIOptionsWrite.h"
#include "NVRAMDef.h"

#include "NVRAMDefOptHMI.h"
#include "KvaserCan.h"
#include "CmdParser.h"
#include "Main.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"

#include "NvramXMLParser.h"
#include "NvramReadWriteVerify.h"

#include "crc32.h"
#include "roxml.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
hmiOptionsBlkStr hmiOptionsInfo;

static strutureMapStr hmiOptionsElementPtrTable[] = 
{																
	{(void*)&hmiOptionsInfo.hmiOptions.optBlockVersionMajor,	sizeof(((NVRAMOptHMIStr*)NULL)->optBlockVersionMajor)	},
	{(void*)&hmiOptionsInfo.hmiOptions.optBlockVersionMinor,	sizeof(((NVRAMOptHMIStr*)NULL)->optBlockVersionMinor)	},
	{(void*)&hmiOptionsInfo.hmiOptions.displayType,		        sizeof(((NVRAMOptHMIStr*)NULL)->displayType)		    },
	{(void*)&hmiOptionsInfo.hmiOptions.contrastDigipotSet,	    sizeof(((NVRAMOptHMIStr*)NULL)->contrastDigipotSet)	}
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR NvramWriteHMIOptionsPrepare(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR retError = SB_ERROR_NONE;
	lu_uint8_t* hmiOptionsNVRamBufferPtr, i;

	/* Filling factory info bufer with 0xFF */
	memset(&hmiOptionsInfo, 0xFF, sizeof(hmiOptionsBlkStr));

	if(argsFromCommand->nvramAddr == ID_NVRAM_ADDRESS)
	{
		hmiOptionsInfo.nvRamAddr.addr.addr    = ID_NVRAM_ADDRESS;
		hmiOptionsInfo.nvRamAddr.addr.i2cChan = ID_NVRAM_I2C_CHANNEL;
		hmiOptionsInfo.nvRamAddr.addr.offset  = NVRAM_ID_BLK_OPTS_OFFSET;
	}

	if(argsFromCommand->nvramAddr == APP_NVRAM_ADDRESS)
	{
		hmiOptionsInfo.nvRamAddr.addr.addr    = APP_NVRAM_ADDRESS;
		hmiOptionsInfo.nvRamAddr.addr.i2cChan = APP_NVRAM_I2C_CHANNEL;
		hmiOptionsInfo.nvRamAddr.addr.offset  = NVRAM_APP_BLK_OPTS_OFFSET;
	}

	if(strncmp((lu_int8_t*)argsFromCommand->sourceFileName,"erase.xml",9) != 0)
	{		
		retError = FetchAndPackFactoryInfoFromXML( argsFromCommand->sourceFileName,
												   HMI_OPTIONS_FIELD_MAX,
												   hmiOptionsMap,
												   hmiOptionsElementPtrTable
												 );
		if(retError != SB_ERROR_PARAM)
		{
			hmiOptionsInfo.nvRamAddr.size = NVRAM_ID_BLK_INFO_SIZE;
			hmiOptionsInfo.factoryBlkHeader.dataSize = NVRAM_ID_BLK_OPTS_SIZE - sizeof(NVRAMBlkHeadStr);
		
			crc32_calc32( (lu_uint8_t*)&hmiOptionsInfo.hmiOptions, 
				          hmiOptionsInfo.factoryBlkHeader.dataSize, 
						  (lu_uint32_t*)&hmiOptionsInfo.factoryBlkHeader.dataCrc32
						);
		}
		else
		{
			printf("Error in Opening of XML File...");
			hmiOptionsInfo.nvRamAddr.addr.i2cChan = 0xFF;
			memcpy(&argsFromCommand->msgBuff[0], (lu_uint8_t*)&hmiOptionsInfo.nvRamAddr.addr, sizeof(hmiOptionsInfo.nvRamAddr.addr));
		}
	}
	return retError;
}

lu_bool_t NvramWriteHMIOptions(ParsingArgsStr *argsFromCommand)
{
	lu_bool_t finish;
	SB_ERROR retValue = SB_ERROR_NONE;
	flagDecoderPtr    = argsFromCommand;
	
	/* Writing and verifying PSM Options to NVRAM*/
	retValue = NvramWriteAndVerify( (MODULE)argsFromCommand->CANHeader.deviceDst,
								    (MODULE_ID)argsFromCommand->CANHeader.deviceIDDst,
								    hmiOptionsInfo.nvRamAddr,
								    (lu_uint8_t*)&hmiOptionsInfo.factoryBlkHeader
								  );
	Sleep(10);

	finish = LU_FALSE;

	return finish;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************

/*
 *********************** End of file ******************************************
*/