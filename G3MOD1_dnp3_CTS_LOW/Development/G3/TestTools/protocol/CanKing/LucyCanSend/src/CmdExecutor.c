/*! \file
******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
******************************************************************************
*    PROJECT:
*       G3 [Module Name]
*
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*       \brief
*
*       CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*
*
*    CREATION
*
*   Date          Name        Details
*   --------------------------------------------------------------------------
*   08/08/2012    venkat_s    Initial version / Original by Venkat.
*
******************************************************************************
*   COPYRIGHT
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
******************************************************************************
*/


/*
******************************************************************************
* INCLUDES - Standard Library Modules Used
******************************************************************************
*/

/*
******************************************************************************
* INCLUDES - Project Specific Modules Used
******************************************************************************
*/
#include "lu_types.h"
#include "Main.h"
#include "CmdExecutor.h"
#include "SingleCmdSender.h"
#include "FirmwareDownloader.h"
#include "FirmwareVerifier.h"
#include "CalElementReader.h"
#include "CalElementAdder.h"
#include "NvramFactoryInfoRead.h"
#include "NvramFactoryInfoWrite.h"
#include "NvramPSMOptionsWrite.h"
#include "NvramBatteryOptionsWrite.h"
#include "NvramPSMOptionsRead.h"
#include "NvramBatteryOptionsRead.h"
#include "CmdParser.h"

/*
******************************************************************************
* LOCAL - Definitions And Macros
******************************************************************************
*/

/*
******************************************************************************
* LOCAL - Typedefs and Structures
******************************************************************************
*/


/*
*******************************************************************************
* LOCAL - Prototypes Of Local Functions
*******************************************************************************
*/


/*
******************************************************************************
* EXPORTED - Variables
******************************************************************************
*/


/*
******************************************************************************
* LOCAL - Global Variables
******************************************************************************
*/

/*
******************************************************************************
* Exported Functions
******************************************************************************
*/

void CommandExecutorInitialize(ParsingArgsStr *argsFromCommand)
{
	switch(argsFromCommand->parserFlag)
	{
	case PARSER_FLAG_PIC_FIRMWARE_DOWNLOAD:
		PicProgDownloaderFilePrepare(argsFromCommand);
		break;

	case PARSER_FLAG_FIRMWARE_DOWNLOAD:
		{
			FirmwareDownloaderFilePrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_FIRMWARE_VERIFY:
		{
			FirmwareVerifierFilePrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_CAL_ADD_ELEMENT:
		{
			CalElementAdderPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_CAL_READ_ELEMENT:
		{
			CalElementReaderPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_READ_FACTORY:
		{
			NvramReadFactoryInfoPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_FACTORY:
		{
			NvramWriteFactoryInfoPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_READ_PSM_OPTS:
		{
			NvramReadPSMOptionsPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_PSM_OPTS:
		{
			NvramWritePSMOptionsPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_BATT_OPTS:
		{
			NvramWriteBatteryOptionsPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_READ_BATT_OPTS:
		{
			NvramReadBatteryOptionsPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_SINGLE_COMMAND:
		{
			SendSingleCommandPrepare(argsFromCommand);
		}
		break;

	case PARSER_FLAG_SINGLE_COMMAND_LOOP:
		{
			SendSingleCommandLoopPrepare(argsFromCommand);
		}
		break;

	default:
		{
			printf("No Command has given. Please see Help for Details...\n\n");
		}
		break;
	}

}

PARSER_FLAG CommandExecutorRun(ParsingArgsStr *argsFromCommand)
{
    switch(argsFromCommand->parserFlag)
	{
    case PARSER_FLAG_PIC_FIRMWARE_DOWNLOAD:
    	PicProgDownloaderBlockSend(argsFromCommand);
		break;

	case PARSER_FLAG_FIRMWARE_DOWNLOAD:
		{
			FirmwareDownloaderBlockSend(&firmwareFile, argsFromCommand);
		}
		break;

	case PARSER_FLAG_FIRMWARE_VERIFY:
		{
			FirmwareVerifierGetBlock(&srcFirmwareFile, &dstFirmwareFile, argsFromCommand);
		}
		break;

	case PARSER_FLAG_CAL_ADD_ELEMENT:
		{
			CalElementAdderSend(argsFromCommand);
		}
		break;

	case PARSER_FLAG_CAL_READ_ELEMENT:
		{
			CalElementReaderSend(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_READ_FACTORY:
		{
			NvramReadFactoryInfo(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_FACTORY:
		{
			NvramWriteFactoryInfo(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_PSM_OPTS:
		{
			NvramWritePSMOptions(argsFromCommand);
		}
		break;
	
	case PARSER_FLAG_NVRAM_READ_PSM_OPTS:
		{
			NvramReadPSMOptions(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_WRITE_BATT_OPTS:
		{
			NvramWriteBatteryOptions(argsFromCommand);
		}
		break;

	case PARSER_FLAG_NVRAM_READ_BATT_OPTS:
		{
			NvramReadBatteryOptions(argsFromCommand);
		}
		break;

	case PARSER_FLAG_SINGLE_COMMAND:
		{
			SendSingleCommand(argsFromCommand);
		}
		break;

	case PARSER_FLAG_SINGLE_COMMAND_LOOP:
		{
			SendSingleCommandLoop(argsFromCommand);
		}
		break;
	
	default:
		{
			printf("Error in Command Execution");
		}
		break;
	}
}

/*
******************************************************************************
* Local Functions
******************************************************************************
*/


/*!
******************************************************************************
*   \brief Brief description
*
*   Detailed description
*
*   \param parameterName Description 
*
*
*   \return 
*
******************************************************************************
*/

/*
*********************** End of file ******************************************
*/

