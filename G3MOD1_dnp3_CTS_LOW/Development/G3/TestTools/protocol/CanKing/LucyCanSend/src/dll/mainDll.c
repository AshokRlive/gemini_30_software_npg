#include <windows.h>
#include <string.h>
#include <ctype.h>

#include "Main.h"
#include "mainDll.h"
#include "CmdParser.h"
#include "CmdExecutorDLL.h"
#include "LucyCANDecoderDLL.h"

BOOL WINAPI  DllMain ( HINSTANCE    hModule,
					   DWORD     dwFunction,
					   LPVOID    lpNot
					 ) 
{
    return TRUE;
}

lu_uint32_t CANCodecInitDll(void)
{
	return CANCodecInitDllVersion( MODULE_MCM,
								   MODULE_ID_0,
								   LucyCANProtocolDecoderDll
								 );
}

lu_uint32_t CANCodecDeInitDll(lu_uint32_t handle)
{
	return CANCodecDeInitDllVersion(handle);
}

SB_ERROR LucyCANSendMainDll(void)
{
	SB_ERROR ret;
	lu_bool_t finish;
	
	ret = CommandExecutorInitialize(&argsFromCommand);

	if ((argsFromCommand.parserFlag != PARSER_FLAG_NONE) && 
		(ret == SB_ERROR_NONE)
	   )
	{
		while (LU_TRUE)
		{
			finish = CommandExecutorRun(&argsFromCommand);
			ret = CANCDecodeDll();
			
			if (finish == LU_FALSE || ret == SB_ERROR_CANC_NO_DATA )
			{
				return ret;
			}
		}
	}
	else

	{
		ret = SB_ERROR_PARAM;
	}

	return ret;
}

void CANHeaderFiller ( MODULE module, 
					   MODULE_ID moduleId
					 )
{
	argsFromCommand.CANHeader.deviceDst   = module;
	argsFromCommand.CANHeader.deviceIDDst = moduleId;
	argsFromCommand.CANHeader.deviceSrc   = MODULE_MCM;
	argsFromCommand.CANHeader.deviceIDSrc = MODULE_ID_0;
}

void CANMsgFiller ( lu_uint8_t *msgBuff,
					lu_uint16_t length
				  )
{
	lu_uint16_t i;

	for(i = 0; i < length; i++)
	{
		argsFromCommand.msgBuff[i] = msgBuff[i];
	}
	
	argsFromCommand.msgBuffLen = length;
	argsFromCommand.CANHeader.fragment    = 0;

	if(length > 8)
	{
		argsFromCommand.CANHeader.fragment    = 1;
	}
}

