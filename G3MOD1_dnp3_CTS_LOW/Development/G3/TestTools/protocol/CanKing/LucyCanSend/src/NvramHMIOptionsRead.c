/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: NvramFactoryInfoRead.c 3512 2013-07-08 11:35:05Z saravanan_v $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 3512 $: (Revision of last commit)
 *               $Author: saravanan_v $: (Author of last commit)
 *       \date   $Date: 2013-07-08 12:35:05 +0100 (Mon, 08 Jul 2013) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30/08/12     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "NVRAMDef.h"

#include "KvaserCan.h"
#include "CmdParser.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"

#include "NvramHMIOptionsRead.h"
#include "NvramXMLParser.h"

#include "crc32.h"
#include "roxml.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
*/

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
hmiOptionsBlkStr readHMIOptions;

static strutureMapStr hmiOptionsElementPtrTable[] = 
{																	//sizeof(((struct A*)0)->arr);
	{(void*)&readHMIOptions.hmiOptions.optBlockVersionMajor,	sizeof(((NVRAMOptHMIStr*)NULL)->optBlockVersionMajor)	},
	{(void*)&readHMIOptions.hmiOptions.optBlockVersionMinor,	sizeof(((NVRAMOptHMIStr*)NULL)->optBlockVersionMinor)	},
	{(void*)&readHMIOptions.hmiOptions.displayType,			    sizeof(((NVRAMOptHMIStr*)NULL)->displayType)		    },
	{(void*)&readHMIOptions.hmiOptions.contrastDigipotSet,	    sizeof(((NVRAMOptHMIStr*)NULL)->contrastDigipotSet)	    }
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR NvramReadHMIOptionsPrepare(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR retError = SB_ERROR_NONE;

	if(argsFromCommand->nvramAddr == ID_NVRAM_ADDRESS)
	{
		readHMIOptions.nvRamAddr.addr.addr    = ID_NVRAM_ADDRESS;
		readHMIOptions.nvRamAddr.addr.i2cChan = ID_NVRAM_I2C_CHANNEL;
		readHMIOptions.nvRamAddr.addr.offset  = NVRAM_ID_BLK_OPTS_OFFSET;
		readHMIOptions.nvRamAddr.size         = NVRAM_ID_BLK_OPTS_SIZE;
	}
	
	if(argsFromCommand->nvramAddr == APP_NVRAM_ADDRESS)
	{
		readHMIOptions.nvRamAddr.addr.addr    = APP_NVRAM_ADDRESS;
		readHMIOptions.nvRamAddr.addr.i2cChan = APP_NVRAM_I2C_CHANNEL;
		readHMIOptions.nvRamAddr.addr.offset  = NVRAM_APP_BLK_OPTS_OFFSET;
		readHMIOptions.nvRamAddr.size         = NVRAM_APP_BLK_OPTS_SIZE;
	}

	memcpy((lu_uint8_t*)&argsFromCommand->msgBuff[0], (lu_uint8_t*)&readHMIOptions, sizeof(TestNVRAMReadBuffStr));

	return retError;
}

lu_bool_t NvramReadHMIOptions(ParsingArgsStr *argsFromCommand)
{
	lu_bool_t finish;
	SB_ERROR retValue = SB_ERROR_NONE;
	flagDecoderPtr    = argsFromCommand;
	
	/* Sending the firmware block via CAN bus */
	retValue = CANCSendFromMCM( MODULE_MSG_TYPE_BLTST,
								MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_C,
								argsFromCommand->CANHeader.deviceDst,
								argsFromCommand->CANHeader.deviceIDDst,
								sizeof(TestNVRAMReadBuffStr),
								(lu_uint8_t*)&argsFromCommand->msgBuff
							  );
	Sleep(1000);

	finish = LU_FALSE;

	return finish;
}

SB_ERROR LucyCANSendNvramReadHMIOptionsProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_CANC_NOT_HANDLED;
	
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_NVRAM_READ_HMI_OPTS)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_BLTST:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_R: 
						if(msgPtr->msgLen == NVRAM_APP_BLK_INFO_SIZE)
						{
							memcpy(&readHMIOptions.factoryBlkHeader, msgPtr->msgBufPtr, sizeof(hmiOptionsBlkStr)); 
							retError = BundleNVRAMDataToXMLFile( msgPtr->msgBufPtr,
																 (lu_int8_t*)flagDecoderPtr->destinationFileName,
																 HMI_OPTIONS_FIELD_MAX,
																 &hmiOptionsElementPtrTable[0],
																 &hmiOptionsMap[0]	
															   );
						}
						break;
						
					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;

			default:
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
*/