/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1577 2012-07-23 20:18:25Z fryers_j $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1577 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-07-23 21:18:25 +0100 (Mon, 23 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/11/14     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "ADE78xxSPIDLL.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

lu_uint32_t *readRegValue;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR FPMV2ADE78xxSPIEnable( MODULE      module, 
							    MODULE_ID   moduleId,
								lu_uint8_t  sspBus,
								lu_uint8_t  csPort,
								lu_uint8_t  csPin,
								lu_uint8_t  pm0Port,
								lu_uint8_t  pm0Pin,
								lu_uint8_t  pm1Port,
								lu_uint8_t  pm1Pin,
								lu_uint8_t  resetPort,
								lu_uint8_t  resetPin,
								lu_uint8_t  irq0Port,
								lu_uint8_t  irq0Pin,
								lu_uint8_t  irq1Port,
								lu_uint8_t  irq1Pin,
								lu_uint8_t  cf1Port,
								lu_uint8_t  cf1Pin,
								lu_uint8_t  cf2Port,
								lu_uint8_t  cf2Pin
						     )
{
	SB_ERROR retError;
	TestSpiADE78xxInitStr ade78xxSpiParam;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST_1;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_1_ADE78xx_ENABLE_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	ade78xxSpiParam.sspBus             = sspBus;
	ade78xxSpiParam.csPort             = csPort;
	ade78xxSpiParam.csPin              = csPin;
	ade78xxSpiParam.pinMap.pm0.port    = pm0Port;
	ade78xxSpiParam.pinMap.pm0.pin     = pm0Pin;
	ade78xxSpiParam.pinMap.pm1.port    = pm1Port;
	ade78xxSpiParam.pinMap.pm1.pin     = pm1Pin;
	ade78xxSpiParam.pinMap.reset.port  = resetPort;
	ade78xxSpiParam.pinMap.reset.pin   = resetPin;
	ade78xxSpiParam.pinMap.irq0.port   = irq0Port;
	ade78xxSpiParam.pinMap.irq0.pin    = irq0Pin;
	ade78xxSpiParam.pinMap.irq1.port   = irq1Port;
	ade78xxSpiParam.pinMap.irq1.pin    = irq1Pin;
	ade78xxSpiParam.pinMap.cf1.port   = cf1Port;
	ade78xxSpiParam.pinMap.cf1.pin    = cf1Pin;
	ade78xxSpiParam.pinMap.cf2.port   = cf2Port;
	ade78xxSpiParam.pinMap.cf2.pin    = cf2Pin;
		
	CANMsgFiller((lu_uint8_t*)&ade78xxSpiParam,sizeof(TestSpiADE78xxInitStr));

	retError = LucyCANSendMainDll();
		
	return retError;
}

SB_ERROR FPMV2ADE78xxSPIReadReg( MODULE       module, 
								 MODULE_ID    moduleId,
								 lu_uint8_t   sspBus,
								 lu_uint8_t   csPort,
								 lu_uint8_t   csPin,
								 lu_uint16_t  reg,
								 lu_uint8_t   length,
								 lu_uint32_t  *readValuePtr
						       )
{
	SB_ERROR retError;
	TestSpiADE78xxReadRegStr readRegParam;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST_1;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_1_ADE78xx_READ_REG_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	readRegParam.sspBus  = sspBus;
	readRegParam.csPort  = csPort;
	readRegParam.csPin   = csPin;
	readRegParam.reg     = reg;
	readRegParam.length  = length;
	readRegParam.value   = 0;
		
	CANMsgFiller((lu_uint8_t*)&readRegParam,sizeof(TestSpiADE78xxReadRegStr));

	retError = LucyCANSendMainDll();

	*readValuePtr = *readRegValue;
	
	return retError;
}

SB_ERROR FPMV2ADE78xxSPIWriteReg( MODULE       module, 
								  MODULE_ID    moduleId,
								  lu_uint8_t   sspBus,
								  lu_uint8_t   csPort,
								  lu_uint8_t   csPin,
								  lu_uint16_t  reg,
								  lu_uint8_t   length,
							      lu_uint32_t  value
						        )
{
	SB_ERROR retError;
	TestSpiADE78xxWriteRegStr writeRegParam;
	lu_uint16_t byteCount = 0;

	moduleId       = 0;
	
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST_1;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_1_ADE78xx_WRITE_REG_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	writeRegParam.sspBus = sspBus;
	writeRegParam.csPort = csPort;
	writeRegParam.csPin  = csPin;
	writeRegParam.reg    = reg;
	writeRegParam.length = length; 
	writeRegParam.value  = value;
		
	memcpy(&argsFromCommand.msgBuff[0], &writeRegParam, sizeof(TestSpiADE78xxWriteRegStr));
	
	argsFromCommand.msgBuffLen = sizeof(TestSpiADE78xxWriteRegStr);

	retError = LucyCANSendMainDll();
		
	return retError;
}

SB_ERROR FPMV2ADE78xxSPITestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_PARAM;
		
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_SINGLE_COMMAND)
	{
		switch (msgPtr->messageType)
		{
		case MODULE_MSG_TYPE_BLTST_1:
			switch(msgPtr->messageID)
			{
				case MODULE_MSG_ID_BLTST_1_ADE78xx_ENABLE_R: 
					if(msgPtr->msgLen == sizeof(lu_uint8_t))
						{
							retError = (SB_ERROR)(*msgPtr->msgBufPtr);
						}
					break;

				case MODULE_MSG_ID_BLTST_1_ADE78xx_READ_REG_R: 
					if(msgPtr->msgLen == sizeof(lu_uint32_t))
						{
							readRegValue = (lu_uint32_t*)msgPtr->msgBufPtr;
							retError = SB_ERROR_NONE;
						}
					break;

				case MODULE_MSG_ID_BLTST_1_ADE78xx_WRITE_REG_R: 
					if(msgPtr->msgLen == sizeof(lu_uint8_t))
						{
							retError = (SB_ERROR)(*msgPtr->msgBufPtr);
						}
					break;
				
				default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
					break;
			}
			break;

			default:
					retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	  }
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
*/