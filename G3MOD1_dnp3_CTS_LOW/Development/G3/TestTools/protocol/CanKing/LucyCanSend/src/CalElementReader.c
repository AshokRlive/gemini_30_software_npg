/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1577 2012-07-23 20:18:25Z fryers_j $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1577 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-07-23 21:18:25 +0100 (Mon, 23 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30/08/12     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleProtocol.h"
#include "CANProtocol.h"

#include "CalElementReader.h"
#include "KvaserCan.h"
#include "CmdParser.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"
#include "Calibration.h"
#include "LinearInterpolation.h"
#include "ADE78xxConfig.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
SB_ERROR StoreCalElement(lu_uint8_t *calFileName, CANFramingMsgStr *msgPtr);
void Write1DU16S16CalElementToFile(FILE *calElementFilePtr, CalElementStr *calElementPtr, CANFramingMsgStr *msgPtr);
void Write1DU16U16CalElementToFile(FILE *calElementFilePtr, CalElementStr *calElementPtr, CANFramingMsgStr *msgPtr);
void Write1DU16U32CalElementToFile(FILE *calElementFilePtr, CalElementStr *calElementPtr, CANFramingMsgStr *msgPtr);
void Write1DU32U32CalElementToFile(FILE *calElementFilePtr, CalElementStr *calElementPtr, CANFramingMsgStr *msgPtr);
void WriteAde78xxRegCalElementToFile();

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR CalElementReaderPrepare(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR retError = SB_ERROR_NONE;

	argsFromCommand->msgBuff[0] = argsFromCommand->calID;
	
	argsFromCommand->msgBuffLen = sizeof(argsFromCommand->calID);

	return retError;
}

lu_bool_t CalElementReaderSend(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR ret;
	lu_bool_t finish;
	
	flagDecoderPtr = argsFromCommand;

	argsFromCommand->CANHeader.messageType = MODULE_MSG_TYPE_CALTST;
	argsFromCommand->CANHeader.messageID   = MODULE_MSG_ID_CALTST_READ_ELEMENT_C;

	ret = CANCSendFromMCM( argsFromCommand->CANHeader.messageType,
						   argsFromCommand->CANHeader.messageID,
						   argsFromCommand->CANHeader.deviceDst,
			               argsFromCommand->CANHeader.deviceIDDst,
			               argsFromCommand->msgBuffLen,
			               argsFromCommand->msgBuff
			             );
	finish = LU_FALSE;

	return finish; 
}

SB_ERROR  LucyCANSendCalElementReaderProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError;
	
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_CAL_READ_ELEMENT)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_CALTST:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_CALTST_READ_ELEMENT_R: 
						{
							StoreCalElement(flagDecoderPtr->destinationFileName, msgPtr);
							retError = SB_ERROR_NONE;
						}
						break;
					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;
			default:
				printf("\n Error Reading Calibration Element");
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
/*!
 ******************************************************************************
 *   \brief 
 *
 *   \param parameterName Description 
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR StoreCalElement(lu_uint8_t *calFileName, CANFramingMsgStr *msgPtr)
{
	SB_ERROR retError;
	FILE *calElementFilePtr;
	CalElementStr *calElementPtr;

	calElementFilePtr = fopen((lu_int8_t*)calFileName,"w");
	if(calElementFilePtr == NULL)
	{
		printf(" Error in opening file ");
		fclose(calElementFilePtr);
		retError = SB_ERROR_INITIALIZED;
	} 
	
	calElementPtr  = (CalElementStr*)msgPtr->msgBufPtr;

	if(calElementPtr->calID == flagDecoderPtr->calID)
	{
		switch(calElementPtr->calType)
		{
			case CAL_TYPE_1DU16S16:
				{
					Write1DU16S16CalElementToFile(calElementFilePtr, calElementPtr, msgPtr);
				}
				break;

			case CAL_TYPE_1DU16U16:
				{
					Write1DU16U16CalElementToFile(calElementFilePtr, calElementPtr, msgPtr);
				}
				break;

			case CAL_TYPE_1DU16U32:
				{
					Write1DU16U32CalElementToFile(calElementFilePtr, calElementPtr, msgPtr);
				}
				break;
			
			case CAL_TYPE_1DU32U32:
				{
					Write1DU32U32CalElementToFile(calElementFilePtr, calElementPtr, msgPtr);
				}
				break;

			case CAL_TYPE_1DU16U16_CT_TEMP_COMP:
			case CAL_TYPE_1DU16U16_CT_TEMP_COMP_SUM:
				{
					Write1DU16U16CalElementToFile(calElementFilePtr, calElementPtr, msgPtr);
				}
				break;
			
			default:
				printf("Error in Cal Type");
				retError = SB_ERROR_PARAM;
				break;
		}

	}
	
	fclose(calElementFilePtr);
}

void Write1DU16S16CalElementToFile(FILE *calElementFilePtr, CalElementStr *calElementPtr, CANFramingMsgStr *msgPtr)
{
	lu_uint8_t numberOfCalValues = 0;
	LnInterpTable1DU16S16Str *calType1du16s16Ptr;
	
	numberOfCalValues  = (msgPtr->msgLen - (sizeof(calElementPtr->calID) + sizeof(calElementPtr->calType) + sizeof(calElementPtr->calDatalength)))
										  / sizeof(LnInterpTable1DU16S16Str);
	calType1du16s16Ptr = (LnInterpTable1DU16S16Str*)&calElementPtr->calDataPtr;

	while(numberOfCalValues != 0)
	{
		fprintf(calElementFilePtr, "%ld,",calType1du16s16Ptr->distributionX);
		fprintf(calElementFilePtr, "%ld\n",calType1du16s16Ptr->interpolateToX);
		numberOfCalValues--; calType1du16s16Ptr++;
	}

}

void Write1DU16U16CalElementToFile(FILE *calElementFilePtr, CalElementStr *calElementPtr, CANFramingMsgStr *msgPtr)
{
	lu_uint8_t numberOfCalValues = 0;
	LnInterpTable1DU16U16Str *calType1du16u16Ptr;
	
	numberOfCalValues  = (msgPtr->msgLen - (sizeof(calElementPtr->calID) + sizeof(calElementPtr->calType) + sizeof(calElementPtr->calDatalength)))
										  / sizeof(LnInterpTable1DU16U16Str);
	calType1du16u16Ptr = (LnInterpTable1DU16U16Str*)&calElementPtr->calDataPtr;

	while(numberOfCalValues != 0)
	{
		fprintf(calElementFilePtr, "%ld,",calType1du16u16Ptr->distributionX);
		fprintf(calElementFilePtr, "%ld\n",calType1du16u16Ptr->interpolateToX);
		numberOfCalValues--; calType1du16u16Ptr++;
	}

}

void Write1DU16U32CalElementToFile(FILE *calElementFilePtr, CalElementStr *calElementPtr, CANFramingMsgStr *msgPtr)
{
	lu_uint8_t numberOfCalValues = 0;
	LnInterpTable1DU16U32Str *calType1du16u32Ptr;
	
	numberOfCalValues  = (msgPtr->msgLen - (sizeof(calElementPtr->calID) + sizeof(calElementPtr->calType) + sizeof(calElementPtr->calDatalength)))
										  / sizeof(LnInterpTable1DU16U32Str);
	calType1du16u32Ptr = (LnInterpTable1DU16U32Str*)&calElementPtr->calDataPtr;

	while(numberOfCalValues != 0)
	{
		fprintf(calElementFilePtr, "%ld,",calType1du16u32Ptr->distributionX);
		fprintf(calElementFilePtr, "%ld\n",calType1du16u32Ptr->interpolateToX);
		numberOfCalValues--; calType1du16u32Ptr++;
	}

}

void Write1DU32U32CalElementToFile(FILE *calElementFilePtr, CalElementStr *calElementPtr, CANFramingMsgStr *msgPtr)
{
	lu_uint8_t numberOfCalValues = 0;
	LnInterpTable1DU32U32Str *calType1du32u32Ptr;
	
	numberOfCalValues  = (msgPtr->msgLen - (sizeof(calElementPtr->calID) + sizeof(calElementPtr->calType) + sizeof(calElementPtr->calDatalength)))
										  / sizeof(LnInterpTable1DU32U32Str);
	calType1du32u32Ptr = (LnInterpTable1DU32U32Str*)&calElementPtr->calDataPtr;

	while(numberOfCalValues != 0)
	{
		fprintf(calElementFilePtr, "%ld,",calType1du32u32Ptr->distributionX);
		fprintf(calElementFilePtr, "%ld\n",calType1du32u32Ptr->interpolateToX);
		numberOfCalValues--; calType1du32u32Ptr++;
	}

}


/*
 *********************** End of file ******************************************
*/
