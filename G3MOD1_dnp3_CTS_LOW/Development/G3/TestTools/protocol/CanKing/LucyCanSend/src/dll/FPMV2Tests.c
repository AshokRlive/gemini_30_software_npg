/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1577 2012-07-23 20:18:25Z fryers_j $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1577 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-07-23 21:18:25 +0100 (Mon, 23 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/11/14     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "ADE78xx.h"
#include "FPMV2Tests.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define ADE78XX_24ZP_SIGNUM     (0x08000000)
#define ADE78XX_24ZP_PAD        (0xFF000000)
#define ADE78XX_28ZP_SIGNUM     (0x00800000)
#define ADE78XX_28ZP_PAD        (0xF0000000)

/*!
 * \brief Convert a number from the 32bit standard format to the 28ZP format.
 *
 * The input value range is NOT checked
 */
#define ADE78XX_32_2_28ZP(_val_) (_val_ &= (~(ADE78XX_28ZP_PAD)))

//#define ADE78XX_IGAIN -8385324
//#define ADE78XX_VGAIN -8348885

#define ADE78XX_IGAIN -8336000
#define ADE78XX_VGAIN -8348885
#define ADE78XX_IRMSGAIN -8310000

#define ADE78XX_HSDC_CONFIG 0x0A

#define ADE78XX_CONFIG_A 0xE740

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void CovertValueFrom32ZPSTo24Signed(lu_uint32_t zp32Value, lu_uint32_t *actualValue);
void CovertValueFrom32ZPSTo28Signed(lu_uint32_t zp32Value, lu_uint32_t *actualValue);
void CovertValueFrom32ZPSTo28Unsigned(lu_uint32_t zp32Value, lu_uint32_t *actualValuePtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static lu_int32_t ade78xxAIgainReg;
static lu_int32_t ade78xxBIgainReg;
static lu_int32_t ade78xxCIgainReg;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR FPMV2TestConfigureADE78xx(  MODULE       module, 
									 MODULE_ID    moduleId,
									 lu_uint8_t   sspBus, 
									 lu_uint8_t   csPort,
		  							 lu_uint8_t   csPin,
									 lu_int32_t   gainRegAI,
									 lu_int32_t   gainRegBI,
									 lu_int32_t   gainRegCI
								  )
{
	SB_ERROR    retError;
	lu_uint32_t retValue;
	lu_int32_t  param;
	lu_uint32_t retVal16U;
	
	/* Reset all IRQs. Reset STATUS0 and STATUS1 registers*/
	retError = FPMV2ADE78xxSPIReadReg(module, moduleId, sspBus, csPort, csPin, ADE78XX_STATUS0, sizeof(lu_uint32_t), &retValue);
	Sleep(100);
	retError = FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin,	ADE78XX_STATUS0, sizeof(lu_uint32_t), retValue);
	Sleep(100);
									  
	retError = FPMV2ADE78xxSPIReadReg(module, moduleId, sspBus, csPort, csPin, ADE78XX_STATUS1, sizeof(lu_uint32_t), &retValue);
	Sleep(100);
	retError = FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin,	ADE78XX_STATUS1, sizeof(lu_uint32_t), retValue);
	Sleep(100);
	
	/* Initialise Registers */

	/* Current Gain register */
	ade78xxAIgainReg = gainRegAI;
	//CovertValueFrom32ZPSTo24Signed(ade78xxAIgainReg);
	retError = FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin,	ADE78XX_AIGAIN, sizeof(lu_uint32_t), (lu_uint32_t)ade78xxAIgainReg);
	Sleep(100);
	
	ade78xxBIgainReg    = gainRegBI;
	//ADE78XX_32_2_24ZP(ade78xxBIgainReg);
	retError = FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin,	ADE78XX_BIGAIN, sizeof(lu_uint32_t), (lu_uint32_t)ade78xxBIgainReg);
	Sleep(100);

	ade78xxCIgainReg    = gainRegCI;
	//ADE78XX_32_2_28ZP(ade78xxCIgainReg);
	retError = FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin,	ADE78XX_CIGAIN, sizeof(lu_uint32_t), (lu_uint32_t)ade78xxCIgainReg);
	Sleep(100);

	param    = ade78xxCIgainReg;
	//ADE78XX_32_2_28ZP(param);
	retError = FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin,	ADE78XX_NIGAIN, sizeof(lu_uint32_t), (lu_uint32_t)param);
	

	/* Voltage Gain register */
	//param = ADE78XX_VGAIN;
	//ADE78XX_32_2_28ZP(param);
	
	/*
	 retError = FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin,	ADE78XX_AVGAIN, sizeof(lu_uint32_t), (lu_uint32_t)param);
	 retError = FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin,	ADE78XX_BVGAIN, sizeof(lu_uint32_t), (lu_uint32_t)param);
	 retError = FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin,	ADE78XX_CVGAIN, sizeof(lu_uint32_t), (lu_uint32_t)param);
	*/
	FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin, ADE78XX_CONFIG_A, sizeof(lu_uint8_t), (lu_uint32_t)0x01);
	Sleep(100);

	retError = FPMV2ADE78xxSPIReadReg(module, moduleId, sspBus, csPort, csPin, ADE78XX_CONFIG, sizeof(lu_uint16_t), &retVal16U );
	
	FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin, ADE78XX_CONFIG, sizeof(lu_uint16_t), retVal16U);
	Sleep(100);

	/* Write the last register 2 more times to flush the DSP internal pipeline */
	FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin, ADE78XX_CONFIG, sizeof(lu_uint16_t), (lu_uint32_t)retVal16U);
	Sleep(100);
	FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin, ADE78XX_CONFIG, sizeof(lu_uint16_t), (lu_uint32_t)retVal16U);
	Sleep(100);

	/* Enable memory protection */
	//FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin, 0xE7FE, sizeof(lu_uint8_t), 0x000000AD);
	//FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin, 0xE7E3, sizeof(lu_uint8_t), 0x00000080);

	/* RUN DSP */
	FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin, ADE78XX_RUN, sizeof(lu_uint16_t), 0x00000001);

	return retError;
}

SB_ERROR FPMV2TestStartADE78xx( MODULE     module, 
								MODULE_ID  moduleId,
								lu_uint8_t sspBus, 
								lu_uint8_t csPort,
								lu_uint8_t csPin,
								lu_uint16_t run
							  )
{
	SB_ERROR retError = SB_ERROR_PARAM;

	retError = FPMV2ADE78xxSPIWriteReg( module,
										moduleId,
										sspBus,
										csPort,
										csPin,
										ADE78XX_RUN,
										sizeof(lu_uint16_t),
										run
									  );

	return retError;
}

SB_ERROR FPMV2TestReadIrms( MODULE      module, 
							MODULE_ID   moduleId,
							lu_uint8_t  sspBus, 
							lu_uint8_t  csPort,
							lu_uint8_t  csPin,
							lu_uint8_t  phase,
							lu_uint32_t *IrmsPtr
						  )
{
	SB_ERROR retError = SB_ERROR_PARAM;
	volatile lu_uint16_t phaseReg;
	lu_uint32_t Irms32zp = 0; 

	switch(phase)
	{
		case 1:
			phaseReg = ADE78XX_AIRMS;
			retError = SB_ERROR_NONE;
			break;
		
		case 2:
			phaseReg = ADE78XX_BIRMS;
			retError = SB_ERROR_NONE;
			break;
		
		case 3:
			phaseReg = ADE78XX_CIRMS;
			retError = SB_ERROR_NONE;
			break;
		
		case 4:
			phaseReg = ADE78XX_NIRMS;
			retError = SB_ERROR_NONE;
			break;

		case 5:
			phaseReg = ADE78XX_ISUM;
			retError = SB_ERROR_NONE;
			break;
	
		default:
			retError = SB_ERROR_PARAM;
			break;
	}

	if(retError == SB_ERROR_NONE)
	{
		retError = FPMV2ADE78xxSPIReadReg(  module,
										    moduleId,
											sspBus,
											csPort,
											csPin,
											phaseReg,
											sizeof(lu_uint32_t),
											&Irms32zp
										);
	}

	if(phaseReg == ADE78XX_ISUM)
	{
		CovertValueFrom32ZPSTo28Unsigned(Irms32zp, IrmsPtr);
	}
	else
	{
		//CovertValueFrom32ZPSTo24Signed(Irms32zp, IrmsPtr);
		*IrmsPtr = Irms32zp;
	}
	
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Conversion of 32bit two's compliment to 24 bit value
 *	  
 *   Conversion of 32bit two's compliment to 24 bit value
 *
 *   \zp32Value         32ZP input value
 *   \actualValuePtr 	Pointer to converted value	
 *
 *   \return 
 *
 ******************************************************************************
 */
void CovertValueFrom32ZPSTo24Signed(lu_uint32_t zp32Value, lu_uint32_t *actualValuePtr)
{
	/*  24 bit value transmitted as a 32bit value
        with eight MSBs padded with 0s  */
	if(zp32Value & ADE78XX_24ZP_SIGNUM)
	{
		/* Signed value. Pad the 8 MSBs with 1s */
		zp32Value |= ADE78XX_24ZP_PAD;
	}
	
	*actualValuePtr = zp32Value;
}

/*!
 ******************************************************************************
 *   \brief Conversion of 32bit two's compliment to 28 bit value
 *	  
 *   Conversion of 32bit two's compliment to 28 bit value
 *
 *   \zp32Value         32ZP input value
 *   \actualValuePtr 	Pointer to converted value	
 *
 *   \return 
 *
 ******************************************************************************
 */
void CovertValueFrom32ZPSTo28Signed(lu_uint32_t zp32Value, lu_uint32_t *actualValuePtr)
{
	/*  24 bit value transmitted as a 32bit value
        with eight MSBs padded with 0s  */
	if(zp32Value & ADE78XX_28ZP_SIGNUM)
	{
		/* Signed value. Pad the 8 MSBs with 1s */
		zp32Value |= ADE78XX_28ZP_PAD;
	}
	
	*actualValuePtr = zp32Value;
}

/*!
 ******************************************************************************
 *   \brief Conversion of 32bit two's compliment to 28 bit unsigned value
 *	  
 *   Conversion of 32bit two's compliment to 28 bit unsigned value
 *
 *   \zp32Value         32ZP input value
 *   \actualValuePtr 	Pointer to converted value	
 *
 *   \return 
 *
 ******************************************************************************
 */
void CovertValueFrom32ZPSTo28Unsigned(lu_uint32_t zp32Value, lu_uint32_t *actualValuePtr)
{
	/*  24 bit value transmitted as a 32bit value
        with eight MSBs padded with 0s  */
	
	zp32Value &= 0xFFFFFFF;

	if(zp32Value & ADE78XX_28ZP_SIGNUM)
	{
		/* Signed value. Pad the 8 MSBs with 1s */
		zp32Value  = ~zp32Value;
		zp32Value &= 0xFFFFFF;
		if(zp32Value != 0)
		{
			zp32Value  = zp32Value - 1;
		}
	}
	
	*actualValuePtr = zp32Value;
}

/*
 *********************** End of file ******************************************
*/
