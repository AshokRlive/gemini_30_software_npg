/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: NvramFactoryInfoWrite.c 3512 2013-07-08 11:35:05Z saravanan_v $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 3512 $: (Revision of last commit)
 *               $Author: saravanan_v $: (Author of last commit)
 *       \date   $Date: 2013-07-08 12:35:05 +0100 (Mon, 08 Jul 2013) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   08/07/12     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "NvramBatteryOptionsWrite.h"
#include "NVRAMDef.h"

#include "NVRAMDefOptPSM.h"
#include "NVRAMDefOptBat.h"
#include "KvaserCan.h"
#include "CmdParser.h"
#include "Main.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"

#include "NvramXMLParser.h"
#include "NvramReadWriteVerify.h"

#include "crc32.h"
#include "roxml.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
batteryOptionsBlkStr batteryOptionsInfo;

static strutureMapStr batteryOptionsElementPtrTable[] = 
{																
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryChemistry,	          sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryChemistry)	            },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryCellNominalVolts,	      sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellNominalVolts)       },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryCellFullChargeVolts,	  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellFullChargeVolts)	},
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryCellMaxCapacity,	      sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellMaxCapacity)	    },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryCellChargingCurrent,	  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellChargingCurrent)	},
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryCellChargingVolts,	  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellChargingVolts)	    },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryCellLeakage,		      sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellLeakage)	        },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryCellFloatCharge,		  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellFloatCharge)	    },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryCellChargingMethod,	  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellChargingMethod)	    },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryPackNoOfCols,		      sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackNoOfCols)	        },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryPackNoOfRows,		      sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackNoOfRows)	        },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryPackMaxChargeTemp,	  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackMaxChargeTemp)	    },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryPackMaxChargeTime,	  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackMaxChargeTime)	    },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryPackTimeBetweenCharge,  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackTimeBetweenCharge)  },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryPackOverideTime,		  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackOverideTime)	    },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryPackChargeTrigger,	  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackChargeTrigger)	    },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryPackThresholdVolts,	  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackThresholdVolts)	    },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryPackLowLevel,		      sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackLowLevel)	        },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryTestTimeMinutes,		  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestTimeMinutes)	    },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryTestCurrentMinimum,	  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestCurrentMinimum)     },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryTestSuspendTimeMinutes, sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestSuspendTimeMinutes) },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryTestMaximumDurationTimeMinutes, sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestMaximumDurationTimeMinutes) },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryTestDischargeCapacity, sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestDischargeCapacity) },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryTestThresholdVolts, sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestThresholdVolts) },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryTestLoadType, sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestLoadType) },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryPackShutdownLevelVolts, sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackShutdownLevelVolts) },
	{(void*)&batteryOptionsInfo.batteryOptions.BatteryPackDeepDischargeVolts, sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackDeepDischargeVolts) }

};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR NvramWriteBatteryOptionsPrepare(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR retError = SB_ERROR_NONE;
	lu_uint8_t* batteryOptionsNVRamBufferPtr, i;

	/* Filling factory info bufer with 0xFF */
	memset(&batteryOptionsInfo, 0xFF, sizeof(batteryOptionsBlkStr));

	if(argsFromCommand->nvramAddr == ID_NVRAM_ADDRESS)
	{
		batteryOptionsInfo.nvRamAddr.addr.addr    = BATT_ID_NVRAM_ADDRESS;
		batteryOptionsInfo.nvRamAddr.addr.i2cChan = BATT_ID_NVRAM_I2C_CHANNEL;
		batteryOptionsInfo.nvRamAddr.addr.offset  = NVRAM_BATT_ID_BLK_OPTS_OFFSET;
	}

	if(argsFromCommand->nvramAddr == APP_NVRAM_ADDRESS)
	{
		batteryOptionsInfo.nvRamAddr.addr.addr    = BATT_DATA_NVRAM_ADDRESS;
		batteryOptionsInfo.nvRamAddr.addr.i2cChan = BATT_DATA_NVRAM_I2C_CHANNEL;
		batteryOptionsInfo.nvRamAddr.addr.offset  = NVRAM_BATT_DATA_BLK_OPTS_OFFSET;
	}

	if(strncmp((lu_int8_t*)argsFromCommand->sourceFileName,"erase.xml",9) != 0)
	{		
		retError = FetchAndPackFactoryInfoFromXML( argsFromCommand->sourceFileName,
												   BATTERY_OPTIONS_FIELD_MAX,
												   batteryOptionsMap,
												   batteryOptionsElementPtrTable
												 );
		if(retError != SB_ERROR_PARAM)
		{
			batteryOptionsInfo.nvRamAddr.size = NVRAM_BATT_ID_BLK_OPTS_SIZE;
			batteryOptionsInfo.factoryBlkHeader.dataSize = NVRAM_BATT_ID_BLK_OPTS_SIZE - sizeof(NVRAMBlkHeadStr);
		
			crc32_calc32( (lu_uint8_t*)&batteryOptionsInfo.batteryOptions, 
						  batteryOptionsInfo.factoryBlkHeader.dataSize, 
						  (lu_uint32_t*)&batteryOptionsInfo.factoryBlkHeader.dataCrc32
					    );
		}
		else
		{
			printf("Error in Opening of XML File...");
			batteryOptionsInfo.nvRamAddr.addr.i2cChan = 0xFF;
			memcpy(&argsFromCommand->msgBuff[0], (lu_uint8_t*)&batteryOptionsInfo.nvRamAddr.addr, sizeof(batteryOptionsInfo.nvRamAddr.addr));
		}
	}
	return retError;
}

lu_bool_t NvramWriteBatteryOptions(ParsingArgsStr *argsFromCommand)
{
	lu_bool_t finish;
	SB_ERROR retValue = SB_ERROR_NONE;
	flagDecoderPtr    = argsFromCommand;
	
	/* Writing and verifying battery options to NVRAM*/
	retValue = NvramWriteAndVerify( (MODULE)argsFromCommand->CANHeader.deviceDst,
								    (MODULE_ID)argsFromCommand->CANHeader.deviceIDDst,
								    batteryOptionsInfo.nvRamAddr,
								    (lu_uint8_t*)&batteryOptionsInfo.factoryBlkHeader
								  );
	Sleep(10);

	finish = LU_FALSE;

	return finish;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************

/*
 *********************** End of file ******************************************
*/