/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1577 2012-07-23 20:18:25Z fryers_j $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1577 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-07-23 21:18:25 +0100 (Mon, 23 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   03/08/12     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleProtocol.h"
#include "CANProtocol.h"

#include "CalElementAdder.h"
#include "KvaserCan.h"
#include "CmdParser.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"
#include "Calibration.h"
#include "LinearInterpolation.h"
#include "ADE78xxFPI.h"
#include "ADE78xxConfig.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
typedef struct calSingleValueDef
{
	lu_uint8_t  lastValue;
	lu_int32_t  ValueOne;
	lu_int32_t  ValueTwo;
} calSingleValueStr;


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
SB_ERROR GetValueFromCSV( FileInfoStr        *firmwareFilePtr, 
						  lu_uint8_t		 calType,	
						  calSingleValueStr  *calSingleValue,
						  ade78xxRegCalStr   *calADE78xxRegValue
						);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
LnInterpTable1DU16S16Str  *calType1du16s16Ptr;
LnInterpTable1DU16U16Str  *calType1du16u16Ptr;
LnInterpTable1DU16U32Str  *calType1du16u32Ptr;
LnInterpTable1DU32U32Str  *calType1du32u32Ptr;
ade78xxRegCalStr          *calTypeAde78xxRegPtr; 

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR CalElementAdderPrepare(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR retValue = SB_ERROR_NONE;
	//lu_uint16_t count = 4;
	calSingleValueStr calSingleValue;
	ade78xxRegCalStr  calSingleADE78xxReg;

	calElementFile.filePtr = fopen(argsFromCommand->CalElementFileName,"rb");
	if(calElementFile.filePtr == NULL)
	{
		printf(" Error in opening file ");
		return SB_ERROR_INITIALIZED;
	}

	argsFromCommand->msgBuff[0] = argsFromCommand->calID; 
	argsFromCommand->msgBuff[1] = argsFromCommand->calType;

	argsFromCommand->calLength  = 4;

	argsFromCommand->msgBuff[2] = 0;
	argsFromCommand->msgBuff[3] = 0;
	
	while (!feof(calElementFile.filePtr) && retValue == SB_ERROR_NONE)
	{
		retValue = GetValueFromCSV(&calElementFile,
								   argsFromCommand->calType,
								   &calSingleValue,
								   &calSingleADE78xxReg
								  );

		if (retValue == SB_ERROR_NONE)
		{
			switch (argsFromCommand->calType)
			{
			case CAL_TYPE_1DU16S16:
			{
				calType1du16s16Ptr = (LnInterpTable1DU16S16Str*)malloc(sizeof(LnInterpTable1DU16S16Str));
				calType1du16s16Ptr->distributionX = calSingleValue.ValueOne;
				calType1du16s16Ptr->interpolateToX = calSingleValue.ValueTwo;

				memcpy(&argsFromCommand->msgBuff[argsFromCommand->calLength], calType1du16s16Ptr, sizeof(LnInterpTable1DU16S16Str));
				//count = count + sizeof(LnInterpTable1DU16S16Str);
				argsFromCommand->calLength = argsFromCommand->calLength + sizeof(LnInterpTable1DU16S16Str);

				free(calType1du16s16Ptr);
			}
			break;

			case CAL_TYPE_1DU16U16:
			{
				calType1du16u16Ptr = (LnInterpTable1DU16U16Str*)malloc(sizeof(LnInterpTable1DU16U16Str));
				calType1du16u16Ptr->distributionX = calSingleValue.ValueOne;
				calType1du16u16Ptr->interpolateToX = calSingleValue.ValueTwo;

				memcpy(&argsFromCommand->msgBuff[argsFromCommand->calLength], calType1du16u16Ptr, sizeof(LnInterpTable1DU16U16Str));
				//count = count + sizeof(LnInterpTable1DU16U16Str);
				argsFromCommand->calLength = argsFromCommand->calLength + sizeof(LnInterpTable1DU16U16Str);

				free(calType1du16u16Ptr);
			}
			break;

			case CAL_TYPE_1DU16U32:
			{
				calType1du16u32Ptr = (LnInterpTable1DU16U32Str*)malloc(sizeof(LnInterpTable1DU16U32Str));
				calType1du16u32Ptr->distributionX = calSingleValue.ValueOne;
				calType1du16u32Ptr->interpolateToX = calSingleValue.ValueTwo;

				memcpy(&argsFromCommand->msgBuff[argsFromCommand->calLength], calType1du16u32Ptr, sizeof(LnInterpTable1DU16U32Str));
				//count = count + sizeof(LnInterpTable1DU16U32Str);
				argsFromCommand->calLength = argsFromCommand->calLength + sizeof(LnInterpTable1DU16U32Str);

				free(calType1du16u32Ptr);
			}
			break;

			case CAL_TYPE_1DU32U32:
			{
				calType1du32u32Ptr = (LnInterpTable1DU32U32Str*)malloc(sizeof(LnInterpTable1DU32U32Str));
				calType1du32u32Ptr->distributionX = calSingleValue.ValueOne;
				calType1du32u32Ptr->interpolateToX = calSingleValue.ValueTwo;

				memcpy(&argsFromCommand->msgBuff[argsFromCommand->calLength], calType1du32u32Ptr, sizeof(LnInterpTable1DU32U32Str));
				//count = count + sizeof(LnInterpTable1DU32U32Str);
				argsFromCommand->calLength = argsFromCommand->calLength + sizeof(LnInterpTable1DU32U32Str);

				free(calType1du32u32Ptr);
			}
			break;

			case CAL_TYPE_1DU16U16_CT_TEMP_COMP:
			case CAL_TYPE_1DU16U16_CT_TEMP_COMP_SUM:
			case CAL_TYPE_1DU16U16_DSM_E_CT_COMP:
			{
				calType1du16u16Ptr = (LnInterpTable1DU16U16Str*)malloc(sizeof(LnInterpTable1DU16U16Str));
				calType1du16u16Ptr->distributionX = calSingleValue.ValueOne;
				calType1du16u16Ptr->interpolateToX = calSingleValue.ValueTwo;

				memcpy(&argsFromCommand->msgBuff[argsFromCommand->calLength], calType1du16u16Ptr, sizeof(LnInterpTable1DU16U16Str));
				//count = count + sizeof(LnInterpTable1DU16U16Str);
				argsFromCommand->calLength = argsFromCommand->calLength + sizeof(LnInterpTable1DU16U16Str);

				free(calType1du16u16Ptr);
			}
			break;

			case CAL_TYPE_ADE78XX_REG:
			{
				calTypeAde78xxRegPtr = (ade78xxRegCalStr*)malloc(sizeof(ade78xxRegCalStr));
				calTypeAde78xxRegPtr->reg = calSingleADE78xxReg.reg;
				calTypeAde78xxRegPtr->size = calSingleADE78xxReg.size;
				calTypeAde78xxRegPtr->calValue = calSingleADE78xxReg.calValue;


				memcpy(&argsFromCommand->msgBuff[argsFromCommand->calLength], calTypeAde78xxRegPtr, sizeof(ade78xxRegCalStr));
				//count = count + sizeof(ade78xxRegCalStr);
				argsFromCommand->calLength = argsFromCommand->calLength + sizeof(ade78xxRegCalStr);

				free(calTypeAde78xxRegPtr);
			}
			break;

			default:
				printf("Error in CAL type");
				retValue = SB_ERROR_PARAM;
				break;
			}
		}
	}
	
	/*
		count = count - sizeof(CalDataHeaderStr);
		argsFromCommand->msgBuff[2] = count & 0xFF;
		argsFromCommand->msgBuff[3] = count >> 8;
		argsFromCommand->msgBuffLen = count + 4;
	*/

	argsFromCommand->calLength  = argsFromCommand->calLength - sizeof(CalDataHeaderStr);
	argsFromCommand->msgBuff[2] = argsFromCommand->calLength & 0xFF;
	argsFromCommand->msgBuff[3] = argsFromCommand->calLength >> 8;
	argsFromCommand->msgBuffLen = argsFromCommand->calLength + 4;

	return retValue;
}

lu_bool_t CalElementAdderSend(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR ret;
	lu_bool_t finish;
	
	flagDecoderPtr = argsFromCommand;

	ret = CANCSendFromMCM( argsFromCommand->CANHeader.messageType,
						   argsFromCommand->CANHeader.messageID,
						   argsFromCommand->CANHeader.deviceDst,
			               argsFromCommand->CANHeader.deviceIDDst,
			               argsFromCommand->msgBuffLen,
			               argsFromCommand->msgBuff
			             );

	Sleep(50);

	finish = LU_FALSE;

	return finish; 
}

SB_ERROR  LucyCANSendCalElementAdderProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError;
	CalTstAddElementRspStr *addCalElementResponse;

	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_CAL_ADD_ELEMENT)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_CALTST:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_CALTST_ADD_ELEMENT_R: 
						{
							addCalElementResponse = (CalTstAddElementRspStr*)msgPtr->msgBufPtr;

							if(addCalElementResponse->calID  == flagDecoderPtr->calID &&
							   addCalElementResponse->status == LU_TRUE	 
							  )
							{
								printf("\n\n --- Adding Calibratin Element ID: %d - SUCCESS --- ", flagDecoderPtr->calID );
								retError = SB_ERROR_NONE;
							}
							else
							{
								printf("\n\n --- Adding Calibratin Element ID: %d - FAIL --- ", flagDecoderPtr->calID);
								retError = SB_ERROR_PARAM;
							}
						}
						break;
					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;
			default:
				printf("\n Error Adding Element");
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}

	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
/*!
 ******************************************************************************
 *   \brief 
 *
 *   \param parameterName Description 
 *
 *   \return 
 *
 ******************************************************************************
 */

SB_ERROR GetValueFromCSV( FileInfoStr        *firmwareFilePtr, 
						  lu_uint8_t		 calType,	
						  calSingleValueStr  *calSingleValue,
						  ade78xxRegCalStr   *calADE78xxRegValue
						)
{
	SB_ERROR retError;
	int ret;

	retError = SB_ERROR_NONE;

	if(calType == CAL_TYPE_ADE78XX_REG)
	{
		ret = fscanf(calElementFile.filePtr,"%d,%d,%d",&calADE78xxRegValue->reg, &calADE78xxRegValue->size, &calADE78xxRegValue->calValue);
	}
	else
	{
		ret = fscanf(calElementFile.filePtr,"%d,%d",&calSingleValue->ValueOne, &calSingleValue->ValueTwo );
	}

	switch (ret)
	{
		case 2:
			printf("\n%d %d", calSingleValue->ValueOne, calSingleValue->ValueTwo);
			retError = SB_ERROR_NONE;
			break;

		case 3:
			printf("\n%d %d %d", calADE78xxRegValue->reg, calADE78xxRegValue->size, calADE78xxRegValue->calValue);
			retError = SB_ERROR_NONE;
			break;
	
		case EOF:
			retError = SB_ERROR_PARAM;
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
	}

	return retError;
}	




/*
 *********************** End of file ******************************************
 */
