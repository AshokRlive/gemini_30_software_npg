/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *   14/07/15      saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "Calibration.h"
#include "CalibrationBlock.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define CAL_ID_NONE -1

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
SB_ERROR CalBlockDataCheck(lu_int8_t calID, CalDataHeaderStr *calElementPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static calDataParamStr calDataParam = {0L, 0L, 0, 0, -1};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR CalibrationBlockInitCalDataPtr(lu_uint8_t *calDataPtr, lu_uint16_t size, lu_uint16_t maxCalId)
{
	lu_uint16_t calID;
	CalDataHeaderStr *calElementPtr;
	SB_ERROR retValue;

	retValue = SB_ERROR_NONE;

	/* Initialisation of calibration data parameters */
	calDataParam.calDataStartPtr  = calDataPtr;
	calDataParam.calDataNextPtr   = calDataPtr;
	calDataParam.calDataMaxSize   = size;
	calDataParam.lastCalId        = CAL_ID_NONE;

    /* Check for valid Parameters */
    if( (calDataPtr != NULL) && (size > 0) )
    {
    	/* To traverse by calibration ID and storing the location of Next data to be written on NV RAM or Flash */
		for(calID = 0; calID < maxCalId; calID++)
		{
			/* Set current Calibration element for next available */
			calElementPtr = (CalDataHeaderStr *)calDataParam.calDataNextPtr;

			if (CalBlockDataCheck(calID, calElementPtr) == SB_ERROR_NONE)
			{
				/* To check sequence order of calibration elements */
				if (calElementPtr->calId == calID)
				{
					 /* Updating calibration data parameters */
					 calDataParam.calDataNextPtr   = (lu_uint8_t *)calElementPtr;
					 calDataParam.calDataNextPtr  += sizeof(CalDataHeaderStr);
					 calDataParam.calDataNextPtr  += calElementPtr->length;
					 calDataParam.lastCalId        = calElementPtr->calId;
				}
				else
				{
					 /* If the calibration IDs are not in sequential order, break the search */
					 retValue = SB_ERROR_CAL_SEQUENCE;
					 break;
				}
		   }
		   else
		   {
				/* If the calibration table is empty */
				retValue = SB_ERROR_INITIALIZED;
				break;
		   }
		}
	}
    else
    {
    	retValue = SB_ERROR_PARAM;
    }

    return retValue;
}

SB_ERROR CalibrationBlockAddElement( lu_uint8_t    calID,
									 lu_uint8_t    calType,
									 lu_uint16_t   calDatalength,
									 lu_uint8_t    *calDataPtr
								   )
{
	SB_ERROR retValue = SB_ERROR_NONE;
	CalDataHeaderStr  *calElementPtr;
	lu_uint16_t       totalCalSize;

	/* To check memory for calibration initialised or not */
	if (calDataParam.calDataStartPtr != NULL)
	{
		/* Checking the enough space is available to add this calibration element */
		totalCalSize  = calDatalength;
		totalCalSize += sizeof(CalDataHeaderStr);

		if ((calDataParam.calDataMaxSize - calDataParam.calDataSize) >= totalCalSize)
		{
			if(calDatalength > 0)
			{
				/* Checking for calibration ID is in the sequential order */
				if(calID == calDataParam.lastCalId + 1 )
				{
					/* Copying new element header to memory location and incrementing by number bytes written*/
					calElementPtr = (CalDataHeaderStr *)calDataParam.calDataNextPtr;

					calElementPtr->calId  = calID;
					calElementPtr->type   = calType;
					calElementPtr->length = calDatalength;

					calDataParam.calDataNextPtr += sizeof(CalDataHeaderStr);

					/* Copying new element's data in to memory location */
					memcpy(calDataParam.calDataNextPtr, (void *)calDataPtr, calDatalength);

					/* Updating the calibration data parameters after adding new element */
					calDataParam.lastCalId       = calID;
					calDataParam.calDataSize    += (sizeof(CalDataHeaderStr) + calDatalength);
					calDataParam.calDataNextPtr += calDatalength;
				}
				else
				{
					retValue = SB_ERROR_CAL_SEQUENCE;
				}
			}
			else
			{
				retValue = SB_ERROR_CAL_LENGTH;
			}
		}
		else
		{
			retValue = SB_ERROR_CAL_EXCEED;
		}
	}
	else
	{
		retValue = SB_ERROR_PARAM;
	}
	return retValue;
}

SB_ERROR CalibrationBlockEraseAllElements(void)
{
	lu_uint32_t calId;
	SB_ERROR retValue = SB_ERROR_INITIALIZED;

	if (calDataParam.calDataStartPtr != NULL)
	{
		/* To fill calibration table with zeros */
		memset((void *)calDataParam.calDataStartPtr , 0, calDataParam.calDataMaxSize);

		/* To keep start and next address equal to keep calibration table empty */
		calDataParam.calDataNextPtr = calDataParam.calDataStartPtr;
		calDataParam.calDataSize    = 0;

		/* Initialise with -1, because the calibration ID starts from 0 */
		calDataParam.lastCalId  = CAL_ID_NONE;

		retValue = SB_ERROR_NONE;
	}

	return retValue;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
/*!
 ******************************************************************************
 *   \brief Getting the address of particular calibration value by using it's ID
 *
 *   Detailed description
 *
 *   \param calId  - calibration ID
 *   \param calElementPtr - Pointer to a location where calibration data is located
 *
 *   \return Pointer to a calibration data
 *
 ******************************************************************************
 */
SB_ERROR CalBlockDataCheck(lu_int8_t calID, CalDataHeaderStr *calElementPtr)
{
	lu_uint16_t totalCalSize;
	SB_ERROR retError;

	retError = SB_ERROR_CAL_NODATA;

	/* Checking for given calibration ID's presence in the memory */
	if (calID == calElementPtr->calId)
	{
		if (calElementPtr->length > 0)
		{
			totalCalSize  = calElementPtr->length;
			totalCalSize += sizeof(CalDataHeaderStr);

			/* Check calibration data is within memory bounds */
			if ((calDataParam.calDataMaxSize - calDataParam.calDataSize) >= totalCalSize)
			{
				retError = SB_ERROR_NONE;
			}
		}
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */