/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1577 2012-07-23 20:18:25Z fryers_j $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1577 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-07-23 21:18:25 +0100 (Mon, 23 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/12     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleProtocol.h"
#include "CANProtocol.h"

#include "FirmwareVerifier.h"
#include "FirmwareDownloader.h"
#include "LucyCANDecoder.h"

#include "KvaserCan.h"
#include "CmdParser.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static lu_uint32_t msgBlockCount;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR FirmwareVerifierFilePrepare(ParsingArgsStr *argsFromCommand)
{
 	SB_ERROR retError = SB_ERROR_NONE;
	lu_uint32_t bytesToSend; 
	lu_uint32_t msgByteCount = 0, lastBlockSize = 0;
		
	flagDecoderPtr = argsFromCommand;

	srcFirmwareFile.filePtr = fopen(argsFromCommand->sourceFileName, "rb");
	if(srcFirmwareFile.filePtr == NULL)
	{
		printf(" Error in opening file ");
		retError = SB_ERROR_PARAM;
	} 

	dstFirmwareFile.filePtr = fopen(argsFromCommand->destinationFileName, "wb");
	if(dstFirmwareFile.filePtr == NULL)
	{
		printf(" Error in opening file ");
		retError = SB_ERROR_PARAM;
	} 

	/* Seeking to end to find size of the file */
	fseek (srcFirmwareFile.filePtr , 1 , SEEK_END);
	srcFirmwareFile.fileSize = ftell (srcFirmwareFile.filePtr);
	bytesToSend = srcFirmwareFile.fileSize;
	rewind (srcFirmwareFile.filePtr);
	printf(" File Size = %d",srcFirmwareFile.fileSize);

	/* Calculatin of number of 256bytes blocks and remaining bytes */
	srcFirmwareFile.lastBlockSize  = srcFirmwareFile.fileSize % BLOCK_256;
	srcFirmwareFile.numberOfBlocks = srcFirmwareFile.fileSize / BLOCK_256;
	if(srcFirmwareFile.lastBlockSize > 0)
	{
		srcFirmwareFile.numberOfBlocks = srcFirmwareFile.numberOfBlocks + 1;  
	}
	printf("\n Last Block Size = %d\n\n",  srcFirmwareFile.lastBlockSize);
	printf("Reading Firmware Block : ");

	return retError;
}

lu_bool_t FirmwareVerifierGetBlock( FileInfoStr *srcFirmwareFile, 
	                                FileInfoStr *dstFirmwareFile, 
									ParsingArgsStr *argsFromCommand
								  )
{
	lu_bool_t finish = LU_FALSE;
	if(msgBlockCount != srcFirmwareFile->numberOfBlocks)
	{
		Sleep(1);
		CANCSendFromMCM( MODULE_MSG_TYPE_BL,
						 MODULE_MSG_ID_BL_READ_FIRMWARE_C,
						 argsFromCommand->CANHeader.deviceDst,
						 argsFromCommand->CANHeader.deviceIDDst,
						 sizeof(lu_uint16_t),
						 (lu_uint8_t*)&msgBlockCount
					    ); 

		 msgBlockCount = msgBlockCount + 1;
		 finish = LU_TRUE;
	}	

	return finish;
}

SB_ERROR LucyCANSendFirmwareVerifierProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_NONE;
	
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_FIRMWARE_VERIFY)
	{
	switch (msgPtr->messageType)
	{
		case MODULE_MSG_TYPE_BL:
			switch(msgPtr->messageID)
			{
				case MODULE_MSG_ID_BL_READ_FIRMWARE_R: 
					if(msgPtr->msgLen == 256)
					{
						printf("%03X\b\b\b", msgBlockCount);
						retError = FirmwareVerifierPutBlock(&srcFirmwareFile, &dstFirmwareFile, msgPtr, &argsFromCommand);
					}
					break;
				default:
					retError = SB_ERROR_CANC_NOT_HANDLED;
					break;
			}
			break;
		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
	}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Downloading firmware to the slave board
 *
 *   Downloading the specified firmware file in to slave board
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR FirmwareVerifierPutBlock(FileInfoStr *srcFirmwareFile, 
	                              FileInfoStr *dstFirmwareFile,
								  CANFramingMsgStr *msgPtr,
								  ParsingArgsStr *argsFromCommand
								 )
{
	lu_bool_t finish = LU_FALSE;

	if(msgBlockCount == srcFirmwareFile->numberOfBlocks)
	{
		fwrite(msgPtr->msgBufPtr, sizeof(lu_uint8_t), srcFirmwareFile->lastBlockSize - 1, dstFirmwareFile->filePtr);
		finish = LU_FALSE;
		fclose(srcFirmwareFile->filePtr);
		fclose(dstFirmwareFile->filePtr);
		FirmwareCompare(argsFromCommand->sourceFileName, argsFromCommand->destinationFileName);
	}
	else
	{
		fwrite(msgPtr->msgBufPtr, sizeof(lu_uint8_t), 256, dstFirmwareFile->filePtr);
		finish = LU_TRUE;
	}
	
	return finish;
}

/*!
 ******************************************************************************
 *   \brief Downloading firmware to the slave board
 *
 *   Downloading the specified firmware file in to slave board
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

SB_ERROR FirmwareCompare(lu_int8_t *fileOriginal, lu_int8_t *fileFromBoard)
{
	FILE *fileOriginalfptr, *fileFromBoardfptr; 
	lu_int32_t sourceChar, destinationChar, errorByte = 0;

	fileOriginalfptr = fopen(fileOriginal, "rb");
	if(fileOriginalfptr == NULL)
	{
		printf(" Error in opening file ");
		fclose(fileOriginalfptr);
	} 

	fileFromBoardfptr = fopen(fileFromBoard, "rb");
	if(fileFromBoardfptr == NULL)
	{
		printf(" Error in opening file ");
		fclose(fileFromBoardfptr);
	} 
	
	sourceChar       = fgetc(fileOriginalfptr);
	destinationChar  = fgetc(fileFromBoardfptr);

    while((sourceChar != EOF)			  && 
		  (destinationChar != EOF)		  && 
		  (sourceChar == destinationChar)
		 )
	{
		sourceChar       = fgetc(fileOriginalfptr);
		destinationChar  = fgetc(fileFromBoardfptr);
		errorByte++;
	}
	
	if (sourceChar == destinationChar )   
            printf(" Files are identical \n");   
    else
        printf("\n Files differ on %0X\n", errorByte);   

	fclose(fileOriginalfptr);
	fclose(fileFromBoardfptr);
}

/*
 *********************** End of file ******************************************
 */