/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09/08/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "CANProtocolFraming.h"
#include "CmdParser.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

extern SB_ERROR I2CExpIOTestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time);

extern SB_ERROR I2CExpIOReadDDRDll( MODULE module, 
						MODULE_ID moduleId, 
						lu_uint8_t i2cChan,
						lu_uint8_t addr,
						lu_uint16_t *value_r
					  );

extern SB_ERROR I2CExpIOWriteDDRDll( MODULE module, 
							  MODULE_ID moduleId, 
							  lu_uint8_t i2cChan,
							  lu_uint8_t addr,
							  lu_uint16_t value
							);

extern SB_ERROR I2CExpIOReadPinDll( MODULE module, 
							 MODULE_ID moduleId, 
							 lu_uint8_t i2cChan,
							 lu_uint8_t addr,
							 lu_uint8_t pin,
							 lu_uint8_t *value_r
				           );

extern SB_ERROR I2CExpIOWritePinDll( MODULE module, 
							  MODULE_ID moduleId, 
							  lu_uint8_t i2cChan,
							  lu_uint8_t addr,
							  lu_uint8_t pin,
							  lu_uint8_t value
							);
/*
 *********************** End of file ******************************************
 */