/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: NvramFactoryInfoRead.c 3853 2013-09-12 09:19:21Z saravanan_v $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 3853 $: (Revision of last commit)
 *               $Author: saravanan_v $: (Author of last commit)
 *       \date   $Date: 2013-09-12 10:19:21 +0100 (Thu, 12 Sep 2013) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30/08/12     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "NVRAMDef.h"

#include "KvaserCan.h"
#include "CmdParser.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"

#include "NvramFactoryInfoRead.h"
#include "NvramXMLParser.h"

#include "crc32.h"
#include "roxml.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
*/

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
/*SB_ERROR BundleNVRAMDataToXMLFile( lu_uint8_t *readBuffer,
								   lu_int8_t  *fileName,
								   lu_uint8_t numberOfItemTags,
								   strutureMapStr *elementMapStrPtr,
								   xmlInfoStr *xmlTagMapStrPtr
								 );
								 */
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
readFactoryInfoBlkStr readFactoryInfo;

static strutureMapStr factoryInfoElementPtrTable[] = 
{																	//sizeof(((struct A*)0)->arr);
	{(void*)&readFactoryInfo.factoryInfoData.InfoBlockVersionMajor,		sizeof(((NVRAMInfoStr*)NULL)->InfoBlockVersionMajor)	},
	{(void*)&readFactoryInfo.factoryInfoData.InfoBlockVersionMinor,		sizeof(((NVRAMInfoStr*)NULL)->InfoBlockVersionMinor)	},
	{(void*)&readFactoryInfo.factoryInfoData.moduleType,				sizeof(((NVRAMInfoStr*)NULL)->moduleType)				},
	{(void*)&readFactoryInfo.factoryInfoData.moduleFeatureMajor,		sizeof(((NVRAMInfoStr*)NULL)->moduleFeatureMajor)		},
	{(void*)&readFactoryInfo.factoryInfoData.moduleFeatureMinor,		sizeof(((NVRAMInfoStr*)NULL)->moduleFeatureMinor)		},
	{(void*)&readFactoryInfo.factoryInfoData.batchNumber,				sizeof(((NVRAMInfoStr*)NULL)->batchNumber)				},
	{(void*)&readFactoryInfo.factoryInfoData.supplierId,				sizeof(((NVRAMInfoStr*)NULL)->supplierId)				},
	{(void*)&readFactoryInfo.factoryInfoData.assemblyNo,				sizeof(((NVRAMInfoStr*)NULL)->assemblyNo)				},
	{(void*)&readFactoryInfo.factoryInfoData.assemblyRev,				sizeof(((NVRAMInfoStr*)NULL)->assemblyRev)				},
	{(void*)&readFactoryInfo.factoryInfoData.serialNumber,				sizeof(((NVRAMInfoStr*)NULL)->serialNumber)			    },
	{(void*)&readFactoryInfo.factoryInfoData.subAssemblyNo,				sizeof(((NVRAMInfoStr*)NULL)->subAssemblyNo)			},
	{(void*)&readFactoryInfo.factoryInfoData.subAssemblyRev,			sizeof(((NVRAMInfoStr*)NULL)->subAssemblyRev)			},
	{(void*)&readFactoryInfo.factoryInfoData.subAssemblySerialNumber,	sizeof(((NVRAMInfoStr*)NULL)->subAssemblySerialNumber)  },
	{(void*)&readFactoryInfo.factoryInfoData.subAssemblyBatchNumber,	sizeof(((NVRAMInfoStr*)NULL)->subAssemblyBatchNumber)	},
	{(void*)&readFactoryInfo.factoryInfoData.buildDay,					sizeof(((NVRAMInfoStr*)NULL)->buildDay)				    },
	{(void*)&readFactoryInfo.factoryInfoData.buildMonth,				sizeof(((NVRAMInfoStr*)NULL)->buildMonth)				},
	{(void*)&readFactoryInfo.factoryInfoData.buildYear,					sizeof(((NVRAMInfoStr*)NULL)->buildYear)				}
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR NvramReadFactoryInfoPrepare(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR retError = SB_ERROR_NONE;

	if(argsFromCommand->nvramAddr == ID_NVRAM_ADDRESS && argsFromCommand->nvramI2CChannel == ID_NVRAM_I2C_CHANNEL)
	{
		readFactoryInfo.nvRamAddr.addr.addr    = ID_NVRAM_ADDRESS;
		readFactoryInfo.nvRamAddr.addr.i2cChan = ID_NVRAM_I2C_CHANNEL;
		readFactoryInfo.nvRamAddr.addr.offset  = NVRAM_ID_BLK_INFO_OFFSET;
	}
	
	if(argsFromCommand->nvramAddr == APP_NVRAM_ADDRESS && argsFromCommand->nvramI2CChannel == APP_NVRAM_I2C_CHANNEL)
	{
		readFactoryInfo.nvRamAddr.addr.addr    = APP_NVRAM_ADDRESS;
		readFactoryInfo.nvRamAddr.addr.i2cChan = APP_NVRAM_I2C_CHANNEL;
		readFactoryInfo.nvRamAddr.addr.offset  = NVRAM_APP_BLK_INFO_OFFSET;
	}

	if(argsFromCommand->nvramAddr == BATT_ID_NVRAM_ADDRESS && argsFromCommand->nvramI2CChannel == BATT_ID_NVRAM_I2C_CHANNEL)
	{
		readFactoryInfo.nvRamAddr.addr.addr    = BATT_ID_NVRAM_ADDRESS;
		readFactoryInfo.nvRamAddr.addr.i2cChan = argsFromCommand->nvramI2CChannel;
		readFactoryInfo.nvRamAddr.addr.offset  = NVRAM_BATT_ID_BLK_INFO_OFFSET;
	}

	if(argsFromCommand->nvramAddr == BATT_DATA_NVRAM_ADDRESS && argsFromCommand->nvramI2CChannel == BATT_DATA_NVRAM_I2C_CHANNEL)
	{
		readFactoryInfo.nvRamAddr.addr.addr    = BATT_DATA_NVRAM_ADDRESS;
		readFactoryInfo.nvRamAddr.addr.i2cChan = argsFromCommand->nvramI2CChannel;
		readFactoryInfo.nvRamAddr.addr.offset  = NVRAM_BATT_DATA_BLK_INFO_OFFSET;
	}

	readFactoryInfo.nvRamAddr.size = NVRAM_APP_BLK_INFO_SIZE;

	memcpy((lu_uint8_t*)&argsFromCommand->msgBuff[0], (lu_uint8_t*)&readFactoryInfo, sizeof(TestNVRAMReadBuffStr));

	return retError;
}

lu_bool_t NvramReadFactoryInfo(ParsingArgsStr *argsFromCommand)
{
	lu_bool_t finish;
	SB_ERROR retValue = SB_ERROR_NONE;
	flagDecoderPtr    = argsFromCommand;
	
	/* Sending the firmware block via CAN bus */
	retValue = CANCSendFromMCM( MODULE_MSG_TYPE_BLTST,
								MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_C,
								argsFromCommand->CANHeader.deviceDst,
								argsFromCommand->CANHeader.deviceIDDst,
								sizeof(TestNVRAMReadBuffStr),
								(lu_uint8_t*)&argsFromCommand->msgBuff
							  );
	Sleep(1000);

	finish = LU_FALSE;

	return finish;
}

SB_ERROR LucyCANSendNvramReadFactoryInfoProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_CANC_NOT_HANDLED;
	
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_NVRAM_READ_FACTORY)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_BLTST:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_R: 
						if(msgPtr->msgLen == NVRAM_APP_BLK_INFO_SIZE)
						{
							
							memcpy(&readFactoryInfo.factoryBlkHeader, msgPtr->msgBufPtr, sizeof(readFactoryInfoBlkStr)); 
							retError = BundleNVRAMDataToXMLFile( msgPtr->msgBufPtr,
																 (lu_int8_t*)flagDecoderPtr->destinationFileName,
																 FACTORYINFO_FIELD_MAX,
																 &factoryInfoElementPtrTable[0],
																 &factoryInfoMap[0]	
															   );
						}
						break;
						
					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;

			default:
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
*/