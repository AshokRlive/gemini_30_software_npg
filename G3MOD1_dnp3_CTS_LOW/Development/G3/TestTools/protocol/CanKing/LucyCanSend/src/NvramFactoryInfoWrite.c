/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: NvramFactoryInfoWrite.c 10224 2016-02-04 10:30:31Z saravanan_v $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 10224 $: (Revision of last commit)
 *               $Author: saravanan_v $: (Author of last commit)
 *       \date   $Date: 2016-02-04 10:30:31 +0000 (Thu, 04 Feb 2016) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   03/08/12     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "NVRAMDef.h"

#include "KvaserCan.h"
#include "CmdParser.h"
#include "Main.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"

#include "NvramFactoryInfoWrite.h"
#include "NvramXMLParser.h"
#include "NvramReadWriteVerify.h"

#include "crc32.h"
#include "roxml.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
factoryInfoBlkStr factoryInfo;

static strutureMapStr factoryInfoElementPtrTable[] = 
{																	//sizeof(((struct A*)0)->arr);
	{(void*)&factoryInfo.factoryInfoData.InfoBlockVersionMajor,		sizeof(((NVRAMInfoStr*)NULL)->InfoBlockVersionMajor)	},
	{(void*)&factoryInfo.factoryInfoData.InfoBlockVersionMinor,		sizeof(((NVRAMInfoStr*)NULL)->InfoBlockVersionMinor)	},
	{(void*)&factoryInfo.factoryInfoData.moduleType,				sizeof(((NVRAMInfoStr*)NULL)->moduleType)				},
	{(void*)&factoryInfo.factoryInfoData.moduleFeatureMajor,		sizeof(((NVRAMInfoStr*)NULL)->moduleFeatureMajor)		},
	{(void*)&factoryInfo.factoryInfoData.moduleFeatureMinor,		sizeof(((NVRAMInfoStr*)NULL)->moduleFeatureMinor)		},
	{(void*)&factoryInfo.factoryInfoData.batchNumber,				sizeof(((NVRAMInfoStr*)NULL)->batchNumber)				},
	{(void*)&factoryInfo.factoryInfoData.supplierId,				sizeof(((NVRAMInfoStr*)NULL)->supplierId)				},
	{(void*)&factoryInfo.factoryInfoData.assemblyNo,				sizeof(((NVRAMInfoStr*)NULL)->assemblyNo)				},
	{(void*)&factoryInfo.factoryInfoData.assemblyRev,				sizeof(((NVRAMInfoStr*)NULL)->assemblyRev)				},
	{(void*)&factoryInfo.factoryInfoData.serialNumber,				sizeof(((NVRAMInfoStr*)NULL)->serialNumber)			    },
	{(void*)&factoryInfo.factoryInfoData.subAssemblyNo,				sizeof(((NVRAMInfoStr*)NULL)->subAssemblyNo)			},
	{(void*)&factoryInfo.factoryInfoData.subAssemblyRev,			sizeof(((NVRAMInfoStr*)NULL)->subAssemblyRev)			},
	{(void*)&factoryInfo.factoryInfoData.subAssemblySerialNumber,	sizeof(((NVRAMInfoStr*)NULL)->subAssemblySerialNumber)  },
	{(void*)&factoryInfo.factoryInfoData.subAssemblyBatchNumber,	sizeof(((NVRAMInfoStr*)NULL)->subAssemblyBatchNumber)	},
	{(void*)&factoryInfo.factoryInfoData.buildDay,					sizeof(((NVRAMInfoStr*)NULL)->buildDay)				    },
	{(void*)&factoryInfo.factoryInfoData.buildMonth,				sizeof(((NVRAMInfoStr*)NULL)->buildMonth)				},
	{(void*)&factoryInfo.factoryInfoData.buildYear,					sizeof(((NVRAMInfoStr*)NULL)->buildYear)				}
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR NvramWriteFactoryInfoPrepare(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR retError = SB_ERROR_NONE;
	lu_uint8_t* factoryInfoNVRamBufferPtr, i;

	flagDecoderPtr = argsFromCommand;

	/* Filling factory info bufer with 0xFF */
	memset(&factoryInfo, 0xFF, sizeof(factoryInfoBlkStr));

	if(argsFromCommand->nvramAddr == ID_NVRAM_ADDRESS && argsFromCommand->nvramI2CChannel == ID_NVRAM_I2C_CHANNEL)
	{
		factoryInfo.nvRamAddr.addr.addr    = ID_NVRAM_ADDRESS;
		factoryInfo.nvRamAddr.addr.i2cChan = argsFromCommand->nvramI2CChannel;
		factoryInfo.nvRamAddr.addr.offset  = NVRAM_ID_BLK_INFO_OFFSET;
	}
	
	if(argsFromCommand->nvramAddr == APP_NVRAM_ADDRESS && argsFromCommand->nvramI2CChannel == APP_NVRAM_I2C_CHANNEL)
	{
		factoryInfo.nvRamAddr.addr.addr    = APP_NVRAM_ADDRESS;
		factoryInfo.nvRamAddr.addr.i2cChan = argsFromCommand->nvramI2CChannel;
		factoryInfo.nvRamAddr.addr.offset  = NVRAM_APP_BLK_INFO_OFFSET;
	}

	if(argsFromCommand->nvramAddr == BATT_ID_NVRAM_ADDRESS && argsFromCommand->nvramI2CChannel == BATT_ID_NVRAM_I2C_CHANNEL)
	{
		factoryInfo.nvRamAddr.addr.addr    = BATT_ID_NVRAM_ADDRESS;
		factoryInfo.nvRamAddr.addr.i2cChan = argsFromCommand->nvramI2CChannel;
		factoryInfo.nvRamAddr.addr.offset  = NVRAM_BATT_ID_BLK_INFO_OFFSET;
	}

	if(argsFromCommand->nvramAddr == BATT_DATA_NVRAM_ADDRESS && argsFromCommand->nvramI2CChannel == BATT_DATA_NVRAM_I2C_CHANNEL)
	{
		factoryInfo.nvRamAddr.addr.addr    = BATT_DATA_NVRAM_ADDRESS;
		factoryInfo.nvRamAddr.addr.i2cChan = argsFromCommand->nvramI2CChannel;
		factoryInfo.nvRamAddr.addr.offset  = NVRAM_BATT_DATA_BLK_INFO_OFFSET;
	}

	if(strncmp((lu_int8_t*)argsFromCommand->sourceFileName,"erase.xml",9) != 0)
	{		
		retError = FetchAndPackFactoryInfoFromXML( argsFromCommand->sourceFileName,
												   FACTORYINFO_FIELD_MAX,
												   factoryInfoMap,
												   factoryInfoElementPtrTable
												 );
		if(retError != SB_ERROR_PARAM)
		{
			factoryInfo.nvRamAddr.size = NVRAM_ID_BLK_INFO_SIZE;
			factoryInfo.factoryBlkHeader.dataSize = NVRAM_ID_BLK_INFO_SIZE - sizeof(NVRAMBlkHeadStr);
		
			//factoryInfoNVRamBufferPtr += sizeof(factoryInfo.nvRamAddr.addr);

			/* Calculating CRC block for factory info block (expect NVRAM block header) */
			crc32_calc32( (lu_uint8_t*)&factoryInfo.factoryInfoData, 
				          factoryInfo.factoryBlkHeader.dataSize, 
						  (lu_uint32_t*)&factoryInfo.factoryBlkHeader.dataCrc32
						);
		}
		else
		{
			printf("Error in Opening of XML File...");
			factoryInfo.nvRamAddr.addr.i2cChan = 0xFF;
			memcpy(&argsFromCommand->msgBuff[0], (lu_uint8_t*)&factoryInfo.nvRamAddr.addr, sizeof(factoryInfo.nvRamAddr.addr));
		}
	}
		
	return retError;
}

lu_bool_t NvramWriteFactoryInfo(ParsingArgsStr *argsFromCommand)
{
	lu_bool_t finish;
	SB_ERROR retValue = SB_ERROR_NONE;
	flagDecoderPtr    = argsFromCommand;
	
	/* Writing and verifying factory info to NVRAM*/
	retValue = NvramWriteAndVerify( (MODULE)argsFromCommand->CANHeader.deviceDst,
								    (MODULE_ID)argsFromCommand->CANHeader.deviceIDDst,
								    factoryInfo.nvRamAddr,
								    (lu_uint8_t*)&factoryInfo.factoryBlkHeader
								  );
	Sleep(10);

	finish = LU_FALSE;

	return finish;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
*/
