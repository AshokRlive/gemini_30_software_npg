/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1577 2012-07-23 20:18:25Z fryers_j $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1577 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-07-23 21:18:25 +0100 (Mon, 23 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/07/15     saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleProtocol.h"
#include "CANProtocol.h"

#include "CalBlockWriter.h"
#include "KvaserCan.h"
#include "CmdParser.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"
#include "Calibration.h"
#include "LinearInterpolation.h"
#include "ADE78xxFPI.h"
#include "ADE78xxConfig.h"
#include "CalElementAdder.h"
#include "mainDll.h"
#include "NvramReadWriteVerify.h"

#include "CalibrationBlock.h"
#include "BoardCalibration.h"
#include "NVRAMDefID.h"

#include "crc32.h"
#include "roxml.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
typedef struct calConfigDef
{
	lu_uint8_t  calID;
	lu_uint8_t  calType;
	lu_int8_t   *calFileName;
}calConfigStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
SB_ERROR ReadXMLCalConfigFile(node_t *calElementConfigXMLTag, calConfigStr *calConfigParam);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
CalBlkStr calBlock;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR CalBlockWritePrepare( MODULE      module, 
							   MODULE_ID   moduleId,
							   lu_int8_t   *calFilesPath,
							   lu_int8_t   *calConfigFile,
							   lu_uint8_t  NvramAddress,
							   lu_uint8_t  NvramI2CChan,
							   lu_uint8_t  blkOffset
							 )
{
	lu_uint8_t  calElementIndex;
	SB_ERROR retError = SB_ERROR_NONE;
	calConfigStr calConfigParam;
	lu_int8_t calFileName[800];
	lu_uint8_t fileNameLen;
	node_t *root, *calConfigFileNode, *calElementConfigNode;	
	lu_uint8_t* calBlockNVRamBufferPtr;
	lu_uint16_t i;

	/* Opening an XML file using roxml  */
	root = roxml_load_doc((lu_int8_t*)calConfigFile);
	if(root == NULL)
	{
		retError = SB_ERROR_PARAM;
	}
	else
	{
		calConfigFileNode    = roxml_get_chld(root              , "CalConfigFile"    ,  0);
		calElementConfigNode = roxml_get_chld(calConfigFileNode , "CalElementConfig" ,  0);

		/* Assigning calBlkData[] as calibration data buffer and verifying */
		CalibrationBlockInitCalDataPtr(&calBlock.calBlkData[0], NVRAM_ID_BLK_CAL_SIZE, MAX_CAL_ID);

		/* To erase all the calibration data*/
		CalibrationBlockEraseAllElements();

		while(calElementConfigNode != NULL)
		{
			retError = ReadXMLCalConfigFile(calElementConfigNode, &calConfigParam);
	  
			argsFromCommand.calID   = calConfigParam.calID;
			argsFromCommand.calType = calConfigParam.calType;

			strcpy(calFileName, calFilesPath);
			strcat(calFileName, "\\");
			strcat(calFileName, calConfigParam.calFileName);
		
			fileNameLen = strlen(calFileName);

			memset(argsFromCommand.CalElementFileName, 0x00, sizeof(argsFromCommand.CalElementFileName));
			memcpy(argsFromCommand.CalElementFileName, calFileName, fileNameLen);

			CalElementAdderPrepare(&argsFromCommand);
				
			calElementConfigNode = roxml_get_next_sibling(calElementConfigNode);

			CalibrationBlockAddElement( argsFromCommand.calID,
										argsFromCommand.calType,
										argsFromCommand.calLength,
										&argsFromCommand.msgBuff[sizeof(CalDataHeaderStr)]
									  );
		}
		roxml_close(root);

		/* Assign size of the calibration data block excluding header */
		calBlock.calBlkHeader.dataSize = NVRAM_ID_BLK_CAL_SIZE - sizeof(NVRAMBlkHeadStr);

		crc32_calc32( &calBlock.calBlkData[0], 
					  calBlock.calBlkHeader.dataSize,
					  (lu_uint32_t*)&calBlock.calBlkHeader.dataCrc32
					);

		calBlock.nvRamAddr.addr.addr    = NvramAddress;
		calBlock.nvRamAddr.addr.i2cChan = NvramI2CChan;
		calBlock.nvRamAddr.addr.offset  = NVRAM_ID_BLK_CAL_OFFSET;

		/* Clear the buffer first before fill with calibration elements */
		calBlockNVRamBufferPtr = &argsFromCommand.msgBuff[0];
		memset(calBlockNVRamBufferPtr, 0, sizeof(argsFromCommand.msgBuff));

		/* Fill buffer with Test NVram parameters and increment buffer pointer */
		memcpy(calBlockNVRamBufferPtr , (lu_uint8_t*)&calBlock.nvRamAddr.addr, sizeof(calBlock.nvRamAddr.addr));
		calBlockNVRamBufferPtr += sizeof(calBlock.nvRamAddr.addr);

		/* Fill buffer with  NVram block header and increment buffer pointer */
		memcpy(calBlockNVRamBufferPtr , (lu_uint8_t*)&calBlock.calBlkHeader, sizeof(calBlock.calBlkHeader));
		calBlockNVRamBufferPtr += sizeof(calBlock.calBlkHeader);

		/* Copy the calibtaion data buffer into CAN message buffer (Half of the buffer = 320) */
		memcpy(calBlockNVRamBufferPtr , (lu_uint8_t*)&calBlock.calBlkData[0], (NVRAM_ID_BLK_CAL_SIZE / 2) - sizeof(NVRAMBlkHeadStr));

		Sleep(500);

		/* Sending the calibration block via CAN bus */
		calBlock.nvRamAddr.size = (NVRAM_ID_BLK_CAL_SIZE / 2);

		retError= NvramWriteAndVerify( module,
						  			   moduleId,
									   calBlock.nvRamAddr,	
									  (lu_uint8_t*)&calBlock.calBlkHeader
									);
	
		if(retError == SB_ERROR_NONE)
		{
			calBlock.nvRamAddr.addr.offset  += (sizeof(NVRAMBlkHeadStr) + (NVRAM_ID_BLK_CAL_SIZE / 2) - sizeof(NVRAMBlkHeadStr));
	
			/* Clear the buffer first before fill with calibration elements */
			calBlockNVRamBufferPtr = &argsFromCommand.msgBuff[0];
			memset(calBlockNVRamBufferPtr, 0, sizeof(argsFromCommand.msgBuff));

			/* Fill buffer with Test NVram parameters and increment buffer pointer */
			memcpy(calBlockNVRamBufferPtr , (lu_uint8_t*)&calBlock.nvRamAddr.addr, sizeof(calBlock.nvRamAddr.addr));
			calBlockNVRamBufferPtr += sizeof(calBlock.nvRamAddr.addr);

			/* Copy the calibtaion data buffer into CAN message buffer (Half of the buffer = 320) */
			memcpy( calBlockNVRamBufferPtr , 
					(lu_uint8_t*)&calBlock.calBlkData[(NVRAM_ID_BLK_CAL_SIZE / 2)- sizeof(NVRAMBlkHeadStr)], 
					(NVRAM_ID_BLK_CAL_SIZE / 2)
				  );
	
			retError = NvramWriteAndVerify( module,
											moduleId,
											calBlock.nvRamAddr,	
											(lu_uint8_t*)&calBlock.calBlkData[(NVRAM_ID_BLK_CAL_SIZE / 2)- sizeof(NVRAMBlkHeadStr)]
										  );
		}
	}
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief 
 *
 *   \param parameterName Description 
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR ReadXMLCalConfigFile(node_t *calElementConfigXMLTag, calConfigStr *calConfigParam)
{
	SB_ERROR retError = SB_ERROR_NONE;
	int ret, i = 0,len;
	lu_int32_t c = NULL;
	lu_int8_t *calID, *calType, *calFileName;
	node_t *calIDNode, *calTypeNode, *calFileNameNode;	
	
	calIDNode                    = roxml_get_attr(calElementConfigXMLTag, "calID", 0);
	calTypeNode                  = roxml_get_attr(calElementConfigXMLTag, "calType", 0);
	calFileNameNode              = roxml_get_attr(calElementConfigXMLTag, "calFileName", 0);
	
	calID                    = roxml_get_content(calIDNode, NULL, 0, &len);
	calConfigParam->calID    = strtoul(calID, NULL, 10);

	calType                  = roxml_get_content(calTypeNode, NULL, 0, &len);
	calConfigParam->calType  = strtoul(calType, NULL, 10);

	calConfigParam->calFileName  = roxml_get_content(calFileNameNode, NULL, 0, &len);
	
	return retError;
}	

/*
 *********************** End of file ******************************************
 */
