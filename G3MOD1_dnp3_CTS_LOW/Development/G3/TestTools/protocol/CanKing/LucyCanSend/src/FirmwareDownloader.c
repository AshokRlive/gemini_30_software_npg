/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1577 2012-07-23 20:18:25Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/branches/F491_Slave_Bootloader/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1577 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-07-23 21:18:25 +0100 (Mon, 23 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/12     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
//#include <memory.h>
//#include <windows.h>
//#include <conio.h>
///#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "FirmwareDownloader.h"
#include "CmdParser.h"
#include "Main.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static lu_uint32_t msgBlockCount;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Downloading firmware to the slave board
 *
 *   Downloading the specified firmware file in to slave board
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR FirmwareDownloaderFilePrepare(ParsingArgsStr *argsFromCommand)
{
   	SB_ERROR retValue = SB_ERROR_NONE;
	lu_uint32_t bytesToSend; 
	lu_uint32_t msgByteCount = 0, msgBlockCount = 0, lastBlockSize = 0;

	flagDecoderPtr = argsFromCommand;

	memset(&argsFromCommand->msgBuff[0], 0, MODULE_MESSAGE_LENGTH);

	firmwareFile.filePtr = fopen(argsFromCommand->sourceFileName,"rb");
			
	if(firmwareFile.filePtr == NULL)
	{
		printf(" Error in opening file ");
		return SB_ERROR_INITIALIZED;
	} 
	
	/* Seeking to end to find size of the file */
	fseek (firmwareFile.filePtr , 1 , SEEK_END);
	firmwareFile.fileSize = ftell (firmwareFile.filePtr);
	bytesToSend = firmwareFile.fileSize;
	rewind (firmwareFile.filePtr);
	printf("\n\n File Size = %d Bytes",firmwareFile.fileSize);

	/* Calculatin of number of 256bytes blocks and remaining bytes */
	firmwareFile.lastBlockSize  = firmwareFile.fileSize % BLOCK_256;
	firmwareFile.numberOfBlocks = firmwareFile.fileSize / BLOCK_256;
	if(firmwareFile.lastBlockSize > 0)
	{
		firmwareFile.numberOfBlocks = firmwareFile.numberOfBlocks + 1;  
	}

	printf("\n\n Number of Blocks = %x\n", firmwareFile.numberOfBlocks + 1);
	printf("\n Downloading Firmware Block :  ");

	return retValue;
}

lu_bool_t FirmwareDownloaderBlockSend(FileInfoStr *firmwareFilePtr, ParsingArgsStr *argsFromCommand)
{
	SB_ERROR retValue;
	lu_bool_t finish;
	lu_uint32_t msgByteCount;
	
	msgByteCount = 0;
	memset(argsFromCommand->msgBuff,0xFF,BLOCK_256+2);
	 	 	 
	argsFromCommand->msgBuff[0] = msgBlockCount & 0xFF; 
	argsFromCommand->msgBuff[1] = msgBlockCount >> 8; 
	 
	msgByteCount = fread(&argsFromCommand->msgBuff[2], sizeof(lu_int8_t), BLOCK_256, firmwareFilePtr->filePtr);
	 
	msgByteCount  = msgByteCount  + 2; 
	msgBlockCount = msgBlockCount + 1;
	
	if(msgBlockCount == firmwareFilePtr->numberOfBlocks)
	{
		finish = LU_FALSE;	
		/* Closing of the firmware file */
		fclose(firmwareFilePtr->filePtr);
		msgBlockCount = 0;
	}
	else
	{
		finish = LU_TRUE;
	}
		 
	printf("%0X%02X ", argsFromCommand->msgBuff[1],argsFromCommand->msgBuff[0] );
	
	 /* Sending the firmware block via CAN bus */
	retValue = CANCSendFromMCM( MODULE_MSG_TYPE_BL,
								MODULE_MSG_ID_BL_WRITE_FIRMWARE_C,
								argsFromCommand->CANHeader.deviceDst,
								argsFromCommand->CANHeader.deviceIDDst,
								BLOCK_256 + 2,
								argsFromCommand->msgBuff
							  );
	//Sleep(5);

	return finish;
}

SB_ERROR LucyCANSendFirmwareDownloaderProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_NONE;
	ModStatusReplyStr *downloadStatus;
	
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_FIRMWARE_DOWNLOAD)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_BL:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_BL_WRITE_FIRMWARE_R: 
						if(msgPtr->msgLen == 1)
						{
							downloadStatus = (ModStatusReplyStr*)msgPtr;
							if(downloadStatus->status == SB_ERROR_WRITE_FAIL)
							{
								printf("%02X\b\b\b\b\b\b Error in Downloading", downloadStatus->status);
								retError = SB_ERROR_WRITE_FAIL;
							}
							printf("%02X\b\b\b\b\b\b", downloadStatus->status);
						}
						break;
					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;
			default:
				printf("\n Error in Downloading ");
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}

	return retError;
}

/*
 *********************** End of file ******************************************
 */
