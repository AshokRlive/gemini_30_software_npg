/*! \file
******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
******************************************************************************
*    PROJECT:
*       G3 [Module Name]
*
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*       \brief
*
*      
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*
*
*    CREATION
*
*   Date          Name        Details
*   --------------------------------------------------------------------------
*   09/08/12    venkat_s    Initial version / Original by Venkat.
*
******************************************************************************
*   COPYRIGHT
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
******************************************************************************
*/


/*
******************************************************************************
* INCLUDES - Standard Library Modules Used
******************************************************************************
*/

/*
******************************************************************************
* INCLUDES - Project Specific Modules Used
******************************************************************************
*/

#include "LucyCANDecoder.h"
#include "CmdParser.h"

#include "SingleCmdSender.h"
#include "FirmwareDownloader.h"
#include "FirmwareVerifier.h"
#include "CalElementReader.h"
#include "CalElementAdder.h"
#include "NvramFactoryInfoRead.h"
#include "NvramFactoryInfoWrite.h"
#include "NvramPSMOptionsRead.h"
#include "NvramPSMOptionsWrite.h"
#include "NvramBatteryOptionsWrite.h"
#include "NvramBatteryOptionsRead.h"


/*
******************************************************************************
* LOCAL - Definitions And Macros
******************************************************************************
*/

/*
******************************************************************************
* LOCAL - Typedefs and Structures
******************************************************************************
*/


/*
*******************************************************************************
* LOCAL - Prototypes Of Local Functions
*******************************************************************************
*/


/*
******************************************************************************
* EXPORTED - Variables
******************************************************************************
*/

/*
******************************************************************************
* LOCAL - Global Variables
******************************************************************************
*/

/*
******************************************************************************
* Exported Functions
******************************************************************************
*/

SB_ERROR LucyCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_NONE;
	
	LU_UNUSED(time);

	retError = LucyCANSendSingleCommandSendProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = LucyCANSendFirmwareVerifierProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = LucyCANSendFirmwareDownloaderProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = LucyCANSendCalElementAdderProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = LucyCANSendCalElementReaderProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}
	
	retError = LucyCANSendNvramReadFactoryInfoProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = LucyCANSendNvramReadPSMOptionsProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = LucyCANSendNvramReadBatteryOptionsProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	return retError;
}


/*
******************************************************************************
* Local Functions
******************************************************************************
*/

/*!
******************************************************************************
*   \brief Brief description
*
*   Detailed description
*
*   \param parameterName Description 
*
*
*   \return 
*
******************************************************************************
*/


/*
*********************** End of file ******************************************
*/

