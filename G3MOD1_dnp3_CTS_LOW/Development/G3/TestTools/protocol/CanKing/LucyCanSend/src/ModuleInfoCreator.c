/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *  30/07/15      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ModuleInfoCreator.h"
#include "roxml.h"
#include "NVRAMDef.h"
#include "NVRAMDefOptPSM.h"
#include "NVRAMDefOptHMI.h"
#include "NVRAMDefOptBat.h"
#include "NvramXMLParser.h"
#include "TestIOManager.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef struct moduleXmlToDataDef
{
	node_t      *elementPtr;
	node_t      *attributePtr;
	lu_int8_t   *valueStringPtr;
	void	    *valuePtr;
	lu_uint8_t  size;
}moduleXmlToDataStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
SB_ERROR ModuleInfoCreatorOptions( lu_uint16_t      handle,
								   lu_int8_t	    *moduleInfoXMLFile,
								   lu_int8_t	    *optionsXMLFile,
								   lu_int8_t	    *assemblyNumber,
								   lu_int8_t	    *assemblyRevision,
								   void			    *optionsStrPtr,
								   strutureMapStr   optionsElementPtrTable[],
								   lu_uint8_t	    numberOfOptions,
								   lu_int8_t	    *optionsXmlTagTable[],
								   const xmlInfoStr *xmlTagMapStrPtr
								);

void StringToByteData(lu_int8_t *string, void *dataPtr, lu_uint8_t size);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
moduleXmlToDataStr moduleXmlData; 
NVRAMInfoStr       moduleFactrotyInfo;
psmOptionsBlkStr   modulePSMOptions;
hmiOptionsBlkStr   moduleHMIOptions;
NVRAMDefOptBatStr  batteryOptions;

static strutureMapStr moduleFactoryInfoElementPtrTable[FACTORYINFO_FIELD_MAX] = 
{																	//sizeof(((struct A*)0)->arr);
	{(void*)&moduleFactrotyInfo.InfoBlockVersionMajor,		sizeof(((NVRAMInfoStr*)NULL)->InfoBlockVersionMajor)	},
	{(void*)&moduleFactrotyInfo.InfoBlockVersionMinor,		sizeof(((NVRAMInfoStr*)NULL)->InfoBlockVersionMinor)	},
	{(void*)&moduleFactrotyInfo.moduleType,					sizeof(((NVRAMInfoStr*)NULL)->moduleType)				},
	{(void*)&moduleFactrotyInfo.moduleFeatureMajor,			sizeof(((NVRAMInfoStr*)NULL)->moduleFeatureMajor)		},
	{(void*)&moduleFactrotyInfo.moduleFeatureMinor,			sizeof(((NVRAMInfoStr*)NULL)->moduleFeatureMinor)		},
	{(void*)&moduleFactrotyInfo.batchNumber,				sizeof(((NVRAMInfoStr*)NULL)->batchNumber)				},
	{(void*)&moduleFactrotyInfo.supplierId,					sizeof(((NVRAMInfoStr*)NULL)->supplierId)				},
	{(void*)&moduleFactrotyInfo.assemblyNo,					sizeof(((NVRAMInfoStr*)NULL)->assemblyNo)				},
	{(void*)&moduleFactrotyInfo.assemblyRev,				sizeof(((NVRAMInfoStr*)NULL)->assemblyRev)				},
	{(void*)&moduleFactrotyInfo.serialNumber,				sizeof(((NVRAMInfoStr*)NULL)->serialNumber)			    },
	{(void*)&moduleFactrotyInfo.subAssemblyNo,				sizeof(((NVRAMInfoStr*)NULL)->subAssemblyNo)			},
	{(void*)&moduleFactrotyInfo.subAssemblyRev,				sizeof(((NVRAMInfoStr*)NULL)->subAssemblyRev)			},
	{(void*)&moduleFactrotyInfo.subAssemblySerialNumber,	sizeof(((NVRAMInfoStr*)NULL)->subAssemblySerialNumber)  },
	{(void*)&moduleFactrotyInfo.subAssemblyBatchNumber,		sizeof(((NVRAMInfoStr*)NULL)->subAssemblyBatchNumber)	},
	{(void*)&moduleFactrotyInfo.buildDay,					sizeof(((NVRAMInfoStr*)NULL)->buildDay)				    },
	{(void*)&moduleFactrotyInfo.buildMonth,					sizeof(((NVRAMInfoStr*)NULL)->buildMonth)				},
	{(void*)&moduleFactrotyInfo.buildYear,					sizeof(((NVRAMInfoStr*)NULL)->buildYear)				}
};

static strutureMapStr psmOptionsElementPtrTable[PSM_OPTIONS_FIELD_MAX] = 
{																	//sizeof(((struct A*)0)->arr);
	{(void*)&modulePSMOptions.psmOptions.optBlockVersionMajor,	sizeof(((NVRAMOptPSMStr*)NULL)->optBlockVersionMajor)	},
	{(void*)&modulePSMOptions.psmOptions.optBlockVersionMinor,	sizeof(((NVRAMOptPSMStr*)NULL)->optBlockVersionMinor)	},
	{(void*)&modulePSMOptions.psmOptions.optionBitField,		sizeof(((NVRAMOptPSMStr*)NULL)->optionBitField)		},
	{(void*)&modulePSMOptions.psmOptions.commsSecondaryFitted,	sizeof(((NVRAMOptPSMStr*)NULL)->commsSecondaryFitted)	},
	{(void*)&modulePSMOptions.psmOptions.commsPrimaryVolts,		sizeof(((NVRAMOptPSMStr*)NULL)->commsPrimaryVolts)		},
	{(void*)&modulePSMOptions.psmOptions.commsSecondaryVolts,	sizeof(((NVRAMOptPSMStr*)NULL)->commsSecondaryVolts)	}
};

static strutureMapStr hmiOptionsElementPtrTable[HMI_OPTIONS_FIELD_MAX] = 
{																	//sizeof(((struct A*)0)->arr);
	{(void*)&moduleHMIOptions.hmiOptions.optBlockVersionMajor,	sizeof(((NVRAMOptHMIStr*)NULL)->optBlockVersionMajor)	},
	{(void*)&moduleHMIOptions.hmiOptions.optBlockVersionMinor,	sizeof(((NVRAMOptHMIStr*)NULL)->optBlockVersionMinor)	},
	{(void*)&moduleHMIOptions.hmiOptions.displayType,		    sizeof(((NVRAMOptHMIStr*)NULL)->displayType)		    },
	{(void*)&moduleHMIOptions.hmiOptions.contrastDigipotSet,	sizeof(((NVRAMOptHMIStr*)NULL)->contrastDigipotSet)	    }
};

static strutureMapStr batteryOptionsElementPtrTable[BATTERY_OPTIONS_FIELD_MAX] = 
{																
	{(void*)&batteryOptions.BatteryChemistry,						sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryChemistry)					  },
	{(void*)&batteryOptions.BatteryCellNominalVolts,				sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellNominalVolts)				  },
	{(void*)&batteryOptions.BatteryCellFullChargeVolts,				sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellFullChargeVolts)			  },
	{(void*)&batteryOptions.BatteryCellMaxCapacity,					sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellMaxCapacity)				  },
	{(void*)&batteryOptions.BatteryCellChargingCurrent,				sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellChargingCurrent)			  },
	{(void*)&batteryOptions.BatteryCellChargingVolts,				sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellChargingVolts)			  },
	{(void*)&batteryOptions.BatteryCellLeakage,						sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellLeakage)					  },
	{(void*)&batteryOptions.BatteryCellFloatCharge,					sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellFloatCharge)				  },
	{(void*)&batteryOptions.BatteryCellChargingMethod,				sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellChargingMethod)			  },
	{(void*)&batteryOptions.BatteryPackNoOfCols,					sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackNoOfCols)					  },
	{(void*)&batteryOptions.BatteryPackNoOfRows,					sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackNoOfRows)					  },
	{(void*)&batteryOptions.BatteryPackMaxChargeTemp,				sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackMaxChargeTemp)			  },
	{(void*)&batteryOptions.BatteryPackMaxChargeTime,				sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackMaxChargeTime)			  },
	{(void*)&batteryOptions.BatteryPackTimeBetweenCharge,			sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackTimeBetweenCharge)		  },
	{(void*)&batteryOptions.BatteryPackOverideTime,					sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackOverideTime)				  },
	{(void*)&batteryOptions.BatteryPackChargeTrigger,				sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackChargeTrigger)			  },
	{(void*)&batteryOptions.BatteryPackThresholdVolts,				sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackThresholdVolts)			  },
	{(void*)&batteryOptions.BatteryPackLowLevel,					sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackLowLevel)					  },
	{(void*)&batteryOptions.BatteryTestTimeMinutes,					sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestTimeMinutes)				  },
	{(void*)&batteryOptions.BatteryTestCurrentMinimum,	            sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestCurrentMinimum)			  },
	{(void*)&batteryOptions.BatteryTestSuspendTimeMinutes,          sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestSuspendTimeMinutes)         },
	{(void*)&batteryOptions.BatteryTestMaximumDurationTimeMinutes,  sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestMaximumDurationTimeMinutes) },
	{(void*)&batteryOptions.BatteryTestDischargeCapacity,           sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestDischargeCapacity)          },
	{(void*)&batteryOptions.BatteryTestThresholdVolts,              sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestThresholdVolts)             },
	{(void*)&batteryOptions.BatteryTestLoadType,                    sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestLoadType)                   },
	{(void*)&batteryOptions.BatteryPackShutdownLevelVolts,          sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackShutdownLevelVolts)         },
	{(void*)&batteryOptions.BatteryPackDeepDischargeVolts,          sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackDeepDischargeVolts)         }

};


lu_int8_t *moduleFactoryInfoXmlTagTable[FACTORYINFO_FIELD_MAX] = 
{
	"InfoBlockMajor",
	"InfoBlockMinor", 	 		
	"ModuleType",          		
	"FeatureMajor",        		
	"FeatureMinor",        		
	"BatchNumber",         		
	"SupplierID",    		 		
	"AssemblyNumber",      		
	"AssemblyRevision",    		
	"SerialNumber",		 		
	"SubAssemblyNumber",   		
	"SubAssemblyRevision",     	
	"SubAssemblySerialNumber", 	
	"SubAssemblyBatchNumber", 	
	"BuildDay",					
	"BuildMonth",					
	"BuildYear"					
};

lu_int8_t *psmOptionsXmlTagTable[PSM_OPTIONS_FIELD_MAX] = 
{
	"OptBlockMajor",
	"OptBlockMinor",
	"CommsPrimaryFitted",
	"CommsSecondaryFitted",
	"CommsPrimaryVolts",
	"CommsSecondaryVolts"
};

lu_int8_t *hmiOptionsXmlTagTable[HMI_OPTIONS_FIELD_MAX] = 
{
	"OptBlockMajor",
	"OptBlockMinor",
	"DisplayType",
	"ContrastSetDigipot"
};

lu_int8_t *batteryOptionsXmlTagTable[BATTERY_OPTIONS_FIELD_MAX] = 
{
	"BatteryChemistry",  			   			
	"BatteryCellNominalVolts",    			
	"BatteryCellFullChargeVolts", 			
	"BatteryCellMaxCapacity",     			
	"BatteryCellChargingCurrent",  		
	"BatteryCellChargingVolts",  			
	"BatteryCellLeakage",  				
	"BatteryCellFloatCharge",  			
	"BatteryCellChargingMethod",  			
	"BatteryPackNoOfCols",  					
	"BatteryPackNoOfRows",  					
	"BatteryPackMaxChargeTemp",  				
	"BatteryPackMaxChargeTime",  				
	"BatteryPackTimeBetweenCharge",  			
	"BatteryPackOverideTime",  				
	"BatteryPackChargeTrigger",  				
	"BatteryPackThresholdVolts",  			
	"BatteryPackLowLevel",  				
	"BatteryTestTimeMinutes", 				
	"BatteryTestCurrentMinimum",  			
	"BatteryTestSuspendTimeMinutes",  		
	"BatteryTestMaximumDurationTimeMinutes",  
	"BatteryTestDischargeCapacity",  			
	"BatteryTestThresholdVolts",  			
	"BatteryTestLoadType",  				
	"BatteryPackShutdownLevelVolts",  		
	"BatteryPackDeepDischargeVolts"  		
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR ModuleInfoCreatorFactoryInfo( lu_uint16_t  handle,
									   lu_int8_t   *moduleInfoXMLFile,
									   lu_int8_t   *factoryInfoXMLFile,
									   lu_int8_t   *supplierID,
									   lu_int8_t   *assemblyNumber,
									   lu_int8_t   *assemblyRevision,
									   lu_uint32_t *serialNumber,
									   lu_uint8_t  *buildDay,
									   lu_uint8_t  *buildMonth,
									   lu_uint16_t *buildYear
									 )
{
	SB_ERROR retError = SB_ERROR_NONE;
	node_t *root, *moduleNode, *factoryInfoNode, *moduleAssemblyNumberNode, *moduleAssemblyRevisionNode, *moduleAssemblyTypeNode;
	lu_int8_t *moduleAssemblyNumber, *moduleAssemblyRevision, *moduleAssemblyType, *idNvramIOID, *appNvramIOID;
	int len = 0;
	lu_uint8_t factoryInfoTag;

	root = roxml_load_doc((lu_int8_t*)moduleInfoXMLFile);
	if(root == NULL)
	{
		retError = SB_ERROR_PARAM;
	}
	else
	{
		moduleNode      = roxml_get_chld(root,       "Module"       ,  0);
		factoryInfoNode = roxml_get_chld(moduleNode, "FactoryInfo"  ,  0);

		moduleAssemblyTypeNode     = roxml_get_attr(moduleNode, "Type",   0);
		moduleAssemblyNumberNode   = roxml_get_attr(moduleNode, "AssemblyNumber",   0);
		moduleAssemblyRevisionNode = roxml_get_attr(moduleNode, "AssemblyRevision", 0);

		moduleAssemblyType      = roxml_get_content(moduleAssemblyTypeNode,   NULL, 0, &len);
		moduleAssemblyNumber    = roxml_get_content(moduleAssemblyNumberNode,   NULL, 0, &len);
		moduleAssemblyRevision  = roxml_get_content(moduleAssemblyRevisionNode, NULL, 0, &len);

		if( (strcmp(moduleAssemblyNumber, assemblyNumber)     == 0 ) &&
			(strcmp(moduleAssemblyRevision, assemblyRevision) == 0 )
		   )
		{
			for(factoryInfoTag = 0; factoryInfoTag < FACTORYINFO_FIELD_MAX; factoryInfoTag++)
			{
				moduleXmlData.elementPtr      = roxml_get_chld(factoryInfoNode, moduleFactoryInfoXmlTagTable[factoryInfoTag],  NULL);
				moduleXmlData.attributePtr    = roxml_get_attr(moduleXmlData.elementPtr, "value",           NULL);

				moduleXmlData.valueStringPtr  = roxml_get_content(moduleXmlData.attributePtr, NULL, 0, &len);

				StringToByteData( moduleXmlData.valueStringPtr,
								  moduleFactoryInfoElementPtrTable[factoryInfoTag].elementPtr,
								  moduleFactoryInfoElementPtrTable[factoryInfoTag].size
								);
			}

			memcpy(&moduleFactrotyInfo.supplierId,   supplierID,       sizeof(((NVRAMInfoStr*)NULL)->supplierId)  );
			//memcpy(&moduleFactrotyInfo.assemblyNo,   assemblyNumber,   sizeof(((NVRAMInfoStr*)NULL)->assemblyNo)  );
			//memcpy(&moduleFactrotyInfo.assemblyRev,  assemblyRevision, sizeof(((NVRAMInfoStr*)NULL)->assemblyRev) );

			memcpy(&moduleFactrotyInfo.serialNumber, &serialNumber,    sizeof(((NVRAMInfoStr*)NULL)->serialNumber));

			/* Copying Serial number to Sub Assembly Number */
			memcpy(&moduleFactrotyInfo.subAssemblySerialNumber, &serialNumber,    sizeof(((NVRAMInfoStr*)NULL)->subAssemblySerialNumber));
			
			memcpy(&moduleFactrotyInfo.buildDay,     &buildDay,        sizeof(((NVRAMInfoStr*)NULL)->buildDay)    );
			memcpy(&moduleFactrotyInfo.buildMonth,   &buildMonth,      sizeof(((NVRAMInfoStr*)NULL)->buildMonth)  );
			memcpy(&moduleFactrotyInfo.buildYear,    &buildYear,       sizeof(((NVRAMInfoStr*)NULL)->buildYear)   );
		}
	}

	retError = BundleNVRAMDataToXMLFile( (lu_uint8_t*)&moduleFactrotyInfo,
										 factoryInfoXMLFile,
										 FACTORYINFO_FIELD_MAX,
										 &moduleFactoryInfoElementPtrTable[0],
										 &factoryInfoMap[0]	
									   );

#ifndef MCMTestIOManager

	if((strcmp(moduleAssemblyType,"BATTERY")== 0))
	{
		idNvramIOID   = "IO_ID_BATTERY_ID_NVRAM";
        appNvramIOID  = "IO_ID_BATTERY_DATA_NVRAM";
	}
	else
	{
		idNvramIOID   = "IO_ID_IDENTITY_NVRAM";
        appNvramIOID  = "IO_ID_APPLICATION_NVRAM";
	}
	
	retError = WriteFileToNVRAM( handle,
								 idNvramIOID,
								 factoryInfoXMLFile,
								 NVRAM_ID_BLK_INFO
							   );

	retError = WriteFileToNVRAM( handle,
								 appNvramIOID,
								 factoryInfoXMLFile,
								 NVRAM_ID_BLK_INFO
							   );
#endif

	return retError;
}

#ifndef MCMTestIOManager
SB_ERROR ModuleInfoCreatorPSMOptions( lu_uint16_t  handle,
									  lu_int8_t *moduleInfoXMLFile,
									  lu_int8_t *PSMOptionsXMLFile,
									  lu_int8_t *assemblyNumber,
									  lu_int8_t *assemblyRevision
									)
{
	SB_ERROR retError;

	ModuleInfoCreatorOptions( handle,
	   				          moduleInfoXMLFile,
							  PSMOptionsXMLFile,
							  assemblyNumber,
							  assemblyRevision,
							  &modulePSMOptions,
							  psmOptionsElementPtrTable,
							  PSM_OPTIONS_FIELD_MAX,
							  psmOptionsXmlTagTable,
							  psmOptionsMap
						   );

	retError = WriteFileToNVRAM( handle,
								 "IO_ID_IDENTITY_NVRAM",
								 PSMOptionsXMLFile,
								 NVRAM_ID_BLK_OPTS
							   );

	retError = WriteFileToNVRAM( handle,
								 "IO_ID_APPLICATION_NVRAM",
								 PSMOptionsXMLFile,
								 NVRAM_ID_BLK_OPTS
							   );

	return retError;
}
#endif

#ifndef MCMTestIOManager
SB_ERROR ModuleInfoCreatorHMIOptions( lu_uint16_t  handle,
									  lu_int8_t *moduleInfoXMLFile,
									  lu_int8_t *HMIOptionsXMLFile,
									  lu_int8_t *assemblyNumber,
									  lu_int8_t *assemblyRevision
									)
{
	SB_ERROR retError;

	ModuleInfoCreatorOptions( handle,
	   				          moduleInfoXMLFile,
							  HMIOptionsXMLFile,
							  assemblyNumber,
							  assemblyRevision,
							  &moduleHMIOptions,
							  hmiOptionsElementPtrTable,
							  HMI_OPTIONS_FIELD_MAX,
							  hmiOptionsXmlTagTable,
							  hmiOptionsMap
						   );

	retError = WriteFileToNVRAM( handle,
								 "IO_ID_IDENTITY_NVRAM",
								 HMIOptionsXMLFile,
								 NVRAM_ID_BLK_OPTS
							   );

	retError = WriteFileToNVRAM( handle,
								 "IO_ID_APPLICATION_NVRAM",
								 HMIOptionsXMLFile,
								 NVRAM_ID_BLK_OPTS
							   );

	return retError;
}
#endif


#ifndef MCMTestIOManager
SB_ERROR ModuleInfoCreatorBatteryOptions( lu_uint16_t handle,
										  lu_int8_t	  *moduleInfoXMLFile,
										  lu_int8_t	  *batteryOptionsXMLFile,
										  lu_int8_t	  *assemblyNumber,
										  lu_int8_t	  *assemblyRevision
										)
{
	SB_ERROR retError;
	
	retError = ModuleInfoCreatorOptions( handle,
	   							         moduleInfoXMLFile,
										 batteryOptionsXMLFile,
										 assemblyNumber,
										 assemblyRevision,
										 &batteryOptions,
										 batteryOptionsElementPtrTable,
										 BATTERY_OPTIONS_FIELD_MAX,
										 batteryOptionsXmlTagTable,
										 batteryOptionsMap
									   );

	if(retError == SB_ERROR_NONE)
	{
	   
		InitIOID( handle, "IO_ID_BAT_ID_WEN");
		WriteIOID( handle, "IO_ID_BAT_ID_WEN", 0);

	    retError = WriteFileToNVRAM( handle,
									 "IO_ID_BATTERY_ID_NVRAM",
									 batteryOptionsXMLFile,
									 NVRAM_BATT_ID_BLK_OPTS
								   );

		retError = WriteFileToNVRAM( handle,
									 "IO_ID_BATTERY_DATA_NVRAM",
									 batteryOptionsXMLFile,
									 NVRAM_BATT_ID_BLK_OPTS
								   );
	}
	
	return retError;
}
#endif

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR ModuleInfoCreatorOptions( lu_uint16_t      handle,
								   lu_int8_t	    *moduleInfoXMLFile,
								   lu_int8_t	    *optionsXMLFile,
								   lu_int8_t	    *assemblyNumber,
								   lu_int8_t	    *assemblyRevision,
								   void			    *optionsStrPtr,
								   strutureMapStr   optionsElementPtrTable[],
								   lu_uint8_t	    numberOfOptions,
								   lu_int8_t	    *optionsXmlTagTable[],
								   const xmlInfoStr *xmlTagMapStrPtr
								 )
{
	SB_ERROR retError = SB_ERROR_NONE;
	node_t *root, *moduleNode, *optionsNode, *moduleAssemblyNumberNode, *moduleAssemblyRevisionNode;
	lu_int8_t *moduleAssemblyNumber, *moduleAssemblyRevision;
	int len = 0;
	lu_uint8_t optionsTag;

	root = roxml_load_doc((lu_int8_t*)moduleInfoXMLFile);
	if(root == NULL)
	{
		retError = SB_ERROR_PARAM;
	}
	else
	{
		retError = SB_ERROR_PARAM;
		moduleNode  = roxml_get_chld(root,       "Module"   ,  0);
		optionsNode = roxml_get_chld(moduleNode, "Options"  ,  0);

		moduleAssemblyNumberNode   = roxml_get_attr(moduleNode, "AssemblyNumber",   0);
		moduleAssemblyRevisionNode = roxml_get_attr(moduleNode, "AssemblyRevision", 0);

		moduleAssemblyNumber    = roxml_get_content(moduleAssemblyNumberNode,   NULL, 0, &len);
		moduleAssemblyRevision  = roxml_get_content(moduleAssemblyRevisionNode, NULL, 0, &len);

		if( (strcmp(moduleAssemblyNumber, assemblyNumber)     == 0 ) &&
			(strcmp(moduleAssemblyRevision, assemblyRevision) == 0 )
		   )
		{
			for(optionsTag = 0; optionsTag < numberOfOptions; optionsTag++)
			{
				moduleXmlData.elementPtr      = roxml_get_chld(optionsNode, optionsXmlTagTable[optionsTag],  NULL);
				moduleXmlData.attributePtr    = roxml_get_attr(moduleXmlData.elementPtr, "value",           NULL);

				moduleXmlData.valueStringPtr  = roxml_get_content(moduleXmlData.attributePtr, NULL, 0, &len);

				StringToByteData( moduleXmlData.valueStringPtr,
								  optionsElementPtrTable[optionsTag].elementPtr,
								  optionsElementPtrTable[optionsTag].size
								);
			}

			retError = BundleNVRAMDataToXMLFile( (lu_uint8_t*)optionsStrPtr,
										 optionsXMLFile,
										 numberOfOptions,
										 &optionsElementPtrTable[0],
										 &xmlTagMapStrPtr[0]	
									   );
		}
	}

	return retError;
}


void StringToByteData(lu_int8_t *string, void *dataPtr, lu_uint8_t size)
{
	if(size == sizeof(lu_uint8_t))
	{
		*(lu_uint8_t*)dataPtr = strtoul(string, NULL, 10);
	}
				
	else if(size == sizeof(lu_uint16_t))
	{
		*(lu_uint16_t*)(dataPtr) = strtoul(string, NULL, 10);
	}

	else if(size == sizeof(lu_uint32_t))
	{
		*(lu_uint32_t*)(dataPtr) = strtoul(string, NULL, 10);
	}
	else
	{
		strcpy((lu_int8_t*)(dataPtr), string);
	}
}

/*
 *********************** End of file ******************************************
 */