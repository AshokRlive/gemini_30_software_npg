/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol global definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/09/11      krauklis_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _CAN_MSG_TABLES_INCLUDED
#define _CAN_MSG_TABLES_INCLUDED



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define MODULE_TYPE_INIT(name) {#name, MODULE_##name}
#define MSG_TYPE_INIT(name) {#name, MODULE_MSG_TYPE_##name}

#define MSG_ID_INIT(Typename, IDname) {#IDname, MODULE_MSG_ID_##Typename##_##IDname}

#define	REPLY_STATUS_INIT(name) {#name, REPLY_STATUS_##name}

/* ModuleProtocolPSM.h structure initializations*/
#define	PSM_CH_DINPUT_INIT(name) {#name, PSM_CH_DINPUT_##name}
#define	PSM_CH_AINPUT_INIT(name) {#name, PSM_CH_AINPUT_##name}
#define	PSM_CH_PSUPPLY_INIT(name) {#name, PSM_CH_PSUPPLY_##name}
#define	PSM_CH_PSUPPLY_COMMS(name) {#name, PSM_CH_PSUPPLY_COMMS_##name}

// SCM
#define	SCM_CH_DINPUT_INIT(name) {#name, SCM_CH_DINPUT_##name}
#define	SCM_CH_AINPUT_INIT(name) {#name, SCM_CH_AINPUT_##name}

// FDM
#define	FDM_CH_AINPUT_INIT(name) {#name, FDM_CH_AINPUT_##name}

// HMI
#define	HMI_CH_AINPUT_INIT(name) {#name, HMI_CH_AINPUT_##name}

//#define CAN_MSG_TABLE_SIZE(table, type) (sizeof(table) / sizeof(type))
// IOM
#define	IOM_CH_DINPUT_INIT(name) {#name, IOM_CH_DINPUT_##name}
#define	IOM_CH_AINPUT_INIT(name) {#name, IOM_CH_AINPUT_##name}

#define	DSM_CH_DINPUT_INIT(name) {#name, DSM_CH_DINPUT_##name}
#define	DSM_CH_AINPUT_INIT(name) {#name, DSM_CH_AINPUT_##name}

#define	FPM_CH_DINPUT_INIT(name) {#name, FPM_CH_DINPUT_##name}
#define	FPM_CH_AINPUT_INIT(name) {#name, FPM_CH_AINPUT_##name}

#define END_OF_TABLE (255)



/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*!
 * Data structure for the internal message queue
 */

typedef struct CANMsgDecoderDef
{
	lu_char_t		*msgTypePtr;
	lu_uint8_t		msgTypeValue;
} CANMsgDecoderStr;
	

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/* moduleprotocol.h tables*/
const CANMsgDecoderStr	CAN_MODULE_TYPE_Table[];
const CANMsgDecoderStr	CAN_MSG_TYPE_Table[];

const CANMsgDecoderStr	CAN_MSG_HPRIO_ID_Table[];
const CANMsgDecoderStr	CAN_MSG_EVENT_ID_Table[];
const CANMsgDecoderStr	CAN_MSG_HMI_EVENT_ID_Table[];
const CANMsgDecoderStr	CAN_MSG_MD_CMD_ID_Table[];
const CANMsgDecoderStr	CAN_MSG_CMD_ID_Table[];
const CANMsgDecoderStr	CAN_MSG_CFG_ID_Table[];
const CANMsgDecoderStr	CAN_MSG_CALTST_ID_Table[];
const CANMsgDecoderStr	CAN_MSG_IOMAN_ID_Table[];
const CANMsgDecoderStr	CAN_MSG_SYSALARM_ID_Table[];
const CANMsgDecoderStr	CAN_MSG_BLTST_ID_Table[];
const CANMsgDecoderStr	CAN_MSG_BLTST_1_ID_Table[];
const CANMsgDecoderStr  CAN_MSG_NVRAM_ID_Table[];
const CANMsgDecoderStr	CAN_MSG_LPRIO_ID_Table[];

/* ModuleProtocolPSM.h tables*/
const CANMsgDecoderStr	CAN_MSG_REPLY_STATUS[];
const CANMsgDecoderStr	CAN_MSG_PSM_CH_DINPUT[];
const CANMsgDecoderStr	CAN_MSG_PSM_CH_AINPUT[];
const CANMsgDecoderStr	CAN_MSG_PSM_CH_PSUPPLY[];

/* SCM */
const CANMsgDecoderStr	CAN_MSG_SCM_CH_DINPUT[];
const CANMsgDecoderStr	CAN_MSG_SCM_CH_AINPUT[];

/* FDM */
const CANMsgDecoderStr	CAN_MSG_FDM_CH_AINPUT[];

/* HMI */
const CANMsgDecoderStr	CAN_MSG_HMI_CH_DINPUT[];
const CANMsgDecoderStr	CAN_MSG_HMI_CH_AINPUT[];

/* IOM */
const CANMsgDecoderStr	CAN_MSG_IOM_CH_DINPUT[];
const CANMsgDecoderStr	CAN_MSG_IOM_CH_AINPUT[];

/* DSM */
const CANMsgDecoderStr	CAN_MSG_DSM_CH_DINPUT[];
const CANMsgDecoderStr	CAN_MSG_DSM_CH_AINPUT[];

/* FPM */
const CANMsgDecoderStr	CAN_MSG_FPM_CH_DINPUT[];
const CANMsgDecoderStr	CAN_MSG_FPM_CH_AINPUT[];


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */


#endif /* _CAN_MSG_TABLES_INCLUDED */

/*
 *********************** End of file ******************************************
 */

