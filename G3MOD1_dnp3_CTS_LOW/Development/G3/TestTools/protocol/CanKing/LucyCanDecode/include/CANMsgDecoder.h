/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol global definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26/09/11      krauklis_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _CAN_MSG_DECODER_INCLUDED
#define _CAN_MSG_DECODER_INCLUDED



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CANMsgTables.h"


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define CANMsgDecodeFormatDEC		0 
#define CANMsgDecodeFormatHEX		1 

#define	CANMsgDecodedFormatLen		20		/* Double Word string in HEX (0xFFFFFFFF ) or DEC (4294967295 ) */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */




/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

void ResolveModuleType(DevTypeValue, outputstring);
void ResolveMsg (canHeadPtr, outputstring);
void DecodeCANdata (CANHeaderStr* canHeadPtr, lu_uint8_t *CANdata, lu_uint8_t MsgLength, lu_char_t *outputstring, lu_uint8_t DecodeFormat);

void CANMsgDecoderSearchID(Table_name, MsgID, outputstring);

//lu_char_t *FormatCANOutputByte(ByteValue, DecodeFormat);
void FormatCANOutputByte (lu_uint8_t ByteValue, lu_char_t *ValueString, lu_uint8_t DecodeFormat);
//void FormatCANOutputByte(ByteValue, ValueString, DecodeFormat);
void FormatCANOutputWord (lu_uint16_t WordValue, lu_char_t *ValueString, lu_uint8_t DecodeFormat);
//void FormatCANOutputWord(WordValue, ValueString, DecodeFormat);
void FormatCANOutputDWord (lu_uint32_t DWordValue, lu_char_t *ValueString, lu_uint8_t DecodeFormat);

void FormatStatus (const CANMsgDecoderStr Table_name[], lu_uint8_t Value, lu_char_t *outputstring, lu_char_t *ValueString, lu_uint8_t DecodeFormat);
void FormatChannel (const CANMsgDecoderStr Table_name[], lu_uint8_t Value, lu_char_t *outputstring, lu_char_t *ValueString, lu_uint8_t DecodeFormat);
//void FormatChannel (Table_name, Value, outputstring, ValueString, DecodeFormat);



#endif /* _CAN_MSG_DECODER_INCLUDED */

/*
 *********************** End of file ******************************************
 */

