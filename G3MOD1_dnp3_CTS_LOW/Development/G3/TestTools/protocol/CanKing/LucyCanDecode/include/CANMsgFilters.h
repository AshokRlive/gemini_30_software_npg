/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol global definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/10/11      krauklis_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _CAN_MSG_FILTERS_INCLUDED
#define _CAN_MSG_FILTERS_INCLUDED



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CANMsgTables.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define CANMsgFilterSRCModuleSize		32
#define CANMsgFilterMsgTypeSize			64
#define CANMsgFilterMsgIDSize			64
#define CANMsgFilterFileSize			4096

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef struct {
	lu_uint8_t	filter;
} FilterStr;



typedef struct {
	lu_uint8_t	filter;
	lu_uint8_t	IDs[CANMsgFilterMsgIDSize];
} MsgFilterStr;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
lu_uint8_t InitilaizeFilters (FileName, SRCModuleFilter, MsgTypeFilter, MsgTypeFilterActive, DecodeFormat);
void CANMsgFilterSearchID (const CANMsgDecoderStr Table_name[], lu_char_t *token, MsgFilterStr *MsgTypeFilter, lu_uint8_t MsgType);
//void CANMsgFilterSearchID (Table_name, token, MsgTypeFilter, MsgType);

#endif /* _CAN_MSG_FILTERS_INCLUDED */

/*
 *********************** End of file ******************************************
 */

