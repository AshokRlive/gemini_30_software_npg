/*
**                   Copyright 2007 by KVASER AB, SWEDEN      
**                        WWW: http://www.kvaser.com
**
** This software is furnished under a license and may be used and copied
** only in accordance with the terms of such license.
**
** Description:
**   Simple example DLL formatter for Kvaser CanKing.
**   
*/
#include "CANMsgDecoderDLL.h"

#include "stdio.h"

#include "lu_types.h"
#include "CANProtocol.h"
#include "ModuleProtocol.h"
#include "CANMsgDecoder.h"
#include "CANMsgFilters.h"



static MsgFilterStr		*MsgTypeFilterPtr = 0;
static FilterStr		*SRCModuleFilterPtr = 0;
static lu_uint8_t		DecodeFormat = CANMsgDecodeFormatDEC;
static lu_uint8_t		FiltersActive = 0;



//===========================================================================
void CKAPI Create(void)
//lu_uint32_t * CKAPI Create(void)
//MsgFilterStr * CKAPI Create(void)
{
	//return MsgTypeFilterPtr;
}


//===========================================================================
void CKAPI Destroy(void)
{

}


//===========================================================================
void CKAPI DebugFilters(MsgFilterStr **MsgTypeFilterOut, FilterStr **SRCModuleFilterOut, lu_uint8_t **DecodeFormatOut, lu_uint8_t **FiltersActiveOut)
{
	*MsgTypeFilterOut = MsgTypeFilterPtr;
	*SRCModuleFilterOut = SRCModuleFilterPtr;
	*DecodeFormatOut = &DecodeFormat;
	*FiltersActiveOut = &FiltersActive;
}


//===========================================================================
unsigned long CKAPI FormatMsg(void *msg, int messageType,
                              char *buf, unsigned long bufsiz)
{
  // The variable "foo" is just a demonstration of how to handle a
  // formatting routine that returns two strings.
  //static int   foo = 0;

	TCanMessage *m = (TCanMessage*) msg;
	unsigned long  id;
	CANHeaderStr	*canHeadPtr;
	
	

  // We currently understand only CAN messages so if it's something
  // else just return, telling CanKing we're useless.

	if (messageType != CKMSG_CAN_MESSAGE) return 0;
  
	id = m->id;
	canHeadPtr = (CANHeaderStr *)&id;
  
	strcpy(buf,"");


	if (FiltersActive == 0)
	{
		GenerateOutput (buf, m, canHeadPtr, &DecodeFormat);
	}
	else
	{
		if (SRCModuleFilterPtr[canHeadPtr->deviceSrc].filter == 1)
		{
			if((MsgTypeFilterPtr + canHeadPtr->messageType)->IDs[canHeadPtr->messageID] == 1)
			{
				GenerateOutput (buf, m, canHeadPtr, &DecodeFormat);
			}
		}
	}

  //foo = !foo;
  //if (foo) return MF_USE_RESULT | MF_COME_AGAIN;
  return MF_USE_RESULT;
}


//===========================================================================
void GenerateOutput (char *DecodedOutput, TCanMessage *canMessage, CANHeaderStr *canHeadPtr, lu_uint8_t *DecodeFormatPtr)
{
	lu_char_t*		DirStr = "Rx";
	lu_char_t		SrcDevice[20] = "";
	lu_char_t		FragInfo[20] = "";
	lu_char_t		DstDevice[20] = "";
	lu_char_t		MsgType[100] = "";
	lu_char_t		DecodedData[100] = "";

	lu_int8_t		cnt;
	lu_char_t		FormatedTime[20];
	lu_char_t		RawHEX[41]= "";
	lu_char_t		RawHEX_[10];
	lu_char_t*		Space = " ";

	lu_uint32_t		TimeSec;
	lu_uint32_t		TimeMs;

	CANFragmentHeaderStr *fragHeadPtr;



	if (canMessage->dir == 1) 
	{
		DirStr = "Rx";
	}
	else 
	{
		DirStr = "Tx";
	}


	ResolveModuleType(canHeadPtr->deviceSrc, SrcDevice);
	ResolveModuleType(canHeadPtr->deviceDst, DstDevice);
	ResolveMsg(canHeadPtr, MsgType);
	
	for (cnt = strlen(MsgType); cnt < 28; cnt++)
	{
		strcat(MsgType,Space);
	};
	

	DecodeCANdata (canHeadPtr, canMessage->data, canMessage->dlc, DecodedData, DecodeFormat);

	
	TimeSec = canMessage->time / 1e9;
	TimeMs = (canMessage->time / 1e6) - (TimeSec * 1000);
	sprintf_s(FormatedTime, 20, "%05u.%03us", TimeSec, TimeMs);
	


	for (cnt = 7; cnt >= 0; cnt--) 
	{
		if (canMessage->dlc <= cnt)
		{
			sprintf_s(RawHEX_, 6, "     ");
		}
		else
		{
			sprintf_s(RawHEX_, 6, "0x%02x ", canMessage->data[cnt]);
		}
		//strcat(RawHEX, RawHEX_);
		//strcat_s(RawHEX, 40, RawHEX_);
		strcat_s(RawHEX, sizeof(RawHEX), RawHEX_);
	};


	if (canHeadPtr->fragment)
	{
		fragHeadPtr = &canMessage->data[0];
		wsprintf(FragInfo, "Frag=%02x s=%d:e=%d", fragHeadPtr->id, fragHeadPtr->start, fragHeadPtr->stop);
	}
	else
	{               
		wsprintf(FragInfo,"Non-Fragmented ");
	}

	wsprintf(DecodedOutput,
             "%s  %s  %s[%u] %s[%u] [%s] %s %s %s   n=%u",
             FormatedTime, DirStr,  
			 SrcDevice, canHeadPtr->deviceIDSrc, 
			 DstDevice, canHeadPtr->deviceIDDst,
			 //ResolveModuleType(canHeadPtr->deviceSrc), canHeadPtr->deviceIDSrc, 
			 //ResolveModuleType(canHeadPtr->deviceDst), canHeadPtr->deviceIDDst,
			 RawHEX,
			 MsgType, 
			 DecodedData,
			 FragInfo,
			 canMessage->number
			 );


}


//===========================================================================
unsigned long CKAPI CanConfigure(void)
{
  return TRUE;
}


//===========================================================================
unsigned long CKAPI Configure(HWND h)
{
	lu_char_t		*FileName = "filters.ini";
	
	if (MsgTypeFilterPtr != 0)
	{
		if (InitilaizeFilters(FileName, SRCModuleFilterPtr, MsgTypeFilterPtr, &DecodeFormat) == 0)
		{
			FiltersActive = 1;
			return (MessageBox(h, "Filters.ini loaded", "Caption", MB_OK) == IDOK);
		}
		else
		{
			return (MessageBox(h, "No Filter file found", "Caption", MB_OK) == IDOK);
		}
	}
	else
	{
		return (MessageBox(h, "Filter memory not Allocated", "Caption", MB_OK) == IDOK);
	}

	//return (MessageBox(h, "MsgBox text", "Caption", MB_OK) == IDOK);

}


//===========================================================================
void CKAPI ExplainCurrentConfig(char *buf, unsigned long bufsiz)
{
  strncpy(buf, "Not much to explain", bufsiz);
}


//===========================================================================
void CKAPI GetHeaderString(char *buf, unsigned long bufsiz)
{
  strncpy(buf, "   Time     Dir  SRC    DST    Raw HEX value                             TYPE   MSG_ID", bufsiz);
}


//===========================================================================
void CKAPI ExecuteVerb(int verb)
{
}


//===========================================================================
void CKAPI SaveState(void *buf, unsigned long *buflen)
{
  *buflen = 0;
}


//===========================================================================
void CKAPI RestoreState(void *buf, unsigned long buflen)
{
}


//===========================================================================
//
// The DLL Entry Point.
// 
BOOL WINAPI
#ifdef __BORLANDC__
   DllEntryPoint
#else
   DllMain
#endif
   (HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{

  switch(fdwReason) {
    case DLL_PROCESS_ATTACH:
		//MessageBox(NULL, "DLL Load", "Caption", MB_OK);
		MsgTypeFilterPtr = (MsgFilterStr *) calloc(CANMsgFilterMsgTypeSize, sizeof(MsgFilterStr));
		SRCModuleFilterPtr = (FilterStr *) calloc(CANMsgFilterSRCModuleSize, sizeof(FilterStr));
		
		//MsgTypeFilterPtr[0].filter = 0x31;
		//MsgTypeFilterPtr[1].filter = 0x35;
		break;
    case DLL_THREAD_ATTACH:
		//MessageBox(NULL, "Theread Attach", "Caption", MB_OK);
		break;
    case DLL_THREAD_DETACH:
		//MessageBox(NULL, "Theread Detach", "Caption", MB_OK);
		break;
	case DLL_PROCESS_DETACH:
		//MessageBox(NULL, "DLL Unload", "Caption", MB_OK);
		
		if (MsgTypeFilterPtr != 0)
		{
			free(MsgTypeFilterPtr);
			MsgTypeFilterPtr = 0;
		}
		if (SRCModuleFilterPtr != 0)
		{
			free(SRCModuleFilterPtr);
			SRCModuleFilterPtr = 0;
		}

		break;
  }
  return TRUE;
}



