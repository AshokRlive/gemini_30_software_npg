/*
**                   Copyright 2007 by KVASER AB, SWEDEN      
**                        WWW: http://www.kvaser.com
**
** This software is furnished under a license and may be used and copied
** only in accordance with the terms of such license.
**
** Description:
**   Header file for DLL formatters for Kvaser CanKing.
**   
*/
#ifndef _FORMDLL_H_
#define _FORMDLL_H_

#include <windows.h>

#include "lu_types.h"
#include "CANMsgFilters.h"


#define CKAPI __stdcall

#pragma pack(1)
typedef struct {
  unsigned long channel;  // The CAN channel (0,1,2,..)  
  unsigned long id;       // The CAN identifier
  unsigned long dlc;      // The data length code, 0..15
  unsigned long flags;    // message flags (canMSG_xxx from CANLIB)
  __int64  time;          // timestamp, 64 bits; unit is nanoseconds
  unsigned long dir;      // TKFMessageDirection, 0=tx, 1=rx 
  unsigned long number;   // "serial no"
  unsigned char data[8];  // Message data.
} TCanMessage;
#pragma pack()

// Note: (February, 2007) - the "channel" argument is not currently
// used by CanKing as available on the web. It will be used in a (not
// too) future release.


//
// MF_xxx: Return codes of FormatMsg().
//

// The DLL produced a string.
#define MF_USE_RESULT   0x01

// For filters: stop processing of the other formatters.
#define MF_SKIP_REST    0x02

// CanKing calls immediately FormatMsg() again to get a second,
// third, ... string.
#define MF_COME_AGAIN   0x04


// VERB_START is executed after the Start method when the main program thinks
// it's time to start to run (probably someone pressed the Run button)
#define VERB_START      1

// Broadcast when it's time top stop run.
#define VERB_STOP       2

// Used to "clear" things whatever this means (it's plugin defined.)
#define VERB_CLEAR      3


//
// Message types; used when FormatMsg() is called.
//
#define CKMSG_CAN_MESSAGE     1   // A TCanMessage



#ifdef __cplusplus
extern "C" {
#endif

// Called when the DLL is loaded.
void CKAPI Create(void);
//lu_uint32_t * CKAPI Create(void);
//MsgFilterStr * CKAPI Create(void);

// Called when the DLL is unloaded.
void CKAPI Destroy(void);

// Called to format one message. Returns a combination of the MF_xxx flags.
unsigned long CKAPI FormatMsg(void *msg, int messageType,
                              char *buf, unsigned long bufsiz);

// Returns TRUE if the DLL provides GUI configuration.
unsigned long CKAPI CanConfigure(void);

// Called when GUI configuration is to be done. OK = true CANCEL = false
// h is the handle of the parent window.
unsigned long CKAPI Configure(HWND h);

// Returns a string that explains the current configuration to the user.
void CKAPI ExplainCurrentConfig(char *buf, unsigned long bufsiz);

// Returns the header string to be used by e.g. the output window.
void CKAPI GetHeaderString(char *buf, unsigned long bufsiz);

// Execute a program-wide verb (any of VERB_xxx)
void CKAPI ExecuteVerb(int verb);

// Called to save the state of the DLL to the configuration file. Place the
// information in the buffer;
// buflen is set to the buffer's maximum length at the entry and you are responsible
// for setting it to the actual length of your data.
void CKAPI SaveState(void *buf, unsigned long *buflen);

// Called to restore the state of the DLL. buf points to a buffer containing the
// data you stored in the implementation of the SaveState API.
void CKAPI RestoreState(void *buf, unsigned long buflen);

void CKAPI DebugFilters(MsgTypeFilterOut, SRCModuleFilterOut, DecodeFormatOut, FiltersActiveOut);

void GenerateOutput (DecodedOutput, canMessage, canHeadPtr, DecodeFormatPtr);


#ifdef __cplusplus
}
#endif

#endif
