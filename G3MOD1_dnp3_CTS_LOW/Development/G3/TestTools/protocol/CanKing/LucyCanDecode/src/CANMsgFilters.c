/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Decoder functions 
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/10/11      krauklis_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <windows.h>
#include <stdio.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CANProtocol.h"
#include "ModuleProtocol.h"
#include "CANMsgTables.h"
#include "CANMsgFilters.h"
#include "CANMsgDecoder.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */





/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


lu_uint8_t InitilaizeFilters (lu_char_t *FileName, FilterStr *SRCModuleFilter, MsgFilterStr *MsgTypeFilter, lu_uint8_t *DecodeFormat)
{
	FILE			*stream;
	lu_char_t		FilterData[CANMsgFilterFileSize] = "";
	lu_char_t		*token;
	lu_uint8_t		cnt;
	lu_uint8_t		cnt2;

	
	if (fopen_s(&stream, FileName, "r") > 0)
	{
		printf("File %s not found\n", FileName);
		return 1;
	}
	else
	{
		fread(FilterData, 1, (CANMsgFilterFileSize - 1), stream);

		*DecodeFormat = CANMsgDecodeFormatDEC;

		token = strtok(FilterData, ",\t\n");		//Initialize token search
		while(token != NULL)
		{
		  
			if ((strstr(token, "//") == 0) && (strstr(token, "[") == 0))
			{
				
				if (strstr(token, "DECODE_HEX") != 0)
				{
					*DecodeFormat = CANMsgDecodeFormatHEX;
				}




				if (strstr(token, "MODULE_") != 0)
				{
					//printf( "%s\n", token);
					if (strstr(token, "MODULE_ALL") != 0)
					{
						for (cnt = 0; cnt < CANMsgFilterSRCModuleSize; cnt++)
						{
							SRCModuleFilter[cnt].filter = 1;
						}
					}

					cnt = 0;
					while (CAN_MODULE_TYPE_Table[cnt].msgTypeValue != END_OF_TABLE)
					{
						if (strstr(token, CAN_MODULE_TYPE_Table[cnt].msgTypePtr) != 0)
						{
							SRCModuleFilter[CAN_MODULE_TYPE_Table[cnt].msgTypeValue].filter = 1;
							//printf("0x%02x\t%s\n", CAN_MODULE_TYPE_Table[cnt].msgTypeValue, CAN_MODULE_TYPE_Table[cnt].msgTypePtr);
							break;
						}
						cnt++;
					} 
				}




/*
				if (strstr(token, "MSG_TYPE_") != 0)
				{
					//printf("%s\n", token);
					if (strstr(token, "ALL") != 0)
					{

						for (cnt = 0; cnt < CANMsgFilterMsgTypeSize; cnt++)
						{
							MsgTypeFilter[cnt].filter = 1;
							for (cnt2 = 0; cnt2 < CANMsgFilterMsgIDSize; cnt2++)
							{
								MsgTypeFilter[cnt].IDs[cnt2] = 1;
							}
						}
					}
				  

					cnt = 0;
					while (CAN_MSG_TYPE_Table[cnt].msgTypeValue != END_OF_TABLE)
					{
						if (strstr(token, CAN_MSG_TYPE_Table[cnt].msgTypePtr) != 0)
						{
							MsgTypeFilter[CAN_MSG_TYPE_Table[cnt].msgTypeValue].filter = 1;
							//printf("0x%02x\t%s\n", CAN_MSG_TYPE_Table[cnt].msgTypeValue, CAN_MSG_TYPE_Table[cnt].msgTypePtr);
							break;
						}
						cnt++;
					} 
				}
*/




				if (strstr(token, "MSG_ID_") != 0)
				{
					//printf("%s\n", token);
					if (strstr(token, "MSG_ID_ALL") != 0)
					{
						for (cnt = 0; cnt < CANMsgFilterMsgTypeSize; cnt++)
						{
							MsgTypeFilter[cnt].filter = 1;
							for (cnt2 = 0; cnt2 < CANMsgFilterMsgIDSize; cnt2++)
							{
								MsgTypeFilter[cnt].IDs[cnt2] = 1;
							}
						}

					}



					cnt = 0;
					while (CAN_MSG_TYPE_Table[cnt].msgTypeValue != END_OF_TABLE)
					{
						if (strstr(token, CAN_MSG_TYPE_Table[cnt].msgTypePtr) != 0)
						{
							MsgTypeFilter[CAN_MSG_TYPE_Table[cnt].msgTypeValue].filter = 1;
							//printf("0x%02x\t%s\n", CAN_MSG_TYPE_Table[cnt].msgTypeValue, CAN_MSG_TYPE_Table[cnt].msgTypePtr);
							
							if (strstr(token, "ALL") != 0)
							{
								for (cnt2 = 0; cnt2 < CANMsgFilterMsgIDSize; cnt2++)
								{
									MsgTypeFilter[CAN_MSG_TYPE_Table[cnt].msgTypeValue].IDs[cnt2] = 1;
								}
							}


							switch (CAN_MSG_TYPE_Table[cnt].msgTypeValue)
							{

							case MODULE_MSG_TYPE_HPRIO :
								CANMsgFilterSearchID (CAN_MSG_HPRIO_ID_Table, token, MsgTypeFilter, CAN_MSG_TYPE_Table[cnt].msgTypeValue);
								break;


							case MODULE_MSG_TYPE_EVENT :
								CANMsgFilterSearchID (CAN_MSG_EVENT_ID_Table, token, MsgTypeFilter, CAN_MSG_TYPE_Table[cnt].msgTypeValue);
								break;

							case MODULE_MSG_TYPE_HMI_EVENT:
								CANMsgFilterSearchID (CAN_MSG_HMI_EVENT_ID_Table, token, MsgTypeFilter, CAN_MSG_TYPE_Table[cnt].msgTypeValue);
								break;

							case MODULE_MSG_TYPE_CMD :
								CANMsgFilterSearchID (CAN_MSG_CMD_ID_Table, token, MsgTypeFilter, CAN_MSG_TYPE_Table[cnt].msgTypeValue);
								break;


							case MODULE_MSG_TYPE_CFG :
								CANMsgFilterSearchID (CAN_MSG_CFG_ID_Table, token, MsgTypeFilter, CAN_MSG_TYPE_Table[cnt].msgTypeValue);
								break;


							case MODULE_MSG_TYPE_IOMAN :
								CANMsgFilterSearchID (CAN_MSG_IOMAN_ID_Table, token, MsgTypeFilter, CAN_MSG_TYPE_Table[cnt].msgTypeValue);
								break;


							case MODULE_MSG_TYPE_BLTST :
								CANMsgFilterSearchID (CAN_MSG_BLTST_ID_Table, token, MsgTypeFilter, CAN_MSG_TYPE_Table[cnt].msgTypeValue);
								break;


							case MODULE_MSG_TYPE_LPRIO :
								CANMsgFilterSearchID (CAN_MSG_LPRIO_ID_Table, token, MsgTypeFilter, CAN_MSG_TYPE_Table[cnt].msgTypeValue);
								break;
							}
								

							break;
						}
						cnt++;
					} 

				}


			}
			token = strtok(NULL, ",\t\n");			//Resume token search
		}
	}
	_fcloseall();
	return 0;
} 








void CANMsgFilterSearchID (const CANMsgDecoderStr Table_name[], lu_char_t *token, MsgFilterStr *MsgTypeFilter, lu_uint8_t MsgType)
{
	lu_uint8_t	cnt = 0;
	
	while (Table_name[cnt].msgTypeValue != END_OF_TABLE)
	{
		if (strstr(token, Table_name[cnt].msgTypePtr) != 0)
		{
			MsgTypeFilter[MsgType].IDs[Table_name[cnt].msgTypeValue] = 1;
			//printf("0x%02x\t%s\t(Type=0x%02x)\n", Table_name[cnt].msgTypeValue, Table_name[cnt].msgTypePtr, MsgType);
			return;
		}
		cnt++;
	}
}




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
