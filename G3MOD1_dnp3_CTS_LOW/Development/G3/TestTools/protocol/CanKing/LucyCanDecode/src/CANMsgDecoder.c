/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Decoder functions 
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26/09/11      krauklis_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <windows.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CANProtocol.h"
#include "ModuleProtocol.h"
#include "CANMsgTables.h"
#include "CANMsgDecoder.h"



/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

//#define		INVALID_DATA_LENGTH {wsprintf(CANDecodeString, "!Invalid Data Length!");}
#define			INVALID_DATA_LENGTH {strcat(outputstring, "!Invalid Data Length!");}
//#define		OUTPUT_FORMATER2 (stringDEC, var1, var2) {wsprintf(CANDecodeString, " ")}
//#define		OUTPUT_FORMATER2 (stringDEC) {wsprintf(CANDecodeString, " ")}
//#define		OUTPUT_FORMATER2(stringDEC) {stringDEC}

//lu_char_t	*stringDEC;
//lu_uint32_t	var1;
//lu_uint32_t	var2;
lu_char_t	*teststr = "test";

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */




/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


void ResolveModuleType (lu_uint32_t DevTypeValue, lu_char_t *outputstring)
{
	lu_uint8_t cnt;

	cnt = 0;
	do 
	{
		if (CAN_MODULE_TYPE_Table[cnt].msgTypeValue == DevTypeValue)
		{
			wsprintf(outputstring, "%s", CAN_MODULE_TYPE_Table[cnt].msgTypePtr);
			//strcpy(outputstring, CAN_MODULE_TYPE_Table[cnt].msgTypePtr);
			break;
		}
		
		cnt++;
		
		if (CAN_MODULE_TYPE_Table[cnt].msgTypeValue == END_OF_TABLE)
		{
			wsprintf(outputstring, "0x%02x", DevTypeValue);
			//strcpy(outputstring, FormatCANOutputByte(DevTypeValue, CANMsgDecodeFormatHEX));
		}

	} while (CAN_MODULE_TYPE_Table[cnt].msgTypeValue != END_OF_TABLE);

	return;
}


void ResolveMsg (CANHeaderStr *canHeadPtr, lu_char_t *outputstring)
{
	lu_uint8_t	cnt;
	lu_char_t	ValueString[CANMsgDecodedFormatLen] = "";
	lu_uint32_t MsgValue;
	lu_uint32_t MsgID;

	MsgID    = canHeadPtr->messageID;
	MsgValue = canHeadPtr->messageType;

	cnt = 0;
	while (CAN_MSG_TYPE_Table[cnt].msgTypeValue != END_OF_TABLE) 
	{
		if (CAN_MSG_TYPE_Table[cnt].msgTypeValue == MsgValue)
		{
			strcpy(outputstring, CAN_MSG_TYPE_Table[cnt].msgTypePtr);
			strcat(outputstring, " ");
			break;
		}
		
		cnt++;
		
		if (CAN_MSG_TYPE_Table[cnt].msgTypeValue == END_OF_TABLE)
		{
			strcpy(outputstring, "TYPE=");
			FormatCANOutputByte(MsgValue, ValueString, CANMsgDecodeFormatHEX);
			strcat(outputstring, ValueString);
		}
	} 

	switch (MsgValue)
	{
		case MODULE_MSG_TYPE_HPRIO :
			CANMsgDecoderSearchID(CAN_MSG_HPRIO_ID_Table, MsgID, outputstring);
			break;

		case MODULE_MSG_TYPE_EVENT :
			CANMsgDecoderSearchID(CAN_MSG_EVENT_ID_Table, MsgID, outputstring);
			break;
		
		case MODULE_MSG_TYPE_HMI_EVENT:
			CANMsgDecoderSearchID(CAN_MSG_HMI_EVENT_ID_Table, MsgID, outputstring);
			break;

		case MODULE_MSG_TYPE_MD_CMD:
			CANMsgDecoderSearchID(CAN_MSG_MD_CMD_ID_Table, MsgID, outputstring);
			break;

		case MODULE_MSG_TYPE_CMD :
			CANMsgDecoderSearchID(CAN_MSG_CMD_ID_Table, MsgID, outputstring);
			break;

		case MODULE_MSG_TYPE_CFG :
			CANMsgDecoderSearchID(CAN_MSG_CFG_ID_Table, MsgID, outputstring);
			break;

		case MODULE_MSG_TYPE_IOMAN :
			CANMsgDecoderSearchID(CAN_MSG_IOMAN_ID_Table, MsgID, outputstring);
			break;

		case MODULE_MSG_TYPE_BLTST :
			CANMsgDecoderSearchID(CAN_MSG_BLTST_ID_Table, MsgID, outputstring);
			break;

		case MODULE_MSG_TYPE_BLTST_1:
			CANMsgDecoderSearchID(CAN_MSG_BLTST_1_ID_Table, MsgID, outputstring);
			break;

		case MODULE_MSG_TYPE_CALTST:
			CANMsgDecoderSearchID(CAN_MSG_CALTST_ID_Table, MsgID, outputstring);
			break;

		case MODULE_MSG_TYPE_NVRAM :
			CANMsgDecoderSearchID(CAN_MSG_NVRAM_ID_Table, MsgID, outputstring);
			break;

		case MODULE_MSG_TYPE_LPRIO :
			CANMsgDecoderSearchID(CAN_MSG_LPRIO_ID_Table, MsgID, outputstring);
			break;

		default : 
			strcat(outputstring, "MSGID=");
			FormatCANOutputByte(MsgID, ValueString, CANMsgDecodeFormatHEX);
			strcat(outputstring, ValueString);
	}
	return;
}






void CANMsgDecoderSearchID (const CANMsgDecoderStr Table_name[], lu_uint32_t MsgID, lu_char_t* outputstring)
{
	lu_uint8_t	cnt = 0;
	lu_char_t	ValueString[CANMsgDecodedFormatLen] = "";

		do
		{
			if (Table_name[cnt].msgTypeValue == MsgID)
			{
				//wsprintf(outputstring, "%s", Table_name[cnt].msgTypePtr);
				strcat(outputstring, Table_name[cnt].msgTypePtr);
				strcat(outputstring, " ");
				break;
			}
			
			cnt ++;
			
			if (Table_name[cnt].msgTypeValue == END_OF_TABLE)
			{
				//wsprintf(outputstring, "MSGID=0x%02x", MsgID);
				strcat(outputstring, "MSGID=");
				//strcat(outputstring, FormatCANOutputByte(MsgID, CANMsgDecodeFormatHEX));
				FormatCANOutputByte(MsgID, ValueString, CANMsgDecodeFormatHEX);
				strcat(outputstring, ValueString);
			}
		
		} while (Table_name[cnt].msgTypeValue != END_OF_TABLE) ;

	return;
}

void DecodeCANdata (CANHeaderStr *canHeadPtr, lu_uint8_t *CANdata, lu_uint8_t MsgLength, lu_char_t *outputstring, lu_uint8_t DecodeFormat)
{
	lu_uint8_t				cnt = 0;
	lu_char_t				ValueString[CANMsgDecodedFormatLen] = "";
	lu_uint32_t MsgValue;
	lu_uint32_t MsgID;

	/*! HIPRO */
	ModuleTsynchStr         *ModuleTsynchPtr;

	/*! Structures for EVENT type CAN messages */
	ModuleDEventStr			*ModuleDEventPtr;
	ModuleAEventStr			*ModuleAEventPtr;
	ModuleTimeStr			*ModuleTimePtr;

	/*! Structures for CMD type Battery Charger messages */
	BatterySelectStr		*BatterySelectPtr;
	BatteryOperateStr 	    *BatteryOperatePtr;
	BatteryCancelStr		*BatteryCancelPtr;
	ModReplyStr				*ModReplyPtr;
	
	/*! Structures for CMD type Power Supply messages */
	SupplySelectStr			*SupplySelectPtr;
	SupplyOperateStr		*SupplyOperatePtr;
	SupplyCancelStr			*SupplyCancelPtr;
	
	/*! Structures for CMD type FAN Controller messages */
	FanTestStr				*FanTestPtr;
	
	/*! Structures for CMD type POLL messages */
	ModulePollChanIdStr		*ModulePollChanIdPtr;
	ModuleAIPollRspStr		*ModuleAIPollRspPtr;
	ModuleDIPollRspStr		*ModuleDIPollRspPtr;

	/*! Structures for CMD type DIAG messages */
	ModuleReadIoIdStr		*ModuleReadIoIdPtr;
	ModuleReadIoIdRspStr	*ModuleReadIoIdRspPtr;
	ADE7878ReadRspStr       *ADE7878ReadRspPtr;
	ModuleWriteIoIdStr		*ModuleWriteIoIdPtr;

	ModuleCfgIoChanAIStr    *ModuleCfgIoChanAIPtr;
	ModuleCfgIoChanDIStr    *ModuleCfgIoChanDIPtr;

	/*! Structures for BLTST type GPIO messages */
	TestGPIOPinPortStr		*TestGPIOPinPortPtr;
	TestGPIOModeStr			*TestGPIOModePtr;
	TestGPIOReadStr			*TestGPIOReadPtr;
	TestGPIOWriteStr		*TestGPIOWritePtr;
	TestGPIOPortStr			*TestGPIOPortPtr;
	TestGPIOPortReadStr     *TestGPIOPortReadPtr;
	TestGPIOPortWriteStr    *TestGPIOPortWritePtr;
	
	/*! Structures for BLTST type ADC messages */
	TestADCPinChanStr		*TestADCPinChanPtr;
	TestADCReadStr			*TestADCReadPtr;
	
	/*! Structures for BLTST type DIGIPOT messages */
	TestSPIDigiPotAddrStr	*TestSPIDigiPotAddrPtr;
	TestSPIDigiPotWriteStr	*TestSPIDigiPotWritePtr;
	TestSPIDigiPotWriteRspStr *TestSPIDigiPotWriteRspPtr;
	TestSPIDigiPotReadStr	*TestSPIDigiPotReadPtr;
	TestSPIDigiPotReadRspStr *TestSPIDigiPotReadRspPtr;
	
	/*! Structures for BLTST type NVRAM messages */
	TestNVRAMAddrStr		*TestNVRAMAddrPtr;
	TestNVRAMReadStr		*TestNVRAMReadPtr;
	TestNVRAMReadRspStr		*TestNVRAMReadRspPtr;
	TestNVRAMWriteStr		*TestNVRAMWritePtr;

	/*! Structures for BLTST_1 type ADE78xx messages */
//	TestADE78xxSPIEnableStr   *TestADE78xxSPIEnablePtr;
//	TestADE78xxSPIReadRegStr  *TestADE78xxSPIReadRegPtr;
//	TestADE78xxSPIWriteRegStr *TestADE78xxSPIWriteRegPtr;

	/*! LPRO */
	ModuleHBeatSStr			  *ModuleHBeatSPtr;
	ModuleInfoStr			  *ModuleInfoPtr;
//	ModuleTimeStr			  *ModuleTimePtr;

	MsgID    = canHeadPtr->messageID;
	MsgValue = canHeadPtr->messageType;

	switch (MsgValue)
	{
		case MODULE_MSG_TYPE_HPRIO :
			
			switch (MsgID)
			{
			
			case MODULE_MSG_ID_HPRIO_RESERVED :				/* No structure associated with this command */
				strcpy(outputstring, "0");
				break;


			case MODULE_MSG_ID_HPRIO_TSYNCH :
				
				if (MsgLength == sizeof(ModuleTsynchStr))
				{
					ModuleTsynchPtr = (ModuleTsynchStr *) CANdata;

					strcat(outputstring, " TimeSlot=");
					FormatCANOutputByte(ModuleTsynchPtr->timeSlot, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
				}
				else	INVALID_DATA_LENGTH
				break;

			default : 
					strcpy(outputstring, "---");

			}
			break;


		case MODULE_MSG_TYPE_EVENT :
		
			switch (MsgID)
			{
		
				case MODULE_MSG_ID_EVENT_FPI :				/* No structure associated with this command */
					strcpy(outputstring, "0");
					break;

				case MODULE_MSG_ID_EVENT_DIGITAL :

					if (MsgLength == sizeof(ModuleDEventStr))
					{
						ModuleDEventPtr = (ModuleDEventStr *) CANdata;
						ModuleTimePtr = &ModuleDEventPtr->time;
						//strcpy(outputstring, "Chan=");
						switch (canHeadPtr->deviceSrc)
						{
						case MODULE_PSM:
							FormatChannel(CAN_MSG_PSM_CH_DINPUT, ModuleDEventPtr->channel, outputstring, ValueString, DecodeFormat);
							break;

						case MODULE_SCM:
							FormatChannel(CAN_MSG_SCM_CH_DINPUT, ModuleDEventPtr->channel, outputstring, ValueString, DecodeFormat);
							break;

						case MODULE_FPM:
							FormatChannel(CAN_MSG_FPM_CH_DINPUT, ModuleDEventPtr->channel, outputstring, ValueString, DecodeFormat);
							break;

						default:
							strcat(outputstring, " Chan=");
							FormatCANOutputByte(ModuleDEventPtr->channel, ValueString, DecodeFormat);
							strcat(outputstring, ValueString);
							break;
						}
						
						ModuleDEventPtr = (ModuleDEventStr *) CANdata;
						strcat(outputstring, "Val=");
						FormatCANOutputByte(ModuleDEventPtr->value, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, " TimeSlot=");
						FormatCANOutputByte(ModuleTimePtr->slot, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Time=");
						FormatCANOutputWord(ModuleTimePtr->time, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
						break;


				case MODULE_MSG_ID_EVENT_ANALOGUE : 
					
					if (MsgLength == sizeof(ModuleAEventStr))
					{
						ModuleAEventPtr = (ModuleAEventStr *) CANdata;
						ModuleTimePtr = &ModuleAEventPtr->time;
						//strcpy(outputstring, "Chan=");

						switch (canHeadPtr->deviceSrc)
						{
						case MODULE_PSM:
							FormatChannel(CAN_MSG_PSM_CH_AINPUT, ModuleAEventPtr->channel, outputstring, ValueString, DecodeFormat);
							break;

						case MODULE_SCM:
							FormatChannel(CAN_MSG_SCM_CH_AINPUT, ModuleAEventPtr->channel, outputstring, ValueString, DecodeFormat);
							break;

						case MODULE_FDM:
							//FormatChannel(CAN_MSG_FDM_CH_AINPUT, ModuleAEventPtr->channel, outputstring, ValueString, DecodeFormat);
							break;

						case MODULE_HMI:
							FormatChannel(CAN_MSG_HMI_CH_AINPUT, ModuleAEventPtr->channel, outputstring, ValueString, DecodeFormat);
							break;

						case MODULE_IOM:
							FormatChannel(CAN_MSG_IOM_CH_AINPUT, ModuleAEventPtr->channel, outputstring, ValueString, DecodeFormat);
							break;
						
						case MODULE_DSM:
							FormatChannel(CAN_MSG_DSM_CH_AINPUT, ModuleAEventPtr->channel, outputstring, ValueString, DecodeFormat);
							break;

						case MODULE_FPM:
							FormatChannel(CAN_MSG_FPM_CH_AINPUT, ModuleAEventPtr->channel, outputstring, ValueString, DecodeFormat);
							break;

						default:
							strcat(outputstring, " Chan=");
							FormatCANOutputByte(ModuleAEventPtr->channel, ValueString, DecodeFormat);
							strcat(outputstring, ValueString);
							break;
						}
						
						strcat(outputstring, "Val=");
						FormatCANOutputDWord(ModuleAEventPtr->value, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, " TimeSlot=");
						FormatCANOutputByte(ModuleTimePtr->slot, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Time=");
						FormatCANOutputWord(ModuleTimePtr->time, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
						break;

				default : 
					strcpy(outputstring, "---");
			
			}
			break;
	


		case MODULE_MSG_TYPE_CMD :
		
			switch (MsgID)
			{
				case MODULE_MSG_ID_CMD_BAT_CH_SELECT_C :
					
					if (MsgLength == sizeof(BatterySelectStr))
					{
						BatterySelectPtr = (BatterySelectStr *) CANdata;
						strcpy(outputstring, "Chan=");
						//strcat(outputstring, FormatCANOutputByte(PSMBatterySelectPtr->channel, DecodeFormat));
						FormatCANOutputByte(BatterySelectPtr->channel, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);

						strcpy(outputstring, "Timeout=");
						FormatCANOutputByte(BatterySelectPtr->SelectTimeout, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);

						strcat(outputstring, "Local=");
						//strcat(outputstring, FormatCANOutputByte(PSMBatterySelectPtr->local, DecodeFormat));
						FormatCANOutputByte(BatterySelectPtr->local, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
						break;


				case MODULE_MSG_ID_CMD_BAT_CH_SELECT_R :
				case MODULE_MSG_ID_CMD_BAT_CH_OPERATE_R :
				case MODULE_MSG_ID_CMD_BAT_CH_CANCEL_R :
				case MODULE_MSG_ID_CMD_FAN_CH_TEST_R :
					
					if (MsgLength == sizeof(ModReplyStr))
					{
						ModReplyPtr = (ModReplyStr *) CANdata;
						strcpy(outputstring, "Chan=");
						//strcat(outputstring, FormatCANOutputByte(PSMReplyPtr->channel, DecodeFormat));
						FormatCANOutputByte(ModReplyPtr->channel, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);

						FormatStatus(CAN_MSG_REPLY_STATUS, ModReplyPtr->status, outputstring, ValueString, DecodeFormat);
					}	
					else	INVALID_DATA_LENGTH
						break;


				case MODULE_MSG_ID_CMD_BAT_CH_OPERATE_C :
					if (MsgLength == sizeof(BatteryOperateStr))
					{
						BatteryOperatePtr = (BatteryOperateStr *) CANdata;
						strcpy(outputstring, "Chan=");
						//strcat(outputstring, FormatCANOutputByte(PSMBatteryOperatePtr->channel, DecodeFormat));
						FormatCANOutputByte(BatteryOperatePtr->channel, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);

						strcpy(outputstring, "Duration=");
						FormatCANOutputByte(BatteryOperatePtr->operationDuration, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);

						strcat(outputstring, "Local=");
						//strcat(outputstring, FormatCANOutputByte(PSMBatteryOperatePtr->local, DecodeFormat));
						FormatCANOutputByte(BatteryOperatePtr->local, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
						break;


				case MODULE_MSG_ID_CMD_BAT_CH_CANCEL_C :
					if (MsgLength == sizeof(BatteryCancelStr))
					{
						BatteryCancelPtr = (BatteryCancelStr *) CANdata;
						strcpy(outputstring, "Chan=");
						//strcat(outputstring, FormatCANOutputByte(PSMBatteryCancelPtr->channel, DecodeFormat));
						FormatCANOutputByte(BatteryCancelPtr->channel, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Local=");
						//strcat(outputstring, FormatCANOutputByte(PSMBatteryCancelPtr->local, DecodeFormat));
						FormatCANOutputByte(BatteryCancelPtr->local, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
						break;

				
				case MODULE_MSG_ID_CMD_PSC_CH_SELECT_C :
					if (MsgLength == sizeof(SupplySelectStr))
					{
						SupplySelectPtr = (SupplySelectStr *) CANdata;
						//strcpy(outputstring, "Chan=");
						FormatChannel(CAN_MSG_PSM_CH_PSUPPLY, SupplySelectPtr->channel, outputstring, ValueString, DecodeFormat);
						strcat(outputstring, "SelTimeout=");
						FormatCANOutputByte(SupplySelectPtr->SelectTimeout, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Local=");
						FormatCANOutputByte(SupplySelectPtr->local, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
						break;


				case MODULE_MSG_ID_CMD_PSC_CH_SELECT_R :
				case MODULE_MSG_ID_CMD_PSC_CH_OPERATE_R :
				case MODULE_MSG_ID_CMD_PSC_CH_CANCEL_R :
					
					if (MsgLength == sizeof(ModReplyStr))
					{
						ModReplyPtr = (ModReplyStr *) CANdata;
						FormatChannel(CAN_MSG_PSM_CH_PSUPPLY, ModReplyPtr->channel, outputstring, ValueString, DecodeFormat);
						FormatStatus(CAN_MSG_REPLY_STATUS, ModReplyPtr->status, outputstring, ValueString, DecodeFormat);
					}	
					else	INVALID_DATA_LENGTH
						break;

				
				case MODULE_MSG_ID_CMD_PSC_CH_OPERATE_C :
					if (MsgLength == sizeof(SupplyOperateStr))
					{
						SupplyOperatePtr = (SupplyOperateStr *) CANdata;
						//strcpy(outputstring, "Chan=");
						FormatChannel(CAN_MSG_PSM_CH_PSUPPLY, SupplyOperatePtr->channel, outputstring, ValueString, DecodeFormat);
						strcat(outputstring, "Duration=");
						FormatCANOutputByte(SupplyOperatePtr->supplyDuration, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "ILimit=");
						FormatCANOutputDWord(SupplyOperatePtr->currentLimit, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Local=");
						FormatCANOutputByte(SupplyOperatePtr->local, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
						break;

					
				case MODULE_MSG_ID_CMD_PSC_CH_CANCEL_C :
					if (MsgLength == sizeof(SupplyCancelStr))
					{
						SupplyCancelPtr = (SupplyCancelStr *) CANdata;
						//strcpy(outputstring, "Chan=");
						FormatChannel(CAN_MSG_PSM_CH_PSUPPLY, SupplyCancelPtr->channel, outputstring, ValueString, DecodeFormat);
						strcat(outputstring, "Local=");
						FormatCANOutputByte(SupplyCancelPtr->local, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
						break;
			
				
				case MODULE_MSG_ID_CMD_FAN_CH_TEST_C :
					if (MsgLength == sizeof(FanTestStr))
					{
						FanTestPtr = (FanTestStr *) CANdata;
						strcpy(outputstring, "Chan=");
						FormatCANOutputByte(FanTestPtr->channel, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Duration=");
						FormatCANOutputByte(FanTestPtr->testDuration, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
						break;


				default : 
					strcpy(outputstring, "---");

			}
			break;

		case MODULE_MSG_TYPE_IOMAN :
		
			switch (MsgID)
			{
				
			case MODULE_MSG_ID_IOMAN_CFG_IOCHAN_AI_C:
				if (MsgLength == sizeof(ModuleCfgIoChanAIStr))
				{
					ModuleCfgIoChanAIPtr = (ModuleCfgIoChanAIStr *)CANdata;

					strcpy(outputstring, "Chan=");
					FormatCANOutputByte(ModuleCfgIoChanAIPtr->channel, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, " Enable=");
					FormatCANOutputByte(ModuleCfgIoChanAIPtr->enable, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, " EventEnable=");
					FormatCANOutputByte(ModuleCfgIoChanAIPtr->eventEnable, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, " EventMs=");
					FormatCANOutputByte(ModuleCfgIoChanAIPtr->eventMs, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
				}
				else	INVALID_DATA_LENGTH
				break;

			case MODULE_MSG_ID_IOMAN_CFG_IOCHAN_DI_C:
				if (MsgLength == sizeof(ModuleCfgIoChanDIStr))
				{
					ModuleCfgIoChanDIPtr = (ModuleCfgIoChanDIStr *)CANdata;

					strcpy(outputstring, "Chan=");
					FormatCANOutputByte(ModuleCfgIoChanDIPtr->channel, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, " dbHigh2LowMs=");
					FormatCANOutputByte(ModuleCfgIoChanDIPtr->dbHigh2LowMs, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, " dbLow2HighMs=");
					FormatCANOutputByte(ModuleCfgIoChanDIPtr->dbLow2HighMs, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, " Enable=");
					FormatCANOutputByte(ModuleCfgIoChanDIPtr->enable, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, " eventEnable=");
					FormatCANOutputByte(ModuleCfgIoChanDIPtr->eventEnable, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, " extEquipInvert=");
					FormatCANOutputByte(ModuleCfgIoChanDIPtr->extEquipInvert, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
				}
				else	INVALID_DATA_LENGTH
				break;

			case MODULE_MSG_ID_IOMAN_CFG_IOCHAN_AI_R:
			case MODULE_MSG_ID_IOMAN_CFG_IOCHAN_DI_R:
				if (MsgLength == sizeof(ModReplyStr))
				{
					ModReplyPtr = (ModReplyStr *)CANdata;

					strcpy(outputstring, "Chan=");
					FormatCANOutputByte(ModReplyPtr->channel, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, " Status=");
					FormatCANOutputByte(ModReplyPtr->status, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
				}
				else	INVALID_DATA_LENGTH
				break;

			case MODULE_MSG_ID_IOMAN_CMD_POLL_AI_CHAN_C :
			case MODULE_MSG_ID_IOMAN_CMD_POLL_DI_CHAN_C :
				if (MsgLength == sizeof(ModulePollChanIdStr))
				{
					ModulePollChanIdPtr = (ModulePollChanIdStr *) CANdata;
					strcpy(outputstring, "Chan=");
					FormatCANOutputByte(ModulePollChanIdPtr->channel, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
				}
				else	INVALID_DATA_LENGTH
					break;
				

			case MODULE_MSG_ID_IOMAN_CMD_POLL_AI_CHAN_R :	
				if (MsgLength == sizeof(ModuleAIPollRspStr))
				{
					ModuleAIPollRspPtr = (ModuleAIPollRspStr *) CANdata;
					strcpy(outputstring, "Chan=");
					FormatCANOutputByte(ModuleAIPollRspPtr->channel, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, "Val=");
					FormatCANOutputDWord(ModuleAIPollRspPtr->value, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
				}
				else	INVALID_DATA_LENGTH
					break;

				
			case MODULE_MSG_ID_IOMAN_CMD_POLL_DI_CHAN_R :
				if (MsgLength == sizeof(ModuleDIPollRspStr))
				{
					ModuleDIPollRspPtr = (ModuleDIPollRspStr *) CANdata;
					strcpy(outputstring, "Chan=");
					FormatCANOutputByte(ModuleDIPollRspPtr->channel, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, "Val=");
					FormatCANOutputByte(ModuleDIPollRspPtr->value, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, "Raw=");
					FormatCANOutputByte(ModuleDIPollRspPtr->rawValue, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
				}
				else	INVALID_DATA_LENGTH
					break;


			case MODULE_MSG_ID_IOMAN_CMD_DIAG_READ_IOID_C :
				if (MsgLength == sizeof(ModuleReadIoIdStr))
				{
					ModuleReadIoIdPtr = (ModuleReadIoIdStr *) CANdata;
					strcpy(outputstring, "ioID=");
					FormatCANOutputWord(ModuleReadIoIdPtr->ioID, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
				}
				else	INVALID_DATA_LENGTH
					break;

				
			case MODULE_MSG_ID_IOMAN_CMD_DIAG_READ_IOID_R :
				if (MsgLength == sizeof(ModuleReadIoIdRspStr))
				{
					ModuleReadIoIdRspPtr = (ModuleReadIoIdRspStr *) CANdata;
					strcpy(outputstring, "Val=");
					FormatCANOutputDWord(ModuleReadIoIdRspPtr->value, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, "Raw=");
					FormatCANOutputDWord(ModuleReadIoIdRspPtr->rawValue, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
				}
				else	INVALID_DATA_LENGTH
					break;
			
//			case MODULE_MSG_ID_IOMAN_CMD_ADE7878_READ_REG_R :
//				if (MsgLength == sizeof(ADE7878ReadRspStr))
//				{
//					ADE7878ReadRspPtr = (ADE7878ReadRspStr*) CANdata;
//					strcpy(outputstring, "Val=");
//					FormatCANOutputDWord(ADE7878ReadRspPtr->value, ValueString, DecodeFormat);
//					strcat(outputstring, ValueString);
//					strcat(outputstring, "Error Code=");
//					FormatCANOutputDWord(ADE7878ReadRspPtr->errorCode, ValueString, DecodeFormat);
//					strcat(outputstring, ValueString);
//				}
//				else	INVALID_DATA_LENGTH
//					break;


								
			case MODULE_MSG_ID_IOMAN_CMD_DIAG_WRITE_IOID_C :
				if (MsgLength == sizeof(ModuleWriteIoIdStr))
				{
					ModuleWriteIoIdPtr = (ModuleWriteIoIdStr *) CANdata;
					strcpy(outputstring, "ioID=");
					FormatCANOutputWord(ModuleWriteIoIdPtr->ioID, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
					strcat(outputstring, "Val=");
					FormatCANOutputDWord(ModuleWriteIoIdPtr->value, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
				}
				else	INVALID_DATA_LENGTH
					break;


				
			case MODULE_MSG_ID_IOMAN_CMD_DIAG_WRITE_IOID_R :			/* No structure associated with this command */
				strcpy(outputstring, "0");
				break;
			
			
			default : 
				strcpy(outputstring, "---");
			
			}
			break;

		case MODULE_MSG_TYPE_BLTST_1 :
		
			switch (MsgID)
			{
//				case MODULE_MSG_ID_BLTST_1_ADE78xx_ENABLE_C :
//				if (MsgLength == sizeof(TestSpiADE78xxInitStr) - 6)
//					{
//						TestADE78xxSPIEnablePtr = (TestSpiADE78xxInitStr*) CANdata;
//						strcpy(outputstring, "sspBus=");
//						FormatCANOutputByte(TestADE78xxSPIEnablePtr->sspBus, ValueString, DecodeFormat);
//						strcat(outputstring, ValueString);
//						strcat(outputstring, "csPort=");
//						FormatCANOutputByte(TestADE78xxSPIEnablePtr->csPort, ValueString, DecodeFormat);
//						strcat(outputstring, ValueString);
//						strcat(outputstring, "csPin=");
//					}
//					else
//						INVALID_DATA_LENGTH
//						break;

				default:
					break;
			}
			break;

		case MODULE_MSG_TYPE_BLTST :
		
			switch (MsgID)
			{
				case MODULE_MSG_ID_BLTST_GPIO_GET_PIN_MODE_C :
				case MODULE_MSG_ID_BLTST_GPIO_READ_PIN_C :
					if (MsgLength == sizeof(TestGPIOPinPortStr))
					{
						TestGPIOPinPortPtr = (TestGPIOPinPortStr *) CANdata;
						strcpy(outputstring, "Port=");
						//strcat(outputstring, FormatCANOutputByte(TestGPIOPinPortPtr->port, DecodeFormat));
						FormatCANOutputByte(TestGPIOPinPortPtr->port, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Pin=");
						//strcat(outputstring, FormatCANOutputByte(TestGPIOPinPortPtr->pin, DecodeFormat));
						FormatCANOutputByte(TestGPIOPinPortPtr->pin, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
				break;


				case MODULE_MSG_ID_BLTST_GPIO_GET_PIN_MODE_R :
				case MODULE_MSG_ID_BLTST_GPIO_SET_PIN_MODE_C :
					if (MsgLength == sizeof(TestGPIOModeStr))
					{
						TestGPIOModePtr = (TestGPIOModeStr *) CANdata;
						strcpy(outputstring, "Port=");
						//strcat(outputstring, FormatCANOutputByte(TestGPIOModePtr->port, DecodeFormat));
						FormatCANOutputByte(TestGPIOModePtr->port, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Pin=");
						//strcat(outputstring, FormatCANOutputByte(TestGPIOModePtr->pin, DecodeFormat));
						FormatCANOutputByte(TestGPIOModePtr->pin, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Func=");
						//strcat(outputstring, FormatCANOutputByte(TestGPIOModePtr->pinFunc, DecodeFormat));
						FormatCANOutputByte(TestGPIOModePtr->pinFunc, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Mode=");
						//strcat(outputstring, FormatCANOutputByte(TestGPIOModePtr->pinMode, DecodeFormat));
						FormatCANOutputByte(TestGPIOModePtr->pinMode, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "OpnDrain=");
						//strcat(outputstring, FormatCANOutputByte(TestGPIOModePtr->openDrain, DecodeFormat));
						FormatCANOutputByte(TestGPIOModePtr->openDrain, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Dir=");
						//strcat(outputstring, FormatCANOutputByte(TestGPIOModePtr->dir, DecodeFormat));
						FormatCANOutputByte(TestGPIOModePtr->dir, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
				break;

				case MODULE_MSG_ID_BLTST_GPIO_READ_PORT_C :
					if (MsgLength == sizeof(TestGPIOPortStr))
					{
						TestGPIOPortPtr = (TestGPIOPortStr *) CANdata;
				
						strcpy(outputstring, "Port=");
						FormatCANOutputByte(TestGPIOPortPtr->port, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
					break;

				case MODULE_MSG_ID_BLTST_GPIO_READ_PORT_R :			
					if (MsgLength == sizeof(TestGPIOPortReadStr))
					{
						TestGPIOPortReadPtr  = (TestGPIOPortReadStr *) CANdata;

						strcpy(outputstring, "Value=");
						FormatCANOutputByte(TestGPIOPortReadPtr->value, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
					break;

				case MODULE_MSG_ID_BLTST_GPIO_WRITE_PORT_C :
					if (MsgLength == sizeof(TestGPIOPortWriteStr))
					{
						TestGPIOPortWritePtr  = (TestGPIOPortWriteStr *) CANdata;

						strcpy(outputstring, "Port=");
						FormatCANOutputByte(TestGPIOPortWritePtr->port, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);

						strcpy(outputstring, "Value=");
						FormatCANOutputByte(TestGPIOPortWritePtr->value, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
					break;

				

				case MODULE_MSG_ID_BLTST_GPIO_SET_PIN_MODE_R :		/* No structure associated with this command */
				case MODULE_MSG_ID_BLTST_GPIO_WRITE_PIN_R :			/* No structure associated with this command */
				case MODULE_MSG_ID_BLTST_GPIO_WRITE_PORT_R :		/* No structure associated with this command */
				case MODULE_MSG_ID_BLTST_I2C_ADC_MCP342X_READ_C :	/* No structure associated with this command */
				case MODULE_MSG_ID_BLTST_I2C_ADC_MCP342X_READ_R :	/* No structure associated with this command */

				case MODULE_MSG_ID_BLTST_NVRAM_WRITE_R :			/* No structure associated with this command */
					strcpy(outputstring, "0");
				break;


				case MODULE_MSG_ID_BLTST_GPIO_READ_PIN_R :
					if (MsgLength == sizeof(TestGPIOReadStr))
					{
						TestGPIOReadPtr = (TestGPIOReadStr *) CANdata;
						strcpy(outputstring, "Val=");
						//strcat(outputstring, FormatCANOutputByte(TestGPIOReadPtr->value, DecodeFormat));
						FormatCANOutputByte(TestGPIOReadPtr->value, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
				break;
					

				case MODULE_MSG_ID_BLTST_GPIO_WRITE_PIN_C :
					if (MsgLength == sizeof(TestGPIOWriteStr))
					{
						TestGPIOWritePtr = (TestGPIOWriteStr *) CANdata;
						strcpy(outputstring, "Port=");
						//strcat(outputstring, FormatCANOutputByte(TestGPIOWritePtr->port, DecodeFormat));
						FormatCANOutputByte(TestGPIOWritePtr->port, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Pin=");
						//strcat(outputstring, FormatCANOutputByte(TestGPIOWritePtr->pin, DecodeFormat));
						FormatCANOutputByte(TestGPIOWritePtr->pin, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Val=");
						//strcat(outputstring, FormatCANOutputByte(TestGPIOWritePtr->value, DecodeFormat));
						FormatCANOutputByte(TestGPIOWritePtr->value, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
				break;
				

				case MODULE_MSG_ID_BLTST_ADC_READ_C :
					if (MsgLength == sizeof(TestADCPinChanStr))
					{
						TestADCPinChanPtr = (TestADCPinChanStr *) CANdata;
						strcpy(outputstring, "Chan=");
						//strcat(outputstring, FormatCANOutputByte(TestADCPinChanPtr->chan, DecodeFormat));
						FormatCANOutputByte(TestADCPinChanPtr->chan, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "RatekHz=");
						//strcat(outputstring, FormatCANOutputByte(TestADCPinChanPtr->rateKhz, DecodeFormat));
						FormatCANOutputByte(TestADCPinChanPtr->rateKhz, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
				break;


				case MODULE_MSG_ID_BLTST_ADC_READ_R :
					if (MsgLength == sizeof(TestADCReadStr))
					{
						TestADCReadPtr = (TestADCReadStr *) CANdata;
						strcpy(outputstring, "Val=");
						FormatCANOutputWord(TestADCReadPtr->value, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
				break;


				case MODULE_MSG_ID_BLTST_SPI_DIGIPOT_WRITE_C :
					if (MsgLength == sizeof(TestSPIDigiPotWriteStr))
					{
						TestSPIDigiPotWritePtr = (TestSPIDigiPotWriteStr *) CANdata;
						TestSPIDigiPotAddrPtr = &TestSPIDigiPotWritePtr->addr;
						strcpy(outputstring, "sspChan=");
						//strcat(outputstring, FormatCANOutputByte(TestSPIDigiPotAddrPtr->sspChan, DecodeFormat));
						FormatCANOutputByte(TestSPIDigiPotAddrPtr->sspChan, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "csChan=");
						//strcat(outputstring, FormatCANOutputByte(TestSPIDigiPotAddrPtr->csChan, DecodeFormat));
						FormatCANOutputByte(TestSPIDigiPotAddrPtr->csChan, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "potChan=");
						//strcat(outputstring, FormatCANOutputByte(TestSPIDigiPotAddrPtr->potChan, DecodeFormat));
						FormatCANOutputByte(TestSPIDigiPotAddrPtr->potChan, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Val=");
						FormatCANOutputWord(TestSPIDigiPotWritePtr->potVal, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
				break;
					

				case MODULE_MSG_ID_BLTST_SPI_DIGIPOT_WRITE_R :
					if (MsgLength == sizeof(TestSPIDigiPotWriteRspStr))
					{
						TestSPIDigiPotWriteRspPtr = (TestSPIDigiPotWriteRspStr *) CANdata;
						strcpy(outputstring, "Val=");
						//strcat(outputstring, FormatCANOutputByte(TestSPIDigiPotWriteRspPtr->retVal, DecodeFormat));
						FormatCANOutputByte(TestSPIDigiPotWriteRspPtr->retVal, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
				break;


				case MODULE_MSG_ID_BLTST_SPI_DIGIPOT_READ_C :
					if (MsgLength == sizeof(TestSPIDigiPotReadStr))
					{
						TestSPIDigiPotReadPtr = (TestSPIDigiPotReadStr *) CANdata;
						TestSPIDigiPotAddrPtr = &TestSPIDigiPotReadPtr->addr;
						strcpy(outputstring, "sspChan=");
						//strcat(outputstring, FormatCANOutputByte(TestSPIDigiPotAddrPtr->sspChan, DecodeFormat));
						FormatCANOutputByte(TestSPIDigiPotAddrPtr->sspChan, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "csChan=");
						//strcat(outputstring, FormatCANOutputByte(TestSPIDigiPotAddrPtr->csChan, DecodeFormat));
						FormatCANOutputByte(TestSPIDigiPotAddrPtr->csChan, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "potChan=");
						//strcat(outputstring, FormatCANOutputByte(TestSPIDigiPotAddrPtr->potChan, DecodeFormat));
						FormatCANOutputByte(TestSPIDigiPotAddrPtr->potChan, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
				break;


				case MODULE_MSG_ID_BLTST_SPI_DIGIPOT_READ_R :
					if (MsgLength == sizeof(TestSPIDigiPotReadRspStr))
					{
						TestSPIDigiPotReadRspPtr = (TestSPIDigiPotReadRspStr *) CANdata;
						strcpy(outputstring, "Val=");
						FormatCANOutputWord(TestSPIDigiPotReadRspPtr->potVal, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
				break;


				case MODULE_MSG_ID_BLTST_NVRAM_READ_C :
					if (MsgLength == sizeof(TestNVRAMReadStr))
					{
						TestNVRAMReadPtr = (TestNVRAMReadStr *) CANdata;
						TestNVRAMAddrPtr = &TestNVRAMReadPtr->addr;
						strcpy(outputstring, "i2cChan=");
						//strcat(outputstring, FormatCANOutputByte(TestNVRAMAddrPtr->i2cChan, DecodeFormat));
						FormatCANOutputByte(TestNVRAMAddrPtr->i2cChan, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Addr=");
						//strcat(outputstring, FormatCANOutputByte(TestNVRAMAddrPtr->addr, DecodeFormat));
						FormatCANOutputByte(TestNVRAMAddrPtr->addr, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Offset=");
						FormatCANOutputWord(TestNVRAMAddrPtr->offset, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
				break;


				case MODULE_MSG_ID_BLTST_NVRAM_READ_R :
					if (MsgLength == sizeof(TestNVRAMReadRspStr))
					{
						TestNVRAMReadRspPtr = (TestNVRAMReadRspStr *) CANdata;
						strcpy(outputstring, "Val=");
						//strcat(outputstring, FormatCANOutputByte(TestNVRAMReadRspPtr->retVal, DecodeFormat));
						FormatCANOutputByte(TestNVRAMReadRspPtr->retVal, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
					}
					else	INVALID_DATA_LENGTH
				break;


				case MODULE_MSG_ID_BLTST_NVRAM_WRITE_C :
					if (MsgLength == sizeof(TestNVRAMWriteStr))
					{
						TestNVRAMWritePtr = (TestNVRAMWriteStr *) CANdata;
						TestNVRAMAddrPtr = &TestNVRAMWritePtr->addr;
						strcpy(outputstring, "i2cChan=");
						//strcat(outputstring, FormatCANOutputByte(TestNVRAMAddrPtr->i2cChan, DecodeFormat));
						FormatCANOutputByte(TestNVRAMAddrPtr->i2cChan, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Addr=");
						//strcat(outputstring, FormatCANOutputByte(TestNVRAMAddrPtr->addr, DecodeFormat));
						FormatCANOutputByte(TestNVRAMAddrPtr->addr, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Offset=");
						FormatCANOutputWord(TestNVRAMAddrPtr->offset, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);
						strcat(outputstring, "Val=");
						//strcat(outputstring, FormatCANOutputByte(TestNVRAMWritePtr->value, DecodeFormat));
						FormatCANOutputByte(TestNVRAMWritePtr->value, ValueString, DecodeFormat);
						strcat(outputstring, ValueString);

					}
					else	INVALID_DATA_LENGTH
				break;

				default : 
					strcpy(outputstring, "---");
					break;
			}
			break;

		case MODULE_MSG_TYPE_LPRIO:
			switch (MsgID)
			{
			case MODULE_MSG_ID_LPRIO_HBEAT_M:
			case MODULE_MSG_ID_LPRIO_MINFO_C:
			case MODULE_MSG_ID_LPRIO_TIME_C:
			case MODULE_MSG_ID_LPRIO_PING_C:
			case MODULE_MSG_ID_LPRIO_PING_R:
				strcpy(outputstring, "-");
				break;
				
			case MODULE_MSG_ID_LPRIO_HBEAT_S:
				
				if (MsgLength == sizeof(ModuleHBeatSStr))
				{
					ModuleHBeatSPtr = (ModuleHBeatSStr *) CANdata; 

					strcat(outputstring, " Error=");
					FormatCANOutputByte(ModuleHBeatSPtr->error, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);

					strcat(outputstring, " Status=");
					FormatCANOutputByte(ModuleHBeatSPtr->status, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);

					strcat(outputstring, " Serial=");
					FormatCANOutputDWord(ModuleHBeatSPtr->moduleUID.serialNumber, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);

					strcat(outputstring, " Supplier=");
					FormatCANOutputDWord(ModuleHBeatSPtr->moduleUID.supplierId, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
				}
				else	INVALID_DATA_LENGTH
				break;

			

			case MODULE_MSG_ID_LPRIO_TIME_R:
				if (MsgLength == sizeof(ModuleTimeStr))
				{

					ModuleTimePtr = (ModuleTimeStr *) CANdata; 

					strcat(outputstring, " Slot=");
					FormatCANOutputByte(ModuleTimePtr->slot, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);

					strcat(outputstring, " Time=");
					FormatCANOutputByte(ModuleTimePtr->time, ValueString, DecodeFormat);
					strcat(outputstring, ValueString);
				}
				else	INVALID_DATA_LENGTH
				break;

			default:
				strcpy(outputstring, "---");
				break;
			}
			break;

		default : 
			strcpy(outputstring, "???");
			break;
	
	}
	
	return;
}




void FormatCANOutputByte (lu_uint8_t ByteValue, lu_char_t *ValueString, lu_uint8_t DecodeFormat)
{
	if (DecodeFormat == CANMsgDecodeFormatDEC)
		wsprintf(ValueString, "%u ",  ByteValue);
	else
		wsprintf(ValueString, "0x%02X ", ByteValue);
	return;
}



void FormatCANOutputWord (lu_uint16_t WordValue, lu_char_t *ValueString, lu_uint8_t DecodeFormat)
{
	if (DecodeFormat == CANMsgDecodeFormatDEC)
		wsprintf(ValueString, "%u ",  WordValue);
	else
		wsprintf(ValueString, "0x%04X ", WordValue);
	return;
}


void FormatCANOutputDWord (lu_uint32_t DWordValue, lu_char_t *ValueString, lu_uint8_t DecodeFormat)
{
	if (DecodeFormat == CANMsgDecodeFormatDEC)
		wsprintf(ValueString, "%u ",  DWordValue);
	else
		wsprintf(ValueString, "0x%08X ", DWordValue);
	return;
}





void FormatStatus (const CANMsgDecoderStr Table_name[], lu_uint8_t Value, lu_char_t *outputstring, lu_char_t *ValueString, lu_uint8_t DecodeFormat)
{
	lu_uint8_t	cnt = 0;
	do 
	{
		if (Table_name[cnt].msgTypeValue == Value)
		{
			strcat(outputstring, Table_name[cnt].msgTypePtr);
			strcat(outputstring, " ");
			break;
		}
		cnt++;
				
		if (Table_name[cnt].msgTypeValue == END_OF_TABLE)
		{
			strcat(outputstring, "Status=");
			FormatCANOutputByte(Value, ValueString, DecodeFormat);
			strcat(outputstring, ValueString);
		}
	} while (Table_name[cnt].msgTypeValue != END_OF_TABLE); 
	
	return;
}



void FormatChannel (const CANMsgDecoderStr Table_name[], lu_uint8_t Value, lu_char_t *outputstring, lu_char_t *ValueString, lu_uint8_t DecodeFormat)
{
	lu_uint8_t	cnt = 0;
	
	do 
	{
		if (Table_name[cnt].msgTypeValue == Value)
		{
			strcat(outputstring, Table_name[cnt].msgTypePtr);
			strcat(outputstring, "  ");
			break;
		}
		cnt++;
				
		if (Table_name[cnt].msgTypeValue == END_OF_TABLE)
		{
			strcat(outputstring, "Chan=");
			FormatCANOutputByte(Value, ValueString, DecodeFormat);
			strcat(outputstring, ValueString);
		}
	} while (Table_name[cnt].msgTypeValue != END_OF_TABLE); 
	
	return;
}




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
