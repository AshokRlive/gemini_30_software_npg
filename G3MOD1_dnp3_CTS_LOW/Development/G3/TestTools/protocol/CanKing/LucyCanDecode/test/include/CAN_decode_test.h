/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol global definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/09/11      krauklis_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


//#ifndef _CAN_MSG_TABLES_INCLUDED
//#define _CAN_MSG_TABLES_INCLUDED



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef struct {
  unsigned long channel;  // The CAN channel (0,1,2,..)  
  unsigned long id;       // The CAN identifier
  unsigned long dlc;      // The data length code, 0..15
  unsigned long flags;    // message flags (canMSG_xxx from CANLIB)
  __int64  time;          // timestamp, 64 bits; unit is nanoseconds
  unsigned long dir;      // TKFMessageDirection, 0=tx, 1=rx 
  unsigned long number;   // "serial no"
  unsigned char data[8];  // Message data.
} TCanMessage; 



/*
typedef struct {
	lu_uint8_t MessageType;
	lu_uint8_t enable;
} MsgTypeFilterStr;
*/

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

//const TCanMessage	msg;
//TCanMessage	msg;
TCanMessage	m[];
//TCanMessage	m;


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

void main(void);
void GenerateOutput(canMessage, canHeadPtr, DecodeFormatPtr);





//#endif /* _CAN_MSG_TABLES_INCLUDED */

/*
 *********************** End of file ******************************************
 */

