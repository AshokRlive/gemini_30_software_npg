/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Codec module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28/09/11      krauklis_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <windows.h>
#include <stdio.h>
//#include <afx.h>


//#include "ModuleProtocol.h"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CAN_decode_test.h"
#include "lu_types.h"

//#include "ModuleProtocol.h"
//#include "CANMsgTables.h"
#include "CANMsgDecoder.h"
#include "CANProtocol.h"
#include "CANMsgFilters.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */




/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

  TCanMessage	m[] =
{
	0,		//channel
	0x821000,		//id {MsgTYPE[6], MsgID[6], Dst[5], DstID[3], Src[5], SrcID[3], fragment[1]}
	4,		//dlc
	0,		//flags
	0,		//time
	1,		//dir
	0,		//number
	3,		//data[0]
	6,		//data[1]
	1,		//data[2]
	0,		//data[3]
	0,		//data[4]
	1,		//data[5]
	0,		//data[6]
	8		//data[7]
};


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


void main (void)
{


	lu_uint32_t		id;
	CANHeaderStr	*canHeadPtr;
	//lu_char_t		*FileName = "filters.ini";
	//lu_char_t*	TestPtr;
	//lu_uint8_t		Test8UInt;
	//lu_uint32_t	Test32UInt;
	//lu_uint8_t		cnt2;
	lu_char_t		DecodedOutput[100] = "";
	
	//HMODULE Lucy_library = LoadLibrary("lucyDll.dll");
	HMODULE			Lucy_library = LoadLibrary("../../build/Debug/lucyDll.dll");
	FARPROC			ConfigureFunction = GetProcAddress(Lucy_library,"Configure");
	FARPROC			LucyDecodeFunction = GetProcAddress(Lucy_library,"FormatMsg");
	FARPROC			CreateFunction = GetProcAddress(Lucy_library,"Create");
	FARPROC			DestroyFunction = GetProcAddress(Lucy_library,"Destroy");
	FARPROC			DebugFiltersFunction = GetProcAddress(Lucy_library,"DebugFilters");
	
	FilterStr		*SRCModuleFilterPtr = 0;
	MsgFilterStr	*MsgTypeFilterPtr = 0;
	lu_uint8_t		*DecodeFormatPtr = 0;
	lu_uint8_t		*FiltersActivePtr = 0;
	lu_uint8_t		DecodeFormat = CANMsgDecodeFormatDEC;
	
	lu_uint8_t		MsgFilterActive = 0;
	//MsgFilterStr	*MsgTypeFilterPtr2;
	lu_uint32_t		DllReturn;
	
	HWND			hhh = 0;
	
	
   


	
  

	id = m->id;
	canHeadPtr = (CANHeaderStr *) &id;

	DllReturn = ConfigureFunction(hhh);

	DebugFiltersFunction(&MsgTypeFilterPtr, &SRCModuleFilterPtr, &DecodeFormatPtr, &FiltersActivePtr);


	


/*
	MsgTypeFilterPtr = (MsgFilterStr *) calloc(CANMsgFilterMsgTypeSize, sizeof(MsgFilterStr));
	if (InitilaizeFilters(FileName, SRCModuleFilterPtr, MsgTypeFilterPtr, &DecodeFormat) == 0)
	{
		MsgFilterActive = 1;
		//MsgTypeFilterPtr2 = MsgTypeFilterPtr;
	}

*/


	
 
	//hhh = CreateDialog(GetModuleHandle(NULL),0,0,ToolDlgProc);
	
	LucyDecodeFunction(m, 1, &DecodedOutput, 0);
	
	
	printf("%s\n", DecodedOutput);

	
	
	printf("End");
	




  return;
}





void GenerateOutput (TCanMessage *canMessage, CANHeaderStr *canHeadPtr, lu_uint8_t *DecodeFormatPtr)
{
	
	lu_char_t		MsgType[100] = "";
	lu_char_t		DecodedData[100] = "";
	lu_uint8_t		cnt2;
	lu_char_t		*Space = " ";
	lu_char_t		*DirStr = "Rx";


	
	//for (cnt2 = 0; cnt2 < 32; cnt2++)
	//{
		//canHeadPtr->messageType = cnt2;
		//canHeadPtr->deviceSrc = cnt2;

		//ResolveModuleType(canHeadPtr->deviceSrc, DecodedData);
		ResolveMsg (canHeadPtr->messageType, canHeadPtr->messageID, MsgType);
		DecodeCANdata (canHeadPtr->messageType,canHeadPtr->messageID, canMessage->data, canMessage->dlc, DecodedData, *DecodeFormatPtr);


		for (cnt2 = strlen(MsgType); cnt2 < 28; cnt2++)
		{
			strcat(MsgType,Space);
		};

			
		printf("%s %s \n", MsgType, DecodedData);
		//printf("%s %s \n", DecodedData, MsgType);

}







/*
 *********************** End of file ******************************************
 */


