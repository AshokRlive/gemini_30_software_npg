/*
**                         Copyright 1998 by KVASER AB            
**                   P.O Box 4076 SE-51104 KINNAHULT, SWEDEN
**             E-mail: staff@kvaser.se   WWW: http://www.kvaser.se
**
** This software is furnished under a license and may be used and copied
** only in accordance with the terms of such license.
**
** $Header: /home/cvs/beta/canlib40/DDK/PCcan/16bit/src/version.hx,v 1.1.1.1 2003/12/14 13:43:54 db Exp $
** $Revision: 1.1.1.1 $   $NoKeywords:$
*/
/*
** $$CAUTION$$
*/
#define MAJOR_VERSION       $$MAJOR_N$$
#define MINOR_VERSION       $$MINOR_N$$
#define BUILD_NUMBER        $$BUILD_N$$
#define VERSION_STRING      "$$MAJOR$$.$$MINOR$$.$$BUILD$$"
#define BUILD_TIME_STRING   "$$CTIME$$"
