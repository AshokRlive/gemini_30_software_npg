/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.diagnostic.dll.DummyDLL;

import sun.awt.AppContext;

/**
 *
 */
//@Ignore // This is a manual test for learning JAVA Scheduler.
public class SchedulerTest {

  private static final String EXPECTED_EXCEPTION = "Expected exception!";
  private static DummyDLL dll = new DummyDLL();

  private ScheduledExecutorService sch;
  @Before
  public void setUp() throws Exception {
    sch = new ScheduledThreadPoolExecutor(1);
  }

  @After
  public void tearDown() throws Exception {
    sch.shutdown();
  }

  @Test
  public void testSubmit() throws InterruptedException, ExecutionException {
    Future<?> t = sch.submit(new MyExceptionTask());
    Future<?> t2 = sch.submit(new MyCommandTask(0));
    sch.submit(new MyCommandTask(2));

    try {
      t.get();
      Assert.fail("Exception expected");
    } catch (Throwable e) {
    }
  }
  
  @Test
  public void testPolling() throws InterruptedException, ExecutionException {
    MyPollingSwingWorker sw = new MyPollingSwingWorker(0);
    MyPollingTask pt = new MyPollingTask();
    
    ExecutorService service = new ScheduledThreadPoolExecutor(1);
    AppContext.getAppContext().put(SwingWorker.class, service);
    sw.execute();
    
    ScheduledFuture<?> f = sch.scheduleAtFixedRate(sw, 0, 500, TimeUnit.MILLISECONDS);
    //f.get();

    Thread.sleep(3000);
  }

  static class MyExceptionTask implements Runnable {

    @Override
    public void run() {
      throw new RuntimeException(EXPECTED_EXCEPTION);
    }

  }

  static class MyCommandTask extends SwingWorker<Void, Void> {

    static int count = 0;
    private final int id;


    MyCommandTask(int id) {
      this.id = id;
    }

    @Override
    protected Void doInBackground() throws Exception {
      count++;
      log("doInBackground");

      dll.WriteIOID((short) 0, "", 1);
      publish();
      log("published");
      return null;
    }

    @Override
    protected void process(List<Void> chunks) {
      // TODO Auto-generated method stub
      super.process(chunks);
      log("process");
    }

    @Override
    protected void done() {
      super.done();
      Assert.assertTrue(SwingUtilities.isEventDispatchThread());
      try {
        get();
        log("done");
      } catch (Throwable e) {
        System.err.println("Exception caught:  " + e);
        e.printStackTrace();
      }
    }

    private void log(String msg) {
      String thName = Thread.currentThread().getName();
      System.out.println(String.format("%-8s [Task id:%d Thread:%s] %s [Count: %d] ", "Command", id, thName, msg, count));
    }
  }
  
  static class MyPollingTask implements Runnable {

    @Override
    public void run() {
      System.out.println("Polling");
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    
  }

  static class MyPollingSwingWorker extends SwingWorker<Void, Void> {

    static int count = 0;
    private final int id;


    MyPollingSwingWorker(int id) {
      this.id = id;
    }

    @Override
    protected Void doInBackground() throws Exception {
      count++;
      log("doInBackground");

//      dll.ReadIOID((short) 0, "", null);
//      publish();
      return null;
    }

    @Override
    protected void process(List<Void> chunks) {
      // TODO Auto-generated method stub
      super.process(chunks);
      log("process");
    }

    @Override
    protected void done() {
      super.done();
      log("done");

    }

    private void log(String msg) {
      String thName = Thread.currentThread().getName();
      System.out.println(String.format("%-8s [Task id:%d Thread:%s] %s [Count:%d] ", "Polling", id, thName, msg, count));
   }

  }
  

}
