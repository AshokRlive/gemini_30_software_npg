/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.utils;

import java.awt.Window;
import java.util.Enumeration;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;


/**
 *
 */
public class UIUtilities {
  private UIUtilities(){}
  
  /**
   * Adjusts all columns' width to fit their content.
   *
   * @param table
   *          the table to be set column width
   */
  public static void packTableColumns(JTable table) {
    JTableHeader header = table.getTableHeader();
    int rowCount = table.getRowCount();
    Enumeration<?> columns = table.getColumnModel().getColumns();
    while (columns.hasMoreElements()) {
      TableColumn column = (TableColumn) columns.nextElement();
      int col = header.getColumnModel().getColumnIndex(
          column.getIdentifier());
      int width = (int) table.getTableHeader().getDefaultRenderer()
          .getTableCellRendererComponent(table, column.getIdentifier(), false, false, -1, col)
          .getPreferredSize().getWidth();
      for (int row = 0; row < rowCount; row++) {
        int preferedWidth = (int) table
            .getCellRenderer(row, col)
            .getTableCellRendererComponent(table,
                table.getValueAt(row, col), false, false, row,
                col).getPreferredSize().getWidth();
        width = Math.max(width, preferedWidth);
      }
      header.setResizingColumn(column);
      column.setWidth(width + table.getIntercellSpacing().width + 10);
    }
  }
  
  public static Window getParent(JComponent component) {
    if(component == null)
      return null;
    return SwingUtilities.getWindowAncestor(component);
  }
}

