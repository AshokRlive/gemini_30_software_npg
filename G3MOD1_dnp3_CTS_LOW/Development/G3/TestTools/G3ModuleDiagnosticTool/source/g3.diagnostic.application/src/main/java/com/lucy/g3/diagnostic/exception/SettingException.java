/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.exception;


/**
 *
 */
public class SettingException extends Exception {

  public SettingException() {
    super();
  }

  public SettingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public SettingException(String message, Throwable cause) {
    super(message, cause);
  }

  public SettingException(String message) {
    super(message);
  }

  public SettingException(Throwable cause) {
    super(cause);
  }

}

