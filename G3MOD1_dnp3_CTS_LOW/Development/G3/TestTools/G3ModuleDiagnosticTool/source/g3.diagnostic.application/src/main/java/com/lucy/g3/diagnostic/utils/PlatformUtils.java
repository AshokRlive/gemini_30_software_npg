/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.utils;


/**
 *
 */
public class PlatformUtils {
  public static boolean checkJre32Bit(){
     return System.getProperty("sun.arch.data.model").endsWith("32");
  }

  public static String getJREBits() {
    String bits = System.getProperty("sun.arch.data.model");
    bits.substring(bits.length() - 2);
    bits += " Bits";
    return bits;
  }
}

