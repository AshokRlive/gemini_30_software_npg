/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.ui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JMenuItem;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
public class LoggerAppender extends AppenderSkeleton  {
  public final static String NAME = "SwingLoggerAppender";
  
  public LoggerAppender() {
    initComponents();
    super.setName(NAME);
  }
  
  public JPanel getPanel() {
    return panel;
  }

  @Override
  public void setName(String name) {
    throw new UnsupportedOperationException("Not allow to change name!");
  }

  @Override
  public void close() {
    // Nothing to do.
  }
  

  @Override
  public boolean requiresLayout() {
    return false;
  }

  @Override
  protected void append(LoggingEvent event) {
    taAppender.append(String.format("[%-5s] %s \n", event.getLevel(), event.getMessage()));
  }

  private void menuItemClearActionPerformed(ActionEvent e) {
    taAppender.setText("");
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel = new JPanel();
    scrollPane1 = new JScrollPane();
    taAppender = new JTextArea();
    popupMenu1 = new JPopupMenu();
    menuItemClear = new JMenuItem();

    //======== panel ========
    {
      panel.setBorder(new TitledBorder("Log"));
      panel.setLayout(new BorderLayout());

      //======== scrollPane1 ========
      {

        //---- taAppender ----
        taAppender.setEditable(false);
        taAppender.setFont(new Font("Monospaced", Font.PLAIN, 14));
        taAppender.setComponentPopupMenu(popupMenu1);
        scrollPane1.setViewportView(taAppender);
      }
      panel.add(scrollPane1, BorderLayout.CENTER);
    }

    //======== popupMenu1 ========
    {

      //---- menuItemClear ----
      menuItemClear.setText("Clear");
      menuItemClear.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          menuItemClearActionPerformed(e);
        }
      });
      popupMenu1.add(menuItemClear);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel;
  private JScrollPane scrollPane1;
  private JTextArea taAppender;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItemClear;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
