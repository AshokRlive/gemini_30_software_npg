/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.lucy.g3.diagnostic.gpio.GPIO;
import com.lucy.g3.diagnostic.utils.UIUtilities;


/**
 *
 */
public class MessageBox {
  public static void showSuccessDialog(JComponent invoker, String successMsg) {
    JOptionPane.showMessageDialog(UIUtilities.getParent(invoker),
        successMsg,
        "Success", JOptionPane.INFORMATION_MESSAGE);
  }
  public static void showFailureDialog(JComponent invoker, ArrayList<GPIO> failIolist, String errorMsg) {
    JLabel msg = new JLabel(errorMsg);
    JTextArea ta = new JTextArea();
    ta.setEditable(false);
    ta.setFont(msg.getFont());
    for (GPIO io : failIolist) {
      ta.append(io.toString());
      ta.append("\n");
    }

    JPanel container = new JPanel(new BorderLayout(10, 10));
    container.add(msg, BorderLayout.NORTH);
    container.add(new JScrollPane(ta), BorderLayout.CENTER);
    container.setPreferredSize(new Dimension(400, 200));

    JOptionPane.showMessageDialog(invoker,
        container,
        "Failure", JOptionPane.ERROR_MESSAGE);
  }
  
  public static void showFailureDialog(JComponent invoker, String msg) {
    JOptionPane.showMessageDialog(invoker,
        msg,
        "Failure", JOptionPane.ERROR_MESSAGE);
  }

}

