/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.dll;

import java.util.Random;

import com.sun.jna.ptr.IntByReference;


/**
 *
 */
public class DummyDLL implements IIOManagerDLL {

  @Override
  /*synchronized*/ public short InitAndOpenBoard(int comMode, String rtuIPAddress, int module, int moduleId) {
    sleep(1);
    return 0;
  }

  @Override
  /*synchronized*/ public short CloseAndDeinitBoard(short boardHandle) {
    sleep(1);
    return 0;
  }

  @Override
  /*synchronized*/ public int GetAllIOIDStr(String[] IOIDStringBuf, int size) {
    sleep(1);
    return 0;
  }

  @Override
  /*synchronized*/ public int CANCodecInitDll() {
    sleep(1);
    return 0;
  }

  @Override
  /*synchronized*/ public int CANCodecDeInitDll(int handle) {
    sleep(1);
    return 0;
  }

  @Override
  /*synchronized*/ public int hat_client_init(String rtuIpAddress, int port) {
    sleep(1);
    return 0;
  }

  @Override
  /*synchronized*/ public int InitIOMAP(short handle) {
    sleep(1);
    return 0;
  }

  @Override
  /*synchronized*/ public int InitIOID(short handle, String IOIDString) {
    sleep(1);
    return 0;
  }

  @Override
  /*synchronized*/ public short OpenBoard(int comMode, int module, int moduleId) {
    sleep(1);
    return 0;
  }

  @Override
  /*synchronized*/ public short CloseBoard(short handle) {
    sleep(1);
    return 0;
  }

  @Override
  /*synchronized*/ public int ReadIOID(short handle, String IOIDString, IntByReference valueToBeRead) {
    sleep(1);
    return 0;
  }

  @Override
  /*synchronized*/ public int WriteIOID(short handle, String IOIDString, int valueToBeWritten) {
    sleep(1);
    return 0;
  }
  
  private final Random rand = new Random(100);
  
  private void sleep(int timeSec) {
    sleep();
  }
  
  private void sleep() {
    int timeSec = rand.nextInt(4)+1;
    try {
      Thread.sleep(timeSec*1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}

