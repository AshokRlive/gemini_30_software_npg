/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.tasks;

import java.util.concurrent.ExecutionException;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import org.apache.log4j.Logger;

import com.lucy.g3.diagnostic.dll.DLLManager;
import com.lucy.g3.diagnostic.dll.IIOManagerDLL;
import com.lucy.g3.diagnostic.exception.DLLFuncCallFailedException;
import com.lucy.g3.diagnostic.exception.SettingException;
import com.lucy.g3.diagnostic.ui.MessageBox;
import com.lucy.g3.diagnostic.utils.UIUtilities;

/**
 * A task for calling DLL to initialise IO Map.
 */
public class InitIOMapTask extends AbstractTask<Void, Void> {

  private Logger log = Logger.getLogger(InitIOMapTask.class);
  
  private final DLLManager dllMgr;
  private final JComponent invoker;

  public InitIOMapTask(JComponent invoker, DLLManager dllMgr) {
    super();
    this.invoker = invoker;
    this.dllMgr = dllMgr;
  }

  @Override
  protected Void doInBackground() throws Exception {
    dllMgr.checkReadyForTesting();
    IIOManagerDLL dll = dllMgr.getDll();
    short handle = dllMgr.getHandle();
    
    synchronized (dll) {
      int errorCode = dll.InitIOMAP(handle);
      if(errorCode != 0)
        throw new DLLFuncCallFailedException("InitIOMAP", errorCode);
    }
    
    return null;
  }

  @Override
  protected void done() {
   if(isCancelled())
     return;
    
    try {
        get();
        MessageBox.showSuccessDialog(invoker, "The IO Map has been initialised!");
    } catch (ExecutionException e) {
      log.error(e.getCause());
      MessageBox.showFailureDialog(invoker, e.getCause().getMessage());
    } catch( InterruptedException e1) {
      log.warn("InitIOMap task was interrupted!");
    }
  }

}
