/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.dll;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.awt.Frame;
import java.io.File;
import java.net.URISyntaxException;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.diagnostic.dll.DLLManager;
import com.lucy.g3.diagnostic.dll.IIOManagerDLL;
import com.lucy.g3.diagnostic.ui.AboutDialog;
import com.lucy.g3.diagnostic.utils.PlatformUtils;

/**
 * The Class DLLManagerTest. 
 */
public class DLLManagerTest extends DLLTestResources{

  private DLLManager fixture;


  @Before
  public void setUp() throws Exception {
    
    /*
     * This unit test only work under 32bits cause the DLL is built by win32,
     * which means the JRE must be 32bit to load these DLLs. See JNA doc.
     */
    Assume.assumeTrue(PlatformUtils.checkJre32Bit());
    
    
    fixture = new DLLManager();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testLoadDLL() throws URISyntaxException {
    assertNull(fixture.getDll());
    File dllFile = getDLLFile();
    IIOManagerDLL dll = fixture.loadDLL(dllFile.getParent(), dllFile.getName());
    
    assertNotNull(dll);
  }

  public static void main(String[] args) {
    new AboutDialog((Frame)null).setVisible(true);
  }
}
