/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.Logger;
public class HelpWindow extends JFrame {
  private Logger log = Logger.getLogger(HelpWindow.class);
  private static final KeyStroke STROKE_ESC = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);

  public HelpWindow() {
    initComponents();
    getRootPane().registerKeyboardAction(btnClose.getActionListeners()[0], STROKE_ESC,
        JComponent.WHEN_IN_FOCUSED_WINDOW);
    
    loadHelp();
  }

  private void loadHelp() {
    StringBuffer sb = new StringBuffer();
    InputStream is = getClass().getClassLoader().getResourceAsStream("help/index.html");
    if (is != null) {
      try {
        final BufferedReader reader = new BufferedReader(
            new InputStreamReader(is));
        String line = null;
        while ((line = reader.readLine()) != null) {
          sb.append(line);
        }
        reader.close();
      } catch (final Exception e) {
        log.error("Failed to load help content!", e);
      }
    } else {
      log.error("help content not found!");
    }
    
    textPaneHelp.setContentType("text/html");
    textPaneHelp.setText(sb.toString());
  }

  private void btnCloseActionPerformed(ActionEvent e) {
    dispose();
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    dialogPane = new JPanel();
    contentPanel = new JPanel();
    scrollPane1 = new JScrollPane();
    textPaneHelp = new JTextPane();
    buttonBar = new JPanel();
    btnClose = new JButton();

    //======== this ========
    setTitle("Help");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setIconImage(new ImageIcon(getClass().getResource("/images/frameicon.png")).getImage());
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new BorderLayout());

        //======== scrollPane1 ========
        {

          //---- textPaneHelp ----
          textPaneHelp.setEditable(false);
          scrollPane1.setViewportView(textPaneHelp);
        }
        contentPanel.add(scrollPane1, BorderLayout.CENTER);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);

      //======== buttonBar ========
      {
        buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
        buttonBar.setLayout(new GridBagLayout());
        ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] { 0, 80 };
        ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] { 1.0, 0.0 };

        //---- btnClose ----
        btnClose.setText("Close");
        btnClose.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnCloseActionPerformed(e);
          }
        });
        buttonBar.add(btnClose, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    setSize(495, 410);
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JScrollPane scrollPane1;
  private JTextPane textPaneHelp;
  private JPanel buttonBar;
  private JButton btnClose;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
