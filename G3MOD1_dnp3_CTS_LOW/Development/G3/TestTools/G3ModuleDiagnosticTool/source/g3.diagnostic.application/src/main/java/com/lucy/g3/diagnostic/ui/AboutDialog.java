/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.Logger;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.diagnostic.utils.PlatformUtils;

public class AboutDialog extends JDialog {

  private Logger log = Logger.getLogger(AboutDialog.class);

  private final String[] columnNames = { "Property", "Value" };

  private String version;

  private static Object[][] feautres;


  public AboutDialog(Frame owner) {
    super(owner);
    loadInfo();
    initComponents();
  }

  public AboutDialog(Dialog owner) {
    super(owner);
    loadInfo();
    initComponents();
  }

  private void loadInfo() {

    // Load building information from MANIFEST.MF
    final String manifestFile = "META-INF/MANIFEST.MF";
    URLClassLoader cl = (URLClassLoader) AboutDialog.class.getClassLoader();

    try {
      URL url = cl.findResource(manifestFile);
      Manifest manifest = new Manifest(url.openStream());
      Attributes att = manifest.getMainAttributes();
      version = att.getValue("Implementation-Version");
      

      LinkedHashMap<String, String> featureMap = new LinkedHashMap<String, String>();
      featureMap.put("JRE Version", System.getProperty("java.version"));
      featureMap.put("JRE Arch:", PlatformUtils.getJREBits());

      String attName = "SCM-Revision";
      String attValue = att.getValue(attName);
      featureMap.put(attName, attValue);

      attName = "Build-Date";
      attValue = att.getValue(attName);
      featureMap.put(attName, attValue);

      attName = "Build-Jdk";
      attValue = att.getValue(attName);
      featureMap.put(attName, attValue);

      feautres = new Object[featureMap.size()][2];
      Set<Entry<String, String>> entries = featureMap.entrySet();
      Iterator<Entry<String, String>> entriesIterator = entries.iterator();
      int index = 0;
      while (entriesIterator.hasNext()) {
        Entry<String, String> mapping = entriesIterator.next();
        feautres[index][0] = mapping.getKey();
        feautres[index][1] = mapping.getValue();
        index++;
      }
    } catch (IOException E) {
      log.warn(manifestFile + " NOT found.");
    }
  }

  private void createUIComponents() {

    featureTable = new JTable(feautres, columnNames) {

      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
  }

  private void okButtonActionPerformed(ActionEvent e) {
    dispose();
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    contentPanel = new JPanel();
    contentPanel2 = new JPanel();
    labelLogo = new JLabel();
    label1 = new JLabel();
    JLabel labelVersion = new JLabel();
    labelVersionNo = new JLabel();
    JLabel labelVendor = new JLabel();
    label2 = new JLabel();
    JLabel labelWeb = new JLabel();
    label3 = new JLabel();
    scrollPane1 = new JScrollPane();
    buttonBar = new JPanel();
    okButton = new JButton();

    //======== this ========
    setTitle("About");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new BorderLayout());

        //======== contentPanel2 ========
        {
          contentPanel2.setOpaque(false);
          contentPanel2.setLayout(new FormLayout(
              "default, $ugap, default, $lcgap, pref:grow",
              "2*(default, 6dlu), fill:default, 2*(6dlu, default), 20dlu, $lgap, fill:80dlu:grow"));

          //---- labelLogo ----
          labelLogo.setIcon(new ImageIcon(getClass().getResource("/images/lucy.png")));
          contentPanel2.add(labelLogo, CC.xywh(1, 1, 1, 9));

          //---- label1 ----
          label1.setText("Gemini 3 Module Diagnositc Tool");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD,
              label1.getFont().getSize() + 4f));
          contentPanel2.add(label1, CC.xywh(3, 1, 3, 1));

          //---- labelVersion ----
          labelVersion.setText("Product Version:");
          labelVersion.setFont(labelVersion.getFont().deriveFont(labelVersion.getFont().getStyle() | Font.BOLD));
          contentPanel2.add(labelVersion, CC.xy(3, 5));

          //---- labelVersionNo ----
          labelVersionNo.setName("labelVersionNo");
          labelVersionNo.setText("1.0");
          contentPanel2.add(labelVersionNo, CC.xy(5, 5));

          //---- labelVendor ----
          labelVendor.setText("Vendor:");
          labelVendor.setFont(labelVendor.getFont().deriveFont(labelVendor.getFont().getStyle() | Font.BOLD));
          contentPanel2.add(labelVendor, CC.xy(3, 7));

          //---- label2 ----
          label2.setText("Lucy Electric");
          contentPanel2.add(label2, CC.xy(5, 7));

          //---- labelWeb ----
          labelWeb.setText("Website:");
          labelWeb.setFont(labelWeb.getFont().deriveFont(labelWeb.getFont().getStyle() | Font.BOLD));
          contentPanel2.add(labelWeb, CC.xy(3, 9));

          //---- label3 ----
          label3.setText("www.lucyelectric.co.uk");
          contentPanel2.add(label3, CC.xy(5, 9));

          //======== scrollPane1 ========
          {

            //---- featureTable ----
            featureTable.setFocusable(false);
            featureTable.setName("featureTable");
            featureTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            featureTable.setFillsViewportHeight(true);
            scrollPane1.setViewportView(featureTable);
          }
          contentPanel2.add(scrollPane1, CC.xywh(1, 12, 5, 1));
        }
        contentPanel.add(contentPanel2, BorderLayout.CENTER);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);

      //======== buttonBar ========
      {
        buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
        buttonBar.setLayout(new GridBagLayout());
        ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] { 0, 80 };
        ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] { 1.0, 0.0 };

        //---- okButton ----
        okButton.setText("Close");
        okButton.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            okButtonActionPerformed(e);
          }
        });
        buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    setSize(560, 480);
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  
    labelVersionNo.setText(version);
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JPanel contentPanel2;
  private JLabel labelLogo;
  private JLabel label1;
  private JLabel labelVersionNo;
  private JLabel label2;
  private JLabel label3;
  private JScrollPane scrollPane1;
  private JTable featureTable;
  private JPanel buttonBar;
  private JButton okButton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
