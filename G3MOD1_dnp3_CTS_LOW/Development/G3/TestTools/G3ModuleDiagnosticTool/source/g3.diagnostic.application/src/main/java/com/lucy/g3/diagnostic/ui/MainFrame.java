/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.NumberFormatter;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.diagnostic.dll.DLLManager;
import com.lucy.g3.diagnostic.dll.IModuleProtocol;
import com.lucy.g3.diagnostic.gpio.GPIO;
import com.lucy.g3.diagnostic.gpio.GPIOManager;
import com.lucy.g3.diagnostic.prefs.PreferencesManager;
import com.lucy.g3.diagnostic.tasks.AbstractTask;
import com.lucy.g3.diagnostic.tasks.BoardOpenCloseTask;
import com.lucy.g3.diagnostic.tasks.GPIOPollingTask;
import com.lucy.g3.diagnostic.tasks.InitIOMapTask;
import com.lucy.g3.diagnostic.tasks.InitIOTask;
import com.lucy.g3.diagnostic.tasks.WriteIOTask;
import com.lucy.g3.diagnostic.utils.UIUtilities;

/**
 * Main GUI.
 */
public class MainFrame extends JFrame {
  private final static Level[] LOG_LEVELS= new Level[]{
    Level.DEBUG, Level.INFO,Level.WARN,Level.ERROR,Level.FATAL};

  private Logger log = Logger.getLogger(MainFrame.class);

  public final DLLManager dllManager = new DLLManager();

  private ExecutorService exec = createThreadPool();

  private ExecutorService createThreadPool() {
    /*
     * one thread for polling, another for command. 
     */
    return Executors.newFixedThreadPool(2);
  }
  
  private final PreferencesManager prefs = PreferencesManager.INSTANCE;
  
  private GPIOManager ioManager = new GPIOManager();
  
  private GPIOListTableModel ioTableModel = new GPIOListTableModel();
  
  private GPIOPollingTask  pollingTask;

  public MainFrame() {
    initComponents();
    initEventHandling();
    
    
    // Load DLL
    try {
      setDLLFilePath(tfDLLPath.getText());
    } catch(Throwable e){
      tfDLLPath.setText("");
    }
    
    startstopPolling();
  }

  private void startstopPolling() {
    if(pollingTask != null && !pollingTask.isCancelled()) {
      /*Stop polling*/
      pollingTask.cancel(true);
      log.info("Stopped polling GPIO.");
    }
    
    if(dllManager.isBoardOpen() && toggleBtnPolling.isSelected()) {
      /*Start polling*/
      int pollingRate = Integer.parseInt(ftfPollingRate.getText());
      pollingTask = new GPIOPollingTask(null, ioTableModel, dllManager, pollingRate); 
      exec.execute(pollingTask);
      log.info("Started polling GPIO. Polling Rate: " + pollingRate);
    }
  }
  
  private void initEventHandling() {
    /*Update button enable states*/
    updateComponentStates(dllManager.isBoardOpen());
    dllManager.addPropertyChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        if(evt.getPropertyName().equals(DLLManager.PROPERTY_BOARD_OPEN)) {
          updateComponentStates((boolean) evt.getNewValue());
          startstopPolling();
        }
      }
    });
  }

  private void updateComponentStates(boolean isBoardOpen) {
    btnOpenBoard.setEnabled(!isBoardOpen);
    btnCloseBoard.setEnabled(isBoardOpen);
    btnReadGPIO.setEnabled(isBoardOpen);
    btnWriteGPIO.setEnabled(isBoardOpen);
    toggleBtnPolling.setEnabled(isBoardOpen);
    btnInitIO.setEnabled(isBoardOpen);
    btnInitIOMap.setEnabled(isBoardOpen);
  }

  public void updateGPIOList(ArrayList<GPIO> list) {
    ioTableModel.setList(list);
    UIUtilities.packTableColumns(tableGPIO);
  }

  public void close() {
    int ret = JOptionPane.showConfirmDialog(this, 
        "Do you want to exit?", 
        "Confirm", 
        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
    if(ret == JOptionPane.YES_OPTION) {
      prefs.saveSession(this);
      dispose();
      exec.shutdownNow();
    } 
  }

  private void setDLLFilePath(String path) throws Exception {
    if (!path.trim().isEmpty()) {
      try {
        File file = new File(path);
        dllManager.loadDLL(file.getParent(), file.getName());
      } catch (Throwable e1) {
        throw e1;
      }
    }
  }

  private void createUIComponents() {
    separator4 = createTitleSeparator("Step 1. Board Initialisation");
    separator5 = createTitleSeparator("Step 2. Board GPIO List");
    
    menuLogLevel = createLogLeveMenu();
    
    combModule = new JComboBox<>(IModuleProtocol.MODULE_TYPES);
    combModule.setRenderer(new ModuleCellRenderer());
    combModule.setSelectedItem(null);
    
    combModuleId = new JComboBox<>(IModuleProtocol.MODULE_IDS);
    combModuleId.setRenderer(new ModuleIDCellRenderer());
    combModuleId.setSelectedItem(null);
    
    NumberFormatter nf = new NumberFormatter();
    nf.setValueClass(Integer.class);
    nf.setMaximum(new Integer(1000000));
    nf.setMinimum(new Integer(0));
    ftfPollingRate = new JFormattedTextField(nf);
    
    tableGPIO = new JTable(ioTableModel);
    panelLog = ((LoggerAppender) Logger.getRootLogger().getAppender(LoggerAppender.NAME)).getPanel();
  }

  private JMenu createLogLeveMenu() {
    JMenu menu = new JMenu();
    ButtonGroup bg = new ButtonGroup();
    Level curLevel = Logger.getRootLogger().getLevel();
    for (int i = 0; i < LOG_LEVELS.length; i++) {
      JRadioButtonMenuItem levelItem = new JRadioButtonMenuItem(LOG_LEVELS[i].toString());
      levelItem.putClientProperty("value", LOG_LEVELS[i]);
      levelItem.setSelected(curLevel == LOG_LEVELS[i]);
      bg.add(levelItem);
      menu.add(levelItem);
      levelItem.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          JRadioButtonMenuItem  s = (JRadioButtonMenuItem) e.getSource();
          Level level = (Level)s.getClientProperty("value");
          Logger.getRootLogger().setLevel(level);
          prefs.saveLoggerLevel(level.toString());
        }
      });
    }
    return menu;
  }

  private JComponent createTitleSeparator(String  title) {
    JLabel titleLabel = new JLabel(title);
    titleLabel.setHorizontalAlignment(SwingConstants.LEFT);
    titleLabel.setFont(titleLabel.getFont().deriveFont(Font.BOLD));
    return DefaultComponentFactory.getInstance().createSeparator(titleLabel);
  }

  private void btnBrowseActionPerformed(ActionEvent e) {
    JFileChooser fc = new JFileChooser(prefs.getFileChooseDir());
    fc.setFileFilter(new FileNameExtensionFilter("DLL file", "dll"));
    fc.setAcceptAllFileFilterUsed(true);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    int ret = fc.showOpenDialog(this);
    
    if(ret == JFileChooser.APPROVE_OPTION) {
      String filePath = fc.getSelectedFile().getPath();
      try {
        setDLLFilePath(filePath);
        tfDLLPath.setText(filePath);
      } catch(Throwable e1) {
        log.error(e1.getMessage());
        JOptionPane.showMessageDialog(this, 
            "Cannot load the selected DLL file!", "Error", JOptionPane.ERROR_MESSAGE);
      }
      
      // Save previous dir
      prefs.saveFileChooseDir(fc.getSelectedFile().getParent());
    }
  }

  private void btnOpenBoardActionPerformed(ActionEvent e) {
    execute(new BoardOpenCloseTask(this, BoardOpenCloseTask.OPEN, ioManager));
  }
  
  private void btnCloseBoardActionPerformed(ActionEvent e) {
    /* Close all running tasks immediately so we don't need to wait for tasks to complete.*/
    exec.shutdownNow();
    
    /*Waiting for thread pool close*/
    getRootPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    try {
      exec.awaitTermination(10, TimeUnit.SECONDS);
      exec = createThreadPool();
      
      execute(new BoardOpenCloseTask(this, BoardOpenCloseTask.CLOSE, ioManager));
      
    } catch (InterruptedException e1) {
      log.warn("Close Board action interrupted!");
      
    } finally {
      getRootPane().setCursor(Cursor.getDefaultCursor());
    }
  }

  private void execute(SwingWorker<?, ?> task) {
    getRootPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    String name = task.getClass().getSimpleName();
    try {
      exec.execute(task);
      log.info("Task:\""+name+"\" is running...");
      task.get(AbstractTask.TIMEOUT,AbstractTask.TIMEOUT_UNIT);
      log.info("Task:\""+name+"\" was completed!");
    } catch (InterruptedException e1) {
      log.warn("Task:\""+name+"\" was interrupted");
    } catch(ExecutionException  e) {
      log.warn("Task:\""+name+"\" finished with execution error.");
    } catch (TimeoutException e) {
      log.warn("Task:\""+name+"\" timeout!");
    } finally {
      getRootPane().setCursor(Cursor.getDefaultCursor());
    }
  }
  private void btnInitIOMapActionPerformed(ActionEvent e) {
    execute(new InitIOMapTask((JComponent) e.getSource(), dllManager));
  }

  private void btnInitIOActionPerformed(ActionEvent e) {
    if(checkSelectionNotEmpty()) {
      execute(new InitIOTask((JComponent) e.getSource(), 
        ioTableModel, dllManager, tableGPIO.getSelectedRows()));
    }
  }

  private void btnReadGPIOActionPerformed(ActionEvent e) {
    if(checkSelectionNotEmpty()) {
      execute(new GPIOPollingTask((JComponent) e.getSource(),
          ioTableModel, dllManager, -1, 
          tableGPIO.getSelectedRows()));
    }
  }

  private void btnWriteGPIOActionPerformed(ActionEvent e) {
    if(checkSelectionNotEmpty()) {
      String input = JOptionPane.showInputDialog(this, "Please give the integer value to be written:", 0);
      if(input == null)
        return; // User cancelled.
      
      try {
        int newValue = Integer.parseInt(input);
        execute(new WriteIOTask((JComponent) e.getSource(),
            ioTableModel, dllManager, newValue, 
            tableGPIO.getSelectedRows()));
      } catch (Exception e1) {
        JOptionPane.showMessageDialog(this, "Invalid integer value: " + input+"!", 
            "Invalid Input", JOptionPane.ERROR_MESSAGE);
      }
      
    }
  }

  private boolean checkSelectionNotEmpty() {
    if(tableGPIO.getSelectionModel().isSelectionEmpty()) {
      JOptionPane.showMessageDialog(this,
          "Please select the IO in the list before operation!",
          "No Selection", JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  private void toggleBtnPollingActionPerformed(ActionEvent e) {
    startstopPolling();
  }

  private void radioBtnETHItemStateChanged(ItemEvent e) {
    panelAddress.setVisible(e.getStateChange() == ItemEvent.SELECTED);
  }

  private void thisWindowClosing(WindowEvent e) {
    close();
  }

  private void menuItemAboutActionPerformed(ActionEvent e) {
    new AboutDialog(this).setVisible(true);
  }
  
  private void menuItemHelpActionPerformed(ActionEvent e) {
    showHelp();
  }

  public void showHelp() {
    HelpWindow window = new HelpWindow();
    window.setLocationRelativeTo(this);
    window.setVisible(true);
  }

  private void panel5MouseClicked(MouseEvent e) {
    tableGPIO.clearSelection();
  }

  private void menuItemExitActionPerformed(ActionEvent e) {
    close();
  }

 
  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    JMenuBar menuBar1 = new JMenuBar();
    JMenu menuOption = new JMenu();
    menuItemExit = new JMenuItem();
    JMenu menuHelp = new JMenu();
    menuItemHelp = new JMenuItem();
    menuItemAbout = new JMenuItem();
    JTabbedPane tabbedPane1 = new JTabbedPane();
    JPanel panel2 = new JPanel();
    JPanel panel1 = new JPanel();
    JLabel label3 = new JLabel();
    tfDLLPath = new JTextField();
    JButton btnBrowse = new JButton();
    JPanel panel3 = new JPanel();
    JLabel label5 = new JLabel();
    JLabel label6 = new JLabel();
    JPanel panel6 = new JPanel();
    radioBtnCAN = new JRadioButton();
    radioBtnETH = new JRadioButton();
    panelAddress = new JPanel();
    JLabel label2 = new JLabel();
    tfIPAddress = new JTextField();
    JPanel panel4 = new JPanel();
    JPanel panel5 = new JPanel();
    btnOpenBoard = new JButton();
    btnCloseBoard = new JButton();
    btnInitIOMap = new JButton();
    btnInitIO = new JButton();
    btnReadGPIO = new JButton();
    btnWriteGPIO = new JButton();
    toggleBtnPolling = new JCheckBox();
    JLabel label4 = new JLabel();
    splitPane1 = new JSplitPane();
    JScrollPane scrollPane1 = new JScrollPane();
    JPopupMenu popupMenu1 = new JPopupMenu();
    JMenuItem menuItemInitIO = new JMenuItem();
    JMenuItem menuItemRead = new JMenuItem();
    JMenuItem menuItemWrite = new JMenuItem();

    //======== this ========
    setTitle("Gemini 3 Module Diagnostics");
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    setIconImage(new ImageIcon(getClass().getResource("/images/frameicon.png")).getImage());
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        thisWindowClosing(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== menuBar1 ========
    {

      //======== menuOption ========
      {
        menuOption.setText("Options");
        menuOption.setMnemonic('O');

        //======== menuLogLevel ========
        {
          menuLogLevel.setText("Log Level");
          menuLogLevel.setMnemonic('L');
        }
        menuOption.add(menuLogLevel);

        //---- menuItemExit ----
        menuItemExit.setText("Exit");
        menuItemExit.setMnemonic('E');
        menuItemExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_MASK));
        menuItemExit.addActionListener(e -> menuItemExitActionPerformed(e));
        menuOption.add(menuItemExit);
      }
      menuBar1.add(menuOption);

      //======== menuHelp ========
      {
        menuHelp.setText("Help");
        menuHelp.setMnemonic('H');

        //---- menuItemHelp ----
        menuItemHelp.setText("Help");
        menuItemHelp.setMnemonic('H');
        menuItemHelp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
        menuItemHelp.addActionListener(e -> menuItemHelpActionPerformed(e));
        menuHelp.add(menuItemHelp);

        //---- menuItemAbout ----
        menuItemAbout.setText("About");
        menuItemAbout.setMnemonic('A');
        menuItemAbout.addActionListener(e -> menuItemAboutActionPerformed(e));
        menuHelp.add(menuItemAbout);
      }
      menuBar1.add(menuHelp);
    }
    setJMenuBar(menuBar1);

    //======== tabbedPane1 ========
    {

      //======== panel2 ========
      {
        panel2.setBorder(Borders.TABBED_DIALOG);
        panel2.setLayout(new FormLayout(
          "default:grow",
          "3*(default, $ugap), fill:default:grow, $lgap, default"));

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(null, "Select DLL File (E.g. \"IOMTestIOManager.dll\")", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
            new Font("Tahoma", Font.BOLD, 11)));
          panel1.setLayout(new FormLayout(
            "default, $lcgap, [50dlu,default]:grow, $lcgap, default",
            "default"));

          //---- label3 ----
          label3.setText("DLL File Path:");
          panel1.add(label3, CC.xy(1, 1));

          //---- tfDLLPath ----
          tfDLLPath.setEditable(false);
          panel1.add(tfDLLPath, CC.xy(3, 1));

          //---- btnBrowse ----
          btnBrowse.setText("Browse...");
          btnBrowse.addActionListener(e -> btnBrowseActionPerformed(e));
          panel1.add(btnBrowse, CC.xy(5, 1));
        }
        panel2.add(panel1, CC.xy(1, 1));

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder(null, "Select Module", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
            new Font("Tahoma", Font.BOLD, 11)));
          panel3.setLayout(new FormLayout(
            "default, $lcgap, 50dlu, $ugap, default, $lcgap, 50dlu",
            "default"));

          //---- label5 ----
          label5.setText("Module:");
          panel3.add(label5, CC.xy(1, 1));
          panel3.add(combModule, CC.xy(3, 1));

          //---- label6 ----
          label6.setText("Module ID:");
          panel3.add(label6, CC.xy(5, 1));
          panel3.add(combModuleId, CC.xy(7, 1));
        }
        panel2.add(panel3, CC.xy(1, 3));

        //======== panel6 ========
        {
          panel6.setBorder(new TitledBorder(null, "Select Comms Mode", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
            new Font("Tahoma", Font.BOLD, 11)));
          panel6.setLayout(new FormLayout(
            "2*(default, $lcgap), default",
            "default"));

          //---- radioBtnCAN ----
          radioBtnCAN.setText("CAN (Kvaser)");
          panel6.add(radioBtnCAN, CC.xy(1, 1));

          //---- radioBtnETH ----
          radioBtnETH.setText("Ethernet (HAT)");
          radioBtnETH.setSelected(true);
          radioBtnETH.addItemListener(e -> radioBtnETHItemStateChanged(e));
          panel6.add(radioBtnETH, CC.xy(3, 1));

          //======== panelAddress ========
          {
            panelAddress.setLayout(new FormLayout(
              "default, $lcgap, [80dlu,default]",
              "default"));

            //---- label2 ----
            label2.setText("RTU IP Address:");
            label2.setHorizontalAlignment(SwingConstants.RIGHT);
            panelAddress.add(label2, CC.xy(1, 1));

            //---- tfIPAddress ----
            tfIPAddress.setText("0.0.0.0");
            panelAddress.add(tfIPAddress, CC.xy(3, 1));
          }
          panel6.add(panelAddress, CC.xy(5, 1));
        }
        panel2.add(panel6, CC.xy(1, 5));
      }
      tabbedPane1.addTab("Settings", panel2);

      //======== panel4 ========
      {
        panel4.setBorder(Borders.TABBED_DIALOG);
        panel4.setLayout(new BorderLayout());

        //======== panel5 ========
        {
          panel5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              panel5MouseClicked(e);
            }
          });
          panel5.setLayout(new FormLayout(
            "7*(default, $lcgap), default:grow",
            "min, $lgap, default, $lgap, 10dlu, $lgap, default, $lgap, min"));

          //---- separator4 ----
          separator4.setFont(new Font("Tahoma", Font.BOLD, 11));
          panel5.add(separator4, CC.xywh(1, 1, 15, 1));

          //---- btnOpenBoard ----
          btnOpenBoard.setText("Open Board");
          btnOpenBoard.addActionListener(e -> btnOpenBoardActionPerformed(e));
          panel5.add(btnOpenBoard, CC.xy(3, 3));

          //---- btnCloseBoard ----
          btnCloseBoard.setText("Close Board");
          btnCloseBoard.addActionListener(e -> btnCloseBoardActionPerformed(e));
          panel5.add(btnCloseBoard, CC.xy(5, 3));

          //---- btnInitIOMap ----
          btnInitIOMap.setText("Init IOMap");
          btnInitIOMap.addActionListener(e -> btnInitIOMapActionPerformed(e));
          panel5.add(btnInitIOMap, CC.xy(7, 3));

          //---- separator5 ----
          separator5.setFont(new Font("Tahoma", Font.BOLD, 11));
          panel5.add(separator5, CC.xywh(1, 5, 15, 1));

          //---- btnInitIO ----
          btnInitIO.setText("Init IO");
          btnInitIO.setToolTipText("Initialse the selected GPIO in the list.");
          btnInitIO.addActionListener(e -> btnInitIOActionPerformed(e));
          panel5.add(btnInitIO, CC.xy(3, 7));

          //---- btnReadGPIO ----
          btnReadGPIO.setText("Read IO");
          btnReadGPIO.setToolTipText("Read the selected GPIO in the list.");
          btnReadGPIO.addActionListener(e -> btnReadGPIOActionPerformed(e));
          panel5.add(btnReadGPIO, CC.xy(5, 7));

          //---- btnWriteGPIO ----
          btnWriteGPIO.setText("Write IO");
          btnWriteGPIO.setToolTipText("Write the selected GPIO in the list.");
          btnWriteGPIO.addActionListener(e -> btnWriteGPIOActionPerformed(e));
          panel5.add(btnWriteGPIO, CC.xy(7, 7));

          //---- toggleBtnPolling ----
          toggleBtnPolling.setText("Polling every:");
          toggleBtnPolling.setToolTipText("Enable/Disable Polling");
          toggleBtnPolling.addActionListener(e -> toggleBtnPollingActionPerformed(e));
          panel5.add(toggleBtnPolling, CC.xy(9, 7));

          //---- ftfPollingRate ----
          ftfPollingRate.setColumns(5);
          ftfPollingRate.setText("200");
          panel5.add(ftfPollingRate, CC.xy(11, 7));

          //---- label4 ----
          label4.setText("ms");
          panel5.add(label4, CC.xy(13, 7));
        }
        panel4.add(panel5, BorderLayout.PAGE_START);

        //======== splitPane1 ========
        {
          splitPane1.setOneTouchExpandable(true);
          splitPane1.setDividerLocation(400);
          splitPane1.setResizeWeight(0.1);
          splitPane1.setDividerSize(10);

          //======== scrollPane1 ========
          {

            //---- tableGPIO ----
            tableGPIO.setComponentPopupMenu(popupMenu1);
            scrollPane1.setViewportView(tableGPIO);
          }
          splitPane1.setLeftComponent(scrollPane1);
          splitPane1.setRightComponent(panelLog);
        }
        panel4.add(splitPane1, BorderLayout.CENTER);
      }
      tabbedPane1.addTab("Diagnositcs", panel4);
    }
    contentPane.add(tabbedPane1, BorderLayout.CENTER);

    //======== popupMenu1 ========
    {

      //---- menuItemInitIO ----
      menuItemInitIO.setText("Init IO");
      menuItemInitIO.addActionListener(e -> btnInitIOActionPerformed(e));
      popupMenu1.add(menuItemInitIO);

      //---- menuItemRead ----
      menuItemRead.setText("Read IO");
      menuItemRead.addActionListener(e -> btnReadGPIOActionPerformed(e));
      popupMenu1.add(menuItemRead);

      //---- menuItemWrite ----
      menuItemWrite.setText("Write IO");
      menuItemWrite.addActionListener(e -> btnWriteGPIOActionPerformed(e));
      popupMenu1.add(menuItemWrite);
    }

    //---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(radioBtnCAN);
    buttonGroup1.add(radioBtnETH);

    setSize(705, 655);
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
    
    prefs.loadSession(this);
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JMenu menuLogLevel;
  private JMenuItem menuItemExit;
  private JMenuItem menuItemHelp;
  private JMenuItem menuItemAbout;
  public JTextField tfDLLPath;
  public JComboBox<Integer> combModule;
  public JComboBox<Integer> combModuleId;
  public JRadioButton radioBtnCAN;
  public JRadioButton radioBtnETH;
  private JPanel panelAddress;
  public JTextField tfIPAddress;
  private JComponent separator4;
  public JButton btnOpenBoard;
  public JButton btnCloseBoard;
  private JButton btnInitIOMap;
  private JComponent separator5;
  private JButton btnInitIO;
  public JButton btnReadGPIO;
  private JButton btnWriteGPIO;
  public JCheckBox toggleBtnPolling;
  public JFormattedTextField ftfPollingRate;
  private JSplitPane splitPane1;
  private JTable tableGPIO;
  private JPanel panelLog;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

  private static class ModuleCellRenderer extends DefaultListCellRenderer{
    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      
      Integer module = (Integer) value;
      if(module != null && module >=0 && module <IModuleProtocol.MODULE_TYPES_STR.length)
        setText(IModuleProtocol.MODULE_TYPES_STR[module]);
      
      return comp;
    }
  }
  
  private static class ModuleIDCellRenderer extends DefaultListCellRenderer{
    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      
      Integer moduleId = (Integer) value;
      if(moduleId != null && moduleId >=0 && moduleId <IModuleProtocol.MODULE_IDS_STR.length)
        setText(IModuleProtocol.MODULE_IDS_STR[moduleId]);
      
      return comp;
    }
  }
}

