/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.exception;

/**
 *
 */
public class DLLFuncCallFailedException extends Exception {

  final public int errorCode;


  public DLLFuncCallFailedException(String functionName, int errorCode) {
    super(String.format("DLL function call failed! \"%s()\" returned error:%d", functionName, errorCode));
    this.errorCode = errorCode;
  }
}
