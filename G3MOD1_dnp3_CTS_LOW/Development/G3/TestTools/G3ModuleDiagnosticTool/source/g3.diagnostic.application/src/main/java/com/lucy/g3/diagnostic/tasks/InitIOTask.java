/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.tasks;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

import org.apache.log4j.Logger;

import com.lucy.g3.diagnostic.dll.DLLManager;
import com.lucy.g3.diagnostic.dll.IIOManagerDLL;
import com.lucy.g3.diagnostic.gpio.GPIO;
import com.lucy.g3.diagnostic.ui.GPIOListTableModel;
import com.lucy.g3.diagnostic.ui.MessageBox;
import com.lucy.g3.diagnostic.utils.UIUtilities;

/**
 * A task for calling DLL to initialise a list of GPIO.
 */
public class InitIOTask extends AbstractTask<Void, GPIO> {

  private Logger log = Logger.getLogger(InitIOTask.class);
  
  // private final GPIOManager ioMgr;
  private final DLLManager dllMgr;
  private final JComponent invoker;
  private final GPIOListTableModel dataModel;
  private final ArrayList<GPIO> iolist;
  protected final ArrayList<GPIO> failIolist = new ArrayList<>();
  
  public InitIOTask(JComponent invoker, GPIOListTableModel dataModel, DLLManager dllMgr, int... ioIDs) {
    super();
    this.invoker = invoker;
    this.dataModel = dataModel;
    this.dllMgr = dllMgr;
    this.iolist = dataModel == null ? new ArrayList<GPIO>() : dataModel.getIolist(ioIDs);
  }

  @Override
  protected Void doInBackground() throws Exception {
    IIOManagerDLL dll;
    short handle;
    int err;
    
    for (GPIO io : iolist) {
      if(isCancelled())
        break;
      
      dllMgr.checkReadyForTesting();
      dll = dllMgr.getDll();
      handle = dllMgr.getHandle();
        
      synchronized (dll) {
        try {
          if((err = dll.InitIOID(handle, io.getName())) == 0){
            io.setValue(null); // IO re-init, clear value.
            publish(io);
          } else {
            failIolist.add(io);
            log.error(String.format("Failed to initialise GPIO:%s with error:%s",io,err));
          }
        }catch(Throwable e) {
          failIolist.add(io);
          log.error(String.format("Failed to initialise GPIO:%s with exception: %s",io,e.getMessage()));
        }
      } 
      
      Thread.sleep(100);
    }
    
    return null;
  }

  @Override
  protected void process(List<GPIO> chunks) {
    for (GPIO io: chunks) {
      if(dataModel != null)
        dataModel.fireTableRowsUpdated(io.getId(), io.getId());
    }
  }

  @Override
  protected void done() {
    if(isCancelled()) 
      return;
    
    /* Show success dialog */
    if (failIolist.isEmpty()) {
      MessageBox.showSuccessDialog(invoker, "The selected IO have been initialised!");

    } else {
      /* Show failure dialog */
      MessageBox.showFailureDialog(invoker, failIolist, "Failed to initialise the following GPIO:");
    }
  }

  

}
