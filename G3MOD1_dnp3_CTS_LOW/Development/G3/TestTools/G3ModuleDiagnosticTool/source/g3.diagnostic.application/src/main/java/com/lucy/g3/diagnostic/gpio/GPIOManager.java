/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.gpio;

import java.util.ArrayList;


/**
 * Manager of GPIO list.
 */
public class GPIOManager {
  private final ArrayList<GPIO> iolist = new ArrayList<>();
  
  public void addAllGPIO(String[] gpioIDArray) {
    if(gpioIDArray == null)
      return;
    
    for (int i = 0; i < gpioIDArray.length; i++) {
      iolist.add(new GPIO(i, gpioIDArray[i]));
    }
  }
  
  public void clearAllGPIO() {
    iolist.clear();
  }
  
  public ArrayList<GPIO> getList() {
    return new ArrayList<GPIO>(iolist);
  }
}

