
package com.lucy.g3.diagnostic.tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JComponent;
import javax.swing.SwingWorker;

import org.apache.log4j.Logger;

import com.lucy.g3.diagnostic.dll.DLLManager;
import com.lucy.g3.diagnostic.dll.IIOManagerDLL;
import com.lucy.g3.diagnostic.gpio.GPIO;
import com.lucy.g3.diagnostic.ui.GPIOListTableModel;
import com.lucy.g3.diagnostic.ui.MessageBox;
import com.sun.jna.ptr.IntByReference;

public class GPIOPollingTask extends AbstractTask<Void, GPIO> {
  private Logger log = Logger.getLogger(GPIOPollingTask.class);
  
  private final DLLManager dllManager;
  private final GPIOListTableModel model;
  private int pollingRateMs;
  private final ArrayList<GPIO> ioList;
  protected final ArrayList<GPIO> failIolist = new ArrayList<>();
  private final JComponent invoker;
  
  public GPIOPollingTask(JComponent invoker, 
          GPIOListTableModel dataModel, 
          DLLManager dllManager, 
          int pollingRateMs, int... ioIDs) {
    
    this.invoker = invoker;
    this.dllManager = dllManager;
    this.model = dataModel;
    this.pollingRateMs = pollingRateMs;
    if(ioIDs.length == 0)
      this.ioList = model.getIolist();
    else {
      this.ioList = model.getIolist(ioIDs);;
    }
  }

  @Override
  protected Void doInBackground() throws Exception {
    int err;
    dllManager.checkReadyForTesting();
    
    IntByReference value = new IntByReference();
    IIOManagerDLL dll = dllManager.getDll();
    
    while (!isCancelled()) {
      failIolist.clear();
      
      for (GPIO io : ioList) {
        if(isCancelled())
          break;
        
        synchronized (dll) {
          try {
            io.setValue(null);
            err = dll.ReadIOID(dllManager.getHandle(), io.getName(), value);
            if (err == 0) {
              io.setValue(value.getValue());
              log.debug("Succeeded to read IO:" + io + " value:" + value.getValue());
              
            } else {
              log.error("Failed to read IO:" + io + " error:" + err);
              failIolist.add(io);
            }
          } catch (Throwable e) {
            log.fatal("Unexpected error:"+e.getMessage());
            failIolist.add(io);
          }
        }
      }
      
      publish(ioList.toArray(new GPIO[ioList.size()]));
      
      if(pollingRateMs >= 0)
        Thread.sleep(pollingRateMs);
      else
        break;
    }

    return null;
  }

  @Override
  protected void process(List<GPIO> chunks) {
    int rowCount = model.getRowCount();
    if(rowCount > 0)
      model.fireTableRowsUpdated(0, rowCount);
  }

  private boolean isPerodicTask() {
    return pollingRateMs >= 0;
  }
  
  @Override
  protected void done() {
    if(isCancelled())
      return;

    if(isPerodicTask()) {
      /* Keep silent if it is peroidc.*/
      try {
        get();
      } catch(ExecutionException e){
          String err = "Polling finished with error:\n" + e.getMessage();
          log.error(err);
      } catch (InterruptedException e) {}
      
    } else {
      /* Show result to user if it is a single command task(e.g. ReadGPIO once).*/
      if (failIolist.isEmpty()) {
        MessageBox.showSuccessDialog(invoker, "The selected IO have been read!");

      } else {
        /* Show failure dialog */
        MessageBox.showFailureDialog(invoker, failIolist, "Failed to read the following GPIO:");
      }
    }
  }

}