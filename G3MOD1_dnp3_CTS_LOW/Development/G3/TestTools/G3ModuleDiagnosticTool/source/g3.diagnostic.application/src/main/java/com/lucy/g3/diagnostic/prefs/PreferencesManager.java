/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.prefs;

import java.util.prefs.Preferences;

import com.lucy.g3.diagnostic.ui.MainFrame;


/**
 *
 */
public class PreferencesManager {
  private final Preferences prefs = Preferences.userNodeForPackage(PreferencesManager.class);
  
  public final static String KEY_FILE_CHOOSE_DIR ="fileChooseDir";
  
  private final static String KEY_DLL_PATH = "dllPath";
  private final static String KEY_RTU_IP_ADDRESS = "rtuIPAddress";
  private final static String KEY_MODULE = "module";
  private final static String KEY_MODULE_ID= "moduleID";
  private final static String KEY_COM_CAN= "comCAN";
  private final static String KEY_COM_ETH= "comETH";
  private final static String KEY_LOG_LEVEL= "logLevel";
  
  public static final PreferencesManager INSTANCE = new PreferencesManager();
  
  private PreferencesManager(){}
  
  public String getFileChooseDir() {
    return prefs.get(KEY_FILE_CHOOSE_DIR, System.getProperty("user.dir"));
  }
  
  public void saveFileChooseDir(String dir) {
    prefs.put(KEY_FILE_CHOOSE_DIR, dir);
  }
  
  public void saveLoggerLevel(String level){
    prefs.put(KEY_LOG_LEVEL, level);
  }
  
  public String getLoggerLevel(){
    return prefs.get(KEY_LOG_LEVEL, "");
  }
  
  public void saveSession(MainFrame ui) {
    prefs.put(KEY_DLL_PATH, ui.tfDLLPath.getText());
    prefs.put(KEY_RTU_IP_ADDRESS, ui.tfIPAddress.getText());
    prefs.putInt(KEY_MODULE, ui.combModule.getSelectedIndex());
    prefs.putInt(KEY_MODULE_ID, ui.combModuleId.getSelectedIndex());
    prefs.putBoolean(KEY_COM_CAN, ui.radioBtnCAN.isSelected());
    prefs.putBoolean(KEY_COM_ETH, ui.radioBtnETH.isSelected());
  }
  
  public void loadSession(MainFrame ui) {
    ui.tfDLLPath.setText(prefs.get(KEY_DLL_PATH, ""));
    ui.tfIPAddress.setText(prefs.get(KEY_RTU_IP_ADDRESS, ui.tfIPAddress.getText()));
    ui.combModule.setSelectedIndex(prefs.getInt(KEY_MODULE, ui.combModule.getSelectedIndex()));
    ui.combModuleId.setSelectedIndex(prefs.getInt(KEY_MODULE_ID, ui.combModuleId.getSelectedIndex()));
    ui.radioBtnCAN.setSelected(prefs.getBoolean(KEY_COM_CAN, ui.radioBtnCAN.isSelected()));
    ui.radioBtnETH.setSelected(prefs.getBoolean(KEY_COM_ETH, ui.radioBtnETH.isSelected()));
  }

}

