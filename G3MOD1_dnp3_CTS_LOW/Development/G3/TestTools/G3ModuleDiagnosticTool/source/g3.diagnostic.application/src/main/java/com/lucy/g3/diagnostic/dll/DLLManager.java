/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.dll;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.apache.log4j.Logger;

import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;



/**
 * This class is for managing the loaded DLL.
 */
public class DLLManager {
  public final static String PROPERTY_BOARD_OPEN = "boardOpen";
  
  private Logger log = Logger.getLogger(DLLManager.class);
  
  private IIOManagerDLL dll;
  private short handle = -1;   // Board handle
  private boolean boardOpen; 
  
  private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

  static {
    Logger log = Logger.getLogger(DLLManager.class);
    String candllpath = "lib\\canlib32.dll"; 
    try {
      NativeLibrary.getInstance(candllpath );
    }catch (Throwable e) {
      log.error("canlib32.dll not found: " + System.getProperty("user.dir") 
      + "\\"+candllpath);
    }
  }
  
  /**
   * 
   * @param path the path of the library.
   * @param libraryName the name of the library without suffix. E.g."IOMTestIOManager". 
   */
  public synchronized  IIOManagerDLL loadDLL(String path, String libraryName) throws UnsatisfiedLinkError{
    libraryName = removeFileExtension(libraryName);
    NativeLibrary.addSearchPath(libraryName, path);
    dll = (IIOManagerDLL) Native.loadLibrary(libraryName, IIOManagerDLL.class);
    return dll;
  }
  
  public IIOManagerDLL getDll() {
    return dll;
  }
  
  public short getHandle() {
    return handle;
  }
  
  public synchronized  void setHandle(short handle) {
    this.handle = handle;
    setBoardOpen(handle >= 0);
  }
  
  private void setBoardOpen(boolean boardOpen) {
    this.pcs.firePropertyChange(PROPERTY_BOARD_OPEN, this.boardOpen, this.boardOpen = boardOpen);
  }
  
  public boolean isBoardOpen() {
    return boardOpen;
  }
  
  private static String removeFileExtension(String fileName) {
    if(fileName != null && fileName.endsWith(".dll")) {
      int pos = fileName.lastIndexOf(".");
      if (pos > 0) {
          fileName = fileName.substring(0, pos);
      }
    }
    return fileName;
  }
  
  
  public void addPropertyChangeListener(PropertyChangeListener listener) {
    this.pcs.addPropertyChangeListener(listener);
  }

  public void removePropertyChangeListener(PropertyChangeListener listener) {
      this.pcs.removePropertyChangeListener(listener);
  }
  
  /**
   * Checks if the DLL is loaded and board is ready(open) for testing. 
   * @throws IllegalStateException if the board is not ready.
   */
  public synchronized void checkReadyForTesting() throws IllegalStateException{
    if (dll == null || handle < 0) 
      throw new IllegalStateException("The board is not ready!");
  }
}

