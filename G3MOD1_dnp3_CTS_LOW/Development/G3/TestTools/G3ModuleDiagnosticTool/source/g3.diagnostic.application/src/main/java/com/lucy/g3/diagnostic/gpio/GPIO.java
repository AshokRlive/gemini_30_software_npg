/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.gpio;


/**
 *
 */
public class GPIO {
  private final int id;
  private final String name;
//  private boolean isInput;
//  private boolean isoutput;
  private Integer value;
  
  public GPIO(int id, String name) {
    super();
    this.id = id;
    this.name = name;
  }
  
  public Integer getValue() {
    return value;
  }
  
  public void setValue(Integer value) {
    this.value = value;
  }

  
  public int getId() {
    return id;
  }
  
  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return String.format("[%-2d] %s", getId(), getName());
  }

}

