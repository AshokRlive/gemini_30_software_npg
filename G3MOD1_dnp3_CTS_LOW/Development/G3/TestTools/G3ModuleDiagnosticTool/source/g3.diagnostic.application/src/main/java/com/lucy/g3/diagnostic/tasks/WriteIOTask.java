/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.tasks;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.SwingWorker;

import org.apache.log4j.Logger;

import com.lucy.g3.diagnostic.dll.DLLManager;
import com.lucy.g3.diagnostic.dll.IIOManagerDLL;
import com.lucy.g3.diagnostic.gpio.GPIO;
import com.lucy.g3.diagnostic.ui.GPIOListTableModel;
import com.lucy.g3.diagnostic.ui.MessageBox;


/**
 *
 */
public class WriteIOTask extends AbstractTask<Void, GPIO> {
  private Logger log = Logger.getLogger(WriteIOTask.class);
  
  private final DLLManager dllManager;
  private final GPIOListTableModel model;
  private final JComponent invoker;
  
  private final ArrayList<GPIO> ioList;
  protected final ArrayList<GPIO> failIolist = new ArrayList<>();
  
  private final int newValue;
  
  public WriteIOTask(JComponent invoker, 
      GPIOListTableModel dataModel, 
      DLLManager dllManager, 
      int newValue,
      int... ioIDs) {
    this.invoker = invoker;
    this.dllManager = dllManager;
    this.model = dataModel;
    this.ioList = model.getIolist(ioIDs);
    this.newValue = newValue;
  }
  
  
  @Override
  protected Void doInBackground() throws Exception {
    IIOManagerDLL dll;
    short handle;
    int err;
    
    for (GPIO io : ioList) {
      if(isCancelled())
        break;
      
      dllManager.checkReadyForTesting();
      dll = dllManager.getDll();
      handle = dllManager.getHandle();
      
      
      synchronized (dll) {
        err = dll.WriteIOID(handle, io.getName(), newValue);
        if(err == 0) {
          publish(io);
          io.setValue(newValue);
        } else {
          log.error("Failed to write IO:" + io + " error:" + err);
          failIolist.add(io);
        }
      }
      
    }
    
    return null;
  }

  @Override
  protected void process(List<GPIO> chunks) {
    for (GPIO io: chunks) {
      if(model != null)
        model.fireTableRowsUpdated(io.getId(), io.getId());
    }
  }

  @Override
  protected void done() {
    if(isCancelled())
      return;
    
    /* Show result to user if it is a single command task(e.g. ReadGPIO once).*/
    if (failIolist.isEmpty()) {
      MessageBox.showSuccessDialog(invoker, "The selected IO have been written!");

    } else {
      /* Show failure dialog */
      MessageBox.showFailureDialog(invoker, failIolist, "Failed to write the following GPIO:");
    }
  }

}

