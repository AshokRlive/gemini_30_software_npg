/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.dll;

import java.io.File;
import java.net.URISyntaxException;


/**
 *
 */
public class DLLTestResources {
  private final static String DLL_FILE = "dll\\IOMTestIOManager.dll";
  
  public static File getDLLFile() {
    try {
      File file = new File(DLLTestResources.class.getClassLoader().getResource(DLL_FILE).toURI());
      return file;
    } catch (URISyntaxException e) {
      return null;
    }
  }

}

