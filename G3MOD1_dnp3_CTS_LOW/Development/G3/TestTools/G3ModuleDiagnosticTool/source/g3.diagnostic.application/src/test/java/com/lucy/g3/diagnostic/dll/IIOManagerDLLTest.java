/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.dll;

import java.io.File;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.diagnostic.dll.IIOManagerDLL;
import com.lucy.g3.diagnostic.utils.PlatformUtils;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;
import com.sun.jna.ptr.IntByReference;


/**
 *
 */
public class IIOManagerDLLTest extends DLLTestResources{
  private static Logger log = Logger.getLogger(IIOManagerDLLTest.class);
  
  public static final String PROPERTY_HOST_IP = "maven.test.rtu_ip";
  
  private final static String DEFAULT_HOST = "";//"10.11.11.102";
  private final static int COM_MODE = IIOManagerDLL.COM_MODE_ETH; 
  private final static int MODULE_TYPE = 5; //IOM
  private final static int MODULE_ID = 0; 
  
  private IIOManagerDLL dll = null;
  private static String host;
  
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    host = System.getProperty(PROPERTY_HOST_IP);
    if(Strings.isBlank(host))
      host = DEFAULT_HOST;
    log.info("Test RTU Address: " + host);
    log.info("Test JRE: " + PlatformUtils.getJREBits());
    
    /*
     * This unit test only work under 32bits cause the DLL is built by win32,
     * which means the JRE must be 32bit to load these DLLs. See JNA doc.
     */
    Assume.assumeTrue(PlatformUtils.checkJre32Bit());
    Assume.assumeTrue(!Strings.isBlank(host));

    
    //System.setProperty("jna.debug_load", "true");
    Native.setProtected(true);
    NativeLibrary.getInstance(".\\dll\\canlib32.dll");
    init();
  }
  
  @Before
  public void setUp() throws Exception {
    File dllFile = getDLLFile();
    NativeLibrary.addSearchPath(dllFile.getName(), dllFile.getParent());
    dll = (IIOManagerDLL) Native.loadLibrary(dllFile.getName(), IIOManagerDLL.class);
  }

  @After
  public void tearDown() throws Exception {
    
  }

  private static void init() {
    Native.setProtected(true);
        
    log.info("jna.library.path:"+System.getProperty("jna.library.path"));
    log.info("os.arch:"+System.getProperty("os.arch"));
    log.info("java.version: "+System.getProperty("java.vm.version"));
    log.info("java.home:"+System.getProperty("java.home"));
    log.info("java.compiler:"+System.getProperty("java.compiler"));
    log.info("java.vm.version"+System.getProperty("java.vm.version"));
    log.info("sun.arch.data.model"+System.getProperty("sun.arch.data.model"));
    log.info("java.class.path:"+System.getProperty("java.class.path"));
  }
  

  @Test 
  public void testInitAndOpen() {
    int err;
    
    short boardHandle = dll.InitAndOpenBoard(COM_MODE, host, MODULE_TYPE, MODULE_ID);
    if(boardHandle < 0)
      Assert.fail("InitAndOpenBoard failed with boardHandle:"+boardHandle);
    
    err = dll.CloseAndDeinitBoard(boardHandle);
    if(err != 0)
      Assert.fail("CloseAndDeinitBoard failed with err:"+err);
  }


  @Test 
  public void testGetAllIOIDStr() {
    String [] IOIDStringBuf = new String[100];
    int size = dll.GetAllIOIDStr(IOIDStringBuf, 100);
    System.out.println("Get String size: " + size);
    for (int i = 0; i < size && i < IOIDStringBuf.length; i++) {
      System.out.println(i+": "+IOIDStringBuf[i]);
    }
  }
  
  @Test @Ignore
  public void testWriteIO() {
    short handle = dll.InitAndOpenBoard(IIOManagerDLL.COM_MODE_ETH, host, MODULE_TYPE, MODULE_ID);
    dll.WriteIOID(handle, "IO_ID_DO1_LED", 1);
  }

  
  @org.junit.Ignore // Old API not working properly if canlib driver is not installed.
  @Test
  public void testOldAPI() {
    short boardHandle = openBoard(dll);
    
    IntByReference value = new IntByReference();
    value.setValue(123);
    log.info("Value Before: "+value.getValue());
    dll.ReadIOID(boardHandle, "IO_ID_AINPUT_A1", value);
    log.info("Value After: "+value.getValue());
    
    dll.WriteIOID(boardHandle, "IO_ID_DO1_LED", 1);
    dll.CloseBoard(boardHandle);
  }
  
  private short openBoard(IIOManagerDLL dll) {
    short boardHandle = -1;
    
    int err;
    if((err = dll.hat_client_init(host, 5000)) ==0) {
      log.info("Hat initialised");
      if((err = dll.CANCodecInitDll())==0) {
        log.info("CANCodec initialised");
        if((err = dll.OpenBoard(COM_MODE, MODULE_TYPE, MODULE_ID)) == 0){
          log.info("Board opened");
          boardHandle = (short) err;
        }else
          log.error("Board open error:"+err);
      } else
        log.error("CANCodec init error:"+err);
    }else {
      log.error("Hat init error:"+err);
    }
    return boardHandle;
  }
  
}

