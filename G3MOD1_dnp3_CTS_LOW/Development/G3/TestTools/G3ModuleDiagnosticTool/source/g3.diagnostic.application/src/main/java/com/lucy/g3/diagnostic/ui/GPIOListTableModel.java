/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.ui;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import com.lucy.g3.diagnostic.gpio.GPIO;

/**
 *
 */
public class GPIOListTableModel extends AbstractTableModel {

  private final static String COLUMN_NAMES[] = {
      "ID",
      "Name",
      "Value"
  };

  private final static int COLUMN_ID = 0;
  private final static int COLUMN_NAME = 1;
  private final static int COLUMN_VALUE = 2;

  private final ArrayList<GPIO> iolist = new ArrayList<>();
  

  public GPIOListTableModel() {
    super();
  }
  
  public void setList(ArrayList<GPIO> list) {
    iolist.clear();
    iolist.addAll(list);
    fireTableDataChanged();
  }
  
  
  public ArrayList<GPIO> getIolist() {
    return new ArrayList<>(iolist);
  }

  @Override
  public int getRowCount() {
    return iolist.size();
  }

  @Override
  public int getColumnCount() {
    return COLUMN_NAMES.length;
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    GPIO io = iolist.get(rowIndex);
    switch (columnIndex) {
    case COLUMN_ID:
      return io.getId();
    case COLUMN_NAME:
      return io.getName();
    case COLUMN_VALUE:
      Integer v = io.getValue();
      return v == null ? "?" : v;
    default:
      break;
    }
    return null;
  }

  @Override
  public String getColumnName(int column) {
    return COLUMN_NAMES[column];
  }

  public ArrayList<GPIO> getIolist(int[] ioIDs) {
    ArrayList<GPIO> list = new ArrayList<>();
    for (int i = 0; i < ioIDs.length; i++) {
      list.add(iolist.get(ioIDs[i]));
    }
    return list;
  }

}
