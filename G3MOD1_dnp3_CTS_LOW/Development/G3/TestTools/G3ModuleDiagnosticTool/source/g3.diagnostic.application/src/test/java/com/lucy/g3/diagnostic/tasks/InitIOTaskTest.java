/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.tasks;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.diagnostic.gpio.GPIO;
import com.lucy.g3.diagnostic.tasks.InitIOTask;


/**
 *
 */
public class InitIOTaskTest {

  @Ignore // Manual GUI test.
  @Test
  public void testDone() {
    InitIOTask task = new InitIOTask(null, null, null);
    for (int i = 0; i < 100; i++) {
      task.failIolist.add(new GPIO(i, "GPIO "+i));
    }
    task.done();
  }

}

