/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.main;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.lucy.g3.diagnostic.prefs.PreferencesManager;
import com.lucy.g3.diagnostic.ui.LoggerAppender;
import com.lucy.g3.diagnostic.ui.MainFrame;
import com.sun.jna.Callback;
import com.sun.jna.Callback.UncaughtExceptionHandler;
import com.sun.jna.Native;

public class Main {
  
  /* Main Entry*/
  public static void main(String[] args) {
    initLogger();
    initJNA();
    printEnv();
    launchUI();
  }

  private static void initLogger() {
    String levelStr = PreferencesManager.INSTANCE.getLoggerLevel();
    Level level = Level.toLevel(levelStr);
    Logger root = Logger.getRootLogger();
    root.setLevel(level);
    root.addAppender(new LoggerAppender());
  }

  private static void launchUI() {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        try {
          UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Throwable e) {
        }

        String jrebits = System.getProperty("sun.arch.data.model");
        if(jrebits.equals("32")) {
          MainFrame mainUI = new MainFrame();
          mainUI.setVisible(true);
          mainUI.showHelp();
        } else {
          String err = "32 bits JRE required!";
          Logger.getLogger(Main.class).error(err);
          JOptionPane.showMessageDialog(null, err, "Error", JOptionPane.ERROR_MESSAGE);
        }
      }
    });
  }

  private static void initJNA() {
    Native.setProtected(true);

    Native.setCallbackExceptionHandler(new UncaughtExceptionHandler() {

      @Override
      public void uncaughtException(Callback c, Throwable e) {
        Logger.getLogger(Main.class).error("uncaughtException, callback:" + c, e);
      }
    });
  }

  private static void printEnv() {
    final Logger log = Logger.getLogger("Main");
    log.info("Launching application with system properties:");

    String properties[] = {
        "jna.library.path",
        "os.arch",
        "java.vm.version",
        "java.home",
        "java.compiler",
        "java.vm.version",
        "sun.arch.data.model"
    };
    for (int i = 0; i < properties.length; i++) {
      log.debug(properties[i] + ":" + System.getProperty(properties[i]));
    }
  }
}