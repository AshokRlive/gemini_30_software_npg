/*****************************************************P**************************
 * Lucy Electric: http://www.lucyelectric.com/ - AutomaENtion Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.tasks;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;

import com.lucy.g3.diagnostic.dll.IIOManagerDLL;
import com.lucy.g3.diagnostic.exception.DLLFuncCallFailedException;
import com.lucy.g3.diagnostic.exception.SettingException;
import com.lucy.g3.diagnostic.gpio.GPIOManager;
import com.lucy.g3.diagnostic.ui.MainFrame;
import com.lucy.g3.diagnostic.ui.MessageBox;


/**
 *
 */
public class BoardOpenCloseTask extends AbstractTask<Short, Void> {
  /**
   * IP address regx pattern.
   */
  private static final String IP_PATTERN =
      "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." 
          + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." 
          + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." 
          + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

  private static final int MAX_GPIO_NUM = 100;

  private Logger log = Logger.getLogger(BoardOpenCloseTask.class);
  
  public final static int OPEN = 0;
  public final static int CLOSE = 1;
  
  private final int operation ;
  private final MainFrame ui;
  private final Integer comMode, module, moduleId;
  private final String rtuIPAddress;
  private final IIOManagerDLL dll; 
  
  private final GPIOManager ioMgr;
  
  public BoardOpenCloseTask(MainFrame ui, int operation, GPIOManager ioMgr) {
    this.operation = operation;
    this.ui = ui;
    this.ioMgr = ioMgr;
    
    comMode = ui.radioBtnCAN.isSelected() 
        ? IIOManagerDLL.COM_MODE_CAN : IIOManagerDLL.COM_MODE_ETH;
    rtuIPAddress = ui.tfIPAddress.getText();
    module = (Integer)ui.combModule.getSelectedItem();
    moduleId = (Integer)ui.combModuleId.getSelectedItem();
    dll = ui.dllManager.getDll();

  }

  @Override
  protected Short doInBackground() throws Exception {
    if (dll == null) {
      throw new SettingException("DLL not loaded yet!");
    }
    
    short handle = -1;
    short err;

    synchronized (dll) {
    if (operation == OPEN) {
      checkSettingsForOpen();
      err = dll.InitAndOpenBoard(comMode, rtuIPAddress, module, moduleId);
      if(err < 0)
        throw new DLLFuncCallFailedException("InitAndOpenBoard", handle);
      else
        handle = err;

    } else if (operation == CLOSE) {
      if((err = dll.CloseAndDeinitBoard(ui.dllManager.getHandle())) != 0) {
        throw new DLLFuncCallFailedException("CloseAndDeinitBoard", err);
      }
    }
    }
    
    // Update GPIO list
    if(operation == OPEN && handle >= 0) 
      readGPIOList(dll);
    else 
      clearGPIOList();

    return handle;
  }

  private void checkSettingsForOpen() throws Exception {
    if (comMode == null) {
      throw new SettingException("Comms mode not selected yet!");
    }
    
    if (module == null) {
      throw new SettingException("Module not selected yet!");
    }
    
    if (moduleId == null) {
      throw new SettingException("Module ID not selected yet!");
    }
    
    if (rtuIPAddress == null || !rtuIPAddress.matches(IP_PATTERN)) {
      throw new SettingException("RTU IP Address is invalid!");
    }
  }

  private void clearGPIOList() {
    ioMgr.clearAllGPIO();
  }

  private void readGPIOList(IIOManagerDLL dll) {
    String[] IOIDStringBuf = new String[MAX_GPIO_NUM];
    int size = dll.GetAllIOIDStr(IOIDStringBuf, 100);
    size--; // Remove the last ID "IO_ID_LAST"
    
    if (size > MAX_GPIO_NUM)
      size = MAX_GPIO_NUM;
    
    if (size >= 0)
      ioMgr.addAllGPIO(Arrays.copyOf(IOIDStringBuf, size));
  }
  
  @Override
  protected void done() {
    if(isCancelled())
      return;
    
    try {
      short handle = get();
      this.ui.updateGPIOList(ioMgr.getList());
      ui.dllManager.setHandle(handle);
    } catch (InterruptedException e) {
      log.warn("Board open/close task was interrupted!");
    } catch (ExecutionException e) {
        log.error(e.getCause().getMessage());
        MessageBox.showFailureDialog(this.ui.getRootPane(), e.getCause().getMessage());
    } 
  }
  
}

