/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.diagnostic.dll;

import com.sun.jna.Library;
import com.sun.jna.ptr.IntByReference;

/**
 * <p>The interface of XXXTestIOMananger DLL.</p> E.g. "IOMIOManagerDLL.dll".
 */
public interface IIOManagerDLL extends Library {
  int COM_MODE_CAN = 0;
  int COM_MODE_ETH = 1;
  
  
  /**
   * Wrapper API for simplifying init process.
   * @param comMode 0: CAN 1: ETH(HAT)
   */
  short InitAndOpenBoard(int comMode, String rtuIPAddress, int module, int moduleId);

  /**
   * Wrapper API for simplifying de-init process.
   * @return boardHandle handle return from {@link #InitAndOpenBoard(int, String, int, int)}.
   */
  short CloseAndDeinitBoard(short boardHandle);
  
  /**
   * Gets all available IO id as strings.
   * @param IOIDStringBuf for storing all GPIO id.
   * @param size the size of the buffer.
   * @return the actual size in the buffer.
   */
  int GetAllIOIDStr(String[] IOIDStringBuf, int size);
  
  /**
   * Initialises CAN Codec.
   * @return handle Codec handle.
   * @deprecated replaced by {@link #InitAndOpenBoard(int, String, int, int)}.
   */
  @Deprecated
  int CANCodecInitDll();
  
  /**
   * De-initialises CAN Codec.
   * @param handle the handle return from {@link #CANCodecInitDll()} .
   * @return
   * @deprecated replaced by {@link #InitAndOpenBoard(int, String, int, int)}.
   */
  @Deprecated
  int CANCodecDeInitDll(int handle);

  /**
   * Initialises HAT client.
   * @param rtuIpAddress
   * @param port
   * @return
   * @deprecated replaced by {@link #InitAndOpenBoard(int, String, int, int)}.
   */
  @Deprecated
  int hat_client_init(String rtuIpAddress, int port);
  
  /**
   * 
   * @param handle Board handle.
   * @return error code.
   */
  int InitIOMAP(short handle);
 
  /**
   * Initialise one IO. 
   * @param handle Board handle.
   * @param IOIDString IO ID as string.
   * @return error code.
   */
  int InitIOID(short handle, String IOIDString);

  /**
  * Open a board. 
  * @param comMode 0: CAN 1: ETH(HAT)
  * @param module module type.
  * @param moduleId module ID.
  * @return board handle.
  * @deprecated replaced by {@link #InitAndOpenBoard(int, String, int, int)}.
  */
  @Deprecated
  short OpenBoard(int comMode, int module, int moduleId);
  
  /**
   * Open a board. 
   * @param handle board handle.
   * @return error code.
   * @deprecated replaced by {@link #InitAndOpenBoard(int, String, int, int)}.
   */
  @Deprecated
  short CloseBoard(short handle);
  
  /**
   * Reads one GPIO. 
   * @param handle Board handle.
   * @param IOIDString IO ID as string.
   * @param valueToBeRead object for storing the value to be read.
   * @return error code.
   */
  int ReadIOID(short handle, String IOIDString, IntByReference valueToBeRead);
  
  /**
   * Writes one GPIO. 
   * @param handle Board handle.
   * @param IOIDString IO ID as string.
   * @param valueToBeWritten the value to be written.
   * @return error code.
   */
  int WriteIOID(short handle, String IOIDString, int valueToBeWritten);

}
