/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *  20/05/14      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>
#include <math.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "TestIOManager.h"
#include "CPLDDesignVerifier.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
*/

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
typedef struct LineAndLengthDef
{
	lu_int8_t  *line;
	lu_uint16_t length;
}LineAndLengthStr;

typedef struct InputTableDef
{
	lu_int8_t  *IOID;
	lu_int32_t value;
}InputTableStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR IOIDFetcherFromCPLDEquation( FILE *filePtr,
									  const lu_int8_t terminationChar, 
									  LineAndLengthStr *IOIDString
									);

lu_uint16_t GetNumberOfInputs(FILE *CPLDEquationFile);

void InputSequeneGenerator(lu_uint8_t power);

lu_bool_t CheckCPLDInputCombiantion( lu_uint16_t handle,
									 lu_uint8_t outputCheck,	
									 lu_uint16_t numberOfInputs,
									 InputTableStr *CPLDInputTable,
									 lu_int8_t *outputIOID
								   );

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

lu_int8_t oneLineScan[100];

InputTableStr *CPLDInputTable;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR VerifyDSMCPLDEquation( lu_uint16_t handle, 
								lu_int8_t *booleanEquationFile, 
								lu_int8_t outputCheck,
								lu_int8_t *testFile
							  )
{
	return VerifyCPLDOutput( handle, 
							 booleanEquationFile, 
							 outputCheck,
							 testFile
						   );
}

SB_ERROR VerifyCPLDOutput( lu_uint16_t handle, 
						   lu_int8_t *booleanEquationFile, 
						   lu_uint8_t outputCheck,
						   lu_int8_t *testFile
						 )
{
	FILE *filePtr, *checkPtr;

	SB_ERROR ret = SB_ERROR_NONE;

	lu_int8_t *output, binaryString[16], tenBitString[16];
	lu_uint16_t i = 0, j, numberOfInputs, numberOfSequence, length;
	lu_bool_t validCombination;
	LineAndLengthStr ioIDInput;

	filePtr  = fopen(booleanEquationFile, "r");
	checkPtr = fopen(testFile, "w");
	
	if(filePtr == NULL)
	{
		ret = SB_ERROR_PARAM;
	}

	if(ret == SB_ERROR_NONE)
	{
		numberOfInputs = GetNumberOfInputs(filePtr);
		numberOfInputs = numberOfInputs - 1;

		WriteIOID( handle,
				   "IO_ID_PULL_UP_DN",
				   0
				 );

		if( numberOfInputs > 1 )
		{
			CPLDInputTable = (InputTableStr*) malloc(numberOfInputs * sizeof(InputTableStr));
			
			IOIDFetcherFromCPLDEquation(filePtr, '=', &ioIDInput);
			output = (lu_int8_t*)malloc(ioIDInput.length);
			strcpy(output, ioIDInput.line); 
			memset(ioIDInput.line, '\0', ioIDInput.length);

			for(i = 0; i < numberOfInputs; i++)
			{
				IOIDFetcherFromCPLDEquation(filePtr, '.', &ioIDInput);
				CPLDInputTable[i].IOID = (lu_int8_t*)malloc(ioIDInput.length);
				strcpy(CPLDInputTable[i].IOID, ioIDInput.line);
				memset(ioIDInput.line, '\0', ioIDInput.length);
				//fprintf(checkPtr,"%s\n",CPLDInputTable[i].IOID);
			}

			/* Generate the n-bit binary sequence and apply to CPLD inputs */
			numberOfSequence = pow(2.0, numberOfInputs);
			
			for(i = 0; i < numberOfSequence; i++ )
			{
				length = strlen(itoa(i, binaryString, 2));
				
				for(j = 0; j < numberOfInputs - length; j++)
				{
					tenBitString[j] = '0';
				}
				
				tenBitString[j] = '\0';
								
				strcat(tenBitString,binaryString);

				for(j = 0; j < numberOfInputs; j++)
				{
					CPLDInputTable[j].value = (lu_int32_t)tenBitString[j] - 0x30;
				}

				fprintf(checkPtr,"\n\t---------------- %d --------------------\n", i);

				for(j = 0; j < numberOfInputs;  j++)
				{
					//fprintf(checkPtr,"%c - ",tenBitString[j]);
					fprintf(checkPtr,"%d - ",CPLDInputTable[j].value);
					fprintf(checkPtr,"%s\n" ,CPLDInputTable[j].IOID);
				}

				validCombination = CheckCPLDInputCombiantion( handle,
														      outputCheck,	
															  numberOfInputs,
															  CPLDInputTable,
															  output
															);
				if(validCombination == LU_TRUE)
				{
					fprintf(checkPtr,"\n\t****************--> %d <--*************************\n", i);
					validCombination = LU_FALSE;
				}
			}

			ret = SB_ERROR_NONE;
		}
		
		fclose(filePtr);
		fclose(checkPtr);
	}

	return ret;
}

lu_bool_t CheckCPLDInputCombiantion( lu_uint16_t handle,
									 lu_uint8_t outputCheck,	
									 lu_uint16_t numberOfInputs,
									 InputTableStr *CPLDInputTable,
									 lu_int8_t *outputIOID
								   )
{
	lu_uint16_t i;
	lu_uint8_t valueRead;

	for(i = 0; i < numberOfInputs; i++)
	{
		WriteIOID( handle,
				   CPLDInputTable[i].IOID,
				   CPLDInputTable[i].value
				 );
	}

	Sleep(5000);

	ReadIOID( handle, 
			  outputIOID, 
			  (lu_uint32_t*)&valueRead
			);

	if( valueRead == outputCheck)
	{
		return LU_TRUE;
	}
	
	return LU_FALSE;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
/*!
 ******************************************************************************
 *   \brief To fetch and store IOIDs from CPLD Equation file
 *
 *   Detailed description
 *
 *   \filePtr			CPLD Equation file pointer.
 *	 \terminationChar	Line termination character.
 *	 \IOIDString	    Structure contains line and length of IOID.
 *
 *   \return		    SB_ERROR
 ******************************************************************************
 */
SB_ERROR IOIDFetcherFromCPLDEquation( FILE *filePtr, 
									  const lu_int8_t terminationChar,
									  LineAndLengthStr *IOIDString
									)
{
	SB_ERROR ret;
	lu_uint16_t i = 0;
	lu_int32_t c =  NULL;

	while( c != terminationChar && c != EOF)
	{
		c = fgetc(filePtr);
		if( (c != terminationChar) && (c != '\n')) 
		{
			oneLineScan[i] = (lu_int8_t)c;
			i++;
		}
	}

	if(c != EOF)
	{
		IOIDString->line   = &oneLineScan[0];
		IOIDString->length = i; 
		ret = SB_ERROR_NONE;
	}
	else
	{
		ret = SB_ERROR_OFFLINE;
	}

 	return ret;
}

/*!
 ******************************************************************************
 *   \brief To get number of lines in the CPLD equation file
 *
 *   Detailed description
 *
 *   \CPLDEquationFile	CPLD Equation file pointer.
 *
 *   \return		    number of lines in CPLD Equation file.
 ******************************************************************************
 */
lu_uint16_t GetNumberOfInputs(FILE *CPLDEquationFile)
{
	lu_int32_t c;
	lu_uint16_t numberOfLines = 0;

	do 
	{
		c = fgetc(CPLDEquationFile);
		if( (c == '.') || (c == '=') )
		{
			c = fgetc(CPLDEquationFile);
			if(c == '\n')
			{
    			numberOfLines++;
			}
		}
	}while (c != EOF);

	rewind(CPLDEquationFile);

	return numberOfLines;
}

/*
 *********************** End of file ******************************************
 */