cd protocol\CanKing\LucyCanSend\
rd /Q /S buildCmdTool
md buildCmdTool
cd ../../../
pause

cd LabviewTesting\DSM\
rd /Q /S build
md build
cd ../../
pause

cd LabviewTesting\FDM\
rd /Q /S build
md build
cd ../../
pause

cd LabviewTesting\FPM\
rd /Q /S build
md build
cd ../../
pause

cd LabviewTesting\FPMV2\
rd /Q /S build
md build
cd ../../
pause

cd LabviewTesting\HMI\
rd /Q /S build
md build
cd ../../
pause

cd LabviewTesting\IOM\
rd /Q /S build
md build
cd ../../
pause

cd LabviewTesting\MCM\
rd /Q /S build
md build
cd ../../
pause

cd LabviewTesting\PSM\
rd /Q /S build
md build
cd ../../
pause

cd LabviewTesting\SCM\
rd /Q /S build
md build
cd ../../
pause

cd LabviewTesting\SCMMK2\
rd /Q /S build
md build
cd ../../
pause
