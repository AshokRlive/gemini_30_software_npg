/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1577 2012-07-23 20:18:25Z fryers_j $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1577 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-07-23 21:18:25 +0100 (Mon, 23 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/11/14     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "ADE78xxI2CAPI.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static lu_uint32_t *readRegValue;
TestI2cADE78xxReadPeakRespStr *peakRegResp;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR FPMV2ADE78xxI2CEnable( MODULE                 module, 
							    MODULE_ID              moduleId,
								TestI2cADE78xxInitStr  *fpiChanInitParams
						     )
{
	SB_ERROR retError;
	TestI2cADE78xxInitStr ade78xxI2cParam;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_I2C_ADE78XX_INIT_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	ade78xxI2cParam.i2cChan            = fpiChanInitParams->i2cChan;
	ade78xxI2cParam.sspBus             = fpiChanInitParams->sspBus;
	ade78xxI2cParam.csPort             = fpiChanInitParams->csPort;
	ade78xxI2cParam.csPin              = fpiChanInitParams->csPin;
	ade78xxI2cParam.pinMap.pm0.port    = fpiChanInitParams->pinMap.pm0.port;
	ade78xxI2cParam.pinMap.pm0.pin     = fpiChanInitParams->pinMap.pm0.pin;
	ade78xxI2cParam.pinMap.pm1.port    = fpiChanInitParams->pinMap.pm1.port;
	ade78xxI2cParam.pinMap.pm1.pin     = fpiChanInitParams->pinMap.pm1.pin;
	ade78xxI2cParam.pinMap.reset.port  = fpiChanInitParams->pinMap.reset.port;
	ade78xxI2cParam.pinMap.reset.pin   = fpiChanInitParams->pinMap.reset.pin;
	ade78xxI2cParam.pinMap.irq0.port   = fpiChanInitParams->pinMap.irq0.port;
	ade78xxI2cParam.pinMap.irq0.pin    = fpiChanInitParams->pinMap.irq0.pin;
	ade78xxI2cParam.pinMap.irq1.port   = fpiChanInitParams->pinMap.irq1.port;
	ade78xxI2cParam.pinMap.irq1.pin    = fpiChanInitParams->pinMap.irq1.pin;
	ade78xxI2cParam.pinMap.cf1.port    = fpiChanInitParams->pinMap.cf1.port;
	ade78xxI2cParam.pinMap.cf1.pin     = fpiChanInitParams->pinMap.cf1.pin;
	ade78xxI2cParam.pinMap.cf2.port    = fpiChanInitParams->pinMap.cf2.port;
	ade78xxI2cParam.pinMap.cf2.pin     = fpiChanInitParams->pinMap.cf2.pin;
		
	CANMsgFiller((lu_uint8_t*)&ade78xxI2cParam,sizeof(TestI2cADE78xxInitStr));

	retError = LucyCANSendMainDll();
		
	return retError;
}

SB_ERROR FPMV2TestI2CADE78xxReadPeak( MODULE       module, 
								      MODULE_ID    moduleId,
								      lu_uint8_t   fpiChan,
								      lu_uint8_t   peakChan,
									  lu_uint32_t  *peakValuePtr
							        )
{
	SB_ERROR retError;
	TestI2cADE78xxReadPeakStr peakParam;		

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST_1;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_1_I2C_ADE78xx_READ_PEAK_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	peakParam.fpiChan  = fpiChan;
	peakParam.peakChan = peakChan;

	CANMsgFiller((lu_uint8_t*)&peakParam, sizeof(TestI2cADE78xxReadPeakStr));

	retError = LucyCANSendMainDll();

	if(retError == SB_ERROR_NONE)
	{
		*peakValuePtr = peakRegResp->peakValue;
	}

	return retError;
}

SB_ERROR FPMV2ADE78xxI2CReadReg( MODULE       module, 
							     MODULE_ID    moduleId,
							     lu_uint8_t   i2cChan,
							     lu_uint16_t  reg,
							     lu_uint8_t   length,
							     lu_uint32_t  *readValuePtr
						       )
{
	SB_ERROR retError;
	TestI2cADE78xxReadRegStr readRegParam;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_I2C_ADE78XX_READ_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	readRegParam.i2cChan = i2cChan;
	readRegParam.reg     = reg;
	readRegParam.length  = length;
	readRegParam.value   = 0;
		
	CANMsgFiller((lu_uint8_t*)&readRegParam,sizeof(TestI2cADE78xxReadRegStr));

	retError = LucyCANSendMainDll();

	Sleep(50);

	*readValuePtr = *readRegValue;
	
	return retError;
}

SB_ERROR FPMV2ADE78xxI2CWriteReg( MODULE       module, 
								  MODULE_ID    moduleId,
								  lu_uint8_t   i2cChan,
							      lu_uint16_t  reg,
							      lu_uint8_t   length,
							      lu_uint32_t  value
						        )
{
	SB_ERROR retError;
	TestI2cADE78xxWriteRegStr writeRegParam;
	lu_uint16_t byteCount = 0;

	moduleId       = 0;
	
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_I2C_ADE78XX_WRITE_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	writeRegParam.i2cChan = i2cChan;
	writeRegParam.reg     = reg;
	writeRegParam.length  = length; 
	writeRegParam.value   = value;
		
	memcpy(&argsFromCommand.msgBuff[0], &writeRegParam, sizeof(TestI2cADE78xxWriteRegStr));
	
	argsFromCommand.msgBuffLen = sizeof(TestI2cADE78xxWriteRegStr);

	retError = LucyCANSendMainDll();
		
	return retError;
}

SB_ERROR FPMV2ADE78xxI2CTestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_PARAM;
		
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_SINGLE_COMMAND)
	{
		switch (msgPtr->messageType)
		{
		case MODULE_MSG_TYPE_BLTST:
			switch(msgPtr->messageID)
			{
				case MODULE_MSG_ID_BLTST_I2C_ADE78XX_INIT_R: 
					if(msgPtr->msgLen == sizeof(lu_uint8_t))
						{
							retError = (SB_ERROR)(*msgPtr->msgBufPtr);
						}
					break;

				case MODULE_MSG_ID_BLTST_I2C_ADE78XX_READ_R: 
					if(msgPtr->msgLen == sizeof(lu_uint32_t))
						{
							readRegValue = (lu_uint32_t*)msgPtr->msgBufPtr;
							retError = SB_ERROR_NONE;
						}
					break;

				case MODULE_MSG_ID_BLTST_I2C_ADE78XX_WRITE_R: 
					if(msgPtr->msgLen == sizeof(lu_uint8_t))
						{
							retError = (SB_ERROR)(*msgPtr->msgBufPtr);
						}
					break;

				default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
					break;
			}
			break;

			case MODULE_MSG_TYPE_BLTST_1:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_BLTST_1_I2C_ADE78xx_READ_PEAK_R: 
						if(msgPtr->msgLen == sizeof(TestI2cADE78xxReadPeakRespStr))
							{
								peakRegResp = (TestI2cADE78xxReadPeakRespStr*)msgPtr->msgBufPtr;
								retError = SB_ERROR_NONE;
							}
						break;
					
					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;

			default:
					retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	  }
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}
	
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
*/