/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol global definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/11/14      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
/*!
 ******************************************************************************
 *   \brief Configuring ADE78xx Chip
 *
 *   Start / Stop ADE78xx Chip
 *
 *   \module			Module Type
 *   \moduleId			Module ID
 *	 \sspBus			SSP bus connected to ADE78xx
 *	 \csPort			Chip Select Port
 *	 \csPin				Chip Select Pin
 *   \gainRegAI         Gain value of AIGAIN reg
 *   \gainRegBI			Gain value of BIGAIN reg
 *	 \gainRegCI			Gain value of CIGAIN reg
 *
 *   \return			SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR FPMV2TestConfigureADE78xx(  MODULE       module, 
									 MODULE_ID    moduleId,
									 lu_uint8_t   sspBus, 
									 lu_uint8_t   csPort,
									 lu_uint8_t   csPin,
									 lu_int32_t   gainRegAI,
									 lu_int32_t   gainRegBI,
									 lu_int32_t   gainRegCI
								  );

/*!
 ******************************************************************************
 *   \brief Start / Stop ADE78xx Chip
 *
 *   Start / Stop ADE78xx Chip
 *
 *   \module			Module Type
 *   \moduleId			Module ID
 *	 \sspBus			SSP bus connected to ADE78xx
 *	 \csPort			Chip Select Port
 *	 \csPin				Chip Select Pin
 *	 \run               Start or Stop bit for ADE768xx
 * 
 *   \return			SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR FPMV2TestStartADE78xx( MODULE       module, 
								MODULE_ID    moduleId,
								lu_uint8_t sspBus, 
								lu_uint8_t csPort,
								lu_uint8_t csPin,
								lu_uint16_t run
							  );

/*!
 ******************************************************************************
 *   \brief Reading Irms value from ADE78xx 
 *
 *   Reading Irms value from ADE78xx  
 *
 *   \module			Module Type
 *   \moduleId			Module ID
 *	 \sspBus			SSP bus connected to ADE78xx
 *	 \csPort			Chip Select Port
 *	 \csPin				Chip Select Pin
 *	 \Phase             Phase of the Irms Value [ 1 -> A, 2 -> B, 3 -> C, 4 -> N ]
 * 
 *   \return			SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR FPMV2TestReadIrms( MODULE     module, 
							MODULE_ID  moduleId,
							lu_uint8_t sspBus, 
							lu_uint8_t csPort,
							lu_uint8_t csPin,
							lu_uint8_t phase,
							lu_uint32_t *IrmsPtr
						  );

/*
*********************** End of file ******************************************
 */