/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol global definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/11/14      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Enabling SPI and Initializing ADE78xx
 *
 *   Enabling SPI bus and initialization of ADE78xx chip
 *
 *   \module			Module Type
 *	 \moduleId			Module ID
 *	 \sspBus			SSP bus connected to ADE78xx
 *	 \csPort			Chip Select Port
 *	 \csPin				Chip Select Pin
 *	 \pm0Port			PM0 Port
 *	 \pm0Pin			PM0 Pin
 *	 \pm1Port			PM1 Port
 *	 \pm1Pin			PM1 Pin
 *	 \resetPort			Reset Port of ADE78xx
 *	 \resetPin			Reset Pin of ADE78xx
 *	 \irq0Port			IRQ0 Port of ADE78xx
 *	 \irq0Pin			IRQ0 Pin of ADE78xx
 *	 \irq1Port			IRQ1 Port of ADE78xx
 *	 \irq1Pin			IRQ1 Pin of ADE78xx
 *	 \cf1Port			CF1 Port of ADE78xx
 *	 \cf1Pin			CF1 Pin of ADE78xx
 *	 \cf2Port			CF2 Port of ADE78xx
 *	 \cf2Pin			CF2 Pin of ADE78xx
 *	 \return			SB_ERROR
 * 
 ******************************************************************************
 */
SB_ERROR FPMV2ADE78xxSPIEnable( MODULE      module, 
							    MODULE_ID   moduleId,
								lu_uint8_t  sspBus,
								lu_uint8_t  csPort,
								lu_uint8_t  csPin,
								lu_uint8_t  pm0Port,
								lu_uint8_t  pm0Pin,
								lu_uint8_t  pm1Port,
								lu_uint8_t  pm1Pin,
								lu_uint8_t  resetPort,
								lu_uint8_t  resetPin,
								lu_uint8_t  irq0Port,
								lu_uint8_t  irq0Pin,
								lu_uint8_t  irq1Port,
								lu_uint8_t  irq1Pin,
								lu_uint8_t  cf1Port,
								lu_uint8_t  cf1Pin,
								lu_uint8_t  cf2Port,
								lu_uint8_t  cf2Pin
						     );
 /*!
 ******************************************************************************
 *   \brief Reading ADE78xx register
 *
 *   To read registers of ADE78xx 
 *
 *   \module			Module Type
 *   \moduleId			Module ID
 *	 \sspBus			SSP bus connected to ADE78xx
 *	 \csPort			Chip Select Port
 *	 \csPin				Chip Select Pin
 *   \reg				Register to be read from ADE78xx
 *   \length			Length of the data to be read
 *   \readValuePtr		Pointer to the read value
 *
 *   \return			SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR FPMV2ADE78xxSPIReadReg( MODULE       module, 
							     MODULE_ID    moduleId,
							     lu_uint8_t   sspBus,
							     lu_uint8_t   csPort,
							     lu_uint8_t   csPin,
							     lu_uint16_t  reg,
							     lu_uint8_t   length,
							     lu_uint32_t  *readValuePtr
						       );

/*!
 ******************************************************************************
 *   \brief Writing ADE78xx registers
 *
 *   To Write ADE78xx Registers
 *
 *   \module			Module Type
 *   \moduleId			Module ID
 *	 \sspBus			SSP bus connected to ADE78xx
 *	 \csPort			Chip Select Port
 *	 \csPin				Chip Select Pin
 *   \reg				Register to be read from ADE78xx
 *   \length			Length of the data to be written
 *   \value				Value to be wrtitten to the register
 *
 *   \return			SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR FPMV2ADE78xxSPIWriteReg( MODULE       module, 
								  MODULE_ID    moduleId,
								  lu_uint8_t   sspBus,
								  lu_uint8_t   csPort,
								  lu_uint8_t   csPin,
								  lu_uint16_t  reg,
								  lu_uint8_t   length,
							      lu_uint32_t  value
						        );

/*!
 ******************************************************************************
 *   \brief FPMV2 CAN Protocol decoder
 *
 *   FPMV2 CAN Protocol decoder - For processing replies from FMP V2
 *
 *   \msgPtr Message3 Poointer of CAN reply
 *   \time   Time stamp of CAN message
 *
 *   \return SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR FPMV2ADE78xxSPITestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time);

/*
 *********************** End of file ******************************************
 */