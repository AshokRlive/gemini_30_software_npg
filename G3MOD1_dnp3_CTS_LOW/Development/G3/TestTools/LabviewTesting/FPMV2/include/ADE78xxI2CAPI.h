/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol global definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/11/14      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Enabling I2C and Initializing ADE78xx
 *
 *   Enabling SPI bus and initialization of ADE78xx chip
 *
 *   \module			 Module Type
 *	 \moduleId			 Module ID
 *   \fpiChanInitParams  Pointer to structure TestI2cADE78xxInitStr
 *
 *	 \return			SB_ERROR
 * 
 ******************************************************************************
 */
SB_ERROR FPMV2ADE78xxI2CEnable( MODULE                 module, 
							    MODULE_ID              moduleId,
								TestI2cADE78xxInitStr  *fpiChanInitParams
						      );

 /*!
 ******************************************************************************
 *   \brief Reading measured peak current from HSDC ISR
 *
 *   To read registers of ADE78xx 
 *
 *   \module			Module Type
 *   \moduleId			Module ID
 *	 \fpiChan			FPI Channel
 *	 \peakChan			Peak Channel (Phase)
 *	 \peakValue			Pointer to the peak value
 *
 *   \return			SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR FPMV2TestI2CADE78xxReadPeak( MODULE       module, 
								      MODULE_ID    moduleId,
								      lu_uint8_t   fpiChan,
								      lu_uint8_t   peakChan,
									  lu_uint32_t  *peakValuePtr
							        );

 /*!
 ******************************************************************************
 *   \brief Reading ADE78xx register
 *
 *   To read registers of ADE78xx 
 *
 *   \module			Module Type
 *   \moduleId			Module ID
 *	 \sspBus			SSP bus connected to ADE78xx
 *	 \csPort			Chip Select Port
 *	 \csPin				Chip Select Pin
 *   \reg				Register to be read from ADE78xx
 *   \length			Length of the data to be read
 *   \readValuePtr		Pointer to the read value
 *
 *   \return			SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR FPMV2ADE78xxI2CReadReg( MODULE       module, 
							     MODULE_ID    moduleId,
							     lu_uint8_t   i2cChan,
							     lu_uint16_t  reg,
							     lu_uint8_t   length,
							     lu_uint32_t  *readValuePtr
						       );

/*!
 ******************************************************************************
 *   \brief Writing ADE78xx registers
 *
 *   To Write ADE78xx Registers
 *
 *   \module			Module Type
 *   \moduleId			Module ID
 *	 \sspBus			SSP bus connected to ADE78xx
 *	 \csPort			Chip Select Port
 *	 \csPin				Chip Select Pin
 *   \reg				Register to be read from ADE78xx
 *   \length			Length of the data to be written
 *   \value				Value to be wrtitten to the register
 *
 *   \return			SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR FPMV2ADE78xxI2CWriteReg( MODULE       module, 
								  MODULE_ID    moduleId,
								  lu_uint8_t   i2cChan,
							      lu_uint16_t  reg,
							      lu_uint8_t   length,
							      lu_uint32_t  value
						        );

/*!
 ******************************************************************************
 *   \brief ADE78xx I2C CAN Protocol decoder
 *
 *   ADE78xx I2C CAN Protocol decoder - For processing replies from ADE78xx I2C
 *
 *   \msgPtr Message3 Pointer of CAN reply
 *   \time   Time stamp of CAN message
 *
 *   \return SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR FPMV2ADE78xxI2CTestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time);

/*!
 ******************************************************************************
 *   \brief Conversion of 32bit two's compliment to 24 bit value
 *	  
 *   Conversion of 32bit two's compliment to 24 bit value
 *
 *   \zp32Value         32ZP input value
 *   \actualValuePtr 	Pointer to converted value	
 *
 *   \return 
 *
 ******************************************************************************
 */
void CovertValueFrom32ZPSTo24Signed(lu_uint32_t zp32Value, lu_uint32_t *actualValuePtr);

/*!
 ******************************************************************************
 *   \brief Conversion of 32bit two's compliment to 28 bit value
 *	  
 *   Conversion of 32bit two's compliment to 28 bit value
 *
 *   \zp32Value         32ZP input value
 *   \actualValuePtr 	Pointer to converted value	
 *
 *   \return 
 *
 ******************************************************************************
 */
void CovertValueFrom32ZPSTo28Signed(lu_uint32_t zp32Value, lu_uint32_t *actualValuePtr);

/*!
 ******************************************************************************
 *   \brief Conversion of 32bit two's compliment to 28 bit unsigned value
 *	  
 *   Conversion of 32bit two's compliment to 28 bit unsigned value
 *
 *   \zp32Value         32ZP input value
 *   \actualValuePtr 	Pointer to converted value	
 *
 *   \return 
 *
 ******************************************************************************
 */
void CovertValueFrom32ZPSTo28Unsigned(lu_uint32_t zp32Value, lu_uint32_t *actualValuePtr);

/*
 *********************** End of file ******************************************
 */