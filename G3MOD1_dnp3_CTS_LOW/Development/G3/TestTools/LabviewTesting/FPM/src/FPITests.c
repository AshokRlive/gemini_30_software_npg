/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1577 2012-07-23 20:18:25Z fryers_j $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1577 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-07-23 21:18:25 +0100 (Mon, 23 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   31/07/14     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "FPITests.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

TestErrorRspStr *fpiInitResp;
TestFEPDataStr *fpiAdcValueResp = 0;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR FPIEnable( MODULE module, 
				    MODULE_ID moduleId,
					lu_uint8_t sspBus,
					lu_uint8_t enable,
					lu_uint16_t *errorCode
				  )
{
	SB_ERROR retError;

	TestSSPDMAStr fpiInput;

	moduleId = 0;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST_1;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_1_SSP_DMA_FEP_INIT_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	fpiInput.sspBus = sspBus;
	fpiInput.enable = enable;

	CANMsgFiller((lu_uint8_t*)&fpiInput,sizeof(TestSSPDMAStr));

	retError = LucyCANSendMainDll();

	*errorCode = fpiInitResp->errorCode;

	return retError;
}

SB_ERROR FPIReadADC( MODULE module, 
				     MODULE_ID moduleId,
					 lu_uint8_t sspBus,
					 lu_uint8_t adcChan,
					 lu_uint16_t *fpiAdcValue
				   )
{
	SB_ERROR retError;

	TestFEPReadStr fpiAdcSelect;

	moduleId = 0;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST_1;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_1_SSP_FEP_READ_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	fpiAdcSelect.sspBus  = sspBus;
	fpiAdcSelect.adcChan = adcChan;

	CANMsgFiller((lu_uint8_t*)&fpiAdcSelect,sizeof(TestFEPReadStr));

	retError = LucyCANSendMainDll();

	*fpiAdcValue = fpiAdcValueResp->value;

	return retError;

}

SB_ERROR FPITestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_NONE;
		
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_SINGLE_COMMAND)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_BLTST_1:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_BLTST_1_SSP_DMA_FEP_INIT_R: 
						if(msgPtr->msgLen == sizeof(TestErrorRspStr))
						{
							fpiInitResp = (TestErrorRspStr*)msgPtr->msgBufPtr;
							retError = SB_ERROR_NONE;
						}
						break;
				
					case MODULE_MSG_ID_BLTST_1_SSP_FEP_READ_R: 
						if(msgPtr->msgLen == sizeof(TestFEPDataStr))
						{
							fpiAdcValueResp = (TestFEPDataStr*)msgPtr->msgBufPtr;
							retError = SB_ERROR_NONE;
						}
						break;

					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;
			default:
				printf("\n Error SSP DMA Reading... ");
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
/*!
 ******************************************************************************
 *   \brief 
 *
 *   \param parameterName Description 
 *
 *   \return 
 *
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
*/