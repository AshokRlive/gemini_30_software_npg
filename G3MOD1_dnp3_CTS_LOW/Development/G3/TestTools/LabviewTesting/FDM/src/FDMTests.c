/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1577 2012-07-23 20:18:25Z fryers_j $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1577 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-07-23 21:18:25 +0100 (Mon, 23 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   31/07/14     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "FDMTests.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define ADE78XX_24ZP_SIGNUM     (0x08000000)
#define ADE78XX_24ZP_PAD        (0xFF000000)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

//void CovertValueFrom32ZPSTo24S(lu_uint32_t zp32Value, lu_uint32_t *actualValue);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static ADE7878ReadRspStr readiPeakReg;
static ADE7878ReadRspStr readReg;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR FDMReadIPeakReg( MODULE module, 
						  MODULE_ID moduleId,
						  lu_uint8_t phase,
						  lu_uint16_t *errorCode,
						  lu_uint32_t *iPeakARegValue,
						  lu_uint32_t *iPeakBRegValue,
						  lu_uint32_t *iPeakCRegValue
				         )
{
	SB_ERROR retError;
	ADE7878ReadRegStr readRegParam;
	lu_uint32_t iPeakRegValue = 0; 
	lu_uint8_t phaseA = LU_FALSE, phaseB = LU_FALSE, phaseC = LU_FALSE;

	moduleId       = 0;
	*errorCode     = 0;
	*iPeakARegValue = 0;
	*iPeakBRegValue = 0;
	*iPeakCRegValue = 0;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_IOMAN;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_IOMAN_CMD_ADE7878_READ_REG_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	readRegParam.reg    = 0xE500;
	readRegParam.length = 4; 

	CANMsgFiller((lu_uint8_t*)&readRegParam,sizeof(ADE7878ReadRegStr));

	retError = LucyCANSendMainDll();

	*errorCode     = readiPeakReg.errorCode;
	iPeakRegValue = readiPeakReg.value;

	if ( iPeakRegValue & (1 << 24) )
	{
		phaseA = LU_TRUE;
	}

	if ( iPeakRegValue & (1 << 25) )
	{
		phaseB = LU_TRUE;
	}

	if ( iPeakRegValue & (1 << 26) )
	{
		phaseC = LU_TRUE;
	}
	
	if ( iPeakRegValue & (1 << 24) ||
		 iPeakRegValue & (1 << 25) ||
		 iPeakRegValue & (1 << 26)
	   )
	{
			iPeakRegValue &= 0xffffff;

			/* 24 bit value transmitted as a 32bit value
			 * with eight MSBs padded with 0s
			 */
			if(iPeakRegValue & ADE78XX_24ZP_SIGNUM)
			{
				/* Signed value. Pad the 8 MSBs with 1s */
				iPeakRegValue |= ADE78XX_24ZP_PAD;
			}

			if(phaseA)
			{
				*iPeakARegValue = iPeakRegValue;
			}

			if(phaseB)
			{
				*iPeakBRegValue = iPeakRegValue;
			}

			if(phaseC)
			{
				*iPeakCRegValue = iPeakRegValue;
			}
	}

	return retError;
}

SB_ERROR FDMADE78xxReadReg( MODULE module, 
						    MODULE_ID moduleId,
							lu_uint16_t reg,
							lu_uint8_t length,
							lu_uint16_t *errorCode,
							lu_uint32_t *regValue
						  )
{
	SB_ERROR retError;
	ADE7878ReadRegStr readRegParam;

	moduleId       = 0;
	*errorCode     = 0;
	*regValue = 0;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_IOMAN;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_IOMAN_CMD_ADE7878_READ_REG_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	readRegParam.reg    = reg;
	readRegParam.length = length; 

	CANMsgFiller((lu_uint8_t*)&readRegParam,sizeof(ADE7878ReadRegStr));

	retError = LucyCANSendMainDll();

	*errorCode    = readReg.errorCode;
	*regValue     = readReg.value;

	return retError;
}

SB_ERROR FDMADE78xxReadIrmsRegs( MODULE module, 
							     MODULE_ID moduleId,
								 lu_uint16_t *errorCode,
						         lu_uint32_t *AIrms,
								 lu_uint32_t *BIrms,
								 lu_uint32_t *CIrms,
								 lu_uint32_t *NIrms
				               )
{
	
	SB_ERROR retError;
	lu_uint32_t zp32AIrms = 0, zp32BIrms = 0, zp32CIrms = 0, zp32NIrms = 0;

	retError = FDMADE78xxReadReg(module, moduleId, 0x43C0, 4, errorCode, &zp32AIrms);
	retError = FDMADE78xxReadReg(module, moduleId, 0x43C2, 4, errorCode, &zp32BIrms);
	retError = FDMADE78xxReadReg(module, moduleId, 0x43C4, 4, errorCode, &zp32CIrms);
	retError = FDMADE78xxReadReg(module, moduleId, 0x43BF, 4, errorCode, &zp32NIrms);

	CovertValueFrom32ZPSTo24S(zp32AIrms , AIrms);
	CovertValueFrom32ZPSTo24S(zp32BIrms , BIrms);
	CovertValueFrom32ZPSTo24S(zp32CIrms , CIrms);

	/* 24 bit value transmitted as a 32bit value
     * with eight MSBs padded with 0s
	 */
	if(zp32NIrms & ADE78XX_24ZP_SIGNUM)
	{
		/* Signed value. Pad the 8 MSBs with 1s */
		zp32NIrms |= ADE78XX_24ZP_PAD;
		zp32NIrms = ~zp32NIrms;
		zp32NIrms &= 0xffffff;
		//zp32NIrms--;
	}
	
	*NIrms = zp32NIrms;

	//CovertValueFrom32ZPSTo24S(zp32NIrms , NIrms);

	return retError;

}

SB_ERROR FDMADE78xxWriteReg( MODULE module, 
							 MODULE_ID moduleId,
						     lu_uint16_t reg,
							 lu_uint8_t length,
							 lu_uint8_t *data
				           )
{
	SB_ERROR retError;
	ADE7878WriteRegStr writeRegParam;
	lu_uint16_t byteCount = 0;

	moduleId       = 0;
	
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_IOMAN;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_IOMAN_CMD_ADE7878_WRITE_REG_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	writeRegParam.reg    = reg;
	writeRegParam.length = length; 
		
	byteCount = (sizeof(ADE7878WriteRegStr) - sizeof(lu_uint8_t*));
	memcpy(&argsFromCommand.msgBuff[0], &writeRegParam, byteCount);
	memcpy(&argsFromCommand.msgBuff[byteCount], data, length);

	argsFromCommand.msgBuffLen = byteCount + length;

	retError = LucyCANSendMainDll();
		
	return retError;
}

SB_ERROR FDMADE78xxSPIEnable( MODULE module, 
							  MODULE_ID moduleId,
							  lu_uint8_t sspBus,
							  lu_uint8_t csPort,
							  lu_uint8_t csPin
							)
{
	SB_ERROR retError;
	TestADE78xxSPIEnableStr ade78xxSpiParam;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST_1;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_1_ADE78xx_ENABLE_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	ade78xxSpiParam.sspBus  = sspBus;
	ade78xxSpiParam.csPort  = csPort;
	ade78xxSpiParam.csPin   = csPin;
		
	CANMsgFiller((lu_uint8_t*)&ade78xxSpiParam,sizeof(TestADE78xxSPIEnableStr));

	retError = LucyCANSendMainDll();
		
	return retError;
}

SB_ERROR FDMADE78xxSPIReadReg( MODULE module, 
							   MODULE_ID moduleId,
							   lu_uint8_t sspBus,
							   lu_uint8_t csPort,
							   lu_uint8_t csPin,
							   lu_uint16_t reg,
							   lu_uint8_t length
							 )
{
	SB_ERROR retError;
	TestADE78xxSPIReadRegStr readRegParam;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST_1;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_1_ADE78xx_READ_REG_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	readRegParam.sspBus  = sspBus;
	readRegParam.csPort  = csPort;
	readRegParam.csPin   = csPin;
	readRegParam.reg     = reg;
	readRegParam.length  = length;
		
	CANMsgFiller((lu_uint8_t*)&readRegParam,sizeof(TestADE78xxSPIReadRegStr));

	retError = LucyCANSendMainDll();
		
	return retError;
}

SB_ERROR FDMTestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_PARAM;
		
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_SINGLE_COMMAND)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_IOMAN:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_IOMAN_CMD_ADE7878_READ_REG_R: 
						if(msgPtr->msgLen == sizeof(ADE7878ReadRspStr))
						{
							
							memcpy(&readiPeakReg, msgPtr->msgBufPtr, sizeof(ADE7878ReadRspStr) );
							memcpy(&readReg,      msgPtr->msgBufPtr, sizeof(ADE7878ReadRspStr) );
							retError = SB_ERROR_NONE;
						}
						break;

					case MODULE_MSG_ID_IOMAN_CMD_ADE7878_WRITE_REG_R: 
						if(msgPtr->msgLen == 1)
						{
							retError = (SB_ERROR)(*msgPtr->msgBufPtr);
						}
						break;

					case MODULE_MSG_ID_BLTST_1_ADE78xx_ENABLE_R: 
						if(msgPtr->msgLen == sizeof(SB_ERROR))
						{
							retError = (SB_ERROR)(*msgPtr->msgBufPtr);
						}
						break;

					case MODULE_MSG_ID_BLTST_1_ADE78xx_READ_REG_R: 
						if(msgPtr->msgLen == sizeof(SB_ERROR))
						{
							retError = (SB_ERROR)(*msgPtr->msgBufPtr);
						}
						break;
				
					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;
			default:
				printf("\n Error SSP DMA Reading... ");
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
/*!
 ******************************************************************************
 *   \brief 
 *
 *   \param parameterName Description 
 *
 *   \return 
 *
 ******************************************************************************
 */
/*
void CovertValueFrom32ZPSTo24S(lu_uint32_t zp32Value, lu_uint32_t *actualValue)
{
	zp32Value &= 0xffffff;

	// /* 24 bit value transmitted as a 32bit value
    //  * with eight MSBs padded with 0s
	// 
	if(zp32Value & ADE78XX_24ZP_SIGNUM)
	{
		///* Signed value. Pad the 8 MSBs with 1s 
		zp32Value |= ADE78XX_24ZP_PAD;
	}
	
	*actualValue = zp32Value;
}

*/

/*
 *********************** End of file ******************************************
*/