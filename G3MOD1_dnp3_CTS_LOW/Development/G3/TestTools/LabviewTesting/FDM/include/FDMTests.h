/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol global definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   31/07/14      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
 
SB_ERROR FDMReadIPeakReg( MODULE module, 
						  MODULE_ID moduleId,
						  lu_uint8_t phase,
						  lu_uint16_t *errorCode,
						  lu_uint32_t *iPeakARegValue,
						  lu_uint32_t *iPeakBRegValue,
						  lu_uint32_t *iPeakCRegValue
				         );

SB_ERROR FDMADE78xxReadReg( MODULE module, 
							MODULE_ID moduleId,
						    lu_uint16_t reg,
							lu_uint8_t length,
							lu_uint16_t *errorCode,
							lu_uint32_t *regValue
				          );

SB_ERROR FDMADE78xxReadIrmsRegs( MODULE module, 
							     MODULE_ID moduleId,
								 lu_uint16_t *errorCode,
						         lu_uint32_t *AIrms,
								 lu_uint32_t *BIrms,
								 lu_uint32_t *CIrms,
								 lu_uint32_t *NIrms
				               );

SB_ERROR FDMADE78xxWriteReg( MODULE module, 
							 MODULE_ID moduleId,
						     lu_uint16_t reg,
							 lu_uint8_t length,
							 lu_uint8_t *data
				           );

extern SB_ERROR FDMADE78xxSPIEnable( MODULE module, 
									 MODULE_ID moduleId,
									 lu_uint8_t sspBus,
									 lu_uint8_t csPort,
									 lu_uint8_t csPin
								   );

extern SB_ERROR FDMADE78xxSPIReadReg( MODULE module, 
									  MODULE_ID moduleId,
									  lu_uint8_t sspBus,
									  lu_uint8_t csPort,
									  lu_uint8_t csPin,
									  lu_uint16_t reg,
									  lu_uint8_t length
								    );

SB_ERROR FDMTestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time);

/*
 *********************** End of file ******************************************
 */

