/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Processor specific GPIO Map module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/02/13     saravanan_v    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "IOManager.h"
#include "ProcessorGPIOMap.h"

#include "GPIOTestsDLL.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR ProcGPIOConfigure(
		lu_uint8_t 		port,
		lu_uint8_t 		pin,
		IO_CLASS 		ioClass,
		lu_uint32_t 	pinMode
		)
{
	SB_ERROR  			ret;
	lu_uint8_t  		portPinIdx;
	lu_uint8_t			pinDir;
	lu_uint8_t			pinValue;
	lu_uint8_t          openDrain;
	lu_uint8_t          pinPullMode;
	//PINSEL_CFG_Type 	pinCfg;
	lu_uint32_t	        pinShift;


	ret = SB_ERROR_INITIALIZED;

	pinDir = 0; /* Default to input */
	pinValue = 1; /* Default to high */
	if (ioClass == IO_CLASS_DIGITAL_OUTPUT || ioClass == IO_CLASS_DIGITAL_OUTPUT_SET_DIR_ONLY)
	{
		pinDir = 1; /* Output direction */
		if (pinMode & GPIO_PM_OUTPUT_LOW)
		{
			pinValue = 0; /* Set pin low */
		}
	}

	/* Set pin configuration */
	//pinCfg.Funcnum = 0;
	//pinCfg.Portnum = port;
	//pinCfg.Pinnum = pin;
	openDrain   = PINSEL_PINMODE_NORMAL;
	pinPullMode = PINSEL_PINMODE_TRISTATE;

	if (pinMode & GPIO_PM_OPEN_DRAIN)
	{
		openDrain = PINSEL_PINMODE_OPENDRAIN;
	}

	if (pinMode & GPIO_PM_PULL_UP)
	{
		pinPullMode = PINSEL_PINMODE_PULLUP;
	}
	if (pinMode & GPIO_PM_PULL_DOWN)
	{
		pinPullMode = PINSEL_PINMODE_PULLDOWN;
	}

	pinShift = 1;
	pinShift = pinShift << (pin & 0x1f);

	/* Now do the pin configuration */
	if (port < MAX_GPIO_PORTS && pin < MAX_GPIO_PORT_BITS)
	{
		portPinIdx = pin + (port << 5);
		if (portPinIdx < NUM_GPIO_PINPORT_IDX)
		{
			
			if (ioClass == IO_CLASS_DIGITAL_OUTPUT)
				{
					/* Set pin value */
					if (pinValue)
					{
						//GPIO_SetValue(port, pinShift);
						ret = GPIOWritePinDll( MODULE_PSM, 
												MODULE_ID_0, 
												port,
												pin,
												pinValue
				                                );

					}
					else
					{
						ret = GPIOWritePinDll( MODULE_PSM, 
											   MODULE_ID_0, 
											   port,
											   pin,
											   0
				                              );
					}
			

				/* Configure pin type */
				//PINSEL_ConfigPin(&pinCfg);

				/* Set pin direction */
				//GPIO_SetDir(port, pinShift, pinDir);

				         ret = GPIOSetPinModeDll( MODULE_PSM, 
												  MODULE_ID_0, 
												  port,
												  pin,
												  BoardIOMap[ioMapIdx].ioAddr.gpio.periphFunc,
												  pinPullMode,
										   	      openDrain,
												  pinDir
										        );

				/* pin configuration was successful */
				ret = SB_ERROR_NONE;
			}
		}
	}

	return ret;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
