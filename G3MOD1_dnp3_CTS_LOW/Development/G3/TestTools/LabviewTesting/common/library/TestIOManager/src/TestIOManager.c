/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *  18/12/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"

#include "TestIOManager.h"
#include "BoardIOMap.c"
#include "BoardIOID.h"

#include "CalElementAdderDll.h"
#include "GPIOTestsDLL.h"
#include "ADCTestsDLL.h"
#include "I2CExpIOTestsDLL.h"
#include "I2CADCTestsDLL.h"
#include "I2CTemperatureSensorTestsDLL.h"
#include "SPIADDigiPotTestsDLL.h"
#include "SPIADCTestsDLL.h"
#include "I2CHumiditySensorTests.h"
#include "NvramFactoryInfoWriteDLL.h"
#include "NvramFactoryInfoReadDLL.h"
#include "NvramPSMOPtionsWriteDLL.h"
#include "NvramPSMOPtionsReadDLL.h"
#include "NvramBatteryOptionsWriteDLL.h"
#include "NvramBatteryOptionsReadDLL.h"
#include "DACTestDLL.h"
#include "HeartBeatMsg.h"
#include "BoardCommandsDLL.h"
#include "hat_protocol.h"


#include "CalBlockWriterDLL.h"
#include "ModuleInfoCreator.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
*/
#define MAXHANDLE				16
#define HEART_BEAT_TIMEOUT_S	10

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
SB_ERROR InitI2CExpIO(lu_uint16_t ioMapIdx, handleParamStr handle);
SB_ERROR InitGPIO(lu_uint16_t ioMapIdx, handleParamStr handle);

MODULE FindModuleType(lu_uint16_t handle);
lu_uint16_t FindIOMapElement (lu_int8_t *IOIDString);
lu_uint8_t convertIoIDtoAdcChan(lu_uint32_t ioID, lu_uint8_t *retAdcChanPtr);
lu_uint8_t compareString(lu_int8_t *first, lu_uint8_t *second);
static SB_ERROR ReadCommandSelectorFromIOMap( handleParamStr handle,
									   IOMapStr IOMapElement,
									   lu_int32_t *valueToBeRead,
									   lu_int8_t *fileName,
									   lu_uint8_t blkOffset
								     );
SB_ERROR WriteCommandSelectorFromIOMap( handleParamStr handle,
										IOMapStr IOMapElement,
									    lu_uint32_t valueToBeWritten,
										lu_int8_t *fileName,
										lu_uint8_t blkOffset
								      );
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

IOMapStr IOMapElement;

volatile handleParamStr boardHandle[MAXHANDLE] = {{-1,-1},{-1,-1},{-1,-1},{-1,-1},
												  {-1,-1},{-1,-1},{-1,-1},{-1,-1},
												  {-1,-1},{-1,-1},{-1,-1},{-1,-1},
												  {-1,-1},{-1,-1},{-1,-1},{-1,-1}
												 };
static lu_int16_t handleIndex, noOfHandle;
ModuleHBeatSStr *PSMBoardStatusPtr;
lu_uint8_t boardReset = 0;
lu_uint16_t boardType;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
static lu_uint32_t codecHandle;
lu_int16_t InitAndOpenBoard( COM_MODE comMode,
                      lu_char_t* rtuIPAddress,
                      MODULE module,
                      MODULE_ID moduleId
                    )
{

    /* Initialise hat*/
    if(comMode == COM_MODE_ETH)
    {
        if(hat_client_init(rtuIPAddress, HAT_TCP_PORT) != HAT_ERR_NONE)
            return -1;
    }

    /* Initialise args before CAN codec initialisation.*/
    memset(&argsFromCommand, 0x00, sizeof(ParsingArgsStr));
    argsFromCommand.comMode = comMode;

    /* Initialise CAN codec*/
    codecHandle = CANCodecInitDll();


    /* Open board*/
	if (codecHandle >= 0)
		return OpenBoard(comMode, module, moduleId);
	else
		return -1;
}


lu_int16_t CloseAndDeinitBoard(lu_int16_t boardHandle)
{

    /* Initialise hat*/
    if (argsFromCommand.comMode == COM_MODE_ETH)
    {
        hat_client_close();
    }

    CANCodecDeInitDll(codecHandle);

    return CloseBoard(boardHandle);
}

lu_uint32_t GetAllIOIDStr(lu_char_t* IOIDStringBuf[], lu_uint32_t size)
{
	lu_int32_t i;
	
	if (size > BOARD_IO_MAX_IDX)
		size = BOARD_IO_MAX_IDX;

	for (i = 0; i < size; i++)
	{
		IOIDStringBuf[i] = BoardIOID[i];
	}

	return size;
}

lu_int16_t OpenBoard( COM_MODE comMode,
					  MODULE module, 
					  MODULE_ID moduleId
					)
{
	SB_ERROR retError = SB_ERROR_PARAM;
	lu_uint32_t powerStatus = 0;
	lu_uint16_t i, j;
	lu_uint8_t readPinTest;
	lu_uint8_t moduleExist = 0;
	time_t hBStartTime, hBEndTime, hBDiffTime;

	handleParamStr handle;

	memset(&argsFromCommand, 0x00, sizeof(ParsingArgsStr));

	argsFromCommand.comMode = comMode;

	if(argsFromCommand.comMode == COM_MODE_ETH)
	{
		CANCodecInitDll();
	}

	if( argsFromCommand.comMode == COM_MODE_CAN 
		// Testing HAT with Heartbeat Support
		//|| argsFromCommand.comMode == COM_MODE_ETH
      )
	{
		CANHeaderFiller( module, moduleId );

		argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_MD_CMD;
		argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_MD_CMD_RESTART;

		argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

		CANMsgFiller(&boardReset, 1);

		retError = LucyCANSendMainDll();

		Sleep(500);

		time(&hBStartTime);

		for(j = 0; j < 300; j++)
		{
			HeartBeatMsgSend(); 
			//Sleep(1);
			time(&hBEndTime);
			hBDiffTime = difftime(hBEndTime, hBStartTime);
			if( hBDiffTime > HEART_BEAT_TIMEOUT_S)
			{
				handle.handleId = -12;
				return handle.handleId;
			}
		}

		Sleep(500);
	}
		
	retError = SB_ERROR_PARAM;
	retError = GPIOReadPinDll( module, 
							   moduleId,
							   0,
							   0,
							   &readPinTest
					    	 );
	//retError = SB_ERROR_NONE;
	if(retError == SB_ERROR_NONE)
	{
		for(i = 0; i < MAXHANDLE; i++)
		{
			if(moduleId == boardHandle[i].handleId)
			{
				moduleExist = 1;
			}
		}
		if((boardHandle[handleIndex].handleId == -1) && (moduleExist == 0))
		{
			boardHandle[handleIndex].handleId   = moduleId;
			boardHandle[handleIndex].handleType = module;

			handleIndex++; noOfHandle++;
			handle.handleId = boardHandle[handleIndex - 1].handleId;
			
		}
		else
		{
			handle.handleId = -11;
		}
	}
	else
	{
		handle.handleId = -10;
	}
	return handle.handleId;
}

lu_int16_t OpenBoardApp( MODULE module, 
					     MODULE_ID moduleId
					   )
{
	SB_ERROR retError = SB_ERROR_PARAM;
	lu_uint32_t powerStatus = 0;
	lu_uint16_t i;
	lu_uint8_t moduleExist = 0;

	handleParamStr handle;

	memset(&argsFromCommand, 0x00, sizeof(ParsingArgsStr));

	retError = SB_ERROR_NONE;
	if(retError == SB_ERROR_NONE)
	{
		for(i = 0; i < MAXHANDLE; i++)
		{
			if(moduleId == boardHandle[i].handleId)
			{
				moduleExist = 1;
			}
		}
		if((boardHandle[handleIndex].handleId == -1) && (moduleExist == 0))
		{
			boardHandle[handleIndex].handleId   = moduleId;
			boardHandle[handleIndex].handleType = module;

			handleIndex++; noOfHandle++;
			handle.handleId = boardHandle[handleIndex - 1].handleId;
			
		}
		else
		{
			handle.handleId = -10;
		}
	}
	else
	{
		handle.handleId = -10;
	}
	return handle.handleId;
}

lu_uint16_t CloseBoard(lu_int16_t handle)
{
	int i, error = 0, removedHandle = -1;

	if(handle >= 0)
	{
		for(i = 0; i < MAXHANDLE; i++)
		{
			if(boardHandle[i].handleId == handle)
			{
				boardHandle[i].handleId = -1;
				boardHandle[i].handleType = -1;
				noOfHandle--; handleIndex--;
				removedHandle = i;
				error = 0;
				break;
			}
			else
			{
				error = -1;
			}
		}
		if(removedHandle != -1)
		{
			for(i = removedHandle + 1 ; i < MAXHANDLE; i++)
			{
				boardHandle[i-1].handleId = boardHandle[i].handleId;
			}
		}
	}

	return error;
}

SB_ERROR DownloadFirmware( lu_uint16_t handle,
  						   lu_int8_t   *firmwareFile
						 )
{
	SB_ERROR retError = SB_ERROR_NONE;

	argsFromCommand.parserFlag          = PARSER_FLAG_FIRMWARE_DOWNLOAD;

	argsFromCommand.CANHeader.deviceIDDst = (MODULE_ID)handle;
	argsFromCommand.CANHeader.deviceDst   = FindModuleType(handle); 
	
	memcpy(&argsFromCommand.sourceFileName[0], firmwareFile, strlen(firmwareFile));
	
	retError = LucyCANSendMainDll();

	return retError;
}

SB_ERROR GoToApplicationMode( lu_uint16_t handle )
{
	MODULE_ID  handleId;
	MODULE handleType;
	
	handleId    = (MODULE_ID)handle;
	handleType  = FindModuleType(handle);

	return GoToApplicationDll( handleType,
							   handleId	
							 );
}

SB_ERROR GoToBootloaderMode( lu_uint16_t handle )
{
	MODULE_ID  handleId;
	MODULE handleType;
	
	handleId    = (MODULE_ID)handle;
	handleType  = FindModuleType(handle);

	return GoToBootloaderDll( handleType,
							  handleId	
							);
}

SB_ERROR CalNVRAMSelect( lu_uint16_t handle, 
	 				  lu_uint8_t  nvRamType
				    )
{
	MODULE_ID  handleId;
	MODULE handleType;
	
	handleId    = (MODULE_ID)handle;
	handleType  = FindModuleType(handle);

	return CalibrationNVRAMSelectDLL( handleType, 
								      handleId,
								      nvRamType
									);
}

SB_ERROR CalNVRAMUpdate( lu_uint16_t handle, 
	 					 lu_uint8_t  nvRamType,
						 lu_uint8_t  nvRamBlk
					   )
{
	MODULE_ID  handleId;
	MODULE handleType;
	
	handleId    = (MODULE_ID)handle;
	handleType  = FindModuleType(handle);

	return CalibrationNVRAMUpdateDLL( handleType, 
								      handleId,
								      nvRamType,
									  nvRamBlk
									);
}

SB_ERROR CalNVRAMErase( lu_uint16_t handle )
{
	MODULE_ID  handleId;
	MODULE handleType;
	
	handleId    = (MODULE_ID)handle;
	handleType  = FindModuleType(handle);

	return CalibrationEraseAllDLL( handleType, 
								   handleId
								 );
}

SB_ERROR WriteCalibrationFile( lu_uint16_t handle, 
	 						   lu_uint8_t calID,
							   lu_uint8_t calType,
							   lu_int8_t *calElementFile
						     )
{
	MODULE_ID  handleId;
	MODULE handleType;
	
	handleId    = (MODULE_ID)handle;
	handleType  = FindModuleType(handle);

	return CalElementAdderDll( handleType,
							   handleId, 
	 						   calID,
							   calType,
							   calElementFile
							 );
}

SB_ERROR WriteCalibrationBlock( lu_uint16_t handle,
								lu_int8_t   *calFilesPath,
							    lu_int8_t   *calConfigFile,
								lu_int8_t   *IOIDString,
								lu_uint8_t  blkOffset
							  )
{
	SB_ERROR retError;
	lu_uint16_t foundIOID;
	lu_bool_t IOIDValid;
	handleParamStr boardParam;

	foundIOID = FindIOMapElement(IOIDString);	

	if(foundIOID <= BOARD_IO_MAX_IDX)
	{
		IOIDValid = TRUE;
		IOMapElement = BoardIOMap[foundIOID];
	}
	else
	{
		return SB_ERROR_PARAM;
	}

	boardParam.handleId    = (MODULE_ID)handle;
	boardParam.handleType  = FindModuleType(handle);

	switch(IOMapElement.ioClass)
	{
    	case IO_CLASS_GPIO_PERIPH:
	 		{
				 if(IOMapElement.ioDev == IO_DEV_I2C_NVRAM)
				 {
					 if((IOMapElement.ioAddr.i2cNVRAM.wenIoID) != (lu_int8_t)IO_ID_LAST)
					{
							InitGPIO((lu_uint16_t)IOMapElement.ioAddr.i2cNVRAM.wenIoID, boardParam);

							retError = GPIOWritePinDll( boardParam.handleType,
														boardParam.handleId, 
														BoardIOMap[IOMapElement.ioAddr.i2cNVRAM.wenIoID].ioAddr.gpio.port,
														BoardIOMap[IOMapElement.ioAddr.i2cNVRAM.wenIoID].ioAddr.gpio.pin,
														0x00
													  );
					}
		
					if((blkOffset == NVRAM_ID_BLK_CAL) || (blkOffset == NVRAM_APP_BLK_CAL))
					{
						retError = CalBlockWriterDLL( boardParam.handleType,
													  boardParam.handleId, 
												      calFilesPath,
													  calConfigFile,
													  IOMapElement.ioAddr.i2cNVRAM.address,
													  IOMapElement.ioAddr.i2cNVRAM.busI2c,
													  blkOffset
												    );
					}
		        }
			}
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
	}

	return retError;
}

SB_ERROR WriteModuleFactoryInfo( lu_uint16_t  handle,
								 lu_int8_t    *moduleInfoXMLFile,
								 lu_int8_t    *factoryInfoXMLFile,
								 lu_int8_t    *supplierID,
								 lu_int8_t    *assemblyNumber,
								 lu_int8_t    *assemblyRevision,
								 lu_uint32_t  *serialNumber,
								 lu_uint8_t   *buildDay,
								 lu_uint8_t   *buildMonth,
								 lu_uint16_t  *buildYear
							  )
{
	return ModuleInfoCreatorFactoryInfo( handle,
										 moduleInfoXMLFile,
										 factoryInfoXMLFile,
										 supplierID,
										 assemblyNumber,
										 assemblyRevision,
										 serialNumber,
										 buildDay,
										 buildMonth,
										 buildYear
									  );
}

SB_ERROR WriteModuleHMIOptions(  lu_uint16_t  handle,
								 lu_int8_t    *moduleInfoXMLFile,
								 lu_int8_t    *hmiOptionsXMLFile,
								 lu_int8_t    *assemblyNumber,
								 lu_int8_t    *assemblyRevision
							  )
{
	return ModuleInfoCreatorHMIOptions( handle,
										moduleInfoXMLFile,
										hmiOptionsXMLFile,
										assemblyNumber,
									    assemblyRevision
									  );
}

SB_ERROR WriteModulePSMOptions(  lu_uint16_t  handle,
								 lu_int8_t    *moduleInfoXMLFile,
								 lu_int8_t    *psmOptionsXMLFile,
								 lu_int8_t    *assemblyNumber,
								 lu_int8_t    *assemblyRevision
							  )
{
	return ModuleInfoCreatorPSMOptions( handle,
										moduleInfoXMLFile,
										psmOptionsXMLFile,
										assemblyNumber,
									    assemblyRevision
									  );
}

SB_ERROR WriteBatteryOptions( lu_uint16_t  handle,
							  lu_int8_t    *moduleInfoXMLFile,
							  lu_int8_t    *batteryOptionsXMLFile,
							  lu_int8_t    *assemblyNumber,
							  lu_int8_t    *assemblyRevision
							)
{
	return ModuleInfoCreatorBatteryOptions( handle,
											moduleInfoXMLFile,
											batteryOptionsXMLFile,
											assemblyNumber,
											assemblyRevision
										  );
}

SB_ERROR InitIOID( lu_uint16_t handle, lu_int8_t *IOIDString )
{
	lu_uint16_t	ioMapIdx, ioIDIdx;
    SB_ERROR    retError;
	handleParamStr boardParam;
	
    retError = SB_ERROR_INITIALIZED;
	
	ioIDIdx = FindIOMapElement(IOIDString);

    /* Check if IOMAP is valid */
	for (ioMapIdx = 0 ; ioMapIdx < BOARD_IO_MAX_IDX; ioMapIdx++)
	{
		if (BoardIOMap[ioMapIdx].ioID == ioIDIdx)
		{
			retError = SB_ERROR_NONE;
			break;
		}
	}

	boardParam.handleId    = (MODULE_ID)handle;
	boardParam.handleType  = FindModuleType(handle);

	if (retError == SB_ERROR_NONE)
	{
		/* Initialise IO using IOMAP */
	
			switch (BoardIOMap[ioIDIdx].ioClass)
			{
			 case IO_CLASS_DIGITAL_OUTPUT:
			 case IO_CLASS_DIGITAL_INPUT:
			 case IO_CLASS_DIGITAL_INPUT_OUTPUT:
				switch (BoardIOMap[ioIDIdx].ioDev)
				{
				 case IO_DEV_GPIO:
						InitGPIO(ioIDIdx, boardParam);
					break;

					case IO_DEV_I2C_IO_EXPANDER:
						 InitI2CExpIO(ioIDIdx, boardParam);
						break;
				default:
					break;
				}
				break;

			default:
				break;
			}

			if (retError == SB_ERROR_INITIALIZED)
			{
				retError = SB_ERROR_INITIALIZED;
			}
	}
	return retError;
}

SB_ERROR InitIOMAP( lu_uint16_t handle )
{
	volatile lu_uint32_t	ioMapIdx;
    SB_ERROR    retError;
	handleParamStr boardParam;
	
    retError = SB_ERROR_NONE;
	
	boardParam.handleId    = (MODULE_ID)handle;
	boardParam.handleType  = FindModuleType(handle);

	if (retError == SB_ERROR_NONE)
	{
		/* Initialise IO using IOMAP */
		for (ioMapIdx = 0 ; ioMapIdx < BOARD_IO_MAX_IDX; ioMapIdx++)
		{
			switch (BoardIOMap[ioMapIdx].ioClass)
			{
			 case IO_CLASS_DIGITAL_OUTPUT:
			 case IO_CLASS_DIGITAL_INPUT:
			 case IO_CLASS_DIGITAL_INPUT_OUTPUT:
				switch (BoardIOMap[ioMapIdx].ioDev)
				{
				 case IO_DEV_GPIO:
						InitGPIO(ioMapIdx, boardParam);
					break;

					case IO_DEV_I2C_IO_EXPANDER:
						 InitI2CExpIO(ioMapIdx, boardParam);
						break;
				default:
					break;
				}
				break;

			default:
				break;
			}

			if (retError == SB_ERROR_INITIALIZED)
			{
				retError = SB_ERROR_INITIALIZED;
			}
		}
	}
	return retError;
}


SB_ERROR ReadIOID( lu_uint16_t handle,
				   lu_int8_t *IOIDString,
				   lu_int32_t *valueToBeRead
				 )
{
	SB_ERROR retError;
	lu_uint16_t foundIOID;
	lu_bool_t IOIDValid;
	handleParamStr boardParam;

	foundIOID = FindIOMapElement(IOIDString);	

	if(foundIOID <= BOARD_IO_MAX_IDX)
	{
		IOIDValid = TRUE;
	}

	IOMapElement = BoardIOMap[foundIOID];

	boardParam.handleId    = (MODULE_ID)handle;
	boardParam.handleType  = FindModuleType(handle);

	retError = ReadCommandSelectorFromIOMap( boardParam,
											 IOMapElement, 
										     valueToBeRead,
											 0,
											 0
										   );

	printf("\n%s --> %d", IOIDString, *valueToBeRead);

	return retError;
}

SB_ERROR WriteIOID( lu_uint16_t handle,
					lu_int8_t *IOIDString,
					lu_uint32_t valueToBeWritten
				  )
{
	SB_ERROR retError;
	lu_uint16_t foundIOID;
	lu_bool_t IOIDValid;
	handleParamStr boardParam;

	foundIOID = FindIOMapElement(IOIDString);	

	if(foundIOID <= BOARD_IO_MAX_IDX)
	{
		IOIDValid = TRUE;
	}

	IOMapElement = BoardIOMap[foundIOID];

	boardParam.handleId    = (MODULE_ID)handle;
	boardParam.handleType  = FindModuleType(handle);
	
	retError = WriteCommandSelectorFromIOMap( boardParam,
											  IOMapElement, 
											  valueToBeWritten,
											  0,
											  0
											);
	return retError;
}

SB_ERROR WriteFileToNVRAM( lu_uint16_t handle,
						   lu_int8_t *IOIDString,
						   lu_int8_t *fileName,
						   lu_uint8_t blkOffset
						 )
{
	SB_ERROR retError;
	lu_uint16_t foundIOID;
	lu_bool_t IOIDValid;
	handleParamStr boardParam;

	foundIOID = FindIOMapElement(IOIDString);	

	if(foundIOID <= BOARD_IO_MAX_IDX)
	{
		IOIDValid = TRUE;
	}

	IOMapElement = BoardIOMap[foundIOID];

	boardParam.handleId    = (MODULE_ID)handle;
	boardParam.handleType  = FindModuleType(handle);
	
	retError = WriteCommandSelectorFromIOMap( boardParam,
											  IOMapElement, 
											  0,
											  fileName,
											  blkOffset
											);
	return retError;
}

SB_ERROR ReadFileFromNVRAM( lu_uint16_t handle,
					 	    lu_int8_t *IOIDString,
						    lu_int8_t *fileName,
						    lu_uint8_t blkOffset
						  )
{
	SB_ERROR retError;
	lu_uint16_t foundIOID;
	lu_bool_t IOIDValid;
	handleParamStr boardParam;

	foundIOID = FindIOMapElement(IOIDString);

	if(foundIOID <= BOARD_IO_MAX_IDX)
	{
		IOIDValid = TRUE;
	}

	IOMapElement = BoardIOMap[foundIOID];

	boardParam.handleId    = (MODULE_ID)handle;
	boardParam.handleType  = FindModuleType(handle);
	
	retError = ReadCommandSelectorFromIOMap( boardParam,
											 IOMapElement, 
											 0,
											 fileName,
											 blkOffset
											);
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
*/

/*!
 ******************************************************************************
 *   \brief To select a command for reading IO resource corresponding to IO Map Element
 *
 *   Detailed description
 *
 *   \handle			Board Handle
 *	 \IOMapElement		An Element from BoardIOMap
 *	 \valueToBeRead     Pointer to a value to be read from board
 *
 *   \return SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR ReadCommandSelectorFromIOMap( handleParamStr handle,
									   IOMapStr   IOMapElement,
									   lu_int32_t *valueToBeRead,
									   lu_int8_t *fileName,
									   lu_uint8_t blkOffset
							         )
{
	lu_uint8_t ADCChan;
	SB_ERROR retError;

	switch(IOMapElement.ioClass)
	{
		case IO_CLASS_DIGITAL_OUTPUT:
		case IO_CLASS_DIGITAL_INPUT:
			{
				if(IOMapElement.ioDev == IO_DEV_GPIO)
				{
					retError = GPIOReadPinDll( handle.handleType,
											   handle.handleId,
											   IOMapElement.ioAddr.gpio.port,
											   IOMapElement.ioAddr.gpio.pin,
											   (lu_uint8_t*)valueToBeRead
					    					 );
				}

				if(IOMapElement.ioDev == IO_DEV_I2C_IO_EXPANDER)
				{
					retError = I2CExpIOReadPinDll( handle.handleType,
												   handle.handleId,
												   IOMapElement.ioAddr.i2cExpander.busI2c,
												   IOMapElement.ioAddr.i2cExpander.address,
												   IOMapElement.ioAddr.i2cExpander.ioPin,
												   (lu_uint8_t*)valueToBeRead
					   							 );
				}

			}
			break;

		case IO_CLASS_ANALOG_INPUT:
		case IO_CLASS_ANALOG_INPUT_MUX:
			{
			
				if(IOMapElement.ioDev == IO_DEV_PERIPH)
				{
					   if (BoardIOMap[IOMapElement.ioID].ioClass == IO_CLASS_ANALOG_INPUT_MUX)
						{
							/* Select Mux Pin */ 
							retError = GPIOWritePinDll( handle.handleType,
														handle.handleId,
														BoardIOMap[IOMapElement.ioAddr.adc.muxIoID].ioAddr.gpio.port,
														BoardIOMap[IOMapElement.ioAddr.adc.muxIoID].ioAddr.gpio.pin,
														1
					    							  );
						}
                  
					   convertIoIDtoAdcChan(IOMapElement.ioID, &ADCChan);
					   retError = ADCReadDll ( handle.handleType,
											   handle.handleId,
											   ADCChan,
											   2,
											   (lu_uint16_t*)valueToBeRead
											 );
					   if (BoardIOMap[IOMapElement.ioID].ioClass == IO_CLASS_ANALOG_INPUT_MUX)
						{
							/* Select Mux Pin */ 
							retError = GPIOWritePinDll( handle.handleType,
														handle.handleId,
														BoardIOMap[IOMapElement.ioAddr.adc.muxIoID].ioAddr.gpio.port,
														BoardIOMap[IOMapElement.ioAddr.adc.muxIoID].ioAddr.gpio.pin,
														0
					    							  );
						}
				}
				if(IOMapElement.ioDev == IO_DEV_SPI_ADC)
				{
					retError = SPIADCReadDll ( handle.handleType,
											   handle.handleId,
											   IOMapElement.ioAddr.spiAdc.busSspi,
											   BoardIOMap[IOMapElement.ioAddr.spiAdc.csIoID].ioAddr.gpio.port,
											   BoardIOMap[IOMapElement.ioAddr.spiAdc.csIoID].ioAddr.gpio.pin,
											   IOMapElement.ioAddr.spiAdc.adcChan,
											   (lu_uint16_t*)valueToBeRead
											 );
						}

				if(IOMapElement.ioDev == IO_DEV_I2C_TEMP_LM73)
				{
					retError = I2CTempLM73ReadDll( handle.handleType,
							  					   handle.handleId,
												   IOMapElement.ioAddr.i2cTempLM73.busI2c,
												   IOMapElement.ioAddr.i2cTempLM73.address,
												   valueToBeRead
												 );
				}

				if(IOMapElement.ioDev == IO_DEV_I2C_ADC_MCP324X)
				{
					retError = I2CADCMPC324xDll(   handle.handleType,
							  					   handle.handleId,
												   IOMapElement.ioAddr.i2cAdcMCP342x.busI2c,
												   IOMapElement.ioAddr.i2cAdcMCP342x.address,
												   IOMapElement.ioAddr.i2cAdcMCP342x.adcChan,
												   (lu_int16_t*)valueToBeRead
												 );
				}
				
				if(IOMapElement.ioDev == IO_DEV_I2C_HUMIDITY)
				{
					if(IOMapElement.ioAddr.i2cHumidity.reg == I2C_REG_SENS_TEMP)
					{
						retError = I2CHumiditySensorReadTemperatureDll( (MODULE)handle.handleType,
								  										(MODULE_ID)handle.handleId,
																		IOMapElement.ioAddr.i2cHumidity.busI2c,
																		(lu_int32_t*)valueToBeRead
																	  );
					}

					if(IOMapElement.ioAddr.i2cHumidity.reg == I2C_REG_SENS_HUMIDITY)
					{
						retError = I2CHumiditySensorReadHumidityDll( (MODULE)handle.handleType,
								  								     (MODULE_ID)handle.handleId,
																	 IOMapElement.ioAddr.i2cHumidity.busI2c,
																	 (lu_uint32_t*)valueToBeRead
																   );
					}
				}
			}
			break;

			case IO_CLASS_GPIO_PERIPH:
			{
				 if(IOMapElement.ioDev == IO_DEV_I2C_NVRAM)
				 {
					 if((IOMapElement.ioAddr.i2cNVRAM.wenIoID) != (lu_int8_t)IO_ID_LAST)
					{
							InitGPIO((lu_uint16_t)IOMapElement.ioAddr.i2cNVRAM.wenIoID, handle);

							retError = GPIOWritePinDll( handle.handleType,
														handle.handleId, 
														BoardIOMap[IOMapElement.ioAddr.i2cNVRAM.wenIoID].ioAddr.gpio.port,
														BoardIOMap[IOMapElement.ioAddr.i2cNVRAM.wenIoID].ioAddr.gpio.pin,
														0x00
													  );
					}
					 
					 if( (blkOffset == NVRAM_ID_BLK_INFO)  || 
						 (blkOffset == NVRAM_APP_BLK_INFO)
					  )
					{
						retError = NvramFactoryInfoReadDll( handle.handleType, 
															handle.handleId,
															IOMapElement.ioAddr.i2cNVRAM.address,
															IOMapElement.ioAddr.i2cNVRAM.busI2c,
															fileName
														  );
					}

					if( ( (blkOffset == NVRAM_ID_BLK_OPTS)    || 
						  (blkOffset == NVRAM_APP_BLK_OPTS) ) &&   
						(IOMapElement.ioAddr.i2cNVRAM.busI2c == 0x01)
					  )
					{
						if(handle.handleType == MODULE_HMI)
						{
							retError = NvramHMIOptionsReadDll( handle.handleType, 
															    handle.handleId,
															    IOMapElement.ioAddr.i2cNVRAM.address,
															    fileName
														      );
						}
						if(handle.handleType == MODULE_PSM)
						{
							retError = NvramPSMOptionsReadDll( handle.handleType, 
																handle.handleId,
																IOMapElement.ioAddr.i2cNVRAM.address,
																fileName
																);
						}
					}

					if( ( (blkOffset == NVRAM_ID_BLK_OPTS)    || 
						  (blkOffset == NVRAM_APP_BLK_OPTS) ) &&   
						(IOMapElement.ioAddr.i2cNVRAM.busI2c == 0x00)
					  )
					{
						retError = NvramBatteryOptionsReadDll( handle.handleType, 
															   handle.handleId,
															   IOMapElement.ioAddr.i2cNVRAM.address,
															   fileName
															 );
				    }
					break;
				}
			}

		default:
			retError = SB_ERROR_PARAM;
			break;
	}
	
	return retError;
}

/*!
 ******************************************************************************
 *   \brief To select a command for writing IO resource corresponding to IO Map Element
 *
 *   Detailed description
 *
 *   \handle			Board Handle
 *	 \IOMapElement		An Element from BoardIOMap
 *	 \valueToBeRead     Value to be written to board
 *
 *   \return SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR WriteCommandSelectorFromIOMap( handleParamStr handle,
										IOMapStr   IOMapElement,
									    lu_uint32_t valueToBeWritten,
										lu_int8_t *fileName,
										lu_uint8_t blkOffset 
									  )
{
	SB_ERROR retError = SB_ERROR_PARAM;
		
	switch(IOMapElement.ioClass)
	{
		case IO_CLASS_DIGITAL_OUTPUT:
			{
				if(IOMapElement.ioDev == IO_DEV_GPIO)
				{
					retError = GPIOWritePinDll(handle.handleType,
											   handle.handleId, 
											   IOMapElement.ioAddr.gpio.port,
											   IOMapElement.ioAddr.gpio.pin,
											   (lu_uint8_t)valueToBeWritten
					    					 );
				}

				if(IOMapElement.ioDev == IO_DEV_I2C_IO_EXPANDER)
				{
				   retError = I2CExpIOWritePinDll( handle.handleType,
											       handle.handleId,
												   IOMapElement.ioAddr.i2cExpander.busI2c,
												   IOMapElement.ioAddr.i2cExpander.address,
												   IOMapElement.ioAddr.i2cExpander.ioPin,
												   (lu_uint8_t)valueToBeWritten
					   							);
				}
			}
			case IO_CLASS_ANALOG_OUTPUT:
				{
				 if(IOMapElement.ioDev == IO_DEV_SPI_DIGIPOT ||
					IOMapElement.ioDev == IO_DEV_SPI_AD_DIGITAL_POT )
				 {
					 
					retError =  SPIADDigipotWriteDll( handle.handleType,
													  handle.handleId, 
													  IOMapElement.ioAddr.spiDigiPot.busSspi,
													  BoardIOMap[IOMapElement.ioAddr.spiDigiPot.csIoID].ioAddr.gpio.port,
													  BoardIOMap[IOMapElement.ioAddr.spiDigiPot.csIoID].ioAddr.gpio.pin,
													  IOMapElement.ioAddr.spiDigiPot.chan,
													  (lu_uint8_t)valueToBeWritten
													);
				 }

				 if(IOMapElement.ioDev == IO_DEV_PERIPH)
				 {
					retError =  DACWritedDll( handle.handleType,
											  handle.handleId, 
											  (lu_uint16_t)valueToBeWritten
											);
				 }



				}
				break;

			case IO_CLASS_GPIO_PERIPH:
			{
				 if(IOMapElement.ioDev == IO_DEV_I2C_NVRAM)
				 {
					 if((IOMapElement.ioAddr.i2cNVRAM.wenIoID) != (lu_int8_t)IO_ID_LAST)
					{
							InitGPIO((lu_uint16_t)IOMapElement.ioAddr.i2cNVRAM.wenIoID, handle);

							retError = GPIOWritePinDll( handle.handleType,
														handle.handleId, 
														BoardIOMap[IOMapElement.ioAddr.i2cNVRAM.wenIoID].ioAddr.gpio.port,
														BoardIOMap[IOMapElement.ioAddr.i2cNVRAM.wenIoID].ioAddr.gpio.pin,
														0x00
													  );
					}
					 
					 if( (blkOffset == NVRAM_ID_BLK_INFO)  || 
						 (blkOffset == NVRAM_APP_BLK_INFO)
					  )
					{
						retError = NvramFactoryInfoWriteDll( handle.handleType, 
															 handle.handleId,
															 IOMapElement.ioAddr.i2cNVRAM.address,
															 IOMapElement.ioAddr.i2cNVRAM.busI2c,
															 fileName
														   );
					}

					if( ( (blkOffset == NVRAM_ID_BLK_OPTS)    || 
						  (blkOffset == NVRAM_APP_BLK_OPTS) ) &&   
						(IOMapElement.ioAddr.i2cNVRAM.busI2c == 0x01)
					  )
					{
						if(handle.handleType == MODULE_HMI)
						{
							retError = NvramHMIOptionsWriteDll( handle.handleType, 
															    handle.handleId,
															    IOMapElement.ioAddr.i2cNVRAM.address,
															    fileName
														      );
						}
						if(handle.handleType == MODULE_PSM)
						{
							retError = NvramPSMOptionsWriteDll( handle.handleType, 
																handle.handleId,
																IOMapElement.ioAddr.i2cNVRAM.address,
																fileName
															  );
						}
					}

					if( ( (blkOffset == NVRAM_ID_BLK_OPTS)    || 
						  (blkOffset == NVRAM_APP_BLK_OPTS) ) &&   
						(IOMapElement.ioAddr.i2cNVRAM.busI2c == 0x00)
					  )
					{
						retError = NvramBatteryOptionsWriteDll( handle.handleType, 
																handle.handleId,
																IOMapElement.ioAddr.i2cNVRAM.address,
																fileName
															  );
				    }
					break;
				}
			}
		default:
			retError = SB_ERROR_PARAM;
			break;
	}
	
	return retError;
}

/*!
 ******************************************************************************
 *   \brief To initialse the GPIO resouces according to the Board IO Map
 *
 *   Detailed description
 *
 *   \ioMapIdx		Index of GPIO resource in Board IO Map
 *	 \handle		handle for Board type and ID
 *	 
 *   \return SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR InitGPIO(lu_uint16_t ioMapIdx, handleParamStr handle)
{
	SB_ERROR    retError;
	lu_uint8_t  openDrain, pinDir, pinPullMode, pinValue, pinValue_r;
	
    retError = SB_ERROR_NONE;
	openDrain = PINSEL_PINMODE_NORMAL;
	pinPullMode = PINSEL_PINMODE_TRISTATE;

	if (BoardIOMap[ioMapIdx].ioAddr.gpio.pinMode & GPIO_PM_OPEN_DRAIN)
	{
		openDrain = PINSEL_PINMODE_OPENDRAIN;
	}
	
	if (BoardIOMap[ioMapIdx].ioAddr.gpio.pinMode & GPIO_PM_PULL_UP)
	{
		pinPullMode = PINSEL_PINMODE_PULLUP;
	}
	if (BoardIOMap[ioMapIdx].ioAddr.gpio.pinMode & GPIO_PM_PULL_DOWN)
	{
		pinPullMode = PINSEL_PINMODE_PULLDOWN;
	}

	if (BoardIOMap[ioMapIdx].ioAddr.gpio.pinMode & GPIO_PM_OUTPUT_LOW)
	{
		pinValue = 0; /* Set pin low */
	}
	else
	{
		pinValue = 1; // Set pin High
	}

	// configure Processor GPIO 
	if( BoardIOMap[ioMapIdx].ioClass == IO_CLASS_DIGITAL_INPUT )
	{
		pinDir = 0; // Output direction 
		pinPullMode = PINSEL_PINMODE_NORMAL; 
		pinValue_r = 0;
		openDrain = PINSEL_PINMODE_NORMAL;
		pinValue = 0; /* Set pin low */

		retError = GPIOSetPinModeDll( handle.handleType,
								      handle.handleId, 
								      BoardIOMap[ioMapIdx].ioAddr.gpio.port,
								      BoardIOMap[ioMapIdx].ioAddr.gpio.pin,
							  	      BoardIOMap[ioMapIdx].ioAddr.gpio.periphFunc,
								      pinPullMode,
								      openDrain,
								      pinDir
								    );
	}
	
	/* configure Processor GPIO */
	if( BoardIOMap[ioMapIdx].ioClass == IO_CLASS_DIGITAL_OUTPUT )
	{
		pinDir = 1; /* Output direction */					
		retError = GPIOWritePinDll( handle.handleType,
									handle.handleId, 
									BoardIOMap[ioMapIdx].ioAddr.gpio.port,
									BoardIOMap[ioMapIdx].ioAddr.gpio.pin,
									pinValue
								  );

		retError = GPIOSetPinModeDll( handle.handleType,
								  handle.handleId, 
								  BoardIOMap[ioMapIdx].ioAddr.gpio.port,
								  BoardIOMap[ioMapIdx].ioAddr.gpio.pin,
							  	  BoardIOMap[ioMapIdx].ioAddr.gpio.periphFunc,
								  pinPullMode,
								  openDrain,
								  pinDir
								);
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief To initialse the I2C resouces according to the Board IO Map
 *
 *   Detailed description
 *
 *   \ioMapIdx		Index of I2C resource in Board IO Map
 *	 \handle		handle for Board type and ID
 *	 
 *   \return SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR InitI2CExpIO(lu_uint16_t ioMapIdx, handleParamStr handle)
{
	SB_ERROR    retError;
	lu_uint8_t  pinValue = 0xFF;
	lu_uint16_t pinDir = 0xFFFF;

    retError = SB_ERROR_NONE;
	
	I2CExpIOReadDDRDll( handle.handleType,
						handle.handleId, 
						BoardIOMap[ioMapIdx].ioAddr.i2cExpander.busI2c,
						BoardIOMap[ioMapIdx].ioAddr.i2cExpander.address,
						&pinDir
					  );
      
	if(BoardIOMap[ioMapIdx].ioClass == IO_CLASS_DIGITAL_OUTPUT)
	{
		pinDir   &= ~(0x0001 << BoardIOMap[ioMapIdx].ioAddr.i2cExpander.ioPin); 

		if (BoardIOMap[ioMapIdx].ioAddr.i2cExpander.mode & GPIO_PM_OUTPUT_LOW)
		{
			pinValue &=  (0x0001 << BoardIOMap[ioMapIdx].ioAddr.i2cExpander.ioPin);
		}

		if (BoardIOMap[ioMapIdx].ioAddr.i2cExpander.mode & GPIO_PM_OUTPUT_HIGH)
		{
			pinValue &= ~(0x0000 << BoardIOMap[ioMapIdx].ioAddr.i2cExpander.ioPin);
		}

		if (BoardIOMap[ioMapIdx].ioAddr.i2cExpander.mode & GPIO_PM_OUTPUT_INVERT)
		{
			pinValue &= ~(0x0001 << BoardIOMap[ioMapIdx].ioAddr.i2cExpander.ioPin);
		}
	}
							
	/* configure Processor GPIO */
	if(BoardIOMap[ioMapIdx].ioClass == IO_CLASS_DIGITAL_OUTPUT)
	{
		retError = I2CExpIOWritePinDll( handle.handleType,
									    handle.handleId, 
										BoardIOMap[ioMapIdx].ioAddr.i2cExpander.busI2c,
										BoardIOMap[ioMapIdx].ioAddr.i2cExpander.address,
										BoardIOMap[ioMapIdx].ioAddr.i2cExpander.ioPin,
										pinValue
									 );
	}

	retError = I2CExpIOWriteDDRDll( handle.handleType,
									handle.handleId,
									BoardIOMap[ioMapIdx].ioAddr.i2cExpander.busI2c,
									BoardIOMap[ioMapIdx].ioAddr.i2cExpander.address,
									pinDir
								  );
	
	return retError;

}

/*!
 ******************************************************************************
 *   \brief To find a index of IOID in Board IO Map using IOIDString
 *
 *   Detailed description
 *
 *   \IOIDString		IOID in String format (Same name as BoardIOID Enum)
 *	 	 
 *   \return Index of an IOIDString in Board IO Map
 *
 ******************************************************************************
 */
lu_uint16_t FindIOMapElement(lu_int8_t *IOIDString)
{
	lu_uint16_t idCount, retValue;

	for(idCount = 0; idCount < BOARD_IO_MAX_IDX; idCount++)
	{
		if(compareString(IOIDString, BoardIOID[idCount]) == 0)
		{
			break;
		}
	}

  if(idCount <= BOARD_IO_MAX_IDX)
	{
		retValue = idCount;
	}
	else
	{
		retValue = (lu_uint16_t)SB_ERROR_PARAM;
	}

	return retValue;
}

MODULE FindModuleType(lu_uint16_t handle)
{
	lu_uint16_t i;

	for(i=0; i < MAXHANDLE; i++)
	{
		if(boardHandle[i].handleId == handle)
		{
			break;
		}
	}
	return boardHandle[i].handleType;
}

/*!
 ******************************************************************************
 *   \brief To convert IOID to an Processor's ADC channel
 *
 *   Detailed description
 *
 *   \ioID				IO ID 
 *	 \retAdcChanPtr		Pointer to a returned ADC channel number
 *	 
 *   \return		    
 *
 ******************************************************************************
 */
lu_uint8_t convertIoIDtoAdcChan(lu_uint32_t ioID, lu_uint8_t *retAdcChanPtr)
{
	lu_uint8_t retVal;

	retVal = LU_TRUE;

	switch (BoardIOMap[ioID].ioAddr.gpio.periphFunc)
	{
	case FUNC_AD0_CH_0:
		*retAdcChanPtr = 0;
		break;

	case FUNC_AD0_CH_1:
		*retAdcChanPtr = 1;
		break;

	case FUNC_AD0_CH_2:
		*retAdcChanPtr = 2;
		break;

	case FUNC_AD0_CH_3:
		*retAdcChanPtr = 3;
		break;

	case FUNC_AD0_CH_4:
		*retAdcChanPtr = 4;
		break;

	case FUNC_AD0_CH_5:
		*retAdcChanPtr = 5;
		break;

	case FUNC_AD0_CH_6:
		*retAdcChanPtr = 6;
		break;

	case FUNC_AD0_CH_7:
		*retAdcChanPtr = 7;
		break;

	default:
		retVal = LU_FALSE;
		break;
	}

	return retVal;
}

lu_uint8_t compareString(lu_int8_t *first, lu_uint8_t *second)
{
   while(*first==*second)
   {
      if ( *first == '\0' || *second == '\0' )
         break;
 
      first++;
      second++;
   }
   if( *first == '\0' && *second == '\0' )
      return 0;
   else
      return 1;
}

/*
 *********************** End of file ******************************************
*/
