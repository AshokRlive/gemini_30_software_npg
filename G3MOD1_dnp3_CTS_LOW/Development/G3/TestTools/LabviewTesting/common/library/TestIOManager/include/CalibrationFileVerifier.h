/*! \file
******************************************************************************
*       \author         Lucy Electric Ltd: http://www.lucyelectric.co.uk
*                       - Automation Dept.
******************************************************************************
*    PROJECT:
*       G3 [Module Name]
*
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*       \brief
*
*       Detailed description of this template module
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*
*
*    CREATION
*
*   Date          Name        Details
*   --------------------------------------------------------------------------
*   05/10/16      saravanan_v     Initial version.
*
******************************************************************************
*   COPYRIGHT
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
******************************************************************************
*/

#ifndef _CALIBRATION_FILE_VERIFIER_INCLUDED
#define _CALIBRATION_FILE_VERIFIER_INCLUDED

/*
******************************************************************************
* INCLUDES - Standard Library Modules Used
******************************************************************************
*/


/*
******************************************************************************
* INCLUDES - Project Specific Modules Used
******************************************************************************
*/
#include "lu_types.h"
#include "errorCodes.h"

/*
******************************************************************************
* EXPORTED - Definitions And Macros
******************************************************************************
*/

/*
******************************************************************************
* EXPORTED - Typedefs and Structures
******************************************************************************
*/
typedef struct PointDef
{
	lu_float32_t pointX;
	lu_float32_t pointY;
}PointStr;
/*
******************************************************************************
* EXPORTED - Variables
******************************************************************************
*/

/*
******************************************************************************
* EXPORTED - Functions
******************************************************************************
*/

/*!
******************************************************************************
*   \Calibration file verification 
*
*   To verify the calibration elements are in tolerance range.
*
*   \param      parameter name      Description
*   \calFiles   Calibration Files   Calibration files in CSV format
*
*   \return SB_ERROR
*
******************************************************************************
*/
extern int CalibrationFileVerifier( lu_int8_t    *refCalFile,
										 lu_int8_t    *actualCalFile,
										 lu_float32_t numberOfPointXTolerance,
										 lu_float32_t numberOfPointYTolerance, 
										 lu_float32_t percentageOfSlopeTolerance,
									     lu_uint8_t   *calUpperLineFile,
										 lu_uint8_t   *calLowerLineFile
								      );

#endif //CALIBRATION_FILE_VERIFIER_INCLUDED
/*
*********************** End of file ******************************************
*/