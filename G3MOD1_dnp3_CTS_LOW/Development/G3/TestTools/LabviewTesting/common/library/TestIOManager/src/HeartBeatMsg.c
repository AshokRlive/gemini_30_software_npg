/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *   18/02/13      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "HeartBeatMsg.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
ModuleHBeatMStr stayBL;

ModuleHBeatSStr *slaveBoardStatusPtr;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
ModuleHBeatSStr *HeartBeatMsgSend()
{
	/* Set defaults for Can Header */
	CANHeaderFiller((MODULE)0x1F, (MODULE_ID)0x07);
	//CANHeaderFiller( MODULE_PSM, MODULE_ID_0 );

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_LPRIO;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_LPRIO_HBEAT_M;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	stayBL.bootloaderMode = 1;
	stayBL.powerSaveMode  = 0;
	
	CANMsgFiller((lu_uint8_t*)&stayBL,1);

	CANCSendFromMCM( argsFromCommand.CANHeader.messageType,
					 argsFromCommand.CANHeader.messageID,
					 argsFromCommand.CANHeader.deviceDst,
			         argsFromCommand.CANHeader.deviceIDDst,
			         argsFromCommand.msgBuffLen,
			         argsFromCommand.msgBuff
			       );
	
	LucyCANSendMainDll();

	return slaveBoardStatusPtr;
}

SB_ERROR HeartBeatMsgSendCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_NONE;
		
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_SINGLE_COMMAND)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_LPRIO:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_LPRIO_HBEAT_S: 
						{
							if(msgPtr->msgLen == sizeof(ModuleHBeatSStr))
							{
								slaveBoardStatusPtr = (ModuleHBeatSStr*)msgPtr->msgBufPtr;
								retError = SB_ERROR_NONE;
							}
						break;
							retError = SB_ERROR_NONE;
						}
					break;

				default:
					slaveBoardStatusPtr->moduleUID.serialNumber = 0;
					slaveBoardStatusPtr->moduleUID.supplierId   = 0;
					slaveBoardStatusPtr->status                 = MODULE_BOARD_STATUS_LAST;
					slaveBoardStatusPtr->error                  = MODULE_BOARD_ERROR_LAST;
					printf("\n Error in Downloading ");
					retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
				}
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */