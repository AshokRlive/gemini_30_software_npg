/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/02/13      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */
#ifndef _TESTIOMANAGER_H_INCLUDED
#define _TESTIOMANAGER_H_INCLUDED
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "CANProtocolFraming.h"
#include "CmdParser.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

typedef struct handleParamDef
{
	lu_int8_t  handleType; 
	lu_int8_t  handleId;
}handleParamStr;

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
 /**
 * arguments
 * rtuIPAddress - ignored if the comMode is CAN.
 * return - board handle, -1 if fails.
 */
extern lu_int16_t InitAndOpenBoard( COM_MODE comMode,
                      lu_char_t* rtuIPAddress,
                      MODULE module,
                      MODULE_ID moduleId
                    );
/**
* arguments
* handle - board handle return from InitAndOpenBoard().
* return - error code.
*/
extern lu_int16_t CloseAndDeinitBoard(lu_int16_t boardHandle);

/**
* Get all ID strings.
* return - actual size of the string.
*/
extern lu_uint32_t GetAllIOIDStr(lu_char_t* IOIDStringBuf[], lu_uint32_t size);

extern lu_int16_t OpenBoard(COM_MODE comMode,
							MODULE module,
							 MODULE_ID moduleId);

extern lu_uint16_t CloseBoard(lu_int16_t handle);

SB_ERROR InitIOID( lu_uint16_t handle, 
				   lu_int8_t *IOIDString 
				 );

extern SB_ERROR GoToApplicationMode( lu_uint16_t handle );

extern SB_ERROR GoToBootloaderMode( lu_uint16_t handle );

extern SB_ERROR CalNVRAMSelect( lu_uint16_t handle, 
	 							lu_uint8_t  nvRamType
							  );

extern SB_ERROR CalNVRAMUpdate( lu_uint16_t handle, 
	 							lu_uint8_t  nvRamType,
								lu_uint8_t  nvRamBlk
							  );

extern SB_ERROR CalNVRAMErase( lu_uint16_t handle );

extern SB_ERROR InitIOMAP( lu_uint16_t handle );

extern SB_ERROR WriteCalibrationFile( lu_uint16_t handle,
								      lu_uint8_t calID,
								      lu_uint8_t calType,
								      lu_int8_t *calElementFile
									);

extern SB_ERROR ReadIOID( lu_uint16_t handle,
						  lu_int8_t *IOIDString,
						  lu_int32_t *valueToBeRead
						);

extern SB_ERROR WriteIOID( lu_uint16_t handle,
						   lu_int8_t *IOIDString,
						   lu_uint32_t valueToBeWritten
						);

extern SB_ERROR WriteFileToNVRAM( lu_uint16_t handle,
								  lu_int8_t *IOIDString,
								  lu_int8_t *fileName,
								  lu_uint8_t blkOffset
								);

extern SB_ERROR ReadFileFromNVRAM( lu_uint16_t handle,
								   lu_int8_t *IOIDString,
								   lu_int8_t *fileName,
								   lu_uint8_t blkOffset
								 );

extern SB_ERROR VerifyDSMCPLDEquation( lu_uint16_t handle, 
									   lu_int8_t *booleanEquationFile, 
									   lu_int8_t outputCheck,
									   lu_int8_t *testFile
							         );

extern SB_ERROR WriteCalibrationBlock( lu_uint16_t  handle,
								       lu_int8_t    *calFilesPath,
									   lu_int8_t    *calConfigFile,
									   lu_int8_t    *IOIDString,
									   lu_uint8_t   blkOffset
									 );

extern SB_ERROR WriteModuleFactoryInfo( lu_uint16_t  handle,
										lu_int8_t    *moduleInfoXMLFile,
									    lu_int8_t    *factoryInfoXMLFile,
									    lu_int8_t   *supplierID,
									    lu_int8_t   *assemblyNumber,
									    lu_int8_t   *assemblyRevision,
									    lu_uint32_t *serialNumber,
									    lu_uint8_t  *buildDay,
									    lu_uint8_t  *buildMonth,
									    lu_uint16_t *buildYear
									  );

extern SB_ERROR DownloadFirmware( lu_uint16_t handle,
								  lu_int8_t* firmwareFile
								);

extern SB_ERROR WriteModuleHMIOptions( lu_uint16_t  handle,
			  						   lu_int8_t    *moduleInfoXMLFile,
								       lu_int8_t    *hmiOptionsXMLFile,
									   lu_int8_t    *assemblyNumber,
									   lu_int8_t    *assemblyRevision
									 );
#endif /*_TESTIOMANAGER_H_INCLUDED*/
/*
 *********************** End of file ******************************************
 */
