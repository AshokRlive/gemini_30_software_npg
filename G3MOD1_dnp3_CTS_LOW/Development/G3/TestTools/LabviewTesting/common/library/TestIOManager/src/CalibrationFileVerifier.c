/*! \file
******************************************************************************
*       \author         Lucy Electric Ltd: http://www.lucyelectric.co.uk
*                       - Automation Dept.
******************************************************************************
*    PROJECT:
*       G3 [Module Name]
*
*    FILE NAME:
*               $Id: $
*               $HeadURL: $
*
*    DESCRIPTION:
*       \brief
*
*       Detailed description of this template module
*
*    CURRENT REVISION
*
*               $Revision: $: (Revision of last commit)
*               $Author: $: (Author of last commit)
*       \date   $Date: $: (Date of last commit)
*
*
*    CREATION
*
*   Date          Name        Details
*   --------------------------------------------------------------------------
*   05/10/16     saravanan_v     Initial version.
*
******************************************************************************
*   COPYRIGHT
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
******************************************************************************
*/

/*
******************************************************************************
* INCLUDES - Standard Library Modules Used
******************************************************************************
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
******************************************************************************
* INCLUDES - Project Specific Modules Used
******************************************************************************
*/
#include "CalibrationFileVerifier.h"
#include "TestIOManagerErrorCodes.h"

/*
******************************************************************************
* LOCAL - Definitions And Macros
******************************************************************************
*/
#define CAL_ELEMENT_MAX		32

/*
******************************************************************************
* LOCAL - Typedefs and Structures
******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Prototypes Of Local Functions
*******************************************************************************
*/
TEST_ERROR CheckSlopeToleranceRange( lu_float32_t numberOfPointXTolerance,
									 lu_float32_t	numberOfPointYTolerance,
									 lu_float32_t percentageOfTolerance,
									 PointStr     *referencePoints,
									 PointStr     *actualPoints,
									 lu_uint8_t   numberofPoints
								   );

TEST_ERROR StoreAndCountCalElementsFromFile( lu_int8_t  *refCalFile,
										     lu_int8_t  *actualCalFile,
										     lu_uint8_t *numberOfPoints
										   );

TEST_ERROR CheckPointsToleranceRange( lu_float32_t  numberOfPointXTolerance,
									  lu_float32_t  numberOfPointYTolerance,
									  PointStr      *referencePoints,
									  PointStr      *actualPoints,
									  lu_uint8_t    numberOfPoints,
									  lu_uint8_t    *calUpperLineFile,
								      lu_uint8_t    *calLowerLineFile
								    );

TEST_ERROR PointsWithInRange( PointStr     referencePoint,
							  PointStr     actualPoint,
							  lu_float32_t numberOfPointXTolerance,
						      lu_float32_t numberOfPointYTolerance,
							  PointStr     *pointUpperLimit,
						      PointStr     *pointLowerLimit
						    );

lu_float32_t GetSlopeBetweenTwoPoints( PointStr point1,
									   PointStr point2
							         );

lu_float32_t PercentageToFraction(lu_float32_t percentage);

/*
******************************************************************************
* EXPORTED - Variables
******************************************************************************
*/

/*
******************************************************************************
* LOCAL - Global Variables
******************************************************************************
*/
PointStr referencePoints[CAL_ELEMENT_MAX];
PointStr actualPoints   [CAL_ELEMENT_MAX];

/*
******************************************************************************
* Exported Functions
******************************************************************************
*/
int CalibrationFileVerifier( lu_int8_t    *refCalFile,
								    lu_int8_t    *actualCalFile,
								    lu_float32_t numberOfPointXTolerance,
								    lu_float32_t numberOfPointYTolerance, 
								    lu_float32_t percentageOfSlopeTolerance,
								    lu_uint8_t   *calUpperLineFile,
								    lu_uint8_t   *calLowerLineFile
								  )
{
	TEST_ERROR testError = TEST_ERROR_CAL_VERIFICATION_FAIL;
	lu_uint8_t numberOfElements = 0;

	testError = StoreAndCountCalElementsFromFile( refCalFile,
												  actualCalFile,
												  &numberOfElements
											    );

	if(testError == TEST_ERROR_NONE)
	{
		testError = CheckPointsToleranceRange( numberOfPointXTolerance,
									           numberOfPointYTolerance,
									           &referencePoints,
									           &actualPoints,
									           numberOfElements,
									           calUpperLineFile,
									           calLowerLineFile
								            );
	}
	
	if(testError == TEST_ERROR_NONE)	
	{
		testError = CheckSlopeToleranceRange( numberOfPointXTolerance,
											  numberOfPointYTolerance,
											  percentageOfSlopeTolerance,
											  &referencePoints,
											  &actualPoints,
											  numberOfElements
										    );
	}
		
	return testError;
}

/*
******************************************************************************
* Local Functions
******************************************************************************
*/

/*!
******************************************************************************
*   \Checking Points Tolerance Limits
*
*   \param parameterName Description
*
*   \return
*
******************************************************************************
*/
TEST_ERROR CheckPointsToleranceRange( lu_float32_t  numberOfPointXTolerance,
									  lu_float32_t  numberOfPointYTolerance,
									  PointStr      *referencePoints,
									  PointStr      *actualPoints,
									  lu_uint8_t    numberOfPoints,
									  lu_uint8_t    *calUpperLineFile,
									  lu_uint8_t    *calLowerLineFile
								    )
{
	TEST_ERROR pointsRangeError = TEST_ERROR_CAL_LIMIT_FILE_INCORRECT;
	lu_uint8_t pointsIdx;
	lu_bool_t pointsRangeOK = LU_TRUE;
	FILE *upperCalLineFilePtr, *lowerCalLineFilePtr;
	PointStr pointUpperLimit, pointLowerLimit;

	upperCalLineFilePtr = fopen(calUpperLineFile, "wb");
	lowerCalLineFilePtr = fopen(calLowerLineFile, "wb");

	if( (upperCalLineFilePtr != NULL) &&
	    (lowerCalLineFilePtr != NULL)
	  )
	{
		for(pointsIdx = 0; pointsIdx < numberOfPoints; pointsIdx++)
		{
			pointUpperLimit.pointX = referencePoints[pointsIdx].pointX + numberOfPointXTolerance;
			pointUpperLimit.pointY = referencePoints[pointsIdx].pointY + numberOfPointYTolerance;
			
			fprintf(upperCalLineFilePtr, "%f,", pointUpperLimit.pointX);
			fprintf(upperCalLineFilePtr, "%f\n",  pointUpperLimit.pointY);
			
			pointLowerLimit.pointX = referencePoints[pointsIdx].pointX - numberOfPointXTolerance;
			pointLowerLimit.pointY = referencePoints[pointsIdx].pointY - numberOfPointYTolerance;

			fprintf(lowerCalLineFilePtr, "%f,", pointLowerLimit.pointX);
			fprintf(lowerCalLineFilePtr, "%f\n",  pointLowerLimit.pointY);
		}

		fclose(upperCalLineFilePtr);
		fclose(lowerCalLineFilePtr);

		pointsRangeError = TEST_ERROR_NONE;

		/* Checking all points are in range and storing upper and lower point limits */
		for(pointsIdx = 0; pointsIdx < numberOfPoints; pointsIdx++)
		{
			if(pointsRangeError == TEST_ERROR_NONE)
			{
				pointUpperLimit.pointX = referencePoints[pointsIdx].pointX + numberOfPointXTolerance;
				pointUpperLimit.pointY = referencePoints[pointsIdx].pointY + numberOfPointYTolerance;
				pointLowerLimit.pointX = referencePoints[pointsIdx].pointX - numberOfPointXTolerance;
				pointLowerLimit.pointY = referencePoints[pointsIdx].pointY - numberOfPointYTolerance;

				if( (actualPoints[pointsIdx].pointX >= pointLowerLimit.pointX) &&
					(actualPoints[pointsIdx].pointY >= pointLowerLimit.pointY) &&
					(actualPoints[pointsIdx].pointX <= pointUpperLimit.pointX) &&
					(actualPoints[pointsIdx].pointY <= pointUpperLimit.pointY)
				  )
				{
					pointsRangeError = TEST_ERROR_NONE;
				}
				else
				{
					pointsRangeError = TEST_ERROR_CAL_POINT_RANGE;
					break;
				}
			}
		}
	}

	return pointsRangeError;
}

/*!
******************************************************************************
*   \Checking Limits
*
*   \param parameterName Description
*
*   \return
*
******************************************************************************
*/
TEST_ERROR CheckSlopeToleranceRange( lu_float32_t numberOfPointXTolerance,
								     lu_float32_t	numberOfPointYTolerance,
								     lu_float32_t percentageOfTolerance,
								     PointStr     *referencePoints,
								     PointStr     *actualPoints,
								     lu_uint8_t   numberOfPoints
								   )
{
	TEST_ERROR slopeRangeError = TEST_ERROR_CAL_SLOPE_RANGE;

	lu_float32_t  upperLimit, lowerLimit, tolerance, slopeOfReferencePoints, slopeOfActualPoints; 
	lu_uint16_t slopeIdx;
		
	slopeRangeError = TEST_ERROR_NONE;

	/* Calcualting tolerance factor */
	tolerance = PercentageToFraction(percentageOfTolerance);

	for(slopeIdx = 0; slopeIdx < (numberOfPoints - 1); slopeIdx++)
	{
		if(slopeRangeError == TEST_ERROR_NONE)
		{
			slopeOfReferencePoints = GetSlopeBetweenTwoPoints(referencePoints[slopeIdx], referencePoints[slopeIdx + 1]);
			slopeOfActualPoints    = GetSlopeBetweenTwoPoints(actualPoints[slopeIdx],    actualPoints[slopeIdx + 1]);

			lowerLimit = slopeOfReferencePoints - (slopeOfReferencePoints * tolerance);
			upperLimit = slopeOfReferencePoints + (slopeOfReferencePoints * tolerance);
									
			if((slopeOfActualPoints >= lowerLimit) &&
				(slopeOfActualPoints <= upperLimit)
			  )
			{
				slopeRangeError = TEST_ERROR_NONE;
			}
			else
			{
				slopeRangeError = TEST_ERROR_CAL_SLOPE_RANGE;
				break;
			}
		} 
	} 
	
	return slopeRangeError;
}

/*!
******************************************************************************
*   \Storing and checking count of calibration elements in arrry
*
*   \param parameterName Description
*
*   \return
*
******************************************************************************
*/
TEST_ERROR StoreAndCountCalElementsFromFile( lu_int8_t  *refCalFile,
										     lu_int8_t  *actualCalFile,
										     lu_uint8_t *numberOfPoints
										   )
{
	
	/* SB_ERROR prtotype */
	TEST_ERROR testError = TEST_ERROR_CAL_FILE_INCORRECT;

	/* Files line scanning indexes */
	lu_uint32_t refCalFileScanIdx = NULL, actualCalFileScanIdx = NULL;

	lu_uint8_t elementIdx = 0;

	/* File pointers for calibration files */
	FILE *refCalFilePtr, *actualCalFilePtr;

	/* Opening reference and actual calibration files */
	refCalFilePtr    = fopen(refCalFile,    "rb");
	actualCalFilePtr = fopen(actualCalFile, "rb");

	if( (refCalFilePtr    != NULL)  && 
	    (actualCalFilePtr != NULL)
	  )
	{
		testError = TEST_ERROR_NONE;

		/* Scanning every line of the files until EOF */
		while( ( refCalFileScanIdx    != EOF  ) &&
			   ( actualCalFileScanIdx != EOF  ) &&
			   ( testError == TEST_ERROR_NONE )
			 )
		{
			testError = TEST_ERROR_CAL_FILE_INCORRECT;

			refCalFileScanIdx    = fscanf( refCalFilePtr,    
										   "%f,%f", 
										   &referencePoints[elementIdx].pointX, 
										   &referencePoints[elementIdx].pointY
										 );

			actualCalFileScanIdx = fscanf( actualCalFilePtr, 
										   "%f,%f", 
										   &actualPoints[elementIdx].pointX, 
										   &actualPoints[elementIdx].pointY
										 );

			if( ( actualCalFileScanIdx == refCalFileScanIdx ) )
			{
			    testError = TEST_ERROR_NONE;

				if(( refCalFileScanIdx    != EOF ) &&
				   ( actualCalFileScanIdx != EOF ) 
				  )
				{
					elementIdx++;
				}
			}
		}
		
		*numberOfPoints = elementIdx;

		/* Closing reference and actual calibration files */
		fclose(refCalFilePtr);
		fclose(actualCalFilePtr);
	}

	return testError;
}

/*!
******************************************************************************
*   \Calculating Slope of line between two points
*
*   \param parameterName Description
*
*   \return
*
******************************************************************************
*/
lu_float32_t GetSlopeBetweenTwoPoints( PointStr point1,
									   PointStr point2
							         )
{
	lu_float32_t  slope;
	lu_float32_t  deltaX, deltaY;

	deltaX = ( point2.pointX - point1.pointX );
	deltaY = ( point2.pointY - point1.pointY );

	slope  = (lu_float32_t)( deltaY / deltaX );

	return slope;
}

lu_float32_t PercentageToFraction(lu_float32_t percentage)
{
	return (percentage / 100);
}

/*
*********************** End of file ******************************************
*/
