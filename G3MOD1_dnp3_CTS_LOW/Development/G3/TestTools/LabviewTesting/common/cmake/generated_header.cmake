# The project that needs to generate headers from XML/SVN should include this cmake file.
# How to use:
#  1. Make sure the required varible "CMAKE_G3_PATH_ROOT" is defined
#  2. Make sure the required varible "XML_BOARDIOID" is defined
#  3. Include this cmake file in your CMakeLists.txt

FIND_PROGRAM(XSLTPROC xsltproc PATHS /usr/bin /usr/local/bin c:/xsltproc)

if(NOT XSLTPROC)
message(SEND_ERROR "!!!Program xsltproc is not found!")
endif() 

if(NOT DEFINED CMAKE_G3_PATH_ROOT)
message(SEND_ERROR "!!!Please specify the path of G3 root with varible CMAKE_G3_PATH_ROOT in your project CMakeLists.txt")
else()
message("CMAKE_G3_PATH_ROOT: ${CMAKE_G3_PATH_ROOT}")
endif()

if(NOT DEFINED XML_BOARDIOID)
message(SEND_ERROR "!!!Please specify the path of the XML file for generating BoardIOID.h by defining the varible XML_BOARDIOID in your project CMakeLists.txt!!!")
endif()


if(NOT EXISTS ${XML_BOARDIOID})
message(SEND_ERROR "XML file not found: ${XML_BOARDIOID}")
endif()


# ------------------------GENERATE COMMON HEADER -------------------------------
# Set the path of the header files generated from XMLs, svn ,etc
set(CMAKE_G3_GENERATED_HEADER_BUILD_PATH "${CMAKE_G3_PATH_ROOT}/TestTools/LabviewTesting/common/includeGenerated/")
set(CMAKE_G3_GENERATED_HEADER_INCLUDE 
    "${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/"
    "${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/SysAlarm/"
    "${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/ModuleProtocol/"
    "${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/NVRAMDef/"
    )
add_subdirectory(${CMAKE_G3_GENERATED_HEADER_BUILD_PATH} commom/includeGenerated/)
# Generated header 
include_directories(${CMAKE_G3_GENERATED_HEADER_INCLUDE}/)


# ------------------------GENERATE BOARDIO HEADER -------------------------------
# Generate BoardIO header
set(XSLT_BOARDIOID "${CMAKE_G3_PATH_ROOT}/common/xslt/xml2C_boardArray.xsl")
set(OUTPUT_BOARDIOID "${CMAKE_CURRENT_SOURCE_DIR}/../include/BoardIOID.h")

ADD_CUSTOM_COMMAND( OUTPUT ${CMAKE_CURRENT_SOURCE_DIR}/../include/BoardIOID.h
                    PRE_BUILD
                    COMMAND ${XSLTPROC} ${XSLT_BOARDIOID} ${XML_BOARDIOID} > ${OUTPUT_BOARDIOID}
                    DEPENDS ${XML_BOARDIOID} 
                  )

ADD_CUSTOM_TARGET( BoardIOID
                  #COMMAND ${CMAKE_COMMAND}                    
                   DEPENDS ${OUTPUT_BOARDIOID}  
                 )
                 
                 