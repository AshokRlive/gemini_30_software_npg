#This is a common cmake scripts included by all slave buildings in LabviewTesting.


#--------------------------- VARIBLES ------------------------------------------
# Set library variables
SET(CRC32_LIB Crc32)
SET(LIBROXML_LIB roxml-static)
if(WIN32)
    SET (SOCKET_LIB ws2_32) # win32 socket library
else()
    UNSET(SOCKET_LIB)
endif()

# Set library source list
SET(LIB_SRC
	 ${CMAKE_G3_PATH_ROOT}/TestTools/LabviewTesting/common/library/TestIOManager/src/CalibrationFileVerifier.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/LabviewTesting/common/library/TestIOManager/src/HeartBeatMsg.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/LabviewTesting/common/library/TestIOManager/src/TestIOManager.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/MainDll.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/BoardCommandsDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/CalElementAdderDLL.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/CalBlockWriterDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/GPIOTestsDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/I2CExpIOTestsDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/ADCTestsDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/LucyCANDecoderDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/SPIADDigiPotTestsDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/I2CTemperatureSensorTestsDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/SPIADCTestsDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/CmdExecutorDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/NvramFactoryInfoWriteDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/NvramFactoryInfoReadDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/NvramPSMOptionsWriteDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/NvramPSMOptionsReadDLL.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/NvramHMIOptionsWriteDLL.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/NvramHMIOptionsReadDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/NvramBatteryOptionsWriteDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/NvramBatteryOptionsReadDLL.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/DACTestDLL.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/I2CADCTestsDLL.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/KvaserCan.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/CANProtocolCodec.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/CANProtocolFraming.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/FirmwareDownloader.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/FirmwareVerifier.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/CalibrationBlock.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/CalElementReader.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/CalElementAdder.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/CalBlockWriter.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/FileCompare.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/SingleCmdSender.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/NvramReadWriteVerify.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/NvramFactoryInfoWrite.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/NvramFactoryInfoRead.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/NvramPSMOptionsWrite.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/NvramHMIOptionsWrite.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/NvramHMIOptionsRead.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/NvramPSMOptionsRead.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/NvramBatteryOptionsWrite.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/NvramBatteryOptionsRead.c
     ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/NvramXMLParser.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/ModuleInfoCreator.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/FPMV2I2CADE78xxTests.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/ADE78xxI2CDLL.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/FPMV2SPIADE78xxTests.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/ADE78xxSPIDLL.c
	 ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/I2CHumiditySensorTests.c

	 ${CMAKE_G3_PATH_ROOT}/common/library/lib_hat/src/hat_client.c
	 ${CMAKE_G3_PATH_ROOT}/common/library/lib_hat/src/hat_serialization.c
	 ${CMAKE_G3_PATH_ROOT}/common/library/lib_hat/src/hat_socket.c
	 ${CMAKE_G3_PATH_ROOT}/common/library/lib_hat/src/hat_utils.c
) 

if(${EXE_NAME} STREQUAL "DSMTestIOManager")
	add_library( ${EXE_NAME} SHARED
		         ${EXE_NAME}DLL.def
			     ${LIB_SRC}
				 ${CMAKE_G3_PATH_ROOT}/TestTools/LabviewTesting/DSM/src/CPLDDesignVerifier.c
				)
endif()

if(${EXE_NAME} STREQUAL "PSMTestIOManager" OR 
   ${EXE_NAME} STREQUAL "SCMTestIOManager" OR
   ${EXE_NAME} STREQUAL "IOMTestIOManager" OR
   ${EXE_NAME} STREQUAL "HMITestIOManager" OR
   ${EXE_NAME} STREQUAL "FPMV2TestIOManager" OR
   ${EXE_NAME} STREQUAL "SCMMK2TestIOManager"
  )
    #List all the source files needed to build the executable
	add_library( ${EXE_NAME} SHARED
		         ${EXE_NAME}DLL.def
				 ${LIB_SRC}				 
				)
endif()

if(${EXE_NAME} STREQUAL "FDMTestIOManager")
   add_library( ${EXE_NAME} SHARED
	            ${EXE_NAME}DLL.def
	            ${LIB_SRC}
		        ${CMAKE_G3_PATH_ROOT}/TestTools/LabviewTesting/FDM/src/FDMTests.c
			   )
endif()

if(${EXE_NAME} STREQUAL "FPMTestIOManager")
   add_library( ${EXE_NAME} SHARED
	            ${EXE_NAME}DLL.def
	            ${LIB_SRC}
		        ${CMAKE_G3_PATH_ROOT}/TestTools/LabviewTesting/FPM/src/FPITests.c
			   )
endif()


#--------------------------- INCLUDE -------------------------------------------
# Global include
include_directories (${CMAKE_G3_PATH_ROOT}/common/include/)
include_directories (${CMAKE_G3_PATH_ROOT}/common/include/NVRAMDef)
include_directories (${CMAKE_G3_PATH_ROOT}/common/library/LinearInterpolation/include)
include_directories (${CMAKE_G3_PATH_ROOT}/common/library/crc32/include)
include_directories (${CMAKE_G3_PATH_ROOT}/RTU/slaveBoards/commom/library/Calibration/include)
include_directories (${CMAKE_G3_PATH_ROOT}/RTU/slaveBoards/commom/library/ADE78xxFPI/include)
include_directories (${CMAKE_G3_PATH_ROOT}/RTU/slaveBoards/commom/include)

# Lucy Can Send include
include_directories (${CMAKE_G3_PATH_ROOT}/TestTools/protocol/thirdparty/Canlib/INC)

# Kvaser Can library include
include_directories (${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/include/)
include_directories (${CMAKE_G3_PATH_ROOT}/TestTools/protocol/CanKing/LucyCanSend/src/dll/include)


# LabviewTesting common include
include_directories (${CMAKE_G3_PATH_ROOT}/TestTools/labviewTesting/common/library/TestIOManager/include/)
include_directories (${CMAKE_G3_PATH_ROOT}/TestTools/labviewTesting/common/include)

# SlaveBoards common include
include_directories (${CMAKE_G3_PATH_ROOT}/RTU/slaveBoards/commom/library/IOManager/include/)

# Local include
include_directories (../include/)
include_directories (./IOManager/include/)
include_directories (./)

# Add XML parser libroxml library include
include_directories (${CMAKE_G3_PATH_ROOT}/thirdParty/libroxml-2.2.0/inc/)

#Add HAT library include
include_directories (${CMAKE_G3_PATH_ROOT}/common/library/lib_hat/src/)
include_directories (${CMAKE_G3_PATH_ROOT}/common/library/lib_hat/include/)


#--------------------------- BUILDING DIRECTORY---------------------------------
# XML parser libroxml library
add_subdirectory(${CMAKE_G3_PATH_ROOT}/thirdParty/libroxml-2.2.0/ thirdparty/libroxml)

# CRC32 library
add_subdirectory(${CMAKE_G3_PATH_ROOT}/common/library/crc32/src/ common/library/crc32)

#--------------------------- DEPANDENCIES- -------------------------------------
ADD_DEPENDENCIES(${EXE_NAME} globalHeaders)
ADD_DEPENDENCIES(${EXE_NAME} Crc32)
ADD_DEPENDENCIES(${EXE_NAME} roxml-static)
ADD_DEPENDENCIES(${EXE_NAME} BoardIOID)

#--------------------------- LINKING- ------------------------------------------
target_link_libraries( ${EXE_NAME} 
                       ${CMAKE_G3_PATH_ROOT}/TestTools/protocol/thirdparty/Canlib/Lib/MS/canlib32.lib
                       ${LIBROXML_LIB}
                       ${CRC32_LIB}
                       ${SOCKET_LIB}  
                     )
                     
# Remove "lib" from the name of library                     
set_target_properties(${EXE_NAME} PROPERTIES PREFIX "")

# Get site name                     
SITE_NAME(HOST_NAME)
MESSAGE("HOST_NAME ='${HOST_NAME}' ")

# Copying artifacts
if(${HOST_NAME} STREQUAL "NI-PROJECT3")

add_custom_command( TARGET ${EXE_NAME} 
                    POST_BUILD
                    COMMAND ${CMAKE_COMMAND} -E copy ../build/Release/${EXE_NAME}.dll C:/LabviewData/${EXE_NAME}.dll 
                  )

add_custom_command( TARGET ${EXE_NAME} 
                    POST_BUILD
                    COMMAND ${CMAKE_COMMAND} -E copy ../build/Release/${EXE_NAME}.dll C:/sw_dev/gemini_30_labview/Library/G3TestAPI/TestDLL/${EXE_NAME}.dll
                  )
endif()


install (TARGETS ${EXE_NAME}
		RUNTIME DESTINATION bin
		)