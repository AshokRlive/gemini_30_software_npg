/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Processor specific GPIO MAP header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _PROCESSORGPIOMAP_INCLUDED
#define _PROCESSORGPIOMAP_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "lpc17xx_pinsel.h"
//#include "lpc17xx_gpio.h"



/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define MAX_GPIO_PORTS					5
#define MAX_GPIO_PORT_BITS				32

#define NUM_GPIO_PINPORT_IDX			(MAX_GPIO_PORTS * MAX_GPIO_PORT_BITS)


#define PROC_MAX_FUNC_IDX	3

#define PROC_MAX_PORT		PINSEL_PORT_4

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */



typedef struct ProcGPIOMapDef {
	lu_int8_t		port;
	lu_int8_t		pin;
	lu_int8_t		enable;
	GPIO_PIN_FUNC	pinFunc[PROC_MAX_FUNC_IDX];
}ProcGPIOMapStr;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR ProcGPIOConfigure(
		lu_uint8_t 		port,
		lu_uint8_t 		pin,
		IO_CLASS 		ioClass,
		lu_uint32_t 	pinMode
		);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_uint8_t ProcGPIOCheckPeripheralPinPort(GPIO_PIN_FUNC pinFunc, lu_int8_t port, lu_int8_t pin);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint32_t ProcGPIORead(lu_uint8_t port, lu_uint8_t pin);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void ProcGPIOWrite(lu_uint8_t port, lu_uint8_t pin, lu_uint8_t value);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint32_t ProcGPIOReadPort(lu_uint8_t port);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void ProcGPIOWritePort(lu_uint8_t port, lu_uint32_t value);

#endif /* _PROCESSORGPIOMAP_INCLUDED */

/*
 *********************** End of file ******************************************
 */
