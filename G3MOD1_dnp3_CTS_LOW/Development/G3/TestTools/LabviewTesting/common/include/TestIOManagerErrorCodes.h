/*! \file
******************************************************************************
*       \author         Lucy Electric Ltd: http://www.lucyelectric.co.uk
*                       - Automation Dept.
******************************************************************************
*    PROJECT:
*       G3 [Module Name]
*
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*       \brief
*
*       Detailed description of this template module
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*
*
*    CREATION
*
*   Date          Name        Details
*   --------------------------------------------------------------------------
*   10/10/16      saravanan_v     Initial version.
*
******************************************************************************
*   COPYRIGHT
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
******************************************************************************
*/

/*
******************************************************************************
* INCLUDES - Standard Library Modules Used
******************************************************************************
*/


/*
******************************************************************************
* INCLUDES - Project Specific Modules Used
******************************************************************************
*/

/*
******************************************************************************
* EXPORTED - Definitions And Macros
******************************************************************************
*/

/*
******************************************************************************
* EXPORTED - Typedefs and Structures
******************************************************************************
*/
typedef enum
{
    TEST_ERROR_NONE,

	/* Common Errors */

	/* Communication Error */
	TEST_ERROR_CAN_INIT_FAIL,
	
	/* Calibration Test Errors */
	TEST_ERROR_CAL_VERIFICATION_FAIL,
	TEST_ERROR_CAL_FILE_INCORRECT,
	TEST_ERROR_CAL_LIMIT_FILE_INCORRECT,
	TEST_ERROR_CAL_SLOPE_RANGE,
	TEST_ERROR_CAL_POINT_RANGE,

    TEST_ERROR_LAST
}TEST_ERROR;


/*
******************************************************************************
* EXPORTED - Variables
******************************************************************************
*/

/*
******************************************************************************
* EXPORTED - Functions
******************************************************************************
*/
/*
*********************** End of file ******************************************
*/