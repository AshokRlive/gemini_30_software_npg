/*
****** LOCAL COPY OF THIS FILE USED TO AVOID CONFLICT BETWEEN GCC AND MSVS TOOL CHAINS *******
*/


/* Public Macros -------------------------------------------------------------- */
/** @defgroup PINSEL_Public_Macros PINSEL Public Macros
 * @{
 */

/*********************************************************************//**
 *!< Macros define for PORT Selection
 ***********************************************************************/
#define PINSEL_PORT_0 	((0))	/**< PORT 0*/
#define PINSEL_PORT_1 	((1))	/**< PORT 1*/
#define PINSEL_PORT_2 	((2))	/**< PORT 2*/
#define PINSEL_PORT_3 	((3))	/**< PORT 3*/
#define PINSEL_PORT_4 	((4))	/**< PORT 4*/

/***********************************************************************
 * Macros define for Pin Function selection
 **********************************************************************/
#define PINSEL_FUNC_0	((0))	/**< default function*/
#define PINSEL_FUNC_1	((1))	/**< first alternate function*/
#define PINSEL_FUNC_2	((2))	/**< second alternate function*/
#define PINSEL_FUNC_3	((3))	/**< third or reserved alternate function*/

/***********************************************************************
 * Macros define for Pin Number of Port
 **********************************************************************/
#define PINSEL_PIN_0 	((0)) 	/**< Pin 0 */
#define PINSEL_PIN_1 	((1)) 	/**< Pin 1 */
#define PINSEL_PIN_2 	((2)) 	/**< Pin 2 */
#define PINSEL_PIN_3 	((3)) 	/**< Pin 3 */
#define PINSEL_PIN_4 	((4)) 	/**< Pin 4 */
#define PINSEL_PIN_5 	((5)) 	/**< Pin 5 */
#define PINSEL_PIN_6 	((6)) 	/**< Pin 6 */
#define PINSEL_PIN_7 	((7)) 	/**< Pin 7 */
#define PINSEL_PIN_8 	((8)) 	/**< Pin 8 */
#define PINSEL_PIN_9 	((9)) 	/**< Pin 9 */
#define PINSEL_PIN_10 	((10)) 	/**< Pin 10 */
#define PINSEL_PIN_11 	((11)) 	/**< Pin 11 */
#define PINSEL_PIN_12 	((12)) 	/**< Pin 12 */
#define PINSEL_PIN_13 	((13)) 	/**< Pin 13 */
#define PINSEL_PIN_14 	((14)) 	/**< Pin 14 */
#define PINSEL_PIN_15 	((15)) 	/**< Pin 15 */
#define PINSEL_PIN_16 	((16)) 	/**< Pin 16 */
#define PINSEL_PIN_17 	((17)) 	/**< Pin 17 */
#define PINSEL_PIN_18 	((18)) 	/**< Pin 18 */
#define PINSEL_PIN_19 	((19)) 	/**< Pin 19 */
#define PINSEL_PIN_20 	((20)) 	/**< Pin 20 */
#define PINSEL_PIN_21 	((21)) 	/**< Pin 21 */
#define PINSEL_PIN_22 	((22)) 	/**< Pin 22 */
#define PINSEL_PIN_23 	((23)) 	/**< Pin 23 */
#define PINSEL_PIN_24 	((24)) 	/**< Pin 24 */
#define PINSEL_PIN_25 	((25)) 	/**< Pin 25 */
#define PINSEL_PIN_26 	((26)) 	/**< Pin 26 */
#define PINSEL_PIN_27 	((27)) 	/**< Pin 27 */
#define PINSEL_PIN_28 	((28)) 	/**< Pin 28 */
#define PINSEL_PIN_29 	((29)) 	/**< Pin 29 */
#define PINSEL_PIN_30 	((30)) 	/**< Pin 30 */
#define PINSEL_PIN_31 	((31)) 	/**< Pin 31 */

/***********************************************************************
 * Macros define for Pin mode
 **********************************************************************/
#define PINSEL_PINMODE_PULLUP		((0))	/**< Internal pull-up resistor*/
#define PINSEL_PINMODE_TRISTATE 	((2))	/**< Tri-state */
#define PINSEL_PINMODE_PULLDOWN 	((3)) 	/**< Internal pull-down resistor */

/***********************************************************************
 * Macros define for Pin mode (normal/open drain)
 **********************************************************************/
#define	PINSEL_PINMODE_NORMAL		((0))	/**< Pin is in the normal (not open drain) mode.*/
#define	PINSEL_PINMODE_OPENDRAIN	((1)) 	/**< Pin is in the open drain mode */
