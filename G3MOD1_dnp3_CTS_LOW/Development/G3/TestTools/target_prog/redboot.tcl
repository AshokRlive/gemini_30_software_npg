# This module contains procedures to interact with RedBoot

# Procedure to read a line from RedBoot. Returns block, eof, prompt or txt,
# and puts the line in linebuff
proc read_boot {} {
    global inbuff linebuff params

    # Assemble line in linebuff. This is either handed back on
    # terminator or timeout
    set linebuff ""
    set timeout 0

    # Loop until we find a reason to exit
    while { 1 } {
	# If we haven't got any characters get some
	if { [ string length $inbuff ] <= 0 } {
	    set ret [ get_boot ]
	    if { $ret != "" } {
		return $ret
	    }
	}

	# If we didn't get any chars either we have a prompt or no output
	if { [ string length $inbuff ] <= 0 } {
	    if { [ string length $linebuff ] <= 0 } {
		return "block" ;
	    } else {
		incr timeout ;
		if { $timeout < 10 } {
		    after 20
		} else {
		    return "prompt" ;
		}
	    }
	}

	# Try to move characters to linebuf until we get a terminator
	set chr [ string range $inbuff 0 0 ]
	set inbuff [ string range $inbuff 1 1000 ]
	if { $chr >= " " } {
	    set timeout 0
	    append linebuff $chr ;
	} else {
	    if { $chr == "\n" } {
		return "txt" ;
	    }
	}
    }
}

# Procedure to read lines from RedBoot until one matching a regexp is
# found. Then return a name and the submatches. If RedBoot stops on a
# prompt return null.
#
# Parameter is a list of lists, one for each possible match, the list
# containing the name of the list and the regexp.
#
# Called with a null list this will run on to the next prompt
proc match_boot { match } {
    global linebuff

    while { 1 } {
	set state [ read_boot ] ;

	if { $state == "txt" } {
	    log_boot $linebuff "" ""
	}

	if { $state == "txt" || $state == "prompt" } {
	    foreach { sublist } $match {
		foreach { name exp } $sublist {
		    set tst [ regexp -inline $exp $linebuff ]
		    if { $tst != "" } {
			return [ concat $name $tst ]
		    }
		}
	    }
	}
	if { $state == "prompt" } {
	    return ""
	}
    }
}

# Procedure to do the initial handshakes and get a prompt
proc start_boot {} {

    set init {
	{ "type" "Board Type" }
	{ "redboot" "RedBoot>" }
    }

    while { 1 } {
	set res [ match_boot $init ]
	switch -exact [ lindex $res 0 ] {
	    "redboot" { break }
	    "type" { stop_boot }
	}
    }

}

# Procedure to turn a dotted decimal IP address into an integer
proc dots_to_int { dots } {
    regexp "(\[0-9\]+)\\.(\[0-9\]+)\\.(\[0-9\]+)\\.(\[0-9\]+)" $dots xx a b c d
    set n [ expr ( ( $a * 256 + $b ) * 256 + $c ) * 256 + $d ]
}

# Procedure to turn an "n bits" netmask into an integer
proc bits_to_int { bits } {
    return [ expr ( -( 1 << ( 32 - $bits ) ) ) & 0xFFFFFFFF ]
}

# Procedure to check that a string is a valid IP address
proc valid_ip { str } {
    regexp "(\[0-9\]+)\\.(\[0-9\]+)\\.(\[0-9\]+)\\.(\[0-9\]+)" $str xx a b c d
    return [ expr $a < 256 && $b < 256 && $c < 256 && $d < 256 ]
}

# Procedure to turn an integer netmask into a bit count. Returns -1 if
# the integer is not a contiguous netmask
proc int_to_bits { mask } {
    for { set n 0 } { $n <= 32 } { incr n } {
	if { $mask == [ bits_to_int $n ] } {
	    return $n
	}
    }
}
 
# This procedure either uses the params(ip) and params(server)
# variables to set the target ip address, or if params(ip) is "dhcp"
# uses the DHCP server on the local subnet to set the addresses. It
# returns IP, Netmask, Gateway and Server in a list.
proc ip_boot {} {
    global params

    set addr {
	{ "ip" "IP: (\[0-9\]+\\.\[0-9\]+\\.\[0-9\]+\\.\[0-9\]+)/(\[0-9\]+\\.\[0-9\]+\\.\[0-9\]+\\.\[0-9\]+), Gateway: (\[0-9\]+\\.\[0-9\]+\\.\[0-9\]+\\.\[0-9\]+)" }
	{ "def" "Default server: (\[0-9\]+\\.\[0-9\]+\\.\[0-9\]+\\.\[0-9\]+)" }
    }

    # Send command
    if { $params(dhcp) } {
	send_boot "ip_address -b"
    } else {
	set netmask [ int_to_bits [ dots_to_int $params(netmask) ] ]
	send_boot "ip_address -l $params(ip)/$netmask -h $params(server)"
    }

    # Parse the response
    while { 1 } {
	set res [ match_boot $addr ]
	switch -exact [ lindex $res 0 ] {
	    "" { break }
	    "ip"  { foreach { dummy1 dummy2 ip mask gateway } $res {} }
	    "def" { foreach { dummy1 dummy2 defsvr } $res {} }
	}
    }
    return [list $ip $mask $gateway $defsvr]
}

# This procedure parses the partition list and returns a list of lists
# which are the partition entries
proc fis_boot {} {
    set fis {
	{"fis" "(.*?) *(0x\[0-9a-fA-F\]{8})  (0x\[0-9a-fA-F\]{8})  (0x\[0-9a-fA-F\]{8})  (0x\[0-9a-fA-F\]{8})"}
    }
    set parts ""

    send_boot "fis list"
    while { 1 } {
	set res [ match_boot $fis ]
	switch -exact [ lindex $res 0 ] {
	    "" { break }
	    "fis" {
		set entry [ list [ lindex $res 2 ] [ lindex $res 3 ] [ lindex $res 5 ] [ lindex $res 4 ] [ lindex $res 6 ] ]
		set parts [ linsert $parts end $entry ]
	    }
	}
    }
    return $parts
}

# Procedure to program the configuration with the fconfig command. In
# general it runs fconfig with the -n option to make the output easier
# to parse, then only changes the parameters that need
# changing. Parameters are a list of changes to make and the boot
# script.

proc config_boot {changes script} {
    # Get started
    send_boot "fconfig -n"

    # Matching for the prompts
    set fprompts {
	{ "val" "^(\\w+): " }
	{ "conf" "continue \\(y/n\\)" }
	{ "script" "^>>" }
	{ "end" "^RedBoot" }
    }

    # Loop handling the requests
    set scriptpos 0
    while {1} {
	set res [ match_boot $fprompts ]
	switch -exact [ lindex $res 0 ] {
	    "" {
		# Unrecognised prompt
		log_error "Unexpected response"
	    }

	    "val" {
		# This is a "set value" prompt. If it matches a new
		# value enter the change, if not just CR through
		# it. Note that prompts we want to ignore have a val
		# of null.
		set keep 1
		foreach { z } $changes {
		    foreach { sym val } $z {}
		    if { $sym == [ lindex $res 2 ] } {
			if { $val != "" } {
			    def_boot $val
			}
			set keep 0
			break ;
		    }
		}
		if { $keep } {
		    send_boot ""
		}
	    }

	    "script" {
		send_boot [ lindex $script $scriptpos ]
		incr scriptpos
	    }

	    "conf" { send_boot "y" }

	    "end" { break }
	}
    }
}

# Procedure to execute a list of commands
proc exec_boot {cmds} {
    foreach { c } $cmds {
	send_boot $c
	while { 1 } {
	    set res [ match_boot {
		{ "err" "Error:" }
		{ "cont" "continue \(y/n\)\?" }
	    }]
	    switch -exact [ lindex $res 0 ] {
		"" { break }
		"err" {
		    puts "Error, load abandoned"
		    error
		}
		"cont" {
		    send_boot "y"
		}
	    }
	}
    }
}
