# Functions to handle partitions and their contents

# Partitions are expressed as lists with entries
#
# Name, Flash add, Size, RAM add, Entry

# Find and read the distribution kit. Parameter is the path, returns
# the partition table.
proc part_getkit {path} {
    global temp_path
    if { [ file exists $path ] } {
	exec unzip -o -d $temp_path $path
	set partitions $temp_path/partitions.txt
	if { [ file exists $partitions ] } {
	    source $partitions
	} else {
	    error "File $path is not a distribution kit"
	}
    } else {
	error "Can't find $path"
    }
    return $partlist
}

# Procedure returns true if two partitions overlap.
proc part_overlap {left right} {
    set l_bot [ expr [ lindex $left 1 ] + 0 ]
    set l_top [ expr $l_bot + [ lindex $left 2 ] - 1 ]
    set r_bot [ expr [ lindex $right 1 ] + 0 ]
    set r_top [ expr $r_bot + [ lindex $right 2 ] - 1 ]
    set overlap [ expr ( $r_top >= $l_bot ) && ( $l_top >= $r_bot ) ]
    return $overlap ;
}

# Procedure returns true if partition is system owned
proc part_sys {part} {
    return [ regexp "\[A-Z\]" [lindex $part 0] ] ;
}


# Merge old and new partition tables. If a new partition overlaps an
# old one the old one becomes a candidate for deletion. Parameters are
# the old and new partition lists from RedBoot and the distribution
# kit, return is a list of commands
proc part_merge {old new} {
    global params

    # Compare every old partition against every new one. If partitions
    # are identical the old one is kept, if they overlap the old one
    # is deleted and the new one created, if not the new one is
    # created. Don't touch system partitions ever.
    foreach { p } $new {
	set creates([lindex $p 0]) 1
    }

    foreach { q } $old {
	set deletes([lindex $q 0]) 0
	foreach { p } $new {
	    # Sys is true if system partition, r is new descriptor
	    # fiddled to look like old, s is true if new & old
	    # overlap, same is true if new and old are identical
	    set sys [ part_sys $q ]
	    set r [ lrange $p 0 4 ]
	    set s [ part_overlap $p $q ]
	    set same [ expr \$r == \$q ]

	    # Delete old parts that overlap new ones unless identical
	    if { !$sys && $s && ! $same } {
		set deletes([lindex $q 0]) 1
	    }

	    # If an identical partition exists inhibit create
	    if {$same} {
		set creates([lindex $p 0]) 0
	    }
	}
    }

    # Display what has happened
    if { $params(verbose) } {
	set fmt "%s %20s %08X %08X %08X %08X%s"
	set hdg "Sys               Name    Flash     Size      RAM    Entry"
	log_boot "Existing partitions:\n$hdg" "" ""
	foreach { q } $old {
	    set stat [ expr $deletes([lindex $q 0]) ? \" Delete\" : \"\" ]
	    set sys [ expr [ part_sys $q ] ? \"*\" : \" \" ]
	    set str "format \$fmt \"$sys\" $q \"$stat\""
	    log_boot [ eval $str ] "" ""
	}
	log_boot "Distribution kit:\n$hdg" "" ""
	foreach { q } $new {
	    set stat [ expr $creates([lindex $q 0]) ? \" Create\" : \"\" ]
	    set str "format \$fmt \" \" [lrange $q 0 4] \"$stat\""
	    log_boot [ eval $str ] "" ""
	}
    }

    # Create the commands
    set commands ""
    foreach { q } $old {
	if { $deletes([lindex $q 0]) } {
	    set cmd "fis delete [lindex $q 0]"
	    set commands [linsert $commands end $cmd]
	}
    }
    foreach { q } $new {
	if { $creates([lindex $q 0]) } {
	    set cmd "fis create"
	    set cmd "$cmd -n [lindex $q 0]"
	    set cmd "$cmd -f [lindex $q 1]"
	    set cmd "$cmd -l [lindex $q 2]"
	    set cmd "$cmd -r [lindex $q 3]"
	    set cmd "$cmd -e [lindex $q 4]"
	} else {
	    set cmd "fis erase"
	    set cmd "$cmd -f [lindex $q 1]"
	    set cmd "$cmd -l [lindex $q 2]"
	}
	set commands [linsert $commands end $cmd]
    }

    return $commands
}

# Procedure to program any partitions that have file names associated
# with them. Note that these may previously have been created or
# erased. Parameter is the partition list.
proc part_prog {new} {
    global params separator
    foreach { q } $new {

	# If this partition has a load file name get the file into the
	# tftp server
	set name [lindex $q 5]
	if { $name != "" } {
	    tftp_avail $name
	    log_info "Download file $name for partition [lindex $q 0]"

	    # Try to do the tftp operation to pull the file in. If this
	    # fails keep trying
	    set tcmd "load -r -h $params(tftpip) -b [lindex $q 3] $params(tftppath)$separator$name"
	    set tftpresp {
		{ "good" "Raw file loaded (.{10})-(.{10})" }
		{ "fail" "Unable to" }
	    }

	    # Retry loop 
	    for { set retry 0 } { $retry < 10 } { incr retry } {
		send_boot $tcmd
		set res [ match_boot $tftpresp ]
		switch -exact [ lindex $res 0 ] {
		    fail {
			match_boot ""
		    }

		    good {
			set length [ format "0x%08X" [ expr [lindex $res 3] - [lindex $res 2] + 1] ]
			log_info "Download good, $length bytes"
			match_boot ""
			break
		    }

		    default {
			error "Download aborted"
		    }
		}
	    }

	    # Program the flash from the copy in RAM
	    set cmd "{fis create [lindex $q 0]}"
	    exec_boot $cmd
	    log_info "Partition [lindex $q 0] programmed"
	}
    }
}
