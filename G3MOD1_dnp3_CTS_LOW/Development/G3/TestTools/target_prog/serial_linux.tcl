# Serial port bindings for Linux. Different versions can exist for different OSes

# Procedure to get in contact with RedBoot
proc init_boot { port baud } {
    global params target inbuff linebuff
    if { $params(verbose) } {
	log_info "Connecting on $port"
    }
    if { [ catch { set target [ open $port "w+" ] } msg ] } {
	error "Can't open port $port\n$msg"
    }
    if { [ catch { fconfigure $target -mode $baud,n,8,1 -blocking 0 } msg ] } {
	error stderr "Can't configure $port\n$msg"
    }

    set inbuff ""
}

# CLI version
if { $logto == "stdout" } {
    # Procedure to perform logging. Note that if it logs input on a prompt
    # it suppresses the echo of the same input
    proc log_boot {sys host supp} {
	global params memory
	if { $params(verbose) && $memory == "" } {
	    puts stdout "\033\[30m$sys\033\[35m$host\033\[39m"
	}
	set memory $supp
    }
    set memory ""

    # Procedure to display an error message
    proc log_error {msg} {
	puts "\033\[37m\033\[41m$msg\033\[39m\033\[49m"
    }

    # Procedure to display an information message
    proc log_info {msg} {
	puts "\033\[32m$msg\033\[39m"
    }
}

# GUI version
if { $logto == "gui" } {
    # Procedure to perform logging. Note that if it logs input on a prompt
    # it suppresses the echo of the same input
    proc log_boot {sys host supp} {
	global params memory
	if { $params(verbose) && $memory == "" } {
	    put_gui $sys norm
	    put_gui "$host\n" reply
	}
	set memory $supp
    }
    set memory ""

    # Procedure to display an error message
    proc log_error {msg} {
	put_gui "$msg\n" err
    }

    # Procedure to display an information message
    proc log_info {msg} {
	put_gui "$msg\n" inf
    }
}

# Send a command to RedBoot. Only call this after read_boot has
# reported a prompt
proc send_boot { command } {
    global target params linebuff
    puts -nonewline $target "$command\r"
    flush $target
    log_boot $linebuff $command "+"   
}

# Similar to send_boot but erases a default value
proc def_boot { command } {
    global target params linebuff
    puts -nonewline $target "\033\[H\013$command\r"
    flush $target
    log_boot $linebuff " -> $command" "+"
}

# Send a CRTL/C to RedBoot
proc stop_boot {} {
    global target params logto
    puts $target "\003"
    flush $target
    if { $params(verbose) } {
	if { $logto == "stdout" } {
	    puts stdout "\033\[31m*** BREAK ***\033\[39m"
	}
	if { $logto == "gui" } {
	    put_gui "*** BREAK ***\n" brk
	}
    }
}

# Get characters from target
proc get_boot {} {
    global target inbuff
    set inbuff [ read $target 100 ]
    if { [ eof $target ] } {
	return "eof" ;
    } else {
	return ""
    }
}

# Get a list of serial ports
proc get_ports {} {
    set ports [ glob "/dev/tty\[a-zA-Z\]*" ]
    return [ lsort $ports ]
}
