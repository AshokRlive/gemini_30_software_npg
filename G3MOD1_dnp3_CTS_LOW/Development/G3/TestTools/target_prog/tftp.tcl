# This module contains the mechanics of getting a file into TFTP so
# that it can be downloaded by RedBoot. It can either make use of a
# conventional server if one is available, or load up an internal
# server.

set intserver 0

if { $op_sys == "linux" } {
    # Binding to conventional Linux tftp server
    # Procedure to make a file available
    proc tftp_avail {name} {
	global temp_path params
	file copy -force $temp_path/$name $params(tftproot)/$params(tftppath)/
    }
}

if { $op_sys == "windows" } {
    # Binding to Tftpd_32
    # Procedure to make a file available
    proc tftp_avail {name} {
	global temp_path params
	file copy -force $temp_path\\$name $params(tftproot)\\$params(tftppath)\\
    }
}
