# This is a gui wrapper around the tgtprog mechanism. It provides a
# menu structure and a system of windows to allow a target module to
# be programmed. It uses the same resource file and hence setting
# changes are common to this and to the CLI version.

# Where to look for scripts
set scriptdir [ file dirname $argv0 ]

# Read resources. Note that these come from the current (not script)
# directory
source "$scriptdir/common.tcl"

# Read overlays
set logto gui
source "$scriptdir/serial_$op_sys.tcl"
source "$scriptdir/redboot.tcl"
source "$scriptdir/partitions.tcl"
source "$scriptdir/tftp.tcl"

package require Tk
package require Ttk

# Root window
wm geometry . 1000x750

# Main menu
set menu ".menu"
menu $menu

$menu add cascade -menu $menu.file -label "File"
menu $menu.file -tearoff 0
$menu.file add command -label "Open" -command do_open
$menu.file add command -label "Program" -command do_program
$menu.file add command -label "Quit" -command exit

$menu add cascade -menu $menu.comms -label "Comms"
menu $menu.comms -tearoff 0
$menu.comms add command -label "Serial"  -command "popup .serial  \"Serial port\"          serial_set serial_in"
$menu.comms add command -label "RedBoot" -command "popup .redBoot \"RedBoot Settings\"     red_set    red_in"
$menu.comms add command -label "TFTP"    -command "popup .tftp    \"TFTP server settings\" tftp_set   tftp_in"

$menu add cascade -menu $menu.view -label "View"
menu $menu.view -tearoff 0
$menu.view add checkbutton -label "Verbose" -variable params(verbose) -command resource_write

$menu add cascade -menu $menu.debug -label "Debug"
menu $menu.debug -tearoff 0
$menu.debug add checkbutton -label "KGDB" -variable params(kgdb) -command resource_write

. configure -menu $menu

# Status box
set status .status
frame $status -borderwidth 3 -relief groove

ttk::label $status.prog -anchor nw -text "Set parameters then select 'File -> Program' to start"
pack $status.prog -side top -fill x -padx 5 -pady 5

set statlist "-----"
ttk::label $status.state -anchor nw -textvariable statlist
pack $status.state -side top -fill x -padx 5 -pady 5

# Text object takes up most of window
set main .main
frame $main
set text .main.text
set scroll .main.scroll
text $text -yscrollcommand [list $scroll set]
scrollbar $scroll -command [list $text yview]

pack $text -fill both -expand 1 -side left
pack $scroll -side left -fill y

$text tag configure norm  -foreground black        -background white
$text tag configure reply -foreground magenta      -background white
$text tag configure err   -foreground white        -background red
$text tag configure inf   -foreground "dark green" -background white
$text tag configure brk   -foreground red          -background white

pack $status -side top -fill x -padx 2 -pady 2
pack $main -fill both -expand 1 -side top

# This procedure displays a popup box. Parameters are the window name
# and title, and the names of two procedures which set up the objects
# within the box, and validate user input. It is assumed that the
# box will be two columns, the left being labels and the right being
# values. Objects should be called $path.lnn and $path.vnn so that
# they can be automatically gridded.

proc popup {name title setup valid} {
    toplevel $name
    wm title $name $title
    global popup_result
    set popup_result destroy

    # Create the contents frame
    frame $name.contents -borderwidth 0

    # Call the setup proc which will create widgets but not place
    # them.
    $setup $name.contents
    set subwin [ winfo children $name.contents ]
    set len [ string length $name.contents. ]
    set tail [ expr $len + 1 ]
    set lastrow 0
    set pad "-padx 5 -pady 5"

    # The label that becomes an error message
    label $name.error -text "Enter new values and click OK"
    eval pack $name.error -side top $pad

    # Go through all the widgets placing them in the grid
    foreach { win } $subwin {
	set type [ string range $win $len $len ]
	set row [ expr [ string range $win $tail end ] + 0 ]
	if { $row > $lastrow } {
	    set lastrow $row
	}
	set col [ expr \"$type\" == \"v\" ]
	set stick [ expr \"$type\" == \"v\" ? \"w\" : \"e\" ]
	eval grid configure $win -column $col -row $row -sticky $stick $pad
    }
    eval pack $name.contents -side top $pad

    # The buttons that dismiss the dialog
    frame $name.buttons -borderwidth 0
    set cmd1 "if {\[ $valid \]} { destroy $name ; set popup_result ok ; set_statlist } else "
    set cmd2 "{ $name.error configure -text \"Error, please check settings\" }"
    button $name.buttons.ok     -text "OK"     -command "$cmd1$cmd2"
    button $name.buttons.cancel -text "Cancel" -command \
	"destroy $name ; set popup_result cancel"
    eval pack $name.buttons.ok $name.buttons.cancel -side left $pad
    eval pack $name.buttons -side top $pad

    # Loop waiting for user to close dialog
    while {[ winfo exists $name ]} {
	update
    }

    # Tell caller what happened
    return $popup_result
}

# This procedure adds labels to a popup box
proc popup_labels { name labels } {
    # Do the same with the labels
    set row 0
    foreach { l } $labels {
	set lblname $name.l[format "%02d" $row]
	if { ! [ winfo exists $lblname ] } {
	    label $lblname -text $l
	}
	incr row
    }
}

# Setup RedBoot GUI
proc red_set { name } {
    global editing params
    array set editing [ array get params ]

    checkbutton $name.v00 -command "dhcp_switch $name \$editing(dhcp)" -variable editing(dhcp)
    entry $name.v01 -width 20 -textvariable editing(ip)
    entry $name.v02 -width 20 -textvariable editing(netmask)
    entry $name.v03 -width 20 -textvariable editing(server)
    popup_labels $name [ list "DHCP enable" "IP Address" "Netmask" "Default server" ]
    dhcp_switch $name $editing(dhcp)
}

# Deal with DHCP on/off events
proc dhcp_switch { name on } {
    foreach { n } [ list $name.v01 $name.v02 $name.v03 $name.l01 $name.l02 $name.l03 ] {
	if { $on } {
	    $n configure -state disabled
	} else {
	    $n configure -state normal
	}
    }
}

# If RedBoot settings are valid copy them back to params and return 1, if not return 0
proc red_in {} {
    global editing params
    if { $editing(dhcp) ||
	 ( [ valid_ip $editing(ip) ] &&
	   [ valid_ip $editing(netmask) ] && [ int_to_bits [ dots_to_int $editing(netmask) ] ] >= 0 &&
	   [ valid_ip $editing(server) ] ) } {
	array set params [ array get editing ]
	resource_write
	return 1
    } else {
	return 0
    }
}

# Setup serial GUI
proc serial_set { name } {
    global editing params
    array set editing [ array get params ]

    set baudrates [ list 1200 2400 4800 9600 19200 38400 115200 ]
    ttk::combobox $name.v00 -values [ get_ports ] -state readonly -textvariable editing(port)
    ttk::combobox $name.v01 -values $baudrates -state readonly -textvariable editing(baud)
    popup_labels $name [ list "Port" "Baud Rate" ]
}

# Serial update
proc serial_in {} {
    global editing params
    array set params [ array get editing ]
    resource_write
    return 1
}

# Setup TFTP GUI
proc tftp_set { name }  {
    global editing params
    array set editing [ array get params ]
    entry $name.v00 -width 20 -textvariable editing(tftproot)
    entry $name.v01 -width 20 -textvariable editing(tftppath)
    entry $name.v02 -width 20 -textvariable editing(tftpip)
    popup_labels $name [ list "root" "path" "server" ]
}

# Serial update
proc tftp_in {} {
    global editing params
    array set params [ array get editing ]
    resource_write
    return 1
}

# Procedure to open a distribution kit file
proc do_open {} {
    global params
    set f [ tk_getOpenFile -defaultextension .zip -title "Open distribution kit" ]
    if { $f != "" } {
	set params(distkit) $f
	resource_write
	set_statlist
    }
}

# Procedure to set the status list for the display
proc set_statlist {} {
    global params statlist

    set statlist "Port: $params(port) Baud: $params(baud)"
    set statlist "$statlist\nFile: $params(distkit)"
}
set_statlist

# Procedure to transfer console output to the GUI
proc put_gui { str tag } {
    global text
    $text insert end $str $tag
    $text yview moveto 1
    update
}

# Porocedure to run the programming sequence and catch errors
proc do_program {} {
    global status
    $status.prog configure -text "Starting" -background blue -foreground white
    if { [catch {programmer} err dict] } {
	log_error $err
	$status.prog configure -text "FAILED!" -background red -foreground white
    }
    $status.prog configure -text "Programming complete" -background "dark green" -foreground white
}

# Inner procedure to program a board
proc programmer {} {
    global params newconf newscript temp_path status

    # Target specifics
    set_target

    # Read the distribution kit
    set partlist [ part_getkit $params(distkit) ]

    # Tell user
    $status.prog configure -text "Please reset or power up target"

    # Open serial
    init_boot $params(port) $params(baud)

    # Get a prompt
    start_boot
    $status.prog configure -text "PROGRAMMING, PLEASE WAIT" -background yellow -foreground black

    # Get or set IP address
    set tgt_ip [ ip_boot ]

    # Program configuration
    config_boot $newconf $newscript

    # Get the existing partition table
    set p [ fis_boot ]

    # Work out what needs to change
    set changes [ part_merge $p $partlist ]
    exec_boot $changes

    # Program the partitions
    part_prog $partlist

    # Remove temporaries
    file delete -force $temp_path

    # Indicate completion
    log_info "Programming finished"
}
