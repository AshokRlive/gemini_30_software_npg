# This is the CLI version of the main program. It reads the resource
# file and programs the target.

# Where to look for scripts
set scriptdir [ file dirname $argv0 ]

# Read resources. Note that these come from the current (not script)
# directory
source "$scriptdir/common.tcl"

# Read overlays
set logto stdout
source "$scriptdir/serial_$op_sys.tcl"
source "$scriptdir/redboot.tcl"
source "$scriptdir/partitions.tcl"
source "$scriptdir/tftp.tcl"

# Handle command line
resource_argv $argv
resource_write

# Target specifics
set_target

# Read the distribution kit
set partlist [ part_getkit $params(distkit) ]

# Open serial
init_boot $params(port) $params(baud)

# Tell user
log_info "Ready, please reset target"

# Get a prompt
start_boot

# Get or set IP address
set tgt_ip [ ip_boot ]

# Program configuration
config_boot $newconf $newscript

# Get the existing partition table
set p [ fis_boot ]

# Work out what needs to change
set changes [ part_merge $p $partlist ]
exec_boot $changes

# Program the partitions
part_prog $partlist

# Remove temporaries
file delete -force $temp_path