# Common startup code. Sets default values for parameters and deals
# with .tgtprog_rc

# Procedure to set a parameter if it is unset. This allows backward
# compatibility of the resource file over script revisions.
proc setparam {name val} {
    global params
    if { [ array names params $name ] != $name } {
	set params($name) $val
    }
}

# Determine operating system
if { [ regexp "Windows" $tcl_platform(os) ] } {
    if { [ array names env LOCALAPPDATA ] != "" } {
	set temp_path "$env(LOCALAPPDATA)\\tgtprog"
	set resources "$env(LOCALAPPDATA)\\tgtprog_rc"
    } elseif { [ array names env APPDATA ] != "" } {
	set temp_path "$env(APPDATA)\\tgtprog"
	set resources "$env(APPDATA)\\tgtprog_rc"
    } else {
	puts "Can't identify OS, environment is [ array get env ]"
	exit
    }
    set op_sys windows
    set user_name $env(USERNAME)
    set separator "\\"
} elseif { [ regexp "Linux" $tcl_platform(os) ] } {
    set op_sys linux
    set user_name $env(USER)
    set temp_path "/tmp/$user_name-tgtprog"
    set resources ".tgtprog_rc"
    set separator "/"
} else {
    die "Can't identify operating system from $tcl_platform(os)"
}

# Create work directory
file mkdir $temp_path

# If there is a resource file read it
if { [file exists $resources] } {
    source $resources
}

# Set OS-dependent stuff and default parameters. Any parameters not
# set in the rc file get set here.
if { $op_sys == "windows" } {
    setparam port     "COM0"
    setparam tftproot "\\tftpboot"
    package require twapi
}

if { $op_sys == "linux" } {
    setparam port     "/dev/ttyS0"
    setparam tftproot "/tftpboot"
}

setparam tftppath $user_name
setparam verbose  1
setparam target   "Gemini 3.0"
setparam baud     "115200"
setparam dhcp     1
setparam ip       ""
setparam server   ""
setparam netmask  ""
setparam distkit  "distkit.zip"
setparam tftpip   ""
setparam kgdb     0

# Procedure to rewrite resource file
proc resource_write {} {
    global params resources

    set rcfile [ open $resources "w" ]
    puts $rcfile "\# Automatically generated resource file\n\narray set params {"
    foreach { p q } [array get params] {
	puts $rcfile "    $p [list $q]"
    }
    puts $rcfile "}"
    close $rcfile
}

# Procedure to convert argv to parameters. Command line stuff that
# isn't parameters is handled separately
proc resource_argv { arg } {
    global params
    set dump 0

    foreach { p q } $arg {
	if { [ array names params -exact $p ] == $p } {
	    set params($p) $q
	} else {
	    switch $p {
		dump { set dump $q }
		default { log_error "Unknown option $p $q" }
	    }
	}
    }
    if { $dump > 0 } {
	foreach { p } [ lsort [ array names params ] ] {
	    puts [ format "* %-20s %s" $p [ list $params($p) ] ]
	}
    }
}

# Target specifics.
proc set_target {} {
    global params newconf newscript
    if { $params(target) == "Gemini 3.0" } {

	# Configuration options
	set newconf {
	    { boot_script true }
	    { boot_script_timeout 100 }
	    { boot_script_data "" }
	    { bootp false }
	}

	# Startup script
	set gdbswitch [ expr ( $params(kgdb) != 0 ) ? \" kgdboc=ttymxc1,115200 kgdbwait\" : \"\" ]
	set consoles "console=ttymxc0,115200"
	set rootfs "ubi.mtd=1 root=ubi0:rootfs rootfstype=ubifs rw"
	set newscript "
	{fis load linux}
	{exec -w 1 -c \"noinitrd $rootfs $consoles panic=5 lpj=995328 $gdbswitch\"}
    "
    }
}
