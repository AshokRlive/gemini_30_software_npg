/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:lucy_nvram_test.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10 Feb 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "NVRAMDef.h"
#include "MCMIOMap.h"
#include "crc32.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

// Forward declaration needed here (func pointers used in structure)
void set_appram_en(void);
void clear_appram_en(void);
void printf_verbose(lu_int8_t *fmt, ...);


typedef enum
{
    MEM_DEV_ID = 0,
    MEM_DEV_APP = 1,
    MEM_DEV_APP_COPY = 2,
    MEM_DEV_LAST

} MEM_DEV;

typedef struct MemDeviceDef
{
    MEM_DEV             device;
    const lu_int8_t     friendlyName[64];
    const lu_int8_t     devicePath[64];
    lu_uint32_t         offset;
    lu_uint32_t         header_size;
    lu_uint32_t         data_size;
    void                (*open_func)(void);
    void                (*close_func)(void);

} MEMDeviceStr;

// Each NVRAM Block has a header before the data
#define NVRAM_ID_BLOCK_SIZE    sizeof(NVRAMBlkHeadStr) + sizeof(NVRAMInfoStr)

// This is the size of the header portion of the ID_BLOCK (to be calculated by this program)
#define NVRAM_ID_HEADER_SIZE  sizeof(NVRAMBlkHeadStr)

// This is the size of the data portion of the ID_BLOCK (to be supplied on the command line)
#define NVRAM_ID_DATA_SIZE    sizeof(NVRAMInfoStr)


// /sys/bus/i2c/devices/0-0050/eeprom  : Protected by the WEN Write Enable Switch
// /sys/bus/i2c/devices/0-0054/eeprom  : Protected by GPIO6 which must be set to write


const MEMDeviceStr memDevices[] = { { MEM_DEV_ID,       "ID Memory    (protected by ID WEN)", "/sys/bus/i2c/devices/0-0050/eeprom", NVRAM_ID_BLK_INFO_OFFSET,    NVRAM_ID_HEADER_SIZE, NVRAM_ID_DATA_SIZE,      NULL,          NULL            },
                                    { MEM_DEV_APP,      "App ID Memory (protected by GPIO6)", "/sys/bus/i2c/devices/0-0054/eeprom", NVRAM_APP_BLK_INFO_OFFSET,   NVRAM_ID_HEADER_SIZE, NVRAM_ID_DATA_SIZE,      set_appram_en, clear_appram_en },
                                    { MEM_DEV_APP_COPY, "App Memory    (protected by GPIO6)", "/sys/bus/i2c/devices/0-0054/eeprom", NVRAM_APP_BLK_USER_A_OFFSET, 0,                    NVRAM_APP_BLK_INFO_SIZE, set_appram_en, clear_appram_en }
                                  };

#if 0
const MEMDeviceStr memDevices[] = { { MEM_DEV_ID,       "ID Memory",               "/sys/bus/i2c/devices/0-0050/eeprom", 0x00  /* NVRAM_ID_BLK_INFO_OFFSET */,    0x2 /* NVRAM_ID_BLK_INFO_SIZE */,  NULL,          NULL  },
                                    { MEM_DEV_APP,      "Application ID Memory",   "/sys/bus/i2c/devices/0-0054/eeprom", 0x00  /* NVRAM_APP_BLK_INFO_OFFSET */,   0x2 /* NVRAM_ID_BLK_INFO_SIZE */,  set_appram_en, clear_appram_en },
                                    { MEM_DEV_APP_COPY, "Application Memory",      "/sys/bus/i2c/devices/0-0054/eeprom", 0x100 /* NVRAM_APP_BLK_USER_A_OFFSET */, 0x2 /* NVRAM_APP_BLK_INFO_SIZE */, set_appram_en, clear_appram_en }
                                  };
#endif

#define NUM_MEM_DEVICES (sizeof(memDevices) / sizeof(MEMDeviceStr))

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void printUsage(lu_uint8_t *argv[]);
lu_int32_t convertHexToChar(const lu_int8_t *hexString, lu_uint32_t hexStringLength, lu_int8_t *charBuff);
lu_int32_t insertHeader(const MEMDeviceStr *memPtr, lu_int8_t *buff);
FILE *open_memory(const MEMDeviceStr *device);
void close_memory(const MEMDeviceStr *device, FILE *fd);
lu_int32_t read_memory(FILE *fd, lu_uint32_t offset, lu_uint32_t size, lu_int8_t *buff);
lu_int32_t free_buff(lu_int8_t *buff);
lu_int32_t write_memory(FILE *fd, lu_uint32_t offset, lu_uint32_t size, lu_int8_t *buff);
lu_int32_t verify_memory(lu_int8_t *writtenBytes, lu_int8_t *readBytes, lu_uint32_t size);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
FILE          *IDNVRamFd;

// Command Line Arguments
lu_int32_t     ArgMemory  = -1;
lu_int8_t     *ArgData    = NULL;
lu_int32_t     ArgVerbose = 0;
lu_int32_t     ArgDoGPIO6 = 1;   // Set to 0 to ignore WEN when writing to NVRam (should cause failure)

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Main
 *
 *
 *   \param argc
 *          argv
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
int main(int argc, char *argv[])
{
    lu_int8_t *readBuff     = NULL;
    lu_int8_t *writeBuff    = NULL;
    FILE *memFd             = NULL;
    lu_int32_t arg;


    if (argc < 3)
    {
        printUsage((lu_uint8_t **)argv);
        return -1;
    }

    // Handle command line arguments
    while ((arg = getopt(argc, argv, "vwd:m:")) != -1)
    {
        switch (arg)
        {
            case 'm':
                ArgMemory = (int) strtol(optarg, NULL, 10);
            break;

            case 'd':
                ArgData = optarg;
            break;

            case 'v':
                ArgVerbose = 1;
            break;

            case 'w':
                ArgDoGPIO6 = 0;
            break;
        }
    }

    if ((ArgMemory < 0) || (ArgMemory > (lu_int32_t)NUM_MEM_DEVICES))
    {
        printf_verbose("\n-m must be in the range (0 - %d)\n", NUM_MEM_DEVICES);
        return -1;
    }

    if (ArgData == NULL)
    {
        printf_verbose("\n-d option is mandatory\n");
        return -1;
    }

    // Check the correct number of bytes were given
    if ((strlen((const lu_int8_t *)ArgData) / 2) != memDevices[ArgMemory].data_size)
    {
        printf_verbose("\nSupplied Hex string incorrect size.  Found [%d], require [%d]!\n", strlen((const lu_int8_t *)ArgData) / 2, memDevices[ArgMemory].data_size);
        return -1;
    }

    // Allocate writeBuff
    if ((writeBuff = calloc(memDevices[ArgMemory].header_size + memDevices[ArgMemory].data_size, sizeof(*writeBuff))) == NULL)
    {
        printf_verbose("Unable to allocate buffer\n");
        return -1;
    }

    if (convertHexToChar(ArgData, strlen((const lu_int8_t *)ArgData), writeBuff+memDevices[ArgMemory].header_size) != 0)
    {
        printf_verbose("Error converting supplied string [%s]\n", ArgData);
        return -1;
    }

    // Write the header (CRC etc)
    insertHeader(&(memDevices[ArgMemory]), writeBuff);

    if ((memFd = open_memory(&(memDevices[ArgMemory]))) == NULL)
    {
        return -1;
    }
    printf_verbose("Opened [%s]\n", memDevices[ArgMemory].devicePath);

// WRITE
    if (write_memory(memFd, memDevices[ArgMemory].offset, memDevices[ArgMemory].header_size + memDevices[ArgMemory].data_size, writeBuff) != 0)
    {
        printf_verbose("Failed to write to [%s]\n", memDevices[ArgMemory].devicePath);
        free_buff(readBuff);
        return -1;
    }
    printf_verbose("Written to [%s]\n", memDevices[ArgMemory].devicePath);

// READ
    // Allocate readBuff
    if ((readBuff = calloc(memDevices[ArgMemory].header_size + memDevices[ArgMemory].data_size, sizeof(*readBuff))) == NULL)
    {
        return -1;
    }

    if (read_memory(memFd, memDevices[ArgMemory].offset, memDevices[ArgMemory].header_size + memDevices[ArgMemory].data_size, readBuff) != 0)
    {
        printf_verbose("Failed to read from [%s]\n", memDevices[ArgMemory].devicePath);
        free_buff(readBuff);
        return -1;
    }
    printf_verbose("Read from [%s]\n", memDevices[ArgMemory].devicePath);

// VERIFY
    if (verify_memory(writeBuff, readBuff, sizeof(readBuff)) != 0)
    {
        printf_verbose("Memory verify on [%s] failed!\n", memDevices[ArgMemory].devicePath);
        return -1;
    }
    printf_verbose("Memory verified on [%s] OK\n", memDevices[ArgMemory].devicePath);

    printf_verbose("OK\n");

    // Tidy Up
    close_memory(&(memDevices[ArgMemory]), memFd);
    free_buff(readBuff);
    free_buff(writeBuff);

    return 0;
}

/*!
 ******************************************************************************
 *   \brief Print Usage Information
 *
 *
 *   \param argv
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
void printUsage(lu_uint8_t *argv[])
{
    lu_uint32_t i;

    fprintf(stderr, "\nUsage: %s -m<Memory Device> -d<Hex Data>\n", argv[0]);

    fprintf(stderr,"Options:\n-m <Memory Device> where <Memory Device> is:\n");
    for (i = 0; i < NUM_MEM_DEVICES; i++)
    {
        fprintf(stderr, "\t%d - %s\n", i, memDevices[i].friendlyName);
    }

    fprintf(stderr,"-d <Hex Data> e.g. -d 00A0BC7B.....04\n");
    fprintf(stderr,"-v Verbose Mode\n");
    fprintf(stderr,"-w Do NOT toggle GPIO 6 on write\n");
}


/*!
 ******************************************************************************
 *   \brief Print message when in verbose mode
 *
 *   \param variable
 *
 *   \return N/A
 *
 ******************************************************************************
 */
void printf_verbose(lu_int8_t *fmt, ...)
{
    if (ArgVerbose == 0)
    {
        return;
    }

    va_list ap;
    va_start(ap, fmt);
    vprintf(fmt, ap);
    va_end(ap);
}

/*!
 ******************************************************************************
 *   \brief Converts an ASCII Hex String to Binary
 *
 *   \param hexString       ASCI Hex String, e.g. "0ad488..1f"
 *          hexStringLength Length of hexString
 *          charBuff        Buffer where the binary version is stored
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
lu_int32_t convertHexToChar(const lu_int8_t *hexString, lu_uint32_t hexStringLength, lu_int8_t *charBuff)
{
    lu_int8_t   *hexPos;
    lu_int8_t   *charPos;
    lu_uint32_t  c;


    if ((hexString == NULL) || (charBuff == NULL))
    {
        return -1;
    }

    hexPos = (lu_int8_t *) hexString;
    charPos = charBuff;

    // Loop over the hex string
    while ((hexStringLength > 0) && (sscanf(hexPos, "%2x", &c) == 1))
    {
        *charPos = c;

        hexStringLength--;
        hexPos += 2;
        charPos++;
    }

    return 0;
}

/*!
 ******************************************************************************
 *   \brief Inserts the header information in to the ID NVRam Buffer
 *
 *   \param memPtr       Pointer to MEMDevice structure
 *          buff         Buffer containing data that header will be written to
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
lu_int32_t insertHeader(const MEMDeviceStr *memPtr, lu_int8_t *buff)
{
#if DEBUG
    lu_int32_t i;
#endif
    NVRAMBlkHeadStr *headerPtr;
    lu_uint32_t calcCRC32 = 0xFFFFFFFF;

    headerPtr = (NVRAMBlkHeadStr *) buff;

    headerPtr->version.updateCounter = 0;
    headerPtr->dataSize = memPtr->data_size;
    headerPtr->dataCrc32 = 0x00000000;

    // Recalculate the CRC
    crc32_calc32((lu_uint8_t *) (buff + memPtr->header_size), memPtr->data_size, &calcCRC32);

    headerPtr->dataCrc32 = calcCRC32;

#if DEBUG
    printf("Calculated CRC to be 0x%08X\n\n", calcCRC32);

    for (i=0; i<(mem.header_size + memPtr->data_size); i++)
    {
        printf("%02X ", (unsigned char) buff[i]);
    }
    printf("\n");
#endif

    return 0;
}


/*!
 ******************************************************************************
 *   \brief Opens a device for Read/Write
 *
 *   \param device      Device Structure pointer
 *
 *   \return File Handle
 *
 ******************************************************************************
 */
FILE *open_memory(const MEMDeviceStr *device)
{
    if (device == NULL)
    {
        return NULL;
    }

    if (device->open_func != NULL)
    {
        device->open_func();
    }

    if ((IDNVRamFd = fopen(device->devicePath, "r+")) == NULL)
    {
        return NULL;
    }

    return IDNVRamFd;
}

/*!
 ******************************************************************************
 *   \brief Closes a device
 *
 *   \param device      Device Structure pointer
 *          fd          File Handle
 *
 *   \return N/A
 *
 ******************************************************************************
 */
void close_memory(const MEMDeviceStr *device, FILE *fd)
{
    if ((device == NULL) || (fd == NULL))
    {
        return;
    }

    fclose(fd);

    if (device->close_func != NULL)
    {
        device->close_func();
    }
}

/*!
 ******************************************************************************
 *   \brief Read memory
 *
 *   \param fd          File Handle
 *          offset      Memory offset
 *          size        Memory size
 *          buff        Buffer pointer
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
int read_memory(FILE *fd, unsigned int offset, unsigned int size, lu_int8_t *buff)
{
    if ((fd == NULL) || (buff == NULL))
    {
        return -1;
    }

    if (fseek(fd, (long)offset, SEEK_SET) != 0)
    {
        return -1;
    }

    if (fread(buff, 1, size, fd) != size)
    {
        return -1;
    }

    return 0;
}


/*!
 ******************************************************************************
 *   \brief Free Buffer memory
 *
 *   \param buff        Buffer pointer
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
int free_buff(lu_int8_t *buff)
{
    if (buff == NULL)
    {
        return -1;
    }

    free(buff);
    return 0;
}

/*!
 ******************************************************************************
 *   \brief Write memory
 *
 *   \param fd          File Handle
 *          offset      Memory offset
 *          size        Memory size
 *          buff        Buffer pointer
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
int write_memory(FILE *fd, lu_uint32_t offset, lu_uint32_t size, lu_int8_t *buff)
{
    if ((fd == NULL) || (buff == NULL))
    {
        return -1;
    }

    if (fseek(fd, (long)offset, SEEK_SET) != 0)
    {
        return -1;
    }

    if (fwrite(buff, 1, size, fd) != size)
    {
        return -1;
    }

    return fflush(fd);
}

/*!
 ******************************************************************************
 *   \brief Verify memory contents
 *
 *   \param writtenBytes    Write Buffer
 *          readBytes       Read Buffer
 *          size            Buffer size
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
int verify_memory(lu_int8_t *writtenBytes, lu_int8_t *readBytes, lu_uint32_t size)
{
    if ((readBytes == NULL) || (writtenBytes == NULL))
    {
        return -1;
    }

    return memcmp(writtenBytes, readBytes, size);
}

/*!
 ******************************************************************************
 *   \brief Set APPRAM write enable
 *
 *   \param  N/A
 *
 *   \return N/A
 *
 ******************************************************************************
 */
void set_appram_en(void)
{
    FILE *fd;

    if (ArgDoGPIO6 == 0)
    {
        printf_verbose("Not toggling GPIO 6\n");
        return;
    }

    if (initGPIO(MCMOutputGPIO_APPRAM_WEN, GPIOLIB_DIRECTION_OUTPUT, &fd) != 0)
    {
        return;
    }

    writeGPIO(fd, LU_FALSE); // Inverted!

    closeGPIO(fd);

    printf_verbose("Set GPIO 6 (WEN)\n");
}

/*!
 ******************************************************************************
 *   \brief Clear APPRAM write enable
 *
 *   \param  N/A
 *
 *   \return N/A
 *
 ******************************************************************************
 */
void clear_appram_en(void)
{
    FILE *fd;

    if (ArgDoGPIO6 == 0)
    {
        printf_verbose("Not toggling GPIO 6\n");
        return;
    }

    if (initGPIO(MCMOutputGPIO_APPRAM_WEN, GPIOLIB_DIRECTION_OUTPUT, &fd) != 0)
    {
        return;
    }

    writeGPIO(fd, LU_TRUE); // Inverted!

    closeGPIO(fd);

    printf_verbose("Clear GPIO 6 (WEN)\n");
}


/*
 *********************** End of file ******************************************
 */
