/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:lucy_sertest.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 Jan 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <termios.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <fcntl.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lucy_ser_test.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define MAX_PORT_LEN     20
#define TEST_STRING_LEN  255
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
typedef struct portConfigDef
{
    char           name[MAX_PORT_LEN];
    int            baudrate;
    int            handle;

    unsigned char  hwFlowControl;

    struct termios settings;

    int            gpioDTR;  // DTR is controlled by a GPIO
    FILE          *gpioFP;   // File Pointer to DTR GPIO file

} portConfigStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void setPortConfig(const char *device, portConfigStr *port);
static int openPort(portConfigStr *port);
static int getCTS(portConfigStr *port, unsigned char *state);
static int setCTS(portConfigStr *port, unsigned char state);
static int setDTR(portConfigStr *port, unsigned char state);
static int setRTS(portConfigStr *port, unsigned char state);
static int testPort(portConfigStr *txPort, portConfigStr *rxPort);
static void initTestString(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
unsigned char   ArgFlowControl = FALSE;
unsigned char   ArgDTR         = FALSE;
int             ArgBaudRate    = 115200;
char            ArgPortA[MAX_PORT_LEN];
char            ArgPortB[MAX_PORT_LEN];

char            testString[TEST_STRING_LEN];
char            rxBuff[TEST_STRING_LEN];



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
int main(int argc, char *argv[])
{
    portConfigStr port1, port2;
    int arg;
    int ret;

    // set up defaults
    strncpy(ArgPortA, RS232A, MAX_PORT_LEN);
    strncpy(ArgPortB, RS232B, MAX_PORT_LEN);


    // Handle command line arguments
    while ((arg = getopt(argc, argv, "dfhs:a:b:")) != -1)
    {
        switch (arg)
        {
            case 'a':
                strncpy(ArgPortA, optarg, MAX_PORT_LEN);
//                fprintf(stderr, "Port A [%s]\n", ArgPortA);
            break;
            case 'b':
                strncpy(ArgPortB, optarg, MAX_PORT_LEN);
//                fprintf(stderr, "Port B [%s]\n", ArgPortB);
            break;
            case 'd':
                ArgDTR = TRUE;
//                fprintf(stderr, "DTR Mode\n");
            break;
            case 'f':
                ArgFlowControl = TRUE;
//                fprintf(stderr, "Flow Control Enabled\n");
            break;
            case 's':
                ArgBaudRate = strtol(optarg, NULL, 10);
            break;

            default:
                fprintf(stderr,"Options:\n-h Display this help\n-s <Baud Rate> (current %d)\n-f Enable Flow Control\n", ArgBaudRate);
                fprintf(stderr,"-d DTR Test\n-a <port a> (current %s)\n-b <port b> (current %s)\n", ArgPortA, ArgPortB);

                exit(0);
        }
    }

    setPortConfig(ArgPortA, &port1);
    setPortConfig(ArgPortB, &port2);

    if (openPort(&port1) < 0)
    {
//        fprintf(stderr, "Unable to open port [%s]\n", port1.name);
        return 1;
    }

    if (openPort(&port2) < 0)
    {
//        fprintf(stderr, "Unable to open port [%s]\n", port2.name);
        close(port1.handle);
        return 1;
    }

    // Initialise the test string
    initTestString();


    // Test the ports
    ret = 0;
    ret += testPort(&port1, &port2);
    ret += testPort(&port2, &port1);

    // Tidy Up
    close(port1.handle);
    close(port2.handle);

    if (port1.gpioFP != NULL)
    {
        fclose(port1.gpioFP);
    }

    if (port2.gpioFP != NULL)
    {
        fclose(port2.gpioFP);
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Configures the port
 *
 *   Detailed description
 *
 *   \param device Device name e.g. ttymxc0
 *          port   pointer to a portConfigStr
 *
 *   \return N/A
 *
 ******************************************************************************
 */
static void setPortConfig(const char *device, portConfigStr *port)
{
    port->handle = -1;
    strncpy(port->name, device, MAX_PORT_LEN);

    // Only two ports have a DTR, so invalidate DTR in the general case
    port->gpioDTR = -1;

    // These two ports have DTR but they are controlled through GPIO
    if (strncmp(port->name, RS232A, MAX_PORT_LEN) == 0)
    {
        port->gpioDTR = 5;
    }
    else if (strncmp(port->name, RS232B, MAX_PORT_LEN) == 0)
    {
        port->gpioDTR = 4;
    }

    port->hwFlowControl = ArgFlowControl;

    // Set the Baud Rate
    switch (ArgBaudRate)
    {
        case 1200:    port->baudrate = B1200;    break;
        case 2400:    port->baudrate = B2400;    break;
        case 9600:    port->baudrate = B9600;    break;
        case 19200:   port->baudrate = B19200;   break;
        case 38400:   port->baudrate = B38400;   break;
        case 115200:  port->baudrate = B115200;  break;
        case 230400:  port->baudrate = B230400;  break;
        case 460800:  port->baudrate = B460800;  break;
        case 576000:  port->baudrate = B576000;  break;
        case 1152000: port->baudrate = B1152000; break;
        default:      port->baudrate = B115200;
    }
}

/*!
 ******************************************************************************
 *   \brief Opens a port and applies configuration
 *
 *   Detailed description
 *
 *   \param port   pointer to a portConfigStr
 *
 *   \return port handle
 *
 ******************************************************************************
 */
static int openPort(portConfigStr *port)
{
    if (port == NULL)
    {
        return -1;
    }

    if ((port->handle = open(port->name, O_RDWR)) < 0)
    {
        return port->handle;
    }

    // Apply baudrate, mode and databits
    port->settings.c_cflag = port->baudrate | CREAD | CS8;

    // Set flow control
    if (port->hwFlowControl == TRUE)
    {
        port->settings.c_cflag |= CRTSCTS;
        port->settings.c_cflag |= HUPCL;    // Drop Control Lines after hang up
    }
    else
    {
        port->settings.c_cflag |= CLOCAL;   // Ignore MODEM control lines
    }


    // Ignore incoming parity
    port->settings.c_iflag = IGNPAR;

    // Clear all other settings
    port->settings.c_oflag = 0;
    port->settings.c_lflag = 0;

    // Apply the port settings
    tcsetattr(port->handle, TCSANOW, &(port->settings));

    // Set port to non blocking
    fcntl(port->handle, F_SETFL, O_NDELAY);

    port->gpioFP = NULL;

    return port->handle;
}

/*!
 ******************************************************************************
 *   \brief Gets the status of the CTS line
 *
 *   Detailed description
 *
 *   \param port   pointer to a portConfigStr
 *          state  True or False returned
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static int getCTS(portConfigStr *port, unsigned char *state)
{
    int status;

    if (ioctl(port->handle, TIOCMGET, &status) == -1)
    {
        return -1;
    }
    if (status & TIOCM_CTS)
    {
        *state = TRUE;
    }
    else
    {
        *state = FALSE;
    }
    return 0;
}


/*!
 ******************************************************************************
 *   \brief Sets the status of the CTS line
 *
 *   Detailed description
 *
 *   \param port   pointer to a portConfigStr
 *          state  True or False
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static int setCTS(portConfigStr *port, unsigned char state)
{
    int status;

    if (ioctl(port->handle, TIOCMGET, &status) == -1)
    {
        return -1;
    }

    if (state == TRUE)
    {
        status |= TIOCM_CTS;
    }
    else
    {
        status &= ~TIOCM_CTS;
    }

    if (ioctl(port->handle, TIOCMSET, &status) == -1)
    {
        return -1;
    }

    return 0;
}


/*!
 ******************************************************************************
 *   \brief Sets the status of the DTR line
 *
 *   Detailed description
 *
 *   \param port   pointer to a portConfigStr
 *          state  True or False
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static int setDTR(portConfigStr *port, unsigned char state)
{
// TODO - SKA - This currently does not work!!

    int  status;
    int  ret;
    char gpio_path[MAX_GPIO_PATH_LEN];

    if (port->gpioDTR < 0)
    {
//        printf("DTR not supported on port [%d]\n", port->name);
        return -1;
    }

    if (port->gpioFP == NULL)
    {
        snprintf(gpio_path, MAX_GPIO_PATH_LEN, GPIO_PATH, port->gpioDTR);
        if ((port->gpioFP = fopen(gpio_path, "r+")) == NULL)
        {
//            printf("Unable to open GPIO file [%s]\n", gpio_path);
            return -1;
        }
    }

    if ((ret = fprintf(port->gpioFP, "%i\n", (state == TRUE) ? 1 : 0)) <= 0)
    {
//        printf("Error writing to gpio [%d]\n", port->gpioDTR);
        return ret;
    }

    fflush(port->gpioFP);

#if 0
    if (ioctl(port->handle, TIOCMGET, &status) == -1)
    {
        return -1;
    }

    if (state == TRUE)
    {
        status |= TIOCM_DTR;
//        status |= TIOCM_DSR;
    }
    else
    {
        status &= ~TIOCM_DTR;
//        status &= ~TIOCM_DSR;
    }

    if (ioctl(port->handle, TIOCMSET, &status) == -1)
    {
        fprintf(stderr, "Failed to alter DTR!\n");
        return -1;
    }
#endif

    return 0;
}


/*!
 ******************************************************************************
 *   \brief Sets the status of the RTS line
 *
 *   Detailed description
 *
 *   \param port   pointer to a portConfigStr
 *          state  True or False
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static int setRTS(portConfigStr *port, unsigned char state)
{
    int status;

    if (ioctl(port->handle, TIOCMGET, &status) == -1)
    {
        return -1;
    }

    if (state == TRUE)
    {
        status |= TIOCM_RTS;
    }
    else
    {
        status &= ~TIOCM_RTS;
    }

    if (ioctl(port->handle, TIOCMSET, &status) == -1)
    {
        return -1;
    }

    return 0;
}

/*!
 ******************************************************************************
 *   \brief Test ports
 *
 *   The two supplied ports must be physically connected together with a NULL
 *   Modem cable!
 *
 *   \param txport   port that is transmitted on
 *          rxport   port that is received on
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static int testPort(portConfigStr *txPort, portConfigStr *rxPort)
{
    int             i;
    fd_set          readfds;
    struct timeval  tv = { 0, 50000 };  // timeout of read
    struct timeval  copy;
    int             ret;
    char           *readPos;
    char           *writePos;

    writePos = testString;
    readPos = rxBuff;

    // Clear the rxBuffer
    memset(rxBuff, 0, TEST_STRING_LEN);


    // Turn on DTR on the transmitting port
    if (ArgDTR == TRUE)
    {
        setDTR(txPort, TRUE);
//        setDTR(rxPort, FALSE);
    }

    for (i = 0; i < TEST_STRING_LEN; i++)
    {
        // Write a byte
        if (write(txPort->handle, writePos, 1) > 0)
        {
            writePos++;
        }

        // Prepare for read
        FD_ZERO(&readfds);
        FD_SET(rxPort->handle, &readfds);

        // Copy timeout (linux adjusts it)
        copy = tv;
        if (select(rxPort->handle+1, &readfds, NULL, NULL, &copy) > 0)
        {
            if (FD_ISSET(rxPort->handle, &readfds) > 0)
            {
                if ((ret = read(rxPort->handle, readPos, TEST_STRING_LEN)) < 0)
                {
//                    fprintf(stderr, "Error receiving test string on port [%s]\n", rxPort->name);
                    return 1;
                }
                else if (ret > 0)
                {
                    readPos = readPos + ret;
                    if ((readPos - rxBuff) >= TEST_STRING_LEN)
                    {
                        break;
                    }
                }
            }
        }
        else
        {
            return 1;
        }
    }

    // Turn off DTR
    if (ArgDTR == TRUE)
    {
        setDTR(txPort, FALSE);
//        setDTR(rxPort, FALSE);
    }

    // Compare the test string with the rx buffer - the same = SUCCESS!
    if (memcmp(testString, rxBuff, TEST_STRING_LEN) == 0)
    {
        return 0;
    }

    return 1;
}

/*!
 ******************************************************************************
 *   \brief Initialise the test string
 *
 *   \param None
 *
 *   \return N/A
 *
 ******************************************************************************
 */
static void initTestString(void)
{
    int i;

    for (i = 0; i < TEST_STRING_LEN; i++)
    {
        testString[i] = (char)i;
    }
}

/*
 *********************** End of file ******************************************
 */
