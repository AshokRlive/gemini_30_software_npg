/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Conversion of Board IOMAP to XML File
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/10/12     saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


#include "lpc17xx_pinsel.h"

#include "ModuleProtocol.h"
#include "IOManager.h"

/* Local includes */
#include "BoardIOMap.h"
#include "BoardIOMap.c"
#include "BoardIOChanMap.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
char* itoa(long int value, char* result, int base);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

int main(int argv,char *argc[])
{
	lu_uint16_t i;
	FILE* xmlFilePtr;
	lu_int8_t result[32];

	xmlFilePtr = fopen(argc[1],"w");
	if(xmlFilePtr == NULL)
	{
		printf("Error in Opening XML file...");
	}

	printf("%s", argc[1]);
	fprintf(xmlFilePtr, "<SourceFile classname=\"IOMAP\">\n");
	fprintf(xmlFilePtr, "<Array name=\"BoardIOMap\" type=\"IOMapStr\">\n");

	for(i=0;i<IO_ID_LAST;i++)
	{

	fprintf(xmlFilePtr, "<StructDef index=\"%s\">\n", itoa(BoardIOMap[i].ioID, result, 16));

	fprintf(xmlFilePtr, "<StructSet>\n");

		fprintf(xmlFilePtr, "<Item name=\"ioID\" type=\"lu_uint16_t\" value=\"%s\"/>\n", itoa(BoardIOMap[i].ioID, result, 16));
		fprintf(xmlFilePtr, "<Item name=\"ioChan\" type=\"lu_uint32_t\" value=\"%s\"/>\n", itoa(BoardIOMap[i].ioChan, result, 16));
		fprintf(xmlFilePtr, "<Item name=\"ioClass\" type=\"lu_uint16_t\" value=\"%s\"/>\n", itoa(BoardIOMap[i].ioClass, result, 16));
		fprintf(xmlFilePtr, "<Item name=\"ioDev\" type=\"lu_uint16_t\" value=\"%s\"/>\n", itoa(BoardIOMap[i].ioDev, result, 16));

		fprintf(xmlFilePtr, "<Item name=\"ioAddr\" type=\"IOAddressStr\">\n");

		fprintf(xmlFilePtr, "<StructDef>\n");
		fprintf(xmlFilePtr, "<StructSet>\n");
			fprintf(xmlFilePtr, "<Item name=\"param1\" type=\"lu_uint8_t\" value=\"%s\"/>\n", itoa(BoardIOMap[i].ioAddr.gpio.port,       result, 16));
			fprintf(xmlFilePtr, "<Item name=\"param2\" type=\"lu_uint8_t\" value=\"%s\"/>\n", itoa(BoardIOMap[i].ioAddr.gpio.pin,        result, 16));
			fprintf(xmlFilePtr, "<Item name=\"param3\" type=\"lu_uint8_t\" value=\"%s\"/>\n", itoa(BoardIOMap[i].ioAddr.gpio.periphFunc, result, 16));
			fprintf(xmlFilePtr, "<Item name=\"param4\" type=\"lu_uint32_t\" value=\"%s\"/>\n", itoa(BoardIOMap[i].ioAddr.gpio.pinMode,   result, 16));
		fprintf(xmlFilePtr, "</StructSet>\n");
		fprintf(xmlFilePtr, "</StructDef>\n");
		fprintf(xmlFilePtr, "</Item>\n");

		fprintf(xmlFilePtr, "<Item name=\"db\" type=\"IODebounceStr\">\n");
		fprintf(xmlFilePtr, "<StructDef>\n");
		fprintf(xmlFilePtr, "<StructSet>\n");
			fprintf(xmlFilePtr, "<Item name=\"high2LowMs\" type=\"lu_uint8_t\" value=\"%s\"/>\n", itoa(BoardIOMap[i].db.high2LowMs, result, 16));
			fprintf(xmlFilePtr, "<Item name=\"low2HighMs\" type=\"lu_uint8_t\" value=\"%s\"/>\n", itoa(BoardIOMap[i].db.low2HighMs, result, 16));
			fprintf(xmlFilePtr, "</StructSet>\n");
		fprintf(xmlFilePtr, "</StructDef>\n");
		fprintf(xmlFilePtr, "</Item>\n");

	fprintf(xmlFilePtr, "</StructSet>\n");

	fprintf(xmlFilePtr, "</StructDef>\n");
   }

	fprintf(xmlFilePtr, "</Array>\n");

	fprintf(xmlFilePtr, "</SourceFile>");


/******  For Debugging purpose ******/

//	printf("%s", itoa(0xffffffff, result, 16));
/*
 for(i=0; i < IO_ID_LAST; i++)
	{
		printf("%0x   ",BoardIOMap[i].ioID);
		printf("%0x  ",BoardIOMap[i].ioChan);
		printf("%0x   ",BoardIOMap[i].ioClass);
		printf("%0x   ",BoardIOMap[i].ioDev);
		printf("%0x   ",BoardIOMap[i].ioAddr.gpio.port);
		printf("%0x   ",BoardIOMap[i].ioAddr.gpio.pin);
		printf("%0x   ",BoardIOMap[i].ioAddr.gpio.periphFunc);
		printf("%0x   ",BoardIOMap[i].ioAddr.gpio.pinMode);
		printf("%0x   ",BoardIOMap[i].db.high2LowMs);
		printf("%0x\n ",BoardIOMap[i].db.low2HighMs);
	}
*/
    fclose(xmlFilePtr);
	return 0;
}

char* itoa(long int value, char* result, int base)
{
		// check that the base if valid
	if (base < 2 || base > 36) { *result = '\0'; return result; }

	char* ptr = result, *ptr1 = result, tmp_char;
	long int tmp_value;

	do
	{
		tmp_value = value;
		value /= base;
		*ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
	} while ( value );

	// Apply negative sign
	if (tmp_value < 0) *ptr++ = '-';
	*ptr-- = '\0';
	while(ptr1 < ptr)
	{
		tmp_char = *ptr;
		*ptr--= *ptr1;
		*ptr1++ = tmp_char;
	}
	return result;
}

