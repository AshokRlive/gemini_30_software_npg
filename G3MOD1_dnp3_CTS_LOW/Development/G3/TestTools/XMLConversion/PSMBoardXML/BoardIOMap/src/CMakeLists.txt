cmake_minimum_required(VERSION 2.8)

# Project name
project (PSMBoardIOMap2XML)

# Set the executable name
SET(EXE_NAME PSMBoardIOMap2XML)

SET(CMAKE_C_COMPILER /usr/bin/gcc)

# Global include
include_directories (../../../../../common/include/)

#module specific include
include_directories (../../../../../RTU/slaveBoards/commom/library/IOManager/include/)
include_directories (../../../../../RTU/slaveBoards/commom/library/CANProtocolCodec/include/)
include_directories (../../../../../RTU/slaveBoards/commom/library/CANProtocolFraming/include/)
include_directories (../../../../../RTU/slaveBoards/commom/library/systemUtils/include/)
include_directories (../../../../../RTU/slaveBoards/commom/include/)

# IOMAPConvert include
include_directories (../../../../../RTU/slaveBoards/PSMBoard/src/IOManager/include/)
include_directories (../../../../../RTU/slaveBoards/PSMBoard/src/IOManager/src/)

# LPC1700CMSIS includes
include_directories(SYSTEM "../../../../../RTU/slaveBoards/thirdParty/LPC1700CMSIS/Drivers/include/")
include_directories(SYSTEM "../../../../../RTU/slaveBoards/thirdParty/LPC1700CMSIS/Core/CM3/DeviceSupport/NXP/LPC17xx/")
include_directories(SYSTEM "../../../../../RTU/slaveBoards/thirdParty/LPC1700CMSIS/Core/CM3/CoreSupport/")

# List all the source files needed to build the executable
add_executable(${EXE_NAME} ../../../../common/library/IOMap2XML/src/BoardIOMap2XML.c)
			  