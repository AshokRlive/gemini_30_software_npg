#!/bin/sh

#!/bin/sh

# Remove Debug directory
rm -rf Debug

# Create Debug directory
mkdir Debug

# Run cmake and create Debug Makefiles
cd Debug
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../../src/

#******************* PSM Board ************************

cpp -fpreprocessed -o PSMIOID ../../../../../../RTU/slaveBoards/PSMBoard/src/IOManager/include/BoardIOMap.h
sed -i '/^$/d' PSMIOID
sed -i '/^#/d' PSMIOID
sed -i '/^extern/d' PSMIOID
sed -i 's/^[ \t]*//;s/[ \t]*$//' PSMIOID

make
./PSMBoardIOID2XML PSMIOID
rm PSMIOID 
#********************************************************

