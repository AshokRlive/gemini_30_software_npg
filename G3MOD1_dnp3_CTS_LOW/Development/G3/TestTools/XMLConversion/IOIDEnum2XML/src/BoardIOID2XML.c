/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Conversion of Board IO_ID Enum to XML file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/10/12     saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************/
#include <stdio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
lu_int8_t* EnumNameFind(lu_int8_t *singleLine);

lu_int8_t enumName[40], enumDef[40], singleChar = 0;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*!
*******************************************************************************
* EXPORTED:
*   \name
*******************************************************************************
* @{
*/

void main(int argc, char* argv[])
{

	static lu_int8_t inputFileName[40], tempString[40], lineFromFile[80];
	lu_int8_t *retValue;
	FILE *inputEnumFilePtr, *outputXMLFilePtr;
	lu_uint8_t enumNameFlag = LU_FALSE, enumOpenFlag = LU_FALSE;

	sscanf(argv[1], "%s", inputFileName);

	inputEnumFilePtr = fopen(inputFileName,"rb");
	if(inputEnumFilePtr == NULL)
	{
		printf("Error in opening file...");
	}

	strcpy(tempString, inputFileName);
	outputXMLFilePtr = fopen(strcat(tempString,"Enum.xml"),"wb");
	if(outputXMLFilePtr == NULL)
	{
		printf("Error in opening file...");
	}

	fputs("<SourceFile classname=\"", outputXMLFilePtr);
	strcpy(tempString, inputFileName);
	fputs(strcat(tempString,"Enum\">\n"), outputXMLFilePtr);
	fputs("<EnumDef name=\"", outputXMLFilePtr);

	while(singleChar != EOF)
	{
		singleChar = fgetc(inputEnumFilePtr);
		if(singleChar == '}')
		{
			fgets(lineFromFile, sizeof(lineFromFile), inputEnumFilePtr);
			fputs(EnumNameFind(&lineFromFile[0]), outputXMLFilePtr);
			fputs("\">", outputXMLFilePtr);
			//printf("%s",lineFromFile);
		}
	}

	fputs("\n", outputXMLFilePtr);
	//fputs("\n<EnumSet>\n", outputXMLFilePtr);
	rewind(inputEnumFilePtr);
	do
	{
		retValue = fgets(lineFromFile, sizeof(lineFromFile), inputEnumFilePtr);
		
		if(strstr(lineFromFile,"}"))
		{
	        //EnumNameFind(&lineFromFile[0]);
		    enumNameFlag = LU_FALSE;
		}
		
		if(enumNameFlag == LU_TRUE && enumOpenFlag == LU_TRUE)
		{
			fputs("<Item name=\"", outputXMLFilePtr);
			fputs(EnumNameFind(&lineFromFile[0]), outputXMLFilePtr);
			fputs("\"/>", outputXMLFilePtr);
			fprintf(outputXMLFilePtr,"\n");
		}

		if(strstr(lineFromFile,"typedef enum"))
		{
			enumNameFlag = LU_TRUE;
		}

		if(strstr(lineFromFile,"{"))
		{
			enumOpenFlag = LU_TRUE;
		}

	} while(retValue != NULL);

	//fputs("</EnumSet>\n", outputXMLFilePtr);
	fputs("</EnumDef>\n", outputXMLFilePtr);
	fputs("</SourceFile>", outputXMLFilePtr);
	fclose(outputXMLFilePtr);
	fclose(inputEnumFilePtr);
//	printf("%s\n", inputFileName);
//	printf("%s\n", retValue);

}

/*!
*******************************************************************************
* @}
*/


/*!
*******************************************************************************
* LOCAL:
*   \name Private Functions
*******************************************************************************
* @{
*/

lu_int8_t* EnumNameFind(lu_int8_t *singleLine)
{
	lu_int8_t i = 0,j = 0;

	memset(&enumName[0], 0 , sizeof(enumName));

	while(singleLine[i] != '\n')
	{
		if(singleLine[i] != '}' && 
		   singleLine[i] != ';' && 
		   singleLine[i] != ' ' && 
		   singleLine[i] != ',' &&  
		   singleLine[i] != '{' &&
		   singleLine[i] != '=')
		{
			enumName[j] = singleLine[i];
			j++;
		}
		i++;
	}
//	printf("\n");
	return (&enumName[0]);
}

/*
*********************** End of file *******************************************
*/
