#!/bin/sh

#!/bin/sh

# Remove Debug directory
rm -rf Debug

# Create Debug directory
mkdir Debug

# Run cmake and create Debug Makefiles
cd Debug
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../../src/

#******************* PSM Board ************************

cpp -fpreprocessed -o PSMIOID ../../../../../RTU/slaveBoards/PSMBoard/src/IOManager/include/BoardIOMap.h
sed -i '/^$/d' PSMIOID
sed -i '/^#/d' PSMIOID
sed -i '/^extern/d' PSMIOID
sed -i 's/^[ \t]*//;s/[ \t]*$//' PSMIOID

make
./BoardIOID2XML PSMIOID
cp PSMIOIDEnum.xml ../../BoardIOIDXMLFiles

#********************************************************

#******************* SCM Board **************************

cpp -fpreprocessed -o SCMIOID ../../../../../RTU/slaveBoards/SCMBoard/src/IOManager/include/BoardIOMap.h
sed -i '/^$/d' SCMIOID
sed -i '/^#/d' SCMIOID
sed -i '/^extern/d' SCMIOID
sed -i 's/^[ \t]*//;s/[ \t]*$//' SCMIOID

make
./BoardIOID2XML SCMIOID
cp SCMIOIDEnum.xml ../../BoardIOIDXMLFiles

#********************************************************

#******************* FDM Board **************************

cpp -fpreprocessed -o FDMIOID ../../../../../RTU/slaveBoards/FDMBoard/src/IOManager/include/BoardIOMap.h
sed -i '/^$/d' FDMIOID
sed -i '/^#/d' FDMIOID
sed -i '/^extern/d' FDMIOID
sed -i 's/^[ \t]*//;s/[ \t]*$//' FDMIOID

make
./BoardIOID2XML FDMIOID
cp FDMIOIDEnum.xml ../../BoardIOIDXMLFiles

#********************************************************

#******************* IOM Board **************************

cpp -fpreprocessed -o IOMIOID ../../../../../RTU/slaveBoards/IOMBoard/src/IOManager/include/BoardIOMap.h
sed -i '/^$/d' IOMIOID
sed -i '/^#/d' IOMIOID
sed -i '/^extern/d' IOMIOID
sed -i 's/^[ \t]*//;s/[ \t]*$//' IOMIOID

make
./BoardIOID2XML IOMIOID
cp IOMIOIDEnum.xml ../../BoardIOIDXMLFiles

#********************************************************

#******************* HMI Board **************************

cpp -fpreprocessed -o HMIIOID ../../../../../RTU/slaveBoards/HMIBoard/src/IOManager/include/BoardIOMap.h
sed -i '/^$/d' HMIIOID
sed -i '/^#/d' HMIIOID
sed -i '/^extern/d' HMIIOID
sed -i 's/^[ \t]*//;s/[ \t]*$//' HMIIOID

make
./BoardIOID2XML HMIIOID
cp HMIIOIDEnum.xml ../../BoardIOIDXMLFiles

#********************************************************

#************************* End of File ***************************************************** 
