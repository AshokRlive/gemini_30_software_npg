// ? #include "stdafx.h"
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>


int main(int argc, char* argv[])
{
	// Get the top level suite from the registry
	  CPPUNIT_NS::Test *suite = CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest();

	  // Adds the test to the list of test to run
	  CPPUNIT_NS::TextUi::TestRunner runner;
	  runner.addTest( suite );

	  // Change the default outputter to a compiler error format outputter
	  runner.setOutputter( new CPPUNIT_NS::XmlOutputter( &runner.result(),
	                                                          CPPUNIT_NS::stdCErr() ) );

  // Run the tests.
  bool wasSucessful = runner.run();

  // Return error code 1 if the one of test failed.
  //return wasSucessful ? 0 : 1;
}
