#include <embUnit/embUnit.h>

//#include <stdio.h>

#include "MoneyTest.h"
#include "Money.h"


static void moneyTest_setUp(void)
{

}

static void moneyTest_tearDown(void)
{

}

static void moneyTest_getAmount(void)
{
	TEST_ASSERT_EQUAL_INT(0, Money_getAmount());
	//TEST_ASSERT_EQUAL_INT(1, Money_getAmount());
}


TestRef moneyTest_tests(void)
{
	EMB_UNIT_TESTFIXTURES(fixtures) {
		new_TestFixture("moneyTest_getAmount",moneyTest_getAmount),
		new_TestFixture("moneyTest_getAmount2",moneyTest_getAmount),

	};
	EMB_UNIT_TESTCALLER(CounterTest,"MoneyTest",moneyTest_setUp,moneyTest_tearDown,fixtures);

	return (TestRef)&CounterTest;
}
