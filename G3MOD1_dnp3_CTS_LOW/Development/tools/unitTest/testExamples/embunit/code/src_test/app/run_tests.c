


#include <embUnit/embUnit.h>
//#include <embUnit/XMLOutputter.h>
//#include <embUnit/CompilerOutputter.h>
//#include <embUnit/TextUIRunner.h>


#include "MoneyTest.h"

int main(int argc, char* argv[])
{
#if 1

	//extUIRunner_setOutputter(XMLOutputter_outputter());

	TextUIRunner_start();

	TextUIRunner_runTest(moneyTest_tests());

        //TextUIRunner_runTest(peopleTest_tests());

	TextUIRunner_end();
#else
	TestRunner_start();

	TestRunner_runTest(moneyTest_tests());

    //TextUIRunner_runTest(peopleTest_tests());

	TestRunner_end();
#endif

	return (int)0;

  // Return error code 1 if the one of test failed.
  //return wasSucessful ? 0 : 1;
}
