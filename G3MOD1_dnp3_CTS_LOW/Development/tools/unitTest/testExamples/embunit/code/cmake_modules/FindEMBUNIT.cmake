#
# Find the EmbUnit includes and library
#
# This module defines
# EMBUNIT_INCLUDE_DIR, where to find tiff.h, etc.
# EMBUNIT_LIBRARIES, the libraries to link against to use EmbUnit.
# EMBUNIT_FOUND, If false, do not try to use EmbUnit.

# also defined, but not for general use are
# EMBUNIT_LIBRARY, where to find the EmbUnit library.
# EMBUNIT_DEBUG_LIBRARY, where to find the EmbUnit library in debug
# mode.

SET(EMBUNIT_FOUND "NO")

FIND_PATH(EMBUNIT_INCLUDE_DIR embUnit/TestCase.h /usr/local/include /usr/include)

# With Win32, important to have both
IF(WIN32)
  FIND_LIBRARY(EMBUNIT_LIBRARY embunit
               ${EMBUNIT_INCLUDE_DIR}/../lib
               /usr/local/lib
               /usr/lib)
  FIND_LIBRARY(EMBUNIT_DEBUG_LIBRARY embunitd
               ${EMBUNIT_INCLUDE_DIR}/../lib
               /usr/local/lib
               /usr/lib)
ELSE(WIN32)
  # On unix system, debug and release have the same name
  FIND_LIBRARY(EMBUNIT_LIBRARY embunit
               ${EMBUNIT_INCLUDE_DIR}/../lib
               /usr/local/lib
               /usr/lib)
  FIND_LIBRARY(EMBUNIT_DEBUG_LIBRARY embunit
               ${EMBUNIT_INCLUDE_DIR}/../lib
               /usr/local/lib
               /usr/lib)
ENDIF(WIN32)

#Message ("msg: ${EMBUNIT_INCLUDE_DIR}")

IF(EMBUNIT_INCLUDE_DIR)
  IF(EMBUNIT_LIBRARY)
    SET(EMBUNIT_FOUND "YES")
    SET(EMBUNIT_LIBRARIES ${EMBUNIT_LIBRARY} ${CMAKE_DL_LIBS})
    SET(EMBUNIT_DEBUG_LIBRARIES ${EMBUNIT_DEBUG_LIBRARY} ${CMAKE_DL_LIBS})
  ELSE (EMBUNIT_LIBRARY)
    IF (EMBUNIT_FIND_REQUIRED)
      MESSAGE(SEND_ERROR "Could not find library EmbUnit.1")
    ENDIF (EMBUNIT_FIND_REQUIRED)
  ENDIF(EMBUNIT_LIBRARY)
ELSE(EMBUNIT_INCLUDE_DIR)
  IF (EMBUNIT_FIND_REQUIRED)
    MESSAGE(SEND_ERROR "Could not find library EmbUnit.2")
  ENDIF(EMBUNIT_FIND_REQUIRED)
ENDIF(EMBUNIT_INCLUDE_DIR)
