/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       FSM test main module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      fryers_j_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

//#include "lu_types.h"
#include "Charger.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


struct Charger chargerFsm;


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

int main ()
{
	ChargerInit(&chargerFsm);

	setDebugFlag(&chargerFsm._fsm, 1);

	ChargerContext_BatteryConnect(&chargerFsm._fsm);

	ChargerContext_BatteryConnect(&chargerFsm._fsm);

	// Test transition to ChargingNormal
	batteryVoltage = 20;
	ChargerContext_ChargingRequested(&chargerFsm._fsm);

	ChargerContext_ChargingComplete(&chargerFsm._fsm);

	ChargerContext_LVFail(&chargerFsm._fsm);

	ChargerContext_LVPresent(&chargerFsm._fsm);

	ChargerContext_BatteryDisconnect(&chargerFsm._fsm);

	ChargerContext_BatteryConnect(&chargerFsm._fsm);
	
	ChargerContext_ChargingRequested(&chargerFsm._fsm);

	ChargerContext_BatteryOverTemperature(&chargerFsm._fsm);

	ChargerContext_ChargingOveride(&chargerFsm._fsm);

	ChargerContext_BatteryUnderTemperature(&chargerFsm._fsm);

	ChargerContext_LVFail(&chargerFsm._fsm);

	ChargerContext_LVFail(&chargerFsm._fsm);

	ChargerContext_LVPresent(&chargerFsm._fsm);

	return 0;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
