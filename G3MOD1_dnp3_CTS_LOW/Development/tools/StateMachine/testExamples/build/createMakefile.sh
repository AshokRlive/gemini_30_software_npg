#!/bin/sh

# Remove Debug directory
rm -rf Debug

# Create Debug directory
mkdir Debug

# Run cmake and create Debug Makefiles
cd Debug
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../../code
cd ..

# Remove Release directory
rm -rf Release 

# Create Debug directory
mkdir Release

# Run cmake and create Debug Makefiles
cd Release
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ../../code
cd ..

