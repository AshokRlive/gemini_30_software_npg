<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <title>SMC Manual: Section 9</title>
    <meta name="Author" content="Charles W. Rapp"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link rel="stylesheet" type="text/css" media="print" href="print.css"/>
    <base target="SmcManPageFrame"/>
  </head>
  <body>
    <div class="heading">
      <!-- Put the page's title here. -->
      Be Persistant!
    </div>
    <div class="main-body">
      <p>
        <!-- Put page body here. -->
        This section describes how to persist an SMC-generated
        finite state machine and restore it at a later date. In
        the following examples I persist the FSM to a flat file
        but they can be modified to work with other storage
        types. The focus is only capturing the FSM's current
        state and state stack. There is no other data to
        persist in the SMC-generated code.
      </p>
      <p>
        The examples use the class <code>AppClass</code> which
        has an associated finite state machine stored in its
        data member <code>_fsm</code>.
      </p>
      <p>
        <b>
          Note: the following sample code requires the .sm
          file be compiled with the <code>-serial</code> option.
        </b>
      </p>
      <hr/>
      <p>
        <a name="C++">
          <img src="./Images/CwrCpp.png" alt="C++ Transition Queue" border="0"/>
        </a>
      </p>
      <hr/>
      <p>
        If you are <i>not</i> using push/pop transitions, then
        the use the following code to persist the current
        state:
      </p>
      <div class="code-block">
<div class="datatype">int</div> AppClass::serialize(<div class="datatype">const char *</div>filename) <div class="keyword">const</div>
{
    <div class="datatype">int</div> fd;
    <div class="datatype">int</div> stateId(_fsm.getState().getId());
    <div class="datatype">int</div> retcode(-1);

    fd = <div class="datatype">open</div>(filename,
              (O_WRONLY | O_CREAT | O_TRUNC),
              (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH));
    if (fd &gt;= 0)
    {
        retcode = <div class="datatype">write</div>(fd, &amp;stateId, <div class="keyword">sizeof</div>(<div class="datatype">int</div>));

        (<div class="datatype">void</div>) <div class="datatype">close</div>(fd);
        fd = -1;
    }

    <div class="keyword">return</div> (retcode);
}
      </div>
      <p>
        Deserializing is the mirror:
      </p>
      <div class="code-block">
<div class="datatype">int</div> AppClass::deserialize(<div class="datatype">const char *</div>filename) <div class="keyword">const</div>
{
    <div class="datatype">int</div> fd;
    <div class="datatype">int</div> stateId;
    <div class="datatype">int</div> retcode(-1);

<div class="comment">    // This code assumes _fsm is already instantiated.</div>

    fd = <div class="datatype">open</div>(filename, O_RDONLY);
    if (fd &gt;= 0)
    {
        retcode = <div class="datatype">read</div>(fd, &amp;stateId, <div class="keyword">sizeof</div>(<div class="datatype">int</div>));
        if (retcode &gt;= 0)
        {
            _fsm.setState(_fsm.valueOf(stateId));
        }

        (<div class="datatype">void</div>) <div class="datatype">close</div>(fd);
        fd = -1;
    }

    <div class="keyword">return</div> (retcode);
}
      </div>
      <p>
        If you <i>are</i> using push/pop transitions, then the
        serialization will be require your to persist the state
        stack in reverse order (bottom to top) followed by the
        current state.
      </p>
      <p>
        <b>Warning!</b> Reading in the state stack results
        in emptying the stack and corrupting the FSM. This
        should not be a problem because you are persisting the
        FSM for use later when you will restore the state
        stack.
      </p>
      <div class="code-block">
<div class="datatype">int</div> AppClass::serialize(<div class="datatype">const char *</div>filename) <div class="keyword">const</div>
{
    <div class="datatype">int</div> fd;
    <div class="datatype">int</div> retcode(-1);

    fd = <div class="datatype">open</div>(filename,
              (O_WRONLY | O_CREAT | O_TRUNC),
              (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH));
    if (fd &gt;= 0)
    {
        <div class="datatype">int</div> size(_fsm.getStateStackDepth() + 1);
        <div class="datatype">int</div> bufferSize(size * <div class="keyword">sizeof</div>(<div class="datatype">int</div>));
        <div class="datatype">int</div> buffer[size + 1];
        <div class="datatype">int</div> i;

<div class="comment">        // Copy the states into the buffer in reverse order:
        // from the state stack bottom to the top and the current
        // state last. The first element is the number of states.</div>
        buffer[0] = size;
        buffer[size] = (_fsm.getState()).getId();
        for (i = (size - 1); i &gt; 0; --i)
        {
            _fsm.popState();
            buffer[i] = (_fsm.getState()).getId();
        }

        retcode = <div class="datatype">write</div>(fd, buffer, (bufferSize + <div class="keyword">sizeof</div>(<div class="datatype">int</div>)));

        (<div class="datatype">void</div>) <div class="datatype">close</div>(fd);
        fd = -1;
    }

    <div class="keyword">return</div> (retcode);
}
        </div>
        <p>
          When reading in the persisted FSM, first read the
          state count and then read in the states:
        </p>
        <div class="code-block">
<div class="datatype">int</div> AppClass::deserialize(<div class="keyword">const</div> <div class="datatype">char *</div>filename)
{
    <div class="datatype">int</div> fd;
    <div class="datatype">int</div> size;
    <div class="datatype">int</div> retcode(-1);

<div class="comment">    // The FSM's current state is probably set to the start
    // state. Clear it out because it is not correct.</div>
    _fsm.clearState();

<div class="comment">    // Open the file for reading and then read in the number
    // of persisted states.</div>
    fd = <div class="datatype">open</div>(filename, O_RDONLY);
    if (fd &gt;= 0 &amp;&amp; <div class="datatype">read</div>(fd, &amp;size, <div class="keyword">sizeof</div>(<div class="datatype">int</div>)) &gt;= 0)
    {
        <div class="datatype">int</div> bufferSize(size * <div class="keyword">sizeof</div>(<div class="datatype">int</div>));
        <div class="datatype">int</div> buffer[size];

        if (<div class="datatype">read</div>(fd, buffer, bufferSize) &gt;= 0)
        {
            <div class="datatype">int</div> i;

<div class="comment">            // Note: Do not call setState for the final current
            // state because pushState actually sets the current
            // state while pushing the next state on the stack.</div>
            for (i = 0; i &lt; size; ++i)
            {
                _fsm.pushState(_fsm.valueOf(buffer[i]));
            }
        }
    }

<div class="comment">    // Make sure the file is closed before leaving.</div>
    if (fd &gt;= 0)
    {
        (<div class="datatype">void</div>) <div class="datatype">close</div>(fd);
        fd = -1;
    }

    <div class="keyword">return</div> (retcode);
}
      </div>
      <hr/>
      <p>
        <a name="Java">
          <img height="88" width="52" src="./Images/javalogo52x88.png" alt="Java Transition Queue" border="0"/>
        </a>
      </p>
      <hr/>
      <p>
        SMC makes full use of Java's object serialization.
        Assuming that AppClass declares
        <code>implements java.io.Serializable</code> and that
        the <code>_fsm</code> member data is not marked as
        <code>transient</code>, then by serializing the
        AppClass instance will also serialize the FSM.
      </p>
      <p>
        SMC considers the code it generates to be subservient to
        the application code. For this reason the SMC code does
        not serialize its references to the FSM context owner
        or property listeners. The application code after
        deserializing the FSM <i>must</i> call the
        <code>setOwner</code> method to re-establish the
        application/FSM link. If the application listens for
        FSM state transitions <code>addStateChangeListener</code>
        must also be called to put the listeners in place.
      </p>
      <div class="code-block">
<div class="keyword">public final class</div> AppClass
    <div class="keyword">implements</div> <div class="datatype">java.io.Serializable</div>
{
    <div class="keyword">private</div> <div class="datatype">AppClassContext</div> _fsm;

    <div class="keyword">public</div> AppClass()
    {
        _fsm = <div class="keyword">new</div> <div class="datatype">AppClassContext</div>();
    }

<div class="comment">    // Restore the FSM context's reference to this object.
    // This is necessary because AppClass and AppClassContext
    // reference each other. When AppClass is serialized, its
    // AppClassContext instance is serialized. But when
    // AppClassContext is serialized, its AppClass instance is
    // not serialized which breaks the circual reference. When
    // AppClassContext is deserialized, its AppClass reference
    // is null. Hence the need to implement readObject
    // and reset AppClassContext's AppClass reference.
    // The state change property listeners list is empty for
    // the same reason.</div>
    <div class="keyword">private</div> <div class="datatype">void</div> readObject(<div class="datatype">java.io.ObjectInputStream</div> istream)
        <div class="keyword">throws</div> <div class="datatype">java.io.IOException</div>,
               <div class="datatype">ClassNotFoundException</div>
    {
<div class="comment">        // Do the default read first which sets _fsm to null.</div>
        istream.defaultReadObject();

<div class="comment">        // Now set the FSM's owner.</div>
        _fsm.setOwner(<div class="keyword">this</div>);

<div class="comment">        // State change listeners must also be added back.</div>
        _fsm.addStateChangeListener(_stateListener);

        <div class="keyword">return</div>;
    }

    <div class="keyword">public static</div> <div class="datatype">void</div> main(<div class="datatype">String[]</div> args)
    {
        Serialize();
        Deserialize();
    }

    <div class="keyword">public static</div> <div class="datatype">void</div> Serialize()
    {
        <div class="datatype">AppClass</div> appInstance = <div class="keyword">new</div> <div class="datatype">AppClass</div>();
        <div class="datatype">ObjectOutputStream</div> ostream =
            <div class="keyword">new</div> <div class="datatype">ObjectOutputStream</div>(
                <div class="keyword">new</div> <div class="datatype">FileOutputStream</div>(<div class="string">"./fsm_serial.bin"</div>));

        <div class="keyword">try</div>
        {
            ostream.writeObject(appInstance);
        }
        <div class="keyword">catch</div> (<div class="datatype">java.io.IOException</div> ioex)
        {
<div class="comment">            // Handle serialization exception.</div>
        }
        <div class="keyword">finally</div>
        {        
            ostream.close();
        }
    }

    ...
}
      </div>
      <p>
        Recreating the persisted AppClass instance is equally
        simple:
      </p>
      <div class="code-block">
    <div class="keyword">public static</div> <div class="datatype">void</div> Deserialize()
    {
        <div class="datatype">AppClass</div> appInstance = <div class="keyword">null</div>;
        <div class="datatype">ObjectInputStream</div> istream =
            <div class="keyword">new</div> <div class="datatype">ObjectInputStream</div>(
                <div class="keyword">new</div> <div class="datatype">FileInputStream</div>(<div class="string">"./fsm_serial.bin"</div>));

        <div class="keyword">try</div>
        {
            appInstance = (<div class="datatype">AppClass</div>) istream.readObject();
        }
        <div class="keyword">catch</div> (<div class="datatype">java.io.IOException</div> ioex)
        {
<div class="comment">            // Handle deserialization exception.</div>
        }
        <div class="keyword">finally</div>
        {
            istream.close();
        }
    }
      </div>
      <hr/>
<p>
        <a name="Tcl">
          <img height="100" width="75" src="./Images/Itclins.png" alt="Tcl Transition Queues" border="0"/>
</a>
      </p>
      <hr/>
<p>
        <b>Note:</b> the following sample code requires the .sm
        file is compiled with the <code>-serial</code> option.
      </p>
      <p>
        Tcl persistance is similar to C++.
        If you are <i>not</i> using push/pop transitions, then
        the use the following code to persist the current
        state:
      </p>
      <div class="code-block">
<div class="keyword">public method</div> serialize {fileName} {
    <div class="keyword">if</div> [<div class="datatype">catch</div> {<div class="datatype">open</div> $fileName w 0644} fileId] {
        <div class="keyword">set</div> retcode <div class="datatype">error</div>;
        <div class="keyword">set</div> retval <div class="string">"Failed to open ${filename} for writing"</div>;
    } <div class="keyword">else</div> {
        <div class="datatype">puts</div> $fileId [[$_fsm getState] getId];
        <div class="datatype">close</div> $fileId;

        <div class="keyword">set</div> retcode <div class="datatype">ok</div>;
        <div class="keyword">set</div> retval <div class="string">""</div>;
    }

    <div class="keyword">return</div> -code ${retcode} ${retval};
}
      </div>
      <p>
        The following deserializes the current state:
      </p>
      <div class="code-block">
<div class="keyword">public method</div> deserialize {fileName} {
    <div class="keyword">if</div> [<div class="datatype">catch</div> {<div class="datatype">open</div> $fileName r} fileId] {
        <div class="keyword">set</div> retcode <div class="datatype">error</div>;
        <div class="keyword">set</div> retval <div class="string">"Failed to open ${filename} for reading"</div>;
    } <div class="keyword">else</div> {
        <div class="datatype">gets</div> $fileId stateId;
        $_fsm setState [$_fsm valueOf $stateId];

        <div class="datatype">close</div> $fileId;

        <div class="keyword">set</div> retcode <div class="datatype">ok</div>;
        <div class="keyword">set</div> retval <div class="string">""</div>;
    }

    <div class="keyword">return</div> -code ${retcode} ${retval};
}
      </div>
      <p>
        If you <i>are</i> using push/pop transitions, then the
        serialization will be require your to persist the state
        stack in reverse order (bottom to top) followed by the
        current state.
      </p>
      <p>
        <b>Warning!</b> Reading in the state stack results
        in emptying the stack and corrupting the FSM. This
        should not be a problem because you are persisting the
        FSM for use later when you will restore the state
        stack.
      </p>
      <div class="code-block">
<div class="keyword">public method</div> serialize {fileName} {
    <div class="keyword">if</div> [<div class="datatype">catch</div> {<div class="datatype">open</div> $fileName w 0644} fileId] {
        <div class="keyword">set</div> retcode <div class="datatype">error</div>;
        <div class="keyword">set</div> retval <div class="string">"${fileName} open failed"</div>;
    } <div class="keyword">else</div> {
        <div class="keyword">set</div> state [$_fsm getState];
        <div class="keyword">set</div> states {};

        <div class="datatype">lappend</div> states [$state getId];
        <div class="keyword">while</div> {[<div class="datatype">catch</div> {$_fsm popState} retcode] == 0} {
            <div class="keyword">set</div> state [$_fsm getState];
            <div class="keyword">set</div> states [<div class="datatype">linsert</div> $states 0 [$state getId]];
        }

        <div class="keyword">set</div> size [<div class="datatype">llength</div> $states];
        <div class="datatype">puts</div> $fileId $size;

        <div class="keyword">foreach</div> stateId $states {
            <div class="datatype">puts</div> $fileId $stateId;
        }

        <div class="datatype">close</div> $fileId;

        <div class="keyword">set</div> retcode ok;
        <div class="keyword">set</div> retval <div class="string">""</div>;
    }

    <div class="keyword">return</div> -code ${retcode} ${retval};
}
      </div>
      <p>
        When reading in the persisted FSM, first read the
        state count and then read in the states:
      </p>
      <div class="code-block">
<div class="keyword">public method</div> deserialize {fileName} {
    <div class="keyword">if</div> [<div class="datatype">catch</div> {<div class="datatype">open</div> $fileName r} fileId] {
        <div class="keyword">set</div> retcode <div class="datatype">error</div>;
        <div class="keyword">set</div> retval <div class="string">"${fileName} open failed"</div>;
    } <div class="keyword">else</div> {
        <div class="comment"># Clear out the default start state.</div>
        $_fsm clearState;

        <div class="datatype">gets</div> $fileId size;
        <div class="keyword">for</div> {<div class="keyword">set</div> i 0} {$i &lt; $size} {<div class="keyword">incr</div> i} {
            <div class="datatype">gets</div> $fileId stateId;
            <div class="keyword">set</div> state [$_fsm valueOf $stateId];

            $_fsm pushState $state;
        }

        <div class="datatype">close</div> $fileId;

        <div class="keyword">set</div> retcode <div class="datatype">ok</div>;
        <div class="keyword">set</div> retval <div class="string">""</div>;
    }

    <div class="keyword">return</div> -code ${retcode} ${retval};
}
      </div>
      <hr/>
      <p>
        <a name="VB.net">
          <font face="arial" size="+4">
            VB.net
          </font>
        </a>
      </p>
      <hr/>
      <p>
        SMC makes full use of .net's object serialization.
        Assuming that AppClass has the
        <code>&lt;Serializable()&gt;</code> attribute and that
        the <code>_fsm</code> member data is not marked as
        <code>&lt;NonSerializable()&gt;</code>, then
        serializing the AppClass instance will also serialize
        the FSM:
      </p>
      <p>
        SMC considers the code it generates to be subservient to
        the application code. For this reason the SMC code does
        not serialize its references to the FSM context owner
        or property listeners. The application code after
        deserializing the FSM <i>must</i> call the
        <code>Owner</code> property setter to re-establish the
        application/FSM link. If the application listens for
        FSM state transitions, then event handlers must also
        be put back in place.
      </p>
      <div class="code-block">
<div class="keyword">Imports</div> <div class="datatype">System</div>
<div class="keyword">Imports</div> <div class="datatype">System.IO</div>
<div class="keyword">Imports</div> <div class="datatype">System.Runtime.Serialization</div>
<div class="keyword">Imports</div> <div class="datatype">System.Runtime.Serialization.Formatters.Binary</div>

&lt;Serializable()&gt; <div class="keyword">Public Class</div> AppClass
    <div class="keyword">Implements</div> <div class="datatype">IDeserializationCallback</div>

    <div class="keyword">Private</div> _fsm <div class="keyword">As</div> AppClassContext

    <div class="keyword">Public Sub New</div>()

        _fsm = <div class="keyword">New</div> AppClassContext(<div class="keyword">Me</div>)
    <div class="keyword">End Sub</div>

<div class="comment">    ' Restore the FSM context's reference to this object.
    ' This is necessary because AppClass and AppClassContext
    ' reference each other. When AppClass is serialized, its
    ' AppClassContext instance is serialized. But when
    ' AppClassContext is serialized, its AppClass instance is
    ' not serialized which breaks the circual reference. When
    ' AppClassContext is deserialized, its AppClass reference
    ' is Nothing. Hence the need to implement IDeserialization
    ' and reset AppClassContext's AppClass reference.
    ' Event handlers must also be put back in place for the same
    ' reason.</div>
    <div class="keyword">Private Sub</div> OnDeserialization(<div class="keyword">ByVal</div> send <div class="keyword">As</div> <div class="datatype">Object</div>) _
        <div class="keyword">Implements</div> <div class="datatype">IDeserializationCallback.OnDeserialization</div>

        _fsm.Owner = <div class="keyword">Me</div>
        AddHandler _fsm.StateChange, handler
    <div class="keyword">End Sub</div>

    <div class="keyword">Shared Sub</div> Main()

        Serialize()
        Deserialize()
    <div class="keyword">End Sub</div>

    <div class="keyword">Shared Sub</div> Serialize()

        <div class="keyword">Dim</div> appInstance <div class="keyword">As New</div> AppClass()
        <div class="keyword">Dim</div> stream <div class="keyword">As</div> <div class="datatype">Stream</div> = _
            <div class="datatype">File</div>.Open(<div class="string">"fsm_serial.bin"</div>, <div class="datatype">FileMode.Create</div>)
        <div class="keyword">Dim</div> formatter <div class="keyword">As New</div> <div class="datatype">BinaryFormatter</div>()

        <div class="keyword">Try</div>

            formatter.Serialize(stream, appInstance)
        <div class="keyword">Catch</div> serialex <div class="keyword">As</div> <div class="datatype">SerializationException</div>

<div class="comment">            ' Handle serialization failure.</div>
        <div class="keyword">Finally</div>

            stream.Close()
        <div class="keyword">End Try</div>
    <div class="keyword">End Sub</div>

    ...

<div class="keyword">End Class</div>
      </div>
      <p>
        Recreating the persisted AppClass instance is equally
        simple:
      </p>
      <div class="code-block">
    <div class="keyword">Shared Sub</div> Deserialize()

        <div class="keyword">Dim</div> appInstance <div class="keyword">As</div> AppClass = <div class="keyword">Nothing</div>
        <div class="keyword">Dim</div> stream <div class="keyword">As</div> <div class="datatype">Stream</div> = _
            <div class="datatype">File</div>.Open(<div class="string">"fsm_serial.bin"</div>, <div class="datatype">FileMode.Open</div>)
        <div class="keyword">Dim</div> formatter <div class="keyword">As New</div> BinaryFormatter()

        <div class="keyword">Try</div>

            appInstance = _
                <div class="keyword">CType</div>(formatter.Deserialize(stream), AppClass)
        <div class="keyword">Catch</div> serialex <div class="keyword">As</div> <div class="datatype">SerializationException</div>

<div class="comment">            ' Handle deserialization failure.</div>
        <div class="keyword">Finally</div>

            stream.Close()
        <div class="keyword">End Try</div>
    <div class="keyword">End Sub</div>
      </div>
      <hr/>
      <p>
        <a name="CSharp">
          <font face="arial" size="+4">
            C#
          </font>
        </a>
      </p>
      <hr/>
      <p>
        SMC makes full use of .net's object serialization.
        Assuming that AppClass has the
        <code>[Serializable]</code> attribute and that
        the <code>_fsm</code> member data is not marked as
        <code>[NonSerialized]</code>, then
        serializing the AppClass instance will also serialize
        the FSM:
      </p>
      <p>
        SMC considers the code it generates to be subservient to
        the application code. For this reason the SMC code does
        not serialize its references to the FSM context owner
        or property listeners. The application code after
        deserializing the FSM <i>must</i> call the
        <code>Owner</code> property setter to re-establish the
        application/FSM link. If the application listens for
        FSM state transitions, then event handlers must also
        be put back in place.
      </p>
      <div class="code-block">
<div class="keyword">using</div> <div class="datatype">System</div>;
<div class="keyword">using</div> <div class="datatype">System.IO</div>;
<div class="keyword">using</div> <div class="datatype">System.Runtime.Serialization</div>;
<div class="keyword">using</div> <div class="datatype">System.Runtime.Serialization.Formatters.Binary</div>;

<div class="keyword">[Serializable]</div>
<div class="keyword">public class</div> AppClass :
    <div class="datatype">IDeserializationCallback</div>
{
    <div class="keyword">private</div> AppClassContext _fsm;

    <div class="keyword">public</div> AppClass()
    {
        _fsm = <div class="keyword">new</div> AppClassContext(<div class="keyword">this</div>);
    }

<div class="comment">    // Restore the FSM context's reference to this object.
    // This is necessary because AppClass and AppClassContext
    // reference each other. When AppClass is serialized, its
    // AppClassContext instance is serialized. But when
    // AppClassContext is serialized, its AppClass instance is
    // not serialized which breaks the circual reference. When
    // AppClassContext is deserialized, its AppClass reference
    // is null. Hence the need to implement IDeserialization
    // and reset AppClassContext's AppClass reference.
    // Event handlers must also be put back in place for the same
    // reason.</div>
    <div class="datatype">void</div> IDeserializationCallback.OnDeserialization(<div class="datatype">Object</div> sender)
    {
        _fsm.Owner = <div class="keyword">this</div>;
        _fsm.StateChange += handler;
    }

    <div class="keyword">static</div> <div class="datatype">void</div> Main(<div class="datatype">string</div>[] args)
    {
        Serialize();
        Deserialize();
    }

    <div class="keyword">static</div> <div class="datatype">void</div> Serialize()
    {
        AppClass appInstance = <div class="keyword">new</div> AppClass();
        <div class="datatype">FileStream</div> fstream =
            <div class="keyword">new</div> <div class="datatype">FileStream</div>(<div class="string">"fsm_serial.dat"</div>, FileMode.Create);
        <div class="datatype">BinaryFormatter</div> formatter = <div class="keyword">new</div> <div class="datatype">BinaryFormatter</div>();

        <div class="keyword">try</div>
        {
            formatter.Serialize(fstream, appInstance);
        }
        <div class="keyword">catch</div> (<div class="datatype">SerializationException</div> serialex)
        {
<div class="comment">            // Handle exception.</div>
        }
        <div class="keyword">finally</div>
        {
            fstream.Close();
        }
    }

    ...
}
      </div>
      <p>
        Recreating the persisted AppClass instance is equally
        simple:
      </p>
      <div class="code-block">
    <div class="keyword">static</div> <div class="datatype">void</div> Deserialize()
    {
       AppClass appInstance = <div class="keyword">null</div>;
       <div class="datatype">FileStream</div> fstream =
           <div class="keyword">new</div> <div class="datatype">FileStream</div>(<div class="string">"fsm_serial.dat"</div>, FileMode.Open);
       <div class="datatype">BinaryFormatter</div> formatter = <div class="keyword">new</div> <div class="datatype">BinaryFormatter</div>();
       
       <div class="keyword">try</div>
       {
           appInstance = (AppClass) formatter.Deserialize(fstream);
       }
       <div class="keyword">catch</div> (<div class="datatype">SerializationException</div> serialex)
       {
<div class="comment">           // Handle exception.</div>
       }
       <div class="keyword">finally</div>
       {
           fstream.Close();
       }
    }
      </div>
    </div>
    <!-- This is the page's footer. -->
    <div class="footer">
      Copyright � 2000 - 2009. Charles W. Rapp.  All rights reserved.
    </div>
  </body>
</html>
