<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>SMC Manual: Section 2</title>
<meta name="Author" content="Charles W. Rapp"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<link rel="stylesheet" type="text/css" href="style.css"/>
<link rel="stylesheet" type="text/css" media="print" href="print.css"/>
<base target="SmcManPageFrame"/>
</head>
<body>
<div class="heading">
      <!-- Put Section Title Here -->
      From Model to SMC
    </div>
<!-- Put manual text here. -->
<div class="main-body">
  <p>
    This section shows a quasi-UML state machine snipet and
    the equivalent SMC code. I use the word "quasi" because
    SMC is not directly derived from UML or Harel state
    machines. That means that there are capabilities in UML
    that are not in SMC and vice versa. See the
    <a href="http://smc.sourceforge.net/SmcFAQ.htm#NotUML" target="_blank">
      SMC FAQ
    </a>
    for why this is.
  </p>
  <h3>
    <a name="Instantiating">
      Instantiating a Finite State Machine
    </a>
  </h3>
  <p>
    Care must be taken when instantiating an SMC-generated finite
    state machine. The application class passes a its reference
    to the FSM context and this reference is used when FSM
    actions are performed. It is safe to instantiation an FSM
    within a constructor but unsafe to enter the start state
    while in a constructor because the start state entry actions
    will call your application object before its constructor has
    completed.
  </p>
  <p>
    <div class="code-block-tiny">
private final AppClassContext _fsm;

public AppClass()
{
    // Initialize you application class.
    // Instantiate your finite state machine.
    // Note: it is safe to pass <i>this</i> to the the FSM
    //       constructor because the FSM constructor only
    //       stores <i>this</i> in a data member.
    _fsm = new AppClassContext(this);
}

// Enter the FSM start state <i>after</i> instantiating this
// application object.
public void startWorking()
{
    _fsm.enterStartState();
    return;
}
    </div>
  </p>
  <p>
    Prior to SMC v. 6, the FSM constructor incorrectly used the
    <i>this</i> pointer which meant instantiating the FSM within
    the application constructor could lead to incorrect behavior.
    SMC v. 6 corrects this problem and it is now safe to
    instantiate the FSM within your application constructor.
  </p>
  <table align="center" border="3" cellspacing="2" cellpadding="2">
    <tr>
      <td>
        <b>
          The <code>enterStartState</code> method should be
          called only once after instantiating the finite state
          machine and prior to issuing any transition. This
          method is unprotected and does not prevent its being
          called multiple times. If
          <code>enterStartState</code> is called after the
          first transition, then incorrect behavior may occur.
        </b>
      </td>
    </tr>
  </table>
  <h3>
    <a name="SimpleTrans">
      A Simple Transition
    </a>
  </h3>
  <table>
    <tr>
      <td align="left" valign="middle">
        <img src="./Images/SimpleTrans.png" alt="Simple Transition"/>
      </td>
      <td align="left" valign="middle">
        <div class="code-block-tiny">
// State
Idle
{
    // Trans   Next State     Actions
    Run        Running        {}
}
        </div>
      </td>
    </tr>
  </table>
  <p>
    A state and transition names must have the form
    "[A-Za-z_][A-Za-z0-9_]*".
  </p>
  <h3>
    <a name="JumpTrans">
      A Jump Transition
    </a>
  </h3>
  <table>
    <tr>
      <td align="left" valign="middle">
        <img src="./Images/SimpleTrans.png" alt="Jump Transition"/>
      </td>
      <td align="left" valign="middle">
        <div class="code-block-tiny">
// State
Idle
{
    // Trans   Next State     Actions
    Run        jump(Running)  {}
}
        </div>
      </td>
    </tr>
  </table>
  <p>
    The Jump transition is equivalent to the
    <a href="./SmcManSec2.htm#SimpleTrans">
      Simple Transition
    </a>
    and is provided since this transition is used in an
    <a href="http://en.wikipedia.org/wiki/Augmented_transition_network" target="_blank">
      Augmented Transition Network.
    </a>
  </p>
  <table align="center" border="3" cellspacing="2" cellpadding="2">
    <tr>
      <td>
        <b>
          In a future SMC release, the Jump transition will
          become the only way for make an end state outside the
          current map. With the syntax : jump(AnotherMap::NextState)
        </b>
      </td>
    </tr>
  </table>
      <h3>
        <a name="ExternalLoopback">
          An External Loopback Transition
        </a>
      </h3>
      <table>
        <tr>
          <td align="left" valign="middle">
            <img src="./Images/Loopback.png" alt="External Loopback Transition"/>
          </td>
          <td align="left" valign="middle">
            <div class="code-block-tiny">
// State
Idle
{
    // Trans   Next State     Actions
    Timeout    Idle           {}
}
            </div>
          </td>
        </tr>
      </table>
      <p>
        An external loopback does leave the current state and
        comes back to it. This means that the state's
        <a href="./SmcManSec2.htm#EntryExit">exit and entry</a>
        actions are executed. This is in contrast to an
        <a href="./SmcManSec2.htm#InternalLoopback">internal</a>
        loopback transition.
      </p>
      <h3>
        <a name="InternalLoopback">
          An Internal Loopback Transition
        </a>
      </h3>
      <table>
        <tr>
          <td align="left" valign="middle">
            <img src="./Images/InternalLoopback.png" alt="Internal Loopback Transition"/>
          </td>
          <td align="left" valign="middle">
            <div class="code-block-tiny">
// State
Idle
{
    // Trans   Next State     Actions
    Timeout    nil            {}
}
            </div>
          </td>
        </tr>
      </table>
      <p>
        Using "nil" as the next state causes the transition
        to remain in the current state and not leave it. This
        means that the state's
        <a href="./SmcManSec2.htm#EntryExit">exit and entry</a>
        actions are <i>not</i> executed. This is in contrast to an
        <a href="./SmcManSec2.htm#ExternalLoopback">external</a>
        loopback transition.
      </p>
      <h3>
        <a name="TransActs">
          A Transition with Actions
        </a>
      </h3>
      <table>
        <tr>
          <td align="left" valign="middle">
            <img src="./Images/TransAct.png" alt="Transition Action"/>
          </td>
          <td align="left" valign="middle">
            <div class="code-block-tiny">
// State
Idle
{
    // Trans
    Run
        // Next State
        Running
        // Actions
        {
            StopTimer("Idle");
            DoWork();
        }
}
            </div>
          </td>
        </tr>
      </table>
      <ol>
        <li>
          A transition's actions must be enclosed in a "{ }"
          pair.
        </li>
        <li>
          The action's form is
          "[A-Za-z][A-Za-z0-9_-]*(&lt;argument list&gt;)". The
          argument list must be either empty or consist
          of comma-separated literals. Examples of
          literals are: integers (positive or negative,
          decimal, octal or hexadecimal), floating point,
          strings enclosed in double quotes, constants and
          <a href="#TransArgs">transition arguments.</a>
        </li>
        <li>
          Actions must be member functions in the
          %class class and accessible by the state machine.
          Usually that means public member functions in C++
	  or package in Java.
        </li>
      </ol>
      <p>
        <a name="ArgTypes">Action arguments include:</a>
      </p>
      <ul>
        <li>
          An integer number (e.g. 1234).
	</li>
	<li>
	  A float number (e.g. 12.34).
        </li>
        <li>
	  A string (e.g. "abcde").
	</li>
	<li>
	  A <a href="#TransArgs">transition argument.</a>
	</li>
	<li>
	  A constant, #define or global variable.
	</li>
	<li>
	  An independent subroutine or method call
	  (e.g. event.getType()).
	  <br/>
<b>Note:</b> this
	  subroutine/method call may also include arguments.
	</li>
      </ul>
<p>
	If you want to call a method in the
	<a href="SmcManSec1.htm#ClassDecl">context class</a>,
	then use the <code>ctxt</code> variable. For example,
	if the context class contains a method
	<code>getName()</code> and you want to call it inside
	an action's argument list, then write
	<code>ctxt.getName()</code>.
      </p>
      <p>
	Go <a href="SmcManSec1.htm#Ctxt">here</a> for sample
	code using the <code>ctxt</code> variable.
      </p>
      <p>
	<b>Note:</b> Only use <code>ctxt</code> inside argument
	lists and <a href="#TransGuards">transition guards</a>.
      </p>
      <h3>
        <a name="TransGuards">
          Transition Guards
        </a>
      </h3>
      <table>
<tr>
<td align="left" valign="middle">
            <img src="./Images/TransGuard.png" alt="Transition Guard"/>
</td>
          <td align="left" valign="middle">
            <div class="code-block-tiny">
// State
Idle
{
    // Trans
    Run

      // Guard condition
      [ctxt.isProcessorAvailable() == true &amp;&amp;
       ctxt.getConnection().isOpen() == true]

        // Next State
        Running
        // Actions
        {
            StopTimer("Idle");
            DoWork();
        }

    Run        nil            {RejectRequest();}
}
            </div>
          </td>
        </tr>
</table>
<p>
        The guard must contain a condition that is valid
        target language source code - that is, it would be a
        valid "if" statement. Your guard may contain
        &amp;&amp;s, ||s, comparison operators (==, &lt;, etc.)
        and nested expressions. SMC copies your guard condition
        verbatim into the generated output.
      </p>
      <p>
        <b>Note:</b> If your are calling a context class
        method, then you must prefix the method with
        <code>ctxt</code> - SMC will <i>not</i> append
        <code>ctxt</code> for you.
      </p>
      <p>
        If the guard condition evaluates to true,
        then the transition is taken. If the guard condition
        evaluates to false, then one of the following
        occurs (ordered by precedence):
      </p>
      <ol>
<li>
          If the state has another guarded transition with
          the same name and
          <a href="#TransArgs">arguments</a>, that
          transition's guard is checked.
        </li>
        <li>
          Failing that, If the state has another unguarded
          transition with the same name and
          <a href="#TransArgs">argument list</a>, that
          transition is taken.
        </li>
        <li>
          If none of the above, then the
          <a href="./SmcManSec2.htm#DefaultTrans">
            default transition
          </a>
          logic is followed.
        </li>
      </ol>
<p>
        A state may have multiple transitions with the same
        name and <a href="#TransArgs">argument list</a> as long
        as they all have unique guards. When a state does have
        multiple transitions with the same name, care must be
        taken when ordering them. The state machine compiler will
        check the transitions in the same top-to-bottom order
        that you use <i>except</i> for the unguarded version.
        That will always be taken only if all the guarded
        versions fail. Guard ordering is only important if the
        guards are not mutually exclusive, i.e., it is possible
        for multiple guards to evaluate to true for the same
        event.
      </p>
      <p>
	Allowable argument types for a transition guard are the
	same as for a <a href="#ArgTypes">transition action.</a>
      </p>
      <h3>
        <a name="TransArgs">
          Transtion Arguments
        </a>
      </h3>
      <table>
<tr>
<td align="left" valign="middle">
            <img src="./Images/TransArg.png" alt="Transition Argument"/>
</td>
          <td align="left" valign="middle">
            <div class="code-block-tiny">
// State
Idle
{
    // Transition
    Run(msg: const Message&amp;)

      // Guard condition
      [ctxt.isProcessorAvailable() == true &amp;&amp;
       msg.isValid() == true]

        // Next State
        Running
        // Actions
        {
            StopTimer("Idle");
            DoWork(msg);
        }

    Run(msg: const Message&amp;)
        // Next State    Actions
        nil              {RejectRequest(msg);}
}
            </div>
          </td>
        </tr>
</table>
<p>
        <b>Note:</b> When using transition guards and
        transition arguments, multiple instances of the same
        transition must have the same argument list. Just as with
        C++ and Java methods, the transitions
        <code>Run(msg: const Message&amp;)</code> and
        <code>Run()</code> are <i>not</i> the same transition.
        Failure to use the identical argument list when defining
        the same transition with multiple guards will result in
        incorrect code being generated.
      </p>
      <p>
        <a name="TclArgs">
          <b>Tcl "arguments":</b>
        </a>
      </p>
      <p>
        While Tcl is a type-less
        language, Tcl does distinguish between call-by-value
        and call-by-reference. By default SMC will generate
        call-by-value Tcl code if the transition argument has
        no specified type. But you may use the artificial types
        "value" or "reference".
      </p>
      <p>
        If your Tcl-targeted FSM has a transition:
      </p>
      <div class="code-block">
DoWork(task: value)
    Working
    {
        workOn(task);
    }
      </div>
      <p>
        then the generated Tcl is:
      </p>
      <div class="code-block">
public method DoWork {task} {
    workOn $this $task;
}
        </div>
      <p>
        If your Tcl-targeted FSM has a transition:
      </p>
      <div class="code-block">
DoWork(task: reference)
    Working
    {
        workOn(task);
    }
      </div>
      <p>
        then the generated Tcl is:
      </p>
      <div class="code-block">
public method DoWork {task} {
    workOn $this task;
}
      </div>
      <p>
        The method <code>workOn</code> must <code>upvar</code>
        the task parameter:
      </p>
      <div class="code-block">
public method workOn {taskName} {
    upvar $taskName task;
    ...
}
      </div>
      <p>
        <a name="PythonArgs">
          <b>Lua/Python/Ruby "arguments":</b>
        </a>
      </p>
      <p>
        While Lua/Python/Ruby is a dynamically typed language and does not
        use types for function parameter definitions, you could
        provide a optional data type for transition arguments. This
        "data type" is ignored when generating the target Lua/Python/Ruby
        code. I suggest using meaningful type names.
      </p>
      <div class="code-block">
DoWork(task: TaskObj, runtime: Ticks)
    Working
    {
        ...
    }
      </div>
      <p>
        <a name="GroovyArgs">
          <b>Groovy/PHP "arguments":</b>
        </a>
      </p>
      <p>
        While Groovy/PHP gives the choice between static and dynamic typing,
        you could provide a optional data type for transition arguments.
        In this case, the type is used when generating the target Groovy/PHP
        code.
      </p>
      <p>
        The PHP variable syntax is like Perl (named with '$').
      </p>
      <p>
        <a name="PerlArgs">
          <b>Perl "arguments":</b>
        </a>
      </p>
      <p>
        While Perl is a dynamically typed language and does not
        use types for function parameter definitions, you could
        provide a optional data type for transition arguments. This
        "data type" is ignored when generating the target Perl
        code. I suggest using meaningful type names.
      </p>
      <p>
        Only Perl scalar values (ie, named with '$') are allowed.
      </p>
      <div class="code-block">
DoWork($task: TaskObj, $runtime: Ticks)
    Working
    {
        ...
    }
      </div>
      <h3>
        <a name="EntryExit">
          Entry and Exit Actions
        </a>
      </h3>
      <table>
<tr>
<td align="left" valign="middle">
            <img src="./Images/EntryExit.png" alt="Entry, Exit Actions"/>
</td>
          <td align="left" valign="middle">
            <div class="code-block-tiny">
// State
Idle
Entry {StartTimer("Idle", 1); CheckQueue();}
Exit {StopTimer("Idle");}
{
    // Transitions
}
            </div>
          </td>
        </tr>
      </table>
      <p>
        When a transition leaves a state, it executes the state's
        exit actions before any of the transition actions. When a
        transition enters a state, it executes the state's entry
        actions. A transition executes actions in this order:
        <ol>
          <li>
            "From" state's exit actions.
          </li>
          <li>
            Set the current state to null.
          </li>
          <li>
            The transition actions in the same order as defined
            in the .sm file.
          </li>
          <li>
            Set the current state to the "to" state.
          </li>
          <li>
            "To" state's entry actions.
          </li>
        </ol>
        As of version 6.0.0, SMC generates a
        <code>enterStartState</code> method which executes the
        start state's entry acitons. It is now up to the
        application to call the start method after instantiating
        the finite state machine context. If it is not
        appropriate to execute the entry actions upon start up,
        then do not call <code>enterStartState</code>. You are
        not required to call this method to set the finite state
        machine start state. That is done when the FSM is
        instantiated. This method is used only to execute the
        start state's entry actions.
      </p>
      <p>
        If you do call this method, be sure to do it outside of
        the context class' constructor. This is because entry
        actions call context class methods. If you call
        <code>enterStateState</code> from within you context
        class' constructor, the context instance will be
        referenced before it has completed initializing which is
        a bad thing to do.
      </p>
      <p>
        <b>
          <code>enterStartState</code> does not protect against
          being called multiple times.
        </b>
        It should be called at most once and prior to issuing any
        transitions. Failure to follow this requirement may
        result in inappropriate finite state machine behavior.
      </p>
      <p>
        Whether a state's Entry and Exit actions are executed
        depends on the type of transition taken. The following
        table shows which transitions execute the "from"
        state's Exit actions and which transitions execute the
        "to" state's Entry actions.
      </p>
      <table align="center" border="3" cellspacing="2" cellpadding="2">
        <tr>
          <th>
            <b>Transition Type</b>
          </th>
          <th>
            <b>Execute "From" State's<br/>Exit Actions?</b>
          </th>
          <th>
            <b>Execute "To" State's<br/>Entry Actions?</b>
          </th>
        </tr>
        <tr>
          <td align="left">
            Simple Transition
          </td>
          <td align="center">
            Yes.
          </td>
          <td align="center">
            Yes.
          </td>
        </tr>
        <tr>
          <td align="left">
            External Loopback Transition
          </td>
          <td align="center">
            Yes.
          </td>
          <td align="center">
            Yes.
          </td>
        </tr>
        <tr>
          <td align="left">
            Internal Loopback Transition
          </td>
          <td align="center">
            No.
          </td>
          <td align="center">
            No.
          </td>
        </tr>
        <tr>
          <td align="left">
            Push Transition
          </td>
          <td align="center">
            No.
          </td>
          <td align="center">
            Yes.
          </td>
        </tr>
        <tr>
          <td align="left">
           Pop Transition
          </td>
          <td align="center">
            Yes.
          </td>
          <td align="center">
            No.
          </td>
        </tr>
      </table>
      <br/>
      <table align="center" border="3" cellspacing="2" cellpadding="2">
        <tr>
          <td>
              <b>
                From this point on, SMC diverges from UML.
              </b>
              SMC uses the idea of multiple machines and pushing
              and popping states as way of breaking complicated
              behavior up into simpler parts. UML acheives much
              the same by grouping states into superstates. They
              <i>may</i> be equivalent in ability but I find the
              idea of pushing to a new state easier to understand
              because it is similar to the subroutine call.
          </td>
        </tr>
      </table>
      <h3>
        <a name="PushTrans">
          Push Transition
        </a>
      </h3>
      <table>
        <tr>
          <td align="left" valign="middle">
            <img src="./Images/PushTrans.png" alt="Push Transition"/>
          </td>
          <td align="left" valign="middle">
            <div class="code-block-tiny">
Running
{
    Blocked    push(WaitMap::Blocked)    {GetResource();}
}
            </div>
          </td>
        </tr>
      </table>
      <p>
        <b>Note:</b> The end state does not have to be in
        another map - it could be in the same %map construct.
        Conversely, a plain transition's end state may
        be in another map. But chances are that you will set
        up maps so that you will push to another map's state
        and simple transitions will stay within the same map.
        You use multiple maps for the same reason you create
        multiple subroutines: to separate out functionality
        into easy-to-understand pieces.
      </p>
      <p>
        <a name="MultiPushTrans">
	  With SMC v. 1.3.2, the push syntax was modified yet is
	  backward compatible with the initial syntax. The new
	  syntax is
	</a>
      </p>
      <div class="code-block">
Running
{
    Blocked    BlockPop/push(WaitMap::Blocked)  {GetResource();}
}
      </div>
      <p>
	This causes the state machine to:
      </p>
<p>
      </p>
<ol>
<li>
	  Transition to the <code>BlockPop</code> state.
	</li>
	<li>
	  Execute the <code>BlockPop</code> entry actions.
	</li>
	<li>
	  Push to the <code>WaitMap::Blocked</code> state.
	</li>
	<li>
	  Execute the <code>WaitMap::Blocked</code> entry
	  actions.
	</li>
      </ol>
<p>
	When <code>WaitMap</code> issues a
	<a href="#PopTrans">pop</a> transition, control will
	return to <code>BlockPop</code> and the pop transition
	is issued from there.
      </p>
      <p>
	Use this new syntax when a state has two different
	transitions which push to the same state but need
	to handle the pop transition differently. For example:
      </p>
      <div class="code-block">
Idle
{
    NewTask     NewTask/push(DoTask)    {}
    RestartTask OldTask/push(DoTask)    {}
}

NewTask
{
    TaskDone    Idle                    {}

    // Try running the task one more time.
    TaskFailed  OldTask/push(DoTask)    {}
}

OldTask
{
    TaskDone    Idle                    {}
    TaskFailed  Idle                    {logFailure();}
}
      </div>
      <h3>
        <a name="PopTrans">
          Pop Transition
        </a>
      </h3>
      <table>
<tr>
<td align="left" valign="middle">
            <img src="./Images/PopTrans.png" alt="Pop Transition"/>
</td>
          <td align="left" valign="middle">
            <div class="code-block-tiny">
Waiting
{
    Granted    pop(OK)        {cleanUp();}
    Denied     pop(FAILED)    {cleanUp();}
}
            </div>
          </td>
        </tr>
</table>
<p>
        The pop transition differs from the simple and push
        transition in that:
      </p>
<p>
      </p>
<ul>
<li>
          The end state is not specified. That is because the
          pop transition will return to whatever state issued
          the corresponding push.
        </li>
        <li>
          There pop transition has an <i>optional</i>
          argument: a transition name.
        </li>
      </ul>
<p>
        In the above example, if the resource request is
        granted, the state machine returns to the corresponding
        state that did the push and then takes that state's
        OK transition. If the request is denied, the same
        thing happens except the FAILED transition is taken.
        The code for the corresponding push transition is:
      </p>
      <div class="code-block">
Running
{
    Blocked    push(WaitMap::Blocked)    {GetResource();}

    // Handle the return "transitions" from WaitMap.
    OK         nil                       {}
    FAILED     Idle                      {Abend(INSUFFICIENT_RESOURCES);}
}
      </div>
      <p>
        As of SMC v. 1.2.0, additional arguments may be added
        <i>after</i> the pop transition's transition argument.
        These additional arguments are like any others passed
        to an action and will be passed into the named
        transition. Following the above example, given the
        pop transition <code>pop(FAILED, errorCode, reason)</code>,
        then the <code>FAILED</code> should be coded as:
      </p>
      <div class="code-block">
FAILED(errorCode: ErrorCode, reason: string)
    Idle
    {
        Abend(errorCode, reason);
    }
      </div>
      <h3>
        <a name="DefaultTrans">
          Default Transitions
        </a>
      </h3>
      <p>
        What happens if a state receives a transition that is
        not defined in that state? SMC has two separate
        mechanisms for handling that situation.
      </p>
      <p>
        The first is the "Default" state. Every %map may have
        a special state named "Default" (the uppercase D is
        significant). Like all other states, the Default state
        contains transitions.
      </p>
      <div class="code-block">
Default
{
    // Valid run request but transition occurred in an invalid
    // state. Send a reject reply to valid messages.
    Run(msg: const Message&amp;)
      [ctxt.isProcessorAvailable() == true &amp;&amp;
       msg.isValid() == true]
        nil
        {
            RejectRequest(msg);
        }

    // Ignore invalid messages are ignored when received in
    // an invalid state.
    Run(msg: const Message&amp;)
        nil
        {}

    Shutdown
        ShuttingDown
        {
            StartShutdown();
        }
}
      </div>
      <p>
        Default state transitions may have guards and arguments
        features as non-default transitions. This means the
        Default state may contain multiple guarded and one
        unguarded definition for the same transition.
      </p>
      <p>
        The second mechanism is the "Default" transition. This
        is placed inside a state and is used to back up all
        transitions.
      </p>
      <div class="code-block">
Connecting
{
    // We are now connected to the far-end. Now we can logon.
    Connected
        Connected
        {
            logon();
        }

    // Any other transition at this point is an error.
    // Stop the connecting process and retry later.
    Default
        RetryConnection
        {
            stopConnecting();
        }
}
      </div>
      <p>
        Because any transition can fall through to
        the Default transition, Default transitions:
      </p>
      <ul>
<li>
          May <i>not</i> have an argument list.
        </li>
        <li>
          A Default transition may take a guard.
        </li>
        <li>
          Putting a Default transition in the Default
          state means that all transitions will be handled - it
          is the transition definition of last resort.
        </li>
      </ul>
<h3>
        <a name="DefaultPrecedence">
          Transition Precedence
        </a>
      </h3>
      <p>
        Transition definitions have the following
        precedence:
      </p>
      <ol>
<li>
          Guarded transition
        </li>
        <li>
          Unguarded transition
        </li>
        <li>
          The Default state's guarded definition.
        </li>
        <li>
          The Default state's unguarded definition.
        </li>
        <li>
          The current state's guarded Default transition.
        </li>
        <li>
          The current state's unguarded Default transition.
        </li>
        <li>
          The Default state's guarded Default transition.
        </li>
        <li>
          The Default state's unguarded Default transition.
        </li>
      </ol>
<table align="center" border="3" cellspacing="2" cellpadding="2">
<tr>
<td>
            <b>
              Since SMC does not force you to specify a Default
              state or a Default transition, it is possible that
              there is no transition defined. If SMC falls
              through this list, it will throw a
              "Transition Undefined" exception.
            </b>
          </td>
        </tr>
</table>
</div>
<!-- This is the page's footer. -->
<div class="footer">
      Copyright � 2000 - 2009. Charles W. Rapp.  All rights reserved.
    </div>
</body>
</html>
