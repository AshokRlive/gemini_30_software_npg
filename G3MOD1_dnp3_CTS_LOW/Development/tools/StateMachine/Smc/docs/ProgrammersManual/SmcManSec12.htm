<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <title>SMC: The State Machine Compiler</title>
    <meta name="Author" content="Charles W. Rapp"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link rel="stylesheet" type="text/css" media="print" href="print.css"/>
    <base target="SmcManPageFrame"/>
  </head>
  <body>
    <div class="heading">
      <!-- Put the page's title here. -->
      Getting Noticed.
    </div>
    <div class="main-body">
      <p>
        <!-- Put page body here. -->
        SMC uses the Java Bean event notification and .Net event
        raising features to
        inform listeners when an SMC-generated finite state
        machine changes state. The following sample code
        demonstrates how to use this feature in
        <a href="#Java">Java</a>, <a href="#CSharp">C#</a>,
        <a href="#VB">VB.net</a>, <a href="#Groovy">Groovy</a>
        and <a href="#Scala">Scala</a>.
      </p>
      <h3>
        <a name="Java">
          Java Sample
        </a>
      </h3>
      <p>
        SMC uses Java Beans package for state change
        notification. <code>statemap.FSMContext</code> defines
        the <code>addStateChangeListener</code> and
        <code>removeStateChangeListener</code> methods. Because
        the SMC-generated <code>FSMContext</code> subclass is
        privately contained within an application class, it will
        be necessary to expose the add, remove methods by
        adding these methods to the application class.
      </p>
      <p>
        <b>Note:</b> Because applications use multiple state
        machines and a state change handler may register with
        multiple FSMs, I have added the methods
        <code>FSMContext.getName()</code> and
        <code>FSMContext.setName(String)</code>. This name
        should be set and used to distinguish between FSMs.
      </p>
      <p>
      </p>
      <div class="code-block">
import java.beans.PropertyChangeListener;

public class AppClass
{<div class="comment">
//---------------------------------------------------------------
// Member methods.
//</div>
    ...

    public void addStateChangeListener(PropertyChangeListener listener)
    {
        _fsm.addStateChangeListener(listener);
        return;
    }

    public void removeStateChangeListener(PropertyChangeListener listener)
    {
        _fsm.removeStateChangeListener(listener);
        return;
    }
<div class="comment">
//---------------------------------------------------------------
// Member data.
//</div>
    ...

    private final AppClassContext _fsm;
}<div class="comment"> // end of class AppClass</div>
      </div>
      <p>
        Implement the
        <code>java.beans.PropertyChangeListener</code> interface
        and pass that implementation to
        <code>AppClass.addStateChangeListener</code>. When the
        AppClass finite state machine changes state, the listener
        receives a
        <code>java.beans.PropertyChangeEvent</code>
        containing:
      </p>
      <ul>
<li>
          the <code>FSMContext</code> instance as the source.
        </li>
        <li>
          the "State" property name.
        </li>
        <li>
          the previous state
          (class <code>statemap.State</code>).
        </li>
        <li>
          the new state (class <code>statemap.State</code>).
        </li>
      </ul>
<div class="code-block">
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import statemap.FSMContext;
import statemap.State;

public class StateChangeListener
    implements PropertyChangeListener
{<div class="comment">
//---------------------------------------------------------------
// Member methods.
//</div>
    ...

    public void propertyChange(PropertyChangeEvent event)
    {
        FSMContext fsm = (FSMContext) event.getSource();
        String propertyName = event.getPropertyName();
        State previousStatus = (State) event.getOldValue();
        State newState = (State) event.getNewValue();

        <div class="comment">// Handle the state change event.</div>
        System.out.println(
            "FSM " + fsm.getName() + " went from " + previousState + " to " + newState + ".");

        return;
    }
}<div class="comment"> // end of class StateChangeListener</div>
      </div>
      <p>
        Register the <code>StateChangeListener</code> with the
        FSM as follows:
      </p>
      <div class="code-block">
StateChangeListener listener = new StateChangeListener();
AppClass appInstance = new AppClass();

appInstance.addStateChangeListener(listener);
      </div>
        <b>Note:</b>
        <a name="Groovy">Groovy</a> and
        <a name="Scala">Scala</a> also use
        <a href="#Java">Java Bean</a> event notification.
      <h3>
        <a name="CSharp">
          C# Sample
        </a>
      </h3>
      <p>
        .Net events required listeners to register an event
        directly with the event-raising object. For SMC, the
        finite state machine class FSMContext does the event
        raising. But the context class should be kept private
        within the application class. The following code shows
        how to add and remove state change listeners indirectly,
        leaving the FSM inaccessible.
      </p>
      <p>
        The StateChangeEventArgs data methods are:
      </p>
      <ul>
<li>
          <b>FSMName():</b> returns the FSM's name as a
          <code>string</code>.
        </li>
        <li>
          <b>TransitionType():</b> returns one of the following
          <code>string</code>'s: SET, PUSH or POP.
        </li>
        <li>
          <b>PreviousState():</b> returns the state which the
          FSM exited.
        </li>
        <li>
          <b>NewState():</b> returns the state which at the FSM
          entered.
        </li>
      </ul>
<p>
        <b>Note:</b> Because applications use multiple state
        machines and a state change handler may register with
        multiple FSMs, I have added the property
        <code>FSMContext.Name</code>. This name is placed into
        <code>StateChangeEventArgs</code> to allow ready
        identification of which FSM changed state. The default
        state name is "FSMContext".
      </p>
<p>
      </p>
<div class="code-block">
use System;
use statemap;

public class AppClass
{
    ....

    public void AddStateChangeHandler(StateChangeEventHandler handler)
    {
        _fsm.StateChange += handler;
        return;
    }

    public void RemoveStateChangeHandler(StateChangeEventHandler handler)
    {
        _fsm.StateChange -= handler;
        return;
    }
}
      </div>
      <p>
        If a class wishes to receive state change events, then it
        must implement a method with the following signature.
        <b>Note:</b> the method does not have to be named
        <code>StateChanged</code>.
      </p>
      <div class="code-block">
use System;
use statemap;

public class StateChangeHandler
{
    public void StateChanged(object sender, StateChangeEventArgs args)
    {
        <div class="comment">//Handle the state change event.</div>
        Console.WriteLine("FSM " +
                          args.FSMName() +
                          " " +
                          args.TransitionType() +
                          " transition from " +
                          args.PreviousState() +
                          " to " +
                          args.NewState() +
                          ".");

        return;
    }
}
      </div>
      <p>
        Register the state change handler instance with the FSM
        as follows:
      </p>
      <div class="code-block">
StateChangeHandler handler = new StateChangeHandler();
AppClass appInstance = new AppClass();

appInstance.AddStateChangeHandler(new StateChangeEventHandler(handler.StateChange));
      </div>
      <h3>
        <a name="VB">
          VB.Net Sample
        </a>
      </h3>
      <p>
        State change event handlers register indirectly via these
        application class methods (which you must add to your
        code).
      </p>
      <div class="code-block">
Public Class AppClass
    ...

    Public Sub AddStateChangeHandler(handler As statemap.StateChangeEventHandler)

        AddHandler _fsm.StateChange, handler
    End Sub

    Public Sub RemoveStateChangeHandler(handler As statemap.StateChangeEventHandler)

        RemoveHandler _fsm.StateChange, handler
    End Sub

End Class
      </div>
      <p>
        If a class wishes to receive state change events, then it
        must implement a method with the following signature.
      </p>
      <p>
        See the <a href="#CSharp">C#</a> sample for more about
        StateChangeEventArgs.
      </p>
      <div class="code-block">
Imports System
Imports statemap

Public Class StateChangeHandler
    ...

    Public Sub StateChange(sender As Object, e As StateChangeEventArgs)

       Console.Write("FSM ")
       Console.Write(e.FSMName())
       Console.Write(" ")
       Console.Write(e.TransitionType())
       Console.Write(" transition from ")
       Console.Write(e.PreviousState())
       Console.Write(" to ")
       Console.Write(e.NewState())
       Console.WriteLine(".")

    End Sub

End Class
      </div>
      <p>
        Register the state change handler with the FSM as
        follows:
      </p>
      <div class="code-block">
Dim handler As StateChangeHandler = new StateChangeHandler()
Dim appInstance As AppClass = new AppClass()

appInstance.addStateChangeHandler(AddressOf handler.StateChange)
      </div>
    </div>
    <!-- This is the page's footer. -->
    <div class="footer">
      Copyright � 2008, 2009. Charles W. Rapp.  All rights reserved.
    </div>
  </body>
</html>
