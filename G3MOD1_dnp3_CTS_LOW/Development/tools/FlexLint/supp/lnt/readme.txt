
The files in this directory for the most part fall into one of the
following classifications:

co-*.lnt    Compiler options files for some popular and some obsolete
            compilers.  Some of these compilers are embedded compilers
            and they show how unusual compiler features can be
            supported with FlexeLint.

lib-*.lnt   Library options files for some popular libraries.  The
            FlexeLint user might be interested in employing one
            or more of these files since they have in some cases
            been ported to operating systems other than Windows.

env-*.lnt   Environment options files for some popular editing
            or development environments.  They provide a few options
            and mostly commentary on how to adapt PC-lint/FlexeLint
            to a particular working environment.  Some of these
            environments, particulary the editors, have been
            ported to non Windows operating systems and so the
            FlexeLint customer may be able to use these directly.
            Also, any customer attempting to adapt FlexeLint to any
            environment would probably benefit from these recipes
            for other environments.

au-*.lnt    Author options files for specific author recommended
            option settings.  These also include option configurations
            to support standards organizations standard programming
            practice.  These files are decidedly not Operating System
            dependent.


Gimpel Software
June, 2010
