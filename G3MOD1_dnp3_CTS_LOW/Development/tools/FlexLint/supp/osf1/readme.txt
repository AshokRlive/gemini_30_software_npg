
The operating systems OSF1 and Tru64 are similar enough to Unix for
the purpose of linting that you will find it best to go to the
directory named 'unix' parallel to this directory.

If you are using gcc as a compiler then by all means go to the
directory 'gcc' which is again parallel to this one.

Gimpel Software, 2008
