
The contents of this directory represents an experimental effort
to adapt FlexeLint to processing programs written for Cell Broadband
Engine.  It is, in fact, a work in progress and no guarantee can be
made as to its fitness for its intended purpose.  We can only
claim that it can serve as a useful starting point for programs
written in this arena in which we ourselves are not expert.

Feedback, including extensions and suggestions regarding this material,
are welcome

Gimpel Software,  July 2008

