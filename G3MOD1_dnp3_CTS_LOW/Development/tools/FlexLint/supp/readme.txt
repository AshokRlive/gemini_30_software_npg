    This is the FlexeLint Supplementary directory. It is intended as a
    repository of system-specific information.  It contains little
    snippets of information, command scripts, setup hints, etc.
    Accordingly it has been organized by Operating Environment.

    In addition it houses ANSI and POSIX header files.  These headers
    have been meticulously prepared so as to avoid extraneous (i.e.,
    non-ANSI and non-POSIX) facilities.  In this way programs can be
    prepared with the assurance that no local-only facilities have been
    used.  Thus to assure POSIX compliance you should, from time to
    time, use the -i option to direct FlexeLint to look only at the
    POSIX headers in this directory and to not look at your compiler's
    headers.  Using the verbosity option -vf will confirm where the
    headers are coming from.

    In some cases, information has been supplied by our customers, and
    may not have been completely checked out by us.  We pass it along
    anyway in hopes that it may be of some use.  Although, we cannot
    vouch for the accuracy of all the information, we have been diligent
    in listening to user's complaints when they may have felt some of
    the information to be faulty, misleading or obscure.  Accordingly,
    customer reports on the accuracy and completeness of this
    information will be gratefully appreciated.

                                      Gimpel Software
                                      July 2010
