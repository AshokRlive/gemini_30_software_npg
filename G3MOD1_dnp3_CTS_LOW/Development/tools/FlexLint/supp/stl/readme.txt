This directory (STL) refers to the Standard Template Library

The file lib-stl.lnt is an options file that is useful in processing
programs using the Standard Template Library.

For example:

	lint  lib-stl   module(s)

Naturally it should be placed in a centralized location so that it needn't
be referred to on each invocation.
