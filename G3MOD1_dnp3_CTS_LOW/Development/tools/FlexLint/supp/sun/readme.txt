
Compiler options files are available in this directory for a few
recent versions of the Sun compiler.

Using the Gnu compiler (GCC)?  Check in directory '../gcc'.
Also, directory '../linux' has a few older compiler options files
for GCC.

Check out directory ../unix for some basic options for using
FlexeLint in a Unix setting.

Gimpel Software, 2008

