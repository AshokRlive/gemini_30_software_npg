
This directory is intended for programmers using the Data General
AOS/VS operating system.  If you are using the Unix operating system
on a Data General machine see 'unix' and possibly 'gcc'.

For the former case, the compiler options file co-aos.lnt found in this
directory should be placed in a centrally located place and used
for each lint run.


