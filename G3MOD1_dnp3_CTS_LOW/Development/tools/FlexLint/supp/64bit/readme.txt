
This directory contains a number of files to assist you
in linting in and/or porting to 64-bit programming.

o   au-lp64.lnt, au-llp64.lnt and au-ilp64.lnt serve to
    establish the sizes of basic datatypes to support the
    LP64, LLP64, and ILP64 models respectively.

o   au-64.lnt is referenced by each of the previous .lnt files
    to support and/or enable Warnings, Informational, and Elective Note
    messages that deal with 64 bit issues.  Great for transitioning
    to 64 bit mode.  Look inside au-64.lnt and you will find additional
    ways to torture test code you are porting to 64-bit.

o   64bit.c can assist you in determining whether a change to one
    of our editable headers (viz. custom.h) will result in an increase
    in the precision in which we compute user values.
    Specifically we are attempting to learn whether the use of
    "long long" is syntactically viable and, if so, whether that
    datatype has more precision than just "long".


Gimpel Software, June 2009
