/*
    64bit.c

    This program is all about FlexeLint using the maximum integer
    type to calculate user integral values.

    There are three primary 64-bit data models:

        LP64 -- longs and pointers are 64 bit.
        LLP64 -- long longs and pointers are 64 bit.
        ILP64 -- ints, longs and pointers are 64 bit.

    The type that we use to calculate user integral values is defined
    in custom.h and has the name MAX_INT_t.  If you are using C99 then
    this type is predefined to be intmax_t; we pick that up and you
    have no problem.  Otherwise we just assume long to be the largest
    type.  This is fine for the LP64 model and the ILP64 model but
    is not so nice with the LLP64 model.  For this model it would
    be best to use long long.

    The purpose of this program is to test your compiler to see if
    long long is available and whether the size of long long is
    greater than that of long.

    Directions:
    1.  Compile this program!
        If the program does not compile then it does not support the
        long long type.  goto 3.

    2.  If the program did compile then run it and receive further
        instructions.  It will either say that it will be profitable to
        edit custom.h or not.  If you are directed to edit custom.h
        do that and you are finished.  If not go to 3.

    3.  You can't use the long long type to extend your precision but
        perhaps there is some compiler-dependent type (such as __int64)
        that does support increased precision.  If so edit the line
        below that contains:
        long long x;
        so that you replace "long long" by this type and compile and run
        the program to obtain further instructions.
 */

#include <stdio.h>

long long x;        // test to see if your system supports long long

void pr( const char *s )
    {
    printf( "%s\n", s );
    }

void message_ok()
    {
    pr( "Our tests indicate that the long datatype already supports" );
    pr( "64 bits.  We will proceed under the assumption you are seeking to" );
    pr( "obtain more than 64 bits." );
    }

void message_go()
    {
    pr( "Our tests show that your compiler supports the long long data type" );
    pr( "and moreover the size of long long is greater than the size of long." );
    pr( "This means that you will probably be able to obtain more bits of" );
    pr( "precision for tracking user values, computing constants, and making" );
    pr( "arithmetic computations at preprocessor time by doing one of the following:" );
    pr( "" );
    pr( "1.  When you compile FlexeLint use an option such as:" );
    pr( "" );
    pr( "   \"-DLINT_MAX_INT=long long\"   " );
    pr( "" );
    pr( "The intent is to pre-define macro LINT_MAX_INT to be the largest" );
    pr( "integer type.  This is normally preferred over modifying custom.h" );
    pr( "" );
    pr( "                OR    " );
    pr( "" );
    pr( "2.  In custom.h you will find the following pair of declarations:" );
    pr( "" );
    pr( "     typedef long MAX_INT_t;" );
    pr( "     typedef unsigned long MAX_UINT_t;" );
    pr( "" );
    pr( "In both of these declarations replace the 'long' by 'long long'." );
    pr( "Then recompile FlexeLint and FlexeLint will be able to support" );
    pr( "larger integer computations." );
    }

void message_nogo()
    {
    pr( "Thank you for taking the test.  Although your compiler supports" );
    pr( "the long long type it will do you no good to recompile FlexeLint" );
    pr( "to take advantage of it.  This is because the precision of long is" );
    pr( "the same as long long (using your compiler).  Since we have used" );
    pr( "the long type to represent the largest integer type no gain in precision" );
    pr( "would be detected." );
    }


int main()
    {
    if( sizeof(long) >= 8 )
        message_ok();
    if( sizeof(x) > sizeof(long) )
        message_go();
    else
        message_nogo();
    }




