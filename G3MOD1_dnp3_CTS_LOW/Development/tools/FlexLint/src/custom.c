
/*  custom.c  --  customizable and system specific routines
                    for FlexeLint

    Contents:
                    Initial Macros
        Part I:     Customizable Data
        Part II:    Customizable Functions
        Part III:   ANSI function equivalents (if you need them)

                    Gimpel Software
                    July 2008
 */

#include <stdio.h>

#include "vers.h"
#include "custom.h"
#if os_MPW | os_THINKC
#include <CursorCtl.h>
#endif

#define TRUE 1
#define FALSE 0

#if OS_MINT
#define SEP1 '\\'   /* separates directory names in a path */
#define SEP1A ':'   /* an alternate separation character */
#define SEP1B '/'   /* yet another separation character */
#define SEP2 ';'    /* separates paths in a list of paths */
#endif

#if os_UNIX | os_LINUX | os_UCOS
#define SEP1 '/'
#define SEP2 ':'
#endif

#if os_MPW | os_THINKC
#define SEP1 ':'
#define SEP2 ','
#endif

#if os_AMIGA
#define SEP1 '/'
#define SEP2 ':'
#endif

#if os_VMS
#define SEP1 ':'
#define SEP1A ']'
#define SEP2 ','
#endif

#if os_VOS
#define SEP1 '>'
#define SEP2 ';'
#endif

#if os_MVS
#define SEP1 '('
#define SEP1A '.'
#define SEP2 ';'
#endif

#ifndef SEP1
#define SEP1 '/'
#define SEP2 ';'
#endif

#ifndef SEP1A
#define SEP1A SEP1
#endif

#ifndef SEP1B
#define SEP1B SEP1
#endif

/*  If filenames are not case sensitive then they frequently
    should be mapped to one case or another.  This aids in the
    detection of certain standard extensions.  The following flags
    govern this mapping.  See also dot_lnt, dot_lob, sys_fnmap()
 */

#if os_VMS | os_MVS
#define FNMAP_UC 1      /* map file names to upper case */
#else
#define FNMAP_UC 0
#endif

#if OS_DOSCO | os_MPW | os_THINKC
#define FNMAP_LC 1      /* map file names to lower case */
#else
#define FNMAP_LC 0
#endif

#if os_NT
#define FNMAP_MIXED 1   /* support mixed upper-lower that map to the same */
#else
#define FNMAP_MIXED 0
#endif



/*  This file is provided so that users of FlexeLint can customize
    their software.  All rights are reserved by Gimpel Software

    Part I:  Customizable Data
 */

/* ansi_flag, if 1 at FlexeLint start, will inhibit common MS extensions;
   It also causes __STDC__ to be set to 1 rather than remain undefined.
 */

#if OS_MINT | os_XENIX | os_QNX
int ansi_flag = 0;  /* enable  keywords far, near, huge, etc. */
#else
int ansi_flag = 1;  /* causes warning for keywords far, near, huge, etc. */
#endif

/*  dot_c, dot_cpp, dot_h and dot_lnt can be appended onto extentless file
    names depending on os_ and language support.  Also these extensions
    govern the interpretation of files.  These extensions are mapped
    via sys_fnmap() shortly after startup so that the extensions shown
    could be upper case names if FNMAP_UC.
 */

char dot_c[] = ".c";
char dot_h[] = ".h";    /* requires option +fdh for this to be appended */
#if os_VERS
    /* Versados can't handle three character extents */
char dot_cpp[] = ".cp";
char dot_cxx[] = ".cx";
char dot_lnt[] = ".lt";
char dot_lob[] = ".lb";
#else
char dot_cpp[] = ".cpp";
char dot_cxx[] = ".cxx";
char dot_lnt[] = ".lnt";
char dot_lob[] = ".lob";
char dot_lph[] = ".lph";
char dot_vac[] = ".vac";
#endif



/*  If a file does not have an extension (no '.' in the name) a
    default extension is tried.  These will be in the order listed
    in this array unless an +ext option is given overriding this
    sequence.  If you modify this array be sure to leave the
    NULL terminator.
 */

cCHARPTR default_ext[] =
    {
#if OS_MINT
    dot_vac,
#endif
    dot_lnt,
#if OS_MINT
#if l_CPP
    dot_cpp,
    dot_cxx,
#endif
    dot_c,
#endif
    NULL
    };

/* The sys_id (System ID) is used only on the banner line */

cCHARPTR sys_id =

#if os_DOS
     "PC-lint"
#endif
#if os_DOSX && m_8086 == 3
     "PC-lint (TNT 8.02)"
#endif
#if os_DOSX && m_8086 != 3
     "PC-lint (Extended DOS)"
#endif
#if os_2
    "PC-lint (DOS-OS/2)"
#endif
#if os_2_2
    "PC-lint (OS/2 32-bit)"
#endif
#if os_NT
    "PC-lint (NT)"
#endif
#if os_AMIGA
    "Lint for the Amiga"
#endif
#if os_ATARI
    "FlexeLint (Atari)"
#endif
#if os_VMS
    "FlexeLint (VAX/VMS)"
#endif
#if os_9
    "FlexeLint (OS-9)"
#endif
#if os_XENIX
#if m_8086 == 3
    "FlexeLint (Xx386)"
#else
    "FlexeLint (Xx286)"
#endif
#endif
#if os_UNIX
#if m_8086 == 3
    "FlexeLint (Unix/386)"
#else
    "FlexeLint (Unix)"
#endif
#endif
#if os_LINUX
    "FlexeLint (Linux)"
#endif
#if os_IBM
    "FlexeLint (VM)"
#endif
#if os_MVS
    "FlexeLint (MVS)"
#endif
#if os_VERS
    "FlexeLint (VERSADOS)"
#endif
#if os_DG
    "FlexeLint (DatGen)"
#endif
#if os_MPW
    "FlexeLint (Macintosh MPW)"
#endif
#if os_THINKC
    "FlexeLint (Macintosh-ThinkC)"
#endif
#if os_QNX
    "FlexeLint for QNX"
#endif
#if os_A2GS
    "FlexeLint for Apple 2GS"
#endif
#if os_UCOS
    "FlexeLint (UCOS)"
#endif
#if os_VOS
    "FlexeLint (VOS)"
#endif
    ;

/*  Additional Option Characters:  Several of our option characters
    such as '+', '?', etc. cannot be used easily on a variety of
    systems because the commands are processed by a command interpreter
    which views some of them as having special meaning.  This means
    that if the option character is to be used from the command line
    it has to be escaped.

    It is important to note that characters below are ADDITIONAL
    characters.  The default characters are still valid
    so that the documentation is technically correct and so that
    programs bearing lint-comments can be transferred between systems.
 */

#if os_MPW | os_THINKC
#define PLUS '&'
#define QUOTE '\''
#endif

#if OS_MINT
#define COMMA '!'   /* an alternate separator character that can be */
                    /* passed into batch scripts */
#endif

#if os_UNIX | os_LINUX | os_UCOS
#define QUEST '.'
#define LPAREN '['      /* allow constructs such as +ppw[word] */
#define RPAREN ']'
#else
#define LPAREN '['
#define RPAREN ']'
#endif

#ifndef PLUS
#define PLUS '+'        /* default value */
#endif
#ifndef QUOTE
#define QUOTE '"'       /* default value */
#endif
#ifndef QUEST
#define QUEST '?'       /* default value */
#endif
#ifndef LPAREN
#define LPAREN '('       /* default value */
#endif
#ifndef RPAREN
#define RPAREN ')'       /* default value */
#endif
#ifndef COMMA
#define COMMA ','
#endif
#ifndef STAR
#define STAR '*'
#endif

char aoc_plus = PLUS;       /* used to introduce options as in +fmd */
char aoc_quest = QUEST;     /* used in error inhibition as in -e7?? */
char aoc_quote = QUOTE;     /* used to quote blank-embedded names as in
                               lint "hard disk:C programs:myprog.c"  */
char aoc_lp = LPAREN;       /* '(' used in -option(list) */
char aoc_rp = RPAREN;       /* ')' used in -option(list) */
char aoc_cm = COMMA;
char aoc_star = STAR;       /* an additional wild card character for '*' */

/*  sys_olist (System options list) can be used to pre-request options.
    All options should appear blank-separated and are pointed to by
    the character pointer sys_olist.  These options take effect before
    all others.
 */

#if OS_MINT
#define OPTIONS "+fhg"      /* use IBM PC graphics characters */
#endif

#if os_9
#define OPTIONS "+fdi"   /* use directory of including file */
#endif

#if os_UNIX | os_LINUX | os_UCOS
#define OPTIONS "-split +ppw(ident) +fdi"  /* #ident and dir. of incl'g file */
#endif

#if os_MPW | os_THINKC
    /* the 2nd option below allows binary constants */
#define OPTIONS "-split +fbc"
#endif

#if os_QNX
#define OPTIONS "-split +fdi"
#endif

#if os_AMIGA
#define OPTIONS "-width(76,4)"
#endif

#if os_DG
#define OPTIONS "+ppw(assert)"  /* enable #assert */
#endif

#if os_DOSX
#define OPTIONS "+fhg"  /* previously included -si2 -spN2 -spF4 */
#endif

/*  The following causes #dictionary to invoke i_open() with
    a special delimeter (#) but special treatment is up to the user.
 */
#if os_VMS
#define OPTIONS "+ppw(dictionary)"
#endif

#ifndef OPTIONS
#define OPTIONS ""
#endif

#if !os_QNX
cCHARPTR sys_olist = OPTIONS;
#endif

/*  This sequence of strings is used to output the set of options
    in response to the user providing no options on the command line
    Special escapes are:
        .c     ==  Center this line
        .C     ==  Center line and start a new section.
                   the 'previous' and 'next' commands will tend to hunt
                   for the the start of a new section.
        .n<N>  ==  Need <N> lines (call scr_need(<N>) below.)
 */

cCHARPTR sys_odesc[] =
    {
    "",
    ".n8",
    ".C _________________________ MESSAGE GROUP _________________________",
    "",
    ".C ----- Error Inhibition Options -----",
    ".c (- inhibits and + enables error messages)",
    ".c (# and Symbol may include wild-card characters '?' and '*')",
    "",
    "-e#   Inhibit message number #",
    "!e#   Inhibit message # this line",
    "-e(#) Inhibit for next expression",
    "--e(#) For entire current expression",
    "-e{#} Inhibit for next {region}",
    "--e{#} For entire current {region}",
    "-eai  Args differ sub-integer",
    "-ean  Args differ nominally",
    "-eas  Args same size",
    "-eau  Args differ signed-unsigned",
    "-ecall(#,<Func>) By number, fnc call",
    "-efile(#,<File>) By number, file" ,
    "-efunc(#,<Func>) By number, function",
    "+efreeze  disable Message inhibition",
    "+efreeze(w<lvl>)  Freeze for <lvl>",
    "++efreeze[(w<lvl>)]  Deep-freeze <lvl>",
    "-elib(#)  Within library headers",
    "-elibcall(#) Calls to library fnctns",
    "-elibmacro(#) For all library macros",
    "-elibsym(#) For all library symbols",
    "-emacro(#,Symbol) Within macro",
    "-emacro((#),Symbol) Within expr macro",
    "--emacro((#),Symbol) Within expr macro",
    "-emacro({#},Symbol) Next stmt macro",
    "--emacro({#},Symbol) Within stmt macro",
    "-epn  Pointers to nominal",
    "-epnc Pointers to nominal chars",
    "-epp  Pointers are pointers",
    "-eps  Pointers to same size",
    "-epu  Pointers to signed-unsigned",
    "-epuc Pointers to sgnd-unsgnd chars",
    "-estring(#,String)  By number, string",
    "-esym(#,Symbol)  By number, symbol",
    "+esym(#,Symbol) Enable by no. symbol",
    "-etd(<TypeDiff>) Ignore type diff.",
    "-etemplate(#) In template expansion",
    "-etype(#,<TypeName>) By number, type",
    "-limit(n) Limits number of messages",
    "++limit(n) Locks in limit of n",
    "-save Saves error inhibitions",
    "-restore Resets error inhibitions",
    "-restore_at_end Restores at module end",
    "-w<lvl> Set warning level (0,1,2,3,4)",
    "-wlib(<lvl>) Library warning level",
    "-zero Sets exit code to 0",
    "-zero(#) like -zero unless msg no. < #",
    "",
    ".n4",
    ".C ------ Verbosity Options ------",
    "",
    ".c Format: -/+v[aceh-iostw#]{mf<int>*}",
    "     -v...  Output to stdout only",
    "     -v     Turn off verbosity (note absence of option letters)",
    "     +v...  Output to stderr and also to stdout",
    "     +v     Don't change options but output to stderr and stdout",
    "Zero or more of:",
    " a  Attempts to open",
    " c  Unique Calls",
    " e  Function templates",
    " h  Dump strong type hierarchy",
    " h- Compressed form of h",
    " i  Indirect files",
    " o  Display options",
    " s  Storage consumed",
    " t  Template expansions",
    " w  Specific Walks",
    " #  Append file ID nos.",
    "One of:                                         ",
    " m  Module names (the default)",
    " f  Header Files (implies m)",
    " <int>  Every <int> lines (implies f) ",
    " *  All verbosity",
    "",
    ".n4",
    ".C ----- Message Presentation Options -----",
    "",
    "-h[abefFrsSm/<M>/<I>]<ht>  message height (default = -ha_3)",
    "   a  Position indicator Above line",
    "   b  Indicator Below line",
    "   f  Frequent file information",
    "   F  Always produce file info",
    "   e  Place source line @ End of msg",
    "   r  Repeat source line each msg",
    "   s  Space after each non-wrapup msg",
    "   S  space after each msg",
    "   m/<M>/ Macro indicator is <M>",
    "   mn Turn off macro indication",
    "   <I> The position Indicator",
    "   <ht> Height of messages",
    "-width(<Width>,<Indent>)   Message width (default = -width(79,4))",
    "-append(errno,msg)     Appends msg for message errno",
    "-format=...            Specifies message format",
    "-format4a=,  -format4b=    Specifies format if msg. ht. = 4",
    "-format_specific=...   Prologue to specific Walk messages",
    "-format_stack=...      Format for output of stack information",
    "-format_template=...   Format for prologue to template instantiation",
    "-format_verbosity=...  Format for verbosity under +html",
    "    format codes (%...)                            format escapes (\\...)",
    "    %c  column no.           %l  Line no.          \\t   Tab",
    "    %C  Column no. + 1       %m  Msg text          \\s   Space",
    "    %f  Filename             %n  msg Number        \\a   Alarm",
    "    %i  FunctIon name        %t  msg Type          \\q   Quote",
    "    %(...%) Use ... if '%f' or '%i' are non null   \\\\   Backslash",
    "                                                   \\n   Newline",
    "+source(suboptions)     Echos entire source files(s); suboptions:",
    "  -number     Do not number lines",
    "  -indent     Do not indent hdr lines",
    "  -m(files)   Ignore given modules",
    "  +h(hdrs)    Echo given hdrs",
    "  -h(hdrs)    Do not echo hdrs",
    "  +dir(dirs)  Echo hdrs from these",
    "  -dir(dirs)  Do not echo hdrs from",
    "  +class(all) Echo all hdrs",
    "  +class(project) Echo project hdrs",
    "+html(options)    Output in html format (example in env-html.lnt)",
    "    version(...) Can be used to specify the version of html",
    "    head(file)   Includes file just after <html>",
    "+xml([name])     Activate escapes for xml  (example in env-xml.lnt)",
    "    If name is provided, output appears within <name> ... </name>",
    "-message(String)       Output String as an informational message",
    "-summary([out-file])   Issues or appends a summary of error messages",
    "-t#                    Sets tab size to #   ",
    "+typename(#)           Includes types of Symbols in message #",
    "+xml(name)             Format output messages in xml",
    ".c .... message presentation flags ....",
#if FNMAP_UC | FNMAP_LC
    "fff  Fold Filenames to one case %s",
#endif
    "ffn  use Full file Names %s",
    "ffo  Flush Output each msg %s",
    "flm  Lock Message format %s",
    "frl  Reference Location info %s",
    "fsn  treat Strings as Names %s",
    "",
    ".n8",
    ".C ___________________________ DATA GROUP ___________________________",
    "",
    ".n4",
    ".C --- Scalar Data Size and Alignment Options (default value(s)) ---",
    "",
    "-sb#   bits in a byte (%s)",
    "-sbo#  sizeof(bool) (%s)",
    "-sc#   sizeof(char) (%s)",
    "-slc#  sizeof(long char) (%s)",
    "-ss#   sizeof(short) (%s)",
    "-si#   sizeof(int) (%s)",
    "-sl#   sizeof(long) (%s)",
    "-sll#  sizeof(long long) (%s)",
    "-sf#   sizeof(float) (%s)",
    "-sd#   sizeof(double) (%s)",
    "-sld#  sizeof(long double) (%s)",
#if l_CPP
    "-smp#  size of all member ptrs (%s)",
    "-smpD# size of mem ptr (data) (%s)",
#if m_8086
    "-smpFP# size, mem ptr (Far Prog) (%s)",
    "-smpNP# size, mem ptr (Near Prog) (%s)",
#endif
    "-smpP# size of mem ptrs (prog) (%s)",
#endif
    "-sp#   sizeof(all pointers) (%s)",
    "-spD#  size of both data ptrs (%s)",
#if m_8086
    "-spF#  size of both far ptrs (%s)",
    "-spFD# size of far data pointer (%s)",
    "-spFP# size of far prog pointer (%s)",
    "-spN#  size of both near ptrs (%s)",
    "-spND# size of near data pointer (%s)",
    "-spNP# size of near prog pointer (%s)",
#endif
    "-spP#  size of both program ptrs (%s)",
    "-sw#   size of wide char (%s)",
    "",
    "-a<code># Specifies alignment, <code> is any code used above in -s<code>#",
    "   # = 1, no alignment; # = 2, 2-byte boundary; etc.  By default, a type's ",
    "   alignment is the largest power of 2 that divides the size of the type",
    " ",
    "-align_max(n)    Set the maximum alignment to n",
    "-align_max(push) Saves the current maximum alignment",
    "-align_max(pop)  Restores previously pushed maximum alignment",
    "",
    ".c .... scalar data flags ....",
    "fba  Bit Addressability %s",
    "fbc  Binary Constants 0b... %s",
    "fbo  Activate bool, true, false %s",
    "fcu  char-is-unsigned %s",
    "fdc  (C++) Distinguish plain Char %s",
    "fdl  pointer-diff.-is-long %s",
    "fis  Integral consts. are Signed %s",
    "flc  allow long char %s",
    "fll  allow long long int %s",
    "fmd  multiple definitons %s",
    "fpd  Pointers Differ in size %s",
    "fsc  strings are const char * %s",
    "fsu  string unsigned %s",
    "fwc  internal wchar_t for C %s",
    "fwm  MS wprintf conventions %s",
    "fwu  wchar_t is unsigned %s",
    "",
    ".n5",
    ".C ------- Data Modifier Options -------",
    "",
#if m_8086
    "-mS  Small model",
    "-mD  large Data model",
    "-mP  large Program model",
    "-mL  Large program and data",
#endif
    ".c .... data modifier flags ....",
    "f@m  @ is a modifier %s",
    "fat  Parse .net ATtributes %s",
#if OS_MINT
    "fcd  cdecl is significant %s",
#endif
    "fem  allow Early Modifiers %s",
    "fiq  Ignore default Qualifier %s",
    "fqb  Qualifiers Before types %s",
    "",
    ".n5",
    ".C ------ struct, union, class, enum, namespace Flags ------",
    "",
    "fab  ABbreviated structures %s",
    "fan  ANonymous unions %s",
    "fas  Anonymous Structures %s",
    "fbu  force Bit fields Unsigned %s",
    "fct  Create Tags %s",
    "feb  enum's can be Bitfields %s",
    "fie  Integer-model-for-Enums %s",
    "fld  Label Designator %s",
    "fns  Nested Struct %s",
    "fnt  (C++) Nested Tags %s",
    "fsg  std is Global %s",
    "fss  regard Sub-Struct as base %s",
    "fus  Using namespace std %s",
    "fvl  Variable Length arrays %s",
    "",
    ".n8",
    ".C ________________________ PROCESSING GROUP ________________________",
    "",
    ".n5",
    ".C ------- Preprocessor Options -------",
    "",
    "-d<name>[=<value>]     Defines preprocessor symbol",
    "-d<name>{definition}   For use with -scavenge",
    "-D<nm>[=<val>][;<nm>[=<val>]]... Define set of symbols",
    "+d... or +D...         Same as -d or -D except it locks in a definition",
    "-d<name>()[=<value>]   Define function-like macro ",
    "-#d<name>[=<value>]    Defines symbol (for #include only)",
    "-header(file)          Auto-includes file in each module",
    "-i<directory>          Search directory for #include",
    "-incvar(name)          Change name of INCLUDE environment variable",
    "-/+macros              Halve/double the maximum macro size",
    "-pch(hdr)              Designates hdr as the pre-compiled header",
    "-ppw(word[,...])       Disables(-) or enables(+) pre-processor words",
    "+ppw(word[,...])            eg: +ppw(ident) enables #ident",
    "--ppw(word[,...])      Removes built-in meaning of word",
    "-ppw_asgn(w1,w2)       Assigns pre-proc meaning of w2 to w1",
    "+pragma(name,action)   Associates action with name; action is one of",
    "     off               Turns processing off (as with assembly code)",
    "     on                Turns processing back on",
    "     once              Physically include this file just once",
    "     message           Issue a message",
    "     macro             pragma_name becomes a macro",
    "     fmacro            pragma_name becomes a function-like macro",
    "     options           pragma_name_suboption becomes a function macro",
    "     ppw               The pragma becomes a preprocessor command",
    "     push_macro        push_macro(\"nm\") saves the current definition of nm",
    "     pop_macro         pop_macro(\"nm\") restores a pushed definition ",
    "-pragma(name)          Disables pragma        ",
    "-u<name>               Undefines <name>",
    "--u<name>              Ignore past and future defines of <name>",
    ".c .... preprocessor flags ....",
    "fce  continue-on-error %s",
    "fep  Extended Preprocessor exps. %s",
    "fim  -i can have multiple dirs. %s",
    "fln  activate #line %s",
    "fps  Parameters within Strings %s",
    "",
    ".n5",
    ".C --------- Tokenizing Options ---------",
    ".c (see also Compiler Adaptation Keywords)",
    "",
    "-$                     Permits $ in identifiers",
    "-ident(<chars>)        Add identifier characters",
    "-ident1(<char>)        Define a 1-char identifier",
    "+linebuf               Doubles size of line buffer",
    "-rw(word[,...])        Disables(-) or enables(+) reserved words ...",
    "+rw(word[,...])        word = '*ms' implies all MS keywords",
    "--rw(word[,...])       Removes built-in meaning of word",
    "-rw_asgn(w1,w2)        Assigns reserved word meaning of w2 to w1",
    ".c .... flags affecting tokenization ....",
    "fnc  nested comments %s",
    "ftg  permit Tri Graphs %s",
    "",
    ".n5",
    ".C --------- Parsing Options ---------",
    "",
    "-fallthrough           A switch case allowing flow from above",
    "-unreachable           A point in a program is unreachable",
    ".c .... parsing flags ....",
    "ffb  for clause creates Block %s",
#if l_CPP
    "flf  (C++) process lib func defs %s",
#endif
    "fna  (C++) allow 'operator new[]' %s",
    "fpc  Pointer Casts retain lvalue%s",
    "fpm  Precision is Max of args %s",
    "",
    ".n5",
    ".C -------- Template Options --------",
    "",
    "-tr_limit(n)           Sets a template recursion limit",
    "-template(X)           Hex constant X sets esoteric template flags",
    ".c .... template flags ....",
    "ftf  raw template functions %s",
    "",
    ".n4",
    ".C ---- Compiler-adaptation Options ----",
    "",
    "-A                     Specifies strict ANSI",
    "-A(Cyear)              Specifies the year of the assumed C standard",
    "-A(C++year)            Specifies the year of the assumed C++ standard",
    "-c<code>               Identifies the compiler",
    "-a#<predicate>(tokens) Assert the truth of #<predicate> for tokens (Unix)",
    "+/-compiler(flag[,...]) Sets/resets flag;   default value shown by (ON/OFF)",
    "   std_digraphs        (OFF) Enables '<:' and ':>' with standard meaning",
    "   base_op             (OFF) Enables digraph ':>' with ancient meaning",
    "   std_alt_keywords    (OFF) Enables keywords: and, and_eq, bitand, bitor,",
    "                             compl, not, not_eq, or, or_eq, xor, xor_eq",
    "-overload(X)           Hex constant X sets esoteric overload resolution flags",
    "-plus(Char)            Identifies Char as an alternate option char equiv to +",
    "-scavenge(filename-pat[,...])              ",
    "                       Turns lint into a scavenger of macro names within",
    "                       files matching filename-patterns",
    "-scavenge(clean,file)  Used subsequently to clean up file which bears",
    "                       the results of compiler macro replacement",
    "-template(X)           Hex constant X sets esoteric template flags",
    ".c .... compiler-adaptation reserved words (keywords) ....",
    "@                      Ignore expression to the right",
    "__assert               'Ideal' assert function      ",
    "___assert              Like _assert but it always returns",
    "_bit                   1 bit wide type          ",
    "_gobble                Ignore next token    ",
    "_ignore_init           Ignore initializer for data and ...",
    "                       ignore function body for functions",
    "_to_brackets           Ignore next parenthesized (or bracketed) expression ",
    "_to_semi               Ignore until ;      ",
    "_to_eol                Ignore until end-of-line   ",
    "_up_to_brackets        Ignore up to and including a bracketed expression ",
    "__packed               struct data members are packed",
    "__typeof__(e)          Like sizeof but returns the type of e",
    "",
    ".n5",
    ".C ----------- Old C Flags -----------",
    "",
    "fdr  deduce-return-mode %s",
    "ffd  promote floats to double %s",
    "fkp  K&R preprocessor %s",
    "fmc  macro concatenation %s",
    "fsa  structure-assignment %s",
    "ful  unsigned long %s",
    "fva  variable arguments %s",
    "fvo  void data type %s",
    "fvr  varying-return-mode %s",
    "fxa  eXact array arg. %s",
    "fxc  eXact char arg. %s",
    "fxf  eXact float arg. %s",
    "fxs  eXact short arg. %s",
    "fzl  sizeof-is-long %s",
    "fzu  sizeof-is-unsigned %s",
    "",
    ".n8",
    ".C ____________________ SPECIAL DETECTION GROUP ____________________",
    "",
    ".n5",
    ".C ------- Strong Type Options -------",
    "",
    "-strong(Flags,Type(s)) Check strong types ..., Flags are:",
    "    A[irpacz] == on Asgn to (except Init, Ret, Param, Asgn op, Consts, Zero);",
    "    J[erocz] == on Joining (except Eqlty, Rel, Other ops, Constants, Zero);",
    "    X == on eXtraction;   l == allow library;    B[f] or b[f] == strong and",
    "    weak Boolean (f == length-1 bit fields are NOT Boolean)",
    "-index(flags,ixtype,type(s)) Establish ixtype as index type",
    "    flags: c == allow Constants, d == allow Dimensions",
    "-parent(Parent,Children) Augment strong type hierarchy",
    "-father(Parent,Children) A stricter verson of parent",
    ".c .... strong type flags ....",
    "fhd  hierarchy down warning %s",
    "fhg  hierarchy uses graph. chars %s",
    "fhs  hierarchy of strong types %s",
    "fhx  Hierarchy of indeX types %s",
    "",
    ".n5",
    ".C -------- Semantic Options --------",
    "",
    "-function(f0,f1, ...)  Assign semantics of f0 to f1, ...",
    "-printf(#,f1, ...)     f1, ... are printf-like, # is arg. no. of format",
    "-scanf(#,f1, ...)      f1, ... are scanf-like, # is arg. no. of format",
    "-printf_code(Code[,Type]) Allows user-defined printf codes",
    "-scanf_code(Code[,Type])  Allows user-defined scanf codes",
    "-sem(fnc,sem1, ...)   Associates a set of semantics with a function",
    "-wprintf(#,f1, ...)    Wide char version of -printf",
    "-wscanf(#,f1, ...)     Wide char version of -scanf",
    " ",
    ".c .... semantics (i.e. arguments to the -sem option) ....",
    "4 kinds of semantics: 0 == overall, a == argument, r == return, f == flag",
    "kind:  semantic:       meaning:",
    " r     r_null          function may return NULL",
    " r     r_no            function does not return",
    " f     initializer     function initializes all data members",
    " f     cleanup         function that clears all pointer data members",
    " a     #p              #th argument must not be NULL",
    " a     custodial(#)    #th argument takes custody",
    " a     type(#)         #th argument is reflected into the return type",
    " a     pod(#)          #th argument must be POD",
    " a     nulterm(#)      #th argument is nul-terminated",
    " a     inout(#)        #th argument is read and written",
    " f     pure            the function is a pure function",
    " r     expression (see 'expression components' below) containing the @",
    "       operator is assumed true with @ representing the return value",
    " 0     any other expression is considered a function requirement",
    "",
    ".c .... expression components: .... ",
    "Parentheses: ()        Unary operators: + - | ~ ",
    "Binary operators: + - * / % < <= == != > >= | & ^ << >> || && ",
    "Ternary operator:  ?:                  ",
    "#n = integer value of #th arg",
    "#p = item count of #th arg",
    "#P = byte count of #th arg",
    "integer = itself",
    "name of macro, enum, const = current value of same",
    "malloc(exp) = malloc'ed area of size exp",
    "new(exp) = new'ed area of size exp",
    "new[](exp) = new[]ed area of size exp",
    "",
    ".c .... thread semantics ....",
    "thread = the function is a thread",
    "thread_mono = a single-instance thread",
    "no_thread = function is not a thread",
    "thread_lock = function locks a mutex",
    "thread_unlock = ftn unlocks a mutex",
    "thread_create(#) = #th argument is a thread",
    "thread_unsafe = unsafe to call by multiple threads",
    "thread_unsafe(groupid) = group can't be called by multiple threads",
    "thread_safe = not thread_unsafe",
    "thread_protected = function is protected by critical section",
    "thread_not(list) = lists threads that may not invoke function",
    "thread_only(list) = lists the only threads that may invoke function",
    "",
    ".c .... semantic flags ....",
    "ffc  Function takes Custody %s",
    "",
    ".n5",
    ".C ----- Thread Options -----",
    ".c (see also thread semantics)",
    "prefix '+' designates a property and '-' reverses it",
    "+/-thread_unsafe_h(hdr[,...])    ftns in headers are thread_unsafe",
    "+/-thread_unsafe_group_h(hdr[,...]) ftns in hdrs form a thread_unsafe group",
    "+/-thread_unsafe_dir(dir[,...]) hdrs in dirs house thread_unsafe ftns",
    "+/-thread_unsafe_group_dir(dir[,...]) hdrs in dirs form a thread_unsafe group",
    "",
    ".n5",
    ".C ----- Value Tracking Options -----",
    "",
    "-passes(k[,Opt1[,Opt2]]) Requests k passes",
    "-specific(op1[,op2])   Options op1 before and op2 after every specific walk",
    "-specific_climit(n)    Per function Limit on the no. of recorded calls",
    "-specific_wlimit(n)    Walk limit on the number of recursively generated calls",
    "-specific_retry(n)     n== 0 implies inhibiting rewalking with same parameters",
    "-static_depth(n)       Adjusts the depth of static variable analysis",
    ".c .... value tracking flags ....",
    "fai  pointed-to Arg is Initialized%s",
    "fii  Inhibit Inference %s",
    "fiw  Initialization is a Write %s",
    "fiz  Init'n by Zero is a Write %s",
    "fnn  (C++) new can return NULL %s",
    "fnr  Null ptr may be Returned %s",
    "fpn  pointer param may be NULL %s",
    "fsp  SPecific function calls %s",
    "fsv  track Static Variables %s",
    "",
    ".n5",
    ".C ---- Miscellaneous Detection Options ----",
    "",
    "-deprecate(category,name,commentary) Deprecates use of a name",
    "                       categories:  function, keyword, macro, variable ",
    "+headerwarn(file)      Causes Msg 829 to be issued for a given file",
    "-idlen(<n>[,opt])      Report identifier clashes in <n> chars",
    "                       opt: x=external, p=preprocessor, c=compiler",
    "-size(flags,amount)    Report large aggregates; flags: a auto, s static",
    ".c .... special detection flags ....",
    "fet  requires Explicit Throws %s",
    "fil  Indentation check of Labels %s",
    "",
    ".n8",
#if 0
    ".C - Note on Flag Options -",
    " general form: 'fxx' where xx designates exactly 2 characters ",
    " a description of a flag contains its current value (ON/OFF)",
    " +fxx  sets flag xx",
    " -fxx  resets flag xx",
    "++fxx  increments flag xx",
    "--fxx  decrements flag xx",
    "",
    ".n8",
#endif
    ".C __________________________ MISC. GROUP __________________________",
    "",
    ".n5",
    ".C ---------- Global Options ----------",
    "",
#if os_NT
    "-background            Reduces task priority",
#endif
    "-/+/++b                No/Redirect/Produce Banner line",
    "-p[(n)]                Just preprocess, n == max output width",
    "-setenv(name=val)      Sets an environment variable",
    "-u                     Unit checkout",
    "",
    ".c .... global flags ....",
    "fpa  PAuse before exiting %s",
    "",
    ".n5",
    ".C ----------- File Options -----------",
    "",
    "+bypclass([all,angle,ansi,foreign]...) Default bypass headers",
    "-/+bypdir(directory[,...])  Deny or specify bypass directory",
    "-/+byph(header[,...])  Deny or specify bypass header by name",
    "-/+cpp(extension)      Remove/add .ext for C++ files",
    "+ext(ext[,ext]...)     Extensions attempted for extensionless files",
#if OS_MINT
    #if l_CPP
    "                       defaults to +ext(vac,lnt,cpp,cxx,c)",
    #else
    "                       defaults to +ext(vac,lnt,c)",
    #endif
#else
    #if l_CPP
    "                       defaults to +ext(lnt,cpp,cxx,c)",
    #else
    "                       defaults to +ext(lnt,c)",
    #endif
#endif
    "-indirect(file)        Process indirect (.lnt) file",
    "+libclass([all,angle,ansi,foreign]...) Default library headers",
    "-/+libdir(directory[,...])  Deny or specify library directory",
    "-/+libh(header[,...])  Deny or specify library header by name",
    "-/+libm(module[,...])  Deny or specify library module by name",
    "-library               Sets library flag",
    "+lnt(ext)              File.ext is treated like File.lnt",
    "-maxfiles(<n>)         Sets an upper limit on the number of files",
    "-maxopen(<n>)          Assumed number of openable files",
    "-pch(hdr)              Designates hdr as the pre-compiled header",
    "-subfile( indirect-file, options|modules )  Process just 'options'",
    "                       or just 'modules' from indirect-file",
    "--u                    -u and ignore modules at lower .lnt level",
    ".c .... file flags ....",
    "fcp  (C++) Force C++ processing %s",
    "fda  dbl-qts to Angle brackets %s",
    "fdh  append '.h' to header names %s",
    "fdi  Directory of Including file%s",
    "flb  library %s",
    "frb  Files fopen'ed with \"rb\" %s",
    "frn  Treat CR as new-line %s",
#if OS_MINT
    "fsh  shared file open %s",
#endif
    "ftr  truncate filenames to 8x3 %s",
    "",
    ".n5",
    ".C --------- Output Options ---------",
    "",
    "-od[options](filename) Output declarations of defined external",
    "    objects and functions including prototypes,",
    "    options: f=only functions, i=internal functions, s=structs",
    "             <width>=specify break width",
    "-oe(filename)          Redirect to standard Error (+oe appends)",
    "-ol(filename)          Output library file   ",
    "-oo[(filename)]        Output to lint object file",
    "  -lobbase(filename)   Establish a lob base file",
    "-os(filename)          Redirect to standard out (+os appends)",
    "+program_info(output_prefix=<path>, suboptions ) Dumps information into",
    "    <path>file.txt     information about files",
    "    <path>symbol.txt   information about symbols",
    "    <path>type.txt     information about types",
    "    <path>macro.txt    information about macros",
    "+stack(sub-options)    Issue report on function stack usage",
    "    &file=filename     Designates name of file to receive stack report",
    "    &overhead(n)       Sets the overhead of each function call",
    "    &external(n)       Sets assumed stack usage by each external function",
    "    name(n)            Sets stack usage (n) explicitly for named function",
    "    &summary           Requests just a summary of stack info report.",
    ".c .... output option flags ....",
    "fod  Output Declared to object %s",
    "fol  Output Library to object %s",
    "",
    "-------------------------- END OF OPTIONS -----------------------------",
    NULL
    };


/*  sys_vers (System Version) is used on the banner line
    and is picked up during patching.  Important:
    the format of sys_vers must conform to that specified within the
    function patch_level():
        n.0Bc           for Betas (of an entire version)
        n.00c           for normal versions
        n.00cd          (where d is a digit) for patch Betas
    Note: c may be a two alphabetic character sequence.
 */

/*lint -esym(714,sys_sentinel)  not referenced by mortals. */

cCHARPTR sys_sentinel = "{Version}",        /* patch sentinel */
         sys_vers = "Vers. 9.00f";         /* a pointer to version info */

#if os_AMIGA
#define SCR_HT 23
#define SCR_WTH 76
#endif

#if os_MPW
#define SCR_WTH 100
#endif

#ifndef SCR_HT
#define SCR_HT 24   /* default screen height */
#endif

#ifndef SCR_WTH
#define SCR_WTH 79  /* default screen width */
#endif

int scr_ht =  SCR_HT;   /* The number of lines on the screen */
int scr_wth = SCR_WTH;  /* The number of characters on a line of the screen */
int scr_n = 0;      /* The number of lines that have been written */


/*  Part II: Customizable Functions (in alphabetic order) */

/*  forward references */

#if FNMAP_MIXED
static Void sys_fnfold();   /* copy and folds file names to upper case */
#endif
static Void scr_param();

/*  i_open( name, delimeter, ordinal, prefix, parent, line )
    is called to open a file for reading.  It is so named because
    originally the open was used only for include files.  Now it
    is used for all opens, modules and indirect files as well as
    headers.  If it bothers you that a directory search is used
    to open modules, that can be easily fixed by modifying this file.
    So far it has proven to be quite beneficial.

    name is the name that was extracted from between either
    quote delimeters or angle bracket delimeters or was found
    on the (extended) command line.  The delimeter is, accordingly,
    either '"' or '<' or 0.  As a special case, if the #dictionary
    preprocessor command is issued, the delimeter is '#'.  This
    is configured automatically for VAX/VMS.

    When a file is to be opened, i_open is at first called with

        ordinal == 1
        prefix == <ptr to buffer initially containing the null string>
        parent == <name of an actively #include'ing file if any>

    i_open will return the FILE * associated with the opened file.
    If parent is non-null, the directory name of the parent will be
    placed into the location pointed to by prefix.

    If i_open returns NULL, indicating failure to find the file,
    and if the Lint option +compiler(search_actively_including_stack)
    (needed to support Microsoft's compiler strategy),
    lint will make repeated calls to i_open with the same arguments
    but where parent is the parent #including the prior parent.
    This goes on until the current including stack is exhausted;

    if i_open returns NULL in each of those instances then
    i_open is called again with

        ordinal == 2
        prefix == the first -i parameter (specifying an include directory).

    This is repeated for each -i option increasing ordinal each time until
    i_open returns a non NULL or the -i list is exhausted.  If the -i list
    is exhausted and there is still no success, i_open is called one last
    time with ordinal == 0 and prefix == NULL.  If NULL is returned this
    time, error No. 7 is reported.

    If the name of the file opened is not simply the concatenation
    of the name with the prefix then name should be overwritten
    (name is a pointer to a buffer of MAXFNM length).

    parent is the name of the current file being processed. i.e. the
    name of the including file.  It is NULL for modules and indirect
    files.  line is the current include line, if any.

    As an exceptional case, if the include directive contains neither
    quotes nor angle brackets, the information following the include
    is passed as the name, the delimeter is 0, and the parent is nonNULL.

    This argument list has been made deliberately more general
    than was required in order to handle the diverse requirements
    of a variety of systems.

    Lint will close files if the include file depth becomes larger than
    MAX_OPEN.  Subsequently, i_open is used to reopen the files, with the
    ordinal = -1, and line is either null or if the name had previously been
    overwritten will point to the overwritten name).  The rest of
    the arguments are identical to the ones used when the file was
    opened originally. The prefix is never NULL when reopening the file.
 */

/*lint +fvr */
CHARPTR strcpy();
CHARPTR strcat();
/*lint -fvr */
int sys_env_var();     /* forward reference */

FILE * i_fopen( name, mode )
    cCHARPTR name;
    cCHARPTR mode;
    {
    if( !attempt_open( name, mode ) ) return NULL;
#if OS_MINT
    if( gfl_sh > 0 ) return shopen( name, mode );
#endif
    return fopen( name, mode );
    }

/*lint -e{715}  unused paramters are OK in this function */

FILE * i_open( name, delimeter, ordinal, prefix, parent, line )
    cCHARPTR name;
    int delimeter;
    int ordinal;
    CHARPTR prefix;
    cCHARPTR parent;
    cCHARPTR line;
    {
    char buffer[ARG_LEN]; /* max file name length set in custom.h */
    FILE *f = NULL;
    cCHARPTR mode = "r";  /* 2nd argument to fopen() */
    CHARPTR p;


    /* if the +frb flag is set then "rb" will be the open mode if available */
    if( gfl_rb > 0 ) mode = MD_RD_B;
    if( delimeter == 0 && parent )
        {
        /* do not accept delimeter == 0 unless this is a module
           in which case parent == NULL  */
        if( ordinal == 1 )
            {
            Errorno(12);
            /* This will be issued only once (even though it's possible to
               come here more than once with ordinal==1 for the same
               file search in the case of the "actively-including-stack"
               search). */
            }
        /* Note the above error is in addition to error 7 */
        return NULL;
        }
    switch(ordinal)
        {
    case 1:     /* Either:
                    - it's the first time through or
                    - we're doing an "actively-including-stack" search.
                      */
        if( delimeter == '<' ) return NULL;  /* try directories first */
        if( gfl_di > 0 && parent )  /* use directory of including file? */
            {
            strcpy( buffer, parent );
            p = buffer + strlen(buffer);
            while( *--p != SEP1 && *p != SEP1A && *p != SEP1B && p > buffer )
                /* loop */ ;
            /* emerging: p points to SEP1 (or SEP1A or SEP1B)
               OR neither char found and p == buffer */
            if( p > buffer ) p[1] = 0;
            else buffer[0] = 0;
            if( prefix ) strcpy( prefix, buffer );
            strcat( buffer, name );
            }
        else
            strcpy( buffer, name );
        break;
    case -1:    /* attempt to reopen after a forcible close */
        if( line )   /* if a name was originally manufactured */
            {
            buffer[0] = 0;
            sg_sfcat( buffer, line, MAXFNM );
            break;
            }
        /*lint -fallthrough otherwise fall through */
    default:    /* the middle case, or the restore case, use prefix if there */
        strcpy( buffer, prefix ? prefix : "" );
#if os_MPW
        (void) sys_env_var( buffer );
#endif
        strcat( buffer, name );
#if os_MVS
            {
            int level = 0;
            for( p = buffer; *p; p++ )
                {
                if( *p == '(' ) level++;
                else if( *p == ')' ) level--;
                }
            if( level > 0 )  /* must be a partitioned data set */
                {
                /* remove .H */
                p = buffer + strlen( buffer ) - 2;
                if( strcmp( p, ".H" ) == 0 ) *p = 0;
                strcat( buffer, ")'" );
                }
            }
#endif
        break;
    case 0:  /* the case of last resort -- give plain name a shot */
        strcpy( buffer, name );
        break;
        }
    /* if name ends in ".lob" use special binary mode */
    if( last_ext( name, ext_TEST, dot_lob ) ||
        last_ext( name, ext_TEST, dot_lph ) )
        {
        mode = MD_RD_B;
        f = i_fopen( buffer, mode );
#if OS_MINT | os_QNX | os_MPW
        /*  this establishes a buffer of 2048 bytes for binary
            input files (the only kind of file for which I/O
            time is significant.  If your compiler supports
            the ANSI function setvbuf, by all means use it.
         */
#ifndef C_terp
        if( f && setvbuf( f, NULL, _IOFBF, 2048 ) ) f = NULL;
#endif
#endif
        }
    else f = i_fopen( buffer, mode );
    return f;
    }

/*  ind_tst(fn) is called to determine if the file-name provided
    is to be considered an indirect file.  The filename was previously
    determined not to end in dot_lnt (see above).  This is here for
    historical reasons and for completeness and generality.  A better
    approach would be to place the option "+lnt=LNT" in sys_olist.
 */

/*lint -e{715}  unused paramters are OK in this function */

    int
ind_tst(fn)
    char *fn;
    {
#if os_MVS
    int len;

    len = (int) strlen(fn);
    if( len > 5 && strcmp( fn + (len - 5), ".LNT'" ) == 0 )
        return 1;
#endif
    return 0;
    }

 int
/*  extract_file_id( idname, len, fileNm ) will look for the last name
    of a full path name and return the result (minus the extension)
    in idname.  len is the maximum length of idname.  E.g.
    fileNm = "C:\directory\subdir\alpha.cpp"
    will return "alpha" in idname provided len is at least 6.
    If len were 5 then "alph" is returned.
    The return value is 1 if an appropriate name could be found
 */
extract_file_id( idname, len, fileNm )
    CHARPTR idname;
    unsigned len;
    cCHARPTR fileNm;
    {
    cCHARPTR lastDot = NULL;   /* points to last dot if any */
    cCHARPTR lastNm = NULL;    /* points to last file in the path */
    cCHARPTR nameEnd;          /* points just after last char of fileNm */
    cCHARPTR probe;            /* used to traverse fileNm */
    char  c;                   /* temporary character variable */
    unsigned nchars;           /* number of characters to be copied back */

    lastNm = fileNm;
    for( probe = fileNm; c = *probe; ++probe )
        {
        if( c == '.' ) { lastDot = probe; }
        else if( c == SEP1 || c == SEP1A || c == SEP1B )
            {
            lastDot = NULL;
            lastNm = probe+1;
            }
        }
    nameEnd = probe;
    if( lastNm && *lastNm )
        {
        if( lastDot ) nameEnd = lastDot;
        nchars = (unsigned) (nameEnd - lastNm);
        if( nchars >= len ) nchars = len-1;
        strncpy( idname, lastNm, (size_t) nchars );
        idname[nchars] = '\0';
        return 1;
        }
    else return 0;
    }

/*  last_ext(name, action, suff ) will look for the last extent
    of a file name and either replace it (action = ext_REP) or append
    a default (action = ext_APP) if none exists or test it (action
    == ext_TEST).  The suffix must include the dot as in
            last_ext( name, ext_REP, ".bak" )
 */

    int
last_ext( name, action, suff )
    register CHARPTR  name;
    int action;
    cCHARPTR suff;
    {
    CHARPTR  last_dot = NULL;    /* points to last dot if any */
    CHARPTR  name_end;           /* points just after last char of name */
    register CHARPTR probe;      /* used to traverse name */
    char  c;                     /* temporary character variable */
    int retval = FALSE;          /* the return value */

#if os_VMS
    CHARPTR  trailer;    /* will point to a file name trailer */
       /* such trailers will be put back after the ext_TEST case */
    char  tch = 0;          /* trailer character */
    for( probe = name + strlen(name) - 1;
         probe > name && isdigit(*probe);  probe-- )
         /* loop */ ;
    if( *probe == ';' )
        {
        /* we really do have a trailer, snip if off */
        tch = *probe;  *probe = 0;  trailer = probe;
        }
#endif

    for( probe = name; c = *probe; ++probe )
        {
        if( c == '.' ) { last_dot = probe; }
        else if( c == SEP1 || c == SEP1A || c == SEP1B ) last_dot = NULL;
        }
    name_end = probe;
#if OS_MINT
    if( name_end > name && name_end[-1] == ':' ) return FALSE;
#endif
    if( action == ext_TEST )
        {
        if( last_dot && sys_fncmp( last_dot, suff) == 0 ) retval = TRUE;
        else if( !last_dot && *suff == 0 ) retval = TRUE;
#if os_VMS
        if( tch ) *trailer = tch;
#endif
        }
    else if( action == ext_APP && !last_dot )
        {
        int useit = 0;

#if os_UNIX | os_LINUX
        /*  for Unix (and maybe for others ?),
            source files without extents should not have
            .c appended to it.  But indirect files -- OK
            Also dot_h would have been requested with +fdh
         */
        if( suff == dot_lnt || suff == dot_h ) useit = 1;
#else
        useit = 1;
#endif
        if( useit )     /*lint !e774 always evaluates to true */
            { strcpy( name_end, suff ); retval = TRUE; }
        }
    else if( action == ext_REP ) strcpy( last_dot ? last_dot : name_end, suff );
#if os_VMS
    else if( tch ) *trailer = tch;
#endif
    return retval;
    }

/*  main(argc,argv) is, of course, the call made by the operating
    system to get lint started.  This gives the customizer the
    chance to process stylized options.  For example if the customer
    would want ~567 to mean turn off error 567 then this should be
    translated into -e567 before passing control to sys_main();
    Note, there is no return from sys_main(); rather,
    sys_exit() is called.
 */
    int
main(argc,argv)
    int argc;
    char **argv;
    {
    scr_param();
#if debug && 0
    test_wild_match(); return 0;
#endif
#if os_9
        {
        int i;
        for( i = 0; i < argc; i++ )
            {
            /* -? on command line implies options dump */
            if( strcmp( argv[i] , "-?" ) == 0 ) argc = 1;
            }
        }
#endif
#if os_THINKC
    /* Here we assume that it is difficult or impossible to prepare
       a command line.  If no argument exists, an arglist consisting
       of a single name is created.  This is the name of a file in which
       the user places arguments that would have otherwise been placed
       on the command line.
     */
    if( argc <= 1 )
        {
        char *av[3];    /* internally generated arglist */

        av[0] =  "";     /* command name not examined */
        av[1] = "flexe.lnt"; /* user places arguments into this file */
        av[2] = NULL;    /* terminate arglist */
        sys_main( 2, av );
        return 0;
        }
#endif
    sys_main(argc,argv);
    /* control doesn't really arrive here */
    return 0;
    }

/*  scr_need(n) requests n more lines for this screenful.
    If n more lines do not exist then a 'new page' is required.
 */
    Void
scr_need(n)
    int n;
    {
    if( n > (scr_ht - scr_n) )      /* if not n lines left */
        scr_n = scr_ht;             /* force new line next scr_out */
    }

/*  scr_out(s,nlflag) is called to output help information.
    If nlflag is 1 a new line is added.  The value returned is:
    0   == the line was written normally.  Otherwise the line was not written
    -1  == the user requested printing of the previous screen
    +1  == the user requested printing of the next screen.
    The logic of filling lines is done by the caller.
    The logic of when to stop filling the screen is done here.
    The logic of how far to back up and how far to go forward is based
    in part on the appearance of subheadings (.c lines) within the text.
    See also scr_ht and scr_wth above.
 */
    int
scr_out(s, nlflag)
    cCHARPTR s;
    int nlflag;
    {
    if( scr_n >= scr_ht && std_err == stderr )
        {
        scr_n = 1;
#if !os_MPW && !os_THINKC
            {
            char buf[120];
            unsigned char ch;

            fprintf( std_err, "------> quit (q), previous (p), or next [n] " );
            (Void) fgets( buf, 119, stdin );
            scr_n = 0;
            ch = (unsigned char) tolower( buf[0] & 0xFF );
            if( ch == 'p' ) return -1;
            else if( ch == 'e' || ch == 'q' ) sys_exit( 0, 0 );
            else return 1;
            }
#endif
        }
    fprintf( std_err, "%s", s );
    if( nlflag )
        {
        ++scr_n;
        fprintf( std_err, "\n" );
        }
    return 0;
    }


/*  scr_param() is called early on and just once.  It is a chance
    to set the screen parameters (scr_ht, scr_wth) from run-time
    considerations.  Otherwise they default to static initializations.
 */
static void scr_param()
    {
#if os_NT || os_DOSX
    screen_height();
#endif
    }


/*  sys_arg() is called for each argument.  The argument could come
    from the command line, or from an environment variable (LINT)
    or from within a .LNT file.  System dependent modifications
    to the argument may be made here.  The difference between this
    function and sys_option() is that sys_arg() is called for ALL
    arguments not just options; also, sys_option() is called for options
    within a lint comment and sys_arg() is not.
    It returns a 1 if an alteration to the argument has been made, else 0.
 */
    int
sys_arg(abuf)
    char abuf[ARG_LEN];
    {
#if OS_MINT | os_MPW | os_THINKC | os_UNIX | os_LINUX
    return sys_env_var(abuf);
#else
    return 0;
#endif
    }

/*  sys_def() is called before each module to pre-define
    preprocessor variables.
 */
    Void
sys_def()
    {
#if os_AMIGA
    scan_sym( "AMIGA", "1" );
#endif
#if os_VMS
    scan_sym( "VAX", "1" );
#endif
    }


/*  sys_dircmp(nm1,nm2) is called to compare two directory names
    for equality.  It returns 0 if they are equal (just like strcmp).
    This differs from sys_fncmp() in that
    either directory may have an optional SEP1 tacked on.
 */
    int
sys_dircmp(nm1,nm2)
    CHARPTR nm1;
    CHARPTR nm2;
    {
    size_t len1, len2;

    len1 = strlen(nm1);
    len2 = strlen(nm2);
    if( len1 == len2 ) return sys_fncmp( nm1, nm2 );
    if( len1 > len2 ) return sys_dircmp( nm2, nm1 );
    /* we can be assured that len1 < len2  */
    if( len1 + 1 == len2 )
        {
        /* nm2 has exactly one more character */
        int ch;
        int retval;

        ch = nm2[len1];
        if( ch == SEP1 || ch == SEP1A || ch == SEP1B )
            {
            nm2[len1] = '\0';
            retval = sys_fncmp( nm1, nm2 );
            nm2[len1] = (char) ch;
            return retval;
            }
        }
    return -1;
    }

#if !os_QNX
/*  sys_env(nm) returns the environment string associated with
    name nm.  It is called with arguments "LINT" and "INCLUDE".
 */
    CHARPTR
sys_env(nm)
    cCHARPTR nm;
    {
    /* So far as we know the following systems do not support getenv().
       Actually, VMS supports getenv() but our doc suggests using 'lint'
       as the name of the FlexeLint executable.  So until the Installation
       Notes get updated we can't support getenv() for VMS.
     */
#if os_ATARI | os_VERS | os_DG | os_A2GS | os_VOS | os_VMS
    return NULL;
#else
    CHARPTR getenv();
    return getenv( nm );
#endif
    }

/*  sys_putenv(directive) takes a directive of the form "name=value"
    and assigns value to the environment variable name.
 */
    int
sys_putenv(directive)
    cCHARPTR directive;
    {
#if os_ATARI | os_VERS | os_DG | os_A2GS | os_VOS | os_VMS
    return NULL;
#else
    int putenv();
    return putenv( directive );
#endif
    }

#endif


/*  sys_env_var( arg ) is called to expand environment
    variables (shell variables).  Sequences within arg of the form:

        VAR_START  variable-name  VAR_END

    are replaced by the value of the environment variable.
    The maximum length of arg is assumed to be ARG_LEN.  characters
    beyond this length are truncated.
    If the arg is an option (such as -d..., -format..., -sem...)
    that may contain the '%' character as data we do not look for
    this special sequence but return immediately.
    It returns a 1 if the argument has been altered, otherwise 0.

 */
    int
sys_env_var( arg )
    char arg[ARG_LEN];
    {
#if OS_MINT | os_UNIX | os_LINUX
#define VAR_START '%'
#define VAR_END '%'
#endif
#if os_MPW | os_THINKC
#define VAR_START '{'
#define VAR_END '}'
#endif

#ifdef VAR_START
    static cCHARPTR const opt_ignore[] =
        { "d", "format", "sem", "program_info", NULL };
    int option = 0;     /* this is an option */
    cCHARPTR optname;
    char tmpbuf[ARG_LEN];
    char  envbuf[ARG_LEN];
    char *s,    /* pointer to arg */
         *ss,   /* another pointer to arg */
         *t,    /* pointer to tmpbuf */
         *varstart,   /* position of VAR_START */
         *env_value,  /* value of environment variable */
         *lastvarend;   /* position just after the last variable */
    int varopen = 0;    /* we are processing a variable */
    int i;      /* a handy index */
    int count = 0;   /* number of substitutions */

    /* pre-scan looking for an option we should not process */
    for( s = arg;  *s == '-' || *s == '+' || *s == aoc_plus;  s++ )
        option = 1;
    if( option )
        for( i = 0; optname = opt_ignore[i]; i++ )
            {
            for( ss = s; *optname ; optname++, ss++ )
                {
                while( *ss == '\"' || *ss == aoc_quote ) ++ss;
                if( *optname != tolower(*ss) ) break;
                }
            if( !*optname ) return 0;
            }
    /* ok, its not one of the forbidden options, lets go further */
    tmpbuf[0] = 0;
    varstart = arg;
    lastvarend = arg;
    for( s = arg; *s; s++ )
        {
        if( !varopen && *s == VAR_START )
            {
            varopen = 1;
            varstart = s;
            }
        else if( varopen && *s == VAR_END )
            {
            varopen = 0;
            count++;
            *varstart = 0;   /*  temporily establish prefix */
            sg_sfcat( tmpbuf, lastvarend, ARG_LEN );
            *varstart = VAR_START;  /* restore */
            for( t = varstart + 1, i = 0; *t && *t != VAR_END; t++ )
                 envbuf[i++] = *t;
            envbuf[i] = 0;
            env_value = sys_env( envbuf );
            if( env_value )
                sg_sfcat( tmpbuf, env_value, ARG_LEN );
            /* else return 0; */
            lastvarend = s + 1;
            }
        }
    if( count && !varopen )
        {
        sg_sfcat( tmpbuf, lastvarend, ARG_LEN );
        strcpy( arg, tmpbuf );
        return 1;
        }
#endif
    return 0;
    }

/*  sys_exit( err_cnt, zero_opt ) is called to exit from
    Lint.  err_cnt is the total number of errors encountered.
    zero_opt is 1 if the -zero option was requested and is
    0 otherwise.
 */

    Void
sys_exit( err_cnt, zero_opt )
    int err_cnt, zero_opt;
    {
    extern Void exit();

    xmlout_tag(-1);
    if( (unsigned) err_cnt > 255 ) err_cnt = 255;
    if( zero_opt ) /* act as though no errors were encountered */
        err_cnt = 0;
    if( gfl_pa > 0 )  /* the pause flag -- serves to keep window open */
        {
        char buf[120];

        fprintf( std_err, "Type any key to terminate:" );
        (Void) fgets( buf, 119, stdin );
        }
    pipe_out( 'F', NULL );      /* FlexeLint is Finished */
#if os_VMS
    exit( err_cnt ? 02000000000 : 1 );
#else
    exit( err_cnt );
#endif
    }


/*  sys_expand(name) is used to expand wild card characters
    within file names.  If the argument name is non-NULL, this
    is a setup call.  sys_expand returns non-NULL to indicate that
    the name is worthy of expansion.  If sys_expand returns NULL on
    this first call the name does not contain wild-card characters
    and will be treated normally (will be passed to i_open above).
    If a nonNULL is returned then this return value is ignored
    but sys_expand() will be called repeatedly with a NULL
    argument to indicate that the caller is interested in
    the next name.  sys_expand() will return the name as a
    char *.  When no more names are left, sys_expand() will
    return NULL.  The char * that sys_expand() returns may
    point to a static area within this module or it may be a
    malloc'ed area. sys_expand()'s caller will only read
    the information there and it can be overwritten as soon
    as sys_expand() is called again. Of course sys_expand() cannot
    return a pointer to an auto area.
 */
    CHARPTR
sys_expand(name)
    CHARPTR name;
    {
    if( name )
        {
        cCHARPTR s;
        /* setup call */
        for( s = name; *s; s++ )
            {
            if( *s == '?' || *s == '*' )
                {
                return os_expand( name );
                }
            }
        return NULL;  /* signal no wild cards */
        }
    else return os_expand( name );
    }

 unsigned
/*  sys_fattrib( path ) returns file attributes.  The file attributes
    for our limited purposes include at least the following flags.
    fatt_ORDIN => ordinary file
    fatt_DIR   => directory
    This is a wrapper function around a system call.
    For example, on POSIX systems it results in a call to stat().
 */
sys_fattrib( path )
    cCHARPTR path;
    {
    return os_fattrib( path );
    }

/*  sys_fgets( buffer, len, file, last ) is like the library function
    fgets() except that it can have system dependent properties.
    The first three arguments are identical to fgets().  The 4th
    (last) if used will point to the last (extended) character read.
 */

#if !os_DOSX && !os_IBM
/*  sys_fgets( buffer, len, file, last ) will be identical to fgets()
    if the frn flag is OFF.  If it is ON, a solitary carriage return
    (cr) is treated as a newline (nl), whereas a cr followed by a nl
    is taken together as a nl.
    The last character read by this input stream is saved by the caller
    via the pointer last.
 */
CHARPTR sys_fgets( buffer, len, file, last )
    CHARPTR buffer;
    unsigned len;
    FILE *file;
    int *last;
    {
    register CHARPTR s;    /* points to next location in the buffer */
    register int ch;       /* character just read from input stream */
    register unsigned rem; /* number of writable bytes remaining in buffer */


    if( gfl_rn <= 0 ) return fgets( buffer, len, file );
    /*  len must be at least 4  */
    if( !file || len < 4 || *last == EOF ) return NULL;
    s = buffer;
    rem = len - 2;
    ch = getc( file );
    if( *last == '\r' && ch == '\n' ) ch = getc( file );
    if( ch == EOF ) { *last = EOF; return NULL; }
    while( ch != '\r' && ch != '\n' && ch != EOF && --rem != 0 )
        {
        *s++ = (char) ch;
        ch = getc( file );
        }
    *last = ch;
    if( ch == '\r' || ch == '\n' ) *s++ = '\n';
    *s++ = 0;
    return buffer;
    }
#endif

#if os_DOSX
/*  The following rendition of fgets() treats nl (i.e. '\n')
    as a line break and does not insist on finding cr-nl
    (i.e. the '\r' '\n' combination) to terminate a line as is
    done by the compiler we use to compile the DOSX version of our
    product.  The 4th argument is unused (hence the name).
 */
CHARPTR sys_fgets( buffer, len, file, un_last )
    CHARPTR buffer;
    unsigned len;
    FILE *file;
    int *un_last;
    {
    register CHARPTR s;
    register int ch;
    register unsigned rem; /* number of writable bytes remaining in buffer */

    /* len must be at least 4  */
    if( !file || (ch = getc( file )) == EOF || len < 4 ) return NULL;
    s = buffer;
    *s = (char) ch;
    rem = len - 2;  /* no. of bytes minus current position and minus Nul */
    if( ch != '\n' )
        {
        while( (ch = getc(file)) != EOF && ch != '\n' )
            {
            *++s = (char) ch;
            if( --rem == 0 ) break;  /* if no bytes remain quit */
            }
        /* we coalesce cr-nl into nl and a terminal cr becomes an nl */
        if( *s == '\r' ) *s = '\n';
        /* otherwise append a nl only if we actually saw one. */
        else if( ch == '\n' ) *++s = '\n';
        }
    *++s = 0;
    return buffer;
    }
#endif


#if os_IBM
    CHARPTR
sys_fgets( buffer, len, file, un_last )
    CHARPTR buffer;
    unsigned len;
    FILE *file;
    int *un_last;
    {
    CHARPTR endptr;

    if( fgets( buffer, len, file ) && buffer[0] )
        {
        endptr = buffer + (strlen( buffer ) - 1 );
        if( *endptr-- == '\n' && *endptr == ' ' )
            {
            while( *endptr == ' ' ) endptr--;
            *++endptr = '\n';
            *++endptr = 0;
            }
        return buffer;
        }
    else return NULL;
    }
#endif





/*  sys_i( option, pri ) is called for each -i option.  (If pri == 0, then
    this is a --i option.) The purpose is to convert a directory into a
    prefix by tacking on the local directory separation character.
    Thus, for Unix, "-i/usr" becomes "-i/usr/" The flag +fip (gfl_ip)
    enables -i to work in the old way (a prefix only).  A change made
    for version 6 is to make a call on add_search() at the bottom of
    this routine.  This enables us to call sys_include() which in turn
    enables us to support multiple directories with -i
 */
    Void
sys_i( option, pri )
    CHARPTR option;
    int pri;
    {
    unsigned len;
    int c;

    if( gfl_im > 0 )
        {
        sys_include( NULL, option+2, pri );
        return;
        }
    len = (unsigned) strlen(option);
    if( gfl_ip > 0 && len > 1 && option[1] == 'i' )
        /* we are done, do not add a file separation character */ ;
    else if( len )
        {
        c = option[len-1];
        if( c == SEP1 || c == SEP1A || c == SEP1B ) /* OK */;
        else if( len < ARG_LEN-2 )   /* if we don't exceed an argument length */
            {
            option[len] = SEP1;      /* tack on the ... */
            option[len+1] = 0;       /* separation character */
            }
        }
    add_search( option+2, pri );
    }


/*  sys_include( incvar, plist, pri ) processes an environment variable
    specified by incvar (if NULL it defaults to INCLUDE)
    incvar specifies a list of directories separated by SEP2.
    Each directory is converted into a directory prefix by
    appending SEP1.  It is then passed to function add_search()
    to add it to the directory search list.  These same search
    strings reappear as the argument named 'prefix' to i_open.
    If plist is non NULL it gets used as the input source instead
    of calling sys_env().
    pri determines search priority.  Directories given by the $INCLUDE
    environment variable or --i options are considered to have low
    priority (i.e., pri == 0). -i options have pri == 1
    Mod 2/26/97: At one time we would routinely ignore
    blanks.  Now we copy them provided they are interior blanks.
 */
    Void
sys_include( incvar, plist, pri )
    cCHARPTR incvar;
    cCHARPTR plist;
    int pri;
    {
    cCHARPTR  p;
    CHARPTR  q;
    char  buf[MAXFNM + 2];  /* limited to length of single directory name */
    int c, c1;
    int count;      /* count of characters added to buffer */
    int blank = 0;  /* number of interior blanks that we owe */

    if( plist ) p = plist;
    else
        {
        if( incvar ) p = sys_env( incvar );
#if os_MPW | os_THINKC
        else p = sys_env( "CIncludes" );
#else
        else p = sys_env( "INCLUDE" );
#endif
        }
    if( !p ) return;
    q = buf;
    count = 0;
    for(;;)
        {
        c = *p++;
        if( c == SEP2 || c == 0 )
            {
            if( q > buf )
                {
                c1 = q[-1];
                if( c1 != SEP1 && c1 != SEP1A && c != SEP1B ) *q++ = SEP1;
                *q = 0;
                sys_fnmap( buf, 'd' );
                add_search( buf, plist && pri ? 1 : 0 );
                /* adds the string buf to the search path
                   with low (0) priority (i.e. lower than
                   -i<directory> option).  if the 2nd argument
                   were 1 then the priority would be high.
                 */
                }
            q = buf;
            count = 0;
            blank = 0;
            if( c == 0 ) break;
            }
        else
            {
            if( c == ' ' )  /* don't insert blanks just yet */
                { if( count ) blank++; continue; }
            /* now dump out the accumulated blanks */
            while( blank > 0 && count < MAXFNM-1 )
                { *q++ = ' '; blank--; count++; }
            /* add in the character if it fits */
            if( count++ < MAXFNM-1 ) *q++ = (char) c;
            /* count < MAXFNM */
            }
        }
    }

/*  sys_fncmp(nm1,nm2) is called to compare two file names for equality.
    Most systems support exact comparision.  This is especially
    true since most file names are passed sys_fnorm() which normalizes
    the name.  However some systems support mixed case in directories
    but do not require the user to employ the same case when specifying
    the file.  In this case the comparison must be done case
    independently.
 */
    int
sys_fncmp(nm1,nm2)
    cCHARPTR nm1, nm2;
    {
#if FNMAP_MIXED
    char buf1[ MAXFNM ], buf2[ MAXFNM ];

    sys_fnfold( buf1, nm1 );
    sys_fnfold( buf2, nm2 );
    return strcmp( buf1, buf2 );
#else
    return strcmp( nm1, nm2 );
#endif
    }


/*  sys_fnfold( buffer, name ) is a utility routine to copy a file
    name into buffer and fold any mixed alphabetic letters to a
    monotone case for the purpose of comparisons.
 */
#if FNMAP_MIXED
    static Void
sys_fnfold( buffer, name )
    CHARPTR buffer;
    cCHARPTR name;
    {
    register int ch;
    register int i;

    for( i = 0; i < MAXFNM-1; i++ )
        {
        if( ch = (unsigned char) name[i] )
            buffer[i] = (char) (gfl_ff > 0 ? toupper(ch) : ch);
        else break;
        }
    buffer[i] = 0;
    }
#endif

/*  sys_fnmap(nm,context) is called to map file names and directory names
    to lower case or upper case if the O.S. does not distinguish the case
    of filenames, or to neither if the O.S. does distinguish case.
    This function is called for every name found on
    the command line or in an indirect file (context == 'c'),
    in an include file (context == '"', '<' or 0 depending on the
    kind of #include statement), in a -i option, -libdir option,
    or INCLUDE environment variable (context == 'd' for directory),
    a -libh opton (context == 0) or in a -o...() option
    (context == 'o' for output).  File names could be mapped in i_open
    but by mapping here, it aids in the early detection of .lnt and
    .lob files.
 */

/*lint -e{715}  unused paramters are OK in this function */

    Void
sys_fnmap(nm, context)
    CHARPTR nm;
    int context;
    {
#if FNMAP_UC | FNMAP_LC | os_NT | os_UNIX | os_LINUX
    register int c;

    if(nm) while( c = (unsigned char) *nm )
        {
#if os_VMS
        if( c == '/' ) c = ':';
#endif
#if FNMAP_UC
        if( gfl_ff > 0 ) c = toupper(c);   /* map to upper case */
#endif
#if FNMAP_LC
        if( gfl_ff > 0 ) c = tolower(c);   /* map to lower case */
#endif
#if OS_MINT
        if( c == '/' ) c = '\\';
#endif
#if os_UNIX | os_LINUX
        if( c == '\\' ) c = '/';
#endif
        *nm++ = (char) c;
        }
#endif
    }

/*  sys_fnorm(nm,context) is called to normalize a file name.
    In principle this means selecting a unique representation
    for a file name.  In practice, this means removing parent
    directory designations (..)  from a full path name.
 */
    Void
sys_fnorm(nm, context)
    CHARPTR nm;
    int context;
    {
#if OS_MINT | os_AMIGA | os_UNIX | os_LINUX | os_XENIX | os_9 | os_UCOS | os_QNX
    register int c;
    CHARPTR src, dest;

    sys_fnmap( nm, context );   /* map to case as appropriate */
    if( !nm ) return;
    src = nm;       /* initially source (src) and destination (dest)  ... */
    dest = nm;       /* are identical */
    /* first remove initial current directory specifications */
    while( src[0] == '.' && src[1] == SEP1 ) src += 2;
    while( c = (unsigned char) *src++ )
        {
        if( c == SEP1 && src[0] == '.' && src[1] == '.' &&
                         src[2] == SEP1 && dest != nm )
            {
            /* found a parent specification, remove prior directory.
               At the top of the loop, src (prior to the increment)
               and dest both pointed to a SEP1;  this situation must be
               reproduced with dest pointing to the previous SEP1 and
               src pointing to the following SEP1.  If there is no prior
               SEP1, src must point just after the following SEP1.
             */
            while( dest > nm )
                {
                register int ch;

                ch = (unsigned char) *--dest;
                if( ch == SEP1 || ch == SEP1A || ch == SEP1B )
                    { src += 2; break; }
                else if( dest == nm )  /* no prior SEP */
                    { src += 3; break; }
                }
            continue;  /* dest and src have been properly adjusted */
            }
        else if( c == SEP1 && src[0] == '.' && src[1] == SEP1 )
            {
            /* remove redundant current directory specification: */
            src++;
            continue;
            /* We'll write a separator next time around (if appropriate).
             */
            }
        *dest++ = (char) c;
        }
#if OS_MINT
    /* it was found experimentally that attempting to open a file whose
       name ends in a trailing blank opens the same file as if the
       blank weren't there creating a nasty bug.
     */
    while( dest > nm && isspace( dest[-1] ) )
        --dest;
#endif
    *dest = 0;
#else
    sys_fnmap( nm, context );   /* map to case as appropriate */
#endif
    }


 CHARPTR
/*  sys_fnsimple(name) will return the simple name of the file name
    by pointing to the correct spot within its buffer.
 */
sys_fnsimple(name)
    CHARPTR name;
    {
    CHARPTR p;

    p = name + strlen( name );
    while( *p != SEP1 && *p != SEP1A && *p != SEP1B && *p != SEP2 &&
           p != name )
        p--;
    if( *p == SEP1 || *p == SEP1A || *p == SEP1B || *p == SEP2 )
        p++;
    return p;
    }

/*  sys_mixed() returns the value of FNMAP_MIXED
 */
    int
sys_mixed()
    {
    return FNMAP_MIXED;
    }


#if !os_QNX
/*  sys_option( s, state ) is called whenever option s is to
    be processed whether the option appeared A) on the command
    line, B) in an indirect (.lnt) file, C) within a LINT environment
    variable, D) within sys_olist (see above) or E) within a lint
    comment.   This gives the customizer the chance to
    massage any option before passing it off to lint.

    state is used for split options.  state is initially 0 and otherwise
    equals the last return value of sys_option (which is normally 0).
    Some systems such as MPW insist that options such as -dNAME
    be split into -d NAME.  In this case sys_option( "-d", 0 )
    is called and a 1 is returned.  The caller then knows that
    NAME is not a filename and calls sys_option("NAME", 1).
    If you are not using option -split you can forget about this (other
    than to always return 0).
 */
    int
sys_option( s , state )
    char *s;
    int state;
    {
    return lnt_option( s, state );
    }
#endif

/* the following include is to gain access to getcwd().  This stands for
   "get Current Working Directory".  It's no big loss if this isn't available.
   Just define this as a macro returning 0 as indicated below.
 */
#if os_UNIX | os_LINUX | os_UCOS | os_9 | os_XENIX | os_QNX
/*  If you don't have unistd.h change the '1' on the next line to a '0'  */
#if 1
#include <unistd.h>
#else
#define getcwd(a,b) 0
#endif
#endif

#if os_NT
#include <direct.h>
#ifdef __WATCOMC__
#define _getdcwd(a,b,c) 0  /* get cwd for a given drive -- not avail in V. 10 */
#endif
#endif

/*  sys_pathname( buffer, name ) returns in buffer the full path name
    of a file.  This usually amounts to prepending the current directory
    if this is not already a full name.  The returned name may have
    redundancies (e.g. "/x/../y/z" == "/y/z"). File normalization is done
    elsewhere (sys_fnorm).  As a last resort name is simply copied
    into buffer.
 */
    void
sys_pathname( buffer, name )
    char buffer[MAXFNM];
    CHARPTR name;
    {

    strcpy( buffer, name );

#if os_UNIX | os_LINUX | os_UCOS | os_9 | os_XENIX | os_QNX
        {
        char locbuf[MAXFNM];

        if( name[0] == '/' ) return;
        if( getcwd( locbuf, MAXFNM - 1 ) )
            {
            strcpy( buffer, locbuf );
            sg_sfcat( buffer, "/", MAXFNM );
            sg_sfcat( buffer, name, MAXFNM );
            }
        }
#endif

#if os_NT
        {
        char locbuf[MAXFNM];
        char longbf[MAXFNM];
        longbf[0] = 0;
#define is_SLASH(ch) ((ch)=='\\' || (ch) == '/')

        /* there are four possibilities:
           drive: full path
           drive: partial path
           full path
           partial path
         */
        if( name[0] && name[1] == ':' )
            {
            int drive;

            if( is_SLASH(name[2]) ) return;  /* drive: full path */
            /* drive: partial path */
            drive = tolower( (unsigned char) name[0] ) + 1 - 'a';
            name += 2;
            if( _getdcwd( drive, locbuf, MAXFNM - 1 ) )
                {
                win_lngnm( locbuf, longbf );
                if( longbf[ strlen( longbf ) - 1 ] == '\\' )
                    longbf[ strlen( longbf ) - 1 ] = 0;
                strcpy( locbuf, longbf );
                }
            else return;
            }
        else if( is_SLASH(name[0]) && is_SLASH(name[1]) )
            {
            win_lngnm( name, longbf );
            strcpy( buffer, longbf );
            return;
            }
        else
            {
            /* no drive, full or partial path */
            if( getcwd( locbuf, MAXFNM - 1 ) )
                {
                win_lngnm( locbuf, longbf );
                if( longbf[ strlen( longbf ) - 1 ] == '\\' )
                    longbf[ strlen( longbf ) - 1 ] = 0;
                strcpy( locbuf, longbf );
                if( is_SLASH(name[0]) )
                    {
                    locbuf[2] = 0; /* full path -- just use drive */
                    name++;        /* but don't use leading character */
                    }
                }
            else return;
            }
        strcpy( buffer, locbuf );
        sg_sfcat( buffer, "\\", MAXFNM );
        sg_sfcat( buffer, name, MAXFNM );
        }
#endif

    }


/*  sys_pragma(s) is called at initialization with s == NULL.  If control
    is wanted for each pragma then return 1 at this time. else return 0.
    If 1 was returned then for each pragma, sys_pragma(s) is called
    with s containing the pragma line absent the "#pragma".
    Continuations will have been processed so you will receive one long
    string.  Return values on these subsequent calls are not examined.
 */
    int
sys_pragma(s)
    cCHARPTR s;
    {
    if( s == NULL ) /* first call */ return 0;  /* no thanks */
    else return 1;  /* this won't happen because of prior return of 0 */
    }

/*  sys_tick() is called periodically; this has several purposes.
    On a non-preemptive scheduler a check can be made to see if some
    higher priority task requires work.  This is done on the MAC with
    SpinCursor().  It can also be used to respond more rapidly to an
    interrupt request from the user.  This is the purpose of the
    dos_break() call which releases a pending interrupt.
 */
    Void
sys_tick()
    {
#if os_DOS | os_DOSX
    dos_break();
#endif
#if os_MPW | os_THINKC
    SpinCursor( 1 ) ;
#endif
    pipe_out( 'P', NULL );  /* post a progress message */
    }

/*  sys_truncate(name) will truncate a filename to an 8X3 name
    As a peculiarity, the names comprising the path are not truncated,
    nor is the extension.
 */
    Void
sys_truncate(name)
    register CHARPTR  name;
    {
    CHARPTR  last_dot = NULL;    /* points to last dot if any */
    CHARPTR  last_name = NULL;   /* points to the last name */
    register CHARPTR probe;      /* used to traverse name */
    char  c;                     /* temporary character variable */

    last_name = name;
    for( probe = name; c = *probe; ++probe )
        {
        if( c == SEP1 || c == SEP1A || c == SEP1B )
            {
            last_dot = NULL;
            last_name = NULL;
            }
        else
            {
            if( !last_name ) last_name = probe;
            if( c == '.' )  last_dot = probe;
            }
        }
    if( !last_dot ) last_dot = probe;
    if( last_name && last_dot - last_name > 8 )
        strcpy( last_name+8, last_dot );
    }


/*  sys_vmsg(sflag,consumed) can be used to partially customize
    the verbosity messages (see -v... option).  This returns
    a string to be appended to the normal verbosity message.
    If sflag is set to 1 then the user has previously used the 's'
    (storage report) request with the -v option (as in -vsm ).
    The second argument is equal to the nominal number of bytes
    consumed so far (as measured by the arguments to malloc().
    NULL can be returned to signify no message.
 */

CHARPTR sys_vmsg( sflag, consumed )
    int sflag;
    Unsigned32 consumed;
    {
    static char buf[100];
    char numeral[60];

    if( !sflag ) return NULL;
    /* we now convert from long to thousand numeral */
    cvt_lth( numeral, (long) consumed );
    sprintf( buf, "consumed %s bytes", numeral );
    return buf;
    }



/*  Part III
    ANSI functions.
    You might need them depending on your library.
 */

#if os_AMIGA | os_UCOS

    Void *
memcpy(to,from,n)
    char *to, *from;
    unsigned int n;
    {
    char *areaptr = to;
    while( n-- > 0 ) *areaptr++ = *from++;
    return to;
    }

    Void *
memset(p,val,len)
    char *p;
    int val;
    unsigned int len;
    {
    char *areaptr = p;
    while( len-- > 0 ) *areaptr++ = val;
    return p;
    }

/*  strchr(s,ch) is an ANSI function to locate a character
    within a string
 */
    CHARPTR
strchr( s, ch )
    CHARPTR s;
    char ch;
    {
    char c = ch;    /* old compilers 'expand' ch */
    while( *s && *s != c ) s++;
    return *s ? s : NULL;
    }

#endif



/*lint -restore Restore error settings */
