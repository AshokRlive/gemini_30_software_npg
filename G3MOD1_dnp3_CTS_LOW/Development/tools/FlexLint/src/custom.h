
/*  custom.h  --  Header file for custom.c and all modules within
                    FlexeLint

    Contents:
        A.  System dependent macros
        B.  System dependent typedefs
        C.  Declarations for objects within custom.c
            Part I:     Customizable Data
            Part II:    Customizable Functions
        D.  Declarations for objects used by custom.c

                    Gimpel Software
                    July 2008
 */

/*  Section A.      Macros  */

    /*  Convenient sets of systems:  */

#define OS_DOSCO (os_DOS | os_DOSX | os_2 | os_2_2)  /* DOS compatible */
#define OS_MINT (OS_DOSCO | os_NT)              /* Microsoft IBM INTel */
#define OS_POSIX (os_UNIX | os_LINUX | os_XENIX | os_QNX)  /* supports POSIX */

    /*  Limits:  */

/* Determin MAXimum length of a File NaMe (MAXFNM) */

#ifdef FILENAME_MAX
#if FILENAME_MAX > 140
#define MAXFNM FILENAME_MAX     /* ANSI maximum filename length */
#else
#define MAXFNM 140      /* but no less than 140 */
#endif
#else
#define MAXFNM 275      /* 275 is the default */
#endif

#if MAXFNM > 612
#define ARG_LEN MAXFNM  /* maximum argument length */
#else
#define ARG_LEN 612     /* at least 612 chars */
#endif

#ifndef M_DSTR
#define M_DSTR 300      /* Maximum Depth of #Define'd STRings (macros) */
#endif

#ifndef M_IF
#define M_IF 50         /* Maximum depth of nested #IF's */
#endif

#ifndef M_CONTROL
#define M_CONTROL 100   /* Maximum depth of nested control structures */
                        /* if(), while(), do, for(), switch() */
#endif

#ifndef M_LINE
#define M_LINE 600      /* Maximum input LINE length  */
#endif

#ifndef M_INC
#define M_INC 40        /* Maximum No. of nested INClude files */
#endif

#ifndef M_STRING
#define M_STRING 4096   /* Maximum macro, maximum type length for -od */
#endif

#ifndef M_NAME
#define M_NAME 1024     /* Maximum name -- also used for template lines */
#endif

#ifndef FSETLEN
#define FSETLEN 800     /* File SET LENgth -- the number of bytes needed */
                        /* to represent the set of files */
/*  n.b.  the maximum number of files is given by:
            FSETLEN * CHARLEN
    CHARLEN (set below) is almost certainly 8.  Therefore the maximum
    number of files is by default 800*8 = 6400.
    If you need more it is safe to bump FSETLEN but the number of files
    should not exceed 64K.
    You can also use the option -maxfiles(n) to set the maximum number
    of files and this is now recommended.
 */

#endif

#ifndef MAX_OPEN
#if os_2_2
#define MAX_OPEN 12   /* small number of open files supported */
#elif os_NT
#define MAX_OPEN 50   /* can't fseek on \n lines */
#else
#define MAX_OPEN 15   /* reasonable compromise */
#endif
#endif

#ifndef M_TOKEN
#define M_TOKEN 200      /* Maximum token length */
#endif

/*  If your compiler does not support void you can modify this definition
    of Void by changing the "void" below to "int".
 */

#define Void void

/*  Character set dependencies are taken care of here. */

#if cs_ASCII
#if os_NT || __STDC__  /* most compilers support ctype.h */
#include <ctype.h>
#else       /* but some don't implement tolower properly */
#define isdigit(c)  ('0' <= (c) && (c) <= '9')
#define islower(c)  ('a' <= (c) && (c) <= 'z')
#define isupper(c)  ('A' <= (c) && (c) <= 'Z')
#define tolower(c)  (isupper(c) ? ((c) + ('a' - 'A')) : (c))
#define toupper(c)  (islower(c) ? ((c) - ('a' - 'A')) : (c))
#define isspace(c)  ((c)==' '||(c)=='\t'||(c)=='\n'||(c)=='\r')
#endif
#define NUM_CHARS 256 /* No. of characters in char. set */
#else
#include <ctype.h>
#if cs_EBCDIC
#define NUM_CHARS 256 /* No. of characters in char. set */
#endif
#endif

/*  Data sizes -- what follows are default data sizes.
    These are subject to further modification via the -s...
    option.
 */

#define CHARMASK 0xFF   /* the character portion of an int */
#define CHARLEN 8       /* the length of a character */

#define SZ_CHAR  sizeof( char )
#define SZ_SHORT sizeof( short )
#define SZ_INT   sizeof( int )
#define SZ_LONG  sizeof( long )
#define SZ_LLI    8     /* size of long long int (__int64) */
#define SZ_MAX_INT sizeof( MAX_INT_t )
#define SZ_FLOAT sizeof( float )
#define SZ_DBL   sizeof( double )
#define SZ_LDBL  16   /* most compilers do not yet support long double */
#define SZ_WCHAR 2    /* sizeof built-in wchar_t */

/*  pointer sizes:
    Note that near pointers are the default.  If you are troubled
    by the distinction between near and far pointers, don't be.
    This is an anomoly of the 8086 segmentation architecture.
 */

#if m_8086
#if m_8086 == 3
    /* 80386 */
#define SZ_PTR_ND 4     /* size of near data pointers */
#define SZ_PTR_NP 4     /* size of near program (function) pointers */
#define SZ_PTR_FD 6     /* size of far data pointers */
#define SZ_PTR_FP 6     /* size of far program (function pointers */
#else
    /* 8086, 8088, 80186, 80286 */
#define SZ_PTR_ND 2     /* size of near data pointers */
#define SZ_PTR_NP 2     /* size of near program (function) pointers */
#define SZ_PTR_FD 4     /* size of far data pointers */
#define SZ_PTR_FP 4     /* size of far program (function pointers */
#endif
#else
#define SZ_PTR_ND sizeof( char * )
#define SZ_PTR_NP sizeof( int (*)() )
#define SZ_PTR_FD sizeof( char * )
#define SZ_PTR_FP sizeof( int (*)() )
#endif

/*  The following macros (ARB and ARBA) are used when defining
    data that does not have to be especially initialized.  For
    example:

    int n ARB;
    int a[10] ARBA;

    ARBA is used for aggregates.
    Some compilers require the explicit initializer;  others
    generate more efficient code when it is there.
    But it is not strictly necessary and better error checking
    is possible without it.
 */

#ifdef _lint
#define ARB             /* Denotes no special initialization */
#define ARBA
#else
#define ARB   = 0
#define ARBA  = { 0 }
#endif



/*  modes for binary reading and writing.  MD_RD_B and MD_WR_B
    are used as second arguments to fopen to indicate that the I/O
    is for binary read and write respectiveley.  Non-unix systems
    frequently distinguish between binary and non-binary i/o modes
    because new-line needs to be translated to a line-end sequence.
    But all ANSI C compilers are supposed to support the 'b' even if
    they ignore it.
 */

#if os_UNIX | os_LINUX | os_UCOS | os_AMIGA | os_9
#define MD_RD_B "r"     /* fopen mode for binary reading */
#define MD_WR_B "w"     /* fopen mode for binary writing */
#else
#define MD_RD_B "rb"    /* fopen mode for binary reading */
#define MD_WR_B "wb"    /* fopen mode for binary writing */
#endif

/*  last_ext() actions
 */

#define ext_REP 1   /* Replace */
#define ext_APP 2   /* Append */
#define ext_TEST 3  /* Test extension */


/*  What follows are macro-ized versions of various system routines.
    They are made macros rather than functions for efficiency.
    They are placed here in custom.h in the event that a particular
    operating system/compiler does not support these functions
    the way we expect in which case they can be rewritten.
 */


/* sys_write() is like fwrite except it returns 1 on error */

#define sys_write(buf,size,cnt,f) \
    (fwrite(buf,(size_t)(size),(size_t)(cnt),f) != (size_t)(cnt))

/* sys_read() is like fread() */

#define sys_read(buf,size,cnt,f) fread(buf,(size_t)(size),(size_t)(cnt),f)

/*  The following test is not as precise as we would like.
    We would really like to be able to test to see if size_t is a
    typedef and, if not, typedef it.  Unfortunately that is not a test
    you can do at pre-processor time.  Experience indicates that
    size_t is available with most C compilers.  About the only ones
    that do not are ancient Unix compilers.  But any compiler supporting
    C90 to the extent of providing a built-in preprocessor variable of
    __STDC__ will certainly support a size_t
    If not we will just assume that size_t is unsigned.
 */

#if os_UNIX | os_LINUX
#ifndef __STDC__
#define size_t unsigned
#endif
#endif

/*  The following macros are ANSI required for fseek(), 3rd argument  */

#ifndef SEEK_SET
#define SEEK_SET (0)
#define SEEK_CUR (1)
#endif


/*  Section B.      typedefs  */

typedef char *CHARPTR;          /* a convenient type */
typedef const char *cCHARPTR;   /* the const version therof */

/*
   FlexeLint makes use of types UNLONG (unsigned long) and
   UNSHORT (unsigned short) for a variety of purposes.
   If your compiler does not support unsigned long
   and/or unsigned short then you will have to use substitutes.

    For UNLONG use:
        typedef unsigned int UNLONG;   -- if int is same size as long
        typedef long UNLONG;           -- if int is smaller than long
    for UNSHORT use:
        typedef unsigned int UNSHORT;   -- if int is same size as short
        typedef short UNSHORT;          -- if int is larger than short

   No great harm results in making these substitutions.

   Starting with version 6.00 of FlexeLint we are making use of the
   typedef symbol size_t hopefully defined in stdio.h.  If not use
   the following declaration:
   typedef unsigned size_t;
 */

typedef unsigned long UNLONG;    /* if compiler complains see above */
typedef unsigned short UNSHORT;  /* if compiler complains see above */


/*
   FlexeLint makes use of types Unsigned32 (unsigned integer of length 32)
   and Unsigned16 (unsigned integer of length 16) for a variety
   of purposes.  For most machines this is easily achieved with the
   types unsigned int and unsigned short.
   If your compiler's int and short differ in length from these numbers
   then use the nearest unsigned integer greater than or equal to these
   lengths.
 */

typedef unsigned int    Unsigned32;
typedef unsigned short  Unsigned16;

typedef int Int32;
typedef short Int16;

/* The types MAX_INT_t and MAX_UINT_t defined below are used to
   manipulate user values during value tracking and should designate
   the largest integer type available.  You may use a
    -DLINT_MAX_INT=integer-type
   on the command line to override the default determination of what
   this maximum integer should be.
 */

#ifdef LINT_MAX_INT
    typedef LINT_MAX_INT MAX_INT_t;             /* maximum integer type */
    typedef unsigned LINT_MAX_INT MAX_UINT_t;   /* maximum unsigned type */
#else
    #if os_NT
        typedef __int64 MAX_INT_t;              /* maximum integer type */
        typedef unsigned __int64 MAX_UINT_t;    /* maximum unsigned type */
    #else
        #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
            #include <stdint.h>
            typedef intmax_t MAX_INT_t;
            typedef uintmax_t MAX_UINT_t;
        #else
            typedef long MAX_INT_t;
            typedef unsigned long MAX_UINT_t;
        #endif
    #endif
#endif

/*  Section C. -- declarations for objects within custom.c */

/*  Part I, Data  */

extern int ansi_flag;       /* flag to inhibit common MS extensions */
extern char aoc_cm;         /* used to separate sub-options */
extern char aoc_plus;       /* used to introduce options as in +fmd */
extern char aoc_quest;      /* used in error inhibition as in -e7?? */
extern char aoc_quote;      /* used to quote blank-embedded names as in
                               lint "hard disk:C programs:myprog.c"  */
extern char aoc_lp;         /* '(' used in -option(list) */
extern char aoc_rp;         /* ')' used in -option(list) */
extern char aoc_star;       /* used as wild-card in -esym names */
extern cCHARPTR default_ext[]; /* default extents */
extern char dot_c[];        /* c extent */
extern char dot_cpp[];      /* cpp extent */
extern char dot_cxx[];      /* cxx extent */
extern char dot_h[];        /* header extent */
extern char dot_lnt[];      /* normally .lnt */
extern char dot_lob[];      /* normally .lob */
extern char dot_lph[];      /* normally .lph */
extern char dot_vac[];      /* normally .vac */
extern int scr_ht;  /* The number of lines on the screen */
extern int scr_wth; /* The number of characters on a line of the screen */
extern int scr_n;   /* The number of lines that have been written */
extern cCHARPTR sys_id;           /* System Identification */
extern cCHARPTR sys_odesc[];      /* System option description */
extern cCHARPTR sys_olist;        /* System Options List       */
extern cCHARPTR sys_vers;         /* Version Number */

/*  Part II, Functions */

int extract_file_id();          /* called to extract a module name */
FILE * i_open();                /* called to open a file */
FILE * i_fopen();               /* low level call to open a file */
int last_ext();                 /* tests/modifies a filename extenstion */
int ind_tst();                  /* indirect-file test */
Void scr_need();                /* use to ensure n lines of screen */
int scr_out();                  /* use to output a line to the screen */
int sys_arg();                  /* argument modification */
extern int sys_dircmp();        /* used to compare two directory names */
Void sys_def();                 /* used to pre-define variables */
CHARPTR sys_env();              /* used to get an environment string */
Void sys_exit();                /* used to exit to system */
CHARPTR sys_expand();           /* expands file names */
extern unsigned sys_fattrib();  /* returns file attributes */
extern CHARPTR sys_fgets();     /* fgets() replacement */
extern int sys_fncmp();         /* compares two filenames or portions thereof */
extern Void sys_fnmap();        /* map file names to preferred case */
extern Void sys_fnorm();        /* used to normalize a file name */
extern CHARPTR sys_fnsimple();  /* obtain simplified file name */
extern Void sys_i();            /* used to append to -i option */
extern Void sys_include();      /* used to process INCLUDE variables */
extern int sys_mixed();         /* indicates that we are in mixed mode */
extern int sys_option();        /* passes each option onto lint */
extern int sys_pragma();        /* passes the pragma line in for custom use */
extern Void sys_pathname();     /* determines the full pathname */
extern int sys_putenv();        /* modifies an environment variable */
extern Void sys_tick();         /* called periodically to show activity */
extern Void sys_truncate();     /* truncates a filename to 8x3 */
extern CHARPTR sys_vmsg();      /* customize verbosity message */

/*  Section D.
    Declarations for objects within FlexeLint or within libraries
    that are not declared within stdio.h and are used by custom.c
 */

typedef int UFLAG;
extern UFLAG gfl_di;        /* directory of including file */
extern UFLAG gfl_ff;        /* fold file names */
extern UFLAG gfl_im;        /* -i can have multiple directories */
extern UFLAG gfl_ip;        /* treat -i directives as prefixs not directories */
extern UFLAG gfl_pa;        /* pause at termination */
extern UFLAG gfl_rb;        /* always use the "rb" form of fopen */
extern UFLAG gfl_rn;        /* \r not followed by \n is regarded as a \n */
extern UFLAG gfl_sh;        /* shared-file form of fopen */
extern double atof();       /* converts Ascii to Float */
int attempt_open();         /* indicate an attempt to open a file */
Void background();          /* runs FlexeLint in the background */
Void add_search();          /* adds a directory to the search sequence */
Void cvt_lth();             /* convert from long to thousand numeral */
Void Errorno();             /* issues an error message by number */
int lnt_option();           /* processes options */
Void pipe_out();            /* progress information to a pipe */
Void scan_sym();            /* defines a specific symbol with a value */
#if os_NT
Void screen_height();       /* sets the screen height */
extern void win_lngnm();    /* converts short path names to long */
#endif
Void sg_sfcat();            /* safe strcat() -- checks buffer length */
int strcmp();               /* standard string comparison */
extern FILE *shopen();      /* shared open */
extern FILE *std_err;       /* possibly redirected stderr file */
CHARPTR strchr();           /* find char within a string */
CHARPTR strncpy();          /* copy n characters to a string */
CHARPTR strrchr();          /* find char within a string scanning from right */
CHARPTR strstr();           /* find string within a string */
size_t strlen();            /* NOTE: if this fails to compile, activate
                               the declaration for size_t above */
extern Void sys_main();     /* entry point to lint */
extern Void xmlout_tag();   /* can be used to properly terminate xml output */

extern CHARPTR os_expand();
extern unsigned os_fattrib();
/*  File Attributes: */
#define fatt_ORDIN  0x01 /* Ordinary file */
#define fatt_DIR    0x02 /* Directory */

#if os_DOS | os_DOSX
extern Void dos_break();
#endif

