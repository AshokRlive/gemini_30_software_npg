
/*  vers.h  --  file to set version information for FlexeLint

    Please look through this list and see if any of the variables
    should be changed from 1 to 0 or from 0 to 1.
    These variables are only used mostly in custom.h and custom.c.
    You may check those files to see what use is made of them.
    Please do not feel as though you HAVE to make changes.
    FlexeLint may work just fine without them.

            Gimpel Software
 */

/*  The character set -- set only one of these to 1 */

#define cs_ASCII 1      /* Ascii character set */
#define cs_EBCDIC 0     /* IBM's EBCDIC character set */

/*  The operating system -- set one of these if appropriate */

#define os_DOS 0        /* MS-DOS */
#define os_DOSX 0       /* a DOS extender */
#define os_2 0          /* OS/2 */
#define os_2_2 0        /* OS/2, Version 2 */
#define os_NT 0         /* Windows NT */
#define os_AMIGA 0      /* AmigaDos */
#define os_ATARI 0      /* Atari ST */
#define os_VMS 0        /* OpenVMS */
#define os_9 0          /* OS-9 */
#define os_XENIX 0      /* Xenix */
#define os_UNIX 1       /* Unix */
#define os_LINUX 0      /* Linux */
#define os_IBM 0        /* IBM's VM */
#define os_VERS 0       /* VERSADOS */
#define os_DG 0         /* Data General */
#define os_MPW 0        /* Macintosh MPW environment */
#define os_QNX 0        /* QNX Operating System */
#define os_A2GS 0       /* Apple 2GS */
#define os_UCOS 0       /* Honeywell's UCOS Unix-like system */
#define os_MVS 0        /* IBM'S MVS */
#define os_VOS 0        /* Stratus' VOS */
#define os_THINKC 0     /* Think C */

/*  The machine
    Currently we distinguish only the Intel Chip because of
    its segmentation hardware.  This leads to two different
    kinds of addresses (near and far) and four memory models.
    Note that other machine differences are largely taken into
    account automatically with the sizeof operator.  See custom.h.

    The variable m_8086 is set to 3 for the 80386
    and to 1 for all other chips in this family.
 */

#define m_8086 0

/*  The language --
    This variable makes certain fine adjustments in the custom.c header
    to allow for the C++ dialect of C.  It cannot be arbitrarily
    changed from 0 to 1.
 */

#define l_CPP 1

