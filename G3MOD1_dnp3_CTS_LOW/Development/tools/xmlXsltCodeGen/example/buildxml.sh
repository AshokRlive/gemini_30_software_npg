#!/bin/sh
#
# Compile code.xml (enum definitions) for C/C++/Java
#
#
#xsltproc c.xsl code2.xml >code.h

#xsltproc cpp.xsl code2.xml >code.cph

xsltproc java.xsl channels_source_psm.xml >PSMChannelsEnum.java
xsltproc java.xsl channels_source_mcm.xml >MCMChannelsEnum.java
xsltproc java.xsl channels_source_fdm.xml >FDMChannelsEnum.java
xsltproc java.xsl channels_source_scm.xml >SCMChannelsEnum.java