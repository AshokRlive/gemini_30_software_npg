
// Autogenerated from XML file!!!
//

import java.util.*;

public class ModuleProtocolPSM {
  
   public enum PSM_CH_DINPUT {

      PSM_CH_DINPUT_LS_OVERVOLTAGE(0),
      
   PSM_CH_DINPUT_LS_UNDERVOLTAGE(1),
      
   PSM_CH_DINPUT_LS_SELECT_OPERATE_TIMEOUT(2),
      
   PSM_CH_DINPUT_MS_SELECT_OPERATE_TIMEOUT(3),
      
   PSM_CH_DINPUT_MS_MOTOR_ON_TIMEOUT(4),
      
   PSM_CH_DINPUT_MS_RELAY_ON(5),
      
   PSM_CH_DINPUT_MS_OVERVOLTAGE(6),
      
   PSM_CH_DINPUT_MS_OVERCURRENT(7),
      
   PSM_CH_DINPUT_MS_LOCAL_REMOTE_FAULT(8),
      
   PSM_CH_DINPUT_AUX_OVERVOLTAGE(9),
      
   PSM_CH_DINPUT_AUX_UNDERVOLTAGE(10),
      
   PSM_CH_DINPUT_AC_SELECT_OPERATE_TIMEOUT(11),
      
   PSM_CH_DINPUT_AC_OFF(12),
      
   PSM_CH_DINPUT_AC_OVERVOLTAGE(13),
      
   PSM_CH_DINPUT_AC_UNDERVOLTAGE(14),
      
   PSM_CH_DINPUT_CH_FAULT(15),
      
   PSM_CH_DINPUT_CH_ON(16),
      
   PSM_CH_DINPUT_BT_COMMS_FAIL(17),
      
   PSM_CH_DINPUT_BT_DISCONNECTED(18),
      
   PSM_CH_DINPUT_BT_OVER_TEMPERATURE(19),
      
   PSM_CH_DINPUT_BT_CHARGING_OVERRIDE(20),
      
   PSM_CH_DINPUT_BT_TEST_FAIL(21),
      
   PSM_CH_DINPUT_BT_LOW(22),
      
   PSM_CH_DINPUT_BT_FAULT(23),
      
   PSM_CH_DINPUT_FAN_ON(24),
      
   PSM_CH_DINPUT_FAN_FAULT(25),
      
   PSM_CH_DINPUT_LAST(26);
      

      private static final HashMap<Integer, PSM_CH_DINPUT> typesByValue = new HashMap<Integer, PSM_CH_DINPUT>();

      static {
         for (PSM_CH_DINPUT type : PSM_CH_DINPUT.values()) {
            typesByValue.put(type.value, type);
         }
      }

      private final int value;
      
      PSM_CH_DINPUT(int value) {
         this.value = value;
      } 

      public final int getValue() {
         return this.value;
      }

      public static PSM_CH_DINPUT forValue(int value) {
         return typesByValue.get(value);
      }

   }

   public enum PSM_CH_AINPUT {

      PSM_CH_AINPUT_LS_VOLTAGE(0),
      
   PSM_CH_AINPUT_LS_CURRENT(1),
      
   PSM_CH_AINPUT_MS_VOLTAGE(2),
      
   PSM_CH_AINPUT_MS_CURRENT(3),
      
   PSM_CH_AINPUT_MS_PEAK_HOLD_CURRENT(4),
      
   PSM_CH_AINPUT_AC_VOLTAGE(5),
      
   PSM_CH_AINPUT_AC_LOWER_TRIP_LEVEL(6),
      
   PSM_CH_AINPUT_AC_UPPER_TRIP_LEVEL(7),
      
   PSM_CH_AINPUT_AUX_VOLTAGE(8),
      
   PSM_CH_AINPUT_CH_VOLTAGE(9),
      
   PSM_CH_AINPUT_CH_CHARGING_CURRENT(10),
      
   PSM_CH_AINPUT_BT_VOLTAGE(11),
      
   PSM_CH_AINPUT_DRAIN_CURRENT(12),
      
   PSM_CH_AINPUT_ON_ELAPSED_TIME(13),
      
   PSM_CH_AINPUT_TS_AMBIENT_TEMPERATURE(14),
      
   PSM_CH_AINPUT_TS_BATTERY_TEMPERATURE(15),
      
   PSM_CH_AINPUT_LAST(16);
      

      private static final HashMap<Integer, PSM_CH_AINPUT> typesByValue = new HashMap<Integer, PSM_CH_AINPUT>();

      static {
         for (PSM_CH_AINPUT type : PSM_CH_AINPUT.values()) {
            typesByValue.put(type.value, type);
         }
      }

      private final int value;
      
      PSM_CH_AINPUT(int value) {
         this.value = value;
      } 

      public final int getValue() {
         return this.value;
      }

      public static PSM_CH_AINPUT forValue(int value) {
         return typesByValue.get(value);
      }

   }

   public enum PSM_CH_PSUPPLY {

      PSM_CH_PSUPPLY_LOGIC(0),
      
   PSM_CH_PSUPPLY_MOTOR(1),
      
   PSM_CH_PSUPPLY_AC(2),
      
   PSM_CH_PSUPPLY_AUX(3),
      
   PSM_CH_PSUPPLY_LAST(4);
      

      private static final HashMap<Integer, PSM_CH_PSUPPLY> typesByValue = new HashMap<Integer, PSM_CH_PSUPPLY>();

      static {
         for (PSM_CH_PSUPPLY type : PSM_CH_PSUPPLY.values()) {
            typesByValue.put(type.value, type);
         }
      }

      private final int value;
      
      PSM_CH_PSUPPLY(int value) {
         this.value = value;
      } 

      public final int getValue() {
         return this.value;
      }

      public static PSM_CH_PSUPPLY forValue(int value) {
         return typesByValue.get(value);
      }

   }

   public enum PSM_CH_BCHARGER {

      PSM_CH_BCHARGER_BATTERY_DISCONNECT(0),
      
   PSM_CH_BCHARGER_BATTERY_TEST(1),
      
   PSM_CH_BCHARGER_LAST(2);
      

      private static final HashMap<Integer, PSM_CH_BCHARGER> typesByValue = new HashMap<Integer, PSM_CH_BCHARGER>();

      static {
         for (PSM_CH_BCHARGER type : PSM_CH_BCHARGER.values()) {
            typesByValue.put(type.value, type);
         }
      }

      private final int value;
      
      PSM_CH_BCHARGER(int value) {
         this.value = value;
      } 

      public final int getValue() {
         return this.value;
      }

      public static PSM_CH_BCHARGER forValue(int value) {
         return typesByValue.get(value);
      }

   }

   public enum PSM_CH_FAN {

      PSM_CH_FAN_FAN_TEST(0),
      
   PSM_CH_FAN_LAST(1);
      

      private static final HashMap<Integer, PSM_CH_FAN> typesByValue = new HashMap<Integer, PSM_CH_FAN>();

      static {
         for (PSM_CH_FAN type : PSM_CH_FAN.values()) {
            typesByValue.put(type.value, type);
         }
      }

      private final int value;
      
      PSM_CH_FAN(int value) {
         this.value = value;
      } 

      public final int getValue() {
         return this.value;
      }

      public static PSM_CH_FAN forValue(int value) {
         return typesByValue.get(value);
      }

   }

   public enum PSM_CH_DINPUT {

      PSM_CH_DINPUT_LS_OVERVOLTAGE(0),
      
   PSM_CH_DINPUT_LAST(1);
      

      private static final HashMap<Integer, PSM_CH_DINPUT> typesByValue = new HashMap<Integer, PSM_CH_DINPUT>();

      static {
         for (PSM_CH_DINPUT type : PSM_CH_DINPUT.values()) {
            typesByValue.put(type.value, type);
         }
      }

      private final int value;
      
      PSM_CH_DINPUT(int value) {
         this.value = value;
      } 

      public final int getValue() {
         return this.value;
      }

      public static PSM_CH_DINPUT forValue(int value) {
         return typesByValue.get(value);
      }

   }

   public enum PSM_CH_AINPUT {

      PSM_CH_AINPUT_LS_VOLTAGE(0),
      
   PSM_CH_AINPUT_LAST(1);
      

      private static final HashMap<Integer, PSM_CH_AINPUT> typesByValue = new HashMap<Integer, PSM_CH_AINPUT>();

      static {
         for (PSM_CH_AINPUT type : PSM_CH_AINPUT.values()) {
            typesByValue.put(type.value, type);
         }
      }

      private final int value;
      
      PSM_CH_AINPUT(int value) {
         this.value = value;
      } 

      public final int getValue() {
         return this.value;
      }

      public static PSM_CH_AINPUT forValue(int value) {
         return typesByValue.get(value);
      }

   }
}
