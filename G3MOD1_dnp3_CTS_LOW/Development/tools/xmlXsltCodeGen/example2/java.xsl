<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:strip-space elements="*"/>
  <xsl:output method="text" encoding="utf-8" media-type="text/plain" indent="no"/>

  <xsl:template match="Enumerations">
// Autogenerated from XML file!!!
//

import java.util.*;

public class <xsl:value-of select="@classname"/> {
  <xsl:for-each select="EnumDef">
   public enum <xsl:value-of select="@name"/> {

   <xsl:for-each select="EnumSet/Item">
   <xsl:variable name="cur" select='position()-1'/>
      <xsl:text>   </xsl:text><xsl:value-of select="@name"/>(<xsl:value-of select="$cur"/>)<xsl:choose><xsl:when test="position() != last()">,</xsl:when><xsl:otherwise>;</xsl:otherwise></xsl:choose><xsl:text>
      
</xsl:text>
   </xsl:for-each>
      private static final HshMap<xsl:text>&#60;</xsl:text>Integer, <xsl:value-of select="@name"/><xsl:text>&#62;</xsl:text> typesByValue = new HashMap&#60;Integer, <xsl:value-of select="@name"/>&#62;();

      static {
         for (<xsl:value-of select="@name"/> type : <xsl:value-of select="@name"/>.values()) {
            typesByValue.put(type.value, type);
         }
      }

      private final int value;
      
      <xsl:value-of select="@name"/>(int value) {
         this.value = value;
      } 

      public final int getValue() {
         return this.value;
      }

      public static <xsl:value-of select="@name"/> forValue(int value) {
         return typesByValue.get(value);
      }

   }
</xsl:for-each>}
</xsl:template>
</xsl:stylesheet> 
