#!/bin/sh
#
# Compile code.xml (enum definitions) for C/C++/Java
#
xsltproc c.xsl code.xml >code.h

xsltproc cpp.xsl code.xml >code.cph

xsltproc java.xsl code.xml >code.java
