<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference
  PUBLIC "-//OASIS//DTD DITA Reference//EN" "http://docs.oasis-open.org/dita/v1.1/OS/dtd/reference.dtd" >
   <reference xml:lang="en-us" id="BBDMin-Max-Avg-Logic">
   <title>Max/Min/Average control logic page</title>
   <prolog/>
   <refbody>
      <refsyn>You are here: <menucascade>
            <uicontrol>Configuration</uicontrol>
            <uicontrol>Control Logic</uicontrol>
            <uicontrol>Max/Min/Average</uicontrol>
         </menucascade></refsyn>
      <section>
         <p>The <uicontrol>Max/Min/Average</uicontrol> control logic makes three calculations from
            an analogue value during a specified interval of time: the minimum and the maximum
            values registered during this period, and the average value of all the values registered
            during such period.</p>
         <p>These values are displayed as analogue values in the <uicontrol>Max</uicontrol>,
               <uicontrol>Min</uicontrol>, and <uicontrol>Average</uicontrol> points. For older
            systems, it is possible to display these values converted to <i>16-bit counter
               values</i>.</p>
         <note>This option is only available when creating the control logic, and cannot be added or
            removed afterwards. </note>
      </section>
      <section>
         <title>Max/Min/Average Settings dialog</title>
         <table
            conref="../components/Global-Control-Logic-Settings.dita#ReusableComponent-kq3_my4_wq/table-ibz_kx4_wq"
            id="table-px3_my4_wq">
            <tgroup cols="cols_a53_my4_wq">
               <tbody>
                  <row>
                     <entry/>
                  </row>
               </tbody>
            </tgroup>
         </table>
         <p>Use the <uicontrol>Event Mode</uicontrol> panel to configure the behaviour of the
            control logic:</p>
         <table frame="all" rowsep="1" colsep="1" id="table-kqr_4t4_5q">
            <tgroup cols="2">
               <colspec colname="c1" colnum="1" colwidth="1.0*"/>
               <colspec colname="c2" colnum="2" colwidth="1.0*"/>
               <tbody>
                  <row>
                     <entry><uicontrol>Interval</uicontrol></entry>
                     <entry>Set the fixed-time interval of the measurement, e.g. make calculations
                        every 20 minutes. <note>This setting is <i>not</i> affected by any
                           synchronisation adjustments and depends on the RTU startup
                        time.</note></entry>
                  </row>
                  <row>
                     <entry><uicontrol>From O'Clock</uicontrol></entry>
                     <entry>Take a measurement every time the clock reaches a multiple of the
                           <uicontrol>From O'Clock</uicontrol> time.<p>For example, a setting of 20
                           minutes will report the values at 12:00, 12:20, 12:40, 13:00, and so
                           on.</p><note>This setting is affected by synchronisation adjustments. For
                           example, a setting of 30 minutes may take less or more than 30 minutes if
                           the time is adjusted forwards or backwards.</note></entry>
                  </row>
               </tbody>
            </tgroup>
         </table>
         <p>If you are using <i>counter</i> values, configure the settings in the <uicontrol>Counter
               Support</uicontrol> panel:</p>
         <table frame="all" rowsep="1" colsep="1" id="table-tm5_kgw_5q">
            <tgroup cols="2">
               <colspec colname="c1" colnum="1" colwidth="1.0*"/>
               <colspec colname="c2" colnum="2" colwidth="1.0*"/>
               <tbody>
                  <row>
                     <entry><uicontrol>Conversion Scaling Factor</uicontrol></entry>
                     <entry>
                        <p>Apply the following formula to the output:</p>
                        <p><i>counter value</i> = <i>analogue value</i> x <i>scaling factor</i></p>
                        <p>For example, an average value of -123.4567 with a <uicontrol>Conversion
                              Scaling Factor</uicontrol> of 100 will be converted into -12345
                           (discarding the rest of the decimals), and it will be fitted into a
                           16-bit positive value, giving a counter value of 53191. </p>
                        <note>This scaling factor is applied independently of the scaling factor of
                           the analogue <uicontrol>Input</uicontrol> value.</note>
                     </entry>
                  </row>
               </tbody>
            </tgroup>
         </table>
      </section>
      <section>
         <title>View control logic</title>
         <p><b>Input</b></p>
         <p>In order to make the calculations, an analogue <uicontrol>Input value</uicontrol> point
            is needed as a source in the <uicontrol>Input</uicontrol> list.</p>
         <p><b>Status/Result</b></p>
         <p>The <i>analogue</i> values are displayed in the <uicontrol>Max</uicontrol>,
               <uicontrol>Min</uicontrol>, and <uicontrol>Average</uicontrol> points.</p>
         <p>The <i>counter</i> values are displayed in the <uicontrol>Max(Counter)</uicontrol>,
               <uicontrol>Min(Counter)</uicontrol>, and <uicontrol>Average(Counter)</uicontrol>
            points.</p>
      </section>
   </refbody>
</reference>
