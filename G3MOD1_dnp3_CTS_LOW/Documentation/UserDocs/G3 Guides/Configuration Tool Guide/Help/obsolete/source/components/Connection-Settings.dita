<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE oXygen:ReusableComponent [
<!ELEMENT oXygen:ReusableComponent (oXygen:ComponentDescription, oXygen:ComponentDefinition)>
<!ATTLIST oXygen:ReusableComponent 
    xmlns:oXygen CDATA #FIXED "http://www.oxygenxml.com/ns/dita/reuse" 
    id CDATA #IMPLIED
    domains CDATA "(topic reference)                            (topic hi-d)                             (topic ut-d)                             (topic indexing-d)                            (topic hazard-d)                            (topic abbrev-d)                            (topic pr-d)                             (topic sw-d)                            (topic ui-d)     (topic oXygen-reuse-d) "
    class CDATA "- topic/topic oXygen:ReusableComponent/oXygen:ReusableComponent "    
    xmlns:ditaarch CDATA #FIXED "http://dita.oasis-open.org/architecture/2005/"
    ditaarch:DITAArchVersion CDATA #FIXED "1.2" 
    >
<!ELEMENT oXygen:ComponentDescription (#PCDATA)>
<!ATTLIST oXygen:ComponentDescription 
 class CDATA "- topic/title oXygen:ReusableComponent/oXygen:ComponentDescription ">
<!ELEMENT oXygen:ComponentDefinition ANY>
<!ATTLIST oXygen:ComponentDefinition
    class CDATA "- topic/body oXygen:ReusableComponent/oXygen:ComponentDefinition ">

<!ENTITY % dtd PUBLIC "-//OASIS//DTD DITA Reference//EN" "../references/reference.dtd">
%dtd;

]>
<oXygen:ReusableComponent id="ReusableComponent-gtc_5m3_zq">
    <oXygen:ComponentDescription>Channel-TCP Connection Manager</oXygen:ComponentDescription>
    <oXygen:ComponentDefinition><section id="section-htc_5m3_zq"><title id="title-tnl_jns_fq">Connection Manager</title>
            <p>If enabled, these settings allow you to configure intelligent connection
                handling.</p>
            <p><uicontrol>Failover Group</uicontrol></p>
            <simpletable outputclass="content" id="simpletable-u3m_m1y_xp">
                <strow>
                    <stentry><uicontrol>IP Address</uicontrol></stentry>
                    <stentry>IP address of alternate SCADA</stentry>
                </strow>
                <strow>
                    <stentry><uicontrol>Port</uicontrol></stentry>
                    <stentry>TCP port to connect to on alternate SCADA</stentry>
                </strow>
                <strow>
                    <stentry><uicontrol>Master Address</uicontrol></stentry>
                    <stentry>DNP3.0 master address of alternate SCADA</stentry>
                </strow>
            </simpletable>
            <p><uicontrol>Connection Settings</uicontrol></p><simpletable outputclass="content" id="simpletable-nrd_qks_fq"><strow><stentry><uicontrol>Connect on Event</uicontrol></stentry><stentry>Select which class of event will cause a connection attempt to the SCADA.</stentry></strow><strow><stentry><uicontrol>Disconnect on Inactivity</uicontrol></stentry><stentry>Enables the RTU to disconnect from SCADA if no communication is exchanged for this period (in seconds).</stentry></strow><strow><stentry><uicontrol>Connect Retry Delay</uicontrol></stentry><stentry>The period (in seconds) between connectivity checks to SCADA.</stentry></strow></simpletable>
            <p><uicontrol>Comms Device Power Cycle</uicontrol></p><p>If enabled, the RTU will power cycle a modem or other comms device if communications to SCADA
                become unreliable.</p><simpletable id="simpletable-fp5_mj3_xq"><strow><stentry><uicontrol>Comms Power Supply</uicontrol></stentry><stentry>In the dropdown list, select which power supply to toggle in the event of unreliable
                        communication to SCADA.</stentry></strow><strow><stentry><uicontrol>Power Cycle Duration</uicontrol></stentry><stentry>The number of seconds to withdraw power from the communications device before applying it.</stentry></strow><strow><stentry><uicontrol>Initialisation Time</uicontrol></stentry><stentry>The duration between applying power to the comms device and the device being ready to
                        communicate. This time will delay outgoing connections after a power cycle
                        by this period.</stentry></strow></simpletable>
            <p><b>IP Connectivity Check</b></p>
            <p>If enabled, the RTU will periodically attempt to open a TCP connection to each IP
                dddress in the <uicontrol>Failover Group</uicontrol> in turn.</p>
            <table id="table-vrx_c43_zq">
                <tgroup cols="2">
                    <colspec colnum="1" colname="col1" colwidth="*"/>
                    <colspec colnum="2" colname="col2" colwidth="*"/>
                    <tbody>
                        <row>
                            <entry><b>Period</b></entry>
                            <entry>How often the RTU will attempt to make a connection to
                                SCADA.</entry>
                        </row>
                        <row>
                            <entry><b>Port</b></entry>
                            <entry>The TCP port number to connect to on SCADA.</entry>
                        </row>
                        <row>
                            <entry><b>Retries</b></entry>
                            <entry>The number of retry attempts to connect to SCADA on the port
                                    specified.<p>If this number is reached, the communications
                                    device will be power cycled.</p></entry>
                        </row>
                    </tbody>
                </tgroup>
            </table>
            <p><b>Outgoing Connections</b></p>
            <p>If enabled, the RTU will power cycle the communications device if a number of
                connection attempts to SCADA fail.</p>
            <table id="table-dgb_tp3_zq">
                <tgroup cols="2">
                    <colspec colnum="1" colname="col1" colwidth="*"/>
                    <colspec colnum="2" colname="col2" colwidth="*"/>
                    <tbody>
                        <row>
                            <entry><b>Retries</b></entry>
                            <entry>The number of retry attempts to connect to SCADA on the DNP3.0
                                    port.<p>If this number is reached the communications device will
                                    be power cycled.</p></entry>
                        </row>
                    </tbody>
                </tgroup>
            </table>
            <p><b>Incoming Connections</b></p>
            <p>If enabled, the RTU will power cycle the communications device if it has not received
                an incoming connection from SCADA.</p>
            <table id="table-frg_xp3_zq">
                <tgroup cols="2">
                    <colspec colnum="1" colname="col1" colwidth="*"/>
                    <colspec colnum="2" colname="col2" colwidth="*"/>
                    <tbody>
                        <row>
                            <entry><b>Not received for</b></entry>
                            <entry>The time that the RTU expects to have received an incoming
                                connection request from SCADA.<p>If this time expires without an
                                    incoming connection from SCADA, the communications device will
                                    be power cycled.</p></entry>
                        </row>
                    </tbody>
                </tgroup>
            </table></section></oXygen:ComponentDefinition>
</oXygen:ReusableComponent>