<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE task PUBLIC "-//OASIS//DTD DITA Task//EN" "task.dtd">
<task id="New-Config-File-Scratch">
    <title>Create a new configuration from scratch</title>
    <taskbody>
        <context>You are here: <menucascade>
                <uicontrol>File</uicontrol>
                <uicontrol>New</uicontrol>
                <uicontrol>New from scratch</uicontrol>
            </menucascade></context>
        <steps>
            <step>
                <cmd>On the <uicontrol>Basic</uicontrol> page, configure the settings, and then
                    click <uicontrol>Next</uicontrol>.</cmd>
                <info>You will find extra information about some of the settings in the table
                        below.<table id="table-uf5_j1k_vq">
                        <tgroup cols="2">
                            <colspec colname="col1" colwidth="*"/>
                            <colspec colname="col2" colwidth="*"/>
                            <thead>
                                <row>
                                    <entry morerows="0" namest="col1" nameend="col1">Setting</entry>
                                    <entry morerows="0" namest="col2" nameend="col2"
                                        >Description</entry>
                                </row>
                            </thead>
                            <tbody>
                                <row>
                                    <entry morerows="0" namest="col1" nameend="col1">
                                        <uicontrol>RTU Site Name</uicontrol>
                                    </entry>
                                    <entry morerows="0" namest="col2" nameend="col2">Enter a maximum
                                        of 31 characters to keep the text visible.</entry>
                                </row>
                                <row>
                                    <entry morerows="0" namest="col1" nameend="col1">
                                        <uicontrol>Version</uicontrol>
                                    </entry>
                                    <entry morerows="0" namest="col2" nameend="col2">Type the
                                        version number of the configuration.<p>This setting is
                                            optional.</p></entry>
                                </row>
                                <row>
                                    <entry morerows="0" namest="col1" nameend="col1">
                                        <uicontrol>Description</uicontrol>
                                    </entry>
                                    <entry morerows="0" namest="col2" nameend="col2">Enter a maximum
                                        of 31 characters to keep the text visible.<p>This setting is
                                            optional.</p></entry>
                                </row>
                            </tbody>
                        </tgroup>
                    </table></info>
            </step>
            <step>
                <cmd>On the <uicontrol>IP Configuration</uicontrol> page, configure the settings,
                    and then click <uicontrol>Next</uicontrol>.</cmd>
                <info>Select or clear the <uicontrol>Enable Ethernet 0</uicontrol> check box to
                    enable or disable the Ethernet 0 port on the RTU. By default, the Ethernet 0
                    (ETH-0) port is used to communicate with the SCADA.</info>
            </step>
            <step>
                <cmd>On the <uicontrol>G3 Modules</uicontrol> page, configure the settings, and then
                    click <uicontrol>Next</uicontrol>.</cmd>
                <info>You will find extra information about some of the settings in the table
                        below.<table id="table-wf3_pbk_vq">
                        <tgroup cols="2">
                            <colspec colname="col1" colwidth="*"/>
                            <colspec colname="col2" colwidth="*"/>
                            <thead>
                                <row>
                                    <entry morerows="0" namest="col1" nameend="col1">Setting</entry>
                                    <entry morerows="0" namest="col2" nameend="col2"
                                        >Description</entry>
                                </row>
                            </thead>
                            <tbody>
                                <row>
                                    <entry morerows="0" namest="col1" nameend="col1">
                                        <uicontrol>Backplane Type</uicontrol>
                                    </entry>
                                    <entry morerows="0" namest="col2" nameend="col2">In the list,
                                        click the backplane with the correct number of
                                        slots.</entry>
                                </row>
                                <row>
                                    <entry morerows="0" namest="col1" nameend="col1">
                                        <uicontrol>Backplane Off-Local-Remote Switch
                                            Fitted</uicontrol>
                                    </entry>
                                    <entry morerows="0" namest="col2" nameend="col2">The default
                                        local-remote switch is on the MCM module. If a separate
                                        local remote switch is fitted, select the checkbox.</entry>
                                </row>
                                <row>
                                    <entry morerows="0" namest="col1" nameend="col1">
                                        <uicontrol>G3 Modules</uicontrol>
                                    </entry>
                                    <entry morerows="0" namest="col2" nameend="col2">Select the
                                        module and quantity boxes that match your hardware
                                        configuration.</entry>
                                </row>
                                <row>
                                    <entry namest="col1" nameend="col1"><uicontrol>Default HMI
                                            menu</uicontrol></entry>
                                    <entry namest="col2" nameend="col2">Select the check box to
                                        create an HMI menu that contains the default menus and
                                        screens for the G3 modules that you have selected.<p>The
                                            following HMI menu is created by default:</p><ul
                                            id="ul-fjk_jck_vq">
                                            <li><uicontrol>Data</uicontrol> menu</li>
                                            <li><uicontrol>Control</uicontrol> menu</li>
                                            <li><uicontrol>Info</uicontrol> menu</li>
                                            <li><uicontrol>Alarms</uicontrol> info screen</li>
                                        </ul>
                                    </entry>
                                </row>
                            </tbody>
                        </tgroup>
                    </table></info>
            </step>
            <step>
                <cmd>On the <uicontrol>Virtual Points</uicontrol> page, follow the instructions on
                    screen, and then click <uicontrol>Next</uicontrol>.</cmd>
                <info>If you only want to define a small number of virtual points, select
                        <uicontrol>None</uicontrol>. The virtual points can be added later.</info>
            </step>
            <step>
                <cmd>On the <uicontrol>Control Logic</uicontrol> page, follow the instructions on
                    screen, and then click <uicontrol>Next</uicontrol>.</cmd>
            </step>
            <step>
                <cmd>On the <uicontrol>SCADA Protocol</uicontrol> page, follow the instructions on
                    screen, and then click <uicontrol>Next</uicontrol>.</cmd>
                <info>If you only want to define a small number of protocol points, select
                        <uicontrol>None</uicontrol>. You can define these later in the
                    configuration.</info>
            </step>
            <step>
                <cmd>On the <uicontrol>Field Device Protocol</uicontrol> page, follow the
                    instructions on screen, and then click <uicontrol>Finish</uicontrol>.</cmd>
            </step>
        </steps>
        <result>The basic configuration items are generated for you.</result>
        <postreq>
            <p props="exclude">The next stage is to map the appropriate virtual and pseudo points to
                new protocol points according to your SCADA system’s requirements.</p>
        </postreq>
    </taskbody>
</task>
