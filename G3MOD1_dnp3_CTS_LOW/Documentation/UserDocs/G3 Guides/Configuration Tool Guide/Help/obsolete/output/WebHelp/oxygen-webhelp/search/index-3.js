
// Auto generated index for searching.
w["periodically"]="11*0,18*0,19*0";
w["periods"]="0*0,61*0";
w["permanently"]="18*0,19*0";
w["persistent"]="37*2";
w["personnel"]="2*0";
w["phase"]="45*0,59*1";
w["phase-to-earth"]="59*0";
w["phases"]="59*5";
w["physical"]="21*0,35*0,36*0,51*0,52*2,83*0";
w["picker"]="80*0";
w["pieces"]="21*0";
w["place"]="71*0";
w["planners"]="2*0";
w["pltu"]="59*48,81*0";
w["plus"]="35*0";
w["point"]="0*0,15*1,26*0,27*1,29*45,30*0,31*0,32*0,33*33,35*40,36*38,37*38,38*31,47*0,49*0,58*0,63*0,65*1,66*2,73*2,78*1,79*0,80*15,81*0,85*35,86*31,87*36,89*32,90*32,91*32,92*33";
w["point's"]="15*0,80*0";
w["points"]="26*33,27*0,34*33,35*2,36*1,37*1,38*0,49*2,59*0,65*153,73*1,78*153,79*92,80*34,84*0,89*4,90*4,91*6,92*3,97*0";
w["polarity"]="31*0,46*0,70*0";
w["poll"]="21*0,52*1,53*1";
w["polling"]="52*1,53*0";
w["port"]="0*0,18*5,19*20,20*15,43*1,51*6,60*1,61*2,62*14,83*0,95*1,96*5,97*1";
w["ports"]="0*0,51*0,61*0,62*32,95*0,96*0";
w["port’s"]="62*0";
w["position"]="30*0,32*3,59*4,71*1";
w["positive"]="37*0,49*0";
w["possible"]="35*1,36*1,37*1,39*0,49*0,52*1";
w["power"]="18*4,19*4,50*2,63*91";
w["powered"]="68*0";
w["predefined"]="80*0";
w["preferences"]="64*112";
w["presence"]="59*8";
w["present"]="32*0,58*1,59*1";
w["press"]="45*0";
w["pressing"]="45*1";
w["pressure"]="35*1";
w["prevent"]="59*0,71*1";
w["prevented"]="59*0";
w["previously"]="65*0";
w["primary"]="0*1,60*0,96*0";
w["printed"]="0*0,61*0";
w["priority"]="21*0";
w["problem"]="71*0";
w["product"]="24*0";
w["professionals"]="2*0";
w["progress"]="30*0";
w["prompting"]="98*0";
w["proper"]="11*0";
w["properties"]="57*0";
w["protected"]="46*0";
w["protocol"]="18*0,19*0,20*0,21*42,26*35,27*43,28*0,29*0,33*0,34*0,35*10,42*14,51*2,56*0,62*1,65*153,68*0,69*36,78*0,80*2,82*32,83*2,85*1,86*2,87*2,88*32,94*0,95*0,97*1";
w["provided"]="41*0,59*1,73*1";
w["provider"]="0*0,43*0";
w["pseudo"]="66*41,80*0,81*0";
w["psm"]="16*2,41*0,63*41";
w["pstn"]="0*9,60*33";
w["public"]="0*0,37*0";
w["pulse"]="15*1,29*4,31*1,37*4,46*1,70*1";
w["pulse-driven"]="37*0";
w["pumping"]="35*0";
w["purposes"]="32*0";
w["quantity"]="97*0";
w["queue"]="51*1";
w["radio"]="0*12,61*5";
w["radio-pad"]="0*11,61*5";
w["rails"]="45*0";
w["range"]="35*12,53*0,80*0,83*0";
w["rapid"]="46*0";
w["rate"]="31*0,46*0,48*0,51*1,52*3,62*1,63*0,64*0,83*0";
w["raw"]="22*93,35*4,36*1,37*0";
w["re"]="21*0,50*0,53*0";
w["re-establish"]="21*0,53*0";
w["re-synchronisation"]="50*0";
w["reached"]="35*0,37*0";
w["reaches"]="37*0,49*0";
w["read"]="3*10,27*0,52*2";
w["read-only"]="52*2";
w["read/write"]="3*9";
w["reads"]="11*0";
w["real"]="37*1,56*0,65*1,68*0,77*0,78*1";
w["real-time"]="37*1,56*0,65*1,68*0,77*0,78*1";
w["realtime"]="11*72,55*0,56*0,64*9,65*0,68*0,78*0,98*0";
w["reasons"]="59*0";
w["rebuild"]="45*0";
w["receive"]="19*0,20*0,21*4,51*0,62*1";
w["received"]="21*5,27*1,50*0";
w["receiving"]="21*0,51*0,62*0";
w["recent"]="27*1";
w["recommend"]="62*1";
w["recommends"]="21*1";
w["recovers"]="53*0";
w["recovery"]="73*0";
w["red"]="70*0";
w["reducing"]="53*0";
w["reference"]="7*30,50*0,80*0,81*5";
w["reflects"]="37*0";
w["refresh"]="64*0";
w["register"]="52*16";
w["registered"]="49*1";
w["registers"]="52*0";
w["register’s"]="52*2";
w["regular"]="0*0,61*0";
w["regularly"]="0*0,61*0";
w["related"]="16*0,44*0,73*0,81*5,96*5";
w["relative"]="73*0";
w["relay"]="36*0,38*0";
w["relays"]="30*0,46*0";
w["relays/switches"]="46*0";
w["reliable"]="50*0";
w["relies"]="59*0";
w["remain"]="27*0";
w["remote"]="29*0,30*0,45*4,57*1,58*12,59*1,71*8,72*0,97*2";
w["remove"]="21*0,23*0,25*0,26*0,28*0,42*1,45*0,47*0,51*0,52*0,54*0,57*0,69*0,75*0,80*0";
w["removed"]="49*0,50*0";
w["report"]="36*5,37*0,49*0";
w["reported"]="35*3,36*3,37*0,56*0,58*0,59*1,65*0,68*0,73*0,78*0";
w["reporting"]="35*1,36*1,66*0";
w["reports"]="41*0,59*0,71*0,73*0";
w["represent"]="53*2";
w["request"]="0*0,18*0,19*0,27*1,50*0,51*2,52*3,61*0";
w["requested"]="27*1";
w["requesting"]="27*0,52*1";
w["requests"]="27*0";
w["require"]="27*0";
w["required"]="21*0,35*1,67*0";
w["requires"]="16*0";
w["reset"]="0*0,37*21,39*49,59*0,60*0,65*0,73*0,78*0,81*0";
w["resetting"]="0*1,39*0,61*1";
w["respectively"]="59*0";
w["respond"]="27*1";
w["response"]="0*0,18*0,19*1,20*0,27*8,51*2,53*1,61*0";
w["responses"]="19*0,20*0,27*3";
w["responsible"]="73*0";
w["rest"]="49*0";
w["restart"]="96*0,98*2";
w["restarted"]="68*0";
w["restored"]="39*1";
w["result"]="29*6,30*5,32*5,47*6,49*5,58*5,59*5,66*0";
w["resynchronise"]="62*0";
w["retained"]="36*0";
w["retries"]="21*0,53*0";
w["retrieve"]="52*2";
w["retry"]="0*0,18*0,19*0,21*0,27*3,61*0";
w["retrying"]="0*0,27*1,61*0";
w["return"]="35*1,80*0";
w["returned"]="36*0";
w["right"]="11*0,65*0,78*0";
w["rights"]="75*0";
w["ring"]="59*4";
w["rise"]="15*0,46*1";
w["rises"]="37*0";
w["rmu"]="59*5";
w["rmu's"]="59*1";
w["rmus"]="59*0";
w["rollover"]="37*0,85*0";
w["rolls"]="37*0";
w["root"]="68*0";
w["root-fs"]="68*0";
w["rounding"]="64*0";
w["row"]="89*0,90*0,91*0";
w["rows"]="89*0,90*0,91*0";
w["rs"]="51*4,62*27,83*1";
w["rs-232"]="51*2,62*18,83*0";
w["rs-485"]="51*1,62*9,83*0";
w["rts"]="62*1";
w["rts/cts"]="62*1";
w["rtu"]="3*5,11*2,16*0,17*0,18*5,19*5,21*0,27*1,29*0,32*0,35*0,37*1,39*0,40*0,41*1,43*0,44*42,49*0,50*3,51*0,56*42,57*40,58*1,60*0,61*0,62*3,65*0,67*92,68*81,71*0,72*0,74*1,77*172,78*0,83*0,96*36,97*1,98*156";
w["rtu's"]="37*0";
w["rtu’s"]="68*0";
w["run"]="15*0,16*0,29*0";
w["running"]="50*0,62*0";
w["runs"]="15*2";
w["safe"]="71*0";
w["safety"]="71*0";
w["same"]="35*1,36*1,37*1,38*0,39*0,51*0,71*0,80*0,83*0";
w["sampled"]="46*0";
w["sampling"]="31*0,46*0,48*0,63*0";
w["save"]="3*4,50*0,56*3,65*3,68*3,78*3";
w["saving"]="50*0";
w["scada"]="0*3,3*0,18*11,19*11,20*0,21*43,26*0,27*41,28*0,29*0,32*1,33*0,34*0,35*3,36*0,39*0,40*0,50*0,56*1,58*1,60*1,61*1,62*0,65*1,66*0,68*2,69*34,71*1,78*1,85*1,86*1,87*0,88*31,94*0,95*0,96*0,97*1";
w["scadas"]="18*0,19*0,21*0";
w["scale"]="73*1,80*0";
w["scaled"]="35*3";
w["scaling"]="35*11,49*6,80*0";
w["scm"]="30*0,70*42,71*0";
w["scratch"]="3*0,97*31";
w["screen"]="0*2,35*1,45*3,81*0,82*0,83*1,84*2,88*0,97*4";
w["screens"]="97*0";
w["search"]="89*1,90*1,92*1";
w["second"]="51*0,59*0,62*0,83*0";
w["secondary"]="0*1,60*1,96*0";
w["seconds"]="0*0,15*0,18*2,19*2,45*1,66*0";
w["security"]="71*0";
w["see"]="52*0,81*0,84*0";
w["select"]="0*0,18*4,19*4,21*1,27*9,29*0,32*0,35*2,36*4,37*4,38*2,39*1,45*1,51*0,52*3,56*1,57*0,62*5,65*1,68*1,74*3,78*1,80*0,83*2,84*0,85*0,86*0,87*0,89*5,90*5,91*4,92*1,94*0,96*1,97*3";
w["select/deselect"]="89*0,90*0,91*0";
w["selected"]="21*1,23*1,25*2,28*1,42*3,45*2,51*1,52*1,54*1,57*3,62*0,69*1,76*0,80*3,89*1,90*1,91*1,92*0,97*0";
w["selecting"]="16*0";
w["self"]="27*0";
w["send"]="19*2,20*2,21*0,27*1";
w["sending"]="21*0,50*0,66*0";
w["sensitive"]="2*0";
w["sensor"]="36*0,38*0,59*1";
w["sensor/input"]="36*0";
w["sent"]="0*1,21*0,27*0,51*0,52*2,60*1,62*1,83*0";
w["separate"]="45*0,57*0,97*0";
w["separated"]="0*0";
w["sequence"]="27*0,53*2";
w["serial"]="0*0,21*0,51*3,56*0,61*0,62*0,83*1,95*33";
w["serially"]="53*0";
w["server"]="18*3,19*3,51*0,62*10,74*1,96*3";
w["servers"]="96*0";
w["service"]="0*1,2*0,68*1";
w["session"]="21*5,26*3,27*99,33*3,34*3,51*4,53*41,85*3,86*3,87*3";
w["sessions"]="21*9,51*9,53*0";
w["set"]="0*0,16*0,17*0,18*0,19*0,27*1,31*5,35*7,36*0,37*2,40*0,41*0,45*1,46*6,47*0,48*1,49*0,50*0,57*0,59*0,63*2,65*0,70*5,71*0,73*3,75*0,78*0,80*0,83*0";
w["set-point"]="35*2";
w["sets"]="35*1,37*1";
w["setting"]="0*3,18*0,19*0,21*0,27*0,32*0,35*3,36*0,43*0,44*2,49*3,52*2,53*1,57*0,60*0,61*0,62*2,63*0,83*1,97*3";
w["settings"]="0*5,12*30,15*10,16*9,18*9,19*9,21*9,25*0,26*0,29*9,30*9,31*0,32*9,36*0,39*9,40*9,41*9,45*0,47*9,49*10,51*15,53*18,58*9,59*9,62*2,64*28,66*9,70*0,71*9,73*9,81*2,83*2,85*1,86*1,87*1,95*0,96*36,97*4";
w["several"]="35*0,36*0,37*0,47*0,71*0,73*0";
w["short"]="21*0";
w["shorter"]="45*0";
w["should"]="37*0,59*0,71*0";
w["show"]="80*0";
w["shown"]="11*0,32*0,94*0,95*0";
w["shows"]="80*0";
w["shut"]="37*0";
w["side"]="35*0";
w["signal"]="15*1,30*0,31*0,35*2,37*1,39*0,46*0,48*0,62*0,63*0,73*0";
w["signals"]="30*0,35*3,36*0,38*0";
w["sim"]="0*0,43*0";
w["similar"]="37*0,51*0,83*0";
w["similarly"]="73*0";
w["simply"]="37*0";
w["since"]="35*3,37*0,41*0,50*0,68*1";
w["single"]="15*0,26*0,47*1,66*0,73*1,89*0,90*0,91*0";
w["site"]="44*1,68*11,97*0";
w["situation"]="59*0";
w["size"]="21*1,51*0";
w["sizes"]="21*1";
w["slave"]="27*2,51*0,53*2,83*1";
w["slots"]="57*0,97*0";
w["snapshot"]="37*0";
w["so"]="11*0,31*0,37*2,46*0,48*0,49*0,50*0,52*0,63*0,66*0,71*0,76*0,96*0";
w["soe"]="27*0";
w["software"]="68*11,74*3";
w["some"]="0*2,83*1,85*0,86*0,87*0,97*1";
w["sometimes"]="21*0";
w["soon"]="35*1,37*0";
w["sound"]="45*0";
w["source"]="27*2,29*0,35*3,36*3,37*2,38*3,39*0,40*0,49*0,80*2,85*1,86*1,87*0";
w["specification"]="21*1";
w["specified"]="15*1,36*0,37*2,38*0,49*0";
w["specifies"]="37*0";
w["specify"]="27*7,32*0,35*0,37*0,52*0,96*2";
w["specifying"]="15*0";
w["stamp"]="36*1";
w["stamped"]="56*0,65*0,68*0,78*0";
w["standby"]="61*0";
w["start"]="35*0,41*0,62*0,74*0";
w["started"]="41*0";
w["starting"]="37*0,59*0,71*0";
w["starts"]="41*0";
w["startup"]="27*0,49*0";
w["state"]="36*0,45*0,56*1,58*5,59*1,65*0,68*0,71*3,72*0,78*0";
w["stated"]="59*0";
w["states"]="35*0,45*0";
w["station"]="58*1,71*1";
w["statistics"]="55*0";
w["status"]="11*0,27*1,29*6,30*5,32*8,35*1,47*5,49*5,55*0,56*0,58*5,59*6,66*0,68*0,71*1";
w["status/result"]="29*5,30*5,32*5,47*5,49*5,58*5,59*5";
w["step"]="37*0";
w["still"]="50*0,53*0";
w["stop"]="51*0,62*1";
w["stopping"]="41*0";
w["store"]="27*2";
w["stored"]="56*0,62*3,65*0,68*0,78*0";
w["stream"]="62*0";
w["string"]="0*6,60*6";
w["structure"]="51*0";
w["sub"]="45*0";
w["submask"]="62*2,96*0";
w["subnet"]="62*1";
w["subsystems"]="77*0";
w["summary"]="56*0,68*0";
w["supplied"]="0*0,43*0";
w["supplies"]="39*0,40*0,59*0";
w["supply"]="18*1,19*1,45*1,59*0,63*91";
w["support"]="21*0,49*0,67*0";
w["supported"]="82*0,83*0,88*0";
w["sw1"]="59*0";
w["sw1l1"]="59*5";
w["sw1l2"]="59*5";
w["sw1l3"]="59*5";
w["sw2"]="59*0";
w["sw2l1"]="59*5";
w["sw2l2"]="59*5";
w["sw2l3"]="59*5";
w["swap"]="53*3";
w["switch"]="16*0,30*0,31*92,32*49,45*2,57*2,58*0,59*2,70*91,71*2,72*1,97*2";
w["switched"]="0*0,35*0";
w["switches"]="46*0,59*3";
w["switchgear"]="32*0,58*0,59*0,71*46,81*0";
w["synchronisation"]="37*1,49*1,50*0,66*0";
w["synchronised"]="50*0";
w["synchronization"]="27*0,50*0,51*0";
w["system"]="3*0,35*0,56*1,58*0,65*1,68*5,74*0,77*173,78*1,96*0";
w["systems"]="49*0,59*10";
w["tab"]="21*27,42*18,51*27,52*27,53*0,56*18,62*18,65*18,68*18,78*18";
w["table"]="0*2,21*1,27*0,31*4,45*1,46*2,48*0,51*6,63*4,70*4,83*1,85*0,86*0,87*0,97*1";
w["tabs"]="51*0,62*9";
w["take"]="49*1,71*0";
w["takes"]="37*0";
w["tasks"]="96*5";
w["tba"]="18*5,19*5,27*0,33*0,34*0,35*6,36*1,38*7,63*0,64*6,80*0,85*11,86*10,87*4";
w["tcp"]="18*74,19*74,20*30,21*1,51*2,53*0,62*0,83*1,94*30,95*30";
w["tcp/ip"]="18*1,19*1,53*0";
w["tcp/udp"]="19*31,21*0";
w["telephone"]="0*0";
w["tell"]="50*0";
w["temperature"]="45*0,63*0";
w["test"]="16*3,40*41,41*44,58*0,63*1,71*0,72*35,81*1";
w["tested"]="41*0";
w["tested/operated"]="41*0";
w["testing"]="32*0";
w["text"]="15*0,16*0,29*0,30*0,32*0,35*0,36*0,37*0,38*0,39*0,40*0,41*0,44*0,47*0,49*0,58*0,59*0,66*0,71*0,73*0,83*0,93*0,97*1";
w["than"]="35*1,37*0,45*2,49*0,51*0,59*1,80*0,83*0";
w["them"]="89*0,90*0,91*0,92*0";
w["therefore"]="71*0";
w["those"]="94*0,95*0";
w["three"]="45*1,49*0,75*0";
w["threshold"]="35*1,59*0,63*0,73*52,81*0";
w["thresholds"]="73*2";
w["throughput"]="51*0,83*0";
w["time"]="0*6,17*93,18*0,19*0,27*5,31*0,35*3,36*3,37*12,38*0,39*0,41*2,48*0,49*5,50*5,51*0,56*1,59*2,60*0,61*3,65*2,68*5,70*0,77*0,78*2,83*0";
w["time-stamped"]="56*0,65*0,68*0,78*0";
w["timeout"]="0*2,18*1,19*1,21*7,27*13,31*0,50*1,51*4,53*1,61*2,70*0";
w["times"]="21*0,27*1,31*0,36*0,45*0,46*3,53*0,70*0";
w["timing"]="36*0";
w["timings"]="31*0,63*0,70*0";
w["tips"]="65*5,78*5";
w["to/from"]="21*0";
w["toggle"]="18*0,19*0,45*0";
w["toggled"]="46*0";
w["toggles"]="66*1";
w["too"]="35*3";
w["tool"]="2*34,3*1,11*2,24*94,37*0,62*1,64*0,67*0,68*0,74*1,75*2";
w["top"]="51*1,83*1";
w["topic"]="3*0";
w["topics"]="2*0";
w["total"]="62*1,80*0";
w["track"]="44*0";
w["transmission"]="51*0,62*2";
w["transmit"]="21*3";
w["transmitted"]="21*4,51*0";
w["trigger"]="29*0";
w["triggered"]="35*0";
w["trip"]="59*4";
w["tripping"]="59*1";
w["trips"]="59*0";
w["try"]="53*0";
w["trying"]="30*0,32*0";
w["two"]="15*0,35*0,51*1,52*0,59*4,83*1,92*0";
w["two-phase-to-earth"]="59*0";
w["type"]="0*11,18*0,19*0,20*0,21*0,43*0,44*1,51*0,52*7,53*0,57*0,60*0,61*0,65*0,78*0,80*0,81*0,83*1,84*0,93*0,94*0,95*1,96*1,97*1";
w["udp"]="19*45,20*45,21*1";
w["unavailable"]="18*0,19*0";
w["undefined"]="57*0";
w["under"]="18*1,19*1,35*1,73*7,94*0,95*0,98*0";
w["underflow"]="35*1";
w["understand"]="2*0";
w["unique"]="0*0,52*2,53*0,61*0,80*0";
w["unit"]="35*0,59*2";
w["unless"]="37*0";
w["unlimited"]="51*0";
w["unsolicited"]="18*0,19*2,20*1,27*20,50*0";
w["unsuccessful"]="18*0,19*0";
w["until"]="32*0,36*0,37*0,73*0,96*0";
w["unwanted"]="36*1,38*1,71*0";
w["up"]="15*0,16*0,51*0,57*3,68*0,73*1,75*0,80*1,83*0";
w["update"]="27*0,50*0,56*1,65*1,68*1,78*1";
w["updated"]="66*0";
w["updates"]="11*0";
w["upgrade"]="74*35";
w["upload"]="3*0,96*1,98*30";
w["uploaded"]="98*0";
w["uploading"]="98*60";
w["upper"]="35*0,80*0";
w["uptime"]="68*1";
w["use"]="11*0,15*1,16*1,21*1,27*0,29*1,30*1,32*1,35*0,39*3,40*1,41*1,44*0,47*2,49*1,55*0,56*0,57*0,58*1,59*1,62*1,65*0,66*0,68*0,71*0,73*0,78*0,85*0,89*1,90*1,92*0";
w["used"]="0*1,3*0,16*0,27*0,29*1,32*0,35*1,36*0,37*0,39*0,51*3,53*0,59*1,60*1,62*0,64*0,71*0,74*0,83*1,96*0,97*0";
w["user"]="0*1,43*0,61*0,63*0,75*40,80*1";
w["user-defined"]="80*0";
w["users"]="22*0,77*0,79*0";
w["uses"]="3*0";
w["using"]="3*0,21*1,41*0,47*0,49*0,68*0,71*0,73*0,74*1";
w["utilities"]="13*30,72*0,74*0";
w["valid"]="18*0,19*0,27*1,37*0";
w["validate"]="18*1,19*1,27*0,45*1";
w["value"]="18*1,19*1,21*3,22*92,27*8,29*0,31*1,35*26,36*11,37*37,38*1,49*12,51*5,52*7,53*0,57*0,62*3,64*0,66*1,70*1,73*6,79*92,80*6";
w["values"]="0*0,11*0,22*0,31*1,35*2,38*9,45*0,46*0,48*0,49*11,63*2,65*2,70*0,78*2,79*0";
w["variation"]="27*11,85*1,86*1";
w["variety"]="16*0";
w["various"]="77*0";
w["vds"]="59*2";
w["version"]="2*0,24*0,44*1,56*0,68*4,74*3,97*1";
w["view"]="21*0,23*0,25*0,27*0,28*0,29*9,30*9,32*9,39*9,42*1,44*0,47*9,49*9,51*1,52*1,53*1,54*0,55*1,56*0,58*9,59*9,68*0,69*0,71*9,76*32,77*113,80*2";
w["view-only"]="51*0,53*1";
w["viewable"]="75*0";
w["viewing"]="21*0,23*0,25*0,28*0,42*1,51*0,54*0,69*0";
w["virtual"]="15*0,29*2,32*0,35*34,36*34,37*35,38*31,65*0,66*0,78*153,79*93,80*42,84*0,89*35,90*35,91*35,92*34,97*0";
w["visible"]="44*0,83*0,93*0,97*1";
w["vital"]="35*0";
w["vodafone"]="0*0";
w["voltage"]="39*0,59*25";
w["voltages"]="59*0";
w["vpis"]="59*2";
w["wait"]="0*3,27*0,51*0,61*3";
w["waiting"]="0*1,51*0";
w["want"]="89*0,90*0";
w["way"]="15*0,35*0,37*0";
w["we"]="62*1";
w["web"]="74*2";
w["well"]="45*0";
w["what"]="37*0";
w["when"]="18*1,19*1,30*0,32*1,35*1,36*0,37*5,39*4,41*3,45*0,46*0,49*0,50*0,52*0,53*0,58*0,59*0,66*1,68*0,73*6,74*0,80*0,85*0,86*0";
w["where"]="37*0,80*0";
w["whether"]="98*0";
w["which"]="0*0,18*1,19*1,27*0,35*1,36*0,52*2,59*0,61*0,62*0";
w["while"]="37*0,51*0,53*0";
w["width"]="37*0";
w["window"]="11*0,80*0";
w["windows"]="56*1,65*1,68*1,78*1";
w["withdraw"]="18*0,19*0";
w["within"]="35*1,50*0,80*0";
w["without"]="51*0,83*0";
w["wizard"]="3*0,81*0,89*0,90*0,91*0,92*0,96*0";
w["wizards"]="14*30";
w["word"]="53*1";
w["write"]="3*10,98*0";
w["writing"]="98*60";
w["xml"]="3*1";
w["yellow"]="65*1,78*1";
w["you"]="0*2,2*0,15*0,16*1,17*1,18*0,19*0,20*0,21*0,22*1,23*0,24*0,25*0,26*0,27*0,28*0,29*0,30*0,31*0,32*0,33*0,34*0,36*0,37*1,39*0,40*0,41*0,42*0,43*0,44*2,45*0,46*0,47*0,48*0,49*1,50*0,51*1,52*1,53*0,54*0,55*0,56*0,57*0,58*0,59*0,60*0,61*0,62*0,63*0,64*1,65*0,66*0,67*0,68*0,69*0,70*0,71*0,72*0,73*0,74*0,75*0,76*1,77*1,78*0,79*1,80*0,81*3,82*0,83*3,84*2,85*1,86*1,87*1,88*0,89*2,90*2,91*1,92*1,93*0,94*0,95*0,96*3,97*4";
w["your"]="81*0,96*1,97*0";
w["yourself"]="96*0";
w["zip"]="74*1";

