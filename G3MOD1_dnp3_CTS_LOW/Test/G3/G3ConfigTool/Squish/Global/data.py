def load_tsv_data(filename):
    comboboxes = []
    checkboxes = []
    for row, record in enumerate(testData.dataset(filename)):
        field_names = testData.fieldNames(record)
        combobox = testData.field(record, "combobox")
        if combobox!="":
            comboboxes.append(combobox)
        checkbox = testData.field(record, "checkbox")
        if checkbox!="":
            checkboxes.append(checkbox)
    return[comboboxes, checkboxes]

