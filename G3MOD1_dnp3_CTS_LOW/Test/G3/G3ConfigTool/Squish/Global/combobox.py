def test_combobox_binding(pane_container_name, ignore_list):
    launch_login_open_page(pane_container_name)
    comboboxes = find_widgets(pane_container_name)[2]
    sizes = determine_combo_sizes(comboboxes)
    max_combo_length = 0
    for size in sizes:
        if size > max_combo_length:
            max_combo_length = size
    save_exit(full_path)
    for selection in range(max_combo_length):
        launch_login_open_page(pane_container_name)
        comboboxes = find_widgets(pane_container_name)[2]
        selections = []
        for combobox in range(len(comboboxes)):
            selections.append(set_combobox_item(selection, combobox, sizes, comboboxes))
        save_exit(full_path)
        launch_login_load_page(pane_container_name)
        comboboxes = find_widgets(pane_container_name)[2]
        results = read_comboboxes(comboboxes)[0]
        labels = read_comboboxes(comboboxes)[1]
        exit_sure(full_path)
        for i in range(len(results)):
            if not check_ignored(labels[i], ignore_list):
                if selections[i]!=None:
                    test.compare(results[i], selections[i], "compare " + labels[i] + " combobox value after binding")
            else:
                test.log(labels[i] + " ignored")


def determine_combo_sizes(comboboxes):
    max_combo_length = 0;
    sizes = []
    for combobox in comboboxes:
        model = combobox.getModel()
        size = model.getSize();
        sizes.append(size)
    return sizes


def set_combobox_item(selection, combobox_index, sizes, comboboxes):
    if sizes[combobox_index]>selection:
        combobox = comboboxes[combobox_index]
        if(combobox.isShowing()):
            list_items = get_combo_list(combobox)
            caption = list_items[selection].getCaption()
            mouseClick(waitForObjectItem(":Gemini3 Configuration Tool.ComboBox.list_JList", caption), 47, 7, 0, Button.Button1)
            return combobox.getSelectedIndex()
        else:
            return None


def get_combo_list(combobox):
    mouseClick(combobox, 11, 5, 0, Button.Button1)
    list = findObject(":Gemini3 Configuration Tool.ComboBox.list_JList")
    return object.children(list)


def read_comboboxes(comboboxes):
    combobox_values = []
    labels = []
    for combobox in comboboxes:
        if combobox.isShowing() and combobox.isEnabled():
            combobox_values.append(combobox.getSelectedIndex())
            symbolic_name = objectMap.symbolicName(combobox)
            split_symbolic_name = symbolic_name.split(" ")
            caption_index = symbolic_name.index("caption")
            caption = symbolic_name[caption_index:]
            end_index = caption.index("\' ")
            caption = caption[:end_index]
            caption = caption[10:-1]
            labels.append(caption.strip())
    return [combobox_values, labels]

