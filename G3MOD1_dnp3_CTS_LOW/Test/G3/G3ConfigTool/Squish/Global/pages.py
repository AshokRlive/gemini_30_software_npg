import __builtin__


tool =              ":Gemini3 Configuration Tool"
config_type =       "Configuration.SCADA Protocol"
config_tree =       ":Configuration_ConfTree"
dnp3_channel_tcp =  ".DNP3.Channel-TCP."
dnp3 =              ".DNP3."
config_button =     ".Configuration_JButton"


click_button_cmd =      "clickButton"
expand_cmd =            "expand"
mouse_click_cmd =       "mouseClick"


session_pane_container =            ":Gemini3 Configuration Tool_JXTaskPaneContainer" 
channel_pane_container =            ":Channel_JXTaskPaneContainer"
channel_settings_pane_container =   ":Channel Settings_JXTaskPaneContainer"
channel_settings_tab =              ":Gemini3 Configuration Tool.Channel Settings_TabProxy"


def expand_dnp3(full_path, suffix1):
    open_item([click_button_cmd, tool + full_path + config_button])
    open_item([expand_cmd, config_tree + suffix1, config_type])
    obj_name = config_tree + suffix1
    return open_item([expand_cmd, obj_name, config_type + dnp3])


def open_scada_protocol(full_path, suffix1):
    item = expand_dnp3(full_path, suffix1)
    item_obj = open_item([expand_cmd, config_tree + suffix1, config_type + dnp3_channel_tcp])
    scadas = object.children(item_obj)
    protocol_name = determine_nodename(scadas[0])
    open_item([mouse_click_cmd, config_tree + suffix1, config_type + dnp3_channel_tcp + protocol_name])


def open_DNP3_channel(full_path, suffix1):
    item = expand_dnp3(full_path, suffix1)
    scadas = object.children(item)
    protocol_name = determine_nodename(scadas[0])
    item_name =  config_type + dnp3 + protocol_name
    obj_name = config_tree + suffix1
    open_item([mouse_click_cmd, obj_name, item_name])


def open_DNP3_channel_settings(full_path, suffix1):
    open_DNP3_channel(full_path, suffix1)
    tab = insert_full_path(channel_settings_tab, full_path)
    clickTab(waitForObject(tab))


def open_page_and_expand(full_path, suffix1, pane_container):
    if pane_container==session_pane_container:
        open_scada_protocol(full_path, suffix1)
    elif pane_container==channel_settings_pane_container:
        open_DNP3_channel_settings(full_path, suffix1)
    elif pane_container==channel_pane_container:
        open_DNP3_channel(full_path, suffix1)
    expand_panes(pane_container)


def launch_login_open_page(pane_container):
    launch_and_login_offline()
    create_new_from_scratch()
    open_page_and_expand(no_path, "", pane_container)


def launch_login_load_page(pane_container):
    launch_and_login_offline()
    open_file(filename)
    open_page_and_expand(full_path, "_2", pane_container)

