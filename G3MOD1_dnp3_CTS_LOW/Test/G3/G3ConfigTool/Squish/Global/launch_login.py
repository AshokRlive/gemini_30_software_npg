def launch_config_tool():
    startApplication("G3ConfigTool-0.8.1.jar")
    
    
def login_offline():
    clickButton(waitForObject(":Login.Offline_JButton"))
    
    
def launch_and_login_offline():
    launch_config_tool()
    login_offline()

