def determine_nodename(item):
    properties = item.getValue()
    return properties.nodename

    
def open_item(item):
    type = item[0]
    if type == "clickButton":
        item_obj = waitForObject(item[1])
        clickButton(item_obj)
        return item_obj
    elif type == "expand":
        item_obj = waitForObjectItem(item[1], item[2])
        expand(item_obj)
        return item_obj
    elif type == "mouseClick":
        item_obj = waitForObjectItem(item[1], item[2])
        mouseClick(item_obj, 18, 10, 0, Button.Button1)
        return item_obj


def open_all_modules():
    
    general_configuration_name       ="General Configuration"
    module_list_name                 ="Module List"    
    virtual_points_name              ="Virtual Points"    
    control_logic_list_name          ="Control Logic List"    
    scada_protocol_management_name   ="SCADA Protocol Management"    
    field_devices_management_name    ="Field Devices Management"
    comms_devices_management_name    ="Comms Devices Management"    
    ports_name                       ="Ports"
    user_account_management_name     ="User Account Management"    
    misc_name                        ="Miscellaneous"
    
    config_tree = ":Configuration_ConfTree"
    list = waitForObject(config_tree)
    list_children = object.children(list)
    tree_items = object.children(list_children[0])
    for tree_item in tree_items:
        if tree_item.getValue().toString() == general_configuration_name:
            pass
        elif tree_item.getValue().toString() == module_list_name:
            module_list(tree_item)
        elif tree_item.getValue().toString() == virtual_points_name:
            pass
        elif tree_item.getValue().toString() == control_logic_list_name:
            control_logic_list(tree_item)
        elif tree_item.getValue().toString() == scada_protocol_management_name:
            pass
        elif tree_item.getValue().toString() == field_devices_management_name:
            pass
        elif tree_item.getValue().toString() == comms_devices_management_name:
            pass
        elif tree_item.getValue().toString() == ports_name:
            pass
        elif tree_item.getValue().toString() == user_account_management_name:
            pass
        elif tree_item.getValue().toString() == misc_name:
            pass
        

def module_list(tree_item):
    expand(tree_item)
    modules = object.children(tree_item)
    for module in modules:
        mouseClick(module, 40,10,0, Button.Button1)
        tab_pane = waitForObject(":Gemini3 Configuration Tool_JTabbedPane")
        tabs = object.children(tab_pane)
        for tab in tabs:
            class_name = className(tab)
            if class_name == "com_froglogic_squish_awt_TabProxy":
                clickTab(tab)
                test_textfield_binding(tab, [])


def control_logic_list(tree_item):
    expand(tree_item)
    modules = object.children(tree_item)
    for module in modules:
        mouseClick(module, 40,10,0, Button.Button1)