checkbox_type = "javax.swing.JCheckBox"
combobox_type = "javax.swing.JComboBox"
formatted_textfield_type = "javax.swing.JFormattedTextField"
textfield_type = "javax.swing.JTextField"
collapsable_pane_type = "com.l2fprod.common.swing.JCollapsiblePane"
task_pane_type = "org.jdesktop.swingx.JXTaskPane"
panel_type = "javax.swing.JPanel"


def find_widgets(pane_container_name):
    textfields = []
    checkboxes = []
    comboboxes = []
    panes = object.children(find_pane(pane_container_name))
    for pane in panes:
        add_panel_widgets(pane, textfields, checkboxes, comboboxes)
        if is_task_pane(pane):
            task_pane_children = object.children(pane)
            collapsible_pane = task_pane_children[0]
            collapsible_pane_children = object.children(collapsible_pane)
            collapsible_pane_children_wrapper = object.children(collapsible_pane_children[0])
            widgets = object.children(collapsible_pane_children_wrapper[0])
            for widget in widgets:
                textfields, checkboxes, comboboxes = update_lists(widget, textfields, checkboxes, comboboxes)
                if is_collapsible_pane(widget):
                    children = object.children(widget)
                    children = object.children(children[0])
                    children = object.children(children[0])
                    for child in children:
                         textfields, checkboxes, comboboxes = update_lists(child, textfields, checkboxes, comboboxes)
                         add_panel_widgets(child, textfields, checkboxes, comboboxes)                         
    return [textfields, checkboxes, comboboxes]


def add_panel_widgets(child, textfields, checkboxes, comboboxes):
    if is_panel(child):
        children = object.children(child)
        for child in children:
            textfields, checkboxes, comboboxes = update_lists(child, textfields, checkboxes, comboboxes)  


def find_pane(pane_container_name):
    if object.exists(pane_container_name):
        return findObject(pane_container_name)
    elif object.exists(pane_container_name + "_2"):
        return findObject(pane_container_name + "_2")
    else:
        return findObject(insert_full_path(pane_container_name, full_path))


def update_lists(child, textfields, checkboxes, comboboxes):
    textfields = is_textfield_add(child, textfields)
    checkboxes = is_checkbox_add(child, checkboxes)
    comboboxes = is_combobox_add(child, comboboxes)
    return textfields, checkboxes, comboboxes


def is_checkbox_add(obj, checkboxes):
    if is_checkbox(obj):
        checkboxes.append(obj) 
    return checkboxes


def is_checkbox(obj):
    type = obj.getClass()
    if type.getName() == checkbox_type:
        return True
    else:
        return False


def is_combobox_add(obj, comboboxes):
    if is_combobox(obj):
        comboboxes.append(obj) 
    return comboboxes


def is_combobox(obj):
    type = obj.getClass()
    if type.getName() == combobox_type:
        return True
    else:
        return False


def is_textfield_add(obj, textfields):
    if is_textfield(obj):
        textfields.append(obj) 
    return textfields
                    

def is_textfield(obj):
    type = obj.getClass()
    if type.getName() == formatted_textfield_type or type.getName() == textfield_type:
        return True
    else:
        return False


def is_collapsible_pane(obj):
    type = obj.getClass()
    if type.getName() == collapsable_pane_type:
        return True
    else:
        return False


def is_task_pane(obj):
    type = obj.getClass()
    if type.getName() == task_pane_type:
        return True
    else:
        return False
    
    
def is_panel(obj):
    type = obj.getClass()
    if type.getName() == panel_type:
        return True
    else:
        return False

