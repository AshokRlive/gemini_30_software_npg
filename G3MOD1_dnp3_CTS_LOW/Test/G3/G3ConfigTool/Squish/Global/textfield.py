source(findFile("scripts", "names.py"))
source(findFile("scripts", "widgets.py"))


def test_textfield_binding(pane_container_name, ignore_list):
    launch_login_open_page(pane_container_name)
    textfields = find_widgets(pane_container_name)[0]
    original_values = read_textfields("", textfields)[0]
    test_data = modify_textfield_data(original_values)
    set_textfields("", test_data, textfields)
    save_exit(full_path)
    launch_login_load_page(pane_container_name)
    textfields = find_widgets(pane_container_name)[0]
    results = read_textfields(full_path, textfields)[0]
    labels = read_textfields(full_path, textfields)[1]
    exit_sure(full_path)
    for i in range(len(results)):
        if not check_ignored(labels[i], ignore_list):
            test.compare(results[i], test_data[i], "compare " + labels[i] +" textbox value after binding")
        else:
            test.log(labels[i] + " ignored")


def set_textfields(full_path, test_data, textfields):
    i=0
    for textfield in textfields:
        if textfield.isShowing() and textfield.isEnabled():
            mouseClick(textfield, 10, 10, 0, Button.Button1)
            type(textfield, "<Ctrl+A>")
            test_value = test_data[i]
            type(textfield, test_value)
            i+=1


def read_textfields(full_path, textfields):
    textfield_values = []
    textfield_names = []
    for textfield in textfields:
        if textfield.isShowing() and textfield.isEnabled():
            textfield_values.append(textfield.getText())
            textfield_names.append(determine_label(textfield))
    return [textfield_values, textfield_names]


def determine_label(textfield):
    symbolic_name = objectMap.symbolicName(textfield)
    split_symbolic_name = symbolic_name.split(" ")
    if symbolic_name.find("caption") !=-1:
        label_index = symbolic_name.index("caption")
        label = symbolic_name[label_index:]
        end_index = label.index("\' ")
        label = label[:end_index]
        label = label[10:-1]
        return label.strip()
    elif symbolic_name.find("Tool.") !=-1:
        label_index = symbolic_name.index("Tool.") + 5
    else:
        label_index = symbolic_name.index("xml.") + 4
    label = symbolic_name[label_index:]
    label_end_index = label.index("_")
    return label[:label_end_index]


def modify_textfield_data(data):
    modified_data = []
    for i in range(len(data)):
        value = data[i]
        if is_number(value):
            int_value = __builtin__.int(value)
            if int_value>1:
                int_value = int_value-1
            else:
                int_value = int_value +1
            modified_data.append(str(int_value))
        elif is_string(value):
            newValue = "new"+value
            modified_data.append(newValue)
    return modified_data
    
