def test_checkbox_binding(pane_container_name, ignore_list):
    test_checkbox(True, pane_container_name, ignore_list)
    test_checkbox(False, pane_container_name, ignore_list)
    
    
def test_checkbox(selected, pane_container_name, ignore_list):
    launch_login_open_page(pane_container_name)
    checkboxes = find_widgets(pane_container_name)[1]
    set_checkboxes(checkboxes, selected)
    save_exit(full_path)
    launch_login_load_page(pane_container_name)
    checkboxes = find_widgets(pane_container_name)[1]
    results = read_checkboxes(checkboxes)[0]
    labels = read_checkboxes(checkboxes)[1]
    exit_sure(full_path)
    for i in range(len(results)):
        if not check_ignored(labels[i], ignore_list):
            test.compare(results[i], __builtin__.int(selected == True), "compare " + labels[i] + "checkbox value after binding")
        else:
            test.log(labels[i] + " ignored")


def check_ignored(label, ignore_list):
    for item in ignore_list:
        if label == item:
            return True
    return False
        

def read_checkboxes(checkboxes):
    checkbox_values = []
    labels = []
    for checkbox in checkboxes:
        if checkbox.isShowing() and checkbox.isEnabled():
            checkbox_values.append(checkbox.isSelected())
            labels.append(checkbox.getLabel().strip())
    return [checkbox_values, labels]
        
    
def set_checkbox(checkbox, selected):
    if checkbox.isShowing() and checkbox.isEnabled():
        if (checkbox.isSelected()==False and selected):
            mouseClick(checkbox, 10, 10, 0, Button.Button1)
        elif checkbox.isSelected()==True and selected==False:
            mouseClick(checkbox, 10, 10, 0, Button.Button1)
    
    
def set_checkboxes(checkboxes, selected):
    for checkbox in checkboxes:
        set_checkbox(checkbox, selected)

