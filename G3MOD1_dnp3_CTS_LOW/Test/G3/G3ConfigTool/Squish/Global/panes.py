def expand_panes(parent_name):
    snooze(1)
    if object.exists(parent_name):
        panes = waitForObject(parent_name)
    elif object.exists(parent_name + "_2"):
        panes = waitForObject(parent_name + "_2")
    else:
        panes = waitForObject(insert_full_path(parent_name, full_path))
    pane_children = object.children(panes)
    for pane in pane_children:
        if pane.isShowing():
            type = pane.getClass()
            if type.getName() == "org.jdesktop.swingx.JXTaskPane":
                collapsed = pane.isCollapsed()
                if collapsed==1:
                    mouseClick(pane, 158, 6, 0, Button.Button1)

