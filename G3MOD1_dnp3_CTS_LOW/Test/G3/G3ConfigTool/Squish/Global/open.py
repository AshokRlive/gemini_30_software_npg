def open_file(filename):
    activateItem(waitForObjectItem(":Gemini3 Configuration Tool_JMenuBar", "File   "))
    activateItem(waitForObjectItem(":File   _JMenu", "New"))
    activateItem(waitForObjectItem(":New_JMenu", "Load from a file..."))
    mouseClick(waitForObjectItem(":Open Configuration File_JList", filename+".xml"), 32, 8, 0, Button.Button1)
    doubleClick(waitForObjectItem(":Open Configuration File_JList", filename+".xml"), 32, 8, 0, Button.Button1)

