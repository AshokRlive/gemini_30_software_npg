new_config = ":New Configuration"


def create_new_from_scratch():
    new()
    finish()
    
    
def create_new_from_scratch_full():
    new()
    mouseClick(waitForObject(new_config+".[SCM] Switch Control Module_JCheckBox"), 10, 15, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".[DSM] Dual Switch Module_JCheckBox"), 16, 10, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".[FPM] Fault Passage Module_JCheckBox"), 10, 6, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".[IOM] IO Module_JCheckBox"), 10, 7, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".Backplane Off-Local-Remote Switch Fitted_JCheckBox"), 10, 9, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".Fan Fitted_JCheckBox"), 8, 13, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".Quantity:_BasicArrowButton"), 9, 6, 0, Button.Button1)
    mouseClick(waitForObjectItem(new_config+".ComboBox.list_JList", "2"), 6, 5, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".Quantity:_BasicArrowButton_2"), 6, 13, 0, Button.Button1)
    mouseClick(waitForObjectItem(new_config+".ComboBox.list_JList", "2"), 9, 6, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".Quantity:_BasicArrowButton_3"), 11, 12, 0, Button.Button1)
    mouseClick(waitForObjectItem(new_config+".ComboBox.list_JList", "2"), 13, 6, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".Quantity:_BasicArrowButton_4"), 5, 14, 0, Button.Button1)
    mouseClick(waitForObjectItem(new_config+".ComboBox.list_JList", "2"), 3, 4, 0, Button.Button1)
    clickButton(waitForObject(new_config+".Next >_JButton"))
    
    mouseClick(waitForObject(new_config+"_NewConfigStep3"), 17, 62, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".Full (For testing purpose only)_JRadioButton"), 5, 7, 0, Button.Button1)
    clickButton(waitForObject(new_config+".Next >_JButton"))
    
    mouseClick(waitForObject(new_config+".Custom_JRadioButton"), 9, 4, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".Dummy Switch Control Logic_JCheckBox"), 9, 9, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".Fan Test_JCheckBox"), 14, 8, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".Switch Control Logic_JCheckBox"), 13, 13, 0, Button.Button1)
    mouseClick(waitForObject(new_config+".Digital IO Control Logic_JCheckBox"), 5, 7, 0, Button.Button1)
    clickButton(waitForObject(new_config+".Next >_JButton"))
    
    mouseClick(waitForObject(new_config+".DNP3_JRadioButton"), 6, 7, 0, Button.Button1)
    clickButton(waitForObject(new_config+".Next >_JButton"))
    
    mouseClick(waitForObject(new_config+".ModBus_JRadioButton"), 9, 15, 0, Button.Button1)
    finish()


def new():
    clickButton(waitForObject(":Gemini3 Configuration Tool.Configuration_JButton"))
    clickButton(waitForObject(":Gemini3 Configuration Tool.New..._JSplitButton"))
    clickButton(waitForObject(new_config+".Next >_JButton"))


def finish():
    clickButton(waitForObject(new_config+".Finish_JButton"))

