menu_bar = ":Gemini3 Configuration Tool_JMenuBar"
exit_button = "Exit"
ok_button = ":Save.OK_JButton"
file_menu = ":File   _JMenu"
quit_yes_button = ":Quit.Yes_JButton"
save_as_button = ".Save As_JButton"
file = "File   "
save_as_item = "Save As..."
overwrite_yes = ":Overwrite.Yes_JButton"
save_config_file = ":Save Configuration File"
textfield = "_JTextField"
list = "_JList"
j_menu_bar = "_JMenuBar"


def save_exit(full_path):
    save_as(filename)
    exit_sure(full_path)


def save():
    activateItem(waitForObjectItem(menu_bar, file))
    activateItem(waitForObjectItem(file_menu, save_as_item))
    mouseClick(waitForObject(save_config_file + "_BasicArrowButton"), 16, 14, 0, Button.Button1)
    mouseClick(waitForObjectItem(save_config_file + ".ComboBox.list_JList", "Gemini3 RTU Configuration(.xml)"), 194, 10, 0, Button.Button1)
    mouseClick(waitForObjectItem(save_config_file + list, "test scada protocol1.xml"), 104, 5, 0, Button.Button1)
    doubleClick(waitForObjectItem(save_config_file + list, "test scada protocol1.xml"), 104, 5, 0, Button.Button1)
    clickButton(waitForObject(overwrite_yes))
    clickButton(waitForObject(ok_button))


def save_as(filename):
    activateItem(waitForObjectItem(menu_bar, file))
    activateItem(waitForObjectItem(file_menu, save_as_item))
    mouseClick(waitForObject(save_config_file + ".Look in:_JComboBox"), 311, 11, 0, Button.Button1)
    mouseClick(waitForObject(save_config_file + ".Look in:_BasicArrowButton"), 7, 7, 0, Button.Button1)
    mouseClick(waitForObjectItem(save_config_file + ".ComboBox.list_JList", "squish g3 config tool saves"), 96, 6, 0, Button.Button1)
    mouseClick(waitForObject(save_config_file + "_BasicArrowButton"), 5, 6, 0, Button.Button1)
    mouseClick(waitForObjectItem(save_config_file + ".ComboBox.list_JList", "Gemini3 RTU Configuration(.xml)"), 161, 4, 0, Button.Button1)
    mouseClick(waitForObject(save_config_file + textfield), 93, 15, 0, Button.Button1)
    type(waitForObject(save_config_file + textfield), filename)
    clickButton(waitForObject(save_config_file + save_as_button))
    clickButton(waitForObject(overwrite_yes))
    clickButton(waitForObject(ok_button))


def exit_sure(full_path):
    if object.exists(menu_bar):
        activateItem(waitForObjectItem(menu_bar, file))
    if object.exists(file_menu):
        activateItem(waitForObjectItem(file_menu, exit_button))
    folder = "D:\\squish g3 config tool saves\\"
    menu_bar_name = ":Gemini3 Configuration Tool" + full_path + j_menu_bar
    activateItem(waitForObjectItem(menu_bar_name, file))
    if object.exists(file_menu + "_1"):
        activateItem(waitForObjectItem(file_menu + "_1", exit_button))
    elif object.exists(file_menu + "_2"):
        activateItem(waitForObjectItem(file_menu + "_2", exit_button))
    elif object.exists(file_menu + "_3"):
        activateItem(waitForObjectItem(file_menu + "_3", exit_button))
    elif object.exists(file_menu + "_4"):
        activateItem(waitForObjectItem(file_menu + "_4", exit_button))
    elif object.exists(file_menu + "_5"):
        activateItem(waitForObjectItem(file_menu + "_5", exit_button))
    elif object.exists(file_menu + "_6"):
        activateItem(waitForObjectItem(file_menu + "_6", exit_button))
    clickButton(waitForObject(quit_yes_button))

