def get_open_module_names():
    modules = get_open_modules()
    values = []
    for i in range(modules.getSize()):
        value = modules.get(i)
        values.append(value.getShortName())
    return values

def get_open_modules():
    list = waitForObject(":Gemini3 Configuration Tool_JList")
    model = list.getModel()
    return model.getList()

def add_module(name):
    clickButton(waitForObject(":Gemini3 Configuration Tool.Add Module_JButton"))
    mouseClick(waitForObject(":New Module.Module Type_BasicArrowButton"), 8, 12, 0, Button.Button1)
    mouseClick(waitForObjectItem(":New Module.ComboBox.list_JList", name), 79, 12, 0, Button.Button1)
    ok = waitForObject(":New Module.OK_JButton")
    cancel = waitForObject(":New Module.Cancel_JButton")
    if ok.isEnabled():
        clickButton(ok)
    else:
        clickButton(cancel)
        
def add_all_modules():
    mouseClick(waitForObjectItem(":Configuration_ConfTree", "Configuration.Module [3]"), 28, 10, 0, Button.Button1)
    clickButton(waitForObject(":Gemini3 Configuration Tool.Add Module_JButton"))
    mouseClick(waitForObject(":New Module.Module Type_BasicArrowButton"), 8, 12, 0, Button.Button1)
    list = waitForObject(":New Module.ComboBox.list_JList")
    clickButton(waitForObject(":New Module.Cancel_JButton"))
    modules = object.children(list)
    for name in modules:
        open = get_open_module_names()
        if not name.getText() in open:
            add_module(name.getText())