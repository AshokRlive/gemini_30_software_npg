def main():
    startApplication("G3ConfigTool-0.8.0.jar")
    
    source(findFile("scripts", "logon.py"))
    login_offline()
    
    source(findFile("scripts", "configuration.py"))
    create_new_from_scratch()
    
    source(findFile("scripts", "modules.py"))
    add_all_modules()
    source(findFile("scripts", "control_logic.py"))
    open_c_logic_tree()
    add_all_c_logic()
    open_all_c_logic()
    #set_inputs()
    
    set_pltu_output()
    
    source(findFile("scripts", "save.py"))
    save_as()
    #:Validation Result_JXEditorPane
    
    source(findFile("scripts", "exit.py"))
    exit_sure()
    
    
    

