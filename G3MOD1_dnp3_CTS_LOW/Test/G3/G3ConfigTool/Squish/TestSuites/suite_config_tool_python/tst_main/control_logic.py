def open_c_logic_tree():
    tree = waitForObject(":Configuration_ConfTree")
    model = tree.getModel()
    root = model.getRoot()
    children = root.children()
    achild = root.getChildAt(3)
    num_c_logics = achild.getChildCount();
    mouseClick(waitForObjectItem(":Configuration_ConfTree", "Configuration.Control Logic ["+ str(num_c_logics) +"]"), 14, 10, 0, Button.Button1)
    
def add_all_c_logic():
    clickButton(waitForObject(":Gemini3 Configuration Tool.Add Control Logic_JButton"))
    mouseClick(waitForObject(":New control logic.Type:_BasicArrowButton"), 9, 14, 0, Button.Button1)
    list = waitForObject(":New control logic.ComboBox.list_JList")
    model = list.getModel()
    size = model.getSize()
    clickButton(waitForObject(":New control logic.Cancel_JButton"))
    for i in range(size):
        open = get_open_c_logic_names()
        element = model.getElementAt(i)
        name = element.getFullName()
        if not name in open:
            add_c_logic(name)
    
def add_c_logic(name):  
    clickButton(waitForObject(":Gemini3 Configuration Tool.Add Control Logic_JButton"))
    mouseClick(waitForObject(":New control logic.Type:_BasicArrowButton"), 9, 14, 0, Button.Button1)
    mouseClick(waitForObjectItem(":New control logic.ComboBox.list_JList", name), 114, 3, 0, Button.Button1)
    ok = findObject(":New control logic.OK_JButton")
    cancel = waitForObject(":New control logic.Cancel_JButton")
    if ok.isEnabled():
        clickButton(ok)
    else:
        clickButton(cancel)
    
def open_all_c_logic():
    list = waitForObject(":Gemini3 Configuration Tool_JList")
    model = list.getModel()
    c_logics = model.getList()
    for i in range(model.getSize()):
        open_c_logic_tree()
        open_c_logic_item(list, c_logics.get(i))
        set_inputs()
        
def get_open_c_logic():
    list = waitForObject(":Gemini3 Configuration Tool_JList")
    model = list.getModel()
    return model.getList()

def get_open_c_logic_names():
    c_logics = get_open_c_logic()
    values = []
    for i in range(c_logics.getSize()):
        value = c_logics.get(i)
        values.append(value.getName())
    return values
    
def open_c_logic_item(list, name):
    obj_item = waitForObjectItem(list, str(name) + "\t\t")
    mouseClick(obj_item, 112, 19, 0, Button.Button1)
    doubleClick(obj_item, 112, 19, 0, Button.Button1)
    
def set_inputs():
    canvas = waitForObject(":Gemini3 Configuration Tool_Canvas")
    children = object.children(canvas)
    for child in children:
        if(className(child)=="javax_swing_JButton" and child.getText()=="<null>" and (":Gemini3" == objectMap.symbolicName(child)[0:8]) ):
            symbolicName = objectMap.symbolicName(child)
            set_input(symbolicName)
    if object.exists(":Select Channel.Select_JButton"):
        clickButton(waitForObject(":Select Channel.Select_JButton"))
    #:Select Channel.Select_JButton
    #:Select Channel.Cancel_JButton
    
    #mouseClick(waitForObject(":Select.Select a output control logic:_BasicArrowButton"), 3, 7, 0, Button.Button1)
    #mouseClick(waitForObjectItem(":Select.ComboBox.list_JList", "[7] DSM 1"), 97, 4, 0, Button.Button1)
    #:channelsTable.0_0_TableItemProxy
    #:channelsTable.0_0_TableItemProxy
    #:Select Channel.Select_JButton
    #:Gemini3 Configuration Tool.Output_JButton
    #:Gemini3 Configuration Tool.Circuit Breaker_JButton
    #:Gemini3 Configuration Tool.Switch 1_JButton
    
def set_input(symbolicName):
    test.log(symbolicName)
    clickButton(waitForObject(symbolicName))
    if object.exists(":Virtual Point Selector_JTable"):
        mouseClick(waitForObjectItem(":Virtual Point Selector_JTable", "0/0"), 27, 9, 0, Button.Button1)
        clickButton(waitForObject(":Virtual Point Selector.Select_JButton"))
    elif object.exists(":Select Channel.channelsTable_JTable"):
        mouseClick(waitForObjectItem(":Select Channel.channelsTable_JTable", "0/0"), 23, 9, 0, Button.Button1)
        clickButton(waitForObject(":Select Channel.Select_JButton"))
    elif object.exists(":Select.OptionPane.comboBox_JComboBox"):
        clickButton(waitForObject(":Select.OK_JButton"))
    elif object.exists(":Select.Cancel_JButton"):
        clickButton(waitForObject(":Select.Cancel_JButton"))
    elif object.exists("Select Channel.Cancel_JButton"):
        clickButton(waitForObject("Select Channel.Cancel_JButton"))
            
    
def set_pltu_output():
    clickButton(waitForObject(":Gemini3 Configuration Tool.Circuit Breaker_JButton"))
    dragItemBy(waitForObject(":Select.OptionPane.body_JPanel"), 73, 41, -10, 10, Modifier.None, Button.Button1)
    clickButton(waitForObject(":Select.OK_JButton"))