def save_as():
    activateItem(waitForObjectItem(":Gemini3 Configuration Tool_JMenuBar", "File   "))
    activateItem(waitForObjectItem(":File   _JMenu", "Save As..."))
    
    snooze(1)
    
    if object.exists(":Save Configuration File_BasicArrowButton"):
        arrow_button = waitForObject(":Save Configuration File_BasicArrowButton")
        mouseClick(arrow_button, 15, 16, 0, Button.Button1)
        mouseClick(waitForObjectItem(":Save Configuration File.ComboBox.list_JList", "Gemini3 RTU Configuration(.xml)"), 213, 4, 0, Button.Button1)
        mouseClick(waitForObject(":Save Configuration File_JTextField"), 79, 9, 0, Button.Button1)
        type(waitForObject(":Save Configuration File_JTextField"), "test.xml")
        type(waitForObject(":Save Configuration File_JTextField"), "<Return>")
        clickButton(waitForObject(":Save.OK_JButton"))
        test.passes("Saved")
    elif object.exists(":Validation Result_JXErrorPane"):
        validation_dialog = waitForObject(":Validation Result_JXErrorPane")
        ok_button = waitForObject(":Validation Result.Close_JButton")
        mouseClick(ok_button)
        test.fail("Could not validate")
    
    
    