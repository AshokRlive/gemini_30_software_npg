source(findFile("scripts", "launch_login.py"))
source(findFile("scripts", "configuration.py"))
source(findFile("scripts", "save_exit.py"))
source(findFile("scripts", "open.py"))
source(findFile("scripts", "convert.py"))
source(findFile("scripts", "data.py"))
source(findFile("scripts", "textfield.py"))
source(findFile("scripts", "combobox.py"))
source(findFile("scripts", "checkbox.py"))
source(findFile("scripts", "panes.py"))
source(findFile("scripts", "names.py"))
source(findFile("scripts", "tree.py"))
source(findFile("scripts", "pages.py"))


def main():
    #pages = [channel_pane_container, channel_settings_pane_container, session_pane_container]

    #test_textfield_binding(channel_pane_container, [])
    #test_checkbox_binding(channel_pane_container, ["Any"])
    #test_combobox_binding(channel_pane_container, [])
    
    #test_textfield_binding(channel_settings_pane_container, [])
    #test_checkbox_binding(channel_settings_pane_container, [])
    #test_combobox_binding(channel_settings_pane_container, [])
    
    #test_textfield_binding(session_pane_container, [])
    #test_checkbox_binding(session_pane_container, [])
    #test_combobox_binding(session_pane_container, [])
    
    launch_and_login_offline()
    create_new_from_scratch_full()
    
    config_tree = ":Configuration_ConfTree"
    tree= waitForObject(config_tree)
    tree_children = object.children(tree)
    open_all_modules()
    
