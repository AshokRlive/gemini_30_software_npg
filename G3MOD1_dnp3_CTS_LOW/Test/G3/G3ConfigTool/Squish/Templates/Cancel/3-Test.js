/******************************************************************************
 *  Run the actual test.
 *  
 *  This is to execute the test actionsd, but NOT verify the actions were 
 *  successful, the verification is carried out by TestCheck()
 *
 *  This should NOT include any messages or error handling since these are 
 *  dealt with by the Global.js script 
 *
 ******************************************************************************/
function Test() {
    clickButton(waitForObject(":Login.Cancel_JButton"));
}
