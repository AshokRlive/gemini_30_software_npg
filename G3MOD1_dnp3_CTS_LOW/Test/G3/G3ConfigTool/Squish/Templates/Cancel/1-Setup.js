/******************************************************************************
 *  Test preparation, sets up the environment ready to run the test.
 *
 *  This should NOT include any messages or error handling since these are 
 *  dealt with by the Global.js script 
 *
 ******************************************************************************/
function Setup() {
    startApplication("G3ConfigTool.jar");
    waitForObject(":Login_JDialog");
}
