/******************************************************************************
 *  Check the test environment is correctly set up.
 *
 *  This should generate a SINGLE test.fail(message) to indicate the failure 
 *  point, but success should be silent
 *
 ******************************************************************************/
function SetupCheck() {
    if (object.exists(":Login_JDialog")) {
        //Success = silence
    } else {
        test.fail("Setup failed - No log in dialog available.");
    }
}
