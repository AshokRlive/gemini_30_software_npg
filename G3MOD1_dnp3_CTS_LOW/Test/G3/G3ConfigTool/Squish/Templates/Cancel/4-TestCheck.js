/******************************************************************************
 *  Check the test executed correctly with the right response.
 *
 *  This should generate a SINGLE test.fail(message) to indicate the failure 
 *  point, but success should be silent
 *
 ******************************************************************************/
function TestCheck() {
    try {
        waitForObject(":Login_JDialog");
        test.fail("Test failed - Login dialog not closed");
    } catch (e) {
      //Success = silence
    }
}
