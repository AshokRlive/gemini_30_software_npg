/******************************************************************************
 *  Do not edit this script
 *
 *  Template script to load and run Global.js, specifically the global() 
 *  function.
 *
 ******************************************************************************/
function main() {
    source(findFile("scripts", "Global.js"));
    global();
}
