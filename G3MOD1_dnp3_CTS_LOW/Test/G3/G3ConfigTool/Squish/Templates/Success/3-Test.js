/******************************************************************************
 *  Run the actual test.
 *  
 *  This is to execute the test actionsd, but NOT verify the actions were 
 *  successful, the verification is carried out by TestCheck()
 *
 *  This should NOT include any messages or error handling since these are 
 *  dealt with by the Global.js script 
 *
 ******************************************************************************/
function Test() {

    type(waitForObject(":Login_JTextField"), "Admin");
    type(waitForObject(":Login_JPasswordField"), "");
    type(waitForObject(":Login_JTextField_2"), "<Ctrl+A>");
    type(waitForObject(":Login_JTextField_2"), "10.11.3.32");

    clickButton(waitForObject(":Login.Login_JButton"));

    waitForObject(":Gemini3 Configuration Tool_JFrame");
}
