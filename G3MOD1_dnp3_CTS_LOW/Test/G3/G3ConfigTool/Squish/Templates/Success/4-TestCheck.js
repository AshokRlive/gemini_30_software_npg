/******************************************************************************
 *  Check the test executed correctly with the right response.
 *
 *  This should generate a SINGLE test.fail(message) to indicate the failure 
 *  point, but success should be silent
 *
 ******************************************************************************/
function TestCheck() {
    if (object.exists(":Gemini3 Configuration Tool_JFrame")) {
        //Success = silence
    } else {
        test.fail("Test failed - Unsuccessful login.");
    }
}
