/* Load and run test scripts */
function runner() {
    loadScripts();
    runTest();
}

/* Load the script for each test step (Setup, SetupCheck, Test etc*/
function loadScripts() {
    source(findFile("scripts", "1-Setup.js"));
    source(findFile("scripts", "2-SetupCheck.js"));
    source(findFile("scripts", "3-Test.js"));
    source(findFile("scripts", "4-TestCheck.js"));
    source(findFile("scripts", "5-Setdown.js"));
    source(findFile("scripts", "6-SetdownCheck.js"));
    source(findFile("scripts", "7-Shutdown.js"));
    source(findFile("scripts", "8-ShutdownCheck.js"));
}

/* Run each step script in turn */
function runTest() {
    Setup();
    SetupCheck();
    Test();
    TestCheck();
    Setdown();
    SetdownCheck();
    Shutdown();
    ShutdownCheck();
}
