/* Return system to base state */
function Setdown() {
    doSetdown();
    msgSetdown();
}

/* Restore the system */
function doSetdown() {
    clickButton(waitForObject(":Login.Cancel_JButton"));
}

/* Message completion of setdown */
function msgSetdown() {
    test.pass("Setdown execution complete, checking...");
}
