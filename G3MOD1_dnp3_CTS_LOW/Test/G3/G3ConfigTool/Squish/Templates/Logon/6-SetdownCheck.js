/* Test setdown successfully returned the system to base state */
function SetdownCheck() {
    if (object.exists(":Login_JDialog")) {
        test.fail("Setdown failed - Loin dialog still open");
    } else {
        test.pass("Setdown passed");
    }
}
