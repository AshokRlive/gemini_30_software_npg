/* Shutdown COnfigTool */
function Shutdown() {
    doShutdown();
    msgShutdown();
}

/* Shut down ConfigTool */
function doShutdown() {
    //Shutdown executed earlier
}

/* Message test harness that shudown has completed */
function msgShutdown() {
    test.pass("Shutdown execution complete, checking...");
}
