/* Test ConfigTool has closed */
function ShutdownCheck() {
    if (object.exists(":Login_JDialog")) {
        test.fail("Shutdown failed - Login dialog still open");
    } else if (object.exists(":Gemini3 Configuration Tool_JFrame")) {
        test.fail("Shutdown failed - Config tool still open");
    } else {
        test.pass("Shutdown passed");
    }
}
