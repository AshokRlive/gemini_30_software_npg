/* Run the test */
function Test() {
    doTest();
    msgTest();
}

/* Carry out the actual test actions */
function doTest() {
    type(":Login_JTextField", "Admin");
    type(":Login_JPasswordField", "BadPass");
    clickButton(":Login.Login_JButton");
}

/* Message that the test actions were completed */
function msgTest() {
    test.pass("Test execution complete, checking...");
}
