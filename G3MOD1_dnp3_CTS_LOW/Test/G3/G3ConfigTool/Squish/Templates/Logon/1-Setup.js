/* Prep for the test */
function Setup() {
    doSetup();
    msgSetup();
}

/* Run the setup step */
function doSetup() {
    startApplication("G3ConfigTool.jar");
    waitForObject(":Login_JDialog");
}

/* Return completion message to the test harness */
function msgSetup() {
    test.pass("Setup execution complete, checking...");
}
