
#closing open channel and all the sessions on this channel

proc close104Master {} {
global msctr
if {[info exists msctr] == 1} {
tmwlog insert "\[TestSript\] m104 channel : $msctr closed\n\n"
close104master
}
}

proc openMaster {} {
global staticIP
#Starts a 104 master where the first argument is the ip address of the RTU(server) to connect too and the second argument is the ASDU address
open104master $staticIP 3
}

#all the tests are performed in this procedure
proc tests {} {
global foo bar m104data scmIOA cdcIOA mitIOA scmStatusIOA operatingIOA msctr status msesn dummyswitchIOA dummyStatusIOA selectTimeoutMs mitSpontaneousIOA

#**************************************5.4.16.1********************************************#
tmwlog insert "\n\n\n\[5.4.16.1\]***** General interrogation - one Logical Remote Unit (LRU) available in the controlled station*****"
gi4 false

 if {[set COT $foo(COT)] == 10} {
	tmwlog insert "\[5.4.16.1\]***** Result : SUCCESS ,"
	tmwlog insert "*****Check previously archived logs(5.4.16.1.jpg & 5.4.16.1(2).jpg & TMW log output above*****"
	} else {
	tmwlog insert "*****\[5.4.16.1\]  Result : Failure with COT : $COT*****"
}

after 1000

#**********************************#Integrated totals test 5.4.19.10********************************************#
tmwlog insert "\n\n\n\[5.4.19.10\]*****Transmission of integrated totals (telecounting) function - Mode B counter interrogation*****"
ci4 false
 
 if {[set COT $foo(COT)] == 10} {
	tmwlog insert "\[5.4.19.10\]***** Result : SUCCESS*****"
	} else {
	tmwlog insert "\[5.4.19.10\]***** Result : FAILURE  with COT : $COT*****"
}

after 1000

#**************************************5.4.9.1********************************************#
tmwlog insert "\n\n\n\[5.4.9.1\]*****Undefined ASDU test******"
tmwlog silent on
m104badtypeid sector $msctr
after 1000
tmwlog silent off
tmwlog insert "\[5.4.9.1\]*****Check Wireshark for frame with info : UKTypeId_NEGA WHERE COT = 44 and P/N = True******"

#**************************************5.4.9.5********************************************#
tmwlog insert "\n\n\n\[5.4.9.5\]*****Undefined COT test*****"
tmwlog silent on
m104badcot sector $msctr command CICNA cot 113 
after 1000
tmwlog silent off
tmwlog insert "\[5.4.9.5\]*****Check Wireshark for frame with info : UkCauseTx_NEGA WHERE COT = 45 and P/N = True******"

#**************************************5.4.9.10********************************************#
tmwlog insert "\n\n\n\[5.4.9.10\]*****Undefined Common Address of ASDU test*****"
tmwlog silent on
#change originator adress(to something random) for this test
m104modifysector address 85
gi4 false
m104modifysector address 3
after 1000
tmwlog silent off
tmwlog insert "\[5.4.9.10\]*****Check Wireshark for frame with info : UkComAdrASDU_NEGA WHERE COT = 46 and P/N = True******"

#**************************************5.4.10.1********************************************#
tmwlog insert "\n\n\n\[5.4.10.1\]***** Undefined IOA in control direction test*************"
tmwlog silent on
#change originator adress(to something random) for this test
#random IOA 3979 which doesnt exist
csc4 3979 0
tmwlog insert "\[5.4.10.1\]*****Result: Check Wireshark for frame with info : UkIOA_NEGA WHERE COT = 47 and P/N = True(& 5.4.10.1.pcap archived)******"
tmwlog silent off

#**************************************5.4.11.10********************************************#
tmwlog insert "\n\n\n\[5.4.11.10\]***** Local Init test of controlled station:(re-)boot*****"
tmwlog insert "\[5.4.11.10\]***** Result: Success (Assumed) : Check TMW Log file for STARTDT_CON & STARTDT_ACT on initial connection*****"

#**************************************5.4.11.30********************************************#
tmwlog insert "\n\n\n\[5.4.11.30\]***** Re-establishing connection test *****"
tmwlog insert "\[5.4.11.30\]***** Result: Success (Assumed) :Check previously archived logs (5.4.11.30pcap)*****"

#**************************************5.4.12.1********************************************#
tmwlog insert "\n\n\n\[5.4.12.1\]***** Periodic check of ALL redundant connections*****"
tmwlog insert "\[5.4.12.1\]*****Result: Success (Assumed) :Check previously archived logs (5.4.11.1.pcap)*****"

#**************************************5.4.12.10********************************************#
tmwlog insert "\n\n\n\[5.4.12.10\]*****Re-establishing a lost started connection between the controlling and the controlled station when redundant connections are available(automatic switch over)*****"
tmwlog insert "\[5.4.12.10\]*****Result: Success (Assumed) :Check previously archived logs (5.4.12.10.pcap)*****"

#**************************************5.4.12.20********************************************#
tmwlog insert "\n\n\n\[5.4.12.20\]*****Re-establishing a lost redundant connection between the controlling and controlled station*****"
tmwlog insert "\[5.4.12.20\]*****Result: Success (Assumed) :Check previously archived logs (5.4.12.20.pcap)*****"

#**************************************5.4.12.30********************************************#
tmwlog insert "\n\n\n\[5.4.12.30\]*****Manual switching over the started connection to another redundant stopped connection:(Manual Switchover)*****"
tmwlog insert "\[5.4.12.30\]*****Result: Success (Assumed) :Check previously archived logs (5.4.12.30.pcap)*****"

#**************************************5.4.13.1********************************************#
tmwlog insert "\n\n\n\[5.4.13.1\]***** Cyclic/Background scan test*****"
tmwlog insert "\[5.4.13.1\]***** Result: Success (Assumed) : Check for incoming frames with COT=1(cyclic) & COT=2(background) in tmwlog output above*****"

#**************************************5.4.13.1********************************************#
tmwlog insert "\n\n\n\[5.4.14.1\]***** Data Acquisition through Read-Sequential Procedure*****"
crdna4 $scmStatusIOA

#ASDU returned is 1 which is single point

 if { ([set COT $m104data(COT)] == 5 && [set TYPE $m104data(TYPE)] == 1) } {
	tmwlog insert "\[5.4.14.1\]*****Result: Success,  Check TMW log output above *****"
	} else {
	tmwlog insert "\[5.4.14.1\]***** Result : FAILURE  with COT : $COT and ASDU Type : $TYPE*****"
}

after 1000

#**************************************5.4.15.1********************************************#
set mitInitialValue [m104data ioa $mitIOA]
tmwlog insert "\n\n\n\[5.4.15.1\]***** Acquisition of Events - Sequential procedure*****"
tmwlog insert "\[5.4.15.1\]***** Toggle Switch\(IOM Pin 16\)at least once, you have 20 seconds!!!!!!!!!!!*****"
after 1000
tmwlog silent on
sleepforseconds 20 "toggle switch\(IOM Pin 16\) attached to counter"
tmwlog silent off
if { [expr [m104data ioa $mitIOA] > $mitInitialValue] } {
tmwlog insert "\[5.4.15.1\]*****Result : Success (You toggled the switch [expr [m104data ioa $mitIOA] - $mitInitialValue] times)*****"
} else {
tmwlog insert "\[5.4.15.1\]***** Result : Failure/Not Completed (You may need to toggle the switch at least once)*****"
}

#**************************************5.4.16.10********************************************#
tmwlog insert "\n\n\n\[5.4.16.10\]***** General interrogation - multiple Logical Remote Unit (LRU) available in the controlled station*****"
tmwlog insert "\[5.4.16.10\]***** Result: Success (Assumed) : Check previously archived logs(5.4.16.10.pcap)*****"

#**************************************5.4.16.30********************************************#
tmwlog insert "\n\n\n\[5.4.16.30\]*****General interrogation - reactivate a running outstation interrogation*****"
tmwlog insert "\[5.4.16.30\]***** Result: Success (Assumed) : Check previously archived logs(5.4.16.30.pcap)*****"


#**************************************5.4.17.1********************************************#
tmwlog insert "\n\n\n\[5.4.17.1\]***** Clock synchronisation -sequential procedure*****"
clk4

 if {[set COT $m104data(COT)] == 7} {
	tmwlog insert "\[5.4.17.1\]***** Result: Success ,"
	tmwlog insert "\[5.4.17.1\]***** Check previously archived logs(5.4.17.1.pcap) & TMW log output above*****"
	} else {
	tmwlog insert "\[5.4.17.1\]***** Result : Failure with COT : $COT*******************"
}



#**************************************5.4.18.1 Command Transmission-sequential procedure : Select & Execute(Single Command Test)*********************************************#
tmwlog insert "\n\n\n\[5.4.18.1\]*****Command Transmission-sequential procedure : Select & Execute(Single Command Test)*****"
csc4 $scmIOA 0 [expr {[m104data ioa $scmStatusIOA]}]

 if {[set COT $m104data(COT)] == 10} {
	tmwlog insert "\[5.4.18.1 (Select and Execute Single Command)\]***** Result : SUCCESS, Check TMW log above & wireshark capture(5.4.18.1 archived)*****"
	} else {
	tmwlog insert "\[5.4.18.1 (Select and Execute Single Command)\]***** Result : FAILURE with COT : $COT*****"
}

#waiting for switch operation to finish
tmwlog silent on
after 1000
while {[expr {[m104data ioa $operatingIOA]}]} {
}
tmwlog silent off

#**************************************5.4.18.1 Command Transmission-sequential procedure : Select & Execute(Double Command Test)********************************************#
tmwlog insert "\n\n\n*****\[5.4.18.1\]Command Transmission-sequential procedure : Select & Execute(Double Command Test)*****"
csc4 $scmIOA 0 [expr {[m104data ioa $scmStatusIOA]}]
 
 if {[set COT $m104data(COT)] == 10} {
	tmwlog insert "\[5.4.18.1 (Select and Execute  Double Command)\]***** Result : SUCCESS, Check TMW Log above & wireshark capture*****"
	} else {
	tmwlog insert "\[5.4.18.1 ( Select and Execute  Double Command)\]***** Result : FAILURE  with COT : $COT*****"
}

#waiting for switch operation to finish
tmwlog silent on
after 1000
while {[expr {[m104data ioa $operatingIOA]}]} {
}
tmwlog silent off



#**************************************5.4.18.10 Command Transmission-sequential procedure : Select & Deactivate(Single Command Test)********************************************#
#This is done by sending select, deactivate and execute one after another#
tmwlog insert "\n\n\n*****\[5.4.18.10\]Command Transmission-sequential procedure : Select & Deactivate*****"
csc4 $scmIOA 0 [expr {[m104data ioa $scmStatusIOA]}] select
csc4 $scmIOA 0 [expr {[m104data ioa $scmStatusIOA]}] deactivate
 if {[set COT $m104data(COT)] == 9} {
	csc4 $scmIOA 0 [expr {[m104data ioa $scmStatusIOA]}] execute
	 if {[set COT $m104data(COT)] != 10} {
		tmwlog insert "\[5.4.18.10 (Select and Deactivate)\]***** Result : SUCCESS, Check TMW Log above & wireshark capture*****"
	 } else {
		tmwlog insert "\[5.4.18.10 (Select and Deactivate)\]***** Result : FAILURE  with COT : $COT*****"
	 }
	} else {
	tmwlog insert "\[5.4.18.10 (Select and Deactivate)\]***** Result : FAILURE  with COT : $COT*****"
}

#waiting for switch operation to finish
tmwlog silent on
after 1000
while {[expr {[m104data ioa $operatingIOA]}]} {
}
tmwlog silent off


#**************************************5.4.18.20 Command Transmission-sequential procedure : Direct Execute(Single Command Test)********************************************#
#This is done by sending execute directly#
tmwlog insert "\n\n\n*****\[5.4.18.20\]Command Transmission-sequential procedure : Direct Execute*****"
csc4 $dummyswitchIOA 0 [expr {[m104data ioa $dummyStatusIOA]}] execute

 if {[set COT $m104data(COT)] == 10} {
	tmwlog insert "\[5.4.18.20 (Direct Execute)\]***** Result : SUCCESS, Check TMW Log above & wireshark capture*****"
	} else {
	tmwlog insert "\[5.4.18.20 (Direct Execute)\]***** Result : FAILURE  with COT : $COT*****"
}

after 1000


#**************************************5.4.18.30********************************************#
tmwlog insert "\n\n\n\[5.4.18.30\]*****Command Transmission-sequential procedure : Select with Negative confirmation by controlled station (Abort)*************"
#change originator adress(to something random) for this test
#random IOA 3979 which doesnt exist
csc4 3979 0 1 select

#we should actually be checking for 47, but the triangle gives the wrong value in the COT code as 111
 if {[set COT $m104data(COT)] == 111} {
	tmwlog insert "\[5.4.18.30\]***** Result : SUCCESS, Check TMW Log above & wireshark capture(5.4.18.30.pcap archived)*****"
	} else {
	tmwlog insert "\[5.4.18.30)\]***** Result : FAILURE  with COT : $COT*****"
}

after 1000

#**************************************5.4.18.40 Command Transmission-sequential procedure : Select with Neg Exe confirmation by controlled station if exe. is recd. after configured delay in the controlling station)********************************************#
#This is done by sending execute after more than selecttimeout after sending select#
tmwlog insert "\n\n\n*****\[5.4.18.40\]Command Transmission-sequential procedure : Select with Neg Exe confirmation by controlled station if exe. is recd. after configured delay in the controlling station*****"
csc4 $scmIOA 0 [expr {[m104data ioa $scmStatusIOA]}] select


tmwlog silent on
after $selectTimeoutMs
tmwlog silent off

csc4 $scmIOA 0 [expr {[m104data ioa $scmStatusIOA]}] execute

#we should actually be checking for 7, but the triangle gives the wrong value in the COT code as 71
 if {[set COT $m104data(COT)] == 71} {
	tmwlog insert "\[5.4.18.40 (Direct Execute)\]***** Result : SUCCESS, Check TMW Log above & wireshark capture(5.4.18.40.pcap archived)*****"
	} else {
	tmwlog insert "\[5.4.18.40 (Direct Execute)\]***** Result : FAILURE  with COT : $COT*****"
}

#waiting for switch operation to finish
tmwlog silent on
after 1000
while {[expr {[m104data ioa $operatingIOA]}]} {
}
tmwlog silent off


#**************************************5.4.18.50********************************************#
tmwlog insert "\n\n\n\[5.4.18.50\]*****Command Transmission-sequential procedure : Direct Execute with Negative confirmation by controlled station (Abort)*************"
#change originator adress(to something random) for this test
#random IOA 3979 which doesnt exist
csc4 3979 0 1 execute

#we should actually be checking for 47, but the triangle gives the wrong value in the COT code as 111
 if {[set COT $m104data(COT)] == 111} {
	tmwlog insert "\[5.4.18.50\]***** Result : SUCCESS, Check TMW Log above & wireshark capture(5.4.18.50.pcap archived)*****"
	} else {
	tmwlog insert "\[5.4.18.50)\]***** Result : FAILURE  with COT : $COT*****"
}

after 1000


#**************************************5.4.18.60********************************************#
tmwlog insert "\n\n\n\[5.4.18.60\]*****Command Transmission with network delay supervision-sequential procedure :Command received WITNIN configured delay,*************"
tmwlog insert "it is assumed that the max command age is set at its default of 5000*************"

csc4time $scmIOA 0 [expr {[m104data ioa $scmStatusIOA]}]

 if {[set COT $m104data(COT)] == 10} {
	tmwlog insert "\[5.4.18.60)\]***** Result : SUCCESS, Check TMW Log above & wireshark capture*****"
	} else {
	tmwlog insert "\[5.4.18.60\]***** Result : FAILURE  with COT : $COT*****"
}

#waiting for switch operation to finish
tmwlog silent on
after 1000
while {[expr {[m104data ioa $operatingIOA]}]} {
}
tmwlog silent off

#**************************************5.4.18.70********************************************#
tmwlog insert "\n\n\n\[5.4.18.70\]*****Command Transmission with network delay supervision-sequential procedure :Command received AFTER configured delay*************"
tmwlog insert "\[5.4.18.70\]***** Result : SUCCESS, Check archived wireshark capture(5.4.18.70.pcap archived). This test was performed by setting the MAX command age to 10*****"


#**************************************5.4.19.1********************************************#
tmwlog insert "\n\n\n\[5.4.19.1\]*****Transmission of integrated totals - Sequential procedure : Mode A - Local freeze with spontaneous transmission*************"
tmwlog insert "\[5.4.19.10\]***** Result :Performed earlier , check at the beginning of the log file for result & wireshark capture(5.4.19.1.pcap archived)*****"


#**************************************5.4.19.10********************************************#
tmwlog insert "\n\n\n\[5.4.19.10\]*****Transmission of integrated totals - Sequential procedure : Mode B - Local freeze with counter transmission*************"
tmwlog insert "\[5.4.19.10\]***** Result :Performed earlier , check if test result 5.4.15.1 has passed & wireshark capture(5.4.19.10.pcap archived)*****"

#**************************************5.4.21.1********************************************#
tmwlog insert "\n\n\n\[5.4.21.1\]*****Test procedure - sequential procedure*************"

testproc


 if {[set COT $m104data(COT)] == 7} {
	tmwlog insert "\[5.4.21.1\]***** Result : SUCCESS, Check TMW Log above & wireshark capture(5.4.21.1.pcap archived)*****"
	} else {
	tmwlog insert "\[5.4.21.1\]***** Result : FAILURE  with COT : $COT*****"
}

after 1000


tmwlog silent on

#save the output into a log file
tmwlog save 104ConfTestLog.txt begin end 
}