#
# file: tmw104.tcl
# purpose: This file defines several procedures which can be used to
#  exercise an IEC 60870-5-104 master or slave device. Procedures are 
#  defined for both the master and slave. In addition, a procedure is 
#  defined at the bottom of this file that will exercise both master 
#  and slave sessions in the test harness connected using the TCP/IP 
#  loopback address.
#

# proc: open104master
# purpose: Open an IEC 60870-5-104 master session using IP loopback address
# comments: If talking to an external device modify this routine
#  to specify the required channel, session, and sector parameters.
proc open104master {hostip asduAddress} {
puts "open104master"
  global mchannel msesn msctr
  set mchannel [m104openchannel type tcp mode client host $hostip name m104]
  set msesn [m104opensession channel $mchannel]
  set msctr [m104opensector session $msesn address $asduAddress]
  return 0
}

# proc: close104master
# purpose: Close the IEC 60870-5-104 master session opened above
proc close104master {} {
  tmwlog pause 1
  global mchannel msesn msctr
  m104closesector sector $msctr
  m104closesession session $msesn
  m104closechannel channel $mchannel
  tmwlog pause 0
}

# proc: open104slave
# purpose: Open an IEC 60870-5-104 slave session
# comments: If talking to an external device modify this routine
#  to specify the required channel, session, and sector parameters.
proc open104slave {} {
  global schannel ssesn ssctr
  set schannel [s104openchannel type tcp mode server name s104]
  set ssesn [s104opensession channel $schannel]
  set ssctr [s104opensector session $ssesn]
}

# proc: close104slave
# purpose: Close the IEC 60870-5-104 slave session opened above
proc close104slave {} {
  global schannel ssesn ssctr
  s104closesector sector $ssctr
  s104closesession session $ssesn
  s104closechannel channel $schannel
}

# proc: open104
# purpose: Open an IEC 60870-5-104 master and slave session.
# comments: The default configurations assume the master and slave
#  will communicate with each other using the TCP/IP loopback.
proc open104 {} {
  open104slave
  open104master
}

# proc: close104
# purpose: Close the IEC 60870-5-104 sessions opened above.
proc close104 {} {
  close104master
  close104slave
}

# Slave Commands

# proc: sd4
# purpose: Show slave database
proc sd4 {} {
  global ssctr
  s104showdata sector $ssctr
}

# proc: msp1
# purpose: Set one or more single point values
# arguments:
#  start - first information object address
#  end - last information object address (0 = default to start)
#  eventsPerPoint - how many events to generate for each point
proc msp4 {{start 100} {end 0} {eventsPerPoint 1}} {
  global msctr ssctr
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set oldvalue [s104msp get sector $ssctr ioa $index value]
    for {set index1 0} {$index1 < $eventsPerPoint} {incr index1} {
      set oldvalue [expr !$oldvalue]
      s104msp set sector $ssctr ioa $index value $oldvalue
    }
  }
}
 
# proc: mdp4
# purpose: Set one or more double point values
# arguments:
#  start - first information object address
#  end - last information object address (0 = default to start)
#  eventsPerPoint - how many events to generate for each point
proc mdp4 {{start 200} {end 0} {eventsPerPoint 1}} {
  global msctr ssctr
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set oldvalue [s104mdp get sector $ssctr ioa $index value]
    for {set index1 0} {$index1 < $eventsPerPoint} {incr index1} {
      if {$oldvalue == 1} {
        set oldvalue 2
      } else {
        set oldvalue 1
      }
      s104mdp set sector $ssctr ioa $index value $oldvalue
    }
  }
}

# proc: mst4
# purpose: Set one or more step position values
# arguments:
#  start - first information object address
#  end - last information object address (0 = default to start)
#  eventsPerPoint - how many events to generate for each point
proc mst4 {{start 300} {end 0} {eventsPerPoint 1}} {
  global msctr ssctr
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set oldvalue [s104mst get sector $ssctr ioa $index value]
    for {set index1 0} {$index1 < $eventsPerPoint} {incr index1} {
      set oldvalue [expr $oldvalue + 1]
      s104mst set sector $ssctr ioa $index value $oldvalue
    }
  }
}

# proc: mbo4
# purpose: Set one or more bitstring values
# arguments:
#  start - first information object address
#  end - last information object address (0 = default to start)
#  eventsPerPoint - how many events to generate for each point
proc mbo4 {{start 400} {end 0} {eventsPerPoint 1}} {
  global msctr ssctr
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set oldvalue [s104mbo get sector $ssctr ioa $index value]
    for {set index1 0} {$index1 < $eventsPerPoint} {incr index1} {
      set oldvalue [expr $oldvalue + 1]
      s104mbo set sector $ssctr ioa $index value $oldvalue
    }
  }
}

# proc: addMeasurands4
# purpose: Add some measurands to the slave 101 sector
#  Adding measurands is complicated by the fact that each measurand
#  also has 1 Parameter Activation point and 4 Parameter of measured values points
#  in the simulated database. This requires a unique IOA for each of these. 
# arguments:
#  None
proc addMeasurands4 { } {

  # default database has MMENA IOAs 500-505, PACNA IOAs 1200-1212, PMENA IOAs 900-923
  s104mmena add ioa 506 pacnaIOA 1213 thresholdIOA 924 smoothingIOA 925 lowLimitIOA 926 highLimitIOA 927
  s104mmena add ioa 507 pacnaIOA 1214 thresholdIOA 928 smoothingIOA 929 lowLimitIOA 930 highLimitIOA 931
  s104mmena add ioa 508 pacnaIOA 1215 thresholdIOA 932 smoothingIOA 933 lowLimitIOA 934 highLimitIOA 935
  s104mmena add ioa 509 pacnaIOA 1216 thresholdIOA 936 smoothingIOA 937 lowLimitIOA 938 highLimitIOA 939
  
  # default database has MMENB IOAs 600-603, PMEMB IOAs 1000-1015
  s104mmenb add ioa 604 pacnaIOA 1217 thresholdIOA 1016 smoothingIOA 1017 lowLimitIOA 1018 highLimitIOA 1019
  s104mmenb add ioa 605 pacnaIOA 1218 thresholdIOA 1020 smoothingIOA 1021 lowLimitIOA 1022 highLimitIOA 1023
  
  # default database has MMENC IOAs 700-702, PMEMC IOAs 1100-1111
  s104mmenc add ioa 703 pacnaIOA 1219 thresholdIOA 1112 smoothingIOA 1113 lowLimitIOA 1114 highLimitIOA 1115
  s104mmenc add ioa 704 pacnaIOA 1220 thresholdIOA 1116 smoothingIOA 1117 lowLimitIOA 1118 highLimitIOA 1119
}

# proc: setParOfMeasurands4
# purpose: issue some commands to set parameter of measurands
#  Both the IOA and type must match that in the slave database
# arguments:
#  None
proc setParOfMeasurands4 { } {
  m104pmena ioa 900  type threshold value 111
  m104pmenb ioa 1019 type high      value 444
  m104pmenc ioa 1113 type smoothing value 555
}

# proc: mmena4
# purpose: Set one or more normalized measurand values
# arguments:
#  start - first information object address
#  end - last information object address (0 = default to start)
#  eventsPerPoint - how many events to generate for each point
proc mmena4 {{start 500} {end 0} {eventsPerPoint 1}} {
  global msctr ssctr
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set oldvalue [s104mmena get sector $ssctr ioa $index value]
    for {set index1 0} {$index1 < $eventsPerPoint} {incr index1} {
      set oldvalue [expr $oldvalue + 100]
      s104mmena set sector $ssctr ioa $index value $oldvalue
    }
  }
}

# proc: mmenb4
# purpose: Set one or more scaled measurand values
# arguments:
#  start - first information object address
#  end - last information object address (0 = default to start)
#  eventsPerPoint - how many events to generate for each point
proc mmenb4 {{start 600} {end 0} {eventsPerPoint 1}} {
  global msctr ssctr
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set oldvalue [s104mmenb get sector $ssctr ioa $index value]
    for {set index1 0} {$index1 < $eventsPerPoint} {incr index1} {
      set oldvalue [expr $oldvalue + 100]
      s104mmenb set sector $ssctr ioa $index value $oldvalue
    }
  }
}

# proc: mmenc4
# purpose: Set one or more floating point measurand values
# arguments:
#  start - first information object address
#  end - last information object address (0 = default to start)
#  eventsPerPoint - how many events to generate for each point
proc mmenc4 {{start 700} {end 0} {eventsPerPoint 1}} {
  global msctr ssctr
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set oldvalue [s104mmenc get sector $ssctr ioa $index value]
    for {set index1 0} {$index1 < $eventsPerPoint} {incr index1} {
      set oldvalue [expr $oldvalue + 100]
      s104mmenc set sector $ssctr ioa $index value $oldvalue
    }
  }
}

# proc: mit4
# purpose: Set one or more itegated totals values
# arguments:
#  start - first information object address
#  end - last information object address (0 = default to start)
#  eventsPerPoint - how many events to generate for each point
proc mit4 {{start 800} {end 0}} {
  global msctr ssctr
  
  if {$end == 0} {
    set end $start
  }
  
  set sector $ssctr
  
  for {set index $start} {$index <= $end} {incr index} {
    set oldvalue [s104mit get sector $ssctr ioa $index value]
    s104mit set sector $ssctr ioa $index value [expr $oldvalue + 1]
  }
}

# Master Commands

# proc: md4
# purpose: Show master database
proc md4 {} {
  global msctr
  m104showdata sector $msctr
}

# proc: gi4
# purpose: Issue a general interrogation request and optionally display
#  the results
proc gi4 {{display true}} {
  global msesn msctr stat foo bar 
  
  set stat 0
  m104unsol cmdvariable {set stat 1} datavariable foo session $msesn
  m104cicna cmdvariable {set stat 2} datavariable bar sector $msctr
  while {$stat != 2} {
    vwait stat
    if {$display} {
      puts "**** Showing Intermediate Results ****"
      showarray foo
    }
  }
  
  if {$display} {
    puts "**** GI Results ****"
    showarray bar
  }
  
  # Cancel unsolicited events
  m104unsol
}

# proc: ci4
# purpose: Issue a counter interrogation request and optionally display
#  the results
proc ci4 {{display true}} {
  global msesn msctr stat foo bar
  
  set stat 0
  m104unsol cmdvariable {set stat 1} datavariable foo session $msesn
  m104ccina cmdvariable {set stat 2} datavariable bar sector $msctr
  while {$stat != 2} {
    vwait stat
    if {$display} {
      puts "**** Showing Intermediate Results ****"
      showarray foo
    }
  }
  
  if {$display} {
    puts "**** CI Results ****"
    showarray bar
  }
  
  # Cancel unsolicited events
  m104unsol
}

# proc: frz4
# purpose: Issue a freeze counter request and wait for response
proc frz4 {} {
  global msctr ssctr stat m104data
  set stat 0
  m104ccina cmdvariable {set stat 1} sector $msctr qualifier freeze datavariable m104data
  vwait stat
}

# proc: csc4
# purpose: Issue one or more set single point value requests
# arguments:
#  start - first point to set
#  end - last point to set, 0 means only do 1
proc csc4 {{start 0} {end 0} {valueToSet 0} {modeparam "auto"}} {
  global msctr status m104data
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set status 1
    m104cscna statvariable status sector $msctr ioa $index value $valueToSet mode $modeparam datavariable m104data
    while {$status != 0} {
      vwait status
      if {$status > 1} {
		 return "Returned status = $status"
      #  return -code error "Returned status = $status" removed because -code option creates exception
      }
    }
  }
}

# proc: cdc4
# purpose: Issue one or more set double point value requests
# arguments:
#  start - first point to set
#  end - last point to set, 0 means only do 1
proc cdc4 {{start 0} {end 0} {valueToSet 0}} {
    global msctr status m104data
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set status 1
    m104cdcna statvariable status sector $msctr ioa $index value $valueToSet datavariable m104data
    while {$status != 0} {
      vwait status
      if {$status > 1} {
         return "Returned status = $status"
      #  return -code error "Returned status = $status" removed because -code option creates exception
      }
    }
  }
}

# proc: crc4
# purpose: Issue one or more set step position requests
# arguments:
#  start - first point to set
#  end - last point to set, 0 means only do 1
proc crc4 {{start 2300} {end 0}} {
  global msctr status
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set status 1
    m104crcna statvariable status sector $msctr ioa $index value up
    while {$status != 0} {
      vwait status
      if {$status > 1} {
        return -code error "Returned status = $status"
      }
    }
  }
}

# proc: cbo4
# purpose: Issue one or more set bitstring value requests
# arguments:
#  start - first point to set
#  end - last point to set, 0 means only do 1
proc cbo4 {{start 2400} {end 0}} {
  global msctr status
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set status 1
    set oldvalue 0
    m104cbona statvariable status sector $msctr ioa $index value [expr $oldvalue + 1]
    while {$status != 0} {
      vwait status
      if {$status > 1} {
        return -code error "Returned status = $status"
      }
    }
  }
}

# proc: csena4
# purpose: Issue one or more set normalized measurand requests
# arguments:
#  start - first point to set
#  end - last point to set, 0 means only do 1
proc csena4 {{start 2500} {end 0}} {
  global msctr status
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set status 1
    set oldvalue 0
    m104csena statvariable status sector $msctr ioa $index value [expr $oldvalue + 100]
    while {$status != 0} {
      vwait status
      if {$status > 1} {
        return -code error "Returned status = $status"
      }
    }
  }
}

# proc: csenb4
# purpose: Issue one or more set scaled measurand requests
# arguments:
#  start - first point to set
#  end - last point to set, 0 means only do 1
proc csenb4 {{start 2600} {end 0}} {
  global msctr status
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set status 1
    set oldvalue 0
    m104csenb statvariable status sector $msctr ioa $index value [expr $oldvalue + 100]
    while {$status != 0} {
      vwait status
      if {$status > 1} {
        return -code error "Returned status = $status"
      }
    }
  }
}

# proc: csenc4
# purpose: Issue one or more set floating point measurand requests
# arguments:
#  start - first point to set
#  end - last point to set, 0 means only do 1
proc csenc4 {{start 2700} {end 0}} {
  global msctr status
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set status 1
    set oldvalue 0
    m104csenc statvariable status sector $msctr ioa $index value [expr $oldvalue + 100]
    while {$status != 0} {
      vwait status
      if {$status > 1} {
	  return "Returned status = $status"
      #  return -code error "Returned status = $status"
      }
    }
  }
}

# proc: clk4
# purpose: Issue a clock synchronization request and wait for response
proc clk4 {} {
  global msctr status m104data
  
  set status 1
  m104ccsna statvariable status sector $msctr datavariable m104data
  while {$status != 0} {
    vwait status
    if {$status > 1} {
     return "Returned status = $status"
	 # return -code error "Returned status = $status"
    }
  }
}
 
# proc: ftdir4
# purpose: File transfer request the directory information from the slave 
proc ftdir4 {} {
  global status

  # Show the file directory information in the slave database 
  s104showdata
  
  # request directory from slave
  m104fscna ioa 0 cot request statvariable status
  vwait status
  
  # show the file directory information in the master database
  m104showdata
}

# proc: ftdelete4
# purpose: File Transfer. delete the specified file from slave and master database.
#  Have master send a command to delete a file on the slave
#  Delete the file from the master simulated database, using the TCL callback
#  function to delete the UNIX files (1 UNIX file per section)
# arguments:
#  ioa - information object address of file
#  fileName - name of file
#  verbose - if true, display information
proc ftdelete4 {{ioa 1800} {fileName 1} {verbose false}} {
  global status
  
  # register TCL callback function 
  if {$verbose} {puts "register TCL callback function"}
  m104file callback filecmd "fileCallback4 data4" filedata data4
 
  # request master to send delete to slave
  if {$verbose} {puts "send delete file to slave"}
  m104fscna ioa $ioa filename $fileName scq delfile statvariable status 
  vwait status
  
  # Delete file from master simulated database, including UNIX section files 
  if {$verbose} {puts "delete file and directory entry from master database"}
  m104file remove ioa $ioa filename $fileName 
  
  # clear TCL callback function
  if {$verbose} {puts "clear TCL callback function"}
  m104file callback 
} 

# proc: ftput4
# purpose: file transfer in the control direction using
#  the low level interface. This procedure will send and
#  receive the individual ASDUs necessary to transfer the file.
#  For data this procedure will pass a string to the testharness, 
#  it will not read from an actual or simulated database file.
# arguments:
#  ioa - information object address of file
#  fileName - name of file
#  fileLength - length of file
#  maxSectionLength - maximum length of each section
#  verbose - if true, display information
proc ftput4 {{ioa 321} {fileName 1} {fileLength 1600} {maxSectionLength 400} {verbose false}} {
  global status;

  set dataArray [string repeat "this is a test string,  " 10]
 
  set sectionName 0
  set fileBytesLeft $fileLength
  
  # since the simulated slave database as shipped only holds up to 1000 bytes
  # per section, check this here.
  if {$maxSectionLength > 1000} {
    return -code error "maxSectionLength $maxSectionLength exceeds 1000, this will fail"
  }

  # Send File Ready PDU to slave indicating ioa, name and length of file
  m104ffrna ioa $ioa filename $fileName filelength $fileLength statvariable status datavariable data
  vwait status
  if {$verbose} {showarray data}

  while {$fileBytesLeft > 0} {

    incr sectionName

    set sectionLength $maxSectionLength

    if {$sectionLength >$fileBytesLeft} {
      set sectionLength $fileBytesLeft
    }
      
    # send Section Ready PDU to slave indicating ioa, name, sectionName and length of section
    m104fsrna ioa $ioa filename $fileName sectionname $sectionName sectionlength $sectionLength statvariable status datavariable data
    vwait status
    if {$verbose} {showarray data}
  
    set sectionBytesLeft $sectionLength
  # 240 bytes is too large for 104 link layer frame
    set segmentLength 236
  
    while {$sectionBytesLeft >0} {
      if {$segmentLength > $sectionBytesLeft} {
        set segmentLength $sectionBytesLeft
      }
      
      if {$verbose} {puts "sectionBytesLeft = $sectionBytesLeft"}
      
      # send Segment PDU to slave containing segment data
      # send all of them one after the other
      m104fsgna ioa $ioa filename $fileName sectionname $sectionName sdata [string range $dataArray 0 [expr $segmentLength-1]]
      
      set sectionBytesLeft [expr $sectionBytesLeft - $segmentLength]
      set fileBytesLeft [expr $fileBytesLeft - $segmentLength]
    }
  
    # send Last Segment PDU indicating thats all for this section
    m104flsna ioa $ioa filename $fileName sectionname $sectionName lsq sectionxfer statvariable status datavariable data
    vwait status 
    if {$verbose} {showarray data}
  } 

  # send Last Section PDU indicating thats all for this file
  m104flsna ioa $ioa filename $fileName sectionname $sectionName lsq filexfer statvariable status datavariable data
  vwait status
  if {$verbose} {showarray data}
  
  # show the slave database
  s104showdata
}

# proc: ftget4
# purpose: file transfer in the monitor direction 
#  using low level interface. This procedure will send and
#  receive the individual ASDUs 
# arguments:
#  ioa - information object address of file
#  fileName - name of file
#  verbose - if true, display information
proc ftget4 {{ioa 1811} {fileName 1} {verbose false}} {
  global status;
  set sectionName 1
  
  # Send Call Directory PDU to request directory from slave
  m104fscna ioa 0 cot request statvariable status datavariable data
  vwait status
  if {$verbose} {showarray data}
  
  # Send Select File PDU to slave
  m104fscna ioa $ioa filename $fileName scq selfile sectionname $sectionName statvariable status datavariable data
  vwait status
  if {$verbose} {showarray data}
  if {$status != 0} {
    puts "Select File returned failure"
    return
  }
  
  # Send Call File PDU to slave
  m104fscna ioa $ioa filename $fileName scq callfile sectionname $sectionName statvariable status datavariable data
  vwait status
  if {$verbose} {showarray data}
  if {$status != 0} {
    puts "Select File returned failure"
    return
  }
  
  while {1} {
    # Send Call Section PDU to slave
    m104fscna ioa $ioa filename $fileName scq callsection sectionname $sectionName 

    # look at all of the FSGNA ASDUs
    # get all unsolicited messages
    set status 0
    m104unsol cmdvariable {set status 1} datavariable data
    while {$status != 2} {
      vwait status
      if {$verbose} {showarray data}
      
      # if FLSNA, exit from loop
      if {$data(TYPE) == 123} {
        set status 2
      }
    }
    # turn off reception of unsolicited messages
    m104unsol
  
    # after all segments in this section and FLSNA are received 
    # send ACK Section PDU
    m104fafna ioa $ioa filename $fileName afq acksection sectionname $sectionName statvariable status datavariable data
    vwait status
    if {$verbose} {showarray data}
    
    # if this is FSRNA Section Ready, get the next section
    if {$data(TYPE) == 121} {
      incr sectionName
    } else {
      break  
    }
  }
  
  set sectionName [expr $sectionName - 1]
  
  # after all sections are received
  # send ACK File PDU
  m104fafna ioa $ioa filename $fileName afq ackfile sectionname $sectionName statvariable status
  vwait status
}

# proc: fttest4
# purpose: 
# Issue a few file transfer commands 
#   Create a file on the slave with 5 sections
#   Issue a high level command to get a file from the slave, 
#   Put (send) it back to the slave.
# arguments:
#  ioa - information object address of file
#  fileName - name of file
#  verbose - if true, display information
proc fttest4 {{ioa 1800} {fileName 1} {verbose false}} {
  global status
  
  # remove file if it is already on slave
  # add one section, so the remove command won't fail if file is not there
  s104file add ioa $ioa filename $fileName sdata "this will be removed"
  s104file remove ioa $ioa filename $fileName
  
  # create file on slave containing 5 sections
  if {$verbose} {puts "create a file on the slave"}
  s104file add ioa $ioa filename $fileName sdata "Test string for section 1"
  
  set dataArray [string repeat "Test string for section 2, abcdefghijklmnopqrstuvwxyz" 4]
  s104file add ioa $ioa filename $fileName bdata dataArray
  
  s104file add ioa $ioa filename $fileName sdata "Test string for section 3"
  
  s104file add ioa $ioa filename $fileName sdata "Test string for section 4 1234567890"
  
  set dataArray [string repeat "Test string for section 5, abcdefghijklmnopqrstuvwxyz" 30]
  s104file add ioa $ioa filename $fileName bdata dataArray
 
  # wait briefly for directory FDRTA PDUs that may come as result of
  # s104file add
  set delay 0
  after 3000 {set delay 1}
  vwait delay
 
  # get the file from the slave
  if {$verbose} {puts "get the file from the slave"}
  set status 1
  m104file get ioa $ioa filename $fileName statvariable status
  while {$status == 1} {
    vwait status
  } 
  
  # wait briefly for directory FDRTA PDUs that may come as a result
  # of get 
  set delay 0
  after 3000 {set delay 1}
  vwait delay
  
  # put the file from the master back to the slave
  if {$verbose} {puts "put the file back to the slave"}
  set status 1
  m104file put ioa $ioa filename $fileName statvariable status
  while {$status == 1} {
      vwait status
  } 
  
  # show slave database
  s104showdata
}

# proc: fttest4_wcallback
# purpose:  Same as fttest1 but using TCL callback to read and write from
#   a UNIX file.
# Issue a few file transfer commands 
#   Create a file on the slave with 3 sections
#   Issue a high level command to get a file from the slave, 
#   Put (send) it back to the slave.
# arguments:
#  ioa - information object address of file
#  fileName - name of file
#  verbose - if true, display information
proc fttest4_wcallback {{ioa 1800} {fileName 1} {verbose false}} {
  global status
  
  # remove file if it is already on slave
  # add one section, so the remove command won't fail if file is not there
  s104file add ioa $ioa filename $fileName sdata "this will be removed"
  s104file remove ioa $ioa filename $fileName
  
  # create file on slave containing 3 sections, this will cause a directory to be
  # sent to master spontaneously.
  if {$verbose} {puts "create a file on the slave"}
  
  set dataArray [string repeat "Test string for section 1, abcdefghijklmnopqrstuvwxyz" 4]
  s104file add ioa $ioa filename $fileName bdata dataArray
  
  s104file add ioa $ioa filename $fileName sdata "Test string for section 2 1234567890"
  
  set dataArray [string repeat "Test string for section 3, abcdefghijklmnopqrstuvwxyz" 30]
  s104file add ioa $ioa filename $fileName bdata dataArray
 
  # wait briefly for directory FDRTA PDUs that may come as result of
  # s104file add
  set delay 0
  after 3000 {set delay 1}
  vwait delay
 
  # register TCL callback function 
  if {$verbose} {puts "register TCL callback function"}
  m104file callback filecmd "fileCallback4 data4" filedata data4
 
  # get the file from the slave
  if {$verbose} {puts "get the file from the slave"}
  m104file get ioa $ioa filename $fileName statvariable status
  vwait status
  
  # wait briefly for directory FDRTA PDUs that may come as a result
  # of get 
  set delay 0
  after 3000 {set delay 1}
  vwait delay
  
  # put the file from the master back to the slave
  if {$verbose} {puts "put the file back to the slave"}
  m104file put ioa $ioa filename $fileName statvariable status
  vwait status
  
  # clear TCL callback function
  if {$verbose} {puts "clear TCL callback function"}
  m104file callback 
  
  # show slave database
  s104showdata
}

# proc: fileCallback4
# purpose:
#  This callback function will allow the test harness to read and write
#  UNIX files for the master. There will be a separate UNIX file for each
#  section of the 101/104 file. The UNIX file names will be
#  file$ioa_$filename_sectionName.txt. 
#  ie for ioa 1800, fileName 1, sectionName 1 the UNIX file will be file1800_1_1.txt
# arguments:
#  data - array filled in by test harness
#  verbose - if true, display information
proc fileCallback4 {{data} {verbose false}} {
  global fd fileName
  upvar $data inarray
  
  if {$verbose} {puts "called fileCallback"}
  if {$verbose} {showarray inarray}
  
  # If first time initialize fileName and fd to 0 
  if {[info exists fileName] == 0} {
    set fileName 0
  }
    
  if {[info exists fd] == 0} {
    set fd 0
  } 
  
  # GET FILE LENGTH 
  if { $inarray(CMD) == 0 } {
    if {$verbose} {puts "command get file length file$inarray(FNAME).txt"}
    set sectionName 1
    set fileLength 0
    while { 1 } {
      set fileName file$inarray(IOA)_$inarray(FNAME)_$sectionName.txt
      if {[file exists $fileName]} {
        file stat $fileName info
        incr sectionName
        set fileLength [expr $fileLength + $info(size)]
      } else {
      break
      }
    }
    set inarray(FLEN) $fileLength
    if {$verbose} {puts "fileLength $fileLength"}
  }
  
  # GET SECTION LENGTH
  if { $inarray(CMD) == 1 } {
    if {$verbose} {puts "command get section length"}
    set sectionLength 0
    
    # set fileName based on input array fields
    set fileName file$inarray(IOA)_$inarray(FNAME)_$inarray(SECTNAME).txt
    
    if {[file exists $fileName]} {
      file stat $fileName info 
      set sectionLength $info(size)
      
      # open the file for reading
      set fd [open $fileName r]
      
      # set mode to binary so CRLF are not translated to NEWLINE
      fconfigure $fd -translation binary
    }
    set inarray(SECTLEN) $sectionLength
    if {$verbose} {puts "sectionLength $sectionLength"}
  }
  
  # GET SEGMENT
  if { $inarray(CMD) == 2 } {
    if {$verbose} {puts "command get segment"}
    
    set inarray(BUF) [read $fd $inarray(MAXLEN)]
    if {$verbose} {puts "bytes read $inarray(BUF)"}
    
    # if 0 bytes were read, close file for this section
    if { [llength $inarray(BUF)] == 0 } {
      close $fd
      set fd 0
      if {$verbose} {puts "no bytes left in section, file closed"}
    }
  } 
  
  # SECTION READY
  if { $inarray(CMD) == 3 } {
    if {$verbose} {puts "command section ready"}
        
    # set fileName based on input array fields
    set newFileName file$inarray(IOA)_$inarray(FNAME)_$inarray(SECTNAME).txt
        
    # close the old section file
    if { $fd != 0 } {
      close $fd
      set fd 0
    }
        
    # open the file to hold new section bytes
    if { $fd == 0 } {
      set fd [open $newFileName w+]
      set fileName $newFileName
    }
  } 
    
  # STORE SEGMENT
  if { $inarray(CMD) == 4 } {
    if {$verbose} {puts "command store segment"}
     
    # if file is open write segment bytes 
    if { $fd != 0 } {
      if {$verbose} {puts "$inarray(BUF)"}
      puts -nonewline $fd $inarray(BUF)
      flush $fd
    }
  } 
    
  # CLOSE FILE
  if { $inarray(CMD) == 5 } {
    if {$verbose} {puts "command close file"}
    if { $fd != 0 } {
      close $fd
      set fd 0
    }
  }
    
  # DELETE FILE
  if { $inarray(CMD) == 6 } {
    if {$verbose} {puts "command delete file"}
    set sectionName 1
    
    while { 1 } {
      set fileName file$inarray(IOA)_$inarray(FNAME)_$sectionName.txt
      if {[file exists $fileName]} {
        file delete $fileName
        if {$verbose} {puts "delete $fileName"}
        incr sectionName
      } else { 
        break
      }
    }
  }
}

# proc: run104master
# purpose: Issue a series of requests to the current 104 master
#  session
proc run104master {} {
  
  # Issue requests from master
  puts "Issue single point command"
  csc4
  
  puts "Issue double point command"
  cdc4
  
  puts "Issue step position command"
  crc4
  
  puts "Issue bitstring command"
  cbo4
  
  puts "Issue normalized measurand command"
  csena4
  
  puts "Issue scaled measurand command"
  csenb4
  
  puts "Issue floating point measurand command"
  csenc4
  
  puts "Issue clock sync command"
  clk4
  
  puts "Parameter of Measurand commnds"
  setParOfMeasurands4
  
  # Issue a general interrogation request
  puts "Issue general interrogation"
  gi4 false
  
  # Issue a counter interrogation request
  puts "Issue counter interrogation"
  ci4 false
  
  # Freeze counters
  puts "Freeze counters"
  frz4
  
  # File Transfer
    
  puts "Issue file transfer directory request" 
  ftdir4
    
  puts "Issue file transfer put request using low level commands"
  ftput4
  
  puts "Issue file transfer get request using low level commands"
  ftget4
    
  puts "Issue file transfer high level get and put commands"
  fttest4
   
  #puts "Issue file transfer high level get and put commands with Tcl callback"
  #fttest4_wcallback
    
  #puts "Issue file transfer delete request"
  #ftdelete4
}

# proc: run104slave
# purpose: Issue a series of requests to the current 104 slave
#  session
proc run104slave {} {
  puts "Setting single point"
  msp4
  
  puts "Setting double point"
  mdp4
  
  puts "Setting step position"
  mst4
  
  puts "Setting bitstring"
  mbo4
  
  puts "Setting normalized measurand"
  mmena4
  
  puts "Setting scaled measurand"
  mmenb4
  
  puts "Setting floating point measurand"
  mmenc4
  
  puts "Setting integrated total"
  mit4
  
  puts "Adding measurands to database"
  addMeasurands4
  
  puts "Show database"
  md4
}

# proc: run104
# purpose: Issue a series of requests to the current 104 master
#  and slave sessions
proc run104 {} {
  # Run slave commands
  puts "Run IEC 60870-5-104 Slave Functions"
  run104slave
  
  # Wait 10 seconds for things to settle down
  puts "Waiting for events"
  set delay 0
  after 10000 {set delay 1}
  vwait delay
  
  # Run master commands
  puts "Run IEC 60870-5-104 Master Functions"
  run104master
  
  puts "run104 complete"
}


# proc: crdna (written by sid)
# purpose: read IOA value
# arguments:
#  mioa IOA to read from ( Send read request too)
proc crdna4 {mioa} {
  global msctr ssctr status m104data
  
  set status 1
  m104crdna statvariable status sector $msctr ioa $mioa datavariable m104data
  while {$status != 0} {
    vwait status
    if {$status > 1} {
       return "Returned status = $status"
  }
  }
  }
  
proc sleepforseconds { {time} {action} } {
# Turn off output buffering; important!
#fconfigure stdout -buffering none

set deadline [expr {[clock seconds] + $time}]
while 1 {
    set now [clock seconds]
    if {$deadline <= $now} {
        break
    }

    # Tricky; the CR is critical to making this work...
    puts -nonewline "\rTime left to $action  [expr {$deadline - $now}] "

    # Sleep *half* a second for accuracy
    after 500
}

# Blank the countdown line and put a finished message with a cheap hack
puts -nonewline "[string repeat " " 15]\rTimeup!"
}

# proc: testproc (written by sid)
# purpose: 
# arguments:
# 
proc testproc {} {
  global msctr ssctr status m104data
  
  set status 1
  m104ctsta statvariable status sector $msctr datavariable m104data
  while {$status != 0} {
    vwait status
    if {$status > 1} {
       return "Returned status = $status"
  }
  }
  }

  
  
  # proc: csc4time
# purpose: Issue one or more set single point value requests with time tag
# arguments:
#  start - first point to set
#  end - last point to set, 0 means only do 1
proc csc4time {{start 0} {end 0} {valueToSet 0} {modeparam "auto"}} {
  global msctr status m104data
  
  if {$end == 0} {
    set end $start
  }
  
  for {set index $start} {$index <= $end} {incr index} {
    set status 1
    m104cscta statvariable status sector $msctr ioa $index value $valueToSet mode $modeparam datavariable m104data
    while {$status != 0} {
      vwait status
      if {$status > 1} {
		 return "Returned status = $status"
      #  return -code error "Returned status = $status" removed because -code option creates exception
      }
    }
  }
}