These are the test script files & G3 Config file for automating the 104 slave conformance tests.These scripts are compatible with version 3.16.1.0 of TMW test harness and the installer can be found at 

http://10.11.0.6:81/svn/auto_thirdparty_tools/tags/TriangleTestHarness/v3.16.1.0.

104Tests.tcl (to be added to the root of the protocol test harness directory) 
Main104.tcl (to be added to the root of the protocol test harness directory)
tmwstart.tcl (to be replaced at the root of the protocol test harness directory)
XMLparser.tcl (to be added to the root of the protocol test harness directory)
Tmw104.tcl (to be replaced in examples folder)
G3104SlaveConformanceConfig.xml(to be added to the root of the protocol test harness directory) , this is the config file. This shouldn�t be modified, except for the IOA numbers which can be modified.*This runs successfully with SCM revision 9916 of config tool 
If editing the IOA`s numbers, the config tool with Schema version 6 needs to be used.
G3104SlaveRedundancyConformanceConfig.xml(used excusively for checking redundancy).*This runs successfully with SCM revision 10055 of config tool 
