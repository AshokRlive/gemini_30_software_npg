#
# file: tmwstart.tcl
# purpose: This is the default startup script for the Triangle MicroWorks, Inc.
#  Protocol Test Harness. This script defines procedures that can be executed
#  from the test harness command line. Add additional procedures or scripts here
#  as desired for the target environment.
#

# procedure: filter
# purpose: Change the filter level for the protocol analyzer display
# The default tmwlog command allows you to filter each layer independently.
# This procedure provides a different mechanism that allows the user to
# simply type the level of filtering desired (i.e. 'tmw> filter 3').
#
# Once defined this procedure can be called at any time from the
# test harness command line or from a script etc. By grouping commonly
# used sequences of commands and creating procedures like this you can
# easily customize the test harness to support your needs.
proc filter {{level 2}} {
  # Enable/Disable layers based on level parameter
  tmwlog filter user [expr $level > 0]
  tmwlog filter application [expr $level > 1]
  tmwlog filter transport [expr $level > 2]
  tmwlog filter link [expr $level > 3]
  tmwlog filter physical [expr $level > 4]
  return
}

# procedure: dir
# purpose: implement a basic directory command
proc dir {{path *}} {
  foreach file [glob $path] {
    set type [file type $file]
    set size [file size $file]
    puts [format "%-40s %-10s %8d" $file $type $size]
  }
}

# procedure: cl
# purpose: Define a procedure to clear the protocol analyzer display.
# Typing two letters is often more convenient than pressing the
# 'Clear Display' button in the user interface.
proc cl {} {
  tmwlog clear
}


# procedure puts
#  replace puts with our own function that will call regular puts if more than one argument
#  or tmwputs if only one argument. This will allow puts to output to command window.
rename puts _puts
proc puts { {x} {y ""} {z ""}} {
 if {$z != ""} {
   _puts $x $y $z
 } elseif {$y != ""} {
   if {$x != "-nonewline"} {
     _puts $x $y
   } else {
     tmwputs $x $y
   }
 } else {
   tmwputs $x
 }
}


proc removeFromTclList { listName elementName } {
  upvar $listName list
  set index [lsearch $list $elementName] 
  if {$index >= 0} {
    set $listName [lreplace $list $index $index]
  }  
}



# procedure: showarray
# purpose: Define a procedure used to show a Tcl array. This procedure
# is used frequently to display the response to a request or an unsolicted
# response.
proc showarray {inputarray} {
  upvar $inputarray inarray
  puts "\nContents of Array $inputarray"
  foreach index [lsort -dictionary [array names inarray]] {
    puts "$inputarray\($index\) = $inarray($index)"
  }
}

# proc: tmwdays, tmwminutes, tmwseconds
# purpose: Procedures to turn specified units of time into milliseconds
proc tmwseconds {value} {
  return [expr $value * 1000]
}

proc tmwminutes {value} {
  return [expr $value * 60 * 1000]
}

proc tmwhours {value} {
  return [expr $value * 60 * 60 * 1000]
}

proc tmwdays {value} {
  return [expr $value * 24 * 60 * 60 * 1000]
}

# proc: repeat
# purpose: Specify a command to be executed periodically
# arguments:
#  cmd - the Tcl command to execute
# returns:
#  id to be used to cancel or monitor periodic commands
proc repeat {cmd period} {
  global repeattable

  eval $cmd

  set i 0
  while {[info exists repeattable(repeat#$i,cmd)]} {
    incr i
  }

  set id repeat#$i

  set repeattable($id,cmd) $cmd
  set repeattable($id,period) $period
  set repeattable($id,afterid) [after $period "lowrepeat $id"]

  return $id
}

# proc: showrepeat
# purpose: Show all current periodic commands
# arguments:
#  none
# returns:
#  nothing
proc showrepeat {} {
  global repeattable
  showarray repeattable
}

# proc: cancelrepeat
# purpose: Cancel a periodic command
# arguments:
#  id - periodic id returned from periodic procedure
# returns:
#  nothing
proc cancelrepeat {id} {
  global repeattable
  after cancel $repeattable($id,afterid)
  unset repeattable($id,afterid)
  unset repeattable($id,cmd)
  unset repeattable($id,period)
}

# proc: lowrepeat
# purpose: Internal routine used to repeat a periodic command
# arguments:
#  id - periodic id returned from periodic procedure
# returns:
#  nothing
proc lowrepeat {id} {
  global repeattable
  eval $repeattable($id,cmd)
  set $repeattable($id,afterid) [after $repeattable($id,period) "lowrepeat $id"]
}

# proc: SdnpVtermCommand
# purpose: routine intended to be called by sdnp virtual terminal point
#  to run TCL command. This routine is automatically registered for
#  sdnp vterm points 0 and 1
# arguments:
#  pointNumber - virtual terminal point number
# returns:
#  result of command executed
proc SdnpVtermCommand {pointNumber} {

  set x [eval [sdnpvterm get point $pointNumber value]]

  set maxSize 253
  set start 0
  set fragmentEnd $maxSize
  set endIndex [expr [string length $x] -1]

  while {$fragmentEnd < $endIndex} {
    sdnpvterm set point $pointNumber value [string range $x $start $fragmentEnd]
    set fragmentEnd [expr $fragmentEnd + $maxSize +1]
    set start [expr $start + $maxSize + 1]
  }

  sdnpvterm set point $pointNumber value [string range $x $start $endIndex]
}

# proc: AutoOpenDNPSession
# purpose: routine intended to be called by DNP channel callback when
#  a frame is received for a session that is not opened. To enable this
#  callback when opening a DNP channel set autoopensession to true
# arguments:
#  channelId - id of channel that frame was received on
#  srcAddress - address to user for source parameter (was destination in received frame)
#  destAddress - address to use for destination parameter (was source in received frame)
#  type - 0 if master, 1 if slave
# returns:
#  void
proc AutoOpenDNPSession {channelId srcAddress destAddress type} {

  # type 0 is master
  if {$type == 0} {
    puts "DNP master session automatically opened by AutoOpenDNPSession in tmwstart.tcl"
    if {[catch {mdnpopensession channel $channelId source $srcAddress destination $destAddress} msg]} {
      puts "failed to open session, $msg"
    }
  } else {
    puts "DNP slave session automatically opened by AutoOpenDNPSession in tmwstart.tcl"
    if {[catch {sdnpopensession channel $channelId source $srcAddress destination $destAddress} msg]} {
      puts "failed to open session, $msg"
    }
  }
}

# proc: OpenDNPMonitorSession 
# purpose: monitor mode routine intended to be called by DNP channel callback when
#  a frame is received for a session that is not opened. To enable this
#  callback when opening a DNP channel set monitor to true
# arguments:
#  channelId - id of channel that frame was received on
#  srcAddress - address to user for source parameter (was destination in received frame)
#  destAddress - address to use for destination parameter (was source in received frame)
#  type - 0 if master, 1 if slave
# returns:
#  void
proc OpenDNPMonitorSession {channelId srcAddress destAddress type} {
 
  # type 0 is master
  if {$type == 0} {
    puts "DNP master session automatically opened by OpenDNPMonitorSession in tmwstart.tcl"
    if {[catch {mdnpopensession channel $channelId source $srcAddress destination $destAddress auto 0} msg]} {
      puts "failed to open session, $msg"
    }
  } else {
    puts "DNP slave session automatically opened by OpenDNPMonitorSession in tmwstart.tcl"
    if {[catch {sdnpopensession channel $channelId source $srcAddress destination $destAddress} msg]} {
      puts "failed to open session, $msg"
    } else {
      sdnpdb action clear
    }    
  } 
}


# proc: OpenMonitorSession
# purpose: monitor mode routine intended to be called when
#  a message is received for a session that is not opened. To enable this
#  callback when opening a channel set monitor to true
# arguments:
#  sessionId - id of session that message was received on
#  protocol - which protocol 
#  address - address to use for sector
#  type - 0 if master, 1 if slave
# returns:
#  void
proc OpenMonitorSession {channelId protocol type address asduAddrSize cotSize ioaSize} {
 
  # protocol 0 is 101
  if {$protocol == 0} {
    # type 0 is master
    if {$type == 0} {
      puts "101 master session automatically opened by OpenMonitorSession in tmwstart.tcl"
      if {[catch {m101opensession channel $channelId address $address asduSize $asduAddrSize cotSize $cotSize ioaSize $ioaSize} msg]} {
        puts "failed to open sector, $msg"
      }
    } else {
      puts "101 slave session automatically opened by OpenMonitorSession in tmwstart.tcl"
      if {[catch {s101opensession channel $channelId address $address asduSize $asduAddrSize cotSize $cotSize ioaSize $ioaSize} msg]} {
        puts "failed to open session, $msg"
      }   
    } 
    # protocol 2 is 103
  } elseif {$protocol == 2} {
    # type 0 is master
    if {$type == 0} {
      puts "103 master session automatically opened by OpenMonitorSession in tmwstart.tcl"
      if {[catch {m103opensession channel $channelId address $address} msg]} {
        puts "failed to open sector, $msg"
      }
    } else {
      puts "103 slave session automatically opened by OpenMonitorSession in tmwstart.tcl"
      if {[catch {s103opensession channel $channelId address $address} msg]} {
        puts "failed to open session, $msg"
      }   
    } 
  
  # protocol 3 is 104
  } elseif {$protocol == 3} {
    # type 0 is master
    if {$type == 0} {
      puts "104 master session automatically opened by OpenMonitorSession in tmwstart.tcl"
      if {[catch {m104opensession channel $channelId} msg]} {
        puts "failed to open sector, $msg"
      }
    } else {
      puts "104 slave session automatically opened by OpenMonitorSession in tmwstart.tcl"
      if {[catch {s104opensession channel $channelId} msg]} {
        puts "failed to open session, $msg"
      }   
    }   

  # protocol 5 is Modbus
  } elseif {$protocol == 5} {

    # type 0 is master
    if {$type == 0} {
      puts "Modbus master session automatically opened by OpenMonitorSession in tmwstart.tcl"
      if {[catch {mmbopensession channel $channelId address $address} msg]} {
        puts "failed to open sector, $msg"
      }
    } else {
      puts "Modbus slave session automatically opened by OpenMonitorSession in tmwstart.tcl"
      if {[catch {smbopensession channel $channelId address $address} msg]} {
        puts "failed to open session, $msg"
      } else {
        smbdb action clear
      }
    }   
  }
}

# proc: OpenI870MonitorSector
# purpose: monitor mode routine intended to be called when
#  a message is received for a sector that is not opened. To enable this
#  callback when opening a I870 channel set monitor to true
# arguments:
#  sessionId - id of session that message was received on
#  protocol - which protocol 
#  address - address to use for sector
#  type - 0 if master, 1 if slave
# returns:
#  void
proc OpenI870MonitorSector {sessionId protocol type address} {
 
  # protocol 0 is 101
  if {$protocol == 0} {
    # type 0 is master
    if {$type == 0} {
      puts "101 master sector automatically opened by OpenI870MonitorSector in tmwstart.tcl"
      if {[catch {m101opensector session $sessionId address $address} msg]} {
        puts "failed to open sector, $msg"
      }
    } else {
      puts "101 slave sector automatically opened by OpenI870MonitorSector in tmwstart.tcl"
      if {[catch {s101opensector session $sessionId address $address cyclic 0} msg]} {
        puts "failed to open sector, $msg"
      } else {
        s101db action clear
      }   
    } 
  } elseif {$protocol == 2} {
    # type 0 is master
    if {$type == 0} {
      puts "103 master sector automatically opened by OpenI870MonitorSector in tmwstart.tcl"
      if {[catch {m103opensector session $sessionId address $address} msg]} {
        puts "failed to open sector, $msg"
      }
    } else {
      puts "103 slave sector automatically opened by OpenI870MonitorSession in tmwstart.tcl"
      if {[catch {s103opensector session $sessionId address $address} msg]} {
        puts "failed to open session, $msg"
      } else {
      s103db action clear
      }     
    } 
  
  # protocol 3 is 104
  } elseif {$protocol == 3} {
    # type 0 is master
    if {$type == 0} {
      puts "104 master sector automatically opened by OpenI870MonitorSector in tmwstart.tcl"
      if {[catch {m104opensector session $sessionId address $address} msg]} {
        puts "failed to open sector, $msg"
      }
    } else {
      puts "104 slave sector automatically opened by OpenI870MonitorSector in tmwstart.tcl"
      if {[catch {s104opensector session $sessionId address $address cyclic 0} msg]} {
        puts "failed to open sector, $msg"
      } else {
        s104db action clear
      }   
    }   
  }  
}

# Source the example scripts for each protocol if they are installed.
# This will load procedures that can be used to test each protocol. For
# more information see the test harness user manual and the source for
# example scripts.

# tmwlog clear
tmwlog insert [tmwconfig about]
tmwlog insert "\nType 'help' for information on test harness commands or"
tmwlog insert "use the Help Menu above to access the Manual and Quick Start Guide"
tmwlog insert "\nLoading startup script (tmwstart.tcl)"

# Initialize the simulator list to the default database options.
::TMWOPEN::InitSimulatorList


# IEC 60870-5-101
if {[tmwlicense validate 101]} {
  if {[file exists "examples/tmw101.tcl"]} {
    tmwlog insert "Loading IEC 60870-5-101 examples (tmw101.tcl)"
    source "examples/tmw101.tcl"
  }
   
  # Load S101 Conformance Test Scripts if they were installed
  if {![catch {package require S101Test} msg]} {
    namespace import ::S101Test::*
  }
} else {
  tmwlog insert "\nIEC 60870-5-101 not licensed"
}   

# IEC 60870-5-102
if {[tmwlicense validate 102]} {
  if {[file exists "examples/tmw102.tcl"]} {
    tmwlog insert "Loading IEC 60870-5-102 examples (tmw102.tcl)"
    source "examples/tmw102.tcl"
  }
} else {
  tmwlog insert "\nIEC 60870-5-102 not licensed"
}   

# IEC 60870-5-103
if {[tmwlicense validate 103]} {
  if {[file exists "examples/tmw103.tcl"]} {
    tmwlog insert "Loading IEC 60870-5-103 examples (tmw103.tcl)"
    source "examples/tmw103.tcl"
  }
} else {
  tmwlog insert "\nIEC 60870-5-103 not licensed"
}   

# IEC 60870-5-104
if {[tmwlicense validate 104]} {
  if {[file exists "examples/tmw104.tcl"]} {
    tmwlog insert "Loading IEC 60870-5-104 examples (tmw104.tcl)"
    source "examples/tmw104.tcl"
  }
} else {
  tmwlog insert "\nIEC 60870-5-104 not licensed"
}   

# DNP3
if {[tmwlicense validate dnp]} {
  if {[file exists "examples/tmwdnp.tcl"]} {
    tmwlog insert "Loading DNP3 examples (tmwdnp.tcl)"
    source "examples/tmwdnp.tcl"

    # Load SDNP Conformance Test Scripts if they were installed
    if {![catch {package require SDNPTest} msg]} {
      namespace import ::SDNPTest::*
    }
    
    # Load Example code for Omicron 
    if {[file exists "./examples/Conformance Test Utilities/Omicron.tcl"]} {
      tmwlog insert "Loading OmicronConformance file"
      source "./examples/Conformance Test Utilities/Omicron.tcl"
    }
      
    # Load Example code for Virtual Terminal Support
    if {[file exists "./examples/Conformance Test Utilities/Virtual Terminal.tcl"]} {
      tmwlog insert "Loading VirtualTerminal file "
      source "./examples/Conformance Test Utilities/Virtual Terminal.tcl"
    }

    # Load simulators if installed
    if {[file exists ./Simulators]} {
      # Load Generic DNP3 slave simulator
      if {![catch {package require SDNPSim} msg]} {
        namespace import ::SDNPSim::*
        ::TMWOPEN::RegisterSimulator DNP "Generic Slave Simulator" ::SDNPSim::SDNPSimSession
      }

      # Load ABB PCD DNP3 slave simulator
      if {![catch {package require ABBSim} msg]} {
        namespace import ::ABBSim::*
        ::TMWOPEN::RegisterSimulator DNP "ABB PCD Simulator" ::ABBSim::ABBSimSession
      }

      # Load SEL DNP3 slave simulator
      if {![catch {package require SELSim} msg]} {
        namespace import ::SELSim::*
        ::TMWOPEN::RegisterSimulator DNP "SEL 351S Simulator" ::SELSim::SELSimSession
      }
    }
  }
} else {
  tmwlog insert "\nDNP3 not licensed"
}   

# Modbus
if {[tmwlicense validate mb]} {
  if {[file exists "examples/tmwmb.tcl"]} {
    tmwlog insert "Loading Modbus examples (tmwmb.tcl)"
    source "examples/tmwmb.tcl"

    # Load Modbus Slave Conformance Test Scripts if they were installed
    if {![catch {package require SMBTest} msg]} {
      namespace import ::SMBTest::*
    }
    
    # Load Example code for Omicron 
    if {[file exists "./examples/Conformance Test Utilities/Omicron.tcl"]} {
      tmwlog insert "Loading OmicronConformance file"
      source "./examples/Conformance Test Utilities/Omicron.tcl"
    }
     
    # Load simulators if installed
    if {[file exists ./Simulators]} {
      # Load Generic MODBUS slave simulator
      if {![catch {package require SMBSim} msg]} {
        namespace import ::SMBSim::*
        ::TMWOPEN::RegisterSimulator MB "Generic Slave Simulator" ::SMBSim::SMBSimSession
      }
    }
  }
} else {
  tmwlog insert "\nModbus not licensed"
}   

tmwlog insert "\nReference 'Examples' from Start menu for details"
tmwlog insert "on the defined procedures"

# SDG Tests

proc setCheckboxesOn {} {
   tmwlog filter link 1
   tmwlog filter physical 1
   tmwlog filter application 1
   tmwlog filter user 1
   tmwlog filter transport 1
   tmwlog filter mmi 1
 }

proc setCheckboxesOff {} {
   tmwlog filter link 0
   tmwlog filter physical 0
   tmwlog filter application 0
   tmwlog filter user 0
   tmwlog filter transport 0
   tmwlog filter mmi 0
 }

if {[file exists "/code/gateway/test/sdgmain.tcl"]} {
  tmwlog insert "Loading SCADA Data Gateway Tests (sdgmain.tcl)"
  source "/code/gateway/test/sdgmain.tcl"
}

# Go ahead and setup our default environment
#filter 2

# If 2 command line arguments are passed to tmwtest.exe, assume :
# the 1st arg is tmwtest.tcl
# and the 2nd arg is some script the user wants to excute
# or a command he wants to evaluate (it will not have ".tcl" in it).
# Additional command line arguments are available to the user
# script in $argv.
#

#if {$argc > 1} {
#  set arg [lindex $argv 1]
#  if {[file exists "$arg"]} {
#    tmwlog insert "Loading command argument ($arg)\n"
#    source "$arg"
#  } else {
#    eval $arg
#  }
#} else {
  # The following command opens the load workspace dialog box on startup.  Place a comment character '#'
  # in front of the command below to disable this feature.
  # To load a favorite workspace file on startup every time, add it to the command below.  For example :
  # ::TMWGUI::LoadWorkspace "workspace/dnpsample.tcl"
  # ::TMWGUI::LoadWorkspace
#}