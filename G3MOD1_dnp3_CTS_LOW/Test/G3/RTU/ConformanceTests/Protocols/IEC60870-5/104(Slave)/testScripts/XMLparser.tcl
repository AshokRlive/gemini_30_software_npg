package require tdom
variable staticIP
variable scmIOA dummyswitchIOA cdcIOA scmStatusIOA operatingIOA mitIOA dummyStatusIOA selectTimeoutMs

proc parseXML {} {
global staticIP scmIOA dummyswitchIOA cdcIOA scmStatusIOA dummyStatusIOA operatingIOA mitIOA selectTimeoutMs

tmwlog insert "Entering XML Parser"
set fp [open "G3104SlaveConformanceConfig.xml" r] ;#Open the G3Config.xml file
set XML [read $fp]       ;#read the file into a variable XML
regsub -all \" $XML ' XML  ;#replace all double quotes of attribute values to single quotes, The advantage of using single quotes is that they don't need to be escaped in Tcl.

set doc [dom parse $XML]
set root [$doc documentElement]

#get the staticIP of the RTU to connect too
set staticIPnode [$root getElementsByTagNameNS g3.configuration.ports staticIP] 
set staticIP [$staticIPnode getAttribute ipaddress]

#get the IOA of the open Status indicator
set node [$root selectNodes "scadaProtocol/sIEC104/channel/session/iomap/msp\[1\]"] 
set scmStatusIOA [$node getAttribute protocolID]

set node [$root selectNodes "scadaProtocol/sIEC104/channel/session/iomap/msp\[2\]"] 
set operatingIOA [$node getAttribute protocolID]

set node [$root selectNodes "scadaProtocol/sIEC104/channel/session/iomap/msp\[4\]"] 
set dummyStatusIOA [$node getAttribute protocolID]

set node [$root selectNodes "scadaProtocol/sIEC104/channel/session/iomap/csc\[1\]"] 
set scmIOA [$node getAttribute protocolID]

set node [$root selectNodes "scadaProtocol/sIEC104/channel/session/iomap/csc\[2\]"] 
set dummyswitchIOA [$node getAttribute protocolID]
	
set node [$root selectNodes "scadaProtocol/sIEC104/channel/session/iomap/cdc"] 
set cdcIOA [$node getAttribute protocolID]

set node [$root selectNodes "scadaProtocol/sIEC104/channel/session/iomap/mit\[1\]"] 
set mitIOA [$node getAttribute protocolID]

set node [$root selectNodes "scadaProtocol/sIEC104/channel/session/config"] 
set selectTimeoutMs [$node getAttribute selectTimeoutMs]

#set sIEC104node [$root selectNodes scadaProtocol/sIEC104]
tmwlog insert "Exiting XML Parser"
}
