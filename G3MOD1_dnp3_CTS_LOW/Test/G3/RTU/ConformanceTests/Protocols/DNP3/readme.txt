This folder has the devprofile.xml which is the device profile in the XML format.
devprofile.xml is to be read/edited by DNP3Forge which can be got from http://10.11.0.6:81/svn/auto_thirdparty_tools/tags/TriangleTestHarness/3.16.1.0
DNP3Forge generates a .htm document from the XML.

THE DNP3Conformancetest_Base_Config.xml is the base config for the DNP3 slave testing. This needs to be modified during the
various steps of testing (as prompted by the test harness). The IP address also needs to be modified to suit the RTU under test.

TMW_Test_Harness_DNP_Settings.tmw is the settings for the Test Harness. The test harness is located at http://10.11.0.6:81/svn/auto_thirdparty_tools/tags/TriangleTestHarness/3.16.1.0.




